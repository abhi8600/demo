#import <UIKit/UIKit.h>

#import "TDGLegend.h"
#import "TDGLabel.h"
#import "TDGLegend.h"
#import "TDGPropertyList.h"
#import "TDGElementList.h"

#import "TDGBox.h"
#import "TDGSeries.h"
#import "TDGAnnotationMgr.h"
#import "TDGBLAProperties.h"
#import "TDGPieProperties.h"
#import "TDG3DProperties.h"

#import "TDGOrdinalAxis.h"
#import "TDGNumericAxis.h"

#import "DataProtocols/TDGDataProtocol.h"
#import "TDGDetectionElement.h"
	
#import "graphptr.h"

/** Defines chart types and basic chart elements
*/
#define TDG_SHOW_ALL -1

typedef enum {
	BLAChart,
	PieChart,
	ScatterChart,
	BubbleChart,
	GaugeChart,
	StockChart,
	_3DBarChart,
	WaterfallChart,
} TDGChartTypes;

typedef enum {
	colorByValue,
	colorBySeries,
	colorByGroup,
} TDGDataColorMode;

/** 
* Defines chart types and basic chart elements
*/
@interface TDGChart : NSObject {

	/**
	 Chart Background Box. 
	Defines the background box for the overall chart. 
	This is the overall box that fills the entire view context passed to the chart library.
	*/
	TDGBox* box;

	/** Chart Background Size.
	 This property defines the size of the chart background.
	 */
	CGRect bounds;
	
	/**
	Chart Frame. 
	Defines the frame for the chart data.
	The chart frame is the area in which all data is drawn.
	For chart with vertical or horizontal axes, the axes are drawn on the border of the chart frame.
	Chart Titles and legends (if visible) are drawn outside the frame (i.e., between the frame and chart background box).
	*/
	TDGBox* frame;
	
	/** Chart Frame Size.
	 This property defines the size of the chart frame.
	 */
	CGRect frameRect;

	/**
	Chart Legend. 
	This property defines the chart legend area and text.
	*/
	TDGLegend* legend;

	/**
	Chart Type. 
	This property defines the chart type using a value from the TDGChartTypes enum.
	*/
	TDGChartTypes chartType;
	
	/**
	 Number of values in one data point.
	 The number of values necessary to define a single data point for a given chart type.
	 */
	int sizeOfDataPoint;

	/**
	Chart Color Mode. 
	This property defines the chart color mode: 0=Color by Value, 1=Color by Series (default), 2=Color by Group.
	*/
	TDGDataColorMode colorMode;

	/**
	Use Depth. 
	This property defines whether or not a 2.5D depth effect is applied to the chart.
	*/
	BOOL useDepth;

	/**
	Automatic Layout. 
	This property enables/disables automatic layout of chart elements.
	*/
	BOOL autoLayout;

	/**
	Chart Title. 
	This property defines the Chart title string and format as defined in TDGLabel.h.
	*/
	TDGLabel* title;

	/**
	Chart Subtitle. 
	This property defines the Chart subtitle string and format as defined in TDGLabel.h.
	*/
	TDGLabel* subtitle;

	/**
	Chart Footnote. 
	This property defines the Chart footnote string and format as defined in TDGLabel.h. The footnote is normally drawn in the lower right corner of the chart frame.
	*/
	TDGLabel* footnote;

	/**
	Annotation Manager.
	This property defines user-defined areas, lines, and labels.
	*/
	TDGAnnotationMgr* annotationManager;

	/**
	Ordinal Axis.
	This property defines the chart's non-numeric, ordinal axis.
	*/
	TDGOrdinalAxis* ordinalAxis;

	/**
	Numeric Y-Axis.
	This property defines the chart's numeric Y-Axis.
	*/
	TDGNumericAxis* numericYAxis;

	/**
	Numeric X-Axis.
	This property defines the chart's numeric X-Axis.
	*/
	TDGNumericAxis* numericXAxis;
	
	/** Swap Series/Group Orientation. 
	TRUE = Series is a column & group is a row in the data matrix. FALSE = Series is a row & group is a column in the data matrix
	*/
	BOOL swapSeriesGroups;

	/** Data Manager. */
	id <TDGDataProtocol> dataManager;

	/** Data Text font format.
	 Controls the font, color & rotation of the data text labels drawn on each riser
	 */
	TDGTextFormat* dataTextFormat;
	
	/** Bar/Line/Area Chart Properties.
	This property defines a group of properties that are applicable to Bar/Line/Area (BLA) charts.
	*/
	TDGBLAProperties* blaProps;

	/** Pie Chart Properties.
	This properties defines pie chart properties as defined in TDGPieProperties.h
	*/
	TDGPieProperties* pieProps;

	/** 
	This property defines generic 3D Charts Properties as defined in TDG3DProperties.h
	*/
	TDG3DProperties* threeDProps;

	/** Default Series. */
	TDGSeries* defaultSeries;
	
	@private
	DrawEnvPtr pDE;
	GraphPtr pGraph;
	NSMutableDictionary* seriesDict;
}

@property(nonatomic, retain) TDGBox* box;
@property(nonatomic, assign) CGRect bounds;
@property(nonatomic, retain) TDGBox* frame;
@property(nonatomic, assign) CGRect frameRect;

@property(nonatomic, retain) TDGLegend* legend;
@property(nonatomic, assign) TDGChartTypes chartType;
@property(nonatomic, assign, readonly) int sizeOfDataPoint;
@property(nonatomic, assign) TDGDataColorMode colorMode;
@property(nonatomic, retain) TDGLabel* title;
@property(nonatomic, retain) TDGLabel* subtitle;
@property(nonatomic, retain) TDGLabel* footnote;
@property(nonatomic, assign) BOOL useDepth;
@property(nonatomic, assign) BOOL autoLayout;
@property(nonatomic, retain) TDGTextFormat* dataTextFormat;

@property(nonatomic, retain, readonly) TDGAnnotationMgr* annotationManager;

@property(nonatomic, retain) TDGOrdinalAxis* ordinalAxis;
@property(nonatomic, retain) TDGNumericAxis* numericYAxis;
@property(nonatomic, retain) TDGNumericAxis* numericXAxis;

@property(nonatomic, assign) BOOL swapSeriesGroups;
@property(nonatomic, retain) id <TDGDataProtocol> dataManager;
@property(nonatomic, retain) TDGBLAProperties* blaProps;
@property(nonatomic, retain) TDGPieProperties* pieProps;
@property(nonatomic, retain) TDG3DProperties* threeDProps;
@property(nonatomic, retain) TDGSeries* defaultSeries;

/** Do Auto Layout of Chart Elements, */
-(void)doAutoLayout;

/** Draw chart in context. */
-(void)draw:(CGContextRef)context;

/** Deallocate space used to process chart. */
-(void)dealloc;

/** Destroy chart. */
-(void)destroy;

/** Gets the series at the specified index.  
 Throws exception if index is out of bounds.
 */
-(TDGSeries*)seriesAtIndex:(int)index;

/** Get detection element at the specified point. */
-(TDGDetectionElement*)elementAtPoint:(CGPoint)point;

/** 
Get the value of a chart property.
*/
-(void)getValueForProperty:(TDGProperty)prop value:(void *)value;

/** 
Get the value of a chart property for a specified element in a chart.
*/
-(void)getValueForProperty:(TDGProperty)prop value:(void *)value forElement:(TDGElement)element;

/** 
Get the value of a chart property for a specified element and series in a chart.
*/
-(void)getValueForProperty:(TDGProperty)prop value:(void *)value forElement:(TDGElement)element forSeries:(int)series;

/** 
 Get the value of a chart property for a specified element, series and group in a chart.
 */
-(void)getValueForProperty:(TDGProperty)prop value:(void *)value forElement:(TDGElement)element forSeries:(int)series forGroup:(int)group;

/** Set the value of a chart property.
Example Usage: [self setValueForProperty:kTDG_Prop_UseDepth toValue:&bTrue];
*/
-(void)setValueForProperty:(TDGProperty)prop toValue:(void *)value;

/** 
Set the value of a chart property for a specified element in a chart.
*/
-(void)setValueForProperty:(TDGProperty)prop toValue:(void *)value forElement:(TDGElement)element;

/** 
Set the value of a chart property for a specified element and series in a chart.
*/
-(void)setValueForProperty:(TDGProperty)prop toValue:(void *)value forElement:(TDGElement)element forSeries:(int)series;

/** 
 Set the value of a chart property for a specified element, serids and group in a chart.
 */
-(void)setValueForProperty:(TDGProperty)prop toValue:(void *)value forElement:(TDGElement)element forSeries:(int)series forGroup:(int)group;

/**
 Get low level Graph Pointer
 */
-(GraphPtr)getGraphPtr;
@end

