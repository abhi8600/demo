//
//  TDGTextFormat.h
//  objc_pgsdk
//
//  Created by Sam on 11/14/10.
//  Copyright 2010 Three D Graphics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "TDGPropertyList.h"
#import "TDGElementList.h"

@class TDGChart;

typedef enum {
	noRotation = 0,
	rotate45 = 5,
	rotate90 = 2,
	rotate180 = 3,
	rotate270 = 4,
	rotate315 = 6,
} TDGFontRotation;

/** 
* Text Format (Font, Color, Rotation, etc.) Properties.
*/	
@interface TDGTextFormat : NSObject {

	/** Text Color.
	Defines the color of a text object.
	*/
	UIColor* color;

	/** Text Font.
	Defines the font to be applied to a text object.
	*/
	NSString* font;

	/** Font Size.
	Defines the size of the font to be applied to a text object.
	*/
	float fontSize;

	/** Font Rotation.
	Selects a font rotation using a value from the TDGFontRotation enum.
	*/
	TDGFontRotation rotation;
}

@property(nonatomic, retain) UIColor* color;
@property(nonatomic, retain) NSString* font;
@property(nonatomic, assign) float fontSize;
@property(nonatomic, assign) TDGFontRotation rotation;
/** Apply these chart properties */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
