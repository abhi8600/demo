#ifndef __PGSDK_API_GRAPHPTR_H__
#define __PGSDK_API_GRAPHPTR_H__


//#ifdef PG_PUBLIC_API_HEADER
//typedef void * GraphPtr;      //struct _Graph
//typedef void * DrawEnvPtr;    //struct DevGen_Data
//#endif

typedef struct _Graph *      GraphPtr;     
typedef struct DevGen_Data * DrawEnvPtr;


#endif /* __GRAPHPTR_H__ */
