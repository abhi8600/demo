#import <Foundation/Foundation.h>
#import "TDGBox.h"
#import "TDGLabel.h"
#import "TDGTextFormat.h"

typedef enum {
	leftOfLabels = 0,
	rightOfLabels = 1,
	centeredOnLabels = 2,
	aboveLabels = 3,
	belowLabels = 4,
} TDGLegendMarkerPos;

typedef enum {
	leftOfChart = 1,
	rightOfChart = 0,
	belowChart = 2,
	freeFloating = 3,
} TDGLegendPos;

/** Legend Properties
*/
@interface TDGLegend : NSObject {

	/** Legend Title.
	This property defines the legend title text.
	*/
	TDGLabel* title;

	/** Legend Box.
	This property controls the format of the legend box as defined in TDGBox.h
	*/
	TDGBox* box;

	/** Legend Visibility.
	This property controls the visibility of the legend.
	*/
	BOOL visible;

	/** Legend Position.
	This property defines the legend position: Right Side of Chart, Left Side of Chart, Bottom/below chart, or Free Floating.
	*/
	TDGLegendPos legendPos;

	/** Legend Marker Position.
	This property defines the position of legend markers relative to legend labels: Left of labels, Right of labels, Above Labels, or Below Labels.
	*/
	TDGLegendMarkerPos markerPos;

	/** Label Text Format.
	This property defines the format (font, font size, color) of each label in the legend.
	*/
	TDGTextFormat* labelFormat;
}

@property(nonatomic, retain) TDGLabel* title;
@property(nonatomic, retain) TDGBox* box;
@property(nonatomic, assign) BOOL visible;
@property(nonatomic, assign) TDGLegendPos legendPos;
@property(nonatomic, assign) TDGLegendMarkerPos markerPos;
@property(nonatomic, retain) TDGTextFormat* labelFormat;

/** Apply these chart properties. */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
