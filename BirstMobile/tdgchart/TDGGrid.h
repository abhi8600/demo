#import <Foundation/Foundation.h>
#import "TDGLineFormat.h"

typedef enum {
	none = 0,
	grid = 1,
	gridExtended = 2,
	inside = 3,
	outside = 4,
	span = 5
} TDGTickStyles;

/**
* Grid Line Properties
*/
@interface TDGGrid : NSObject {

	/** Show Grid Lines.
	This property controls the visibility of grid lines.
	*/
	BOOL showGridLines;

	/** Grid Line Format.
	This property defines the format (color, width, solid, dot, dash, etc) of grid lines.
	*/
	TDGLineFormat* gridLineFormat;

	/** Tick Line Style.
	This property defines tick line style: None, Inside axis base line, Outside axis base line, Span axis base line.
	*/
	TDGTickStyles tickStyle;
}

@property(nonatomic, assign) BOOL showGridLines;
@property(nonatomic, retain) TDGLineFormat* gridLineFormat;
@property(nonatomic, assign) TDGTickStyles tickStyle;
/** Apply these chart properties. */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
