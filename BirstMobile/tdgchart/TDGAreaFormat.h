//
//  TDGAreaFormat.h
//  objc_pgsdk
//
//  Created by Sam on 11/14/10.
//  Copyright 2010 Three D Graphics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TDGPropertyList.h"
#import "TDGElementList.h"
#import "TDGGradient.h"

@class TDGChart;

/**
* Properties to define the format of area objects
*/
@interface TDGAreaFormat : NSObject {

	/** Area Color. */
    UIColor* color;

    /** Area Gradient. nil if no gradient */
    TDGGradient *gradient;
}

@property(nonatomic, retain) UIColor* color;
@property(nonatomic, retain) TDGGradient* gradient;

/** Apply these properties to an element in the chart */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

/** Apply these properties to an element and series */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element seriesID:(int)seriesID;

@end
