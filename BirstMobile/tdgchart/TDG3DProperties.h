#import <Foundation/Foundation.h>
#import "TDGElementList.h"

#define DEFAULT_VIEW_ANGLE 1

@class TDGChart;

/** 
* Generic 3D Chart Properties.
*/

@interface TDG3DProperties : NSObject {

	/** View Angle.
	This property selects a predefined 3D Cube Viewing Angle.
	*/
	int viewAngleSetting;

	/** X Rotation.
	The property controls the rotation of the 3D cube in the X-direction.
	*/
	int xRotation;

	/** Y Rotation.
	The property controls the rotation of the 3D cube in the Y-direction.
	*/
	int yRotation;

	/** Z Rotation.
	The property controls the rotation of the 3D cube in the Z-direction.
	*/
	int zRotation;
}

@property(nonatomic, assign) int viewAngleSetting;
@property(nonatomic, assign) int xRotation;
@property(nonatomic, assign) int yRotation;
@property(nonatomic, assign) int zRotation;

/** Apply these chart properties */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
