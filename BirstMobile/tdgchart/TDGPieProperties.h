

#import <Foundation/Foundation.h>
#import "TDGElementList.h"
#import "TDGTextFormat.h"

@class TDGChart;

/** 
* Pie Chart Properties.
*/

typedef enum {
	NotShown,
	ShowValue,
	ShowLabel,
	ShowLabelAndValue
} TDGPieLabelDisplay;

@interface TDGPieProperties : NSObject {

	/** Draw Pie Clockwise.
	This property defines the direction pie slices are drawn beginning from the rotation angle defined by rotation. 
	The slices of a pie are drawn in either clockwise or counter clockwise direction.
	*/
	BOOL drawClockwise;

	/** Pie Hole/Ring Size. 
	This property controls the relative size of the ring (inner circle) in a ring pie. 
	*/
	int holeSize;

	/** Pie Rotation.
	This property controls pie rotation (0...359 degrees). 
	0 = No rotation. First slice begins at 3 o'clock position. 
	90 = The pie rotates counter clockwise 90 degrees. 
	The starting angle to draw the first slice of the pie is at 12 o'clock position. 
	359 = The pie rotates counter clockwise 359 degrees.  
	*/
	int rotation;

	/** Pie Tilt.
	This property controls pie tilt (0...89).
	A pie with zero degrees tilt is drawn as a regular 2-D pie. 
	As tilt is applied, the pie is rotated backward on the x-axis. 
	The tilting creates a 3-D effect.
	*/
	int tilt;
	
	/** Pie Data Paging.
	 In a chart with multiple pies, this property controls which pie is currently visible.
	 Valid values range from TDG_SHOW_ALL (-1) up to the number of pie charts in the data set.
	 If this number is zero or greater, only that one pie will be drawn.
	 */
	int visiblePie;
	
	TDGPieLabelDisplay labelDisplay;
	
	BOOL labelOnSlice;
	BOOL valueOnSlice;

	TDGTextFormat* pieTitleTextFormat;
	TDGTextFormat* sliceTextFormat;
}

@property(nonatomic, assign) BOOL drawClockwise;
@property(nonatomic, assign) int holeSize;
@property(nonatomic, assign) int rotation;
@property(nonatomic, assign) int tilt;
@property(nonatomic, assign) int visiblePie;
@property(nonatomic, assign) TDGPieLabelDisplay labelDisplay;
@property(nonatomic, assign) BOOL labelOnSlice;
@property(nonatomic, assign) BOOL valueOnSlice;
@property(nonatomic, retain) TDGTextFormat* pieTitleTextFormat;
@property(nonatomic, retain) TDGTextFormat* sliceTextFormat;

/** Apply these chart properties */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
