#import <UIKit/UIKit.h>
#import "TDGTextFormat.h"

/**
* Label Properties
*/
@interface TDGLabel : NSObject {

	/** Label Text String.
	This property defines the text to draw for a label.
	*/
	NSString* text;

	/** Label Text Format.
	This property defines the format (font, font size, color) of a label.
	*/
	TDGTextFormat* format;
	
	/** Label Visibility.
	This property controls the visibility of a label.
	*/
	BOOL visible;
}

@property(nonatomic, retain) NSString* text;
@property(nonatomic, retain) TDGTextFormat* format;
@property(nonatomic, assign) BOOL visible;
/**
 Apply chart properties.
 */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
