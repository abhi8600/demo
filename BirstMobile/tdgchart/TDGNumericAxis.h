#import <Foundation/Foundation.h>
#import "TDGAxis.h"
#import "TDGLineFormat.h"

typedef enum {
	linear,
	logarithmic
} TDGAxisScaleType;

typedef enum {
	doNotDraw,
	drawAtMax,
	clipDrawing
} TDGOffscaleOptions;

/**
* Numeric Axis Properties
*/
@interface TDGNumericAxis : TDGAxis {
	
	/** Axis Scale Type.
	 This property defines the scale type: 0=Linear, 1=Logarithmic.<br>
	 */
	TDGAxisScaleType scaleType;
	
	/** Off-Scale Options.
	 This property defines how risers are drawn to represent off-scale values: <br>
	 0=Don't draw the riser<br>1=Draw at scale maximum<br>2=Clip to frame edge.<br>
	 */
	TDGOffscaleOptions offscale;
	
	/** Use Automatic Scale.
	 This property enables/disables automatic calculation of a numeric X- or Y-Axis scale.
	 */
	BOOL useAutoScale;
	
	/** Scale Minimum Value.
	 When automatic scaling is disabled (useAutoScale=No), this property defines the minimum value to draw on a numeric axis.
	 */
	float scaleMin;
	
	/** Scale Maximum Value.
	 When automatic scaling is disabled (useAutoScale=No), this property defines the maximum value to draw on a numeric axis.
	 */
	float scaleMax;
	
	/** Zero line Format.
	 This property controls the appearance of the zero line.  
	 The zero line is the axis line drawn at the position of the value zero on this axis.
	 */
	TDGLineFormat* zeroLineFormat;
	
	@private
	TDGChart* _chart;    // Needed for correct scaleMin & scaleMax lookup when useAutoScale is on
	TDGElement _element; // Needed to know which axis (x or y) to lookup
}

@property(nonatomic, assign) TDGAxisScaleType scaleType;
@property(nonatomic, assign) TDGOffscaleOptions offscale;
@property(nonatomic, assign) BOOL useAutoScale;
@property(nonatomic, assign) float scaleMin;
@property(nonatomic, assign) float scaleMax;

@property(nonatomic, retain) TDGLineFormat* zeroLineFormat;
/** Initialize. */
-(id)initWithChart:(TDGChart*)chart forElement:(TDGElement)element;
/** Apply these chart properties. */
-(void)walkPropertiesForChart:(TDGChart*)chart forElement:(TDGElement)element;

@end
