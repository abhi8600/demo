#import <UIKit/UIKit.h>

#import "TDGPropertyList.h"
#import "TDGElementList.h"

@class TDGChart;
@class TDGLineFormat;
@class TDGGradient;

/** Basic Box Properties
*/
@interface TDGBox : NSObject {

	/** Background Color. 
	This property defines the background color of a box.
	*/
	UIColor* backgroundColor;

	TDGGradient* gradient;

	/** Border Line Format. 
	This property defines the style (e.g., solid, dotted, dashed, etc), thickness, and color of the border of a box.
	*/
	TDGLineFormat* border;
}

@property(nonatomic, retain) UIColor* backgroundColor;
@property(nonatomic, retain) TDGGradient* gradient;
@property(nonatomic, retain) TDGLineFormat* border;

/** Apply these chart properties
*/
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
