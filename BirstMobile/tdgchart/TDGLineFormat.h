#import <UIKit/UIKit.h>

#import "TDGPropertyList.h"
#import "TDGElementList.h"

@class TDGChart;

typedef enum {
	solid = 0,
	longDash = 1,
	shortDot = 2,
	dashDot = 3,
} TDGDashStyles;

/**
* Line Format Properties
*/
@interface TDGLineFormat : NSObject {

	/** Line Color.
	This property controls the color and transparency of a line.
	*/
	UIColor* color;

	/** Line Thickness.
	This property controls the thickness of a line.
	*/
	float thickness;

	/** Dash Style.
	This property applies a dash style (solid/none, dots, dash, etc.) to a line.
	*/
	TDGDashStyles dashStyle;
}

@property(nonatomic, retain) UIColor* color; 
@property(nonatomic, assign) float thickness;
@property(nonatomic, assign) TDGDashStyles dashStyle;
/**
 Apply properites to a chart element.
 */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;
/** Apply properties to a chart element and series.
 */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element seriesID:(int)seriesID;

@end
