#include "attrenum.h"

typedef enum {
	kTDG_Prop_ChartType      = A_GRAPH_PRESET,        // int corresponding to chart type enum in TDGChart.h
	kTDG_Prop_ChartFrame     = A_LOCATE_FRAME,        // CGRect, passed in / returned in *device* coordinates

	kTDG_Prop_BLA_Layout     = ATC_UNIFORMROWS,       // int corresponding to TDGBLALayout enum in TDGBLAProperties.h  ATC_foo will be ignored
	kTDG_Prop_Orientation    = A2D_ORIENTATION,       // int corresponding to orientation enum in TDGBLAProperties.h
	kTDG_Prop_SwapData       = ASG_SWAP,              // BOOL to swap or not to swap, that is the question
	kTDG_Prop_UseDepth       = A2D_DEPTH_MODE,        // BOOL

	kTDG_Prop_BarRiserWidth  = A2D_BAR_RISER_WIDTH,   // int from 0 to 100
	kTDG_Prop_GroupSpacing   = A2D_BAR_GROUP_SPACING, // int form 0 to 100

	kTDG_Prop_Color          = A_AREACOLOR_RGB,       // UIColor (includes color and alpha)
	kTDG_Prop_Gradient       = A_AREASFX,             // TDGGradient

	kTDG_Prop_Line_Color     = A_LINECOLOR_RGB,       // UIColor (includes color and alpha)
	kTDG_Prop_Line_Pattern   = A_LINEPATTERN,         // int corresponding to line pattern enum in TDGLineFormat.h
	kTDG_Prop_Line_Thickness = A_LINEWIDTH_POINT_100, // int from 0 to 10 (0=invisible, 10=really really thick)
	
	kTDG_Prop_Visible        = AMD_OPTIONS,          // BOOL - AMD_x is ignored, and will be set according to passed in element

	kTDG_Prop_Label_Text     = ATC_UNIFORMCOLS,       // NSString - ATC_foo will be ignored
	kTDG_Prop_Label_Color    = A_FONTCOLOR_RGB,       // UIColor (includes color and alpha)
	kTDG_Prop_Label_FontName = A_FONTNAME,            // NSString corresponding to traditional OSX Font (which includes bold/italic/etc styling info)
	kTDG_Prop_Label_FontSize = A_FONTSIZE_POINT_100,  // int corresponding to traditional font point size (10, 12, etc)
	
	kTDG_Prop_Legend_Marker_Pos = A2D_LEGEND_MARKER_ORIENTATION,  // int corresponding to marker pos enum in TDGLegend.h
	kTDG_Prop_Legend_Pos        = A2D_LEGEND_LOCK,                // int corresponding to legend pos enum in TDGLegend.h

	kTDG_Prop_Ignore_Series   = A2D_IGNORE_SERIES,     // BOOL; requires a series ID
	kTDG_Prop_Series_DataText = A2D_SD_SHOWDATATEXT,   // BOOL; requires a series ID
	kTDG_Prop_BLA_RiserFormat = A2D_SDEMPHASIZED,      // int corresponding to TDGBLASeriesFormat enum in TDGSeries.h
	kTDG_Prop_Series_YAssign  = A2D_SPLITY,            // int corresponding to Y axis assignment (0 = Y1, 1 = Y2, etc)
	
	kTDG_Prop_Pie_Rotation    = API_ROTATE,            // int from 0 to 359
	kTDG_Prop_Pie_HoleSize    = API_HOLESIZE,          // int from 0 to 100
	kTDG_Prop_Pie_Tilt        = API_TILT,              // int from 0 to 100
	kTDG_Prop_Pie_Clockwise   = API_DRAW_CLOCKWISE,    // BOOL
	kTDG_Prop_Slice_Explode   = API_SLICE_MOVE,        // int from 0 to 100; requires a series ID
	
	kTDG_Prop_Axis_useAutoScale = A2D_SCALE_Y1,        // BOOL; requires an axis element ID
	kTDG_Prop_Axis_min          = A2D_SCALE_Y2,        // int; requires an axis element ID
	kTDG_Prop_Axis_max          = A2D_SCALE_Y3,        // int; requires an axis element ID
	kTDG_Prop_Axis_dir          = A2D_DIRECTION_X,     // BOOL; requires an axis element ID
	
	kTDG_Prop_Axis_logScale     = A2D_LOG_X,           // BOOL; requires an axis element ID
	kTDG_Prop_Axis_offscale     = A2D_SHOW_OFFSCALE_X, // int corresponding to offscale enum in TDGNumericAxis.h
	
	kTDG_Prop_Grid_TickStyle    = A2D_GRIDLINES_X,     // int corresponding to TDGTickStyles in TDGGrid.h
	kTDG_Prop_Grid_MinorCount   = A2D_GRIDLINES_Y1,    // int; requires an axis grid element ID

	kTDG_Prop_Suppress          = A_SUPPRESS,          // BOOL
		
	kTDG_Prop_DataRange         = ATC_LINECONTROL,     // CGRect - ATC_foo will be ignored

	kTDG_Prop_3D_ViewAngle      = A3D_VIEWANGLES,      // int, 0 to 15, choice from preset angles
	kTDG_Prop_3D_Rotation       = A3D_CUSTOMVIEW,      // relative rotation of the 3d view. toElement = 0, 1 or 2 for X, Y or Z axis respectively
	kTDG_Prop_SoftShadow        = A_AREA_ADVDROPSHADOW, // AdvDropShadowInstPtr soft shadow definition (may be changed to some more ObjC friendly type)
	
	kTDG_Max_Prop_Property
} TDGProperty;
