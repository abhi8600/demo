#import <Foundation/Foundation.h>
#import "TDGAxis.h"

/**
* Non-Numeric/Ordinal Axis Properties
*/
@interface TDGOrdinalAxis : TDGAxis {

	/** Auto Layout.
	This property enables/disables automatic layout of ordinal axis labels.
	*/
	BOOL useAutoLayout;

	/** Label Skip Count.
	This property controls the number of labels to skip between each ordinal axis label.
	*/
	int labelSkipCount;
}

@property(nonatomic, assign) BOOL useAutoLayout;
@property(nonatomic, assign) int labelSkipCount;
/** 
 Apply chart properties.
 */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
