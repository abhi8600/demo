#import <UIKit/UIKit.h>
#import "TDGElementList.h"

#define kUndefinedId -1
/** Detection Elements
 */
@interface TDGDetectionElement : NSObject {
	/** Element Type.
	 This property defines a chart object ID associated with a detection element.
	 */
	TDGElement elementType;
	
	/** Series ID.
	 Detected element's series ID (if applicable).
	 */
	int seriesID;
	
	/** Group ID.
	 Detected element's group ID (if applicable).
	 */
	int groupID;
	
	/** Rectangle Bounds.
	 The bounding rectangle of the detected chart object.
	 */
	CGRect bounds;
	
	/** Element ID */
	id element;
}

@property(nonatomic, assign) TDGElement elementType;
@property(nonatomic, assign) int seriesID;
@property(nonatomic, assign) int groupID;
@property(nonatomic, assign) CGRect bounds;
@property(nonatomic, retain) id element;

@end
