//
//  TDGElementObj.h
//  objc_pgsdk
//
//  Created by Sam on 11/14/10.
//  Copyright 2010 Three D Graphics. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
Base Element Properties
*/
@protocol TDGBaseElement

/** Visibility.
 Whether or not to draw this element
*/
@property(nonatomic, assign) BOOL visible;

/** Placeable.
 Whether or not Place is allowed to move/resize this element
*/
@property(nonatomic, assign) BOOL placeable;

@end
