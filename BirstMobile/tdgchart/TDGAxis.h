
/** Axis Properties
*/

#import <Foundation/Foundation.h>
#import "TDGLabel.h"
#import "TDGGrid.h"
#import "TDGLineFormat.h"

@interface TDGAxis : NSObject {

	/** Axis Title.
	This property defines the axis title.
	*/
	TDGLabel* title;

	/** Show/Hide Axis Labels.
	This property controls the visibility of axis labels.
	*/
	BOOL showLabels;

	/** Label Text Format.
	 This property defines the format (font, font size, color) of the axis labels.
	 */
	TDGTextFormat* labelFormat;

	/** Ascending/Descending Axis.
	This property controls the direction of the axis (drawn in ascending or descending order).
	*/
	BOOL invertDirection;

	/** Show Off-Scale Indicators.
	This property controls the visibility of off-scale indicators.
	*/
	BOOL showOffscaleIndicators;

	/** Major Grid Lines.
	This property defines the visibility, format, style, and color of major grid lines.
	*/
	TDGGrid* majorGrid;
	
	/** Axis Line Format.
	 This property controls the format of the axis base line.
	 */
	TDGLineFormat* axisLineFormat;

	@private
	TDGGrid* minorGrid;
	int minorGridCount;
}

@property(nonatomic, retain) TDGLabel* title;
@property(nonatomic, assign) BOOL showLabels;
@property(nonatomic, retain) TDGTextFormat* labelFormat;
@property(nonatomic, assign) BOOL invertDirection;
@property(nonatomic, assign) BOOL showOffscaleIndicators;
@property(nonatomic, retain) TDGGrid* majorGrid;
@property(nonatomic, retain) TDGGrid* minorGrid;
@property(nonatomic, assign) int minorGridCount;
@property(nonatomic, retain) TDGLineFormat* axisLineFormat;

/** Apply these chart properties */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
