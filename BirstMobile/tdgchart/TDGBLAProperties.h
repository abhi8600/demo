#import <Foundation/Foundation.h>
#import "TDGElementList.h"

@class TDGChart;

typedef enum {
	absolute = 0,
	stacked = 4,
	percent = 12
} TDGBLALayout;

typedef enum {
	horizontal = 0,
	vertical = 1,
} TDGOrientation;

/**
* Bar/Line/Area (BLA) Chart Properties
*/
@interface TDGBLAProperties : NSObject {

	/**
	Bar/Line/Area (BLA) Layout.
	This property defines the layout of a BLA chart: 0=Absolute, 4=Stacked, 12=Percent.
	*/
	TDGBLALayout seriesLayout;

	/**
	Orientation.
	This property defines the orientation of Bar/Line/Area Chart: 0=horizontal, 1=vertical.
	*/
	TDGOrientation orientation;

	/**
	Riser Width (1...100).
	This property defines the width of risers in a horizontal bar or vertical column chart.
	*/
	int riserWidth;

	/** Riser Group Gap Width (-1...100).
	This property defines the spacing between groups of bars in a side-by-side horizontal bar or vertical column chart.
	*/
	int groupGapWidth;

	/** Page Index.
	 This property defines the current page index.
	 */
	int pageIndex;
	
	/** Groups Per Page.
	 This property defines the number of groups per page.
	 */
	int groupsPerPage;
	
	@private
	TDGChart* _chart;
	float _scaleMin;
	float _scaleMax;
}

@property(nonatomic, assign) TDGBLALayout seriesLayout;
@property(nonatomic, assign) TDGOrientation orientation;
@property(nonatomic, assign) int riserWidth;
@property(nonatomic, assign) int groupGapWidth;
@property(nonatomic, assign) int pageIndex;
@property(nonatomic, assign) int groupsPerPage;

/** Initialize with chart. */
-(id)initWithChart:(TDGChart*)parentChart;

/** Apply these chart properties */	
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
