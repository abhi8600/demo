#include "oidenum.h"

//when adding entries here 
//also update utilities.m:pg_objc_get_TDGElement_for_ObjectID mapping function
typedef enum {
	kTDG_Background   = O5D_BACKGROUND,
	kTDG_Frame        = O5D_FRAME,
	
	kTDG_Title        = O5D_LBLTITLE,
	kTDG_Subtitle     = O5D_LBLSUBTITLE,
	kTDG_Footnote     = O5D_LBLFOOTNOTE,
	
	kTDG_Legend       = OSG_LEGEND_AREA,
	kTDG_Legend_Marker = OSG_LEGEND_MARKER,
	kTDG_Legend_Label = OSG_LEGEND_TEXT,
	kTDG_Legend_Title = O2D_SERIES_TITLE,
	kTDG_Riser        = O2D_RISER,
	kTDG_Data_Line    = O2D_DATALINE,
	
	kTDG_X_Axis       = O2D_X1_BODY,
	kTDG_Y1_Axis      = O2D_Y1_BODY,
	kTDG_Y2_Axis      = O2D_Y2_BODY,
	kTDG_Ordinal_Axis = O2D_O1_BODY,

	kTDG_X_Axis_title       = O2D_X1_TITLE,
	kTDG_Y1_Axis_title      = O2D_Y1_TITLE,
	kTDG_Y2_Axis_title      = O2D_Y2_TITLE,
	kTDG_Ordinal_Axis_title = O2D_O1_TITLE,
	
	kTDG_X_Axis_Major_Grid  = O2D_X1_MAJOR,
	kTDG_Y1_Axis_Major_Grid = O2D_Y1_MAJOR,
	kTDG_Y2_Axis_Major_Grid = O2D_Y2_MAJOR,
	kTDG_Ordinal_Axis_Major_Grid = O2D_O1_MAJOR,

	kTDG_X_Axis_Minor_Grid  = O2D_X1_MINOR,
	kTDG_Y1_Axis_Minor_Grid = O2D_Y1_MINOR,
	kTDG_Y2_Axis_Minor_Grid = O2D_Y2_MINOR,
	kTDG_Ordinal_Axis_Minor_Grid = O2D_O1_MINOR,
	
	kTDG_X_Axis_Line  = O2D_X1_BODY,
	kTDG_Y1_Axis_Line = O2D_Y1_BODY,
	kTDG_Y2_Axis_Line = O2D_Y2_BODY,
	kTDG_Ordinal_Axis_Line = O2D_O1_BODY,
	
	kTDG_X_Axis_Labels  = O2D_X1_LABEL,
	kTDG_Y1_Axis_Labels = O2D_Y1_LABEL,
	kTDG_Y2_Axis_Labels = O2D_Y2_LABEL,
	kTDG_Ordinal_Axis_Labels = O2D_O1_LABEL,

	kTDG_X_Axis_ZeroLine = O2D_X1_ZERO,
	kTDG_Y1_Axis_ZeroLine = O2D_Y1_ZERO,
	kTDG_Y2_Axis_ZeroLine = O2D_Y2_ZERO,
	
	kTDG_DataText = O2D_DATATEXT,
	
	kTDG_Max_Element
} TDGElement;
