//
//  TDGAnnotation.h
//  objc_pgsdk
//
//  Created by Sam on 11/11/10.
//  Copyright 2010 Three D Graphics. All rights reserved.
//
/** Annotation Properties and Initialization
*/
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TDGLabel.h"


@interface TDGAnnotation : NSObject
{
	/** Annotation Label.
	Text Label to draw with annotation
	*/
	TDGLabel* label;

	/** Draw Annotation Above Risers. */
	BOOL drawAboveRisers;

	/** Draw Annotation at Absolute Position. */
	BOOL absolutePosition;
}

@property(nonatomic, retain) TDGLabel* label;
@property(nonatomic) BOOL drawAboveRisers;
@property(nonatomic) BOOL absolutePosition;

/** Creates a new annotation with the specified text and returns it
*/
- (id) initWithText: (NSString*)text;

@end
