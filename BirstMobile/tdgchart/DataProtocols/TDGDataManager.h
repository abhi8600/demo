#import <Foundation/Foundation.h>
#import "TDGDataProtocol.h"

@interface TDGDataManager : NSObject<TDGDataProtocol> {
	int seriesCount;
	int dataPointCount;
	BOOL useSampleData;

	NSMutableArray* dataArray;
	NSMutableArray* sampleDataArray;
	
	NSMutableArray* seriesLabels;
	NSMutableArray* groupLabels;

	NSMutableArray* sampleSeriesLabels;
	NSMutableArray* sampleGroupLabels;
}

@end
