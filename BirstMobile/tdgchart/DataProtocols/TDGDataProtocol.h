#import <UIKit/UIKit.h>

@protocol TDGDataProtocol

@property(nonatomic, assign, readonly) int seriesCount;
@property(nonatomic, assign) int dataPointCount;
@property(nonatomic, assign) BOOL useSampleData;

-(NSArray*)getDataForSeries:(int)series index:(int)index;
-(void)setData:(NSArray*)data series:(int)series index:(int)index;

-(void)addYPoint:(int)series data:(float)x;
-(void)addXYPoint:(int)series x:(float)x y:(float)y;
-(void)addXYZPoint:(int)series x:(float)x y:(float)y z:(float)z;

-(void)setYPoint:(int)series index:(int)index data:(float)x;
-(void)setXYPoint:(int)series index:(int)index x:(float)x y:(float)y;
-(void)setXYZPoint:(int)series index:(int)index x:(float)x y:(float)y z:(float)z;

-(void)setYArray:(int)series yData:(NSArray*)yData;
-(void)setXYArray:(int)series xData:(NSArray*)xData yData:(NSArray*)yData;
-(void)setXYZArray:(int)series xData:(NSArray*)xData yData:(NSArray*)yData zData:(NSArray*)zData;

-(NSArray*)getSeriesLabels;
-(NSArray*)getGroupLabels;
-(NSString*)getSeriesLabelAtIndex:(int)index;
-(NSString*)getGroupLabelAtIndex:(int)index;

-(void)setSeriesLabels:(NSArray*)labels;
-(void)setGroupLabels:(NSArray*)labels;
-(void)setSeriesLabelAtIndex:(int)index label:(NSString*)label;
-(void)setGroupLabelAtIndex:(int)index label:(NSString*)label;

@end
