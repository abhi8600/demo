//
//  TDGGradient.h
//  objc_pgsdk
//
//  Copyright 2011 Three D Graphics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TDGPropertyList.h"
#import "TDGElementList.h"

@class TDGChart;

enum gradientFormat {
	gradient_none,
	gradient_autoformat,
	gradient_linear,
	gradient_radial
};

/**
* Properties to define the format of area objects
*/
@interface TDGGradient : NSObject {
	@public

	/** Gradient Color at the beggining and at the end. */
    UIColor* startColor;
    UIColor* endColor;

  /** Gradient type */
		enum gradientFormat format;

		/** Assuming that gradient chages from startColor (at 0%) to endColor and back to startColor (at 100%)
		 *  offset (0.0 to 1.0) defines where the endColor will be on that scale. 
		 *  B=1.0 (100%) will be typical startColor - endColor gradient
		 *  B=0.5 (50%)  will be typical startColor - endColor - startColor (ABA) gradient */
		float offset;

		/** linerar gradient angle. 0 to 359 clockwise, where 0 is gradient bottom-to-TOP and 90 is gradient left-to-RIGHT. */
		int angle;
}

@property(nonatomic, retain) UIColor* startColor;
@property(nonatomic, retain) UIColor* endColor;
@property(nonatomic, assign) enum gradientFormat format;
@property(nonatomic, assign) float offset;
@property(nonatomic, assign) int  angle;
@property(nonatomic, assign) bool autoFormat;

/** Apply these properties to an element in the chart */
//-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

/** Apply these properties to an element and series */
//-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element seriesID:(int)seriesID;

@end
