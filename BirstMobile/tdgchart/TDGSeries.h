/** Series Properties.
*/
#import <Foundation/Foundation.h>
#import "TDGAreaFormat.h"
#import "TDGLineFormat.h"

typedef enum {
	bar,
	line,
	area
} TDGBLASeriesFormat;

/**
* Series-Specific Properties
*/
@interface TDGSeries : NSObject {
	/* Series ID.
	 */
	int seriesID;

	/** Series Format.
	 This property defines the series format:<br> 
	 0 = Draw risers for this series as bars<br>
	 1 = Draw risers for this series as lines<br>
	 2 = Draw risers for this series as areas.
	 */
	TDGBLASeriesFormat format;

	/** Series Area Format.
	Defines the fill area format (e.g., color, transparency, etc.) of a series as defined in TDGAreaFormat.h.
	*/
	TDGAreaFormat* fill;

	/** Series Line Format.
	Defines the line format (e.g., color, width, solid, dotted, dashed, etc.) of a series as defined in TDGLineFormat.h.
	*/
	TDGLineFormat* border;

	/** Is Series Ignored.
	Controls whether or not the series is included in the chart.
	*/
	BOOL isIgnored;

	/** Show Data Labels.
	Controls the visibility of data labels for a series.
	*/
	BOOL showDataText;

	/** Axis Assignment.
	Where to draw the riser/marker for this series in a multi-Y axes chart: 0=Y1, 1=Y2.
	*/
	int yAxisAssignment;

	/** Explode Slice.
	 Value from 0 to 100 denoting how far to explode a slice from a pie chart. Default is 0.
	 */
	int explodeSlice;

	@private
	/** Is Absolute.
	 Private boolean to draw series as stacked or absolute.
	 */
	BOOL isAbsolute;
	/** Is Legend Ignored.
	 Private boolean to exclude/include series in legend.
	 */
	BOOL isLegendIgnored;	
}

@property(nonatomic, assign, readonly) int seriesID;
@property(nonatomic, assign) TDGBLASeriesFormat format;
@property(nonatomic, retain) TDGAreaFormat* fill;
@property(nonatomic, retain) TDGLineFormat* border;
@property(nonatomic, assign) BOOL isAbsolute;
@property(nonatomic, assign) BOOL isIgnored;
@property(nonatomic, assign) BOOL isLegendIgnored;
@property(nonatomic, assign) int yAxisAssignment;
@property(nonatomic, assign) int explodeSlice;
@property(nonatomic, assign) BOOL showDataText;
/** Initialize Series with ID. */
-(id)initWithID:(int)newID;
/** Apply these chart properties. */
-(void)walkPropertiesForChart:(TDGChart *)chart forElement:(TDGElement)element;

@end
