//
//  BirstMobileClientImpl.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/6/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "TBXML.h"
#import "BirstMobileClient.h"
#import "Page.h"

@interface BirstMobileClientImpl : NSObject {
    BirstMobileClient *client;
    
    @private 
    ASIFormDataRequest *asyncRequest;
    NSString *username;
    NSString *ssoPasswd;
    NSString *spaceId;
    NSString *token;
    NSString *targetDashboard;
    NSString *targetPage;
    NSString *serverUrl;
    NSString *redirectServerUrl;
    NSString *dashboardListOp;
    
    TBXML *dashboardList;
}

@property (nonatomic, retain) BirstMobileClient *client;
@property (nonatomic, retain) ASIFormDataRequest *asyncRequest;
@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *ssoPasswd;
@property (nonatomic, retain) NSString *spaceId;
@property (nonatomic, retain) NSString *token;
@property (nonatomic, retain) NSString *targetDashboard;
@property (nonatomic, retain) NSString *targetPage;
@property (nonatomic, retain) TBXML *dashboardList;
@property (nonatomic, retain) NSString *serverUrl;
@property (nonatomic, retain) NSString *redirectServerUrl;
@property (nonatomic, retain) NSString *dashboardListOp;

- (id) initWith:(NSString*)userName ssoPassword:(NSString*)ssoPassword spaceId:(NSString*)space;
- (void) authenticate;
- (void) logout;
- (void) onLogoutSuccess:(ASIHTTPRequest *)request;
- (void) onLogoutFail:(ASIHTTPRequest*)request;

- (void) popupFullScreenDashboardPage:(NSString*)dashboard page:(NSString*)page;
- (void) getSession;
- (void) getDashboards;
- (void) getDashboardList:(SEL)success;
- (void) onGetDashboardListFinished:(ASIHTTPRequest *)request;
- (void) onGetDashboardListFailed:(ASIHTTPRequest *)request;
- (void) parseDashsXml:(ASIHTTPRequest *)request;
- (void) showPageRenderViewController:(Page*)page;
- (NSError *)createNSError:(NSInteger)code formatStr:(NSString *)errMsg;
@end
