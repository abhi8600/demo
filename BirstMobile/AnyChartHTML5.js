(function(){function e(a) {
  throw a;
}
var h = void 0, j = !0, p = null, q = !1;
function aa() {
  return function(a) {
    return a
  }
}
function s() {
  return function() {
  }
}
function t(a) {
  return function(b) {
    this[a] = b
  }
}
function u(a) {
  return function() {
    return this[a]
  }
}
function x(a) {
  return function() {
    return a
  }
}
var eb = window.anychart.scope, A = A || eb.goog, ha = ha || eb.Event, pa = pa || eb.EventTarget, ka = ka || eb.EventType, Ga = Ga || eb.XhrIo;
function fb() {
  this.arguments = []
}
fb.prototype.arguments = p;
fb.prototype.pq = function() {
  return this.arguments[0].Pe != h && this.arguments[1].Pe != h
};
fb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh, a.bq, q));
  var c = this.arguments[1];
  c.execute != h ? c = c.execute(a) : "%color" == c && (c = new gb(a.Ai, a.ni, a.Vh, a.bq, q));
  if(b.Pe == h || c.Pe == h) {
    return a
  }
  var a = this.arguments[2], d = 1 - a;
  return new gb(Math.floor((b.Ai * a + c.Ai * d) % 256), Math.floor((b.ni * a + c.ni * d) % 256), Math.floor((b.Vh * a + c.Vh * d) % 256))
};
fb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"Blend function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0]) {
    return"Blend function requires a color, a function returning a color or a %color variable as it's first argument"
  }
  if(this.arguments[1].execute == h && this.arguments[1].Pe == h && "%color" != this.arguments[1]) {
    return"Blend function requires a color, a function returning a color or a %color variable as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"Blend function requires a number as it's third argument"
  }
  1 < this.arguments[2] && (this.arguments[2] = 1);
  0 > this.arguments[2] && (this.arguments[2] = 0);
  return p
};
function hb() {
  this.arguments = []
}
hb.prototype.arguments = p;
hb.prototype.pq = function() {
  return this.arguments[0].Pe != h
};
hb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh));
  if(b.Pe == h) {
    return a
  }
  a = ib(b);
  b = 100 * a.Eh;
  a.Eh -= 0.007 * b * b / 100;
  0 > a.Eh && (a.Eh = 0);
  return jb(a.wD, a.Eh, a.sF)
};
hb.prototype.uq = function() {
  return 1 != this.arguments.length ? "DarkColor function expects 1 argument, " + this.arguments.length + " passed" : this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0] ? "DarkColor function requires a color, a function returning a color or a %color variable as it's argument" : p
};
function kb() {
  this.arguments = []
}
kb.prototype.arguments = p;
kb.prototype.pq = x(j);
kb.prototype.execute = function() {
  return lb(this.arguments[0], this.arguments[1], this.arguments[2])
};
kb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"HSB function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute != h || this.arguments[0].Pe != h || "%color" == this.arguments[0]) {
    return"HSB function requires a number as it's first argument"
  }
  if(this.arguments[1].execute != h || this.arguments[1].Pe != h || "%color" == this.arguments[1]) {
    return"HSB function requires a number as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"HSB function requires a number as it's third argument"
  }
  this.arguments[0] = Math.round(this.arguments[0]);
  360 < this.arguments[0] ? this.arguments[0] = 360 : 0 > this.arguments[0] && (this.arguments[0] = 0);
  for(var a = 1;3 > a;a++) {
    this.arguments[a] = Math.round(this.arguments[a]), 100 < this.arguments[a] ? this.arguments[a] = 100 : 0 > this.arguments[a] && (this.arguments[a] = 0)
  }
  return p
};
function mb() {
  this.arguments = []
}
mb.prototype.arguments = p;
mb.prototype.pq = function() {
  return this.arguments[0].Pe != h
};
mb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh));
  if(b.Pe == h) {
    return a
  }
  a = ib(b);
  b = 100 * a.Eh - 100;
  a.Eh += 0.007 * b * b / 100;
  1 < a.Eh && (a.Eh = 1);
  return jb(a.wD, a.Eh, a.sF)
};
mb.prototype.uq = function() {
  return 1 != this.arguments.length ? "LightColor function expects 1 argument, " + this.arguments.length + " passed" : this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0] ? "LightColor function requires a color, a function returning a color or a %color variable as it's argument" : p
};
function nb() {
  this.arguments = []
}
nb.prototype.arguments = p;
nb.prototype.pq = x(j);
nb.prototype.execute = function() {
  return new gb(this.arguments[0], this.arguments[1], this.arguments[2])
};
nb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"RGB function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute != h || this.arguments[0].Pe != h || "%color" == this.arguments[0]) {
    return"RGB function requires a number as it's first argument"
  }
  if(this.arguments[1].execute != h || this.arguments[1].Pe != h || "%color" == this.arguments[1]) {
    return"RGB function requires a number as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"RGB function requires a number as it's third argument"
  }
  for(var a = 0;3 > a;a++) {
    this.arguments[a] = Math.round(this.arguments[a]), 255 < this.arguments[a] ? this.arguments[a] = 255 : 0 > this.arguments[a] && (this.arguments[a] = 0)
  }
  return p
};
function ob(a) {
  switch(a) {
    case tb.blend:
      return new fb;
    case tb.lightcolor:
      return new mb;
    case tb.darkcolor:
      return new hb;
    case tb.rgb:
      return new nb;
    case tb.hsb:
      return new kb;
    default:
      e(Error("Unknown color operation identifier passed"))
  }
}
;function gb(a, b, c, d, f) {
  a != h && (this.Ai = a);
  b != h && (this.ni = b);
  c != h && (this.Vh = c);
  d != h && (this.bq = d);
  if(d != h && (f == h || f)) {
    this.bq /= 255
  }
}
function lb(a, b, c) {
  b = Number(b / 100);
  c = Number(c / 100);
  if(0 == b) {
    return new gb(Math.floor(255 * c), Math.floor(255 * c), Math.floor(255 * c))
  }
  var d = 6 * Number(a / 360), a = Math.floor(d), f = d - a, d = c * (1 - b), b = a & 1 ? c * (1 - b * f) : c * (1 - b * (1 - f));
  switch(a) {
    case 0:
      return new gb(Math.floor(255 * c), Math.floor(255 * b), Math.floor(255 * d));
    case 1:
      return new gb(Math.floor(255 * b), Math.floor(255 * c), Math.floor(255 * d));
    case 2:
      return new gb(Math.floor(255 * d), Math.floor(255 * c), Math.floor(255 * b));
    case 3:
      return new gb(Math.floor(255 * d), Math.floor(255 * b), Math.floor(255 * c));
    case 4:
      return new gb(Math.floor(255 * b), Math.floor(255 * d), Math.floor(255 * c));
    default:
      return new gb(Math.floor(255 * c), Math.floor(255 * d), Math.floor(255 * b))
  }
}
function jb(a, b, c) {
  var d = 0, f = 0, g = 0;
  if(0 == b) {
    d = f = g = 0
  }else {
    if(0 == c) {
      d = f = g = b
    }else {
      d = 0.5 >= b ? b * (1 + c) : b + c - b * c;
      b = 2 * b - d;
      f = [a + 0.3333333333333333, a, a - 0.3333333333333333];
      a = [];
      for(c = 0;3 > c;c++) {
        0 > f[c] ? f[c] += 1 : 1 < f[c] && (f[c] -= 1), a[c] = 1 > 6 * f[c] ? b + 6 * (d - b) * f[c] : 1 > 2 * f[c] ? d : 2 > 3 * f[c] ? b + 6 * (d - b) * (0.6666666666666666 - f[c]) : b
      }
      d = Math.max(0, Math.min(1, a[0]));
      f = Math.max(0, Math.min(1, a[1]));
      g = Math.max(0, Math.min(1, a[2]))
    }
  }
  return new gb(Math.floor(255 * d), Math.floor(255 * f), Math.floor(255 * g))
}
z = gb.prototype;
z.Ai = 0;
z.ni = 0;
z.Vh = 0;
z.bq = 1;
z.Pe = function() {
  return"rgba(" + this.Ai.toString() + ", " + this.ni.toString() + ", " + this.Vh.toString() + ", " + Math.round(255 * this.alpha).toString() + ")"
};
function ub(a) {
  return"rgb(" + a.Ai.toString() + ", " + a.ni.toString() + ", " + a.Vh.toString() + ")"
}
function vb(a) {
  return"0x" + (a.Ai << 24 | a.ni << 16 | a.Vh << 8 | a.bq).toString(16)
}
z.toString = function() {
  return"rgb(" + this.Ai.toString() + "," + this.ni.toString() + "," + this.Vh.toString() + ")"
};
function ib(a) {
  var b = a.Ai / 255, c = a.ni / 255, a = a.Vh / 255, d = Math.min(b, Math.min(c, a)), f = Math.max(b, Math.max(c, a));
  if(d == f) {
    return new wb(0, (f + d) / 2, 0)
  }
  var g = f - d, d = f + d, k = 0, k = 0.1666 * (b == f ? 6 + (c - a) / g : c == f ? 2 + (a - b) / g : 4 + (b - c) / g);
  1 <= k && (k -= 1);
  return new wb(k, d / 2, 1 >= d ? g / d : g / (2 - d))
}
function wb(a, b, c) {
  this.wD = a;
  this.Eh = b;
  this.sF = c
}
wb.prototype.wD = 0;
wb.prototype.Eh = 0;
wb.prototype.sF = 0;
function xb(a, b) {
  this.Xa = a;
  a.execute != h ? (this.ad = b, this.Ay = j) : "%color" == a ? this.Ay = j : (this.Xa.alpha = b, this.Ay = q)
}
xb.prototype.Xa = p;
xb.prototype.ad = 1;
xb.prototype.Ay = q;
xb.prototype.Ba = function(a) {
  return this.Ay ? (a || (a = new gb), this.Xa.execute != h && (a = this.Xa.execute(a), a.alpha = this.ad), a) : this.Xa
};
function zb(a, b) {
  if(!a) {
    return new xb(new gb(0, 0, 0), 1)
  }
  var c = new Ab(a);
  try {
    return new xb(c.parse(), b == h ? 1 : b)
  }catch(d) {
    return new xb(new gb(0, 0, 0), 1)
  }
}
function Bb(a) {
  this.input = Cb(a);
  this.UD = a.length
}
Bb.prototype.input = "";
Bb.prototype.UD = 0;
Bb.prototype.$j = 0;
var Db = {steelblue:4620980, royalblue:267920, cornflowerblue:6591981, lightsteelblue:11584734, mediumslateblue:8087790, slateblue:6970061, darkslateblue:4734347, midnightblue:1644912, navy:128, darkblue:139, mediumblue:205, blue:255, dodgerblue:2003199, deepskyblue:49151, lightskyblue:8900346, skyblue:8900331, lightblue:11393254, powderblue:11591910, azure:15794175, lightcyan:14745599, paleturquoise:11529966, mediumturquoise:4772300, lightseagreen:2142890, darkcyan:35723, teal:32896, cadetblue:6266528, 
darkturquoise:52945, aqua:65535, cyan:65535, turquoise:4251856, aquamarine:8388564, mediumaquamarine:6737322, darkseagreen:9419919, mediumseagreen:3978097, seagreen:3050327, darkgreen:25600, green:32768, forestgreen:2263842, limegreen:3329330, lime:65280, chartreuse:8388352, lawngreen:8190976, greenyellow:11403055, yellowgreen:10145074, palegreen:10025880, lightgreen:9498256, springgreen:65407, mediumspringgreen:64154, darkolivegreen:5597999, olivedrab:7048739, olive:8421376, darkkhaki:12433259, 
darkgoldenrod:12092939, goldenrod:14329120, gold:16766720, yellow:16776960, khaki:15787660, palegoldenrod:15657130, blanchedalmond:16772045, moccasin:16770229, wheat:16113331, navajowhite:16768685, burlywood:14596231, tan:13808780, rosybrown:12357519, sienna:10506797, saddlebrown:9127187, chocolate:13789470, peru:13468991, sandybrown:16032864, darkred:9109504, maroon:8388608, brown:10824234, firebrick:11674146, indianred:13458524, lightcoral:15761536, salmon:16416882, darksalmon:15308410, lightsalmon:16752762, 
coral:16744272, tomato:16737095, darkorange:16747520, orange:16753920, orangered:16729344, crimson:14423100, red:16711680, deeppink:16716947, fuchsia:16711935, magenta:16711935, hotpink:16738740, lightpink:16758465, pink:16761035, palevioletred:14381203, mediumvioletred:13047173, purple:8388736, darkmagenta:9109643, mediumpurple:9662683, blueviolet:9055202, indigo:4915330, darkviolet:9699539, darkorchid:10040012, mediumorchid:12211667, orchid:14315734, violet:15631086, plum:14524637, thistle:14204888, 
lavender:15132410, ghostwhite:16316671, aliceblue:15792383, mintcream:16121850, honeydew:15794160, lightgoldenrodyellow:16448210, lemonchiffon:16775885, ornsilk:16775388, lightyellow:16777184, ivory:16777200, floralwhite:16775920, linen:16445670, oldlace:16643558, antiquewhite:16444375, bisque:16770244, peachpuff:16767673, papayawhip:16773077, beige:16119260, seashell:16774638, lavenderblush:16773365, istyrose:16770273, snow:16775930, white:16777215, whitesmoke:16119285, gainsboro:14474460, lightgrey:13882323, 
silver:12632256, darkgray:11119017, gray:8421504, lightslategray:7833753, slategray:7372944, imgray:6908265, darkslategray:3100495, black:0}, tb = {blend:1, lightcolor:2, darkcolor:3, rgb:4, hsb:5};
Bb.prototype.cf = 0;
Bb.prototype.Ud = p;
Bb.prototype.ln = 0;
function Eb(a) {
  var b = q, c = 0, d = "", f = q;
  a.cf = 0;
  for(a.ln = a.$j;!b && a.$j < a.UD;) {
    var g = a.input.charAt(a.$j), k = "a" <= g && "z" >= g, l = "0" <= g && "9" >= g, n = l || "a" <= g && "f" >= g, m = " " == g || "\t" == g;
    switch(c) {
      case 0:
        m ? a.ln = a.$j + 1 : (d = g, k || "%" == g ? c = 1 : "0" == g ? c = 4 : l ? c = 2 : "#" == g ? (d = "0x", c = 3) : "." == g ? (f = j, c = 2) : "(" == g ? (a.cf = 6, a.Ud = g, a.$j++, b = j) : ")" == g ? (a.cf = 7, a.Ud = g, a.$j++, b = j) : "," == g ? (a.cf = 5, a.Ud = g, a.$j++, b = j) : a.ln = a.$j + 1);
        break;
      case 1:
        k ? d += g : (Fb(a, d), b = j);
        break;
      case 2:
        l ? d += g : "." == g ? f ? (a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), b = j) : (d += g, f = j) : (a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), b = j);
        break;
      case 3:
        n ? d += g : (a.cf = 3, a.Ud = Gb(Number(d)), b = j);
        break;
      case 4:
        "x" == g ? (d += g, c = 3) : l ? (d += g, c = 2) : "." == g ? (d += g, f = j, c = 2) : (a.cf = 4, a.Ud = 0, b = j)
    }
    b || a.$j++
  }
  if(a.$j < a.UD) {
    return b
  }
  switch(c) {
    case 0:
      return 0 != a.cf;
    case 1:
      return Fb(a, d), j;
    case 2:
      return a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), j;
    case 3:
      return a.cf = 3, a.Ud = Gb(Number(d)), j;
    case 4:
      return a.cf = 4, a.Ud = 0, j
  }
  return q
}
function Fb(a, b) {
  "%color" == b ? (a.cf = 2, a.Ud = b) : tb[b] != h ? (a.cf = 1, a.Ud = tb[b]) : (a.cf = 3, a.Ud = Gb(Db[b] != h ? Db[b] : 13421772))
}
function Gb(a) {
  var b = a >> 8;
  return new gb((b >> 8) % 256, b % 256, a % 256)
}
function Ab(a) {
  this.ue = new Bb(a)
}
Ab.prototype.ue = p;
Ab.prototype.parse = function() {
  for(var a = 0, b = [], c;Eb(this.ue);) {
    switch(a) {
      case 0:
        switch(this.ue.cf) {
          case 1:
            a = 1;
            b.push(ob(this.ue.Ud));
            break;
          case 3:
          ;
          case 2:
            a = 4;
            b.push(this.ue.Ud);
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.ln + ' in "' + this.ue.input + '". Function name, color or %Color expected.'))
        }
        break;
      case 1:
        6 != this.ue.cf && e(Error("Color parser error: unexpected left parenthesis at " + this.ue.ln + ' in "' + this.ue.input + '". Left paren expected.'));
        a = 2;
        break;
      case 2:
        switch(this.ue.cf) {
          case 1:
            a = 1;
            b.push(ob(this.ue.Ud));
            break;
          case 3:
          ;
          case 2:
          ;
          case 4:
            a = 3;
            b[b.length - 1].arguments.push(this.ue.Ud);
            break;
          case 7:
            c = b.pop();
            a = c.uq();
            a != p && e(Error(a));
            c.pq() && (c = c.execute());
            0 == b.length ? (a = 4, b.push(c)) : (a = 3, b[b.length - 1].arguments.push(c));
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.ln + ' in "' + this.ue.input + '". Function name, color, %Color, number or right paren expected.'))
        }
        break;
      case 3:
        switch(this.ue.cf) {
          case 7:
            c = b.pop();
            a = c.uq();
            a != p && e(Error(a));
            c.pq() && (c = c.execute());
            0 == b.length ? (a = 4, b.push(c)) : (a = 3, b[b.length - 1].arguments.push(c));
            break;
          case 5:
            a = 2;
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.ln + ' in "' + this.ue.input + '". Expected comma or right paren.'))
        }
        break;
      case 4:
        e(Error("Color parser error: unexpected lexem at " + this.ue.ln + ' in "' + this.ue.input + '". End of string expected.'))
    }
  }
  4 != a && e(Error('Color parser error: unexpected end of string in "' + this.ue.input + '"'));
  return b.pop()
};
function Hb(a) {
  for(var a = a.replace(/^\s*/, ""), b = /\s/, c = a.length;b.test(a.charAt(--c));) {
  }
  return a.slice(0, c + 1)
}
var Ib = {};
function Jb(a) {
  return Kb((new DOMParser).parseFromString(a, "text/xml"))
}
function Kb(a) {
  if(!a) {
    return p
  }
  switch(a.nodeType) {
    case 1:
      var b = {}, c, d = a.childNodes.length;
      for(c = 0;c < d;c++) {
        var f = Kb(a.childNodes[c]);
        if(f != p) {
          var g = a.childNodes[c].nodeName;
          if("#" == g.charAt(0)) {
            b.value = b.value == h || "string" != typeof b.value ? f.value : b.value + f.value
          }else {
            if(b[g] == h) {
              b[g] = f
            }else {
              if(b[g] instanceof Array) {
                b[g].push(f)
              }else {
                if("value" != g || "string" != typeof b.value) {
                  b[g] = [b[g], f]
                }
              }
            }
          }
        }
      }
      d = a.attributes == p ? 0 : a.attributes.length;
      for(c = 0;c < d;c++) {
        f = a.attributes[c], b[f.nodeName] == h && (b[f.nodeName] = f.nodeValue)
      }
      return b;
    case 3:
      return a = Hb(a.nodeValue), "" == a ? p : {value:a};
    case 4:
      return{value:a.nodeValue};
    case 9:
      return Kb(a.documentElement);
    default:
      return p
  }
}
function C(a, b) {
  return a && a[b] != h
}
function F(a, b) {
  if(!a) {
    return p
  }
  var c = a[b];
  return c == p || c == h ? p : c instanceof Array ? c[0] : c
}
function H(a, b, c) {
  a && (a[b] = c)
}
function Lb(a, b) {
  a && a[b] && delete a[b]
}
function I(a, b) {
  if(!a || !b) {
    return[]
  }
  A.Uo(a[b]) ? a[b] instanceof Array || (a[b] = [a[b]]) : a[b] = [];
  return a[b]
}
function J(a, b) {
  if(!a) {
    return p
  }
  var c = a[b];
  if(!c) {
    return p
  }
  c = c instanceof Array ? c[0] : c;
  return"string" == typeof c ? c : c.value == h ? p : c.value
}
function Mb(a) {
  if(!a) {
    return NaN
  }
  a = Hb(a.toString());
  return"%" == a.charAt(a.length - 1)
}
function Nb(a) {
  return Mb(a) ? (a = Hb(a.toString()), a = a.substr(0, a.length - 1), Ob(a) / 100) : NaN
}
function Pb(a) {
  return Ob(a) / 100
}
function Qb(a, b) {
  return Pb(F(a, b))
}
function Rb(a) {
  return a == h ? q : "1" == a || "true" == a.toString().toLowerCase()
}
function K(a, b) {
  return Rb(F(a, b))
}
function Ob(a) {
  if(A.Qc(a)) {
    if(!Hb(a)) {
      return NaN
    }
  }else {
    if(A.lJ(a)) {
      return a
    }
    if(a == p) {
      return NaN
    }
  }
  return Number(a)
}
function M(a, b) {
  return Ob(F(a, b))
}
function Sb(a) {
  a = Number(a);
  1 < a ? a = 1 : 0 > a && (a = 0);
  return a
}
function Tb(a, b) {
  if(!a) {
    return q
  }
  var c = F(a, "enabled");
  return c == p || c == h ? b == h || b == p ? j : b : Rb(c)
}
function Ub(a, b) {
  return Tb(F(a, b))
}
function Cb(a) {
  return a ? a.toString().toLowerCase() : ""
}
function N(a, b) {
  return Cb(F(a, b))
}
function Vb(a) {
  return a ? a.toString().toLowerCase() : ""
}
function Wb(a, b) {
  return Vb(F(a, b))
}
function Xb(a) {
  return a != h && "auto" == Cb(a)
}
function Yb(a) {
  return zb(F(a, "color"), h)
}
function Zb(a) {
  var b;
  if(a instanceof Function) {
    b = a
  }else {
    if(a instanceof Array) {
      b = [];
      for(var c = 0, d = a.length;c < d;c++) {
        b[c] = Zb(a[c])
      }
    }else {
      if(a instanceof Object) {
        for(c in b = {}, a) {
          b[c] = Zb(a[c])
        }
      }else {
        b = a
      }
    }
  }
  return b
}
function $b(a, b, c, d) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a instanceof Array ? a[a.length - 1] : a
  }
  var a = Zb(a), b = Zb(b), f = {}, f = ac(a, b);
  if(c) {
    if("gradient" == c) {
      f.key = 0 != I(b, "key").length ? b.key : I(a, "key")
    }else {
      if("format" == c || "text" == c || "save_as_image_item_text" == c || "save_as_pdf_item_text" == c || "print_chart_item_text" == c) {
        return b
      }
      if("action" == c) {
        return 0 == I(b, "arg").length ? ac(a, b, f) : f = bc(a, b, d), f
      }
      f = bc(a, b, d)
    }
  }else {
    f = bc(a, b, d)
  }
  return f
}
function cc(a, b) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a
  }
  if(!dc(a)) {
    var c = Zb(a), d;
    for(d in b) {
      if(0 == I(c, d).length) {
        c[d] = b[d]
      }else {
        if(A.isArray(b[d])) {
          for(var f = 0;f < b[d].length;f++) {
            ec(c[d], b[d][f])
          }
        }else {
          ec(c[d], b[d])
        }
      }
    }
  }
  return c
}
function ec(a, b) {
  var c = F(b, "name");
  if(fc(a, c)) {
    var d;
    a: {
      c = Cb(c);
      if(!dc(a)) {
        for(d in a) {
          if(C(a[d], "name") && N(a[d], "name") == c) {
            break a
          }
        }
      }
      d = p
    }
    a[d] = b
  }else {
    a.push(b)
  }
}
function gc(a, b, c) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a
  }
  var a = Zb(a), d;
  for(d in b) {
    a[d] ? d == c ? (C(a[d], "name") && C(b[d], "name") && fc(a[d], F(a[d], "name")) == F(b[d], "name") && delete a[d], a[d] = b[d]) : a[d] = $b(b[d], a[d], d, h) : a[d] = b[d]
  }
  return a
}
function bc(a, b, c) {
  if(b instanceof Function) {
    return b
  }
  if(a instanceof Array && b instanceof Object && !(b instanceof Array)) {
    return $b(a[a.length - 1], b)
  }
  if(a instanceof Object && b instanceof Object) {
    var d, f;
    f = {};
    b instanceof Array && a instanceof Array && (f = []);
    for(d in a) {
      d != c ? (f[d] = $b(a[d], b[d]), delete b[d]) : f[d] = b[d]
    }
    for(d in b) {
      f[d] = b[d]
    }
  }else {
    f = b
  }
  return f
}
function ac(a, b, c) {
  c || (c = {});
  for(var d in a) {
    dc(a[d]) && (c[d] = a[d])
  }
  for(d in b) {
    dc(b[d]) && (c[d] = b[d])
  }
  return c
}
function dc(a) {
  return Boolean(a instanceof Function || !(a instanceof Array || a instanceof Object))
}
function fc(a, b) {
  return hc(a, "name", b)
}
function hc(a, b, c) {
  c = Cb(c);
  if(!dc(a)) {
    for(var d in a) {
      if(A.isArray(a[d])) {
        var f = fc(a[d], c);
        if(f) {
          return f
        }
      }
      if(C(a[d], b) && N(a[d], b) == c) {
        return a[d]
      }
    }
  }
  return p
}
function ic(a, b, c) {
  var d = a.length;
  if(b > d) {
    return a.push(c), a
  }
  var f = [], g;
  for(g = 0;g < b;g++) {
    f[g] = a[g]
  }
  f[b] = c;
  for(g = b + 1;g <= d;g++) {
    f[g] = a[g - 1]
  }
  return f
}
function jc(a, b) {
  var c = a.length, d = [], f = [], g;
  for(g = 0;g < c;g++) {
    if(C(a[g], "id")) {
      var k = F(a[g], "id");
      k == b && (d.push(g), f.push(k))
    }
  }
  c = d.length;
  for(g = 0;g < c;g++) {
    a.splice(d[g], 1)
  }
  return f
}
function kc(a, b) {
  Lb(b, "id");
  ac(a, b, a);
  for(var c in b) {
    c == p || c == h || (a[c] = "actions" == c || "extra_labels" == c || "extra_markers" == c || "extra_tooltips" == c ? b[c] : $b(a[c], b[c], c))
  }
}
function lc(a, b) {
  if(!a) {
    return p
  }
  var c = b.length;
  return!b || 0 == c ? a : mc(a, b, c)
}
function mc(a, b, c) {
  var d;
  if(a instanceof Function) {
    d = a
  }else {
    if(a instanceof Array) {
      d = [];
      for(var f = 0, g = a.length;f < g;f++) {
        d[f] = mc(a[f], b, c)
      }
    }else {
      if(a instanceof Object) {
        for(f in d = {}, a) {
          d[f] = mc(a[f], b, c)
        }
      }else {
        d = a;
        for(f = 0;f < c;f++) {
          -1 != a.indexOf(b[f][0].toString()) && (d = a.split(b[f][0]).join(b[f][1]))
        }
      }
    }
  }
  return d
}
function nc() {
}
var oc = p;
function pc(a, b) {
  var c = qc(a, b), d, f, g;
  if(0 == c.length % 2) {
    if(f = c[Math.floor(c.length / 2) - 1], g = c[Math.floor(c.length / 2) + 1], f == p || g == p) {
      return NaN
    }
  }else {
    if(d = c[Math.ceil(c.length / 2)], d == p) {
      return NaN
    }
  }
  return d ? d.Na(oc) : (f.Na(oc) + g.Na(oc)) / 2
}
nc.prototype.ml = function(a, b) {
  var c = {}, d, f = 0, g = 0;
  for(d = 0;d < a.length;d++) {
    var k = Number(a[d].Na(b));
    k && (c[k] != p ? c[k]++ : c[k] = 1, c[k] > g && (g = c[k], f = k))
  }
  return f
};
function qc(a, b) {
  oc = b;
  for(var c = [], d = 0, d = 0;d < a.length;d++) {
    c.push(a[d])
  }
  return c.sort(function(a, b) {
    return Number(a.Na(oc)) - Number(b.Na(oc))
  })
}
function rc(a, b) {
  var c = Math.floor(b);
  a.setUTCMonth(a.getUTCMonth() + c);
  b != c && tc(a, 30 * (b - c))
}
function tc(a, b) {
  var c = Math.floor(b);
  a.setUTCDate(a.getUTCDate() + c);
  b != c && uc(a, 24 * (b - c))
}
function uc(a, b) {
  var c = Math.floor(b);
  a.setUTCHours(a.getUTCHours() + c);
  b != c && vc(a, 60 * (b - c))
}
function vc(a, b) {
  var c = Math.floor(b);
  a.setUTCMinutes(a.getUTCMinutes() + c);
  b != c && a.setUTCSeconds(a.getUTCSeconds() + 60 * (b - c))
}
function wc() {
  return"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
}
;function xc(a, b, c) {
  isNaN(a) || (this.ad = a);
  b && (this.Xa = b);
  c && (this.Aa = c)
}
z = xc.prototype;
z.Aa = NaN;
z.Ib = u("Aa");
z.qb = t("Aa");
z.Xa = zb(p, 1);
z.Ba = u("Xa");
z.ad = 1;
z.g = function(a) {
  C(a, "position") && (this.Aa = Sb(F(a, "position")));
  C(a, "opacity") && (this.ad *= Sb(F(a, "opacity")));
  C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad))
};
z.MB = 0;
z.Gz = function(a) {
  this.MB = ub(this.Xa.Ba(a))
};
z.toString = function() {
  return"{ position: " + this.Aa + ", opacity: " + this.ad + ", calculatedColor: " + this.MB + "}"
};
function yc(a) {
  this.ad = a;
  this.pb = []
}
z = yc.prototype;
z.ad = 1;
z.pb = p;
z.ac = 0;
z.Wd = t("ac");
z.Rt = 0;
z.Ra = 0;
z.Jb = u("Ra");
z.gh = t("Ra");
z.k = p;
z.n = u("k");
z.g = function(a) {
  C(a, "type") && (this.Ra = "radial" == Vb(F(a, "type")) ? 1 : 0);
  C(a, "angle") && (this.ac = Ob(F(a, "angle")));
  if(C(a, "focal_point")) {
    var b = Number(F(a, "focal_point"));
    1 < b ? b = 1 : -1 > b && (b = -1);
    this.Rt = b
  }
  for(var a = I(a, "key"), b = a.length, c = 0;c < b;c++) {
    var d = new xc(this.ad);
    d.g(a[c]);
    this.pb[c] = d
  }
  a = this.pb.length;
  isNaN(this.pb[0].Ib()) && this.pb[0].qb(0);
  isNaN(this.pb[a - 1].Ib()) && this.pb[a - 1].qb(1);
  for(b = 1;;) {
    for(;b < a && !isNaN(this.pb[b].Ib());) {
      b++
    }
    if(b == a) {
      break
    }
    for(var c = b - 1, f = a - 1;b < a && isNaN(this.pb[b].Ib());) {
      b++
    }
    for(var d = this.pb[c].Ib(), f = this.pb[f].Ib(), g = 1;g < b - c;g++) {
      this.pb[g].qb(d + g * (f - d) / (b - c))
    }
  }
};
z.toString = function() {
  for(var a = "{ type: " + this.Ra + ", angle: " + this.ac + ", focal_point: " + this.Rt + ", keys: [", b = [], c = 0;c < this.pb.length;c++) {
    b.push(this.pb[c].toString())
  }
  a += b.join(", ");
  a = a + "]," + this.k.toString();
  return a + "}"
};
z.Gz = function(a, b) {
  this.k = a;
  for(var c = this.pb.length, d = 0;d < c;d++) {
    this.pb[d].Gz(b)
  }
};
function zc(a) {
  this.F = a;
  this.Um = {}
}
var Ac = 0;
zc.prototype.F = p;
zc.prototype.Um = p;
function Bc(a, b, c) {
  c || (c = q);
  var d = b.toString();
  if(a.Um[d]) {
    return a.Um[d]
  }
  if(0 == b.Jb()) {
    var f = c, c = Cc(a.F, "linearGradient");
    if(f) {
      var f = b.ac, g = b.n(), k = g.y + g.height / 2, l = g.x + g.width, n = g.y + g.height / 2, f = "rotate(" + f + ")";
      c.setAttribute("x1", g.x);
      c.setAttribute("y1", k);
      c.setAttribute("x2", l);
      c.setAttribute("y2", n);
      c.setAttribute("gradientTransform", f);
      c.setAttribute("spreadMethod", "pad");
      c.setAttribute("gradientUnits", "userSpaceOnUse")
    }else {
      f = b.ac * Math.PI / 180, f -= Math.PI / 2, l = Math.tan(f), k = g = 0, Math.abs(l) != Number.POSITIVE_INFINITY && (g = l / 2, k = 1 / (2 * l)), l = q, 0.5 >= Math.abs(g) ? (k = -0.5, l = 0 > Math.cos(f)) : (g = -0.5, l = 0 < Math.sin(f)), f = {x:0.5 + g, y:0.5 + k}, g = {x:0.5 - g, y:0.5 - k}, f = l ? {start:g, end:f} : {start:f, end:g}, c.setAttribute("x1", f.start.x), c.setAttribute("y1", f.start.y), c.setAttribute("x2", f.end.x), c.setAttribute("y2", f.end.y), c.setAttribute("gradientUnits", 
      "objectBoundingBox")
    }
    Dc(a, c, b);
    b = c
  }else {
    c || (c = q), c ? (c = Cc(a.F, "radialGradient"), f = b.ac * Math.PI / 180, k = Math.cos(f), l = Math.sin(f), n = b.n(), f = n.x + n.width / 2, g = n.y + n.height / 2, r = Math.min(n.width, n.height) / 2, n = b.Rt, k = f + n * r * k, l = g + n * r * l, c.setAttribute("cx", f), c.setAttribute("cy", g), c.setAttribute("r", r), c.setAttribute("fx", k), c.setAttribute("fy", l), c.setAttribute("gradientUnits", "userSpaceOnUse")) : (c = Cc(a.F, "radialGradient"), f = b.ac * Math.PI / 180, k = Math.cos(f), 
    l = Math.sin(f), f = new O(0.5, 0.5), g = new O(b.Rt * k, b.Rt * l), c.setAttribute("cx", f.x), c.setAttribute("cy", f.y), c.setAttribute("fx", (g.x + 1) / 2), c.setAttribute("fy", (g.y + 1) / 2), c.setAttribute("r", 1), c.setAttribute("gradientUnits", "objectBoundingBox")), c.setAttribute("spreadMethod", "pad"), Dc(a, c, b), b = c
  }
  c = "gradient_" + (Ac++).toString();
  b.setAttribute("id", c);
  c = "url(#" + c + ")";
  a.F.hl.appendChild(b);
  return a.Um[d] = c
}
function Ec(a, b) {
  var c = b.substr(b.indexOf("fill:url(#") + 10, b.length - 10 - 2), d = "url(#" + c + ")", f;
  for(f in a.Um) {
    if(a.Um[f] == d) {
      delete a.Um[f];
      var g = c, k = a.F.hl, l = k.childNodes, n = h;
      for(n in l) {
        l[n] != h && l[n].id == g && k.removeChild(l[n])
      }
    }
  }
}
function Dc(a, b, c) {
  for(var d = c.pb.length, f = 0;f < d;f++) {
    var g = c.pb[f], k = Cc(a.F, "stop");
    k.setAttribute("offset", "" + 100 * g.Aa + "%");
    k.setAttribute("style", "stop-color:" + g.MB + ";stop-opacity:" + g.ad);
    b.appendChild(k)
  }
}
zc.prototype.clear = function() {
  Ac = 0;
  this.Um = {}
};
function Fc() {
}
function Gc(a) {
  switch(a) {
    case "backwarddiagonal":
      return 0;
    case "forwarddiagonal":
      return 1;
    case "horizontal":
      return 2;
    case "vertical":
      return 3;
    case "dashedbackwarddiagonal":
      return 4;
    case "dashedforwarddiagonal":
      return 6;
    case "dashedhorizontal":
      return 7;
    case "dashedvertical":
      return 8;
    case "diagonalcross":
      return 9;
    case "diagonalbrick":
      return 10;
    case "divot":
      return 11;
    case "horizontalbrick":
      return 12;
    case "verticalbrick":
      return 13;
    case "checkerboard":
      return 14;
    case "confetti":
      return 15;
    case "plaid":
      return 16;
    case "soliddiamond":
      return 17;
    case "zigzag":
      return 18;
    case "weave":
      return 19;
    case "percent05":
      return 20;
    case "percent10":
      return 21;
    case "percent20":
      return 22;
    case "percent25":
      return 23;
    case "percent30":
      return 24;
    case "percent40":
      return 25;
    case "percent50":
      return 26;
    case "percent60":
      return 27;
    case "percent70":
      return 28;
    case "percent75":
      return 29;
    case "percent80":
      return 30;
    case "percent90":
      return 31;
    case "grid":
      return 5;
    default:
      return 0
  }
}
z = Fc.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.Ra = 0;
z.cE = q;
z.Jb = u("Ra");
z.Xa = zb(p, 0.5);
z.Ba = u("Xa");
z.ad = 0.5;
z.Rh = 1;
z.Va = u("Rh");
z.Az = 10;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      var b = N(a, "type");
      this.cE = "%hatchtype" == b;
      this.cE || (this.Ra = Gc(Vb(b)))
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(N(a, "color"), this.ad));
    C(a, "thickness") && (this.Rh = Ob(F(a, "thickness")));
    C(a, "pattern_size") && (this.Az = Math.floor(Number(F(a, "pattern_size"))))
  }
};
z.toString = function() {
  return"T" + this.Ra + "C" + vb(this.Xa.Ba()) + "TH" + this.Rh + "PS" + this.Az
};
function Hc(a, b, c, d) {
  if(!a.Ma) {
    return""
  }
  a.Ra = a.cE ? c : a.Ra;
  b = b.FD;
  c = a.toString();
  if(b.hv[c]) {
    a = b.hv[c]
  }else {
    var f = "hatchfill_" + Ic++, g = "" + a.Az, k = Cc(b.F, "pattern");
    k.setAttribute("id", f);
    k.setAttribute("x", 0);
    k.setAttribute("y", 0);
    k.setAttribute("width", g);
    k.setAttribute("height", g);
    k.setAttribute("patternUnits", "userSpaceOnUse");
    var g = a.Az, l, n, m;
    switch(a.Jb()) {
      case 0:
        l = b.F.ja();
        m = Jc(-1, 0, g + 1, 0, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(-45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 1:
        l = b.F.ja();
        m = Jc(-1, 0, g + 1, 0, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 2:
        l = b.F.ja();
        m = Jc(-1, g / 2, g + 1, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 3:
        l = b.F.ja();
        m = Jc(g / 2, -1, g / 2, g + 1, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 9:
        l = b.F.ja();
        m = Jc(0, g / 2, g, g / 2, a.Va());
        m += Jc(g / 2, 0, g / 2, g, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 5:
        l = b.F.ja();
        m = Jc(-1, g / 2, g + 1, g / 2, a.Va());
        m += Jc(g / 2, -1, g / 2, g + 1, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 12:
        Lc(b, k, a, g, d);
        break;
      case 13:
        Lc(b, k, a, g, d);
        k.setAttribute("patternTransform", "rotate(90)");
        break;
      case 10:
        b.F.ja();
        Lc(b, k, a, g, d);
        k.setAttribute("patternTransform", "rotate(45)");
        break;
      case 14:
        l = Mc(b.F, 0, 0, g / 2, g / 2);
        g = Mc(b.F, g / 2, g / 2, g, g);
        k.appendChild(l);
        k.appendChild(g);
        Nc(a, l, d);
        Nc(a, g, d);
        break;
      case 15:
        l = g / 8;
        g = new P(0, 0, g / 4, g / 4);
        Q(b.F, 0, 2 * l, g.width, g.height, a, k, d);
        Q(b.F, l, 5 * l, g.width, g.height, a, k, d);
        Q(b.F, 2 * l, 0, g.width, g.height, a, k, d);
        Q(b.F, 4 * l, 4 * l, g.width, g.height, a, k, d);
        Q(b.F, 5 * l, l, g.width, g.height, a, k, d);
        Q(b.F, 6 * l, 6 * l, g.width, g.height, a, k, d);
        break;
      case 16:
        Q(b.F, 0, 0, g / 2, g / 2, a, k, d);
        l = g / 8;
        n = q;
        for(m = 0;2 > m;m++) {
          n = q;
          for(var o = 0;4 > o;o++) {
            n = !n;
            for(var v = 0;4 > v;v++) {
              n && Q(b.F, o * l + m * g / 2, v * l + g / 2, l, l, a, k, d), n = !n
            }
          }
        }
        break;
      case 17:
        l = b.F.ja();
        m = S(g / 2, 0);
        m += U(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        m += U(g / 2, 0);
        l.setAttribute("d", m + " Z");
        k.appendChild(l);
        Nc(a, l, d);
        break;
      case 6:
        l = b.F.ja();
        m = Jc(0, 0, g / 2, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 4:
        l = b.F.ja();
        m = Jc(g / 2, 0, 0, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 7:
        l = b.F.ja();
        m = Jc(0, 0, g / 2, 0, a.Va());
        m += Jc(g / 2, g / 2, g, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 8:
        l = b.F.ja();
        m = Jc(0, 0, 0, g / 2, a.Va());
        m += Jc(g / 2, g / 2, g / 2, g, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 11:
        o = 0.1 * g;
        n = g * (0.8 - 0.2) / 2;
        l = b.F.ja();
        m = S(o + n, o);
        m += U(o, o + n / 2);
        m += U(o + n, o + n);
        m += S(g - o - n, g - o - n);
        m += U(g - o, g - o - n / 2);
        m += U(g - o - n, g - o);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 18:
        l = b.F.ja();
        m = S(0, 0);
        m += U(g / 2, g / 2);
        m += U(g, 0);
        m += S(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 19:
        l = b.F.ja();
        m = S(0, 0);
        m += U(g / 2, g / 2);
        m += U(g, 0);
        m += S(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        m += S(g / 2, g / 2);
        m += U(3 * g / 4, 3 * g / 4);
        m += S(g, g / 2);
        m += U(3 * g / 4, g / 4);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 20:
        k.setAttribute("width", 8);
        k.setAttribute("height", 8);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 4, 4, 1, 1, a, k, d);
        break;
      case 21:
        k.setAttribute("width", 8);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 4, 2, 1, 1, a, k, d);
        break;
      case 22:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        break;
      case 23:
        k.setAttribute("width", 4);
        k.setAttribute("height", 2);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 1, 1, 1, a, k, d);
        break;
      case 24:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        break;
      case 25:
        k.setAttribute("width", 4);
        k.setAttribute("height", 8);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 3, 3, 1, 1, a, k, d);
        Q(b.F, 0, 4, 1, 1, a, k, d);
        Q(b.F, 2, 4, 1, 1, a, k, d);
        Q(b.F, 1, 5, 1, 1, a, k, d);
        Q(b.F, 3, 5, 1, 1, a, k, d);
        Q(b.F, 0, 6, 1, 1, a, k, d);
        Q(b.F, 2, 6, 1, 1, a, k, d);
        Q(b.F, 1, 7, 1, 1, a, k, d);
        Q(b.F, 3, 7, 1, 1, a, k, d);
        break;
      case 26:
        k.setAttribute("width", 2);
        k.setAttribute("height", 2);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        break;
      case 27:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 0, 1, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 2, 3, 1, 1, a, k, d);
        Q(b.F, 3, 3, 1, 1, a, k, d);
        break;
      case 28:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 0, 1, 1, a, k, d);
        Q(b.F, 0, 1, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        Q(b.F, 2, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 3, 2, 1, 1, a, k, d);
        Q(b.F, 0, 3, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 2, 3, 1, 1, a, k, d);
        break;
      case 29:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 4, 4, a, k, d);
        Oc(b.F, 0, 0, k);
        Oc(b.F, 2, 2, k);
        break;
      case 30:
        k.setAttribute("width", 8);
        k.setAttribute("height", 4);
        for(g = 0;8 > g;g++) {
          for(l = 0;4 > l;l++) {
            Q(b.F, g, l, 1, 1, a, k, d)
          }
        }
        Oc(b.F, 0, 0, k);
        Oc(b.F, 4, 2, k);
        break;
      case 31:
        k.setAttribute("width", 8);
        k.setAttribute("height", 8);
        for(g = 0;8 > g;g++) {
          for(l = 0;8 > l;l++) {
            Q(b.F, g, l, 1, 1, a, k, d)
          }
        }
        Oc(b.F, 7, 7, k);
        Oc(b.F, 4, 3, k)
    }
    b.F.hl.appendChild(k);
    f = "url(#" + f + ")";
    a = b.hv[c] = f
  }
  return"fill:" + a + ";"
}
function Pc(a) {
  this.F = a;
  this.hv = {}
}
var Ic = 0;
Pc.prototype.F = p;
Pc.prototype.hv = p;
function Lc(a, b, c, d, f) {
  var a = a.F.ja(), g = Jc(0, 0, 0, d / 2 - 1, c.Va()), g = g + Jc(0, d / 2 - 1, d, d / 2 - 1, c.Va()), g = g + Jc(d / 2, d / 2 - 1, d / 2, d - 1, c.Va()), g = g + Jc(0, d - 1, d, d - 1, c.Va());
  a.setAttribute("d", g);
  b.appendChild(a);
  Kc(c, a, f)
}
function Q(a, b, c, d, f, g, k, l) {
  a = Mc(a, b, c, d, f);
  Nc(g, a, l);
  k.appendChild(a)
}
function Oc(a, b, c, d) {
  a = Mc(a, b, c, 1, 1);
  a.setAttribute("fill", "white");
  a.setAttribute("stroke", "none");
  d.appendChild(a)
}
function Kc(a, b, c) {
  b.setAttribute("fill", "none");
  b.setAttribute("stroke-width", a.Va());
  b.setAttribute("stroke-opacity", a.ad);
  b.setAttribute("stroke", ub(a.Ba().Ba(c)))
}
function Nc(a, b, c) {
  b.setAttribute("fill", ub(a.Ba().Ba(c)));
  b.setAttribute("fill-opacity", a.ad);
  b.setAttribute("stroke", "none")
}
Pc.prototype.clear = function() {
  this.wE = 0;
  this.hv = {}
};
function Rc() {
  this.Ma = j
}
z = Rc.prototype;
z.Ma = p;
z.isEnabled = function() {
  return Boolean(this.Ma && this.Hq && 0 < this.Hq.length)
};
z.Hq = p;
z.g = s();
z.toString = function() {
  for(var a = this.Hq.length, b = "", c = 0;c < a;c++) {
    var d = this.Hq[c];
    d && d.isEnabled() && (b += d.toString())
  }
  return b
};
function Sc(a) {
  this.F = a;
  this.clear()
}
Sc.prototype.wE = p;
Sc.prototype.Ax = p;
function Tc(a, b) {
  var c = b.toString();
  if(a.Ax[c]) {
    return a.Ax[c]
  }
  var d;
  d = a.F;
  if(b.Ma) {
    var f = Cc(d, "filter"), g = b.Hq.length;
    if(0 == g) {
      d = p
    }else {
      for(var k = 0;k < g;k++) {
        var l = b.Hq[k];
        l && l.isEnabled() && l.hQ(f, d)
      }
      d = f
    }
  }else {
    d = p
  }
  if(!d) {
    return"filter:none"
  }
  f = "effect_" + a.wE++;
  d.setAttribute("id", f);
  d.setAttribute("filterUnits", "userSpaceOnUse");
  a.F.hl.appendChild(d);
  f = "url(#" + f + ")";
  return a.Ax[c] = f
}
Sc.prototype.clear = function() {
  this.wE = 0;
  this.Ax = {}
};
function Uc(a, b) {
  return 0 >= a ? 0 : a * Math.round(1E15 * Math.cos(b * Math.PI / 180)) / 1E15
}
function Vc(a, b) {
  return 0 >= a ? 0 : a * Math.round(1E15 * Math.sin(b * Math.PI / 180)) / 1E15
}
function O(a, b) {
  a != h && (this.x = Number(a));
  b != h && (this.y = Number(b))
}
O.prototype.x = 0;
O.prototype.y = 0;
O.prototype.Ca = function() {
  return new O(this.x, this.y)
};
O.prototype.translate = function(a, b) {
  this.x += a;
  this.y += b
};
function P(a, b, c, d) {
  a != h && (this.x = Number(a));
  b != h && (this.y = Number(b));
  c != h && (this.width = Number(c));
  d != h && (this.height = Number(d))
}
z = P.prototype;
z.x = 0;
z.y = 0;
z.width = 0;
z.height = 0;
z.tb = u("x");
z.rb = u("y");
z.Ja = function() {
  return this.x + this.width
};
z.ra = function() {
  return this.y + this.height
};
z.bh = function(a) {
  a -= this.tb();
  this.x += a;
  this.width -= a
};
z.eh = function(a) {
  this.width -= this.Ja() - a
};
z.fh = function(a) {
  a -= this.rb();
  this.y += a;
  this.height -= a
};
z.$g = function(a) {
  this.height -= this.ra() - a
};
z.ae = function() {
  return new O(this.x + this.width / 2, this.y + this.height / 2)
};
function Wc(a, b) {
  return!(a.Ja() < b.tb() || a.ra() < b.rb() || a.tb() > b.Ja() || a.rb() > b.ra())
}
function Xc(a, b) {
  return a.x <= b.x && a.Ja() >= b.Ja() && a.y <= b.y && a.ra() >= b.ra()
}
z.translate = function(a, b) {
  this.x += a;
  this.y += b
};
function Yc(a, b, c) {
  a.x -= b;
  a.width += 2 * b;
  a.y -= c;
  a.height += 2 * c
}
z.Ca = function() {
  return new P(this.x, this.y, this.width, this.height)
};
z.toString = function() {
  return this.x.toFixed(1) + " " + this.y.toFixed(1) + " " + this.width.toFixed(1) + " " + this.height.toFixed(1)
};
function Zc() {
}
z = Zc.prototype;
z.bc = 0;
z.Ol = 0;
z.sin = 0;
z.cos = 1;
z.xf = 0;
z.wf = 1;
z.Ae = function() {
  360 <= Math.abs(this.bc) && (this.bc %= 360);
  0 > this.bc && (this.bc += 360);
  this.Ol = this.bc * Math.PI / 180;
  this.sin = Math.sin(this.Ol);
  this.cos = Math.cos(this.Ol);
  this.xf = 0 > this.sin ? -this.sin : this.sin;
  this.wf = 0 > this.cos ? -this.cos : this.cos
};
function $c() {
  this.Mk = document.createElementNS(this.Tv, "svg");
  this.Mk.setAttribute("xmlns", this.Tv);
  this.Mk.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
  this.gs = new P;
  ad(this);
  this.Jr = this.Ca(this.Mk);
  this.Jr.setAttribute("id", "AnyChart SVG element for text measure");
  this.Jr.setAttribute("width", "0");
  this.Jr.setAttribute("height", "0");
  this.hl = Cc(this, "defs");
  this.Mk.appendChild(this.hl);
  this.cr = new zc(this);
  this.HD = new cd(this);
  this.OK = new dd(this);
  this.FD = new Pc(this);
  this.hk = new Sc(this)
}
z = $c.prototype;
z.Tv = "http://www.w3.org/2000/svg";
z.Mk = p;
z.Ic = u("Mk");
z.gs = p;
z.lD = function() {
  ad(this);
  return this.gs
};
function ad(a) {
  try {
    var b = a.Mk.getBBox();
    a.gs.x = b.x;
    a.gs.y = b.y;
    a.gs.width = b.width;
    a.gs.height = b.height
  }catch(c) {
  }
}
z.hl = p;
z.Jr = p;
z.hk = p;
z.cr = p;
z.HD = p;
z.OK = p;
z.FD = p;
z.ja = function(a) {
  var b = Cc(this, "path");
  a && b.setAttribute("shape-rendering", "crispEdges");
  return b
};
function S(a, b) {
  return"M" + a.toString() + "," + b.toString()
}
function U(a, b) {
  return" L" + a.toString() + "," + b.toString()
}
function Jc(a, b, c, d, f) {
  a === c && (a = c = Math.round(a) + f % 2 / 2);
  b === d && (b = d = Math.round(b) + f % 2 / 2);
  return"M" + a.toString() + "," + b.toString() + " L" + c.toString() + "," + d.toString()
}
z.ub = function(a, b, c, d, f, g) {
  5 == arguments.length && (g = a);
  return 0 == a || 0 == g ? "" : " A" + a.toString() + "," + g.toString() + " 0 " + (b ? "1" : "0") + " " + (c ? "1" : "0") + " " + d.toString() + "," + f.toString()
};
function ed(a, b, c, d) {
  var f = b + d, b = b + d * Math.cos(359.999 * Math.PI / 180), g = c + d * Math.sin(359.999 * Math.PI / 180), c = "" + S(f, c);
  return c += a.ub(d, 1, 1, b, g, d)
}
z.XE = function(a, b, c, d, f, g, k) {
  return f && 7 == arguments.length ? " Q" + a.toString() + "," + b.toString() + " " + c.toString() + "," + d.toString() + " T" + g.toString() + "," + k.toString() : " Q" + a.toString() + "," + b.toString() + " " + c.toString() + "," + d.toString()
};
function fd(a, b, c, d) {
  var c = a + c, d = b + d, f = S(a, b), f = f + U(c, b), f = f + U(c, d), f = f + U(a, d);
  return f += U(a, b)
}
function gd(a) {
  return fd(a.x, a.y, a.width, a.height)
}
function hd(a) {
  return Cc(a, "polyline")
}
function id(a, b, c) {
  c && (a = Math.round(10 * a) / 10, b = Math.round(10 * b) / 10);
  return a.toString() + "," + b.toString() + " "
}
var jd = 0;
function Cc(a, b) {
  return a.Mk.ownerDocument.createElementNS(a.Tv, b)
}
function kd(a) {
  for(;0 < a.childNodes.length;) {
    a.removeChild(a.childNodes[0])
  }
}
function Mc(a, b, c, d, f) {
  0 > d && (d = Math.abs(d), b -= d);
  0 > f && (f = Math.abs(f), c -= f);
  a = Cc(a, "rect");
  "undefined" !== typeof b && a.setAttribute("x", b);
  "undefined" !== typeof c && a.setAttribute("y", c);
  "undefined" !== typeof d && a.setAttribute("width", d);
  "undefined" !== typeof f && a.setAttribute("height", f);
  return a
}
function ld(a) {
  a.setAttribute("cursor", "pointer")
}
function md() {
  return"fill:none;"
}
function nd() {
  return"stroke:none;"
}
function od() {
  return md() + nd()
}
$c.prototype.Ca = function(a) {
  return a.cloneNode(q)
};
function W(a) {
  pa.call(this);
  this.G = Cc(a, "g");
  this.F = a;
  this.wq = []
}
A.e(W, pa);
z = W.prototype;
z.Zt = function() {
  return this.G.getAttribute("id")
};
z.F = p;
z.r = u("F");
z.G = p;
z.Ub = 0;
z.ma = 0;
z.Bb = 0;
z.uK = 0;
z.vK = 0;
z.wq = p;
z.jc = p;
z.getParent = u("jc");
z.dm = p;
z.appendChild = function(a) {
  a && this.G.appendChild(a)
};
z.removeChild = function(a) {
  if(a && a.G) {
    var b;
    b = this.wq.length;
    for(var c = 0;c < b;c++) {
      this.wq[c] == a && this.wq.splice(c, 1)
    }
    b = this.PE();
    for(c = 0;c < b;c++) {
      this.G.childNodes[c] == a.G && this.G.removeChild(this.G.childNodes[c])
    }
  }
};
z.PE = function() {
  return this.G.childElementCount
};
z.Xd = function(a) {
  this.Ub = a;
  pd(this)
};
z.Sd = u("Ub");
function qd(a, b) {
  a.Ub += b;
  pd(a)
}
z.gd = function(a) {
  this.ma = a;
  pd(this)
};
z.Xb = u("ma");
z.Vl = function(a, b, c) {
  this.Bb = a;
  b != h && (this.uK = b);
  c != h && (this.vK = c);
  pd(this)
};
z.qb = function(a, b) {
  this.Ub = a;
  this.ma = b;
  pd(this)
};
function pd(a) {
  a.G.setAttribute("transform", "translate(" + Number(a.Ub).toString() + "," + Number(a.ma).toString() + ") rotate(" + Number(a.Bb) + " " + Number(a.uK).toString() + " " + Number(a.vK).toString() + ")")
}
z.Bj = function(a) {
  this.dm = a;
  a === j ? this.G.setAttribute("visibility", "visible") : this.G.setAttribute("visibility", "hidden")
};
z.clear = function() {
  kd(this.G)
};
z.ia = function(a) {
  this.wq.push(a);
  this.G.appendChild(a.G);
  a.jc = this
};
z.bx = p;
function rd(a, b) {
  if(b) {
    var c = Cc(a.F, "clipPath");
    c.setAttribute("id", "anychart_clippath_" + (jd++).toString());
    c.setAttribute("clip-rule", "nonzero");
    a.bx = a.F.ja();
    c.appendChild(a.bx);
    a.appendChild(c);
    a.G.setAttribute("clip-path", "url(#" + c.getAttribute("id") + ")");
    a.G.setAttribute("clipPathUnits", "userSpaceOnUse")
  }
}
function sd(a, b) {
  b && (a.bx || rd(a, b), a.bx.setAttribute("d", gd(b)))
}
function td(a) {
  W.call(this, a);
  this.Gp = new W(a);
  this.wq.push(this.Gp);
  this.G.appendChild(this.Gp.G);
  this.Gp.jc = this
}
A.e(td, W);
td.prototype.Gp = p;
td.prototype.appendChild = function(a) {
  a && this.Gp.appendChild(a)
};
td.prototype.ia = function(a) {
  a && a.G && this.Gp.ia(a)
};
td.prototype.scrollTo = function(a, b) {
  a && !isNaN(a) && b && !isNaN(b) && this.Gp.qb(a, b)
};
function ud(a, b, c, d) {
  switch(b) {
    case 0:
      a.x -= c.width + d;
      break;
    case 1:
      a.x += d;
      break;
    case 2:
      a.x -= c.width / 2
  }
}
function vd(a) {
  a = Cb(a);
  switch(a) {
    case "left":
      return 0;
    case "right":
      return 1;
    case "center":
      return 2;
    default:
      return 2
  }
}
var wd = {};
function xd(a, b, c, d) {
  switch(b) {
    case 0:
      a.y -= c.height + d;
      break;
    case 2:
      a.y += d;
      break;
    case 1:
      a.y -= c.height / 2
  }
}
function yd(a) {
  a = Cb(a);
  switch(a) {
    case "top":
      return 0;
    default:
    ;
    case "center":
      return 1;
    case "bottom":
      return 2
  }
}
function zd(a) {
  a = Cb(a);
  switch(a) {
    default:
    ;
    case "center":
      return 0;
    case "centerleft":
    ;
    case "leftcenter":
    ;
    case "left":
      return 1;
    case "lefttop":
    ;
    case "topleft":
      return 2;
    case "centertop":
    ;
    case "topcenter":
    ;
    case "top":
      return 3;
    case "righttop":
    ;
    case "topright":
      return 4;
    case "centerright":
    ;
    case "rightcenter":
    ;
    case "right":
      return 5;
    case "rightbottom":
    ;
    case "bottomright":
      return 6;
    case "centerbottom":
    ;
    case "bottomcenter":
    ;
    case "bottom":
      return 7;
    case "leftbottom":
    ;
    case "bottomleft":
      return 8;
    case "float":
      return 9;
    case "xaxis":
      return 10
  }
}
function Ad(a) {
  a == p || a == h ? this.Pl(10) : this.Pl(a)
}
z = Ad.prototype;
z.wc = 0;
z.tb = u("wc");
z.bh = t("wc");
z.Qz = 0;
z.Ja = u("Qz");
z.eh = t("Qz");
z.le = 0;
z.rb = u("le");
z.fh = t("le");
z.Mw = 0;
z.ra = u("Mw");
z.$g = t("Mw");
z.Pl = function(a) {
  this.wc = this.Qz = this.le = this.Mw = a
};
z.g = function(a) {
  C(a, "all") && this.Pl(isNaN(M(a, "all")) ? 10 : M(a, "all"));
  C(a, "left") && this.bh(isNaN(M(a, "left")) ? 10 : M(a, "left"));
  C(a, "right") && this.eh(isNaN(M(a, "right")) ? 10 : M(a, "right"));
  C(a, "top") && this.fh(isNaN(M(a, "top")) ? 10 : M(a, "top"));
  C(a, "bottom") && this.$g(isNaN(M(a, "bottom")) ? 10 : M(a, "bottom"))
};
function Bd(a, b) {
  b.x += a.wc;
  b.y += a.le;
  b.width -= a.wc + a.Qz;
  b.height -= a.le + a.Mw
}
;function Cd() {
}
z = Cd.prototype;
z.Ra = 0;
z.Jb = u("Ra");
z.gh = t("Ra");
z.P = 0;
z.ha = 0;
z.kD = u("ha");
z.sa = 0;
z.X = 0;
z.g = function(a) {
  if(C(a, "type")) {
    switch(Cb(F(a, "type"))) {
      case "square":
        this.Ra = 0;
        break;
      case "rounded":
        this.Ra = 1;
        break;
      case "roundedinner":
        this.Ra = 2;
        break;
      case "cut":
        this.Ra = 3
    }
  }
  if(0 != this.Ra && (C(a, "all") && this.Pl(Ob(F(a, "all"))), C(a, "left_top") && (this.P = Ob(F(a, "left_top"))), C(a, "right_top") && (this.ha = Ob(F(a, "right_top"))), C(a, "right_bottom") && (this.X = Ob(F(a, "right_bottom"))), C(a, "left_bottom"))) {
    this.sa = Ob(F(a, "left_bottom"))
  }
};
function Dd(a, b, c) {
  var d;
  switch(a.Ra) {
    default:
    ;
    case 0:
      d = S(c.x, c.y);
      d += U(c.x + c.width, c.y);
      d += U(c.x + c.width, c.y + c.height);
      d += U(c.x, c.y + c.height);
      d += U(c.x, c.y);
      d += " Z";
      break;
    case 1:
      d = S(c.x + a.P, c.y);
      d += U(c.x + c.width - a.ha, c.y);
      d += b.ub(a.ha, q, j, c.x + c.width, c.y + a.ha);
      d += U(c.x + c.width, c.y + c.height - a.X);
      d += b.ub(a.X, q, j, c.x + c.width - a.X, c.y + c.height);
      d += U(c.x + a.sa, c.y + c.height);
      d += b.ub(a.sa, q, j, c.x, c.y + c.height - a.sa);
      0 != a.P && (d += U(c.x, c.y + a.P), d += b.ub(a.P, q, j, c.x + a.P, c.y));
      d += " Z";
      break;
    case 2:
      d = S(c.x + a.P, c.y);
      d += U(c.x + c.width - a.ha, c.y);
      d += b.ub(a.ha, q, q, c.x + c.width, c.y + a.ha);
      d += U(c.x + c.width, c.y + c.height - a.X);
      d += b.ub(a.X, q, q, c.x + c.width - a.X, c.y + c.height);
      d += U(c.x + a.sa, c.y + c.height);
      d += b.ub(a.sa, q, q, c.x, c.y + c.height - a.sa);
      0 != a.P && (d += U(c.x, c.y + a.P), d += b.ub(a.P, q, q, c.x + a.P, c.y));
      d += " Z";
      break;
    case 3:
      d = S(c.x + a.P, c.y), d += U(c.x + c.width - a.ha, c.y), d += U(c.x + c.width, c.y + a.ha), d += U(c.x + c.width, c.y + c.height - a.X), d += U(c.x + c.width - a.X, c.y + c.height), d += U(c.x + a.sa, c.y + c.height), d += U(c.x, c.y + c.height - a.sa), d += U(c.x, c.y + a.P), d += " Z"
  }
  return d
}
z.Pl = function(a) {
  this.P = this.ha = this.X = this.sa = a
};
z.Ca = function(a) {
  a || (a = new Cd);
  a.gh(this.Ra);
  a.sa = this.sa;
  a.X = this.X;
  a.P = this.P;
  a.ha = this.ha;
  return a
};
function Ed(a, b) {
  this.$m = a;
  this.gr = b || Fd
}
var Fd = 0;
Ed.prototype.gr = Fd;
Ed.prototype.$m = p;
function cd(a) {
  this.F = a;
  this.mu = {}
}
var Gd = 0;
cd.prototype.F = p;
cd.prototype.mu = p;
function Hd(a, b, c) {
  var d = b.$m + "-" + b.gr;
  if(a.mu[d]) {
    return a.mu[d]
  }
  var f = Cc(a.F, "pattern"), g = Cc(a.F, "image"), k = "img" + Gd++ + "-" + b.$m + "-" + b.gr;
  f.setAttribute("id", k);
  g.setAttribute("xlink:href", b.$m);
  g.setAttribute("x", 0);
  g.setAttribute("y", 0);
  g.href.baseVal = b.$m;
  g.href.animVal = b.$m;
  f.setAttribute("x", c.x);
  f.setAttribute("y", c.y);
  f.setAttribute("patternUnits", "userSpaceOnUse");
  1 == b.gr ? (f.setAttribute("width", c.width), f.setAttribute("height", c.height), g.setAttribute("width", c.width), g.setAttribute("height", c.height), g.setAttribute("preserveAspectRatio", "none"), g.setAttribute("image-rendering", "optimize-speed")) : 2 == b.gr ? (g.setAttribute("preserveAspectRatio", "xMinYMin meet"), f.setAttribute("width", c.width), f.setAttribute("height", c.height), g.setAttribute("width", c.width), g.setAttribute("height", c.height)) : (f.setAttribute("width", 1), f.setAttribute("height", 
  1), g.setAttribute("width", 1), g.setAttribute("height", 1));
  f.appendChild(g);
  k = "url(#" + k + ")";
  a.F.hl.appendChild(f);
  a.mu[d] = k;
  var l = new Image;
  l.src = b.$m;
  b.gr == Fd && (l.onload = function() {
    var a = l.width, b = l.height;
    f.setAttribute("width", a);
    f.setAttribute("height", b);
    g.setAttribute("width", a);
    g.setAttribute("height", b)
  });
  return k
}
cd.prototype.clear = function() {
  Gd = 0;
  this.mu = {}
};
function Id() {
}
z = Id.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.Ra = 0;
z.se = function() {
  return 1 == this.Ra
};
z.Xa = zb("#EEEEEE", 1);
z.Ba = u("Xa");
z.ad = 1;
z.qy = p;
z.yb = p;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      switch(Vb(F(a, "type"))) {
        case "solid":
          this.Ra = 0;
          break;
        case "gradient":
          this.Ra = 1;
          break;
        case "image":
          this.Ra = 2
      }
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad));
    if(2 == this.Ra) {
      var b, c;
      C(a, "image_url") && (b = F(a, "image_url"));
      if(C(a, "image_mode")) {
        switch(Vb(F(a, "image_mode"))) {
          case "stretch":
            c = 1;
            break;
          case "tile":
            c = Fd;
            break;
          case "fitbyproportions":
            c = 2
        }
      }
      this.qy = new Ed(b, c)
    }
    1 == this.Ra && C(a, "gradient") && (this.yb = new yc(this.ad), this.yb.g(F(a, "gradient")));
    if(2 == this.Ra && (!this.qy || !this.qy.$m)) {
      this.Ra = 0
    }
    if(1 == this.Ra && (this.yb == p || this.yb.pb == p)) {
      this.Ra = 0
    }
  }
};
function Jd(a, b, c, d, f) {
  if(!a.Ma) {
    return""
  }
  switch(a.Ra) {
    case 0:
      return"fill:" + ub(a.Xa.Ba(d)) + ";fill-opacity:" + a.ad + ";";
    case 1:
      return a.yb.Gz(c, d), "fill:" + Bc(b.cr, a.yb, f) + ";";
    case 2:
      return"fill:" + Hd(b.HD, a.qy, c) + ";"
  }
}
;function Kd() {
}
z = Kd.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.Ra = 0;
z.se = function() {
  return 1 == this.Ra
};
z.Jb = u("Ra");
z.Xa = zb(p, 1);
z.ad = 1;
z.Rh = 1;
z.Va = u("Rh");
z.at = 1;
z.Oy = 1;
z.yb = p;
z.CH = q;
z.BH = 2;
z.HK = 2;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      switch(Vb(F(a, "type"))) {
        case "solid":
          this.Ra = 0;
          break;
        case "gradient":
          this.Ra = 1
      }
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad));
    1 == this.Ra && C(a, "gradient") && (this.yb = new yc(this.ad), this.yb.g(F(a, "gradient")));
    if(1 == this.Ra && (this.yb == p || this.yb.pb == p)) {
      this.Ra = 0
    }
    C(a, "thickness") && (this.Rh = Ob(F(a, "thickness")));
    if(C(a, "caps")) {
      switch(Vb(F(a, "caps"))) {
        case "none":
          this.at = 0;
          break;
        case "square":
          this.at = 2;
          break;
        default:
          this.at = 1
      }
    }
    if(C(a, "joints")) {
      switch(Vb(F(a, "joints"))) {
        default:
        ;
        case "round":
          this.Oy = 1;
          break;
        case "miter":
          this.Oy = 0;
          break;
        case "bevel":
          this.Oy = 2
      }
    }
    C(a, "dashed") && (this.CH = Rb(F(a, "dashed")));
    C(a, "dash_length") && (this.BH = Ob(F(a, "dash_length")));
    C(a, "space_length") && (this.HK = Ob(F(a, "space_length")))
  }
};
function Ld(a, b, c, d, f) {
  if(!a.Ma) {
    return""
  }
  var g = "stroke-width:" + a.Rh + ";", k;
  a: {
    switch(a.at) {
      case 0:
        k = "butt";
        break a;
      case 1:
        k = "round";
        break a;
      case 2:
        k = "square"
    }
  }
  var l;
  a: {
    switch(a.Oy) {
      case 0:
        l = "miter";
        break a;
      case 1:
        l = "round";
        break a;
      case 2:
        l = "bevel"
    }
  }
  g = g + ("stroke-linecap:" + k + ";") + ("stroke-linejoin:" + l + ";") + ("stroke-opacity:" + a.ad + ";");
  switch(a.Ra) {
    case 0:
      g += "stroke:" + ub(a.Xa.Ba(d)) + ";";
      break;
    case 1:
      a.yb.Gz(c, d), g += "stroke:" + Bc(b.cr, a.yb, f) + ";"
  }
  a.CH && (g += "stroke-dasharray: " + a.BH + ", " + a.HK + ";");
  return g
}
;function Md(a, b, c) {
  W.call(this, a);
  this.Hg = b;
  this.Mg = c;
  this.appendChild(b);
  this.appendChild(c)
}
A.e(Md, W);
Md.prototype.Hg = p;
Md.prototype.Mg = p;
function Nd() {
}
z = Nd.prototype;
z.sc = p;
z.Mc = u("sc");
function Od(a) {
  return a.sc && a.sc.isEnabled()
}
z.Hb = p;
z.mc = u("Hb");
function Sd(a) {
  return a.Hb && a.Hb.isEnabled()
}
z.Pc = p;
z.De = u("Pc");
z.Ma = j;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.Ve = p;
z.ll = u("Ve");
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(Ub(a, "fill") && (this.Hb = new Id, this.Hb.g(F(a, "fill")), this.Hb.isEnabled() || (this.Hb = q)), Ub(a, "border") && (this.sc = new Kd, this.sc.g(F(a, "border")), this.sc.isEnabled() || (this.sc = q)), Ub(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")), this.Pc.isEnabled() || (this.Pc = q)), Ub(a, "effects")) {
      this.Ve = new Rc, this.Ve.g(F(a, "effects")), this.Ve.isEnabled() || (this.Ve = q)
    }
  }
};
function Td() {
}
A.e(Td, Nd);
z = Td.prototype;
z.D = p;
z.fb = u("D");
z.p = function(a) {
  if(!this.Ma) {
    return p
  }
  var b = this.createElement(a), c = this.createElement(a);
  return this.D = new Md(a, b, c)
};
z.qa = function(a, b, c) {
  var d = a.r(), f;
  this.Ve && this.Ve.isEnabled() && (f = Tc(d.hk, this.Ve));
  d = this.yh(d, b);
  this.rC(a.Hg, d, a, b, c, f);
  Ud(this, a.Mg, d, a, f)
};
z.rC = function(a, b, c, d, f, g) {
  var c = c.r(), k = "", k = this.Hb && this.Hb.isEnabled() ? k + Jd(this.Hb, c, d, f) : k + md(), k = this.sc && this.sc.isEnabled() ? k + Ld(this.sc, c, d, f) : k + nd();
  Vd(a, b, k, g)
};
function Ud(a, b, c, d, f) {
  var g = "", g = a.Pc && a.Pc.isEnabled() ? Hc(a.Pc, d.r()) : od();
  Vd(b, c, g, f)
}
function Vd(a, b, c, d) {
  d ? a.setAttribute("filter", d) : a.removeAttribute("filter");
  a.setAttribute("d", b);
  a.setAttribute("style", c)
}
z.Wa = function(a, b) {
  var c = this.yh(a.r(), b);
  a.Hg.setAttribute("d", c);
  a.Mg.setAttribute("d", c)
};
function Wd() {
}
A.e(Wd, Td);
z = Wd.prototype;
z.ro = p;
z.Oa = p;
function Xd(a, b) {
  a.Oa && Bd(a.Oa, b)
}
z.g = function(a) {
  Wd.f.g.call(this, a);
  if(this.isEnabled() && (C(a, "corners") && (this.ro = new Cd, this.ro.g(F(a, "corners"))), C(a, "inside_margin"))) {
    this.Oa = new Ad, this.Oa.g(F(a, "inside_margin"))
  }
};
z.createElement = function(a) {
  return a.ja()
};
z.yh = function(a, b) {
  if(this.ro) {
    return Dd(this.ro, a, b)
  }
  var c = S(b.x, b.y), c = c + U(b.x + b.width, b.y), c = c + U(b.x + b.width, b.y + b.height), c = c + U(b.x, b.y + b.height), c = c + U(b.x, b.y);
  return c + " Z"
};
var Yd = {u:function(a) {
  return a.getTime().toString()
}, d:function(a) {
  return a.getUTCDate().toString()
}, dd:function(a) {
  a = a.getUTCDate().toString();
  return 1 == a.length ? "0" + a : a
}, ddd:function(a, b) {
  return b.qA[a.getUTCDay()]
}, dddd:function(a, b) {
  return b.YA[a.getUTCDay()]
}, M:function(a) {
  return(a.getUTCMonth() + 1).toString()
}, MM:function(a) {
  a = (a.getUTCMonth() + 1).toString();
  return 1 == a.length ? "0" + a : a
}, MMM:function(a, b) {
  return b.pD(a.getUTCMonth())
}, MMMM:function(a, b) {
  return b.fD(a.getUTCMonth())
}, y:function(a) {
  return a.getUTCFullYear().toString().substr(3, 1)
}, yy:function(a) {
  return a.getUTCFullYear().toString().substr(2, 2)
}, yyyy:function(a) {
  return a.getUTCFullYear().toString()
}, h:function(a) {
  a = a.getUTCHours();
  return 0 == a ? "12" : 12 < a ? (a - 12).toString() : a.toString()
}, hh:function(a) {
  a = a.getUTCHours();
  if(0 == a) {
    return"12"
  }
  if(12 < a) {
    return a = (a - 12).toString(), 1 == a.length ? "0" + a : a
  }
  a = a.toString();
  return 1 == a.length ? "0" + a : a
}, H:function(a) {
  return a.getUTCHours().toString()
}, HH:function(a) {
  a = a.getUTCHours().toString();
  return 1 == a.length ? "0" + a : a
}, m:function(a) {
  return a.getUTCMinutes().toString()
}, mm:function(a) {
  a = a.getUTCMinutes().toString();
  return 1 == a.length ? "0" + a : a
}, s:function(a) {
  return a.getUTCSeconds().toString()
}, ss:function(a) {
  a = a.getUTCSeconds().toString();
  return 1 == a.length ? "0" + a : a
}, t:function(a, b) {
  return 12 <= a.getUTCHours() ? b.pA : b.oA
}, tt:function(a, b) {
  return 12 <= a.getUTCHours() ? b.Fz : b.zw
}, z:function(a) {
  a = -a.getTimezoneOffset() / 60;
  return 0 <= a ? "+" + a.toString() : a.toString()
}, zz:function(a) {
  var a = -a.getTimezoneOffset() / 60, b = Math.abs(a).toString(), b = 1 == b.length ? "0" + b : b;
  return 0 > a ? "-" + b : "+" + b
}, zzz:function(a) {
  var b = (-a.getTimezoneOffset() % 60).toString(), a = -a.getTimezoneOffset() / 60, b = Math.abs(a).toString() + ":" + (1 == b.length ? "0" + b : b), b = 4 == b.length ? "0" + b : b;
  return 0 > a ? "-" + b : "+" + b
}};
function Zd(a, b, c) {
  this.On = a;
  this.az = b == h ? 0 : b;
  this.KC = c === h ? "" : c
}
Zd.prototype.On = p;
Zd.prototype.az = p;
Zd.prototype.KC = p;
function $d(a, b) {
  0 < a.az && b.length > a.az && (b = b.substr(0, a.az), a.KC != p && (b += a.KC));
  return b
}
function ae(a, b, c, d, f) {
  Zd.call(this, a, b, c);
  this.RC = d ? d : [];
  this.SC = f ? f : [""]
}
A.e(ae, Zd);
ae.prototype.ta = function(a, b) {
  for(var c = Number(a.Na(this.On)), c = new Date(isNaN(c) ? 0 : c), d = "", f = this.RC.length, g = 0;g < f;g++) {
    d += this.SC[g] + this.RC[g](c, b)
  }
  d += this.SC[f];
  return $d(this, d)
};
ae.prototype.RC = p;
ae.prototype.SC = p;
function be(a, b, c) {
  Zd.call(this, a, b, c)
}
A.e(be, Zd);
z = be.prototype;
z.XK = j;
z.TK = j;
z.xE = 1;
z.Ih = 2;
z.Nn = ",";
z.so = ".";
function ce(a, b, c) {
  var d = Number(Math.round(b * Math.pow(10, a.Ih)) / Math.pow(10, a.Ih)).toString(), f = d.indexOf(".");
  -1 == f ? (b = d, d = "") : (b = d.substr(0, f), d = d.substr(f + 1, d.length - 1));
  if(a.TK && d.length < a.Ih) {
    for(;d.length < a.Ih;) {
      d += "0"
    }
  }
  if(1 < a.xE) {
    for(;b.length < a.xE;) {
      b = "0" + b
    }
  }
  if(a.Nn && 3 < b.length) {
    for(f = b.length;3 < f;f -= 3) {
      b = b.substr(0, f - 3) + a.Nn + b.substr(f - 3, b.length - 1)
    }
  }
  d = "" != d ? b + a.so + d : b;
  return $d(a, c ? a.XK ? "-" + d : "(" + d + ")" : d)
}
function de(a, b, c, d) {
  Zd.call(this, a, b, c);
  this.La = d
}
A.e(de, be);
de.prototype.La = p;
de.prototype.ta = function(a) {
  var b = a.Na(this.On), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  (b = 0 > a) && (a = -a);
  for(var c = this.La.length - 1, d = Math.floor(a / Number(this.La[c].aq));0 == d && 0 < c;) {
    d = Math.floor(a / Number(this.La[--c].aq))
  }
  return ce(this, a / Number(this.La[c].aq), b) + this.La[c].eG
};
function ee(a, b, c) {
  Zd.call(this, a, b, c)
}
A.e(ee, be);
ee.prototype.ta = function(a) {
  var b = a.Na(this.On), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  b = 0 > a;
  return ce(this, b ? -a : a, b)
};
function fe(a, b, c, d) {
  Zd.call(this, a, b, c);
  this.La = d
}
A.e(fe, be);
fe.prototype.La = p;
fe.prototype.ta = function(a) {
  var b = a.Na(this.On), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  b = 0 > a;
  return ce(this, (b ? -a : a) / Number(this.La.scale), b) + this.La.eG
};
function ge(a, b, c) {
  Zd.call(this, a, b, c)
}
A.e(ge, Zd);
ge.prototype.ta = function(a) {
  a = a.Na(this.On);
  a = a == h ? "" : a.toString();
  return $d(this, a)
};
ge.prototype.On = p;
function he(a) {
  this.KK = a
}
he.prototype.ta = u("KK");
he.prototype.KK = p;
function ie(a) {
  this.St = a;
  this.Mf = []
}
z = ie.prototype;
z.ta = function(a, b) {
  this.lG = a;
  this.cC = b;
  if(0 == this.Mf.length) {
    for(var c = this.St, d = c.length, f = 0, g = "", k, l, n, m, o = q, v = 0;v < d;v++) {
      var w = c.charAt(v);
      switch(f) {
        case 0:
          "{" == w ? f = 1 : g += w;
          break;
        case 1:
          "%" == w ? (0 < g.length && this.Mf.push(new he(g)), g = "%", f = 2) : (g += "{" + w, f = 0);
          break;
        case 2:
          "}" == w ? 1 < g.length ? (l = g, g = "", f = 3) : (g = "{%}", f = 0) : g += w;
          break;
        case 3:
          "{" == w ? f = 4 : (je(this, l, {}), g = w, f = 0);
          break;
        case 4:
          "%" == w ? (je(this, l, {}), g = "%", f = 2) : (k = "{" + w, g = w, n = "", m = {}, o = "\\" == w, f = 5);
          break;
        case 5:
          k += w;
          o ? (g += w, o = q) : "\\" == w ? o = j : ":" == w ? (n = g, g = "", f = 6) : "}" == w ? (n = g, m[n] = "", je(this, l, m), k = g = n = "", f = 0) : g += w;
          break;
        case 6:
          k += w, o ? (g += w, o = q) : "\\" == w ? o = j : "," == w ? (f = g, m[n] = f, n = g = "", f = 5) : "}" == w ? (f = g, m[n] = f, je(this, l, m), k = g = n = "", f = 0) : g += w
      }
    }
    switch(f) {
      case 0:
        0 < g.length && this.Mf.push(new he(g));
        break;
      case 1:
        this.Mf.push(new he("{"));
        break;
      case 2:
        this.Mf.push(new he("{" + g));
        break;
      case 3:
        je(this, l, {});
        break;
      case 4:
        je(this, l, {});
        this.Mf.push(new he("{"));
        break;
      case 5:
      ;
      case 6:
        this.Mf.push(new he(k))
    }
  }
  c = "";
  d = this.Mf.length;
  for(k = 0;k < d;k++) {
    c += this.Mf[k].ta(this.lG, this.cC)
  }
  return c
};
z.St = p;
z.lG = p;
z.cC = p;
z.Mf = p;
function je(a, b, c) {
  var d = a.lG.Pa(b), f = [];
  if(c.enabled != h && "true" != c.enabled.toLowerCase()) {
    a.Mf.push(new ge(b))
  }else {
    switch(d) {
      case 1:
        a.Mf.push(new ge(b, c.maxChar, c.maxCharFinalChars));
        break;
      case 2:
        c.scale != h && (f = ke(c.scale));
        le(a, b, c, f);
        break;
      case 3:
        if(c.scale != h && (f = ke(c.scale)), 0 < f.length) {
          le(a, b, c, f)
        }else {
          for(var d = c.dateTimeFormat == h ? a.cC.$y : c.dateTimeFormat, f = d.length, g = [], k = [], l = 0, n = "", m = "", o = 0;o < f;o++) {
            var v = d.charAt(o);
            switch(l) {
              case 0:
                "%" == v ? (k.push(n), n = "", l = 1) : n += v;
                break;
              case 1:
                "%" == v ? k[k.length - 1] += "%" : (n = m = v, l = 2);
                break;
              case 2:
                v == m ? n += v : (l = Yd[n], l == h ? k[k.length - 1] += "%" + n : g.push(l), n = v, l = 0)
            }
          }
          switch(l) {
            case 0:
              k.push(n);
              break;
            case 1:
              k.push("%");
              break;
            case 2:
              l = Yd[n], l == h ? k[k.length - 1] += "%" + n : (g.push(l), k.push(""))
          }
          a.Mf.push(new ae(b, c.maxChar, c.maxCharFinalChars, g, k))
        }
    }
  }
}
function le(a, b, c, d) {
  var f = p, b = f = 0 == d.length ? new ee(b, c.maxChar, c.maxCharFinalChars) : 1 == d.length ? new fe(b, c.maxChar, c.maxCharFinalChars, d[0]) : new de(b, c.maxChar, c.maxCharFinalChars, d), d = c.useNegativeSign, g = c.trailingZeros, k = c.leadingZeros, l = c.numDecimals, n = c.thousandsSeparator, c = c.decimalSeparator;
  d !== h && (b.XK = "true" == d.toLowerCase());
  g !== h && (b.TK = "true" == g.toLowerCase());
  k !== h && (b.xE = Number(k));
  l !== h && (b.Ih = Number(l));
  n !== h && (b.Nn = n);
  c !== h && (b.so = c);
  a.Mf.push(f)
}
function ke(a) {
  for(var b = a.length, c = j, d = 0, f = [], g = "", k = 0;k < b;k++) {
    var l = a.charAt(k);
    "(" != l && (")" == l ? (c ? (g = Number(g), f[d] = {scale:g, eG:"", aq:g}, 0 < d && (f[d].aq *= f[d - 1].aq)) : f[d].eG = g, g = "", d++) : "|" == l ? (c = q, d = 0, g = "") : g += l)
  }
  return f
}
function me() {
  this.ms = {}
}
me.prototype.ms = p;
me.prototype.get = function(a) {
  return ne(this, a) ? this.ms[a] : ""
};
function ne(a, b) {
  return a.ms[b] != h
}
me.prototype.add = function(a, b) {
  this.ms[a] = b
};
function oe(a, b, c) {
  a.ms[b] = ne(a, b) ? a.ms[b] + c : c
}
function pe(a) {
  return vb(a)
}
function qe(a) {
  return a && (1 == a.indexOf("%") || 0 == a.indexOf("%"))
}
function re(a, b) {
  return 0 == a.indexOf("%DataPlot") || b.pl(a)
}
function se(a, b) {
  return b && (0 == a.indexOf("%Threshold") || b.pr(a))
}
function te(a) {
  return 0 == a.indexOf("%Category")
}
function ue(a) {
  return a.slice(0, 1) + a.slice(2, a.length)
}
;function dd(a) {
  this.F = a;
  this.Sc = Cc(this.F, "text");
  this.Sc.setAttribute("visibility", "hidden");
  this.Sc.setAttribute("x", "0");
  this.Sc.setAttribute("y", "0");
  this.HA = this.F.Mk.ownerDocument.createTextNode("");
  this.Sc.appendChild(this.HA);
  a.Jr.appendChild(this.Sc)
}
dd.prototype.F = p;
dd.prototype.Sc = p;
dd.prototype.HA = p;
dd.prototype.measureText = function(a, b) {
  ve(b, this.Sc);
  this.HA.nodeValue = a;
  var c = this.Sc.getBBox();
  this.HA.nodeValue = "";
  var d = this.Sc;
  d.removeAttribute("font-family");
  d.removeAttribute("font-size");
  d.removeAttribute("font-weight");
  d.removeAttribute("font-style");
  d.removeAttribute("font-decoration");
  d.removeAttribute("fill");
  d.removeAttribute("stroke");
  return new P(c.x, c.y, c.width, c.height)
};
function we(a, b, c) {
  W.call(this, a);
  c && (this.Cc = c, this.ia(c));
  b && (this.PK = b, this.G.appendChild(b))
}
A.e(we, W);
we.prototype.PK = p;
we.prototype.eu = u("PK");
we.prototype.Cc = p;
we.prototype.Mx = u("Cc");
function xe(a, b) {
  W.call(this, a);
  this.kf = b;
  this.ia(b)
}
A.e(xe, W);
xe.prototype.eu = function() {
  return this.kf.eu()
};
xe.prototype.Mx = function() {
  return this.kf.Mx()
};
function ye() {
  ze(this)
}
z = ye.prototype;
z.uH = function() {
  return new Zc
};
z.lC = j;
z.IC = p;
z.ke = p;
z.Mp = t("ke");
z.BB = p;
z.tE = p;
z.rG = p;
z.Xa = p;
z.Tp = p;
z.mF = p;
function ze(a) {
  a.mF = q;
  a.Tp = 0;
  a.Xa = zb(p, 1);
  a.rG = q;
  a.tE = q;
  a.BB = q;
  a.ke = 10;
  a.IC = "Arial";
  a.Bb = a.uH();
  Ae(a, 0);
  a.Y = p
}
z.Qh = p;
z.k = p;
z.n = u("k");
z.nf = p;
z.Ig = function() {
  var a = this.nf.Ca();
  a.x = 1;
  a.y = 1;
  a.width -= 2;
  a.height -= 2;
  return a
};
z.Bb = p;
function Ae(a, b) {
  a.Bb.bc = -b;
  a.Bb.Ae()
}
z.Ma = j;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.GA = p;
z.Y = p;
z.md = u("Y");
z.g = function(a) {
  if(Tb(a)) {
    C(a, "background") && (this.Y = new Wd, this.Y.g(F(a, "background")));
    Ae(this, C(a, "rotation") ? M(a, "rotation") : 0);
    var b = C(a, "text_align") ? "text_align" : "multi_line_align";
    if(C(a, b)) {
      switch(Cb(F(a, b))) {
        case "left":
          this.Tp = 0;
          break;
        case "center":
          this.Tp = 2;
          break;
        case "right":
          this.Tp = 1
      }
    }
    if(C(a, "font") && (a = F(a, "font"), C(a, "family") && (this.IC = Cb(F(a, "family"))), C(a, "size") && (this.ke = Ob(F(a, "size"))), C(a, "bold") && (this.BB = Rb(F(a, "bold"))), C(a, "italic") && (this.tE = Rb(F(a, "italic"))), C(a, "underline") && (this.rG = Rb(F(a, "underline"))), C(a, "color") && (this.Xa = zb(F(a, "color"), h)), C(a, "render_as_html"))) {
      this.mF = K(a, "render_as_html")
    }
  }else {
    this.Ma = q
  }
};
z.uc = function(a, b) {
  this.mF && (b = b.replace(/<.*?>/gi, ""));
  for(var c = b, d = [], f = c.length, g = "", k = 0;k < f;k++) {
    var l = c.charCodeAt(k);
    13 == l || 10 == l ? (d.push(Hb(g)), g = "") : k == f - 1 ? (g += c.charAt(k), d.push(Hb(g)), g = "") : g += c.charAt(k)
  }
  c = this.GA = d;
  d = new P(0, 0, 0, 0);
  for(f = 0;f < c.length;f++) {
    g = a.OK.measureText(c[f], this), d.width = Math.max(d.width, g.width), d.height += g.height
  }
  this.Qh = d;
  this.Qh.x += 1;
  this.Qh.y += 1;
  c = this.Qh.Ca();
  this.Y && this.Y.isEnabled() && this.Y.Oa && (c = this.Qh, d = this.Qh.Ca(), c.x += this.Y.Oa.tb() + 1, c.y += this.Y.Oa.rb() + 1, d.x = c.x, d.y = c.y, d.width += this.Y.Oa.tb() + this.Y.Oa.Ja() + 4, d.height += this.Y.Oa.rb() + this.Y.Oa.ra() + 4, c = d);
  this.nf = c;
  c = this.Bb;
  d = this.nf.Ca();
  !isNaN(c.bc) && 0 != c.bc && (f = d.width, g = d.height, d.width = f * c.wf + g * c.xf, d.height = f * c.xf + g * c.wf);
  this.k = d
};
function ve(a, b, c) {
  b.setAttribute("font-family", a.IC);
  b.setAttribute("font-size", a.ke);
  a.BB && b.setAttribute("font-weight", "bold");
  a.tE && b.setAttribute("font-style", "italic");
  a.rG && b.setAttribute("font-decoration", "underline");
  b.setAttribute("fill", ub(a.Xa.Ba(c)));
  b.setAttribute("stroke", "none")
}
z.ic = function(a, b, c) {
  var d = Cc(a, "text"), f;
  this.Y && this.Y.isEnabled() && (f = this.Y.p(a), this.Y.qa(f, this.Ig(), b));
  f || (f = new Md(a, a.ja(), a.ja()));
  f = new we(a, d, f);
  this.lC && d.setAttribute("pointer-events", "none");
  (c || c == p || c == h) && Be(this, a, d, b);
  this.aI(f);
  return new xe(a, f)
};
z.aI = function(a) {
  a.Vl(this.Bb.bc);
  var b = new O, c = this.Bb, d = this.nf, f = 0, g = 0, k = 0 <= c.sin, l = 0 <= c.cos;
  switch(k && l ? 0 : k && !l ? 1 : !l ? 2 : 3) {
    case 0:
      f = d.height * c.sin;
      break;
    case 1:
      f = d.width * c.wf + d.height * c.xf;
      g = d.height * c.wf;
      break;
    case 2:
      f = d.width * c.wf;
      g = d.width * c.xf + d.height * c.wf;
      break;
    case 3:
      f = 0, g = d.width * c.xf
  }
  b.x += f;
  b.y += g;
  a.qb(b.x, b.y)
};
z.update = function(a, b, c, d, f) {
  Be(this, a, b.eu(), c, d, f);
  this.cw(b.Mx(), this.Ig(), c)
};
function Be(a, b, c, d, f, g) {
  ve(a, c, d);
  c.setAttribute("x", a.Qh.x);
  c.setAttribute("y", a.Qh.y);
  if(g == h || g == p) {
    g = -1
  }
  if(g) {
    switch(g) {
      case 2:
        c.setAttribute("text-anchor", "middle");
        break;
      case 1:
        c.setAttribute("text-anchor", "end")
    }
  }
  if(!f) {
    for(;0 < c.childNodes.length;) {
      c.removeChild(c.childNodes[0])
    }
    for(d = 0;d < a.GA.length;d++) {
      f = Cc(b, "tspan");
      f.appendChild(b.Mk.ownerDocument.createTextNode(a.GA[d]));
      switch(g) {
        case 2:
          f.setAttribute("x", a.Qh.width / 2);
          break;
        case 1:
          f.setAttribute("x", a.Qh.width);
          break;
        default:
          f.setAttribute("x", a.Qh.x)
      }
      f.setAttribute("dy", "1em");
      c.appendChild(f)
    }
  }
}
z.cw = function(a, b, c) {
  this.Y && this.Y.isEnabled() && this.Y.qa(a, b, c)
};
function Ce() {
  ye.apply(this)
}
A.e(Ce, ye);
z = Ce.prototype;
z.D = p;
z.fb = u("D");
z.yc = p;
z.Bh = u("yc");
z.Ia = 5;
z.ga = u("Ia");
z.za = 1;
z.g = function(a) {
  Ce.f.g.call(this, a);
  var b = C(a, "text") ? "text" : "format";
  C(a, b) && (this.yc = new ie(J(a, b)));
  if(C(a, "align")) {
    switch(N(a, "align")) {
      case "near":
      ;
      case "left":
      ;
      case "top":
        this.za = 0;
        break;
      case "far":
      ;
      case "right":
      ;
      case "bottom":
        this.za = 2;
        break;
      case "center":
        this.za = 1
    }
  }
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
z.ic = function(a) {
  return this.D = Ce.f.ic.call(this, a)
};
z.Wa = s();
function De(a, b) {
  ye.apply(this);
  this.HJ = a;
  this.iI = b
}
A.e(De, Ce);
z = De.prototype;
z.Pf = p;
z.HJ = p;
z.iI = p;
z.g = function(a) {
  De.f.g.call(this, a);
  this.Ma && C(a, "actions") && (this.Pf = new Ee(this.HJ, this.iI), this.Pf.g(F(a, "actions")))
};
z.ic = function(a) {
  this.Pf && (this.lC = q);
  De.f.ic.call(this, a);
  this.Pf && (this.D.G.setAttribute("cursor", "pointer"), this.nu(this.D));
  return this.D
};
z.nu = function(a) {
  A.I.nb(a.G, ka.dB, this.Il, q, this)
};
z.Il = function() {
  this.Pf && this.Pf.execute()
};
function Fe(a) {
  ha.call(this, a)
}
A.e(Fe, ha);
function Ge(a, b) {
  ha.call(this, a);
  this.QA = b
}
A.e(Ge, ha);
Ge.prototype.QA = p;
function He(a, b, c, d) {
  ha.call(this, a);
  this.j = b;
  this.PJ = c;
  this.QJ = d
}
A.e(He, ha);
He.prototype.Jb = u("type");
He.prototype.j = p;
He.prototype.PJ = p;
He.prototype.QJ = p;
function Ie(a, b) {
  ha.call(this, a);
  this.ba = b
}
A.e(Ie, ha);
Ie.prototype.Jb = u("type");
Ie.prototype.ba = p;
Ie.prototype.Fe = function() {
  return this.ba.length
};
Ie.prototype.Ga = function(a) {
  return this.ba[a]
};
function Je(a, b, c) {
  ha.call(this, a);
  this.errorCode = b;
  this.errorMessage = c
}
A.e(Je, ha);
Je.prototype.errorCode = p;
Je.prototype.errorMessage = p;
Je.prototype.toString = function() {
  return"Error code: " + this.errorCode + " error message: " + this.errorMessage
};
Je.prototype.toString = function() {
  return"Error message: " + this.errorMessage
};
function Ke(a) {
  e(new Le("anychartError", 22, "Feature is not supported.", a))
}
function Le(a, b, c, d) {
  Je.call(this, a, b, c);
  this.featureName = d
}
A.e(Le, Je);
Le.prototype.featureName = p;
Le.prototype.toString = function() {
  return Le.f.toString.call(this) + " Feature: " + this.featureName + "."
};
function Me() {
}
z = Me.prototype;
z.j = p;
z.G = p;
z.$a = p;
z.p = function(a, b, c) {
  this.j = a;
  this.$a = b;
  this.G = c ? c.call(b) : this.createElement()
};
z.update = function(a, b) {
  a && (this.vf(a), this.ud(a));
  b === j && this.Ta()
};
z.vf = function() {
  A.xa()
};
z.Ta = function() {
  A.xa()
};
z.ud = s();
function Ne() {
}
A.e(Ne, Me);
Ne.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.hD(), f = this.n(), c = c + Ld(this.rD(a), b, f, d), c = c + md();
  this.G.setAttribute("style", c)
};
Ne.prototype.n = function() {
  return this.j.n()
};
Ne.prototype.hD = function() {
  return this.j.Ba().Ba()
};
function Oe() {
}
A.e(Oe, Me);
Oe.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.hD(), f = this.n();
  this.Ie(a) ? (c += Jd(this.Xq(a), b, f, d), c += nd()) : c += od();
  this.G.setAttribute("style", c)
};
Oe.prototype.n = function() {
  return this.j.n()
};
Oe.prototype.hD = function() {
  return this.j.Ba().Ba()
};
Oe.prototype.Ie = x(j);
function Pe() {
}
A.e(Pe, Me);
z = Pe.prototype;
z.Yh = p;
z.createElement = function() {
  return this.$a.createElement()
};
z.Ta = function() {
  this.$a.Ta(this.G)
};
z.oh = function(a) {
  var b = this.j.r();
  this.Yh && (Ec(b.cr, this.Yh), this.Yh = "");
  this.vf(a)
};
z.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.ca().n();
  this.Ie(a) ? (c += Jd(this.mc(a), b, f, d, j), this.Yh = c += nd()) : c = od();
  this.G.setAttribute("style", c)
};
z.ud = s();
function Qe() {
}
Qe.prototype.j = p;
Qe.prototype.p = function(a) {
  this.j = a;
  this.Yc();
  this.ff()
};
function Re(a, b) {
  this.K = a;
  b != h && (this.XF = b)
}
z = Re.prototype;
z.K = p;
z.ka = u("K");
z.XF = 0;
z.g = function(a) {
  if(!a) {
    return p
  }
  a = Zb(a);
  if(C(a, "color")) {
    var b = F(a, "color"), c;
    for(c in a) {
      C(a[c], "color") && H(a[c], "color", N(a[c], "color").split("%color").join(b))
    }
  }
  return a
};
z.copy = function(a) {
  a || e(Error("Unknown target"));
  a.K = this.K;
  a.XF = this.XF;
  return a
};
function Se(a, b) {
  Re.call(this, a, b)
}
A.e(Se, Re);
Se.prototype.Ve = p;
Se.prototype.ll = u("Ve");
Se.prototype.qp = function() {
  return Boolean(this.Ve && this.Ve.isEnabled())
};
Se.prototype.g = function(a) {
  a = Se.f.g.call(this, a);
  if(!a) {
    return p
  }
  Ub(a, "effects") && (this.Ve = new Rc, this.Ve.g(F(a, "effects")));
  return a
};
function $() {
  var a = this.cc();
  this.wa = new a(this, 0);
  this.dc = new a(this, 1);
  this.kc = new a(this, 2);
  this.Yb = new a(this, 3);
  this.gc = new a(this, 4);
  this.nc = new a(this, 5)
}
z = $.prototype;
z.wa = p;
z.dh = t("wa");
z.dc = p;
z.ah = t("dc");
z.kc = p;
z.Yb = p;
z.gc = p;
z.nc = p;
z.g = function(a) {
  if(a) {
    if(a = Zb(a), C(a, "states")) {
      var b = F(a, "states");
      this.wa.g(this.Fh(a, F(b, "normal")));
      this.dc.g(this.Fh(a, F(b, "hover")));
      this.kc.g(this.Fh(a, F(b, "pushed")));
      this.Yb.g(this.Fh(a, F(b, "selected_normal")));
      this.gc.g(this.Fh(a, F(b, "selected_hover")));
      this.nc.g(this.Fh(a, F(b, "missing")))
    }else {
      this.wa.g(a), this.dc.g(a), this.kc.g(a), this.Yb.g(a), this.gc.g(a), this.nc.g(a)
    }
  }
};
z.Fh = function(a, b) {
  return!b ? a : $b(a, b, p, "states")
};
z.p = function(a) {
  var b = this.lc(a);
  b.p(a);
  return b
};
z.copy = function(a) {
  a || (a = new $);
  a.dh(this.wa.copy());
  a.ah(this.dc.copy());
  var b = this.kc.copy();
  a.kc = b;
  b = this.Yb.copy();
  a.Yb = b;
  b = this.gc.copy();
  a.gc = b;
  b = this.nc.copy();
  a.nc = b;
  return a
};
function Te(a) {
  this.Ki = a;
  this.Ti = {}
}
Te.prototype.Ki = p;
Te.prototype.Ti = p;
Te.prototype.ka = function(a, b) {
  return!this.Ti[a] || !this.Ti[a][b] ? p : this.Ti[a][b]
};
function Ue(a, b, c, d, f) {
  var g = I(a.Ki, b), k = [], l;
  c && k.push(c);
  d && (d = Cb(d), l = fc(g, d));
  Ve(k, l, g);
  return We(a, k, b, f)
}
function Xe(a, b, c) {
  if(!b || a.ka(b, c)) {
    return p
  }
  var b = Cb(b), c = Cb(c), d = I(a.Ki, b), c = fc(d, c), f = [];
  Ve(f, c, d);
  return We(a, f, b)
}
function Ve(a, b, c) {
  if(b) {
    for(a.push(Zb(b));"anychart_default" != J(b, "name");) {
      b = !C(b, "parent") || !fc(c, J(b, "parent")) ? fc(c, "anychart_default") : fc(c, F(b, "parent")), a.push(Zb(b))
    }
  }else {
    a.push(fc(c, "anychart_default"))
  }
}
function We(a, b, c, d) {
  var f = b.length;
  if(d == h || d == p) {
    d = j
  }
  if(d) {
    for(d = 0;d < b.length;d++) {
      var g = b, k = d, l;
      l = a;
      var n = b[d], m = c ? c : J(n, "#name#");
      if(m && -1 != ef.indexOf(m)) {
        l = n
      }else {
        var o = {};
        m && (o["#name#"] = m);
        o.states = {"#name#":"states", "#children#":[]};
        ff(o.states, "normal");
        ff(o.states, "hover");
        ff(o.states, "pushed");
        ff(o.states, "selected_normal");
        ff(o.states, "selected_hover");
        ff(o.states, "missing");
        ac(n, o, o);
        o.states.normal = l.Fh(n, "normal");
        o.states.hover = l.Fh(n, "hover");
        o.states.pushed = l.Fh(n, "pushed");
        o.states.selected_normal = l.Fh(n, "selected_normal");
        o.states.selected_hover = l.Fh(n, "selected_hover");
        o.states.missing = l.Fh(n, "missing");
        l = o
      }
      g[k] = l
    }
  }
  a = b[f - 1];
  a.inherits = C(a, "name") ? Cb(F(a, "name")) : "";
  for(d = f - 1;0 < d;d--) {
    a = $b(a, b[d - 1]), C(b[d - 1], "name") && (a.inherits += "," + Cb(F(b[d - 1], "name")))
  }
  return a
}
Te.prototype.Fh = function(a, b) {
  a.states || (a.states = {});
  a.states[b] || (a.states[b] = {});
  return $b(a, a.states[b], b, "states")
};
function ff(a, b) {
  a[b] = {"#name#":b, "#children#":[]}
}
var ef = "animation_style,animation,line_axis_marker_style,range_axis_marker_style,custom_label_style,trendline_style,color_range_style,tooltip_style".split(",");
Te.prototype.clear = function() {
  this.Ki = {};
  this.Ti = {}
};
function gf() {
}
z = gf.prototype;
z.ah = s();
z.dh = s();
z.Od = s();
z.Lj = s();
z.Ck = s();
z.Ok = s();
z.Pk = s();
z.nk = s();
z.wK = s();
z.Mj = s();
z.Nj = s();
z.Dk = s();
z.Qk = s();
z.Jk = s();
z.ok = s();
z.mk = function() {
  e(new Je("anychartError", 1, "This method can be used in axes plot only."))
};
function hf() {
  this.Ge();
  this.gF = this.QD = q
}
A.e(hf, gf);
z = hf.prototype;
z.N = p;
z.dA = t("N");
z.qm = p;
z.xK = t("qm");
z.wp = p;
z.Xr = t("wp");
z.Tj = p;
z.gG = x(j);
z.vF = function(a) {
  this.Tj = Zb(a)
};
z.Y = p;
z.Cc = p;
z.vB = p;
z.Ig = u("k");
z.k = p;
z.n = u("k");
z.Rc = t("k");
z.Vt = u("k");
z.Sf = p;
z.yK = t("Sf");
z.Nk = p;
z.ey = u("Nk");
z.vk = p;
z.Wx = u("vk");
z.Zy = p;
z.zB = p;
z.vt = p;
z.ww = p;
z.g = function(a, b) {
  if(a == p) {
    return p
  }
  C(a, "chart_settings") && this.gC(F(a, "chart_settings"), b);
  C(a, "data_plot_settings") && this.Dq(F(a, "data_plot_settings"));
  this.vx(a, b);
  this.zk(a);
  return a
};
z.gC = function(a) {
  C(a, "data_plot_background") && (this.Y = new Wd, this.Y.g(F(a, "data_plot_background")))
};
z.Dq = s();
function jf(a, b) {
  if(C(b, "interactivity")) {
    var c = F(b, "interactivity");
    C(c, "allow_multiple_select") && (a.im = K(c, "allow_multiple_select"));
    C(c, "select_rectangle") && (c = F(c, "select_rectangle"), C(c, "enabled") && (a.yw = K(c, "enabled")))
  }
}
z.vx = function(a) {
  if(C(a, "data") && (a = F(a, "data"), C(a, "attributes"))) {
    this.jd = {};
    for(var a = I(F(a, "attributes"), "attribute"), b = a.length, c = 0;c < b;c++) {
      var d = a[c];
      if(C(d, "name")) {
        var f;
        C(d, "custom_attribute_value") ? f = F(d, "custom_attribute_value") : C(d, "value") && (f = F(d, "value"));
        f && H(this.jd, "%" + F(d, "name"), f)
      }
    }
  }
};
z.QD = p;
z.F = p;
z.r = u("F");
z.D = p;
z.fb = u("D");
z.sl = function(a) {
  this.F = a;
  this.vB = new W(this.F);
  this.zB = new W(this.F);
  this.vt = new W(this.F);
  this.ww = new W(this.F);
  this.vk = new W(this.F);
  this.Zy = new W(this.F)
};
z.p = function(a, b, c) {
  this.D = new W(this.F);
  this.Nk = b;
  this.k = a;
  this.D.Xd(this.k.x);
  this.D.gd(this.k.y);
  c && this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(this.F), this.Cc.qb(0.5, 0.5), this.vB.ia(this.Cc));
  this.D.ia(this.vB);
  this.sy();
  this.D.ia(this.zB);
  this.D.ia(this.vt);
  this.D.ia(this.ww);
  this.D.ia(this.vk);
  this.D.ia(this.Zy);
  this.Nk && (this.Nk.Xd(this.k.x), this.Nk.gd(this.k.y));
  this.QD = j;
  return this.D
};
z.gF = p;
z.zp = s();
z.qa = function() {
  this.Cc && this.Y.qa(this.Cc, this.Ig())
};
z.nm = function(a) {
  this.k = a.Ca()
};
z.Jq = function() {
  this.Cc && this.Y.Wa(this.Cc, this.Ig())
};
z.b = p;
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%DataPlotPointCount", 0);
  this.b.add("%DataPlotYSum", 0);
  this.b.add("%DataPlotYMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotYMin", Number.MIN_VALUE);
  this.b.add("%DataPlotYAverage", 0);
  this.b.add("%DataPlotBubbleSizeSum", 0);
  this.b.add("%DataPlotBubbleMaxSize", -Number.MAX_VALUE);
  this.b.add("%DataPlotBubbleMinSize", Number.MAX_VALUE);
  this.b.add("%DataPlotBubbleSizeAverage", 0);
  this.b.add("%DataPlotMaxYValuePointName", "");
  this.b.add("%DataPlotMinYValuePointName", "");
  this.b.add("%DataPlotMaxYValuePointSeriesName", "");
  this.b.add("%DataPlotMinYValuePointSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeriesName", "");
  this.b.add("%DataPlotMinYSumSeriesName", "")
};
z.Na = function(a) {
  if(this.pl(a)) {
    return this.jd[a]
  }
  ne(this.b, "%DataPlotBubbleSizeAverage") || this.b.add("%DataPlotBubbleSizeAverage", this.b.get("%DataPlotBubbleSizeSum") / this.b.get("%DataPlotBubblePointCount"));
  return this.b.get(a)
};
z.Pa = function(a) {
  if(this.pl(a)) {
    return 1
  }
  switch(a) {
    case "%DataPlotPointCount":
    ;
    case "%DataPlotBubblePointCount":
    ;
    case "%DataPlotYSum":
    ;
    case "%DataPlotYMax":
    ;
    case "%DataPlotYMin":
    ;
    case "%DataPlotYAverage":
    ;
    case "%DataPlotBubbleSizeSum":
    ;
    case "%DataPlotBubbleMaxSize":
    ;
    case "%DataPlotBubbleMinSize":
    ;
    case "%DataPlotBubbleSizeAverage":
      return 2;
    case "%DataPlotMaxYValuePointName":
    ;
    case "%DataPlotMinYValuePointName":
    ;
    case "%DataPlotMaxYValuePointSeriesName":
    ;
    case "%DataPlotMinYValuePointSeriesName":
    ;
    case "%DataPlotMaxYSumSeriesName":
    ;
    case "%DataPlotMinYSumSeriesName":
      return 1
  }
};
z.jd = p;
z.pl = function(a) {
  return Boolean(this.jd && this.jd[a])
};
z.Ik = function(a, b) {
  this.jd || (this.jd = {});
  this.jd[a] = b;
  if(this.Tj && C(this.Tj, "data")) {
    var c = F(this.Tj, "data"), d;
    C(c, "attributes") ? d = F(c, "attributes") : (d = {}, H(c, "attributes", d));
    C(d, "attribute") ? c = I(d, "attribute") : (c = [], H(d, "attribute", c));
    (d = fc(c, a)) || (d = {});
    A.isArray(d) ? d.push(kf(a, b)) : d = kf(a, b);
    c.push(d)
  }
};
function kf(a, b) {
  return{"#name#":"attribute", "#children#":[b], name:a, value:b}
}
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  if(this.jd) {
    for(var b in this.jd) {
      a[b] = this.jd[b]
    }
  }
  a.YSum = this.b.get("%DataPlotYSum");
  a.YMax = this.b.get("%DataPlotYMax");
  a.YMin = this.b.get("%DataPlotYMin");
  a.YAverage = this.b.get("%DataPlotYSum") / this.b.get("%DataPlotYBasedPointsCount");
  a.BubbleSizeSum = this.b.get("%DataPlotBubbleSizeSum");
  a.BubbleMaxSize = this.b.get("%DataPlotBubbleSizeMax");
  a.BubbleMinSize = this.b.get("%DataPlotBubbleSizeMin");
  a.BubbleSizeAverage = this.b.get("%DataPlotBubbleSizeSum") / this.b.get("%DataPlotBubblePointCount");
  a.MaxYValuePointName = this.b.get("%DataPlotMaxYValuePointName");
  a.MaxYValuePointSeriesName = this.b.get("%DataPlotMaxYValuePointSeriesName");
  a.MinYValuePointName = this.b.get("%DataPlotMinYValuePointName");
  a.MinYValuePointSeriesName = this.b.get("%DataPlotMinYValuePointSeriesName");
  a.MaxYSumSeriesName = this.b.get("%DataPlotMaxYSumSeriesName");
  a.MinYSumSeriesName = this.b.get("%DataPlotMinYSumSeriesName");
  a.PointCount = this.b.get("%DataPlotPointCount");
  return a
};
function lf(a, b, c) {
  var d, f;
  C(c, "charts") ? (c = F(c, "charts"), C(c, "chart") && (d = F(c, "chart"), f = q)) : C(c, "gauges") && (c = F(c, "gauges"), C(c, "gauge") && (d = F(c, "gauge"), f = j));
  if(d) {
    var g, k, l;
    if(C(d, "template")) {
      var c = a.Rp, n = N(d, "template");
      k = [];
      l = {};
      for(var m = c.Sp[n] ? Zb(c.Sp[n]) : p;m != p;) {
        k.push(m);
        l[n] = j;
        n = Cb(F(m, "parent"));
        if(l[n] != h) {
          break
        }
        m = c.Sp[n] ? Zb(c.Sp[n]) : p
      }
      c = k
    }else {
      c = p
    }
    if(!f && c) {
      k = 0;
      for(l = c.length;k < l;k++) {
        if(C(c[k], "chart")) {
          k = F(c[k], "chart");
          C(k, "plot_type") && (g = N(k, "plot_type"));
          break
        }
      }
    }
    C(d, "plot_type") && (g = N(d, "plot_type"));
    var o;
    if(f) {
      o = mf
    }else {
      switch(g) {
        case "map":
          Ke("Plot type Map");
          break;
        case "table":
          Ke("Plot type Table");
          break;
        case "polar":
          Ke("Plot type Polar");
          break;
        case "radar":
          Ke("Plot type Radar");
          break;
        case "treemap":
          o = nf;
          break;
        case "pie":
        ;
        case "doughnut":
          o = of;
          break;
        case "funnel":
          o = pf;
          break;
        default:
          o = qf
      }
    }
    g || (g = "categorizedvertical");
    if(o) {
      o = new o;
      o.Xr(g);
      o.xK(a);
      o.dA(a.dj());
      a = o.br();
      g = Zb(rf);
      f = a.zh();
      g[a.Sq()] = F(g, "chart");
      g = a.$f(g, f);
      if(c) {
        for(f = c.length - 1;0 <= f;f--) {
          g = a.$f(g, c[f])
        }
      }
      d = a.EE(g, d);
      o.gG() && o.vF(d);
      b.Tl(o);
      b.g(d)
    }
  }
}
;var rf = {chart:{chart_settings:{chart_background:{border:{thickness:"2", color:"#2466B1"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"15"}, corners:{type:"Rounded", all:"10"}, effects:{drop_shadow:{enabled:"True", distance:"2", opacity:"0.3"}, enabled:"False"}}, title:{text:{value:"Chart Title"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", 
color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, align_by:"DataPlot"}, subtitle:{text:{value:"Chart Sub-Title"}, background:{border:{gradient:{key:[{position:"0", 
color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.1", angle:"45", blur_x:"1", blur_y:"1", color:"Black", distance:"1", strength:"1"}, enabled:"True"}, enabled:"True"}, font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, enabled:"False", 
align_by:"dataplot"}, footer:{text:{value:"\u00a9 2008 AnyChart.Com Flash Charting Solutions"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.1", angle:"45", blur_x:"1", blur_y:"1", color:"#000000", 
distance:"1", strength:"1"}, enabled:"True"}, enabled:"True"}, font:{family:"Tahoma", size:"11", color:"#232323"}, enabled:"false", position:"bottom", align:"far"}}}, defaults:{legend:{font:{family:"Verdana", size:"10", bold:"false", color:"rgb(35,35,35)"}, format:{value:"{%Icon} {%Name}"}, title:{text:{value:"Legend Title"}, font:{family:"Verdana", size:"10", bold:"true", color:"rgb(35,35,35)"}, background:{inside_margin:{all:"6"}, enabled:"false"}, padding:"2"}, title_separator:{gradient:{key:[{position:"0", 
color:"#333333", opacity:"0"}, {position:"0.5", color:"#333333", opacity:"1"}, {position:"1", color:"#333333", opacity:"0"}]}, enabled:"true", type:"Gradient", padding:"7"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, 
inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, rows_separator:{enabled:"true", padding:"3", type:"Solid", color:"#333333", opacity:"0.05"}, columns_separator:{enabled:"true", padding:"3", type:"Solid", color:"#333333", opacity:"0.05"}, scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"0"}, {color:"White", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", color:"#707173", 
opacity:"1"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, 
{color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, thumb:{background:{corners:{type:"Rounded", all:"0", right_top:"3", right_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, 
{color:"#EDEDED"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"0"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], angle:"0"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"0"}, 
type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"0"}, type:"Gradient"}}}}}, size:"16", enabled:"true"}, position:"right", align:"near", elements_align:"Left", columns_padding:"10", rows_padding:"10"}, panel:{margin:{all:"5"}, title:{font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, align:"center"}, 
background:{border:{thickness:"2", color:"#2466B1"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, corners:{type:"Rounded", all:"10"}}}, dashboard:{margin:{all:"5"}, title:{font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, text:{value:"Dashboard Title"}, align:"center"}}}};
function sf() {
}
z = sf.prototype;
z.zh = x(p);
z.$f = function(a, b) {
  var c = {"#name#":"template"};
  ac(a, b, c);
  var d = this.Sq(), f = F(a, d), g = F(b, d);
  c[d] = {"#name#":d, "#children#":[]};
  this.qn(f, g, c[d]);
  d = $b(F(a, "defaults"), F(b, "defaults"));
  d != p && (c.defaults = d);
  return c
};
z.EE = function(a, b) {
  var c = this.Sq(), d = {"#name#":c};
  this.qn(F(a, c), b, d);
  this.Rs(d, F(a, "defaults"));
  return d
};
z.qn = function(a, b, c) {
  ac(a, b, c);
  var d = F(a, "chart_settings"), f = F(b, "chart_settings"), g = $b(d, f);
  g && (c.chart_settings = g);
  a = cc(F(a, "styles"), F(b, "styles"));
  if(a != p) {
    c.styles = a;
    for(var k in c.styles) {
      C(c.styles[k], "name") && (c.styles[k].name = Cb(F(c.styles[k], "name"))), C(c.styles[k], "parent") && (c.styles[k].name = Cb(F(c.styles[k], "parent")))
    }
  }
  if(d && C(d, "controls")) {
    var l = F(d, "controls")
  }
  if(f && C(d, "controls")) {
    var n = F(f, "controls")
  }
  (c = cc(l, n)) && g && H(g, "controls", c)
};
z.Rs = function(a, b) {
  if(a && b) {
    var c = F(b, "legend"), d = F(a, "chart_settings");
    if(c && d && (C(d, "legend") && H(d, "legend", $b(c, F(d, "legend"), "legend")), C(d, "controls"))) {
      for(var d = F(d, "controls"), f = I(d, "legend"), g = f.length, k = 0;k < g;k++) {
        f[k] = $b(c, f[k], "legend")
      }
      H(d, "legend", f)
    }
  }
};
function tf() {
  this.Sp = {}
}
tf.prototype.Sp = p;
function uf(a, b) {
  for(var c = 0, d = b.length;c < d;c++) {
    var f = b[c], g = Cb(F(f, "name"));
    "" != g && (a.Sp[g] = f)
  }
}
tf.prototype.clear = function() {
  this.Sp = {}
};
var vf = {gauge:{styles:{label_style:{format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], 
angle:"90"}, type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, name:"anychart_default"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True", italic:"False", underline:"False"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, 
{position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default", enabled:"true"}}, 
circular_template:{styles:{custom_label_style:{label:{format:{value:"Custom label"}, align:"Outside", enabled:"true", padding:"10"}, tickmark:{fill:{color:"%Color"}, border:{color:"Red"}, enabled:"true", shape:"Star5", width:"%AxisSize*.10", length:"%AxisSize*.10", align:"Outside"}, name:"anychart_default"}, trendline_style:{line:{color:"%Color"}, name:"anychart_default", size:"%AxisSize*.1"}, color_range_style:{fill:{color:"%Color", opacity:"0.8"}, name:"anychart_default", start_size:"5", end_size:"5", 
align:"Outside", padding:"0"}, knob_pointer_style:{knob_background:{border:{enabled:"true", color:"#494949", opacity:"0.5"}, fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], angle:"45"}, enabled:"true", type:"Gradient"}, effects:{drop_shadow:{enabled:"true", distance:"1", opacity:"0.3"}, enabled:"true"}, radius:"80", gear_height:"5"}, cap:{inner_stroke:{enabled:"false"}, outer_stroke:{enabled:"false"}, background:{fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], 
angle:"45"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", color:"#494949", opacity:"0.5"}, enabled:"true"}, enabled:"true", radius:"40"}, needle:{fill:{color:"#F0673B", type:"Solid"}, border:{enabled:"true", type:"Solid", color:"#494949", opacity:"0.3"}, radius:"90", base_radius:"0", thickness:"15", point_thickness:"10", point_radius:"4"}, name:"anychart_default"}, needle_pointer_style:{fill:{color:"%Color", type:"Solid"}, border:{enabled:"true", type:"Solid", color:"#494949", opacity:"0.3"}, 
states:{hover:{border:{color:"#999999", opacity:"0.7"}, fill:{color:"LightColor(%Color)"}}, pushed:{border:{color:"#999999", opacity:"0.7"}, fill:{color:"LightColor(%Color)"}, effects:{inner_shadow:{enabled:"true", distance:"2", blur_x:"2", blur_y:"2", opacity:"0.4"}, enabled:"true"}}}, cap:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#777777"}], type:"Radial", focal_point:"-0.5", angle:"45"}, type:"Gradient"}, border:{enabled:"true", color:"#575757"}, enabled:"true"}, enabled:"true", 
radius:"15"}, name:"anychart_default", radius:"100", base_radius:"0", point_thickness:"0", thickness:"12", point_radius:"0"}, bar_pointer_style:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, effects:{enabled:"false"}, states:{hover:{fill:{type:"Solid", color:"LightColor(%Color)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize", align:"Center", padding:"0"}, range_bar_pointer_style:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, effects:{enabled:"false"}, states:{hover:{fill:{type:"Solid", color:"LightColor(%Color)"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, marker_pointer_style:{fill:{enabled:"true", color:"%Color"}, 
border:{color:"DarkColor(%Color)"}, effects:{bevel:{enabled:"true", distance:"1", highlight_opacity:"0.5", shadow_opacity:"0.5"}, enabled:"true"}, states:{hover:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}}, pushed:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, 
inner_shadow:{enabled:"true", distance:"2", color:"Black", blur_x:"3", blur_y:"3", opacity:"0.5"}}}}, name:"anychart_default", shape:"Triangle", width:"8", height:"8", align:"Inside", padding:"0"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", 
opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", 
opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, frame:{inner_stroke:{fill:{gradient:{key:[{color:"White"}, {color:"#B0B0B0"}], angle:"-135"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", type:"solid", color:"Rgb(190,190,190)"}, 
enabled:"true", thickness:"5"}, outer_stroke:{fill:{gradient:{key:[{color:"White"}, {color:"#B0B0B0"}], angle:"45"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", type:"solid", color:"Rgb(190,190,190)"}, enabled:"true", thickness:"5"}, background:{fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], angle:"45"}, enabled:"true", type:"Gradient"}, enabled:"true"}, effects:{drop_shadow:{enabled:"true", distance:"3", opacity:"0.4"}, enabled:"true"}, enabled:"true", type:"Auto", 
padding:"10"}, axis:{}, pointers:{label:{enabled:"false"}, tooltip:{enabled:"false"}, color:"#F0673B"}, defaults:{axis:{scale:{minimum:"0", maximum:"100", major_interval:"10"}, scale_bar:{fill:{color:"#75B7E1"}, enabled:"true", size:"%AxisSize"}, major_tickmark:{fill:{enabled:"true", color:"#494949"}, enabled:"true", shape:"Rectangle", width:"2", length:"%AxisSize"}, minor_tickmark:{fill:{enabled:"true", color:"#494949"}, enabled:"true", shape:"Rectangle", width:"1", length:"%AxisSize*0.6"}, labels:{format:{value:"{%Value}{numDecimals:0}"}, 
enabled:"true", rotate_circular:"False"}, scale_line:{enabled:"false", color:"#494949"}, radius:"50", size:"4", start_angle:"20", sweep_angle:"320"}}, name:"default"}, linear_template:{styles:{custom_label_style:{label:{format:{value:"Custom label"}, align:"Outside", enabled:"true", padding:"10"}, tickmark:{fill:{color:"%Color"}, border:{color:"Red"}, enabled:"true", shape:"Star5", width:"%AxisSize*.10", length:"%AxisSize*.10", align:"Outside"}, name:"anychart_default"}, trendline_style:{line:{color:"%Color"}, 
name:"anychart_default", size:"%AxisSize*.1"}, color_range_style:{fill:{color:"%Color", opacity:"0.8"}, name:"anychart_default", start_size:"%AxisSize", end_size:"%AxisSize", align:"Center", padding:"0"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", 
opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", 
opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}, bar_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.5)"}, {color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.5)"}], angle:"0"}, type:"Gradient"}, effects:{enabled:"false"}, 
states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, tank_pointer_style:{states:{hover:{color:"LightColor(%Color)"}, pushed:{color:"DarkColor(%Color)"}}, name:"anychart_default", width:"%AxisSize*0.9"}, thermometer_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.7)"}, 
{color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.7)"}], angle:"0"}, type:"Gradient"}, border:{color:"DarkColor(%Color)"}, effects:{enabled:"false"}, states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, 
{color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize", bulb_radius:"%AxisSize", bulb_padding:"9"}, range_bar_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.5)"}, 
{color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.5)"}], angle:"0"}, type:"Gradient"}, effects:{enabled:"false"}, states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, 
{color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, marker_pointer_style:{fill:{enabled:"true", color:"%Color"}, border:{color:"DarkColor(%Color)"}, effects:{bevel:{enabled:"true", 
distance:"1", highlight_opacity:"0.5", shadow_opacity:"0.5"}, enabled:"true"}, states:{hover:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}}, pushed:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, inner_shadow:{enabled:"true", distance:"2", color:"Black", 
blur_x:"3", blur_y:"3", opacity:"0.5"}}}}, name:"anychart_default", shape:"Triangle", width:"8", height:"8", align:"Inside", padding:"0"}}, axis:{}, frame:{enabled:"false"}, pointers:{label:{enabled:"false"}, tooltip:{enabled:"false"}}, defaults:{axis:{scale:{minimum:"0", maximum:"100", major_interval:"20"}, scale_bar:{fill:{gradient:{key:[{color:"#E9E9E9"}, {color:"#FFFFFF"}, {color:"#E9E9E9"}]}, enabled:"true", type:"Gradient"}, effects:{drop_shadow:{enabled:"true", opacity:"0.4", distance:"1"}, 
enabled:"true"}, border:{enabled:"true", color:"#B9B9B9"}, enabled:"true", size:"%AxisSize"}, scale_line:{enabled:"true", color:"#494949", align:"Inside"}, major_tickmark:{border:{enabled:"true", color:"#494949"}, enabled:"true", align:"Inside", width:"1.5", length:"5", shape:"Line"}, minor_tickmark:{border:{enabled:"true", color:"#494949"}, enabled:"true", align:"Inside", width:"1", length:"2", shape:"Line"}, labels:{format:{value:"{%Value}{numDecimals:0}"}, enabled:"true", align:"Inside", padding:"5"}, 
position:"50", orientation:"Vertical", size:"10", start_margin:"5", end_margin:"5"}}, name:"default"}, label_template:{styles:{label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, 
name:"default"}, image_template:{styles:{label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, 
{color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, name:"default"}, indicator_template:{styles:{tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", 
color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", 
all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], 
angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", 
blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}, indicator_pointer_style:{fill:{color:"%Color"}, border:{color:"Black"}, name:"anychart_default", shape:"Rectangle", width:"10", height:"10"}}, marker:{color:"Orange", value:"10"}, name:"default"}}};
function wf() {
}
A.e(wf, sf);
wf.prototype.Sq = x("gauge");
wf.prototype.zh = function() {
  return this.$f(wf.f.zh.call(this), vf)
};
wf.prototype.qn = function(a, b, c) {
  wf.f.qn.call(this, a, b, c);
  xf(a, b, c, "circular_template");
  xf(a, b, c, "linear_template");
  xf(a, b, c, "label_template");
  xf(a, b, c, "image_template");
  xf(a, b, c, "indicator_template");
  C(b, "circular") && 0 < I(b, "circular").length && H(c, "circular", I(b, "circular"));
  C(b, "linear") && 0 < I(b, "linear").length && H(c, "linear", I(b, "linear"));
  C(b, "label") && 0 < I(b, "label").length && H(c, "label", I(b, "label"));
  C(b, "image") && 0 < I(b, "image").length && H(c, "image", I(b, "image"));
  C(b, "indicator") && 0 < I(b, "indicator").length && H(c, "indicator", I(b, "indicator"))
};
function xf(a, b, c, d) {
  var f = I(a, d), b = I(b, d);
  c[d] = [];
  for(var d = c[d], g = f.length, a = 0;a < g;a++) {
    C(f[a], "name") && (f[a].name = N(f[a], "name")), d.push(f[a])
  }
  f = b.length;
  for(a = 0;a < f;a++) {
    if(C(b[a], "name")) {
      g = N(b[a], "name");
      H(b[a], "name", g);
      for(var k = q, l = 0;l < d.length;l++) {
        if(C(d[a], "name") && J(d[a], "name") == g) {
          k = j;
          c[l] = b[a];
          break
        }
      }
      k || d.push(b[a])
    }else {
      d.push(b[a])
    }
  }
}
;function yf() {
  this.Xa = 0;
  this.qG = q
}
z = yf.prototype;
z.Xa = p;
z.qG = p;
z.D = p;
z.ik = p;
z.K = p;
z.vc = p;
z.g = function(a, b) {
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "under_pointers") && (this.qG = K(a, "under_pointers"));
  Lb(a, "color");
  var c = Ue(b, "label_style", a, C(a, "style") ? N(a, "style") : "anychart_default");
  this.K = new zf;
  this.K.g(c)
};
z.p = function(a, b) {
  this.D = new W(a);
  this.ik = b;
  var c = this.K.wa.Mb();
  c.uc(a, c.eg);
  this.vc = c.ic(a);
  this.D.ia(this.vc);
  return this.D
};
z.qa = function() {
  this.vc && this.K.wa.Mb().update(this.vc.r(), this.vc, this.ik.x, this.ik.y, this.ik.width, this.ik.height)
};
z.Wa = function(a) {
  if(this.vc) {
    var b = this.K.wa.Mb();
    this.ik = a;
    b.update(this.vc.r(), this.vc, this.ik.x, this.ik.y, this.ik.width, this.ik.height)
  }
};
function Af() {
  Re.call(this)
}
A.e(Af, Re);
Af.prototype.Ea = p;
Af.prototype.Mb = u("Ea");
Af.prototype.g = function(a) {
  a = Af.f.g.call(this, a);
  this.Ea = new Bf;
  this.Ea.g(a);
  return a
};
function zf() {
  $.call(this)
}
A.e(zf, $);
zf.prototype.cc = function() {
  return Af
};
function Cf(a) {
  W.call(this, a)
}
A.e(Cf, W);
Cf.prototype.$a = p;
Cf.prototype.update = function(a) {
  this.$a.update(a)
};
Cf.prototype.Gi = function(a) {
  this.$a.Gi(a)
};
function Df() {
}
z = Df.prototype;
z.Ma = q;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.K = p;
z.ka = u("K");
z.rf = t("K");
z.Vq = function() {
  A.xa()
};
z.Ib = function(a, b, c) {
  return a.Wq(this, b, this.n(a, b, c))
};
z.n = x(p);
z.qb = function(a, b) {
  b && a && b.qb(a.x, a.y)
};
z.xH = function(a) {
  return new Cf(a)
};
z.sh = function(a) {
  return!a ? q : C(a, "style") ? j : C(a, "enabled") ? K(a, "enabled") != this.Ma : q
};
z.om = function(a) {
  return a && C(a, "style")
};
z.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"), delete a.enabled);
  Ef(this, a, b)
};
z.Zi = function(a, b) {
  Ff(this, a, b)
};
z.Yi = function(a, b) {
  Ff(this, a, b)
};
function Ff(a, b, c) {
  C(b, "enabled") && (a.Ma = K(b, "enabled"));
  C(b, "style") && Gf(a, c, N(b, "style"))
}
function Ef(a, b, c) {
  var d = N(b, "style"), b = Ue(c, a.Rb(), b, d);
  a.K = a.Qb();
  a.K.g(b)
}
function Gf(a, b, c) {
  var d = a.Rb(), f = b.ka(d, c);
  f ? a.K = f : (a.K = a.Qb(), a.K.g(Xe(b, d, c)))
}
z.Wa = function(a, b, c) {
  b.isEnabled() && this.qb(this.Ib(a, b, c), c)
};
z.copy = function(a) {
  a.Ma = this.Ma;
  a.K = this.K;
  return a
};
function Hf(a, b) {
  Re.call(this, a, b)
}
A.e(Hf, Re);
z = Hf.prototype;
z.enabled = j;
z.isEnabled = u("enabled");
z.padding = 5;
z.ga = u("padding");
z.anchor = 0;
z.lg = u("anchor");
z.ky = 2;
z.Go = u("ky");
z.vAlign = 1;
z.Ko = u("vAlign");
z.g = function(a) {
  a = Hf.f.g.call(this, a);
  this.enabled = Tb(a);
  if(C(a, "hatch_type")) {
    var b = J(a, "hatch_type");
    if(C(a, "hatch_fill")) {
      var c = F(a, "hatch_fill");
      C(c, "type") && H(c, "type", N(c, "type").split("%hatchtype").join(b))
    }
  }
  return a
};
function If() {
}
z = If.prototype;
z.j = p;
z.G = p;
z.D = p;
z.p = function(a, b, c) {
  this.j = a;
  this.G = b;
  this.D = c;
  this.Yc(a, b);
  this.ff(c)
};
z.update = function(a) {
  this.G.qb(this.G.Ib(this.j, a, this.D), this.D)
};
z.Gi = function(a) {
  a ? this.D.G.setAttribute("visibility", "visible") : this.D.G.setAttribute("visibility", "hidden")
};
function Jf() {
  $.call(this)
}
A.e(Jf, $);
Jf.prototype.p = function(a, b) {
  if(!b.isEnabled() || !this.wa.isEnabled() && !this.dc.isEnabled() && !this.kc.isEnabled() && !this.gc.isEnabled() && !this.Yb.isEnabled() && !this.nc.isEnabled()) {
    return p
  }
  var c = b.xH(a.r()), d = this.lc();
  d.p(a, b, c);
  c.$a = d;
  b.Vq(a).ia(c);
  return c
};
function Kf() {
}
A.e(Kf, Me);
Kf.prototype.createElement = function() {
  return this.$a.createElement()
};
Kf.prototype.Ta = function() {
  this.$a.Ta(this.G)
};
Kf.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  a.Ld() ? (c += Ld(a.Mc(), b, f, d), c += md()) : c += od();
  this.G.setAttribute("style", c)
};
Kf.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Lf() {
}
A.e(Lf, Pe);
Lf.prototype.mc = function(a) {
  return a.mc()
};
Lf.prototype.Ie = function(a) {
  return a.Ie()
};
Lf.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Mf() {
}
A.e(Mf, Me);
Mf.prototype.createElement = function() {
  return this.$a.createElement()
};
Mf.prototype.Ta = function() {
  this.$a.Ta(this.G)
};
Mf.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  a.xc() ? (c = a.Ie() ? c + Jd(a.mc(), b, f, d) : c + md(), c = a.Ld() ? c + Ld(a.Mc(), b, f, d) : c + nd()) : c += od();
  this.G.setAttribute("style", c)
};
Mf.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Nf() {
}
A.e(Nf, Me);
Nf.prototype.createElement = function() {
  return this.$a.createElement()
};
Nf.prototype.Ta = function() {
  this.$a.Ta(this.G)
};
Nf.prototype.vf = function(a) {
  this.G.setAttribute("style", a.df() ? Hc(a.De(), this.j.r(), this.j.Ze, this.j.Ba().Ba()) : od())
};
Nf.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Of() {
}
A.e(Of, Qe);
z = Of.prototype;
z.Db = p;
z.Fb = p;
z.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new Mf, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Nf, this.Fb.p(this.j, this))
};
z.ff = function() {
  var a = this.fb();
  this.Db && a.appendChild(this.Db.G);
  this.Fb && a.appendChild(this.Fb.G)
};
z.fb = function() {
  return this.j.fb()
};
z.update = function(a, b) {
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
function Pf(a, b) {
  Re.call(this, a, b)
}
A.e(Pf, Se);
z = Pf.prototype;
z.Hb = p;
z.mc = u("Hb");
z.sc = p;
z.Mc = u("sc");
z.Pc = p;
z.De = u("Pc");
z.g = function(a) {
  a = Pf.f.g.call(this, a);
  C(a, "fill") && (this.Hb = new Id, this.Hb.g(F(a, "fill")));
  this.fC(a);
  C(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  return a
};
z.fC = function(a) {
  C(a, "border") && (this.sc = new Kd, this.sc.g(F(a, "border")))
};
z.Ie = function() {
  return this.Hb && this.Hb.isEnabled()
};
z.Ld = function() {
  return this.sc && this.sc.isEnabled()
};
z.df = function() {
  return this.Pc && this.Pc.isEnabled()
};
z.xc = function() {
  return this.Ie() || this.Ld()
};
z.Ff = function() {
  return this.Ie() && this.Hb.se() || this.Ld() && this.sc.se()
};
function Qf() {
  $.call(this)
}
A.e(Qf, $);
z = Qf.prototype;
z.cc = function() {
  return Pf
};
z.lc = function() {
  return new Of
};
z.Zu = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Yb.xc() || this.gc.xc() || this.nc.xc()
};
z.mz = function() {
  return this.wa.Ld() || this.dc.Ld() || this.kc.Ld() || this.Yb.Ld() || this.gc.Ld() || this.nc.Ld()
};
z.vn = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Yb.xc() || this.gc.xc() || this.nc.xc()
};
z.wi = function() {
  return this.wa.df() || this.dc.df() || this.kc.df() || this.Yb.df() || this.gc.df() || this.nc.df()
};
z.Ff = function() {
  return this.wa.Ff() || this.dc.Ff() || this.kc.Ff() || this.Yb.Ff() || this.gc.Ff() || this.nc.Ff()
};
var Rf = {};
function Sf() {
}
A.e(Sf, Mf);
Sf.prototype.update = function(a) {
  Sf.f.update.call(this, a);
  Tf(this.$a, this.G, a)
};
function Uf() {
}
A.e(Uf, Mf);
Uf.prototype.update = function(a) {
  Uf.f.update.call(this, a);
  Tf(this.$a, this.G, a)
};
function Vf() {
}
A.e(Vf, If);
z = Vf.prototype;
z.Db = p;
z.Fb = p;
z.Yc = function(a) {
  var b = this.j.gb.ka();
  b.Zu() && (this.Db = new Sf, this.Db.p(a, this, this.Am));
  b.wi() && (this.Fb = new Uf, this.Fb.p(a, this, this.Am))
};
z.ff = function(a) {
  this.Db && a.appendChild(this.Db.G);
  this.Fb && a.appendChild(this.Fb.G)
};
z.update = function(a) {
  Vf.f.update.call(this, a);
  this.Db && this.Db.update(a);
  this.Fb && this.Fb.update(a)
};
z.Am = function() {
  return this.j.r().ja()
};
function Tf(a, b, c) {
  var d = c.ng(a.j);
  d === Wf ? b.setAttribute("style", od()) : b.setAttribute("d", a.G.IJ.Io(d, a.j.r(), c.ec(), 0, 0, c.Ka(), c.vb()))
}
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
function Xf(a, b) {
  Re.call(this, a, b)
}
A.e(Xf, Hf);
z = Xf.prototype;
z.Ra = -1;
z.Jb = u("Ra");
z.Jt = q;
z.ke = 10;
z.ec = u("ke");
z.uA = 0;
z.Ka = u("ab");
z.tA = 0;
z.vb = u("ob");
z.Y = p;
z.md = u("Y");
z.ll = function() {
  return this.Y.ll()
};
z.k = p;
z.n = u("k");
z.re = function() {
  return this.Y && this.Y.isEnabled()
};
z.qp = function() {
  return this.re() && Boolean(this.Y.Ve && this.Y.Ve.isEnabled())
};
z.Ie = function() {
  return this.re() && this.Y.mc() && this.Y.mc().isEnabled()
};
z.Ld = function() {
  return this.re() && this.Y.Mc() && this.Y.Mc().isEnabled()
};
z.xc = function() {
  return this.Ie() || this.Ld()
};
z.df = function() {
  return this.re() && this.Y.De() && this.Y.mc().isEnabled()
};
z.mc = function() {
  return this.Y.mc()
};
z.De = function() {
  return this.Y.De()
};
z.Mc = function() {
  return this.Y.Mc()
};
z.ng = function(a) {
  return this.Jt ? a.ng() : this.Ra
};
z.g = function(a) {
  var a = Xf.f.g.call(this, a), b = F(a, "marker");
  C(a, "marker_type") && b && C(b, "type") && H(b, "type", N(b, "type").split("%markertype").join(J(a, "marker_type")));
  if(b && (C(b, "anchor") && (this.anchor = zd(F(b, "anchor"))), C(b, "size") && (this.ke = M(b, "size")), C(b, "size_width") && (this.uA = M(b, "size_width")), C(b, "size_height") && (this.tA = M(b, "size_height")), C(b, "padding") && (this.padding = M(b, "padding")), C(b, "h_align") && (this.ky = vd(F(b, "h_align"))), C(b, "v_align") && (this.vAlign = yd(F(b, "v_align"))), C(b, "type"))) {
    b = N(b, "type"), this.Jt = "%markertype" == b, this.Jt || (this.Ra = Yf(b))
  }
  0 == this.tA && 0 == this.uA && (this.tA = this.uA = this.ke);
  10 == this.anchor && (this.vAlign = this.ky = 2);
  this.Y = new Nd;
  this.Y.g(a);
  this.k = new P(0, 0, this.uA, this.tA);
  return a
};
function Zf() {
  $.call(this)
}
A.e(Zf, Jf);
z = Zf.prototype;
z.cc = function() {
  return Xf
};
z.lc = function() {
  return new Vf
};
z.Zu = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Yb.xc() || this.gc.xc() || this.nc.xc()
};
z.mz = function() {
  return this.wa.Ld() || this.dc.Ld() || this.kc.Ld() || this.Yb.Ld() || this.gc.Ld() || this.nc.Ld()
};
z.vn = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Yb.xc() || this.gc.xc() || this.nc.xc()
};
z.wi = function() {
  return this.wa.df() || this.dc.df() || this.kc.df() || this.Yb.df() || this.gc.df() || this.nc.df()
};
z.Ff = function() {
  return this.wa.Ff() || this.dc.Ff() || this.kc.Ff() || this.Yb.Ff() || this.gc.Ff() || this.nc.Ff()
};
var Wf = -1;
function Yf(a) {
  switch(a) {
    default:
    ;
    case "none":
      return Wf;
    case "circle":
      return 0;
    case "square":
      return 1;
    case "diamond":
      return 2;
    case "cross":
      return 3;
    case "diagonalcross":
      return 4;
    case "hline":
      return 5;
    case "vline":
      return 6;
    case "star4":
      return 7;
    case "star5":
      return 8;
    case "star6":
      return 9;
    case "star7":
      return 10;
    case "star10":
      return 11;
    case "triangleup":
      return 12;
    case "triangledown":
      return 13;
    case "image":
      return 14
  }
}
function $f() {
  this.Ra = Wf;
  this.IJ = new ag
}
A.e($f, Df);
z = $f.prototype;
z.Ra = 0;
z.Jb = u("Ra");
z.IJ = p;
z.Rb = x("marker_style");
z.Qb = function() {
  return new Zf
};
z.Vq = function(a) {
  return a.ca().Zy
};
z.Zi = function(a, b) {
  $f.f.Zi.call(this, a, b);
  bg(this, a)
};
z.Yi = function(a, b) {
  $f.f.Yi.call(this, a, b);
  bg(this, a)
};
function bg(a, b) {
  C(b, "type") && (a.Ra = Yf(N(b, "type")))
}
z.n = function(a, b) {
  return b.n()
};
z.copy = function(a) {
  a || (a = new $f);
  a.Ra = this.Ra;
  return $f.f.copy.call(this, a)
};
function ag() {
}
ag.prototype.Io = function(a, b, c, d, f, g, k) {
  switch(a) {
    default:
    ;
    case Wf:
      return p;
    case 0:
      return k = S(d, f + c / 2), k += b.ub(c / 2, q, j, d + c / 2, f), k += b.ub(c / 2, q, j, d + c, f + c / 2), k += b.ub(c / 2, q, j, d + c / 2, f + c), k += b.ub(c / 2, q, j, d, f + c / 2);
    case 1:
      return k || (k = c), g || (g = c), c = d + 1, f += 1, d = g - 2, k -= 2, g = S(c, f), g += U(c + d, f), g += U(c + d, f + k), g += U(c, f + k), g += U(c, f), g + " Z";
    case 2:
      return k = S(d + c / 2, f), k += U(d + c, f + c / 2), k += U(d + c / 2, f + c), k += U(d, f + c / 2), k += U(d + c / 2, f);
    case 3:
      return k = c / 4, g = S(d + c / 2 - k / 2, f), g += U(d + c / 2 - k / 2, f + c / 2 - k / 2), g += U(d, f + c / 2 - k / 2), g += U(d, f + c / 2 + k / 2), g += U(d + c / 2 - k / 2, f + c / 2 + k / 2), g += U(d + c / 2 - k / 2, f + c), g += U(d + c / 2 + k / 2, f + c), g += U(d + c / 2 + k / 2, f + c / 2 + k / 2), g += U(d + c, f + c / 2 + k / 2), g += U(d + c, f + c / 2 - k / 2), g += U(d + c / 2 + k / 2, f + c / 2 - k / 2), g += U(d + c / 2 + k / 2, f), g += U(d + c / 2 - k / 2, f);
    case 4:
      return k = c / 4 * Math.SQRT2 / 2, g = S(d + k, f), g += U(d + c / 2, f + c / 2 - k), g += U(d + c - k, f), g += U(d + c, f + k), g += U(d + c / 2 + k, f + c / 2), g += U(d + c, f + c - k), g += U(d + c - k, f + c), g += U(d + c / 2, f + c / 2 + k), g += U(d + k, f + c), g += U(d, f + c - k), g += U(d + c / 2 - k, f + c / 2), g += U(d, f + k), g += U(d + k, f);
    case 5:
      return this.sC(b, c, d, f);
    case 6:
      return k = S(d + (c - c / 4) / 2, f), k += U(d + (c - c / 4) / 2 + c / 4, f), k += U(d + (c - c / 4) / 2 + c / 4, f + c), k += U(d + (c - c / 4) / 2, f + c), k += U(d + (c - c / 4) / 2, f);
    case 13:
      return k = S(d + c / 2, f + c), k += U(d + c, f), k += U(d, f), k += U(d + c / 2, f + c);
    case 12:
      return k = S(d + c / 2, f), k += U(d + c, f + c), k += U(d, f + c), k += U(d + c / 2, f);
    case 7:
      return cg(c, d, f, 4);
    case 8:
      return cg(c, d, f, 5);
    case 9:
      return dg(c, d, f, 6);
    case 10:
      return cg(c, d, f, 7);
    case 11:
      return dg(c, d, f, 10)
  }
};
ag.prototype.sC = function(a, b, c, d) {
  a = S(c, d + (b - b / 4) / 2);
  a += U(c + b, d + (b - b / 4) / 2);
  a += U(c + b, d + (b - b / 4) / 2 + b / 4);
  a += U(c, d + (b - b / 4) / 2 + b / 4);
  return a += U(c, d + (b - b / 4) / 2)
};
function cg(a, b, c, d) {
  for(var f = "", a = a / 2, g = a / 2, k = 2 * Math.PI / d, l = -Math.PI / 2, n = l + k / 2, m = 0;m <= d;m++) {
    var o = a * Math.cos(l), v = a * Math.sin(l), w = g * Math.cos(n), y = g * Math.sin(n), o = o + a, v = v + a, w = w + a, y = y + a, f = 0 == m ? f + S(b + o, c + v) : f + U(b + o, c + v), f = f + U(b + w, c + y), l = l + k, n = n + k
  }
  return f
}
function dg(a, b, c, d) {
  var a = a / 2, f = 2 * Math.PI / d, g = [], k, l = "";
  for(k = 0;k <= d;k++) {
    g.push(new O(-a * Math.sin(k * f), -a * Math.cos(k * f)))
  }
  l = g[0];
  k = g[1];
  var n = g[2], m = new O;
  m.x = (l.x - n.x) * (k.y - n.y) / (l.y - n.y) + n.x;
  m.y = k.y;
  m = Math.sqrt(Math.pow(m.x, 2) + Math.pow(m.y, 2));
  n = [];
  for(k = 0;k <= d;k++) {
    n.push(new O(-m * Math.sin(k * f + f / 2), -m * Math.cos(k * f + f / 2)))
  }
  var l = S(b + l.x + a, c + l.y + a), o = 0;
  for(k = 0;k <= 2 * d;k += 2) {
    f = g[o], m = n[o], l += U(b + f.x + a, c + f.y + a), l += U(b + m.x + a, c + m.y + a), o++
  }
  return l
}
;function eg() {
}
A.e(eg, Td);
z = eg.prototype;
z.Rh = 0.05;
z.Va = u("Rh");
z.re = function() {
  return this.isEnabled()
};
z.g = function(a) {
  eg.f.g.call(this, a);
  this.isEnabled() && C(a, "thickness") && (this.Rh = Qb(a, "thickness"))
};
z.p = function(a) {
  if(!this.isEnabled()) {
    return p
  }
  var b = new fg(a);
  b.pi = a.ja();
  b.xi = a.ja();
  b.Kw = a.ja();
  b.Lw = a.ja();
  b.pi && b.appendChild(b.pi);
  b.xi && b.appendChild(b.xi);
  b.Kw && b.appendChild(b.Kw);
  b.Lw && b.appendChild(b.Lw);
  return b
};
z.qa = function(a, b) {
  var c = a.r(), d = a.pi, f = p, f = this.Mc() && this.Mc().isEnabled() ? Ld(this.Mc(), c, b) : nd(), f = f + md();
  d.setAttribute("d", gg(this, c, b));
  d.setAttribute("style", f);
  c = a.r();
  d = a.xi;
  f = p;
  f = this.Mc() && this.Mc().isEnabled() ? Ld(this.Mc(), c, b) : nd();
  f += md();
  d.setAttribute("d", hg(this, c, b));
  d.setAttribute("style", f);
  this.pC(a.r(), a.Kw, a.Lw, b)
};
z.rC = function(a, b, c, d, f, g) {
  var c = c.r(), k = "", k = this.Hb && this.Hb.isEnabled() ? k + Jd(this.Hb, c, d, f, j) : k + md(), k = this.sc && this.sc.isEnabled() ? k + Ld(this.sc, c, d, f, j) : k + nd();
  Vd(a, b, k, g)
};
z.createElement = function(a) {
  return a.ja()
};
function fg(a) {
  W.call(this, a)
}
A.e(fg, W);
fg.prototype.pi = p;
fg.prototype.xi = p;
fg.prototype.Kw = p;
fg.prototype.Lw = p;
function ig(a) {
  switch(a) {
    default:
    ;
    case "none":
      return-1;
    case "circle":
      return 0;
    case "square":
      return 1;
    case "diamond":
      return 2;
    case "cross":
      return 3;
    case "diagonalcross":
      return 4;
    case "hline":
      return 5;
    case "vline":
      return 6;
    case "star4":
      return 7;
    case "star5":
      return 8;
    case "star6":
      return 9;
    case "star7":
      return 10;
    case "star10":
      return 11;
    case "triangleup":
      return 12;
    case "triangledown":
      return 13;
    case "image":
      return 14;
    case "rectangle":
      return 105;
    case "triangle":
      return 106;
    case "trapezoid":
      return 107;
    case "pentagon":
      return 109;
    case "line":
      return 108
  }
}
function jg() {
}
A.e(jg, ag);
jg.prototype.Io = function(a, b, c, d, f, g, k, l) {
  switch(a) {
    case 109:
      g = Math.min(g, k) / 2;
      k = S(d + g * this.bF[0], f + g * this.cF[0]);
      for(a = 1;5 > a;a++) {
        k += U(d + g * this.bF[a], f + g * this.cF[a])
      }
      k += U(d + g * this.bF[0], f + g * this.cF[0]);
      return k + " Z";
    case 105:
      return fd(d, f, g, k);
    case 107:
      return a = g / 3, b = S(d + a, f), b += U(d + g - a, f), b += U(d + g, f + k), b += U(d, f + k), b += U(d + a, f), b + " Z";
    case 106:
      return a = S(d + g / 2, f), a += U(d, f + k), a += U(d + g, f + k), a += U(d + g / 2, f), a + " Z";
    case 108:
      return Jc(d + g / 2, f, d + g / 2, f + k, l);
    case 5:
      return this.sC(b, c, d, f, g, k, l);
    default:
      return jg.f.Io.call(this, a, b, c, d, f, g, k)
  }
};
jg.prototype.bF = [1 + Math.cos((0.4 - 0.5) * Math.PI), 1 + Math.cos((0.8 - 0.5) * Math.PI), 1 + Math.cos(0.7 * Math.PI), 1 + Math.cos(1.1 * Math.PI), 1 + Math.cos(1.5 * Math.PI)];
jg.prototype.cF = [1 + Math.sin((0.4 - 0.5) * Math.PI), 1 + Math.sin((0.8 - 0.5) * Math.PI), 1 + Math.sin(0.7 * Math.PI), 1 + Math.sin(1.1 * Math.PI), 1 + Math.sin(1.5 * Math.PI)];
jg.prototype.sC = function(a, b, c, d, f, g, k) {
  return Jc(c, d + g / 2, c + f, d + g / 2, k)
};
function kg(a) {
  this.Wb = a;
  this.padding = 0;
  this.type = 2
}
z = kg.prototype;
z.Uc = p;
z.Vd = p;
z.background = p;
z.ym = p;
z.type = p;
z.zb = p;
z.Wb = p;
z.padding = p;
z.qB = j;
z.g = function(a) {
  if(!C(a, "enabled") || K(a, "enabled")) {
    if(C(a, "type")) {
      switch(N(a, "type")) {
        case "circular":
          this.type = 0;
          break;
        case "auto":
          this.type = 1;
          break;
        case "rectangular":
          this.type = 2;
          break;
        case "roundedrectangular":
          this.type = 3
      }
    }
    C(a, "auto_fit") && (this.qB = K(a, "auto_fit"));
    C(a, "padding") && (this.padding = Qb(a, "padding"));
    Tb(a, "inner_stroke") && (this.Uc = this.rH(), this.Uc.g(F(a, "inner_stroke")));
    Tb(a, "outer_stroke") && (this.Vd = this.sH(), this.Vd.g(F(a, "outer_stroke")));
    C(a, "background") && (this.background = this.nH(), this.background.g(F(a, "background")));
    C(a, "corners") && (this.ym = new Cd, this.ym.g(F(a, "corners")))
  }
};
z.cd = p;
z.wB = p;
z.SD = p;
z.WE = p;
z.Bg = p;
z.p = function(a, b) {
  this.Bg = a;
  this.zb = b;
  this.cd = new W(this.Bg);
  this.Vd && this.Vd.isEnabled() && (this.WE = this.Vd.p(this.Bg), this.cd.ia(this.WE));
  this.Uc && this.Uc.isEnabled() && (this.SD = this.Uc.p(this.Bg), this.cd.ia(this.SD));
  this.background && this.background.isEnabled() && (this.wB = this.background.p(this.Bg), this.cd.ia(this.wB));
  return this.cd
};
z.qa = function() {
  this.Vd && this.Vd.isEnabled() && this.Vd.qa(this.WE, this.zb);
  this.Uc && this.Uc.isEnabled() && this.Uc.qa(this.SD, this.zb);
  this.background && this.background.isEnabled() && this.background.qa(this.wB, this.zb)
};
z.Wa = function(a) {
  this.zb = a;
  this.qa()
};
function lg(a, b) {
  Re.call(this, a, b)
}
A.e(lg, Hf);
z = lg.prototype;
z.Zv = p;
z.ej = u("Zv");
z.NK = p;
z.Bh = u("NK");
z.g = function(a) {
  a = lg.f.g.call(this, a);
  if(!this.enabled) {
    return a
  }
  this.Zv = this.yH();
  this.Zv.g(a);
  if(!this.Zv.isEnabled()) {
    return this.enabled = q, a
  }
  if(C(a, "position")) {
    var b = F(a, "position");
    C(b, "anchor") && (this.anchor = zd(F(b, "anchor")));
    C(b, "halign") && (this.ky = vd(F(b, "halign")));
    C(b, "valign") && (this.vAlign = yd(F(b, "valign")));
    C(b, "padding") && (this.padding = M(b, "padding"))
  }
  C(a, "format") && (this.NK = new ie(J(a, "format")));
  return a
};
z.yH = function() {
  return new ye
};
function mg() {
}
A.e(mg, If);
z = mg.prototype;
z.kf = p;
z.Yc = function(a, b) {
  var c = a.be(b.ka()), d = c.ej();
  d.uc(a.r(), (b.yc ? b.yc : c.Bh()).ta(a, this.Gd(a)));
  this.ic(a, b, d)
};
z.Gd = function(a) {
  return a.ca ? a.ca().Gd() : p
};
z.ic = function(a, b, c) {
  this.kf = c.ic(a.r(), a.Ba().Ba(), q)
};
z.ff = function(a) {
  a.ia(this.kf)
};
z.update = function(a) {
  if(!a.isEnabled() || !a.ej()) {
    this.Gi(q)
  }else {
    mg.f.update.call(this, a);
    var b = a.ej(), c = this.j.r();
    b.uc(c, (this.G.yc ? this.G.yc : a.Bh()).ta(this.j, this.Gd(this.j)));
    this.HC(c, b, this.kf, this.j.Ba().Ba());
    this.Gi(j)
  }
};
z.HC = function(a, b, c, d) {
  b.update(a, c, d)
};
z.Gi = function(a) {
  a ? this.kf.G.setAttribute("visibility", "visible") : this.kf.G.setAttribute("visibility", "hidden")
};
function ng() {
  $.call(this)
}
A.e(ng, Jf);
ng.prototype.lc = function() {
  return new mg
};
ng.prototype.cc = function() {
  return lg
};
function og() {
}
A.e(og, Df);
z = og.prototype;
z.yc = p;
z.Rb = x("label_style");
z.Qb = function() {
  return new ng
};
z.Vq = function(a) {
  return a.ca().Wx(this)
};
z.om = function(a) {
  return a && (C(a, "format") || C(a, "style"))
};
z.Ue = function(a, b) {
  og.f.Ue.call(this, a, b);
  pg(this, a)
};
z.Zi = function(a, b) {
  og.f.Zi.call(this, a, b);
  pg(this, a)
};
z.Yi = function(a, b) {
  og.f.Yi.call(this, a, b);
  pg(this, a)
};
function pg(a, b) {
  C(b, "format") && (a.yc = new ie(J(b, "format")))
}
z.n = function(a, b, c) {
  b.ej().uc(c.r(), (this.yc ? this.yc : b.Bh()).ta(a, a.Gd ? a.Gd() : p));
  return b.ej().n().Ca()
};
z.copy = function(a) {
  a || (a = new og);
  a.yc = this.yc;
  return og.f.copy.call(this, a)
};
function qg(a, b) {
  Re.call(this, a, b)
}
A.e(qg, lg);
qg.prototype.g = function(a) {
  a = qg.f.g.call(this, a);
  9 == this.anchor && 2 == this.vAlign && (this.padding += 20);
  return a
};
function rg() {
  $.call(this)
}
A.e(rg, ng);
rg.prototype.p = function(a, b) {
  var c = rg.f.p.call(this, a, b);
  c && c.Gi(q);
  return c
};
rg.prototype.Qb = function() {
  return new qg
};
function sg() {
}
A.e(sg, og);
sg.prototype.Rb = x("tooltip_style");
sg.prototype.Qb = function() {
  return new rg
};
sg.prototype.Vq = function(a) {
  return a.ca().ey()
};
function tg(a, b, c, d, f) {
  if(9 == d.lg()) {
    var d = a.n(c, d, f), g = document.getElementById("anychart-tooltip-div"), k = document.getElementById("anychart-tooltip-svg"), l = c.r(), c = l.Ic();
    g || (g = document.createElement("div"), g.id = "anychart-tooltip-div", g.style.position = "absolute", g.style.left = "0px", g.style.top = "0px", g.style.pointerEvents = "none", g.style.zIndex = "99999", document.body.appendChild(g), k = document.createElementNS(l.Tv, "svg"), k.setAttribute("xmlns", l.Tv), k.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink"), k.setAttribute("width", "100%"), k.setAttribute("height", "100%"), k.setAttribute("id", "anychart-tooltip-svg"), g.appendChild(k));
    var n = l = 0;
    if(A.userAgent.gm) {
      l = b.clientX + c.offsetLeft, n = b.clientY + c.offsetTop - d.height
    }else {
      for(c = f.r().Ic().parentNode;c != p;) {
        c = c.offsetParent
      }
      l = b.clientX + (window.pageXOffset || document.body.scrollLeft);
      n = b.clientY + (window.pageYOffset || document.body.scrollTop) - d.height
    }
    g.style.left = l + "px";
    g.style.top = n + "px";
    g.style.x = l + "px";
    g.style.y = n + "px";
    g.style.width = d.width + "px";
    g.style.height = d.height + "px";
    window.lastSprite ? window.lastSprite !== f.G && (k.removeChild(window.lastSprite), k.appendChild(f.G)) : k.appendChild(f.G);
    window.lastSprite = f.G;
    a.qb({x:0, y:0}, f)
  }
}
sg.prototype.copy = function(a) {
  a || (a = new sg);
  return sg.f.copy.call(this, a)
};
function ug(a, b) {
  Re.call(this, a, b)
}
A.e(ug, lg);
ug.prototype.yH = function() {
  return new Bf
};
ug.prototype.iD = function() {
  return this.Zv.iD()
};
function vg() {
}
A.e(vg, mg);
vg.prototype.Gd = x(p);
vg.prototype.HC = function(a, b, c, d) {
  b.update(this.j.r(), c, 0, 0, this.j.w.Zc().n().width, this.j.w.Zc().n().height, d)
};
function wg() {
  $.call(this)
}
A.e(wg, ng);
wg.prototype.lc = function() {
  return new vg
};
wg.prototype.cc = function() {
  return ug
};
function xg() {
  this.Ma = j;
  this.bw = q
}
A.e(xg, og);
z = xg.prototype;
z.Oc = p;
z.Zc = u("Oc");
z.$z = t("Oc");
z.bw = p;
z.Qb = function() {
  return new wg
};
z.Vq = function(a) {
  return this.bw ? a.Zc().Ry : a.Zc().Qy
};
z.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  C(a, "under_pointers") && (this.bw = K(a, "under_pointers"));
  Ef(this, a, b)
};
z.Ib = function(a, b) {
  var c = new O;
  switch(b.iD()) {
    case yg:
      a.Qq(b.lg(), c);
      break;
    case zg:
    ;
    case Ag:
      c.x = this.Oc.n().x, c.y = this.Oc.n().y
  }
  return c
};
z.copy = function(a) {
  a || (a = new xg);
  a.$z(this.Oc);
  a.bw = this.bw;
  return xg.f.copy.call(this, a)
};
function Bg() {
}
A.e(Bg, sg);
Bg.prototype.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  Ef(this, a, b)
};
Bg.prototype.Vq = function(a) {
  return a.w.Zc().uf
};
Bg.prototype.copy = function(a) {
  a || (a = new Bg);
  return Bg.f.copy.call(this, a)
};
function Cg() {
  this.y = this.x = 0;
  this.height = this.width = 1;
  this.zb = new P;
  this.Oa = new Ad(0);
  this.children = [];
  this.O = []
}
z = Cg.prototype;
z.aD = x(0);
z.Sa = p;
z.getName = u("Sa");
z.ZE = p;
z.Tl = s();
z.x = 0;
z.y = 0;
z.width = 1;
z.height = 1;
z.children = p;
z.tf = p;
z.PE = function() {
  return this.children.length
};
z.zb = p;
z.n = u("zb");
z.Oa = p;
z.O = p;
z.tF = p;
z.mb = function(a) {
  a = a || {};
  a.children = [];
  for(var b = 0;b < this.children.length;b++) {
    a.children.push(this.children[b].mb())
  }
  a.name = this.Sa;
  return a
};
z.g = function(a) {
  this.tf = new Te(F(a, "styles"));
  C(a, "name") && (this.Sa = J(a, "name"));
  C(a, "parent") && (this.ZE = J(a, "parent"));
  C(a, "x") && (this.x = Qb(a, "x"));
  C(a, "y") && (this.y = Qb(a, "y"));
  C(a, "width") && (this.width = Qb(a, "width"));
  C(a, "height") && (this.height = Qb(a, "height"));
  C(a, "margin") && this.Oa.g(F(a, "margin"));
  (this.frame = this.qH(F(a, "frame"))) && C(a, "frame") && this.frame.g(F(a, "frame"));
  if(C(a, "labels")) {
    for(var a = I(F(a, "labels"), "label"), b = a.length, c = 0;c < b;c++) {
      var d = new yf;
      d.g(a[c], this.tf);
      this.O.push(d)
    }
  }
};
z.frame = p;
z.Bg = p;
z.cd = p;
z.uf = p;
z.ix = p;
z.yB = p;
z.Ry = p;
z.Qy = p;
z.p = function(a, b, c) {
  this.Bg = a;
  this.Rc(b);
  this.cd = new W(this.Bg);
  this.uf = c;
  this.frame && this.cd.ia(this.frame.p(this.Bg, this.zb));
  this.ix = new W(this.Bg);
  this.yB = new W(this.Bg);
  this.Ry = new W(this.Bg);
  this.Qy = new W(this.Bg);
  this.ix.ia(this.yB);
  this.cd.ia(this.Ry);
  this.cd.ia(this.ix);
  this.cd.ia(this.Qy);
  var d, b = 0;
  for(d = this.O.length;b < d;b++) {
    var f = this.O[b];
    f.qG ? this.Ry.ia(f.p(a, this.zb)) : this.Qy.ia(f.p(a, this.zb))
  }
  b = 0;
  for(d = this.children.length;b < d;b++) {
    this.cd.ia(this.children[b].p(this.Bg, this.zb, c))
  }
  return this.cd
};
z.qa = function() {
  var a, b;
  this.frame && this.frame.qa();
  a = 0;
  for(b = this.O.length;a < b;a++) {
    this.O[a].qa()
  }
  a = 0;
  for(b = this.children.length;a < b;a++) {
    this.children[a].qa()
  }
};
z.Wa = function(a) {
  var b;
  this.Rc(a);
  this.frame && this.frame.Wa(this.zb);
  a = 0;
  for(b = this.O.length;a < b;a++) {
    this.O[a].Wa(this.zb)
  }
  a = 0;
  for(b = this.children.length;a < b;a++) {
    this.children[a].Wa(this.zb)
  }
};
z.Rc = function(a) {
  this.zb.x = a.x + this.x * a.width;
  this.zb.y = a.y + this.y * a.height;
  this.zb.width = a.width * this.width;
  this.zb.height = a.height * this.height;
  var a = this.zb, b = a.width, c = a.height;
  a.bh(a.tb() + this.Oa.tb() * b / 100);
  a.eh(a.Ja() - this.Oa.Ja() * b / 100);
  a.fh(a.rb() + this.Oa.rb() * c / 100);
  a.$g(a.ra() - this.Oa.ra() * c / 100)
};
function Dg() {
}
Dg.prototype.apply = function(a, b, c) {
  var d = Eg(b, "default"), f = C(a, "template") ? N(a, "template") : c;
  if(f) {
    c = [];
    for(f = Eg(b, f);f;) {
      c.push(f), f = C(f, "parent") ? Eg(b, N(f, "parent")) : p
    }
    for(b = c.length - 1;0 <= b;b--) {
      d = Fg(d, c[b])
    }
  }
  return Fg(d, a)
};
function Fg(a, b) {
  var c = $b(a, b);
  a && Gg(a);
  b && Gg(b);
  var d = cc(F(a, "styles"), F(b, "styles"));
  d && H(c, "styles", d);
  if(C(c, "defaults") && (d = F(c, "defaults"), C(d, "axis"))) {
    d = F(d, "axis");
    if(C(c, "axis")) {
      var f = Hg(d, F(a, "axis")), f = Hg(f, F(c, "axis"));
      H(c, "axis", f)
    }
    if(C(c, "extra_axes")) {
      for(var f = [], g = F(c, "extra_axes"), k = I(g, "axis"), l = k.length, n = 0;n < l;n++) {
        f.push(Hg(d, k[n]))
      }
      H(g, "axis", f)
    }
  }
  return c
}
function Hg(a, b) {
  if(b == p && a != p) {
    return a
  }
  if(b != p && a == p) {
    return b
  }
  var c = $b(a, b);
  H(c, "color_ranges", gc(F(a, "color_ranges"), F(b, "color_ranges"), "color_range"));
  H(c, "custom_labels", gc(F(a, "custom_labels"), F(b, "custom_labels"), "custom_label"));
  H(c, "trendlines", gc(F(a, "trendlines"), F(b, "trendlines"), "trendline"));
  return c
}
function Gg(a) {
  if(C(a, "styles")) {
    for(var a = I(a, "styles"), b = 0;b < a.length;b++) {
      C(a[b], "name") && (a[b].name = N(a[b], "name"))
    }
  }
}
function Eg(a, b) {
  for(var b = b.toLowerCase(), c = 0;c < a.length;c++) {
    var d = a[c];
    if(C(d, "name") && N(d, "name") == b) {
      return d
    }
  }
  return p
}
;function Ig(a, b) {
  Re.call(this, a, b);
  this.za = Jg;
  this.Ia = 0
}
A.e(Ig, Pf);
z = Ig.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.g = function(a) {
  var b = a = Ig.f.g.call(this, a);
  C(b, "align") && (this.za = Kg(b));
  C(b, "padding") && (this.Ia = Qb(b, "padding"));
  return a
};
z.Qo = function(a) {
  this.Hb && this.Hb.Ff() && a.Me(this.Hb.yb);
  this.sc && this.sc.Ff() && a.Me(this.sc.yb)
};
z.Qo = function(a) {
  this.Hb && this.Hb.se() && a.Me(this.Hb.yb);
  this.Kb && this.Kb.se() && a.Me(this.Kb.yb)
};
function Lg() {
  this.k = new P;
  this.vv = this.iE = this.vj = q;
  this.Xa = zb("#F1683C", h)
}
z = Lg.prototype;
z.Oc = p;
z.Zc = u("Oc");
z.$z = t("Oc");
z.vj = q;
z.Ra = -1;
z.Sa = p;
z.getName = u("Sa");
z.AD = function() {
  return this.Sa && "" != this.Sa
};
z.vv = q;
z.Tn = j;
z.w = p;
z.wj = t("w");
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.jd = p;
z.Pf = p;
z.K = p;
z.ka = u("K");
z.ya = p;
z.Ce = p;
z.Ea = p;
z.Fc = p;
z.vc = p;
z.uf = p;
z.bb = NaN;
z.ta = u("bb");
z.Kn = t("bb");
z.fs = p;
function Mg(a, b) {
  var c = a.w.fa();
  return!c.pd && b < c.la ? c.la : !c.od && b > c.pa ? c.pa : b
}
z.k = p;
z.n = u("k");
z.Xa = p;
z.Ba = u("Xa");
z.Di = t("Xa");
z.Ze = p;
z.Ip = t("Ze");
z.g = function(a, b, c) {
  a: {
    var d = C(a, "style") ? N(a, "style") : "anychart_default", f = this.Rb(), g = this.Rb() ? F(a, this.Rb()) : a, k;
    if(g) {
      k = Ue(b, f, g, d)
    }else {
      k = b.ka(f, d);
      if(k != p) {
        this.K = k;
        break a
      }
      k = Xe(b, f, d)
    }
    k ? (this.K = this.Qb(), this.K.g(k), g && (g = this.K, b.Ti[f] || (b.Ti[f] = {}), b.Ti[f][d] = g)) : this.K = b.ka(f, "anychart_default")
  }
  d = this.K;
  d.wa.Qo && (d.wa.Qo(this.Oc), d.dc.Qo(this.Oc), d.kc.Qo(this.Oc), d.Yb.Qo(this.Oc), d.gc.Qo(this.Oc));
  C(a, "name") && (this.Sa = F(a, "name"));
  C(a, "value") && (this.bb = M(a, "value"));
  C(a, "selected") && (this.vj = K(a, "selected"));
  C(a, "use_hand_cursor") && (this.Tn = K(a, "use_hand_cursor"));
  C(a, "allow_select") && (this.vv = K(a, "allow_select"));
  if(C(a, "attributes")) {
    this.jd = {};
    d = I(F(a, "attributes"), "attribute");
    f = d.length;
    for(g = 0;g < f;g++) {
      if(k = d[g], C(k, "name")) {
        var l;
        C(k, "custom_attribute_value") ? l = F(k, "custom_attribute_value") : C(k, "value") && (l = F(k, "value"));
        l && (this.jd["%" + J(k, "name")] = l)
      }
    }
  }
  C(a, "actions") && (this.Pf = new Ee(c, this), this.Pf.g(F(a, "actions")));
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "hath_type") && (this.Ze = Gc(N(a, "hatch_type")));
  C(a, "label") && this.Ea.Ue(F(a, "label"), b);
  C(a, "tooltip") && this.Fc.Ue(F(a, "tooltip"), b);
  this.bb && !isNaN(this.bb) && (this.fs = this.bb.toString())
};
z.nu = function(a) {
  this.Tn && ld(a.G);
  A.I.nb(a.G, ka.Ms, this.nj, q, this);
  A.I.nb(a.G, ka.Ls, this.Jl, q, this);
  A.I.nb(a.G, ka.fm, this.xN, q, this);
  A.I.nb(a.G, ka.dB, this.Il, q, this);
  A.I.nb(a.G, ka.Xn, this.RE, q, this)
};
z.nj = function() {
  this.iE = j;
  this.Ce = this.vj ? "SelectedHover" : "Hover";
  this.ya = this.be(this.K);
  this.$a.update(this.ya, q);
  this.uf && (this.uf.update(this.be(this.Fc.ka())), this.uf.Bj(j))
};
z.Jl = function() {
  this.iE = q;
  this.Ce = this.vj ? "SelectedNormal" : "Normal";
  this.ya = this.be(this.K);
  this.$a.update(this.ya, q);
  this.uf && this.uf.Gi(q)
};
z.xN = function() {
  this.Pf && this.Pf.execute(this);
  this.vj || (this.Ce = "Pushed", this.ya = this.be(this.K), this.$a.update(this.ya, q))
};
z.Il = function(a) {
  this.vv && !this.vj && (this.Oc.tF && this.Oc.tF.ux(), this.Oc.tF = this, this.vj = j, this.nj(a))
};
z.RE = function(a) {
  this.iE && this.uf && tg(this.Fc, a, this, this.be(this.Fc.ka()), this.uf)
};
z.ux = function() {
  this.vj = q;
  this.Jl(p)
};
z.p = function(a) {
  this.D = new W(a);
  this.nu(this.D);
  this.Ce = (this.vj = this.vv && this.vj) ? "SelectedNormal" : "Normal";
  this.ya = this.be(this.K);
  this.$a = this.K.p(this);
  this.Ea && (this.vc = this.Ea.ka().p(this, this.Ea)) && this.nu(this.vc);
  this.Fc && (this.uf = this.Fc.ka().p(this, this.Fc));
  return this.D
};
z.qa = function() {
  this.$a.update(this.ya, j);
  if(this.Ea) {
    var a = this.Ea, b = this.vc;
    b && a && b.update(this.be(a.ka()))
  }
};
z.be = function(a) {
  return this.qD(a, this.Ce)
};
z.qD = function(a, b) {
  switch(b) {
    default:
    ;
    case "Normal":
      return a.wa;
    case "Hover":
      return a.dc;
    case "Pushed":
      return a.kc;
    case "SelectedNormal":
      return a.Yb;
    case "SelectedHover":
      return a.gc;
    case "Missing":
      return a.nc
  }
};
z.Wa = function() {
  this.$a.update(p, j);
  this.Dp(this.Ea, this.vc)
};
z.Dp = function(a, b) {
  a && b && b.update(this.be(a.ka()))
};
z.Na = function(a) {
  return"%Value" == a ? this.fs : "%Name" == a ? this.Sa : this.jd[a] ? this.jd[a] : ""
};
z.Pa = function(a) {
  return"%Value" == a ? 2 : 1
};
z.Wq = function(a, b, c) {
  if(!a || !b || !c || this.Gb && !this.aa.qk) {
    return p
  }
  a = new O;
  this.Qq(a, b.lg());
  10 != b.lg() && (this.wv(a, b.lg(), b.Go(), b.Ko(), c, b.ga()), this.xv(a, b.lg(), b.Go(), b.Ko(), c, b.ga()));
  return a
};
z.wv = function(a, b, c, d, f, g) {
  switch(c) {
    case 0:
      a.x -= f.width + g;
      break;
    case 1:
      a.x += g;
      break;
    case 2:
      a.x -= f.width / 2
  }
};
z.xv = function(a, b, c, d, f, g) {
  switch(d) {
    case 0:
      a.y -= f.height + g;
      break;
    case 2:
      a.y += g;
      break;
    case 1:
      a.y -= f.height / 2
  }
};
z.Qq = function(a, b) {
  switch(a) {
    case 0:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 7:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y + this.k.height;
      break;
    case 1:
      b.x = this.k.x;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 5:
      b.x = this.k.x + this.k.width;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 3:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y;
      break;
    case 8:
      b.x = this.k.x;
      b.y = this.k.y + this.k.height;
      break;
    case 2:
      b.x = this.k.x;
      b.y = this.k.y;
      break;
    case 6:
      b.x = this.k.x + this.k.width;
      b.y = this.k.y + this.k.height;
      break;
    case 4:
      b.x = this.k.x + this.k.width, b.y = this.k.y
  }
};
var Jg = 2;
function Kg(a) {
  switch(N(a, "align")) {
    case "inside":
      return 0;
    case "outside":
      return 1;
    default:
      return Jg
  }
}
function Ng(a, b, c) {
  var d = J(a, b);
  return 0 == d.indexOf("%") ? (a = c, -1 != d.indexOf("*") && (a *= Number(d.substr(d.indexOf("*") + 1))), a) : Qb(a, b)
}
function Og(a) {
  return 0 == ("" + a).indexOf("%")
}
function Pg(a) {
  a = "" + a;
  return-1 == a.indexOf("*") ? 1 : Number(a.substr(a.indexOf("*") + 1))
}
;function Qg() {
  Rg(this)
}
z = Qg.prototype;
z.XH = s();
z.tJ = x(q);
z.ld = p;
z.kd = p;
z.Td = q;
z.Nb = u("Td");
z.la = p;
z.pa = p;
z.pd = p;
z.od = p;
z.Wc = NaN;
z.xl = p;
z.je = NaN;
z.Wf = p;
z.kz = p;
z.gz = p;
z.Rf = p;
z.dn = p;
z.Td = p;
z.bD = u("Td");
z.vg = p;
z.ml = u("vg");
z.ef = p;
z.we = p;
z.fc = p;
z.If = p;
z.g = function(a) {
  if(C(a, "mode")) {
    switch(Wb(a, "mode")) {
      default:
      ;
      case "normal":
        this.vg = 0;
        break;
      case "stacked":
        this.vg = 1;
        break;
      case "percentstacked":
        this.vg = 2;
        break;
      case "overlay":
        this.vg = 3;
        break;
      case "sortedoverlay":
        this.vg = 4
    }
  }
  if(C(a, "minimum")) {
    var b = F(a, "minimum");
    Xb(b) ? this.pd = j : (this.pd = q, this.la = this.$i(b))
  }else {
    2 == this.vg && (this.pd = q, this.la = 0)
  }
  C(a, "maximum") ? (b = F(a, "maximum"), Xb(b) ? this.od = j : (this.od = q, this.pa = this.$i(b))) : 2 == this.vg && (this.od = q, this.pa = 100);
  C(a, "major_interval") && (b = F(a, "major_interval"), Xb(b) ? this.xl = j : (this.xl = q, this.Wc = Ob(b)));
  C(a, "minor_interval") && (b = F(a, "minor_interval"), Xb(b) ? this.Wf = j : (this.Wf = q, this.je = Ob(b)));
  C(a, "minimum_offset") && (this.kz = Sb(F(a, "minimum_offset")));
  C(a, "maximum_offset") && (this.gz = Sb(F(a, "maximum_offset")));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  C(a, "base_value") && (a = F(a, "base_value"), Xb(a) ? this.dn = j : (this.dn = q, this.Rf = this.$i(a)))
};
z.$i = function(a) {
  return Ob(a)
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 0, this.od = q, this.pa = 1);
  if(this.od || this.pd) {
    var a = this.kd - this.ld;
    this.pd && (this.la = 0 > this.ld || 0 <= this.ld - this.kz * a ? this.ld - this.kz * a : this.ld);
    this.od && (this.pa = 0 < this.kd || 0 >= this.kd + this.gz * a ? this.kd + this.gz * a : this.kd);
    this.pa <= this.la && (this.od ? this.pa = this.la + 1 : this.pd && (this.la = this.pa - 1))
  }
};
function Rg(a) {
  a.la = 0;
  a.pa = 1;
  a.Rf = 0;
  a.kz = 0.1;
  a.gz = 0.1;
  a.Td = q;
  a.kd = NaN;
  a.ld = NaN;
  a.pd = j;
  a.od = j;
  a.xl = j;
  a.Wf = j;
  a.dn = j;
  a.vg = 0
}
z.contains = function(a) {
  var b = this.la, c = this.pa, d = a;
  0 == d && (d = b);
  0 == d && (d = c);
  var f = Math.pow(10, Math.max(6, Math.floor(Math.log(Math.abs(d)) / Math.log(10)) + 3));
  return(a > b || Math.abs(a - b) <= Math.abs(d) / f) && (a < c || Math.abs(a - c) <= Math.abs(d) / f)
};
z.Vy = function(a) {
  return this.contains(a)
};
z.rd = function(a, b) {
  var c = b ? this.we - this.ef : this.we, d = b ? 0 : this.ef, f = (this.we - this.ef) * ((a - this.la) / (this.pa - this.la));
  return this.Td ? c - f : d + f
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((a - this.la) / (this.pa - this.la));
  return this.Td ? this.we - a : this.ef + a
};
z.Nc = function(a) {
  return(10 * this.Rf + 10 * a * this.Wc) / 10
};
z.Rq = function(a) {
  return this.Nc(a)
};
z.kk = function(a, b) {
  return(10 * a + 10 * b * this.je) / 10
};
function Tg(a) {
  a.fc = parseInt((a.pa - a.Rf) / a.Wc + 0.01);
  0 > a.fc && (a.fc = 0)
}
z.kq = function() {
  Tg(this);
  this.If = parseInt(this.Wc / this.je + 0.01);
  0 > this.If && (this.If = 0)
};
z.Vj = function() {
  this.dn || (this.Rf = 1)
};
function Ug(a, b) {
  if(0 == a) {
    return 0
  }
  var c = a / b;
  return b * (c - Math.floor(c))
}
function Vg(a, b, c) {
  a /= b;
  b = Math.floor(Math.log(a) / Math.LN10);
  b = Math.pow(10, b);
  c = c ? Math.ceil(a / b) : Math.floor(Number(a / b + 0.5));
  5 < c ? c = 10 : 2 < c ? c = 5 : 1 < c && (c = 2);
  return c * b
}
function Wg(a, b) {
  var c = Math.max(a / 7, Math.pow(10, -b)), d = Math.floor(Math.log(c) / Math.LN10), d = Math.pow(10, d), c = Math.floor(Number(c / d + 0.5));
  5 < c ? c = 10 : 2 < c ? c = 5 : 1 < c && (c = 2);
  return c * d
}
z.Wa = s();
function Xg() {
}
function Yg(a, b, c) {
  a = new Date(a);
  switch(b) {
    case 0:
      b = Math.floor(c);
      a.setUTCFullYear(a.getUTCFullYear() + b);
      c != b && rc(a, 12 * (c - b));
      break;
    case 1:
      rc(a, c);
      break;
    case 2:
      tc(a, c);
      break;
    case 3:
      uc(a, c);
      break;
    case Xg.eP:
      vc(a, c);
      break;
    case 5:
      a.setUTCSeconds(a.getUTCSeconds() + c)
  }
  return a.getTime()
}
function Zg(a, b, c) {
  var d = new Date(b), f = c ? 1 : 0, g = d.getUTCFullYear(), k = d.getUTCMonth(), l = d.getUTCDate(), n = d.getUTCHours(), m = d.getUTCMinutes(), d = d.getUTCSeconds(), o = 1 == l, v = 0 == n, w = 0 == m, y = 0 == d;
  switch(a) {
    default:
    ;
    case 0:
      return c && 0 == k && o && v && w && y ? b : (new Date(Date.UTC(g + f, 0))).getTime();
    case 1:
      return c && o && v && w && y ? b : (new Date(Date.UTC(g, k + f))).getTime();
    case 2:
      return c && v && w && y ? b : (new Date(Date.UTC(g, k, l + f))).getTime();
    case 3:
      return c && w && y ? b : (new Date(Date.UTC(g, k, l, n + f))).getTime();
    case 4:
      return c && y ? b : (new Date(Date.UTC(g, k, l, n, m + f))).getTime();
    case 5:
      return(new Date(Date.UTC(g, k, l, n, m, d + f))).getTime()
  }
}
function $g(a, b) {
  var c = a.pa - a.la, d;
  15768E7 < c ? (c = ah(a, c, b), a.Wf && (a.Gh = 0, 1 == c ? a.je = 0.25 : (Wg(c), a.je = h)), d = c) : 31536E6 < c ? (d = ah(a, c, b), a.Wf && (a.Gh = 1, a.je = Math.ceil(c / (3 * b) / 2592E6), 6 < a.je ? a.je = 12 : 3 < a.je && (a.je = 6))) : 7776E6 < c ? (a.Qg = 1, c = Math.ceil(c / b / 2592E6), a.Wf && (a.Gh = 2, a.je = 7.5 * c), d = c) : 864E6 < c ? (c = bh(a, c, b), a.Wf && (a.Gh = 2, d = c / 4, a.je = 12 < d ? 12 : 6 < d ? 6 : 3 < d ? 3 : 2 < d ? 2 : 1), d = c) : 2592E5 < c ? (d = bh(a, c, 
  b), a.Wf && (a.Gh = 3, c = Math.ceil(24 * (c / (3 * b))), a.je = 6 < c ? 12 : 3 < c ? 6 : 1)) : 36002880 < c ? (c = ch(a, c, b), c = 12 < c ? 24 : 6 < c ? 12 : 2 < c ? 6 : 1 < c ? 2 : 1, a.Wf && (a.Gh = 3, a.je = 1 >= c ? 0.25 : 6 >= c ? 1 : 12 >= c ? 2 : 4), d = c) : 108E5 < c ? (d = ch(a, c, b), a.Wf && (a.Gh = 4, a.je = dh(Math.ceil(60 * (c / (3 * b)))))) : 599616 < c ? (c = eh(a, c, b), c = dh(c), a.Wf && (a.Gh = 4, a.je = 1 >= c ? 0.25 : 5 >= c ? 1 : 5), d = c) : 179971.2 < c ? (d = eh(a, 
  c, b), a.Wf && (a.Gh = 5, c = Math.ceil(86400 * (c / (3 * b))), c = dh(c), a.je = c)) : 2592E5 < c && (a.Qg = 5, c = Math.ceil(86400 * (c / b) / 864E5), c = dh(c), a.Wf && (a.Gh = 5, a.je = 1 >= c ? 0.25 : 5 >= c ? 1 : 5), d = c);
  return d
}
function ah(a, b, c) {
  a.Qg = 0;
  return a = Math.ceil(b / c / 31536E6)
}
function bh(a, b, c) {
  a.Qg = 2;
  return a = Math.ceil(b / c / 864E5)
}
function ch(a, b, c) {
  a.Qg = 3;
  return a = Math.ceil(b / c / 864E5)
}
function eh(a, b, c) {
  a.Qg = 4;
  return a = Math.ceil(1440 * (b / c) / 864E5)
}
function dh(a) {
  return 15 < a ? 30 : 5 < a ? 15 : 1 < a ? 5 : 1
}
;function fh(a) {
  this.Cl = a
}
z = fh.prototype;
z.Cl = p;
z.Aa = p;
z.qb = t("Aa");
z.Ib = u("Aa");
z.bB = p;
z.setYear = t("bB");
z.lz = p;
z.setMonth = t("lz");
z.Cq = p;
z.No = p;
z.Vu = p;
z.uv = p;
z.LA = p;
z.wt = p;
z.Ym = p;
z.Ji = p;
z.wy = p;
function gh(a, b) {
  for(var c = b.length, d = 0;d < c;d++) {
    var f = Cb(b[d]);
    if(a.Aa + f.length <= a.Ji.length && a.Ji.substr(Cb(a.Aa)) == f) {
      return d
    }
  }
  return-1
}
function hh(a) {
  var b = a.Ji.charAt(a.Aa);
  for(a.Aa++;2 > b.length && a.Aa < a.Ji.length && !isNaN(Number(a.Ji.charAt(a.Aa)));) {
    b += a.Ji.charAt(a.Aa), a.Aa++
  }
  return Math.floor(Number(b))
}
function ih(a, b) {
  var c = Math.floor(Number(a.Ji.substr(a.Aa, b)));
  a.Aa += b;
  return c
}
function jh(a, b, c) {
  b = Cb(b);
  c = Cb(c);
  a.Ji.substr(Cb(a.Aa)) == b ? (a.wy = j, a.Aa += b.length) : a.Ji.substr(Cb(a.Aa)) == c && (a.wy = q, a.Aa += c.length)
}
z.clear = function() {
  this.Vu = this.Ym = this.No = this.wt = this.Cq = this.lz = this.bB = -1;
  this.LA = this.uv = NaN;
  this.wy = j
};
var kh = {u:function(a) {
  for(var b = "", c = a.Ib(), d = a.Ji;c < d.length && !isNaN(Number(d.charAt(c)));) {
    b += d.charAt(c), c++
  }
  a.qb(c);
  a.LA = Number(b)
}, d:function(a) {
  var b = hh(a);
  a.Cq = b
}, dd:function(a) {
  var b = ih(a, 2);
  a.Cq = b
}, ddd:function(a) {
  var b = gh(a, a.CE.qA[h]);
  a.wt = b;
  a.Aa += a.CE.aS[b].length
}, dddd:function(a) {
  var b = gh(a, a.CE.YA[h]);
  a.wt = b;
  a.Aa += a.CE.qS[b].length
}, M:function(a) {
  a.setMonth(hh(a) - 1)
}, MM:function(a) {
  a.setMonth(ih(a, 2) - 1)
}, MMM:function(a) {
  var b = gh(a, a.Cl.pD());
  a.setMonth(b);
  a.Aa += a.Cl.pD[b].length
}, MMMM:function(a) {
  var b = gh(a, a.Cl.fD());
  a.setMonth(b);
  a.Aa += a.Cl.fD[b].length
}, y:function(a) {
  a.setYear(2E3 + hh(a))
}, yy:function(a) {
  a.setYear(2E3 + ih(a, 2))
}, yyyy:function(a) {
  a.setYear(ih(a, 4))
}, h:function(a) {
  var b = hh(a);
  a.Ym = b
}, hh:function(a) {
  var b = ih(a, 2);
  a.Ym = b
}, H:function(a) {
  var b = hh(a);
  a.No = b
}, HH:function(a) {
  var b = ih(a, 2);
  a.No = b
}, m:function(a) {
  var b = hh(a);
  a.Vu = b
}, mm:function(a) {
  var b = ih(a, 2);
  a.Vu = b
}, s:function(a) {
  var b = hh(a);
  a.uv = b
}, ss:function(a) {
  var b = ih(a, 2);
  a.uv = b
}, t:function(a) {
  jh(a, a.Cl.oA, a.Cl.pA)
}, tt:function(a) {
  jh(a, a.Cl.zw, a.Cl.Fz)
}};
function lh() {
}
z = lh.prototype;
z.pB = p;
z.el = p;
z.gv = p;
z.p = function(a) {
  this.pB = [];
  this.el = [];
  this.gv = new fh(a);
  a = a.$y.split("%");
  this.el = [];
  this.el.push(a[0].length);
  for(var b = a.length, c = 1;c < b;c++) {
    for(var d = ("" + a[c]).charAt(0), f = d, g = ("" + a[c]).length, k = 1;k < g && ("" + a[c]).charAt(k) == d;k++) {
      f += d
    }
    (d = kh[f]) ? (this.pB.push(d), this.el.push(("" + a[c]).substring(k).length)) : this.el[this.el.length - 1] += ("" + a[c]).length
  }
};
z.getDate = function(a) {
  this.gv.clear();
  this.gv.Ji = a;
  var a = this.gv, b = this.pB, c = this.el, d = a.Ji.length, f = b.length;
  a.Aa = c[0];
  for(var g = 0;g < f && !(b[g](a), a.Aa += c[g + 1], a.Aa >= d);g++) {
  }
  b = this.gv;
  if(isNaN(b.LA)) {
    if(-1 == b.No && -1 != b.Ym && (b.Ym = Math.floor(b.Ym % 12), b.No = b.wy ? b.Ym : 12 + b.Ym), a = new Date(Date.UTC(-1 == b.bB ? 1970 : b.bB, -1 == b.lz ? 0 : b.lz, -1 == b.Cq ? 1 : b.Cq, -1 == b.No ? 0 : b.No, -1 == b.Vu ? 0 : b.Vu, isNaN(b.uv) ? 0 : b.uv)), -1 != b.wt && -1 == b.Cq) {
      b = b.wt - a.getDay(), a.setUTCDate(a.getDate() + b + (0 > b ? 7 : 0))
    }
  }else {
    a = new Date(1E3 * b.LA)
  }
  return a
};
function mh() {
  this.IE = "January,February,March,April,May,June,July,August,September,October,November,December".split(",");
  this.PF = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",");
  this.zw = "AM";
  this.Fz = "PM";
  this.oA = "A";
  this.pA = "P";
  this.YA = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(",");
  this.qA = "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(",");
  this.$y = "%yyyy/%M/%d %h:%m:%s";
  this.JA = {};
  this.aF = new lh
}
z = mh.prototype;
z.IE = p;
z.fD = function(a) {
  return this.IE[a]
};
z.PF = p;
z.pD = function(a) {
  return this.PF[a]
};
z.zw = p;
z.oA = p;
z.Fz = p;
z.pA = p;
z.YA = p;
z.qA = p;
z.$y = p;
z.aF = p;
z.g = function(a) {
  if(C(a, "months")) {
    var b = F(a, "months");
    C(b, "names") && (this.IE = J(b, "names").split(","));
    C(b, "short_names") && (this.PF = J(b, "short_names").split(","))
  }
  if(C(a, "week_days") && (b = F(a, "week_days"), C(b, "names") && (this.YA = J(b, "names").split(",")), C(b, "short_names"))) {
    this.qA = J(b, "short_names").split(",")
  }
  if(C(a, "time") && (b = F(a, "time"), C(b, "am_string") && (this.zw = F(b, "am_string")), C(b, "short_am_string") && (this.oA = F(b, "short_am_string")), C(b, "pm_string") && (this.Fz = F(b, "pm_string")), C(b, "short_pm_string"))) {
    this.pA = F(b, "short_pm_string")
  }
  C(a, "format") && (this.$y = J(a, "format"))
};
z.JA = p;
z.p = function() {
  this.aF.p(this)
};
function nh(a, b) {
  if(a.JA[b]) {
    return a.JA[b]
  }
  var c = a.aF.getDate(b).getTime();
  return a.JA[b] = c
}
function oh() {
}
z = oh.prototype;
z.so = ".";
z.Nn = "";
z.bE = j;
z.g = function(a) {
  C(a, "decimal_separator") && (this.so = F(a, "decimal_separator").toString());
  C(a, "thousands_separator") && (this.Nn = F(a, "thousands_separator").toString());
  this.bE = "." == this.so && "" == this.Nn
};
z.clear = function() {
  this.so = ".";
  this.Nn = "";
  this.bE = j
};
z.ta = function(a) {
  this.bE || (a = ("" + a).split(this.Nn).join(""), a = a.split(this.so).join("."));
  return a = Number(a)
};
function ph(a) {
  Qg.apply(this);
  this.w = a;
  this.Ev()
}
A.e(ph, Qg);
z = ph.prototype;
z.Ra = p;
z.Jb = u("Ra");
z.w = p;
z.g = function(a) {
  ph.f.g.call(this, a)
};
z.mI = function(a) {
  return a.getName()
};
function qh(a) {
  ph.call(this, a);
  this.sA = q
}
A.e(qh, ph);
z = qh.prototype;
z.Ev = function() {
  this.Ra = 4
};
z.xJ = q;
z.SJ = j;
z.g = function(a) {
  qh.f.g.call(this, a);
  C(a, "auto_calculation_mode") && ("smart" == N(a, "auto_calculation_mode") ? this.zu = j : this.zu = q);
  C(a, "always_show_zero") && (K(a, "always_show_zero") ? this.sA = j : this.sA = q)
};
z.Ae = function() {
  if(this.pd || this.od || this.xl || this.Wf) {
    qh.f.Ae.call(this);
    if(this.pd || this.od) {
      0 == Number(this.kd - this.ld) && this.pd && this.od && (this.la = this.ld - 0.5, this.pa = this.kd + 0.5);
      if(this.pa - this.la < Number.MIN_VALUE && (this.od && (this.pa += 0.2 * (0 == this.pa ? 1 : Math.abs(this.pa))), this.pd)) {
        this.la -= 0.2 * (0 == this.la ? 1 : Math.abs(this.la))
      }
      this.pd && 0 < this.la && 0.25 > this.la / (this.pa - this.la) && (this.la = 0);
      this.od && 0 > this.pa && 0.25 > Math.abs(this.pa / (this.pa - this.la)) && (this.pa = 0);
      if(this.sA && (this.pd && 0 < this.la && (this.la = 0), this.od && 0 > this.pa)) {
        this.pa = 0
      }
    }
    if(this.xl && (this.Wc = this.zu ? Wg(this.pa - this.la, this.Ih) : Vg(this.pa - this.la, 7, q), rh(this.w) && !this.w.Qs())) {
      this.Vj();
      Tg(this);
      var a = this.w.O.Ys(this.w.aa.r());
      a <= (this.pa - this.la) / this.Wc && (this.Wc = Vg(this.pa - this.la, a, j));
      this.xJ = j
    }
    this.Wf && (this.je = Vg(this.Wc, 5, j));
    this.pd && (this.la -= Ug(this.la, this.Wc));
    this.od && (a = Ug(this.pa, this.Wc), 0 != a && (this.pa += this.Wc - a))
  }
  this.SJ && !this.xJ && rh(this.w) && sh(this.w);
  this.Vj();
  this.kq()
};
z.Vj = function() {
  this.dn && (this.Rf = Math.ceil(Number(this.la) / Number(this.Wc) - 1.0E-8) * Number(this.Wc))
};
z.Ih = p;
z.zu = q;
z.tJ = u("zu");
z.XH = function(a) {
  a != p && (this.zu = j, this.Ih = a)
};
z.sA = q;
function th(a) {
  ph.call(this, a)
}
A.e(th, ph);
th.prototype.Ev = function() {
  this.Ra = 1
};
th.prototype.Ae = function() {
  this.la = this.ld - 0.5;
  this.pa = this.kd + 0.5;
  this.je = 1;
  this.Rf = this.la;
  var a = q;
  this.xl ? rh(this.w) && !this.w.Qs() ? (this.Wc = 1, Tg(this), a = this.w.O.Ys(this.w.aa.r()), this.Wc = Math.ceil((this.pa - this.la) / a), a = j) : this.Wc = Math.floor(Number((this.pa - this.la - 1) / 12)) + 1 : (this.Wc = Math.floor(Number(this.Wc)), 0 >= this.Wc && (this.Wc = 1));
  this.Wf && (this.je = this.Wc);
  !a && rh(this.w) && sh(this.w);
  this.kq()
};
function uh(a) {
  ph.call(this, a)
}
A.e(uh, th);
uh.prototype.Ev = function() {
  this.Ra = 3
};
uh.prototype.$i = function(a) {
  return"" == "" + a ? NaN : nh(this.w.ca().Gd(), "" + a)
};
uh.prototype.mI = function(a) {
  a = a.getName();
  return nh(this.w.ca().Gd(), "" + a)
};
uh.prototype.Ow = function() {
  uh.f.Ow.call(this);
  if(0 != (this.pa - this.Rf) % this.Wc) {
    var a = this.Nc(this.fc);
    a >= this.la && a <= this.pa && this.fc++
  }
};
function vh(a) {
  ph.call(this, a)
}
A.e(vh, ph);
vh.prototype.Ev = function() {
  this.Ra = 0
};
var wh = [0, 0.301029995663981, 0.477121254719662, 0.602059991327962, 0.698970004336019, 0.778151250383644, 0.845098040014257, 0.903089986991944, 0.954242509439325, 1];
z = vh.prototype;
z.Mi = p;
z.Rn = p;
z.Jd = 10;
z.g = function(a) {
  if(a != p) {
    vh.f.g.call(this, a);
    var b = C(a, "logarithmic_base") ? "logarithmic_base" : "log_base";
    C(a, b) && (a = Cb(F(a, b)), this.Jd = "e" == a ? Math.E : Ob(a))
  }
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 1, this.od = q, this.pa = 10);
  vh.f.Ae.call(this);
  this.xl && (this.Wc = 1);
  0 >= this.la && 0 >= this.pa ? (this.la = 1, this.pa = this.Jd) : 0 > this.FE ? this.FE = this.BR / this.Jd : 0 == this.FE ? this.FE = 1 : 0 >= this.pa && (this.pa = this.la * this.Jd);
  if(1.0E-20 > this.pa - this.la && (this.od && (this.pa *= 2), this.pd)) {
    this.la /= 2
  }
  this.pd && (this.la = Math.pow(this.Jd, Math.floor(Math.log(this.la) / Math.log(this.Jd))));
  this.od && (this.pa = Math.pow(this.Jd, Math.ceil(Math.log(this.pa) / Math.log(this.Jd))));
  this.Mi = Math.round(1E3 * this.Zg(this.la)) / 1E3;
  this.Rn = Math.round(1E3 * this.Zg(this.pa)) / 1E3;
  rh(this.w) && sh(this.w);
  this.Vj();
  this.kq()
};
z.Nc = function(a) {
  return this.Rf + a
};
z.Rq = function(a) {
  return Math.pow(this.Jd, this.Nc(a))
};
z.kk = function(a, b) {
  return a + Math.floor(Number(b) / 9) + wh[(b + 9) % 9]
};
z.rd = function(a, b) {
  var c = (this.we - this.ef) * ((a - this.Mi) / (this.Rn - this.Mi)), d = b ? this.we - this.ef : this.we, f = b ? 0 : this.ef;
  return this.Td ? d - c : f + c
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((this.Zg(a) - this.Mi) / (this.Rn - this.Mi));
  return this.Td ? this.we - a : this.ef + a
};
z.Vj = function() {
  this.dn && (this.Rf = Math.ceil(this.Zg(this.la) - 1.0E-8))
};
z.kq = function() {
  this.fc = parseInt(Math.floor(this.Zg(this.pa) + 1.0E-12)) - parseInt(this.Rf);
  this.Nc(this.fc) > this.Zg(this.pa) && this.fc--;
  1 > this.fc && (this.fc = 1);
  this.If = 9
};
z.Zg = function(a) {
  return 1.0E-20 < a ? Math.log(a) / Math.log(this.Jd) : 0
};
z.Vy = function(a) {
  return a >= this.Mi && a <= this.Rn
};
z.eC = function(a) {
  return Math.pow(this.Jd, a)
};
function xh(a) {
  ph.call(this, a);
  this.Gh = this.Qg = 0
}
A.e(xh, ph);
z = xh.prototype;
z.Ev = function() {
  this.Ra = 2
};
z.Qg = p;
z.Gh = p;
z.g = function(a) {
  xh.f.g.call(this, a);
  if(a) {
    var b;
    C(a, "major_interval_unit") && (b = yh(F(a, "major_interval_unit")), -1 != b && (this.Qg = b));
    C(a, "minor_interval_unit") && (b = yh(F(a, "minor_interval_unit")), -1 != b && (this.Gh = b))
  }
};
function yh(a) {
  switch(Vb(a)) {
    case "year":
      return 0;
    case "month":
      return 1;
    case "day":
      return 2;
    case "hour":
      return 3;
    case "minute":
      return 4;
    case "second":
      return 5
  }
  return-1
}
z.$i = function(a) {
  return"" === "" + a ? NaN : nh(this.w.ca().Gd(), "" + a)
};
z.Ae = function() {
  xh.f.Ae.call(this);
  var a = q;
  this.xl && (this.Wc = $g(this, 7), this.w.O && !this.w.O.$n && (this.Vj(), this.Ow(), a = this.w.O.Ys(this.w.r()), a <= (this.pa - this.la) / this.Wc && (this.Wc = $g(this, a)), a = j));
  this.od && (this.pa = Zg(this.Qg, this.pa, j));
  this.pd && (this.la = Zg(this.Qg, this.la, q));
  this.Vj();
  this.Ow();
  this.If = 0;
  for(var b = this.Nc(0), c = this.Nc(1), d = this.kk(b, 0);d < c;) {
    this.If++, d = this.kk(b, this.If)
  }
  this.If = Math.max(0, this.If);
  !a && this.w.O && this.w.O.an(this.w.r())
};
z.Nc = function(a) {
  return Yg(this.Rf, this.Qg, a * this.Wc)
};
z.kk = function(a, b) {
  return Yg(a, this.Gh, b * this.je)
};
z.Vj = function() {
  if(this.dn) {
    var a = new Date(this.la), b;
    switch(this.Qg) {
      default:
      ;
      case 0:
        b = new Date(Date.UTC(a.getUTCFullYear(), 0));
        break;
      case 1:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth()));
        break;
      case 2:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()));
        break;
      case 3:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours()));
        break;
      case 4:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours(), a.getUTCMinutes()));
        break;
      case 5:
        b = a
    }
    this.Rf = b.getTime() < a.getTime() ? Yg(b.getTime(), this.Qg, 1) : b.getTime()
  }
};
z.Ow = function() {
  var a = new Date(this.la), b = new Date(this.pa), c = this.Wc, d = 1, d = b.getUTCFullYear() - a.getUTCFullYear();
  switch(this.Qg) {
    default:
    ;
    case 0:
      d /= c;
      break;
    case 1:
      d = (b.getUTCMonth() - a.getUTCMonth() + 12 * d) / c;
      break;
    case 2:
      d = (this.pa - this.la) / (864E5 * c);
      break;
    case 3:
      d = (this.pa - this.la) / (36E5 * c);
      break;
    case 4:
      d = (this.pa - this.la) / (6E4 * c);
      break;
    case 5:
      d = (this.pa - this.la) / (1E3 * c)
  }
  1 > d && (d = 1);
  this.fc = Math.floor(d)
};
function zh() {
  qh.call(this);
  this.SJ = q
}
A.e(zh, qh);
zh.prototype.BE = aa();
zh.prototype.eC = aa();
function Ah() {
  Rg(this);
  this.Jd = 10
}
A.e(Ah, Qg);
z = Ah.prototype;
z.Mi = p;
z.Rn = p;
z.Jd = 10;
z.g = function(a) {
  a && (Ah.f.g.call(this, a), C(a, "log_base") && (a = N(a, "log_base"), this.Jd = "e" == a ? Math.E : Ob(a)))
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 1, this.od = q, this.pa = 10);
  Ah.f.Ae.call(this);
  this.xl && (this.Wc = 1);
  0 >= this.la && 0 >= this.pa ? (this.la = 1, this.pa = this.Jd) : 0 >= this.la ? this.la = this.pa / this.Jd : 0 >= this.pa && (this.pa = this.la * this.Jd);
  if(1.0E-20 > this.pa - this.la && (this.od && (this.pa *= 2), this.pd)) {
    this.la /= 2
  }
  this.pd && (this.la = Math.pow(this.Jd, Math.floor(Math.log(this.la) / Math.log(this.Jd))));
  this.od && (this.pa = Math.pow(this.Jd, Math.ceil(Math.log(this.pa) / Math.log(this.Jd))));
  this.Mi = Math.round(1E3 * this.Zg(this.la)) / 1E3;
  this.Rn = Math.round(1E3 * this.Zg(this.pa)) / 1E3;
  this.Vj();
  this.kq()
};
z.kq = function() {
  this.fc = parseInt(Math.floor(this.Zg(this.pa) + 1.0E-12)) - parseInt(this.Rf);
  this.Nc(this.fc) > this.Zg(this.pa) && this.fc--;
  1 > this.fc && (this.fc = 1);
  this.If = 9
};
z.Vj = function() {
  this.dn && (this.Rf = Math.ceil(this.Zg(this.la) - 1.0E-8))
};
z.Nc = function(a) {
  return this.Rf + a
};
z.kk = function(a, b) {
  return a + Math.floor(Number(b) / 9) + wh[(b + 9) % 9]
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((this.Zg(a) - this.Mi) / (this.Rn - this.Mi));
  return this.Td ? this.we - a : this.ef + a
};
z.rd = function(a, b) {
  var c = (this.we - this.ef) * ((a - this.Mi) / (this.Rn - this.Mi)), d = b ? this.we - this.ef : this.we, f = b ? 0 : this.ef;
  return this.Td ? d - c : f + c
};
z.Jd = p;
z.BE = function(a) {
  return this.Zg(a)
};
z.Zg = function(a) {
  return 1.0E-20 < a ? Math.log(a) / Math.log(this.Jd) : 0
};
z.eC = function(a) {
  return Math.pow(this.Jd, a)
};
function Bh() {
  this.Y = new Nd;
  this.Ln = new jg
}
z = Bh.prototype;
z.Y = p;
z.md = u("Y");
z.Ia = 0;
z.ga = u("Ia");
z.za = Jg;
z.Ar = 0.05;
z.li = u("Ar");
z.ab = 0.05;
z.Ka = u("ab");
z.Cj = t("ab");
z.ob = 0.05;
z.vb = u("ob");
z.Ql = t("ob");
z.Bb = 0;
z.Vl = t("Bb");
z.bo = j;
z.Yd = 0;
z.ol = u("Yd");
z.Lp = t("Yd");
z.Ln = p;
z.yo = function(a, b, c, d, f, g, k) {
  var l = new Md(a, a.ja(), a.ja()), n = new P(b, c, f, g), b = this.Ln.Io(this.Yd, a, d, b, c, f, g, Od(this.Y) ? this.Y.Mc().Va() : 1), c = "", c = Sd(this.Y) ? c + Jd(this.Y.mc(), a, n, k) : c + md(), c = Od(this.Y) ? c + Ld(this.Y.Mc(), a, n, k) : c + nd();
  l.Hg.setAttribute("style", c);
  c = "";
  c = this.Y.Pc && this.Y.Pc.isEnabled() ? c + Hc(this.background.De(), a, p, k) : c + od();
  l.Mg.setAttribute("style", c);
  l.Hg.setAttribute("d", b);
  l.Mg.setAttribute("d", b);
  return l
};
z.g = function(a, b) {
  this.Y.g(a);
  if(this.Y.isEnabled() && (C(a, "padding") && (this.Ia = Qb(a, "padding")), C(a, "length") && (this.Ar = Ng(a, "length", b)), C(a, "width") && (this.ab = Ng(a, "width", b)), C(a, "shape") && (this.Yd = ig(N(a, "shape"))), C(a, "align") && (this.za = Kg(a)), C(a, "rotation") && (this.Bb = M(a, "rotation")), C(a, "auto_rotate") && (this.bo = K(a, "auto_rotate")), 105 == this.Yd || 106 == this.Yd || 107 == this.Yd || 108 == this.Yd || (this.Ar = this.ab = Math.min(this.ab, this.Ar)), 108 == this.Yd)) {
    this.Y.Hb = p, this.Y.Pc = p
  }
};
function Ch(a, b) {
  Ig.call(a, b)
}
A.e(Ch, Ig);
function Dh() {
}
A.e(Dh, Of);
Dh.prototype.update = function(a, b) {
  b && this.hd();
  Eh(this);
  Dh.f.update.call(this, a, b)
};
function Eh(a) {
  var b = a.j, c = b.ka().wa.Mb(), d = new O(0, 0);
  if(c) {
    d = c.ej();
    c.QC && d.Mp((c.Dy ? b.w.ec() : 1) * c.QC);
    d.uc(b.r(), d.eg);
    var f = b.w.fa().pc(isNaN(c.ta()) ? a.ZC() : c.ta()), g = new O;
    a.Jg(g, f, c.za, c.ga());
    a.vc || (a.vc = c.ej().ic(b.r()), b.fb().ia(a.vc));
    d = d.update(b.r(), a.vc, 0, 0, b.w.Zc().n().width, b.w.Zc().n().height, b.Ba())
  }
  a.vc && a.vc.qb(g.x + d.x, g.y + d.y)
}
Dh.prototype.Jg = function(a, b, c, d) {
  var f = this.j.w;
  f instanceof Fh ? Gh(f, f.og(c, d, 0), b, a) : f.Zc().Vf() ? (a.x = b, a.y = f.Ib(c, d, 0)) : (a.y = b, a.x = f.Ib(c, d, 0))
};
function Hh(a, b) {
  Ch.call(a, b);
  this.wA = 0.5;
  this.Dx = 0.2;
  this.Au = this.wu = q
}
A.e(Hh, Ch);
z = Hh.prototype;
z.wA = p;
z.Dx = p;
z.Au = p;
z.wu = p;
z.Ea = p;
z.Mb = u("Ea");
z.g = function(a) {
  var a = Hh.f.g.call(this, a), b;
  C(a, "start_size") && (b = J(a, "start_size"), this.wA = (this.Au = Og(b)) ? Pg(b) : Ob(b) / 100);
  C(a, "end_size") && (b = J(a, "end_size"), this.Dx = (this.wu = Og(b)) ? Pg(b) : Ob(b) / 100);
  Ub(a, "label") && (this.Ea = new Ih, this.Ea.g(F(a, "label")));
  return a
};
function Jh() {
}
A.e(Jh, Dh);
Jh.prototype.hd = function() {
  var a = this.j, b = a.w, c = b.fa(), d = a.be(), f = a.n(), g = c.pc(a.Tb), a = c.pc(a.Lb);
  this.Zl = new O;
  this.zo = new O;
  this.Op = new O;
  this.Ao = new O;
  var c = (d.Au ? b.ec() : 1) * d.wA, k = (d.wu ? b.ec() : 1) * d.Dx;
  b.Ul(Kh(b, d.za, d.ga(), c, j), g, this.Zl);
  b.Ul(Kh(b, d.za, d.ga(), k, j), a, this.zo);
  b.Ul(Kh(b, d.za, d.ga(), c, q), g, this.Op);
  b.Ul(Kh(b, d.za, d.ga(), k, q), a, this.Ao);
  f.x = Math.min(this.Zl.x, this.zo.x, this.Op.x, this.Ao.x);
  f.y = Math.min(this.Zl.y, this.zo.y, this.Op.y, this.Ao.y);
  f.width = Math.max(this.Zl.x, this.zo.x, this.Op.x, this.Ao.x) - f.x;
  f.height = Math.max(this.Zl.y, this.zo.y, this.Op.y, this.Ao.y) - f.y
};
Jh.prototype.ZC = function() {
  var a = this.j;
  return(a.Tb + a.Lb) / 2
};
Jh.prototype.createElement = function() {
  return this.j.r().ja()
};
Jh.prototype.Ta = function(a) {
  var b = S(this.Zl.x, this.Zl.y), b = b + U(this.zo.x, this.zo.y), b = b + U(this.Ao.x, this.Ao.y), b = b + U(this.Op.x, this.Op.y), b = b + U(this.Zl.x, this.Zl.y);
  a.setAttribute("d", b)
};
function Lh() {
  $.call(this)
}
A.e(Lh, Qf);
Lh.prototype.cc = function() {
  return Hh
};
Lh.prototype.lc = function() {
  return new Jh
};
function Mh() {
}
A.e(Mh, Dh);
Mh.prototype.createElement = function() {
  return this.j.r().ja()
};
Mh.prototype.hd = s();
Mh.prototype.Ta = function(a) {
  var b = this.j, c = b.w, d = c.fa(), f = b.be();
  b.n();
  var g = (f.Au ? c.size : 1) * f.wA, k = (f.wu ? c.size : 1) * f.Dx, l = Nh(c, f.za, f.ga(), g, j), n = Nh(c, f.za, f.ga(), k, j), g = Nh(c, f.za, f.ga(), g, q), f = Nh(c, f.za, f.ga(), k, q), k = d.pc(b.Tb), d = d.pc(b.Lb), b = b.r(), m = c.Zc().oc, o = "", v = 1, w = c.fa().Nb() ? 0 : 1;
  180 > Math.abs(k - d) && (v = 0);
  if(l == n) {
    var n = m.x + l * Math.cos(k * Math.PI / 180), y = m.y + l * Math.sin(k * Math.PI / 180), E = m.x + l * Math.cos(d * Math.PI / 180), V = m.y + l * Math.sin(d * Math.PI / 180), o = o + S(n, y), o = o + b.ub(l, v, w, E, V, l)
  }else {
    y = new O, Gh(c, l, k, y), o += S(y.x, y.y), o += Oh(c, k, d, l, n)
  }
  f == g ? (n = m.x + f * Math.cos(k * Math.PI / 180), y = m.y + f * Math.sin(k * Math.PI / 180), E = m.x + f * Math.cos(d * Math.PI / 180), V = m.y + f * Math.sin(d * Math.PI / 180), o += U(E, V), o += b.ub(f, v, !w, n, y, f)) : o += Oh(c, d, k, f, g);
  a.setAttribute("d", o)
};
function Oh(a, b, c, d, f) {
  var g = "", k, l, n = (f - d) / (c - b), m = new O;
  if(c > b) {
    for(k = b;k <= c;k++) {
      l = d + n * (k - b), Gh(a, l, k, m), g += U(m.x, m.y)
    }
  }else {
    for(k = b;k >= c;k--) {
      l = d + n * (k - b), Gh(a, l, k, m), g += U(m.x, m.y)
    }
  }
  Gh(a, f, c, m);
  return g += U(m.x, m.y)
}
function Ph() {
  $.call(this)
}
A.e(Ph, Qf);
Ph.prototype.cc = function() {
  return Hh
};
Ph.prototype.lc = function() {
  return new Mh
};
function Qh() {
  Ch.call(this)
}
A.e(Qh, Ch);
z = Qh.prototype;
z.Ea = p;
z.Mb = u("Ea");
z.ke = p;
z.ec = u("ke");
z.rE = p;
z.g = function(a) {
  a = Qh.f.g.call(this, a);
  if(C(a, "size")) {
    var b = J(a, "size");
    this.ke = (this.rE = Og(b)) ? Pg(b) : Pb(b)
  }
  Ub(a, "line") && (this.sc = new Kd, this.sc.g(F(a, "line")));
  C(a, "label") && (this.Ea = new Ih, this.Ea.g(F(a, "label")));
  this.Pc = this.Hb = p
};
function Rh() {
}
A.e(Rh, Dh);
Rh.prototype.Tb = p;
Rh.prototype.Lb = p;
Rh.prototype.hd = function() {
  var a = this.j, b = a.w, c = b.fa();
  this.Tb = new O;
  this.Lb = new O;
  var c = c.pc(a.ta()), d = a.be(), f = (d.rE ? b.ec() : 1) * d.ec();
  if(b instanceof Fh) {
    var g = Nh(b, d.za, d.ga(), f, q);
    Gh(b, Nh(b, d.za, d.ga(), f, j), c, this.Tb);
    Gh(b, g, c, this.Lb)
  }else {
    b.Ul(Kh(b, d.za, d.ga(), f, j), c, this.Tb), b.Ul(Kh(b, d.za, d.ga(), f, q), c, this.Lb)
  }
  a = a.n();
  a.x = Math.min(this.Tb.x, this.Lb.x);
  a.y = Math.min(this.Tb.y, this.Lb.y);
  a.width = Math.max(this.Tb.x, this.Lb.x) - a.x;
  a.height = Math.max(this.Tb.y, this.Lb.y) - a.y
};
Dh.prototype.ZC = function() {
  return this.j.ta()
};
Rh.prototype.update = function(a, b) {
  b && this.hd();
  Rh.f.update.call(this, a, b)
};
Rh.prototype.createElement = function() {
  return this.j.r().ja()
};
Rh.prototype.Ta = function(a) {
  var b = S(this.Tb.x, this.Tb.y), b = b + U(this.Lb.x, this.Lb.y);
  a.setAttribute("d", b)
};
function Sh() {
  $.call(this)
}
A.e(Sh, Qf);
Sh.prototype.cc = function() {
  return Qh
};
Sh.prototype.lc = function() {
  return new Rh
};
function Th() {
  Ch.call(this)
}
A.e(Th, Ch);
Th.prototype.Ea = p;
Th.prototype.Mb = u("Ea");
Th.prototype.lh = p;
Th.prototype.g = function(a) {
  a = Th.f.g.call(this, a);
  C(a, "label") && (this.Ea = new Ih, this.Ea.g(F(a, "label")));
  C(a, "tickmark") && (this.lh = new Uh, this.lh.g(F(a, "tickmark")));
  return a
};
function Vh() {
}
A.e(Vh, Dh);
z = Vh.prototype;
z.hs = p;
z.Yc = function() {
  var a = this.j;
  a.ka().wa.lh && (this.hs = new W(a.r()))
};
z.ff = function() {
  this.hs && this.j.fb().ia(this.hs)
};
z.update = function() {
  Eh(this);
  this.hs && (this.hs.clear(), this.hs.ia(this.j.w.yo(this.j.ka().wa.lh, this.j.w.fa().pc(this.j.ta()), this.j.Ba().Ba())))
};
z.ZC = function() {
  return this.j.ta()
};
function Wh() {
  $.call(this)
}
A.e(Wh, Qf);
Wh.prototype.cc = function() {
  return Th
};
Wh.prototype.lc = function() {
  return new Vh
};
var zg = 0, Ag = 1, yg = 2;
function Bf() {
  ze(this);
  this.Lo = 2;
  this.Xp = 1;
  this.Ez = zg;
  this.Ia = 0;
  this.oE = q
}
A.e(Bf, ye);
z = Bf.prototype;
z.Lo = p;
z.Xp = p;
z.Ez = p;
z.iD = u("Ez");
z.Ub = p;
z.Xd = t("Ub");
z.ma = p;
z.gd = t("ma");
z.oE = p;
z.fK = NaN;
z.xg = NaN;
z.cs = p;
z.Ia = p;
z.eg = p;
z.g = function(a) {
  Lb(a, "width");
  Bf.f.g.call(this, a);
  if(C(a, "position")) {
    var b = F(a, "position");
    C(b, "halign") && (this.Lo = vd(N(b, "halign")));
    C(b, "valign") && (this.Xp = yd(N(b, "valign")));
    if(C(b, "placement_mode")) {
      var c;
      a: {
        switch(N(b, "placement_mode")) {
          default:
          ;
          case "bypoint":
            c = zg;
            break a;
          case "byrectangle":
            c = Ag;
            break a;
          case "byanchor":
            c = yg
        }
      }
      this.Ez = c
    }
    C(b, "x") && (this.Ub = Qb(b, "x"));
    C(b, "y") && (this.ma = Qb(b, "y"));
    C(b, "width") && (this.fK = Qb(b, "width"));
    C(b, "height") && (this.xg = Qb(b, "height"));
    C(b, "spread") && (this.cs = K(b, "spread"));
    C(b, "padding") && (this.Ia = M(b, "padding"))
  }
  C(a, "format") && (this.eg = J(a, "format"))
};
z.update = function(a, b, c, d, f, g, k, l) {
  var c = isNaN(this.Ub) ? c : (this.oE ? this.Ub : this.Ub * f) + c, d = isNaN(this.ma) ? d : (this.oE ? this.ma : this.ma * g) + d, n = c, m = d, o = this.k.width, v = this.k.height, f = this.fK * f, g = this.xg * g;
  isNaN(f) && (f = this.nf.width);
  isNaN(g) && (g = this.nf.height);
  switch(this.Ez) {
    case zg:
    ;
    case yg:
      switch(this.Lo) {
        case 2:
          n -= f / 2;
          c -= o / 2;
          break;
        case 0:
          this.cs ? (n -= f + this.Ia, c += -f - this.Ia + (f - o) / 2) : c -= o + this.Ia;
          break;
        case 1:
          this.cs ? (n += this.Ia, c += this.Ia + (f - o) / 2) : c += this.Ia
      }
      switch(this.Xp) {
        case 0:
          this.cs ? (m -= g + this.Ia, d += -g - this.Ia + (g - v) / 2) : d -= v + this.Ia;
          break;
        case 1:
          m -= g / 2;
          d -= v / 2;
          break;
        case 2:
          this.cs ? (m += this.Ia, d += this.Ia + (g - v) / 2) : d += this.Ia
      }
      break;
    case Ag:
      switch(this.Lo) {
        case 0:
          c += this.Ia;
          break;
        case 2:
          c += (f - o) / 2;
          break;
        case 1:
          c += f - o - this.Ia
      }
      switch(this.Xp) {
        case 0:
          d += this.Ia;
          break;
        case 1:
          d += (g - v) / 2;
          break;
        case 2:
          d += g - v - this.Ia
      }
  }
  this.cs && (this.Y && this.Y.Oa && (o = this.Y.Oa, f += o.tb() + o.Ja(), g += o.rb() + o.ra(), this.Y.fb().qb(n - c, m - d)), this.nf.width = f, this.nf.height = g);
  Bf.f.update.call(this, a, b, k, l);
  b.qb(c, d);
  return new O(c, d)
};
z.aI = function(a) {
  a.Vl(this.Bb.bc, this.nf.width / 2, this.nf.height / 2)
};
function Xh() {
}
z = Xh.prototype;
z.Xa = p;
z.Ba = u("Xa");
z.K = p;
z.ka = u("K");
z.w = p;
z.wj = t("w");
z.Zc = function() {
  return this.w.Zc()
};
z.$a = p;
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.k = p;
z.n = u("k");
z.g = function(a, b) {
  this.Xa = C(a, "color") ? Yb(a) : zb("0xCCCCCC", h);
  this.K = this.kC(a, b)
};
z.kC = function(a, b) {
  var c = Ue(b, this.Rb(), a, J(a, "style")), d = this.Qb(a);
  d.g(c);
  return d
};
z.p = function(a) {
  this.k = new P;
  this.D = new W(a);
  this.$a = this.K.p(this);
  return this.D
};
z.be = function() {
  return this.K.wa
};
z.qa = function() {
  this.$a.update(this.be(), j)
};
z.Wa = function() {
  this.$a.update(p, j)
};
z.Gc = function(a, b) {
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
function Ih() {
  this.za = Jg;
  this.Ia = 0;
  this.Sc = new ye;
  this.Dy = q
}
z = Ih.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.Sc = p;
z.ej = u("Sc");
z.QC = p;
z.Dy = p;
z.bb = NaN;
z.ta = u("bb");
z.g = function(a) {
  C(a, "align") && (this.za = Kg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  if(C(a, "font")) {
    var b = F(a, "font");
    C(b, "size") && (b = J(b, "size"), Lb(a, "size"), this.QC = (this.Dy = Og(b)) ? Pg(b) : 100 * Pb(b))
  }
  C(a, "value") && (this.bb = M(a, "value"));
  this.Sc = new Bf;
  this.Sc.g(a)
};
function Yh() {
}
A.e(Yh, Xh);
z = Yh.prototype;
z.Tb = p;
z.Lb = p;
z.Qb = function() {
  switch(this.Zc().aD()) {
    case 2:
      return new Lh;
    case 1:
      return new Ph
  }
};
z.Rb = x("color_range_style");
z.g = function(a, b, c) {
  Yh.f.g.call(this, a, b, c);
  C(a, "start") && (this.Tb = M(a, "start"));
  C(a, "end") && (this.Lb = M(a, "end"))
};
z.vq = function(a) {
  this.Gc(this.Tb, a);
  this.Tb = this.Tb > a.pa ? a.pa : this.Tb < a.la ? a.la : this.Tb;
  this.Gc(this.Lb, a);
  this.Lb = this.Lb > a.pa ? a.pa : this.Lb < a.la ? a.la : this.Lb
};
z.ta = function() {
  return(this.Tb + this.Lb) / 2
};
function Zh() {
}
A.e(Zh, Xh);
z = Zh.prototype;
z.bb = p;
z.ta = u("bb");
z.Rb = x("trendline_style");
z.Qb = function() {
  return new Sh
};
z.g = function(a, b, c) {
  Zh.f.g.call(this, a, b, c);
  C(a, "value") && (this.bb = M(a, "value"));
  (a = this.be().Mc()) && a.se() && this.Zc().Me(a.yb)
};
z.vq = function(a) {
  this.Gc(this.bb, a)
};
function Uh() {
  Bh.call(this)
}
A.e(Uh, Bh);
Uh.prototype.qd = p;
Uh.prototype.vl = p;
Uh.prototype.g = function(a) {
  Uh.f.g.call(this, a);
  var b;
  C(a, "length") && (b = J(a, "length"), this.Ar = (this.qd = Og(b)) ? Pg(b) : Pb(b));
  C(a, "width") && (b = J(a, "width"), this.ab = (this.vl = Og(b)) ? Pg(b) : Pb(b))
};
function $h() {
}
A.e($h, Xh);
z = $h.prototype;
z.bb = p;
z.ta = u("bb");
z.Rb = x("custom_label_style");
z.Qb = function() {
  return new Wh
};
z.g = function(a, b) {
  $h.f.g.call(this, a, b);
  var c = this.K.wa.lh;
  c && ai(this.Zc(), c.md());
  C(a, "value") && (this.bb = M(a, "value"))
};
z.p = function(a) {
  $h.f.p.call(this, a);
  return this.D
};
z.vq = function(a) {
  this.Gc(this.bb, a)
};
function bi() {
  this.nn = []
}
z = bi.prototype;
z.w = p;
z.wj = t("w");
z.La = p;
z.nn = p;
z.D = p;
z.g = function(a, b, c) {
  var d, f, g;
  if(C(a, "color_ranges")) {
    f = I(F(a, "color_ranges"), "color_range");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && ci(this, new Yh, f[d], b, c)
    }
  }
  if(C(a, "trendlines")) {
    f = I(F(a, "trendlines"), "trendline");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && ci(this, new Zh, f[d], b, c)
    }
  }
  if(C(a, "custom_labels")) {
    f = I(F(a, "custom_labels"), "custom_label");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && ci(this, new $h, f[d], b, c)
    }
  }
};
function ci(a, b, c, d, f) {
  b.wj(a.w);
  b.g(c, d, f);
  b.vq(a.La);
  a.nn.push(b)
}
z.p = function(a) {
  this.D = new W(a);
  for(var b = this.nn.length, c = 0;c < b;c++) {
    this.D.ia(this.nn[c].p(a))
  }
  return this.D
};
z.qa = function() {
  for(var a = this.nn.length, b = 0;b < a;b++) {
    this.nn[b].qa()
  }
};
z.Wa = function() {
  for(var a = this.nn.length, b = 0;b < a;b++) {
    this.nn[b].Wa()
  }
};
function di(a) {
  this.Wb = a
}
z = di.prototype;
z.Wb = p;
z.Zc = u("Wb");
z.Sa = p;
z.Rl = t("Sa");
z.AD = function() {
  return this.Sa != p && 0 < this.Sa.length
};
z.getName = u("Sa");
z.Fp = p;
z.Bo = p;
z.enabled = j;
z.size = 0;
z.ec = u("size");
z.lv = p;
z.scale = p;
z.fa = u("scale");
z.gp = p;
z.pp = p;
z.bf = p;
z.cd = p;
z.Uu = p;
z.Lu = p;
z.vk = p;
z.Sg = p;
z.p = function(a) {
  this.cd = new W(a);
  this.Fp && this.cd.ia(this.Fp.p(a));
  this.Bo && this.cd.ia(this.Bo.p(a));
  this.pp && (this.Uu = new W(a), this.cd.ia(this.Uu));
  this.gp && (this.Lu = new W(a), this.cd.ia(this.Lu));
  this.bf && this.bf.isEnabled() && (this.vk = new W(a), this.cd.ia(this.vk));
  this.Sg && this.cd.ia(this.Sg.p(a));
  return this.cd
};
z.jr = s();
z.qa = function() {
  this.Fp && this.Fp.qa();
  this.Bo && this.Bo.qa();
  this.pp && this.uC();
  this.gp && this.tC();
  if(this.bf && this.bf.isEnabled()) {
    for(var a = this.scale.fc, a = this.bf.Jv ? a : a - 1, b = this.cd.r(), c = this.bf.Iv ? 0 : 1;c <= a;c++) {
      this.am = this.scale.Nc(c);
      var d = this.bf;
      d.Sc.uc(b, d.yc.ta(d.w, p));
      d = this.Jg(this.scale.rd(this.am));
      this.vk.ia(this.bf.qa(b, d))
    }
  }
  this.Sg && this.Sg.qa()
};
z.uC = function() {
  for(var a = this.scale.BE(this.scale.la), b = this.scale.BE(this.scale.pa), c = 0;c <= this.scale.fc;c++) {
    for(var d = this.scale.Nc(c), f = 1;f < this.scale.If;f++) {
      var g = this.scale.kk(d, f);
      g < a || g > b || this.Uu.ia(this.yo(this.pp, this.scale.rd(g)))
    }
  }
};
z.tC = function() {
  for(var a = 0;a <= this.scale.fc;a++) {
    this.Lu.ia(this.yo(this.gp, this.scale.rd(this.scale.Nc(a))))
  }
};
z.Na = function(a) {
  if("%Value" == a) {
    return this.scale.eC(this.am)
  }
};
z.Pa = function(a) {
  return"%Value" == a ? 2 : 4
};
z.Wa = function() {
  this.jr();
  this.Lu && this.Lu.clear();
  this.Uu && this.Uu.clear();
  this.vk && this.vk.clear();
  this.qa();
  this.Sg && this.Sg.Wa()
};
z.g = function(a, b) {
  if(a) {
    if(C(a, "enabled") && (this.enabled = K(a, "enabled")), C(a, "size") && (this.size = Qb(a, "size")), C(a, "scale") ? (this.scale = C(F(a, "scale"), "type") && "logarithmic" == N(F(a, "scale"), "type") ? new Ah : new zh, this.scale.g(F(a, "scale"))) : this.scale = new zh, this.enabled) {
      C(a, "scale_bar") && (this.Fp = new ei(this, q), this.Fp.g(F(a, "scale_bar"), this.size), ai(this.Wb, this.Fp.md())), C(a, "scale_line") && (this.Bo = new ei(this, j), this.Bo.g(F(a, "scale_line"), this.size), ai(this.Wb, this.Bo.md())), C(a, "major_tickmark") && (this.gp = new Bh, this.gp.g(F(a, "major_tickmark"), this.size), ai(this.Wb, this.gp.md())), C(a, "minor_tickmark") && (this.pp = new Bh, this.pp.g(F(a, "minor_tickmark"), this.size), ai(this.Wb, this.pp.md())), this.bf = new fi(this), 
      C(a, "labels") && this.bf.g(F(a, "labels"), this.size), this.Sg = new bi, this.Sg.wj(this), this.Sg.La = this.scale, this.Sg.g(a, b, this.size)
    }
  }else {
    this.scale = new zh
  }
};
z.$w = function(a) {
  gi(this, a.ta());
  a.wM && gi(this, a.wM())
};
function gi(a, b) {
  if(isNaN(a.scale.ld) || b < a.scale.ld) {
    a.scale.ld = b
  }
  if(isNaN(a.scale.kd) || b > a.scale.kd) {
    a.scale.kd = b
  }
}
function ei(a, b) {
  this.w = a;
  this.eI = b;
  this.Y = new Nd
}
z = ei.prototype;
z.w = p;
z.eI = q;
z.Yd = 1;
z.ol = u("Yd");
z.ke = 0.05;
z.ec = u("ke");
z.Ia = 0;
z.ga = u("Ia");
z.za = Jg;
z.Y = p;
z.md = u("Y");
z.D = p;
z.p = function(a) {
  this.D = new Md(a, a.ja(), a.ja());
  this.D.G.setAttribute("id", "scaleBar");
  return this.D
};
z.qa = function() {
  var a = this.w.mD(this), b = this.D.r(), c = "", c = Sd(this.Y) ? c + Jd(this.Y.mc(), b, a) : c + md(), c = Od(this.Y) ? c + Ld(this.Y.Mc(), b, a) : c + nd();
  this.D.Hg.setAttribute("style", c);
  c = "";
  c = this.Y.Pc && this.Y.Pc.isEnabled() ? c + Hc(this.Y.De(), b, a) : c + od();
  this.D.Mg.setAttribute("style", c);
  a = this.w.CI(b, this);
  this.D.Hg.setAttribute("d", a);
  this.D.Mg.setAttribute("d", a)
};
z.g = function(a, b) {
  if(this.eI) {
    this.Yd = 0, Sd(this.Y) && this.Y.mc().zg(q), this.Y.g({border:a})
  }else {
    if(C(a, "shape")) {
      switch(N(a, "shape")) {
        case "line":
          this.Yd = 0;
          break;
        case "bar":
          this.Yd = 1
      }
    }
    this.Y.g(a)
  }
  C(a, "size") && (this.ke = Ng(a, "size", b));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "align") && (this.za = Kg(a))
};
function fi(a) {
  this.Sc = new ye;
  this.Sc.zg(q);
  this.w = a
}
z = fi.prototype;
z.za = 0;
z.Ia = 0.02;
z.ga = u("Ia");
z.Iv = j;
z.Jv = j;
z.w = p;
z.Sc = p;
z.ej = u("Sc");
z.isEnabled = function() {
  return this.Sc && this.Sc.isEnabled()
};
z.yc = p;
z.g = function(a, b) {
  if(C(a, "font")) {
    var c = F(a, "font");
    C(c, "size") && H(c, "size", "" + 100 * Ng(c, "size", b))
  }
  this.Sc.g(a);
  C(a, "enabled") && this.Sc.zg(K(a, "enabled"));
  C(a, "format") && (this.yc = new ie(J(a, "format")));
  C(a, "align") && (this.za = Kg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "show_first") && (this.Iv = K(a, "show_first"));
  C(a, "show_last") && (this.Jv = K(a, "show_last"))
};
z.qa = function(a, b) {
  var c = this.Sc.ic(a);
  c.qb(b.x, b.y);
  return c
};
function hi() {
  Cg.call(this);
  this.Pj = [];
  this.sB = {};
  this.zi = [];
  this.iK = {};
  this.Ea = new xg;
  this.Ea.$z(this);
  this.Fc = new Bg
}
A.e(hi, Cg);
z = hi.prototype;
z.Pj = p;
z.sB = p;
z.zi = p;
z.iK = p;
z.Ea = p;
z.Fc = p;
z.mb = function(a) {
  a = hi.f.mb.call(this, a);
  a.zi = [];
  for(var b = 0;b < this.zi.length;b++) {
    a.zi.push(this.zi[b].mb())
  }
  return a
};
function ii(a, b) {
  a.Pj.push(b);
  b.AD() && (a.sB[b.getName()] = b)
}
z.g = function(a) {
  hi.f.g.call(this, a);
  var b, c;
  b = this.UB(F(a, "axis"), this.tf);
  b.Rl("default");
  b.g(F(a, "axis"), this.tf);
  ii(this, b);
  var d = b;
  if(C(a, "extra_axes")) {
    var f = I(F(a, "extra_axes"), "axis");
    for(c = 0;c < f.length;c++) {
      b = this.UB(f[c], this.tf), b.g(f[c], this.tf), C(f[c], "name") && b.Rl(N(f[c], "name")), ii(this, b)
    }
  }
  if(C(a, "pointers")) {
    c = F(a, "pointers");
    this.Ea.Ue(F(c, "label"), this.tf);
    this.Fc.Ue(F(c, "tooltip"), this.tf);
    a = p;
    b = -1;
    var f = q, g = j;
    C(c, "color") && (a = Yb(c));
    C(c, "hatch_type") && (b = Gc(N(c, "hatch_type")));
    C(c, "allow_select") && (f = K(c, "allow_select"));
    C(c, "use_hand_cursor") && (g = K(c, "use_hand_cursor"));
    var k = I(c, "pointer");
    for(c = 0;c < k.length;c++) {
      var l = k[c];
      if(C(l, "value") || C(l, "start") && C(l, "end")) {
        var n = this.tH(Wb(l, "type"));
        if(n) {
          n.$z(this);
          n.vv = f;
          n.Tn = g;
          var m = this.Ea.copy();
          n.Ea = m;
          m = this.Fc.copy();
          n.Fc = m;
          a && n.Di(a);
          n.Ip(b);
          n.g(l, this.tf);
          C(l, "axis") ? (l = this.sB[N(l, "axis")]) ? n.wj(l) : n.wj(d) : n.wj(d);
          this.zi.push(n);
          n.AD() && (this.iK[n.getName()] = n);
          this.$w(n);
          n.w.$w(n)
        }
      }
    }
  }
};
z.$w = s();
z.p = function(a, b, c) {
  hi.f.p.call(this, a, b, c);
  b = 0;
  for(c = this.Pj.length;b < c;b++) {
    this.yB.ia(this.Pj[b].p(this.Bg))
  }
  b = 0;
  for(c = this.zi.length;b < c;b++) {
    this.ix.ia(this.zi[b].p(a))
  }
  b = 0;
  for(c = this.Pj.length;b < c;b++) {
    this.Pj[b].jr()
  }
  return this.cd
};
z.qa = function() {
  hi.f.qa.call(this);
  var a, b;
  a = 0;
  for(b = this.Pj.length;a < b;a++) {
    this.Pj[a].qa()
  }
  a = 0;
  for(b = this.zi.length;a < b;a++) {
    this.zi[a].qa()
  }
};
z.Wa = function(a) {
  hi.f.Wa.call(this, a);
  var b, a = 0;
  for(b = this.Pj.length;a < b;a++) {
    this.Pj[a].Wa()
  }
  a = 0;
  for(b = this.zi.length;a < b;a++) {
    this.zi[a].Wa()
  }
};
function ai(a, b) {
  b && (Sd(b) && b.mc().yb && a.Me(b.mc().yb), Od(b) && b.Mc().yb && a.Me(b.Mc().yb))
}
z.Me = s();
function ji(a) {
  kg.call(this, a)
}
A.e(ji, kg);
z = ji.prototype;
z.mp = NaN;
z.Rh = NaN;
z.TD = NaN;
z.Dw = function(a) {
  var b = 0;
  this.Uc && this.Uc.re() && (b = this.Uc.Va());
  var c = 0;
  this.Vd && this.Vd.re() && (c = this.Vd.Va());
  this.mp = Math.min(a.width, a.height);
  this.TD = b;
  this.Rh = b + c;
  b = (b + c + this.padding) * this.mp;
  a.Ca();
  a.bh(a.tb() + b);
  a.eh(a.Ja() - b);
  a.fh(a.rb() + b);
  a.$g(a.ra() - b)
};
z.rH = function() {
  return new ki(this)
};
z.sH = function() {
  return new li(this)
};
z.nH = function() {
  return new mi(this)
};
z.ja = function(a, b) {
  var c;
  c = this.ym ? this.ym.Ca() : new Cd;
  var d = this.mp / 100;
  c.P *= d;
  c.sa *= d;
  c.ha *= d;
  c.X *= d;
  return Dd(c, a, b)
};
function ni(a) {
  this.frame = a
}
A.e(ni, eg);
ni.prototype.frame = p;
ni.prototype.pC = function(a, b, c, d) {
  var f = p, f = p, g = this.Ut(a, d);
  this.mc() && this.mc().isEnabled() ? (f = Jd(this.mc(), a, d), f += "fill-rule: evenodd") : f = md();
  b.setAttribute("d", g);
  b.setAttribute("style", f);
  b.setAttribute("fill-rule", "evenodd");
  this.De() && this.De().isEnabled() ? (f = Hc(this.De(), a, d), f += "fill-rule: evenodd") : f = od();
  c.setAttribute("d", g);
  c.setAttribute("style", f);
  c.setAttribute("fill-rule", "evenodd")
};
function oi(a, b) {
  var c = b.Ca(), d = a.tI();
  c.x += d;
  c.y += d;
  c.width -= 2 * d;
  c.height -= 2 * d;
  return c
}
function pi(a, b) {
  var c = b.Ca(), d = a.yI();
  c.x += d;
  c.y += d;
  c.width -= 2 * d;
  c.height -= 2 * d;
  return c
}
function gg(a, b, c) {
  c = oi(a, c);
  return a.frame.ja(b, c, j, j)
}
function hg(a, b, c) {
  c = pi(a, c);
  return a.frame.ja(b, c, j, j)
}
ni.prototype.Ut = function(a, b) {
  var c = oi(this, b), d = pi(this, b);
  return this.frame.ja(a, d, j, q) + " " + this.frame.ja(a, c, q, j)
};
function ki(a) {
  this.frame = a
}
A.e(ki, ni);
ki.prototype.tI = function() {
  return-this.frame.padding * this.frame.mp
};
ki.prototype.yI = function() {
  return-(this.frame.TD + this.frame.padding) * this.frame.mp
};
function li(a) {
  this.frame = a
}
A.e(li, ni);
li.prototype.tI = function() {
  return-(this.frame.TD + this.frame.padding) * this.frame.mp
};
li.prototype.yI = function() {
  return-(this.frame.Rh + this.frame.padding) * this.frame.mp
};
function mi(a) {
  this.Oc = a
}
A.e(mi, Td);
mi.prototype.Oc = p;
mi.prototype.createElement = function(a) {
  return a.ja()
};
mi.prototype.yh = function(a, b) {
  return this.Oc.ja(a, b)
};
function qi() {
  Ig.call(this)
}
A.e(qi, Ig);
z = qi.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.ds = p;
z.g = function(a) {
  a = qi.f.g.call(this, a);
  if(C(a, "width")) {
    var b = J(a, "width");
    this.ab = (this.qd = Og(b)) ? Pg(b) : Pb(b)
  }
  C(a, "start_from_zero") && (this.ds = K(a, "start_from_zero"));
  return a
};
function ri() {
}
A.e(ri, Of);
ri.prototype.update = function(a, b) {
  b && this.j.hd();
  ri.f.update.call(this, a, b)
};
ri.prototype.createElement = function() {
  return this.j.r().ja()
};
ri.prototype.Ta = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function si() {
  $.call(this)
}
A.e(si, Qf);
si.prototype.cc = function() {
  return qi
};
si.prototype.lc = function() {
  return new ri
};
function ti(a) {
  this.ob = this.ab = 0.1;
  this.Yd = 108;
  this.za = Jg;
  this.Ia = 0;
  a && (this.Y = new Nd);
  this.vl = this.qd = q;
  this.Bb = 0;
  this.bo = j
}
z = ti.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.ob = p;
z.vb = u("ob");
z.vl = p;
z.Yd = p;
z.Bb = p;
z.bo = p;
z.Y = p;
z.md = u("Y");
z.g = function(a) {
  C(a, "shape") && (this.Yd = ig(Wb(a, "shape")));
  C(a, "align") && (this.za = Kg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "rotation") && (this.Bb = M(a, "rotation"));
  C(a, "auto_rotate") && (this.bo = K(a, "auto_rotate"));
  var b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.qd = Og(b)) ? Pg(b) : Pb(b));
  C(a, "height") && (b = J(a, "height"), this.ob = (this.vl = Og(b)) ? Pg(b) : Pb(b));
  105 == this.Yd || 106 == this.Yd || 107 == this.Yd || 108 == this.Yd || (this.ab = this.ob = Math.min(this.ab, this.ob));
  this.Y && this.Y.g(a)
};
function ui() {
  Ig.call(this);
  this.gb = new ti(q)
}
A.e(ui, Ig);
ui.prototype.gb = p;
ui.prototype.g = function(a) {
  a = ui.f.g.call(this, a);
  this.gb.g(a);
  return a
};
function vi() {
  this.Ln = new jg
}
A.e(vi, Of);
z = vi.prototype;
z.Ln = p;
z.fb = function() {
  return this.j.sd
};
z.update = function(a, b) {
  this.j.hd();
  vi.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function(a) {
  a.setAttribute("d", this.Ln.Io(this.j.ya.gb.Yd, this.j.r(), Math.min(this.j.n().width, this.j.n().height), this.j.hp.x, this.j.hp.y, this.j.n().width, this.j.n().height, this.j.ya.Ld() ? this.j.ya.Mc().Va() : 1))
};
function wi() {
  $.call(this)
}
A.e(wi, Qf);
wi.prototype.cc = function() {
  return ui
};
wi.prototype.lc = function() {
  return new vi
};
function xi() {
  Ig.call(this);
  this.lo = 0.15;
  this.Uw = 0;
  this.So = this.qd = j;
  this.lo = this.ab = 1
}
A.e(xi, Ig);
z = xi.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.lo = p;
z.So = p;
z.Uw = p;
z.WG = p;
z.g = function(a) {
  var a = xi.f.g.call(this, a), b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.qd = Og(b)) ? Pg(b) : Ob(b) / 100);
  C(a, "bulb_radius") && (b = J(a, "bulb_radius"), this.lo = (this.So = Og(b)) ? Pg(b) : Ob(b) / 100);
  C(a, "bulb_padding") && (this.Uw = Qb(a, "bulb_padding"));
  Ub(a, "blub") && (this.WG = new Nd, this.WG.g(F(a, "blub")));
  return a
};
function yi() {
}
A.e(yi, Of);
yi.prototype.update = function(a, b) {
  if(b) {
    var c = this.j, d = c.ya, f = (d.qd ? c.w.ec() : 1) * d.Ka(), g = (d.So ? c.w.ec() : 1) * d.lo, k = Kh(c.w, d.za, d.ga(), f, j), l = Kh(c.w, d.za, d.ga(), f, q), f = c.w.fa().pc(c.w.fa().la), n = c.w.fa().pc(Mg(c, c.bb)), m = c.w.rj, g = m * g, d = m * d.Uw, m = f, o = new P;
    c.w.Zc().Vf() ? (o.y = (k + l) / 2 - g, o.height = 2 * g, c.w.fa().Nb() ? (o.x = n, o.width = m + 2 * g + d - o.x) : (o.x = m - 2 * g - d, o.width = n - o.x)) : (o.x = (k + l) / 2 - g, o.width = 2 * g, c.w.fa().Nb() ? (o.y = n, o.height = m + 2 * g - o.y) : (o.y = m - 2 * g, o.height = n - o.y));
    f = c.w.fa().Nb() ? f + d : f - d;
    c.Oc.Vf() ? (d = new O(n, k), n = new O(n, l), l = new O(f, l), k = new O(f, k)) : (d = new O(k, n), n = new O(l, n), l = new O(l, f), k = new O(k, f));
    f = S(d.x, d.y);
    f += U(n.x, n.y);
    f += U(l.x, l.y);
    f += c.r().ub(g, 1, 1, k.x, k.y, g);
    f += U(k.x, k.y);
    f += U(d.x, d.y);
    c.k.x = Math.min(d.x, n.x, l.x, k.x);
    c.k.y = Math.min(d.y, n.y, l.y, k.y);
    c.k.width = Math.max(d.x, n.x, l.x, k.x) - c.k.x;
    c.k.height = Math.max(d.y, n.y, l.y, k.y) - c.k.y;
    g = f;
    c.MK = g
  }
  yi.f.update.call(this, a, b)
};
yi.prototype.createElement = function() {
  return this.j.r().ja()
};
yi.prototype.Ta = function(a) {
  a.setAttribute("d", this.j.MK)
};
function zi() {
  $.call(this)
}
A.e(zi, Qf);
zi.prototype.cc = function() {
  return xi
};
zi.prototype.lc = function() {
  return new yi
};
function Ai() {
  Ig.call(this);
  this.ab = 0.1;
  this.qd = q
}
A.e(Ai, Ig);
z = Ai.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.Xa = "%color";
z.EB = p;
z.Sj = p;
z.Si = p;
z.AB = p;
z.Rj = p;
z.Ri = p;
z.Hv = p;
z.g = function(a) {
  a = Ai.f.g.call(this, a);
  if(C(a, "width")) {
    var b = J(a, "width");
    this.ab = (this.qd = Og(b)) ? Pg(b) : Pb(b)
  }
  C(a, "color") && (this.Xa = N(a, "color"));
  this.EB = Bi("0xFFFFFF", 0.3);
  this.AB = Bi(this.Xa, 1);
  this.Hv = new Kd;
  this.Hv.g({thickness:"3", color:"0xFFFFFF", opacity:"0.3"});
  this.Sj = Ci(0.3);
  this.Rj = Ci(1);
  this.Si = Di("0xFFFFFF", 0.3);
  this.Ri = Di(this.Xa, 1);
  return a
};
function Bi(a, b) {
  var c = "DarkColor(" + a + ")", d = "LightColor(" + a + ")", f = new Id;
  f.g({type:"gradient", gradient:{key:[{color:c, position:"0", opacity:b}, {color:c, position:"0.05", opacity:b}, {color:d, position:"0.85", opacity:b}, {color:d, position:"0.85", opacity:b}, {color:c, position:"1", opacity:b}]}});
  return f
}
function Ci(a) {
  var b = new Id;
  b.g({type:"gradient", gradient:{key:[{color:"0xFFFFFF", position:"0", opacity:0}, {color:"0xFFFFFF", position:"0.2", opacity:Number(160 / 255) * a}, {color:"0xFFFFFF", position:"0.25", opacity:Number(140 / 255) * a}, {color:"0xFFFFFF", position:"0.3", opacity:Number(30 / 255) * a}, {color:"0xFFFFFF", position:"0.35", opacity:0}, {color:"0xFFFFFF", position:"1", opacity:0}]}});
  return b
}
function Di(a, b) {
  var c = new Id;
  c.g({type:"gradient", gradient:{key:[{color:"0xFFFFFF", position:"0", opacity:0.1 * b}, {color:"DarkColor(" + a + ")", position:"1", opacity:b}]}});
  return c
}
function Ei() {
  Ai.call(this)
}
A.e(Ei, Ai);
Ei.prototype.g = function(a) {
  a = Ei.f.g.call(this, a);
  this.AB.yb.Wd(-90);
  this.EB.yb.Wd(-90);
  this.Rj.yb.Wd(-90);
  this.Sj.yb.Wd(-90);
  this.Ri.yb.Wd(140);
  this.Si.yb.Wd(140);
  return a
};
function Fi() {
  Ai.call(this)
}
A.e(Fi, Ai);
Fi.prototype.g = function(a) {
  a = Fi.f.g.call(this, a);
  this.Ri.yb.Wd(50);
  this.Si.yb.Wd(50);
  return a
};
function Gi() {
}
A.e(Gi, Qe);
z = Gi.prototype;
z.gq = p;
z.Si = p;
z.Sj = p;
z.hq = p;
z.dq = p;
z.Ri = p;
z.Rj = p;
z.eq = p;
z.fq = p;
z.Yc = function() {
  this.gq = new Oe;
  this.gq.p(this.j, this, this.fl);
  this.gq.Xq = function(a) {
    return a.EB
  };
  this.gq.Ta = function() {
    this.G.setAttribute("d", this.j.au(this.j.lm))
  };
  this.hq = new Ne;
  this.hq.p(this.j, this, this.fl);
  this.hq.rD = function(a) {
    return a.Hv
  };
  this.hq.Ta = function() {
    this.G.setAttribute("d", this.j.tD(this.j.lm))
  };
  this.Sj = new Oe;
  this.Sj.p(this.j, this, this.fl);
  this.Sj.Xq = function(a) {
    return a.Sj
  };
  this.Sj.Ta = function() {
    this.G.setAttribute("d", this.j.au(this.j.lm))
  };
  this.Si = new Oe;
  this.Si.p(this.j, this, this.fl);
  this.Si.Xq = function(a) {
    return a.Si
  };
  this.Si.Ta = function() {
    this.G.setAttribute("d", this.j.sD(this.j.lm))
  };
  this.dq = new Oe;
  this.dq.p(this.j, this, this.fl);
  this.dq.Xq = function(a) {
    return a.AB
  };
  this.dq.Ta = function() {
    this.G.setAttribute("d", this.j.au(this.j.Uk))
  };
  this.eq = new Ne;
  this.eq.p(this.j, this, this.fl);
  this.eq.rD = function(a) {
    return a.Hv
  };
  this.eq.Ta = function() {
    this.G.setAttribute("d", this.j.tD(this.j.Uk))
  };
  this.Rj = new Oe;
  this.Rj.p(this.j, this, this.fl);
  this.Rj.Xq = function(a) {
    return a.Rj
  };
  this.Rj.Ta = function() {
    this.G.setAttribute("d", this.j.au(this.j.Uk))
  };
  this.fq = new Ne;
  this.fq.p(this.j, this, this.fl);
  this.fq.rD = function(a) {
    return a.Hv
  };
  this.fq.Ta = function() {
    this.G.setAttribute("d", this.j.lI(this.j.Uk))
  };
  this.Ri = new Oe;
  this.Ri.p(this.j, this, this.fl);
  this.Ri.Xq = function(a) {
    return a.Ri
  };
  this.Ri.Ta = function() {
    this.G.setAttribute("d", this.j.sD(this.j.Uk))
  }
};
z.fl = function() {
  return this.j.r().ja()
};
z.ff = function() {
  this.j.w.fa().Nb() ? (Hi(this), Ii(this)) : (Ii(this), Hi(this))
};
function Ii(a) {
  var b = a.j.fb();
  b.appendChild(a.dq.G);
  b.appendChild(a.eq.G);
  b.appendChild(a.fq.G);
  b.appendChild(a.Rj.G);
  b.appendChild(a.Ri.G)
}
function Hi(a) {
  var b = a.j.fb();
  b.appendChild(a.gq.G);
  b.appendChild(a.hq.G);
  b.appendChild(a.Sj.G);
  b.appendChild(a.Si.G)
}
z.update = function(a, b) {
  b && this.j.hd();
  this.gq.update(a, b);
  this.hq.update(a, b);
  this.Sj.update(a, b);
  this.Si.update(a, b);
  this.dq.update(a, b);
  this.eq.update(a, b);
  this.Rj.update(a, b);
  this.Ri.update(a, b);
  this.fq.update(a, b)
};
function Ji() {
}
A.e(Ji, Gi);
Ji.prototype.ff = function() {
  this.j.w.fa().Nb() ? (Hi(this), Ii(this)) : (Ii(this), Hi(this))
};
function Ki() {
}
A.e(Ki, Gi);
Ki.prototype.ff = function() {
  this.j.w.fa().Nb() ? (Ii(this), Hi(this)) : (Hi(this), Ii(this))
};
function Li() {
  $.call(this)
}
A.e(Li, $);
Li.prototype.cc = function() {
  return Ei
};
Li.prototype.lc = function() {
  return new Ji
};
function Mi() {
  $.call(this)
}
A.e(Mi, $);
Mi.prototype.cc = function() {
  return Fi
};
Mi.prototype.lc = function() {
  return new Ki
};
function Ni() {
  Lg.call(this)
}
A.e(Ni, Lg);
Ni.prototype.p = function(a) {
  Ni.f.p.call(this, a);
  this.PB();
  return this.D
};
Ni.prototype.PB = s();
function Oi() {
  Lg.call(this)
}
A.e(Oi, Ni);
z = Oi.prototype;
z.Rb = x("bar_pointer_style");
z.Qb = function() {
  return new si
};
z.p = function(a) {
  Oi.f.p.call(this, a);
  return this.D
};
z.hd = function() {
  var a = this.rI(), b = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), c = Kh(this.w, this.ya.za, this.ya.ga(), b, j), b = Kh(this.w, this.ya.za, this.ya.ga(), b, q), d = this.w.scale.pc(this.EI()), a = this.w.scale.pc(a);
  this.w.Zc().Vf() ? (this.k.y = Math.min(c, b), this.k.height = Math.max(c, b) - this.k.y, this.k.x = Math.min(d, a), this.k.width = Math.max(d, a) - this.k.x) : (this.k.x = Math.min(c, b), this.k.width = Math.max(c, b) - this.k.x, this.k.y = Math.min(d, a), this.k.height = Math.max(d, a) - this.k.y)
};
z.EI = function() {
  return this.ya.ds ? 0 : this.w.fa().la
};
z.rI = function() {
  return Mg(this, this.bb)
};
function Pi() {
  Lg.call(this)
}
A.e(Pi, Oi);
z = Pi.prototype;
z.Qd = p;
z.Rb = x("range_bar_pointer_style");
z.g = function(a, b) {
  Pi.f.g.call(this, a, b);
  C(a, "start") && (this.bb = M(a, "start"));
  C(a, "end") && (this.Qd = M(a, "end"))
};
z.EI = function() {
  return Mg(this, this.bb)
};
z.rI = function() {
  return Mg(this, this.Qd)
};
z.Na = function(a) {
  if("%Value" == a) {
    return this.Qd - this.bb
  }
  if("%StartValue" == a) {
    return this.bb
  }
  if("%EndValue" == a) {
    return this.Qd
  }
};
z.Pa = x(2);
function Qi() {
  Lg.call(this)
}
A.e(Qi, Ni);
z = Qi.prototype;
z.hp = p;
z.sd = p;
z.Rb = x("marker_pointer_style");
z.Qb = function() {
  return new wi
};
z.p = function(a) {
  this.sd = new W(a);
  Qi.f.p.call(this, a);
  this.D.ia(this.sd);
  return this.D
};
z.hd = function() {
  var a = this.ya.gb, b = this.w.rj, c = (a.vl ? this.w.ec() : 1) * a.vb() * b, d = (a.qd ? this.w.ec() : 1) * a.Ka() * b, b = this.Oc.Vf(), f = this.w.Ib(this.ya.za, this.ya.ga(), 0, q), g = new O;
  this.w.Ul(f, this.w.fa().pc(Mg(this, this.bb)), g);
  g.x -= c / 2;
  g.y -= d / 2;
  this.k.x = -c / 2;
  this.k.y = -d / 2;
  this.k.width = c;
  this.k.height = d;
  this.hp = new O(-c / 2, -d / 2);
  f = g.Ca();
  if(b) {
    this.sd.gd(g.y + d / 2);
    switch(this.ya.za) {
      case 0:
        var k = this.sd;
        k.ma -= d / 2;
        pd(k);
        break;
      case 1:
        k = this.sd, k.ma += d / 2, pd(k)
    }
    this.sd.Xd(g.x + c / 2)
  }else {
    this.sd.Xd(g.x + c / 2);
    switch(this.ya.za) {
      case 0:
        k = this.sd;
        k.Ub -= c / 2;
        pd(k);
        break;
      case 1:
        qd(this.sd, c / 2)
    }
    this.sd.gd(g.y + d / 2)
  }
  c = 0;
  if(a.bo) {
    switch(this.ya.za) {
      case 0:
      ;
      case Jg:
        c = b ? -180 : 90;
        break;
      case 1:
        c = b ? 0 : -90
    }
  }
  this.sd.Vl(c + a.Bb);
  this.k.x = f.x;
  this.k.y = f.y
};
function Ri() {
  Lg.call(this)
}
A.e(Ri, Ni);
Ri.prototype.Rb = x("thermometer_pointer_style");
Ri.prototype.Qb = function() {
  return new zi
};
Ri.prototype.MK = p;
Ri.prototype.PB = function() {
  var a = this.K.wa;
  Si(this, this.K.wa);
  Si(this, this.K.dc);
  Si(this, this.K.kc);
  Si(this, this.K.Yb);
  Si(this, this.K.gc);
  a = 2 * (a.So ? this.w.ec() : 1) * a.lo + a.Uw;
  this.w.fa().Nb() ? this.Oc.bj = Math.max(this.Oc.bj, a) : this.Oc.Ej = Math.max(this.Oc.Ej, a)
};
function Si(a, b) {
  var c = (b.qd ? a.w.ec() : 1) * b.Ka();
  if((b.So ? a.w.ec() : 1) * b.lo < c / 2) {
    b.lo = c / 2 / (b.So ? a.w.ec() : 1)
  }
}
function Ti() {
  Lg.call(this)
}
A.e(Ti, Ni);
Ti.prototype.Rb = x("tank_pointer_style");
Ti.prototype.lm = p;
Ti.prototype.Uk = p;
Ti.prototype.PB = function() {
  var a = 0.1488095238 * (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka();
  this.Oc.bj = Math.max(this.Oc.bj, a);
  this.Oc.Ej = Math.max(this.Oc.Ej, a)
};
function Ui() {
  Lg.call(this)
}
A.e(Ui, Ti);
z = Ui.prototype;
z.Qb = function() {
  return new Li
};
z.hd = function() {
  var a = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), b = Kh(this.w, this.ya.za, this.ya.ga(), a, j), c = this.w.fa().pc(this.w.fa().la), d = this.w.fa().pc(this.w.fa().pa), f = this.w.fa().pc(this.bb), a = a * this.w.rj;
  this.w.fa().Nb() ? (this.k = new P(f, b, c - f, a), this.Uk = new P(f, b, c - f, a), this.lm = new P(d, b, f - d, a)) : (this.k = new P(c, b, f - c, a), this.Uk = new P(c, b, f - c, a), this.lm = new P(f, b, d - f, a))
};
z.au = function(a) {
  var b = this.r(), c = 0.1488095238 * a.height, d = a.height / 2, f = a.y + a.height / 2 + Vc(d, 90), g = S(a.x + Uc(c, 90), f), g = g + b.ub(c, 1, 1, a.x, a.y, d), g = g + U(a.x + a.width, a.y);
  return g += b.ub(c, 0, 1, a.x + a.width, f, d)
};
z.tD = function(a) {
  var b = 0.1488095238 * a.height, c = a.height / 2, d = a.y + a.height / 2 + Vc(c, 90), f = S(a.x + a.width + 1.5, a.y + 1);
  return f += this.r().ub(b, 1, 0, a.x + a.width + 1.5, d, c)
};
z.lI = function(a) {
  var b = 0.1488095238 * a.height, c = a.height / 2, d = a.y + a.height / 2 + Vc(c, 90), f = S(a.x + 1, a.y);
  return f += this.r().ub(b, 0, 0, a.x, d, c)
};
z.sD = function(a) {
  var b = S(a.x + a.width - 1, a.y);
  return b += this.r().ub(0.1488095238 * a.height, 1, 0, a.x + a.width - 1 + 0.1, a.y, a.height / 2)
};
function Vi() {
  Lg.call(this)
}
A.e(Vi, Ti);
z = Vi.prototype;
z.Qb = function() {
  return new Mi
};
z.hd = function() {
  var a = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), b = Kh(this.w, this.ya.za, this.ya.ga(), a, j), c = this.w.fa().pc(this.w.fa().la), d = this.w.fa().pc(this.w.fa().pa), f = this.w.fa().pc(this.bb), a = a * this.w.rj;
  this.w.fa().Nb() ? (this.k = new P(b, f, a, c - f), this.Uk = new P(b, f, a, c - f), this.lm = new P(b, d, a, f - d)) : (this.k = new P(b, c, a, f - c), this.Uk = new P(b, c, a, f - c), this.lm = new P(b, f, a, d - f))
};
z.au = function(a) {
  var b = this.r(), c = S(a.x, a.y), c = c + b.ub(a.width / 2, 0, 1, a.x + a.width, a.y, 0.1488095238 * a.width), c = c + U(a.x + a.width, a.y + a.height);
  return c += b.ub(a.width / 2, 0, 1, a.x, a.y + a.height, 0.1488095238 * a.width)
};
z.tD = function(a) {
  var b = S(a.x, a.y);
  return b += this.r().ub(a.width / 2, 1, 0, a.x, a.y - 0.1, 0.1488095238 * a.width)
};
z.lI = function(a) {
  var b = S(a.x, a.y + a.height - 1);
  return b += this.r().ub(a.width / 2, 0, 0, a.x + a.width, a.y + a.height, 0.1488095238 * a.width)
};
z.sD = function(a) {
  var b = S(a.x, a.y);
  return b += this.r().ub(a.width / 2, 1, 0, a.x, a.y - 0.1, 0.1488095238 * a.width)
};
function Wi() {
  hi.call(this);
  this.oy = q;
  this.bj = this.Ej = 0
}
A.e(Wi, hi);
z = Wi.prototype;
z.oy = q;
z.Vf = u("oy");
z.Ej = 0;
z.bj = 0;
z.p = function(a, b, c) {
  Wi.f.p.call(this, a, b, c);
  return this.cd
};
z.g = function(a) {
  C(a, "orientation") && (this.oy = "horizontal" == N(a, "orientation"));
  Wi.f.g.call(this, a)
};
z.aD = x(2);
z.qH = function() {
  return new ji(this)
};
z.UB = function() {
  return new Xi(this)
};
z.tH = function(a) {
  switch(a) {
    case "bar":
      return new Oi;
    case "rangebar":
      return new Pi;
    case "marker":
      return new Qi;
    case "tank":
      return this.Vf() ? new Ui : new Vi;
    case "thermometer":
      return new Ri;
    default:
      return new Oi
  }
};
z.Me = function(a) {
  this.oy && a && a.Wd(a.ac + 90)
};
z.Rc = function(a) {
  Wi.f.Rc.call(this, a);
  this.frame.Dw(this.zb)
};
function Xi(a) {
  this.Wb = a
}
A.e(Xi, di);
z = Xi.prototype;
z.vp = NaN;
z.Aa = 0.5;
z.Ej = 0.05;
z.bj = 0.05;
z.g = function(a, b) {
  Xi.f.g.call(this, a, b);
  this.Wb.Vf() || (this.scale.Td = !this.scale.bD());
  Yi(this, this.gp);
  Yi(this, this.pp);
  C(a, "position") && (this.Aa = Qb(a, "position"));
  C(a, "start_margin") && (this.Ej = Qb(a, "start_margin"));
  C(a, "end_margin") && (this.bj = Qb(a, "end_margin"))
};
function Yi(a, b) {
  if(b) {
    if(a.Wb.Vf()) {
      var c = b.li();
      b.Ar = b.Ka();
      b.Cj(c)
    }else {
      108 == b.ol() && b.Lp(5)
    }
  }
}
z.jr = function() {
  Xi.f.jr.call(this);
  var a = this.Wb.Vf(), b = this.Wb.n();
  this.Xy = a ? b.width : b.height;
  this.FJ = a ? b.height : b.width;
  this.rj = Math.min(b.width, b.height);
  this.vp = (a ? b.y : b.x) + this.FJ * this.Aa;
  var c = this.Xy, d = this.Ej * c, c = this.bj * c, f = this.rj, d = Math.max(d, this.Wb.Ej * f), c = Math.max(c, this.Wb.bj * f);
  this.mv = this.size * f;
  a ? (d += b.x, c = b.Ja() - c) : (d += b.y, c = b.ra() - c);
  a = this.scale;
  a.ef = d;
  a.we = c;
  this.scale.Ae()
};
z.mv = NaN;
z.rj = NaN;
z.Xy = NaN;
z.FJ = NaN;
z.Ib = function(a, b, c, d) {
  d == h && (d = q);
  var f = this.rj;
  switch(a) {
    case 0:
      return this.vp - f * (d ? b : b + c) - (d ? c : 0) - this.mv / 2;
    case 1:
      return this.vp + f * b + this.mv / 2;
    default:
      return this.vp - (d ? 1 : f) * c / 2 + f * b
  }
};
z.mD = function(a) {
  var b = new P, c = this.Wb.n();
  b.x = c.x;
  b.y = c.y;
  var d = this.rj, f;
  1 == a.ol() ? (f = this.Ib(a.za, a.ga(), a.ec()), a = d * a.ec()) : (f = this.Ib(a.za, a.ga(), 0), a = 0);
  var d = this.Ej * this.Xy, g = this.bj * this.Xy, k = this.rj, d = Math.max(d, this.Wb.Ej * k), g = Math.max(g, this.Wb.bj * k);
  this.Wb.Vf() ? (b.y = f, b.height = a, b.x += d, b.width = c.width - d - g) : (b.x = f, b.width = a, b.y += d, b.height = c.height - d - g);
  return b
};
z.CI = function(a, b) {
  var c = this.mD(b), d = "";
  1 == b.ol() ? (d += S(c.x, c.y), d += U(c.Ja(), c.y), d += U(c.Ja(), c.ra()), d += U(c.x, c.ra())) : (d += S(c.x, c.y), d += U(c.Ja(), c.ra()));
  return d + " Z"
};
z.yo = function(a, b, c) {
  var d = this.cd.r(), f = this.rj, g = a.li() * f, f = a.Ka() * f, k = Math.min(g, f), l = 0, n = 0;
  this.Wb.Vf() ? (n = this.Ib(a.za, a.ga(), f, j), l = b - g / 2) : (l = this.Ib(a.za, a.ga(), g, j), n = b - f / 2);
  b = a.yo(d, l, n, k, g, f, c);
  b.Vl(b.Bb + a.Bb);
  return b
};
z.Jg = function(a) {
  var b = new O, c = this.bf.ej().n();
  this.Wb.Vf() ? (b.x = a - c.width / 2, b.y = this.Ib(this.bf.za, this.bf.ga(), c.height, j)) : (b.y = a - c.height / 2, b.x = this.Ib(this.bf.za, this.bf.ga(), c.width, j));
  return b
};
z.Ul = function(a, b, c) {
  this.Wb.Vf() ? (c.x = b, c.y = a) : (c.x = a, c.y = b)
};
function Kh(a, b, c, d, f) {
  var g = a.rj;
  switch(b) {
    case 0:
      return a.vp - g * (c + (f ? 0 : d)) - a.mv / 2;
    case 1:
      return a.vp + g * (c + (f ? 0 : d)) + a.mv / 2;
    default:
    ;
    case Jg:
      return a.vp + g * d * (f ? -0.5 : 0.5)
  }
}
;function Zi(a) {
  kg.call(this, a)
}
A.e(Zi, kg);
z = Zi.prototype;
z.rH = function() {
  return new $i(this)
};
z.sH = function() {
  return new aj(this)
};
z.nH = function() {
  return new bj(this)
};
z.Va = function() {
  var a = 0;
  this.Uc && this.Uc.isEnabled() && (a += this.Uc.Va());
  this.Vd && this.Vd.isEnabled() && (a += this.Vd.Va());
  return a
};
z.Dw = function(a) {
  var b = 1 + this.Va() + this.padding, c = this.Wb, d = 2 * b, f = 2 * b, b = Math.min(a.width / d, a.height / f), d = d * b, f = f * b;
  a.x += (a.width - d) / 2;
  a.y += (a.height - f) / 2;
  a.width = d;
  a.height = f;
  c.bz = 2 * b;
  c.Pm = b;
  c.Ll.x = 0.5;
  c.Ll.y = 0.5
};
z.ci = function(a, b) {
  var c;
  c = this.Wb.Pm;
  var d = this.Wb.oc;
  c = "" + ed(a, d.x, d.y, c + this.padding * c + b * c);
  return c + " Z"
};
z.Gx = function(a, b, c) {
  var d = "", f = this.Wb.Pm, g = this.padding * f, b = f + g + b * f, c = f + g + c * f, k = this.Wb.oc.x, f = this.Wb.oc.y, g = k + b, l = k + b * Math.cos(359.999 * Math.PI / 180), n = f + b * Math.sin(359.999 * Math.PI / 180), d = d + S(g, f), d = d + a.ub(b, 1, 1, l, n, b), b = k + c, k = k + c * Math.cos(359.999 * Math.PI / 180), l = f + c * Math.sin(359.999 * Math.PI / 180), d = d + S(k, l), d = d + a.ub(c, 1, 0, b, f, c);
  return d += S(g, f)
};
function cj(a) {
  this.frame = a
}
A.e(cj, eg);
cj.prototype.p = function(a) {
  if(!this.Ma) {
    return p
  }
  var b = this.createElement(a), c = this.createElement(a);
  return new Md(a, b, c)
};
cj.prototype.qa = function(a, b, c) {
  var d = a.r(), f;
  this.Ve && this.Ve.isEnabled() && (f = Tc(d.hk, this.Ve));
  d = this.yh(d);
  this.rC(a.Hg, d, a, b, c, f);
  Ud(this, a.Mg, d, a, f)
};
cj.prototype.yh = function(a) {
  var b = "", c, d;
  this instanceof $i ? (c = 0, d = this.Va()) : (c = 0, this.frame.Uc.isEnabled() && (c = this.frame.Uc.Va()), d = c + this.Va());
  return b += this.frame.Gx(a, c, d) + " Z"
};
cj.prototype.frame = p;
function $i(a) {
  this.frame = a
}
A.e($i, cj);
function aj(a) {
  this.frame = a
}
A.e(aj, cj);
function bj(a) {
  this.jI = a
}
A.e(bj, Td);
bj.prototype.jI = p;
bj.prototype.createElement = function(a) {
  return a.ja()
};
bj.prototype.yh = function(a) {
  return this.jI.ci(a, 0) + " Z"
};
function dj(a) {
  kg.call(this, a)
}
A.e(dj, Zi);
dj.prototype.Gx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
dj.prototype.ci = function(a, b) {
  var c = "", d = this.Wb, f = d.Pm;
  if(this.qB) {
    f += this.padding * f + b * f, d = new P(d.oc.x - f, d.oc.y - f, 2 * f, 2 * f)
  }else {
    var g = new Ad, k = this.Va();
    g.Pl(k - b);
    k = new P;
    k.bh(d.zb.x + g.tb() * f);
    k.fh(d.zb.y + g.rb() * f);
    k.eh(d.zb.Ja() - g.Ja() * f);
    k.$g(d.zb.ra() - g.ra() * f);
    d = k
  }
  this.ym == p ? c += gd(d) : (g = this.ym.Ca(), g.sa = g.sa * f / 100, g.P = g.P * f / 100, g.X = g.X * f / 100, g.ha = g.ha * f / 100, c += Dd(g, a, d));
  return c
};
function ej(a) {
  kg.call(this, a)
}
A.e(ej, Zi);
ej.prototype.cj = function(a) {
  var b = Math.cos(a), a = Math.sin(a);
  return 0 <= b && 0 <= a ? 0 : 0 >= b && 0 <= a ? 1 : 0 >= b && 0 > a ? 2 : 3
};
ej.prototype.Dw = function() {
  var a = this.Wb, b = this.Va(), c = this.padding, d = a.Su, f = a.Pu, g = f - d, k = new Ad;
  k.Pl(0);
  if(320 <= g) {
    k.Pl(1 + c)
  }else {
    var d = d * Math.PI / 180, l = f * Math.PI / 180, g = [];
    g.push(d);
    g.push(l);
    var f = [], n = this.cj(d), m = this.cj(l);
    if(n != m) {
      switch(n) {
        case 0:
          1 == m ? f.push(Math.PI / 2) : 2 == m ? (f.push(Math.PI / 2), f.push(Math.PI)) : 3 == m && (f.push(Math.PI / 2), f.push(Math.PI), f.push(3 * Math.PI / 2));
          break;
        case 1:
          0 == m ? (f.push(0), f.push(Math.PI), f.push(3 * Math.PI / 2)) : 2 == m ? f.push(Math.PI) : 3 == m && (f.push(Math.PI), f.push(3 * Math.PI / 2));
          break;
        case 2:
          1 == m ? (f.push(Math.PI / 2), f.push(0), f.push(3 * Math.PI / 2)) : 3 == m ? f.push(3 * Math.PI / 2) : 0 == m && (f.push(0), f.push(3 * Math.PI / 2));
          break;
        case 3:
          0 == m ? f.push(0) : 1 == m ? (f.push(0), f.push(Math.PI / 2)) : 2 == m && (f.push(0), f.push(Math.PI / 2), f.push(Math.PI))
      }
    }else {
      l - d > Math.PI && (f.push(0), f.push(Math.PI / 2), f.push(Math.PI), f.push(3 * Math.PI / 2))
    }
    for(d = 0;d < g.length;d++) {
      var o = g[d], l = c, n = k, m = Math.cos(o), o = Math.sin(o), m = new P(m, o, 0, 0);
      Yc(m, l, l);
      0 > m.tb() ? n.bh(Math.min(n.tb(), m.tb())) : n.eh(Math.max(n.Ja(), m.tb()));
      0 > m.Ja() ? n.bh(Math.min(n.tb(), m.Ja())) : n.eh(Math.max(n.Ja(), m.Ja()));
      0 > m.rb() ? n.fh(Math.min(n.rb(), m.rb())) : n.$g(Math.max(n.ra(), m.rb()));
      0 > m.ra() ? n.fh(Math.min(n.rb(), m.rb())) : n.$g(Math.max(n.ra(), m.ra()))
    }
    for(d = 0;d < f.length;d++) {
      n = f[d], m = c, g = k, l = (1 + m) * Math.cos(n), n = (1 + m) * Math.sin(n), 0 > l ? g.bh(Math.min(g.tb(), l)) : g.eh(Math.max(g.Ja(), l)), 0 > n ? g.fh(Math.min(g.rb(), n)) : g.$g(Math.max(g.ra(), n))
    }
    c += this.Wb.Dl;
    k.bh(Math.max(Math.abs(k.tb()), c));
    k.eh(Math.max(k.Ja(), c));
    k.fh(Math.max(Math.abs(k.rb()), c));
    k.$g(Math.max(k.ra(), c))
  }
  k.bh(k.tb() + b);
  k.eh(k.Ja() + b);
  k.fh(k.rb() + b);
  k.$g(k.ra() + b);
  b = k.tb() + k.Ja();
  c = k.rb() + k.ra();
  f = Math.min(a.zb.width / b, a.zb.height / c);
  d = b * f;
  g = c * f;
  a.zb.x += (a.zb.width - d) / 2;
  a.zb.y += (a.zb.height - g) / 2;
  a.zb.width = d;
  a.zb.height = g;
  a.bz = 2 * f;
  a.Pm = f;
  a.Ll.x = k.tb() / b;
  a.Ll.y = k.rb() / c
};
function fj(a) {
  kg.call(this, a)
}
A.e(fj, ej);
fj.prototype.ci = function(a, b) {
  var c = "", d = this.Wb, f = d.Pm, g = new Ad, k = this.Va();
  g.Pl(k - b);
  k = new P;
  k.bh(d.zb.x + g.tb() * f);
  k.fh(d.zb.y + g.rb() * f);
  k.eh(d.zb.Ja() - g.Ja() * f);
  k.$g(d.zb.ra() - g.ra() * f);
  this.ym == p ? c += gd(k) : (d = this.ym.Ca(), d.sa = d.sa * f / 100, d.P = d.P * f / 100, d.X = d.X * f / 100, d.ha = d.ha * f / 100, c += Dd(d, a, k));
  return c
};
fj.prototype.Gx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
function gj(a) {
  kg.call(this, a)
}
A.e(gj, ej);
gj.prototype.ci = function(a, b) {
  var c = "", d = this.Wb, f = d.Pm, g = d.Dl * f, k = this.padding * f + b * f, l = d.Su, n = d.Pu, m = l * Math.PI / 180, o = n * Math.PI / 180, v = new O;
  v.x = d.oc.x + f * Math.cos(m);
  v.y = d.oc.y + f * Math.sin(m);
  m = new O;
  m.x = d.oc.x + f * Math.cos(o);
  m.y = d.oc.y + f * Math.sin(o);
  o = new P(v.x, v.y, 0, 0);
  Yc(o, k, k);
  v = new P(m.x, m.y, 0, 0);
  Yc(v, k, k);
  m = new P(d.oc.x, d.oc.y, 0, 0);
  Yc(m, k + g, k + g);
  g = new P(d.oc.x, d.oc.y, 0, 0);
  Yc(g, f + k, f + k);
  var w = n - l;
  320 <= w ? c += ed(a, d.oc.x, d.oc.y, f + k) : 270 < w ? (d = 90 - (360 - w) / 2, c += hj(o, l + 270 + d, 90 - d, j), c += hj(g, l, w, q), c += hj(v, n, 90 - d, q)) : (c += hj(o, l + 270, 90, j), c += hj(g, l, w, q), c += hj(v, n, 90, q), c += hj(m, n + 45, 360 - w - 90, q));
  return c
};
function hj(a, b, c, d) {
  var f = "", g = a.x + a.width / 2, k = a.y + a.height / 2, c = b + c, l = a.height / 2, a = a.width / 2, n = d;
  n == h && (n = j);
  var d = "", m, o, v = b;
  m = g + Uc(a, v);
  o = k + Vc(l, v);
  d = n ? d + S(m, o) : d + U(m, o);
  n = 1;
  c < b && (n = -n);
  for(b = 0 < n ? v < c : v > c;b;) {
    m = g + Uc(a, v), o = k + Vc(l, v), d += U(m, o), v += n, b = 0 < n ? v < c : v > c
  }
  v = c;
  m = g + Uc(a, v);
  o = k + Vc(l, v);
  d += U(m, o);
  return f + d
}
gj.prototype.Gx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
function ij() {
}
z = ij.prototype;
z.pi = p;
z.xi = p;
z.fo = p;
z.K = p;
z.p = function(a, b, c) {
  this.K = c;
  this.j = a;
  c.Uc && c.Uc.isEnabled() && (this.pi = a.r().ja());
  c.Vd && c.Vd.isEnabled() && (this.xi = a.r().ja());
  c.md() && c.md().isEnabled() && (this.fo = a.r().ja())
};
z.nB = function(a) {
  this.fo && a.appendChild(this.fo);
  this.pi && a.appendChild(this.pi);
  this.xi && a.appendChild(this.xi)
};
z.update = function() {
  this.fo && this.cw(this.IB());
  if(this.pi) {
    var a = jj(this, this.K.Uc);
    this.pi && (this.pi.setAttribute("d", kj(this, this.j.r())), this.pi.setAttribute("style", this.Xt(this.K.Uc, a)))
  }
  this.xi && (a = jj(this, this.K.Vd), this.xi && (this.xi.setAttribute("d", lj(this, this.j.r())), this.xi.setAttribute("style", this.Xt(this.K.Vd, a))))
};
z.cw = function(a) {
  this.fo && (this.fo.setAttribute("d", this.Ut(this.j.r())), this.fo.setAttribute("style", this.Xt(this.K.md(), a)))
};
z.Xt = function(a, b) {
  var c;
  c = "" + (a.Mc() ? Ld(a.Mc(), this.j.r(), b, p, j) : "");
  c += a.mc() ? Jd(a.mc(), this.j.r(), b, p, j) : "";
  return c += a.De() ? Hc(a.De(), this.j.r()) : ""
};
z.IB = function() {
  var a = this.j.w, b = a.Zc().oc, a = a.vd((this.K.te ? a.size : 1) * this.K.td), c = new P;
  c.x = b.x - a;
  c.y = b.y - a;
  c.width = 2 * a;
  c.height = 2 * a;
  return c
};
function jj(a, b) {
  var c = new P, d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, d = b == a.K.Uc ? d.vd(f + a.K.Uc.Va()) : d.vd(f + a.K.Uc.Va() + a.K.Vd.Va());
  c.x = g.x - d;
  c.y = g.y - d;
  c.width = c.height = 2 * d;
  return c
}
z.Ut = function(a) {
  var b = this.j.w, c = b.Zc().oc, a = "" + ed(a, c.x, c.y, b.vd((this.K.te ? b.size : 1) * this.K.td));
  return a + " Z"
};
function kj(a, b) {
  var c = "", d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, k = d.vd(f), d = d.vd(f + a.K.Uc.Va()), l = g.x, g = g.y, f = l + k, n = l + k * Math.cos(359.999 * Math.PI / 180), m = g + k * Math.sin(359.999 * Math.PI / 180), c = c + S(f, g), c = c + b.ub(k, 1, 1, n, m, k), k = l + d, l = l + d * Math.cos(359.999 * Math.PI / 180), n = g + d * Math.sin(359.999 * Math.PI / 180), c = c + S(l, n), c = c + b.ub(d, 1, 0, k, g, d), c = c + S(f, g);
  return c + " Z"
}
function lj(a, b) {
  var c = "", d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, k = d.vd(f + a.K.Uc.Va()), d = d.vd(f + a.K.Uc.Va() + a.K.Vd.Va()), l = g.x, g = g.y, f = l + k, n = l + k * Math.cos(359.999 * Math.PI / 180), m = g + k * Math.sin(359.999 * Math.PI / 180), c = c + S(f, g), c = c + b.ub(k, 1, 1, n, m, k), k = l + d, l = l + d * Math.cos(359.999 * Math.PI / 180), n = g + d * Math.sin(359.999 * Math.PI / 180), c = c + S(l, n), c = c + b.ub(d, 1, 0, k, g, d), c = c + S(f, g);
  return c + " Z"
}
function mj() {
}
A.e(mj, Td);
z = mj.prototype;
z.ho = p;
z.K = p;
z.j = p;
z.p = function(a, b) {
  this.K = b;
  this.j = a;
  b.md() && b.md().isEnabled() && (this.ho = a.r().ja())
};
z.nB = function(a) {
  this.ho && a.appendChild(this.ho)
};
z.update = function() {
  this.ho && this.cw(this.IB())
};
z.cw = function(a) {
  this.ho && (this.ho.setAttribute("d", this.Ut(this.j.r())), this.ho.setAttribute("style", this.Xt(this.K.md(), a)))
};
z.Xt = function(a, b) {
  var c;
  c = "" + (a.Mc() ? Ld(a.Mc(), this.j.r(), b, p, j) : "");
  c += a.mc() ? Jd(a.mc(), this.j.r(), b, p, j) : "";
  return c += a.De() ? Hc(a.De(), this.j.r()) : ""
};
z.IB = function() {
  var a = this.j.w, b = this.K, a = a.vd((b.te ? a.size : 1) * b.td + (b.Fy ? a.size : 1) * b.aC);
  return new P(-a, -a, 2 * a, 2 * a)
};
z.Ut = function(a) {
  var b = "", c = this.j, d = c.w, f = c.ya.Fu, g = (f.te ? d.size : 1) * f.td, k = f.dM, l = d.vd(g), f = d.vd(g + (f.Fy ? d.size : 1) * f.aC), d = d.Zc().oc, g = 360 / k;
  c.Eu.Xd(d.x);
  c.Eu.gd(d.y);
  var c = 90 - g / 2, n = q, d = new O(0, 0), m = new O;
  m.x = d.x + f * Math.cos(c * Math.PI / 180);
  m.y = d.y + f * Math.sin(c * Math.PI / 180);
  var o, v, w;
  w = m.x;
  v = m.y;
  for(var y, E, b = b + S(w, v), V = 0;V < k;V++) {
    o = n ? l : f, v = c + g * V, w = v + g, y = d.x + o * Math.cos(w * Math.PI / 180), E = d.x + o * Math.sin(w * Math.PI / 180), w = d.x + o * Math.cos(v * Math.PI / 180), v = d.y + o * Math.sin(v * Math.PI / 180), b += U(w, v), b += a.ub(o, 0, 1, y, E, o), n = !n
  }
  b += U(m.x, m.y);
  return b + " Z"
};
function nj() {
  this.background = new Nd;
  this.td = 0.5;
  this.Fy = this.te = q
}
z = nj.prototype;
z.dM = 12;
z.aC = 0.03;
z.td = p;
z.te = p;
z.Fy = p;
z.background = p;
z.md = u("background");
z.g = function(a) {
  var b;
  this.background.g(a);
  C(a, "radius") && (b = J(a, "radius"), this.td = (this.te = Og(b)) ? Pg(b) : Pb(b));
  C(a, "gear_height") && (b = J(a, "gear_height"), this.Fy = Og(b), this.aC = this.te ? Pg(b) : Pb(b))
};
function oj() {
  this.td = 0.1;
  this.te = q
}
z = oj.prototype;
z.td = p;
z.te = p;
z.Uc = p;
z.Vd = p;
z.background = p;
z.md = u("background");
z.g = function(a) {
  var b;
  C(a, "radius") && (b = J(a, "radius"), this.td = (this.te = Og(b)) ? Pg(b) : Pb(b));
  Ub(a, "inner_stroke") && (this.Uc = new eg, this.Uc.g(F(a, "inner_stroke")));
  Ub(a, "outer_stroke") && (this.Vd = new eg, this.Vd.g(F(a, "outer_stroke")));
  Ub(a, "background") && (this.background = new Nd, this.background.g(F(a, "background")))
};
function pj(a) {
  Ig.call(this, a)
}
A.e(pj, Ig);
pj.prototype.mo = p;
pj.prototype.g = function(a) {
  a = pj.f.g.call(this, a);
  Ub(a, "cap") ? (this.mo = new oj, this.mo.g(F(a, "cap"))) : this.mo = p;
  return a
};
function qj() {
}
A.e(qj, Of);
z = qj.prototype;
z.qq = p;
z.Yc = function() {
  qj.f.Yc.call(this);
  var a = this.j.ya.mo;
  a != p && (this.qq = new ij, this.qq.p(this.j, this, a))
};
z.ff = function() {
  qj.f.ff.call(this);
  this.qq != p && this.qq.nB(this.j.Vw)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function() {
  var a = this.j;
  this.Db && this.Db.G.setAttribute("d", a.vh.qa(a.fb(), a));
  this.Fb && this.Fb.G.setAttribute("d", a.vh.qa(a.fb(), a));
  this.qq != p && this.qq.update()
};
z.ud = s();
function rj(a) {
  a && (this.background = new Nd);
  this.Bu = this.te = this.Jy = this.Iy = this.vu = q
}
z = rj.prototype;
z.td = p;
z.te = p;
z.xB = p;
z.vu = p;
z.hK = p;
z.Jy = p;
z.gK = p;
z.Iy = p;
z.IA = p;
z.Bu = p;
z.g = function(a, b) {
  var c;
  C(a, "radius") && (c = J(a, "radius"), this.td = (this.te = Og(c)) ? Pg(c) : Pb(c));
  C(a, "thickness") && (c = J(a, "thickness"), this.IA = (this.Bu = Og(c)) ? Pg(c) : Pb(c));
  C(a, "base_radius") && (c = J(a, "base_radius"), this.xB = (this.vu = Og(c)) ? Pg(c) : Pb(c));
  C(a, "point_thickness") && (c = J(a, "point_thickness"), this.hK = (this.Jy = Og(c)) ? Pg(c) : Pb(c));
  C(a, "point_radius") && (c = J(a, "point_radius"), this.gK = (this.Iy = Og(c)) ? Pg(c) : Pb(c));
  this.background != p && this.background.g(a, b)
};
function sj(a) {
  Ig.call(this, a);
  this.Hh = new rj(q)
}
A.e(sj, pj);
sj.prototype.Hh = p;
sj.prototype.vI = u("Hh");
sj.prototype.g = function(a) {
  a = sj.f.g.call(this, a);
  this.Hh.g(a, this.ka());
  return a
};
function tj() {
}
A.e(tj, qj);
function uj() {
  $.call(this)
}
A.e(uj, Qf);
uj.prototype.cc = function() {
  return sj
};
uj.prototype.lc = function() {
  return new tj
};
function vj(a) {
  Ig.call(this, a);
  this.ds = q;
  this.ab = 0.1;
  this.xy = q
}
A.e(vj, pj);
z = vj.prototype;
z.ds = p;
z.ab = p;
z.Ka = u("ab");
z.xy = p;
z.g = function(a) {
  var a = vj.f.g.call(this, a), b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.xy = Og(b)) ? Pg(b) : Pb(b));
  C(a, "start_from_zero") && (this.ds = K(a, "start_from_zero"));
  return a
};
function wj() {
}
A.e(wj, Of);
wj.prototype.update = function(a, b) {
  wj.f.update.call(this, a, b)
};
wj.prototype.createElement = function() {
  return this.j.r().ja()
};
wj.prototype.Ta = function(a) {
  a.setAttribute("d", xj(this.j))
};
function yj() {
  $.call(this)
}
A.e(yj, Qf);
yj.prototype.cc = function() {
  return vj
};
yj.prototype.lc = function() {
  return new wj
};
function zj() {
  Ig.call(this, h);
  this.gb = new ti(q)
}
A.e(zj, pj);
zj.prototype.gb = p;
zj.prototype.g = function(a) {
  a = zj.f.g.call(this, a);
  this.gb.g(a);
  return a
};
function Aj() {
  this.Ln = new jg
}
A.e(Aj, Of);
z = Aj.prototype;
z.Ln = p;
z.fb = function() {
  return this.j.sd
};
z.update = function(a, b) {
  this.j.hd();
  Aj.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function(a) {
  a.setAttribute("d", this.Ln.Io(this.j.ya.gb.Yd, this.j.r(), Math.min(this.j.n().width, this.j.n().height), this.j.hp.x, this.j.hp.y, this.j.n().width, this.j.n().height, this.j.ya.Ld() ? this.j.ya.Mc().Va() : 1))
};
function Bj() {
  $.call(this)
}
A.e(Bj, Qf);
Bj.prototype.cc = function() {
  return zj
};
Bj.prototype.lc = function() {
  return new Aj
};
function Cj(a) {
  Ig.call(this, a)
}
A.e(Cj, pj);
Cj.prototype.Fu = p;
Cj.prototype.Hh = p;
Cj.prototype.vI = u("Hh");
Cj.prototype.g = function(a) {
  Ub(a, "knob_background") ? (this.Fu = new nj, this.Fu.g(F(a, "knob_background"))) : this.Fu = p;
  Ub(a, "needle") ? (Cj.f.g.call(this, F(a, "needle")), this.Hh = new rj(j), this.Hh.g(F(a, "needle"), this.ka())) : this.Hh = p;
  return a = Cj.f.g.call(this, a)
};
function Dj() {
}
A.e(Dj, qj);
Dj.prototype.zr = p;
Dj.prototype.Yc = function() {
  Dj.f.Yc.call(this);
  var a = this.j.ya.Fu;
  a != p && (this.zr = new mj, this.zr.p(this.j, a))
};
Dj.prototype.ff = function() {
  Dj.f.ff.call(this);
  this.zr != p && this.zr.nB(this.j.Eu)
};
Dj.prototype.Ta = function() {
  Dj.f.Ta.call(this);
  this.zr != p && this.zr.update()
};
function Ej() {
  $.call(this)
}
A.e(Ej, Qf);
Ej.prototype.cc = function() {
  return Cj
};
Ej.prototype.lc = function() {
  return new Dj
};
function Fj() {
}
z = Fj.prototype;
z.bd = p;
z.vd = u("bd");
z.jv = p;
z.Dz = p;
z.Cz = p;
z.kv = p;
z.qa = function(a, b) {
  var c = b.be(b.ka()), d = c.Hh, f = c.mc(), c = b.w, g = 0, k = new O(0, 0), l = new O, n = new O, m = new O, o = new O, v = new O, w = new O;
  f != p && 1 == f.Ra && 0 == f.yb.Ra && (f.yb.ac += g);
  g *= Math.PI / 180;
  this.bd = c.vd((d.te ? c.size : 1) * d.td);
  this.jv = c.vd((d.vu ? c.size : 1) * d.xB);
  this.Dz = c.vd((d.Bu ? c.size : 1) * d.IA);
  this.Cz = c.vd((d.Jy ? c.size : 1) * d.hK);
  this.kv = c.vd((d.Iy ? c.size : 1) * d.gK);
  var d = Math.sin(g), g = Math.cos(g), f = this.Dz / 2 * d, y = this.Dz / 2 * g;
  w.x = k.x + this.jv * g;
  w.y = k.y + this.jv * d;
  l.x = w.x - f;
  l.y = w.y + y;
  n.x = w.x + f;
  n.y = w.y - y;
  f = this.Cz / 2 * d;
  y = this.Cz / 2 * g;
  w.x = k.x + (this.bd - this.kv) * g;
  w.y = k.y + (this.bd - this.kv) * d;
  m.x = w.x - f;
  m.y = w.y + y;
  o.x = w.x + f;
  o.y = w.y - y;
  v.x = k.x + this.bd * g;
  v.y = k.y + this.bd * d;
  k = [l, n, o, v, m];
  w = new P;
  w.x = Math.min(l.x, n.x, m.x, o.x, v.x);
  w.y = Math.min(l.y, n.y, m.y, o.y, v.y);
  w.width = Math.max(l.x, n.x, m.x, o.x, v.x) - w.x;
  w.height = Math.max(l.y, n.y, m.y, o.y, v.y) - w.y;
  b.Kn(b.ta() > c.fa().pa ? c.fa().pa : b.ta() < c.fa().la ? c.fa().la : b.ta());
  a.Xd(c.Wb.oc.x);
  a.gd(c.Wb.oc.y);
  a.Vl(c.fa().pc(b.ta()));
  c = "" + S(k[0].x, k[0].y);
  c += U(k[1].x, k[1].y);
  c += U(k[2].x, k[2].y);
  c += U(k[3].x, k[3].y);
  c += U(k[4].x, k[4].y);
  c += U(k[0].x, k[0].y);
  return c + " Z"
};
function Gj() {
  Lg.call(this)
}
A.e(Gj, Lg);
function Hj(a, b, c, d, f, g, k) {
  var l = new O, n = new O, m = new O, o = new O, v = Math.sin(k * Math.PI / 180), k = Math.cos(k * Math.PI / 180), w = a.Oc.oc, a = w.x + d / 2 * v, y = w.y - d / 2 * k, E = w.x - d / 2 * v, d = w.y + d / 2 * k;
  m.x = a + (g - f / 2) * k;
  m.y = y + (g - f / 2) * v;
  n.x = E + (g + f / 2) * k;
  n.y = d + (g + f / 2) * v;
  l.x = a + (g + f / 2) * k;
  l.y = y + (g + f / 2) * v;
  o.x = E + (g - f / 2) * k;
  o.y = d + (g - f / 2) * v;
  switch(b) {
    case 0:
      c.x = (l.x + o.x) / 2;
      c.y = (l.y + o.y) / 2;
      break;
    case 7:
      c.x = (m.x + o.x) / 2;
      c.y = (m.y + o.y) / 2;
      break;
    case 1:
      c.x = (l.x + m.x) / 2;
      c.y = (l.y + m.y) / 2;
      break;
    case 5:
      c.x = (o.x + n.x) / 2;
      c.y = (o.y + n.y) / 2;
      break;
    case 3:
      c.x = (l.x + n.x) / 2;
      c.y = (l.y + n.y) / 2;
      break;
    case 8:
      c.x = m.x;
      c.y = m.y;
      break;
    case 2:
      c.x = l.x;
      c.y = l.y;
      break;
    case 6:
      c.x = o.x;
      c.y = o.y;
      break;
    case 4:
      c.x = n.x, c.y = n.y
  }
}
function Ij() {
  Lg.call(this)
}
A.e(Ij, Gj);
Ij.prototype.Qm = p;
Ij.prototype.Vw = p;
Ij.prototype.p = function(a) {
  this.Qm = new W(a);
  this.Vw = new W(a);
  Ij.f.p.call(this, a)
};
function Jj() {
  Lg.call(this);
  this.vh = new Fj
}
A.e(Jj, Ij);
z = Jj.prototype;
z.vh = p;
z.Rb = x("needle_pointer_style");
z.Qb = function() {
  return new uj
};
z.p = function(a) {
  Jj.f.p.call(this, a);
  this.Qm.ia(this.D);
  this.Qm.ia(this.Vw);
  return this.Qm
};
z.Qq = function(a, b) {
  var c = this.w.fa().pc(Mg(this, this.bb)), d = Math.max(this.vh.jv, this.vh.kv, this.vh.vd()), f = Math.min(this.vh.jv, this.vh.kv, this.vh.vd()), g = Math.max(this.vh.Cz, this.vh.Dz);
  Hj(this, a, b, g, d - f, (f + d) / 2, c)
};
function Kj() {
  Lg.call(this)
}
A.e(Kj, Gj);
z = Kj.prototype;
z.jb = p;
z.tc = p;
z.Sb = p;
z.Qa = p;
z.Rb = x("bar_pointer_style");
z.Qb = function() {
  return new yj
};
function xj(a) {
  var b = a.w, c = b.Zc().oc, d = a.be(a.ka()), f = (d.xy ? b.size : 1) * d.Ka(), g, k;
  a instanceof Lj ? (g = Mg(a, a.bb), k = Mg(a, a.Qd)) : (g = d.ds ? 0 : b.fa().la, k = Mg(a, a.bb));
  a.jb = b.fa().pc(g);
  a.tc = b.fa().pc(k);
  g = Math.abs(a.jb - a.tc);
  a.Sb = Nh(b, d.za, d.ga(), f, j);
  a.Qa = Nh(b, d.za, d.ga(), f, q);
  var l, n;
  a.tc % 360 != a.jb % 360 ? (n = (a.tc + 0.5) * Math.PI / 180, l = a.jb * Math.PI / 180) : (b = Math.min(a.jb, a.tc), n = (a.tc - b - 0.001) * Math.PI / 180, l = (a.jb - b) * Math.PI / 180);
  b = c.x + a.Sb * Math.cos(l);
  d = c.y + a.Sb * Math.sin(l);
  f = c.x + a.Sb * Math.cos(n);
  k = c.y + a.Sb * Math.sin(n);
  var m = c.x + a.Qa * Math.cos(l);
  l = c.y + a.Qa * Math.sin(l);
  var o = c.x + a.Qa * Math.cos(n), c = c.y + a.Qa * Math.sin(n);
  n = 1;
  180 > g && (n = 0);
  g = a.r();
  var v;
  v = "" + S(b, d);
  v += g.ub(a.Sb, n, 1, f, k, a.Sb);
  v += U(o, c);
  v += g.ub(a.Qa, n, q, m, l, a.Qa);
  v += U(b, d);
  return v + " Z"
}
z.Qq = function(a, b) {
  var c = this.w.Zc().oc, d, f;
  b.x = c.x;
  b.y = c.y;
  switch(a) {
    case 0:
      d = (this.jb + this.tc) / 2;
      f = (this.Sb + this.Qa) / 2;
      break;
    case 7:
      d = (this.jb + this.tc) / 2;
      f = this.Sb;
      break;
    case 1:
      d = this.jb;
      f = (this.Sb + this.Qa) / 2;
      break;
    case 5:
      d = this.Gm;
      f = (this.Sb + this.Qa) / 2;
      break;
    case 3:
      d = (this.jb + this.Gm) / 2;
      f = this.Qa;
      break;
    case 8:
      d = this.jb;
      f = this.Sb;
      break;
    case 2:
      d = this.jb;
      f = this.Qa;
      break;
    case 6:
      d = this.tc;
      f = this.Sb;
      break;
    case 4:
      d = this.tc, f = this.Qa
  }
  b.x += f * Math.cos(d * Math.PI / 180);
  b.y += f * Math.sin(d * Math.PI / 180)
};
function Lj() {
  Lg.call(this)
}
A.e(Lj, Kj);
Lj.prototype.Qd = p;
Lj.prototype.Rb = x("range_bar_pointer_style");
Lj.prototype.g = function(a, b, c) {
  a.value = a.start;
  Lj.f.g.call(this, a, b, c);
  this.Qd = M(a, "end")
};
function Mj() {
  Lg.call(this)
}
A.e(Mj, Gj);
z = Mj.prototype;
z.hp = p;
z.sd = p;
z.p = function(a) {
  this.sd = new W(a);
  Mj.f.p.call(this, a);
  this.D.ia(this.sd);
  return this.D
};
z.Qq = function(a, b) {
  var c = this.ya, d = c.gb, f = this.w, g = f.vd((d.qd ? f.size : 1) * d.Ka()), d = f.vd((d.vl ? f.size : 1) * d.vb()), k = f.og(c.za, c.ga(), 0, q);
  switch(c.za) {
    case 0:
      k -= g / 2;
      break;
    case 1:
      k += g / 2
  }
  var c = new O, l = f.fa().pc(Mg(this, this.bb));
  Gh(f, k, l, c);
  Hj(this, a, b, g, d, k, l)
};
z.hd = function() {
  var a = this.ya, b = a.gb, c = this.w, d = c.vd((b.qd ? c.size : 1) * b.Ka()), f = c.vd((b.vl ? c.size : 1) * b.vb()), g = c.og(a.za, a.ga(), 0, q);
  switch(a.za) {
    case 0:
      g -= d / 2;
      break;
    case 1:
      g += d / 2
  }
  var k = new O, l = c.fa().pc(this.bb);
  Gh(c, g, l, k);
  this.hp = new O(-d / 2, -f / 2);
  this.k.x = k.x;
  this.k.y = k.y;
  this.k.width = d;
  this.k.height = f;
  c = 0;
  if(b.bo) {
    switch(a.za) {
      case 0:
      ;
      case Jg:
        c = l + 90;
        break;
      case 1:
        c = l - 90
    }
  }
  this.sd.Vl(c + b.Bb);
  this.sd.Xd(k.x);
  this.sd.gd(k.y)
};
z.Rb = x("marker_pointer_style");
z.cc = function() {
  return zj
};
z.Qb = function() {
  return new Bj
};
function Nj() {
  Lg.call(this);
  this.vh = new Fj
}
A.e(Nj, Ij);
z = Nj.prototype;
z.vh = p;
z.Eu = p;
z.Rb = x("knob_pointer_style");
z.cc = function() {
  return Cj
};
z.Qb = function() {
  return new Ej
};
z.p = function(a) {
  this.Eu = new W(a);
  Nj.f.p.call(this, a);
  this.Qm.ia(this.Eu);
  this.Qm.ia(this.D);
  this.Qm.ia(this.Vw);
  return this.Qm
};
function Oj() {
  hi.call(this);
  this.Ll = new O(0.5, 0.5);
  this.oc = new O;
  this.cz = 0;
  this.Su = 72E3;
  this.Pu = -72E3;
  this.Dl = 0
}
A.e(Oj, hi);
z = Oj.prototype;
z.bz = p;
z.Pm = p;
z.Ll = p;
z.oc = p;
z.cz = p;
z.Su = p;
z.Pu = p;
z.Dl = p;
z.p = function(a, b, c) {
  Oj.f.p.call(this, a, b, c);
  return this.cd
};
z.g = function(a) {
  Oj.f.g.call(this, a);
  F(a, "pivot_point") && (a = F(a, "pivot_point"), this.Ll.x = Pb(a.x), this.Ll.y = Pb(a.y))
};
z.tH = function(a) {
  switch(a) {
    case "bar":
      return new Kj;
    case "rangebar":
      return new Lj;
    case "marker":
      return new Mj;
    case "knob":
      return new Nj;
    default:
      return new Jj
  }
};
z.$w = function(a) {
  Pj(this, a, a.ka().wa);
  Pj(this, a, a.ka().dc);
  Pj(this, a, a.ka().kc);
  Pj(this, a, a.ka().Yb);
  Pj(this, a, a.ka().gc)
};
function Pj(a, b, c) {
  b = b.w;
  c.mo != p && (a.Dl = Math.max(a.Dl, (c.mo.te ? b.size : 1) * c.mo.td));
  c.vI && (a.Dl = Math.max(a.Dl, (c.Hh.Bu ? b.size : 1) * c.Hh.IA / 2), c = (c.Hh.vu ? b.size : 1) * c.Hh.xB, 0 > c && (a.Dl = Math.max(-c, a.Dl)))
}
z.qH = function(a) {
  if(a != p && C(a, "type")) {
    switch(Wb(a, "type")) {
      case "auto":
        return new gj(this);
      case "rectangular":
        return new dj(this);
      case "autorectangular":
        return new fj(this)
    }
  }
  return new Zi(this)
};
z.UB = function(a, b) {
  var c = new Fh(this);
  c.g(a, b);
  this.cz = Math.max(c.td, this.cz);
  this.Su = Math.min(c.Dj, this.Su);
  this.Pu = Math.max(c.Dj + c.Qp, this.Pu);
  return c
};
z.aD = x(1);
z.Rc = function(a) {
  Oj.f.Rc.call(this, a);
  this.frame.qB ? this.frame.Dw(this.zb) : (this.bz = a = Math.min(this.zb.width, this.zb.height), this.Pm = this.cz * a);
  this.oc.x = this.zb.x + this.Ll.x * this.zb.width;
  this.oc.y = this.zb.y + this.Ll.y * this.zb.height
};
function Fh(a) {
  this.Wb = a;
  this.td = 0.6;
  this.Qp = this.Dj = 90;
  this.Gm = this.Dj + this.Qp
}
A.e(Fh, di);
z = Fh.prototype;
z.Jg = function(a) {
  var b = new O, c = this.og(this.bf.za, this.bf.ga(), 0, q), d = a * Math.PI / 180, a = Math.round(1E3 * Math.cos(d)) / 1E3, d = Math.round(1E3 * Math.sin(d)) / 1E3, f = this.bf.ej().n(), g = f.width / 2, f = f.height / 2;
  switch(this.bf.za) {
    case 1:
      c += g * Math.abs(a) + f * Math.abs(d);
      break;
    case 0:
      c -= g * Math.abs(a) + f * Math.abs(d)
  }
  b.x = this.Zc().oc.x + c * a - g;
  b.y = this.Zc().oc.y + c * d - f;
  return b
};
z.g = function(a, b) {
  Fh.f.g.call(this, a, b);
  C(a, "radius") && (this.td = Qb(a, "radius"));
  C(a, "start_angle") && (this.Dj = M(a, "start_angle") + 90);
  C(a, "sweep_angle") && (this.Qp = M(a, "sweep_angle"));
  this.Gm = this.Dj + this.Qp
};
z.jr = function() {
  Fh.f.jr.call(this);
  this.bd = this.Wb.bz * this.td;
  this.lv = this.bd * this.size;
  var a = this.scale, b = this.Gm;
  a.ef = this.Dj;
  a.we = b;
  this.scale.IR = this.Qp / 10;
  this.scale.Ae()
};
function Gh(a, b, c, d) {
  a = a.Wb.oc;
  c = c * Math.PI / 180;
  d.x = a.x + b * Math.cos(c);
  d.y = a.y + b * Math.sin(c)
}
function Nh(a, b, c, d, f) {
  switch(b) {
    case 0:
      return a.bd * (1 - c - (f ? 0 : d)) - a.lv / 2;
    case 1:
      return a.bd * (1 + c + (f ? 0 : d)) + a.lv / 2;
    default:
      return a.bd * (1 + (f ? -0.5 : 0.5) * d - c)
  }
}
z.og = function(a, b, c, d) {
  switch(a) {
    case 0:
      return this.bd * (1 - (d ? 0 : c) - b) - (d ? c : 0) - this.lv / 2;
    case 1:
      return this.bd * (1 + b) + this.lv / 2;
    default:
      return this.bd * (1 - (d ? 0 : c / 2)) - (d ? c / 2 : 0)
  }
};
z.yo = function(a, b, c) {
  var d = this.cd.r(), f = a.Ka() * this.bd, g = a.li() * this.bd, k = Math.min(f, g), l = this.og(a.za, a.ga(), 0, q);
  switch(a.za) {
    case 0:
      l -= g / 2;
      break;
    case 1:
      l += g / 2
  }
  var n = this.Wb.oc, m = n.x + Uc(l, b) - f / 2, l = n.y + Vc(l, b) - g / 2, c = a.yo(d, m, l, k, f, g, c);
  a.bo ? c.Vl(b - 90 + a.Bb, m + f / 2, l + g / 2) : c.Vl(a.Bb);
  return c
};
z.mD = function(a) {
  var b = new P, c = this.Wb.oc, d = 0, d = 0;
  1 == a.ol() && (d = this.og(a.za, a.ga(), a.ec(), q), d += this.vd(1) * a.ec());
  b.x = c.x - d;
  b.y = c.y - d;
  b.width = 2 * d;
  b.height = 2 * d;
  return b
};
z.CI = function(a, b) {
  var c = this.Wb.oc, d = 1 == b.ol(), f;
  if(d) {
    f = this.og(b.za, b.ga(), b.ec(), q);
    var g = f + this.bd * b.ec()
  }else {
    f = this.og(b.za, b.ga(), 0, q)
  }
  var k, l, n;
  this.Gm % 360 != this.Dj % 360 ? (l = this.Gm * Math.PI / 180, k = this.Dj * Math.PI / 180) : (n = Math.min(this.Dj, this.Gm), l = (this.Gm - n - 0.001) * Math.PI / 180, k = (this.Dj - n) * Math.PI / 180);
  n = c.x + f * Math.cos(k);
  var m = c.y + f * Math.sin(k), o = c.x + f * Math.cos(l), v = c.y + f * Math.sin(l), w = c.x + g * Math.cos(k);
  k = c.y + g * Math.sin(k);
  var y = c.x + g * Math.cos(l), c = c.y + g * Math.sin(l);
  l = 1;
  180 > this.Qp && (l = 0);
  var E;
  E = "" + S(n, m);
  E += a.ub(f, l, 1, o, v, f);
  d && (E += U(y, c), E += a.ub(g, l, q, w, k, g), E += U(n, m));
  E += a.ub(f, l, q, n, m, f);
  return E + " Z"
};
z.td = p;
z.bd = p;
z.vd = function(a) {
  return this.bd * a
};
z.Dj = p;
z.Qp = p;
z.tc = p;
function mf() {
}
A.e(mf, gf);
z = mf.prototype;
z.Pk = s();
z.zp = s();
z.Xr = s();
z.xK = s();
z.Ik = s();
z.zk = s();
z.yK = s();
z.SH = s();
z.Ge = s();
z.Pa = x(4);
z.Na = x(p);
z.clear = s();
z.refresh = s();
z.N = p;
z.dA = t("N");
z.br = function() {
  return new wf
};
z.io = p;
z.Lx = p;
z.Rm = p;
z.TI = p;
z.g = function(a) {
  this.TI = new Dg;
  this.io = new Cg;
  this.Rm = [];
  this.Lx = {};
  var b = [], c, d, f, g, k = p, l, n, m, o, v = p;
  C(a, "circular_default_template") && (l = N(a, "circular_default_template"));
  C(a, "linear_default_template") && (n = N(a, "linear_default_template"));
  C(a, "label_default_template") && (m = N(a, "label_default_template"));
  C(a, "image_default_template") && (o = N(a, "image_default_template"));
  C(a, "indicator_default_template") && (v = N(a, "indicator_default_template"));
  C(a, "circular") && (c = I(a, "circular"), b.push(c));
  C(a, "linear") && (d = I(a, "linear"), b.push(d));
  C(a, "label") && (f = I(a, "label"), b.push(f));
  C(a, "image") && (g = I(a, "image"), b.push(g));
  C(a, "indicator") && (k = I(a, "indicator"), b.push(k));
  var w;
  if(c) {
    w = I(a, "circular_template");
    for(b = 0;b < c.length;b++) {
      Qj(this, "circular", c[b], w, Oj, l)
    }
  }
  if(d) {
    w = I(a, "linear_template");
    for(b = 0;b < d.length;b++) {
      Qj(this, "linear", d[b], w, Wi, n)
    }
  }
  if(f) {
    w = I(a, "label_template");
    for(b = 0;b < f.length;b++) {
      Qj(this, "label", f[b], w, p, m)
    }
  }
  if(g) {
    w = I(a, "image_template");
    for(b = 0;b < g.length;b++) {
      Qj(this, "image", g[b], w, p, o)
    }
  }
  if(k) {
    w = I(a, "indicator_template");
    for(b = 0;b < k.length;b++) {
      Qj(this, "indicator", k[b], w, p, v)
    }
  }
  for(b = 0;b < this.Rm.length;b++) {
    a = p, this.Rm[b].ZE != p && (a = this.Rm[b].ZE, a = this.Lx[a]), a ? a.children.push(this.Rm[b]) : this.io.children.push(this.Rm[b])
  }
};
function Qj(a, b, c, d, f, g) {
  f == p && Ke(b + " gauge");
  b = p;
  if(C(c, "name")) {
    if(b = J(c, "name"), a.Lx[b] != p) {
      return
    }
  }else {
    b = "___gauge__" + a.Rm.length
  }
  f = new f;
  f.Tl(a);
  var k = p;
  C(c, "pointers") && C(F(c, "pointers"), "pointer") && (k = I(F(c, "pointers"), "pointer"));
  c = a.TI.apply(c, d, g);
  k && (c.pointers.pointer = k);
  f.g(c);
  a.Rm.push(f);
  a.Lx[b] = f
}
z.F = p;
z.sl = t("F");
z.k = p;
z.n = u("k");
z.Rc = t("k");
z.Vt = u("k");
z.D = p;
z.fb = u("D");
z.p = function(a, b) {
  this.D = new W(this.F);
  this.k = a;
  this.D.ia(this.io.p(this.F, a, b));
  return this.D
};
z.qa = function() {
  this.io.qa()
};
z.nm = t("k");
z.Jq = function() {
  this.io.Wa(this.k)
};
z.gG = x(q);
z.vF = s();
z.ey = x(p);
z.mb = function(a) {
  a == p && (a = {});
  a.mM = [];
  for(var b = 0;b < this.io.PE();b++) {
    var c = this.io.children[b];
    c && a.mM.push(c.mb())
  }
  return a
};
z.Yw = x(q);
function Rj() {
}
Rj.prototype.ti = p;
function Sj(a, b) {
  b >= a.ti.length && (b %= a.ti.length);
  return a.ti[b]
}
Rj.prototype.g = function() {
  this.ti = []
};
Rj.prototype.Ca = function(a) {
  a == h && (a = new Rj);
  a.ti = this.ti;
  return a
};
function Tj() {
}
function Uj(a, b, c) {
  if(!a.Hc) {
    return p
  }
  for(var c = Cb(c), a = I(a.Hc, b), b = a.length, d = 0;d < b;d++) {
    if(C(a[d], "name") && N(a[d], "name") == c) {
      return a[d]
    }
  }
  return p
}
Tj.prototype.Hc = p;
Tj.prototype.g = t("Hc");
function Vj() {
}
A.e(Vj, Rj);
var Wj = {kL:0, gL:1};
z = Vj.prototype;
z.yb = p;
z.uu = q;
z.Sa = "";
z.Ra = 0;
z.um = "auto";
z.$I = q;
z.g = function(a) {
  Vj.f.g.call(this, a);
  C(a, "name") && (this.Sa = N(a, "name"));
  C(a, "color_count") && (this.um = F(a, "color_count"), Xb(this.um) ? this.$I = j : this.um = Ob(this.um));
  if(C(a, "type")) {
    var b;
    a: {
      switch(N(a, "type")) {
        case "colorrange":
          b = Wj.gL;
          break a;
        default:
          b = Wj.kL
      }
    }
    this.Ra = b
  }
  switch(this.Ra) {
    case 1:
      this.uu = j;
      this.um && !this.$I && (b = new yc(1), b.g(F(a, "gradient")), Xj(this, b, this.um), this.uu = q);
      this.yb = new yc(1);
      this.yb.g(F(a, "gradient"));
      break;
    default:
      a = I(a, "item");
      b = a.length;
      for(var c = 0;c < b;c++) {
        this.ti.push(Yb(a[c]))
      }
  }
};
function Yj(a, b, c) {
  var d = a;
  if(a.uu && !c || a.yb && c) {
    d = new Vj, d.ti = [], Xj(d, a.yb, b)
  }
  return d
}
function Xj(a, b, c) {
  var d;
  if(2 > c) {
    for(d = 0;d < c;d++) {
      b.pb[d] && a.ti.push(b.pb[d].Ba())
    }
  }
  for(d = 0;d < c;d++) {
    a.ti.push(Zj(b, d / (c - 1)))
  }
}
function Zj(a, b) {
  for(var c = -1, d = -1, f = 1E7, g = -1, k = a.pb.length, l = 0;l < k;l++) {
    var n = a.pb[l], m = 255 * n.Aa / 255;
    if(m == b) {
      return n.Ba()
    }
    m < b && m > g && (c = l, g = m);
    m > b && m < f && (d = l, f = m)
  }
  if(-1 == c) {
    return-1 == d ? zb(p, 1) : a.pb[d].Ba()
  }
  if(-1 == d || g == f) {
    return a.pb[c].Ba()
  }
  g = 255 * a.pb[c].Aa / 255;
  f = a.pb[d];
  d = 1 - (b - g) / (255 * a.pb[d].Aa / 255 - g);
  g = new fb;
  c = a.pb[c].Ba().Ba();
  f = f.Ba().Ba();
  g.arguments[0] = c;
  g.arguments[1] = f;
  g.arguments[2] = d;
  return new xb(g.execute())
}
z.Ca = function(a) {
  a == h && (a = new Vj);
  Vj.f.Ca.call(this, a);
  a.Sa = this.Sa;
  a.um = this.um;
  a.yb = this.yb;
  a.Ra = this.Ra;
  a.uu = this.uu;
  return a
};
function $j() {
}
A.e($j, Rj);
$j.prototype.g = function(a) {
  $j.f.g.call(this, a);
  for(var a = I(a, "item"), b = a.length, c = 0;c < b;c++) {
    this.ti.push(Gc(N(a[c], "type")))
  }
};
$j.prototype.Ca = function(a) {
  a == h && (a = new $j);
  $j.f.Ca.call(this, a);
  return a
};
function ak() {
}
A.e(ak, Rj);
ak.prototype.g = function(a) {
  ak.f.g.call(this, a);
  for(var a = I(a, "item"), b = a.length, c = 0;c < b;c++) {
    this.ti.push(Yf(N(a[c], "type")))
  }
};
ak.prototype.Ca = function(a) {
  a == h && (a = new ak);
  ak.f.Ca.call(this, a);
  return a
};
function bk() {
  this.b = new me
}
z = bk.prototype;
z.Sa = p;
z.getName = u("Sa");
z.b = p;
z.g = function(a) {
  C(a, "name") && (this.Sa = J(a, "name"), this.b.add("%ThresholdName", this.Sa))
};
z.jt = function() {
  A.xa()
};
z.ht = function() {
  A.xa()
};
z.ZI = x(q);
z.getData = function() {
  A.xa()
};
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  A.xa()
};
z.IF = function() {
  A.xa()
};
z.Na = function(a) {
  return this.b.get(a)
};
z.pr = x(q);
function ck(a) {
  a = a.split("{").join("");
  return a.split("}").join("")
}
function dk() {
  this.Xa = zb("0#FF0000", 1);
  this.Sa = "condition";
  this.ba = [];
  this.Ge()
}
z = dk.prototype;
z.fw = p;
z.dE = q;
z.gw = p;
z.eE = q;
z.hw = p;
z.fE = q;
z.Sa = p;
z.Xa = p;
z.Ba = u("Xa");
z.ib = p;
z.Zr = t("ib");
z.ba = p;
z.jm = p;
z.g = function(a) {
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "name") && (this.Sa = J(a, "name"));
  var b;
  C(a, "value_1") && (b = J(a, "value_1"), this.dE = qe(b), this.fw = b);
  C(a, "value_2") && (b = J(a, "value_2"), this.eE = qe(b), this.gw = b);
  C(a, "value_3") && (b = J(a, "value_3"), this.fE = qe(b), this.hw = b);
  if(C(a, "attributes")) {
    a = I(F(a, "attributes"), "attribute");
    this.jm = {};
    for(b = 0;b < a.length;b++) {
      var c = a[b];
      C(c, "name") && (this.jm["%Threshold" + J(c, "name")] = J(c, "value"), this.jm["%Condition" + J(c, "name")] = J(c, "value"))
    }
  }
  this.IF()
};
z.Ed = function(a) {
  var b = this.dE ? a.Na(ek(this.fw)) : this.fw, c = this.eE ? a.Na(ek(this.gw)) : this.gw, d = this.fE ? a.Na(ek(this.hw)) : this.hw;
  if(this.bl(isNaN(Number(b)) ? b : Number(b), isNaN(Number(c)) ? c : Number(c), isNaN(Number(d)) ? d : Number(d))) {
    a.Di(this.Xa), a.Cg = this, this.ba.push(a)
  }
};
function ek(a) {
  a = a.split("{").join("");
  return a.split("}").join("")
}
z.bl = function() {
  A.xa()
};
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%ThresholdConditionName", "");
  this.b.add("%ThresholdName", "")
};
z.Pa = x(1);
z.IF = function() {
  this.b.add("%Name", this.Sa);
  this.b.add("%ThresholdConditionName", this.Sa);
  this.b.add("%ThresholdName", this.Sa)
};
z.Na = function(a) {
  return this.jm && this.jm[a] ? this.jm[a] : this.b.get(a)
};
z.pr = function(a) {
  return Boolean(this.jm && this.jm[a])
};
z.ah = s();
z.dh = s();
function fk() {
  dk.apply(this)
}
A.e(fk, dk);
fk.prototype.bl = function(a, b, c) {
  return a >= b && a <= c
};
function gk() {
  dk.apply(this)
}
A.e(gk, dk);
gk.prototype.bl = function(a, b, c) {
  return a < b || a > c
};
function hk() {
  dk.apply(this)
}
A.e(hk, dk);
hk.prototype.bl = function(a, b) {
  return a == b
};
function ik() {
  dk.apply(this)
}
A.e(ik, dk);
ik.prototype.bl = function(a, b) {
  return a != b
};
function jk() {
  dk.apply(this)
}
A.e(jk, dk);
jk.prototype.bl = function(a, b) {
  return a > b
};
function kk() {
  dk.apply(this)
}
A.e(kk, dk);
kk.prototype.bl = function(a, b) {
  return a >= b
};
function lk() {
  dk.apply(this)
}
A.e(lk, dk);
lk.prototype.bl = function(a, b) {
  return a < b
};
function mk() {
  dk.apply(this)
}
A.e(mk, dk);
mk.prototype.bl = function(a, b) {
  return a <= b
};
function nk() {
  bk.apply(this)
}
A.e(nk, bk);
z = nk.prototype;
z.vm = p;
z.g = function(a) {
  nk.f.g.call(this, a);
  this.vm = [];
  for(var a = I(a, "condition"), b = 0;b < a.length;b++) {
    var c;
    c = a[b];
    if(C(c, "type")) {
      var d = h;
      switch(N(c, "type")) {
        default:
        ;
        case "lessthan":
          d = new lk;
          break;
        case "between":
          d = new fk;
          break;
        case "notbetween":
          d = new gk;
          break;
        case "equalto":
          d = new hk;
          break;
        case "notequalto":
          d = new ik;
          break;
        case "greaterthan":
          d = new jk;
          break;
        case "greaterthanorequalto":
          d = new kk;
          break;
        case "lessthanorequalto":
          d = new mk
      }
      d.Zr(this);
      d.g(c);
      c = d
    }else {
      c = p
    }
    c && this.vm.push(c)
  }
};
z.jt = function(a) {
  for(var b = 0;b < this.vm.length && !this.vm[b].Ed(a);b++) {
  }
};
z.ht = s();
z.getData = function(a) {
  for(var b = this.vm.length, c = 0;c < b;c++) {
    var d = this.vm[c];
    ok(a, new pk(d.Ba(), 0, -1, j, d))
  }
};
z.Pa = x(1);
z.pr = function(a) {
  for(var b = this.vm.length, c = 0;c < b;c++) {
    if(this.vm[c].pr(a)) {
      return j
    }
  }
  return q
};
function qk() {
  this.ba = [];
  this.Ge()
}
z = qk.prototype;
z.Tb = p;
z.setStart = t("Tb");
z.Lb = p;
z.setEnd = t("Lb");
z.ba = p;
z.ze = p;
z.contains = function(a) {
  return a >= this.Tb && a < this.Lb
};
z.b = p;
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%ThresholdRangeMin", 0);
  this.b.add("%ThresholdRangeMax", 0);
  this.b.add("%ThresholdValue", 0)
};
z.IF = function() {
  this.b.add("%ThresholdRangeMin", this.Tb);
  this.b.add("%ThresholdRangeMax", this.Lb)
};
z.Na = function(a) {
  return"%RangeMin" == a ? this.Tb : "%RangeMax" == a ? this.Lb : this.b.get(a)
};
z.Pa = x(2);
z.pr = x(q);
z.ah = s();
z.dh = s();
function rk() {
  bk.apply(this)
}
A.e(rk, bk);
z = rk.prototype;
z.ze = p;
z.ie = p;
z.he = p;
z.hf = p;
z.Vc = p;
z.ot = p;
z.wb = p;
z.g = function(a) {
  rk.f.g.call(this, a);
  this.ie = Number.MAX_VALUE;
  this.he = -Number.MAX_VALUE;
  this.wb = C(a, "range_count") ? M(a, "range_count") : 5;
  this.ze = C(a, "auto_value") ? J(a, "auto_value") : "%Value"
};
z.p = function() {
  this.ie == Number.MAX_VALUE && (this.ie = 0);
  this.he == -Number.MAX_VALUE && (this.he = 10);
  this.Vc = []
};
z.ht = function(a) {
  a = sk(a, this.ze);
  a < this.ie && (this.ie = a);
  a > this.he && (this.he = a)
};
function tk(a) {
  for(var b = (a.he - a.ie) / a.wb, c = 0;c < a.wb;c++) {
    var d = new qk;
    d.setStart(0 == c ? a.ie + b * c : a.Vc[c - 1].Lb);
    d.setEnd(d.Tb + b);
    d.ze = a.ze;
    a.Vc[c] = d
  }
}
function uk(a, b) {
  var c = sk(b, a.ze);
  if(c < a.ie || c > a.he) {
    return-1
  }
  for(var d = 0;d < a.wb;d++) {
    if(a.Vc[d] && a.Vc[d].contains(c)) {
      return d
    }
  }
  return c == a.Vc[a.wb - 1].Lb ? a.wb - 1 : -1
}
function sk(a, b) {
  var c = a.Na(ck(b));
  if("" == c) {
    return p
  }
  c = Ob(c);
  return isNaN(c) ? p : c
}
z.ZI = x(j);
z.getData = function(a) {
  for(var b = this.Vc.length, c = 0;c < b;c++) {
    ok(a, new pk(Sj(this.ot, c), 0, -1, j, this.Vc[c]))
  }
};
function vk(a, b) {
  a && (this.j = a);
  b && (this.Be = b)
}
vk.prototype.j = p;
vk.prototype.Ul = t("j");
vk.prototype.Be = p;
function wk() {
  rk.apply(this);
  this.Kc = []
}
A.e(wk, rk);
wk.prototype.Kc = p;
wk.prototype.jt = function(a) {
  var b = uk(this, a);
  -1 != b && (a.Cg = this.Vc[b], this.Vc[b].ba.push(a), a.Di(Sj(this.ot, b)))
};
wk.prototype.ht = function(a) {
  var b = sk(a, this.ze);
  b < this.ie && (this.ie = b);
  b > this.he && (this.he = b);
  isNaN(b) || this.Kc.push(new vk(a, b))
};
function xk(a, b, c) {
  a += 0.5 * (b - a);
  if(0 == a) {
    return 0
  }
  b = Math.floor(Math.log(Math.abs(a)) / Math.log(10));
  b = Math.pow(10, Number(b - 1));
  a /= b;
  a = c ? Math.floor(a) : Math.ceil(a);
  return a * b
}
function yk() {
  rk.apply(this)
}
A.e(yk, rk);
yk.prototype.p = function() {
  yk.f.p.call(this);
  if(this.ie != this.he) {
    var a = Math.pow(10, Math.log(Math.max(Math.abs(this.ie), Math.abs(this.he))) / Math.log(10) - 1);
    this.ie = Math.floor(this.ie / a);
    var b = 10 * this.ie;
    this.ie *= a;
    this.he = Math.ceil(this.he / a);
    this.he += Number(this.wb - (10 * this.he - b) % this.wb) / 10;
    this.he *= a
  }
  this.hf = (this.he - this.ie) / this.wb;
  tk(this)
};
yk.prototype.jt = function(a) {
  var b = uk(this, a);
  -1 != b && (this.Vc[b].ba.push(a), a.Cg = this.Vc[b], b = ck(this.ze), b = a.b.get(b), a.Di(Sj(this.ot, Math.floor((b - this.ie) / this.hf))))
};
function zk() {
  wk.apply(this)
}
A.e(zk, wk);
zk.prototype.p = function() {
  zk.f.p.call(this);
  this.Kc.sort(function(a, b) {
    return a.Be - b.Be
  });
  var a = this.wb;
  if(0 == this.Kc.length) {
    tk(this)
  }else {
    if(a > this.Kc.length && (this.wb = a = this.Kc.length), 2 > a) {
      this.wb = a, this.ie = xk(this.ie, this.ie, j), this.he = xk(this.he, this.he, q), tk(this)
    }else {
      var b = this.Kc.length / a, c = b, d = this.Kc[0].Be, f = this.Kc[Math.round(c - 1)].Be, g = this.Kc[Math.round(c)].Be, k = new qk;
      k.setStart(xk(d, d, j));
      d = xk(f, g, j);
      k.setEnd(d);
      k.ze = this.ze;
      this.Vc[0] = k;
      for(k = 1;k < a - 1;k++) {
        c += b, this.Vc[k] = new qk, this.Vc[k].ze = this.ze, this.Vc[k].setStart(d), d = xk(this.Kc[Math.round(c - 1)].Be, this.Kc[Math.round(c)].Be, j), this.Vc[k].setEnd(d)
      }
      this.Vc[this.wb - 1] = new qk;
      this.Vc[this.wb - 1].setStart(d);
      this.Vc[this.wb - 1].ze = this.ze;
      this.Vc[this.wb - 1].setEnd(xk(this.Kc[this.Kc.length - 1].Be, this.Kc[this.Kc.length - 1].Be, q))
    }
  }
};
function Ak() {
  wk.apply(this)
}
A.e(Ak, wk);
Ak.prototype.p = function() {
  Ak.f.p.call(this);
  this.Kc.sort(function(a, b) {
    return a.Be - b.Be
  });
  if(0 == this.Kc.length) {
    tk(this)
  }else {
    var a = Math.min(this.wb, this.Kc.length);
    if(4 >= a) {
      this.wb = Math.max(this.wb, 1), tk(this)
    }else {
      if(a > this.Kc.length - 3 && (this.wb = this.Kc.length), 4 > a) {
        this.wb = Math.max(this.wb, 1), tk(this)
      }else {
        for(var a = [], b = 0;b < this.Kc.length;b++) {
          a.push(this.Kc[b].Be)
        }
        a = Bk(a, this.wb);
        b = new qk;
        b.ze = this.ze;
        var c = this.Kc[0].Be;
        b.setStart(xk(c, c, j));
        var c = this.Kc[a[0] - 1].Be, d = this.Kc[a[0]].Be, c = xk(c, d, j);
        b.setEnd(c);
        this.Vc[0] = b;
        for(var f = 1;f < Math.min(this.wb - 1, a.length);f++) {
          b = new qk, b.ze = this.ze, b.setStart(c), c = this.Kc[a[f] - 1].Be, d = this.Kc[a[f]].Be, c = xk(c, d, j), b.setEnd(c), this.Vc[f] = b
        }
        b = new qk;
        b.ze = this.ze;
        b.setStart(c);
        b.setEnd(xk(this.Kc[this.Kc.length - 1].Be, this.Kc[this.Kc.length - 1].Be, q));
        this.Vc[this.wb - 1] = b
      }
    }
  }
};
function Bk(a, b) {
  for(var c = [], d = [], f = 0;f < a.length + 1;f++) {
    c.push(Array(b + 1)), d.push(Array(b + 1))
  }
  for(f = 1;f <= b;f++) {
    c[1][f] = 1;
    d[1][f] = 0;
    for(var g = 2;g <= a.length;g++) {
      d[g][f] = Number.POSITIVE_INFINITY
    }
  }
  for(var k = 0, l = 2;l <= a.length;l++) {
    for(var n = 0, m = 0, o = 0, v = 1;v <= l;v++) {
      if(f = l - v, g = Number(a[f]), m += g * g, n += g, o++, k = m - n * n / o, g = f, 0 != g) {
        for(var w = 2;w <= b;w++) {
          d[l][w] >= k + d[g][w - 1] && (c[l][w] = f + 1, d[l][w] = k + d[g][w - 1])
        }
      }
    }
    c[l][1] = 1;
    d[l][1] = k
  }
  d = [];
  g = a.length;
  for(l = b;2 <= l;l--) {
    f = c[g][l] - 2, 0 < f && d.splice(0, 0, f), g = Math.max(c[g][l] - 1, 0)
  }
  d.push(a.length - 1);
  return d
}
function Ck(a) {
  this.iG = a;
  this.xz = {};
  this.$E = [];
  this.rB = {};
  this.Ew = []
}
z = Ck.prototype;
z.iG = p;
z.xz = p;
z.$E = p;
z.Ew = p;
z.rB = p;
function Dk(a) {
  for(var b = 0;b < a.Ew.length;b++) {
    a.Ew[b].p()
  }
}
z.Kg = function(a, b) {
  if(!this.iG) {
    return p
  }
  a = Cb(a);
  if(this.xz[a]) {
    return this.xz[a]
  }
  var c = fc(this.iG, a);
  if(!c) {
    return p
  }
  var d;
  switch(N(c, "type")) {
    default:
    ;
    case "custom":
      d = new nk;
      break;
    case "equalinterval":
    ;
    case "equalsteps":
      d = new yk;
      break;
    case "quantiles":
    ;
    case "equaldistribution":
      d = new zk;
      break;
    case "optimal":
    ;
    case "absolutedeviation":
      d = new Ak
  }
  if(!d) {
    return p
  }
  var f = a;
  d.g(c);
  if(d.ZI()) {
    var g = new Vj, k;
    C(c, "palette") && (c = F(c, "palette"), k = b ? Uj(b, "palette", c) : p);
    k || (k = Uj(b, "palette", "default"));
    g.g(k);
    c = Yj(g, d.wb, j);
    d.ot = c;
    this.Ew.push(d);
    this.rB[f] = d
  }
  this.$E.push(d);
  return this.xz[a] = d
};
function Ek() {
  hf.apply(this);
  this.yw = this.im = q;
  this.qk = j;
  this.Ua = [];
  this.Kh = {};
  this.Hp = {};
  this.jy = {};
  this.o = [];
  this.rp = [];
  this.BJ = new Fk(this)
}
A.e(Ek, hf);
z = Ek.prototype;
z.ck = 0;
z.Jo = u("ck");
z.o = p;
z.rp = p;
function Gk(a) {
  return a.o ? a.o.length : 0
}
function Hk(a, b) {
  return a.o ? a.o[b] : p
}
z.Gd = x(p);
z.jy = p;
z.dy = aa();
z.Nx = function(a, b, c, d) {
  var f = Ik(this, c);
  if(f) {
    return f
  }
  f = this.kx(c);
  if(f == p) {
    return p
  }
  f.Tl(this);
  f.dA(this.N);
  Jk(f, b);
  f.il(b);
  a = F(a, f.Ah());
  f.il(a);
  Jk(f, a);
  f.g(a, d);
  f.Eq(a, d);
  f.Cm(a, d);
  this.jy[this.dy(this.dy(c))] = f;
  this.qk || f.ND();
  return f
};
function Ik(a, b) {
  var c = a.dy(b);
  return a.jy[c] ? a.jy[c] : p
}
function Kk(a, b, c, d) {
  var f = p;
  C(c, b) && (f = Uj(d, b, F(c, b)));
  f == p && (f = Uj(d, b, "default"));
  a = new a;
  a.g(f);
  return a
}
function Lk(a, b, c, d, f, g) {
  if(C(f, b)) {
    if(b = Uj(g, b, F(f, b)), b != p) {
      return a = new a, a.g(b), a
    }
  }else {
    return d.Bw ? c : p
  }
  return p
}
z.Li = p;
z.BJ = p;
hf.prototype.SH = function(a, b, c, d, f) {
  b.Ba() || b.Di(a.color);
  a = Mc(c.r(), d, f, a.Ka(), a.vb());
  d = "stroke:none;";
  d = b.Ba() ? d + ("fill:" + ub(b.Ba().Ba()) + ";") : d + "fill:none;";
  a.setAttribute("style", d);
  c.appendChild(a)
};
z = Ek.prototype;
z.Yz = p;
z.Dq = function(a) {
  Ek.f.Dq.call(this, a);
  C(a, "default_series_type") && (this.ck = this.Jo(Cb(F(a, "default_series_type"))));
  C(a, "ignore_missing") && (this.qk = K(a, "ignore_missing"));
  jf(this, a)
};
z.wx = s();
z.RG = function(a, b) {
  return b
};
z.SE = function(a) {
  this.qk || a.Ab().WI(a);
  if(a.Ab().Yo && a.Ab().$s()) {
    Mk(a.Ab(), a);
    for(var b = 0;b < a.Fe();b++) {
      a.Ga(b).Vr(b)
    }
  }
  a.th(this)
};
z.Yw = function(a) {
  if(!C(a, "data")) {
    return j
  }
  a = F(a, "data");
  return!C(a, "series") ? j : 0 == I(a, "series").length
};
z.vx = function(a, b) {
  Ek.f.vx.call(this, a);
  this.Li = new Ck(F(a, "thresholds"));
  var c = new Tj;
  C(a, "palettes") && c.g(F(a, "palettes"));
  var d = F(a, "data"), f = F(a, "data_plot_settings"), g = I(d, "series"), k = g.length, l = Kk(Vj, "palette", d, c), n = Kk(ak, "marker_palette", d, c), m = Kk($j, "hatch_palette", d, c), l = l ? Yj(l, k) : p, o = p;
  C(d, "threshold") && (o = this.Li.Kg(N(d, "threshold"), c));
  for(var v = 0;v < k;v++) {
    var w = g[v];
    if(!C(w, "visible") || K(w, "visible")) {
      var y = C(w, "type") ? this.Jo(N(w, "type")) : this.ck, E = this.Nx(f, d, y, b);
      if(E != p) {
        y = E.uh(y);
        y.Vr(v);
        y.Rl("Series " + v.toString());
        Nk(y, w);
        y.cb = E;
        Ok(y, E.Qe);
        Jk(y, w);
        if(C(w, "missing_points")) {
          var V = J(w, "missing_points");
          V && (y.GE = V)
        }
        y.rf(E.ka());
        y.Eq(w, b);
        Pk(y, E);
        y.il(w);
        C(w, "color") ? y.Di(Yb(w)) : y.Di(Sj(l, v));
        C(w, "hatch_type") ? y.Ip(Gc(N(w, "hatch_type"))) : y.Ip(Sj(m, v));
        y.ge = C(w, "marker_type") ? Yf(N(w, "marker_type")) : Sj(n, v);
        C(w, "threshold") ? y.Zr(this.Li.Kg(N(w, "threshold"), c)) : y.Zr(o);
        E.Mh(y);
        y.Cm(w, b);
        y.g(w);
        this.wx(y, w);
        var V = Lk(Vj, "palette", l, E, w, c), R = Lk(ak, "marker_palette", n, E, w, c), T = Lk($j, "hatch_palette", n, E, w, c);
        H(w, "point", this.RG(y, I(w, "point")));
        for(var ca = y.kB(), w = I(w, "point"), ra = w.length, V = V ? Yj(V, ra) : p, Qc = y.GE ? y.GE : p, yb = 0;yb < ra;yb++) {
          var Y = w[yb];
          "no_paint" == Qc && !Y.y && !Y.start && !Y.end && (Y.y = 0, Y.ap = j);
          var L = y.qe();
          L.cb = E;
          L.rf(y.ka());
          L.Eq(Y, b);
          Ok(L, y.Qe);
          Jk(L, Y);
          Pk(L, y);
          L.il(Y);
          L.Vr(yb);
          L.o = y;
          L.Rl(yb.toString());
          Nk(L, Y);
          L.g(Y, this.F);
          if(!L.Gb || E.ZG()) {
            this.qk || E.cH(L);
            y.Mh(L);
            L.Cm(Y, b);
            L.Di(y.Ba());
            L.Ip(y.Ze);
            L.ge = y.ng();
            C(Y, "color") ? L.Di(Yb(Y)) : L.Di(V ? Sj(V, L.Nr) : y.Ba());
            C(Y, "hatch_type") ? L.Ip(Gc(N(Y, "hatch_type"))) : L.Ip(T ? Sj(T, L.Nr) : y.Ze);
            L.ge = C(Y, "marker_type") ? Yf(N(Y, "marker_type")) : R ? Sj(R, L.Nr) : y.ng();
            Y.ap && (L.ge = -1);
            C(Y, "threshold") ? L.Zr(this.Li.Kg(N(Y, "threshold"), c)) : L.Zr(y.Kg());
            y.Od(L, L.na);
            L.Gb || y.Ed(L);
            if(ca && (!L.Gb || !this.qk)) {
              L.ib != p && L.Kg().ht(L), this.Ua.push(L)
            }
            Y.ap && (L.ap = j)
          }
        }
        this.SE(y);
        y.tn() && this.rp.push(y);
        this.o.push(y)
      }
    }
  }
  this.Li && Dk(this.Li);
  this.b.add("%DataPlotSeriesCount", this.o.length);
  if(this.im && this.yw && (c = this.F.Ic(), this.im && this.yw && this.N.YH && (d = new Qk(this)))) {
    A.I.nb(c, ka.Xn, d.OM, q, this), A.I.nb(c, ka.fm, d.NM, q, this), A.I.nb(c, ka.Ns, d.PM, q, this), A.I.nb(this.N, "anychartRefresh", d.KM, q, this)
  }
};
z.Pa = function(a) {
  return"%DataPlotSeriesCount" == a ? 2 : Ek.f.Pa.call(this, a)
};
z.br = x(p);
z.qk = j;
z.yw = q;
z.im = q;
z.Kf = p;
z.Fk = p;
z.DI = function(a, b) {
  for(var c = [], d = 0;d < b.length;d++) {
    var f = b[d];
    f.jn(a) && c.push(f)
  }
  return c
};
function Qk(a) {
  function b(b) {
    window._anychart_svg_tmp = a.F.Ic();
    b.Im.preventDefault();
    var c = eval("_anychart_svg_tmp.createSVGPoint()");
    window._anychart_svg_pt_tmp = c;
    b = b.Im;
    c.x = b.clientX;
    c.y = b.clientY;
    window._anychart_svg_tt_tmp = eval("_anychart_svg_tmp.getScreenCTM().inverse()");
    c = eval("_anychart_svg_pt_tmp.matrixTransform(_anychart_svg_tt_tmp)");
    window._anychart_svg_pt_tmp = h;
    window._anychart_svg_tt_tmp = h;
    window._anychart_svg_tmp = h;
    return c
  }
  function c() {
    return 0 < k.width && 0 < k.height ? k : {x:0 > k.width ? k.x + k.width : k.x, y:0 > k.height ? k.y + k.height : k.y, width:0 > k.width ? -k.width : k.width, height:0 > k.height ? -k.height : k.height}
  }
  function d(a) {
    a = b(a);
    k.width = a.x - k.x;
    k.height = a.y - k.y
  }
  var f, g = q, k = {x:0, y:0, width:0, height:0}, l = p;
  this.OM = function(b) {
    f && (d(b), g ? l ? (b = c(), l.setAttribute("x", b.x), l.setAttribute("y", b.y), l.setAttribute("width", b.width), l.setAttribute("height", b.height)) : g = f = q : (l || (b = c(), l = Mc(a.F, b.x, b.y, b.width, b.height), l.setAttribute("style", "fill:rgb(180, 216, 247);stroke-width=1;stroke:rgb(0,0,0);fill-opacity:0.5;stroke-opacity:0.5;"), a.N.dt.appendChild(l)), l.setAttribute("visibility", "visible"), g = j))
  };
  this.NM = function(a) {
    f = j;
    a = b(a);
    k = {x:a.x, y:a.y, width:0, height:0}
  };
  this.PM = function(b) {
    if(l) {
      if(f = q, g) {
        d(b);
        g = q;
        var b = a.N.hb.aa.Ua, m = c(), o = a.N.hb.aa.k;
        m.x -= o.x;
        m.y -= o.y;
        m = a.DI(new P(m.x, m.y, m.width, m.height), b, l);
        l.setAttribute("visibility", "hidden");
        l.setAttribute("x", 0);
        l.setAttribute("y", 0);
        l.setAttribute("width", 0);
        l.setAttribute("height", 0);
        k = {x:0, y:0, width:0, height:0};
        if(m.length && (b = a.N.hb.aa, b.im && m && m.length)) {
          b.Kf == p && (b.Kf = []);
          for(o = 0;o < m.length;o++) {
            var v = m[o];
            v.ap || (v.select(j, j), b.Kh[h] = Rk(v.VR, v.LR), b.Kf.push(v))
          }
          if(b.Kf && b.Kf.length) {
            b.N.dispatchEvent(new Ie("multiplePointsSelect", b.Kf));
            for(m = 0;m < b.Kf.length;m++) {
              b.Kf[m].update()
            }
          }
        }
      }
    }else {
      g = f = q
    }
  };
  this.KM = function() {
    l = p
  }
}
z.uj = function(a, b) {
  this.im ? (this.Kf == p && (this.Kf = []), -1 == this.Kf.indexOf(a) && this.Kf.push(a)) : this.Fk != a && (this.Fk != p && this.Fk.ux(b), this.Fk = a, b && this.Fk.update())
};
z.p = function(a, b, c) {
  Ek.f.p.call(this, a, b, c);
  a = 0;
  for(b = this.Ua.length;a < b;a++) {
    this.vt.appendChild(this.Ua[a].p(this.F).G)
  }
  return this.D
};
z.zp = function() {
  if(this.QD && !this.gF && (this.gF = j, this.Fk && this.Fk.Qe && this.Fk.Qe.execute(this.Fk), this.Kf)) {
    for(var a = this.Kf.length, b = 0;b < a;b++) {
      this.Kf[b].Qe && this.Kf[b].Qe.execute(this.Kf[b])
    }
  }
};
z.Ua = p;
z.qa = function() {
  Ek.f.qa.call(this);
  a: {
    var a, b;
    if(this.o && this.o.length) {
      for(a = 0;a < this.o.length;a++) {
        if(b = this.o[a], b instanceof Sk && b.ba && 1 == b.ba.length) {
          break a
        }
      }
    }
    if(!(1 == this.Ua.length && this.Ua[0].o instanceof Sk)) {
      a = 0;
      for(b = this.rp.length;a < b;a++) {
        this.rp[a].Ug()
      }
      a = 0;
      for(b = this.Ua.length;a < b;a++) {
        var c = this.Ua[a];
        c.ib && c.ib.jt(c);
        this.Ua[a].update()
      }
    }
  }
};
z.Jq = function() {
  Ek.f.Jq.call(this);
  this.Oz()
};
z.Oz = function() {
  var a, b;
  a = 0;
  for(b = this.rp.length;a < b;a++) {
    this.rp[a].zn()
  }
  a = 0;
  for(b = this.Ua.length;a < b;a++) {
    this.Ua[a].Wa()
  }
};
z.mb = function(a) {
  a = Ek.f.mb.call(this, a);
  a.Series = [];
  for(var b = 0, c = Gk(this), d = 0;d < c;d++) {
    a.Series.push(this.o[d].mb()), b += this.o[d].Fe()
  }
  a.SeriesCount = c;
  a.PointCount = b;
  return a
};
z.Od = function(a, b) {
  if(a && b) {
    var c = Tk(this, a);
    if(c) {
      if(b = Uk(b), c = Vk(c), A.isArray(b)) {
        for(var d = b.length, f = 0;f < d;f++) {
          c.push(b[f]), this.Kh[Rk(a, b)] = b
        }
      }else {
        A.gn(b) && (c.push(b), this.Kh[Rk(a, b)] = b)
      }
    }
  }
};
z.Lj = function(a, b, c) {
  if(a && !(b == p || b == h || !c)) {
    var d = Tk(this, a);
    if(d) {
      var c = Uk(c), f = Vk(d), f = ic(f, b, c);
      this.Kh[Rk(a, c)] = c;
      H(d, "point", f)
    }
  }
};
z.Ck = function(a, b) {
  var c = Tk(this, a);
  if(c) {
    for(var c = jc(Vk(c), b), d = c.length, f = 0;f < d;f++) {
      var g = Rk(a, c[f]);
      this.Kh[g] && delete this.Kh[g]
    }
  }
};
z.Ok = function(a, b, c) {
  var d;
  if(!a || !b) {
    d = p
  }else {
    if(d = Rk(a, b), this.Kh[d]) {
      d = this.Kh[d]
    }else {
      var f = Tk(this, a), f = I(f, "point"), f = hc(f, "id", b);
      d = this.Kh[d] = f
    }
  }
  d && (c = Uk(c), kc(d, c), this.Kh[h] = Rk(a, b))
};
z.Pk = s();
z.nk = function(a, b, c) {
  var d = Wk(this, a, b);
  d && (c ? d.ah() : d.dh(), this.Kh[h] = Rk(a, b))
};
z.wK = function(a, b, c) {
  var d = Wk(this, a, b);
  if(!d) {
    return p
  }
  c ? d.select(j) : d.ux(j);
  this.Kh[h] = Rk(a, b);
  return d
};
function Wk(a, b, c) {
  a = Xk(a, b);
  if(!a) {
    return p
  }
  for(var b = a.Fe(), d = 0;d < b;d++) {
    var f = a.Ga(d);
    if(f.Zt() && f.Zt() == c) {
      return f
    }
  }
  return p
}
z.Mj = function(a) {
  if(a) {
    var a = Uk(a), b = Yk(this);
    if(A.isArray(a)) {
      for(var c = a.length, d = 0;d < c;d++) {
        b.push(a[d]), Zk(this, a[d])
      }
    }else {
      A.gn(a) && (b.push(a), Zk(this, a))
    }
    $k(this, b)
  }
};
z.Nj = function(a, b) {
  if(!(a == p || a == h || !b)) {
    var b = Uk(b), c = Yk(this);
    ic(c, a, b);
    $k(this, c);
    Zk(this, b)
  }
};
z.Dk = function(a) {
  if(!(a == p || a == h)) {
    var b = Yk(this);
    jc(b, a);
    $k(this, b);
    this.Hp[a] && delete this.Hp[a]
  }
};
z.Qk = function(a, b) {
  if(a && b) {
    var b = Uk(b), c = Tk(this, a);
    c && (kc(c, b), Zk(this, b, a))
  }
};
z.Jk = function(a, b) {
  if(a && !(b == p || b == h)) {
    var c = Tk(this, a);
    c && (H(c, "visible", Cb(b)), Zk(this, c, a))
  }
};
z.ok = function(a, b) {
  if(a && !(b == p || b == h)) {
    var c = Xk(this, a);
    c && (b ? c.ah(j) : c.dh(j))
  }
};
function Xk(a, b) {
  for(var c = Gk(a), d = 0;d < c;d++) {
    var f = Hk(a, d);
    if(f.Zt() && f.Zt() == b) {
      return f
    }
  }
  return p
}
z.clear = function() {
  H(F(this.Tj, "data"), "series", [])
};
z.refresh = function() {
  this.qm.Gk(this.Tj)
};
z.Hp = p;
function Tk(a, b) {
  if(!b) {
    return p
  }
  if(a.Hp[b]) {
    return a.Hp[b]
  }
  var c = Yk(a), d = hc(c, "id", b);
  $k(a, c);
  Zk(a, d, b);
  return d
}
function Zk(a, b, c) {
  c ? a.Hp[c] = b : C(b, "id") && (a.Hp[F(b, "id")] = b)
}
function Yk(a) {
  var b, c;
  C(a.Tj, "data") ? b = F(a.Tj, "data") : (b = {}, H(a.Tj, "data", b));
  C(b, "series") ? c = I(b, "series") : H(b, "series", []);
  return c
}
function Vk(a) {
  if(C(a, "point")) {
    return I(a, "point")
  }
  var b = [];
  H(a, "point", b);
  return b
}
function $k(a, b) {
  H(F(a.Tj, "data"), "series", b)
}
z.Kh = p;
function Rk(a, b) {
  var c, d;
  c = A.gn(a) ? J(a, "id") : a;
  A.gn(b) ? c = J(b, "id") : d = b;
  return!a || !b ? p : "series:" + c + ":point:" + d
}
function Uk(a) {
  if(A.Qc(a)) {
    return Jb(a)
  }
  if(A.gn(a)) {
    return a
  }
}
;var al = {chart:{palettes:{hatch_palette:{item:[{type:"backwardDiagonal"}, {type:"forwardDiagonal"}, {type:"horizontal"}, {type:"vertical"}, {type:"dashedBackwardDiagonal"}, {type:"grid"}, {type:"dashedForwardDiagonal"}, {type:"dashedHorizontal"}, {type:"dashedVertical"}, {type:"diagonalCross"}, {type:"diagonalBrick"}, {type:"divot"}, {type:"horizontalBrick"}, {type:"verticalBrick"}, {type:"checkerboard"}, {type:"confetti"}, {type:"plaid"}, {type:"solidDiamond"}, {type:"zigzag"}, {type:"weave"}, 
{type:"percent05"}, {type:"percent10"}, {type:"percent20"}, {type:"percent25"}, {type:"percent30"}, {type:"percent40"}, {type:"percent50"}, {type:"percent60"}, {type:"percent70"}, {type:"percent75"}, {type:"percent80"}, {type:"percent90"}], name:"default"}, marker_palette:{item:[{type:"Circle"}, {type:"Square"}, {type:"Diamond"}, {type:"Star4"}, {type:"Star5"}, {type:"Star7"}, {type:"TriangleUp"}, {type:"TriangleDown"}, {type:"Cross"}, {type:"DiagonalCross"}, {type:"HLine"}, {type:"VLine"}, {type:"Star6"}, 
{type:"Star10"}], name:"default"}, palette:{item:[{color:"#1D8BD1"}, {color:"#F1683C"}, {color:"#2AD62A"}, {color:"#DBDC25"}, {color:"#8FBC8B"}, {color:"#D2B48C"}, {color:"#FAF0E6"}, {color:"#20B2AA"}, {color:"#B0C4DE"}, {color:"#DDA0DD"}, {color:"#9C9AFF"}, {color:"#9C3063"}, {color:"#FFFFCE"}, {color:"#CEFFFF"}, {color:"#630063"}, {color:"#FF8284"}, {color:"#0065CE"}, {color:"#CECFFF"}, {color:"#000084"}, {color:"#FF00FF"}, {color:"#FFFF00"}, {color:"#00FFFF"}, {color:"#840084"}, {color:"#840000"}, 
{color:"#008284"}, {color:"#0000FF"}, {color:"#00CFFF"}, {color:"#CEFFFF"}, {color:"#CEFFCE"}, {color:"#FFFF9C"}, {color:"#9CCFFF"}, {color:"#FF9ACE"}, {color:"#CE9AFF"}, {color:"#FFCF9C"}, {color:"#3165FF"}, {color:"#31CFCE"}, {color:"#9CCF00"}, {color:"#FFCF00"}, {color:"#FF9A00"}, {color:"#FF6500"}], name:"default"}}, styles:{animation_style:[{name:"anychart_default", enabled:"true", interpolation_type:"Cubic"}, {name:"defaultLabel", enabled:"True", start_time:"1.2", duration:"0.5", interpolation_type:"Cubic"}, 
{name:"defaultMarker", enabled:"True", start_time:"1.2", duration:"0.5", type:"Appear", interpolation_type:"Cubic"}, {name:"defaultScaleYBottom", enabled:"True", start_time:"0.2", type:"ScaleYBottom", duration:"1", interpolation_type:"Cubic"}, {name:"defaultSideYBottom", enabled:"True", start_time:"0.2", type:"SideFromBottom", duration:"1", interpolation_type:"Cubic"}, {name:"defaultScaleYCenter", enabled:"True", start_time:"0.2", type:"ScaleYCenter", duration:"1", interpolation_type:"Cubic"}, {name:"defaultScaleXYCenter", 
enabled:"True", start_time:"0.2", type:"ScaleXYCenter", duration:"1", interpolation_type:"Cubic"}, {name:"defaultPie", enabled:"True", start_time:"0.2", type:"OutSide", duration:"2", show_mode:"Smoothed", smoothed_delay:"0.5", interpolation_type:"Cubic"}, {name:"defaultChart", enabled:"False", type:"Appear", interpolation_type:"Cubic", start_time:"0", duration:"0.5"}], marker_style:{marker:{type:"%MarkerType", size:"6", anchor:"top"}, fill:{color:"LightColor(%Color)"}, border:{color:"DarkColor(%Color)", 
thickness:"1"}, effects:{drop_shadow:{enabled:"false", distance:"1.5", opacity:"0.15"}}, states:{hover:{marker:{size:"10"}, fill:{color:"LightColor(%Color)"}, effects:{glow:{enabled:"true", blur_x:"2", blur_y:"2", opacity:"0.8"}}}, missing:{fill:{type:"Solid", color:"White", opacity:"0"}, border:{thickness:"1", color:"White", opacity:"1"}}}, name:"anychart_default"}, label_style:[{format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", 
size:"10", color:"rgb(35,35,35)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, 
enabled:"false"}, name:"anychart_default"}, {format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"DarkColor(%Color)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"DarkColor(%Color)"}, {position:"1", color:"DarkColor(%Color)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", 
color:"rgb(255,255,255)"}], angle:"90"}, enabled:"true", type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, name:"Default01"}, {font:{bold:"false"}, name:"Default02", parent:"Default01"}, {font:{color:"rgb(35,35,35)"}, name:"Default03", parent:"Default01"}, {font:{color:"rgb(35,35,35)", bold:"false"}, name:"Default04", parent:"Default01"}, {font:{color:"White", bold:"false"}, background:{border:{enabled:"false"}, 
fill:{enabled:"true", type:"Solid", color:"DarkColor(%Color)", opacity:"0.7"}, effects:{enabled:"false"}, inside_margin:{top:"1", bottom:"2"}, corners:{type:"Rounded", all:"2"}, enabled:"true"}, name:"Default05"}], tooltip_style:[{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True", italic:"False", underline:"False"}, background:{border:{gradient:{key:[{position:"0", 
color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", 
type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, 
name:"anychart_default", enabled:"true"}, {background:{enabled:"false"}, name:"Default01", enabled:"true"}, {position:{halign:"Center", valign:"Top", padding:"5"}, font:{color:"#575757"}, background:{fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#E4E5F0"}], angle:"90"}, type:"gradient"}, inside_margin:{left:"4", right:"4", top:"3", bottom:"3"}, border:{color:"#767676"}, effects:{drop_shadow:{enabled:"true", distance:"2"}}}, name:"Default02", enabled:"true"}]}, data_plot_settings:{}, 
chart_settings:{data_plot_background:{border:{color:"#B7B7B7"}, fill:{color:"#FFFFFF"}, effects:{drop_shadow:{enabled:"True", distance:"2", opacity:"0.2"}}}}}, defaults:{color_swatch:{labels:{format:{value:"{%Value}{numDecimals:2}"}, font:{family:"Verdana", size:"10", bold:"false", color:"#232323"}, padding:"0"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", 
color:"#FFFFFF"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, tickmark:{thickness:"1", size:"4", color:"#232323"}, range_item:{background:{fill:{color:"%Color"}, states:{hover:{fill:{color:"LightColor(%Color)"}}}}}, title:{text:{value:"Color Swatch"}, enabled:"false"}, title_separator:{gradient:{key:[{position:"0", 
color:"#333333", opacity:"0"}, {position:"0.5", color:"#333333", opacity:"1"}, {position:"1", color:"#333333", opacity:"0"}]}, enabled:"true", type:"Gradient"}, orientation:"Horizontal", position:"bottom", align:"near", inside_dataplot:"false", align_by:"dataplot", padding:"5"}}};
function bl() {
}
A.e(bl, sf);
z = bl.prototype;
z.Sq = x("chart");
z.zh = function() {
  return this.$f(bl.f.zh.call(this), al)
};
z.$f = function(a, b) {
  var c = bl.f.$f.call(this, a, b), d = this.Sq(), f = F(F(a, d), "data"), g = F(F(b, d), "data");
  if(f != p || g != p) {
    (f = gc(f, g, "series")) && H(F(c, d), "data", f)
  }
  return c
};
z.EE = function(a, b) {
  var c = bl.f.EE.call(this, a, b), d = F(F(a, this.Sq()), "data"), f = F(b, "data");
  if(f) {
    if(d) {
      var g = $b(d, f, "data", "series");
      H(c, "data", g);
      var f = I(f, "series"), k = f.length, l = [];
      H(g, "series", l);
      for(g = 0;g < k;g++) {
        var n = f[g];
        if(C(n, "name")) {
          var m = fc(I(d, "series"), F(n, "name"));
          m && (l[g] = $b(m, n, "series", "point"))
        }else {
          l[g] = n
        }
      }
    }else {
      c.data = f
    }
  }
  return c
};
z.qn = function(a, b, c) {
  bl.f.qn.call(this, a, b, c);
  var d = $b(F(a, "data_plot_settings"), F(b, "data_plot_settings"));
  d != p && (c.data_plot_settings = d);
  var f, d = cc(F(a, "palettes"), F(b, "palettes"));
  if(d != p) {
    for(f in c.palettes = d, c.palettes) {
      C(c.palettes[f], "name") && (c.palettes[f].name = Cb(F(c.palettes[f], "name")))
    }
  }
  a = cc(F(a, "thresholds"), F(b, "thresholds"));
  if(a != p) {
    for(f in c.thresholds = a, c.thresholds) {
      C(c.thresholds[f], "name") && (c.thresholds[f].name = Cb(F(c.thresholds[f], "name")))
    }
  }
};
z.Rs = function(a, b) {
  bl.f.Rs.call(this, a, b);
  if(a && b && C(b, "color_swatch")) {
    var c = F(b, "color_swatch");
    if(C(a, "chart_settings")) {
      var d = F(a, "chart_settings");
      C(d, "controls") && (d = F(d, "controls"), C(d, "color_swatch") && H(d, "color_swatch", $b(c, F(d, "color_swatch"), "color_swatch")))
    }
  }
};
var cl = {chart:{styles:{line_axis_marker_style:{label:{font:{family:"Tahoma", size:"11", bold:"True"}, format:{value:"{%Value}"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, 
effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"False", position:"BeforeAxisLabels", align:"Near", text_align:"Left"}, name:"anychart_default", thickness:"1", color:"#DC0A0A", caps:"Square", joints:"miter", dash_length:"10", space_length:"10"}, range_axis_marker_style:{label:{format:{value:"{%Minimum} - {%Maximum}"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, 
{position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"False", position:"BeforeAxisLabels", text_align:"Left"}, fill:{color:"%Color", opacity:".4"}, 
minimum_line:{color:"DarkColor(%Color)"}, maximum_line:{color:"DarkColor(%Color)"}, name:"anychart_default", color:"Green"}}, chart_settings:{axes:{scroll_bar_settings:{vert_scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"1"}, {color:"#E7E7E7", position:"0.7"}, {color:"#E7E7E7", position:"0"}], angle:"0"}, enabled:"true", type:"Solid", color:"#FEFEFE"}, border:{enabled:"true", color:"#707173", opacity:"1"}, effects:{inner_shadow:{enabled:"true", angle:"180", blur_x:"15", blur_y:"0", 
distance:"7", color:"#777777"}, enabled:"true"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, 
{color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, thumb:{background:{corners:{type:"Rounded", all:"0", left_top:"3", left_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, 
{color:"#EDEDED"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"0"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], angle:"0"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"0"}, 
type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"0"}, type:"Gradient"}}}}}, size:"16"}, horz_scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"0"}, {color:"#E7E7E7", position:"0.7"}, {color:"#E7E7E7", position:"1"}], angle:"90"}, enabled:"true", type:"Solid", color:"#FEFEFE"}, border:{enabled:"true", 
color:"#707173", opacity:"1"}, effects:{inner_shadow:{enabled:"true", blur_x:"0", blur_y:"15", distance:"7", color:"#555555"}, enabled:"true"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, 
arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, 
thumb:{background:{corners:{type:"Rounded", all:"0", left_bottom:"3", right_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#EDEDED"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"90"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], 
angle:"90"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"90"}, type:"Gradient"}}}}}, size:"16"}}, x_axis:{major_grid:{enabled:"true", interlaced:"false"}, minor_grid:{enabled:"true", 
interlaced:"false"}, title:{text:{value:" "}}}, y_axis:{major_grid:{enabled:"true"}, minor_grid:{enabled:"true"}, title:{text:{value:" "}}}, z_axis:{enabled:"false"}}, data_plot_background:{effects:{enabled:"False"}, x_axis_plane:{fill:{}, border:{}}, y_axis_plane:{fill:{}, border:{}}}}}, defaults:{axis:{line:{enabled:"True", color:"#474747"}, zero_line:{enabled:"True", color:"#FF0000", opacity:"0.3"}, major_grid:{line:{enabled:"true", color:"#C1C1C1", dash_length:"1", space_length:"5"}, interlaced_fills:{even:{fill:{enabled:"true", 
color:"#F5F5F5", opacity:"0.5"}}, odd:{fill:{enabled:"true", color:"#FFFFFF", opacity:"0.5"}}}, enabled:"false", interlaced:"true"}, minor_grid:{line:{enabled:"true", color:"#EEEEEE", dash_length:"1", space_length:"10"}, interlaced_fills:{even:{fill:{enabled:"True", color:"#F5F5F5", opacity:"0.5"}}, odd:{fill:{enabled:"True", color:"#FFFFFF", opacity:"0.5"}}}, enabled:"false", interlaced:"false"}, major_tickmark:{enabled:"true", size:"5", color:"#313131"}, minor_tickmark:{enabled:"true", size:"2", 
color:"#3C3C3C"}, title:{text:{value:"Axis title"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", 
opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"True", text_align:"Center", align:"Center"}, labels:{format:{value:"{%Value}{numDecimals:2}"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", 
distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)"}, enabled:"true", align:"inside", position:"outside", display_mode:"normal", multi_line_align:"center", rotation:"0", allow_overlap:"false", show_first_label:"true", show_last_label:"true", show_cross_label:"true"}, zoom:{enabled:"false"}, enabled:"true"}}};
var dl = {chart:{styles:{bar_style:[{border:{type:"Solid", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}, enabled:"True"}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", 
opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", 
type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, 
fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}, {fill:{color:"%Color"}, name:"AquaLight"}, {fill:{color:"%Color"}, name:"AquaDark"}, {effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}}, 
fill:{color:"%Color"}, border:{color:"DarkColor(%Color)", type:"solid", thickness:"1"}, states:{normal:{fill:{color:"%Color", opacity:"1"}}, hover:{fill:{color:"White", opacity:"0.8"}}, pushed:{fill:{color:"Blend(White,Black,0.9)", opacity:"1"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225", shadow_opacity:"0.1"}}}, selected_normal:{hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.3"}, fill:{color:"%Color", opacity:"1"}}, selected_hover:{fill:{color:"White", 
opacity:".8"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"Black", opacity:"0.3"}}, missing:{fill:{color:"White", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"cylinder"}, {fill:{type:"solid", color:"%color"}, name:"plastic"}, {name:"silver", parent:"cylinder"}]}, data_plot_settings:{bar_series:{animation:{enabled:"True", style:"defaultScaleYBottom"}, 
label_settings:{animation:{enabled:"true", style:"defaultLabel"}, enabled:"false"}, marker_settings:{marker:{anchor:"CenterTop", valign:"Center", halign:"Center"}, animation:{enabled:"true", style:"defaultMarker"}, enabled:"false"}, tooltip_settings:{}, group_padding:"0.5", point_padding:"0.2"}, range_bar_series:{bar_style:{}, animation:{style:"defaultScaleYCenter"}, start_point:{tooltip_settings:{format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}, 
marker_settings:{animation:{style:"defaultMarker"}}, label_settings:{animation:{style:"defaultLabel"}}}, end_point:{tooltip_settings:{format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}}, label_settings:{animation:{style:"defaultLabel"}}}, group_padding:"0.5", point_padding:"0.2"}}}};
var el = {chart:{styles:{bubble_style:[{border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"0.9"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"1"}, effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", 
color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", 
thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6", color:"Black"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3", pattern_size:"6"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", 
thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}, {border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color", opacity:"0.9"}, 
effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(%Color)", thickness:"2"}, fill:{type:"Solid", color:"Blend(%Color,Dark,0.9)", opacity:"0.9"}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6"}}, selected_hover:{border:{type:"Solid", 
color:"DarkColor(%Color)", thickness:"2"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(%Color)", opacity:"0.7", pattern_size:"6"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.9"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, 
hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Flat"}, {border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color", opacity:"0.5"}, effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.5"}}, pushed:{border:{type:"Solid", color:"DarkColor(%Color)", 
thickness:"2"}, fill:{type:"Solid", color:"Blend(%Color,Dark,0.9)", opacity:"0.5"}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6"}}, selected_hover:{border:{type:"Solid", color:"DarkColor(%Color)", thickness:"2"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(%Color)", opacity:"0.7", pattern_size:"6"}, fill:{type:"Solid", color:"LightColor(%Color)", 
opacity:"0.5"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Transparent"}, {effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}}, 
states:{normal:{fill:{color:"%Color", opacity:"1"}}, hover:{fill:{color:"White", opacity:"0.8"}}, pushed:{fill:{color:"Blend(White,Black,0.7)", opacity:"1"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.2"}}, selected_hover:{fill:{color:"White", opacity:"0.8"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.2"}}, missing:{fill:{color:"White", 
opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Aqua", "final":"true"}]}, data_plot_settings:{bubble_series:{animation:{style:"defaultScaleXYCenter"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"Center", v_align:"Center", h_align:"Center"}, enabled:"false"}, tooltip_settings:{}}}}};
var fl = {chart:{styles:{line_style:{line:{thickness:"2", color:"%Color"}, states:{hover:{line:{thickness:"3", color:"LightColor(%Color)"}, effects:{glow:{enabled:"true"}}}, missing:{line:{thickness:"3", color:"%Color", opacity:"0.2"}}}, name:"anychart_default"}, area_style:[{line:{enabled:"True", color:"DarkColor(%Color)"}, fill:{color:"%Color", opacity:"1"}, states:{hover:{fill:{type:"solid", color:"LightColor(%Color)", opacity:"1"}}, pushed:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.7)", 
opacity:"1"}}, selected_normal:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.9)", opacity:"1"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.3", pattern_size:"6"}}, selected_hover:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.95)", opacity:"1"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.2", pattern_size:"6"}}}, name:"anychart_default"}, {line:{enabled:"True", type:"Solid", color:"DarkColor(%Color)"}, 
fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}}, states:{hover:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}}, 
pushed:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{line:{enabled:"True", thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", 
color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, line:{enabled:"True", thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", 
color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Dark"}, {line:{enabled:"True", color:"DarkColor(%Color)"}, fill:{color:"%Color", opacity:"0.5"}, states:{hover:{fill:{type:"solid", color:"LightColor(%Color)", opacity:"0.5"}}, pushed:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.7)", 
opacity:"0.5"}}, selected_normal:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.9)", opacity:"0.5"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.3", pattern_size:"6"}}, selected_hover:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.95)", opacity:"0.5"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.2", pattern_size:"6"}}}, name:"Transparent"}], range_area_style:{start_line:{color:"DarkColor(%Color)", thickness:"2"}, 
end_line:{color:"DarkColor(%Color)", thickness:"2"}, fill:{type:"Solid", color:"%Color"}, states:{hover:{start_line:{color:"%Color", thickness:"2"}, end_line:{color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)"}}}, name:"anychart_default"}}, data_plot_settings:{line_series:{animation:{style:"defaultScaleYCenter"}, marker_settings:{animation:{style:"defaultMarker"}, fill:{gradient:{key:[{position:"0", color:"LightColor(%Color)", opacity:"1"}, {position:"0.7", color:"%Color", 
opacity:"1"}, {position:"1", color:"LightColor(%Color)", opacity:"1"}], angle:"45", type:"radial", focal_point:"-0.7"}, type:"gradient", opacity:"1"}, states:{missing:{marker:{type:"None"}, fill:{opacity:"0"}, border:{thickness:"1", color:"%Color", opacity:"0.3"}}}, enabled:"true"}, effects:{drop_shadow:{enabled:"true", distance:"1.5", opacity:"0.25"}}, tooltip_settings:{enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}}, area_series:{animation:{style:"defaultScaleYBottom"}, 
tooltip_settings:{enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, enabled:"False"}}, range_area_series:{animation:{style:"defaultScaleYCenter"}, start_point:{tooltip_settings:{format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"centerBottom"}, enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, 
format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}}, end_point:{tooltip_settings:{format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"centerTop"}, enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}}}}}};
var gl = {chart:{styles:{ohlc_style:{line:{thickness:"6"}, up:{line:{color:"LightColor(%Color)"}}, down:{line:{color:"DarkColor(%Color)"}}, name:"anychart_default"}, candlestick_style:{fill:{type:"solid", color:"%Color"}, border:{}, line:{color:"Black", thickness:"2"}, up:{fill:{color:"LightColor(%Color)"}}, down:{fill:{color:"DarkColor(%Color)"}}, name:"anychart_default"}}, data_plot_settings:{ohlc_series:{label_settings:{}, tooltip_settings:{}, marker_settings:{}, group_padding:"0.5", point_padding:"0.2"}, 
candlestick_series:{label_settings:{}, tooltip_settings:{}, marker_settings:{}, group_padding:"0.5", point_padding:"0.2"}}}};
var hl = {chart:{styles:{}, data_plot_settings:{marker_series:{animation:{style:"defaultScaleXYCenter"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, tooltip_settings:{}}}}};
var il = {chart:{styles:{heat_map_style:{border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color"}, name:"anychart_default"}}, data_plot_settings:{heat_map:{label_settings:{}, tooltip_settings:{}, marker_settings:{}}}}};
function jl() {
}
A.e(jl, bl);
jl.prototype.zh = function() {
  var a = this.$f(jl.f.zh.call(this), cl), a = this.$f(a, gl), a = this.$f(a, fl), a = this.$f(a, el), a = this.$f(a, hl), a = this.$f(a, il);
  return a = this.$f(a, dl)
};
jl.prototype.qn = function(a, b, c) {
  jl.f.qn.call(this, a, b, c);
  var d = p, f = p, g;
  if(C(a, "chart_settings") && C(g = F(a, "chart_settings"), "axes") && C(g = F(g, "axes"), "extra")) {
    d = F(g, "extra")
  }
  if(C(b, "chart_settings") && C(g = F(b, "chart_settings"), "axes") && C(g = F(g, "axes"), "extra")) {
    f = F(g, "extra")
  }
  a = cc(d, f);
  a != p && (F(F(c, "chart_settings"), "axes").extra = a)
};
jl.prototype.Rs = function(a, b) {
  jl.f.Rs.call(this, a, b);
  if(!(a == p || b == p)) {
    var c = F(b, "axis"), d = p;
    if(c != p && C(a, "chart_settings") && C(d = F(a, "chart_settings"), "axes")) {
      if(d = F(d, "axes"), C(d, "x_axis") && (d.x_axis = $b(c, F(d, "x_axis"), "x_axis")), C(d, "y_axis") && (d.y_axis = $b(c, F(d, "y_axis"), "y_axis")), C(d, "extra")) {
        d = F(d, "extra");
        d.x_axis instanceof Array || (d.x_axis = d.x_axis ? [d.x_axis] : []);
        var f = d.x_axis, g = f.length, k;
        for(k = 0;k < g;k++) {
          f[k] = $b(c, f[k], "x_axis"), C(f[k], "name") && (f[k].name = Cb(F(f[k], "name")))
        }
        d.y_axis instanceof Array || (d.y_axis = d.y_axis ? [d.y_axis] : []);
        f = d.y_axis;
        g = f.length;
        for(k = 0;k < g;k++) {
          f[k] = $b(c, f[k], "y_axis"), C(f[k], "name") && (f[k].name = Cb(F(f[k], "name")))
        }
      }
    }
  }
};
function kl(a) {
  this.ea = a
}
z = kl.prototype;
z.ea = p;
z.Uv = function() {
  A.xa()
};
z.Wv = function() {
  A.xa()
};
z.Vv = function() {
  A.xa()
};
z.yv = function() {
  A.xa()
};
z.Bv = function() {
  A.xa()
};
function ll(a) {
  this.ea = a
}
A.e(ll, kl);
ll.prototype.Wl = function(a, b, c, d) {
  d.x = this.ea.n().x + b - a / 2;
  d.width = a;
  d.height = c
};
function ml(a) {
  this.ea = a
}
A.e(ml, ll);
z = ml.prototype;
z.yv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.y = this.ea.n().ra() - c
};
z.Bv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.y = this.ea.n().ra()
};
z.Uv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c - d, b, this.ea.n().ra() + c, f));
  return a
};
z.Wv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c, b, this.ea.n().ra() + c + d, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y - c, b, this.ea.n().y - c + d, f));
  return a
};
function nl(a) {
  this.ea = a
}
A.e(nl, ll);
z = nl.prototype;
z.yv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.y = this.ea.n().y
};
z.Bv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.y = this.ea.n().y - c
};
z.Uv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y, b, this.ea.n().y + d, f));
  return a
};
z.Wv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y - d - c, b, this.ea.n().y - c, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c - d, b, this.ea.n().ra() + c, f));
  return a
};
function ol(a) {
  this.ea = a
}
A.e(ol, kl);
ol.prototype.Wl = function(a, b, c, d) {
  b = this.ea.n().ra() - b;
  d.y = b - a / 2;
  d.height = a;
  d.width = c
};
function pl(a) {
  this.ea = a
}
A.e(pl, ol);
z = pl.prototype;
z.yv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.x = this.ea.n().x
};
z.Bv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.x = this.ea.n().x - c
};
z.Uv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x, b, this.ea.n().x + d, b, f));
  return a
};
z.Wv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x - d - c, b, this.ea.n().x - c, b, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() - d, b, this.ea.n().Ja(), b, f));
  return a
};
function ql(a) {
  this.ea = a
}
A.e(ql, ol);
z = ql.prototype;
z.yv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.x = this.ea.n().Ja() - c
};
z.Bv = function(a, b, c, d) {
  this.Wl(a, b, c, d);
  d.x = this.ea.n().Ja()
};
z.Uv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() - d, b, this.ea.n().Ja(), b, f));
  return a
};
z.Wv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() + c, b, this.ea.n().Ja() + d + c, b, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x, b, this.ea.n().x + d, b, f));
  return a
};
function rl(a) {
  this.ea = a
}
z = rl.prototype;
z.ea = p;
z.Xv = function() {
  A.xa()
};
z.Yv = function() {
  A.xa()
};
z.CA = function() {
  A.xa()
};
z.Me = function() {
  A.xa()
};
z.zj = function() {
  A.xa()
};
z.Cv = function() {
  A.xa()
};
z.EF = function() {
  A.xa()
};
function sl(a) {
  this.ea = a
}
A.e(sl, rl);
z = sl.prototype;
z.Me = s();
z.Cv = function(a, b, c) {
  c.x = this.ea.n().x + b - a / 2;
  c.width = a;
  c.y = this.ea.n().y;
  c.height = this.ea.n().height
};
z.EF = function(a, b, c) {
  c.x = this.ea.n().x + a;
  c.width = b - a;
  c.y = this.ea.n().y;
  c.height = this.ea.n().height
};
z.zj = function(a, b, c, d, f) {
  f.y = this.ea.n().y;
  f.x = this.ea.n().x + b;
  f.width = c - b;
  f.height = a
};
z.Yv = function(a, b, c) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y + 0.5, b, this.ea.n().ra(), c));
  return a
};
z.CA = function(a, b) {
  return Mc(a, b.x, b.y, b.width, b.height)
};
function tl(a) {
  this.ea = a
}
A.e(tl, sl);
tl.prototype.zj = function(a, b, c, d, f) {
  tl.f.zj.call(this, a, b, c, d, f);
  f.y += this.ea.n().ra() + d - a / 2
};
tl.prototype.Xv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().ra() + d;
  a.setAttribute("d", Jc(b + 0.5, d, c, d, f));
  return a
};
function ul(a) {
  this.ea = a
}
A.e(ul, sl);
ul.prototype.zj = function(a, b, c, d, f) {
  ul.f.zj.call(this, a, b, c, d, f);
  f.y += this.ea.n().y - d - a / 2
};
ul.prototype.Xv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().y - d;
  a.setAttribute("d", Jc(b, d, c + 1, d, f));
  return a
};
ul.prototype.Me = function(a) {
  a.Wd(-a.ac)
};
function vl(a) {
  this.ea = a
}
A.e(vl, rl);
z = vl.prototype;
z.Cv = function(a, b, c) {
  c.y = this.ea.n().ra() - b - a / 2;
  c.height = a;
  c.x = this.ea.n().x;
  c.width = this.ea.n().width
};
z.EF = function(a, b, c) {
  a = this.ea.n().ra() - a;
  b = this.ea.n().ra() - b;
  c.y = Math.min(a, b);
  c.height = Math.max(a, b) - c.y;
  c.x = this.ea.n().x;
  c.width = this.ea.n().width
};
z.zj = function(a, b, c, d, f) {
  f.x = this.ea.n().x;
  f.y = b;
  f.height = c - b;
  f.width = a
};
z.Yv = function(a, b, c) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x + 0.5, b, this.ea.n().Ja(), b, c));
  return a
};
z.CA = function(a, b) {
  return Mc(a, b.x, b.y, b.width, b.height)
};
function wl(a) {
  this.ea = a
}
A.e(wl, vl);
wl.prototype.zj = function(a, b, c, d, f) {
  wl.f.zj.call(this, a, b, c, d, f);
  f.x += this.ea.n().x - d - a / 2
};
wl.prototype.Xv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().x - d;
  a.setAttribute("d", Jc(d, b + 1, d, c, f));
  return a
};
wl.prototype.Me = function(a) {
  a.Wd(180 - (a.ac - 90))
};
function xl(a) {
  this.ea = a
}
A.e(xl, vl);
xl.prototype.zj = function(a, b, c, d, f) {
  xl.f.zj.call(this, a, b, c, d, f);
  f.x += this.ea.n().Ja() + d - a / 2
};
xl.prototype.Xv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().Ja() + d;
  a.setAttribute("d", Jc(d, b + 0.5, d, c, f));
  return a
};
xl.prototype.Me = function(a) {
  a.Wd(-(a.ac + 90))
};
function yl() {
}
z = yl.prototype;
z.Zx = function() {
  A.xa()
};
z.gD = function() {
  A.xa()
};
z.$x = function() {
  A.xa()
};
z.Px = function() {
  A.xa()
};
z.cD = function() {
  A.xa()
};
z.dD = function() {
  A.xa()
};
function zl() {
}
A.e(zl, yl);
z = zl.prototype;
z.Zx = aa();
z.Px = function(a) {
  switch(a) {
    case 7:
      return 3;
    case 3:
      return 7;
    case 8:
      return 2;
    case 2:
      return 8;
    case 6:
      return 4;
    case 4:
      return 6;
    default:
      return a
  }
};
z.gD = aa();
z.$x = function(a, b) {
  return b
};
z.cD = aa();
z.dD = function(a, b) {
  switch(b) {
    case 0:
      return 2;
    case 2:
      return 0;
    default:
      return b
  }
};
function Al() {
}
A.e(Al, zl);
function Bl() {
}
A.e(Bl, zl);
function Cl() {
}
A.e(Cl, yl);
z = Cl.prototype;
z.Zx = function(a) {
  switch(a) {
    case 7:
      return 1;
    case 1:
      return 3;
    case 5:
      return 7;
    case 3:
      return 5;
    case 8:
      return 2;
    case 2:
      return 4;
    case 6:
      return 8;
    case 4:
      return 6;
    default:
      return a
  }
};
z.Px = function(a) {
  switch(a) {
    case 7:
      return 5;
    case 1:
      return 3;
    case 5:
      return 7;
    case 3:
      return 1;
    case 8:
      return 4;
    case 4:
      return 8;
    default:
      return a
  }
};
z.gD = function(a, b) {
  switch(b) {
    case 0:
      return 1;
    case 2:
      return 0;
    default:
      return 2
  }
};
z.$x = function(a) {
  switch(a) {
    case 0:
      return 0;
    case 1:
      return 2;
    default:
      return 1
  }
};
z.cD = function(a, b) {
  switch(b) {
    case 0:
      return 0;
    case 2:
      return 1;
    default:
      return 2
  }
};
z.dD = function(a, b) {
  return this.$x(a, b)
};
function Dl() {
}
A.e(Dl, Cl);
function El() {
}
A.e(El, Cl);
function Fl(a) {
  this.ea = a
}
Fl.prototype.ea = p;
Fl.prototype.Aj = function() {
  A.xa()
};
Fl.prototype.HF = function() {
  A.xa()
};
function Gl(a) {
  this.ea = a
}
A.e(Gl, Fl);
Gl.prototype.HF = s();
Gl.prototype.Aj = function(a, b) {
  switch(a.za) {
    case 0:
      b.x = this.ea.n().x;
      break;
    case 1:
      b.x = this.ea.n().x + (this.ea.n().width - a.nf.width) / 2;
      break;
    case 2:
      b.x = this.ea.n().Ja() - a.nf.width
  }
};
function Hl(a) {
  this.ea = a
}
A.e(Hl, Gl);
Hl.prototype.Aj = function(a, b) {
  Hl.f.Aj.call(this, a, b);
  b.y = this.ea.n().ra() + a.Tf() + a.ga()
};
function Il(a) {
  this.ea = a
}
A.e(Il, Gl);
Il.prototype.Aj = function(a, b) {
  Il.f.Aj.call(this, a, b);
  b.y = this.ea.n().rb() - a.Tf() - a.ga() - a.nf.height
};
function Jl(a) {
  this.ea = a
}
A.e(Jl, Fl);
Jl.prototype.HF = function(a) {
  Ae(a, a.Bb.bc + 90)
};
Jl.prototype.Aj = function(a, b) {
  switch(a.za) {
    case 0:
      b.y = this.ea.n().ra() - a.n().height;
      break;
    case 1:
      b.y = this.ea.n().rb() + (this.ea.n().height - a.n().height) / 2;
      break;
    case 2:
      b.y = this.ea.n().rb()
  }
};
function Kl(a) {
  this.ea = a
}
A.e(Kl, Jl);
Kl.prototype.Aj = function(a, b) {
  Kl.f.Aj.call(this, a, b);
  b.x = this.ea.n().x - a.Tf() - a.ga() - a.n().width
};
function Ll(a) {
  this.ea = a
}
A.e(Ll, Jl);
Ll.prototype.Aj = function(a, b) {
  Ll.f.Aj.call(this, a, b);
  b.x = this.ea.n().Ja() + a.Tf() + a.ga()
};
function Ml(a, b) {
  this.w = a;
  this.J = b;
  ye.apply(this)
}
A.e(Ml, ye);
z = Ml.prototype;
z.uH = function() {
  return new Nl(this.J)
};
z.w = p;
z.Ia = 5;
z.ga = u("Ia");
z.za = 0;
z.tl = q;
z.J = p;
z.Iv = j;
z.Jv = j;
z.yc = p;
z.$n = q;
z.am = NaN;
z.JI = u("am");
z.Ih = p;
z.g = function(a) {
  Ml.f.g.call(this, a);
  if(this.Ma) {
    if(C(a, "format") && (this.yc = new ie(J(a, "format")), this.w.fa().tJ())) {
      var b = this.yc;
      b.ta(this.w);
      var c = p, d;
      for(d in b.Mf) {
        if("%Value" == b.Mf[d].On) {
          c = b.Mf[d];
          break
        }
      }
      this.Ih = c != p && c.Ih != h ? c.Ih : 2;
      this.w.fa().XH(this.Ih)
    }
    C(a, "padding") && (this.Ia = M(a, "padding"));
    if(C(a, "align")) {
      switch(Wb(a, "align")) {
        case "inside":
          this.za = 0;
          break;
        case "outside":
          this.za = 1;
          break;
        case "center":
          this.za = 2
      }
    }
    C(a, "position") && (this.tl = "inside" == N(a, "position"));
    C(a, "show_first_label") && (this.Iv = K(a, "show_first_label"));
    C(a, "show_last_label") && (this.Jv = K(a, "show_last_label"));
    C(a, "allow_overlap") && (this.$n = K(a, "allow_overlap"))
  }
};
z.Kd = p;
z.El = p;
z.uc = function(a, b) {
  this.Bb.Ae();
  Ml.f.uc.call(this, a, b)
};
z.an = function(a) {
  this.El = new P;
  this.Kd = new P;
  for(var b = this.J.J.w.fa(), c = b.fc, d = 0;d <= c;d++) {
    this.am = b.Rq(d);
    var f = this.yc.ta(this.w, this.w.Gd());
    this.uc(a, f);
    var f = this.k, g = this.nf;
    f.width > this.Kd.width && (this.Kd.width = f.width, this.El.width = g.width);
    f.height > this.Kd.height && (this.Kd.height = f.height, this.El.height = g.height)
  }
};
z.Ys = function(a) {
  this.an(a);
  a = this.J.Ho(this.El, this.Kd, this.Bb);
  a = Math.ceil(a);
  return 1 > a ? 1 : a
};
z.yd = 0;
z.nd = u("yd");
z.$v = 0;
z.GB = function() {
  this.yd = this.J.pg(this.Kd);
  this.$v = this.yd + this.Ia
};
z.Qx = function(a, b) {
  var c = new O, d = this.J.J.w.fa(), f = d.Nc(b);
  this.am = d.Rq(b);
  this.uc(a, this.yc.ta(this.w, this.w.Gd()));
  this.J.qf(this, c, this.k, this.nf, d.rd(f));
  Ol(this, c, this.k, this.nf, this.Bb);
  d = this.k.Ca();
  d.x = c.x;
  d.y = c.y;
  return d
};
z.Je = 0;
z.Tf = u("Je");
z.Ei = t("Je");
z.xo = function(a) {
  var b = new O, c = this.J.J.w.fa(), d = !this.$n, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k;
  this.Bb.or || (k = this.J.Cy(this.El, this.Bb));
  var l = this.Iv ? 0 : 1, n = c.fc;
  for(this.Jv || n--;l <= n;l++) {
    var m = c.Nc(l);
    this.am = c.Rq(l);
    if(!c.Vy(m)) {
      break
    }
    var o = this.yc.ta(this.w, this.w.Gd());
    this.uc(a.r(), o);
    var o = this.k, v = this.nf;
    this.J.qf(this, b, o, v, c.rd(m));
    if(d) {
      if(this.J.Gy(f, this.Bb, b, g, k, o, v)) {
        continue
      }
      g = this.J.Rx(f, this.Bb, b, k, o, v)
    }
    Ol(this, b, o, v, this.Bb);
    m = this.ic(a.r());
    m.qb(b.x, b.y);
    a.ia(m)
  }
};
function Ol(a, b, c, d, f) {
  a.Bb.bN ? (b.x += c.width * f.VH, b.y += c.height * f.WH) : 360 >= f.bc && 270 <= f.bc && a.J && a.J instanceof Pl ? b.x -= c.width - d.height / 2 * f.xf : (b.x += d.width * f.VH + d.height * f.bM, b.y += d.width * f.cM + d.height * f.WH)
}
function Ql(a, b) {
  Ml.call(this, a, b)
}
A.e(Ql, Ml);
z = Ql.prototype;
z.fi = p;
z.an = function(a) {
  this.Kd = new P;
  this.fi = new P;
  for(var b = this.w.fa(), c = b.fc, d = 0;d <= c;d++) {
    b.Nc(d);
    this.am = b.Rq(d);
    var f = this.yc.ta(this.w, this.w.Gd());
    this.uc(a, f);
    f = 0 == d % 2 ? this.Kd : this.fi;
    this.k.width > f.width && (f.width = this.k.width);
    this.k.height > f.height && (f.height = this.k.height)
  }
};
z.Ys = function(a) {
  this.an(a);
  var a = this.J.Ho(this.Kd, this.Kd, this.Bb), b = this.J.Ho(this.fi, this.fi, this.Bb);
  1 > a && (a = 1);
  1 > b && (b = 1);
  return a + b
};
z.GB = function() {
  this.yd = this.J.pg(this.Kd) + this.J.pg(this.fi) + this.Ia;
  this.$v = this.yd + this.Ia
};
z.xo = function(a) {
  for(var b = new O, c = this.J.J.w.fa(), d = !this.$n, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k = g, l = this.J.pg(this.Kd), n = this.J.pg(this.fi), m, o, v = 0;v <= c.fc;v++) {
    var w = 0 == v % 2, y = c.Nc(v);
    this.am = c.Rq(v);
    m = this.yc.ta(this.w, this.w.Gd());
    this.uc(a.r(), m);
    w ? (o = l, m = 0) : (o = n, m = l);
    var E = this.k;
    this.J.Fi(this, b, E, c.rd(y), m, o);
    if(d) {
      if(w) {
        if(this.J.xr(f, b, E, g)) {
          continue
        }
        g = this.J.$q(f, b, E)
      }else {
        if(this.J.xr(f, b, E, k)) {
          continue
        }
        k = this.J.$q(f, b, E)
      }
    }
    w = this.ic(a.r());
    w.qb(b.x, b.y);
    a.ia(w)
  }
};
z.g = function(a) {
  Ql.f.g.call(this, a);
  Ae(this, 0)
};
function Nl(a) {
  this.RA = a
}
A.e(Nl, Zc);
Nl.prototype.Ae = function() {
  Nl.f.Ae.call(this);
  this.Zn = this.tan = Math.tan(this.Ol);
  0 > this.Zn && (this.Zn = -this.Zn);
  this.bN = 0 == this.Ol % 180;
  this.or = 0 == this.Ol % 90;
  var a = 0, a = 90 > this.Ol ? 0 : 180 > this.Ol ? 1 : 270 > this.Ol ? 2 : 3;
  this.VH = this.RA.Tx(a, this);
  this.bM = this.RA.Sx(a, this);
  this.cM = this.RA.Vx(a, this);
  this.WH = this.RA.Ux(a, this)
};
function Rl(a) {
  this.J = a
}
z = Rl.prototype;
z.J = p;
z.Ho = function() {
  A.xa()
};
z.pg = function() {
  A.xa()
};
z.qf = function() {
  A.xa()
};
z.Rx = function() {
  A.xa()
};
z.Cy = function() {
  A.xa()
};
z.Gy = function() {
  A.xa()
};
z.Tx = function() {
  A.xa()
};
z.Sx = function() {
  A.xa()
};
z.Vx = function() {
  A.xa()
};
z.Ux = function() {
  A.xa()
};
z.tr = function() {
  A.xa()
};
z.oB = function() {
  A.xa()
};
z.Fi = function() {
  A.xa()
};
z.xr = function() {
  A.xa()
};
z.$q = function() {
  A.xa()
};
function Sl(a) {
  this.J = a
}
A.e(Sl, Rl);
z = Sl.prototype;
z.pg = function(a) {
  return a.height
};
z.Ho = function(a, b, c) {
  var d = this.J.n().width, f = 0;
  return f = 0 != c.bc % 90 ? a.width * c.Zn < a.height ? Number(d * c.wf / a.width) : Number(d * c.xf / a.height) : 0 != c.bc % 180 ? d / a.height : d / b.width
};
z.tr = aa();
z.Cy = function(a, b) {
  return a.width * b.Zn < a.height
};
z.Vx = x(0);
z.Ux = x(0);
z.qf = function(a, b, c, d, f) {
  b.x = this.J.n().x + f;
  b.y = 0
};
z.Gy = function(a, b, c, d, f, g, k) {
  return a ? b.or ? c.x + g.width > d : c.x + (f ? k.width / b.wf : k.height / b.xf) > d : Math.round(c.x) < d
};
z.Rx = function(a, b, c, d, f, g) {
  return a ? c.x : c.x + (b.or ? f.width : d ? g.width / b.wf : g.height / b.xf)
};
z.oB = function(a, b, c) {
  var d = b, f = c;
  this.J.w.fa().Nb() && (d = c, f = b);
  0 > d.x && (a.x += -d.x, a.width += d.x);
  f.x + f.width > this.J.n().x + this.J.n().width && (a.width -= f.x + f.width - this.J.n().width - this.J.n().x)
};
z.Fi = function(a, b, c, d) {
  b.x = d - c.x - c.width / 2 + this.J.n().x;
  b.y = 0
};
z.xr = function(a, b, c, d) {
  return a ? b.x + c.width > d : b.x < d
};
z.$q = function(a, b, c) {
  return a ? b.x : b.x + c.width
};
function Pl(a) {
  this.J = a
}
A.e(Pl, Sl);
Pl.prototype.Tx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 1 == a || 3 == a ? -b.wf : 0
};
Pl.prototype.Sx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 0 == a || 2 == a ? -b.xf : 0
};
Pl.prototype.qf = function(a, b, c, d, f) {
  Pl.f.qf.call(this, a, b, c, d, f);
  b.y += this.J.n().ra() + a.Tf();
  switch(a.za) {
    case 2:
      b.y += (a.nd() - c.height) / 2;
      break;
    case 1:
      b.y += a.nd() - c.height
  }
};
Pl.prototype.Fi = function(a, b, c, d, f, g) {
  Pl.f.Fi.call(this, a, b, c, d, f, g);
  b.y += this.J.n().ra() + a.Tf() + f;
  switch(a.za) {
    case 2:
      b.y += (g - c.height) / 2;
      break;
    case 1:
      b.y += g - c.height
  }
};
function Tl(a) {
  this.J = a
}
A.e(Tl, Sl);
Tl.prototype.Tx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 0 == a || 2 == a ? -b.wf : 0
};
Tl.prototype.Sx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 1 == a || 3 == a ? -b.xf : 0
};
Tl.prototype.qf = function(a, b, c, d, f) {
  Tl.f.qf.call(this, a, b, c, d, f);
  b.y += this.J.n().y - a.Tf() - c.height;
  switch(a.za) {
    case 2:
      b.y -= (a.nd() - c.height) / 2;
      break;
    case 1:
      b.y -= a.nd() - c.height
  }
};
Tl.prototype.Fi = function(a, b, c, d, f, g) {
  Tl.f.Fi.call(this, a, b, c, d, f, g);
  b.y += this.J.n().y - a.Tf() - c.height - f;
  switch(a.za) {
    case 2:
      b.y -= (g - c.height) / 2;
      break;
    case 1:
      b.y -= g - c.height
  }
};
function Ul(a) {
  this.J = a
}
A.e(Ul, Rl);
z = Ul.prototype;
z.pg = function(a) {
  return a.width
};
z.Ho = function(a, b, c) {
  var d = this.J.n().height, f = 0;
  return f = c && 0 != c.bc % 90 ? a.width / c.Zn < a.height ? Number(d * c.xf / a.width) : Number(d * c.wf / a.height) : c && 0 != c.bc % 180 ? d / a.width : d / b.height
};
z.tr = function(a) {
  return!a
};
z.Cy = function(a, b) {
  return a.width / b.Zn < a.height
};
z.Tx = x(0);
z.Sx = x(0);
z.qf = function(a, b, c, d, f) {
  b.y = this.J.n().ra() - f;
  b.x = 0
};
z.Gy = function(a, b, c, d, f, g, k) {
  return a ? b.or ? c.y + g.height > d : c.y + (f ? k.width / b.xf : k.height / b.wf) > d : Math.round(c.y) < d
};
z.Rx = function(a, b, c, d, f, g) {
  return a ? c.y : c.y + (b.or ? f.height : d ? g.width / b.xf : g.height / b.wf)
};
z.oB = function(a, b, c) {
  var d = c, f = b;
  this.J.w.fa().Nb() && (d = b, f = c);
  0 > d.y && (a.y += -d.y, a.height += d.y);
  f.y + f.height > this.J.n().y + this.J.n().height && (a.height -= f.y + f.height - this.J.n().height - this.J.n().y)
};
z.Fi = function(a, b, c, d) {
  b.y = this.J.n().ra() - d - c.height / 2;
  b.x = 0
};
z.xr = function(a, b, c, d) {
  return a ? b.y + c.height > d : b.y < d
};
z.$q = function(a, b, c) {
  return a ? b.y : b.y + c.height
};
function Vl(a) {
  this.J = a
}
A.e(Vl, Ul);
Vl.prototype.Vx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 0 == a || 2 == a ? -b.xf : 0
};
Vl.prototype.Ux = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 1 == a || 3 == a ? -b.wf : 0
};
Vl.prototype.qf = function(a, b, c, d, f) {
  Vl.f.qf.call(this, a, b, c, d, f);
  b.x += this.J.n().x - a.Tf() - c.width;
  switch(a.align) {
    case 2:
      b.x -= (a.nd() - c.width) / 2;
      break;
    case 1:
      b.x -= a.nd() - c.width
  }
};
Vl.prototype.Fi = function(a, b, c, d, f, g) {
  Vl.f.Fi.call(this, a, b, c, d, f, g);
  b.x += this.J.n().x - a.Tf() - c.width - f;
  switch(a.align) {
    case 2:
      b.x -= (g - c.width) / 2;
      break;
    case 1:
      b.x -= g - c.width
  }
};
function Wl(a) {
  this.J = a
}
A.e(Wl, Ul);
Wl.prototype.Vx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 1 == a || 3 == a ? -b.xf : 0
};
Wl.prototype.Ux = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 0 == a || 2 == a ? -b.wf : 0
};
Wl.prototype.qf = function(a, b, c, d, f) {
  Wl.f.qf.call(this, a, b, c, d, f);
  b.x += this.J.n().Ja() + a.Tf();
  switch(a.align) {
    case 2:
      b.x += (a.nd() - c.width) / 2;
      break;
    case 1:
      b.x += a.nd() - c.width
  }
};
Wl.prototype.Fi = function(a, b, c, d, f, g) {
  Wl.f.Fi.call(this, a, b, c, d, f, g);
  b.x += this.J.n().Ja() + a.Tf() + f;
  switch(a.align) {
    case 2:
      b.x += (g - c.width) / 2;
      break;
    case 1:
      b.x += g - c.width
  }
};
function Xl(a) {
  this.J = a
}
z = Xl.prototype;
z.J = p;
z.dg = function() {
  A.xa()
};
z.cg = function() {
  A.xa()
};
z.yj = function() {
  A.xa()
};
z.zv = function() {
  A.xa()
};
function Yl(a, b, c, d, f) {
  if(d == p || d == h) {
    d = j
  }
  if(f == p || f == h) {
    f = q
  }
  var g = new O, k = new O;
  a.dg(b, g);
  a.cg(c, k);
  f && (a = g, g = k, k = a);
  d = d ? S(g.x, g.y) : U(g.x, g.y);
  return d += U(k.x, k.y)
}
function Zl(a) {
  this.J = a
}
A.e(Zl, Xl);
Zl.prototype.dg = function(a, b) {
  b.x = a + this.J.n().x;
  b.y = this.J.n().y
};
Zl.prototype.cg = function(a, b) {
  b.x = a + this.J.n().x;
  b.y = this.J.n().ra()
};
Zl.prototype.yj = function(a, b, c, d, f) {
  f.x = this.J.n().x + a - d.width / 2;
  f.y = 0
};
function $l(a) {
  this.J = a
}
A.e($l, Xl);
$l.prototype.dg = function(a, b) {
  b.x = this.J.n().x;
  b.y = this.J.w.fa().we - a + this.J.n().y
};
$l.prototype.cg = function(a, b) {
  b.x = this.J.n().Ja();
  b.y = this.J.w.fa().we - a + this.J.n().y
};
$l.prototype.yj = function(a, b, c, d, f) {
  f.y = this.J.n().ra() - a - d.height / 2;
  f.x = 0
};
function am(a) {
  this.J = a
}
A.e(am, $l);
am.prototype.yj = function(a, b, c, d, f) {
  am.f.yj.call(this, a, b, c, d, f);
  f.x += this.J.n().tb() - c - d.width - b
};
am.prototype.zv = function(a, b, c, d, f, g) {
  var k = this.J.n(), a = new O(k.x, k.ra() - a), b = new O(k.Ja(), k.ra() - b), l = Math.atan2(b.y - a.y, b.x - a.x), k = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.x = a.x + k;
      g.y = a.y + d - f.height / 2;
      break;
    case 1:
      g.y = (a.y + b.y) / 2 - f.height / 2;
      g.x = (a.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.x = b.x - k - f.width, g.y = b.y - f.height / 2 - d
  }
};
function bm(a) {
  this.J = a
}
A.e(bm, $l);
bm.prototype.yj = function(a, b, c, d, f) {
  bm.f.yj.call(this, a, b, c, d, f);
  f.x += this.J.n().Ja() + c + b
};
bm.prototype.zv = function(a, b, c, d, f, g) {
  var a = new O(this.J.n().Ja(), this.J.n().ra() - a), b = new O(this.J.n().x, this.J.n().ra() - b), k = Math.atan2(b.y - a.y, b.x - a.x), l = d * Math.cos(k), d = d * Math.sin(k);
  switch(c) {
    case 0:
      g.x = a.x - l - f.width;
      g.y = a.y + d - f.height / 2;
      break;
    case 1:
      g.y = (a.y + b.y) / 2 - f.height / 2;
      g.x = (a.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.x = b.x + l, g.y = b.y - f.height / 2 - d
  }
  g.x += this.J.n().x
};
bm.prototype.dg = function(a, b) {
  bm.f.cg.call(this, a, b)
};
bm.prototype.cg = function(a, b) {
  bm.f.dg.call(this, a, b)
};
function cm(a) {
  this.J = a
}
A.e(cm, Zl);
cm.prototype.yj = function(a, b, c, d, f) {
  cm.f.yj.call(this, a, b, c, d, f);
  f.y += this.J.n().rb() - c - d.height - b
};
cm.prototype.zv = function(a, b, c, d, f, g) {
  var k = new O(a, this.J.n().y), b = new O(b, this.J.n().ra()), l = Math.atan2(b.y - k.y, b.x - k.x), n = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.y = k.y + d;
      g.x = a + n - f.width / 2;
      break;
    case 1:
      g.y = (k.y + b.y) / 2 - f.height / 2;
      g.x = (k.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.y = b.y - f.height - d, g.x = b.x - f.width / 2 - n
  }
};
function dm(a) {
  this.J = a
}
A.e(dm, Zl);
dm.prototype.yj = function(a, b, c, d, f) {
  dm.f.yj.call(this, a, b, c, d, f);
  f.y += this.J.n().ra() + c + b
};
dm.prototype.zv = function(a, b, c, d, f, g) {
  var k = new O(a, this.J.n().ra()), b = new O(b, this.J.n().y), l = Math.atan2(b.y - k.y, b.x - k.x), n = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.y = k.y - f.height + d;
      g.x = a + n - f.width / 2;
      break;
    case 1:
      g.y = (k.y + b.y) / 2 - f.height / 2;
      g.x = (k.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.y = b.y - d, g.x = b.x - f.width / 2 - n
  }
  g.x += this.J.n().x
};
dm.prototype.dg = function(a, b) {
  dm.f.cg.call(this, a, b)
};
dm.prototype.cg = function(a, b) {
  dm.f.dg.call(this, a, b)
};
function em() {
}
em.$B = function(a) {
  switch(a.Ib()) {
    case 0:
      return new fm(a);
    case 1:
      return new gm(a);
    case 2:
      return new hm(a);
    case 3:
      return new im(a)
  }
  return p
};
function jm(a) {
  this.w = a
}
z = jm.prototype;
z.k = p;
z.n = u("k");
z.w = p;
z.is = p;
z.$d = p;
z.vi = p;
z.js = p;
z.ij = p;
z.on = p;
z.Jp = t("k");
z.ao = function() {
  A.xa()
};
z.vD = function() {
  A.xa()
};
z.uD = function() {
  A.xa()
};
z.Fv = function() {
  A.xa()
};
z.Ei = function() {
  A.xa()
};
z.dg = function() {
  A.xa()
};
z.cg = function() {
  A.xa()
};
z.nd = function() {
  A.xa()
};
function km(a) {
  this.w = a
}
A.e(km, jm);
z = km.prototype;
z.Fv = function(a, b) {
  b.x = this.k.x + a
};
z.Jp = function(a) {
  km.f.Jp.call(this, a);
  var b = this.w.fa(), a = a.width;
  b.ef = 0;
  b.we = a
};
z.dg = function(a, b) {
  b.x = a;
  b.y = this.k.y
};
z.cg = function(a, b) {
  b.x = a;
  b.y = this.k.ra()
};
z.vD = function() {
  return this.k.tb()
};
z.uD = function() {
  return this.k.Ja()
};
z.nd = function(a) {
  return a.height
};
function lm(a) {
  this.w = a
}
A.e(lm, jm);
z = lm.prototype;
z.Jp = function(a) {
  lm.f.Jp.call(this, a);
  var b = this.w.fa(), a = a.height;
  b.ef = 0;
  b.we = a
};
z.Fv = function(a, b) {
  b.y = this.k.ra() - a
};
z.dg = function(a, b) {
  b.y = a;
  b.x = this.k.x
};
z.cg = function(a, b) {
  b.y = a;
  b.x = this.k.Ja()
};
z.vD = function() {
  return this.k.y
};
z.uD = function() {
  return this.k.ra()
};
z.nd = function(a) {
  return a.width
};
function hm(a) {
  this.w = a;
  this.is = new nl(this);
  this.$d = new ul(this);
  this.vi = new Bl;
  this.js = new Il(this);
  this.ij = new Tl(this);
  this.on = new cm(this)
}
A.e(hm, km);
hm.prototype.ao = function(a, b) {
  a.height -= b;
  a.y += b
};
hm.prototype.Ei = function(a, b, c) {
  c.y = this.k.y - a - b.height;
  c.x -= b.width / 2
};
function im(a) {
  this.w = a;
  this.is = new ml(this);
  this.$d = new tl(this);
  this.vi = new Al;
  this.js = new Hl(this);
  this.ij = new Pl(this);
  this.on = new dm(this)
}
A.e(im, km);
im.prototype.ao = function(a, b) {
  a.height -= b
};
im.prototype.dg = function(a, b) {
  km.prototype.cg.call(this, a, b)
};
im.prototype.cg = function(a, b) {
  km.prototype.dg.call(this, a, b)
};
im.prototype.Ei = function(a, b, c) {
  c.y = this.k.ra() + a;
  c.x -= b.width / 2
};
function fm(a) {
  this.w = a;
  this.is = new pl(this);
  this.$d = new wl(this);
  this.vi = new Dl;
  this.js = new Kl(this);
  this.ij = new Vl(this);
  this.on = new am(this)
}
A.e(fm, lm);
fm.prototype.ao = function(a, b) {
  a.width -= b;
  a.x += b
};
fm.prototype.Ei = function(a, b, c) {
  c.x = this.k.x - a - b.width;
  c.y -= b.height / 2
};
function gm(a) {
  this.w = a;
  this.is = new ql(this);
  this.$d = new xl(this);
  this.vi = new El;
  this.js = new Ll(this);
  this.ij = new Wl(this);
  this.on = new bm(this)
}
A.e(gm, lm);
gm.prototype.ao = function(a, b) {
  a.width -= b
};
gm.prototype.Ei = function(a, b, c) {
  c.x = this.k.Ja() + a;
  c.y -= b.height / 2
};
gm.prototype.dg = function(a, b) {
  lm.prototype.cg.call(this, a, b)
};
gm.prototype.cg = function(a, b) {
  lm.prototype.dg.call(this, a, b)
};
function mm(a, b) {
  Re.call(this, a, b)
}
A.e(mm, Re);
mm.prototype.Ea = p;
mm.prototype.Mb = u("Ea");
mm.prototype.g = function(a) {
  Ub(a, "label") && (this.Ea = new nm, this.Ea.g(F(a, "label")));
  return a
};
function om(a, b) {
  Re.call(this, a, b)
}
A.e(om, mm);
om.prototype.Kb = p;
om.prototype.g = function(a) {
  a = om.f.g.call(this, a);
  this.Kb = new Kd;
  this.Kb.g(a);
  return a
};
function pm() {
  $.call(this)
}
A.e(pm, $);
pm.prototype.cc = function() {
  return om
};
function qm(a, b) {
  Re.call(this, b, h)
}
A.e(qm, mm);
z = qm.prototype;
z.jz = p;
z.fz = p;
z.Hb = p;
z.mc = u("Hb");
z.Pc = p;
z.De = u("Pc");
z.g = function(a) {
  a = qm.f.g.call(this, a);
  Ub(a, "minimum_line") && (this.jz = new Kd, this.jz.g(F(a, "minimum_line")));
  Ub(a, "maximum_line") && (this.fz = new Kd, this.fz.g(F(a, "maximum_line")));
  Ub(a, "fill") && (this.Hb = new Id, this.Hb.g(F(a, "fill")));
  Ub(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  return a
};
function rm() {
  $.call(this)
}
A.e(rm, $);
rm.prototype.cc = function() {
  return qm
};
function sm(a) {
  this.w = a;
  this.dm = this.lB = this.Gt = j;
  this.b = new me
}
z = sm.prototype;
z.w = p;
z.Sg = p;
z.K = p;
z.Gt = p;
z.lB = p;
z.dm = p;
z.D = p;
z.g = function(a, b) {
  C(a, "visible") && (this.dm = K(a, "visible"));
  C(a, "display_under_data") && (this.Gt = K(a, "display_under_data"));
  C(a, "affects_scale_range") && (this.lB = K(a, "affects_scale_range"));
  this.kC(a, b)
};
z.kC = function(a, b) {
  var c = Ue(b, this.Rb(), a, J(a, "style"));
  this.K = this.Qb();
  this.K.g(c)
};
z.Qb = function() {
  A.xa()
};
z.Rb = function() {
  A.xa()
};
z.ta = function(a) {
  "{" == a.charAt(0) && (a = a.substr(1, a.length - 2));
  var b;
  if(this.w.aE()) {
    b = Number(a);
    if(!isNaN(b)) {
      return b - 1
    }
    if(this.w.Wk[a] != h) {
      return this.w.Df(a).na
    }
  }
  b = this.w.fa().$i(a);
  return!isNaN(b) ? b : this.w.Na(a)
};
z.vq = function(a) {
  var b = this.w.fa();
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
z.p = function(a) {
  this.D = new W(a);
  this.Gt ? this.w.ca().zB.ia(this.D) : this.w.ca().ww.ia(this.D);
  var b = this.Mb();
  b && b.p(a, this)
};
z.Mb = function() {
  return this.K.wa.Mb()
};
z.Na = function(a) {
  return re(a, this.w.ca()) ? this.w.ca().Na(a) : 0 == a.indexOf("%Axis") ? this.w.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  return re(a, this.w.ca()) ? this.w.ca().Pa(a) : 0 == a.indexOf("%Axis") ? this.w.Pa(a) : 4
};
z.qa = function() {
  this.D.clear()
};
z.It = function(a, b, c, d) {
  if(b.yu()) {
    var f = this.w.fa().rd(c), d = 0;
    switch(b.Ib()) {
      case tm:
        d = this.w;
        c = d.Ng ? d.Je + d.Zd : d.Je + d.Kl;
        d = c += d.Ml;
        break;
      case um:
        d = this.w;
        c = d.Ng ? d.Je + d.Zd : d.Je + d.Kl;
        c += d.Ml;
        d = c += d.Ou;
        break;
      case vm:
        d = this.w, c = d.Ng ? d.Je + d.Zd : d.Je + d.Kl, c = d.O != p ? c + Math.max(d.wk, wm(d)) : c + d.wk, c += d.Ou, d = c += d.Ml
    }
    this.w.J.on.yj(f, b.ga(), d, b.n(), b.iv);
    b.qa(a, this.Sg.fb())
  }else {
    c = this.w.fa().rd(c);
    d = this.w.fa().rd(d);
    switch(b.Ib()) {
      case xm:
        f = 0;
        break;
      case ym:
        f = 1;
        break;
      case zm:
        f = 2
    }
    this.w.J.on.zv(c, d, f, b.ga(), b.n(), b.iv);
    b.qa(a, this.D)
  }
};
var vm = 0, tm = 1, um = 2, xm = 3, ym = 4, zm = 5, Am = {fL:vm, cL:tm, eL:um, hP:xm, yO:ym, OO:zm};
function nm() {
  sm.call(this);
  this.Sc = new ye;
  this.iv = new O;
  this.hn = j;
  this.Ia = 5;
  this.Aa = vm
}
A.e(nm, sm);
z = nm.prototype;
z.Aa = p;
z.Ib = u("Aa");
z.Ia = p;
z.ga = u("Ia");
z.iv = p;
z.hn = p;
z.yu = u("hn");
z.Sc = p;
z.yc = p;
z.n = function() {
  return this.Sc.n()
};
z.g = function(a) {
  this.Sc.g(a);
  C(a, "padding") && (this.Ia = M(a, "padding"));
  C(a, "format") && (this.yc = new ie(J(a, "format")));
  if(C(a, "position")) {
    switch(Wb(a, "position")) {
      case "beforeaxislabels":
        this.Aa = vm;
        this.hn = j;
        break;
      case "afteraxislabels":
        this.Aa = tm;
        this.hn = j;
        break;
      case "axis":
        this.Aa = um;
        this.hn = j;
        break;
      case "near":
        this.Aa = xm;
        this.hn = q;
        break;
      case "center":
        this.Aa = ym;
        this.hn = q;
        break;
      case "far":
        this.Aa = zm, this.hn = q
    }
  }
};
z.p = function(a, b) {
  this.Sc.uc(a, this.yc.ta(b))
};
z.qa = function(a, b) {
  var c = this.Sc.ic(a);
  c.qb(this.iv.x, this.iv.y);
  b.ia(c)
};
function Bm(a) {
  sm.call(this, a)
}
A.e(Bm, sm);
z = Bm.prototype;
z.bb = p;
z.sf = p;
z.Qd = p;
z.yA = p;
z.xA = p;
z.fs = p;
z.g = function(a, b) {
  Bm.f.g.call(this, a, b);
  C(a, "value") && (this.yA = this.xA = this.fs = J(a, "value"));
  C(a, "start_value") && (this.yA = J(a, "start_value"));
  C(a, "end_value") && (this.xA = J(a, "end_value"))
};
z.Qb = function() {
  return new pm
};
z.Rb = x("line_axis_marker_style");
z.p = function(a) {
  this.yA && (this.sf = this.ta(this.yA));
  this.xA && (this.Qd = this.ta(this.xA));
  this.fs && (this.bb = this.ta(this.fs));
  this.lB && !this.w.aE() && (isNaN(this.sf) || this.vq(this.sf), isNaN(this.Qd) || this.vq(this.Qd));
  Bm.f.p.call(this, a)
};
z.qa = function(a) {
  if(this.dm) {
    Bm.f.qa.call(this, a);
    var b = this.K.wa, c = this.w.fa(), d = Ld(b.Kb, a), d = d + md(), c = Yl(this.w.J.on, c.rd(this.sf), c.rd(this.Qd)), f = a.ja();
    f.setAttribute("d", c);
    f.setAttribute("style", d);
    this.D.appendChild(f);
    b.Mb() && this.It(a, b.Mb(), this.sf, this.Qd)
  }
};
z.Na = function(a) {
  "%Value" == a && !ne(this.b, a) && this.b.add(a, (this.sf + this.Qd) / 2);
  ("%StartValue" == a || "%Start" == a) && !ne(this.b, a) && this.b.add(this.sf);
  ("%EndValue" == a || "%End" == a) && !ne(this.b, a) && this.b.add(this.sf);
  return Bm.f.Na.call(this, a)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%Start":
    ;
    case "%StartValue":
    ;
    case "%End":
    ;
    case "%EndValue":
      return 2
  }
};
function Cm(a) {
  sm.call(this, a)
}
A.e(Cm, sm);
z = Cm.prototype;
z.la = p;
z.pa = p;
z.op = p;
z.np = p;
z.jp = p;
z.ip = p;
z.cG = p;
z.$F = p;
z.bG = p;
z.aG = p;
z.ZF = p;
z.YF = p;
z.g = function(a, b) {
  Cm.f.g.call(this, a, b);
  C(a, "minimum") && (this.cG = J(a, "minimum"));
  C(a, "minimum_start") && (this.bG = J(a, "minimum_start"));
  C(a, "minimum_end") && (this.aG = J(a, "minimum_end"));
  C(a, "maximum") && (this.$F = J(a, "maximum"));
  C(a, "maximum_start") && (this.ZF = J(a, "maximum_start"));
  C(a, "maximum_end") && (this.YF = J(a, "maximum_end"))
};
z.Qb = function() {
  return new rm
};
z.Rb = x("range_axis_marker_style");
z.p = function(a) {
  this.cG && (this.op = this.np = this.la = this.ta(this.cG));
  this.bG && (this.op = this.ta(this.bG));
  this.aG && (this.np = this.ta(this.aG));
  this.$F && (this.jp = this.ip = this.pa = this.ta(this.$F));
  this.ZF && (this.jp = this.ta(this.ZF));
  this.YF && (this.ip = this.ta(this.YF));
  Cm.f.p.call(this, a)
};
z.qa = function(a) {
  if(this.dm) {
    Cm.f.qa.call(this, a);
    var b = this.K.wa, c = this.w.fa(), d = this.w.J.on, f, g = c.rd(this.op), k = c.rd(this.np), l = c.rd(this.jp), c = c.rd(this.ip);
    f = new P;
    var n = new O, m = new O, o = new O, v = new O;
    d.dg(g, n);
    d.dg(l, o);
    d.cg(k, m);
    d.cg(c, v);
    f.x = Math.min(n.x, o.x, m.x, v.x);
    f.y = Math.min(n.y, o.y, m.y, v.y);
    f.width = Math.max(n.x, o.x, m.x, v.x) - f.x;
    f.height = Math.max(n.y, o.y, m.y, v.y) - f.y;
    g = Yl(d, g, k, j);
    k = Yl(d, l, c, q, j);
    b.mc() && (f = Jd(b.mc(), a, f), f += nd(), n = a.ja(), n.setAttribute("d", g + k), n.setAttribute("style", f), this.D.appendChild(n));
    b.jz && (f = Ld(b.jz, a), f += md(), n = a.ja(), n.setAttribute("d", g), n.setAttribute("style", f), this.D.appendChild(n));
    b.fz && (f = Ld(b.fz, a), f += md(), n = a.ja(), n.setAttribute("d", Yl(d, l, c, j)), n.setAttribute("style", f), this.D.appendChild(n));
    b.De() && (f = Hc(b.De(), a), d = a.ja(), d.setAttribute("d", g + k), d.setAttribute("style", f), this.D.appendChild(d));
    b.Mb() && this.It(a, b.Mb(), (this.op + this.jp) / 2, (this.np + this.ip) / 2)
  }
};
z.Na = function(a) {
  ne(this.b, a) || ("%Value" == a && this.b.add(a, (this.op + this.np + this.jp + this.ip) / 4), ("%MinValue" == a || "%Min" == a) && this.b.add(a, (this.op + this.np) / 2), ("%MaxValue" == a || "%Max" == a) && this.b.add(a, (this.jp + this.ip) / 2), ("%MinStartValue" == a || "%MinStart" == a) && this.b.add(a, this.op), ("%MaxStartValue" == a || "%MaxStart" == a) && this.b.add(a, this.jp), ("%MinEndValue" == a || "%MinEnd" == a) && this.b.add(a, this.np), ("%MaxEndValue" == a || "%MaxEnd" == a) && 
  this.b.add(a, this.ip));
  return Cm.f.Na.call(this, a)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%MinValue":
    ;
    case "%Min":
    ;
    case "%MaxValue":
    ;
    case "%Max":
    ;
    case "%MinStartValue":
    ;
    case "%MinStart":
    ;
    case "%MaxStartValue":
    ;
    case "%MaxStart":
    ;
    case "%MinEndValue":
    ;
    case "%MinEnd":
    ;
    case "%MaxEndValue":
    ;
    case "%MaxEnd":
      return 2
  }
  return Cm.f.Pa.call(this, a)
};
function Dm() {
  this.vw = this.Jw = this.Us = 0
}
z = Dm.prototype;
z.w = p;
z.wj = t("w");
z.D = p;
z.fb = u("D");
z.DK = t("D");
z.He = p;
z.Us = p;
z.Jw = p;
z.vw = p;
z.g = function(a, b) {
  this.He = [];
  var c, d, f, g, k;
  if(C(a, "lines")) {
    c = F(a, "lines");
    if(!C(c, "line")) {
      return
    }
    k = Em(c);
    g = I(c, "line");
    c = 0;
    for(d = g.length;c < d;c++) {
      f = new Bm(this.w), f.Gt = k, f.g(g[c], b), f.Sg = this, this.He.push(f)
    }
  }
  if(C(a, "ranges") && (c = F(a, "ranges"), C(c, "range"))) {
    k = Em(c);
    g = I(c, "range");
    c = 0;
    for(d = g.length;c < d;c++) {
      f = new Cm(this.w), f.Gt = k, f.g(g[c], b), f.Sg = this, this.He.push(f)
    }
  }
};
function Em(a) {
  return C(a, "display_under_data") ? K(a, "display_under_data") : j
}
z.p = function(a) {
  for(var b = this.He.length, c = 0;c < b;c++) {
    var d = this.He[c];
    d.p(a);
    if((d = d.Mb()) && d.yu()) {
      var f = Am, g = this.w.J.ij;
      switch(d.Ib()) {
        case f.cL:
          d = g.pg(d.n()) + d.ga();
          this.Us = Math.max(d, this.Us);
          break;
        case f.fL:
          d = g.pg(d.n()) + d.ga();
          this.vw = Math.max(d, this.vw);
          break;
        case f.eL:
          d = g.pg(d.n()) + d.ga(), this.Jw = Math.max(d, this.Jw)
      }
    }
  }
};
z.qa = function(a) {
  for(var b = this.He.length, c = 0;c < b;c++) {
    this.He[c].qa(a)
  }
};
function Fm() {
}
z = Fm.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.VI = j;
z.ee = p;
z.Ye = u("ee");
z.Hm = p;
z.Kt = p;
z.yn = p;
z.$u = p;
z.g = function(a) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  C(a, "interlaced") && (this.VI = K(a, "interlaced"));
  Tb(F(a, "line")) && (this.ee = new Kd, this.ee.g(F(a, "line")));
  if(this.VI && C(a, "interlaced_fills")) {
    a = F(a, "interlaced_fills");
    if(C(a, "even")) {
      var b = F(a, "even");
      C(b, "fill") && Tb(F(b, "fill")) && (this.Hm = new Id, this.Hm.g(F(b, "fill")));
      C(b, "hatch_fill") && Tb(F(b, "hatch_fill")) && (this.Kt = new Fc, this.Kt.g(F(b, "hatch_fill")))
    }
    if(C(a, "odd") && (a = F(a, "odd"), C(a, "fill") && Tb(F(a, "fill")) && (this.yn = new Id, this.yn.g(F(a, "fill"))), C(a, "hatch_fill") && Tb(F(a, "hatch_fill")))) {
      this.$u = new Fc, this.$u.g(F(a, "hatch_fill"))
    }
  }
};
function Gm() {
  this.Kb = new Kd;
  this.Kb.at = 0
}
z = Gm.prototype;
z.Kb = p;
z.ke = 10;
z.ec = u("ke");
z.tl = q;
z.uz = j;
z.yu = u("uz");
z.UE = q;
z.k = p;
z.g = function(a) {
  this.Kb.g(a);
  C(a, "size") && (this.ke = M(a, "size"));
  C(a, "inside") && (this.tl = K(a, "inside"));
  C(a, "outside") && (this.uz = K(a, "outside"));
  C(a, "opposite") && (this.UE = K(a, "opposite"));
  !this.tl && !this.uz && !this.UE ? this.Kb = p : this.k = new P
};
z.qa = function(a, b, c, d, f, g) {
  var k = this.Kb.Va(), l, n;
  this.tl && (b && f.yv(k, c, d, this.ke, this.k), l = g || Ld(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Uv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)));
  this.uz && (b && f.Bv(k, c, d, this.ke, this.k), l = g || Ld(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Wv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)));
  this.UE && (b && A.xa(), l = g || Ld(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Vv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)))
};
function Hm(a, b) {
  Ce.apply(this);
  this.w = a;
  this.J = b;
  this.Aa = new O;
  this.J.js.HF(this)
}
A.e(Hm, Ce);
z = Hm.prototype;
z.Je = 0;
z.Tf = u("Je");
z.Ei = t("Je");
z.yd = 0;
z.nd = u("yd");
z.Aa = p;
z.J = p;
z.w = p;
z.Bh = u("yc");
z.g = function(a) {
  var b = this.Bb.bc;
  Hm.f.g.call(this, a);
  Ae(this, -b - this.Bb.bc)
};
z.p = function(a) {
  this.uc(a, this.yc.ta(this.w, this.w.Gd()));
  return this.yd = this.J.ij.pg(this.n()) + this.Ia
};
function Im() {
  this.ba = [];
  this.b = new me;
  this.Cb();
  this.Vm = new P;
  this.IK = new nc
}
z = Im.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.Jb = x(0);
z.r = function() {
  return this.aa.r()
};
z.Aa = NaN;
z.Ib = u("Aa");
z.qb = t("Aa");
z.Sa = p;
z.getName = function() {
  return this.Ya ? this.Ya.Bh().ta(this) : this.Sa
};
z.Rl = t("Sa");
z.aa = p;
z.ca = u("aa");
z.Tl = t("aa");
z.cn = p;
z.ba = p;
z.Mm = 0;
z.KA = 0;
z.yd = 0;
z.nd = u("yd");
z.Zd = 0;
z.kr = 0;
z.Kl = 0;
z.Cr = 0;
z.wk = 0;
z.Ou = 0;
z.He = p;
z.J = p;
z.La = p;
z.fa = u("La");
z.$B = function(a) {
  this.J = a.$B(this)
};
z.Xf = p;
z.mJ = u("Xf");
z.aE = x(q);
z.Ng = p;
z.Ia = 0;
z.Je = 0;
z.Ei = t("Je");
z.Hx = 0;
z.Yf = p;
z.ag = p;
z.Zf = p;
z.mf = p;
z.ee = p;
z.em = p;
z.Ya = p;
z.O = p;
z.To = q;
z.IK = p;
z.Ml = 0;
z.xp = p;
z.Gd = function() {
  return this.aa.Gd()
};
z.$i = function(a) {
  return this.La.$i(a)
};
z.Gc = function(a) {
  var b = this.La;
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
z.transform = function(a, b, c, d) {
  isNaN(d) && (d = 0);
  this.J.Fv(this.La.pc(b) + d, c)
};
z.zk = s();
z.QB = s();
z.vJ = x(q);
z.g = function(a, b) {
  this.Je = this.Mm = 0;
  this.Hx = this.Ia = 10;
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  if(this.Ma) {
    Ub(a, "zoom") && Ke("Zoom");
    var c = F(a, "major_grid");
    Tb(c) && (this.Yf = new Fm, this.Yf.g(c), Jm(this, this.Yf));
    c = F(a, "minor_grid");
    Tb(c) && (this.ag = new Fm, this.ag.g(c), Jm(this, this.ag));
    this.Kl = this.kr = this.Zd = 0;
    c = F(a, "major_tickmark");
    if(Tb(c) && (this.Zf = new Gm, this.Zf.g(c), this.Zf.Kb != p && (this.Zf.Kb.se() && this.J.$d.Me(this.Zf.Kb.yb), this.Zf.yu() && (this.Zd += this.Zf.ec(), this.Kl = this.Zf.ec()), this.Zf.tl && !this.Ng))) {
      this.Zd += this.Zf.ec(), this.kr = this.Zf.ec()
    }
    c = F(a, "minor_tickmark");
    if(Tb(c) && (this.mf = new Gm, this.mf.g(c), this.mf.Kb != p)) {
      if(this.mf.Kb.se() && this.J.$d.Me(this.mf.Kb.yb), c = 0, this.mf.yu() && (c += this.mf.ec(), this.mf.ec() > this.Kl && (this.Kl = this.mf.ec())), this.mf.tl && !this.Ng && (c += this.mf.ec(), this.mf.ec() > this.kr && (this.kr = this.mf.ec())), c > this.Zd) {
        this.Zd = c
      }
    }
    c = F(a, "line");
    Tb(c) && (this.ee = new Kd, this.ee.g(c), this.ee.se() && (this.AE = new P, this.J.$d.Me(this.ee.yb)));
    c = F(a, "zero_line");
    Tb(c) && (this.em = new Kd, this.em.g(c), this.em.se() && (this.wG = new P, this.J.$d.Me(this.em.yb)));
    this.Mm = this.Zd;
    this.Hx += this.kr;
    this.Ng || (this.Mm += this.Ia);
    c = F(a, "title");
    Tb(c) && (this.Ya = new Hm(this, this.J), this.Ya.g(c));
    c = F(a, "labels");
    Tb(c) && (this.O = this.lx(c)) && this.O.g(c);
    Ub(a, "axis_markers") && (this.He = new Dm, this.He.wj(this), this.He.g(F(a, "axis_markers"), b), this.He.DK(this.tp));
    this.yd = this.Mm
  }
};
function Km(a) {
  return 3 == a.La.Jb() || 2 == a.La.Jb()
}
z.lx = function() {
  A.xa()
};
function Jm(a, b) {
  b.Hm != p && b.Hm.se() && a.J.$d.Me(b.Hm.yb);
  b.yn != p && b.yn.se() && a.J.$d.Me(b.yn.yb);
  b.ee != p && b.Ye().se() && a.J.$d.Me(b.Ye().yb)
}
z.b = p;
z.Cb = function() {
  this.b.add("%AxisName", "");
  this.b.add("%AxisValue", 0);
  this.b.add("%AxisMax", -Number.MAX_VALUE);
  this.b.add("%AxisMin", Number.MAX_VALUE);
  this.b.add("%AxisBubbleSizeMin", Number.MAX_VALUE);
  this.b.add("%AxisBubbleSizeMax", -Number.MAX_VALUE)
};
z.Na = function(a) {
  if(("%AxisValue" == a || "%Value" == a) && this.O) {
    return this.O.JI()
  }
  if(re(a, this.ca())) {
    return this.ca().Na(a)
  }
  "%AxisScaleMax" == a && this.b.add("%AxisScaleMax", this.La.pa);
  "%AxisScaleMin" == a && this.b.add("%AxisScaleMin", this.La.la);
  ne(this.b, "%AxisRange") || this.b.add("%AxisRange", this.b.get("%AxisMax") - this.b.get("%AxisMin"));
  ne(this.b, "%AxisAverage") || this.b.add("%AxisAverage", this.b.get("%AxisSum") / this.ba.length);
  ne(this.b, "%AxisMedian") || this.b.add("%AxisMedian", pc(this.ba, this.cn ? "%XValue" : "%YValue"));
  ne(this.b, "%AxisMode") || this.b.add("%AxisMode", this.IK.ml(this.ba, this.cn ? "%XValue" : "%YValue"));
  return this.b.get(a)
};
z.Pa = function(a) {
  if(Km(this) && "%Value" == a) {
    return 3
  }
  switch(a) {
    default:
      return 1;
    case "%Value":
    ;
    case "%AxisValue":
    ;
    case "%AxisSum":
    ;
    case "%AxisMax":
    ;
    case "%AxisMin":
    ;
    case "%AxisRange":
    ;
    case "%AxisScaleMax":
    ;
    case "%AxisScaleMin":
    ;
    case "%AxisAverage":
    ;
    case "%AxisBubbleSizeSum":
    ;
    case "%AxisBubbleSizeMin":
    ;
    case "%AxisBubbleSizeMax":
    ;
    case "%AxisMode":
    ;
    case "%AxisMedian":
      return 2
  }
};
function Lm(a) {
  a.Cr = 0;
  a.wk = 0;
  a.Ou = 0;
  a.He != p && (a.He.p(a.aa.r()), a.Ou = a.He.Us, a.Cr = a.He.vw + a.He.Us, a.wk = a.He.Jw)
}
function wm(a) {
  return a.O && !a.O.tl ? a.O.$v : 0
}
function Mm(a, b) {
  a.Ma && (a.J.Jp(b), a.La.Ae(), a.O && a.O.GB())
}
function Nm(a, b) {
  a.Ma && (a.O ? a.J.ao(b, Math.max(wm(a), a.wk)) : a.J.ao(b, a.wk))
}
z.Rc = function(a) {
  this.J.Jp(a);
  this.La.Ae();
  Om(this)
};
z.AK = function(a, b) {
  Pm(this, a, b, this.La.fc)
};
function Pm(a, b, c, d) {
  if(a.Ma && a.O != p) {
    var f = a.O.Qx(a.aa.r(), 0), d = a.O.Qx(a.aa.r(), d);
    a.J.Jp(c);
    f && d && (f.x > b.width ? f = d : d.x > b.width && (d = f), f.y > b.height ? f = d : d.y > b.height && (d = f), a.J.ij.oB(b, f, d))
  }
}
function rh(a) {
  return a.O && a.O.isEnabled()
}
z.Qs = function() {
  return this.O.$n
};
function sh(a) {
  a.O.an(a.aa.r())
}
z.Kx = p;
z.Xg = p;
z.tp = p;
z.AE = p;
z.wG = p;
function Qm(a) {
  if(a.Ma && a.ee != p) {
    var b = a.Je, c = a.J.vD(), d = a.J.uD();
    a.ee.se() && a.J.$d.zj(a.ee.Va(), c, d, b, a.AE);
    var f = Ld(a.ee, a.aa.r(), a.AE);
    f && (b = a.J.$d.Xv(a.Kx.r(), c, d, b, a.ee.Va()), b.setAttribute("style", f), a.Kx.appendChild(b))
  }
}
z.vC = function() {
  if(this.Ma && !(this.em == p || !this.La.contains(0) || 0 == this.La.la)) {
    var a = this.La.pc(0);
    this.em.se() && this.J.$d.Cv(this.em.Va(), a, this.wG);
    var b = Ld(this.em, this.Xg.r(), this.wG);
    b && (a = this.J.$d.Yv(this.Xg.r(), a, this.em.Va()), a.setAttribute("style", b), this.Xg.appendChild(a))
  }
};
function Om(a) {
  a.Ma && a.Ya && (a.Ya.p(a.Xg.r()), a.KA = a.Ya.nd())
}
z.zx = function() {
  if(this.Ma && this.Ya) {
    var a = this.Ng ? this.Zd : this.Kl, b = this.Je;
    this.O && this.O.isEnabled() ? a += Math.max(this.wk, wm(this)) : b += this.wk;
    b += this.Ml + this.Cr;
    this.Ya.Ei(b + a);
    a = this.Ya;
    b = this.Kx;
    a.J.js.Aj(a, a.Aa);
    var c = a.ic(b.r());
    c.qb(a.Aa.x, a.Aa.y);
    b.ia(c)
  }
};
z.xo = function() {
  if(this.Ma && this.O != p) {
    if(this.O.tl) {
      this.O.Ei(this.Je - this.kr - 2 * this.O.ga() - this.O.$v)
    }else {
      var a = this.Ng ? this.Zd : this.Kl, b = this.Je, a = a + this.O.ga(), b = b + this.Ml, b = b + this.Ou;
      this.O.Ei(b + a)
    }
    this.O.xo(this.tp)
  }
};
function Rm(a) {
  a.Ma && (a.mf != p && a.mf.Kb != p && a.uC(), a.Zf != p && a.Zf.Kb != p && a.tC())
}
z.tC = function() {
  var a = this.Zf.Kb.se(), b = p;
  a || (b = Ld(this.Zf.Kb, this.tp.r()));
  var c = this.Je, d = this.La.fc, f = 0;
  this.To && (d--, f = 0.5);
  for(var g = 0;g <= d;g++) {
    var k = this.La.Nc(g) + f;
    k >= this.La.la && k <= this.La.pa && this.Zf.qa(this.tp, a, this.La.rd(k), c, this.J.is, b)
  }
};
z.uC = function() {
  var a = this.mf.Kb.se(), b = p;
  a || (b = Ld(this.mf.Kb, this.tp.r()));
  var c = this.Je, d = 0;
  this.To && (d = 0.5);
  var f = this.fa().fc - 1, g = q;
  this.fa().Nc(f) < this.fa().pa && (f++, g = j);
  for(var k = this.fa().If, l = 0;l <= f;l++) {
    for(var n = this.fa().Nc(l), m = 1;m < k;m++) {
      if(!(0 != d && l + 1 == this.La.fc && m == this.La.If)) {
        var o = this.fa().kk(n, m) + d;
        if(g && !this.La.Vy(o)) {
          break
        }
        this.mf.qa(this.tp, a, this.La.rd(o), c, this.J.is, b)
      }
    }
  }
};
function Sm(a) {
  if(a.Ma && a.Yf && a.Yf.ee != p) {
    var b = a.Yf.Ye().se(), c = p;
    b || (c = Ld(a.Yf.Ye(), a.Xg.r()));
    var d = a.La.fc, f = 0;
    a.To && (d--, f = 0.5);
    for(var g = 0;g <= d;g++) {
      var k = a.La.rd(a.La.Nc(g) + f);
      b && (a.J.$d.Cv(a.Yf.Ye().Va(), k, a.Vm), c = Ld(a.Yf.Ye(), a.Xg.r(), a.Vm));
      c && (k = a.J.$d.Yv(a.Xg.r(), k, a.Yf.Ye().Va()), k.setAttribute("style", c), a.Xg.appendChild(k))
    }
  }
}
function Tm(a) {
  if(a.Ma && a.ag && a.ag.ee != p) {
    var b = a.ag.Ye().se(), c = p;
    b || (c = Ld(a.ag.Ye(), a.Xg.r()));
    var d = a.La.fc - 1, f = q;
    a.La.Nc(d) < a.fa().pa && (d++, f = j);
    var g = 0;
    a.To && (g = 0.5);
    for(var k = 0;k <= d;k++) {
      for(var l = a.La.Nc(k), n = 1;n < a.La.If;n++) {
        if(!(0 != g && k + 1 == a.La.fc && n == a.La.If)) {
          var m = a.La.kk(l, n) + g;
          if(f && !a.La.Vy(m)) {
            break
          }
          m = a.La.rd(m);
          b && (a.J.$d.Cv(a.ag.Ye().Va(), m, a.Vm), c = c = Ld(a.ag.Ye(), a.Xg.r(), a.Vm));
          c && (m = a.J.$d.Yv(a.Xg.r(), m, a.ag.Ye().Va()), m.setAttribute("style", c), a.Xg.appendChild(m))
        }
      }
    }
  }
}
function Um(a) {
  if(a.Ma) {
    var b, c;
    if(a.ag != p && (b = a.ag.Hm != p != p || a.ag.Kt != p != p, c = a.ag.yn != p != p || a.ag.$u != p != p, b || c)) {
      var d = q, f = 0;
      a.To && (f = 0.5);
      for(var g = 0;g < a.La.fc;g++) {
        for(var k = a.La.Nc(g), l = 0;l < a.La.If;l++) {
          if(!(0 != f && g + 1 == a.La.fc && l == a.La.If)) {
            if(d && c || !d && b) {
              var n = a.La.kk(k, l) + f, m = a.La.kk(k, l + 1) + f;
              Vm(a, n, m, a.ag, d)
            }
            d = !d
          }
        }
      }
    }
    if(a.Yf != p && (b = a.Yf.Hm != p != p || a.Yf.Kt != p != p, c = a.Yf.yn != p != p || a.Yf.$u != p != p, b || c)) {
      d = q;
      f = a.La.fc;
      g = 0;
      a.To && (f--, g = 0.5);
      for(k = 0;k < f;k++) {
        if(d && c || !d && b) {
          l = a.La.Nc(k) + g, n = a.La.Nc(k + 1) + g, Vm(a, l, n, a.Yf, d)
        }
        d = !d
      }
    }
  }
}
function Vm(a, b, c, d, f) {
  b = a.La.rd(b);
  c = a.La.rd(c);
  a.J.$d.EF(b, c, a.Vm);
  f ? (b = d.yn, d = d.$u) : (b = d.Hm, d = d.Kt);
  c = a.Xg;
  f = p;
  if(b != p && (f = Jd(b, c.r(), a.Vm))) {
    b = a.J.$d.CA(c.r(), a.Vm), b.setAttribute("style", f), c.appendChild(b)
  }
  if(d != p && (f = Hc(d, c.r()))) {
    b = a.J.$d.CA(c.r(), a.Vm), b.setAttribute("style", f), c.appendChild(b)
  }
}
z.mb = function(a) {
  if(a == h || a == p) {
    a = {}
  }
  a.Key = this.Sa;
  a.Name = this.b.get("%AxisName");
  a.Sum = this.b.get("%AxisSum");
  a.Max = this.b.get("%AxisMax");
  a.Min = this.b.get("%AxisMin");
  a.BubbleSizeMax = this.b.get("%AxisBubbleMax");
  a.BubbleSizeMin = this.b.get("%AxisBubbleMin");
  a.BubbleSizeSum = this.b.get("%AxisBubbleSum");
  a.Average = this.b.get("%AxisAverage");
  a.Median = this.b.get("%AxisMedian");
  a.Mode = this.b.get("%AxisMode");
  return a
};
function Wm() {
  Im.apply(this)
}
A.e(Wm, Im);
Wm.prototype.Jb = x(1);
Wm.prototype.vJ = x(j);
Wm.prototype.wH = function(a) {
  switch(Vb(F(a, "type"))) {
    default:
    ;
    case "linear":
      a = new qh(this);
      break;
    case "logarithmic":
      a = new vh(this);
      break;
    case "datetime":
      a = new xh(this)
  }
  return a
};
Wm.prototype.lx = function(a) {
  return C(a, "display_mode") && "stager" == N(a, "display_mode") ? new Ql(this, this.J.ij) : new Ml(this, this.J.ij)
};
function Xm(a) {
  this.aa = a;
  this.ba = [];
  this.Ge()
}
z = Xm.prototype;
z.Sa = p;
z.Rl = t("Sa");
z.getName = u("Sa");
z.na = NaN;
z.Vr = t("na");
z.Xa = 0;
z.aa = p;
z.ca = u("aa");
z.ba = p;
z.Ed = function(a) {
  a.kt(this);
  this.b.add("%CategoryName", this.Sa);
  this.b.add("%Name", this.Sa);
  this.b.add("%CategoryIndex", this.na);
  this.ba.push(a)
};
z.b = p;
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%CategoryName", "");
  this.b.add("%Name", "");
  this.b.add("%CategoryIndex", "");
  this.b.add("%CategoryPointCount", 0);
  this.b.add("%CategoryYSum", 0);
  this.b.add("%CategoryYMax", 0);
  this.b.add("%CategoryYMin", 0);
  this.b.add("%CategoryYRangeMax", -Number.MAX_VALUE);
  this.b.add("%CategoryYRangeMin", Number.MAX_VALUE);
  this.b.add("%CategoryYRangeSum", 0);
  this.b.add("%CategoryYPercentOfTotal", 0);
  this.b.add("%CategoryYAverage", 0)
};
z.Pa = function(a) {
  if(re(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%CategoryPointCount":
    ;
    case "%CategoryYSum":
    ;
    case "%CategoryYMax":
    ;
    case "%CategoryYMin":
    ;
    case "%CategoryYRangeMax":
    ;
    case "%CategoryYRangeMin":
    ;
    case "%CategoryYRangeSum":
    ;
    case "%CategoryYPercentOfTotal":
    ;
    case "%CategoryYAverage":
      return 2;
    default:
    ;
    case "%CategoryName":
    ;
    case "%Name":
      return 1
  }
};
z.Na = function(a) {
  if(re(a, this.ca())) {
    return this.ca().b.get(a)
  }
  ne(this.b, "%CategoryYPercentOfTotal") || this.b.add("%CategoryYPercentOfTotal", 100 * (this.b.get("%CategoryYSum") / this.ca().Na("%DataPlotYSum")));
  ne(this.b, "%CategoryYAverage") || this.b.add("%CategoryYAverage", this.b.get("%CategoryYSum") / this.b.get("%CategoryPointCount"));
  return this.b.get(a)
};
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  a.Name = this.Sa;
  a.XSum = this.b.get("%CategoryXSum");
  a.YSum = this.b.get("%CategoryYSum");
  a.BubbleSizeSum = this.b.get("%CategoryBubbleSizeSum");
  a.YRangeMax = this.b.get("%CategoryRangeMax");
  a.YRangeMin = this.b.get("%CategoryRangeMin");
  a.YRangeSum = this.b.get("%CategoryRangeSum");
  a.YPercentOfTotal = this.b.get("%CategoryYPercentOfTotal");
  a.YMedian = this.b.get("%CategoryYMedian");
  a.YMode = this.b.get("%CategoryYMode");
  a.YAverage = this.b.get("%CategoryYAverage");
  return a
};
z.ah = function() {
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].ah()
  }
};
z.dh = function() {
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].dh()
  }
};
function Ym(a) {
  Xm.call(this, a)
}
A.e(Ym, Xm);
Ym.prototype.w = p;
Ym.prototype.wj = t("w");
function Zm() {
  this.Hw = {}
}
z = Zm.prototype;
z.Wm = NaN;
z.Cn = NaN;
z.Hl = 0;
z.Hw = p;
z.nt = NaN;
z.aH = NaN;
z.Ae = function(a) {
  this.aH = a;
  this.nt = a / (this.Wm + this.Hl + this.Cn * (this.Hl - 1))
};
z.Tf = function(a) {
  return this.nt * (a * (1 + this.Cn) + 0.5 + this.Wm / 2) - this.aH / 2
};
function $m(a) {
  this.qJ = a;
  this.yp = [];
  this.kj = [];
  this.ba = []
}
z = $m.prototype;
z.qJ = q;
z.Nl = 0;
z.Gl = 0;
z.yp = p;
z.kj = p;
z.ba = p;
z.Od = function(a, b) {
  var c = j;
  if(0 == b && 0 < this.ba.length) {
    for(var c = this.ba.length - 1, d = this.ba[c];0 == d.$b && 0 < c;) {
      c--, d = this.ba[c]
    }
    c = 0 <= d.$b
  }
  this.ba.push(a);
  d = 0 < b;
  0 == b && (d = c);
  if(d) {
    return 0 < this.yp.length && (a.gf = this.yp[this.yp.length - 1]), this.yp.push(a), this.Nl += b
  }
  0 < this.kj.length && (a.gf = this.kj[this.kj.length - 1]);
  this.kj.push(a);
  return this.Gl += b
};
function an(a, b) {
  return a.qJ ? 100 : 0 < b ? a.Nl : a.Gl
}
function bn(a, b, c, d, f) {
  Xm.call(this, a);
  this.na = b;
  this.Sa = c;
  this.cn = d;
  this.w = f;
  this.Lk = {};
  this.Bz = []
}
A.e(bn, Ym);
z = bn.prototype;
z.cn = j;
z.zf = p;
z.xF = t("zf");
z.Lk = p;
z.Bz = p;
z.Iw = p;
z.co = p;
z.Ts = p;
function cn(a, b, c, d) {
  a.Lk[b] == p && (a.Lk[b] = {});
  a.Lk[b][c] == p && (a.Lk[b][c] = new $m(d), d && a.Bz.push(a.Lk[b][c]));
  return a.Lk[b][c]
}
z.zk = function() {
  for(var a = 0;a < this.Bz.length;a++) {
    for(var b = this.Bz[a], c = h, c = 0;c < b.yp.length;c++) {
      b.yp[c].GF(b.Nl)
    }
    for(c = 0;c < b.kj.length;c++) {
      b.kj[c].GF(b.Gl);
      var d = b.kj[c].Lf;
      b.kj[c].Lf = -b.kj[c].$b;
      b.kj[c].$b = -d
    }
  }
};
function dn() {
  Im.call(this);
  this.Wh = [];
  this.Wk = {};
  this.mt = []
}
A.e(dn, Im);
z = dn.prototype;
z.Wk = p;
z.Wh = p;
z.mt = p;
z.aE = x(j);
z.vC = s();
z.zk = function() {
  for(var a = 0;a < this.Wh.length;a++) {
    this.Wh[a].zk()
  }
};
z.Rc = function(a) {
  dn.f.Rc.call(this, a);
  for(var a = this.La.pc(this.La.la + 1) - this.La.pc(this.La.la), b = 0;b < this.mt.length;b++) {
    this.mt[b].Ae(a)
  }
};
z.lx = x(p);
z.AK = function(a, b) {
  Pm(this, a, b, this.La.fc - 1)
};
z.Df = function(a, b) {
  return this.Wk[a] == h ? en(this, a, b) : this.Wk[a]
};
z.pK = function(a) {
  this.aa.Bq.push(a)
};
z.g = function(a, b) {
  dn.f.g.call(this, a, b);
  C(a, "tickmarks_placement") && (this.To = "center" == Wb(a, "tickmarks_placement"))
};
function en(a, b, c) {
  var d = new bn(a.aa, a.Wh.length, b, a.cn, a);
  a.cn ? (a.aa.$K.push(d), a.pK(d)) : a.aa.aL.push(d);
  if(a.O != p) {
    var f = a.O;
    f.Vp = d;
    var g = f.yc.ta(f.w, f.w.Gd());
    f.Vp.Iw = g;
    f.uc(c, f.Vp.Iw);
    c = f.k.Ca();
    f.Vp.co = c;
    c = f.nf.Ca();
    f.Vp.Ts = c;
    f.Vp = p
  }
  a.Gc(d.na);
  a.Wh.push(d);
  return a.Wk[b] = d
}
z.lx = function(a) {
  return C(a, "display_mode") && "stager" == N(a, "display_mode") ? new fn(this, this.J.ij) : new gn(this, this.J.ij)
};
z.wH = function(a) {
  return!a || !C(a, "type") || "datetime" != Wb(a, "type") ? new th(this) : new uh(this)
};
function hn() {
  dn.call(this);
  this.SB = {}
}
A.e(hn, dn);
hn.prototype.SB = {};
hn.prototype.Jb = x(2);
hn.prototype.QB = function(a) {
  if(a.en()) {
    var b = this.SB[a.Xj];
    b == p && (b = new Zm, b.Wm = a.Ab().Wm, b.Cn = a.Ab().Cn, this.SB[a.Xj] = b, this.mt.push(b));
    a.xF(b);
    if(a.eH()) {
      a.po = 0, b.Hl = 1
    }else {
      var c = a.eb, d = b.Hl;
      1 != c.Jb() || 0 == c.fa().ml() ? (b.Hl++, b = d) : b.Hw[c.getName()] != p ? b = b.Hw[c.getName()] : (b.Hw[c.getName()] = d, b.Hl++, b = d);
      a.po = b
    }
  }
};
hn.prototype.transform = function(a, b, c, d) {
  a.o.en() ? (isNaN(d) && (d = 0), b = this.La.pc(b) + d, a = a.o, b += a.zf.Tf(a.po), this.J.Fv(b, c)) : hn.f.transform.call(this, a, b, c, d)
};
function jn() {
  dn.call(this)
}
A.e(jn, dn);
jn.prototype.Jb = x(3);
jn.prototype.QB = function(a, b) {
  var c = en(this, a.getName(), b);
  a.Dc = c;
  var d = new Zm;
  d.Wm = a.Ab().Wm;
  d.Cn = a.Ab().Cn;
  c.xF(d);
  c = a.eb;
  1 == c.Jb() && 0 != c.fa().ml() && (a.sJ = j, d.Hl = 1);
  this.mt.push(d)
};
jn.prototype.transform = function(a, b, c, d) {
  isNaN(d) && (d = 0);
  b = this.La.pc(b) + d;
  b += a.Df().zf.Tf(a.po);
  this.J.Fv(b, c)
};
jn.prototype.pK = s();
function gn(a, b) {
  Ml.call(this, a, b)
}
A.e(gn, Ml);
z = gn.prototype;
z.Vp = p;
z.JI = function() {
  return this.w.fa().mI(this.Vp)
};
z.an = function() {
  this.El = new P;
  this.Kd = new P;
  for(var a = this.w.fa(), b = a.fc, c = 0;c <= b;c++) {
    var d;
    if(d = this.w.Wh[a.Nc(c) + 0.5]) {
      var f = d.co;
      d = d.Ts;
      f.width > this.Kd.width && (this.Kd.width = f.width, this.El.width = d.width);
      f.height > this.Kd.height && (this.Kd.height = f.height, this.El.height = d.height)
    }
  }
};
z.xo = function(a) {
  var b = new O, c = this.J.J.w.fa(), d = !this.$n, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k;
  this.Bb.or || (k = this.J.Cy(this.El, this.Bb));
  for(var l = 0;l < c.fc;l++) {
    var n = c.Nc(l), m = this.w.Wh[n + 0.5];
    if(m) {
      var o = m.co, v = m.Ts;
      this.J.qf(this, b, o, v, c.rd(n + 0.5));
      if(d) {
        if(this.J.Gy(f, this.Bb, b, g, k, o, v)) {
          continue
        }
        g = this.J.Rx(f, this.Bb, b, k, o, v)
      }
      Ol(this, b, o, v, this.Bb);
      this.uc(a.r(), m.Iw);
      n = this.ic(a.r());
      n.qb(b.x, b.y);
      a.ia(n)
    }
  }
};
z.Qx = function(a, b) {
  var c = new O, d = this.w.fa().Nc(b) + 0.5, f = this.w.Wh[d];
  if(!f) {
    return p
  }
  this.J.qf(this, c, f.co, f.Ts, this.w.fa().rd(d));
  Ol(this, c, f.co, f.Ts, this.Bb);
  d = f.co.Ca();
  d.x = c.x;
  d.y = c.y;
  return d
};
function fn(a, b) {
  Ml.call(this, a, b)
}
A.e(fn, gn);
z = fn.prototype;
z.an = function() {
  this.Kd = new P;
  this.fi = new P;
  for(var a = this.w.fa(), b = a.fc, c = 0;c <= b;c++) {
    var d;
    if(d = this.w.Wh[a.Nc(c) + 0.5]) {
      d = d.co;
      var f = 0 == c % 2 ? this.Kd : this.fi;
      d.width > f.width && (f.width = d.width);
      d.height > f.height && (f.height = d.height)
    }
  }
};
z.Ys = function(a) {
  this.an(a);
  var a = this.J.Ho(this.Kd, this.Kd, this.Bb), b = this.J.Ho(this.fi, this.fi, this.Bb), a = Math.ceil(a + b);
  return 1 > a ? 1 : a
};
z.GB = function() {
  this.yd = this.J.pg(this.Kd) + this.J.pg(this.fi) + this.Ia;
  this.$v = this.yd + this.Ia
};
z.g = function(a) {
  fn.f.g.call(this, a);
  Ae(this, 0)
};
z.xo = function(a) {
  for(var b = new O, c = !this.$n, d = this.w.fa(), f = c && this.J.tr(d.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k = g, l = this.J.pg(this.Kd), n = this.J.pg(this.fi), m, o, v, w = 0;w < d.fc;w++) {
    var y = d.Nc(w), E = this.w.Wh[y + 0.5];
    if(E) {
      (v = 0 == w % 2) ? (o = l, m = 0) : (o = n, m = l);
      var V = this.k;
      this.J.Fi(this, b, V, d.rd(y + 0.5), m, o);
      if(c) {
        if(v) {
          if(this.J.xr(f, b, V, g)) {
            continue
          }
          g = this.J.$q(f, b, V)
        }else {
          if(this.J.xr(f, b, V, k)) {
            continue
          }
          k = this.J.$q(f, b, V)
        }
      }
      this.uc(a.r(), E.Iw);
      m = this.ic(a.r());
      m.qb(b.x, b.y);
      a.ia(m)
    }
  }
};
function kn(a) {
  switch(a) {
    case "cylinder":
      return 1;
    case "pyramid":
      return 2;
    case "cone":
      return 3;
    default:
      return 0
  }
}
function ln(a, b) {
  Re.call(this, a, b)
}
A.e(ln, Pf);
ln.prototype.ro = p;
ln.prototype.g = function(a) {
  a = ln.f.g.call(this, a);
  C(a, "corners") && (this.ro = new Cd, this.ro.g(F(a, "corners")))
};
function mn(a, b) {
  Re.call(this, a, b)
}
A.e(mn, Pf);
mn.prototype.g = function(a) {
  var a = mn.f.g.call(this, a), b = F(a, "fill");
  this.Et(F(F(a, "fill"), "color"), C(b, "opacity") ? F(b, "opacity") : 1)
};
mn.prototype.Et = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "DarkColor(" + a + ")";
  f.push(g);
  k.position = 0.41;
  k.opacity = b;
  k.color = "LightColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "linear";
  d.key = f;
  d.angle = 90;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Id;
  this.Hb.g(c);
  this.Kb = new Kd;
  this.Kb.g(c)
};
function nn(a, b) {
  Re.call(this, a, b)
}
A.e(nn, Pf);
nn.prototype.g = function(a) {
  var a = nn.f.g.call(this, a), b = F(a, "fill");
  this.Et(F(F(a, "fill"), "color"), C(b, "opacity") ? F(b, "opacity") : 1)
};
nn.prototype.Et = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "DarkColor(" + a + ")";
  f.push(g);
  k.position = 0.41;
  k.opacity = b;
  k.color = "LightColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "linear";
  d.key = f;
  d.angle = "0";
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Id;
  this.Hb.g(c);
  this.Kb = new Kd;
  this.Kb.g(c)
};
function on() {
  this.P = new O;
  this.X = new O
}
A.e(on, Of);
z = on.prototype;
z.P = p;
z.X = p;
z.uJ = p;
z.sf = p;
z.Qd = p;
z.hd = function() {
  var a = this.j.o;
  this.sf = isNaN(this.j.Lf) ? a.ts : this.j.Lf;
  this.Qd = this.j.$b;
  var b = this.j.Ab().WC(this.j) / 2, c;
  if(c = this.j.Df() != p) {
    c = this.j.Df();
    var d = a.eb.getName();
    c = c.Lk && c.Lk[d] && c.Lk[d][a.Jb()]
  }
  this.uJ = c;
  a.Vb.transform(this.j, this.j.Sd(), this.P, -b);
  a.eb.transform(this.j, this.sf, this.P);
  a.Vb.transform(this.j, this.j.Sd(), this.X, b);
  a.eb.transform(this.j, this.Qd, this.X);
  this.j.n().x = Math.min(this.P.x, this.X.x);
  this.j.n().y = Math.min(this.P.y, this.X.y);
  this.j.n().width = Math.max(this.P.x, this.X.x) - this.j.n().x;
  this.j.n().height = Math.max(this.P.y, this.X.y) - this.j.n().y
};
z.Qq = function() {
  A.xa()
};
function pn() {
}
A.e(pn, Mf);
pn.prototype.Ta = function() {
  var a = this.j.n();
  this.G.setAttribute("d", gd(a))
};
function qn() {
}
A.e(qn, Nf);
qn.prototype.Ta = function() {
  var a = this.j.n();
  this.G.setAttribute("d", gd(a))
};
function rn() {
  on.call(this)
}
A.e(rn, on);
rn.prototype.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new pn, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new qn, this.Fb.p(this.j, this))
};
rn.prototype.update = function(a, b) {
  b === j && this.hd();
  rn.f.update.call(this, a, b)
};
rn.prototype.createElement = function() {
  return this.j.r().ja()
};
rn.prototype.bg = function(a, b) {
  if(this.P.x > this.X.x) {
    var c = this.P.x;
    this.P.x = this.X.x;
    this.X.x = c
  }
  0 > this.P.y && (this.P.y = 0);
  this.P.y > this.X.y && (c = this.P.y, this.P.y = this.X.y, this.X.y = c);
  switch(b) {
    case 0:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 1:
      a.x = this.P.x;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 5:
      a.x = this.X.x;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 7:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = this.X.y;
      break;
    case 3:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = this.P.y;
      break;
    case 2:
      a.x = this.P.x;
      a.y = this.P.y;
      break;
    case 4:
      a.x = this.X.x;
      a.y = this.P.y;
      break;
    case 8:
      a.x = this.P.x;
      a.y = this.X.y;
      break;
    case 6:
      a.x = this.X.x, a.y = this.X.y
  }
};
function sn() {
}
A.e(sn, Mf);
sn.prototype.Ta = function() {
  this.G.setAttribute("d", this.$a.yh(this.j.r()))
};
function tn() {
}
A.e(tn, Nf);
tn.prototype.Ta = function() {
  this.G.setAttribute("d", this.$a.yh(this.j.r()))
};
function un() {
  on.call(this);
  this.ha = new O;
  this.sa = new O
}
A.e(un, on);
z = un.prototype;
z.ha = p;
z.sa = p;
z.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new sn, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new tn, this.Fb.p(this.j, this))
};
z.hd = function() {
  un.f.hd.call(this);
  this.uJ ? this.PD(this.Nb(), this.j.n(), this.sf, this.Qd) : this.OD(this.Nb(), this.j.n())
};
z.update = function(a, b) {
  b === j && this.hd();
  un.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.yh = function() {
  var a = S(this.sa.x, this.sa.y), a = a + U(this.P.x, this.P.y), a = a + U(this.ha.x, this.ha.y), a = a + U(this.X.x, this.X.y);
  return a + " Z"
};
z.OD = function() {
  A.xa()
};
z.PD = function() {
  A.xa()
};
z.Nb = x(q);
z.bg = function(a, b) {
  switch(b) {
    case 0:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 1:
      a.x = (this.P.x + this.sa.x) / 2;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 5:
      a.x = (this.ha.x + this.X.x) / 2;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 7:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.X.y + this.sa.y) / 2;
      break;
    case 3:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.P.y + this.ha.y) / 2;
      break;
    case 2:
      a.x = this.P.x;
      a.y = this.P.y;
      break;
    case 4:
      a.x = this.ha.x;
      a.y = this.ha.y;
      break;
    case 8:
      a.x = this.sa.x;
      a.y = this.sa.y;
      break;
    case 6:
      a.x = this.X.x, a.y = this.X.y
  }
};
function vn() {
  un.call(this)
}
A.e(vn, un);
vn.prototype.bD = function() {
  return this.P.x > this.X.x
};
vn.prototype.OD = function(a, b) {
  this.ha.x = this.X.x = b.Ja();
  this.X.y = a ? b.ra() : b.y + b.height / 2;
  this.ha.y = a ? b.rb() : b.y + b.height / 2;
  this.P.x = this.sa.x = b.tb();
  this.sa.y = !a ? b.ra() : b.y + b.height / 2;
  this.P.y = !a ? b.rb() : b.y + b.height / 2
};
vn.prototype.PD = function(a, b, c, d) {
  var f = this.j.o, f = an(cn(this.j.Df(), f.eb.getName(), f.Jb(), 2 == f.eb.fa().vg), this.j.Xb()), c = b.height * (f - c) / f, d = b.height * (f - d) / f;
  this.X.x = this.ha.x = b.Ja();
  this.sa.x = this.P.x = b.tb();
  this.ha.y = b.y + (!a ? (b.height - d) / 2 : (b.height - c) / 2);
  this.X.y = b.y + (!a ? (b.height + d) / 2 : (b.height + c) / 2);
  this.P.y = b.y + (a ? (b.height - d) / 2 : (b.height - c) / 2);
  this.sa.y = b.y + (a ? (b.height + d) / 2 : (b.height + c) / 2)
};
function wn() {
  un.call(this)
}
A.e(wn, un);
wn.prototype.bD = function() {
  return this.P.y < this.X.y
};
wn.prototype.OD = function(a, b) {
  this.ha.y = this.P.y = b.rb();
  this.X.y = this.sa.y = b.ra();
  this.ha.x = a ? b.Ja() : b.x + b.width / 2;
  this.P.x = a ? b.tb() : b.x + b.width / 2;
  this.sa.x = a ? b.x + b.width / 2 : b.tb();
  this.X.x = a ? b.x + b.width / 2 : b.Ja()
};
wn.prototype.PD = function(a, b, c, d) {
  var f = this.j.o, f = an(cn(this.j.Df(), f.eb.getName(), f.Jb(), 2 == f.eb.fa().vg), this.j.Xb()), c = b.width * (f - c) / f, d = b.width * (f - d) / f;
  this.sa.y = this.X.y = b.ra();
  this.P.y = this.ha.y = b.rb();
  this.X.x = b.x + (!a ? (b.width + c) / 2 : (b.width + d) / 2);
  this.sa.x = b.x + (!a ? (b.width - c) / 2 : (b.width - d) / 2);
  this.ha.x = b.x + (a ? (b.width + c) / 2 : (b.width + d) / 2);
  this.P.x = b.x + (a ? (b.width - c) / 2 : (b.width - d) / 2)
};
function xn() {
  $.call(this)
}
A.e(xn, Qf);
xn.prototype.cc = function() {
  return ln
};
function yn() {
  $.call(this)
}
A.e(yn, xn);
yn.prototype.cc = function() {
  return mn
};
function zn() {
  $.call(this)
}
A.e(zn, xn);
zn.prototype.cc = function() {
  return nn
};
xn.prototype.lc = function(a) {
  switch(a.ol()) {
    default:
    ;
    case 0:
      return new rn;
    case 2:
    ;
    case 3:
      return a.o.ca().Vf() ? new vn : new wn;
    case 1:
      return new rn
  }
};
function An() {
  on.call(this)
}
A.e(An, rn);
An.prototype.createElement = function() {
  return this.j.r().ja()
};
An.prototype.hd = function() {
  this.j.hd()
};
An.prototype.update = function(a, b) {
  b === j && this.hd();
  An.f.update.call(this, a, b)
};
function Bn() {
  $.call(this)
}
A.e(Bn, Qf);
Bn.prototype.cc = function() {
  return ln
};
Bn.prototype.lc = function() {
  return new An
};
function Cn(a) {
  this.N = a;
  this.gj = q
}
Cn.prototype.N = p;
Cn.prototype.gj = p;
function Dn(a) {
  Cn.call(this, a)
}
A.e(Dn, Cn);
Dn.prototype.tG = p;
Dn.prototype.WK = p;
Dn.prototype.g = function(a) {
  if(this.gj = C(a, "url")) {
    this.tG = new ie(J(a, "url"));
    var b = "_blank";
    C(a, "target") && (b = J(a, "target"));
    C(a, "url_target") && (b = J(a, "url_target"));
    this.WK = new ie(b)
  }
};
Dn.prototype.execute = function(a) {
  switch(this.WK.ta(a)) {
    default:
    ;
    case "_blank":
      window.open(this.tG.ta(a));
      break;
    case "_self":
      window.location.href = this.tG.ta(a)
  }
  return q
};
function En(a) {
  Cn.call(this, a)
}
A.e(En, Cn);
En.prototype.kI = p;
En.prototype.Cw = p;
En.prototype.g = function(a) {
  C(a, "function") && (this.gj = j, this.kI = new ie(F(a, "function")));
  if(this.gj && C(a, "arg")) {
    this.Cw = [];
    for(var a = I(a, "arg"), b = a.length, c = 0;c < b;c++) {
      this.Cw.push(new ie(dc(a[c]) ? a[c] : F(a[c], "value")))
    }
  }
};
En.prototype.execute = function(a) {
  var b = this.kI.ta(a), c, d = this.Cw.length, f = "";
  for(c = 0;c < d;c++) {
    f += '"' + this.Cw[c].ta(a) + '"', 1 < d && c != d - 1 && (f += ", ")
  }
  eval(b + ".call(null," + f + ");");
  return q
};
function Fn(a) {
  Cn.call(this, a)
}
A.e(Fn, Cn);
Fn.prototype.Lv = p;
Fn.prototype.tj = p;
Fn.prototype.TF = p;
Fn.prototype.g = function(a) {
  if(this.gj = C(a, "source") && C(a, "source_mode")) {
    var b;
    a: {
      switch(Wb(a, "source_mode")) {
        case "externaldata":
          b = 0;
          break a;
        case "internaldata":
          b = 1;
          break a;
        default:
          b = p
      }
    }
    this.TF = b;
    this.Lv = new ie(J(a, "source"));
    if(C(a, "replace")) {
      a = I(a, "replace");
      b = a.length;
      this.tj = [];
      for(var c = 0;c < b;c++) {
        var d = a[c];
        this.tj.push([J(d, "token"), new ie(J(d, "value"))])
      }
    }
  }
};
function Gn(a, b) {
  if(!a.tj) {
    return p
  }
  for(var c = [], d = a.tj.length, f = 0;f < d;f++) {
    c.push([a.tj[f][0], a.tj[f][1].ta(b)])
  }
  return c
}
function Hn(a) {
  Cn.call(this, a)
}
A.e(Hn, Fn);
Hn.prototype.execute = function(a) {
  if(0 == this.TF) {
    this.N.Hi(this.Lv.ta(a), Gn(this, a), q)
  }else {
    var b = this.N, c = this.Lv.ta(a), a = Gn(this, a);
    b.tj = a ? a : p;
    b.Yu = q;
    b.Gk(b.sq[c])
  }
  return q
};
function In(a) {
  Cn.call(this, a)
}
A.e(In, Fn);
In.prototype.YK = p;
In.prototype.g = function(a) {
  In.f.g.call(this, a);
  if(this.gj = this.gj && (C(a, "view") || C(a, "view_id"))) {
    this.YK = new ie(C(a, "view_id") ? J(a, "view_id") : J(a, "view"))
  }
};
In.prototype.execute = function(a) {
  var b = this.YK.ta(a);
  if(0 == this.TF) {
    this.N.Np(b, this.Lv.ta(a), Gn(this, a))
  }else {
    var c = this.N, d = this.Lv.ta(a), a = Gn(this, a), d = c.sq[d], b = Jn(c, b);
    d && b && (a && (d = lc(d, a)), b.hb.D.clear(), b.Gk(d))
  }
  return q
};
function Ee(a, b) {
  this.N = a;
  this.TC = b
}
z = Ee.prototype;
z.N = p;
z.TC = p;
z.Pf = p;
z.g = function(a) {
  this.Pf = [];
  for(var a = I(a, "action"), b = a.length, c = 0;c < b;c++) {
    var d;
    a: {
      d = a[c];
      var f = p;
      if(C(d, "type")) {
        switch(Wb(d, "type")) {
          case "navigatetourl":
            f = new Dn(this.N);
            break;
          case "call":
            f = new En(this.N);
            break;
          case "updatechart":
            f = new Hn(this.N);
            break;
          case "updateview":
            f = new In(this.N);
            break;
          case "scroll":
            Ke("Action scroll");
            break;
          case "zoom":
            Ke("Action zoom");
            break;
          case "scrollview":
            Ke("Action scrollView");
            break;
          case "zoomView":
            Ke("Action zoomView");
            break;
          default:
            d = p;
            break a
        }
        f.g(d);
        f.gj || (f = p)
      }
      d = f
    }
    d && this.Pf.push(d)
  }
};
z.execute = function(a) {
  a && (this.TC = a);
  for(var a = this.Pf.length, b = q, c = 0;c < a;c++) {
    b = b || this.Pf[c].execute(this.TC)
  }
  return b
};
function Kn() {
}
z = Kn.prototype;
z.tz = p;
z.yk = p;
z.kl = p;
z.gI = p;
z.uy = p;
z.p = function(a, b) {
  this.clear();
  this.kl = a;
  this.gI = b
};
function Ln(a, b, c) {
  a.Gb(b) || (a.tz++, a.yk.push(c))
}
z.lr = function(a) {
  if(a.length == this.yk.length) {
    return[]
  }
  this.uy = [];
  if(0 == this.tz) {
    for(var b = a.length, c = 0;c < b;c++) {
      Mn(this, a[c], 1)
    }
  }else {
    if(1 == this.tz) {
      for(var b = a.length, c = Ob(this.kl.call(a[this.yk[0]])), d = 0;d < b;d++) {
        Mn(this, a[d], c)
      }
    }else {
      Nn(this, a)
    }
  }
  return this.uy
};
z.clear = function() {
  this.tz = 0;
  this.yk = [];
  this.uy = []
};
function Nn(a, b) {
  a.yk.sort(function(a, b) {
    return a - b
  });
  var c, d = a.yk.length, f = a.yk[0], g = a.yk[d - 1];
  for(c = 0;c < d - 1;c++) {
    var k = a.yk[c], l = a.yk[c + 1];
    1 < l - k && On(a, b, k, l)
  }
  0 < f && Pn(a, b, f);
  g < b.length - 1 && Qn(a, b, g)
}
function On(a, b, c, d) {
  for(var f = Ob(a.kl.call(b[c])), g = (Ob(a.kl.call(b[d])) - f) / (d - c), k = c + 1;k < d;k++) {
    Mn(a, b[k], f + (k - c) * g)
  }
}
function Pn(a, b, c) {
  for(var d = Ob(a.kl.call(b[c])), f = Ob(a.kl.call(b[c + 1])) - d, g = c - 1;0 <= g;g--) {
    Mn(a, b[g], d - f * (c - g))
  }
}
function Qn(a, b, c) {
  for(var d = Ob(a.kl.call(b[c])), f = Ob(a.kl.call(b[c - 1])), d = f - d, g = c + 1;g < b.length;g++) {
    Mn(a, b[g], f - d * (g - c + 1))
  }
}
function Mn(a, b, c) {
  a.gI.call(b, c);
  a.uy.push(b)
}
z.Gb = function(a) {
  return isNaN(Ob(this.kl.call(a)))
};
function Rn() {
}
A.e(Rn, og);
Rn.prototype.xu = p;
Rn.prototype.aA = t("xu");
Rn.prototype.sh = function(a) {
  return Rn.f.sh.call(this, a) || C(a, "format")
};
Rn.prototype.copy = function(a) {
  a || (a = new Rn);
  return Rn.f.copy.call(this, a)
};
function Sn() {
  $f.apply(this)
}
A.e(Sn, $f);
Sn.prototype.sh = function(a) {
  return Sn.f.sh.call(this, a) || C(a, "type")
};
Sn.prototype.copy = function(a) {
  a || (a = new Sn);
  return Sn.f.copy.call(this, a)
};
function Tn() {
  sg.apply(this)
}
A.e(Tn, sg);
Tn.prototype.sh = function(a) {
  return Tn.f.sh.call(this, a) || C(a, "format")
};
Tn.prototype.copy = function(a) {
  a || (a = new Tn);
  return Tn.f.copy.call(this, a)
};
function Un() {
}
z = Un.prototype;
z.GE = p;
z.wr = j;
z.Vo = j;
z.Tn = j;
function Pk(a, b) {
  a.wr = b.wr;
  a.Vo = b.Vo;
  a.Tn = b.Tn
}
z.il = function(a) {
  C(a, "allow_select") && (this.wr = K(a, "allow_select"));
  C(a, "hoverable") && (this.Vo = K(a, "hoverable"));
  C(a, "use_hand_cursor") && (this.Tn = K(a, "use_hand_cursor"))
};
z.K = p;
z.ka = u("K");
z.rf = t("K");
z.Ea = p;
z.Mb = u("Ea");
z.gb = p;
z.Fc = p;
z.OB = j;
z.di = p;
z.ei = p;
z.wh = p;
z.Cm = function(a, b) {
  this.uo(a, b);
  this.fk(a, b);
  this.vo(a, b);
  this.hC(a, b);
  this.iC(a, b);
  this.jC(a, b)
};
z.uo = function() {
  A.xa()
};
z.fk = function() {
  A.xa()
};
z.vo = function() {
  A.xa()
};
z.hC = function(a, b) {
  if(C(a, "extra_labels")) {
    this.di = [];
    for(var c = I(F(a, "extra_labels"), "label"), d = c.length, f = 0;f < d;f++) {
      var g = this.cb.Aq();
      g.Ue(c[f], b);
      g.aA(j);
      this.di.push(g)
    }
  }
};
z.iC = function(a, b) {
  if(C(a, "extra_markers")) {
    this.ei = [];
    for(var c = I(F(a, "extra_markers"), "marker"), d = c.length, f = 0;f < d;f++) {
      var g = this.cb.Am();
      g.Ue(c[f], b);
      this.ei.push(g)
    }
  }
};
z.jC = function(a, b) {
  if(C(a, "extra_tooltips")) {
    this.wh = [];
    for(var c = I(F(a, "extra_tooltips"), "tooltip"), d = c.length, f = 0;f < d;f++) {
      var g = new Tn;
      g.Ue(c[f], b);
      this.wh.push(g)
    }
  }
};
z.Mh = function(a) {
  a.Ea = this.Ea;
  a.gb = this.gb;
  a.Fc = this.Fc;
  a.di = this.di;
  a.ei = this.ei;
  a.wh = this.wh
};
z.g = s();
z.Qe = p;
function Ok(a, b) {
  b && (a.Qe = b)
}
function Jk(a, b) {
  b && C(b, "actions") && (a.Qe = new Ee(a.dj()), a.Qe.g(F(b, "actions")))
}
function Vn() {
  this.Ge()
}
A.e(Vn, Un);
z = Vn.prototype;
z.Zm = p;
z.Zt = u("Zm");
z.na = p;
z.Vr = t("na");
z.Sa = p;
z.Rl = t("Sa");
z.getName = u("Sa");
z.jd = p;
z.Xa = p;
z.Di = t("Xa");
z.Ba = u("Xa");
z.Ze = p;
z.Ip = t("Ze");
z.ge = p;
z.ng = u("ge");
z.cb = p;
z.Ab = u("cb");
z.dj = function() {
  return this.cb.dj()
};
z.ca = function() {
  return this.cb.ca()
};
z.Gd = function() {
  return this.ca().Gd()
};
z.ib = p;
z.Zr = t("ib");
z.Kg = u("ib");
z.g = function(a, b) {
  Vn.f.g.call(this, a, b);
  C(a, "id") && (this.Zm = J(a, "id"));
  if(C(a, "attributes")) {
    this.jd = {};
    for(var c = I(F(a, "attributes"), "attribute"), d = c.length, f = 0;f < d;f++) {
      var g = c[f];
      if(C(g, "name")) {
        var k;
        C(g, "custom_attribute_value") ? k = F(g, "custom_attribute_value") : C(g, "value") && (k = F(g, "value"));
        k && H(this.jd, "%" + F(g, "name"), k)
      }
    }
  }
};
function Nk(a, b) {
  C(b, "name") && (a.Sa = J(b, "name"))
}
z.Eq = function(a, b) {
  if(C(a, "style")) {
    var c = J(a, "style"), d = this.Ab().Rb();
    if(c = Xe(b, d, c)) {
      this.K = this.Ab().Qb(), this.K.g(c)
    }
  }
};
z.b = p;
z.pl = function(a) {
  return Boolean(this.jd && this.jd[a])
};
z.Ge = function() {
  this.b = new me;
  this.Cb()
};
z.Cb = function() {
  A.xa()
};
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  a.Name = this.Sa;
  var b = a, c;
  var d = this.jd;
  if(d) {
    c = {};
    for(var f in d) {
      d.hasOwnProperty(f) && (c[/^%(.*)$/.exec(f)[1]] = d[f])
    }
  }
  b.Attributes = c;
  a.Color = this.Xa.Ba().Pe();
  a.HatchType = this.Ze;
  a.MarkerType = this.ge;
  this.Zm && (a.ID = this.Zm);
  return a
};
function Wn() {
  this.Ge()
}
A.e(Wn, Vn);
z = Wn.prototype;
z.g = function(a, b) {
  Wn.f.g.call(this, a, b);
  this.Nr = this.na;
  C(a, "selected") && (this.Og = K(a, "selected"))
};
z.Gb = q;
z.o = p;
z.Nr = 0;
z.k = p;
z.n = u("k");
z.Cg = p;
z.jn = x(q);
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.$a = p;
z.p = function(a) {
  this.k || (this.k = new P);
  this.D = new W(a);
  if(this.nc) {
    this.ya = this.K.nc, this.Ce = "Missing"
  }else {
    if(this.wr && this.Og) {
      if(!this.ca().im && (a = this.ca().Fk)) {
        a.Og = q, a.ya = a.ka().wa
      }
      this.ya = this.K.Yb;
      this.Ce = "SelectedNormal";
      this.ca().uj(this, q)
    }else {
      this.ya = this.K.wa, this.Ce = "Normal"
    }
  }
  this.RI();
  this.LD();
  Xn(this, this.D);
  return this.D
};
z.RI = function() {
  this.$a = this.K.p(this)
};
z.rk = s();
z.update = function() {
  this.$a.update(this.ya, j);
  this.Sn()
};
z.Wa = function() {
  this.$a.update(p, j);
  this.Lz()
};
z.ya = p;
z.Ce = p;
z.be = function(a) {
  return this.qD(a, this.Ce)
};
z.qD = function(a, b) {
  switch(b) {
    default:
    ;
    case "Normal":
      return a.wa;
    case "Hover":
      return a.dc;
    case "Pushed":
      return a.kc;
    case "SelectedNormal":
      return a.Yb;
    case "SelectedHover":
      return a.gc;
    case "Missing":
      return a.nc
  }
};
z.hE = q;
function Xn(a, b) {
  b != p && (A.I.nb(b.G, ka.Ms, a.nj, q, a), A.I.nb(b.G, ka.Ls, a.Jl, q, a), A.I.nb(b.G, ka.fm, a.CN, q, a), A.I.nb(b.G, ka.dB, a.Il, q, a), A.I.nb(b.G, ka.Xn, a.RE, q, a), a.Tn && ld(b.G))
}
z.nj = function(a, b) {
  this.Vo && (this.Og ? (this.ya = this.K.gc, this.Ce = "SelectedHover") : (this.ya = this.K.dc, this.Ce = "Hover"), this.update());
  this.sG(a);
  this.hE = j;
  b != j && Yn(this, a, "pointMouseOver")
};
z.Jl = function(a, b) {
  this.hE = q;
  this.Vo && (this.Og ? (this.ya = this.K.Yb, this.Ce = "SelectedNormal") : (this.ya = this.K.wa, this.Ce = "Normal"), this.update());
  this.ny();
  b != j && Yn(this, a, "pointMouseOut")
};
z.RE = function(a) {
  this.hE && this.JE(a)
};
z.CN = function(a) {
  var b = q;
  this.Qe && (b = this.Qe.execute(this));
  !b && this.wr && !this.Og && (this.ya = this.K.kc, this.Ce = "Pushed", this.update(), this.Sn());
  Yn(this, a, "pointMouseDown")
};
z.Il = function(a, b) {
  this.wr && (this.ca().im ? (this.Og = !this.Og, this.Vo ? this.nj(a, j) : (this.Og ? (this.ya = this.K.Yb, this.Ce = "SelectedNormal") : (this.ya = this.K.gc, this.Ce = "SelectedHover"), this.update(), this.Sn()), this.Og ? Yn(this, a, "pointSelect") : Yn(this, a, "pointDeselect"), this.ca().uj(this, j)) : this.Og || (this.Og = j, this.ca().uj(this, j), Yn(this, a, "pointSelect"), this.Vo ? this.nj(a, j) : (this.ya = this.K.Yb, this.Ce = "SelectedNormal")));
  this.update();
  this.Sn();
  b || (Yn(this, a, "pointMouseUp"), Yn(this, a, "pointClick"))
};
function Yn(a, b, c) {
  var d = NaN, f = NaN;
  b && (d = b.clientX, f = b.clientY);
  a.Ab().dj().dispatchEvent(new He(c, a, d, f))
}
z.Og = q;
z.ux = function(a) {
  this.Og = q;
  this.ya = this.K.wa;
  this.Ce = "Normal";
  a != h && a && this.update();
  Yn(this, p, "pointDeselect")
};
z.select = function(a, b) {
  this.Og = j;
  this.ya = this.K.Yb;
  this.Ce = "SelectedNormal";
  a != h && a && this.update();
  b || Yn(this, p, "pointSelect")
};
z.ah = function() {
  this.OB = q;
  this.nj(p, j);
  this.OB = j
};
z.dh = function() {
  this.Jl(p, j)
};
z.uo = function(a, b) {
  this.Ea && (C(a, "label") && this.Ea.om(F(a, "label")) && (this.Ea = this.Ea.copy(), this.Ea.Yi(F(a, "label"), b)), this.Wj(this.Ea))
};
z.fk = function(a, b) {
  this.gb && (C(a, "marker") && this.gb.om(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Yi(F(a, "marker"), b)), this.Wj(this.gb))
};
z.vo = function(a, b) {
  this.Fc && this.Fc.om(F(a, "tooltip")) && (this.Fc = this.Fc.copy(), this.Fc.Yi(F(a, "tooltip"), b));
  this.Wj(this.Fc)
};
z.hC = function(a, b) {
  Wn.f.hC.call(this, a, b);
  if(this.di) {
    for(var c = 0;c < this.di.length;c++) {
      this.Wj(this.di[c])
    }
  }
};
z.iC = function(a, b) {
  Wn.f.iC.call(this, a, b);
  if(this.ei) {
    for(var c = 0;c < this.ei.length;c++) {
      this.Wj(this.ei[c])
    }
  }
};
z.jC = function(a, b) {
  Wn.f.jC.call(this, a, b);
  if(this.wh) {
    for(var c = 0;c < this.wh.length;c++) {
      this.Wj(this.wh[c])
    }
  }
};
z.vc = p;
z.sd = p;
z.uf = p;
z.Jm = p;
z.Km = p;
z.gi = p;
z.LD = function() {
  this.vc = Zn(this, this.Ea, j);
  if(this.di) {
    this.Jm = [];
    for(var a = this.di.length, b = 0;b < a;b++) {
      this.Jm[b] = Zn(this, this.di[b], j)
    }
  }
  this.sd = Zn(this, this.gb, j);
  if(this.ei) {
    this.Km = [];
    a = this.ei.length;
    for(b = 0;b < a;b++) {
      this.Km[b] = Zn(this, this.ei[b], j)
    }
  }
  this.uf = Zn(this, this.Fc, q);
  if(this.wh) {
    this.gi = [];
    a = this.wh.length;
    for(b = 0;b < a;b++) {
      this.gi[b] = Zn(this, this.wh[b], q)
    }
  }
};
function Zn(a, b, c) {
  return b ? ((b = b.ka().p(a, b)) && c && !a.nc && Xn(a, b), b) : p
}
z.Sn = function() {
  this.Ni(this.Ea, this.vc);
  if(this.Jm) {
    for(var a = this.Jm.length, b = 0;b < a;b++) {
      this.Ni(this.di[b], this.Jm[b])
    }
  }
  this.Ni(this.gb, this.sd);
  if(this.Km) {
    a = this.Km.length;
    for(b = 0;b < a;b++) {
      this.Ni(this.ei[b], this.Km[b])
    }
  }
};
z.sG = function(a) {
  if(this.OB) {
    var b = this.Fc, c = this.uf;
    this.Ni(b, c);
    $n(this, a, b, c);
    if(this.gi) {
      b = this.gi.length;
      for(c = 0;c < b;c++) {
        this.Ni(this.wh[c], this.gi[c]), $n(this, a, this.wh[c], this.gi[c])
      }
    }
  }
};
z.ny = function() {
  ao(this.uf);
  if(this.gi) {
    for(var a = this.gi.length, b = 0;b < a;b++) {
      ao(this.gi[b])
    }
  }
};
z.JE = function(a) {
  $n(this, a, this.Fc, this.uf);
  if(this.gi) {
    for(var b = this.gi.length, c = 0;c < b;c++) {
      $n(this, a, this.wh[c], this.gi[c])
    }
  }
};
z.Ni = function(a, b) {
  b && b.update(this.be(a.ka()))
};
function $n(a, b, c, d) {
  d && tg(c, b, a, a.be(c.ka()), d)
}
function ao(a) {
  a && (a.qb(0, 0), a.Gi(q))
}
z.Lz = function() {
  this.Nz(this.Ea, this.vc);
  if(this.Jm) {
    for(var a = this.Jm.length, b = 0;b < a;b++) {
      this.Nz(this.di[b], this.Jm[b])
    }
  }
  this.sd && this.Dp(this.gb, this.sd);
  if(this.Km) {
    a = this.Km.length;
    for(b = 0;b < a;b++) {
      this.Dp(this.ei[b], this.Km[b])
    }
  }
  this.ny()
};
z.Nz = function(a, b) {
  b && this.Dp(a, b)
};
z.Dp = function(a, b) {
  a && b && a.Wa(this, this.be(a.ka()), b)
};
z.Wj = s();
z.Wq = function(a, b, c) {
  if(!a || !b || !c || this.Gb && !this.ca().qk) {
    return p
  }
  a = new O;
  this.bg(a, b.lg(), c, b.ga());
  10 != b.lg() && (this.wv(a, b.lg(), b.Go(), b.Ko(), c, b.ga()), this.xv(a, b.lg(), b.Go(), b.Ko(), c, b.ga()));
  return a
};
z.bg = function(a, b) {
  if(this.k != p) {
    switch(b) {
      default:
      ;
      case 0:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 7:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y + this.k.height;
        break;
      case 1:
        a.x = this.k.x;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 5:
        a.x = this.k.x + this.k.width;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 3:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y;
        break;
      case 8:
        a.x = this.k.x;
        a.y = this.k.y + this.k.height;
        break;
      case 2:
        a.x = this.k.x;
        a.y = this.k.y;
        break;
      case 6:
        a.x = this.k.x + this.k.width;
        a.y = this.k.y + this.k.height;
        break;
      case 4:
        a.x = this.k.x + this.k.width, a.y = this.k.y
    }
  }
};
z.wv = function(a, b, c, d, f, g) {
  switch(c) {
    case 0:
      a.x -= f.width + g;
      break;
    case 1:
      a.x += g;
      break;
    default:
    ;
    case 2:
      a.x -= f.width / 2
  }
};
z.xv = function(a, b, c, d, f, g) {
  switch(d) {
    case 0:
      a.y -= f.height + g;
      break;
    case 2:
      a.y += g;
      break;
    default:
    ;
    case 1:
      a.y -= f.height / 2
  }
};
z.Cb = function() {
  this.b.add("%Index", 0);
  this.b.add("%Name", "");
  this.b.add("%Color", "0xFFFFFF")
};
z.Se = function() {
  this.b.add("%Index", this.na);
  this.b.add("%Name", this.Sa);
  this.Xa && this.b.add("%Color", pe(this.Xa.Ba()))
};
z.Na = function(a) {
  return this.pl(a) ? this.jd[a] : 0 == a.indexOf("%Series") || this.o.pl(a) ? this.o.Uf(a) : re(a, this.ca()) ? this.ca().Na(a) : this.Cg && (0 == a.indexOf("%Threshold") || this.Cg.pr(a)) ? "%ConditionValue1" == a ? this.Cg.dE ? this.Na(ek(this.Cg.fw)) : this.Cg.fw : "%ConditionValue2" == a ? this.Cg.eE ? this.Na(ek(this.Cg.gw)) : this.Cg.gw : "%ConditionValue3" == a ? this.Cg.fE ? this.Na(ek(this.Cg.hw)) : this.Cg.hw : this.Cg.Na(a) : se(a, this.ib) ? this.ib.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  if(this.pl(a)) {
    return 1
  }
  if(0 == a.indexOf("%Series") || this.o.pl(a)) {
    return this.o.Pa(a)
  }
  if(re(a, this.ca())) {
    return this.ca().Pa(a)
  }
  if(se(a, this.ib)) {
    return this.ib.Pa(a)
  }
  switch(a) {
    case "%Index":
      return 2;
    case "%Name":
    ;
    case "%Color":
    ;
    case "%text":
      return 1;
    default:
      return 4
  }
};
z.mb = function(a) {
  a = Wn.f.mb.call(this, a);
  a.SeriesPointCount = this.o.Fe();
  a.Index = this.na;
  return a
};
function bo(a) {
  var b = a.mb();
  b.Series = a.o.mb();
  return b
}
function co() {
  this.Ge();
  this.ba = []
}
A.e(co, Vn);
z = co.prototype;
z.ba = p;
z.Fe = function() {
  return this.ba ? this.ba.length : 0
};
z.Ga = function(a) {
  return this.ba[a]
};
z.qe = function() {
  A.xa()
};
z.Od = function(a, b) {
  this.ba[b] = a
};
z.Ra = NaN;
z.Jb = u("Ra");
z.gh = t("Ra");
z.uo = function(a, b) {
  this.Ea && this.Ea.sh(F(a, "label")) && (this.Ea = this.Ea.copy(), this.Ea.Zi(F(a, "label"), b))
};
z.fk = function(a, b) {
  this.gb && this.gb.sh(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Zi(F(a, "marker"), b))
};
z.vo = function(a, b) {
  this.Fc && this.Fc.sh(F(a, "tooltip")) && (this.Fc = this.Fc.copy(), this.Fc.Zi(F(a, "tooltip"), b))
};
z.g = function(a, b) {
  co.f.g.call(this, a, b)
};
z.ah = function() {
  for(var a = this.Fe(), b = 0;b < a;b++) {
    this.Ga(b).ah()
  }
};
z.dh = function() {
  for(var a = this.Fe(), b = 0;b < a;b++) {
    this.Ga(b).dh()
  }
};
z.Cb = function() {
  this.b.add("%SeriesPointCount", 0);
  this.b.add("%Name", "");
  this.b.add("%SeriesName", "")
};
z.Pa = function(a) {
  if(this.pl(a)) {
    return 1
  }
  if(re(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    default:
      return 1;
    case "%SeriesPointCount":
      return 2
  }
};
z.Ed = function(a) {
  a.Se()
};
z.th = function(a) {
  oe(a.b, "%DataPlotPointCount", this.ba.length);
  this.rh()
};
z.rh = function() {
  this.b.add("%SeriesName", this.Sa);
  this.b.add("%Name", this.Sa);
  this.b.add("%SeriesPointCount", this.ba.length)
};
z.Uf = function(a) {
  return this.jd && this.jd[a] ? this.jd[a] : this.b.get(a)
};
z.Na = function(a) {
  return re(a, this.ca()) ? this.ca().Na(a) : this.Uf(a)
};
z.tn = x(q);
z.Ug = s();
z.zn = s();
z.kB = x(j);
z.mb = function(a) {
  a = co.f.mb.call(this, a);
  a.Points = [];
  for(var b = this.ba.length, c = 0;c < b;c++) {
    this.ba[c] && a.Points.push(this.ba[c].mb())
  }
  return a
};
function eo() {
  this.cb = this
}
A.e(eo, Un);
eo.prototype.aa = p;
eo.prototype.Tl = t("aa");
eo.prototype.ca = u("aa");
Un.prototype.N = p;
Un.prototype.dA = t("N");
Un.prototype.dj = u("N");
z = eo.prototype;
z.py = x(q);
z.Bw = q;
z.Wm = 0.1;
z.Cn = 0.1;
z.Rb = function() {
  A.xa()
};
z.Qb = function() {
  A.xa()
};
z.g = function(a, b) {
  eo.f.g.call(this, a, b);
  C(a, "apply_palettes_to") && (this.Bw = "points" == Cb(F(a, "apply_palettes_to")));
  C(a, "group_padding") && (this.Wm = M(a, "group_padding"));
  C(a, "point_padding") && (this.Cn = M(a, "point_padding"));
  if((this.$s() || this.Tw()) && C(a, "sort")) {
    switch(Wb(a, "sort")) {
      case "asc":
        this.Yo = j;
        this.Kv = q;
        break;
      case "desc":
        this.Kv = this.Yo = j;
        break;
      default:
      ;
      case "none":
        this.Yo = q
    }
  }
};
z.Eq = function(a, b) {
  var c = this.Rb();
  if(c) {
    var d, f;
    C(a, "style") && (d = N(a, "style"));
    C(a, c) && (f = F(a, c), C(f, "parent") && (d = F(f, "parent")));
    c = Ue(b, c, f, d);
    c.name = d;
    this.K = this.Qb(c);
    this.K.g(c)
  }
};
z.il = function(a) {
  eo.f.il.call(this, a);
  C(a, "interactivity") && eo.f.il.call(this, F(a, "interactivity"))
};
z.Kv = q;
z.Yo = q;
z.$s = x(q);
z.Tw = x(q);
function Mk(a, b) {
  !a.Tw() && a.Yo && a.$s() && a.GC(b, a.Kv)
}
z.GC = s();
z.bI = x(p);
z.Aq = function() {
  return new Rn
};
z.Am = function() {
  return new Sn
};
z.uo = function(a, b) {
  (this.Ea = this.Aq()) && (a && C(a, "label_settings") ? this.Ea.Ue(F(a, "label_settings"), b) : this.Ea.rf(new $))
};
z.fk = function(a, b) {
  (this.gb = this.Am()) && (a && C(a, "marker_settings") ? this.gb.Ue(F(a, "marker_settings"), b) : this.gb.rf(new $))
};
z.vo = function(a, b) {
  (this.Fc = new Tn) && (a && C(a, "tooltip_settings") ? this.Fc.Ue(F(a, "tooltip_settings"), b) : this.Fc.rf(new $))
};
z.ZG = x(j);
function fo() {
  eo.apply(this)
}
A.e(fo, eo);
fo.prototype.ow = p;
fo.prototype.ND = function() {
  this.ow = new Kn;
  this.ow.p(go.prototype.Xb, go.prototype.gd)
};
fo.prototype.cH = function(a) {
  Ln(this.ow, a, a.na)
};
eo.prototype.WI = function(a) {
  for(var a = this.ow.lr(a.ba), b = a.length, c = 0;c < b;c++) {
    a[c].rk()
  }
  this.ow.clear()
};
function ho() {
  fo.apply(this)
}
A.e(ho, fo);
ho.prototype.Pr = 0.05;
ho.prototype.nE = j;
ho.prototype.g = function(a, b) {
  ho.f.g.call(this, a, b);
  C(a, "scatter_point_width") && (this.Pr = F(a, "scatter_point_width"), this.Pr = (this.nE = Mb(this.Pr)) ? Nb(this.Pr) : Ib.JQ(this.Pr))
};
function io() {
  Wn.apply(this)
}
A.e(io, Wn);
z = io.prototype;
z.Ub = NaN;
z.Sd = u("Ub");
z.Dc = p;
z.Df = u("Dc");
z.Fd = p;
z.po = 0;
z.zf = p;
z.Jc = q;
z.wl = q;
z.g = function(a, b) {
  this.dl(a);
  io.f.g.call(this, a);
  this.GH(a, b);
  this.Gb || this.$h(a);
  !this.Gb && this.Fd && (this.Fd.Ed(this), this.Fd != this.Dc && this.Dc.Ed(this));
  this.o.Vb.ba.push(this);
  this.o.eb.ba.push(this)
};
z.GH = function(a, b) {
  var c = this.o.Vb;
  C(a, "x") && (this.Ub = c.$i(F(a, "x")));
  this.Jc = 0 == c.Ib() || 1 == c.Ib();
  var d = c.Jb();
  if(1 == d) {
    C(a, "name") && isNaN(this.Ub) && (this.Ub = c.$i(F(a, "name"))), this.o.Vb.Gc(this.Ub)
  }else {
    if(2 == d) {
      this.Fd = this.Dc = c.Df(this.Sa, b), this.Ub = this.Dc.na, this.zf = this.o.zf, this.na = this.Nr = this.Dc.na
    }else {
      if(3 == d) {
        this.Dc = this.o.Df();
        var d = this.cb.ca(), f = this.Sa;
        if(d.rx[f]) {
          c = d.rx[f]
        }else {
          var g = new Ym(d);
          g.wj(c);
          g.Vr(d.Bq.length);
          g.Rl(f);
          d.Bq.push(g);
          c = d.rx[f] = g
        }
        this.Fd = c;
        this.Ub = this.o.Df().na;
        this.o.sJ ? this.po = 0 : (c = this.Df().zf, d = this.Df().zf, c.Hl = d.Hl + 1, this.po = this.na);
        this.zf = this.Df().zf
      }else {
        e(Error("Unknown argument axis type"))
      }
    }
  }
};
z.$h = function() {
  A.xa()
};
function jo(a, b) {
  return a.o.eb.$i(b)
}
z.dl = function() {
  A.xa()
};
z.rk = function() {
  io.f.rk.call(this);
  this.Fd && this.Fd.Ed(this);
  this.Fd != p && this.Dc != p && this.Fd != this.Dc && this.Dc.Ed(this)
};
z.bg = function(a, b, c, d) {
  var f = this.o.Vb;
  10 == b ? (f.transform(this, this.Sd(), a), f.J.Ei(d, c, a)) : (b = this.wl ? f.J.vi.Px(b) : f.J.vi.Zx(b), io.f.bg.call(this, a, b, c, d))
};
z.wv = function(a, b, c, d, f, g) {
  if(9 != b) {
    var k = this.o.Vb, c = this.wl ? k.J.vi.cD(c, d) : k.J.vi.gD(c, d)
  }
  io.f.wv.call(this, a, b, c, d, f, g)
};
z.xv = function(a, b, c, d, f, g) {
  if(9 != b) {
    var k = this.o.Vb, d = this.wl ? k.J.vi.dD(c, d) : k.J.vi.$x(c, d)
  }
  io.f.xv.call(this, a, b, c, d, f, g)
};
z.Wj = function(a) {
  if(10 == a.ka().wa.lg()) {
    var b = this.o.Vb;
    b.xp || (b.xp = []);
    b.xp.push([a, this])
  }
};
z.Nz = function(a, b) {
  b && a && a.ka() && a.ka().wa && 10 == a.ka().wa.lg() && this.ca().Wx(a).ia(b);
  io.f.Nz.call(this, a, b)
};
z.kt = s();
z.Cb = function() {
  io.f.Cb.call(this);
  this.b.add("%XValue", 0);
  this.b.add("%XPercentOfSeries", 0);
  this.b.add("%XPercentOfTotal", 0)
};
z.Pa = function(a) {
  if(te(a) && this.Fd) {
    return this.Fd.Pa(a)
  }
  if(0 == a.indexOf("%YAxis")) {
    return this.o.eb.Pa(ue(a))
  }
  if(0 == a.indexOf("%XAxis")) {
    return this.o.Vb.Pa(ue(a))
  }
  if(Km(this.o.Vb) && ("%XValue" == a || "%Name" == a)) {
    return 3
  }
  switch(a) {
    case "%XValue":
    ;
    case "%XPercentOfSeries":
    ;
    case "%XPercentOfTotal":
      return 2
  }
  return io.f.Pa.call(this, a)
};
z.Se = function() {
  io.f.Se.call(this);
  this.Ub && this.b.add("%XValue", this.Ub)
};
z.Na = function(a) {
  ne(this.b, "%XPercentOfSeries") || this.b.add("%XPercentOfSeries", 100 * (this.Ub / this.o.Uf("%SeriesXSum")));
  ne(this.b, "%XPercentOfTotal") || this.b.add("%XPercentOfTotal", 100 * (this.Ub / this.ca().Na("%DataPlotXSum")));
  return te(a) && this.Fd ? this.Fd.Na(a) : 0 == a.indexOf("%YAxis") ? this.o.eb.Na(ue(a)) : 0 == a.indexOf("%XAxis") ? this.o.Vb.Na(ue(a)) : io.f.Na.call(this, a)
};
z.mb = function(a) {
  a = io.f.mb.call(this, a);
  a.XValue = this.Ub;
  a.XPercentOfSeries = this.b.get("%XPercentOfSeries");
  a.XPercentOfTotal = this.b.get("%XPercentOfTotal");
  a.XPercentOfCategory = this.b.get("%XPercentOfCategory");
  a.Category = this.Fd ? this.Fd.getName() : p;
  return a
};
function ko() {
  co.call(this)
}
A.e(ko, co);
z = ko.prototype;
z.Vb = p;
z.eb = p;
z.Dc = p;
z.Df = u("Dc");
z.po = p;
z.sJ = q;
z.Xj = NaN;
z.en = x(j);
z.eH = x(q);
z.zf = p;
z.xF = t("zf");
z.Cb = function() {
  ko.f.Cb.call(this);
  this.b.add("%SeriesFirstXValue", 0);
  this.b.add("%SeriesLastXValue", 0);
  this.b.add("%SeriesXSum", 0);
  this.b.add("%SeriesXMax", -Number.MAX_VALUE);
  this.b.add("%SeriesXMin", Number.MAX_VALUE);
  this.b.add("%SeriesXAverage", 0);
  this.b.add("%SeriesYAxisName", "");
  this.b.add("%SeriesXAxisName", "")
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesFirstXValue":
    ;
    case "%SeriesLastXValue":
    ;
    case "%SeriesXSum":
    ;
    case "%SeriesXMin":
    ;
    case "%SeriesXMax":
    ;
    case "%SeriesXAverage":
      return Km(this.Vb) ? 3 : 2;
    case "%SeriesYAxisName":
    ;
    case "%SeriesXAxisName":
      return 1
  }
  return ko.f.Pa.call(this, a)
};
z.th = function(a) {
  ko.f.th.call(this, a);
  a = a.b;
  oe(a, "%DataPlotXSum", this.b.get("%SeriesXSum"));
  this.b.get("%SeriesXMax") > a.get("%DataPlotXMax") && a.add("%DataPlotXMax", this.b.get("%SeriesXMax"));
  this.b.get("%SeriesXMin") < a.get("%DataPlotXMin") && a.add("%DataPlotXMin", this.b.get("%SeriesXMin"));
  this.it()
};
z.Ed = function(a) {
  ko.f.Ed.call(this, a);
  var a = a.Sd(), b = Number(this.b.get("%SeriesXMax")), c = Number(this.b.get("%SeriesXMin"));
  oe(this.b, "%SeriesXSum", a);
  a > b && this.b.add("%SeriesXMax", a);
  a < c && this.b.add("%SeriesXMin", a)
};
z.it = function() {
  var a = this.Vb.b;
  oe(a, "%AxisPointCount", this.ba.length);
  oe(a, "%AxisSum", this.b.get("%SeriesXSum"));
  this.b.get("%SeriesXMax") > a.get("%AxisMax") && a.add("%AxisMax", this.b.get("%SeriesXMax"));
  this.b.get("%SeriesXMin") < a.get("%AxisMin") && a.add("%AxisMin", this.b.get("%SeriesXMin"));
  a = this.Vb;
  a.b.add("%AxisName", a.getName())
};
z.rh = function() {
  ko.f.rh.call(this);
  this.b.add("%SeriesYAxisName", this.eb.getName());
  this.b.add("%SeriesXAxisName", this.Vb.getName());
  0 < this.ba.length && this.Ga(0) && this.b.add("%SeriesFirstXValue", this.Ga(0).Sd());
  0 < this.ba.length && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastXValue", this.Ga(this.ba.length - 1).Sd());
  this.b.add("%SeriesXAverage", this.b.get("%SeriesXSum") / this.ba.length)
};
z.mb = function(a) {
  a = ko.f.mb.call(this, a);
  a.StartMarkerType = a.EndMarkerType = this.ge;
  a.FirstXValue = this.b.get("%SeriesFirstXValue");
  a.LastXValue = this.b.get("%SeriesLastXValue");
  a.XSum = this.b.get("%SeriesXSum");
  a.XMax = this.b.get("%SeriesXMax");
  a.XMin = this.b.get("%SeriesXMin");
  a.XAverage = this.b.get("%SeriesXAverage");
  a.XMedian = this.b.get("%SeriesXMedian");
  a.XMode = this.b.get("%SeriesXMode");
  this.eb && (a.YAxis = this.eb.getName());
  this.Vb && (a.XAxis = this.Vb.getName());
  return a
};
function lo() {
  ko.apply(this)
}
A.e(lo, ko);
z = lo.prototype;
z.Cb = function() {
  lo.f.Cb.call(this);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%Value", 0);
  this.b.add("%SeriesValue", 0);
  this.b.add("%SeriesYMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYMin", Number.MAX_VALUE);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%MaxYValuePointName", "");
  this.b.add("%MinYValuePointName", "")
};
z.Pa = function(a) {
  if(re(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%SeriesLastYValue":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesYSum":
    ;
    case "%Value":
    ;
    case "%SeriesValue":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYAverage":
      return Km(this.eb) ? 3 : 2;
    case "%MaxYValuePointName":
    ;
    case "%MinYValuePointName":
      return 1
  }
  return lo.f.Pa.call(this, a)
};
z.th = function(a) {
  lo.f.th.call(this, a);
  a = a.b;
  oe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  oe(a, "%YBasedPointsCount", this.ba.length);
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  lo.f.Ed.call(this, a);
  oe(this.b, "%SeriesYSum", a.Xb());
  oe(this.b, "%Value", a.Xb());
  oe(this.b, "%SeriesValue", a.Xb());
  a.Xb() > this.b.get("%SeriesYMax") && (this.b.add("%SeriesYMax", a.Xb()), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  a.Xb() < this.b.get("%SeriesYMin") && (this.b.add("%SeriesYMin", a.Xb()), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.it = function() {
  lo.f.it.call(this);
  var a = this.eb.b;
  oe(a, "%AxisPointCount", this.ba.length);
  oe(a, "%AxisSum", this.b.get("%SeriesYSum"));
  this.b.get("%SeriesYMax") > a.get("%AxisMax") && a.add("%AxisMax", this.b.get("%SeriesYMax"));
  this.b.get("%SeriesYMin") < a.get("%AxisMin") && a.add("%AxisMin", this.b.get("%SeriesYMin"));
  a = this.eb;
  a.b.add("%AxisName", a.getName())
};
z.rh = function() {
  lo.f.rh.call(this);
  this.b.add("%SeriesYAverage", this.b.get("%SeriesYSum") / this.ba.length);
  this.ba && this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Xb());
  this.ba && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastYValue", this.Ga(this.ba.length - 1).Xb())
};
z.mb = function(a) {
  a = lo.f.mb.call(this, a);
  a.FirstYValue = this.b.get("%SeriesFirstYValue");
  a.LastYValue = this.b.get("%SeriesLastYValue");
  a.YSum = this.b.get("%SeriesYSum");
  a.YMax = this.b.get("%SeriesYMax");
  a.YMin = this.b.get("%SeriesYMin");
  a.YAverage = this.b.get("%SeriesYAverage");
  a.YMedian = this.b.get("%SeriesYMedian");
  a.YMode = this.b.get("%SeriesYMode");
  return a
};
function mo() {
  io.apply(this)
}
A.e(mo, io);
z = mo.prototype;
z.ma = p;
z.Xb = u("ma");
z.dl = function(a) {
  C(a, "y") ? 1 == this.o.eb.Jb() && (this.Gb = isNaN(jo(this, F(a, "y")))) : this.Gb = j
};
z.$h = function(a) {
  1 == this.o.eb.Jb() ? (this.ma = jo(this, F(a, "y")), (this.Gb = isNaN(this.ma)) || this.o.eb.Gc(this.ma)) : (this.nw = this.eb.Df(J(a, "y")), this.y = this.nw.index, this.Gb || this.nw.Ed(this));
  mo.f.$h.call(this, a)
};
z.rk = function() {
  this.o.eb.Gc(this.ma);
  mo.f.rk.call(this)
};
z.Cb = function() {
  mo.f.Cb.call(this);
  this.b.add("%Value", 0);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%YValue":
      return Km(this.o.eb) ? 3 : 2;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
    ;
    case "%YPercentOfCategory":
      return 2
  }
  return mo.f.Pa.call(this, a)
};
z.kt = function(a) {
  a = a.b;
  oe(a, "%CategoryYSum", this.ma);
  oe(a, "%CategoryPointCount", 1);
  this.ma > a.get("%CategoryYMax") && a.add("%CategoryYMax", this.ma);
  this.ma < a.get("%CategoryYMin") && a.add("%CategoryYMin", this.ma)
};
z.Se = function() {
  mo.f.Se.call(this);
  this.ma && this.b.add("%YValue", this.ma);
  this.ma && this.b.add("%Value", this.ma)
};
z.Na = function(a) {
  ne(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.Xb() / this.o.Uf("%SeriesYSum")));
  ne(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.Xb() / this.ca().Na("%DataPlotYSum")));
  !ne(this.b, "%YPercentOfCategory") && this.Fd && this.b.add("%YPercentOfCategory", 100 * (this.Xb() / this.Fd.Na("%CategoryYSum")));
  return mo.f.Na.call(this, a)
};
z.mb = function(a) {
  a = mo.f.mb.call(this, a);
  a.YValue = this.ma;
  a.YPercentOfSeries = this.b.get("%YPercentOfSeries");
  a.YPercentOfTotal = this.b.get("%YPercentOfTotal");
  a.YPercentOfCategory = this.b.get("%YPercentOfCategory");
  return a
};
function no() {
  io.apply(this)
}
A.e(no, mo);
z = no.prototype;
z.gf = p;
z.$b = NaN;
z.$h = function(a) {
  var b = this.o.eb;
  if(1 == b.Jb()) {
    this.ma = jo(this, F(a, "y"));
    if(this.Dc != p) {
      switch(b.fa().ml()) {
        case 1:
          this.$b = cn(this.Dc, b.getName(), this.o.Jb(), q).Od(this, this.ma);
          b.Gc(this.$b);
          break;
        case 2:
          this.$b = cn(this.Dc, b.getName(), this.o.Jb(), j).Od(this, this.ma);
          break;
        default:
          this.$b = this.ma
      }
    }else {
      this.$b = this.ma, b.Gc(this.$b)
    }
    this.Gb = isNaN(this.ma);
    this.wl = b.fa().Nb() && 0 <= this.ma || !b.fa().Nb() && 0 > this.ma;
    this.Gb || b.Gc(this.$b)
  }else {
    no.f.$h.call(this, a)
  }
};
z.rk = function() {
  var a = this.o.eb;
  if(this.Dc != p) {
    switch(a.fa().ml()) {
      case 1:
        this.$b = cn(this.Dc, a.getName(), this.o.Jb(), q).Od(this, this.ma);
        a.Gc(this.$b);
        break;
      case 2:
        this.$b = cn(this.Dc, a.getName(), this.o.Jb(), j).Od(this, this.ma);
        break;
      default:
        this.$b = this.ma
    }
  }else {
    this.$b = this.ma, a.Gc(this.$b)
  }
  this.wl = a.fa().Nb() && 0 <= this.ma || !a.fa().Nb() && 0 > this.ma;
  no.f.rk.call(this, this)
};
z.GF = function(a) {
  this.$b *= 100 / a
};
function oo() {
  io.apply(this)
}
A.e(oo, no);
oo.prototype.Lf = NaN;
oo.prototype.$h = function(a) {
  var b = this.o.eb;
  if(1 == b.Jb()) {
    this.ma = jo(this, F(a, "y"));
    if(this.Dc != p) {
      switch(b.fa().ml()) {
        case 1:
          a = cn(this.Dc, b.getName(), this.o.Jb(), q);
          this.Lf = 0 > this.ma ? a.Gl : a.Nl;
          this.$b = a.Od(this, this.ma);
          b.Gc(this.Lf);
          b.Gc(this.$b);
          break;
        case 2:
          a = cn(this.Dc, b.getName(), this.o.Jb(), j);
          this.Lf = 0 > this.ma ? a.Gl : a.Nl;
          this.$b = a.Od(this, this.ma);
          break;
        default:
          this.Lf = NaN, this.$b = this.ma, b.Gc(this.$b)
      }
    }else {
      this.Lf = NaN, this.$b = this.ma, b.Gc(this.$b)
    }
    this.Gb = isNaN(this.ma);
    this.wl = b.fa().Nb() && 0 <= this.ma || !b.fa().Nb() && 0 > this.ma
  }else {
    oo.f.$h.call(this, a), this.Lf = NaN, this.$b = this.ma
  }
};
oo.prototype.rk = function() {
  var a = this.o.eb;
  if(this.Dc != p) {
    var b;
    switch(a.fa().ml()) {
      case 1:
        b = cn(this.Dc, a.getName(), this.o.Jb(), q);
        this.Lf = 0 > this.ma ? b.Gl : b.Nl;
        this.$b = b.Od(this, this.ma);
        a.Gc(this.Lf);
        a.Gc(this.$b);
        break;
      case 2:
        b = cn(this.Dc, a.getName(), this.o.Jb(), j);
        this.Lf = 0 > this.ma ? b.Gl : b.Nl;
        this.$b = b.Od(this, this.ma);
        break;
      default:
        this.Lf = NaN, this.$b = this.ma, a.Gc(this.$b)
    }
  }else {
    this.Lf = NaN, this.$b = this.ma, a.Gc(this.$b)
  }
  this.wl = a.fa().Nb() && 0 <= this.ma || !a.fa().Nb() && 0 > this.ma
};
oo.prototype.GF = function(a) {
  this.Lf *= 0 == a ? 0 : 100 / a;
  this.$b *= 0 == a ? 0 : 100 / a
};
function po() {
  io.apply(this)
}
A.e(po, io);
z = po.prototype;
z.Tb = p;
z.Lb = p;
z.hf = p;
z.dl = function(a) {
  !C(a, "start") || !C(a, "end") ? this.Gb = j : this.o.eb.vJ() && (this.Gb = (this.Gb = isNaN(jo(this, F(a, "start")))) || isNaN(jo(this, F(a, "end"))))
};
z.$h = function(a) {
  C(a, "start") && (this.Tb = jo(this, F(a, "start")));
  C(a, "end") && (this.Lb = jo(this, F(a, "end")));
  a = this.o.eb;
  this.wl = a.fa().Nb() && this.Tb < this.Lb || !a.fa().Nb() && this.Tb > this.Lb;
  this.Gb || (this.hf = this.Lb - this.Tb, a.Gc(this.Tb), a.Gc(this.Lb))
};
z.rk = function() {
  this.hf = this.Lb - this.Tb;
  var a = this.o.eb;
  a.Gc(this.Tb);
  a.Gc(this.Lb);
  po.f.rk.call(this)
};
z.sw = p;
z.tw = p;
z.uw = p;
z.LD = function() {
  po.f.LD.call(this);
  this.sw = Zn(this, this.Ad, j);
  this.tw = Zn(this, this.Bd, j);
  this.uw = Zn(this, this.Cd, q)
};
z.Sn = function() {
  po.f.Sn.call(this);
  this.Ni(this.Ad, this.sw);
  this.Ni(this.Bd, this.tw)
};
z.sG = function(a) {
  po.f.sG.call(this, a);
  var b = this.Cd, c = this.uw;
  this.Ni(b, c);
  $n(this, a, b, c)
};
z.ny = function() {
  po.f.ny.call(this);
  ao(this.uw)
};
z.JE = function(a) {
  po.f.JE.call(this, a);
  $n(this, a, this.Cd, this.uw)
};
z.Lz = function() {
  po.f.Lz.call(this);
  this.sw && this.Dp(this.Ad, this.sw);
  this.tw && this.Dp(this.Bd, this.tw)
};
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Cm = function(a, b) {
  C(a, "start_point") && (this.uo(F(a, "start_point"), b), this.fk(F(a, "start_point"), b), this.vo(F(a, "start_point"), b));
  C(a, "end_point") && (this.At(F(a, "end_point"), b), this.Bt(F(a, "end_point"), b), this.Ct(F(a, "end_point"), b))
};
z.At = function(a, b) {
  this.Ad && (C(a, "label") && this.Ad.om(F(a, "label")) && (this.Ad = this.Ad.copy(), this.Ad.Yi(F(a, "label"), b)), this.Wj(this.Ad))
};
z.Bt = function(a, b) {
  this.Bd && (C(a, "marker") && this.Bd.om(F(a, "marker")) && (this.Bd = this.Bd.copy(), this.Bd.Yi(F(a, "marker"), b)), this.Wj(this.Bd))
};
z.Ct = function(a, b) {
  this.Cd && C(a, "tooltip") && this.Cd.om(F(a, "tooltip")) && (this.Cd = this.Cd.copy(), this.Cd.Yi(F(a, "tooltip"), b));
  this.Wj(this.Cd)
};
z.Cb = function() {
  po.f.Cb.call(this);
  this.b.add("%RangeStart", 0);
  this.b.add("%YRangeStart", 0);
  this.b.add("%RangeEnd", 0);
  this.b.add("%YRangeEnd", 0);
  this.b.add("%Range", 0);
  this.b.add("%YRange", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%RangeStart":
    ;
    case "%YRangeStart":
    ;
    case "%RangeEnd":
    ;
    case "%YRangeEnd":
      return Km(this.o.eb) ? 3 : 2;
    case "%Range":
    ;
    case "%YRange":
    ;
    case "%Value":
      return 2
  }
  return po.f.Pa.call(this, a)
};
z.kt = function(a) {
  po.f.kt.call(this, a);
  a = a.b;
  oe(a, "%CategoryYRangeSum", this.hf);
  this.hf > a.get("%CategoryYRangeMax") && a.add("%CategoryYRangeMax", this.hf);
  this.hf < a.get("%CategoryYRangeMin") && a.add("%CategoryYRangeMin", this.hf)
};
z.Se = function() {
  po.f.Se.call(this);
  this.b.add("%RangeStart", this.Tb);
  this.b.add("%YRangeStart", this.Tb);
  this.b.add("%RangeEnd", this.Lb);
  this.b.add("%YRangeEnd", this.Lb);
  this.b.add("%Range", this.hf);
  this.b.add("%YRange", this.hf);
  this.b.add("%Value", this.hf)
};
z.mb = function(a) {
  a = po.f.mb.call(this, a);
  a.YRangeStart = this.Tb;
  a.YRangeEnd = this.Lb;
  a.YRange = this.hf;
  a.StartValue = this.Tb;
  a.EndValue = this.Lb;
  a.StartMarkerType = this.ge;
  a.EndMarkerType = this.ge;
  return a
};
function qo() {
  ko.apply(this)
}
A.e(qo, ko);
z = qo.prototype;
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Cm = function(a, b) {
  this.uo(F(a, "start_point"), b);
  this.fk(F(a, "start_point"), b);
  this.vo(F(a, "start_point"), b);
  this.At(F(a, "end_point"), b);
  this.Bt(F(a, "end_point"), b);
  this.Ct(F(a, "end_point"), b)
};
z.At = function(a, b) {
  this.Ad && this.Ad.sh(F(a, "label")) && (this.Ad = this.Ad.copy(), this.Ad.Zi(F(a, "label"), b))
};
z.Bt = function(a, b) {
  this.Bd && this.Bd.sh(F(a, "marker")) && (this.Bd = this.Bd.copy(), this.Bd.Zi(F(a, "marker"), b))
};
z.Ct = function(a, b) {
  this.Cd && this.Cd.sh(F(a, "tooltip")) && (this.Cd = this.Cd.copy(), this.Cd.Zi(F(a, "tooltip"), b))
};
z.Mh = function(a) {
  qo.f.Mh.call(this, a);
  a.Ad = this.Ad;
  a.Bd = this.Bd;
  a.Cd = this.Cd
};
z.Cb = function() {
  qo.f.Cb.call(this);
  this.b.add("%SeriesYRangeMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYRangeMin", Number.MAX_VALUE);
  this.b.add("%SeriesYRangeSum", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesYRangeMax":
    ;
    case "%SeriesYRangeMin":
    ;
    case "%SeriesYRangeSum":
      return Km(this.eb) ? 3 : 2
  }
  return qo.f.Pa.call(this, a)
};
z.th = function(a) {
  var b = a.b;
  oe(b, "%DataPlotYRangeSum", this.b.get("%SeriesYRangeSum"));
  this.b.get("%SeriesYRangeMax") > b.get("%DataPlotYRangeMax") && b.add("%DataPlotYRangeMax", this.b.get("%SeriesYRangeMax"));
  this.b.get("%SeriesYRangeMin") < b.get("%DataPlotYRangeMin") && b.add("%DataPlotYRangeMin", this.b.get("%SeriesYRangeMin"));
  qo.f.th.call(this, a)
};
z.Ed = function(a) {
  qo.f.Ed.call(this, a);
  oe(this.b, "%SeriesYRangeSum", a.hf);
  a.hf > this.b.get("%SeriesYRangeMax") && this.b.add("%SeriesYRangeMax", a.hf);
  a.hf < this.b.get("%SeriesYRangeMin") && this.b.add("%SeriesYRangeMin", a.hf)
};
z.rh = function() {
  qo.f.rh.call(this)
};
z.mb = function(a) {
  a = qo.f.mb.call(this, a);
  a.YRangeSum = this.b.get("%SeriesRangeSum");
  a.YRangeMax = this.b.get("%SeriesRangeMax");
  a.YRangeMin = this.b.get("%SeriesRangeMin");
  return a
};
function ro() {
  ho.apply(this)
}
A.e(ro, ho);
z = ro.prototype;
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Cm = function(a, b) {
  this.uo(F(a, "start_point"), b);
  this.fk(F(a, "start_point"), b);
  this.vo(F(a, "start_point"), b);
  this.At(F(a, "end_point"), b);
  this.Bt(F(a, "end_point"), b);
  this.Ct(F(a, "end_point"), b)
};
z.At = function(a, b) {
  (this.Ad = this.Aq()) && (a && C(a, "label_settings") ? this.Ad.Ue(F(a, "label_settings"), b) : this.Ad.rf(new $))
};
z.Bt = function(a, b) {
  (this.Bd = this.Am()) && (a && C(a, "marker_settings") ? this.Bd.Ue(F(a, "marker_settings"), b) : this.Bd.rf(new $))
};
eo.prototype.Ct = function(a, b) {
  (this.Cd = new Tn) && (a && C(a, "tooltip_settings") ? this.Cd.Ue(F(a, "tooltip_settings"), b) : this.Cd.rf(new $))
};
eo.prototype.Mh = function(a) {
  eo.f.Mh.call(this, a);
  a.Ad = this.Ad;
  a.Bd = this.Bd;
  a.Cd = this.Cd
};
ro.prototype.ND = s();
function so() {
  io.apply(this)
}
A.e(so, oo);
so.prototype.Xl = 0;
so.prototype.ol = u("Xl");
so.prototype.Lp = t("Xl");
so.prototype.jn = function(a) {
  return Wc(this.n(), a)
};
function to() {
  ko.apply(this)
}
A.e(to, lo);
z = to.prototype;
z.ts = NaN;
z.Xl = 0;
z.Lp = t("Xl");
z.tn = x(j);
z.g = function(a, b) {
  to.f.g.call(this, a, b);
  uo(this, a)
};
z.Ug = function() {
  this.ts = 0 < this.eb.fa().la ? this.eb.fa().la : 0
};
z.qe = function() {
  var a = new so;
  a.Lp(this.Xl);
  return a
};
function vo() {
  fo.apply(this)
}
A.e(vo, ho);
z = vo.prototype;
z.Xl = 0;
z.Lp = t("Xl");
z.rr = q;
z.Qt = NaN;
z.g = function(a, b) {
  vo.f.g.call(this, a, b);
  C(a, "point_width") && (this.rr = j, this.Qt = M(a, "point_width"));
  uo(this, a)
};
function uo(a, b) {
  C(b, "shape_type") && a.Lp(kn(N(b, "shape_type")))
}
z.WC = function(a) {
  return this.aa.My ? (this.nE ? this.aa.n().width : 1) * this.Pr : this.rr ? this.Qt : a.zf.nt
};
z.uh = function() {
  var a = new to;
  a.gh(0);
  a.Xj = 0;
  a.Lp(this.Xl);
  return a
};
z.Ah = x("bar_series");
z.Rb = x("bar_style");
z.Qb = function() {
  switch(this.Xl) {
    case 3:
      return this.aa.Jc ? new yn : new zn;
    default:
      return new xn
  }
};
so.prototype.bg = function(a, b, c, d) {
  b = this.wl ? this.o.Vb.J.vi.Px(b) : this.o.Vb.J.vi.Zx(b);
  10 == b ? so.f.bg.call(this, a, b, c, d) : this.$a.bg(a, b, c, d)
};
function wo() {
  po.apply(this)
}
A.e(wo, po);
wo.prototype.ol = x(0);
wo.prototype.p = function(a) {
  wo.f.p.call(this, a);
  return this.D
};
wo.prototype.hd = function() {
  var a = new O, b = new O, c = this.o, d = this.Ab().WC(this) / 2;
  c.Vb.transform(this, this.Sd(), a, -d);
  c.eb.transform(this, this.Tb, a);
  c.Vb.transform(this, this.Sd(), b, d);
  c.eb.transform(this, this.Lb, b);
  c = Math.min(a.x, b.x);
  d = Math.min(a.y, b.y);
  this.k = new P(c, d, Math.max(a.x, b.x) - c, Math.max(a.y, b.y) - d)
};
wo.prototype.jn = function(a) {
  return Wc(this.n(), a)
};
function xo() {
  qo.apply(this);
  this.rr = q
}
A.e(xo, qo);
xo.prototype.qe = function() {
  return new wo
};
function yo() {
  ro.apply(this)
}
A.e(yo, ro);
z = yo.prototype;
z.rr = p;
z.Qt = p;
z.g = function(a) {
  yo.f.g.call(this, a);
  C(a, "point_width") && (this.rr = j);
  this.Qt = M(a, "point_width")
};
z.WC = function(a) {
  return this.rr ? this.Qt : a.zf.nt
};
z.Qb = function() {
  return new Bn
};
z.uh = function() {
  var a = new xo;
  a.gh(1);
  a.Xj = 1;
  return a
};
z.Ah = x("range_bar_series");
z.Rb = x("bar_style");
function zo() {
}
A.e(zo, Mf);
zo.prototype.Ta = function() {
  Ao(this.j, this.G)
};
function Bo() {
}
A.e(Bo, Nf);
Bo.prototype.Ta = function() {
  Ao(this.j, this.G)
};
function Co() {
}
A.e(Co, Of);
Co.prototype.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new zo, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Bo, this.Fb.p(this.j, this))
};
Co.prototype.createElement = function() {
  var a = this.j.bt || 0, b = this.j.ct || 0, c = Math.abs(this.j.bd - this.j.Zh) || 0, d = Cc(this.j.r(), "circle");
  d.setAttribute("cx", a);
  d.setAttribute("cy", b);
  d.setAttribute("r", c);
  return d
};
Co.prototype.Ta = function() {
  Ao(this.j, this.G)
};
function Do() {
  $.call(this)
}
A.e(Do, Qf);
Do.prototype.lc = function() {
  return new Co
};
function Eo() {
  no.apply(this);
  this.Pd = new O
}
A.e(Eo, no);
z = Eo.prototype;
z.qh = p;
z.Co = u("qh");
z.bd = p;
z.vd = u("bd");
z.bt = p;
z.ct = p;
z.Zh = p;
z.Pd = p;
z.ae = u("Pd");
z.hd = function() {
  this.o.Vb.transform(this, this.Ub, this.Pd);
  this.o.eb.transform(this, this.$b, this.Pd);
  this.bt = this.Pd.x;
  this.ct = this.Pd.y;
  this.bd = this.Ab().Co(this.qh) / 2;
  this.Zh = 0;
  this.k = new P(this.bt - this.bd, this.ct - this.bd, 2 * this.bd, 2 * this.bd);
  this.k.x += this.Zh;
  this.k.y += this.Zh;
  this.k.width -= 2 * this.Zh;
  this.k.height -= 2 * this.Zh;
  this.ya.Mc() && this.ya.Mc().isEnabled() && (this.Zh = this.ya.Mc().Va() / 2)
};
function Ao(a, b) {
  a.hd();
  b.setAttribute("cx", a.bt);
  b.setAttribute("cy", a.ct);
  b.setAttribute("r", Math.abs(a.bd - a.Zh));
  a.k.x -= a.Zh;
  a.k.y -= a.Zh;
  a.k.width += 2 * a.Zh;
  a.k.height += 2 * a.Zh
}
z.$h = function(a) {
  Eo.f.$h.call(this, a);
  this.qh = M(a, "size");
  if(isNaN(this.qh)) {
    this.Gb = j
  }else {
    if(a = this.Ab(), this.qh > a.hz && (a.hz = this.qh), this.qh < a.Tu) {
      a.Tu = this.qh
    }
  }
};
z.dl = function(a) {
  Eo.f.dl.call(this, a);
  this.Gb = this.Gb || !C(a, "size") || isNaN(M(a, "size"));
  !this.Ab().NH && 0 > M(a, "size") && (this.Gb = j)
};
z.jn = function(a) {
  var b = new Fo(a.x, a.y), c = new Fo(a.x + a.width, a.y + a.height), d = new Fo(this.bt, this.ct), f = this.n();
  return(b = Go.YM(d, f.width / 2, [b, c])) && b.Fa.length ? j : Xc(a, f) || Xc(f, a)
};
z.Cb = function() {
  Eo.f.Cb.call(this);
  this.b.add("%BubbleSize", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%BubbleSize":
    ;
    case "%BubbleSizePercentOfSeries":
    ;
    case "%BubbleSizePercentOfTotal":
    ;
    case "%BubbleSizePercentOfCategory":
      return 2
  }
  return Eo.f.Pa.call(this, a)
};
z.Se = function() {
  Eo.f.Se.call(this);
  this.b.add("%BubbleSize", this.qh)
};
z.Na = function(a) {
  ne(this.b, "%BubbleSizePercentOfSeries") || this.b.add("%BubbleSizePercentOfSeries", 100 * (this.qh / this.o.Uf("%SeriesBubbleSizeSum")));
  ne(this.b, "%BubbleSizePercentOfTotal") || this.b.add("%BubbleSizePercentOfTotal", 100 * (this.qh / this.ca().Na("%DataPlotBubbleSizeSum")));
  !ne(this.b, "%BubbleSizePercentOfCategory") && this.Fd && this.b.add("%BubbleSizePercentOfCategory", 100 * (this.qh / 0));
  return Eo.f.Na.call(this, a)
};
z.mb = function(a) {
  a = Eo.f.mb.call(this, a);
  a.BubbleSize = this.qh;
  a.BubbleSizePercentOfSeries = this.b.get("%BubbleSizePercentOfSeries");
  a.BubbleSizePercentOfTotal = this.b.get("%BubbleSizePercentOfTotal");
  a.BubbleSizePercentOfCategory = this.b.get("%BubbleSizePercentOfCategory");
  return a
};
function Ho() {
  lo.apply(this)
}
A.e(Ho, lo);
z = Ho.prototype;
z.en = x(q);
z.qe = function() {
  return new Eo
};
z.Cb = function() {
  Ho.f.Cb.call(this);
  this.b.add("%SeriesBubbleSizeSum", 0);
  this.b.add("%SeriesBubbleMaxSize", -Number.MAX_VALUE);
  this.b.add("%SeriesBubbleMinSize", Number.MAX_VALUE)
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesBubbleSizeSum":
    ;
    case "%SeriesBubbleMaxSize":
    ;
    case "%SeriesBubbleMinSize":
    ;
    case "%SeriesBubbleSizeAverage":
    ;
    case "%SeriesBubbleSizeMedian":
    ;
    case "%SeriesBubbleSizeMode":
      return 2
  }
  return Ho.f.Pa.call(this, a)
};
z.th = function(a) {
  Ho.f.th.call(this, a);
  a = a.b;
  oe(a, "%DataPlotBubbleSizeSum", this.b.get("%SeriesBubbleSizeSum"));
  oe(a, "%DataPlotBubblePointCount", this.ba.length);
  this.b.get("%SeriesBubbleMaxSize") > a.get("%DataPlotBubbleMaxSize") && a.add("%DataPlotBubbleMaxSize", this.b.get("%SeriesBubbleMaxSize"));
  this.b.get("%SeriesBubbleMinSize") < a.get("%DataPlotBubbleMinSize") && a.add("%DataPlotBubbleMinSize", this.b.get("%SeriesBubbleMinSize"))
};
z.Ed = function(a) {
  Ho.f.Ed.call(this, a);
  oe(this.b, "%SeriesBubbleSizeSum", a.Co());
  a.Co() > this.b.get("%SeriesBubbleMaxSize") && this.b.add("%SeriesBubbleMaxSize", a.Co());
  a.Co() < this.b.get("%SeriesBubbleMinSize") && this.b.add("%SeriesBubbleMinSize", a.Co())
};
z.it = function() {
  Io(this, this.Vb.b);
  Io(this, this.eb.b);
  Ho.f.it.call(this)
};
function Io(a, b) {
  oe(b, "%AxisBubbleSizeSum", a.b.get("%SeriesBubbleSizeSum"));
  a.b.get("%SeriesBubbleMaxSize") > b.get("%AxisBubbleMax") && b.add("%AxisBubbleSizeMax", a.b.get("%SeriesBubbleMaxSize"));
  a.b.get("%SeriesBubbleMinSize") < b.get("%AxisBubbleSizeMin") && b.add("%AxisBubbleSizeMin", a.b.get("%SeriesBubbleMinSize"))
}
z.rh = function() {
  Ho.f.rh.call(this);
  this.b.add("%SeriesBubbleSizeAverage", this.Uf("%SeriesBubbleSizeSum") / this.ba.length)
};
z.mb = function(a) {
  a = Ho.f.mb.call(this, a);
  a.BubbleSizeSum = this.b.get("%SeriesBubbleSizeSum");
  a.BubbleMaxSize = this.b.get("%SeriesBubbleSizeMax");
  a.BubbleMinSize = this.b.get("%SeriesBubbleSizeMin");
  a.BubbleSizeAverage = this.b.get("%SeriesBubbleSizeAverage");
  a.BubbleSizeMedian = this.b.get("%SeriesBubbleSizeMedian");
  a.BubbleSizeMode = this.b.get("%SeriesBubbleSizeMode");
  return a
};
function Jo() {
  ho.apply(this)
}
A.e(Jo, ho);
z = Jo.prototype;
z.mE = q;
z.Vk = 0;
z.Tu = Number.MAX_VALUE;
z.hz = -Number.MAX_VALUE;
z.kE = j;
z.pn = 0.2;
z.lE = j;
z.rn = 0.01;
z.NH = j;
z.Co = function(a) {
  var b = Math.min(this.ca().n().width, this.ca().n().height);
  if(0 < this.Vk) {
    return this.mE ? a * this.Vk * b : a * this.Vk
  }
  var c = this.lE ? b * this.rn : this.rn, b = this.kE ? b * this.pn : this.pn;
  return isNaN(a) || this.Tu == this.hz ? b : c + (a - this.Tu) / (this.hz - this.Tu) * (b - c)
};
z.g = function(a) {
  if(a && (Jo.f.g.call(this, a), C(a, "maximum_bubble_size") && (this.pn = F(a, "maximum_bubble_size"), Mb(this.pn) ? (this.kE = j, this.pn = Nb(this.pn)) : (this.kE = q, this.pn = Ob(this.pn))), C(a, "minimum_bubble_size") && (this.rn = F(a, "minimum_bubble_size"), Mb(this.rn) ? (this.lE = j, this.rn = Nb(this.rn)) : (this.lE = q, this.rn = Ob(this.rn))), C(a, "bubble_size_multiplier") && (this.Vk = F(a, "bubble_size_multiplier"), Mb(this.Vk) ? (this.mE = j, this.Vk = Nb(this.Vk)) : (this.mE = q, 
  this.Vk = Ob(this.Vk))), C(a, "display_negative_bubbles"))) {
    this.NH = K(a, "display_negative_bubbles")
  }
};
z.Qb = function() {
  return new Do
};
z.uh = function() {
  var a = new Ho;
  a.type = 12;
  a.EL = 12;
  return a
};
z.Ah = x("bubble_series");
z.Rb = x("bubble_style");
function Ko() {
}
A.e(Ko, Me);
Ko.prototype.createElement = function() {
  return this.$a.createElement()
};
Ko.prototype.vf = function(a) {
  var b = this.j.r();
  Lo(a) ? (a = Ld(a.Kb, b, this.j.n(), this.j.Ba().Ba()), a += md()) : a = od();
  this.G.setAttribute("style", a)
};
Ko.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Mo() {
}
A.e(Mo, Qe);
z = Mo.prototype;
z.Hf = p;
z.Yc = function() {
  this.j.ka().mz() && (this.Hf = new Ko, this.Hf.p(this.j, this))
};
z.ff = function() {
  this.Hf && this.j.fb().appendChild(this.Hf.G)
};
z.update = function(a, b) {
  this.Hf && this.Hf.update(a, b)
};
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
function No(a, b) {
  Re.call(this, a, b)
}
A.e(No, Se);
No.prototype.Kb = p;
function Lo(a) {
  return a.Kb && a.Kb.isEnabled()
}
No.prototype.g = function(a) {
  if(!a) {
    return a
  }
  a = No.f.g.call(this, a);
  C(a, "line") && Tb(F(a, "line")) && (this.Kb = new Kd, this.Kb.g(F(a, "line")));
  return a
};
function Oo() {
  $.call(this)
}
A.e(Oo, $);
Oo.prototype.cc = function() {
  return No
};
Oo.prototype.lc = s();
Oo.prototype.mz = function() {
  return Lo(this.wa) || Lo(this.dc) || Lo(this.kc) || Lo(this.Yb) || Lo(this.gc) || Lo(this.nc)
};
function Po() {
}
A.e(Po, Ko);
Po.prototype.Ta = function() {
  this.j.Uj();
  this.j.Hy && this.G.setAttribute("points", this.$a.yh())
};
function Qo() {
}
A.e(Qo, Mo);
Qo.prototype.Yc = function() {
  this.Hf = new Po;
  this.Hf.p(this.j, this)
};
Qo.prototype.createElement = function() {
  return hd(this.j.r())
};
Qo.prototype.yh = function() {
  var a = this.j.Ua;
  if(a) {
    var b = a.length;
    if(!(1 >= b)) {
      for(var c = id(a[0].x, a[0].y), d = 1;d < b;d++) {
        c += id(a[d].x, a[d].y)
      }
      return c
    }
  }
};
function Ro() {
  $.call(this)
}
A.e(Ro, Oo);
Ro.prototype.lc = function() {
  return new Qo
};
function So() {
  io.apply(this);
  this.Da = new O
}
A.e(So, no);
z = So.prototype;
z.rJ = q;
z.Fl = j;
z.Lg = q;
z.Ch = q;
z.Hy = q;
z.Ua = p;
z.jn = function(a) {
  var b = [], c = this.Ua;
  if(2 > c.length) {
    return q
  }
  for(var d = To.by({x:a.x, y:a.y}), f = To.by({x:a.x + a.width, y:a.y + a.height}), g = 0;g < c.length - 1;g++) {
    var k = c[g + 1];
    b.push({Vg:To.by(c[g]), Wg:To.by(k)})
  }
  for(c = 0;c < b.length;c++) {
    g = b[c];
    if((k = Go.nr(g.Vg, g.Wg, d, f)) && k.Fa && k.Fa.length) {
      return j
    }
    g = To.XC([g.Vg, g.Wg]);
    g = new P(g.x, g.y, g.width, g.height);
    if(Xc(a, g) || Xc(g, a)) {
      return j
    }
  }
  return q
};
z.Da = p;
z.p = function(a) {
  So.f.p.call(this, a);
  return this.D
};
z.Uj = function() {
  this.Xo || this.Po();
  this.k = new P(this.Da.x, this.Da.y, 0, 0);
  this.Hy && this.zm()
};
z.Po = function() {
  this.rJ || (this.Lg = 0 < this.na && this.o.Ga(this.na - 1) && (!this.o.Ga(this.na - 1).Gb || !this.ca().qk), this.Ch = this.na < this.o.Fe() - 1 && this.o.Ga(this.na + 1) && (!this.o.Ga(this.na + 1).Gb || !this.ca().qk), this.Hy = this.Lg || this.Ch, this.rJ = j);
  this.aw();
  this.o.eb.transform(this, this.$b, this.Da)
};
z.aw = function() {
  this.o.Vb.transform(this, this.Ub, this.Da)
};
z.zm = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1), a = a.Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (a = this.o.Ga(this.na + 1), a.Po(), a.Fl = q, a = a.Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b))
};
function Uo() {
  ko.apply(this)
}
A.e(Uo, lo);
Uo.prototype.qe = function() {
  return new So
};
Uo.prototype.en = x(q);
Uo.prototype.tn = x(j);
function Vo() {
  fo.apply(this)
}
A.e(Vo, ho);
z = Vo.prototype;
z.OF = q;
z.g = function(a) {
  Vo.f.g.call(this, a);
  C(a, "shift_step_line") && (this.OF = K(a, "shift_step_line"))
};
z.uh = function(a) {
  var b;
  switch(a) {
    case 5:
      b = new Wo;
      break;
    case 4:
      b = new Xo;
      break;
    default:
    ;
    case 2:
      b = new Uo
  }
  b.gh(a);
  b.Xj = 2;
  return b
};
z.Qb = function() {
  return new Ro
};
z.Ah = x("line_series");
z.Rb = x("line_style");
function Wo() {
  ko.apply(this)
}
A.e(Wo, Uo);
Wo.prototype.qe = function() {
  return new Yo
};
function Yo() {
  So.call(this)
}
A.e(Yo, So);
Yo.prototype.zm = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1).Da, b = this.Jc ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (this.o.Ga(this.na + 1).Po(), this.o.Ga(this.na + 1).Fl = q, a = this.o.Ga(this.na + 1).Da, b = this.Jc ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b), b = this.Jc ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b))
};
Yo.prototype.aw = function() {
  Yo.f.aw.call(this);
  if(this.Dc && this.Ab().OF) {
    var a = this.o.Vb, b = a.fa().pc(1) - a.fa().pc(0.5);
    a.transform(this, this.Ub, this.Da, -b)
  }
};
function Xo() {
  ko.apply(this)
}
A.e(Xo, Uo);
Xo.prototype.qe = function() {
  return new Zo
};
function Zo() {
  So.call(this)
}
A.e(Zo, So);
Zo.prototype.zm = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1).Da, b = this.Jc ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), b = this.Jc ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (this.o.Ga(this.na + 1).Po(), this.o.Ga(this.na + 1).KE = q, a = this.o.Ga(this.na + 1).Da, b = this.Jc ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b))
};
Zo.prototype.aw = function() {
  Zo.f.aw.call(this);
  if(this.Dc && this.Ab().OF) {
    var a = this.o.Vb, b = a.fa().pc(1) - a.fa().pc(0.5);
    a.transform(this, this.Ub, this.Da, -b)
  }
};
function $o() {
}
A.e($o, Kf);
$o.prototype.createElement = function() {
  return this.$a.nx()
};
$o.prototype.Ta = function() {
  var a = this.$a;
  this.G.setAttribute("points", a.j.GI(a.j.r()))
};
function ap() {
}
A.e(ap, Lf);
ap.prototype.createElement = function() {
  return this.$a.mx()
};
ap.prototype.Ta = function() {
  this.G.setAttribute("d", this.$a.j.BI())
};
function bp() {
}
A.e(bp, Lf);
bp.prototype.createElement = function() {
  return this.$a.st()
};
bp.prototype.Ta = function() {
  var a = this.$a;
  this.G.setAttribute("d", a.j.oD(a.j.r()))
};
function cp() {
}
A.e(cp, Nf);
cp.prototype.createElement = function() {
  return this.$a.st()
};
cp.prototype.Ta = function() {
  var a = this.$a;
  this.G.setAttribute("d", a.j.oD(a.j.r()))
};
function dp() {
}
A.e(dp, Qe);
z = dp.prototype;
z.Pn = p;
z.tv = p;
z.Db = p;
z.Fb = p;
z.Yc = function() {
  var a = this.j.ka();
  a.mz() && (this.Pn = new $o, this.Pn.p(this.j, this));
  a.Zu() && (this.Db = new bp, this.Db.p(this.j, this), this.tv = new ap, this.tv.p(this.j, this));
  a.wi() && (this.Fb = new cp, this.Fb.p(this.j, this))
};
z.ff = function() {
  this.Db && (this.j.fb().appendChild(this.Db.G), this.j.fb().appendChild(this.tv.G));
  this.Pn && this.j.fb().appendChild(this.Pn.G);
  this.Fb && this.j.fb().appendChild(this.Fb.G)
};
z.nx = function() {
  return this.j.nx(this.j.r())
};
z.mx = function() {
  return this.j.mx(this.j.r())
};
z.st = function() {
  return this.j.st(this.j.r())
};
z.update = function(a, b) {
  this.Db && (this.Db.update(a, b), this.tv.update(a, b));
  this.Pn && this.Pn.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
function ep(a, b) {
  Re.call(this, a, b)
}
A.e(ep, Pf);
ep.prototype.fC = function(a) {
  C(a, "line") && (this.sc = new Kd, this.sc.g(F(a, "line")))
};
function fp() {
  $.call(this)
}
A.e(fp, Qf);
fp.prototype.cc = function() {
  return ep
};
fp.prototype.lc = function() {
  return new dp
};
function gp() {
}
A.e(gp, Kf);
gp.prototype.update = function(a, b) {
  gp.f.update.call(this, a, b)
};
gp.prototype.vf = function(a) {
  hp(a) && gp.f.vf.call(this, a)
};
gp.prototype.Ta = function() {
  for(var a = this.$a.j, b = "", c = a.Ne.length, d = 0;d < c;d++) {
    b += id(a.Ne[d].x, a.Ne[d].y)
  }
  this.G.setAttribute("points", b)
};
function ip() {
}
A.e(ip, dp);
z = ip.prototype;
z.Ws = p;
z.Yc = function() {
  ip.f.Yc.call(this);
  if(hp(this.j.ka().wa) || hp(this.j.ka().dc) || hp(this.j.ka().kc) || hp(this.j.ka().Yb) || hp(this.j.ka().gc) || hp(this.j.ka().nc)) {
    this.Ws = new gp, this.Ws.p(this.j, this, this.VB)
  }
};
z.ff = function() {
  this.Db && (this.j.fb().appendChild(this.Db.G), this.j.fb().appendChild(this.tv.G));
  this.Pn && this.j.fb().appendChild(this.Pn.G);
  this.Ws && this.j.fb().appendChild(this.Ws.G);
  this.Fb && this.j.fb().appendChild(this.Fb.G)
};
z.VB = function() {
  return this.j.VB(this.j.r())
};
z.update = function(a, b) {
  ip.f.update.call(this, a, b);
  a && hp(a) && this.Ws.update(a, b)
};
function jp(a, b) {
  Re.call(this, a, b)
}
A.e(jp, ep);
jp.prototype.Cx = p;
function hp(a) {
  return a.Cx && a.Cx.isEnabled()
}
jp.prototype.fC = function(a) {
  Tb(F(a, "start_line")) && (this.sc = new Kd, this.sc.g(F(a, "start_line")));
  Tb(F(a, "end_line")) && (this.Cx = new Kd, this.Cx.g(F(a, "end_line")))
};
function kp() {
  $.call(this)
}
A.e(kp, Qf);
kp.prototype.cc = function() {
  return jp
};
kp.prototype.lc = function() {
  return new ip
};
function lp() {
  fo.apply(this)
}
A.e(lp, ho);
lp.prototype.Qb = function() {
  return new fp
};
lp.prototype.Ah = x("area_series");
lp.prototype.Rb = x("area_style");
lp.prototype.uh = function(a) {
  var b;
  switch(a) {
    case 9:
      b = new mp;
      break;
    case 8:
      b = new np;
      break;
    case 6:
      b = new Sk;
      break;
    default:
      b = new Sk
  }
  b.gh(a);
  b.Xj = 2;
  return b
};
function Sk() {
  ko.apply(this)
}
A.e(Sk, Uo);
z = Sk.prototype;
z.BK = s();
z.tn = x(j);
z.ts = NaN;
z.P = p;
z.X = p;
z.Xs = function() {
  this.P = new O(Number.MAX_VALUE, Number.MAX_VALUE);
  this.X = new O(-Number.MAX_VALUE, -Number.MAX_VALUE);
  for(var a = this.Fe(), b = 0;b < a;b++) {
    var c = this.Ga(b);
    c && !c.Gb && (c.BF(j), c.CF(j), c.Uj(), op(this, c), c.gf == p ? (this.P.x = Math.min(c.Lc.x, c.ne.x, this.P.x), this.P.y = Math.min(c.Lc.y, c.ne.y, this.P.y), this.X.x = Math.max(c.Lc.x, c.ne.x, this.X.x), this.X.y = Math.max(c.Lc.y, c.ne.y, this.X.y)) : op(this, c.gf))
  }
};
function op(a, b) {
  for(var c = b.Ua, d = 0;d < c.length;d++) {
    a.P.x = Math.min(c[d].x, a.P.x), a.P.y = Math.min(c[d].y, a.P.y), a.X.x = Math.max(c[d].x, a.X.x), a.X.y = Math.max(c[d].y, a.X.y)
  }
}
z.Ug = function() {
  Sk.f.Ug.call(this);
  this.ts = 0 < this.eb.fa().la ? this.eb.fa().la : 0;
  this.Xs()
};
z.zn = function() {
  this.Xs()
};
z.qe = function() {
  return new pp
};
function pp() {
  So.call(this)
}
A.e(pp, So);
z = pp.prototype;
z.ne = p;
z.Lc = p;
z.un = j;
z.BF = t("un");
z.wn = j;
z.CF = t("wn");
z.Fl = p;
z.KE = u("Fl");
z.Av = t("Fl");
z.p = function(a) {
  if(!this.Gb) {
    return this.o.BK(this.K.Ff()), pp.f.p.call(this, a), this.D
  }
};
z.ir = function() {
  this.Po();
  this.wn = q
};
z.Po = function() {
  this.wn && (pp.f.Po.call(this), this.Hy && !this.gf && (this.ne = new O, this.Lc = new O, this.o.eb.transform(this, this.o.ts, this.ne), this.o.eb.transform(this, this.o.ts, this.Lc)))
};
z.zm = function() {
  if(this.un) {
    this.Ua = [];
    var a, b;
    this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.gf == p && (this.Jc ? this.ne.y = b.y : this.ne.x = b.x), this.Ua.push(b)) : this.gf == p && (this.Jc ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
    this.Ua.push(this.Da);
    this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b), this.gf == p && (this.Jc ? this.Lc.y = b.y : this.Lc.x = b.x)) : this.gf == p && (this.Jc ? this.Lc.y = this.Da.y : this.Lc.x = this.Da.x)
  }
  this.un = q
};
z.jn = function(a) {
  var b = [], c = this.ne, d = this.Lc;
  b.push(new Fo(c.x, c.y));
  for(c = 0;c < this.Ua.length;c++) {
    var f = this.Ua[c];
    b.push(new Fo(f.x, f.y))
  }
  b.push(new Fo(d.x, d.y));
  d = new Fo(a.x, a.y);
  c = new Fo(a.x + a.width, a.y + a.height);
  if((d = Go.XD(b, d, c)) && d.Fa.length) {
    return j
  }
  b = To.XC(b);
  b = new P(b.x, b.y, b.width, b.height);
  return Xc(a, b) || Xc(b, a)
};
z.nx = function(a) {
  return hd(a)
};
z.GI = function() {
  for(var a = this.Ua.length, b = id(this.Ua[0].x, this.Ua[0].y), c = 0;c < a;c++) {
    b += id(this.Ua[c].x, this.Ua[c].y)
  }
  return b
};
z.mx = function(a) {
  return a.ja()
};
z.BI = function() {
  if(!this.Lc) {
    return"M 0 0"
  }
  var a = this.Ua[this.Ua.length - 1], b;
  this.Jc ? (b = S(this.Lc.x, a.y - 1), b += U(this.Lc.x + (a.x - this.Lc.x), a.y - 1), b += U(this.Lc.x + (a.x - this.Lc.x), a.y + 1), b += U(this.Lc.x, a.y + 1), b += U(this.Lc.x, a.y)) : (b = S(a.x - 1, a.y), b += U(a.x + 1, a.y), b += U(a.x + 1, a.y + (this.Lc.y - a.y)), b += U(a.x - 1, a.y + (this.Lc.y - a.y)), b += U(a.x - 1, a.y));
  return b
};
z.st = function(a) {
  return a.ja()
};
z.oD = function() {
  for(var a = this.Ua.length, b = S(this.Ua[0].x, this.Ua[0].y), c = 1;c < a;c++) {
    b += U(this.Ua[c].x, this.Ua[c].y)
  }
  if(this.gf) {
    for(var a = this.gf, c = "", d = a.Ua.length - 1;0 <= d;d--) {
      c += U(a.Ua[d].x, a.Ua[d].y)
    }
    a = c
  }else {
    a = "" + U(this.Lc.x, this.Lc.y), a += U(this.ne.x, this.ne.y)
  }
  return b + a + " Z"
};
function mp() {
  ko.apply(this)
}
A.e(mp, Sk);
mp.prototype.qe = function() {
  return new qp
};
function qp() {
  So.call(this)
}
A.e(qp, pp);
qp.prototype.zm = function() {
  this.Ua = [];
  var a, b;
  this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = this.Jc ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b), this.gf == p && (this.Jc ? this.ne.y = b.y : this.ne.x = b.x)) : this.gf == p && (this.Jc ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
  this.Ua.push(this.Da);
  this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = this.Jc ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b), b = this.Jc ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), this.gf == p && (this.Jc ? this.Lc.y = b.y : this.Lc.x = b.x)) : this.gf == p && (this.Jc ? this.Lc.y = this.Da.y : this.Lc.x = this.Da.x)
};
function np() {
  ko.apply(this)
}
A.e(np, Sk);
np.prototype.qe = function() {
  return new rp
};
function rp() {
  So.call(this)
}
A.e(rp, pp);
rp.prototype.zm = function() {
  this.Ua = [];
  var a, b;
  this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = this.Jc ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), this.gf == p && (this.Jc ? this.ne.y = b.y : this.ne.x = b.x), b = this.Jc ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b)) : this.gf == p && (this.Jc ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
  this.Ua.push(this.Da);
  this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = this.Jc ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b), this.gf == p && (this.Jc ? this.Lc.y = b.y : this.Lc.x = b.x)) : this.gf == p && (this.Jc ? this.Lc.y = this.Da.y : this.Lc.x = this.Da.x)
};
function sp() {
  po.apply(this);
  this.Fl = j;
  this.Ch = this.Lg = this.sE = q;
  this.jh = new O;
  this.Fg = new O;
  this.wn = this.un = j
}
A.e(sp, po);
z = sp.prototype;
z.Fl = p;
z.KE = u("Fl");
z.Av = t("Fl");
z.sE = p;
z.jh = p;
z.jn = function(a) {
  for(var b = [], c = this.Ne.concat(this.We), d = 0;d < c.length;d++) {
    var f = c[d];
    b.push(new Fo(f.x, f.y))
  }
  c = new Fo(a.x, a.y);
  d = new Fo(a.x + a.width, a.y + a.height);
  if((c = Go.XD(b, c, d)) && c.Fa.length) {
    return j
  }
  b = To.XC(b);
  b = new P(b.x, b.y, b.width, b.height);
  return Xc(a, b) || Xc(b, a)
};
z.Fg = p;
z.un = p;
z.BF = t("un");
z.wn = p;
z.CF = t("wn");
z.dJ = p;
z.Ne = p;
z.We = p;
z.Lg = p;
z.Ch = p;
z.ir = function() {
  if(this.wn) {
    var a = this.o;
    this.sE || (this.Lg = 0 < this.na && this.o.Ga(this.na - 1) && !this.o.Ga(this.na - 1).Gb, this.Ch = this.na < this.o.Fe() - 1 && this.o.Ga(this.na + 1) && !this.o.Ga(this.na + 1).Gb, this.dJ = this.Lg || this.Ch, this.sE = j);
    a.Vb.transform(this, this.Sd(), this.jh);
    a.eb.transform(this, this.Tb, this.jh);
    a.Vb.transform(this, this.Sd(), this.Fg);
    a.eb.transform(this, this.Lb, this.Fg);
    var a = Math.min(this.jh.x, this.Fg.x), b = Math.min(this.jh.y, this.Fg.y);
    this.k = new P(a, b, Math.max(this.jh.x, this.Fg.x) - a, Math.max(this.jh.y, this.Fg.y) - b)
  }
  this.wn = q
};
z.zm = function() {
  if(this.un) {
    this.Ne = [];
    this.We = [];
    var a = this.o, b, c;
    this.Lg && (b = a.Ga(this.na - 1).jh, c = new O, c.x = (b.x + this.jh.x) / 2, c.y = (b.y + this.jh.y) / 2, this.Ne.push(c));
    this.Ne.push(this.jh);
    this.Ch && (a.Ga(this.na + 1).ir(), a.Ga(this.na + 1).Av(q), b = a.Ga(this.na + 1).jh, c = new O, c.x = (b.x + this.jh.x) / 2, c.y = (b.y + this.jh.y) / 2, this.Ne.push(c), b = a.Ga(this.na + 1).Fg, c = new O, c.x = (b.x + this.Fg.x) / 2, c.y = (b.y + this.Fg.y) / 2, this.We.push(c));
    this.We.push(this.Fg);
    this.Lg && (b = a.Ga(this.na - 1).Fg, c = new O, c.x = (b.x + this.Fg.x) / 2, c.y = (b.y + this.Fg.y) / 2, this.We.push(c))
  }
  this.un = q
};
z.st = function(a) {
  return a.ja()
};
z.oD = function() {
  var a, b = S(this.Ne[0].x, this.Ne[0].y);
  for(a = 1;a < this.Ne.length;a++) {
    b += U(this.Ne[a].x, this.Ne[a].y)
  }
  for(a = 0;a < this.We.length;a++) {
    b += U(this.We[a].x, this.We[a].y)
  }
  return b
};
z.nx = function(a) {
  return hd(a)
};
z.GI = function() {
  for(var a = id(this.We[0].x, this.We[0].y), b = 1;b < this.We.length;b++) {
    a += id(this.We[b].x, this.We[b].y)
  }
  return a
};
z.VB = function(a) {
  return hd(a)
};
z.mx = function(a) {
  return a.ja()
};
z.BI = function() {
  var a = this.Ne[this.Ne.length - 1], b = this.We[0], c;
  this.Jc ? (c = S(a.x, a.y), c += U(a.x, a.y + 0.1), c += U(b.x, a.y + 0.1), c += U(b.x, b.y - 0.1)) : (c = S(a.x, a.y), c += U(a.x + 0.1, a.y), c += U(a.x + 0.1, b.y), c += U(b.x - 0.1, b.y));
  return c
};
function tp() {
  qo.apply(this);
  this.LE = j
}
A.e(tp, qo);
z = tp.prototype;
z.LE = p;
z.BK = t("LE");
z.D = p;
z.fb = u("D");
z.tn = x(j);
z.en = x(q);
z.qe = function() {
  return new sp
};
z.Xs = function() {
  if(this.LE) {
    for(var a = new O(Number.MAX_VALUE, Number.MAX_VALUE), b = new O(-Number.MAX_VALUE, -Number.MAX_VALUE), c = 0;c < this.ba.length;c++) {
      var d = this.ba[c];
      if(!d.Gb) {
        d.BF(j);
        d.CF(j);
        d.KE() && d.ir();
        d.dJ && d.zm();
        for(var f = 0;f < d.Ne.length;f++) {
          a.x = Math.min(d.Ne[f].x, a.x), a.y = Math.min(d.Ne[f].y, a.y), b.x = Math.max(d.Ne[f].x, b.x), b.y = Math.max(d.Ne[f].y, b.y)
        }
        for(f = 0;f < d.We.length;f++) {
          a.x = Math.min(d.We[f].x, a.x), a.y = Math.min(d.We[f].y, a.y), b.x = Math.max(d.We[f].x, b.x), b.y = Math.max(d.We[f].y, b.y)
        }
      }
    }
  }
};
z.Ug = function() {
  tp.f.Ug.call(this);
  this.Xs()
};
z.zn = function() {
  this.Xs()
};
function up() {
  ro.apply(this)
}
A.e(up, ro);
up.prototype.uh = function(a) {
  var b;
  switch(a) {
    default:
    ;
    case 10:
      b = new tp;
      break;
    case 11:
      b = new tp
  }
  b.type = a;
  b.EL = 10;
  return b
};
up.prototype.Qb = function() {
  return new kp
};
up.prototype.Ah = x("range_area_series");
up.prototype.Rb = x("range_area_style");
function vp() {
}
A.e(vp, Me);
z = vp.prototype;
z.FH = p;
z.uI = p;
z.VK = p;
z.p = function(a, b, c, d, f, g) {
  vp.f.p.call(this, a, b, c);
  this.FH = d;
  this.uI = f;
  this.VK = g
};
z.vf = function(a) {
  var b = "", c = this.j.r();
  this.FH.call(this.$a, a) ? (a = this.uI.call(this.$a, a), b += Ld(a, c, this.j.n(), this.j.Ba().Ba()), b += md()) : b += od();
  this.G.setAttribute("style", b)
};
z.Ta = function() {
  this.VK.call(this.$a, this.G)
};
z.ud = function(a) {
  this.$a.ud(this.G, a)
};
function wp() {
}
A.e(wp, Me);
wp.prototype.vf = function(a) {
  var b = this.j.Dh;
  if(a.xc(b)) {
    var c = this.j.r(), d = "", f = this.j.Ba().Ba(), g = this.j.n(), a = a.YC(b), d = a.Ie() ? d + Jd(a.mc(), c, g, f) : d + md(), d = a.Ld() ? d + Ld(a.Mc(), c, g, f) : d + nd()
  }else {
    d += od()
  }
  this.G.setAttribute("style", d)
};
wp.prototype.Ta = function() {
  xp(this.$a, this.G)
};
wp.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function yp() {
}
A.e(yp, Me);
yp.prototype.vf = function(a) {
  var b = this.j.Dh, c;
  zp(a, b) && (a = a.YC(b), c = a.df() ? Hc(a.De(), this.j.r(), this.j.Ze, this.j.Ba().Ba()) : od());
  this.G.setAttribute("style", c)
};
yp.prototype.Ta = function() {
  xp(this.$a, this.G)
};
yp.prototype.ud = function(a) {
  this.$a.ud(this.G, a)
};
function Ap(a, b) {
  Re.call(this, a, b)
}
A.e(Ap, Re);
z = Ap.prototype;
z.Mv = p;
z.ar = u("Mv");
z.sz = function() {
  return this.Mv && this.Mv.isEnabled()
};
z.An = p;
z.Yq = u("An");
z.rz = function() {
  return this.An && this.An.isEnabled()
};
z.tm = p;
z.Tq = u("tm");
z.pz = function() {
  return this.tm && this.tm.isEnabled()
};
z.g = function(a) {
  a = Ap.f.g.call(this, a);
  if(Tb(F(a, "line"))) {
    var b = F(a, "line");
    this.Mv = new Kd;
    this.An = new Kd;
    this.tm = new Kd;
    this.Mv.g(b);
    this.An.g(b);
    this.tm.g(b)
  }
  Tb(F(a, "open_line")) && (this.An || (this.An = new Kd), this.An.g(F(a, "open_line")));
  Tb(F(a, "close_line")) && (this.tm || (this.tm = new Kd), this.tm.g(F(a, "close_line")))
};
function Bp(a, b) {
  Re.call(this, a, b);
  this.Nf = new Ap;
  this.Cf = new Ap
}
A.e(Bp, Se);
z = Bp.prototype;
z.Nf = p;
z.Cf = p;
function Cp(a, b) {
  return b ? a.Nf.sz() : a.Cf.sz()
}
function Dp(a, b) {
  return b ? a.Nf.rz() : a.Cf.rz()
}
function Ep(a, b) {
  return b ? a.Nf.pz() : a.Cf.pz()
}
z.ar = function(a) {
  return a ? this.Nf.ar() : this.Cf.ar()
};
z.Yq = function(a) {
  return a ? this.Nf.Yq() : this.Cf.Yq()
};
z.Tq = function(a) {
  return a ? this.Nf.Tq() : this.Cf.Tq()
};
z.g = function(a) {
  if(!a) {
    return a
  }
  var b = $b(a, F(a, "up"), "up"), c = $b(a, F(a, "down"), "down");
  this.Nf.g(b);
  this.Cf.g(c);
  return a
};
function Fp() {
}
A.e(Fp, Qe);
z = Fp.prototype;
z.es = p;
z.Lr = p;
z.xq = p;
z.Yc = function() {
  var a = this.j.Dh, b = this.j.ka();
  if(Cp(b.wa, a) || Cp(b.dc, a) || Cp(b.kc, a) || Cp(b.Yb, a) || Cp(b.gc, a) || Cp(b.nc, a)) {
    this.es = new vp, this.es.p(this.j, this, this.KL, this.sz, this.ar, this.mO)
  }
  if(Dp(b.wa, a) || Dp(b.dc, a) || Dp(b.kc, a) || Dp(b.Yb, a) || Dp(b.gc, a) || Dp(b.nc, a)) {
    this.Lr = new vp, this.Lr.p(this.j, this, this.IL, this.rz, this.Yq, this.lO)
  }
  if(Ep(b.wa, a) || Ep(b.dc, a) || Ep(b.kc, a) || Ep(b.Yb, a) || Ep(b.gc, a) || Ep(b.nc, a)) {
    this.xq = new vp, this.xq.p(this.j, this, this.GL, this.pz, this.Tq, this.jO)
  }
};
z.ff = function() {
  var a = this.j.fb();
  this.es && a.appendChild(this.es.G);
  this.Lr && a.appendChild(this.Lr.G);
  this.xq && a.appendChild(this.xq.G)
};
z.update = function(a, b) {
  b && this.j.lq();
  this.es && this.es.update(a, b);
  this.Lr && this.Lr.update(a, b);
  this.xq && this.xq.update(a, b)
};
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
z.KL = function() {
  return hd(this.j.r())
};
z.mO = function(a) {
  var b = this.j, c = id(b.qg.x, b.qg.y, b.Ij), c = c + id(b.sg.x, b.sg.y, b.Ij);
  a.setAttribute("points", c)
};
z.sz = function(a) {
  return Cp(a, this.j.Dh)
};
z.ar = function(a) {
  return a.ar(this.j.Dh)
};
z.IL = function() {
  return hd(this.j.r())
};
z.lO = function(a) {
  var b = this.j, c = id(b.ve.x, b.ve.y, b.Ij), c = c + id(b.$k.x, b.$k.y, b.Ij);
  a.setAttribute("points", c)
};
z.rz = function(a) {
  return Dp(a, this.j.Dh)
};
z.Yq = function(a) {
  return a.Yq(this.j.Dh)
};
z.GL = function() {
  return hd(this.j.r())
};
z.jO = function(a) {
  var b = this.j, c = id(b.pe.x, b.pe.y, b.Ij), c = c + id(b.Yk.x, b.Yk.y, b.Ij);
  a.setAttribute("points", c)
};
z.pz = function(a) {
  return Ep(a, this.j.Dh)
};
z.Tq = function(a) {
  return a.Tq(this.j.Dh)
};
z.YC = function(a) {
  return a ? this.Nf : this.Cf
};
function Gp() {
  $.call(this)
}
A.e(Gp, $);
Gp.prototype.lc = function() {
  return new Fp
};
Gp.prototype.cc = function() {
  return Bp
};
function Hp() {
  Re.call(this)
}
A.e(Hp, Re);
z = Hp.prototype;
z.Hb = p;
z.mc = u("Hb");
z.Ie = function() {
  return this.Hb && this.Hb.isEnabled()
};
z.sc = p;
z.Mc = u("sc");
z.Ld = function() {
  return this.sc && this.sc.isEnabled()
};
z.Pc = p;
z.De = u("Pc");
z.df = function() {
  return this.Pc && this.Pc.isEnabled()
};
z.ee = p;
z.Ye = u("ee");
z.qz = function() {
  return this.ee && this.ee.isEnabled()
};
z.g = function(a) {
  if(!a) {
    return a
  }
  a = Hp.f.g.call(this, a);
  if(C(a, "hatch_type")) {
    var b = F(a, "hatch_type");
    if(C(a, "hatch_fill")) {
      var c = F(a, "hatch_fill");
      C(c, "type") && (c.type = J(c, "type").replace("%hatchtype", b))
    }
  }
  Tb(F(a, "fill")) && (this.Hb = new Id, this.Hb.g(F(a, "fill")));
  Tb(F(a, "border")) && (this.sc = new Kd, this.sc.g(F(a, "border")));
  Tb(F(a, "hatch_fill")) && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  Tb(F(a, "line")) && (this.ee = new Kd, this.ee.g(F(a, "line")));
  return a
};
function Ip(a, b) {
  Re.call(this, a, b);
  this.Nf = new Hp;
  this.Cf = new Hp
}
A.e(Ip, Se);
z = Ip.prototype;
z.Nf = p;
z.Cf = p;
z.xc = function(a) {
  a = a ? this.Nf : this.Cf;
  return a.Ie() || a.Ld()
};
function zp(a, b) {
  return b ? a.Nf.df() : a.Cf.df()
}
function Jp(a, b) {
  return b ? a.Nf.qz() : a.Cf.qz()
}
z.Ye = function(a) {
  return a ? this.Nf.Ye() : this.Cf.Ye()
};
z.YC = function(a) {
  return a ? this.Nf : this.Cf
};
z.g = function(a) {
  if(!a) {
    return a
  }
  var b = $b(a, F(a, "up"), "up"), c = $b(a, F(a, "down"), "down");
  this.Nf.g(b);
  this.Cf.g(c);
  return a
};
function Kp() {
}
A.e(Kp, Qe);
z = Kp.prototype;
z.Db = p;
z.Fb = p;
z.Hf = p;
z.Yc = function() {
  var a = this.j.ka(), b = this.j.Dh;
  a.Zu(b) && (this.Db = new wp, this.Db.p(this.j, this, this.pH));
  a.wi(b) && (this.Fb = new yp, this.Fb.p(this.j, this, this.pH));
  if(Jp(a.wa, b) || Jp(a.dc, b) || Jp(a.kc, b) || Jp(a.Yb, b) || Jp(a.gc, b) || Jp(a.nc, b)) {
    this.Hf = new vp, this.Hf.p(this.j, this, this.HL, this.qz, this.Ye, this.kO)
  }
};
z.ff = function() {
  var a = this.j.fb();
  this.Hf && a.appendChild(this.Hf.G);
  this.Db && a.appendChild(this.Db.G);
  this.Fb && a.appendChild(this.Fb.G)
};
z.update = function(a, b) {
  b && this.j.lq();
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b);
  this.Hf && this.Hf.update(a, b)
};
z.ud = function(a, b) {
  b.qp() ? a.setAttribute("filter", Tc(this.j.r().hk, b.ll())) : a.removeAttribute("filter")
};
z.pH = function() {
  return Mc(this.j.r())
};
function xp(a, b) {
  var c = new P(a.j.Jh.x, a.j.Jh.y, a.j.Jh.width, a.j.Jh.height);
  b.setAttribute("x", c.x);
  b.setAttribute("y", c.y);
  b.setAttribute("width", c.width);
  b.setAttribute("height", c.height)
}
z.HL = function() {
  return hd(this.j.r())
};
z.kO = function(a) {
  var b = this.j, c = id(b.qg.x, b.qg.y, b.Ij), c = c + id(b.sg.x, b.sg.y, b.Ij);
  a.setAttribute("points", c)
};
z.qz = function(a) {
  return Jp(a, this.j.Dh)
};
z.Ye = function(a) {
  return a.Ye(this.j.Dh)
};
function Lp() {
  $.call(this)
}
A.e(Lp, $);
Lp.prototype.cc = function() {
  return Ip
};
Lp.prototype.lc = function() {
  return new Kp
};
Lp.prototype.Zu = function(a) {
  return this.wa.xc(a) || this.dc.xc(a) || this.kc.xc(a) || this.Yb.xc(a) || this.gc.xc(a) || this.nc.xc(a)
};
Lp.prototype.wi = function(a) {
  return zp(this.wa, a) || zp(this.dc, a) || zp(this.kc, a) || zp(this.Yb, a) || zp(this.gc, a) || zp(this.nc, a)
};
function Mp() {
  io.apply(this)
}
A.e(Mp, io);
z = Mp.prototype;
z.oj = p;
z.CM = u("oj");
z.RN = t("oj");
z.Vi = p;
z.rM = u("Vi");
z.ON = t("Vi");
z.rl = p;
z.yM = u("rl");
z.PN = t("rl");
z.mn = p;
z.AM = u("mn");
z.QN = t("mn");
z.Dh = p;
z.qg = p;
z.sg = p;
z.dl = function(a) {
  this.rl = Np(this, a, "high", "h");
  this.mn = Np(this, a, "low", "l");
  this.oj = Np(this, a, "open", "o");
  this.Vi = Np(this, a, "close", "c");
  this.Gb = isNaN(this.rl) || isNaN(this.mn) || isNaN(this.oj) || isNaN(this.Vi)
};
z.$h = function() {
  if(!this.Gb) {
    this.Dh = this.Vi > this.oj;
    var a = this.o.eb;
    a.Gc(this.rl);
    a.Gc(this.mn);
    a.Gc(this.oj);
    a.Gc(this.Vi)
  }
};
function Np(a, b, c, d) {
  return C(b, c) ? jo(a, F(b, c)) : C(b, d) ? jo(a, F(b, d)) : NaN
}
z.lq = function() {
  this.qg = new O;
  this.sg = new O;
  var a = this.o;
  a.eb.transform(this, this.rl, this.qg);
  a.eb.transform(this, this.mn, this.sg);
  a.Vb.transform(this, this.Sd(), this.qg);
  a.Vb.transform(this, this.Sd(), this.sg)
};
z.Ka = function() {
  return this.zf.nt / 2
};
z.p = function(a) {
  Mp.f.p.call(this, a);
  return this.D
};
z.ci = function() {
  Mp.f.ci.call(this);
  A.xa()
};
z.Cb = function() {
  Mp.f.Cb.call(this);
  this.b.add("%High", 0);
  this.b.add("%Hight", 0);
  this.b.add("%Low", 0);
  this.b.add("%Open", 0);
  this.b.add("%Close", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%High":
    ;
    case "%Hight":
    ;
    case "%Low":
    ;
    case "%Open":
    ;
    case "%Close":
      return 2
  }
  return Mp.f.Pa.call(this, a)
};
z.Se = function() {
  Mp.f.Se.call(this);
  this.b.add("%High", this.rl);
  this.b.add("%Hight", this.rl);
  this.b.add("%Low", this.mn);
  this.b.add("%Open", this.oj);
  this.b.add("%Close", this.Vi)
};
z.mb = function(a) {
  a = Mp.f.mb.call(this, a);
  a.High = this.rl;
  a.Low = this.mn;
  a.Open = this.oj;
  a.Close = this.Vi;
  return a
};
function Op() {
  ko.apply(this)
}
A.e(Op, ko);
Op.prototype.en = x(j);
Op.prototype.eH = x(j);
Op.prototype.tn = x(j);
function Pp() {
  ho.apply(this)
}
A.e(Pp, ho);
z = Pp.prototype;
z.lu = p;
z.Ku = p;
z.cv = p;
z.lt = p;
z.ND = function() {
  this.lu = new Kn;
  this.Ku = new Kn;
  this.cv = new Kn;
  this.lt = new Kn;
  this.lu.p(Mp.prototype.yM, Mp.prototype.PN);
  this.Ku.p(Mp.prototype.AM, Mp.prototype.QN);
  this.cv.p(Mp.prototype.CM, Mp.prototype.RN);
  this.lt.p(Mp.prototype.rM, Mp.prototype.ON)
};
z.cH = function(a) {
  Ln(this.lu, a, a.na);
  Ln(this.Ku, a, a.na);
  Ln(this.cv, a, a.na);
  Ln(this.lt, a, a.na)
};
z.WI = function(a) {
  this.lu.lr(a.ba);
  this.Ku.lr(a.ba);
  this.cv.lr(a.ba);
  this.lt.lr(a.ba);
  this.lu.clear();
  this.Ku.clear();
  this.cv.clear();
  this.lt.clear()
};
function Qp() {
  Mp.apply(this);
  this.Ij = j
}
A.e(Qp, Mp);
z = Qp.prototype;
z.ve = p;
z.pe = p;
z.$k = p;
z.Yk = p;
z.Ij = p;
z.p = function(a) {
  Qp.f.p.call(this, a);
  return this.D
};
z.lq = function() {
  Qp.f.lq.call(this);
  this.ve = new O;
  this.pe = new O;
  this.$k = new O;
  this.Yk = new O;
  var a = this.Ka(), b = this.o;
  b.eb.transform(this, this.oj, this.ve);
  b.eb.transform(this, this.oj, this.$k);
  b.eb.transform(this, this.Vi, this.pe);
  b.eb.transform(this, this.Vi, this.Yk);
  b.Vb.transform(this, this.Ub, this.ve, -a);
  b.Vb.transform(this, this.Ub, this.$k);
  b.Vb.transform(this, this.Ub, this.pe, a);
  b.Vb.transform(this, this.Ub, this.Yk);
  a = Math.min(this.ve.x, this.pe.x, this.qg.x, this.sg.x, this.$k.x, this.Yk.x);
  b = Math.min(this.ve.y, this.pe.y, this.qg.y, this.sg.y, this.$k.y, this.Yk.y);
  this.k = new P(a, b, Math.max(this.ve.x, this.pe.x, this.qg.x, this.sg.x, this.$k.x, this.Yk.x) - a, Math.max(this.ve.y, this.pe.y, this.qg.y, this.sg.y, this.$k.y, this.Yk.y) - b)
};
function Rp() {
  Op.apply(this)
}
A.e(Rp, Op);
Rp.prototype.qe = function() {
  return new Qp
};
function Sp() {
  Pp.apply(this)
}
A.e(Sp, Pp);
Sp.prototype.uh = function(a) {
  var b = new Rp;
  b.gh(a);
  b.Xj = a;
  return b
};
Sp.prototype.Ah = x("ohlc_series");
Sp.prototype.Rb = x("ohlc_style");
Sp.prototype.Qb = function() {
  return new Gp
};
function Tp() {
  Mp.apply(this);
  this.Ij = j
}
A.e(Tp, Mp);
z = Tp.prototype;
z.ve = p;
z.pe = p;
z.Jh = p;
z.Ij = p;
z.Hg = p;
z.Mg = p;
z.lq = function() {
  Tp.f.lq.call(this);
  this.Jh = new P;
  this.ve = new O;
  this.pe = new O;
  var a = this.o, b = this.Ka();
  a.eb.transform(this, this.oj, this.ve);
  a.eb.transform(this, this.Vi, this.pe);
  a.Vb.transform(this, this.Sd(), this.ve, -b);
  a.Vb.transform(this, this.Sd(), this.pe, b);
  this.Jh.x = Math.min(this.ve.x, this.pe.x);
  this.Jh.y = Math.min(this.ve.y, this.pe.y);
  this.Jh.width = Math.max(this.ve.x, this.pe.x) - this.Jh.x;
  this.Jh.height = Math.max(this.ve.y, this.pe.y) - this.Jh.y;
  0 == this.Jh.height && (this.Jh.height = 1);
  a = Math.min(this.ve.x, this.pe.x, this.qg.x, this.sg.x);
  b = Math.min(this.ve.y, this.pe.y, this.qg.y, this.sg.y);
  this.k = new P(a, b, Math.max(this.ve.x, this.pe.x, this.qg.x, this.sg.x) - a, Math.max(this.ve.y, this.pe.y, this.qg.y, this.sg.y) - b)
};
function Up() {
  Op.apply(this)
}
A.e(Up, Op);
Up.prototype.qe = function() {
  return new Tp
};
function Vp() {
  Pp.apply(this)
}
A.e(Vp, Pp);
Vp.prototype.uh = function(a) {
  var b = new Up;
  b.gh(a);
  b.Xj = a;
  return b
};
Vp.prototype.Ah = x("candlestick_series");
Vp.prototype.Rb = x("candlestick_style");
Vp.prototype.Qb = function() {
  return new Lp
};
function Wp() {
  Sn.apply(this)
}
A.e(Wp, Sn);
Wp.prototype.Ue = function(a, b) {
  var c = this.Rb();
  if(C(a, "style") || C(a, c)) {
    if(C(a, "style") && !C(a, c)) {
      Gf(this, b, F(a, "style"))
    }else {
      var d, f;
      C(a, "style") && (d = F(a, "style"));
      C(a, c) && (f = F(a, c), C(f, "parent") && (d = F(f, "parent")));
      c = Ue(b, c, f, d);
      H(c, "name", d);
      this.K = new Zf;
      this.K.g(c)
    }
  }else {
    this.K = new Zf, d = I(b.Ki, c), this.K.g(fc(d, "anychart_default")), d = this.K, b.Ti[c] || (b.Ti[c] = {}), b.Ti[c].anychart_default = d
  }
};
function Xp() {
  no.apply(this)
}
A.e(Xp, no);
z = Xp.prototype;
z.Or = p;
z.fk = function(a, b) {
  if(C(a, "style")) {
    if(C(a, "marker")) {
      H(a, "marker", F(a, "style"))
    }else {
      var c = {};
      H(c, "style", F(a, "style"));
      H(c, "#name#", "marker");
      H(a, "marker", c)
    }
  }
  this.gb.om(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Yi(F(a, "marker"), b), this.K = this.gb.ka());
  this.gb.zg(j)
};
z.p = function(a) {
  Xp.f.p.call(this, a);
  return this.D
};
z.RI = s();
function Yp(a) {
  a.Or = new O;
  a.o.Vb.transform(a, a.Ub, a.Or);
  a.o.eb.transform(a, a.$b, a.Or)
}
z.update = function() {
  Yp(this);
  this.Sn()
};
z.Wa = function() {
  Yp(this);
  this.Lz()
};
z.bg = function(a) {
  a.x = this.Or.x;
  a.y = this.Or.y
};
z.jn = function(a) {
  var b = this.Or;
  return Xc(a, new P(b.x, b.y, 1, 1))
};
function Zp() {
  lo.apply(this)
}
A.e(Zp, lo);
Zp.prototype.en = x(q);
Zp.prototype.qe = function() {
  return new Xp
};
Zp.prototype.fk = function(a, b) {
  if(C(a, "style")) {
    if(C(a, "marker")) {
      H(a, "marker", F(a, "style"))
    }else {
      var c = {};
      H(c, "style", F(a, "style"));
      H(c, "#name#", "marker");
      H(a, "marker", c)
    }
  }
  this.gb.sh(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Zi(F(a, "marker"), b), this.K = this.gb.ka());
  this.gb.zg(j)
};
function $p() {
  ho.apply(this)
}
A.e($p, ho);
z = $p.prototype;
z.fk = function(a, b) {
  this.gb || (this.gb = this.Am());
  a ? this.gb.Ue(a, b) : this.gb.rf(new $);
  this.gb.zg(j);
  this.K = this.gb.ka()
};
z.uh = function() {
  var a = new Zp;
  a.gh(18);
  return a
};
z.Ah = x("marker_series");
z.Rb = x(p);
z.cc = x(p);
z.Am = function() {
  return new Wp
};
function aq() {
}
A.e(aq, Mf);
aq.prototype.Ta = function() {
  this.$a.Ni(this.G)
};
function bq() {
}
A.e(bq, Nf);
bq.prototype.Ta = function() {
  this.$a.Ni(this.G)
};
function cq() {
}
A.e(cq, Of);
cq.prototype.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new aq, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new bq, this.Fb.p(this.j, this))
};
cq.prototype.update = function(a, b) {
  b && this.j.hd();
  cq.f.update.call(this, a, b)
};
cq.prototype.createElement = function() {
  return this.j.r().ja()
};
cq.prototype.Ni = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function dq() {
  $.call(this)
}
A.e(dq, Qf);
dq.prototype.lc = function() {
  return new cq
};
function eq() {
  io.apply(this);
  this.P = new O;
  this.X = new O
}
A.e(eq, mo);
z = eq.prototype;
z.P = p;
z.X = p;
z.pF = p;
z.GH = function(a, b) {
  var c = this.o.Vb, d = this.o.eb, f = J(a, "column"), g = J(a, "row");
  this.Jc = 0 === c.Ib() || 1 === c.Ib();
  this.Dc = c.Df(f, b);
  this.nw = d.Df(g, b);
  this.fH = this.Dc.na;
  this.pF = this.nw.na;
  this.zf = this.o.zf;
  this.Nr = this.Dc.na;
  this.Gb || this.Dc.Ed(this)
};
z.dl = function(a) {
  C(a, "y") && (this.Gb = isNaN(jo(this, F(a, "y"))));
  this.Gb = this.Gb || !C(a, "column") || !C(a, "row")
};
z.$h = function(a) {
  C(a, "y") && (this.ma = M(a, "y"))
};
z.p = function(a) {
  return eq.f.p.call(this, a)
};
z.hd = function() {
  var a = this.o.Vb, b = this.o.eb;
  a.transform(this, this.fH - 0.5, this.P);
  a.transform(this, this.fH + 0.5, this.X);
  b.transform(this, this.pF - 0.5, this.P);
  b.transform(this, this.pF + 0.5, this.X);
  a = Math.min(this.P.x, this.X.x);
  b = Math.min(this.P.y, this.X.y);
  this.k = new P(a, b, Math.max(this.P.x, this.X.x) - a, Math.max(this.P.y, this.X.y) - b)
};
z.Cb = function() {
  this.b.add("%Column", 0);
  this.b.add("%Row", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Column":
    ;
    case "%Row":
      return 2
  }
  return eq.f.Pa.call(this, a)
};
z.Se = function() {
  this.b.add("%Column", this.Dc.getName());
  this.b.add("%Row", this.nw.getName());
  eq.f.Se.call(this)
};
function fq() {
  ko.apply(this)
}
A.e(fq, lo);
fq.prototype.qe = function() {
  return new eq
};
fq.prototype.en = x(q);
function gq() {
  fo.apply(this)
}
A.e(gq, ho);
gq.prototype.uh = function() {
  var a = new fq;
  a.gh(23);
  a.Xj = 23;
  return a
};
gq.prototype.Ah = x("heat_map");
gq.prototype.Rb = x("heat_map_style");
gq.prototype.Qb = function() {
  return new dq
};
function hq() {
}
A.e(hq, Po);
hq.prototype.Ta = function() {
  this.j.Uj();
  var a = this.j.Ua;
  if(a && !(2 > a.length)) {
    for(var b = this.j.o.VF, c = 0;c < a.length;c++) {
      var d = b, f = a[c].x, g = a[c].y;
      switch(d.Nv) {
        case 3:
          if(d.Vn == f && d.Wn == g) {
            break
          }
          var k = d, l = d.Zp, n = d.Vn, m = d.Wn, o = f, v = new O;
          v.x = l + (n - l) * k.DA;
          v.y = m;
          l = new O;
          l.x = o - (o - n) * k.DA;
          l.y = m;
          o = new O;
          o.x = n;
          o.y = m;
          k.o.da.push([v, l, o]);
          k.na++;
          d.Zp = d.Vn;
          d.aB = d.Wn;
          d.Vn = f;
          d.Wn = g;
          break;
        case 2:
          if(d.Vn == f && d.Wn == g) {
            break
          }
          d.Zp = d.Vn;
          d.aB = d.Wn;
          d.Vn = f;
          d.Wn = g;
          d.Nv = 3;
          break;
        case 1:
          if(d.Zp == f && d.aB == g) {
            break
          }
          k = d;
          n = d.aB;
          m = new O;
          m.x = f - (f - d.Zp) * k.DA;
          m.y = n;
          k.o.da.push(m);
          k.na++;
          d.Vn = f;
          d.Wn = g;
          d.Nv = 2;
          break;
        case 0:
          d.Zp = f, d.aB = g, d.Nv = 1
      }
    }
    this.j.o.Fe() - 1 == this.j.na && (a = new O, a.x = b.Zp + (b.Vn - b.Zp) * b.DA, a.y = b.Wn, b.o.da.push(a));
    this.G.setAttribute("d", this.$a.yh())
  }
};
function iq() {
}
A.e(iq, Qo);
iq.prototype.Yc = function() {
  this.Hf = new hq;
  this.Hf.p(this.j, this)
};
iq.prototype.createElement = function() {
  return this.j.r().ja()
};
iq.prototype.yh = function() {
  var a = this.j.r(), b = this.j.Ua;
  if(b) {
    var c = b.length;
    if(!(2 > c)) {
      var d = this.j.o.da, f, g, k;
      k = this.j.na;
      var l = function(a) {
        if(a.ap) {
          return q
        }
        var b = a.o.ba[k + 1];
        return b && b.ap || 0 < k && (a = a.o.ba[k - 1]) && a.ap ? q : j
      };
      2 == c ? (k == this.j.o.Fe() - 1 ? (f = new O, f.x = d[2 * k - 1].x, f.y = d[2 * k - 1].y, c = S(b[0].x, b[0].y)) : (c = S(b[0].x, b[0].y), f = new O, f.x = d[k].x, f.y = d[k].y), l(this.j) && (c += a.XE(f.x, f.y, b[1].x, b[1].y))) : (f = d[2 * k - 1][0], g = d[2 * k - 1][1], d = d[2 * k - 1][2], c = S(b[0].x, b[0].y), l(this.j) && (c += a.XE(f.x, f.y, d.x, d.y), c += a.XE(g.x, g.y, b[2].x, b[2].y)));
      return c
    }
  }
};
function jq() {
  $.call(this)
}
A.e(jq, Oo);
jq.prototype.lc = function() {
  return new iq
};
function kq() {
  ko.apply(this);
  this.da = [];
  this.VF = new lq(this)
}
A.e(kq, Uo);
kq.prototype.da = p;
kq.prototype.VF = p;
kq.prototype.Ug = s();
kq.prototype.zn = function() {
  this.da = [];
  var a = this.VF;
  a.Nv = 0;
  a.na = 0
};
function lq(a) {
  this.Nv = 0;
  this.DA = 0.5;
  this.na = 0;
  this.o = a
}
function mq() {
  fo.apply(this)
}
A.e(mq, Vo);
mq.prototype.uh = function(a) {
  var b;
  switch(a) {
    case 3:
      b = new kq
  }
  b.gh(a);
  b.Xj = 3;
  return b
};
mq.prototype.Qb = function() {
  return new jq
};
function qf() {
  Ek.call(this);
  this.ck = 0;
  this.me = [];
  this.Qf = [];
  this.Ss = {};
  this.fg = [];
  this.iw = {};
  this.$K = [];
  this.aL = [];
  this.Bq = [];
  this.rx = {}
}
A.e(qf, Ek);
z = qf.prototype;
z.Xr = function(a) {
  qf.f.Xr.call(this, a);
  "heatmap" == a && (this.ck = 23);
  this.Jc = -1 != a.indexOf("horizontal");
  this.My = "scatter" == a
};
z.My = p;
z.eo = p;
z.CB = p;
z.Vt = u("CB");
z.Jc = p;
z.Vf = u("Jc");
z.zD = q;
z.DD = q;
z.ED = q;
z.xD = q;
z.me = p;
z.Qf = p;
z.fg = p;
z.Ss = p;
z.iw = p;
z.dv = p;
z.ev = p;
z.Gw = p;
z.Fw = p;
z.Gd = function() {
  return this.N.Gd()
};
z.$K = p;
z.aL = p;
z.Bq = p;
z.rx = p;
z.br = function() {
  return new jl
};
z.dy = function(a) {
  switch(a) {
    case 2:
    ;
    case 5:
    ;
    case 4:
      return 2;
    case 6:
    ;
    case 7:
    ;
    case 9:
    ;
    case 8:
      return 6;
    default:
      return a
  }
};
z.kx = function(a) {
  switch(a) {
    case 0:
      return new vo;
    case 1:
      return new yo;
    case 12:
      return new Jo;
    case 3:
      return new mq;
    case 2:
    ;
    case 5:
    ;
    case 4:
      return new Vo;
    case 6:
    ;
    case 7:
    ;
    case 9:
    ;
    case 8:
      return new lp;
    case 10:
    ;
    case 11:
      return new up;
    case 13:
      return new Sp;
    case 14:
      return new Vp;
    case 18:
      return new $p;
    case 23:
      return new gq
  }
  return p
};
z.Jo = function(a) {
  if("heatmap" == this.wp) {
    return 23
  }
  if(a == p) {
    return this.ck
  }
  switch(a) {
    case "bubble":
      return 12;
    case "bar":
      return 0;
    case "rangebar":
      return 1;
    case "rangearea":
      return 10;
    case "candlestick":
      return 14;
    case "ohlc":
      return 13;
    case "line":
      return 2;
    case "steplineforward":
      return 4;
    case "steplinebackward":
      return 5;
    case "spline":
      return 3;
    case "area":
      return 6;
    case "splinearea":
      return 7;
    case "stepareaforward":
    ;
    case "steplineforwardarea":
      return 8;
    case "stepareabackward":
    ;
    case "steplinebackwardarea":
      return 9;
    case "rangesplinearea":
      return 11;
    case "marker":
      return 18;
    default:
      return this.ck
  }
};
z.gC = function(a, b) {
  qf.f.gC.call(this, a, b);
  this.xD = this.ED = this.DD = this.zD = q;
  var c = F(a, "axes");
  nq(this, F(c, "x_axis"), j, b, "x_axis");
  nq(this, F(c, "y_axis"), j, b, "y_axis");
  if(C(c, "extra")) {
    var d = F(c, "extra");
    if(C(d, "x_axis")) {
      for(var f = I(d, "x_axis"), c = 0;c < f.length;c++) {
        nq(this, f[c], q, b, "x_axis")
      }
    }
    if(C(d, "y_axis")) {
      d = I(d, "y_axis");
      for(c = 0;c < d.length;c++) {
        nq(this, d[c], q, b, "y_axis")
      }
    }
  }
};
function nq(a, b, c, d, f) {
  if(!(!c && !C(b, "name") || "x_axis" != f && "y_axis" != f)) {
    var g = C(b, "scale") && "linear" == N(F(b, "scale"), "type"), k = "heatmap" == a.wp, l = "categorizedbyserieshorizontal" == a.wp || "categorizedbyseriesvertical" == a.wp, n = "categorizedhorizontal" == a.wp || "categorizedbyserieshorizontal" == a.wp, f = "x_axis" == f, g = !k && (a.My || !f || g) ? new Wm : !k && l ? new jn : new hn;
    g.Tl(a);
    k = p;
    if(C(b, "position") && (k = Wb(b, "position"), "bottom" == k || "right" == k)) {
      H(b, "position", "opposite"), k = "opposite"
    }
    k = "opposite" == k || k == p && !c;
    l = f && (a.My || !n) || !f && n;
    n && l && (k = !k);
    g.cn = f;
    g.La = g.wH(F(b, "scale"));
    C(b, "scale") && g.La.g(F(b, "scale"));
    g.Ng = q;
    l ? k ? (g.qb(2), a.ED || (g.Ng = j, a.ED = j)) : (g.qb(3), a.xD || (g.Ng = j, a.xD = j)) : k ? (g.qb(1), a.DD || (g.Ng = j, a.DD = j)) : (g.qb(0), a.zD || (g.Ng = j, a.zD = j));
    var k = a.Ss.primary, m = a.iw.primary, m = !c && (f ? k : m).Ib() != g.Ib();
    g.Xf = m;
    l = l ? a.dv : a.ev;
    m = a.Fw;
    g.Kx = a.Gw;
    g.tp = l;
    g.Xg = m;
    g.$B(em);
    g.g(b, d);
    b = c ? "primary" : N(b, "name");
    g.Rl(b);
    n && f && (g.fa().Td = !g.fa().Nb());
    n && !f && 1 == k.Ib() && (g.fa().Td = !g.fa().Nb());
    f ? (a.Ss[b] = g, a.Qf.push(g)) : (a.iw[b] = g, a.fg.push(g));
    a.me.push(g)
  }
}
function oq(a, b) {
  return b != p && a.Ss[b] ? a.Ss[b] : oq(a, "primary")
}
function pq(a, b) {
  return b != p && a.iw[b] ? a.iw[b] : pq(a, "primary")
}
z.wx = function(a, b) {
  var c = oq(this, C(b, "x_axis") ? N(b, "x_axis") : p), d = pq(this, C(b, "y_axis") ? N(b, "y_axis") : p);
  a.Vb = c;
  a.eb = d;
  c.QB(a, this.F)
};
z.zk = function() {
  for(var a = 0;a < this.me.length;a++) {
    this.me[a].zk(this.Ua)
  }
};
z.Ig = u("eo");
z.Vt = u("CB");
z.Wx = function(a) {
  return a.ka() && a.ka().wa && 10 == a.ka().wa.lg() ? this.Vf() ? this.ev : this.dv : qf.f.Wx.call(this, a)
};
z.sy = function() {
  this.D.ia(this.Fw)
};
z.sl = function(a) {
  qf.f.sl.call(this, a);
  this.dv = new W(a);
  this.ev = new W(a);
  this.Gw = new W(a);
  this.Fw = new W(a)
};
z.p = function(a, b, c) {
  a.Ca();
  var d, f, g = this.me.length;
  for(d = 0;d < g;d++) {
    if(f = this.me[d], (isNaN(f.La.ld) || isNaN(f.La.kd)) && !f.cn && "primary" != f.getName()) {
      var k = pq(this);
      f.Gc(k.fa().ld);
      f.Gc(k.fa().kd)
    }
  }
  d = this.me.length;
  b = qf.f.p.call(this, a, b, c);
  a = a.Ca();
  b.ia(this.dv);
  b.ia(this.ev);
  b.ia(this.Gw);
  for(c = 0;c < d;c++) {
    if(g = this.me[c], Lm(g), g.Ml = 0, g.xp) {
      for(f = 0;f < g.xp.length;f++) {
        k = g.xp[f][0], g.Ml = Math.max(g.Ml, g.J.nd(k.n(g.xp[f][1], k.ka().wa, g.aa.fb())) + k.ka().wa.ga())
      }
    }
  }
  qq(this, a);
  rq(this);
  for(c = 0;c < d;c++) {
    g = this.me[c], Lm(g)
  }
  return b
};
function rq(a) {
  sd(a.vt, a.eo);
  sd(a.Zy, a.eo);
  sd(a.vk, a.eo)
}
z.qa = function() {
  sq(this);
  qf.f.qa.call(this)
};
function sq(a) {
  var b, c;
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Um(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Um(a.Qf[b])
  }
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Tm(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Tm(a.Qf[b])
  }
  for(b = 0;b < a.fg.length;b++) {
    Sm(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Sm(a.Qf[b])
  }
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Rm(a.fg[b]), a.fg[b].xo()
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Rm(a.Qf[b]), a.Qf[b].xo()
  }
  for(b = 0;b < a.fg.length;b++) {
    Qm(a.fg[b]), a.fg[b].vC()
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Qm(a.Qf[b]), a.Qf[b].vC()
  }
  b = 0;
  for(c = a.me.length;b < c;b++) {
    a.me[b].zx();
    var d = a.me[b];
    d.Ma && d.He && d.He.qa(a.r())
  }
}
function qq(a, b) {
  var c, d = 0, f = a.me.length, b = b.Ca(), g = b.Ca();
  g.x = 0;
  g.y = 0;
  g.width *= 0.75;
  g.height *= 0.75;
  for(d = 0;d < f;d++) {
    c = a.me[d], Mm(c, g), Om(c)
  }
  g.width /= 0.75;
  g.height /= 0.75;
  for(var k = g.Ca(), d = 0;d < f;d++) {
    c = a.me[d], c.Ma && c.J.ao(g, c.Mm + c.KA + c.Ml + c.Cr)
  }
  c = g.Ca();
  for(d = 0;d < f;d++) {
    Nm(a.me[d], g)
  }
  for(d = 0;d < f;d++) {
    var l = a.me[d];
    Mm(l, g);
    l.yd = l.Ma && l.O ? l.Cr + l.Mm + l.KA + Math.max(wm(l), l.wk) : l.Cr + l.Mm + l.KA + l.wk
  }
  g = c;
  for(d = 0;d < f;d++) {
    Nm(a.me[d], g)
  }
  for(var l = g.x - k.x, n = g.y - k.y, m = k.width - g.width - l, o = k.height - g.height - n, d = 0;d < f;d++) {
    c = a.me[d];
    var v = k.Ca();
    c.AK(v, g);
    c = v.x - k.x;
    var w = v.y - k.y, y = k.width - v.width - c, v = k.height - v.height - w;
    c > l && (l = c);
    y > m && (m = y);
    w > n && (n = w);
    v > o && (o = v)
  }
  g.x = k.x + l;
  g.y = k.y + n;
  g.width = k.width - l - m;
  g.height = k.height - n - o;
  for(d = 0;d < f;d++) {
    a.me[d].Rc(g)
  }
  for(d = m = n = l = k = 0;d < f;d++) {
    c = a.me[d];
    o = 0;
    switch(c.Ib()) {
      case 0:
        o = k;
        k += c.nd();
        break;
      case 1:
        o = l;
        l += c.nd();
        break;
      case 2:
        o = n;
        n += c.nd();
        break;
      case 3:
        o = m, m += c.nd()
    }
    c.Ng || (o += c.Hx);
    c.Ei(o);
    c.Rc(g)
  }
  a.eo = g;
  b.x += g.x;
  b.y += g.y;
  b.width = g.width;
  b.height = g.height;
  a.CB = b
}
z.nm = function(a) {
  a.Ca();
  a = a.Ca();
  qq(this, a);
  qf.f.nm.call(this, a)
};
z.Jq = function() {
  this.Gw.clear();
  this.dv.clear();
  this.ev.clear();
  this.Fw.clear();
  qf.f.Jq.call(this);
  sq(this);
  rq(this)
};
z.Cb = function() {
  qf.f.Cb.call(this);
  this.b.add("%DataPlotXMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotXMin", Number.MAX_VALUE);
  this.b.add("%DataPlotXSum", 0);
  this.b.add("%DataPlotXAverage", 0);
  this.b.add("%DataPlotYRangeMin", Number.MAX_VALUE);
  this.b.add("%DataPlotYRangeMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotYRangeSum", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%DataPlotXMax":
    ;
    case "%DataPlotXMin":
    ;
    case "%DataPlotXSum":
    ;
    case "%DataPlotXAverage":
    ;
    case "%DataPlotYAverage":
    ;
    case "%DataPlotYRangeMin":
    ;
    case "%DataPlotYRangeMax":
    ;
    case "%DataPlotYRangeSum":
      return 2
  }
  return qf.f.Pa.call(this, a)
};
z.Na = function(a) {
  ne(this.b, "%DataPlotXAverage") || this.b.add("%DataPlotXAverage", this.b.get("%DataPlotXSum") / this.b.get("%DataPlotPointCount"));
  ne(this.b, "%DataPlotYAverage") || this.b.add("%DataPlotYAverage", this.b.get("%DataPlotYSum") / this.b.get("%DataPlotPointCount"));
  return qf.f.Na.call(this, a)
};
z.mb = function(a) {
  a = qf.f.mb.call(this, a);
  a.YRangeMax = this.b.get("%DataPlotRangeMax");
  a.YRangeMin = this.b.get("%DataPlotRangeMin");
  a.YRangeSum = this.b.get("%DataPlotRangeSum");
  a.XSum = this.b.get("%DataPlotXSum");
  a.XMax = this.b.get("%DataPlotXMax");
  a.XMin = this.b.get("%DataPlotXMin");
  a.XAverage = this.b.get("%DataPlotXSum") / this.b.get("%DataPlotPointCount");
  a.Categories = [];
  var b, c = this.Bq.length;
  for(b = 0;b < c;b++) {
    var d = this.Bq[b];
    d && a.Categories.push(d.mb())
  }
  a.XAxes = {};
  a.YAxes = {};
  var d = this.Qf.length, f = this.fg.length;
  for(b = 0;b < d;b++) {
    c = this.Qf[b], a.XAxes[c.getName()] = c.mb()
  }
  for(b = 0;b < f;b++) {
    c = this.fg[b], a.YAxes[c.getName()] = c.mb()
  }
  return a
};
z.mk = function(a, b) {
  var c, d;
  for(d in this.Wk) {
    if(c = this.Wk[d], c.getName == a) {
      break
    }
  }
  c && (b ? c.ah() : c.dh())
};
function tq(a, b) {
  Re.call(this, a, b)
}
A.e(tq, Pf);
z = tq.prototype;
z.Kb = p;
z.VE = p;
z.RD = p;
z.Et = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "LightColor(" + a + ")";
  f.push(g);
  k.position = 0.95;
  k.opacity = b;
  k.color = "DarkColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "radial";
  d.angle = -145;
  d.focal_point = "0.5";
  d.key = f;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Id;
  this.Hb.g(c);
  this.Kb = new Kd;
  this.Kb.g(c);
  this.Kb.ad = 0
};
z.g = function(a) {
  var a = tq.f.g.call(this, a), b = F(a, "fill"), b = C(b, "opacity") ? F(b, "opacity") : 1;
  this.Et(F(F(a, "fill"), "color"), b);
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = "0";
  g.color = "Black";
  f.push(g);
  k.position = 0.95;
  k.opacity = "0";
  k.color = "Black";
  f.push(k);
  l.position = 1;
  l.opacity = 100 / 255 * b;
  l.color = "Black";
  f.push(l);
  d.type = "radial";
  d.angle = "0";
  d.focal_point = "0";
  d.key = f;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.VE = new Id;
  this.VE.g(c);
  this.RD = new Id;
  return a
};
function uq() {
}
A.e(uq, Me);
z = uq.prototype;
z.Yh = p;
z.Ta = function() {
  this.G.setAttribute("d", this.j.nl())
};
z.createElement = function() {
  return this.$a.createElement()
};
z.oh = function(a) {
  var b = this.j.r();
  this.Yh && -1 == this.Yh.indexOf("fill:none") && Ec(b.cr, this.Yh);
  this.Yh = "";
  this.vf(a)
};
z.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  this.Yh = c += Jd(a.mc(), b, f, d, j);
  this.G.setAttribute("style", c)
};
z.ud = s();
function vq() {
}
A.e(vq, Mf);
vq.prototype.Ta = function() {
  this.G.setAttribute("d", this.j.nl())
};
function wq() {
}
A.e(wq, Nf);
wq.prototype.Ta = function() {
  this.G.setAttribute("d", this.j.nl())
};
function xq() {
}
A.e(xq, Of);
xq.prototype.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new vq, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new wq, this.Fb.p(this.j, this))
};
xq.prototype.createElement = function() {
  return this.j.r().ja()
};
xq.prototype.update = function(a, b) {
  b && this.j.Uj();
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
xq.prototype.oh = s();
function yq() {
}
A.e(yq, uq);
function zq() {
}
A.e(zq, uq);
zq.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  this.Yh = c += Jd(a.VE, b, f, d, j);
  this.G.setAttribute("style", c)
};
function Aq() {
}
A.e(Aq, uq);
Aq.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n(), g = this.j.o, k = g.Sb, l = g.Qa, g = g.na;
  if(0 < k) {
    var k = k / l, l = {}, n = {}, m = [], o = {}, v = {}, w = {};
    o.position = k;
    o.opacity = 100 / 255;
    o.color = "Black";
    m.push(o);
    v.position = 0 == g ? k + 0.1 : k + 0.05;
    v.opacity = "0";
    v.color = "Black";
    m.push(v);
    w.position = 1;
    w.opacity = "0";
    w.color = "Black";
    m.push(w);
    n.type = "radial";
    n.angle = "0";
    n.focal_point = "0";
    n.key = m;
    l.enabled = j;
    l.type = "gradient";
    l.gradient = n;
    a.RD.g(l);
    c += Jd(a.RD, b, f, d, j)
  }else {
    c += "fill:none;"
  }
  this.Yh = c;
  this.G.setAttribute("style", c)
};
function Bq() {
}
A.e(Bq, Qe);
z = Bq.prototype;
z.fb = function() {
  return this.j.fb()
};
z.sm = p;
z.Bn = p;
z.bn = p;
z.Yc = function() {
  this.sm = new yq;
  this.sm.p(this.j, this);
  this.Bn = new zq;
  this.Bn.p(this.j, this);
  this.bn = new Aq;
  this.bn.p(this.j, this)
};
z.ff = function() {
  var a = this.fb();
  this.sm && a.appendChild(this.sm.G);
  this.Bn && a.appendChild(this.Bn.G);
  this.bn && a.appendChild(this.bn.G)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.update = function(a, b) {
  b && this.j.Uj();
  this.sm && this.sm.update(a, b);
  this.Bn && this.Bn.update(a, b);
  this.bn && this.bn.update(a, b)
};
z.oh = function(a) {
  this.sm && this.sm.oh(a);
  this.Bn && this.Bn.oh(a);
  this.bn && this.bn.oh(a)
};
function Cq() {
  Re.call(this, h, h)
}
A.e(Cq, tq);
function Dq() {
  $.call(this)
}
A.e(Dq, Qf);
Dq.prototype.p = function(a) {
  return Dq.f.p.call(this, a)
};
Dq.prototype.lc = function() {
  return new Bq
};
Dq.prototype.cc = function() {
  return Cq
};
function Eq() {
  $.call(this)
}
A.e(Eq, Qf);
Eq.prototype.lc = function() {
  return new xq
};
Eq.prototype.cc = function() {
  return Pf
};
function Fq() {
}
A.e(Fq, Pe);
Fq.prototype.mc = function(a) {
  return a.VC
};
Fq.prototype.Ie = x(j);
Fq.prototype.Ta = function() {
  var a;
  a = this.j;
  var b = "";
  if(a.xh) {
    for(var c = 0;c < a.xh.li();c++) {
      b += a.xh.qa(c)
    }
    a = b + " Z"
  }else {
    a = p
  }
  a != p && " Z" != a && "" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
};
function Gq() {
}
A.e(Gq, Pe);
Gq.prototype.mc = function(a) {
  return a.uB
};
Gq.prototype.Ie = x(j);
Gq.prototype.Ta = function() {
  if(this.j.Qj) {
    var a;
    a = this.j;
    var b = "";
    if(a.Qj) {
      for(var c = 0;c < a.Qj.li();c++) {
        b += a.Qj.qa(c)
      }
      a = b + " Z"
    }else {
      a = p
    }
    a != p && " Z" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
  }
};
function Hq() {
}
A.e(Hq, Pe);
Hq.prototype.mc = function(a) {
  return a.nG
};
Hq.prototype.Ie = x(j);
Hq.prototype.Ta = function() {
  var a;
  a = this.j;
  var b = a.o.Qa, c = a.o.pj, d = a.o.Sb, f = a.o.Ro, g = a.jb % 360 * Math.PI / 180, k = a.tc % 360 * Math.PI / 180;
  a.jb % 360 == a.tc % 360 && (k = g = 0);
  var l = a.ae(), n = l.x + d * Math.cos(g), m = l.y + f * Math.sin(g), o = l.x + d * Math.cos(k), v = l.y + f * Math.sin(k), w = l.x + b * Math.cos(g), g = l.y + c * Math.sin(g), y = l.x + b * Math.cos(k), k = l.y + c * Math.sin(k), E = l = q;
  180 <= a.kh && (l = !l, E = !E);
  if(a.nh != p) {
    var V = a.nh.r(), R;
    R = "" + S(n, m);
    R += V.ub(d, l, j, o, v, f);
    R += U(y, k);
    R += V.ub(b, E, q, w, g, c);
    R += U(n, m);
    w == y && g == k && a.jb % 360 == a.tc % 360 && (R = "" + S(n, m), R += V.ub(d, 1, 1, o, v - 0.001, f), R += S(w, g), R += V.ub(b, 1, 0, y, k + 0.001, c), R += U(n, m));
    a = R + " Z"
  }else {
    a = p
  }
  a != p && " Z" != a && "" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
};
function Iq() {
}
A.e(Iq, Pe);
Iq.prototype.mc = function(a) {
  return a.SF
};
Iq.prototype.Ie = x(j);
Iq.prototype.Ta = function() {
  if(this.j.xh != p) {
    var a;
    a = this.j;
    for(var b = a.ae(), c = "", d = a.o.Qa, f = a.o.pj, g = 0.03 * a.ca().Qa, k, l, n, m = 0;m < a.xh.li();m++) {
      var o = a.xh.Pb[m] - 0.275, v = a.xh.Fo(m) + 0.275;
      for(k = o;k <= v;k++) {
        l = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), n = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), c = k == o ? c + S(b.x + Uc(d - l, k), b.y + Vc(f - n, k)) : c + U(b.x + Uc(d - l, k), b.y + Vc(f - n, k))
      }
      l = g / 2 * Math.abs(Math.sin(v * Math.PI / 180));
      n = g / 2 * Math.abs(Math.sin(v * Math.PI / 180));
      c += U(b.x + Uc(d - l, v), b.y + Vc(f - n, v));
      for(k = v;k >= o;k--) {
        l = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), n = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), c += U(b.x + Uc(d + l, k), b.y + Vc(f + n, k))
      }
      l = g / 2 * Math.abs(Math.sin(o * Math.PI / 180));
      n = g / 2 * Math.abs(Math.sin(o * Math.PI / 180));
      c += U(b.x + Uc(d - l, o), b.y + Vc(f - n, o));
      c += " Z"
    }
    a = c;
    a != p && " Z" != a && "" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
  }
};
function Jq() {
}
A.e(Jq, Pe);
Jq.prototype.mc = function(a) {
  return a.WF
};
Jq.prototype.Ie = x(j);
Jq.prototype.Ta = function() {
  var a = this.j.Ii != p ? Kq(this.j, this.j.jb) : p;
  a != p && " Z" != a && "" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
};
function Lq() {
}
A.e(Lq, Pe);
Lq.prototype.mc = function(a) {
  return a.BC
};
Lq.prototype.Ie = x(j);
Lq.prototype.Ta = function() {
  var a = this.j.bi != p ? Kq(this.j, this.j.tc) : p;
  a != p && " Z" != a && "" != a ? this.G.setAttribute("d", a) : this.G.removeAttribute("d")
};
function Mq() {
}
A.e(Mq, Qe);
z = Mq.prototype;
z.ji = p;
z.Uh = p;
z.Ii = p;
z.bi = p;
z.nh = p;
z.bs = p;
z.Yc = function() {
  this.ji = new Fq;
  this.ji.p(this.j, this);
  this.Uh = new Gq;
  this.Uh.p(this.j, this);
  this.Ii = new Jq;
  this.Ii.p(this.j, this);
  this.bi = new Lq;
  this.bi.p(this.j, this);
  this.nh = new Hq;
  this.nh.p(this.j, this);
  this.bs = new Iq;
  this.bs.p(this.j, this);
  this.bs.G.setAttribute("filter", "url(#smileBlur)")
};
z.ff = function() {
  var a = this.j;
  if(a.nh) {
    var b = new W(a.r());
    b.appendChild(this.nh.G);
    var c = new W(a.r());
    c.appendChild(this.bs.G);
    a.nh.appendChild(b.G);
    a.nh.appendChild(c.G)
  }
  a.ji && a.ji.appendChild(this.ji.G);
  a.Uh && a.Uh.appendChild(this.Uh.G);
  a.Ii && a.Ii.appendChild(this.Ii.G);
  a.bi && a.bi.appendChild(this.bi.G)
};
z.update = function(a, b) {
  b && this.j.Uj();
  this.nh.update(a, b);
  this.bs.update(a, b);
  this.ji && this.ji.update(a, b);
  this.Uh && this.Uh.update(a, b);
  this.Ii.update(a, b);
  this.bi.update(a, b)
};
z.oh = function(a) {
  this.nh.oh(a);
  this.bs.oh(a);
  this.ji && this.ji.oh(a);
  this.Uh && this.Uh.oh(a);
  this.Ii.oh(a);
  this.bi.oh(a)
};
z.Ta = s();
z.createElement = function() {
  return this.j.r().ja()
};
function Nq(a, b) {
  Re.call(this, a, b)
}
A.e(Nq, Pf);
z = Nq.prototype;
z.VC = p;
z.uB = p;
z.WF = p;
z.BC = p;
z.nG = p;
z.SF = p;
z.g = function(a) {
  var a = Nq.f.g.call(this, a), b = F(F(a, "fill"), "color"), c = [], d = {}, f = {}, g = {}, k = {};
  d.position = "0";
  d.opacity = "1";
  d.color = b;
  c.push(d);
  f.position = 1;
  f.opacity = "1";
  f.color = "Blend(" + b + ", DarkColor(" + b + "),0.7)";
  c.push(f);
  g.type = "linear";
  g.angle = "-50";
  g.key = c;
  k.enabled = j;
  k.type = "gradient";
  k.gradient = g;
  this.nG = new Id;
  this.nG.g(k);
  var c = [], d = {}, f = {}, g = {}, k = {}, l = {}, c = [];
  d.position = "0";
  d.opacity = 1;
  d.color = b;
  c.push(d);
  f.position = 0.19;
  f.opacity = 1;
  f.color = "Blend(DarkColor(" + b + "),LightColor(" + b + "),0.3)";
  c.push(f);
  g.position = 1;
  g.opacity = 1;
  g.color = "Blend(" + b + ",DarkColor(" + b + "),0.3)";
  c.push(g);
  k.type = "linear";
  k.angle = "45";
  k.key = c;
  l.enabled = j;
  l.type = "gradient";
  l.gradient = k;
  this.VC = new Id;
  this.VC.g(l);
  var c = [], d = {}, f = {}, g = {}, k = {}, l = {}, n = {};
  d.position = 0;
  d.opacity = 0.29;
  d.color = "white";
  c.push(d);
  f.position = 0.28;
  f.opacity = 0.8;
  f.color = "white";
  c.push(f);
  g.position = 0.72;
  g.opacity = 0.16;
  g.color = "white";
  c.push(g);
  k.position = 1;
  k.opacity = "0";
  k.color = "white";
  c.push(k);
  l.type = "linear";
  l.angle = "0";
  l.key = c;
  n.enabled = j;
  n.type = "gradient";
  n.gradient = l;
  this.SF = new Id;
  this.SF.g(n);
  c = {enabled:j, type:"solid"};
  c.color = "Blend(" + b + ", DarkColor(" + b + "), 0.35)";
  this.uB = new Id;
  this.uB.g(c);
  this.WF = new Id;
  this.WF.g(c);
  this.BC = new Id;
  this.BC.g(c);
  return a
};
function Oq() {
  $.call(this)
}
A.e(Oq, Qf);
Oq.prototype.p = function(a) {
  return Oq.f.p.call(this, a)
};
Oq.prototype.lc = function() {
  return new Mq
};
Oq.prototype.cc = function() {
  return Nq
};
function Pq(a) {
  this.Pb = [];
  this.Zb = [];
  this.$r = [];
  this.F = a
}
z = Pq.prototype;
z.Pb = p;
z.Zb = p;
z.Fo = function(a) {
  return this.Zb[a]
};
z.j = p;
z.$r = p;
z.li = function() {
  return this.$r.length
};
z.af = p;
z.F = p;
z.p = function() {
  A.xa()
};
z.qa = function() {
  A.xa()
};
z.li = function() {
  return this.$r.length
};
z.fb = function(a) {
  return this.$r[a]
};
function Qq(a, b) {
  var c = a.Pb[b], d = a.Zb[b];
  d < c && (d += 360);
  return(c + d) / 2
}
z.isEnabled = function() {
  A.xa()
};
z.cj = function(a, b) {
  return 0 <= a && 0 <= b ? 1 : 0 >= a && 0 <= b ? 2 : 0 >= a && 0 > b ? 3 : 4
};
function Rq(a) {
  Pq.call(this, a)
}
A.e(Rq, Pq);
Rq.prototype.p = function(a) {
  this.j = a;
  var b = a.jb, a = a.Fo(), c = Math.sin(b * Math.PI / 180), d = Math.sin(a * Math.PI / 180), f = Math.cos(b * Math.PI / 180), g = Math.cos(a * Math.PI / 180), c = this.cj(f, c), d = this.cj(g, d);
  if(1 == c) {
    switch(d) {
      case 1:
        f <= g && (this.Pb.push(180), this.Zb.push(360));
        break;
      case 3:
        this.Pb.push(180);
        this.Zb.push(a);
        break;
      case 4:
        this.Pb.push(180), this.Zb.push(a), this.af = j
    }
  }else {
    if(2 == c) {
      switch(d) {
        case 1:
          this.Pb.push(180);
          this.Zb.push(360);
          this.af = j;
          break;
        case 2:
          f <= g && (this.Pb.push(180), this.Zb.push(360), this.af = j);
          break;
        case 3:
          this.Pb.push(180);
          this.Zb.push(a);
          break;
        case 4:
          this.Pb.push(180), this.Zb.push(a), this.af = j
      }
    }else {
      if(3 == c) {
        switch(d) {
          case 1:
          ;
          case 2:
            this.Pb.push(b);
            this.Zb.push(360);
            this.af = j;
            break;
          case 3:
            f >= g ? (this.Pb.push(b), this.Zb.push(360), this.af = j, this.Pb.push(180)) : this.Pb.push(b);
            this.Zb.push(a);
            break;
          case 4:
            this.Pb.push(b), this.Zb.push(a), this.af = j
        }
      }else {
        if(4 == c) {
          switch(d) {
            case 1:
            ;
            case 2:
              this.Pb.push(b);
              this.Zb.push(360);
              break;
            case 3:
              this.Pb.push(b);
              this.Zb.push(360);
              this.Pb.push(180);
              this.Zb.push(a);
              break;
            case 4:
              f >= g ? (this.Pb.push(b), this.Zb.push(360), this.Pb.push(180), this.Zb.push(a), this.af = j) : (this.Pb.push(b), this.Zb.push(a))
          }
        }
      }
    }
  }
  b = this.Pb.length;
  for(f = 0;f < b;f++) {
    a = this.j.Uh, this.$r.push(a)
  }
};
Rq.prototype.qa = function(a) {
  var b = this.fb(a), c = this.j.o.Sb, d = this.j.o.Ro, f = this.j.ae(), g = this.j.ca().Qa * this.j.ca().Pi, k = this.Pb[a] * Math.PI / 180, l = this.Zb[a] * Math.PI / 180, a = f.x + c * Math.cos(k), k = f.y + d * Math.sin(k), n = f.x + c * Math.cos(l), f = f.y + d * Math.sin(l), l = q, m = j;
  180 < this.kh && (l = !l, m = !m);
  var b = b.r(), o;
  o = "" + S(a, k);
  o += b.ub(c, l, m, n, f, d);
  o += U(n, f + g);
  o += b.ub(c, 0, 0, a, k + g, d);
  return o += U(a, k)
};
Rq.prototype.isEnabled = function(a, b) {
  var a = a * (Math.PI / 180), b = b * (Math.PI / 180), c = Math.cos(a), d = Math.cos(b), f = this.cj(c, Math.sin(a)), g = this.cj(d, Math.sin(b));
  return 3 == f || 4 == f ? j : 1 == f ? 3 == g || 4 == g ? j : 1 == g ? c <= d : q : 2 == f ? 2 == g ? c <= d : j : q
};
function Sq(a) {
  Pq.call(this, a)
}
A.e(Sq, Pq);
Sq.prototype.p = function(a) {
  this.j = a;
  var b = a.jb, a = a.Fo(), c = Math.sin(b * Math.PI / 180), d = Math.sin(a * Math.PI / 180), f = Math.cos(b * Math.PI / 180), g = Math.cos(a * Math.PI / 180), c = this.cj(f, c), d = this.cj(g, d);
  if(1 == c) {
    switch(d) {
      case 1:
        f >= g ? (this.Pb.push(b), this.Zb.push(a)) : (this.Pb.push(b), this.Zb.push(180), this.Pb.push(360), this.Zb.push(a), this.af = j);
        break;
      case 2:
        this.Pb.push(b);
        this.Zb.push(a);
        this.af = j;
        break;
      case 3:
      ;
      case 4:
        this.Pb.push(b), this.Zb.push(180), this.af = j
    }
  }else {
    if(2 == c) {
      switch(d) {
        case 1:
          this.Pb.push(b);
          this.Zb.push(180);
          this.Pb.push(360);
          this.Zb.push(a);
          break;
        case 2:
          f >= g ? (this.Pb.push(b), this.Zb.push(a)) : (this.Pb.push(b), this.Zb.push(180), this.Pb.push(360), this.Zb.push(a), this.af = j);
          break;
        case 3:
        ;
        case 4:
          this.Pb.push(b), this.Zb.push(180)
      }
    }else {
      if(3 == c) {
        switch(d) {
          case 1:
            this.Pb.push(360);
            this.Zb.push(a);
            break;
          case 2:
            this.Pb.push(360);
            this.Zb.push(a);
            this.af = j;
            break;
          case 3:
            f >= g && (this.Pb.push(0), this.Zb.push(180), this.af = j)
        }
      }else {
        if(4 == c) {
          switch(d) {
            case 1:
              this.Pb.push(360);
              this.Zb.push(a);
              break;
            case 2:
              this.Pb.push(360);
              this.Zb.push(a);
              this.af = j;
              break;
            case 3:
              this.Pb.push(360);
              this.Zb.push(180);
              this.af = j;
              break;
            case 4:
              f >= g && (this.Pb.push(0), this.Zb.push(180), this.af = j)
          }
        }
      }
    }
  }
  b = this.Pb.length;
  for(f = 0;f < b;f++) {
    a = this.j.ji, this.$r.push(a)
  }
};
Sq.prototype.qa = function(a) {
  var b = this.fb(a), c = this.j.o.Qa, d = this.j.o.pj, f = this.j.ae(), g = this.j.ca().Qa * this.j.ca().Pi, k = this.Pb[a] * Math.PI / 180, l = this.Zb[a] * Math.PI / 180, a = f.x + c * Math.cos(k), k = f.y + d * Math.sin(k), n = f.x + c * Math.cos(l), f = f.y + d * Math.sin(l), l = q, m = j;
  180 < this.kh && (l = !l, m = !m);
  var b = b.r(), o;
  o = "" + S(a, k);
  o += b.ub(c, l, m, n, f, d);
  o += U(n, f + g);
  o += b.ub(c, 0, 0, a, k + g, d);
  return o += U(a, k)
};
Sq.prototype.isEnabled = function(a, b) {
  if(a == b) {
    return q
  }
  var a = a * (Math.PI / 180), b = b * (Math.PI / 180), c = Math.cos(a), d = Math.cos(b), f = this.cj(c, Math.sin(a)), g = this.cj(d, Math.sin(b));
  return 1 == f || 2 == f ? j : 3 == f ? 1 == g || 2 == g ? j : 3 == g ? c >= d : q : 4 == f ? 4 == g ? c >= d : j : q
};
function go() {
  this.Ge()
}
A.e(go, Wn);
z = go.prototype;
z.ma = NaN;
z.Xb = u("ma");
z.gd = t("ma");
z.ta = u("ma");
z.g = function(a, b) {
  go.f.g.call(this, a, b);
  C(a, "value") ? this.ma = F(a, "value") : C(a, "y") && (this.ma = F(a, "y"));
  this.ma = Ob(this.ma)
};
z.Cb = function() {
  go.f.Cb.call(this);
  this.b.add("%Value", 0);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%YValue":
    ;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
      return 2
  }
  return go.f.Pa.call(this, a)
};
z.Se = function() {
  go.f.Se.call(this);
  this.ma && (this.b.add("%Value", this.ma), this.b.add("%YValue", this.ma))
};
z.Na = function(a) {
  ne(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.ma / this.o.Uf("%SeriesYSum")));
  ne(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.ma / this.ca().Na("%DataPlotYSum")));
  return go.f.Na.call(this, a)
};
z.mb = function(a) {
  a = go.f.mb.call(this, a);
  a.YValue = this.b.get("%YValue");
  a.YPercentOfSeries = this.b.get("%YPercentOfSeries");
  a.YPercentOfTotal = this.b.get("%YPercentOfTotal");
  return a
};
function Tq() {
  co.call(this)
}
A.e(Tq, co);
z = Tq.prototype;
z.Cb = function() {
  Tq.f.Cb.call(this);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%SeriesYMax", 0);
  this.b.add("%SeriesYMin", 0);
  this.b.add("%SeriesYBasedPointsCount", 0);
  this.b.add("%SeriesFirstYValue", 0);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%SeriesMaxYValuePointName", "");
  this.b.add("%SeriesMinYValuePointName", "")
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesYSum":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYBasedPointsCount":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesLastYValue":
    ;
    case "%SeriesYAverage":
      return 2;
    case "%SeriesMaxYValuePointName":
    ;
    case "%SeriesMinYValuePointName":
      return 1
  }
  return Tq.f.Pa.call(this, a)
};
z.rh = function() {
  Tq.f.rh.call(this);
  var a = this.b.get("%SeriesYSum");
  this.b.add("%SeriesValue", a);
  this.b.add("%SeriesYValue", a);
  var b = this.Fe();
  this.b.add("%SeriesYAverage", a / b);
  0 >= b || (this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Xb()), this.Ga(b - 1) && this.b.add("%SeriesLastYValue", this.Ga(b - 1).Xb()))
};
z.th = function(a) {
  Tq.f.th.call(this, a);
  a = a.b;
  oe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  oe(a, "%DataPlotYBasedPointsCount", this.Fe());
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") < a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  Tq.f.Ed.call(this, a);
  var b = a.Xb(), c = Number(this.b.get("%SeriesYMax")), d = Number(this.b.get("%SeriesYMin"));
  oe(this.b, "%SeriesYSum", b);
  b > c && (this.b.add("%SeriesYMax", b), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  b < d && (this.b.add("%SeriesYMin", b), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.mb = function(a) {
  a = Tq.f.mb.call(this, a);
  a.FirstYValue = this.Uf("%SeriesFirstYValue");
  a.LastYValue = this.Uf("%SeriesLastYValue");
  a.YSum = this.Uf["%SeriesYSum"];
  a.YMax = this.Uf["%SeriesYMax"];
  a.YMin = this.Uf["%SeriesYMin"];
  a.YAverage = this.Uf("%SeriesYAverage");
  a.YMedian = this.Uf("%SeriesYMedian");
  a.YMode = this.Uf("%SeriesYMode");
  return a
};
function Uq() {
}
A.e(Uq, mg);
Uq.prototype.update = function(a) {
  this.j.o.fj && !this.j.ca().rc[this.j.bp].isEnabled() ? this.Gi(q) : Uq.f.update.call(this, a)
};
Uq.prototype.HC = function(a, b, c, d) {
  b.update(a, c, d, q, b.Tp)
};
function Vq() {
  $.call(this)
}
A.e(Vq, ng);
Vq.prototype.lc = function() {
  return new Uq
};
function Wq() {
}
A.e(Wq, Rn);
z = Wq.prototype;
z.xu = p;
z.aA = t("xu");
z.Qb = function() {
  return new Vq
};
z.Ib = function(a, b, c) {
  return this.xu ? Wq.f.Ib.call(this, a, b, c) : a.Jg(this, b, this.n(a, b, c))
};
z.copy = function(a) {
  a = Wq.f.copy.call(this, a);
  a.aA(this.xu);
  return a
};
function Xq(a, b, c, d) {
  this.ac = a;
  this.ob = b.height;
  this.ab = b.width;
  this.j = c;
  this.Ma = j;
  this.na = d
}
z = Xq.prototype;
z.ac = NaN;
z.Wd = t("ac");
z.ob = NaN;
z.vb = u("ob");
z.Ql = t("ob");
z.ab = NaN;
z.Ka = u("ab");
z.Cj = t("ab");
z.j = p;
z.Ul = t("j");
z.Ma = q;
z.isEnabled = u("Ma");
z.zg = t("Ma");
z.na = NaN;
z.Vr = t("na");
function Yq(a, b, c) {
  this.aa = c;
  this.hr = Zq(a.ac, -90, 270, 90);
  a.ac = this.hr;
  this.wc = 90 <= this.hr && 270 > this.hr;
  this.O = [];
  this.O.push(a);
  this.uk = [];
  this.uk.push(a.ac);
  this.tk = [];
  this.tk.push(a.ob);
  this.ut = this.Th = a.ob;
  this.wc ? (this.ig = this.gl = $q(c, a, q).y + this.Th / 2, this.kg = this.ig - this.Th) : (this.kg = this.gl = $q(c, a, q).y - this.Th / 2, this.ig = this.kg + this.Th);
  this.Zj = b;
  this.tg = Number.MAX_VALUE;
  this.ug = -Number.MAX_VALUE;
  ar(this, 0)
}
z = Yq.prototype;
z.wc = q;
z.Zj = NaN;
z.O = p;
z.uk = p;
z.tk = p;
z.kg = NaN;
z.ig = NaN;
z.tg = NaN;
z.ug = NaN;
z.xC = NaN;
z.gl = NaN;
z.ut = NaN;
z.Th = NaN;
z.aa = p;
z.hr = NaN;
function ar(a, b) {
  var c = a.O[b], d = c.ac, f, g = 0;
  a.wc ? (c.Wd(Math.max(90.01, Zq(a.uk[b] - a.Zj, -90, 270, 90))), f = $q(a.aa, c, q).y - c.vb() / 2 + a.tk[b], g = a.tg, a.tg = Math.min(g, f), c.Wd(Math.min(270, Zq(a.uk[b] + a.Zj, 90, 450, 270))), f = $q(a.aa, c, q).y - c.vb() / 2 + a.tk[b], a.ug < f && (a.ug = f, a.xC = b)) : (c.Wd(Math.max(-90, Zq(a.uk[b] - a.Zj, -270, 90, -90))), f = $q(a.aa, c, q).y + c.vb() / 2 - a.tk[b], g = a.ug, a.ug = Math.max(g, f), c.Wd(Math.min(90, Zq(a.uk[b] + a.Zj, -90, 270, 90))), f = $q(a.aa, c, q).y + c.vb() / 
  2 - a.tk[b], a.tg > f && (a.tg = f, a.xC = b));
  c.Wd(d)
}
z.Zw = function(a) {
  return a.wc != this.wc ? q : this.wc ? this.kg <= a.ig : this.ig >= a.kg
};
function br(a, b) {
  for(var c = b.O.length, d = 0;d < c;d++) {
    var f = b.O[d];
    f.Wd(b.uk[d]);
    a.O.push(f);
    a.uk.push(f.ac);
    a.Th += f.vb();
    a.tk.push(a.Th);
    a.gl = a.wc ? a.gl + ($q(a.aa, f, q).y + f.ob / 2 + a.ut) : a.gl + ($q(a.aa, f, q).y - f.ob / 2 - a.ut);
    a.ut += f.vb();
    ar(a, a.O.length - 1)
  }
  a.wc ? (a.ig = a.gl / a.O.length, a.ug <= a.tg) ? (a.ig > a.tg && (a.ig = a.tg), a.ig < a.ug && (a.ig = a.ug), a.kg = a.ig - a.Th) : (a.ig = a.tg, a.kg = a.ug - a.Th) : (a.kg = a.gl / a.O.length, a.ug <= a.tg) ? (a.kg > a.tg && (a.kg = a.tg), a.kg < a.ug && (a.kg = a.ug), a.ig = a.kg + a.Th) : (a.kg = a.ug, a.ig = a.tg + a.Th)
}
function cr(a) {
  var b, c = a.O.length, d;
  d = a.ug <= a.tg ? 1 : 1 - (a.ug - a.tg) / a.tk[a.xC];
  if(a.wc) {
    for(b = 0;b < c;b++) {
      a.O[b].ac = dr(a, 180 - er(a.aa, a.ig - a.tk[b] * d + a.O[b].ob / 2), a.uk[b])
    }
  }else {
    for(b = 0;b < c;b++) {
      a.O[b].ac = dr(a, er(a.aa, a.kg + a.tk[b] * d - a.O[b].ob / 2), a.uk[b])
    }
  }
}
z.LL = 88 * Math.PI / 180;
function dr(a, b, c) {
  var d = b, b = b * Math.PI / 180, c = c * Math.PI / 180;
  return 0 > Math.cos(b) * Math.cos(c) || Math.abs(Math.cos(b)) < Math.cos(a.LL) ? 0 < Math.sin(c) ? 90 - 2 * (0 < Math.cos(c) ? 1 : -1) : 270 + 2 * (0 < Math.cos(c) ? 1 : -1) : d
}
function Zq(a, b, c, d) {
  for(;a < b;) {
    a += 360
  }
  for(;a >= c;) {
    a -= 360
  }
  if(a == b || a == d) {
    a += 0.01
  }
  return a
}
;function fr() {
  this.Ge();
  this.yz = {}
}
A.e(fr, go);
z = fr.prototype;
z.jb = NaN;
z.tc = NaN;
z.Fo = u("tc");
z.kh = NaN;
z.Pd = p;
z.fn = q;
z.dl = function() {
  isNaN(this.ma) && (this.Gb = j, this.ma = 0)
};
z.g = function(a, b) {
  fr.f.g.call(this, a, b);
  this.dl();
  C(a, "exploded") && (this.fn = K(a, "exploded"));
  this.fn && (this.cb.yD = j, this.ca().Kq = this)
};
z.p = function(a) {
  fr.f.p.call(this, a);
  this.lk() && (this.Bf = a.ja(), this.D.appendChild(this.Bf));
  return this.D
};
z.update = function() {
  this.Uj();
  fr.f.update.call(this);
  this.qC(this.fb().r())
};
z.Uj = function() {
  var a = this.ae();
  this.k.x = a.x - this.o.Qa;
  this.k.y = a.y - this.o.Qa;
  this.k.width = this.k.height = 2 * this.o.Qa
};
z.ae = function() {
  this.Pd == p && (this.Pd = new O);
  this.Pd.x = this.ca().ae().x;
  this.Pd.y = this.ca().ae().y;
  if(this.fn) {
    var a = this.ca().jk(), b = (this.jb + this.tc) / 2, b = b * (Math.PI / 180);
    this.Pd.x += a * Math.cos(b);
    this.Pd.y += a * Math.sin(b)
  }
  return this.Pd
};
z.HB = function() {
  this.jb = 0 == this.na ? this.o.jb : this.o.Ga(this.na - 1).Fo();
  var a = this.o.fG;
  this.kh = 0 == a ? 360 / this.o.ba.length : 360 * this.ta() / a;
  this.tc = this.jb + this.kh
};
z.Il = function(a) {
  var b = this.ca();
  b.Eb.Lt && (b.Eb.kJ || (b.Kq && b.Kq != this && (b.Kq.fn = q, b.Kq.Uj()), b.Kq = this), this.fn = !this.fn);
  fr.f.Il.call(this, a)
};
z.yz = p;
z.nl = function() {
  var a = this.k.toString() + " " + this.fn.toString();
  if(this.yz[a]) {
    return this.yz[a]
  }
  var b = this.ZB();
  return this.yz[a] = b
};
z.ZB = function() {
  var a;
  a = 1 <= this.o.Sb ? this.o.Sb - 1 : this.o.Sb;
  var b = this.o.Qa, c, d, f;
  this.tc % 360 != this.jb % 360 ? (d = (this.tc + 0.5) * Math.PI / 180, c = this.jb * Math.PI / 180) : (f = Math.min(this.jb, this.tc), d = (this.tc - f) * Math.PI / 180, c = (this.jb - f) * Math.PI / 180);
  var g = this.ae();
  f = g.x + a * Math.cos(c);
  var k = g.y + a * Math.sin(c), l = g.x + b * Math.cos(c);
  c = g.y + b * Math.sin(c);
  var n = g.x + a * Math.cos(d), m = g.y + a * Math.sin(d), o = g.x + b * Math.cos(d);
  d = g.y + b * Math.sin(d);
  var v = g = q, w = j;
  180 < this.kh && (v = g = j);
  var y = this.r();
  l == o && (d -= 1.0E-4, w = q);
  f == n && (k += 1.0E-4);
  var E = S(f, k), E = E + S(l, c), E = E + y.ub(b, g, j, o, d), E = E + (w ? U(n, m) : S(n, m)), E = E + y.ub(a, v, q, f, k);
  return E + " Z"
};
z.Bf = p;
z.qt = p;
z.lk = function() {
  return this.vc && this.Ab().lk()
};
z.oF = function() {
  if(this.lk()) {
    var a = this.Wt(this.r());
    a ? (this.Bf.setAttribute("d", a), this.Bf.setAttribute("visibility", "visible")) : this.Bf.setAttribute("visibility", "hidden")
  }
};
z.qC = function(a) {
  if(this.lk()) {
    var b = this.Wt(a);
    b != p ? this.Bf.setAttribute("d", b) : this.Bf.removeAttribute("d");
    this.Zz(a, this.Bf)
  }
};
z.Zz = function(a, b) {
  var c = Ld(this.Ab().rt, a, this.qt), c = c + md();
  b.setAttribute("style", c)
};
z.Wt = function(a) {
  if(this.o.fj && this.vc && this.iJ && this.Ab().fj && this.ca().rc[this.bp].isEnabled()) {
    var b = this.ca(), c = new O, d = new O, f = new O, c = this.ae().Ca(), g = b.rc[this.bp].ac, k = this.Ab().nJ ? b.Qa * this.Ab().Xi : this.Ab().Xi, d = $q(b, b.rc[this.bp], q).Ca(), f = $q(b, b.rc[this.bp], q).Ca();
    d.x = 90 < g % 360 && 270 > g % 360 ? d.x + k : d.x - k;
    return this.RH(a, c, d, f)
  }
};
z.RH = function(a, b, c, d) {
  var a = this.o.Qa, f = (this.jb + this.tc) / 2;
  b.x += Uc(a, f);
  b.y += Vc(a, f);
  return gr(this, b, c, d)
};
function gr(a, b, c, d) {
  var f = "";
  a.Ab().rt != p && (a.qt = new P(Math.min(b.x, c.x, d.x), Math.min(b.y, c.y, d.y), Math.max(b.x, c.x, d.x) - Math.min(b.x, c.x, d.x), Math.max(b.y, c.y, d.y) - Math.min(b.y, c.y, d.y)), f += S(b.x, b.y), f += U(c.x, c.y), f += U(d.x, d.y));
  return f
}
z.bp = NaN;
z.iJ = j;
function hr(a, b) {
  b == h && (b = j);
  a.Mb().Ma = b;
  a.iJ = b
}
z.Jg = function(a, b, c) {
  return this.o.fj ? $q(this.ca(), this.ca().rc[this.bp], j) : this.Wq(a, b, c)
};
z.bg = function(a, b) {
  var c, d, f = this.o.Sb, g = this.o.Qa;
  switch(b) {
    case 0:
      c = this.jb + this.kh / 2;
      d = (f + g) / 2;
      break;
    case 7:
      c = this.jb + this.kh / 2;
      d = f;
      break;
    case 1:
      c = this.jb;
      d = (f + g) / 2;
      break;
    case 5:
      c = this.tc;
      d = (f + g) / 2;
      break;
    case 3:
      c = this.jb + this.kh / 2;
      d = g;
      break;
    case 8:
      c = this.jb;
      d = f;
      break;
    case 2:
      c = this.jb;
      d = g;
      break;
    case 6:
      c = this.tc;
      d = f;
      break;
    case 4:
      c = this.tc, d = g
  }
  f = this.ae();
  c *= Math.PI / 180;
  a.x = f.x + d * Math.cos(c);
  a.y = f.y + d * Math.sin(c)
};
z.Wa = function() {
  fr.f.Wa.call(this);
  this.Sn();
  this.oF();
  this.$a.oh(this.ya)
};
z.kt = s();
z.Cb = function() {
  fr.f.Cb.call(this);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  if(te(a) && this.Fd) {
    return this.Fd.Pa(a)
  }
  switch(a) {
    case "%YValue":
    ;
    case "%Value":
    ;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
    ;
    case "%YPercentOfCategory":
      return 2
  }
  return fr.f.Pa.call(this, a)
};
z.Se = function() {
  fr.f.Se.call(this);
  this.ma && (this.b.add("%YValue", this.ma), this.b.add("%Value", this.ma))
};
z.Na = function(a) {
  if(te(a) && this.Fd) {
    return this.Fd.Na(a)
  }
  ne(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.ma / this.o.Uf("%SeriesYSum")));
  ne(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.ma / this.ca().Na("%DataPlotYSum")));
  !ne(this.b, "%YPercentOfCategory") && this.Fd && this.b.add("%YPercentOfCategory", 100 * (this.ma / this.Fd().Na("%CategoryYSum")));
  return fr.f.Na.call(this, a)
};
function ir() {
  co.call(this);
  this.fj = q
}
A.e(ir, co);
z = ir.prototype;
z.fj = p;
z.jb = 0;
z.Sb = 0;
z.Qa = 0;
z.tn = x(j);
z.fG = 0;
z.qe = function() {
  return new fr
};
z.eA = function() {
  this.Sb = jr(this.cb.ca(), this.na, j);
  this.Qa = jr(this.cb.ca(), this.na, q)
};
z.Od = function(a, b) {
  ir.f.Od.call(this, a, b);
  this.fG += a.ta()
};
z.g = function(a, b) {
  ir.f.g.call(this, a, b);
  C(a, "start_angle") && (this.jb = M(a, "start_angle"));
  this.jb += this.cb.jb
};
z.Ug = function() {
  this.eA()
};
z.zn = function() {
  this.eA()
};
z.Cb = function() {
  ir.f.Cb.call(this);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%SeriesYMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYMin", Number.MAX_VALUE);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%MaxYValuePointName", "");
  this.b.add("%MinYValuePointName", "")
};
z.Pa = function(a) {
  if(re(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%SeriesLastYValue":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesYSum":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYAverage":
      return 2;
    case "%MaxYValuePointName":
    ;
    case "%MinYValuePointName":
      return 1
  }
  return ir.f.Pa.call(this, a)
};
z.th = function(a) {
  ir.f.th.call(this, a);
  a = a.b;
  oe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  oe(a, "%YBasedPointsCount", this.ba.length);
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  ir.f.Ed.call(this, a);
  oe(this.b, "%SeriesYSum", a.Xb());
  a.Xb() > this.b.get("%SeriesYMax") && (this.b.add("%SeriesYMax", a.Xb()), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  a.Xb() < this.b.get("%SeriesYMin") && (this.b.add("%SeriesYMin", a.Xb()), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.rh = function() {
  ir.f.rh.call(this);
  this.b.add("%SeriesYAverage", this.b.get("%SeriesYSum") / this.ba.length);
  this.ba && this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Xb());
  this.ba && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastYValue", this.Ga(this.ba.length - 1).Xb())
};
function kr() {
  fr.call(this)
}
A.e(kr, fr);
z = kr.prototype;
z.Ii = p;
z.bi = p;
z.nh = p;
z.ji = p;
z.Uh = p;
z.xh = p;
z.Qj = p;
z.xG = 0.001;
z.iH = q;
z.p = function(a) {
  this.Ii = new W(a);
  this.bi = new W(a);
  this.nh = new W(a);
  this.ji = new W(a);
  this.Uh = new W(a);
  kr.f.p.call(this, a);
  this.xh = new Sq(a);
  this.Qj = new Rq(a);
  return this.D
};
z.ae = function(a) {
  a || (a = new O);
  a.x = this.ca().ae().x;
  a.y = this.ca().ae().y;
  if(this.fn) {
    var b = this.ca().jk() * this.ca().ph, c = (this.jb + this.tc) / 2;
    a.x += Uc(this.ca().jk(), c);
    a.y += Vc(b, c)
  }
  return a
};
z.Uj = function() {
  var a = this.ae(), b = this.ca().Qa * this.ca().Pi;
  this.SK = new P(a.x - this.o.Qa, a.y - this.o.pj, 2 * this.o.Qa, 2 * this.o.pj);
  this.k = this.SK.Ca();
  this.k.height += b
};
z.HB = function() {
  kr.f.HB.call(this);
  if(!this.Gb) {
    this.ca().dk.oG.ia(this.nh);
    var a = this.Ii, b = this.o.na;
    this.ca().dk.ih.push({s:Math.sin((this.jb + this.xG) * Math.PI / 180), c:a, i:b});
    a = this.bi;
    b = this.o.na;
    this.ca().dk.ih.push({s:Math.sin((this.tc - this.xG) * Math.PI / 180), c:a, i:b});
    this.Gb || (Xn(this, this.Ii), Xn(this, this.bi), Xn(this, this.nh));
    if(this.xh.isEnabled(this.jb, this.tc)) {
      this.xh.p(this);
      this.ca().dk.Pq.push({d:this.xh, i:this.o.na});
      for(a = 0;a < this.xh.li();a++) {
        Xn(this, this.xh.fb(a))
      }
    }
    if(this.Qj.isEnabled(this.jb, this.tc)) {
      this.Qj.p(this);
      this.ca().dk.cq.push({d:this.Qj, i:this.o.na});
      for(a = 0;a < this.Qj.li();a++) {
        Xn(this, this.Qj.fb(a))
      }
    }
  }
};
function Kq(a, b) {
  if(NaN == b) {
    return""
  }
  var c = a.ae(), d = "", f = a.o.Qa, g = a.o.pj, k = a.o.Ro, l = a.ca().Qa * a.ca().Pi, k = new O(c.x + Uc(a.o.Sb, b), c.y + Vc(k, b)), c = new O(c.x + Uc(f, b), c.y + Vc(g, b)), f = new O(c.x, c.y + l), l = new O(k.x, k.y + l), d = d + S(k.x, k.y), d = d + U(c.x, c.y), d = d + U(f.x, f.y), d = d + U(l.x, l.y), d = d + U(k.x, k.y);
  return d + " Z"
}
z.RH = function(a, b, c, d) {
  var a = this.o.Qa, f = this.o.pj, g = (this.jb + this.tc) / 2;
  b.y += this.ca().Qa * this.ca().Pi / 2;
  b.x += Uc(a, g);
  b.y += Vc(f, g);
  this.iH || (0 > Math.sin(g * Math.PI / 180) ? this.ca().dk.tB.appendChild(this.Bf) : this.ca().dk.UC.appendChild(this.Bf), this.iH = j);
  return gr(this, b, c, d)
};
z.bg = function(a, b) {
  this.ae(a);
  var c = this.o.Qa, d = this.o.pj, f = this.o.Sb, g = this.o.Ro, k, l, n;
  switch(b) {
    case 0:
      k = this.jb + this.kh / 2;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 7:
      k = this.jb + this.kh / 2;
      l = f;
      n = g;
      break;
    case 1:
      k = this.jb;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 5:
      k = this.tc;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 3:
      k = this.jb + this.kh / 2;
      l = c;
      n = d;
      break;
    case 8:
      k = this.jb;
      l = f;
      n = g;
      break;
    case 2:
      k = this.jb;
      l = c;
      n = d;
      break;
    case 6:
      k = this.tc;
      l = f;
      n = g;
      break;
    case 4:
      k = this.tc, l = c, n = d
  }
  a.x += Uc(l, k);
  a.y += Vc(n, k)
};
function lr() {
  ir.call(this)
}
A.e(lr, ir);
lr.prototype.pj = p;
lr.prototype.Ro = p;
lr.prototype.qe = function() {
  return new kr
};
lr.prototype.eA = function() {
  lr.f.eA.call(this);
  this.pj = this.Qa * this.ca().ph;
  this.Ro = this.Sb * this.ca().ph
};
function mr() {
  fo.apply(this);
  this.Bw = j
}
A.e(mr, fo);
z = mr.prototype;
z.cI = 0.1;
z.jk = u("cI");
z.yD = q;
z.kJ = j;
z.Lt = j;
z.lK = 1;
z.og = u("lK");
z.Sb = 0.3;
z.jb = 0;
z.MG = q;
z.Qs = u("MG");
z.fj = q;
z.jE = q;
z.uE = 20;
z.yJ = q;
z.GK = q;
z.rt = p;
z.fx = q;
z.nJ = q;
z.Xi = 5;
z.lk = function() {
  return this.rt && this.fx
};
z.uh = function() {
  var a = this.aa.Id ? new lr : new ir;
  a.gh(16);
  return a
};
z.Ah = x("pie_series");
z.Rb = x("pie_style");
z.Qb = function(a) {
  return this.aa.Id ? new Oq : C(a, "name") && "aqua" == N(a, "name") ? new Dq : new Eq
};
z.Aq = function(a) {
  if(a == p || a == h) {
    a = q
  }
  var b = new Wq;
  b.aA(a);
  return b
};
z.g = function(a, b) {
  mr.f.g.call(this, a, b);
  C(a, "radius") && (this.lK = M(a, "radius") / 100);
  C(a, "inner_radius") && (this.Sb = M(a, "inner_radius") / 100);
  C(a, "explode_on_click") && (this.Lt = K(a, "explode_on_click"));
  C(a, "explode") && (this.cI = M(a, "explode") / 100);
  C(a, "allow_multiple_expand") && (this.kJ = K(a, "allow_multiple_expand"));
  C(a, "start_angle") && (this.jb = M(a, "start_angle"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    C(c, "mode") && (this.yJ = j, this.fj = "outside" == N(c, "mode"));
    C(c, "smart_labels") && (this.GK = K(c, "smart_labels"));
    if(this.fj && (C(c, "allow_overlap") && (this.MG = K(c, "allow_overlap")), C(c, "position") && (c = F(c, "position"), C(c, "padding") && (c = F(c, "padding"), this.uE = (this.jE = Mb(c)) ? Nb(c) : Ob(c))), c = F(a, "connector"), Tb(c))) {
      this.fx = j, this.rt = new Kd, this.rt.g(c), C(c, "padding") && (c = F(c, "padding"), Mb(c) ? (this.nJ = j, this.Xi = Nb(c)) : this.Xi = Ob(c))
    }
  }
};
z.Tw = x(j);
z.$s = x(q);
z.GC = function(a, b) {
  b ? a.sort(function(a, b) {
    return b.Xb() - a.Xb()
  }) : a.sort(function(a, b) {
    return a.Xb() - b.Xb()
  })
};
z.bI = function(a, b) {
  a = a.slice();
  b ? a.sort(function(a, b) {
    return M(b, "y") - M(a, "y")
  }) : a.sort(function(a, b) {
    return M(a, "y") - M(b, "y")
  });
  return a
};
var nr = {chart:{styles:{pie_style:[{fill:{type:"solid", color:"%color", opacity:"1"}, border:{type:"solid", color:"DarkColor(%color)"}, states:{hover:{fill:{color:"Blend(White,%Color,0.3)"}}}, name:"anychart_default"}, {fill:{color:"%Color"}, states:{hover:{fill:{color:"Blend(White,%Color,0.9)"}}, pushed:{color:"rgb(200,200,200)"}, selected_hover:{fill:{color:"Blend(White,%Color,0.3)"}}, selected_normal:{fill:{color:"Blend(White,%Color,0.3)"}}}, name:"aqua"}]}, data_plot_settings:{pie_series:{animation:{style:"defaultPie"}, 
label_settings:{position:{padding:"20"}, animation:{style:"defaultLabel"}}, marker_settings:{animation:{style:"defaultMarker"}}, connector:{enabled:"true"}, tooltip_settings:{}, style:"Aqua"}}, chart_settings:{data_plot_background:{enabled:"false"}}}};
function or() {
}
A.e(or, bl);
or.prototype.zh = function() {
  return this.$f(or.f.zh.call(this), nr)
};
/*

 Copyright (c) 2000-2004, Kevin Lindsey
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 - Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.

 - Neither the name of this software nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
var Fo, Go, To, pr, qr;
(function() {
  function a() {
    this.Ha()
  }
  function b() {
    this.Ha()
  }
  function c() {
    this.Ha()
  }
  function d(a) {
    0 < arguments.length && this.Ha(a)
  }
  function f(a) {
    0 < arguments.length && this.Ha(a)
  }
  function g(a, b) {
    0 < arguments.length && this.Ha(a, b)
  }
  function k(a, b) {
    0 < arguments.length && (this.x = a, this.y = b)
  }
  function l() {
    this.Ha(arguments)
  }
  function n(a, b) {
    0 < arguments.length && (this.x = a, this.y = b)
  }
  function m(a) {
    0 < arguments.length && this.Ha(a)
  }
  function o(a) {
    0 < arguments.length && this.Ha(a)
  }
  function v(a) {
    0 < arguments.length && this.Ha(a)
  }
  function w(a, b, c) {
    0 < arguments.length && this.Ha(a, b, c)
  }
  function y(a, b, c, d, f) {
    0 < arguments.length && this.Ha(a, b, c, d, f)
  }
  function E(a, b, c) {
    0 < arguments.length && this.Ha(a, b, c)
  }
  function V(a) {
    0 < arguments.length && this.Ha(a)
  }
  function R(a, b) {
    0 < arguments.length && this.Ha(a, b)
  }
  function T(a) {
    0 < arguments.length && this.Ha(a)
  }
  function ca(a, b, c, d) {
    0 < arguments.length && this.Ha(a, b, c, d)
  }
  function ra(a, b, c) {
    0 < arguments.length && this.Ha("A", a, b, c)
  }
  function Qc(a, b, c) {
    0 < arguments.length && this.Ha("Q", a, b, c)
  }
  function yb(a, b, c) {
    0 < arguments.length && this.Ha("C", a, b, c)
  }
  function Y(a, b, c) {
    0 < arguments.length && this.Ha("H", a, b, c)
  }
  function L(a, b, c) {
    0 < arguments.length && this.Ha("L", a, b, c)
  }
  function Ye(a, b, c) {
    0 < arguments.length && this.Ha("M", a, b, c)
  }
  function Ze(a, b, c) {
    0 < arguments.length && this.Ha("T", a, b, c)
  }
  function Pd(a, b, c) {
    0 < arguments.length && this.Ha("S", a, b, c)
  }
  function ia(a, b, c, d) {
    0 < arguments.length && this.Ha(a, b, c, d)
  }
  function Qd(a, b, c) {
    0 < arguments.length && this.Ha("z", a, b, c)
  }
  function $e(a, b, c) {
    0 < arguments.length && this.Ha("q", a, b, c)
  }
  function af(a, b, c) {
    0 < arguments.length && this.Ha("c", a, b, c)
  }
  function bf(a, b, c) {
    0 < arguments.length && this.Ha("l", a, b, c)
  }
  function cf(a, b, c) {
    0 < arguments.length && this.Ha("m", a, b, c)
  }
  function df(a, b, c) {
    0 < arguments.length && this.Ha("t", a, b, c)
  }
  function Rd(a, b, c) {
    0 < arguments.length && this.Ha("s", a, b, c)
  }
  function bd(a) {
    0 < arguments.length && this.Ha(a)
  }
  function sc(a) {
    0 < arguments.length && this.Ha(a)
  }
  Array.prototype.kM = function(a) {
    for(var b = this.length, c = 0;c < b;c++) {
      a(this[c])
    }
  };
  Array.prototype.map = function(a) {
    for(var b = this.length, c = [], d = 0;d < b;d++) {
      c.push(a(this[d]))
    }
    return c
  };
  Array.prototype.min = function() {
    for(var a = this.length, b = this[0], c = 0;c < a;c++) {
      var d = this[c];
      d < b && (b = d)
    }
    return b
  };
  Array.prototype.max = function() {
    for(var a = this.length, b = this[0], c = 0;c < a;c++) {
      var d = this[c]
    }
    d > b && (b = d);
    return b
  };
  a.VERSION = "1.2";
  a.prototype.Ha = function() {
    var a = svgDocument.documentElement;
    this.BA = [];
    this.scale = 1;
    this.zJ = a.vH();
    a.addEventListener("SVGZoom", this, q);
    a.addEventListener("SVGScroll", this, q);
    a.addEventListener("SVGResize", this, q)
  };
  a.prototype.handleEvent = function(a) {
    var b = a.type;
    this[b] == p && e(Error("Unsupported event type: " + b));
    this[b](a)
  };
  a.prototype.update = function() {
    if(0 < this.BA.length) {
      for(var a = svgDocument.documentElement, b = window.zP != p ? new d(a) : p, b = b != p ? b.HM() : a.vH(), c = a.kQ, b = b.scale(1 / a.jQ), b = b.translate(-c.x, -c.y), a = 0;a < this.BA.length;a++) {
        c = b.multiply(this.zJ.multiply(this.BA[a].BQ())), c = "matrix(" + [c.CP, c.PP, c.YP, c.mQ, c.qQ, c.uQ].join() + ")", this.BA[a].setAttributeNS(p, "transform", c)
      }
      this.zJ = b.inverse()
    }
  };
  b.VERSION = 1;
  b.prototype.Ha = s();
  b.prototype.handleEvent = function(a) {
    this[a.type] == p && e(Error("Unsupported event type: " + a.type));
    this[a.type](a)
  };
  c.prototype = new b;
  c.prototype.constructor = c;
  c.hc = b.prototype;
  c.prototype.Ha = function() {
    this.kb = p;
    this.lb = [];
    this.NF = [];
    this.xd()
  };
  c.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "rect");
      this.kb = a;
      a.setAttributeNS(p, "x", "-16384");
      a.setAttributeNS(p, "y", "-16384");
      a.setAttributeNS(p, "width", "32767");
      a.setAttributeNS(p, "height", "32767");
      a.setAttributeNS(p, "fill", "none");
      a.setAttributeNS(p, "pointer-events", "all");
      a.setAttributeNS(p, "display", "none");
      svgDocument.documentElement.appendChild(a)
    }
  };
  c.prototype.lF = function(a) {
    if(-1 == this.LM(a)) {
      var b = a.wg;
      a.select(j);
      this.lb.push(a);
      b != p && -1 == this.UN(b) && this.NF.push(b)
    }
  };
  c.prototype.LM = function(a) {
    for(var b = -1, c = 0;c < this.lb.length;c++) {
      if(this.lb[c] === a) {
        b = c;
        break
      }
    }
    return b
  };
  c.prototype.UN = function(a) {
    for(var b = -1, c = 0;c < this.NF.length;c++) {
      if(this.NF[c] === a) {
        b = c;
        break
      }
    }
    return b
  };
  d.VERSION = "1.0";
  d.prototype.Ha = function(a) {
    var b = a.getAttributeNS(p, "viewBox"), a = a.getAttributeNS(p, "preserveAspectRatio");
    b != p && "" != b ? (b = b.split(/\s*,\s*|\s+/), this.x = parseFloat(b[0]), this.y = parseFloat(b[1]), this.width = parseFloat(b[2]), this.height = parseFloat(b[3])) : (this.y = this.x = 0, this.width = innerWidth, this.height = innerHeight);
    this.SN(a)
  };
  d.prototype.HM = function() {
    var a = svgDocument.documentElement, b = svgDocument.documentElement.vH(), c = a.getAttributeNS(p, "width"), d = a.getAttributeNS(p, "height"), c = c != p && "" != c ? parseFloat(c) : window.innerWidth ? innerWidth : document.documentElement.viewport.width, d = d != p && "" != d ? parseFloat(d) : window.innerHeight ? innerHeight : document.documentElement.viewport.height, a = this.width / c, f = this.height / d, b = b.translate(this.x, this.y);
    if("none" == this.Ps) {
      b = b.UR(a, f)
    }else {
      if(a < f && "meet" == this.Ru || a > f && "slice" == this.Ru) {
        a = 0, c = c * f - this.width, "Mid" == this.Ps ? a = -c / 2 : "Max" == this.Ps && (a = -c), b = b.translate(a, 0), b = b.scale(f)
      }else {
        if(a > f && "meet" == this.Ru || a < f && "slice" == this.Ru) {
          c = 0, f = d * a - this.height, "Mid" == this.xw ? c = -f / 2 : "Max" == this.xw && (c = -f), b = b.translate(0, c)
        }
        b = b.scale(a)
      }
    }
    return b
  };
  d.prototype.SN = function(a) {
    if(a) {
      var a = a.split(/\s+/), b = a[0];
      "none" == b ? this.xw = this.Ps = "none" : (this.Ps = b.substring(1, 4), this.xw = b.substring(5, 9));
      this.Ru = 2 == a.length ? a[1] : "meet"
    }else {
      this.align = "xMidYMid", this.xw = this.Ps = "Mid", this.Ru = "meet"
    }
  };
  f.prototype.Ha = function(a) {
    this.status = a;
    this.Fa = []
  };
  f.prototype.Oj = function(a) {
    this.Fa.push(a)
  };
  f.prototype.qc = function(a) {
    this.Fa = this.Fa.concat(a)
  };
  var Su = {intersectEllipseRectangle:function(a, b) {
    return f.$M.apply(a, b)
  }, intersectLineRectangle:function(a, b) {
    return f.nr.apply(a, b)
  }, intersectPolygonRectangle:function(a, b) {
    return f.XD.apply(a, b)
  }, intersectLineLine:function(a, b) {
    return f.mr.apply(a, b)
  }};
  f.YI = function(a, b) {
    var c = a.Ee(), d = b.Ee(), g;
    c != p && d != p ? "Path" == c.name ? g = f.XI(a, b) : "Path" == d.name ? g = f.XI(b, a) : (c.name < d.name ? (g = "intersect" + c.name + d.name, c = c.wz.concat(d.wz)) : (g = "intersect" + d.name + c.name, c = d.wz.concat(c.wz)), g = Su[g](p, c)) : g = new f("No Intersection");
    return g
  };
  f.XI = function(a, b) {
    return a.aN(b)
  };
  f.ZQ = function(a, b, c, d, g, n) {
    var m, o, v = new f("No Intersection");
    m = b.multiply(-2);
    c = a.add(m.add(c));
    m = a.multiply(-2);
    o = b.multiply(2);
    b = m.add(o);
    a = new k(a.x, a.y);
    m = g.multiply(-2);
    n = d.add(m.add(n));
    m = d.multiply(-2);
    o = g.multiply(2);
    g = m.add(o);
    d = new k(d.x, d.y);
    m = c.x * b.y - b.x * c.y;
    o = n.x * b.y - b.x * n.y;
    var w = n.x * c.y - c.x * n.y, y = g.x * c.y - c.x * g.y, E = c.x * (a.y - d.y) + c.y * (-a.x + d.x);
    m = (new l(-w * w, -2 * w * y, m * o - y * y - 2 * w * E, m * (g.x * b.y - b.x * g.y) - 2 * y * E, m * (b.x * (a.y - d.y) + b.y * (-a.x + d.x)) - E * E)).mi();
    for(o = 0;o < m.length;o++) {
      if(w = m[o], 0 <= w && 1 >= w && (y = (new l(-c.x, -b.x, -a.x + d.x + w * g.x + w * w * n.x)).mi(), E = (new l(-c.y, -b.y, -a.y + d.y + w * g.y + w * w * n.y)).mi(), 0 < y.length && 0 < E.length)) {
        var T = 0;
        a:for(;T < y.length;T++) {
          var L = y[T];
          if(0 <= L && 1 >= L) {
            for(var R = 0;R < E.length;R++) {
              if(1.0E-4 > Math.abs(L - E[R])) {
                v.Fa.push(n.multiply(w * w).add(g.multiply(w).add(d)));
                break a
              }
            }
          }
        }
      }
    }
    return v
  };
  f.$Q = function(a, b, c, d, g, ma, m) {
    var o, w, v, y = new f("No Intersection");
    o = b.multiply(-2);
    c = a.add(o.add(c));
    o = a.multiply(-2);
    w = b.multiply(2);
    b = o.add(w);
    a = new k(a.x, a.y);
    o = d.multiply(-1);
    w = g.multiply(3);
    v = ma.multiply(-3);
    o = o.add(w.add(v.add(m)));
    m = new n(o.x, o.y);
    o = d.multiply(3);
    w = g.multiply(-6);
    v = ma.multiply(3);
    o = o.add(w.add(v));
    ma = new n(o.x, o.y);
    o = d.multiply(-3);
    w = g.multiply(3);
    v = o.add(w);
    g = new n(v.x, v.y);
    d = new n(d.x, d.y);
    o = b.x * b.x;
    w = b.y * b.y;
    v = c.x * c.x;
    var E = c.y * c.y;
    o = (new l(-2 * c.x * c.y * m.x * m.y + v * m.y * m.y + E * m.x * m.x, -2 * c.x * c.y * ma.x * m.y - 2 * c.x * c.y * ma.y * m.x + 2 * E * ma.x * m.x + 2 * v * ma.y * m.y, -2 * c.x * g.x * c.y * m.y - 2 * c.x * c.y * g.y * m.x - 2 * c.x * c.y * ma.x * ma.y + 2 * g.x * E * m.x + E * ma.x * ma.x + v * (2 * g.y * m.y + ma.y * ma.y), 2 * a.x * c.x * c.y * m.y + 2 * a.y * c.x * c.y * m.x + b.x * b.y * c.x * m.y + b.x * b.y * c.y * m.x - 2 * d.x * c.x * c.y * m.y - 2 * c.x * d.y * c.y * m.x - 2 * c.x * 
    g.x * c.y * ma.y - 2 * c.x * c.y * g.y * ma.x - 2 * a.x * E * m.x - 2 * a.y * v * m.y + 2 * d.x * E * m.x + 2 * g.x * E * ma.x - w * c.x * m.x - o * c.y * m.y + v * (2 * d.y * m.y + 2 * g.y * ma.y), 2 * a.x * c.x * c.y * ma.y + 2 * a.y * c.x * c.y * ma.x + b.x * b.y * c.x * ma.y + b.x * b.y * c.y * ma.x - 2 * d.x * c.x * c.y * ma.y - 2 * c.x * d.y * c.y * ma.x - 2 * c.x * g.x * c.y * g.y - 2 * a.x * E * ma.x - 2 * a.y * v * ma.y + 2 * d.x * E * ma.x - w * c.x * ma.x - o * c.y * ma.y + g.x * g.x * 
    E + v * (2 * d.y * ma.y + g.y * g.y), 2 * a.x * c.x * c.y * g.y + 2 * a.y * c.x * g.x * c.y + b.x * b.y * c.x * g.y + b.x * b.y * g.x * c.y - 2 * d.x * c.x * c.y * g.y - 2 * c.x * d.y * g.x * c.y - 2 * a.x * g.x * E - 2 * a.y * v * g.y + 2 * d.x * g.x * E - w * c.x * g.x - o * c.y * g.y + 2 * v * d.y * g.y, -2 * a.x * a.y * c.x * c.y - a.x * b.x * b.y * c.y - a.y * b.x * b.y * c.x + 2 * a.x * c.x * d.y * c.y + 2 * a.y * d.x * c.x * c.y + b.x * d.x * b.y * c.y + b.x * b.y * c.x * d.y - 2 * d.x * 
    c.x * d.y * c.y - 2 * a.x * d.x * E + a.x * w * c.x + a.y * o * c.y - 2 * a.y * v * d.y - d.x * w * c.x - o * d.y * c.y + a.x * a.x * E + a.y * a.y * v + d.x * d.x * E + v * d.y * d.y)).cy(0, 1);
    for(w = 0;w < o.length;w++) {
      v = o[w];
      var E = (new l(c.x, b.x, a.x - d.x - v * g.x - v * v * ma.x - v * v * v * m.x)).mi(), T = (new l(c.y, b.y, a.y - d.y - v * g.y - v * v * ma.y - v * v * v * m.y)).mi();
      if(0 < E.length && 0 < T.length) {
        var L = 0;
        a:for(;L < E.length;L++) {
          var R = E[L];
          if(0 <= R && 1 >= R) {
            for(var X = 0;X < T.length;X++) {
              if(1.0E-4 > Math.abs(R - T[X])) {
                y.Fa.push(m.multiply(v * v * v).add(ma.multiply(v * v).add(g.multiply(v).add(d))));
                break a
              }
            }
          }
        }
      }
    }
    0 < y.Fa.length && (y.status = "Intersection");
    return y
  };
  f.aR = function(a, b, c, d, g) {
    return f.WM(a, b, c, d, g, g)
  };
  f.WM = function(a, b, c, d, g, n) {
    var m, o = new f("No Intersection");
    m = b.multiply(-2);
    c = a.add(m.add(c));
    m = a.multiply(-2);
    b = b.multiply(2);
    m = m.add(b);
    a = new k(a.x, a.y);
    g *= g;
    n *= n;
    d = (new l(n * c.x * c.x + g * c.y * c.y, 2 * (n * c.x * m.x + g * c.y * m.y), n * (2 * c.x * a.x + m.x * m.x) + g * (2 * c.y * a.y + m.y * m.y) - 2 * (n * d.x * c.x + g * d.y * c.y), 2 * (n * m.x * (a.x - d.x) + g * m.y * (a.y - d.y)), n * (a.x * a.x + d.x * d.x) + g * (a.y * a.y + d.y * d.y) - 2 * (n * d.x * a.x + g * d.y * a.y) - g * n)).mi();
    for(n = 0;n < d.length;n++) {
      g = d[n], 0 <= g && 1 >= g && o.Fa.push(c.multiply(g * g).add(m.multiply(g).add(a)))
    }
    0 < o.Fa.length && (o.status = "Intersection");
    return o
  };
  f.pu = function(a, b, c, d, g) {
    var m, o, Sg, v, w = d.min(g), y = d.max(g), E = new f("No Intersection");
    m = b.multiply(-2);
    Sg = a.add(m.add(c));
    m = a.multiply(-2);
    o = b.multiply(2);
    m = m.add(o);
    o = new k(a.x, a.y);
    v = new n(d.y - g.y, g.x - d.x);
    roots = (new l(v.gk(Sg), v.gk(m), v.gk(o) + (d.x * g.y - g.x * d.y))).mi();
    for(Sg = 0;Sg < roots.length;Sg++) {
      m = roots[Sg], 0 <= m && 1 >= m && (m = a.rg(b, m).rg(b.rg(c, m), m), d.x == g.x ? w.y <= m.y && m.y <= y.y && (E.status = "Intersection", E.Oj(m)) : d.y == g.y ? w.x <= m.x && m.x <= y.x && (E.status = "Intersection", E.Oj(m)) : m.KI(w) && m.GJ(y) && (E.status = "Intersection", E.Oj(m)))
    }
    return E
  };
  f.bR = function(a, b, c, d) {
    for(var g = new f("No Intersection"), k = d.length, l = 0;l < k;l++) {
      var n = f.pu(a, b, c, d[l], d[(l + 1) % k]);
      g.qc(n.Fa)
    }
    0 < g.Fa.length && (g.status = "Intersection");
    return g
  };
  f.cR = function(a, b, c, d, g) {
    var l = d.min(g), n = d.max(g), g = new k(n.x, l.y), m = new k(l.x, n.y), d = f.pu(a, b, c, l, g), g = f.pu(a, b, c, g, n), n = f.pu(a, b, c, n, m), a = f.pu(a, b, c, m, l), b = new f("No Intersection");
    b.qc(d.Fa);
    b.qc(g.Fa);
    b.qc(n.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.dR = function(a, b, c, d, g, k, m, o) {
    var v, w, y, E = new f("No Intersection");
    v = a.multiply(-1);
    w = b.multiply(3);
    y = c.multiply(-3);
    v = v.add(w.add(y.add(d)));
    d = new n(v.x, v.y);
    v = a.multiply(3);
    w = b.multiply(-6);
    y = c.multiply(3);
    v = v.add(w.add(y));
    c = new n(v.x, v.y);
    v = a.multiply(-3);
    w = b.multiply(3);
    y = v.add(w);
    b = new n(y.x, y.y);
    a = new n(a.x, a.y);
    v = g.multiply(-1);
    w = k.multiply(3);
    y = m.multiply(-3);
    v = v.add(w.add(y.add(o)));
    o = new n(v.x, v.y);
    v = g.multiply(3);
    w = k.multiply(-6);
    y = m.multiply(3);
    v = v.add(w.add(y));
    m = new n(v.x, v.y);
    v = g.multiply(-3);
    w = k.multiply(3);
    y = v.add(w);
    k = new n(y.x, y.y);
    g = new n(g.x, g.y);
    v = a.x * a.x;
    w = a.y * a.y;
    y = b.x * b.x;
    var T = b.x * b.x * b.x, L = b.y * b.y, R = b.y * b.y * b.y, X = c.x * c.x, V = c.x * c.x * c.x, Z = c.y * c.y, ca = c.y * c.y * c.y, G = d.x * d.x, Y = d.x * d.x * d.x, D = d.y * d.y, ja = d.y * d.y * d.y, ra = g.x * g.x, ia = g.y * g.y, pb = k.x * k.x, bb = k.y * k.y, qb = m.x * m.x, cb = m.y * m.y, rb = o.x * o.x, sb = o.y * o.y;
    v = (new l(-Y * o.y * o.y * o.y + ja * o.x * o.x * o.x - 3 * d.x * D * rb * o.y + 3 * G * d.y * o.x * sb, -6 * d.x * m.x * D * o.x * o.y + 6 * G * d.y * m.y * o.x * o.y + 3 * m.x * ja * rb - 3 * Y * m.y * sb - 3 * d.x * D * m.y * rb + 3 * G * m.x * d.y * sb, -6 * k.x * d.x * D * o.x * o.y - 6 * d.x * m.x * D * m.y * o.x + 6 * G * m.x * d.y * m.y * o.y + 3 * k.x * ja * rb + 3 * qb * ja * o.x + 3 * k.x * G * d.y * sb - 3 * d.x * k.y * D * rb - 3 * d.x * qb * D * o.y + G * d.y * o.x * (6 * k.y * 
    o.y + 3 * cb) + Y * (-k.y * sb - 2 * cb * o.y - o.y * (2 * k.y * o.y + cb)), b.x * c.y * d.x * d.y * o.x * o.y - b.y * c.x * d.x * d.y * o.x * o.y + 6 * k.x * m.x * ja * o.x + 3 * b.x * c.x * d.x * d.y * sb + 6 * a.x * d.x * D * o.x * o.y - 3 * b.x * c.x * D * o.x * o.y - 3 * b.y * c.y * d.x * d.y * rb - 6 * a.y * G * d.y * o.x * o.y - 6 * g.x * d.x * D * o.x * o.y + 3 * b.y * c.y * G * o.x * o.y - 2 * c.x * Z * d.x * o.x * o.y - 6 * k.x * d.x * m.x * D * o.y - 6 * k.x * d.x * D * m.y * o.x - 
    6 * d.x * k.y * m.x * D * o.x + 6 * k.x * G * d.y * m.y * o.y + 2 * X * c.y * d.y * o.x * o.y + m.x * m.x * m.x * ja - 3 * a.x * ja * rb + 3 * a.y * Y * sb + 3 * g.x * ja * rb + ca * d.x * rb - V * d.y * sb - 3 * a.x * G * d.y * sb + 3 * a.y * d.x * D * rb - 2 * b.x * c.y * G * sb + b.x * c.y * D * rb - b.y * c.x * G * sb + 2 * b.y * c.x * D * rb + 3 * g.x * G * d.y * sb - c.x * Z * d.y * rb - 3 * g.y * d.x * D * rb + X * c.y * d.x * sb - 3 * d.x * qb * D * m.y + G * d.y * o.x * (6 * g.y * o.y + 
    6 * k.y * m.y) + G * m.x * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * k.y * m.y * o.y - g.y * sb - m.y * (2 * k.y * o.y + cb) - o.y * (2 * g.y * o.y + 2 * k.y * m.y)), 6 * b.x * c.x * d.x * d.y * m.y * o.y + b.x * c.y * d.x * m.x * d.y * o.y + b.x * c.y * d.x * d.y * m.y * o.x - b.y * c.x * d.x * m.x * d.y * o.y - b.y * c.x * d.x * d.y * m.y * o.x - 6 * b.y * c.y * d.x * m.x * d.y * o.x - 6 * a.x * m.x * ja * o.x + 6 * g.x * m.x * ja * o.x + 6 * a.y * Y * m.y * o.y + 2 * ca * d.x * m.x * o.x - 
    2 * V * d.y * m.y * o.y + 6 * a.x * d.x * m.x * D * o.y + 6 * a.x * d.x * D * m.y * o.x + 6 * a.y * d.x * m.x * D * o.x - 3 * b.x * c.x * m.x * D * o.y - 3 * b.x * c.x * D * m.y * o.x + 2 * b.x * c.y * m.x * D * o.x + 4 * b.y * c.x * m.x * D * o.x - 6 * a.x * G * d.y * m.y * o.y - 6 * a.y * G * m.x * d.y * o.y - 6 * a.y * G * d.y * m.y * o.x - 4 * b.x * c.y * G * m.y * o.y - 6 * g.x * d.x * m.x * D * o.y - 6 * g.x * d.x * D * m.y * o.x - 2 * b.y * c.x * G * m.y * o.y + 3 * b.y * c.y * G * m.x * 
    o.y + 3 * b.y * c.y * G * m.y * o.x - 2 * c.x * Z * d.x * m.x * o.y - 2 * c.x * Z * d.x * m.y * o.x - 2 * c.x * Z * m.x * d.y * o.x - 6 * g.y * d.x * m.x * D * o.x - 6 * k.x * d.x * k.y * D * o.x - 6 * k.x * d.x * m.x * D * m.y + 6 * g.x * G * d.y * m.y * o.y + 2 * X * c.y * d.x * m.y * o.y + 2 * X * c.y * m.x * d.y * o.y + 2 * X * c.y * d.y * m.y * o.x + 3 * k.x * qb * ja + 3 * pb * ja * o.x - 3 * d.x * k.y * qb * D - 3 * pb * d.x * D * o.y + G * m.x * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + 
    G * d.y * o.x * (6 * g.y * m.y + 3 * bb) + k.x * G * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * g.y * m.y * o.y - o.y * (2 * g.y * m.y + bb) - k.y * (2 * k.y * o.y + cb) - m.y * (2 * g.y * o.y + 2 * k.y * m.y)), b.x * k.x * c.y * d.x * d.y * o.y + b.x * c.y * d.x * k.y * d.y * o.x + b.x * c.y * d.x * m.x * d.y * m.y - b.y * c.x * k.x * d.x * d.y * o.y - b.y * c.x * d.x * k.y * d.y * o.x - b.y * c.x * d.x * m.x * d.y * m.y - 6 * b.y * k.x * c.y * d.x * d.y * o.x - 6 * a.x * k.x * ja * o.x + 6 * 
    g.x * k.x * ja * o.x + 2 * k.x * ca * d.x * o.x + 6 * a.x * k.x * d.x * D * o.y + 6 * a.x * d.x * k.y * D * o.x + 6 * a.x * d.x * m.x * D * m.y + 6 * a.y * k.x * d.x * D * o.x - 3 * b.x * c.x * k.x * D * o.y - 3 * b.x * c.x * k.y * D * o.x - 3 * b.x * c.x * m.x * D * m.y + 2 * b.x * k.x * c.y * D * o.x + 4 * b.y * c.x * k.x * D * o.x - 6 * a.y * k.x * G * d.y * o.y - 6 * a.y * G * k.y * d.y * o.x - 6 * a.y * G * m.x * d.y * m.y - 6 * g.x * k.x * d.x * D * o.y - 6 * g.x * d.x * k.y * D * o.x - 
    6 * g.x * d.x * m.x * D * m.y + 3 * b.y * k.x * c.y * G * o.y - 3 * b.y * c.y * d.x * qb * d.y + 3 * b.y * c.y * G * k.y * o.x + 3 * b.y * c.y * G * m.x * m.y - 2 * c.x * k.x * Z * d.x * o.y - 2 * c.x * k.x * Z * d.y * o.x - 2 * c.x * Z * d.x * k.y * o.x - 2 * c.x * Z * d.x * m.x * m.y - 6 * g.y * k.x * d.x * D * o.x - 6 * k.x * d.x * k.y * m.x * D + 6 * g.y * G * k.y * d.y * o.x + 2 * X * k.x * c.y * d.y * o.y + 2 * X * c.y * k.y * d.y * o.x + 2 * X * c.y * m.x * d.y * m.y - 3 * a.x * qb * ja + 
    3 * g.x * qb * ja + 3 * pb * m.x * ja + ca * d.x * qb + 3 * a.y * d.x * qb * D + b.x * c.y * qb * D + 2 * b.y * c.x * qb * D - c.x * Z * qb * d.y - 3 * g.y * d.x * qb * D - 3 * pb * d.x * D * m.y + X * c.y * d.x * (2 * k.y * o.y + cb) + b.x * c.x * d.x * d.y * (6 * k.y * o.y + 3 * cb) + k.x * G * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + V * d.y * (-2 * k.y * o.y - cb) + a.y * Y * (6 * k.y * o.y + 3 * cb) + b.y * c.x * G * (-2 * k.y * o.y - cb) + b.x * c.y * G * (-4 * k.y * o.y - 2 * cb) + a.x * 
    G * d.y * (-6 * k.y * o.y - 3 * cb) + G * m.x * d.y * (6 * g.y * m.y + 3 * bb) + g.x * G * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * g.y * k.y * o.y - m.y * (2 * g.y * m.y + bb) - g.y * (2 * k.y * o.y + cb) - k.y * (2 * g.y * o.y + 2 * k.y * m.y)), -a.x * b.x * c.y * d.x * d.y * o.y + a.x * b.y * c.x * d.x * d.y * o.y + 6 * a.x * b.y * c.y * d.x * d.y * o.x - 6 * a.y * b.x * c.x * d.x * d.y * o.y - a.y * b.x * c.y * d.x * d.y * o.x + a.y * b.y * c.x * d.x * d.y * o.x + b.x * b.y * c.x * c.y * 
    d.x * o.y - b.x * b.y * c.x * c.y * d.y * o.x + b.x * g.x * c.y * d.x * d.y * o.y + b.x * g.y * c.y * d.x * d.y * o.x + b.x * k.x * c.y * d.x * d.y * m.y + b.x * c.y * d.x * k.y * m.x * d.y - g.x * b.y * c.x * d.x * d.y * o.y - 6 * g.x * b.y * c.y * d.x * d.y * o.x - b.y * c.x * g.y * d.x * d.y * o.x - b.y * c.x * k.x * d.x * d.y * m.y - b.y * c.x * d.x * k.y * m.x * d.y - 6 * b.y * k.x * c.y * d.x * m.x * d.y - 6 * a.x * g.x * ja * o.x - 6 * a.x * k.x * m.x * ja - 2 * a.x * ca * d.x * o.x + 
    6 * g.x * k.x * m.x * ja + 2 * g.x * ca * d.x * o.x + 2 * k.x * ca * d.x * m.x + 2 * a.y * V * d.y * o.y - 6 * a.x * a.y * d.x * D * o.x + 3 * a.x * b.x * c.x * D * o.y - 2 * a.x * b.x * c.y * D * o.x - 4 * a.x * b.y * c.x * D * o.x + 3 * a.y * b.x * c.x * D * o.x + 6 * a.x * a.y * G * d.y * o.y + 6 * a.x * g.x * d.x * D * o.y - 3 * a.x * b.y * c.y * G * o.y + 2 * a.x * c.x * Z * d.x * o.y + 2 * a.x * c.x * Z * d.y * o.x + 6 * a.x * g.y * d.x * D * o.x + 6 * a.x * k.x * d.x * D * m.y + 6 * a.x * 
    d.x * k.y * m.x * D + 4 * a.y * b.x * c.y * G * o.y + 6 * a.y * g.x * d.x * D * o.x + 2 * a.y * b.y * c.x * G * o.y - 3 * a.y * b.y * c.y * G * o.x + 2 * a.y * c.x * Z * d.x * o.x + 6 * a.y * k.x * d.x * m.x * D - 3 * b.x * g.x * c.x * D * o.y + 2 * b.x * g.x * c.y * D * o.x + b.x * b.y * Z * d.x * o.x - 3 * b.x * c.x * g.y * D * o.x - 3 * b.x * c.x * k.x * D * m.y - 3 * b.x * c.x * k.y * m.x * D + 2 * b.x * k.x * c.y * m.x * D + 4 * g.x * b.y * c.x * D * o.x + 4 * b.y * c.x * k.x * m.x * D - 
    2 * a.x * X * c.y * d.y * o.y - 6 * a.y * g.x * G * d.y * o.y - 6 * a.y * g.y * G * d.y * o.x - 6 * a.y * k.x * G * d.y * m.y - 2 * a.y * X * c.y * d.x * o.y - 2 * a.y * X * c.y * d.y * o.x - 6 * a.y * G * k.y * m.x * d.y - b.x * b.y * X * d.y * o.y - 2 * b.x * L * d.x * d.y * o.x + 3 * g.x * b.y * c.y * G * o.y - 2 * g.x * c.x * Z * d.x * o.y - 2 * g.x * c.x * Z * d.y * o.x - 6 * g.x * g.y * d.x * D * o.x - 6 * g.x * k.x * d.x * D * m.y - 6 * g.x * d.x * k.y * m.x * D + 3 * b.y * g.y * c.y * 
    G * o.x + 3 * b.y * k.x * c.y * G * m.y + 3 * b.y * c.y * G * k.y * m.x - 2 * c.x * g.y * Z * d.x * o.x - 2 * c.x * k.x * Z * d.x * m.y - 2 * c.x * k.x * Z * m.x * d.y - 2 * c.x * Z * d.x * k.y * m.x - 6 * g.y * k.x * d.x * m.x * D - L * c.x * c.y * d.x * o.x + 2 * g.x * X * c.y * d.y * o.y + 6 * g.y * G * k.y * m.x * d.y + 2 * y * b.y * d.x * d.y * o.y + y * c.x * c.y * d.y * o.y + 2 * X * g.y * c.y * d.y * o.x + 2 * X * k.x * c.y * d.y * m.y + 2 * X * c.y * k.y * m.x * d.y + k.x * k.x * k.x * 
    ja + 3 * v * ja * o.x - 3 * w * Y * o.y + 3 * ra * ja * o.x + R * G * o.x - T * D * o.y - b.x * L * G * o.y + y * b.y * D * o.x - 3 * v * d.x * D * o.y + 3 * w * G * d.y * o.x - y * Z * d.x * o.y + L * X * d.y * o.x - 3 * pb * d.x * k.y * D - 3 * ra * d.x * D * o.y + 3 * ia * G * d.y * o.x + b.x * c.x * d.x * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + V * d.y * (-2 * g.y * o.y - 2 * k.y * m.y) + a.y * Y * (6 * g.y * o.y + 6 * k.y * m.y) + b.y * c.x * G * (-2 * g.y * o.y - 2 * k.y * m.y) + X * c.y * 
    d.x * (2 * g.y * o.y + 2 * k.y * m.y) + b.x * c.y * G * (-4 * g.y * o.y - 4 * k.y * m.y) + a.x * G * d.y * (-6 * g.y * o.y - 6 * k.y * m.y) + g.x * G * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + k.x * G * d.y * (6 * g.y * m.y + 3 * bb) + Y * (-2 * g.y * k.y * m.y - ia * o.y - k.y * (2 * g.y * m.y + bb) - g.y * (2 * g.y * o.y + 2 * k.y * m.y)), -a.x * b.x * c.y * d.x * d.y * m.y + a.x * b.y * c.x * d.x * d.y * m.y + 6 * a.x * b.y * c.y * d.x * m.x * d.y - 6 * a.y * b.x * c.x * d.x * d.y * m.y - a.y * 
    b.x * c.y * d.x * m.x * d.y + a.y * b.y * c.x * d.x * m.x * d.y + b.x * b.y * c.x * c.y * d.x * m.y - b.x * b.y * c.x * c.y * m.x * d.y + b.x * g.x * c.y * d.x * d.y * m.y + b.x * g.y * c.y * d.x * m.x * d.y + b.x * k.x * c.y * d.x * k.y * d.y - g.x * b.y * c.x * d.x * d.y * m.y - 6 * g.x * b.y * c.y * d.x * m.x * d.y - b.y * c.x * g.y * d.x * m.x * d.y - b.y * c.x * k.x * d.x * k.y * d.y - 6 * a.x * g.x * m.x * ja - 2 * a.x * ca * d.x * m.x + 2 * g.x * ca * d.x * m.x + 2 * a.y * V * d.y * m.y - 
    6 * a.x * a.y * d.x * m.x * D + 3 * a.x * b.x * c.x * D * m.y - 2 * a.x * b.x * c.y * m.x * D - 4 * a.x * b.y * c.x * m.x * D + 3 * a.y * b.x * c.x * m.x * D + 6 * a.x * a.y * G * d.y * m.y + 6 * a.x * g.x * d.x * D * m.y - 3 * a.x * b.y * c.y * G * m.y + 2 * a.x * c.x * Z * d.x * m.y + 2 * a.x * c.x * Z * m.x * d.y + 6 * a.x * g.y * d.x * m.x * D + 6 * a.x * k.x * d.x * k.y * D + 4 * a.y * b.x * c.y * G * m.y + 6 * a.y * g.x * d.x * m.x * D + 2 * a.y * b.y * c.x * G * m.y - 3 * a.y * b.y * c.y * 
    G * m.x + 2 * a.y * c.x * Z * d.x * m.x - 3 * b.x * g.x * c.x * D * m.y + 2 * b.x * g.x * c.y * m.x * D + b.x * b.y * Z * d.x * m.x - 3 * b.x * c.x * g.y * m.x * D - 3 * b.x * c.x * k.x * k.y * D + 4 * g.x * b.y * c.x * m.x * D - 2 * a.x * X * c.y * d.y * m.y - 6 * a.y * g.x * G * d.y * m.y - 6 * a.y * g.y * G * m.x * d.y - 6 * a.y * k.x * G * k.y * d.y - 2 * a.y * X * c.y * d.x * m.y - 2 * a.y * X * c.y * m.x * d.y - b.x * b.y * X * d.y * m.y - 2 * b.x * L * d.x * m.x * d.y + 3 * g.x * b.y * 
    c.y * G * m.y - 2 * g.x * c.x * Z * d.x * m.y - 2 * g.x * c.x * Z * m.x * d.y - 6 * g.x * g.y * d.x * m.x * D - 6 * g.x * k.x * d.x * k.y * D + 3 * b.y * g.y * c.y * G * m.x + 3 * b.y * k.x * c.y * G * k.y - 2 * c.x * g.y * Z * d.x * m.x - 2 * c.x * k.x * Z * d.x * k.y - L * c.x * c.y * d.x * m.x + 2 * g.x * X * c.y * d.y * m.y - 3 * b.y * pb * c.y * d.x * d.y + 6 * g.y * k.x * G * k.y * d.y + 2 * y * b.y * d.x * d.y * m.y + y * c.x * c.y * d.y * m.y + 2 * X * g.y * c.y * m.x * d.y + 2 * X * 
    k.x * c.y * k.y * d.y - 3 * a.x * pb * ja + 3 * g.x * pb * ja + 3 * v * m.x * ja - 3 * w * Y * m.y + 3 * ra * m.x * ja + pb * ca * d.x + R * G * m.x - T * D * m.y + 3 * a.y * pb * d.x * D - b.x * L * G * m.y + b.x * pb * c.y * D + 2 * b.y * c.x * pb * D + y * b.y * m.x * D - c.x * pb * Z * d.y - 3 * g.y * pb * d.x * D - 3 * v * d.x * D * m.y + 3 * w * G * m.x * d.y - y * Z * d.x * m.y + L * X * m.x * d.y - 3 * ra * d.x * D * m.y + 3 * ia * G * m.x * d.y + X * c.y * d.x * (2 * g.y * m.y + bb) + 
    b.x * c.x * d.x * d.y * (6 * g.y * m.y + 3 * bb) + V * d.y * (-2 * g.y * m.y - bb) + a.y * Y * (6 * g.y * m.y + 3 * bb) + b.y * c.x * G * (-2 * g.y * m.y - bb) + b.x * c.y * G * (-4 * g.y * m.y - 2 * bb) + a.x * G * d.y * (-6 * g.y * m.y - 3 * bb) + g.x * G * d.y * (6 * g.y * m.y + 3 * bb) + Y * (-2 * g.y * bb - ia * m.y - g.y * (2 * g.y * m.y + bb)), -a.x * b.x * c.y * d.x * k.y * d.y + a.x * b.y * c.x * d.x * k.y * d.y + 6 * a.x * b.y * k.x * c.y * d.x * d.y - 6 * a.y * b.x * c.x * d.x * k.y * 
    d.y - a.y * b.x * k.x * c.y * d.x * d.y + a.y * b.y * c.x * k.x * d.x * d.y - b.x * b.y * c.x * k.x * c.y * d.y + b.x * b.y * c.x * c.y * d.x * k.y + b.x * g.x * c.y * d.x * k.y * d.y + 6 * b.x * c.x * g.y * d.x * k.y * d.y + b.x * g.y * k.x * c.y * d.x * d.y - g.x * b.y * c.x * d.x * k.y * d.y - 6 * g.x * b.y * k.x * c.y * d.x * d.y - b.y * c.x * g.y * k.x * d.x * d.y - 6 * a.x * g.x * k.x * ja - 2 * a.x * k.x * ca * d.x + 6 * a.y * g.y * Y * k.y + 2 * g.x * k.x * ca * d.x + 2 * a.y * V * k.y * 
    d.y - 2 * V * g.y * k.y * d.y - 6 * a.x * a.y * k.x * d.x * D + 3 * a.x * b.x * c.x * k.y * D - 2 * a.x * b.x * k.x * c.y * D - 4 * a.x * b.y * c.x * k.x * D + 3 * a.y * b.x * c.x * k.x * D + 6 * a.x * a.y * G * k.y * d.y + 6 * a.x * g.x * d.x * k.y * D - 3 * a.x * b.y * c.y * G * k.y + 2 * a.x * c.x * k.x * Z * d.y + 2 * a.x * c.x * Z * d.x * k.y + 6 * a.x * g.y * k.x * d.x * D + 4 * a.y * b.x * c.y * G * k.y + 6 * a.y * g.x * k.x * d.x * D + 2 * a.y * b.y * c.x * G * k.y - 3 * a.y * b.y * k.x * 
    c.y * G + 2 * a.y * c.x * k.x * Z * d.x - 3 * b.x * g.x * c.x * k.y * D + 2 * b.x * g.x * k.x * c.y * D + b.x * b.y * k.x * Z * d.x - 3 * b.x * c.x * g.y * k.x * D + 4 * g.x * b.y * c.x * k.x * D - 6 * a.x * g.y * G * k.y * d.y - 2 * a.x * X * c.y * k.y * d.y - 6 * a.y * g.x * G * k.y * d.y - 6 * a.y * g.y * k.x * G * d.y - 2 * a.y * X * k.x * c.y * d.y - 2 * a.y * X * c.y * d.x * k.y - b.x * b.y * X * k.y * d.y - 4 * b.x * g.y * c.y * G * k.y - 2 * b.x * L * k.x * d.x * d.y + 3 * g.x * b.y * 
    c.y * G * k.y - 2 * g.x * c.x * k.x * Z * d.y - 2 * g.x * c.x * Z * d.x * k.y - 6 * g.x * g.y * k.x * d.x * D - 2 * b.y * c.x * g.y * G * k.y + 3 * b.y * g.y * k.x * c.y * G - 2 * c.x * g.y * k.x * Z * d.x - L * c.x * k.x * c.y * d.x + 6 * g.x * g.y * G * k.y * d.y + 2 * g.x * X * c.y * k.y * d.y + 2 * y * b.y * d.x * k.y * d.y + y * c.x * c.y * k.y * d.y + 2 * X * g.y * k.x * c.y * d.y + 2 * X * g.y * c.y * d.x * k.y + 3 * v * k.x * ja - 3 * w * Y * k.y + 3 * ra * k.x * ja + R * k.x * G - T * 
    k.y * D - 3 * ia * Y * k.y - b.x * L * G * k.y + y * b.y * k.x * D - 3 * v * d.x * k.y * D + 3 * w * k.x * G * d.y - y * Z * d.x * k.y + L * X * k.x * d.y - 3 * ra * d.x * k.y * D + 3 * ia * k.x * G * d.y, a.x * a.y * b.x * c.y * d.x * d.y - a.x * a.y * b.y * c.x * d.x * d.y + a.x * b.x * b.y * c.x * c.y * d.y - a.y * b.x * b.y * c.x * c.y * d.x - a.x * b.x * g.y * c.y * d.x * d.y + 6 * a.x * g.x * b.y * c.y * d.x * d.y + a.x * b.y * c.x * g.y * d.x * d.y - a.y * b.x * g.x * c.y * d.x * d.y - 
    6 * a.y * b.x * c.x * g.y * d.x * d.y + a.y * g.x * b.y * c.x * d.x * d.y - b.x * g.x * b.y * c.x * c.y * d.y + b.x * b.y * c.x * g.y * c.y * d.x + b.x * g.x * g.y * c.y * d.x * d.y - g.x * b.y * c.x * g.y * d.x * d.y - 2 * a.x * g.x * ca * d.x + 2 * a.y * V * g.y * d.y - 3 * a.x * a.y * b.x * c.x * D - 6 * a.x * a.y * g.x * d.x * D + 3 * a.x * a.y * b.y * c.y * G - 2 * a.x * a.y * c.x * Z * d.x - 2 * a.x * b.x * g.x * c.y * D - a.x * b.x * b.y * Z * d.x + 3 * a.x * b.x * c.x * g.y * D - 4 * 
    a.x * g.x * b.y * c.x * D + 3 * a.y * b.x * g.x * c.x * D + 6 * a.x * a.y * g.y * G * d.y + 2 * a.x * a.y * X * c.y * d.y + 2 * a.x * b.x * L * d.x * d.y + 2 * a.x * g.x * c.x * Z * d.y + 6 * a.x * g.x * g.y * d.x * D - 3 * a.x * b.y * g.y * c.y * G + 2 * a.x * c.x * g.y * Z * d.x + a.x * L * c.x * c.y * d.x + a.y * b.x * b.y * X * d.y + 4 * a.y * b.x * g.y * c.y * G - 3 * a.y * g.x * b.y * c.y * G + 2 * a.y * g.x * c.x * Z * d.x + 2 * a.y * b.y * c.x * g.y * G + b.x * g.x * b.y * Z * d.x - 3 * 
    b.x * g.x * c.x * g.y * D - 2 * a.x * X * g.y * c.y * d.y - 6 * a.y * g.x * g.y * G * d.y - 2 * a.y * g.x * X * c.y * d.y - 2 * a.y * y * b.y * d.x * d.y - a.y * y * c.x * c.y * d.y - 2 * a.y * X * g.y * c.y * d.x - 2 * b.x * g.x * L * d.x * d.y - b.x * b.y * X * g.y * d.y + 3 * g.x * b.y * g.y * c.y * G - 2 * g.x * c.x * g.y * Z * d.x - g.x * L * c.x * c.y * d.x + 3 * w * b.x * c.x * d.x * d.y + 3 * b.x * c.x * ia * d.x * d.y + 2 * g.x * X * g.y * c.y * d.y - 3 * v * b.y * c.y * d.x * d.y + 
    2 * y * b.y * g.y * d.x * d.y + y * c.x * g.y * c.y * d.y - 3 * ra * b.y * c.y * d.x * d.y - a.x * a.x * a.x * ja + a.y * a.y * a.y * Y + g.x * g.x * g.x * ja - g.y * g.y * g.y * Y - 3 * a.x * ra * ja - a.x * R * G + 3 * v * g.x * ja + a.y * T * D + 3 * a.y * ia * Y + g.x * R * G + v * ca * d.x - 3 * w * g.y * Y - w * V * d.y + ra * ca * d.x - T * g.y * D - V * ia * d.y - a.x * y * b.y * D + a.y * b.x * L * G - 3 * a.x * w * G * d.y - a.x * L * X * d.y + a.y * y * Z * d.x - b.x * L * g.y * G + 
    3 * v * a.y * d.x * D + v * b.x * c.y * D + 2 * v * b.y * c.x * D - 2 * w * b.x * c.y * G - w * b.y * c.x * G + y * g.x * b.y * D - 3 * a.x * ia * G * d.y + 3 * a.y * ra * d.x * D + b.x * ra * c.y * D - 2 * b.x * ia * c.y * G + g.x * L * X * d.y - b.y * c.x * ia * G - v * c.x * Z * d.y - 3 * v * g.y * d.x * D + 3 * w * g.x * G * d.y + w * X * c.y * d.x - y * g.y * Z * d.x + 2 * ra * b.y * c.x * D + 3 * g.x * ia * G * d.y - ra * c.x * Z * d.y - 3 * ra * g.y * d.x * D + X * ia * c.y * d.x)).cy(0, 
    1);
    for(w = 0;w < v.length;w++) {
      if(y = v[w], T = (new l(d.x, c.x, b.x, a.x - g.x - y * k.x - y * y * m.x - y * y * y * o.x)).mi(), L = (new l(d.y, c.y, b.y, a.y - g.y - y * k.y - y * y * m.y - y * y * y * o.y)).mi(), 0 < T.length && 0 < L.length) {
        R = 0;
        a:for(;R < T.length;R++) {
          if(X = T[R], 0 <= X && 1 >= X) {
            for(V = 0;V < L.length;V++) {
              if(1.0E-4 > Math.abs(X - L[V])) {
                E.Fa.push(o.multiply(y * y * y).add(m.multiply(y * y).add(k.multiply(y).add(g))));
                break a
              }
            }
          }
        }
      }
    }
    0 < E.Fa.length && (E.status = "Intersection");
    return E
  };
  f.eR = function(a, b, c, d, g, k) {
    return f.XM(a, b, c, d, g, k, k)
  };
  f.XM = function(a, b, c, d, g, k, m) {
    var o, v, w, y = new f("No Intersection");
    o = a.multiply(-1);
    v = b.multiply(3);
    w = c.multiply(-3);
    o = o.add(v.add(w.add(d)));
    d = new n(o.x, o.y);
    o = a.multiply(3);
    v = b.multiply(-6);
    w = c.multiply(3);
    o = o.add(v.add(w));
    c = new n(o.x, o.y);
    o = a.multiply(-3);
    v = b.multiply(3);
    w = o.add(v);
    b = new n(w.x, w.y);
    a = new n(a.x, a.y);
    k *= k;
    m *= m;
    g = (new l(d.x * d.x * m + d.y * d.y * k, 2 * (d.x * c.x * m + d.y * c.y * k), 2 * (d.x * b.x * m + d.y * b.y * k) + c.x * c.x * m + c.y * c.y * k, 2 * d.x * m * (a.x - g.x) + 2 * d.y * k * (a.y - g.y) + 2 * (c.x * b.x * m + c.y * b.y * k), 2 * c.x * m * (a.x - g.x) + 2 * c.y * k * (a.y - g.y) + b.x * b.x * m + b.y * b.y * k, 2 * b.x * m * (a.x - g.x) + 2 * b.y * k * (a.y - g.y), a.x * a.x * m - 2 * a.y * g.y * k - 2 * a.x * g.x * m + a.y * a.y * k + g.x * g.x * m + g.y * g.y * k - k * m)).cy(0, 
    1);
    for(m = 0;m < g.length;m++) {
      k = g[m], y.Fa.push(d.multiply(k * k * k).add(c.multiply(k * k).add(b.multiply(k).add(a))))
    }
    0 < y.Fa.length && (y.status = "Intersection");
    return y
  };
  f.qu = function(a, b, c, d, g, k) {
    var m, o, v, w, y, E = g.min(k), L = g.max(k), T = new f("No Intersection");
    m = a.multiply(-1);
    o = b.multiply(3);
    v = c.multiply(-3);
    w = m.add(o.add(v.add(d)));
    y = new n(w.x, w.y);
    m = a.multiply(3);
    o = b.multiply(-6);
    v = c.multiply(3);
    w = m.add(o.add(v));
    w = new n(w.x, w.y);
    m = a.multiply(-3);
    o = b.multiply(3);
    v = m.add(o);
    m = new n(v.x, v.y);
    o = new n(a.x, a.y);
    v = new n(g.y - k.y, k.x - g.x);
    roots = (new l(v.gk(y), v.gk(w), v.gk(m), v.gk(o) + (g.x * k.y - k.x * g.y))).mi();
    for(y = 0;y < roots.length;y++) {
      w = roots[y], 0 <= w && 1 >= w && (m = b.rg(c, w), w = a.rg(b, w).rg(m, w).rg(m.rg(c.rg(d, w), w), w), g.x == k.x ? E.y <= w.y && w.y <= L.y && (T.status = "Intersection", T.Oj(w)) : g.y == k.y ? E.x <= w.x && w.x <= L.x && (T.status = "Intersection", T.Oj(w)) : w.KI(E) && w.GJ(L) && (T.status = "Intersection", T.Oj(w)))
    }
    return T
  };
  f.fR = function(a, b, c, d, g) {
    for(var k = new f("No Intersection"), l = g.length, m = 0;m < l;m++) {
      var n = f.qu(a, b, c, d, g[m], g[(m + 1) % l]);
      k.qc(n.Fa)
    }
    0 < k.Fa.length && (k.status = "Intersection");
    return k
  };
  f.gR = function(a, b, c, d, g, l) {
    var m = g.min(l), n = g.max(l), l = new k(n.x, m.y), o = new k(m.x, n.y), g = f.qu(a, b, c, d, m, l), l = f.qu(a, b, c, d, l, n), n = f.qu(a, b, c, d, n, o), a = f.qu(a, b, c, d, o, m), b = new f("No Intersection");
    b.qc(g.Fa);
    b.qc(l.Fa);
    b.qc(n.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.hR = function(a, b, c, d) {
    var g;
    g = Math.abs(b - d);
    var l = a.QH(c);
    if(l > b + d) {
      g = new f("Outside")
    }else {
      if(l < g) {
        g = new f("Inside")
      }else {
        g = new f("Intersection");
        var m = (b * b - d * d + l * l) / (2 * l), d = a.rg(c, m / l), b = Math.sqrt(b * b - m * m) / l;
        g.Fa.push(new k(d.x - b * (c.y - a.y), d.y + b * (c.x - a.x)));
        g.Fa.push(new k(d.x + b * (c.y - a.y), d.y - b * (c.x - a.x)))
      }
    }
    return g
  };
  f.iR = function(a, b, c, d, g) {
    return f.ZM(a, b, b, c, d, g)
  };
  f.ru = function(a, b, c, d) {
    var g;
    g = (d.x - c.x) * (d.x - c.x) + (d.y - c.y) * (d.y - c.y);
    var k = 2 * ((d.x - c.x) * (c.x - a.x) + (d.y - c.y) * (c.y - a.y)), a = k * k - 4 * g * (a.x * a.x + a.y * a.y + c.x * c.x + c.y * c.y - 2 * (a.x * c.x + a.y * c.y) - b * b);
    0 > a ? g = new f("Outside") : 0 == a ? g = new f("Tangent") : (b = Math.sqrt(a), a = (-k + b) / (2 * g), k = (-k - b) / (2 * g), (0 > a || 1 < a) && (0 > k || 1 < k) ? g = 0 > a && 0 > k || 1 < a && 1 < k ? new f("Outside") : new f("Inside") : (g = new f("Intersection"), 0 <= a && 1 >= a && g.Fa.push(c.rg(d, a)), 0 <= k && 1 >= k && g.Fa.push(c.rg(d, k))));
    return g
  };
  f.jR = function(a, b, c) {
    for(var d = new f("No Intersection"), g = c.length, k, l = 0;l < g;l++) {
      k = f.ru(a, b, c[l], c[(l + 1) % g]), d.qc(k.Fa)
    }
    d.status = 0 < d.Fa.length ? "Intersection" : k.status;
    return d
  };
  f.YM = function(a, b, c) {
    var d = c.min(h), g = c.max(h), l = new k(g.x, d.y), m = new k(d.x, g.y), c = f.ru(a, b, d, l), l = f.ru(a, b, l, g), g = f.ru(a, b, g, m), a = f.ru(a, b, m, d), b = new f("No Intersection");
    b.qc(c.Fa);
    b.qc(l.Fa);
    b.qc(g.Fa);
    b.qc(a.Fa);
    b.status = 0 < b.Fa.length ? "Intersection" : c.status;
    return b
  };
  f.ZM = function(a, b, c, d, g, m) {
    for(var a = [c * c, 0, b * b, -2 * c * c * a.x, -2 * b * b * a.y, c * c * a.x * a.x + b * b * a.y * a.y - b * b * c * c], d = [m * m, 0, g * g, -2 * m * m * d.x, -2 * g * g * d.y, m * m * d.x * d.x + g * g * d.y * d.y - g * g * m * m], g = f.xL(a, d).mi(), m = 0.001 * (a[0] * a[0] + 2 * a[1] * a[1] + a[2] * a[2]), b = 0.001 * (d[0] * d[0] + 2 * d[1] * d[1] + d[2] * d[2]), c = new f("No Intersection"), n = 0;n < g.length;n++) {
      for(var o = (new l(a[0], a[3] + g[n] * a[1], a[5] + g[n] * (a[4] + g[n] * a[2]))).mi(), v = 0;v < o.length;v++) {
        var w = (a[0] * o[v] + a[1] * g[n] + a[3]) * o[v] + (a[2] * g[n] + a[4]) * g[n] + a[5];
        Math.abs(w) < m && (w = (d[0] * o[v] + d[1] * g[n] + d[3]) * o[v] + (d[2] * g[n] + d[4]) * g[n] + d[5], Math.abs(w) < b && c.Oj(new k(o[v], g[n])))
      }
    }
    0 < c.Fa.length && (c.status = "Intersection");
    return c
  };
  f.su = function(a, b, c, d, g) {
    var k = n.lM(d, g), a = (new n(d.x, d.y)).Ph(new n(a.x, a.y)), l = new n(a.x / (b * b), a.y / (c * c)), b = k.gk(new n(k.x / (b * b), k.y / (c * c))), k = k.gk(l), a = a.gk(l) - 1, a = k * k - b * a;
    0 > a ? b = new f("Outside") : 0 < a ? (l = Math.sqrt(a), a = (-k - l) / b, k = (-k + l) / b, (0 > a || 1 < a) && (0 > k || 1 < k) ? b = 0 > a && 0 > k || 1 < a && 1 < k ? new f("Outside") : new f("Inside") : (b = new f("Intersection"), 0 <= a && 1 >= a && b.Oj(d.rg(g, a)), 0 <= k && 1 >= k && b.Oj(d.rg(g, k)))) : (k = -k / b, 0 <= k && 1 >= k ? (b = new f("Intersection"), b.Oj(d.rg(g, k))) : b = new f("Outside"));
    return b
  };
  f.kR = function(a, b, c, d) {
    for(var g = new f("No Intersection"), k = d.length, l = 0;l < k;l++) {
      var m = f.su(a, b, c, d[l], d[(l + 1) % k]);
      g.qc(m.Fa)
    }
    0 < g.Fa.length && (g.status = "Intersection");
    return g
  };
  f.$M = function(a, b, c, d, g) {
    var l = d.min(g), m = d.max(g), g = new k(m.x, l.y), n = new k(l.x, m.y), d = f.su(a, b, c, l, g), g = f.su(a, b, c, g, m), m = f.su(a, b, c, m, n), a = f.su(a, b, c, n, l), b = new f("No Intersection");
    b.qc(d.Fa);
    b.qc(g.Fa);
    b.qc(m.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.mr = function(a, b, c, d) {
    var g, l = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x);
    g = (b.x - a.x) * (a.y - c.y) - (b.y - a.y) * (a.x - c.x);
    c = (d.y - c.y) * (b.x - a.x) - (d.x - c.x) * (b.y - a.y);
    0 != c ? (l /= c, g /= c, 0 <= l && 1 >= l && 0 <= g && 1 >= g ? (g = new f("Intersection"), g.Fa.push(new k(a.x + l * (b.x - a.x), a.y + l * (b.y - a.y)))) : g = new f("No Intersection")) : g = 0 == l || 0 == g ? new f("Coincident") : new f("Parallel");
    return g
  };
  f.tu = function(a, b, c) {
    for(var d = new f("No Intersection"), g = c.length, k = 0;k < g;k++) {
      var l = f.mr(a, b, c[k], c[(k + 1) % g]);
      d.qc(l.Fa)
    }
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.nr = function(a, b, c, d) {
    var g = c.min(d), l = c.max(d), d = new k(l.x, g.y), m = new k(g.x, l.y), c = f.mr(g, d, a, b), d = f.mr(d, l, a, b), l = f.mr(l, m, a, b), a = f.mr(m, g, a, b), b = new f("No Intersection");
    b.qc(c.Fa);
    b.qc(d.Fa);
    b.qc(l.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.lR = function(a, b) {
    for(var c = new f("No Intersection"), d = a.length, g = 0;g < d;g++) {
      var k = f.tu(a[g], a[(g + 1) % d], b);
      c.qc(k.Fa)
    }
    0 < c.Fa.length && (c.status = "Intersection");
    return c
  };
  f.XD = function(a, b, c) {
    var d = b.min(c), g = b.max(c), c = new k(g.x, d.y), l = new k(d.x, g.y), b = f.tu(d, c, a), c = f.tu(c, g, a), g = f.tu(g, l, a), a = f.tu(l, d, a), d = new f("No Intersection");
    d.qc(b.Fa);
    d.qc(c.Fa);
    d.qc(g.Fa);
    d.qc(a.Fa);
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.mR = function(a, b, c, d) {
    var g;
    g = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x);
    var l = (b.x - a.x) * (a.y - c.y) - (b.y - a.y) * (a.x - c.x), c = (d.y - c.y) * (b.x - a.x) - (d.x - c.x) * (b.y - a.y);
    0 != c ? (l = g / c, g = new f("Intersection"), g.Fa.push(new k(a.x + l * (b.x - a.x), a.y + l * (b.y - a.y)))) : g = 0 == g || 0 == l ? new f("Coincident") : new f("Parallel");
    return g
  };
  f.nR = function(a, b, c, d) {
    var g = a.min(b), l = a.max(b), b = new k(l.x, g.y), m = new k(g.x, l.y), a = f.nr(g, b, c, d), b = f.nr(b, l, c, d), l = f.nr(l, m, c, d), c = f.nr(m, g, c, d), d = new f("No Intersection");
    d.qc(a.Fa);
    d.qc(b.Fa);
    d.qc(l.Fa);
    d.qc(c.Fa);
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.xL = function(a, b) {
    var c = a[0] * b[1] - b[0] * a[1], d = a[0] * b[2] - b[0] * a[2], f = a[0] * b[3] - b[0] * a[3], g = a[0] * b[4] - b[0] * a[4], k = a[0] * b[5] - b[0] * a[5], m = a[1] * b[2] - b[1] * a[2], n = a[3] * b[5] - b[3] * a[5], o = a[1] * b[5] - b[1] * a[5] + (a[3] * b[4] - b[3] * a[4]), v = a[1] * b[4] - b[1] * a[4] - (a[2] * b[3] - b[2] * a[3]);
    return new l(c * m - d * d, c * v + f * m - 2 * d * g, c * o + f * v - g * g - 2 * d * k, c * n + f * o - 2 * g * k, f * n - k * k)
  };
  g.prototype.Ha = function(a, b) {
    this.name = a;
    this.wz = b
  };
  k.prototype.Ca = function() {
    return new k(this.x, this.y)
  };
  k.prototype.add = function(a) {
    return new k(this.x + a.x, this.y + a.y)
  };
  k.prototype.LG = function(a) {
    this.x += a.x;
    this.y += a.y;
    return this
  };
  k.prototype.Ph = function(a) {
    return new k(this.x - a.x, this.y - a.y)
  };
  k.prototype.multiply = function(a) {
    return new k(this.x * a, this.y * a)
  };
  k.prototype.nC = function(a) {
    return new k(this.x / a, this.y / a)
  };
  k.prototype.gH = function(a) {
    return this.x - a.x || this.y - a.y
  };
  k.prototype.GJ = function(a) {
    return this.x <= a.x && this.y <= a.y
  };
  k.prototype.KI = function(a) {
    return this.x >= a.x && this.y >= a.y
  };
  k.prototype.rg = function(a, b) {
    return new k(this.x + (a.x - this.x) * b, this.y + (a.y - this.y) * b)
  };
  k.prototype.QH = function(a) {
    var b = this.x - a.x, a = this.y - a.y;
    return Math.sqrt(b * b + a * a)
  };
  k.prototype.min = function(a) {
    return new k(Math.min(this.x, a.x), Math.min(this.y, a.y))
  };
  k.prototype.max = function(a) {
    return new k(Math.max(this.x, a.x), Math.max(this.y, a.y))
  };
  k.prototype.toString = function() {
    return this.x + "," + this.y
  };
  l.Qi = 1.0E-6;
  l.bL = 6;
  l.lr = function(a, b, c, d, f) {
    (a.constructor !== Array || b.constructor !== Array) && e(Error("Polynomial.interpolate: xs and ys must be arrays"));
    (isNaN(c) || isNaN(d) || isNaN(f)) && e(Error("Polynomial.interpolate: n, offset, and x must be numbers"));
    for(var g = 0, k = 0, l = Array(c), m = Array(c), n = 0, g = Math.abs(f - a[d]), o = 0;o < c;o++) {
      var v = Math.abs(f - a[d + o]);
      v < g && (n = o, g = v);
      l[o] = m[o] = b[d + o]
    }
    g = b[d + n];
    n--;
    for(b = 1;b < c;b++) {
      for(o = 0;o < c - b;o++) {
        var k = a[d + o] - f, v = a[d + o + b] - f, w = k - v;
        if(0 == w) {
          break
        }
        w = (l[o + 1] - m[o]) / w;
        m[o] = v * w;
        l[o] = k * w
      }
      k = 2 * (n + 1) < c - b ? l[n + 1] : m[n--];
      g += k
    }
    return{y:g, pQ:k}
  };
  l.prototype.Ha = function(a) {
    this.Ec = [];
    for(var b = a.length - 1;0 <= b;b--) {
      this.Ec.push(a[b])
    }
    this.KG = "t"
  };
  l.prototype.eval = function(a) {
    isNaN(a) && e(Error("Polynomial.eval: parameter must be a number"));
    for(var b = 0, c = this.Ec.length - 1;0 <= c;c--) {
      b = b * a + this.Ec[c]
    }
    return b
  };
  l.prototype.add = function(a) {
    for(var b = new l, c = this.ki(), d = a.ki(), f = Math.max(c, d), g = 0;g <= f;g++) {
      b.Ec[g] = (g <= c ? this.Ec[g] : 0) + (g <= d ? a.Ec[g] : 0)
    }
    return b
  };
  l.prototype.multiply = function(a) {
    for(var b = new l, c = 0;c <= this.ki() + a.ki();c++) {
      b.Ec.push(0)
    }
    for(c = 0;c <= this.ki();c++) {
      for(var d = 0;d <= a.ki();d++) {
        b.Ec[c + d] += this.Ec[c] * a.Ec[d]
      }
    }
    return b
  };
  l.prototype.WN = function() {
    for(var a = this.ki();0 <= a;a--) {
      if(Math.abs(this.Ec[a]) <= l.Qi) {
        this.Ec.pop()
      }else {
        break
      }
    }
  };
  l.prototype.Vs = function(a, b) {
    var c = this.eval(a), d = this.eval(b), f;
    if(Math.abs(c) <= l.Qi) {
      f = a
    }else {
      if(Math.abs(d) <= l.Qi) {
        f = b
      }else {
        if(0 >= c * d) {
          for(var d = Math.log(b - a), d = Math.ceil((d + Math.LN10 * l.bL) / Math.LN2), g = 0;g < d;g++) {
            f = 0.5 * (a + b);
            var k = this.eval(f);
            if(Math.abs(k) <= l.Qi) {
              break
            }
            0 > k * c ? b = f : (a = f, c = k)
          }
        }
      }
    }
    return f
  };
  l.prototype.toString = function() {
    for(var a = [], b = [], c = this.Ec.length - 1;0 <= c;c--) {
      var d = Math.round(1E3 * this.Ec[c]) / 1E3;
      if(0 != d) {
        var f = 0 > d ? " - " : " + ", d = Math.abs(d);
        0 < c && (d = 1 == d ? this.KG : d + this.KG);
        1 < c && (d += "^" + c);
        b.push(f);
        a.push(d)
      }
    }
    b[0] = " + " == b[0] ? "" : "-";
    d = "";
    for(c = 0;c < a.length;c++) {
      d += b[c] + a[c]
    }
    return d
  };
  l.prototype.ki = function() {
    return this.Ec.length - 1
  };
  l.prototype.sM = function() {
    for(var a = new l, b = 1;b < this.Ec.length;b++) {
      a.Ec.push(b * this.Ec[b])
    }
    return a
  };
  l.prototype.mi = function() {
    var a;
    this.WN();
    switch(this.ki()) {
      case 0:
        a = [];
        break;
      case 1:
        a = this.zM();
        break;
      case 2:
        a = this.FM();
        break;
      case 3:
        a = this.pI();
        break;
      case 4:
        a = this.GM();
        break;
      default:
        a = []
    }
    return a
  };
  l.prototype.cy = function(a, b) {
    var c = [], d;
    if(1 == this.ki()) {
      d = this.Vs(a, b)
    }else {
      var f = this.sM().cy(a, b);
      if(0 < f.length) {
        d = this.Vs(a, f[0]);
        d != p && c.push(d);
        for(i = 0;i <= f.length - 2;i++) {
          d = this.Vs(f[i], f[i + 1]), d != p && c.push(d)
        }
        d = this.Vs(f[f.length - 1], b)
      }else {
        d = this.Vs(a, b)
      }
    }
    d != p && c.push(d);
    return c
  };
  l.prototype.zM = function() {
    var a = [], b = this.Ec[1];
    0 != b && a.push(-this.Ec[0] / b);
    return a
  };
  l.prototype.FM = function() {
    var a = [];
    if(2 == this.ki()) {
      var b = this.Ec[2], c = this.Ec[1] / b, b = c * c - 4 * (this.Ec[0] / b);
      0 < b ? (b = Math.sqrt(b), a.push(0.5 * (-c + b)), a.push(0.5 * (-c - b))) : 0 == b && a.push(0.5 * -c)
    }
    return a
  };
  l.prototype.pI = function() {
    var a = [];
    if(3 == this.ki()) {
      var b = this.Ec[3], c = this.Ec[2] / b, d = this.Ec[1] / b, f = (3 * d - c * c) / 3, b = (2 * c * c * c - 9 * d * c + 27 * (this.Ec[0] / b)) / 27, c = c / 3, d = b * b / 4 + f * f * f / 27, b = b / 2;
      Math.abs(d) <= l.Qi && (d = 0);
      if(0 < d) {
        var f = Math.sqrt(d), g, d = -b + f;
        g = 0 <= d ? Math.pow(d, 1 / 3) : -Math.pow(-d, 1 / 3);
        d = -b - f;
        g = 0 <= d ? g + Math.pow(d, 1 / 3) : g - Math.pow(-d, 1 / 3);
        a.push(g - c)
      }else {
        0 > d ? (f = Math.sqrt(-f / 3), d = Math.atan2(Math.sqrt(-d), -b) / 3, b = Math.cos(d), d = Math.sin(d), g = Math.sqrt(3), a.push(2 * f * b - c), a.push(-f * (b + g * d) - c), a.push(-f * (b - g * d) - c)) : (d = 0 <= b ? -Math.pow(b, 1 / 3) : Math.pow(-b, 1 / 3), a.push(2 * d - c), a.push(-d - c))
      }
    }
    return a
  };
  l.prototype.GM = function() {
    var a = [];
    if(4 == this.ki()) {
      var b = this.Ec[4], c = this.Ec[3] / b, d = this.Ec[2] / b, f = this.Ec[1] / b, b = this.Ec[0] / b, g = (new l(1, -d, c * f - 4 * b, -c * c * b + 4 * d * b - f * f)).pI()[0], k = c * c / 4 - d + g;
      Math.abs(k) <= l.Qi && (k = 0);
      if(0 < k) {
        if(b = Math.sqrt(k), g = 3 * c * c / 4 - b * b - 2 * d, f = (4 * c * d - 8 * f - c * c * c) / (4 * b), d = g + f, f = g - f, Math.abs(d) <= l.Qi && (d = 0), Math.abs(f) <= l.Qi && (f = 0), 0 <= d && (d = Math.sqrt(d), a.push(-c / 4 + (b + d) / 2), a.push(-c / 4 + (b - d) / 2)), 0 <= f) {
          d = Math.sqrt(f), a.push(-c / 4 + (d - b) / 2), a.push(-c / 4 - (d + b) / 2)
        }
      }else {
        if(!(0 > k) && (f = g * g - 4 * b, f >= -l.Qi && (0 > f && (f = 0), f = 2 * Math.sqrt(f), g = 3 * c * c / 4 - 2 * d, g + f >= l.Qi && (d = Math.sqrt(g + f), a.push(-c / 4 + d / 2), a.push(-c / 4 - d / 2)), g - f >= l.Qi))) {
          d = Math.sqrt(g - f), a.push(-c / 4 + d / 2), a.push(-c / 4 - d / 2)
        }
      }
    }
    return a
  };
  n.prototype.length = function() {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  };
  n.prototype.gk = function(a) {
    return this.x * a.x + this.y * a.y
  };
  n.prototype.add = function(a) {
    return new n(this.x + a.x, this.y + a.y)
  };
  n.prototype.LG = function(a) {
    this.x += a.x;
    this.y += a.y;
    return this
  };
  n.prototype.Ph = function(a) {
    return new n(this.x - a.x, this.y - a.y)
  };
  n.prototype.multiply = function(a) {
    return new n(this.x * a, this.y * a)
  };
  n.prototype.nC = function(a) {
    return new n(this.x / a, this.y / a)
  };
  n.prototype.toString = function() {
    return this.x + "," + this.y
  };
  n.lM = function(a, b) {
    return new n(b.x - a.x, b.y - a.y)
  };
  m.prototype = new b;
  m.prototype.constructor = m;
  m.hc = b.prototype;
  m.prototype.Ha = function(a) {
    this.kb = a;
    this.Ks = j;
    this.selected = q;
    this.YG = p
  };
  m.prototype.show = function(a) {
    this.Ks = a;
    this.kb.setAttributeNS(p, "display", a ? "inline" : "none")
  };
  m.prototype.refresh = s();
  m.prototype.update = function() {
    this.refresh();
    this.wg && this.wg.update(this);
    this.YG != p && this.YG(this)
  };
  m.prototype.translate = s();
  m.prototype.select = t("selected");
  o.prototype = new m;
  o.prototype.constructor = o;
  o.hc = m.prototype;
  o.prototype.Ha = function(a) {
    if("circle" == a.localName) {
      o.hc.Ha.call(this, a);
      var b = parseFloat(a.getAttributeNS(p, "cx")), c = parseFloat(a.getAttributeNS(p, "cy")), a = parseFloat(a.getAttributeNS(p, "r"));
      this.gg = new w(b, c, this);
      this.td = new w(b + a, c, this)
    }else {
      e(Error("Circle.init: Invalid SVG Node: " + a.localName))
    }
  };
  o.prototype.xd = function() {
    this.kb != p && (this.gg.xd(), this.td.xd(), this.gg.show(q), this.td.show(q), this.kb.addEventListener("mousedown", this, q))
  };
  o.prototype.translate = function(a) {
    this.gg.translate(a);
    this.td.translate(a);
    this.refresh()
  };
  o.prototype.refresh = function() {
    var a = this.td.ua.QH(this.gg.ua);
    this.kb.setAttributeNS(p, "cx", this.gg.ua.x);
    this.kb.setAttributeNS(p, "cy", this.gg.ua.y);
    this.kb.setAttributeNS(p, "r", a)
  };
  o.prototype.Ee = function() {
    return new g("Circle", [this.gg.ua, parseFloat(this.kb.getAttributeNS(p, "r"))])
  };
  v.prototype = new m;
  v.prototype.constructor = v;
  v.hc = m.prototype;
  v.prototype.Ha = function(a) {
    (a == p || "ellipse" != a.localName) && e(Error("Ellipse.init: Invalid localName: " + a.localName));
    v.hc.Ha.call(this, a);
    var b = parseFloat(a.getAttributeNS(p, "cx")), c = parseFloat(a.getAttributeNS(p, "cy")), d = parseFloat(a.getAttributeNS(p, "rx")), a = parseFloat(a.getAttributeNS(p, "ry"));
    this.gg = new w(b, c, this);
    this.iF = new w(b + d, c, this);
    this.jF = new w(b, c + a, this)
  };
  v.prototype.xd = function() {
    this.gg.xd();
    this.iF.xd();
    this.jF.xd();
    this.gg.show(q);
    this.iF.show(q);
    this.jF.show(q);
    this.kb.addEventListener("mousedown", this, q)
  };
  v.prototype.refresh = function() {
    var a = Math.abs(this.gg.ua.x - this.iF.ua.x), b = Math.abs(this.gg.ua.y - this.jF.ua.y);
    this.kb.setAttributeNS(p, "cx", this.gg.ua.x);
    this.kb.setAttributeNS(p, "cy", this.gg.ua.y);
    this.kb.setAttributeNS(p, "rx", a);
    this.kb.setAttributeNS(p, "ry", b)
  };
  v.prototype.Ee = function() {
    return new g("Ellipse", [this.gg.ua, parseFloat(this.kb.getAttributeNS(p, "rx")), parseFloat(this.kb.getAttributeNS(p, "ry"))])
  };
  w.prototype = new m;
  w.prototype.constructor = w;
  w.hc = m.prototype;
  w.pL = 0;
  w.iL = 1;
  w.jL = 2;
  w.prototype.Ha = function(a, b, c) {
    w.hc.Ha.call(this, p);
    this.ua = new k(a, b);
    this.wg = c;
    this.kH = w.pL
  };
  w.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "rect"), b;
      b = this.wg != p && this.wg.kb != p ? this.wg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "x", this.ua.x - 2);
      a.setAttributeNS(p, "y", this.ua.y - 2);
      a.setAttributeNS(p, "width", 4);
      a.setAttributeNS(p, "height", 4);
      a.setAttributeNS(p, "stroke", "black");
      a.setAttributeNS(p, "fill", "white");
      a.addEventListener("mousedown", this, q);
      b.appendChild(a);
      this.kb = a;
      this.show(this.Ks)
    }
  };
  w.prototype.translate = function(a) {
    this.kH == w.iL ? this.ua.x += a.x : this.kH == w.jL ? this.ua.y += a.y : this.ua.LG(a);
    this.refresh()
  };
  w.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x", this.ua.x - 2);
    this.kb.setAttributeNS(p, "y", this.ua.y - 2)
  };
  w.prototype.select = function(a) {
    w.hc.select.call(this, a);
    a ? this.kb.setAttributeNS(p, "fill", "black") : this.kb.setAttributeNS(p, "fill", "white")
  };
  y.prototype = new m;
  y.prototype.constructor = y;
  y.hc = m.prototype;
  y.prototype.Ha = function(a, b, c, d, f) {
    y.hc.Ha.call(this, p);
    this.ua = new w(a, b, this);
    this.Ju = new E(c, d, this);
    this.wg = f
  };
  y.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "line"), b;
      b = this.wg != p && this.wg.kb != p ? this.wg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "x1", this.ua.ua.x);
      a.setAttributeNS(p, "y1", this.ua.ua.y);
      a.setAttributeNS(p, "x2", this.Ju.ua.x);
      a.setAttributeNS(p, "y2", this.Ju.ua.y);
      a.setAttributeNS(p, "stroke", "black");
      b.appendChild(a);
      this.kb = a;
      this.ua.xd();
      this.Ju.xd();
      this.show(this.Ks)
    }
  };
  y.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x1", this.ua.ua.x);
    this.kb.setAttributeNS(p, "y1", this.ua.ua.y);
    this.kb.setAttributeNS(p, "x2", this.Ju.ua.x);
    this.kb.setAttributeNS(p, "y2", this.Ju.ua.y)
  };
  E.prototype = new w;
  E.prototype.constructor = E;
  E.hc = w.prototype;
  E.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "circle"), b;
      b = this.wg != p && this.wg.kb != p ? this.wg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "cx", this.ua.x);
      a.setAttributeNS(p, "cy", this.ua.y);
      a.setAttributeNS(p, "r", 2.5);
      a.setAttributeNS(p, "fill", "black");
      a.addEventListener("mousedown", this, q);
      b.appendChild(a);
      this.kb = a;
      this.show(this.Ks)
    }
  };
  E.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "cx", this.ua.x);
    this.kb.setAttributeNS(p, "cy", this.ua.y)
  };
  E.prototype.select = function(a) {
    E.hc.select.call(this, a);
    this.kb.setAttributeNS(p, "fill", "black")
  };
  V.prototype = new m;
  V.prototype.constructor = V;
  V.hc = m.prototype;
  V.prototype.Ha = function(a) {
    (a == p || "line" != a.localName) && e(Error("Line.init: Invalid localName: " + a.localName));
    V.hc.Ha.call(this, a);
    var b = parseFloat(a.getAttributeNS(p, "x2")), c = parseFloat(a.getAttributeNS(p, "y2"));
    this.Vg = new w(parseFloat(a.getAttributeNS(p, "x1")), parseFloat(a.getAttributeNS(p, "y1")), this);
    this.Wg = new w(b, c, this)
  };
  V.prototype.xd = function() {
    this.Vg.xd();
    this.Wg.xd();
    this.Vg.show(q);
    this.Wg.show(q);
    this.kb.addEventListener("mousedown", this, q)
  };
  V.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x1", this.Vg.ua.x);
    this.kb.setAttributeNS(p, "y1", this.Vg.ua.y);
    this.kb.setAttributeNS(p, "x2", this.Wg.ua.x);
    this.kb.setAttributeNS(p, "y2", this.Wg.ua.y)
  };
  V.prototype.Ee = function() {
    return new g("Line", [this.Vg.ua, this.Wg.ua])
  };
  R.prototype.Ha = function(a, b) {
    this.type = a;
    this.text = b
  };
  R.prototype.pG = function(a) {
    return this.type == a
  };
  T.prototype = new m;
  T.prototype.constructor = T;
  T.hc = m.prototype;
  T.hL = 0;
  T.fB = 1;
  T.HG = 2;
  T.hB = {A:"rx,ry,x-axis-rotation,large-arc-flag,sweep-flag,x,y".split(","), a:"rx,ry,x-axis-rotation,large-arc-flag,sweep-flag,x,y".split(","), C:"x1,y1,x2,y2,x,y".split(","), c:"x1,y1,x2,y2,x,y".split(","), H:["x"], h:["x"], L:["x", "y"], l:["x", "y"], M:["x", "y"], m:["x", "y"], Q:["x1", "y1", "x", "y"], q:["x1", "y1", "x", "y"], S:["x2", "y2", "x", "y"], s:["x2", "y2", "x", "y"], T:["x", "y"], t:["x", "y"], V:["y"], v:["y"], Z:[], z:[]};
  T.prototype.Ha = function(a) {
    (a == p || "path" != a.localName) && e(Error("Path.init: Invalid localName: " + a.localName));
    T.hc.Ha.call(this, a);
    this.Ek = p;
    this.DN(a.getAttributeNS(p, "d"))
  };
  T.prototype.xd = function() {
    for(var a = 0;a < this.Ek.length;a++) {
      this.Ek[a].xd()
    }
    this.kb.addEventListener("mousedown", this, q)
  };
  T.prototype.refresh = function() {
    for(var a = [], b = 0;b < this.Ek.length;b++) {
      a.push(this.Ek[b].toString())
    }
    this.kb.setAttributeNS(p, "d", a.join(" "))
  };
  T.prototype.DN = function(a) {
    var a = this.aO(a), b = 0, c = a[b], d = "BOD";
    for(this.Ek = [];!c.pG(T.HG);) {
      var f, g = [];
      "BOD" == d ? "M" == c.text || "m" == c.text ? (b++, f = T.hB[c.text].length, d = c.text) : e(Error("Path data must begin with a moveto command")) : c.pG(T.fB) ? f = T.hB[d].length : (b++, f = T.hB[c.text].length, d = c.text);
      if(b + f < a.length) {
        for(c = b;c < b + f;c++) {
          var k = a[c];
          k.pG(T.fB) ? g[g.length] = k.text : e(Error("Parameter type is not a number: " + d + "," + k.text))
        }
        var l, c = this.Ek.length, c = 0 == c ? p : this.Ek[c - 1];
        switch(d) {
          case "A":
            l = new ra(g, this, c);
            break;
          case "C":
            l = new yb(g, this, c);
            break;
          case "c":
            l = new af(g, this, c);
            break;
          case "H":
            l = new Y(g, this, c);
            break;
          case "L":
            l = new L(g, this, c);
            break;
          case "l":
            l = new bf(g, this, c);
            break;
          case "M":
            l = new Ye(g, this, c);
            break;
          case "m":
            l = new cf(g, this, c);
            break;
          case "Q":
            l = new Qc(g, this, c);
            break;
          case "q":
            l = new $e(g, this, c);
            break;
          case "S":
            l = new Pd(g, this, c);
            break;
          case "s":
            l = new Rd(g, this, c);
            break;
          case "T":
            l = new Ze(g, this, c);
            break;
          case "t":
            l = new df(g, this, c);
            break;
          case "Z":
            l = new Qd(g, this, c);
            break;
          case "z":
            l = new Qd(g, this, c);
            break;
          default:
            e(Error("Unsupported segment type: " + d))
        }
        this.Ek.push(l);
        b += f;
        c = a[b];
        "M" == d && (d = "L");
        "m" == d && (d = "l")
      }else {
        e(Error("Path data ended before all parameters were found"))
      }
    }
  };
  T.prototype.aO = function(a) {
    for(var b = [];"" != a;) {
      a.match(/^([ \t\r\n,]+)/) ? a = a.substr(RegExp.$1.length) : a.match(/^([aAcChHlLmMqQsStTvVzZ])/) ? (b[b.length] = new R(T.hL, RegExp.$1), a = a.substr(RegExp.$1.length)) : a.match(/^(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)/) ? (b[b.length] = new R(T.fB, parseFloat(RegExp.$1)), a = a.substr(RegExp.$1.length)) : e(Error("Unrecognized segment command: " + a))
    }
    b[b.length] = new R(T.HG, p);
    return b
  };
  T.prototype.aN = function(a) {
    for(var b = new f("No Intersection"), c = 0;c < this.Ek.length;c++) {
      var d = f.YI(this.Ek[c], a);
      b.qc(d.Fa)
    }
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  T.prototype.Ee = function() {
    return new g("Path", [])
  };
  ca.prototype.Ha = function(a, b, c, d) {
    this.Xh = a;
    this.wg = c;
    this.Ob = d;
    this.lb = [];
    for(a = 0;a < b.length;) {
      this.lb.push(new w(b[a], b[a + 1], c)), a += 2
    }
  };
  ca.prototype.xd = function() {
    for(var a = 0;a < this.lb.length;a++) {
      var b = this.lb[a];
      b.xd();
      b.show(q)
    }
  };
  ca.prototype.toString = function() {
    var a = [], b = "";
    if(this.Ob == p || this.Ob.constructor != this.gx) {
      b = this.Xh
    }
    for(var c = 0;c < this.lb.length;c++) {
      a.push(this.lb[c].ua.toString())
    }
    return b + a.join(" ")
  };
  ca.prototype.Rd = function() {
    return this.lb[this.lb.length - 1].ua
  };
  ca.prototype.Ee = x(p);
  ra.prototype = new ca;
  ra.prototype.constructor = ra;
  ra.hc = ca.prototype;
  ra.prototype.Ha = function(a, b, c, d) {
    var f = [], g = b.pop(), k = b.pop();
    f.push(k, g);
    ra.hc.Ha.call(this, a, f, c, d);
    this.qF = parseFloat(b.shift());
    this.rF = parseFloat(b.shift());
    this.OG = parseFloat(b.shift());
    this.QG = parseFloat(b.shift());
    this.LK = parseFloat(b.shift())
  };
  ra.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.gx && (a = this.Xh);
    return a + [this.qF, this.rF, this.OG, this.QG, this.LK, this.lb[0].ua.toString()].join()
  };
  ra.prototype.Ee = function() {
    return new g("Ellipse", [this.pM(), this.qF, this.rF])
  };
  ra.prototype.pM = function() {
    var a = this.Ob.Rd(), b = this.lb[0].ua, c = this.qF, d = this.rF, f = this.OG * Math.PI / 180, g = Math.cos(f), f = Math.sin(f), l = a.Ph(b).nC(2), m = l.x * g + l.y * f, l = l.x * -f + l.y * g, n = m * m, o = l * l, v = n / (c * c) + o / (d * d);
    1 < v && (v = Math.sqrt(v), c *= v, d *= v);
    var v = c * c, w = d * d, o = v * o, n = w * n, v = (v * w - o - n) / (o + n);
    1.0E-6 > Math.abs(v) && (v = 0);
    n = Math.sqrt(v);
    this.QG == this.LK && (n = -n);
    a = a.add(b).nC(2);
    b = n * c * l / d;
    c = n * -d * m / c;
    return new k(b * g - c * f + a.x, b * f + c * g + a.y)
  };
  Qc.prototype = new ca;
  Qc.prototype.constructor = Qc;
  Qc.hc = ca.prototype;
  Qc.prototype.Eo = function() {
    return this.lb[0].ua
  };
  Qc.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Rd(), this.lb[0].ua, this.lb[1].ua])
  };
  yb.prototype = new ca;
  yb.prototype.constructor = yb;
  yb.hc = ca.prototype;
  yb.prototype.$t = function() {
    return this.lb[1].ua
  };
  yb.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Rd(), this.lb[0].ua, this.lb[1].ua, this.lb[2].ua])
  };
  Y.prototype = new ca;
  Y.prototype.constructor = Y;
  Y.hc = ca.prototype;
  Y.prototype.Ha = function(a, b, c, d) {
    var f = d.Rd(), g = [];
    g.push(b.pop(), f.y);
    Y.hc.Ha.call(this, a, g, c, d)
  };
  Y.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.gx && (a = this.Xh);
    return a + this.lb[0].ua.x
  };
  L.prototype = new ca;
  L.prototype.constructor = L;
  L.hc = ca.prototype;
  L.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.gx && this.Ob.constructor != Ye && (a = this.Xh);
    return a + this.lb[0].ua.toString()
  };
  L.prototype.Ee = function() {
    return new g("Line", [this.Ob.Rd(), this.lb[0].ua])
  };
  Ye.prototype = new ca;
  Ye.prototype.constructor = Ye;
  Ye.hc = ca.prototype;
  Ye.prototype.toString = function() {
    return"M" + this.lb[0].ua.toString()
  };
  Ze.prototype = new ca;
  Ze.prototype.constructor = Ze;
  Ze.hc = ca.prototype;
  Ze.prototype.Eo = function() {
    var a = this.Ob.Rd();
    if(this.Ob.Xh.match(/^[QqTt]$/)) {
      var b = this.Ob.Eo().Ph(a), a = a.Ph(b)
    }
    return a
  };
  Ze.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Rd(), this.Eo(), this.lb[0].ua])
  };
  Pd.prototype = new ca;
  Pd.prototype.constructor = Pd;
  Pd.hc = ca.prototype;
  Pd.prototype.$C = function() {
    var a = this.Ob.Rd();
    return this.Ob.Xh.match(/^[SsCc]$/) ? a.Ph(this.Ob.$t().Ph(a)) : a
  };
  Pd.prototype.$t = function() {
    return this.lb[0].ua
  };
  Pd.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Rd(), this.$C(), this.lb[0].ua, this.lb[1].ua])
  };
  ia.prototype = new ca;
  ia.prototype.constructor = ia;
  ia.hc = ca.prototype;
  ia.prototype.Ha = function(a, b, c, d) {
    this.Xh = a;
    this.wg = c;
    this.Ob = d;
    this.lb = [];
    a = this.Ob ? this.Ob.Rd() : new k(0, 0);
    for(d = 0;d < b.length;) {
      this.lb.push(new w(a.x + b[d], a.y + b[d + 1], c)), d += 2
    }
  };
  ia.prototype.toString = function() {
    var a = [], b = "", c;
    c = this.Ob ? this.Ob.Rd() : new k(0, 0);
    if(this.Ob == p || this.Ob.constructor != this.constructor) {
      b = this.Xh
    }
    for(var d = 0;d < this.lb.length;d++) {
      a.push(this.lb[d].ua.Ph(c).toString())
    }
    return b + a.join(" ")
  };
  Qd.prototype = new ia;
  Qd.prototype.constructor = Qd;
  Qd.hc = ia.prototype;
  Qd.prototype.Rd = function() {
    for(var a = this.Ob, b;a;) {
      if(a.Xh.match(/^[mMzZ]$/)) {
        b = a.Rd();
        break
      }
      a = a.Ob
    }
    return b
  };
  Qd.prototype.Ee = function() {
    return new g("Line", [this.Ob.Rd(), this.Rd()])
  };
  $e.prototype = new ia;
  $e.prototype.constructor = $e;
  $e.hc = ia.prototype;
  $e.prototype.Eo = function() {
    return this.lb[0].ua
  };
  $e.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Rd(), this.lb[0].ua, this.lb[1].ua])
  };
  af.prototype = new ia;
  af.prototype.constructor = af;
  af.hc = ia.prototype;
  af.prototype.$t = function() {
    return this.lb[1].ua
  };
  af.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Rd(), this.lb[0].ua, this.lb[1].ua, this.lb[2].ua])
  };
  bf.prototype = new ia;
  bf.prototype.constructor = bf;
  bf.hc = ia.prototype;
  bf.prototype.toString = function() {
    var a;
    a = this.lb[0].ua.Ph(this.Ob ? this.Ob.Rd() : new Point(0, 0));
    this.Ob.constructor != this.gx && this.Ob.constructor != cf && (cmd = this.Xh);
    return cmd + a.toString()
  };
  bf.prototype.Ee = function() {
    return new g("Line", [this.Ob.Rd(), this.lb[0].ua])
  };
  cf.prototype = new ia;
  cf.prototype.constructor = cf;
  cf.hc = ia.prototype;
  cf.prototype.toString = function() {
    return"m" + this.lb[0].ua.toString()
  };
  df.prototype = new ia;
  df.prototype.constructor = df;
  df.hc = ia.prototype;
  df.prototype.Eo = function() {
    var a = this.Ob.Rd();
    if(this.Ob.Xh.match(/^[QqTt]$/)) {
      var b = this.Ob.Eo().Ph(a), a = a.Ph(b)
    }
    return a
  };
  df.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Rd(), this.Eo(), this.lb[0].ua])
  };
  Rd.prototype = new ia;
  Rd.prototype.constructor = Rd;
  Rd.hc = ia.prototype;
  Rd.prototype.$C = function() {
    var a = this.Ob.Rd();
    return this.Ob.Xh.match(/^[SsCc]$/) ? a.Ph(this.Ob.$t().Ph(a)) : a
  };
  Rd.prototype.$t = function() {
    return this.lb[0].ua
  };
  Rd.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Rd(), this.$C(), this.lb[0].ua, this.lb[1].ua])
  };
  bd.prototype = new m;
  bd.prototype.constructor = bd;
  bd.hc = m.prototype;
  bd.prototype.Ha = function(a) {
    if("polygon" == a.localName) {
      bd.hc.Ha.call(this, a);
      a = a.getAttributeNS(p, "points").split(/[\s,]+/);
      this.lb = [];
      for(var b = 0;b < a.length;b += 2) {
        this.lb.push(new w(parseFloat(a[b]), parseFloat(a[b + 1]), this))
      }
    }else {
      e(Error("Polygon.init: Invalid SVG Node: " + a.localName))
    }
  };
  bd.prototype.xd = function() {
    if(this.kb != p) {
      for(var a = 0;a < this.lb.length;a++) {
        this.lb[a].xd(), this.lb[a].show(q)
      }
      this.kb.addEventListener("mousedown", this, q)
    }
  };
  bd.prototype.refresh = function() {
    for(var a = [], b = 0;b < this.lb.length;b++) {
      a.push(this.lb[b].ua.toString())
    }
    this.kb.setAttributeNS(p, "points", a.join(" "))
  };
  bd.prototype.Ee = function() {
    for(var a = [], b = 0;b < this.lb.length;b++) {
      a.push(this.lb[b].ua)
    }
    return new g("Polygon", [a])
  };
  sc.prototype = new m;
  sc.prototype.constructor = sc;
  sc.hc = m.prototype;
  sc.prototype.Ha = function(a) {
    if("rect" == a.localName) {
      sc.hc.Ha.call(this, a);
      var b = parseFloat(a.getAttributeNS(p, "x")), c = parseFloat(a.getAttributeNS(p, "y")), d = parseFloat(a.getAttributeNS(p, "width")), a = parseFloat(a.getAttributeNS(p, "height"));
      this.Vg = new w(b, c, this);
      this.Wg = new w(b + d, c + a, this)
    }else {
      e(Error("Rectangle.init: Invalid SVG Node: " + a.localName))
    }
  };
  sc.prototype.xd = function() {
    this.kb != p && (this.Vg.xd(), this.Wg.xd(), this.Vg.show(q), this.Wg.show(q), this.kb.addEventListener("mousedown", this, q))
  };
  sc.prototype.refresh = function() {
    var a = this.Vg.ua.min(this.Wg.ua), b = this.Vg.ua.max(this.Wg.ua);
    this.kb.setAttributeNS(p, "x", a.x);
    this.kb.setAttributeNS(p, "y", a.y);
    this.kb.setAttributeNS(p, "width", b.x - a.x);
    this.kb.setAttributeNS(p, "height", b.y - a.y)
  };
  sc.prototype.Ee = function() {
    return new g("Rectangle", [this.Vg.ua, this.Wg.ua])
  };
  To = {XC:function(a) {
    var b = 999999999, c = -999999999, d = 999999999, f = -999999999;
    a.kM(function(a) {
      a.x < b && (b = a.x);
      a.x > c && (c = a.x);
      a.y < d && (d = a.y);
      a.y > f && (f = a.y)
    });
    return{x:b, y:d, width:c - b, height:f - d}
  }, by:function(a) {
    return new k(a.x, a.y)
  }};
  Fo = k;
  pr = T;
  qr = sc;
  Go = f
})();
function rr(a) {
  this.F = a.r();
  this.Pq = [];
  this.cq = [];
  this.ih = [];
  this.oG = new W(this.F);
  this.QF = new W(this.F);
  this.UC = new W(this.F);
  this.tB = new W(this.F);
  a.appendChild(this.tB.G);
  a.appendChild(this.QF.G);
  a.appendChild(this.oG.G);
  a.appendChild(this.UC.G)
}
z = rr.prototype;
z.oG = p;
z.QF = p;
z.UC = p;
z.tB = p;
z.Pq = p;
z.cq = p;
z.ih = p;
z.F = p;
z.FK = q;
function sr(a) {
  if(!a.FK) {
    var b, c, d, f, g = a.Pq.length;
    for(b = 0;b < g;b++) {
      if(a.Pq[b]) {
        var k = a.Pq[b].d;
        c = a.Pq[b].i;
        f = k.li();
        for(d = 0;d < f;d++) {
          a.ih.push({c:k.fb(d), s:k.af ? 1 : Math.sin(Qq(k, d) * Math.PI / 180), i:c})
        }
      }
    }
    g = a.cq.length;
    for(b = 0;b < g;b++) {
      if(a.cq[b]) {
        k = a.cq[b].d;
        c = a.cq[b].i;
        f = k.li();
        for(d = 0;d < f;d++) {
          a.ih.push({c:k.fb(d), s:k.af ? -1 : Math.sin(Qq(k, d) * Math.PI / 180), i:c})
        }
      }
    }
    g = a.ih.length;
    for(b = 0;b < g;b++) {
      a.ih[b].s = 0 <= a.ih[b].s ? a.ih[b].s + 2 * a.ih[b].i : a.ih[b].s - 2 * a.ih[b].i
    }
    a.ih.sort(function(a, b) {
      return a.s - b.s
    });
    for(b = 0;b < g;b++) {
      a.QF.appendChild(a.ih[b].c.G)
    }
    a.FK = j
  }
}
function of() {
  Ek.call(this);
  this.Pd = new O;
  this.Id = this.zy = q;
  this.Wk = {};
  this.Wh = []
}
A.e(of, Ek);
z = of.prototype;
z.Eb = p;
z.qE = q;
z.zy = q;
z.jH = p;
z.Id = q;
z.Pi = 0.2;
z.ph = 0.45;
z.dk = p;
z.Wk = p;
z.Wh = p;
z.Qa = NaN;
z.Sb = NaN;
z.Pd = p;
z.ae = u("Pd");
z.pj = NaN;
z.Ro = NaN;
z.Kq = p;
z.jk = function() {
  return this.Qa * this.Yt()
};
z.Yt = function() {
  return this.Eb.jk()
};
z.Zj = -10;
z.cp = NaN;
z.rc = p;
z.Xr = function(a) {
  of.f.Xr.call(this, a);
  "doughnut" == a && (this.zy = j)
};
z.br = function() {
  return new or
};
z.kx = function() {
  return new mr
};
z.Nx = function(a, b, c, d) {
  return this.Eb ? this.Eb : this.Eb = of.f.Nx.call(this, a, b, c, d)
};
z.wx = s();
z.zk = s();
z.RG = function(a, b) {
  return this.Eb.Tw() && this.Eb.Yo ? this.Eb.bI(b, this.Eb.Kv) : b
};
z.Dq = function(a) {
  C(a, "enable_3d_mode") && (this.Id = K(a, "enable_3d_mode"));
  this.Id !== j && jf(this, a)
};
z.DI = function(a, b, c) {
  function d(a, b, c, d) {
    c = Math.atan2(d.y - c.y, d.x - c.x);
    0 > c && (c += 2 * Math.PI);
    c = 180 * c / Math.PI;
    b = (a + b) % 360;
    c = (360 + c % 360) % 360;
    a = (36E5 + a) % 360;
    b = (36E5 + b) % 360;
    return a < b ? a - 5 <= c && c <= b + 5 : a - 5 <= c || c <= b + 5
  }
  function f(a, b) {
    return Math.sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y))
  }
  var g = this.N.hb.aa;
  a.x = c.getAttribute("x") - g.D.Ub;
  a.y = c.getAttribute("y") - g.D.ma;
  c.setAttribute("x", a.x);
  c.setAttribute("y", a.y);
  for(var g = [], k = [{x:a.x, y:a.y}, {x:a.x + a.width, y:a.y}, {x:a.x, y:a.y + a.height}, {x:a.x + a.width, y:a.y + a.height}], l = 0;l < b.length;l++) {
    for(var n = b[l], m = new pr(n.D.G.firstChild), o = new qr(c), v = Go.YI(m, o), m = n.o.Sb, o = n.o.Qa, w = n.ae(), y = q, E = 0;E < v.Fa.length;E++) {
      var V = v.Fa[E], R = f(w, V);
      if(R > m - 5 && R < o + 5 && d(n.jb, n.kh, w, V)) {
        y = j;
        break
      }
    }
    if(y) {
      g.push(n)
    }else {
      y = j;
      for(v = 0;4 > v;v++) {
        if(E = k[v], R = f(w, E), R > m - 5 && R < o + 5) {
          if(!d(n.jb, n.kh, w, E)) {
            y = q;
            break
          }
        }else {
          y = q;
          break
        }
      }
      y ? g.push(n) : Xc(a, n.n()) && g.push(n)
    }
  }
  return g
};
z.Jo = x(16);
z.ck = 16;
function jr(a, b, c) {
  b = (a.zy ? a.Qa - a.Sb : a.Qa) * (c ? b : b + 1) / Gk(a);
  return a.zy ? a.Sb + b : b
}
z.jk = function() {
  return this.Qa * this.Eb.jk()
};
z.Yt = function() {
  return this.Eb.jk()
};
z.qa = function() {
  of.f.qa.call(this);
  this.dk && sr(this.dk)
};
z.nm = function(a) {
  this.k = this.Id ? tr(this, a) : ur(this, a);
  of.f.nm.call(this, a);
  this.Eb.fj && vr(this, a)
};
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
z.sy = s();
z.p = function(a, b, c) {
  this.jH = new W(this.r());
  this.ww.ia(this.jH);
  var d = Gk(this);
  if(this.Id) {
    this.dk = new rr(this.vt);
    var f = this.r(), g = f.hl, k = Cc(f, "filter");
    k.setAttribute("id", "smileBlur");
    f = Cc(f, "feGaussianBlur");
    f.setAttribute("in", "SourceGraphic");
    f.setAttribute("stdDeviation", "1.1");
    k.appendChild(f);
    g.appendChild(k)
  }
  of.f.p.call(this, a, b, c);
  for(b = 0;b < d;b++) {
    g = this.o[b].Fe();
    for(c = 0;c < g;c++) {
      Hk(this, b).Ga(c).HB()
    }
  }
  this.qE = 1 == d;
  this.k = this.Id ? tr(this, a) : ur(this, a);
  this.qE || (this.Eb.Lt = q);
  this.Eb.fj && (this.o[d - 1].fj = j, vr(this, a));
  return this.D
};
function ur(a, b) {
  var c = b, b = b.Ca(), d = Math.min(b.width, b.height);
  b.x += (b.width - d) / 2;
  b.y += (b.height - d) / 2;
  b.width = b.height = d;
  d /= 2;
  a.Pd.x = c.width / 2;
  a.Pd.y = c.height / 2;
  if(!(0 >= Gk(a))) {
    return a.qE && (a.Eb.yD || a.Eb.Lt) ? (c = d / (a.Eb.og() + a.Eb.jk()), a.Qa = c * a.Eb.og(), a.Sb = c * a.Eb.Sb) : (a.Qa = a.Eb.og() * d, a.Sb = a.Eb.Sb * d), b
  }
}
function tr(a, b) {
  var c = b, b = b.Ca(), d = Math.min(b.width / 2, b.height / (2 * a.ph + a.Pi));
  b.x = b.x + b.width / 2 - d;
  b.y = b.y + b.height / 2 - d * (a.ph + a.Pi / 2);
  b.width = 2 * d;
  b.height = 2 * d * a.ph + d * a.Pi;
  a.Pd.x = c.width / 2;
  a.Pd.y = c.height / 2 - d * a.Pi / 2;
  c = Gk(a);
  0 < c && (1 == c && (a.Eb.yD || a.Eb.Lt) ? (d /= a.Eb.og() + a.Eb.jk(), a.Qa = d * a.Eb.og(), a.Sb = d * a.Eb.Sb) : (a.Qa = a.Eb.og() * d, a.Sb = a.Eb.Sb * d), a.pj = a.Qa * a.ph, a.Ro = a.Sb * a.ph);
  return b
}
function vr(a, b) {
  var c = a.Eb.jE ? a.Eb.uE * a.Qa : a.Eb.uE, d = a.Eb.fx ? a.Eb.Xi : 0;
  a.rc = [];
  for(var f = a.o.length - 1, g = 0;g < Hk(a, f).Fe();g++) {
    var k = Hk(a, f).Ga(g);
    if(k.vc != p) {
      var l = (k.jb + k.Fo()) * Math.PI / 360;
      k.Mb().ka().wa.padding = c;
      k.Mb().ka().dc.padding = c;
      k.Mb().ka().kc.padding = c;
      k.Mb().ka().Yb.padding = c;
      k.Mb().ka().gc.padding = c;
      k.Mb().ka().nc.padding = c;
      var n = k.Mb().n(k, k.Mb().ka().wa, k.fb());
      a.Qa = Math.min(a.Qa, Math.abs((b.width - 2 * (n.width + d)) / (2 * (1 + a.Yt() + c / a.Qa) * Math.cos(l))));
      a.Qa = a.Id ? Math.min(a.Qa, Math.abs((b.height * a.ph - 2 * n.height) / (2 * (1 + a.Yt() + c / a.Qa) * a.ph * Math.sin(l))) / a.ph) : Math.min(a.Qa, Math.abs((b.height - 2 * n.height) / (2 * (1 + a.Yt() + c / a.Qa) * Math.sin(l))));
      a.rc.push(new Xq((k.jb + k.Fo()) / 2, n, k, a.rc.length));
      k.bp = a.rc.length - 1;
      -10 == a.Zj && (a.Zj = Math.acos(1 / (1 + c / a.Qa)))
    }
  }
  a.cp = c + a.Qa;
  a.Sb = a.Qa * a.Eb.Sb;
  if(a.Eb.yJ) {
    if(a.Eb.GK) {
      wr(a);
      var m;
      if(!a.Eb.Qs() && 0 < a.rc.length) {
        for(var o in a.rc) {
          m = a.rc[o], xr(m, q)
        }
        c = xr(a.rc[a.rc.length - 1]);
        for(o in a.rc) {
          m = a.rc[o];
          if(d = m != c) {
            f = a, g = m, d = c, k = $q(f, g, j), g = new P(k.x, k.y, g.ab, g.ob), f = $q(f, d, j), d = !Wc(g, new P(f.x, f.y, d.ab, d.ob))
          }
          d && (c = xr(m))
        }
      }else {
        for(o in a.rc) {
          m = a.rc[o], xr(m)
        }
      }
    }else {
      if(a.rm()) {
        o = a.rc.length - 1;
        for(d = 0;d <= o;d++) {
          if(c = a.rc[d], k = d == o ? a.rc[0] : a.rc[d + 1], f = $q(a, c, j), g = $q(a, k, j), f = new P(f.x, f.y, c.Ka(), c.vb()), g = new P(g.x, g.y, k.Ka(), k.Ka()), Wc(f, g) || !(c.ac > k.ac)) {
            k = Math.ceil(c.ac - 180 * a.Zj / Math.PI);
            for(l = c.ac;l > k;l--) {
              if(c.Wd(l), n = $q(a, c, j), f.x = n.x, f.y = n.y, !Wc(f, g)) {
                c.Wd(l);
                break
              }
            }
          }
        }
        a.Zw ? m = a.Zw() : a.rm && (m = a.rm());
        if(m) {
          m = a.rc.length - 1;
          for(c = 0;c <= m;c++) {
            if(o = a.rc[c], g = 0 == c ? a.rc[m] : a.rc[c - 1], d = $q(a, o, j), f = $q(a, g, j), d = new P(d.x, d.y, o.Ka(), o.vb()), f = new P(f.x, f.y, g.Ka(), g.vb()), Wc(d, f) || !(o.ac > g.ac)) {
              g = Math.ceil(o.ac + 180 * a.Zj / Math.PI);
              for(k = o.ac;k < g;k++) {
                if(o.Wd(k), l = $q(a, o, j), d.x = l.x, d.y = l.y, !Wc(d, f)) {
                  o.Wd(k);
                  break
                }
              }
            }
          }
          a.Zw || a.rm && a.rm()
        }
      }
      for(m = 0;m < a.rc.length;m++) {
        o = a.rc[m], o.isEnabled() && !yr(a, o) ? a.Eb.Qs() ? (o.zg(j), hr(o.j, j)) : (o.zg(q), hr(o.j, q)) : (o.zg(j), hr(o.j, j))
      }
    }
  }
}
function $q(a, b, c) {
  var d = b.j.ae();
  if(a.Id) {
    var f = a.Qa * a.ph, g = a.cp - a.Qa;
    d.x += (a.Qa + g) * Math.cos(b.ac * Math.PI / 180);
    d.y += (f + g) * Math.sin(b.ac * Math.PI / 180);
    d.y += a.Qa * a.Pi / 2
  }else {
    d.x += a.cp * Math.cos(b.ac * Math.PI / 180), d.y += a.cp * Math.sin(b.ac * Math.PI / 180)
  }
  f = new P(0, 0, b.Ka(), b.vb());
  a.Eb.fx && (d.x = 90 < b.ac && 270 > b.ac ? d.x - a.Eb.Xi : d.x + a.Eb.Xi);
  c && (0 > Math.cos(b.ac * Math.PI / 180) && ud(d, 0, f, 0), xd(d, 1, f, 0));
  return d
}
function er(a, b) {
  var b = b - a.ae().y, c;
  if(a.Id) {
    c = a.Qa * a.ph;
    var d = a.cp - a.Qa, b = b - a.Qa * a.Pi / 2;
    c = 180 * Math.asin(b / (c + d)) / Math.PI
  }else {
    c = 180 * Math.asin(b / a.cp) / Math.PI
  }
  return isNaN(c) ? 0 < b ? 90.01 : -89.99 : c
}
function wr(a) {
  var b = [], c, d = a.rc.length;
  for(c = 0;c < d;c++) {
    b.push(new Yq(a.rc[c], 180 * Math.acos(a.Qa / a.cp) / Math.PI, a))
  }
  b.sort(function(a, b) {
    return a.hr - b.hr
  });
  a = [];
  d = b.length;
  for(c = 0;c < d;c++) {
    for(var f = b[c], g = a.pop();g != p;) {
      if(g.Zw(f)) {
        br(g, f), f = g, g = a.pop()
      }else {
        a.push(g);
        break
      }
    }
    a.push(f)
  }
  d = a.length;
  for(c = 0;c < d;c++) {
    cr(a[c])
  }
}
function xr(a, b) {
  if(b == p || b == h) {
    b = j
  }
  a.zg(b);
  hr(a.j, b);
  return b ? a : p
}
z.rm = function() {
  for(var a = this.rc.length, b = 0;b < a;b++) {
    if(!yr(this, this.rc[b])) {
      return j
    }
  }
  return q
};
function yr(a, b) {
  for(var c = $q(a, b, j), c = new P(c.x, c.y, b.width, b.height), d = 0;d < a.rc.length;d++) {
    if(d != b.na) {
      var f = a.rc[d];
      if(f.isEnabled()) {
        var g = $q(a, f, j);
        if(Wc(c, new P(g.x, g.y, f.width, f.height))) {
          return q
        }
      }
    }
  }
  return j
}
;function zr() {
  this.yg = this.Te = -1;
  this.yx = 1;
  this.Oi = this.oi = 5
}
z = zr.prototype;
z.Te = p;
z.yg = p;
z.yx = p;
z.oi = p;
z.ur = q;
z.Oi = p;
z.vr = q;
z.g = function(a) {
  C(a, "columns") && (this.Te = M(a, "columns"));
  C(a, "rows") && (this.yg = M(a, "rows"));
  if(C(a, "direction")) {
    var b;
    a: {
      switch(N(a, "direction")) {
        default:
        ;
        case "vertical":
          b = 0;
          break a;
        case "horizontal":
          b = 1
      }
    }
    this.yx = b
  }
  C(a, "vertical_padding") && (b = F(a, "vertical_padding"), Mb(b) ? (this.Oi = Nb(b), this.vr = j) : this.Oi = Ob(b));
  C(a, "horizontal_padding") && (b = F(a, "horizontal_padding"), Mb(b) ? (this.oi = Nb(b), this.ur = j) : this.oi = Ob(b))
};
z.p = function(a) {
  -1 == this.Te && -1 != this.yg && (this.Te = Math.ceil(a.length / this.yg));
  -1 == this.Te && -1 == this.yg && (this.Te = 4, 4 >= a.length && (this.Te = a.length));
  this.yg = Math.ceil(a.length / this.Te)
};
z.fA = function(a, b) {
  var c = 0, d = 0, f = 0, g = 0;
  this.ur && (this.oi *= b.width);
  this.vr && (this.Oi *= b.height);
  for(var k = b.width - this.oi * (this.Te - 1), l = b.height - this.Oi * (this.yg - 1), n, m = 0;m < a.length;m++) {
    if(n = a[m], n.fA(new P(b.x + f, b.y + g, k / this.Te, l / this.yg)), 1 == this.yx && (d++, f += n.Ac.width + this.oi, d > this.Te - 1 && (f = d = 0, g += n.Ac.height + this.Oi, c++)), 0 == this.yx) {
      c++, g += n.Ac.height + this.Oi, c > this.yg - 1 && (g = c = 0, f += n.Ac.width + this.oi, d++)
    }
  }
};
function Ar() {
  Ek.call(this);
  this.Id = q;
  this.Gu = new zr
}
A.e(Ar, Ek);
z = Ar.prototype;
z.Id = p;
z.Gu = p;
z.Dq = function(a) {
  Ar.f.Dq.call(this, a);
  C(a, "enable_3d_mode") && (this.Id = q);
  F(a, "multiple_series_layout") && this.Gu.g(F(a, "multiple_series_layout"))
};
z.p = function(a, b, c) {
  a.Ca();
  b = Ar.f.p.call(this, a, b, c);
  a = a.Ca();
  a.x = 0;
  a.y = 0;
  this.Y && Xd(this.Y, a);
  this.Gu.p(this.o);
  this.Gu.fA(this.o, a);
  return b
};
z.nm = function(a) {
  a.Ca();
  var b = a.Ca();
  b.x = 0;
  b.y = 0;
  this.Y && Xd(this.Y, b);
  this.Gu.fA(this.o, b);
  Ar.f.nm.call(this, a)
};
z.Cb = function() {
  Ar.f.Cb.call(this);
  this.b.add("%DataPlotMaxYValuePointName", "");
  this.b.add("%DataMaxYValuePointSeriesName", "");
  this.b.add("%DataPlotMinYValuePointName", "");
  this.b.add("%DataPlotMinYValuePointSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeriesName", "");
  this.b.add("%DataPlotMinYSumSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeries", 0);
  this.b.add("%DataPlotMinYSumSeries", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%DataPlotMaxYValuePointName":
    ;
    case "%DataMaxYValuePointSeriesName":
    ;
    case "%DataPlotMinYValuePointName":
    ;
    case "%DataPlotMinYValuePointSeriesName":
    ;
    case "%DataPlotMaxYSumSeriesName":
    ;
    case "%DataPlotMinYSumSeriesName":
      return 1;
    case "%DataPlotMaxYSumSeries":
    ;
    case "%DataPlotMinYSumSeries":
      return 2
  }
  return Ar.f.Pa.call(this, a)
};
z.SE = function(a) {
  Ar.f.SE.call(this, a);
  for(var b = a.Fe(), c = q, d = 0, f, g, k = 0;k < b;k++) {
    f = a.Ga(k), g = f.ta(), f.Dg = g, 0 == g && d++
  }
  d == b && (c = j);
  if(c) {
    for(k = 0;k < b;k++) {
      a.Ga(k).Dg = 1
    }
  }
};
var Br = {chart:{styles:{funnel_style:{border:{type:"Solid", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}, enabled:"True"}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", 
opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", 
type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, 
fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}}, chart_settings:{data_plot_background:{inside_margin:{all:"0"}}}, data_plot_settings:{funnel_series:{label_settings:{}, marker_settings:{}, tooltip_settings:{}, 
apply_palettes_to:"points", neck_height:"0.2", min_width:"0.2", mode:"circular", padding:"0.02", min_point_size:"0.02"}}}};
function Cr() {
}
A.e(Cr, bl);
Cr.prototype.zh = function() {
  return this.$f(Cr.f.zh.call(this), Br)
};
function Dr() {
  this.sa = new O;
  this.P = new O;
  this.X = new O;
  this.ha = new O;
  this.k = new P
}
z = Dr.prototype;
z.k = p;
z.sa = p;
z.P = p;
z.X = p;
z.ha = p;
z.kD = u("ha");
z.Qn = p;
z.km = p;
z.n = u("k");
z.kD = u("ha");
z.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y;
  this.P.x = b ? f + c / 2 - a.oe / 2 * c : f + c / 2 - a.xe / 2 * c;
  this.ha.x = b ? f + c / 2 + a.oe / 2 * c : f + c / 2 + a.xe / 2 * c;
  this.sa.x = b ? f + c / 2 - a.xe / 2 * c : f + c / 2 - a.oe / 2 * c;
  this.X.x = b ? f + c / 2 + a.xe / 2 * c : f + c / 2 + a.oe / 2 * c;
  this.P.y = this.ha.y = b ? g + d - a.Re * d : g + a.Md * d;
  this.sa.y = this.X.y = b ? g + d - a.Md * d : g + a.Re * d
};
z.dw = function(a) {
  this.sa.x -= a;
  this.P.x -= a;
  this.X.x -= a;
  this.ha.x -= a
};
z.bg = function(a, b) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.sa.y + this.P.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.X.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.ha.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.sa.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
function Er() {
  Dr.call(this);
  this.de = new O;
  this.Ke = new O;
  this.Eg = new O;
  this.hg = new O;
  this.al = new O;
  this.Xk = new O;
  this.pm = new O;
  this.no = new O;
  this.Ag = 0
}
A.e(Er, Dr);
z = Er.prototype;
z.de = p;
z.Ke = p;
z.Eg = p;
z.hg = p;
z.al = p;
z.Xk = p;
z.pm = p;
z.no = p;
z.Ww = p;
z.Ag = p;
z.SK = p;
z.bg = function(a, b, c) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.Eg.y + this.hg.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      this.P.y != this.de.y && (b.x = b.y <= this.de.y ? c ? this.P.x : this.P.x + (this.de.x - this.P.x) * (b.y - this.P.y) / (this.de.y - this.P.y) : c ? this.de.x - (this.de.x - this.sa.x) / (this.sa.y - this.de.y) * (b.y - this.de.y) : this.sa.x);
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      this.ha.y != this.Ke.y && (b.x = b.y <= this.de.y ? c ? this.ha.x : this.ha.x - (this.ha.x - this.Ke.x) / (this.Ke.y - this.ha.y) * (b.y - this.ha.y) : c ? this.Ke.x + (this.X.x - this.Ke.x) / (this.X.y - this.Ke.y) * (b.y - this.Ke.y) : this.X.x);
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.hg.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.Eg.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.hg.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
z.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y, k = a.o.Ag, l = a.o.Ac.width, n = a.o.Ac.height, m = a.cb.Xu, o = a.cb.oM * c, v = a.cb.JC, c = d * v;
  0 == k ? c > l && (c = l, d = l / v, g = (n - d) / 2) : c > k && (c = k, d = k / v, g = (n - d) / 2);
  this.Ag = c - l;
  k = a.cb.Yl * d;
  a.Id || (k = 0);
  this.km = a.oe * k;
  this.Qn = a.xe * k;
  this.Ww = a.rq * k;
  this.Xk.x = this.hg.x = this.no.x = this.pm.x = this.al.x = this.Eg.x = f + l / 2;
  !b && 0 == m && 0 == o ? (this.Xk.y = g + d - a.Md * d, this.no.y = g + d - a.oo * d, this.al.y = g + d - a.Re * d, this.hg.y = this.Xk.y + 2 * this.km, this.pm.y = this.no.y + 2 * this.Ww, this.Eg.y = this.al.y + 2 * this.Qn) : (this.hg.y = b ? g + a.Re * d : g + d - a.Md * d, this.pm.y = b ? g + a.oo * d : g + d - a.oo * d, this.Eg.y = b ? g + a.Md * d : g + d - a.Re * d, this.Xk.y = this.hg.y - 2 * this.km, this.no.y = this.pm.y - 2 * this.Ww, this.al.y = this.Eg.y - 2 * this.Qn);
  this.sa.y = this.X.y = this.hg.y - this.km;
  this.de.y = this.Ke.y = this.pm.y - this.Ww;
  this.P.y = this.ha.y = this.Eg.y - this.Qn;
  this.sa.x = f + l / 2 - a.oe * c / 2;
  this.de.x = f + l / 2 - a.rq * c / 2;
  this.P.x = f + l / 2 - a.xe * c / 2;
  this.X.x = f + l / 2 + a.oe * c / 2;
  this.Ke.x = f + l / 2 + a.rq * c / 2;
  this.ha.x = f + l / 2 + a.xe * c / 2
};
z.dw = function(a) {
  this.sa.x -= a;
  this.de.x -= a;
  this.P.x -= a;
  this.X.x -= a;
  this.Ke.x -= a;
  this.ha.x -= a;
  this.Eg.x -= a;
  this.hg.x -= a;
  this.al.x -= a;
  this.Xk.x -= a;
  this.no.x -= a;
  this.pm.x -= a
};
function Fr() {
  Er.call(this)
}
A.e(Fr, Er);
Fr.prototype.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y;
  this.km = a.oe * a.cb.Yl * d;
  this.Qn = a.xe * a.cb.Yl * d;
  a.Id || (this.Qn = this.km = 0);
  this.hg.x = this.Xk.x = this.al.x = this.Eg.x = f + c / 2;
  this.hg.y = b ? g + d - a.Re * d : g + a.Re * d;
  this.Eg.y = b ? g + d - a.Md * d : g + a.Md * d;
  this.Xk.y = this.hg.y - 2 * this.km;
  this.al.y = this.Eg.y - 2 * this.Qn;
  this.sa.x = f + c / 2 - a.oe * c / 2;
  this.X.x = f + c / 2 + a.oe * c / 2;
  this.P.x = f + c / 2 - a.xe * c / 2;
  this.ha.x = f + c / 2 + a.xe * c / 2;
  this.sa.y = this.X.y = this.hg.y - this.km;
  this.P.y = this.ha.y = this.Eg.y - this.Qn
};
Fr.prototype.bg = function(a, b) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.sa.y + this.P.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.X.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.ha.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.sa.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
function Gr() {
}
A.e(Gr, Mf);
Gr.prototype.Ta = function() {
  this.$a.Ta(this.G)
};
function Hr() {
}
A.e(Hr, Nf);
Hr.prototype.Ta = function() {
  this.$a.Ta(this.G)
};
function Ir() {
}
A.e(Ir, Of);
z = Ir.prototype;
z.Yc = function() {
  var a = this.j.ka();
  a.vn() && (this.Db = new Gr, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Hr, this.Fb.p(this.j, this))
};
z.createElement = function() {
  var a = this.j.r().ja();
  a.setAttribute("d", this.nl());
  return a
};
z.nl = function() {
  return this.j.nl()
};
z.update = function(a, b) {
  b && this.j.Rc();
  Ir.f.update.call(this, a, b)
};
z.Ta = function(a) {
  a.setAttribute("d", this.nl())
};
function Jr() {
  $.call(this)
}
A.e(Jr, Qf);
Jr.prototype.lc = function() {
  return new Ir
};
Jr.prototype.cc = function() {
  return Pf
};
function Kr(a) {
  switch(a) {
    default:
    ;
    case "outsideleft":
      return 1;
    case "outsideleftincolumn":
      return 0;
    case "outsiderightincolumn":
      return 2;
    case "outsideright":
      return 3;
    case "inside":
      return 4
  }
}
function Lr() {
}
A.e(Lr, Rn);
Lr.prototype.Ib = function(a, b, c) {
  return a.Jg(b, this, this.n(a, b, c))
};
function Mr() {
  this.Ge()
}
A.e(Mr, go);
z = Mr.prototype;
z.oe = p;
z.xe = p;
z.Re = p;
z.Md = p;
z.da = p;
z.xg = p;
z.du = function(a) {
  return 1 - a * (this.o.ba.length - 1)
};
z.Dg = p;
z.p = function(a) {
  Mr.f.p.call(this, a)
};
z.update = function() {
  Mr.f.update.call(this);
  this.qC(this.r())
};
z.Wa = function() {
  Mr.f.Wa.call(this);
  this.oF()
};
z.oF = function() {
  if(this.lk()) {
    var a = this.r();
    this.Bf.setAttribute("d", this.Wt(a));
    this.Ab().wm.se() && this.Zz(a, this.Bf)
  }
};
z.Bf = p;
z.qt = p;
z.qC = function(a) {
  this.lk() && (this.Bf = a.ja(), this.Bf.setAttribute("d", this.Wt(a)), this.Zz(a, this.Bf), this.D.appendChild(this.Bf))
};
z.Zz = function(a, b) {
  b.setAttribute("style", Ld(this.Ab().wm, a, this.qt))
};
z.Wt = function() {
  var a = this.Ab(), b = a.dp, c = a.wm, d = a.Xi;
  if(4 == b) {
    return""
  }
  var f = new O, a = new O, g = new O, k = new P, l = new O, l = this.be(this.Ea.ka()), k = this.Ea.n(this, l, this.vc), l = this.Jg(l, this.Ea, k);
  switch(b) {
    case 1:
    ;
    case 0:
      f.x = (this.da.P.x + this.da.sa.x) / 2;
      f.y = (this.da.P.y + this.da.sa.y) / 2;
      a.x = l.x + k.width;
      a.y = l.y + k.height / 2;
      g.y = a.y;
      g.x = a.x + d;
      break;
    case 3:
    ;
    case 2:
      f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, a.x = l.x, a.y = l.y + k.height / 2, g.y = a.y, g.x = a.x - d
  }
  c.se() && (this.qt = new P(Math.min(f.x, a.x), Math.min(f.y, a.y), Math.max(f.x, a.x) - Math.min(f.x, a.x), Math.max(f.y, a.y) - Math.min(f.y, a.y)));
  b = S(f.x, f.y);
  b += U(g.x, g.y);
  return b += U(a.x, a.y)
};
z.lk = function() {
  return this.vc && this.Ab().lk()
};
z.Rc = function() {
  var a = this.o.vb();
  this.k.x = Math.min(this.da.P.x, this.da.sa.x);
  this.k.y = Math.min(this.da.P.y, this.da.sa.y);
  this.k.width = Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.height = this.xg * a
};
z.FF = function() {
  this.oe = this.Re;
  this.xe = this.Md
};
z.CK = function(a, b, c, d, f) {
  this.xg = a ? 1 / (this.o.ba.length - 1) : this.Dg < b ? b / d : this.Dg / d;
  this.xg *= this.du(c, f);
  this.Re = this.o.Jf;
  this.o.Jf -= this.xg;
  this.Md = this.o.Jf;
  this.o.Jf -= c
};
z.Jg = function(a, b, c) {
  var d = this.cb.dp, f = new O;
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d ? (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, xd(f, 0, c, a.ga()), xd(f, 1, c, a.ga())) : 3 == d ? (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, xd(f, 1, c, a.ga()), xd(f, 1, c, a.ga())) : 2 == d && this.Ea != p ? (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.da.X.y) / 2, xd(f, 1, c, a.ga())) : 0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + 
  this.da.X.y) / 2, xd(f, 1, c, a.ga()));
  return f
};
function Nr() {
  co.call(this)
}
A.e(Nr, Tq);
z = Nr.prototype;
z.Ac = p;
z.fA = t("Ac");
z.Jf = 1;
z.rv = p;
z.Yp = p;
z.Ag = 0;
z.vb = function() {
  return this.Ac.height
};
z.Ka = function() {
  return this.Ac.width
};
z.bA = function() {
  for(var a = new O, b = new P, c = this.Ka(), d = this.vb(), f = c / (2 * d), g, k = this.ba.length, l, n, m = 0;m < k;m++) {
    if(g = this.ba[m], g.Ea.isEnabled() && !g.Gb && (l = (g.da.X.x + g.da.kD.x) / 2, b = g.Mb().n(g, g.Mb().ka().wa, this.Mb().fb()), a = g.Jg(g.Mb().ka().wa, g.Mb(), b), n = g.Mb().ka().wa.ga(), g = (g.Re + g.Md) * d / 2, a.x - b.width < this.Ac.x || a.x + b.width > this.Ka() || l + b.width > this.Ka())) {
      f = Math.min(f, (c - b.width - n) / (d + g))
    }
  }
  this.Ag = 2 * d * f
};
function Or(a) {
  for(var b = a.cb.dp, c = a.cb.Nb(), d, f = 0;f < a.ba.length;f++) {
    switch(d = a.ba[f], b) {
      case 1:
        d.da.Kp(d, c, a.Ag);
        d.da.dw((a.Ag - a.Ka()) / 2);
        break;
      case 3:
        d.da.Kp(d, c, a.Ag);
        d.da.dw((a.Ka() - a.Ag) / 2);
        break;
      case 0:
        d.da.Kp(d, c, a.Ag);
        d.da.dw((a.Ag - a.Ka()) / 2);
        break;
      case 2:
        d.da.Kp(d, c, a.Ag), d.da.dw((a.Ka() - a.Ag) / 2)
    }
  }
}
z.tn = x(j);
z.Ug = function() {
  this.bA();
  Or(this)
};
z.zn = function() {
  this.bA();
  Or(this)
};
function Pr() {
  eo.apply(this);
  this.Yl = 0.09;
  this.Ia = 0.02;
  this.iz = 0.01;
  this.dp = 1;
  this.Bm = this.Td = q;
  this.Xi = 5;
  this.JC = 0.6
}
A.e(Pr, fo);
z = Pr.prototype;
z.Yl = p;
z.Ia = p;
z.ga = u("Ia");
z.iz = p;
z.wm = p;
z.lk = function() {
  return this.wm && this.wm.isEnabled()
};
z.wm = p;
z.Xi = p;
z.dp = p;
z.Td = p;
z.Nb = u("Td");
z.Bm = p;
z.JC = p;
z.ZG = x(q);
z.g = function(a, b) {
  Pr.f.g.call(this, a, b);
  C(a, "depth_3d") && (this.Yl = Sb(F(a, "depth_3d")));
  C(a, "padding") && (this.Ia = Sb(F(a, "padding")));
  C(a, "min_point_size") && (this.iz = Sb(F(a, "min_point_size")));
  C(a, "fit_aspect") && (this.JC = M(a, "fit_aspect"));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  C(a, "data_is_width") && (this.Bm = K(a, "data_is_width"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    C(c, "placement_mode") && (this.dp = Kr(Wb(c, "placement_mode")))
  }
  Ub(a, "connector") && (c = F(a, "connector"), this.wm = new Kd, this.wm.g(c), C(c, "padding") && (this.Xi = M(c, "padding")))
};
z.Aq = function() {
  return new Lr
};
function Qr() {
  this.da = new Er;
  this.Ge()
}
A.e(Qr, Mr);
z = Qr.prototype;
z.oo = p;
z.rq = p;
z.Id = p;
z.Rc = function() {
  var a = this.cb.Xu, b = this.o.vb();
  this.k.x = Math.min(this.da.de.x, this.da.P.x, this.da.sa.x);
  this.k.width = this.Re < a && this.Md < a ? this.da.ha.x - this.da.sa.x : Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.y = Math.min(this.da.sa.y, this.da.de.y, this.da.P.y);
  this.k.height = b * this.xg
};
z.FF = function() {
  if(this.cb.Bm) {
    if(this.na != this.o.ba.length - 1) {
      var a = this.na, b = this.cb.Tk;
      this.oe = (this.Dg - this.o.b.get("%SeriesYMin")) / (this.NN.b.get("%SeriesYMax") - this.NN.b.get("%SeriesYMin")) * (1 - b) + b;
      this.oe < b && (this.oe = b);
      this.o.Ga(a + 1) != p ? (this.xe = this.o.Ga(a + 1).Dg - this.o.b.get("%SeriesYMin") / (this.o.b.get("%SeriesYMax") - this.o.b.get("%SeriesYMin")) * (1 - b) + b, this.xe < b && (this.xe = b)) : this.xe = (this.Dg - this.o.b.get("%SeriesYMin")) / (this.o.b.get("%SeriesYMax") - this.o.b.get("%SeriesYMin")) * (1 - b) + b
    }
  }else {
    var a = this.cb.Xu, b = this.cb.Tk, c = b * (1 - a) / (1 - b), d = this.cb.Nb();
    this.oe = d ? (this.Re - a + c) / (1 - a + c) : (this.Md - a + c) / (1 - a + c);
    this.xe = d ? (this.Md - a + c) / (1 - a + c) : (this.Re - a + c) / (1 - a + c);
    this.oo = this.Md;
    this.rq = d ? this.xe : this.oe;
    this.Md < a && this.Re < a ? this.rq = this.xe = this.oe = b : this.Md < a && (this.oo = a, this.rq = b, d ? this.xe = b : this.oe = b)
  }
};
z.du = function(a, b) {
  var c = this.cb.Bm, d = this.cb.Tk, f = this.cb.Nb();
  this.o.b.get("%SeriesYSum");
  return 0 == this.cb.Tk && !c ? 1 - a * (this.o.Yp - 1) : f ? 1 - a * (this.o.Yp - 1) - 2 * d * b : 1 - a * (this.o.Yp - 1) - 2 * b
};
z.Jg = function(a, b, c) {
  var d = this.cb.dp, f = new O, g = this.cb.Nb();
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d && (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, this.oo != this.Md && (f.x = f.y >= this.da.Ke.y ? g ? (this.da.de.x + this.da.sa.x) / 2 : this.da.de.x : g ? this.da.de.x : (this.da.de.x + this.da.P.x) / 2), ud(f, 0, c, a.ga()), xd(f, 1, c, a.ga()));
  3 == d && (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, this.oo != this.Md && (f.x = f.y >= this.da.Ke.y ? g ? (this.da.Ke.x + this.da.X.x) / 2 : this.da.Ke.x : g ? this.da.Ke.x : (this.da.Ke.x + this.da.ha.x) / 2), ud(f, 1, c, a.ga()), xd(f, 1, c, a.ga()));
  2 == d && this.Ea != p && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.aM.X.y) / 2, xd(f, 1, c, a.ga()));
  0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + this.da.X.y) / 2, xd(f, 1, c, a.ga()));
  return f
};
z.bg = function(a, b) {
  this.da.bg(b, a, this.cb.Nb())
};
function Rr() {
  Qr.call(this);
  this.Id = q
}
A.e(Rr, Qr);
Rr.prototype.p = function(a) {
  Rr.f.p.call(this, a);
  return this.D
};
Rr.prototype.nl = function() {
  return this.ZB()
};
Rr.prototype.ZB = function() {
  var a = S(this.da.P.x, this.da.P.y), a = a + U(this.da.de.x, this.da.de.y), a = a + U(this.da.sa.x, this.da.sa.y), a = a + U(this.da.X.x, this.da.X.y), a = a + U(this.da.Ke.x, this.da.Ke.y), a = a + U(this.da.ha.x, this.da.ha.y), a = a + U(this.da.P.x, this.da.P.y);
  return a + " Z"
};
Rr.prototype.du = function(a) {
  return 1 - a * (this.o.Yp - 1)
};
function Sr() {
  Qr.call(this);
  this.Id = j
}
A.e(Sr, Qr);
function Tr() {
  Qr.call(this);
  this.Id = q;
  this.aM = new Fr
}
A.e(Tr, Qr);
z = Tr.prototype;
z.Tk = p;
z.CK = function(a, b, c, d, f) {
  this.Tk = this.o.b.get("%SeriesYMin") / this.o.b.get("%SeriesYMax");
  f = this.o.Ga(0);
  a = this.o.Ga(this.na - 1);
  f = this.cb.Nb() ? f.Dg / this.o.b.get("%SeriesYMax") * this.cb.Yl : 0;
  this.Id || (f = 0);
  this.xg = 1 / (this.o.ba.length - 1);
  this.xg *= this.du(c, f);
  this.Re = this.o.Jf;
  this.o.Jf -= this.xg;
  this.Md = this.o.Jf;
  this.o.Jf -= c;
  this.Md -= 2 * f;
  this.Re -= 2 * f;
  this.na == this.o.ba.length - 1 && (this.Md = this.Re = a.Md, this.xg = 0)
};
z.du = function(a, b) {
  var c = this.o.Ga(0), d = this.o.Ga(this.o.ba.length - 1), b = this.cb.Nb() ? c.Dg / this.o.b.get("%SeriesYMax") * this.cb.Yl : d.Dg / this.o.b.get("%SeriesYMax") * this.cb.Yl;
  this.Id || (b = 0);
  return 1 - a * (this.o.ba.length - 2) - 2 * b
};
z.FF = function() {
  var a = this.o.Ga(this.na + 1);
  this.oe = this.Dg / this.o.b.get("%SeriesYMax");
  this.xe = a != p ? a.Dg / this.o.b.get("%SeriesYMax") : this.oe
};
z.Rc = function() {
  var a = this.o.vb();
  this.k.x = Math.min(this.da.P.x, this.da.sa.x);
  this.k.width = Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.y = Math.min(this.da.sa.y, this.da.P.y);
  this.k.height = a * this.xg
};
z.Jg = function(a, b, c) {
  var d = this.cb.dp, f = new O, g = this.cb.Nb();
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d && (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, ud(f, 0, c, a.ga()), xd(f, 1, c, a.ga()), f.x < this.o.Ac.x && (f.x = this.o.Ac.x));
  3 == d && (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, ud(f, 1, c, a.ga()), xd(f, 1, c, a.ga()), f.x + this.Ea.n(this, this.Ea.ka().wa, b).width > this.o.Ac.x + this.o.Ka() && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width));
  2 == d && this.Ea != p && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.da.X.y) / 2, wd(f, 1, c, a.ga()));
  0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + this.da.X.y) / 2, xd(f, 1, c, a.ga()));
  this.na == this.o.ba.length - 1 && (f.y = g ? f.y - c.height / 2 : f.y + c.height / 2);
  return f
};
function Ur() {
  Tr.call(this);
  this.Id = j
}
A.e(Ur, Tr);
function Vr() {
  co.call(this)
}
A.e(Vr, Nr);
Vr.prototype.bA = function() {
  for(var a = new W(this.ca().r()), b = new O, c = new P, d = this.Ka(), f = this.vb(), b = this.cb.Xu * f, c = this.cb.Tk * d, g = b - (f - b) * c / (d - c), k = d / (f - g) / 2, l, n, m, o = 0;o < this.ba.length;o++) {
    if(l = this.ba[o], l.Mb().isEnabled() && !l.Gb && (n = (l.da.X.x + l.da.ha.x) / 2, c = l.Mb().n(l, l.Mb().ka().wa, a), b = l.Jg(l.Mb().ka().wa, p, c), m = l.Mb().ka().wa.ga(), l = (l.Re + l.Md) * f / 2 - g, b.x - c.width < this.Ac.x || b.x + c.width > this.Ka() || n + c.width > this.Ka())) {
      k = Math.min(k, (d - c.width - m) / (f - g + l))
    }
  }
  this.Ag = 2 * (f - g) * k
};
Vr.prototype.g = function(a, b) {
  Vr.f.g.call(this, a, b)
};
function Wr() {
  Pr.call(this)
}
A.e(Wr, Pr);
z = Wr.prototype;
z.Tk = p;
z.oM = u("Tk");
z.Xu = p;
z.vg = p;
z.ml = u("vg");
z.uh = function() {
  var a;
  a = this.aa.Id ? this.Bm ? new Xr : new Yr : this.Bm ? new Zr : new $r;
  a.gh(19);
  return a
};
z.Ah = x("funnel_series");
z.Rb = x("funnel_style");
z.g = function(a, b) {
  Wr.f.g.call(this, a, b);
  C(a, "min_width") && (this.Tk = Sb(F(a, "min_width")));
  C(a, "neck_height") && (this.Xu = Sb(F(a, "neck_height")));
  if(C(a, "mode")) {
    switch(Wb(a, "mode")) {
      case "square":
        this.vg = as;
        break;
      case "circular":
        this.vg = bs
    }
  }
};
z.Qb = function() {
  return new Jr
};
function $r() {
  co.call(this)
}
A.e($r, Vr);
$r.prototype.qe = function() {
  return new Rr
};
$r.prototype.Ug = function() {
  this.Jf = 1;
  for(var a, b = this.cb.Bm, c = this.cb.iz * this.b.get("%SeriesYSum"), d = this.Yp = this.rv = 0;d < this.ba.length;d++) {
    a = this.ba[d], this.Yp++, d == this.ba.length - 1 && b ? this.Yp-- : this.rv = a.Dg < c ? this.rv + c : this.rv + a.Dg
  }
  a = this.cb.ga();
  for(var b = this.cb.Bm, c = this.cb.Nb(), d = this.cb.iz * this.b.get("%SeriesYSum"), f = this.cb.Yl, g = 0;g < this.ba.length;g++) {
    var k = this.ba[g];
    k.CK(b, d, a, this.rv, f);
    k.FF();
    k.da.Kp(k, c, this.Ka())
  }
  $r.f.Ug.call(this)
};
function Yr() {
  co.call(this)
}
A.e(Yr, Vr);
Yr.prototype.qe = function() {
  return new Sr
};
Yr.prototype.kB = x(q);
function Zr() {
  co.call(this)
}
A.e(Zr, Vr);
Zr.prototype.EA = p;
Zr.prototype.qe = function() {
  return new Tr
};
Zr.prototype.Ug = s();
Zr.prototype.bA = function() {
  var a = new P, b, c;
  this.EA = this.Ac.Ka();
  for(var d = 0;d < this.ba.length;d++) {
    c = this.ba[d], c.Mb().isEnabled() && !c.Gb && (a = c.Mb().n(c, c.Mb().ka().wa, c.fb()), c.Jg(c.Mb().ka().wa, p, a), b = c.Mb().ka().wa.ga(), c = (c.oe + c.xe) / 2 * this.Ac.Ka(), b + a.width + c / 2 + this.Ac.Ka() / 2 > this.Ac.Ka() && (this.EA = Math.max(this.EA, b + a.width + c / 2 + this.Ac.Ka() / 2)))
  }
  this.Ag = Math.ceil(this.Ac.Ka() - (this.EA - this.Ac.Ka()))
};
function Xr() {
  co.call(this)
}
A.e(Xr, Zr);
Xr.prototype.qe = function() {
  return new Ur
};
Xr.prototype.Ug = s();
Xr.prototype.zn = s();
Xr.prototype.kB = x(q);
function pf() {
  Ar.call(this);
  this.ck = 19
}
A.e(pf, Ar);
z = pf.prototype;
z.br = function() {
  return new Cr
};
z.Yw = function(a) {
  return!C(a, "data") ? j : !C(F(a, "data"), "series")
};
z.kx = function(a) {
  switch(a) {
    case 19:
      return new Wr
  }
  return p
};
z.Jo = function(a) {
  switch(Vb(a)) {
    case "funnel":
      return 19;
    case "pyramid":
      return 22;
    case "cone":
      return 21;
    default:
      return this.ck
  }
};
z.wx = s();
z.zk = s();
z.sy = s();
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
var as = 0, bs = 1;
function cs(a, b, c, d, f) {
  a != p && a != h && (this.index = a);
  b && (this.data = b);
  c && (this.tf = c);
  d && (this.fv = d);
  f && (this.Fj = f)
}
z = cs.prototype;
z.data = p;
z.tf = p;
z.fv = p;
z.index = p;
z.Fj = p;
z.copy = function(a) {
  if(a == p || a == h) {
    a = new cs
  }
  a.index = this.index;
  a.data = this.data;
  a.tf = this.tf;
  a.fv = this.fv;
  a.Fj = this.Fj;
  return a
};
function ds() {
  this.Ge()
}
A.e(ds, Wn);
z = ds.prototype;
z.jc = p;
z.getParent = u("jc");
z.Sl = t("jc");
z.bb = p;
z.ta = u("bb");
z.Kn = t("bb");
z.us = p;
z.EK = t("us");
z.Un = p;
z.KF = t("Un");
z.Xm = p;
z.zF = t("Xm");
z.Ka = function() {
  return this.k.width
};
z.Cj = function(a) {
  this.k.width = a
};
z.vb = function() {
  return this.k.height
};
z.Ql = function(a) {
  this.k.height = a
};
z.Sd = function() {
  return this.k.x
};
z.Xd = function(a) {
  this.k.x = a
};
z.Xb = function() {
  return this.k.y
};
z.gd = function(a) {
  this.k.y = a
};
z.dm = p;
z.Bj = function(a) {
  this.dm = a;
  this.D && this.D.Bj(a);
  this.vc && this.vc.Gi(a);
  this.sd && this.sd.Gi(a);
  this.uf && this.uf.Gi(a)
};
z.Se = function() {
  ds.f.Se.call(this);
  this.b.add("%Value", this.bb);
  this.b.add("%YValue", this.bb)
};
z.Pa = function(a) {
  return"%Value" == a || "%YValue" == a ? 2 : ds.f.Pa.call(this, a)
};
function es() {
  this.Ge()
}
A.e(es, ds);
es.prototype.g = function(a, b) {
  es.f.g.call(this, a, b);
  this.bb = C(a, "y") ? M(a, "y") : C(a, "value") ? M(a, "value") : 0;
  if(0 > this.bb || isNaN(this.bb)) {
    this.bb = 0
  }
};
es.prototype.ek = function(a) {
  this.o = this.jc;
  this.na = a.index;
  this.Sa = this.na.toString();
  Nk(this, a.data);
  this.cb = this.jc.Ab();
  Ok(this, this.jc.Qe);
  Jk(this, a.data);
  Pk(this, this.jc);
  this.il(a.data);
  this.rf(this.jc.ka());
  this.Eq(a.data, a.tf);
  this.g(a.data);
  this.jc.Mh(this);
  this.Cm(a.data, a.tf);
  this.Xa = C(a.data, "color") ? Yb(a.data) : this.jc.Ba();
  this.Ze = C(a.data, "hatch_type") ? M(a.data, "hatch_type") : this.jc.Ze;
  this.ge = this.jc.ng();
  this.ib = this.jc.Kg();
  C(a.data, "threshold") && (this.ib = this.ca().Li.Kg(N(a.data, "threshold"), a.fv));
  this.jc.Ed(this);
  this.ib && this.ib.ht(this);
  0 <= this.bb && this.ca().Ua.push(this)
};
es.prototype.createElement = function(a) {
  return a.ja()
};
es.prototype.nl = function() {
  return gd(this.k)
};
function fs() {
  this.Ge()
}
A.e(fs, ds);
z = fs.prototype;
z.ek = function() {
  this.o = this.jc;
  this.na = this.jc.index;
  this.Sa = this.jc.getName();
  this.Xa = this.jc.Ba();
  this.Ze = this.jc.Ze;
  this.ge = this.jc.ng();
  this.ib = this.jc.Kg();
  this.rf(this.jc.ka());
  this.cb = this.jc.Ab();
  Pk(this, this.jc);
  Ok(this, this.jc.Qe);
  this.jc.Ed(this)
};
z.p = function(a) {
  fs.f.p.call(this, a);
  if(this.K.wa.re() || this.K.kc.re() || this.K.dc.re() || this.K.Yb.re() || this.K.gc.re() || this.K.nc.re()) {
    this.Cc = (this.K.wa.re() ? this.K.wa.md() : this.K.kc.re() ? this.K.kc.md() : this.K.dc.re() ? this.K.dc.md() : this.K.Yb.re() ? this.K.Yb.md() : this.K.gc.re() ? this.K.gc.md() : this.K.nc.re() ? this.K.nc.md() : p).p(this.r()), this.D.ia(this.Cc)
  }
  this.iu = gs(this).ic(a, this.Xa, this.Ze);
  this.D.ia(this.iu);
  return this.D
};
z.update = function() {
  fs.f.update.call(this);
  hs(this);
  is(this)
};
z.Wa = function() {
  fs.f.Wa.call(this);
  hs(this);
  is(this)
};
function gs(a) {
  if(js(a.K.wa) || js(a.K.kc) || js(a.K.dc) || js(a.K.Yb) || js(a.K.gc) || js(a.K.nc)) {
    var b = js(a.K.wa) ? a.K.wa.getHeader() : js(a.K.kc) ? a.K.kc.getHeader() : js(a.K.dc) ? a.K.dc.getHeader() : js(a.K.Yb) ? a.K.Yb.getHeader() : js(a.K.gc) ? a.K.gc.getHeader() : js(a.K.nc) ? a.K.nc.getHeader() : p;
    b.uc(a.r(), b.Bh().ta(a));
    a.MI = b.n();
    return b
  }
}
function hs(a) {
  if(a.iu) {
    if(js(a.ya)) {
      var b = a.ya.getHeader();
      b.uc(a.r(), b.Bh().ta(a));
      b.update(a.iu, a.k);
      if((b = b.md()) && b.isEnabled()) {
        var c = new P(0, 0, a.k.width, Math.min(ks(a), a.k.height));
        b.qa(a.iu.Mx(), c)
      }
    }else {
      a.iu.Bj(q)
    }
  }
}
function ks(a) {
  if(!js(a.ya)) {
    return 0
  }
  var b = a.ya.getHeader();
  if(b.vb()) {
    return b.vb()
  }
  gs(a);
  return a.MI ? a.MI.height : 0
}
function is(a) {
  a.Cc && a.ya.re() && a.ya.md().qa(a.Cc, a.k, a.Xa)
}
z.Il = function(a) {
  fs.f.Il.call(this, a);
  if(this.Ab().AC) {
    var a = this.ca(), b = this.jc;
    b == a.ak ? b != a.Rz && ls(a, b.getParent()) : ls(a, b);
    a.Oz()
  }
};
function ms() {
  co.call(this);
  this.k = new P;
  this.Gq = j
}
A.e(ms, co);
z = ms.prototype;
z.jc = p;
z.getParent = u("jc");
z.Sl = t("jc");
z.bb = NaN;
z.ta = u("bb");
z.Kn = t("bb");
z.us = p;
z.EK = t("us");
z.Un = p;
z.KF = t("Un");
z.Xm = p;
z.zF = t("Xm");
z.k = p;
z.ls = p;
z.ks = p;
z.Ka = function() {
  return this.k.width
};
z.Cj = function(a) {
  this.k.width = a
};
z.vb = function() {
  return this.k.height
};
z.Ql = function(a) {
  this.k.height = a
};
z.Sd = function() {
  return this.k.x
};
z.Xd = function(a) {
  this.k.x = a
};
z.Xb = function() {
  return this.k.y
};
z.gd = function(a) {
  this.k.y = a
};
z.dm = p;
z.Bj = function(a) {
  var b = this.Fe();
  this.Lh && this.Lh.Bj(a);
  for(var c = 0;c < b;c++) {
    this.Ga(c).Bj(a)
  }
};
z.Lh = p;
function ns(a) {
  a.Lh && a.Lh.Kn(a.bb)
}
z.Gq = p;
z.Yr = t("ba");
z.qi = p;
z.yi = p;
z.ek = function(a) {
  this.na = a.index;
  this.Sa = this.na.toString();
  var b = a.data;
  Nk(this, b);
  this.Xa = C(b, "color") ? Yb(b) : zb("0xCCCCCC", h);
  this.Ze = C(b, "hatch_type") ? Gc(Wb(b, "hatch_type")) : 0;
  this.ge = 0;
  this.jc && (this.cb = this.jc.Ab(), this.Qe = this.jc.Qe, Jk(this, b), Pk(this, this.cb), this.il(b), this.K = this.jc.ka(), this.Eq(b, a.tf));
  C(b, "threshold") && (this.ib = this.ca().Li.Kg(N(b, "threshold"), a.fv));
  this.ib ? a.Fj = this.ib : this.ib = a.Fj;
  this.Cm(b, a.tf);
  this.g(b, a.tf);
  this.qi = this.K.wa.qi;
  this.yi = this.K.wa.yi;
  var c = this.ca();
  c.o && this && c.o.push(this);
  this.Gq && (this.Lh = new fs, this.Lh.Sl(this), b = Zb(b), a.data = {"#name#":"point"}, this.Lh.ek(a), a.data = b, this.ca().Ua.push(this.Lh))
};
z.Ug = function() {
  this.Nw()
};
z.zn = function() {
  this.Nw()
};
z.Nw = function() {
  if(0 != this.bb) {
    var a = this.k.width, b = this.k.height, c = this.k.x, d = this.k.y;
    if(this.Gq) {
      this.yi && (a -= this.yi.tb() + this.yi.Ja(), b -= this.yi.ra() + this.yi.rb(), c += this.yi.tb(), d += this.yi.rb());
      this.Lh.n().height = b;
      this.Lh.n().width = a;
      this.Lh.n().x = c;
      this.Lh.n().y = d;
      var f = ks(this.Lh), b = b - f, d = d + f;
      this.qi && (a -= this.qi.tb() + this.qi.tb(), b -= this.qi.ra() + this.qi.rb(), c += this.qi.tb(), d += this.qi.rb())
    }
    var g, f = this.ba.length, k = q;
    if(1 >= a) {
      k = j;
      for(g = 0;g < f;g++) {
        this.ba[g].n().width = 0
      }
    }
    if(1 >= b) {
      k = j;
      for(g = 0;g < f;g++) {
        this.ba[g].n().height = 0
      }
    }
    if(!k) {
      this.ls = a;
      this.ks = b;
      var k = 0, l, k = a * b / this.bb;
      for(g = 0;g < f;g++) {
        this.ba[g].EK(this.ba[g].ta() * k)
      }
      var n = k = 0, m = this.ls > this.ks;
      g = Number.MAX_VALUE;
      for(var o, v = 0, w = 0;n < f;) {
        l = n;
        o = m;
        for(var y = 0, E = 0, V = E = h, R = h, T = h, T = k;T <= l;T++) {
          y += this.ba[T].us
        }
        o ? (V = this.ks / y, E = y / this.ks) : (E = this.ls / y, V = y / this.ls);
        for(T = k;T <= l;T++) {
          R = this.ba[T], o ? (R.zF(V * R.us), R.KF(E)) : (R.KF(E * R.us), R.zF(V))
        }
        R = this.ba[l];
        o = E = Math.max(R.Xm / R.Un, R.Un / R.Xm);
        if(o > g) {
          y = o = 0;
          for(g = k;g < n;g++) {
            l = this.ba[g], l.Xd(c + v + o), l.gd(d + w + y), m ? y += l.vb() : o += l.Ka()
          }
          m ? v += this.ba[k].Ka() : w += this.ba[k].vb();
          this.ls = a - v;
          this.ks = b - w;
          m = this.ls > this.ks;
          n = k = n;
          g = Number.MAX_VALUE
        }else {
          for(g = k;g <= n;g++) {
            l = this.ba[g], l.Cj(1 > l.Un || isNaN(l.Un) ? 1 : l.Un), l.Ql(1 > l.Xm || isNaN(l.Xm) ? 1 : l.Xm)
          }
          g = o;
          n++
        }
      }
      b = a = 0;
      for(g = k;g < n;g++) {
        l = this.ba[g], l.Xd(c + v + a), l.gd(d + w + b), m ? b += l.vb() : a += l.Ka()
      }
      for(g = 0;g < f;g++) {
        l = this.ba[g], l.Nw && l.Nw()
      }
    }
  }
};
function os() {
  this.cb = this;
  this.Bw = j;
  this.Bx = ps;
  this.Yo = this.AC = j
}
A.e(os, eo);
z = os.prototype;
z.Bx = p;
z.AC = p;
z.Kv = j;
z.$s = x(j);
z.GC = function(a, b) {
  b ? a.sort(function(a, b) {
    return b.ta() - a.ta()
  }) : a.sort(function(a, b) {
    return a.ta() - b.ta()
  })
};
z.Ah = x("tree_map");
z.Rb = x("tree_map_style");
z.Qb = function() {
  switch(this.Bx) {
    case qs:
      return new rs;
    case ss:
      return new ts;
    default:
    ;
    case ps:
      return new us
  }
};
z.Aq = function() {
  return new vs
};
z.g = function(a, b) {
  os.f.g.call(this, a, b);
  C(a, "enable_drilldown") && (this.AC = F(a, "enable_drilldown"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    if(C(c, "drawing_mode")) {
      a: {
        switch(Wb(c, "drawing_mode")) {
          case "displayall":
            c = qs;
            break a;
          case "hide":
            c = ss;
            break a;
          default:
          ;
          case "crop":
            c = ps
        }
      }
      this.Bx = c
    }
  }
};
function ws() {
  this.yf = []
}
ws.prototype.DH = p;
ws.prototype.getData = u("DH");
ws.prototype.setData = t("DH");
ws.prototype.yf = p;
function xs() {
}
z = xs.prototype;
z.VD = p;
z.ov = p;
z.fF = p;
z.fr = p;
z.Sz = p;
z.Wu = p;
z.AA = p;
z.g = function(a) {
  var b;
  a: {
    switch(Wb(a, "input_mode")) {
      case "byparent":
        b = 1;
        break a;
      case "bylevel":
        b = 2;
        break a;
      default:
      ;
      case "bytree":
        b = 0
    }
  }
  this.VD = b;
  switch(this.VD) {
    case 2:
      this.fr = 0;
      this.ov = I(a, "point");
      this.fF = this.ov.length;
      break;
    case 1:
      var c = I(a, "point");
      b = c.length;
      this.Wu = {};
      this.AA = [];
      var d, f;
      for(f = 0;f < b;f++) {
        var g = c[f];
        d = new ws;
        d.setData(g);
        this.AA.push(d);
        C(g, "id") && (this.Wu[J(g, "id")] = d)
      }
      this.Sz = new ws;
      this.Sz.setData(a);
      for(f = 0;f < b;f++) {
        d = this.AA[f];
        a = d.getData();
        if(C(a, "parent") && (a = J(a, "parent"), this.Wu[a])) {
          this.Wu[a].yf.push(d);
          continue
        }
        this.Sz.yf.push(d)
      }
      b = this.AA;
      if(b != p) {
        d = b.length;
        for(f = 0;f < d;f++) {
          b[f] = p, delete b[f]
        }
      }
      b = this.Wu;
      if(b != p) {
        for(var k in b) {
          b[k] = p, delete b[k]
        }
      }
  }
};
function ys(a, b, c, d, f, g) {
  if(!b) {
    return p
  }
  if(g == p || g == h) {
    g = j
  }
  var k = b.yf, l = k.length;
  c.data = b.getData();
  if(0 < l || f) {
    b = new ms;
    b.Sl(d);
    var n = b;
    n.Gq = g;
    f ? (n.cb = f, Ok(n, f.Qe), Pk(n, f), n.rf(f.ka()), f.Mh(n)) : d && d.Mh(n);
    d = c.Fj;
    n.ek(c);
    f = [];
    n.Yr(f);
    n.Kn(0);
    for(g = 0;g < l;g++) {
      var m = k[g];
      c.index = g;
      c.data = m.getData();
      m = ys(a, m, c, n);
      if(0 != m.ta()) {
        var o = m.ta();
        n.bb += o;
        f.push(m)
      }
    }
    c.Fj = d;
    Mk(n.Ab(), n.ba);
    ns(n)
  }else {
    b = new es, b.Sl(d), b.ek(c)
  }
  return b
}
function zs(a, b) {
  return a.fr < a.fF ? As(a.ov[a.fr]) <= b ? p : a.ov[a.fr++] : p
}
function As(a) {
  return!C(a, "level") ? 0 : M(a, "level")
}
function Bs(a, b, c) {
  var d, f = As(b.data);
  if(a.fr >= a.fF ? 0 : As(a.ov[a.fr]) > f) {
    d = new ms;
    d.Sl(c);
    var g = d;
    g.cb = c.Ab();
    Ok(g, c.Ab().Qe);
    Pk(g, c.Ab());
    g.rf(c.Ab().ka());
    c.Ab().Mh(g);
    g.ek(b);
    c = [];
    g.Yr(c);
    g.Kn(0);
    var k = 0;
    do {
      b.index = k++;
      b.data = zs(a, f);
      if(!b.data) {
        break
      }
      var l = Bs(a, b, g), n = l.ta();
      g.bb += n;
      c.push(l)
    }while(1);
    Mk(g.Ab(), g.ba);
    ns(g)
  }else {
    d = new es, d.Sl(c), d.ek(b)
  }
  return d
}
function Cs(a, b, c, d, f) {
  if(!b) {
    return p
  }
  if(f == p || f == h) {
    f = j
  }
  var g, k = I(b.data, "point"), l = k.length;
  if(0 < l || d) {
    g = new ms;
    g.Sl(c);
    var n = g;
    n.Gq = f;
    d ? (n.cb = d, Ok(n, d.Qe), Pk(n, d), n.rf(d.ka()), d.Mh(n)) : c && c.Mh(n);
    c = b.Fj;
    n.ek(b);
    d = [];
    n.Yr(d);
    n.Kn(0);
    for(f = 0;f < l;f++) {
      b.index = f;
      b.data = k[f];
      var m = Cs(a, b, n), o = m.ta();
      n.bb += o;
      d.push(m)
    }
    b.Fj = c;
    Mk(n.Ab(), n.ba);
    ns(n)
  }else {
    g = new es, g.Sl(c), g.ek(b)
  }
  return g
}
;function Ds() {
}
A.e(Ds, mg);
Ds.prototype.Yc = function(a, b) {
  Ds.f.Yc.call(this, a, b);
  this.kf && rd(this.kf, a.n())
};
Ds.prototype.update = function(a) {
  Ds.f.update.call(this, a);
  this.ns(a)
};
Ds.prototype.ns = function() {
  this.kf && sd(this.kf, new P(this.j.n().x - this.kf.getParent().Sd(), this.j.n().y - this.kf.getParent().Xb(), this.j.n().width, this.j.n().height))
};
function Es() {
}
A.e(Es, mg);
Es.prototype.update = function(a) {
  Es.f.update.call(this, a);
  this.ns(a)
};
Es.prototype.ns = function(a) {
  var b = new P(this.j.n().x - this.kf.getParent().Sd(), this.j.n().y - this.kf.getParent().Xb(), this.j.n().width, this.j.n().height), a = this.j.Mb().n(this.j, a, this.kf);
  Xc(b, a) ? this.kf.Bj(j) : this.kf.Bj(q)
};
function Fs() {
  $.call(this)
}
A.e(Fs, ng);
Fs.prototype.zA = p;
Fs.prototype.p = function(a, b) {
  switch(a.Ab().Bx) {
    case qs:
      this.zA = mg;
      break;
    case ss:
      this.zA = Es;
      break;
    default:
    ;
    case ps:
      this.zA = Ds
  }
  return Fs.f.p.call(this, a, b)
};
Fs.prototype.lc = function() {
  return new this.zA
};
var qs = 0, ps = 1, ss = 2;
function Gs() {
  ye.apply(this)
}
A.e(Gs, Ce);
Gs.prototype.ob = p;
Gs.prototype.vb = u("ob");
Gs.prototype.g = function(a) {
  Gs.f.g.call(this, a);
  C(a, "height") && (this.ob = M(a, "height"))
};
Gs.prototype.update = function(a, b, c, d) {
  Gs.f.update.call(this, a.r(), a, c, d);
  var f;
  switch(this.za) {
    case 0:
      f = b.x;
      break;
    case 1:
      f = b.x + (b.width - this.k.width) / 2;
      break;
    case 2:
      f = b.x + (b.width - this.k.width)
  }
  a.qb(f, b.y)
};
function Hs() {
  ye.apply(this)
}
A.e(Hs, Gs);
Hs.prototype.ic = function(a, b, c) {
  a = Hs.f.ic.call(this, a, b, c);
  rd(a, new P);
  return a
};
Hs.prototype.update = function(a, b, c, d) {
  Hs.f.update.call(this, a, b, c, d);
  sd(a, new P(0, 0, b.width, b.height))
};
function Is() {
  ye.apply(this)
}
A.e(Is, Gs);
Is.prototype.update = function(a, b, c, d) {
  Is.f.update.call(this, a, b, c, d);
  b.width >= this.k.width && b.height >= this.k.height ? a.eu().setAttribute("visibility", "visible") : a.eu().setAttribute("visibility", "hidden")
};
function Js(a) {
  W.call(this, a)
}
A.e(Js, Cf);
Js.prototype.ns = function(a) {
  this.$a.ns(a)
};
function vs() {
}
A.e(vs, Rn);
vs.prototype.Qb = function() {
  return new Fs
};
vs.prototype.Wa = function(a, b, c) {
  vs.f.Wa.call(this, a, b, c);
  c.ns(b)
};
vs.prototype.xH = function(a) {
  return new Js(a)
};
function Ks(a, b) {
  Re.call(this, a, b)
}
A.e(Ks, Pf);
z = Ks.prototype;
z.Y = p;
z.md = u("Y");
z.re = function() {
  return this.Y && this.Y.isEnabled()
};
z.qi = p;
z.yi = p;
z.ju = p;
z.getHeader = u("ju");
z.g = function(a) {
  a = Ks.f.g.call(this, a);
  if(!C(a, "branch")) {
    return a
  }
  var b = F(a, "branch");
  Ub(b, "background") && (this.Y = new Wd, this.Y.g(F(b, "background")));
  Ub(b, "inside_margin") && (this.qi = new Ad, this.qi.g(F(b, "inside_margin")));
  Ub(b, "outside_margin") && (this.yi = new Ad, this.yi.g(F(b, "outside_margin")));
  Ub(b, "header") && (this.ju = this.WB(), this.ju.g(F(b, "header")));
  return a
};
z.WB = function() {
  return new Gs
};
function js(a) {
  return a.ju && a.ju.isEnabled()
}
function Ls(a, b) {
  Re.call(this, a, b)
}
A.e(Ls, Ks);
Ls.prototype.WB = function() {
  return new Hs
};
function Ms(a, b) {
  Re.call(this, a, b)
}
A.e(Ms, Ks);
Ms.prototype.WB = function() {
  return new Is
};
function Ns() {
}
A.e(Ns, Of);
Ns.prototype.createElement = function() {
  return this.j.r().ja()
};
Ns.prototype.Ta = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function rs() {
  $.call(this)
}
A.e(rs, Qf);
rs.prototype.cc = function() {
  return Ks
};
rs.prototype.lc = function() {
  return new Ns
};
function us() {
  $.call(this)
}
A.e(us, rs);
us.prototype.cc = function() {
  return Ls
};
function ts() {
  $.call(this)
}
A.e(ts, rs);
ts.prototype.cc = function() {
  return Ms
};
var Os = {chart:{styles:{tree_map_style:{branch:{background:{border:{enabled:"true"}, fill:{enabled:"true"}, enabled:"false"}, inside_margin:{all:"0", top:"-1"}, outside_margin:{all:"0"}, header:{text:{value:"{%Name}"}, background:{inside_margin:{all:"0", left:"6", top:"2"}, enabled:"true"}, enabled:"true", align:"Near"}}, states:{normal:{branch:{header:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, 
{color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}}, enabled:"true", align:"Near"}}, fill:{enabled:"true", type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}}, hover:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, 
fill:{enabled:"true", type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"White", type:"Percent50", opacity:"0.6"}}, pushed:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, fill:{enabled:"true", type:"Solid", 
color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.6"}}, selected_normal:{branch:{header:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}}, enabled:"true", align:"Near"}}, fill:{enabled:"true", 
type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.7"}}, selected_hover:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, fill:{enabled:"true", type:"Solid", color:"%Color", 
opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.5"}, color:"%Color"}}, name:"anychart_default"}}, data_plot_settings:{tree_map:{label_settings:{position:{anchor:"Center", valign:"Center", halign:"Center"}, font:{bold:"false"}, format:{value:"{%Name}"}, enabled:"false"}, tooltip_settings:{format:{value:"{%Name} - {%YValue}{numDecimals:0}"}, enabled:"false"}, interactivity:{use_hand_cursor:"false", 
allow_select:"true"}, marker_settings:{}, enable_drilldown:"true"}}}};
function Ps() {
}
A.e(Ps, bl);
Ps.prototype.zh = function() {
  return this.$f(Ps.f.zh.call(this), Os)
};
function nf() {
  Ek.call(this)
}
A.e(nf, Ek);
z = nf.prototype;
z.Rz = p;
z.ak = p;
z.br = function() {
  return new Ps
};
z.Ab = function() {
  return Ik(this, 23)
};
z.kx = function() {
  return new os
};
z.vx = function(a, b) {
  var c = F(a, "data"), d = new Tj;
  C(a, "palettes") && d.g(F(a, "palettes"));
  this.Li = new Ck(F(a, "thresholds"));
  var f = this.Nx(F(a, "data_plot_settings"), c, 23, b), d = new cs(0, c, b, d, p), g = new xs;
  g.g(c);
  var k;
  a: {
    switch(g.VD) {
      case 2:
        if(0 < I(d.data, "point").length && f) {
          c = new ms;
          c.Gq = q;
          c.Sl(p);
          c.cb = f;
          Ok(c, f.Qe);
          Pk(c, f);
          c.rf(f.ka());
          f.Mh(c);
          f = d.Fj;
          c.ek(d);
          c.Yr([]);
          c.Kn(0);
          k = 0;
          do {
            d.index = k++;
            d.data = zs(g, -Number.MAX_VALUE);
            if(!d.data) {
              break
            }
            var l = Bs(g, d, c), n = l.ta();
            c.bb += n;
            c.ba.push(l)
          }while(1);
          d.Fj = f;
          Mk(c.Ab(), c.ba);
          ns(c)
        }else {
          c = p
        }
        k = c;
        break a;
      case 1:
        k = ys(g, g.Sz, d, p, f, q);
        break a;
      case 0:
        k = Cs(g, d, p, f, q)
    }
  }
  this.Rz = k;
  ls(this, this.Rz);
  Dk(this.Li)
};
z.zk = s();
function ls(a, b) {
  a.ak && a.ak.Bj(q);
  a.ak = b;
  a.rp[0] = a.ak;
  a.ak.Bj(j)
}
z.p = function(a, b, c) {
  nf.f.p.call(this, a, b, c);
  Qs(this);
  sd(this.D, this.Ig());
  return this.D
};
z.sy = s();
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
z.Oz = function() {
  Qs(this);
  nf.f.Oz.call(this);
  sd(this.D, this.Ig())
};
function Qs(a) {
  a.Rz && (a.ak.Xd(0), a.ak.gd(0), a.ak.Cj(1 > a.k.width ? 1 : a.k.width - 1), a.ak.Ql(1 > a.k.height ? 1 : a.k.height - 1))
}
z.Yw = function(a) {
  return!a || !C(a, "data") || !C(F(a, "data"), "point")
};
function Rs(a) {
  switch(a) {
    default:
    ;
    case "near":
      return 0;
    case "center":
      return 1;
    case "far":
      return 2;
    case "spread":
      return 3
  }
}
function Ss(a) {
  switch(a) {
    default:
    ;
    case "right":
      return 1;
    case "left":
      return 0;
    case "top":
      return 2;
    case "bottom":
      return 3;
    case "float":
      return 4;
    case "fixed":
      return 5
  }
}
function Ts() {
  this.Oa = new Ad(0)
}
z = Ts.prototype;
z.Oa = p;
z.Aa = 1;
z.Ib = u("Aa");
z.za = 0;
z.ou = q;
z.NG = 0;
z.lg = u("NG");
z.ab = p;
z.Cj = t("ab");
z.zl = q;
z.hj = q;
z.ob = p;
z.Ql = t("ob");
z.yl = q;
z.si = q;
z.oi = 0;
z.ur = q;
z.dF = p;
z.Oi = 0;
z.vr = q;
z.eF = p;
z.Ir = p;
z.Fr = p;
z.Lo = -1;
z.Go = u("Lo");
z.Xp = -1;
z.Ko = u("Xp");
z.OC = NaN;
z.PC = NaN;
z.Tc = j;
z.YD = q;
z.g = function(a) {
  C(a, "position") && (this.Aa = Ss(Wb(a, "position")));
  C(a, "align") && (this.za = Rs(Wb(a, "align")));
  C(a, "inside_dataplot") && (this.ou = K(a, "inside_dataplot"));
  C(a, "anchor") && (this.NG = zd(Wb(a, "anchor")));
  var b;
  C(a, "width") && (b = F(a, "width"), this.ab = (this.zl = Mb(b)) ? Nb(b) : Ob(b), this.hj = j);
  C(a, "height") && (b = F(a, "height"), this.ob = (this.yl = Mb(b)) ? Nb(b) : Ob(b), this.si = j);
  C(a, "horizontal_padding") && (b = F(a, "horizontal_padding"), this.oi = (this.ur = Mb(b)) ? Nb(b) : Ob(b));
  C(a, "vertical_padding") && (b = F(a, "vertical_padding"), this.Oi = (this.vr = Mb(b)) ? Nb(b) : Ob(b));
  C(a, "halign") && (this.Lo = vd(Wb(a, "halign")));
  C(a, "valign") && (this.Xp = yd(Wb(a, "valign")));
  if(this.ou) {
    this.YD = this.Tc = j
  }else {
    if(C(a, "align_by")) {
      this.Tc = "chart" != Wb(a, "align_by"), this.YD = j
    }else {
      if(4 == this.Aa || 5 == this.Aa) {
        this.Tc = q
      }
    }
  }
  if(4 != this.Aa && 5 != this.Aa) {
    C(a, "padding") || H(a, "padding", 10);
    b = M(a, "padding");
    if(this.ou) {
      switch(this.Aa) {
        case 0:
          this.Oa.bh(b);
          break;
        case 1:
          this.Oa.eh(b);
          break;
        case 2:
          this.Oa.fh(b);
          break;
        case 3:
          this.margin.$g(b)
      }
    }else {
      switch(this.Aa) {
        case 0:
          this.Oa.eh(b);
          break;
        case 1:
          this.Oa.bh(b);
          break;
        case 2:
          this.Oa.$g(b);
          break;
        case 3:
          this.Oa.fh(b)
      }
    }
    C(a, "margin") && this.Oa.g(C(a, "margin"))
  }
};
z.Rc = function(a, b, c) {
  a.width = p;
  a.height = p;
  c = this.Tc ? c : b;
  if(4 != this.Aa && 5 != this.Aa && !this.ou) {
    switch(this.Aa) {
      case 0:
      ;
      case 1:
        this.ab && (a.width = this.zl ? b.width * this.ab : this.ab);
        this.ob && (a.height = this.yl ? c.height * this.ob : this.ob);
        break;
      case 2:
      ;
      case 3:
        if(this.ab && (a.width = this.zl ? c.width * this.ab : this.ab), this.ob) {
          a.height = this.yl ? b.height * this.ob : this.ob
        }
    }
  }else {
    if(this.ab && (a.width = this.zl ? c.width * this.ab : this.ab), this.ob) {
      a.height = this.yl ? c.height * this.ob : this.ob
    }
  }
  this.dF = this.ur ? c.width * this.oi : this.oi;
  this.eF = this.vr ? c.height * this.Oi : this.Oi
};
function Us() {
  this.Za = new Ts;
  this.k = new P;
  this.xb = new P;
  this.FC = new pa
}
z = Us.prototype;
z.k = p;
z.n = u("k");
z.xb = p;
z.Y = p;
z.Cc = p;
z.Za = p;
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.aa.r()
};
z.N = p;
z.aa = p;
z.nv = p;
z.p = function(a) {
  this.D = new W(a);
  this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(a), this.D.ia(this.Cc));
  A.I.nb(this.D.G, ka.fm, this.GN, q, this);
  return this.D
};
z.KD = function(a, b, c) {
  this.N = a;
  this.aa = b;
  this.nv = c;
  this.Al = new td(this.aa.r())
};
z.g = function(a) {
  this.Za.g(a);
  C(a, "background") && (a = F(a, "background"), Tb(a) && (this.Y = new Wd, this.Y.g(a)))
};
z.Rc = function(a, b) {
  this.Za.Rc(this.k, a, b);
  this.cl();
  this.In()
};
function Vs(a) {
  var b = a.k.width;
  Ws(a) && (b -= Xs(a));
  return b
}
z.Do = function() {
  var a = this.k.height;
  Ws(this) && (a -= Ys(this));
  return a
};
z.Xx = function() {
  var a = this.Za.Fr;
  Ws(this) && (a -= Ys(this));
  return a
};
z.cl = function() {
  this.k.width += Zs(this);
  this.k.height += $s(this);
  if(Ws(this) && (this.Za.hj || (this.k.width += Xs(this)), !this.Za.si)) {
    this.k.height += Ys(this)
  }
};
z.In = function() {
  this.xb.x = 0;
  this.xb.y = 0;
  this.xb.width = this.k.width - Zs(this);
  this.xb.height = this.k.height - $s(this);
  Ws(this) && Bd(this.Y.Oa, this.xb)
};
z.qa = function() {
  at(this);
  this.pC()
};
function at(a) {
  a.D.Xd(a.k.x + a.Za.Oa.tb());
  a.D.gd(a.k.y + a.Za.Oa.rb())
}
z.pC = function() {
  this.Cc && this.Y.qa(this.Cc, this.Ig())
};
z.FC = p;
z.addEventListener = function(a, b, c, d) {
  this.FC.addEventListener(ka.fm, b, c, d)
};
z.GN = function(a) {
  a.target = this;
  this.FC.dispatchEvent(a)
};
function Ws(a) {
  return a.Y && a.Y.Oa
}
function Xs(a) {
  return a.Y.Oa.tb() + a.Y.Oa.Ja()
}
function Ys(a) {
  return a.Y.Oa.rb() + a.Y.Oa.ra()
}
function Zs(a) {
  return a.Za.Oa.tb() + a.Za.Oa.Ja()
}
function $s(a) {
  return a.Za.Oa.rb() + a.Za.Oa.ra()
}
z.Ig = function() {
  return new P(0, 0, this.k.width - Zs(this), this.k.height - $s(this))
};
z.Wa = function() {
  at(this);
  this.Y && this.Y.Wa(this.Cc, this.Ig())
};
function bt() {
  Ce.apply(this)
}
A.e(bt, Ce);
z = bt.prototype;
z.yd = p;
z.nd = u("yd");
z.JJ = p;
z.Fn = p;
z.Tr = p;
z.g = function(a) {
  a && (bt.f.g.call(this, F(a, "title")), C(a, "title_separator") && (this.Fn = new Kd, this.Fn.g(F(a, "title_separator"))))
};
z.uc = function(a, b) {
  bt.f.uc.call(this, a, this.yc.ta(b));
  this.yd = this.k.height + this.Ia + 4;
  this.JJ = this.k.width;
  this.Fn && (this.yd += this.Fn.Va())
};
z.ic = function(a, b) {
  bt.f.ic.call(this, a);
  var c = new W(a), d = this.Ia, f;
  switch(this.za) {
    case 0:
      f = b.x;
      break;
    case 1:
      f = b.x + (b.width - this.k.width) / 2;
      break;
    case 2:
      f = b.Ja() - this.k.width
  }
  this.D.Xd(f);
  this.D.gd(d);
  c.ia(this.D);
  if(this.Fn) {
    d += this.k.height + this.Fn.Va() / 2 + 2;
    this.Tr = new P(b.x, d, b.width, this.Fn.Va() / 2);
    d = Ld(this.Fn, a, this.Tr);
    f = a.ja(j);
    var g = S(this.Tr.tb(), this.Tr.y), g = g + U(this.Tr.Ja(), this.Tr.y);
    f.setAttribute("d", g);
    f.setAttribute("style", d);
    c.appendChild(f)
  }
  return c
};
function ct() {
  Us.apply(this)
}
A.e(ct, Us);
z = ct.prototype;
z.Ya = p;
z.mh = p;
z.OE = p;
z.g = function(a, b) {
  ct.f.g.call(this, a, b);
  Ub(a, "title") && (this.Ya = new bt, this.Ya.g(a), this.Ya.uc(this.aa.r(), this.aa))
};
z.Xx = function() {
  var a = ct.f.Xx.call(this);
  this.Ya && (a -= this.Ya.nd());
  return a
};
z.Do = function() {
  var a = ct.f.Do.call(this);
  this.Ya && (a -= this.Ya.nd());
  return a
};
z.cl = function() {
  if(this.Ya && (this.Za.hj || (this.k.width = Math.max(this.k.width, this.Ya.JJ)), !this.Za.si)) {
    this.k.height += this.Ya.nd()
  }
  ct.f.cl.call(this)
};
z.In = function() {
  ct.f.In.call(this);
  this.OE = this.xb.Ca();
  if(this.Ya) {
    var a = this.Ya, b = this.xb;
    b.y += a.yd;
    b.height -= a.yd
  }
};
z.qa = function() {
  ct.f.qa.call(this);
  this.Ya && (this.mh = this.Ya.ic(this.D.r(), this.OE), this.D.ia(this.mh))
};
z.Wa = function() {
  ct.f.Wa.call(this);
  this.Ya && (this.D.removeChild(this.mh), this.mh = this.Ya.ic(this.D.r(), this.OE), this.D.ia(this.mh))
};
function dt() {
}
dt.prototype.ye = function() {
  A.xa()
};
dt.prototype.hi = function() {
  A.xa()
};
function et() {
  this.controls = []
}
A.e(et, dt);
et.prototype.controls = p;
et.prototype.ye = function(a) {
  this.controls.push(a)
};
et.prototype.hi = function(a, b) {
  for(var c = this.controls.length, d = 0;d < c;d++) {
    this.controls[d].Za.Ir = b.width, this.controls[d].Za.Fr = b.height, this.controls[d].Rc(b, a), ft(this.controls[d], this.controls[d].Za.Tc ? a : b)
  }
  return j
};
function ft(a, b) {
  var c = a.Za, d, f, g = new O(b.x, b.y);
  switch(c.lg()) {
    case 0:
      d = 2;
      f = 1;
      g.x += b.width / 2;
      g.y += b.height / 2;
      break;
    case 7:
      d = 2;
      f = 0;
      g.x += b.width / 2;
      g.y += b.height;
      break;
    case 1:
      f = d = 1;
      g.y += b.height / 2;
      break;
    case 5:
      d = 0;
      f = 1;
      g.x += b.width;
      g.y += b.height / 2;
      break;
    case 3:
      f = d = 2;
      g.x += b.width / 2;
      break;
    case 8:
      d = 1;
      f = 0;
      g.y += b.height;
      break;
    case 2:
      d = 1;
      f = 2;
      break;
    case 6:
      f = d = 0;
      g.x += b.width;
      g.y += b.height;
      break;
    case 4:
      d = 0, f = 2, g.x += b.width
  }
  d = -1 == c.Go() ? d : c.Go();
  f = -1 == c.Ko() ? f : c.Ko();
  var k = a.n().width, l = a.n().height;
  switch(d) {
    case 0:
      g.x -= k + c.dF;
      break;
    case 2:
      g.x -= k / 2;
      break;
    case 1:
      g.x += c.dF
  }
  switch(f) {
    case 0:
      g.y -= l + c.eF;
      break;
    case 1:
      g.y -= l / 2;
      break;
    case 2:
      g.y += c.eF
  }
  a.n().x = g.x;
  a.n().y = g.y
}
function gt() {
  et.apply(this)
}
A.e(gt, et);
function ht() {
  et.apply(this)
}
A.e(ht, et);
z = ht.prototype;
z.jJ = q;
z.N = p;
z.jg = p;
z.QE = p;
z.Jf = p;
z.ye = function(a) {
  this.N = a.nv;
  a.addEventListener(ka.fm, this.rN, q, this);
  ht.f.ye.call(this, a)
};
z.hi = function(a, b) {
  if(!this.N) {
    return j
  }
  if(this.jJ) {
    for(var c = this.N.n(), d = this.controls.length, f = 0;f < d;f++) {
      this.controls[f].Za.Ir = b.width, this.controls[f].Za.Fr = b.height, this.controls[f].Rc(b, a), isNaN(this.controls[f].Za.OC && !isNaN(this.controls[f].Za.PC)) ? ft(this.controls[f], this.controls[f].Za.Tc ? a : b) : (this.controls[f].n().x = this.controls[f].Za.OC * c.width, this.controls[f].n().y = this.controls[f].Za.PC * c.height)
    }
  }else {
    ht.f.hi.call(this, a, b), this.jJ = j
  }
  return j
};
z.rN = function(a) {
  this.jg = a.target;
  this.QE = a.clientX;
  this.Jf = a.clientY;
  A.I.nb(this.jg.r().Ic(), ka.Xn, this.NJ, q, this);
  A.I.nb(this.jg.r().Ic(), ka.Ns, this.JK, q, this)
};
z.NJ = function(a) {
  if(this.jg) {
    var b = this.N.n(), c = this.jg.n(), a = new O(a.clientX - this.QE + c.x, a.clientY - this.Jf + c.y);
    a.x < b.tb() ? a.x = b.tb() : a.x + c.width > b.Ja() && (a.x = b.Ja() - c.width);
    a.y < b.rb() ? a.y = b.rb() : a.y + c.height > b.ra() && (a.y = b.ra() - c.height);
    this.jg.fb().qb(a.x, a.y)
  }
};
z.JK = function(a) {
  A.I.ed(this.jg.r().Ic(), ka.Xn, this.NJ, q, this);
  A.I.ed(this.jg.r().Ic(), ka.Ns, this.JK, q, this);
  this.QE = a.clientX;
  this.Jf = a.clientY;
  var a = this.jg.fb().Sd(), b = this.jg.fb().Xb();
  this.jg.n().x = a;
  this.jg.n().y = b;
  this.jg.Za.OC = a / this.N.n().width;
  this.jg.Za.PC = b / this.N.n().height;
  this.jg = p
};
function it() {
  this.controls = [];
  this.Tc = j;
  this.ZD = q
}
it.prototype.controls = p;
it.prototype.Tc = p;
it.prototype.ZD = p;
it.prototype.ye = function(a) {
  a.Za.Tc || (this.Tc = q);
  this.controls.push(a)
};
function jt(a) {
  if(!a.ZD && !a.Tc) {
    a.ZD = j;
    for(var b = a.controls.length, c = 0;c < b;c++) {
      a.controls[c].Za.Tc = q
    }
  }
}
function kt(a) {
  this.Xf = a;
  this.sn = new it;
  this.Zk = new it;
  this.Lm = new it
}
z = kt.prototype;
z.sn = p;
z.Zk = p;
z.Lm = p;
z.Xf = p;
z.mJ = u("Xf");
z.Dd = p;
z.ye = function(a) {
  switch(a.Za.za) {
    case 0:
      this.sn.ye(a);
      break;
    case 1:
      this.Zk.ye(a);
      break;
    case 2:
      this.Lm.ye(a);
      break;
    case 3:
      this.dG(a.Za), this.Zk.ye(a)
  }
};
z.dG = function() {
  A.xa()
};
z.Lq = function() {
  A.xa()
};
z.Pt = function() {
  A.xa()
};
function lt(a, b, c) {
  jt(a.sn);
  jt(a.Zk);
  jt(a.Lm);
  var d = a.AF(b, c), f = a.wF(b, c), b = a.yF(b, c);
  a.Lq(a.sn.controls, d);
  a.Lq(a.Lm.controls, b);
  a.Pt(a.Zk.controls, f);
  return Math.max(d, f, b)
}
z.AF = x(0);
z.wF = x(0);
z.yF = x(0);
z.Hk = function() {
  A.xa()
};
function mt(a) {
  kt.call(this, a)
}
A.e(mt, kt);
z = mt.prototype;
z.Lq = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? d.n().y = this.Dd.ra() - b : d.n().y = this.Dd.rb() + b - d.n().height
  }
};
z.Pt = function(a, b) {
  for(var c = this.Xf ? this.Dd.ra() - b : this.Dd.rb() + b, d = 0;d < a.length;d++) {
    var f = a[d];
    this.Xf ? (f.n().y = c, c += f.n().height) : (f.n().y = c - f.n().height, c -= f.n().height)
  }
};
z.dG = function(a) {
  a.hj = j;
  a.zl = j;
  a.Cj(1)
};
z.AF = function(a, b) {
  for(var c = 0, d = (this.sn.Tc ? b : a).x, f = this.sn.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    k.n().x = d;
    this.Hk(k.Za, a, b);
    k.Rc(a, b);
    d += k.n().width;
    c = Math.max(c, k.n().height)
  }
  return c
};
z.wF = function(a, b) {
  for(var c = 0, d = this.Zk.Tc ? b : a, d = d.x + d.width / 2, f = this.Zk.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Hk(k.Za, a, b);
    k.Rc(a, b);
    k.n().x = d - k.n().width / 2;
    c += k.n().height
  }
  return c
};
z.yF = function(a, b) {
  for(var c = 0, d = (this.Lm.Tc ? b : a).Ja(), f = this.Lm.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Hk(k.Za, a, b);
    k.Rc(a, b);
    k.n().x = d - k.n().width;
    d -= k.n().width;
    c = Math.max(c, k.n().height)
  }
  return c
};
z.Hk = function(a, b, c) {
  a.Ir = a.Tc ? c.width : b.width;
  a.Fr = 0.5 * b.height
};
function nt(a) {
  kt.call(this, a)
}
A.e(nt, mt);
nt.prototype.Lq = function(a) {
  for(var b = 0;b < a.length;b++) {
    var c = a[b];
    this.Xf ? c.n().y = this.Dd.ra() - c.n().height : c.n().y = this.Dd.rb()
  }
};
nt.prototype.Pt = function(a) {
  for(var b = this.Xf ? this.Dd.ra() : this.Dd.rb(), c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? (d.n().y = b - d.n().height, b -= d.n().height) : (d.n().y = b, b += d.n().height)
  }
};
nt.prototype.Hk = function(a, b, c) {
  a.Ir = a.Tc ? c.width : b.width;
  a.Fr = b.height
};
function ot(a) {
  kt.call(this, a)
}
A.e(ot, kt);
z = ot.prototype;
z.Lq = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? d.n().x = this.Dd.Ja() - b : d.n().x = this.Dd.tb() + b - d.n().width
  }
};
z.Pt = function(a, b) {
  for(var c = this.Xf ? this.Dd.Ja() - b : this.Dd.tb() + b, d = 0;d < a.length;d++) {
    var f = a[d];
    this.Xf ? (f.n().x = c, c += f.n().width) : (f.n().x = c - f.n().width, c -= f.n().width)
  }
};
z.dG = function(a) {
  a.si = j;
  a.yl = j;
  a.Ql(1)
};
z.AF = function(a, b) {
  var c = 0, d = (this.sn.Tc ? b : a).rb(), f = this.sn.controls, g, k;
  for(g = 0;g < f.length;g++) {
    k = f[g], k.n().y = d, this.Hk(k.Za, a, b), k.Rc(a, b), d += k.n().height, c = Math.max(c, k.n().width)
  }
  return c
};
z.wF = function(a, b) {
  for(var c = 0, d = this.Zk.Tc ? b : a, d = d.y + d.height / 2, f = this.Zk.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Hk(k.Za, a, b);
    k.Rc(a, b);
    k.n().y = d - k.n().height / 2;
    c += k.n().width
  }
  return c
};
z.yF = function(a, b) {
  var c = 0, d = (this.Lm.Tc ? b : a).ra(), f = this.Lm.controls, g, k;
  for(g = 0;g < f.length;g++) {
    k = f[g], this.Hk(k.Za, a, b), k.Rc(a, b), k.n().y = d - k.n().height, d -= k.n().height, c = Math.max(c, k.n().width)
  }
  return c
};
z.Hk = function(a, b, c) {
  a.Fr = a.Tc ? c.height : b.height;
  a.Ir = 0.5 * b.width
};
function pt(a) {
  kt.call(this, a)
}
A.e(pt, ot);
pt.prototype.Lq = function(a) {
  for(var b = 0;b < a.length;b++) {
    var c = a[b];
    this.Xf ? c.n().x = this.Dd.Ja() - c.n().width : c.n().x = this.Dd.tb()
  }
};
pt.prototype.Pt = function(a) {
  for(var b = this.mJ ? this.Dd.Ja() : this.Dd.tb(), c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? (d.n().x = b - d.n().width, b -= d.n().width) : (d.n().x = b, b += d.n().width)
  }
};
pt.prototype.Hk = function(a, b, c) {
  a.maxHeight = a.Tc ? c.height : b.height;
  a.maxWidth = b.width
};
function qt() {
}
A.e(qt, dt);
z = qt.prototype;
z.ep = p;
z.Ep = p;
z.Wp = p;
z.jo = p;
z.ye = function(a) {
  switch(a.Za.Ib()) {
    case 0:
      this.ep.ye(a);
      break;
    case 1:
      this.Ep.ye(a);
      break;
    case 2:
      this.Wp.ye(a);
      break;
    case 3:
      this.jo.ye(a)
  }
};
function rt() {
  this.ep = new pt(q);
  this.Ep = new pt(j);
  this.Wp = new nt(q);
  this.jo = new nt(j)
}
A.e(rt, qt);
rt.prototype.hi = function(a, b) {
  this.ep.Dd = a;
  this.Ep.Dd = a;
  this.Wp.Dd = a;
  this.jo.Dd = a;
  lt(this.ep, b, a);
  lt(this.Ep, b, a);
  lt(this.Wp, b, a);
  lt(this.jo, b, a);
  return j
};
function st() {
  this.NL = this.PL = this.OL = j;
  this.ep = new ot(q);
  this.Ep = new ot(j);
  this.Wp = new mt(q);
  this.jo = new mt(j)
}
A.e(st, qt);
z = st.prototype;
z.EH = p;
z.Oa = p;
z.ye = function(a) {
  if(!a.Za.YD) {
    switch(a.Za.Ib()) {
      case 0:
        a.Za.Tc = this.EH;
        break;
      case 1:
        a.Za.Tc = this.OL;
        break;
      case 2:
        a.Za.Tc = this.PL;
        break;
      case 3:
        a.Za.Tc = this.NL
    }
  }
  st.f.ye.call(this, a)
};
z.hi = function(a, b) {
  var c = new Ad;
  this.Wr(c, a, b);
  var d = c.tb() == this.Oa.tb() && c.Ja() == this.Oa.Ja() && c.rb() == this.Oa.rb() && c.ra() == this.Oa.ra();
  this.Oa = c;
  return d
};
z.Nh = function(a, b) {
  this.Oa = new Ad;
  this.Wr(this.Oa, a, b);
  this.ox(a)
};
z.ox = function(a) {
  Bd(this.Oa, a)
};
z.Wr = function(a, b, c) {
  this.ep.Dd = c;
  this.Ep.Dd = c;
  this.Wp.Dd = c;
  this.jo.Dd = c;
  a.fh(lt(this.Wp, c, b));
  a.$g(lt(this.jo, c, b));
  a.bh(lt(this.ep, c, b));
  a.eh(lt(this.Ep, c, b))
};
function pk(a, b, c, d, f) {
  a && (this.Xa = a);
  b && (this.Ze = b);
  c != p && c != h && (this.ge = c);
  d && (this.OI = d);
  f && (this.$l = f)
}
z = pk.prototype;
z.Xa = p;
z.Ba = u("Xa");
z.Di = t("Xa");
z.Ze = p;
z.ge = p;
z.ng = u("ge");
z.OI = p;
z.py = u("OI");
z.$l = p;
z.Hn = 0;
z.Jo = u("Hn");
function Fk(a) {
  this.aa = a
}
Fk.prototype.aa = p;
function tt(a, b, c) {
  for(var b = C(b, "series") ? J(b, "series") : p, d = Gk(a.aa), f = 0;f < d;f++) {
    var g = Hk(a.aa, f);
    if(!b || g.getName() == b) {
      var k;
      k = g.gb.ka().wa.Jt ? -1 == g.gb.Jb() ? g.ng() : g.gb.Jb() : g.gb.ka().wa.Jb();
      k = new pk(g.Ba(), g.Ze, k, g.Ab().py(), g);
      k.Hn = g.Jb();
      ok(c, k)
    }
  }
}
function ut(a, b, c) {
  for(var b = C(b, "threshold") ? N(b, "threshold") : p, a = a.aa.Li.$E, d = a.length, f = 0;f < d;f++) {
    var g = a[f];
    (!b || b == g.getName().toLowerCase()) && g.getData(c)
  }
}
function vt() {
  this.ge = Wf
}
z = vt.prototype;
z.ab = 20;
z.Ka = u("ab");
z.ob = 10;
z.vb = u("ob");
z.Xa = p;
z.Ba = u("Xa");
z.Yy = q;
z.ge = p;
z.ng = u("ge");
z.Mu = p;
z.Nu = p;
z.g = function(a) {
  if(a && (C(a, "width") && (this.ab = F(a, "width")), C(a, "height") && (this.ob = F(a, "height")), C(a, "color") && "%color" != J(a, "color") && (this.color = Yb(a)), C(a, "marker"))) {
    if(a = F(a, "marker"), C(a, "enabled") && (this.Yy = K(a, "enabled")), C(a, "type") && "%markertype" != N(a, "type") && (this.ge = Yf(Wb(a, "type"))), C(a, "color") && (this.Mu = Yb(a)), C(a, "size")) {
      this.Nu = M(a, "size")
    }
  }
};
z.copy = function(a) {
  a && (a = new vt);
  a.ab = this.ab;
  a.ob = this.ob;
  a.Yy = this.Yy;
  a.ge = this.ge;
  a.Nu = this.Nu;
  a.Mu = this.Mu;
  a.Xa = this.Xa;
  return a
};
function wt(a) {
  this.ge = Wf;
  this.aa = a;
  this.Ny = q;
  this.By = j
}
A.e(wt, vt);
z = wt.prototype;
z.Ny = p;
z.aa = p;
z.By = p;
z.Hn = p;
z.Jo = u("Hn");
z.g = function(a) {
  wt.f.g.call(this, a);
  C(a, "type") && (this.Ny = "box" == Wb(a, "type"));
  C(a, "series_type") && (a = Wb(a, "series_type"), "%SeriesType" != a && (this.By = q), this.Hn = a)
};
z.copy = function(a) {
  a || (a = new wt(this.aa));
  wt.f.copy.call(this, a);
  a.Ny = this.Ny;
  a.By = this.By;
  a.Hn = this.Hn;
  return a
};
function xt() {
}
A.e(xt, Kd);
xt.prototype.Ia = 1;
xt.prototype.ga = u("Ia");
xt.prototype.g = function(a) {
  xt.f.g.call(this, a);
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
function yt(a) {
  W.call(this, a);
  this.xb = new P;
  A.I.nb(this.G, ka.Ms, this.nj, q, this);
  A.I.nb(this.G, ka.Ls, this.Jl, q, this)
}
A.e(yt, W);
yt.prototype.xb = p;
yt.prototype.$l = p;
yt.prototype.nj = function() {
  zt(this, j)
};
yt.prototype.Jl = function() {
  zt(this, q)
};
function zt(a, b) {
  var c = a.$l;
  if(c && c.K) {
    c.ya = b ? c.K.dc : c.K.wa;
    var d = b ? function(a) {
      a.ah()
    } : function(a) {
      a.dh()
    };
    if(c.ba) {
      for(var f = c.ba.length, g = 0;g < f;g++) {
        d(c.ba[g])
      }
    }else {
      c instanceof fr && d(c)
    }
  }
}
function At(a) {
  this.aa = a;
  this.Nq = this.Oq = NaN
}
z = At.prototype;
z.eg = p;
z.Mo = p;
z.fp = p;
z.Hu = p;
z.er = p;
z.Pz = p;
z.Sr = p;
z.dr = p;
z.Hd = p;
z.aa = p;
z.Tl = t("aa");
z.Oq = p;
z.Nq = p;
z.g = function(a) {
  C(a, "format") ? Bt(this, J(a, "format")) : C(a, "text") && Bt(this, J(a, "text"));
  this.Hd && (this.Hd = this.Hd.copy(), C(a, "icon") && this.Hd.g(F(a, "icon")))
};
function Ct(a) {
  var b = "fill:" + ub(a.Ba()) + ";fill-opacity:1; stroke-thickness:1; stroke-opacity:1;stroke:", a = "darkcolor(" + ub(a.Ba()) + ")", a = zb(a, h);
  return b + a
}
function Bt(a, b) {
  a.Mo = q;
  a.er = q;
  var c = b.indexOf("{%Icon}");
  a.dr = -1 != c;
  if(a.dr) {
    if(0 < c && (a.Mo = j, a.zE = b.substring(0, c), a.Hu = new ie(a.zE)), c < b.length - 7) {
      a.er = j, a.MN = b.substring(c + 7), a.Pz = new ie(a.MN)
    }
  }else {
    a.Mo = j, a.zE = b, a.Hu = new ie(a.zE)
  }
}
z.copy = function(a) {
  a || (a = new At);
  a.Hd = this.Hd;
  a.dr = this.dr;
  a.Mo = this.Mo;
  a.Hu = this.Hu;
  a.fp = this.fp;
  a.er = this.er;
  a.Sr = this.Sr;
  a.Pz = this.Pz;
  a.eg = this.eg;
  a.aa = this.aa;
  a.Oq = this.Oq;
  a.Nq = this.Nq;
  return a
};
function Dt() {
  Us.apply(this);
  this.Tz = this.ex = 2;
  this.Fm = this.En = 0;
  this.St = "{%Icon} {%Name}";
  this.Wi = this.ex;
  this.Ci = this.Tz
}
A.e(Dt, ct);
z = Dt.prototype;
z.ex = p;
z.Tz = p;
z.Fm = p;
z.Iz = p;
z.St = p;
z.Af = p;
z.Le = p;
z.Wi = p;
z.Ci = p;
z.Qr = p;
z.Al = p;
z.Gn = p;
z.Iu = p;
z.QI = p;
z.Rk = p;
z.hJ = q;
z.px = p;
z.gu = p;
z.xt = p;
z.En = p;
z.g = function(a, b) {
  Dt.f.g.call(this, a, b);
  a = Zb(a);
  C(a, "width") && delete a.width;
  C(a, "height") && delete a.height;
  C(a, "columns_padding") && (this.ex = M(a, "columns_padding"));
  C(a, "rows_padding") && (this.Tz = M(a, "rows_padding"));
  C(a, "elements_align") && (this.Fm = vd(Wb(a, "elements_align")));
  C(a, "format") && (this.St = J(a, "format"));
  Ub(a, "columns_separator") && (this.Af = new xt, this.Af.g(F(a, "columns_separator")));
  Ub(a, "rows_separator") && (this.Le = new xt, this.Le.g(F(a, "rows_separator")));
  C(a, "ignore_auto_item") && (this.QI = K(a, "ignore_auto_item"));
  var c = a;
  this.gu = [];
  this.Iu = [];
  var d = this.xt = new At(this.aa);
  d.eg = new ye;
  d.eg.g(c);
  d.eg.Y = p;
  d.eg.lC = q;
  d.g(c);
  if(d.aa && (d.Hd = new wt(d.aa), C(c, "icon") && d.Hd.g(F(c, "icon")), C(c, "fixed_item_width") && (d.Oq = M(c, "fixed_item_width")), C(c, "fixed_item_height"))) {
    d.Nq = M(c, "fixed_item_height")
  }
  this.QI || (this.Iu.push({"#name#":"item", "#children#":[], source:"auto"}), this.gu.push(this.xt));
  if(C(c, "items")) {
    for(var c = I(F(c, "items"), "item"), d = c.length, f = 0;f < d;f++) {
      var g = c[f], k = this.xt.copy();
      k.g(g);
      this.gu.push(k);
      this.Iu.push(g)
    }
  }
  this.Wi = this.ex;
  this.Ci = this.Tz;
  this.Af && (this.Wi += this.Af.ga());
  this.Le && (this.Ci += this.Le.ga());
  this.Iz = this.Fm
};
z.p = function(a) {
  Dt.f.p.call(this, a);
  this.Gn = new W(this.Al.r());
  this.Al.ia(this.Gn);
  return this.D
};
z.ty = function() {
  if(!this.hJ) {
    this.Rk = [];
    for(var a = this.Iu.length, b = 0;b < a;b++) {
      this.px = this.gu[b];
      this.px.Tl(this.aa);
      var c = this.Iu[b];
      if(C(c, "source")) {
        var d = this.aa.BJ, f = c;
        switch(Wb(c, "source")) {
          case "series":
            tt(d, f, this);
            break;
          case "points":
            c = C(f, "name") ? J(f, "name") : p;
            f = C(f, "series") ? J(f, "series") : p;
            for(d.aa.Yz = 0;d.aa.Yz < d.aa.o.length;) {
              var g;
              g = d.aa;
              var k = g.o[g.Yz];
              g.Yz++;
              g = k;
              if(!(f && g.getName() != f)) {
                for(var k = g.ba.length, l = 0;l < k;l++) {
                  var n = g.Ga(l);
                  if(n && (!c || c == n.getName())) {
                    n.Kg() && n.Kg().jt(n);
                    var m;
                    m = n.gb.ka().wa.Jt ? -1 == n.gb.Jb ? n.ng() : n.gb.Jb() : g.gb.ka().wa.ng();
                    n = new pk(n.Ba(), n.Ze, m, n.Ab().py(), n);
                    n.Hn = g.Jb();
                    ok(this, n)
                  }
                }
              }
            }
            break;
          case "thresholds":
            ut(d, f, this);
            break;
          case "auto":
            tt(d, f, this), ut(d, f, this)
        }
      }else {
        ok(this, new pk(p, 0, -1, j, p))
      }
    }
    this.gu = this.px = this.xt = p;
    this.hJ = j
  }
};
z.In = function() {
  Dt.f.In.call(this)
};
z.qa = function() {
  Dt.f.qa.call(this);
  this.Al.qb(this.xb.x, this.xb.y);
  this.mC();
  this.D.ia(this.Al)
};
z.Wa = function() {
  Dt.f.Wa.call(this);
  this.Al.qb(this.xb.x, this.xb.y);
  this.Gn && this.Gn.clear();
  this.mC()
};
function ok(a, b) {
  b.$l || (b.$l = a.aa);
  var c = a.px, d = new yt(c.aa.r());
  c.Mo && (c.eg.uc(c.aa.r(), c.Hu.ta(b.$l)), c.fp = c.eg.n(), d.xb.width += c.fp.width + 4, d.xb.height = Math.max(c.fp.height, d.xb.height));
  c.er && (c.eg.uc(c.aa.r(), c.Pz.ta(b.$l)), c.Sr = c.eg.n(), d.xb.width += c.Sr.width, d.xb.height = Math.max(c.Sr.height, d.xb.height));
  c.dr && (d.xb.width += c.Hd.Ka() + 4, d.xb.height = Math.max(c.Hd.vb(), d.xb.height));
  var f = 0, g, k;
  c.Mo && (g = (d.xb.height - c.fp.height) / 2, k = c.eg.ic(c.aa.r(), b.Ba()), k.qb(f, g), d.ia(k), f += c.fp.width + 4);
  if(c.dr) {
    k = new W(c.aa.r());
    g = (d.xb.height - c.Hd.vb()) / 2;
    c.aa.SH(c.Hd, b, k, 0, 0);
    d.ia(k);
    if(b.py() && c.Hd.Yy) {
      var l = c.Hd.ng() == Wf ? b.ng() : c.Hd.ng();
      if(l != Wf) {
        var n = c.Hd.Nu ? c.Hd.Nu : 0.5 * Math.min(c.Hd.ab, c.Hd.ob), m = new Rf.gP(c.aa.r()), o = c.Hd.Mu ? c.Hd.Mu : b.Ba(), v = c.aa.r().ja(), l = ag.Io(l, c.aa.r(), n, 0, 0);
        v.setAttribute("style", Ct(o));
        v.setAttribute("d", l);
        m.appendChild(v);
        m.Xd((c.Hd.Ka() - n) / 2);
        m.gd((c.Hd.vb() - n) / 2);
        d.ia(m)
      }
    }
    k.Xd(f);
    k.gd(g);
    f += c.Hd.Ka()
  }
  c.er && (g = (d.xb.height - c.Sr.height) / 2, k = c.eg.ic(c.aa.r(), b.Ba()), k.qb(f + 4, g), d.ia(k));
  d.$l = b.$l;
  isNaN(c.Oq) || (d.xb.width = c.Oq);
  isNaN(c.Nq) || (d.xb.height = c.Nq);
  a.Rk.push(d);
  a.Al.ia(d)
}
function Et() {
  Dt.call(this);
  this.Te = 1
}
A.e(Et, Dt);
z = Et.prototype;
z.Te = p;
z.yr = p;
z.qo = p;
z.Pp = p;
z.pt = p;
z.dx = p;
z.wc = p;
z.le = p;
z.g = function(a, b) {
  Et.f.g.call(this, a, b);
  C(a, "columns") && (this.Te = M(a, "columns"))
};
z.ty = function() {
  Et.f.ty.call(this);
  this.MD()
};
z.MD = function() {
  var a = this.Rk.length;
  this.yr = Math.ceil(a / this.Te);
  this.Te = Math.ceil(a / this.yr);
  this.pt = [];
  this.dx = [];
  for(var b = this.qo = this.Pp = 0, c = 0, d = 0;d < a;d++) {
    var f = this.Rk[d], b = Math.max(b, f.xb.width), c = c + f.xb.height;
    d == this.yr - 1 || d == this.Rk.length - 1 || 0 == (d + 1) % this.yr ? (this.dx.push(b), this.pt.length + 1 == this.Te || (b += this.Wi), this.pt.push(b), this.Pp += b, this.qo = Math.max(c, this.qo), c = b = 0) : c += this.Ci
  }
};
z.cl = function() {
  this.ty();
  this.le = this.wc = 0;
  this.Fm = this.Iz;
  this.En = 0;
  this.Za.si || (this.k.height = this.qo);
  var a = this.Xx();
  this.Za.si ? this.Do() : this.k.height > a && (this.k.height = a);
  this.Qr = this.qo;
  if(this.Za.hj) {
    if(this.wc = 0, a = Vs(this) - this.En, this.Pp < a) {
      switch(this.Iz) {
        case 2:
          this.wc = (a - this.Pp) / 2;
          break;
        case 1:
          this.wc = a - this.Pp
      }
    }else {
      this.Fm = 0
    }
  }else {
    this.k.width = this.Pp + this.En
  }
  Et.f.cl.call(this)
};
z.mC = function() {
  if(this.Af) {
    var a = new P(0, 0, this.Af.Va(), this.qo)
  }
  if(this.Le) {
    var b = new P(0, 0, 0, this.Le.Va())
  }
  var c = this.Al.r(), d, f, g, k, l = this.wc, n = this.le, m = this.Rk.length, o = 0, v = this.Wi / 2, w = v;
  this.Af && (v += this.Af.ga() / 2, w -= this.Af.ga() / 2);
  for(var y = 0;y < m;y++) {
    d = this.Rk[y];
    d.Xd(l);
    d.gd(n);
    switch(this.Fm) {
      case 2:
        qd(d, (this.dx[o] - d.xb.width) / 2);
        break;
      case 1:
        qd(d, this.dx[o] - d.xb.width)
    }
    if(y == this.yr - 1 || y == this.Rk.length - 1 || 0 == (y + 1) % this.yr) {
      l += this.pt[o], n = 0, this.Af && o + 1 != this.Te && (g = l - this.Wi / 2, a && (a.x = g - this.Af.Va() / 2), d = c.ja(), f = S(g, 0), f += U(g, this.qo), g = Ld(this.Af, c, a), d.setAttribute("d", f), d.setAttribute("style", g), this.Gn.appendChild(d)), o++
    }else {
      if(n += d.xb.height + this.Ci, this.Le) {
        k = n - this.Ci / 2;
        g = l;
        var E = this.pt[o] - v;
        0 < o && (g -= w, E += w);
        o + 1 == this.Te && (E += v);
        b && (b.width = E, b.x = g, b.y = k - this.Le.Va() / 2);
        d = c.ja();
        f = S(g, k);
        f += U(g + E, k);
        g = Ld(this.Le, c, b);
        d.setAttribute("d", f);
        d.setAttribute("style", g);
        this.Gn.appendChild(d)
      }
    }
  }
};
function Ft() {
  this.kn = [];
  this.ma = this.ob = this.ab = 0
}
z = Ft.prototype;
z.kn = p;
z.ab = p;
z.Ka = u("ab");
z.Cj = t("ab");
z.ob = p;
z.vb = u("ob");
z.Ql = t("ob");
z.ma = p;
z.gd = t("ma");
z.Xb = u("ma");
function Gt(a, b) {
  for(var c = a.kn.length, d = 0;d < c;d++) {
    qd(a.kn[d], b)
  }
}
function Ht() {
  Dt.call(this)
}
A.e(Ht, Dt);
z = Ht.prototype;
z.jj = p;
z.yg = p;
z.cl = function() {
  this.ty();
  this.Fm = this.Iz;
  this.MD();
  Ht.f.cl.call(this)
};
z.MD = function() {
  var a;
  this.Za.hj ? a = Vs(this) : (a = this.Za.Ir, Ws(this) && (a -= Xs(this)));
  var b = this.Za.si ? this.Do() : this.Xx();
  It(this, a);
  this.Za.hj || (this.k.width = this.jj + this.En);
  this.Za.si || (this.k.height = Math.min(this.Qr, b));
  if(this.Qr > b && (It(this, a - this.En), this.Za.hj || (this.k.width = this.jj + this.En), !this.Za.si)) {
    this.k.height = Math.min(this.Qr, b)
  }
};
function It(a, b) {
  a.jj = 0;
  a.yg = [];
  var c = a.Rk.length, d = a.Qr = 0, f = 0, g = q, k = new Ft;
  k.gd(f);
  for(var l = 0;l < c;l++) {
    var n = a.Rk[l];
    if(g = d + n.xb.width > b) {
      k.Cj(d - a.Wi), a.jj = Math.max(k.Ka(), a.jj), a.yg.push(k), f += k.vb() + a.Ci, d = 0, k = new Ft, k.gd(f)
    }
    n.Xd(d);
    n.gd(f);
    k.Ql(Math.max(k.vb(), n.xb.height));
    d += n.xb.width + a.Wi;
    k.kn.push(n)
  }
  k.Cj(d - a.Wi);
  a.yg.push(k);
  a.jj = Math.max(k.Ka(), a.jj);
  f += k.vb();
  a.Qr = f
}
z.mC = function() {
  if(this.Af) {
    var a = new P(0, 0, this.Af.Va(), 0)
  }
  if(this.Le) {
    var b = new P(0, 0, this.jj, this.Le.Va())
  }
  for(var c, d, f, g = this.Al.r(), k = this.yg.length, l = 0;l < k;l++) {
    var n = this.yg[l], m = n.kn.length, o = 0;
    if(0 == this.En) {
      if(3 == this.Za.za) {
        var v = n;
        c = Vs(this);
        if(!(v.ab >= c)) {
          var w = v.kn.length;
          c /= w;
          for(d = 0;d < w;d++) {
            var y = v.kn[d];
            y.Xd(c * d + (c - y.xb.width) / 2)
          }
        }
      }else {
        switch(this.Fm) {
          case 2:
            o = (Vs(this) - this.jj) / 2;
            Gt(n, (Vs(this) - n.Ka()) / 2);
            break;
          case 1:
            o = Vs(this) - this.jj, Gt(n, Vs(this) - n.Ka())
        }
      }
    }
    if(this.Af) {
      v = n.Xb();
      w = n.vb();
      0 == l && 0 < k ? (w += this.Ci / 2, this.Le && (w -= this.Le.ga() / 2)) : 0 < l && l == k - 1 ? (c = this.Ci / 2, this.Le && (c -= this.Le.ga() / 2), w += c, v -= c) : (c = this.Ci, this.Le && (c -= this.Le.ga()), w += c, v -= c / 2);
      for(y = 0;y < m;y++) {
        y < m - 1 && (c = n.kn[y], f = c.Sd() + c.xb.width + this.Wi / 2, a.x = f - this.Af.IA / 2, a.y = v, a.height = w, c = g.ja(), d = S(f, v), d += U(f, v + w), f = Ld(this.Af, g, a), c.setAttribute("d", d), c.setAttribute("style", f), this.Gn.appendChild(c))
      }
    }
    this.Le && l < k - 1 && (n = n.Xb() + n.vb() + this.Ci / 2, b.y = n - this.Le.Va() / 2, c = g.ja(), d = S(o, n), d += U(o + this.jj, n), f = Ld(this.Le, g, b), c.setAttribute("d", d), c.setAttribute("style", f), this.Gn.appendChild(c))
  }
};
function Jt() {
  ze(this);
  this.Ia = 5;
  this.b = new me
}
A.e(Jt, ye);
z = Jt.prototype;
z.jx = p;
z.Ia = p;
z.ga = u("Ia");
z.yc = p;
z.ib = p;
z.vc = p;
z.F = p;
z.$c = p;
function Kt(a) {
  return a.$c[a.$c.length - 1]
}
z.vE = p;
z.Er = p;
z.Dr = p;
z.Qv = p;
z.Pv = p;
z.Hr = p;
z.Gr = p;
z.Sv = p;
z.Rv = p;
z.dz = p;
z.ez = p;
z.DE = p;
z.Qu = p;
z.sf = p;
z.Qd = p;
z.bb = p;
z.g = function(a) {
  Jt.f.g.call(this, a);
  C(a, "format") && (this.yc = new ie(J(a, "format")));
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
z.p = function(a) {
  this.$c = [];
  this.vE = [];
  this.F = a;
  this.Qu = this.DE = this.ez = this.dz = this.Sv = this.Rv = this.Hr = this.Gr = this.Qv = this.Pv = this.Er = this.Dr = 0;
  this.ib = this.jx.Kg();
  this.jx.Oe ? Lt(this, this.SM) : Lt(this, this.RM)
};
function Lt(a, b) {
  var c = a.ib.wb, d;
  if(a.jx.Nb()) {
    for(d = c - 1;0 <= d;d--) {
      b.call(a, d)
    }
  }else {
    for(d = 0;d <= c;d++) {
      b.call(a, d)
    }
  }
  c = a.ib.wb;
  for(d = 0;d < c;d++) {
    a.dz = Math.max(a.dz, a.$c[d].width + a.$c[d + 1].width), a.DE = Math.max(a.DE, a.$c[d].height + a.$c[d + 1].height)
  }
  if(2 < c) {
    for(d = 0;d <= c - 2;d++) {
      a.ez = Math.max(a.ez, a.$c[d].width + a.$c[d + 1].width), a.Qu = Math.max(a.Qu, a.$c[d].height + a.$c[d + 1].height)
    }
  }
}
z.SM = function(a) {
  this.sf = this.ib.Vc[a].Tb;
  this.Qd = this.ib.Vc[a].Lb;
  this.bb = this.sf + (this.Qd - this.sf) / 2;
  Mt(this);
  Nt(this, 0 == a % 2)
};
z.RM = function(a) {
  this.sf = this.Qd = this.bb = a != this.ib.wb ? this.ib.Vc[a].Tb : this.ib.Vc[a - 1].Lb;
  Mt(this);
  Nt(this, 0 == a % 2)
};
function Nt(a, b) {
  var c = a.yc.ta(a);
  a.uc(a.F, c);
  a.$c.push(a.n());
  a.vE.push(c);
  b ? (a.Qv += a.n().width, a.Pv += a.n().height, a.Er = Math.max(a.Er, a.n().width), a.Dr = Math.max(a.Dr, a.n().height)) : (a.Sv += a.n().width, a.Rv += a.n().height, a.Hr = Math.max(a.Hr, a.n().width), a.Gr = Math.max(a.Gr, a.n().height))
}
z.It = function(a, b, c) {
  this.uc(this.F, this.vE[c]);
  this.vc = this.ic(this.F);
  this.vc.qb(b.x, b.y);
  a.ia(this.vc)
};
z.b = p;
function Mt(a) {
  a.b.add("%RangeMin", a.sf);
  a.b.add("%RangeMax", a.Qd);
  a.b.add("%Value", a.bb)
}
z.Na = function(a) {
  return se(a) ? this.ib.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  if(se(a)) {
    return this.ib.Pa(a)
  }
  switch(a) {
    case "%RangeMin":
    ;
    case "%RangeMax":
    ;
    case "%Value":
      return 2
  }
  return 4
};
function Ot(a) {
  W.call(this, a);
  ld(this.G);
  this.k = new P;
  A.I.nb(this.G, ka.Ms, this.nj, q, this);
  A.I.nb(this.G, ka.Ls, this.Jl, q, this)
}
A.e(Ot, W);
z = Ot.prototype;
z.K = p;
z.rf = t("K");
z.k = p;
z.n = u("k");
z.cx = p;
z.ba = p;
z.Yr = t("ba");
z.ya = p;
z.Hg = p;
z.Mg = p;
z.nj = function() {
  this.ya = this.K.dc;
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].ah()
  }
  this.update()
};
z.Jl = function() {
  this.ya = this.K.wa;
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].dh()
  }
  this.update()
};
z.qa = function() {
  this.ya = this.K.wa;
  this.Hg = Mc(this.F);
  this.Mg = Mc(this.F);
  this.G.appendChild(this.Hg);
  this.G.appendChild(this.Mg);
  this.update()
};
z.update = function() {
  var a;
  this.ya.xc() ? (a = "", a = this.ya.Ie() ? a + Jd(this.ya.mc(), this.F, this.k, this.cx.Ba()) : a + md(), a = this.ya.Ld() ? a + Ld(this.ya.Mc(), this.F, this.k, this.cx.Ba()) : a + nd()) : a = od();
  this.Hg.setAttribute("style", a);
  this.Ta(this.Hg);
  a = this.ya.df() ? Hc(this.ya.De(), this.F, p, this.cx) : a + nd();
  this.Mg.setAttribute("style", a);
  this.Ta(this.Mg)
};
z.Ta = function(a) {
  a.setAttribute("x", this.k.x);
  a.setAttribute("y", this.k.y);
  a.setAttribute("width", this.k.width);
  a.setAttribute("height", this.k.height)
};
function Pt() {
  Us.apply(this);
  this.Oe = q;
  this.Gj = 5;
  this.mj = this.Gf = this.xn = this.Zd = 0;
  this.ui = 2;
  this.Td = this.Ky = this.Ly = q;
  this.vy = 1;
  this.pE = q
}
A.e(Pt, ct);
z = Pt.prototype;
z.pf = p;
z.of = p;
z.MF = p;
z.LF = p;
z.yE = p;
z.mG = p;
z.kF = p;
z.zq = p;
z.O = p;
z.ui = p;
z.mj = p;
z.Gf = p;
z.lh = p;
z.Oe = p;
z.Gj = p;
z.xn = p;
z.Zd = p;
z.Td = p;
z.Nb = u("Td");
z.vy = p;
z.ib = p;
z.Kg = u("ib");
z.Ly = p;
z.Ky = p;
z.zt = p;
z.yt = p;
z.sj = p;
z.wc = p;
z.le = p;
z.Ty = p;
z.Sy = p;
z.Uy = p;
z.g = function(a, b) {
  Pt.f.g.call(this, a, b);
  if(C(a, "range_item")) {
    var c = F(a, "range_item");
    C(c, "width") && (this.MF = M(c, "width"));
    C(c, "height") && (this.LF = M(c, "height"));
    Ub(c, "background") && (this.kF = new Qf, this.kF.g(F(c, "background")))
  }
  if(C(a, "labels") && (c = F(a, "labels"), Tb(c))) {
    var d;
    a: {
      switch(Wb(c, "position")) {
        case "normal":
          d = 0;
          break a;
        default:
        ;
        case "combined":
          d = 2;
          break a;
        case "opposite":
          d = 1
      }
    }
    this.ui = d;
    this.O = new Jt;
    this.O.g(c);
    this.O.jx = this
  }
  Ub(a, "tickmark") && (c = F(a, "tickmark"), this.lh = new Kd, this.lh.g(c), C(c, "size") && (this.Gj = M(c, "size")));
  C(a, "tickmarks_placement") && (this.Oe = "center" == Wb(a, "tickmarks_placement"));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  F(a, "interval") && (this.vy = M(a, "interval"));
  C(a, "threshold") && (this.ib = this.aa.Li.rB[N(a, "threshold")])
};
z.p = function(a) {
  Pt.f.p.call(this, a);
  this.zq = new W(a);
  return this.D
};
z.In = function() {
  if(this.ib) {
    var a = this.aa.r();
    if(!this.pE) {
      this.O && this.O.p(a);
      this.sj = [];
      var b, c = this.ib.wb - 1;
      if(this.Td) {
        for(b = c;0 <= b;b--) {
          Qt(this, a, b)
        }
      }else {
        for(b = 0;b <= c;b++) {
          Qt(this, a, b)
        }
      }
      this.xn = this.Zd = 0;
      if(this.lh && this.lh.isEnabled()) {
        switch(this.ui) {
          case 2:
            this.Zd = 2 * this.Gj;
            this.xn = this.Gj;
            break;
          case 0:
            this.xn = this.Zd = this.Gj;
            break;
          case 1:
            this.Zd = this.Gj
        }
      }
      this.JB();
      this.pE = j
    }
    this.mG = this.yE = 0;
    !this.Za.hj && !this.Ly ? (this.pf = this.LB(), this.k.width = this.mq()) : this.Za.hj && !this.Ly ? (this.pf = this.Qw(), 0 > this.pf && (this.pf = 0, this.k.width = this.mq())) : !this.Za.hj && this.Ly ? (this.pf = this.MF, this.k.width = this.mq()) : (a = this.Qw(), this.pf = Math.min(a, this.MF), a = this.mq(), b = Vs(this), a < b && (this.yE = (b - a) / 2));
    !this.Za.si && !this.Ky ? (this.of = this.KB(), this.k.height = this.jq()) : this.Za.si && !this.Ky ? (this.of = this.Pw(), 0 > this.of && (this.of = 0, this.k.height = this.jq())) : !this.Za.si && this.Ky ? (this.of = this.LF, this.k.height = this.jq()) : (a = this.Pw(), this.of = Math.min(a, this.LF), a = this.jq(), b = this.Do(), a < b && (this.mG = (b - a) / 2));
    Pt.f.In.call(this)
  }else {
    this.k.width = 0, this.k.height = 0, Pt.f.In.call(this, this)
  }
};
z.pE = p;
function Qt(a, b, c) {
  b = new Ot(b);
  b.rf(a.kF);
  b.cx = Sj(a.ib.ot, c);
  b.Yr(a.ib.Vc[c].ba);
  a.sj.push(b)
}
z.qa = function() {
  Pt.f.qa.call(this);
  Rt(this, j);
  this.D.ia(this.zq)
};
function St(a, b, c, d) {
  var f = a.D.r().ja();
  f.setAttribute("style", Ld(a.lh, a.D.r(), b));
  f.setAttribute("d", a.FI(b, c, d));
  a.zq.appendChild(f)
}
z.Wa = function() {
  Pt.f.Wa.call(this);
  Rt(this)
};
function Rt(a, b) {
  if(a.ib) {
    a.zq.clear();
    var c = new O, d = 0;
    a.av();
    for(var f = a.sj.length, g = 0;g < f;g++) {
      a.Dv(a.sj[g].n(), g);
      b ? a.sj[g].qa() : a.sj[g].update();
      if(0 == g % a.vy) {
        a.lh && St(a, a.sj[g].n(), g, d);
        if(a.O && a.O.isEnabled()) {
          var k = a, l = c, n = a.sj[g].n(), m = g, o = d;
          k.qf(l, n, m, o);
          k.rm(m, l, o) || k.O.It(k.zq, l, m)
        }
        a.D.ia(a.sj[g])
      }
      d++
    }
    !a.Oe && 0 == f % a.vy && (a.lh && a.lh.isEnabled() && St(a, a.sj[f - 1].n(), f, d), a.O && a.O.isEnabled() && (g = a.sj[f - 1].n(), a.qf(c, g, f, d), a.rm(f, c, d) || a.O.It(a.zq, c, f)))
  }
}
z.rm = function(a, b, c) {
  a = this.O.$c[a].Ca();
  a.x = b.x;
  a.y = b.y;
  if(2 == this.ui) {
    if(0 == c % 2) {
      if(this.Sy) {
        if(Wc(this.Sy, a)) {
          return j
        }
      }else {
        this.Sy = a
      }
      this.Sy = a
    }else {
      if(this.Uy) {
        if(Wc(this.Uy, a)) {
          return j
        }
      }else {
        this.Uy = a
      }
      this.Uy = a
    }
  }else {
    if(this.Ty) {
      if(Wc(this.Ty, a)) {
        return j
      }
    }else {
      this.Ty = a
    }
    this.Ty = a
  }
  return q
};
z.av = function() {
  this.wc = this.xb.tb() + this.yE;
  this.le = this.xb.rb() + this.mG
};
z.Dv = function(a) {
  a.width = this.pf;
  a.height = this.of
};
z.JB = s();
z.LB = x(NaN);
z.mq = x(NaN);
z.Qw = x(NaN);
z.KB = x(NaN);
z.jq = x(NaN);
z.Pw = x(NaN);
function Tt(a, b) {
  return 0 == a.ui || 2 == a.ui && 0 == b % 2
}
function Ut() {
  Pt.call(this);
  this.zt = 20;
  this.yt = 10
}
A.e(Ut, Pt);
z = Ut.prototype;
z.av = function() {
  Ut.f.av.call(this);
  this.le += this.mj + this.xn;
  if(this.O) {
    if(this.Oe) {
      var a = this.O.$c[0].width;
      a > this.pf && (this.wc += (a - this.pf) / 2)
    }else {
      this.wc += this.O.$c[0].width / 2
    }
  }
};
z.Dv = function(a, b) {
  Ut.f.Dv.call(this, a, b);
  a.x = this.wc + this.pf * b;
  a.y = this.le
};
z.KB = u("yt");
z.Pw = function() {
  return this.Do() - this.Zd - this.Gf
};
z.jq = function() {
  return this.of + this.Zd + this.Gf
};
z.LB = function() {
  if(!this.O || !this.O.isEnabled()) {
    return this.zt
  }
  switch(this.ui) {
    case 2:
      return!this.Oe && 1 < this.ib.wb || this.Oe && 2 < this.ib.wb ? 0.25 * this.O.ez : this.zt;
    case 0:
    ;
    case 1:
      return this.O.dz / 2
  }
  return NaN
};
z.mq = function() {
  var a = this.pf * this.ib.wb;
  this.O && this.O.isEnabled() && (this.Oe ? (this.O.$c[0].width > this.pf && (a += (this.O.$c[0].width - this.pf) / 2), Kt(this.O).width > this.pf && (a += (Kt(this.O).width - this.pf) / 2)) : a += this.O.$c[0].width / 2 + Kt(this.O).width / 2);
  return a
};
z.Qw = function() {
  var a = Vs(this);
  if(this.O && this.O.isEnabled()) {
    if(this.Oe) {
      var b = q, b = 2 == this.ui ? this.O.Qv <= a && this.O.Sv <= a : this.O.Qv + this.O.Sv <= a, c = this.O.$c[0].width, d = Kt(this.O).width, f = this.ib.wb;
      if(b) {
        b = a / this.ib.wb;
        if(b >= c && b >= d) {
          return b
        }
        b = (2 * a - c) / (2 * f - 1);
        if(b <= c && b >= d) {
          return b
        }
        b = (2 * a - d) / (2 * f - 1);
        if(b >= c && b <= d) {
          return b
        }
        b = (2 * a - c - d) / (2 * (f - 1));
        if(b <= c && b <= d) {
          return b
        }
      }
      return(2 * a - c - d) / (2 * (f - 1))
    }
    a -= this.O.$c[0].width / 2 + Kt(this.O).width / 2
  }
  return a / this.ib.wb
};
z.qf = function(a, b, c, d) {
  var f = this.O.$c[c];
  a.x = this.Oe ? b.x + (b.width - f.width) / 2 : (c == this.ib.wb ? b.Ja() : b.tb()) - f.width / 2;
  b = this.O.ga() + this.xn;
  a.y = Tt(this, d) ? this.le - b - f.height : this.le + this.of + b
};
z.JB = function() {
  this.mj = this.Gf = 0;
  if(this.O && this.O.isEnabled()) {
    switch(this.ui) {
      case 2:
        this.Gf = this.mj = this.O.Dr + this.O.ga();
        if(1 < this.ib.wb && this.Oe || 0 < this.ib.wb && !this.Oe) {
          this.Gf += this.O.Gr + this.O.ga()
        }
        break;
      case 0:
        this.mj = this.Gf = Math.max(this.O.Dr, this.O.Gr) + this.O.ga();
        break;
      case 1:
        this.Gf = Math.max(this.O.Dr, this.O.Gr)
    }
  }
};
z.FI = function(a, b, c) {
  a = this.Oe ? a.x + (a.width - 0) / 2 : (b == this.ib.wb ? a.Ja() : a.tb()) - 0;
  c = Tt(this, c) ? this.le - this.Gj : this.le + this.of;
  b = S(a, c);
  return b += U(a, c + this.Gj)
};
function Vt() {
  Pt.call(this);
  this.zt = 10;
  this.yt = 20
}
A.e(Vt, Pt);
z = Vt.prototype;
z.av = function() {
  Vt.f.av.call(this);
  this.wc += this.mj + this.xn;
  if(this.O) {
    if(this.Oe) {
      var a = this.O.$c[0].height;
      a > this.of && (this.le += (a - this.of) / 2)
    }else {
      this.le += this.O.$c[0].height / 2
    }
  }
};
z.Dv = function(a, b) {
  Vt.f.Dv.call(this, a, b);
  a.x = this.wc;
  a.y = this.le + this.of * b
};
z.KB = function() {
  if(!this.O || !this.O.isEnabled()) {
    return this.yt
  }
  switch(this.ui) {
    case 2:
      return!this.Oe && 1 < this.ib.wb || this.Oe && 2 < this.ib.wb ? 0.25 * this.O.Qu : this.yt;
    case 0:
    ;
    case 1:
      return this.O.Qu / 2
  }
  return NaN
};
z.Pw = function() {
  var a = this.of * this.ib.wb;
  this.O && this.O.isEnabled() && (this.Oe ? (this.O.$c[0].height > this.of && (a += (this.O.$c[0].height - this.of) / 2), Kt(this.O).height > this.of && (a += (Kt(this.O).height - this.of) / 2)) : a += this.O.$c[0].height / 2 + Kt(this.O).height / 2);
  return a
};
z.jq = function() {
  var a = this.Do();
  if(this.O && this.O.isEnabled()) {
    if(this.Oe) {
      var b = q, b = 2 == this.ui ? this.O.Pv <= a && this.O.Rv <= a : this.O.Pv + this.O.Rv <= a, c = this.O.$c[0].height, d = Kt(this.O).height, f = this.ib.wb;
      if(b) {
        b = a / this.ib.wb;
        if(b >= c && b >= d) {
          return b
        }
        b = (2 * a - c) / (2 * f - 1);
        if(b <= c && b >= d) {
          return b
        }
        b = (2 * a - d) / (2 * f - 1);
        if(b >= c && b <= d) {
          return b
        }
        b = (2 * a - c - d) / (2 * (f - 1));
        if(b <= c && b <= d) {
          return b
        }
      }
      return(2 * a - c - d) / (2 * (f - 1))
    }
    a -= this.O.$c[0].height / 2 + Kt(this.O).height / 2
  }
  return a / this.ib.wb
};
z.LB = u("zt");
z.mq = function() {
  return Vs(this) - this.Gf - this.Zd
};
z.Qw = function() {
  return this.pf + this.Gf + this.Zd
};
z.qf = function(a, b, c, d) {
  var f = this.O.Qx(c);
  a.y = this.Oe ? b.y + (b.height - f.height) / 2 : (c == this.ib.wb ? b.ra() : b.rb()) - f.height / 2;
  b = this.O.ga() + this.xn;
  a.x = Tt(this, d) ? this.wc - b - f.width : this.wc + this.pf + b
};
z.JB = function() {
  this.mj = this.Gf = 0;
  if(this.O && this.O.isEnabled()) {
    switch(this.ui) {
      case 2:
        this.mj = this.O.Er + this.O.ga();
        this.Gf = this.mj + this.O.Hr + this.O.ga();
        break;
      case 0:
        this.mj = this.Gf = Math.max(this.O.Er, this.O.Hr) + this.O.ga();
        break;
      case 1:
        this.Gf = Math.max(this.O.Er, this.O.Hr) + this.O.ga()
    }
  }
};
z.FI = function(a, b, c) {
  a = this.Oe ? a.y + (a.height - 0) / 2 : (b == this.ib.wb ? a.ra() : a.rb()) - 0;
  c = Tt(this, c) ? this.wc - this.Gj : this.wc + this.pf;
  b = S(c, a);
  return b += U(c + this.Gj, a)
};
function Wt() {
  Us.call(this)
}
A.e(Wt, Us);
z = Wt.prototype;
z.Ya = p;
z.mh = p;
z.KD = function(a, b, c) {
  Wt.f.KD.call(this, a, b, c);
  this.Ya = new De(a, b)
};
z.g = function(a, b) {
  var c = Ue(b, "label_style", a, C(a, "style") ? F(a, "style") : p, q);
  Wt.f.g.call(this, c, b);
  this.Ya.g(c);
  this.Ya.Y = p
};
z.p = function(a) {
  Wt.f.p.call(this, a);
  this.Ya.uc(a, this.Ya.Bh().ta(this.aa));
  this.mh = this.Ya.ic(a);
  this.Ya.nu(this.D);
  return this.D
};
z.cl = function() {
  this.Ya.uc(this.aa.r(), this.Ya.Bh().ta(this.aa));
  var a = this.Ya.n();
  this.k.width || (this.k.width = a.width, 2 == this.Ya.Tp && (this.k.width += this.Ya.ga()));
  this.k.height || (this.k.height = a.height);
  Wt.f.cl.call(this)
};
z.qa = function() {
  Wt.f.qa.call(this);
  this.D.ia(this.mh);
  this.zx()
};
z.zx = function() {
  var a = this.xb.x, b = this.xb.y;
  switch(this.Ya.Tp) {
    case 0:
      a += this.Ya.ga();
      break;
    case 2:
      a += (this.xb.width - this.Ya.n().width) / 2;
      break;
    case 1:
      a += this.xb.width - this.Ya.n().width - this.Ya.ga()
  }
  this.mh.qb(a, b)
};
z.Wa = function() {
  Wt.f.Wa.call(this);
  this.zx()
};
function Xt() {
}
Xt.prototype.create = function(a, b) {
  if(!b) {
    return p
  }
  switch(b) {
    case "legend":
      var c;
      a: {
        if(C(a, "elements_layout") && "horizontal" != N(a, "elements_layout")) {
          c = new Et
        }else {
          if(C(a, "position") && (c = N(a, "position"), "float" == c || "fixed" == c || "left" == c || "right" == c)) {
            c = new Et;
            break a
          }
          c = new Ht
        }
      }
      return c;
    case "color_swatch":
      a: {
        switch(N(a, "orientation")) {
          default:
          ;
          case "vertical":
            c = new Vt;
            break a;
          case "horizontal":
            c = new Ut
        }
      }
      return c;
    case "image":
      return p;
    case "label":
      return new Wt
  }
  return new Et
};
function Yt(a, b, c) {
  this.xm = [];
  this.N = a;
  this.aa = b;
  this.nv = c;
  this.mH = new Xt;
  this.NC = new ht;
  this.MC = new gt;
  this.WD = new rt;
  this.Mr = new st
}
z = Yt.prototype;
z.D = p;
z.fb = u("D");
z.xm = p;
z.aa = p;
z.N = p;
z.nv = p;
z.mH = p;
z.WD = p;
z.Mr = p;
z.NC = p;
z.MC = p;
z.g = function(a) {
  C(a, "align_left_by") && (this.Mr.EH = "chart" == Wb(a, "align_left_by"))
};
z.Dt = function(a, b, c) {
  c = this.mH.create(a, c);
  c != p && (c.KD(this.N, this.aa, this.nv), c.g(a, b), 4 == c.Za.Ib() ? this.NC.ye(c) : 5 == c.Za.Ib() ? this.MC.ye(c) : c.Za.ou ? this.WD.ye(c) : this.Mr.ye(c), this.xm.push(c))
};
z.p = function(a) {
  this.D = new W(a);
  for(var b = this.xm.length, c = 0;c < b;c++) {
    this.D.ia(this.xm[c].p(a))
  }
};
z.qa = function() {
  for(var a = this.xm.length, b = 0;b < a;b++) {
    this.xm[b].qa()
  }
};
z.Wa = function() {
  for(var a = this.xm.length, b = 0;b < a;b++) {
    this.xm[b].Wa()
  }
};
z.Nh = function(a, b) {
  this.Mr.Nh(a, b)
};
z.ox = function(a) {
  this.Mr.ox(a)
};
z.hi = function(a, b) {
  return this.Mr.hi(a, b) ? (this.WD.hi(a, b), this.NC.hi(a, b), this.MC.hi(a, b), j) : q
};
function Zt(a, b) {
  De.call(this, a, b)
}
A.e(Zt, De);
z = Zt.prototype;
z.Aa = 2;
z.Tc = q;
function $t(a, b) {
  var c = a.n(), d = a.ga(), f = c.width + d, d = c.height + d;
  switch(a.Aa) {
    case 0:
      a.D.Xd(b.x);
      b.x += f;
      b.width -= f;
      break;
    case 1:
      a.D.Xd(b.Ja() - c.width);
      b.width -= f;
      break;
    case 2:
      a.D.gd(b.y);
      b.y += d;
      b.height -= d;
      break;
    case 3:
      a.D.gd(b.ra() - c.height), b.height -= d
  }
}
z.qb = function(a, b) {
  var c = this.Tc ? b : a;
  switch(this.Aa) {
    case 2:
    ;
    case 3:
      var d = 0;
      switch(this.za) {
        case 0:
          d = c.x;
          break;
        case 1:
          d = c.x + (c.width - this.n().width) / 2;
          break;
        case 2:
          d = c.Ja() - this.n().width
      }
      d < c.x && (d = c.x);
      this.D.Xd(d);
      break;
    case 0:
    ;
    case 1:
      d = 0;
      switch(this.za) {
        case 0:
          d = c.y;
          break;
        case 1:
          d = c.y + (c.height - this.n().height) / 2;
          break;
        case 2:
          d = c.ra() - this.n().height
      }
      d < c.y && (d = c.y);
      this.D.gd(d)
  }
};
z.g = function(a) {
  Zt.f.g.call(this, a);
  if(C(a, "position")) {
    switch(Cb(F(a, "position"))) {
      case "left":
        this.Aa = 0;
        Ae(this, this.Bb.bc + 90);
        break;
      case "right":
        this.Aa = 1;
        Ae(this, this.Bb.bc - 90);
        break;
      case "top":
        this.Aa = 2;
        break;
      case "bottom":
        this.Aa = 3
    }
  }
  C(a, "align_by") && (this.Tc = "dataplot" == N(a, "align_by"))
};
z.ic = function(a, b) {
  this.uc(a, this.yc.ta(b));
  return Zt.f.ic.call(this, a)
};
z.Na = function(a) {
  return this.aa ? this.aa.Na(a) : ""
};
z.Pa = function(a) {
  return this.aa ? this.aa.Pa(a) : 1
};
function au(a, b) {
  this.N = a;
  this.qm = b;
  this.Xo = q
}
z = au.prototype;
z.N = p;
z.qm = p;
z.F = p;
z.sl = t("F");
z.Ya = p;
z.Mn = p;
z.Om = p;
z.Y = p;
z.Cc = p;
z.aa = p;
z.Tl = t("aa");
z.ca = u("aa");
z.D = p;
z.k = p;
z.n = u("k");
z.Oa = p;
z.Wr = t("Oa");
z.Sf = p;
z.Ki = p;
z.ax = function() {
  this.Ki && this.Ki.clear()
};
z.aJ = x(q);
z.ql = q;
z.Xo = p;
z.gE = u("Xo");
function bu(a, b, c) {
  return Tb(F(b, c)) ? (a = new Zt(a.N, a.aa), a.g(F(b, c)), a) : p
}
z.g = function(a) {
  this.ql = q;
  if(a) {
    if(!this.aa || this.aa.Yw(a)) {
      this.qm.as(), cu(this, a)
    }else {
      if(this.ql = j, this.Ki = new Te(F(a, "styles")), this.Sf = new Yt(this.N, this.aa, this), this.aa && (this.aa.sl(this.F), this.aa.yK(this.Sf), this.aa.g(a, this.Ki), this.aa.gG() && this.aa.vF(a)), C(a, "chart_settings")) {
        var b = F(a, "chart_settings");
        cu(this, a);
        this.Ya = bu(this, b, "title");
        this.Mn = bu(this, b, "subtitle");
        this.Om = bu(this, b, "footer");
        C(b, "controls") && this.Sf.g(F(b, "controls"));
        Ub(b, "legend") && this.Sf.Dt(F(b, "legend"), this.Ki, "legend");
        if(C(b, "controls")) {
          var a = F(b, "controls"), c;
          for(c in a) {
            if(b = a[c], b instanceof Array) {
              for(var d = b.length, f = 0;f < d;f++) {
                this.Dt(b[f], c)
              }
            }else {
              b instanceof Object && this.Dt(b, c)
            }
          }
        }
      }
    }
  }else {
    this.qm.as()
  }
};
z.Dt = function(a, b) {
  Tb(a) && this.Sf.Dt(a, this.Ki, b)
};
function cu(a, b) {
  if(C(b, "chart_settings")) {
    var c = F(b, "chart_settings");
    Tb(F(c, "chart_background")) && (a.Y = new Wd, a.Y.g(F(c, "chart_background")))
  }
}
z.ce = function(a, b) {
  this.Xo = j;
  this.k = b;
  this.D = a;
  this.Oa && Bd(this.Oa, b);
  this.Y && (this.Cc = this.Y.p(this.F), this.D.ia(this.Cc), this.eo = b.Ca(), Xd(this.Y, b));
  if(!this.ql) {
    return this.D
  }
  var c = this.Ya;
  c && this.D.ia(c.ic(this.F, this.aa));
  (c = this.Mn) && this.D.ia(c.ic(this.F, this.aa));
  (c = this.Om) && this.D.ia(c.ic(this.F, this.aa));
  c = du(this, b);
  this.aa.Rc(c);
  var d = c.Ca();
  this.Sf.p(this.F, this.D);
  this.aa && (this.Sf.Nh(c, d), this.aa.p(c, this.qm.ey(), j), eu(this, c, d), this.D.ia(this.aa.fb()));
  fu(this, b, this.aa.Vt());
  this.D.ia(this.Sf.fb());
  return this.D
};
function eu(a, b, c) {
  for(var d = a.Sf.hi(b, c), f = 0;!d;) {
    f++, 10 == f && e(Error("Finalize layout error")), a.Sf.ox(b), d = a.Sf.hi(b, c)
  }
}
function fu(a, b, c) {
  a.Om && a.Om.qb(b, c);
  a.Ya && a.Ya.qb(b, c);
  a.Mn && a.Mn.qb(b, c)
}
z.qa = function() {
  this.Cc && this.Y.qa(this.Cc, this.eo);
  this.aa && this.ql ? (this.aa.qa(), this.Sf.qa(), this.N.XA && (this.WA = new W(this.D.r()), this.lw = new ye, this.D.ia(this.WA), gu(this))) : this.ql || this.qm.as()
};
function du(a, b) {
  var c = b.Ca();
  a.Om && $t(a.Om, c);
  a.Ya && $t(a.Ya, c);
  a.Mn && $t(a.Mn, c);
  return c
}
z.Wa = function(a) {
  a = a.Ca();
  this.Oa && Bd(this.Oa, a);
  this.Y && this.Y.Wa(this.Cc, a);
  if(this.ql) {
    if(this.aa) {
      this.Y && Xd(this.Y, a);
      var b = du(this, a), c = b.Ca();
      this.aa.Rc(b);
      this.Sf.Nh(b, c);
      this.aa.nm(b);
      eu(this, b, c);
      this.aa.Jq(b);
      this.Sf.Wa();
      fu(this, a, this.aa.Vt())
    }
    this.k = a.Ca();
    this.N.XA && (this.WA.clear(), gu(this))
  }
};
z.DF = function() {
  this.ql = q
};
z.zp = function() {
  this.ql && this.aa && this.aa.zp()
};
z.WA = p;
z.lw = p;
function gu(a) {
  a.lw.g({font:{family:"Verdana", size:a.k.width / 12.4}});
  a.lw.uc(a.D.r(), a.N.XA);
  var b = a.lw.ic(a.D.r(), zb("red", h), j), c = a.lw.n();
  b.qb(a.k.width / 2 - c.width / 2 + a.k.x, a.k.height / 2 - c.height / 2 + a.k.y);
  b.G.setAttribute("opacity", 0.15);
  a.WA.ia(b)
}
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  this.Ya && (a.Title = this.Ya.Bh().ta(this.aa));
  this.Mn && (a.SubTitle = this.Mn.Bh().ta(this.aa));
  this.Om && (a.Footer = this.Om.Bh().ta(this.aa));
  this.ql && this.aa && this.aa.mb(a);
  return a
};
function hu(a) {
  this.bk = a;
  this.Ub = 0;
  this.Rw = NaN;
  this.oJ = j;
  this.ma = 0;
  this.Sw = NaN;
  this.pJ = j;
  this.ab = 1;
  this.zl = j;
  this.oq = NaN;
  this.ob = 1;
  this.yl = j;
  this.nq = NaN;
  this.Oa = new Ad(5)
}
z = hu.prototype;
z.Oa = p;
z.k = p;
z.D = p;
z.F = p;
z.sl = t("F");
z.Ub = p;
z.Sd = function(a) {
  return isNaN(this.Rw) ? a.x + (this.oJ ? a.width * this.Ub : this.Ub) : this.Rw
};
z.oJ = p;
z.Rw = p;
z.ma = p;
z.Xb = function(a) {
  return isNaN(this.Sw) ? a.y + (this.pJ ? a.height * this.ma : this.ma) : this.Sw
};
z.pJ = p;
z.Sw = p;
z.ab = p;
z.Ka = function(a) {
  return isNaN(this.oq) ? this.zl ? a.width * this.ab : this.ab : this.oq
};
z.zl = p;
z.oq = p;
z.ob = p;
z.vb = function(a) {
  return isNaN(this.nq) ? this.yl ? a.height * this.ob : this.ob : this.nq
};
z.yl = p;
z.nq = p;
z.bk = p;
z.g = function(a) {
  var b;
  C(a, "margin") && (this.Oa = new Ad, this.Oa.g(F(a, "margin")));
  C(a, "x") && (b = F(a, "x"), this.Ub = Mb(b) ? Nb(b) : Ob(b));
  C(a, "y") && (b = F(a, "y"), this.ma = Mb(b) ? Nb(b) : Ob(b));
  C(a, "width") && (b = F(a, "width"), this.ab = Mb(b) ? Nb(b) : Ob(b));
  C(a, "height") && (b = F(a, "height"), this.ob = Mb(b) ? Nb(b) : Ob(b))
};
z.ce = function(a, b) {
  this.D || (this.D = new W(a));
  this.Nh(b);
  return this.D
};
z.qa = s();
z.Wa = function(a) {
  this.Nh(a)
};
z.Nh = function(a) {
  this.qb(a)
};
z.qb = function(a) {
  this.k = a.Ca();
  this.k.x = this.Sd(a);
  this.k.y = this.Xb(a);
  this.k.width = this.Ka(a);
  this.k.height = this.vb(a);
  this.D.qb(this.k.x, this.k.y);
  this.k.x = 0;
  this.k.y = 0;
  this.Oa && Bd(this.Oa, this.k)
};
function iu(a) {
  hu.call(this, a);
  this.cm = new ju(a.dj(), this)
}
A.e(iu, hu);
z = iu.prototype;
z.cm = p;
z.Sa = p;
z.getName = u("Sa");
z.et = p;
z.g = function(a) {
  iu.f.g.call(this, a);
  this.bk.bm.push(this.cm);
  if(C(a, "name")) {
    var b = this.Sa = J(a, "name"), c = this.cm;
    b && c && (this.bk.kw[b] = c)
  }
  C(a, "source") && (b = J(a, "source"), c = q, C(a, "source_mode") && (a = Wb(a, "source_mode"), c = "external" === a || "externaldata" === a), c ? this.cm.mw = b : this.et = b)
};
z.ce = function(a, b) {
  iu.f.ce.call(this, a, b);
  this.et && (this.cm.Hc = this.bk.dj().sq[this.et], this.cm.ft = this.bk.dj().Xw[this.et]);
  this.et = p;
  this.cm.ce(a, this.D, new P(0, 0, this.k.width, this.k.height));
  if(this.Oa) {
    qd(this.D, this.Oa.tb());
    var c = this.D;
    c.ma += this.Oa.rb();
    pd(c)
  }
  c = this.cm;
  c.Hc ? c.Gk(c.Hc) : c.mw ? c.Hi(c.mw) : c.Du ? c.Du = c.Du : ku(c, c.N.uG);
  return this.D
};
z.Wa = function(a) {
  iu.f.Wa.call(this, a);
  this.cm.Wa(new P(0, 0, this.k.width, this.k.height))
};
function lu(a) {
  hu.call(this, a);
  this.yf = []
}
A.e(lu, hu);
z = lu.prototype;
z.yf = p;
z.RB = p;
z.hx = p;
z.g = function(a) {
  lu.f.g.call(this, a);
  for(var b in a) {
    var c = a[b];
    if(!dc(c) && (Boolean(c instanceof Object && !(c instanceof Function || c instanceof Array)) && mu(this, c, b), A.isArray(c))) {
      for(var d = c.length, f = 0;f < d;f++) {
        mu(this, c[f], b)
      }
    }
  }
};
function mu(a, b, c) {
  if(c = "hbox" === c ? new nu(a.bk) : "vbox" === c ? new ou(a.bk) : "view" === c && C(b, "type") ? "panel" == N(b, "type") ? new pu(a.bk) : new iu(a.bk) : p) {
    c.g(b), a.yf.push(c)
  }
}
z.ce = function(a, b) {
  this.RB = new W(a);
  this.hx = new W(a);
  lu.f.ce.call(this, a, b);
  this.D.ia(this.hx);
  this.D.ia(this.RB);
  for(var c = this.yf.length, d = 0;d < c;d++) {
    this.RB.ia(this.yf[d].ce(a, this.k))
  }
  return this.D
};
z.qa = function() {
  for(var a = this.yf.length, b = 0;b < a;b++) {
    this.yf[b].qa()
  }
};
z.Wa = function(a) {
  lu.f.Wa.call(this, a);
  for(var a = this.yf.length, b = 0;b < a;b++) {
    this.yf[b].Wa(this.k)
  }
};
function qu(a) {
  De.call(this, a, this)
}
A.e(qu, De);
z = qu.prototype;
z.yd = p;
z.nd = u("yd");
z.uc = function(a) {
  qu.f.uc.call(this, a, this.yc.ta());
  this.yd = this.Ia + this.k.height
};
z.ic = function(a, b) {
  var c = qu.f.ic.call(this, a);
  this.qb(c, b);
  return c
};
z.qb = function(a, b) {
  var c;
  switch(this.za) {
    case 0:
      c = b.x;
      break;
    case 2:
      c = b.x + b.width - this.k.width;
      break;
    case 1:
      c = b.x + (b.width - this.k.width) / 2
  }
  a.qb(c, b.y)
};
z.Na = x("");
z.Pa = x(1);
function pu(a) {
  lu.call(this, a)
}
A.e(pu, lu);
z = pu.prototype;
z.Ya = p;
z.mh = p;
z.Y = p;
z.Cc = p;
z.vz = p;
z.lH = p;
z.g = function(a) {
  a = ru(Zb(rf.defaults.panel), a);
  su(this, a)
};
function su(a, b) {
  pu.f.g.call(a, b);
  if(Ub(b, "title")) {
    var c = F(b, "title");
    C(c, "text") && (a.Ya = new qu(a.bk.dj()), a.Ya.g(c))
  }
  C(b, "background") && (a.Y = new Wd, a.Y.g(F(b, "background")))
}
function ru(a, b) {
  var c = Zb(b);
  ac(a, b, c);
  for(var d in a) {
    var f = a[d];
    dc(f) || (c[d] = c[d] ? $b(f, c[d], d) : f)
  }
  return c
}
z.ce = function(a, b) {
  this.Ya && this.Ya.uc(a);
  pu.f.ce.call(this, a, b);
  this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(a), this.hx.ia(this.Cc));
  this.Ya && (this.mh = this.Ya.ic(a, this.lH), this.hx.ia(this.mh));
  return this.D
};
z.qa = function() {
  pu.f.qa.call(this);
  this.Cc && this.Y.qa(this.Cc, this.vz)
};
z.Wa = function(a) {
  pu.f.Wa.call(this, a);
  this.mh && this.Ya.qb(this.mh, this.vz);
  this.Cc && this.Y.Wa(this.Cc, this.vz)
};
z.Nh = function(a) {
  pu.f.Nh.call(this, a);
  this.vz = this.k.Ca();
  this.Y && this.Y.isEnabled() && Xd(this.Y, this.k);
  this.lH = this.k.Ca();
  this.Ya && (this.k.y += this.Ya.nd(), this.k.height -= this.Ya.nd())
};
function nu(a) {
  lu.call(this, a)
}
A.e(nu, lu);
nu.prototype.ce = function(a, b) {
  nu.f.ce.call(this, a, b);
  return this.D
};
nu.prototype.Nh = function(a) {
  nu.f.Nh.call(this, a);
  for(var b = this.k.x, c = this.yf.length, d = 0;d < c;d++) {
    var f = this.yf[d];
    f.Rw = b;
    f.oq = NaN;
    f.oq = f.Ka(a);
    b += f.oq
  }
};
function ou(a) {
  lu.call(this, a)
}
A.e(ou, lu);
ou.prototype.ce = function(a, b) {
  ou.f.ce.call(this, a, b);
  return this.D
};
ou.prototype.Nh = function(a) {
  ou.f.Nh.call(this, a);
  for(var b = this.k.y, c = this.yf.length, d = 0;d < c;d++) {
    var f = this.yf[d];
    f.Sw = b;
    f.nq = NaN;
    f.nq = f.vb(a);
    b += f.nq
  }
};
function tu(a) {
  lu.call(this, this);
  this.N = a;
  this.kw = {};
  this.bm = []
}
A.e(tu, pu);
z = tu.prototype;
z.N = p;
z.dj = u("N");
z.kw = p;
z.bm = p;
z.Xo = q;
z.gE = x(j);
z.aJ = x(j);
z.fu = p;
z.Wr = t("fu");
z.fb = u("D");
z.n = u("k");
z.g = function(a) {
  var b = Zb(F(F(rf, "defaults"), "dashboard")), c = Zb(F(F(rf, "defaults"), "panel")), a = ru($b(c, b), a);
  su(this, a)
};
z.ce = function(a, b) {
  this.D = a;
  var c = b.Ca();
  this.fu && Bd(this.fu, c);
  tu.f.ce.call(this, this.F, c);
  this.Xo = j;
  return this.D
};
z.zp = function() {
  uu(this)
};
function uu(a) {
  if(a.Xo) {
    var b, c = a.bm.length;
    for(b = 0;b < c;b++) {
      var d = a.bm[b];
      if(d.hb && !d.hb.gE()) {
        return
      }
    }
    for(b = 0;b < c;b++) {
      a.bm[b].hb && a.bm[b].hb.gE() && a.bm[b].hb.zp()
    }
  }
}
z.Wa = function(a) {
  a = a.Ca();
  this.fu && Bd(this.fu, a);
  tu.f.Wa.call(this, a)
};
function vu(a, b) {
  var c;
  a instanceof vu ? (this.Ur(b == p ? a.pk : b), wu(this, a.nD()), xu(this, a.II()), yu(this, a.qI()), zu(this, a.jD()), Au(this, a.cu()), Bu(this, a.zI().Ca()), Cu(this, a.sI())) : a && (c = A.uri.R.split("" + a)) ? (this.Ur(!!b), wu(this, c[1] || "", j), xu(this, c[2] || "", j), yu(this, c[3] || "", j), zu(this, c[4]), Au(this, c[5] || "", j), Bu(this, c[6] || "", j), Cu(this, c[7] || "", j)) : (this.Ur(!!b), this.Bk = new Du(p, this, this.pk))
}
z = vu.prototype;
z.Dn = "";
z.ew = "";
z.wo = "";
z.pv = p;
z.qj = "";
z.Tt = "";
z.gN = q;
z.pk = q;
z.toString = function() {
  if(this.Ui) {
    return this.Ui
  }
  var a = [];
  this.Dn && a.push(Eu(this.Dn, Fu), ":");
  this.wo && (a.push("//"), this.ew && a.push(Eu(this.ew, Fu), "@"), a.push(A.Qc(this.wo) ? encodeURIComponent(this.wo) : p), this.pv != p && a.push(":", "" + this.jD()));
  this.qj && (this.wo && "/" != this.qj.charAt(0) && a.push("/"), a.push(Eu(this.qj, "/" == this.qj.charAt(0) ? Gu : Hu)));
  var b = "" + this.Bk;
  b && a.push("?", b);
  this.Tt && a.push("#", Eu(this.Tt, Iu));
  return this.Ui = a.join("")
};
z.Ca = function() {
  var a = this.Dn, b = this.ew, c = this.wo, d = this.pv, f = this.qj, g = this.Bk.Ca(), k = this.Tt, l = new vu(p, this.pk);
  a && wu(l, a);
  b && xu(l, b);
  c && yu(l, c);
  d && zu(l, d);
  f && Au(l, f);
  g && Bu(l, g);
  k && Cu(l, k);
  return l
};
z.nD = u("Dn");
function wu(a, b, c) {
  Ju(a);
  delete a.Ui;
  a.Dn = c ? b ? decodeURIComponent(b) : "" : b;
  a.Dn && (a.Dn = a.Dn.replace(/:$/, ""))
}
z.II = u("ew");
function xu(a, b, c) {
  Ju(a);
  delete a.Ui;
  a.ew = c ? b ? decodeURIComponent(b) : "" : b
}
z.qI = u("wo");
function yu(a, b, c) {
  Ju(a);
  delete a.Ui;
  a.wo = c ? b ? decodeURIComponent(b) : "" : b
}
z.jD = u("pv");
function zu(a, b) {
  Ju(a);
  delete a.Ui;
  b ? (b = Number(b), (isNaN(b) || 0 > b) && e(Error("Bad port number " + b)), a.pv = b) : a.pv = p
}
z.cu = u("qj");
function Au(a, b, c) {
  Ju(a);
  delete a.Ui;
  a.qj = c ? b ? decodeURIComponent(b) : "" : b
}
function Bu(a, b, c) {
  Ju(a);
  delete a.Ui;
  b instanceof Du ? (a.Bk = b, a.Bk.qs = a, a.Bk.Ur(a.pk)) : (c || (b = Eu(b, Ku)), a.Bk = new Du(b, a, a.pk))
}
z.zI = u("Bk");
z.sI = u("Tt");
function Cu(a, b, c) {
  Ju(a);
  delete a.Ui;
  a.Tt = c ? b ? decodeURIComponent(b) : "" : b
}
z.oN = function() {
  Ju(this);
  var a = A.U.AI();
  Ju(this);
  delete this.Ui;
  this.Bk.set("zx", a);
  return this
};
function Ju(a) {
  a.gN && e(Error("Tried to modify a read-only Uri"))
}
z.Ur = function(a) {
  this.pk = a;
  this.Bk && this.Bk.Ur(a);
  return this
};
var Lu = /^[a-zA-Z0-9\-_.!~*'():\/;?]*$/;
function Eu(a, b) {
  var c = p;
  A.Qc(a) && (c = a, Lu.test(c) || (c = encodeURI(a)), 0 <= c.search(b) && (c = c.replace(b, Mu)));
  return c
}
function Mu(a) {
  a = a.charCodeAt(0);
  return"%" + (a >> 4 & 15).toString(16) + (a & 15).toString(16)
}
var Fu = /[#\/\?@]/g, Hu = /[\#\?:]/g, Gu = /[\#\?]/g, Ku = /[\#\?@]/g, Iu = /#/g;
function Du(a, b, c) {
  this.jl = a || p;
  this.qs = b || p;
  this.pk = !!c
}
function Nu(a) {
  if(!a.wd && (a.wd = new Ca, a.zc = 0, a.jl)) {
    for(var b = a.jl.split("&"), c = 0;c < b.length;c++) {
      var d = b[c].indexOf("="), f = p, g = p;
      0 <= d ? (f = b[c].substring(0, d), g = b[c].substring(d + 1)) : f = b[c];
      f = A.U.PA(f);
      f = Ou(a, f);
      a.add(f, g ? A.U.PA(g) : "")
    }
  }
}
z = Du.prototype;
z.wd = p;
z.zc = p;
z.Sm = function() {
  Nu(this);
  return this.zc
};
z.add = function(a, b) {
  Nu(this);
  Pu(this);
  a = Ou(this, a);
  if(this.Yj(a)) {
    var c = this.wd.get(a);
    A.isArray(c) ? c.push(b) : this.wd.set(a, [c, b])
  }else {
    this.wd.set(a, b)
  }
  this.zc++;
  return this
};
z.remove = function(a) {
  Nu(this);
  a = Ou(this, a);
  if(this.wd.Yj(a)) {
    Pu(this);
    var b = this.wd.get(a);
    A.isArray(b) ? this.zc -= b.length : this.zc--;
    return this.wd.remove(a)
  }
  return q
};
z.clear = function() {
  Pu(this);
  this.wd && this.wd.clear();
  this.zc = 0
};
z.ri = function() {
  Nu(this);
  return 0 == this.zc
};
z.Yj = function(a) {
  Nu(this);
  a = Ou(this, a);
  return this.wd.Yj(a)
};
z.yq = function(a) {
  var b = this.Ef();
  return A.$.contains(b, a)
};
z.mg = function() {
  Nu(this);
  for(var a = this.wd.Ef(), b = this.wd.mg(), c = [], d = 0;d < b.length;d++) {
    var f = a[d];
    if(A.isArray(f)) {
      for(var g = 0;g < f.length;g++) {
        c.push(b[d])
      }
    }else {
      c.push(b[d])
    }
  }
  return c
};
z.Ef = function(a) {
  Nu(this);
  if(a) {
    if(a = Ou(this, a), this.Yj(a)) {
      var b = this.wd.get(a);
      if(A.isArray(b)) {
        return b
      }
      a = [];
      a.push(b)
    }else {
      a = []
    }
  }else {
    for(var b = this.wd.Ef(), a = [], c = 0;c < b.length;c++) {
      var d = b[c];
      A.isArray(d) ? A.$.extend(a, d) : a.push(d)
    }
  }
  return a
};
z.set = function(a, b) {
  Nu(this);
  Pu(this);
  a = Ou(this, a);
  if(this.Yj(a)) {
    var c = this.wd.get(a);
    A.isArray(c) ? this.zc -= c.length : this.zc--
  }
  this.wd.set(a, b);
  this.zc++;
  return this
};
z.get = function(a, b) {
  Nu(this);
  a = Ou(this, a);
  if(this.Yj(a)) {
    var c = this.wd.get(a);
    return A.isArray(c) ? c[0] : c
  }
  return b
};
z.toString = function() {
  if(this.jl) {
    return this.jl
  }
  if(!this.wd) {
    return""
  }
  for(var a = [], b = 0, c = this.wd.mg(), d = 0;d < c.length;d++) {
    var f = c[d], g = A.U.rs(f), f = this.wd.get(f);
    if(A.isArray(f)) {
      for(var k = 0;k < f.length;k++) {
        0 < b && a.push("&"), a.push(g), "" !== f[k] && a.push("=", A.U.rs(f[k])), b++
      }
    }else {
      0 < b && a.push("&"), a.push(g), "" !== f && a.push("=", A.U.rs(f)), b++
    }
  }
  return this.jl = a.join("")
};
function Pu(a) {
  delete a.dC;
  delete a.jl;
  a.qs && delete a.qs.Ui
}
z.Ca = function() {
  var a = new Du;
  this.dC && (a.dC = this.dC);
  this.jl && (a.jl = this.jl);
  this.wd && (a.wd = this.wd.Ca());
  return a
};
function Ou(a, b) {
  var c = "" + b;
  a.pk && (c = c.toLowerCase());
  return c
}
z.Ur = function(a) {
  a && !this.pk && (Nu(this), Pu(this), A.zd.forEach(this.wd, function(a, c) {
    var d = c.toLowerCase();
    c != d && (this.remove(c), this.add(d, a))
  }, this));
  this.pk = a
};
z.extend = function(a) {
  for(var b = 0;b < arguments.length;b++) {
    A.zd.forEach(arguments[b], function(a, b) {
      this.add(b, a)
    }, this)
  }
};
function Qu() {
  this.Tg = [];
  this.Kr = {}
}
z = Qu.prototype;
z.Tg = p;
z.Kr = p;
z.Ak = p;
z.sp = p;
function Ru(a, b) {
  if(Tu[b] != h ? 0 : a.Kr[b] == h) {
    var c = new Uu;
    c.p(b, q);
    Vu(a, c)
  }
}
function Wu(a, b) {
  if(Tu[b] != h ? 0 : a.Kr[b] == h) {
    var c = new Xu;
    c.p(b, j);
    Vu(a, c)
  }
}
function Vu(a, b) {
  a.Tg.push(b);
  a.Kr[b.cu()] = b;
  b.Ak = function() {
    a.uN.call(a, this)
  };
  b.sp = function(b) {
    a.tN.call(a, this, b)
  }
}
z.uN = function(a) {
  delete this.Kr[a.cu()];
  a = this.Tg.indexOf(a);
  -1 != a && this.Tg.splice(a, 1);
  this.Ak && 0 == this.Tg.length && this.Ak()
};
z.tN = function(a, b) {
  this.Ov();
  this.sp && this.sp(a, b)
};
z.load = function() {
  for(var a = 0;a < this.Tg.length;a++) {
    var b = this.Tg[a];
    b.Bl.send(b.qs, "GET", p, "anychart.com")
  }
};
z.Ov = function() {
  for(var a = 0;a < this.Tg.length;a++) {
    this.Tg[a].Ov()
  }
  this.Tg = [];
  this.Kr = {}
};
var Tu = {};
function Yu() {
  this.FB = j;
  this.Bl = new Ga;
  this.Bl.vG = j;
  A.I.nb(this.Bl, "complete", this.wN, p, this);
  this.sp && A.I.nb(this.Bl, ["error", "abort"], this.sp, p, this)
}
z = Yu.prototype;
z.Bl = p;
z.FB = j;
z.qj = p;
z.qs = p;
z.cu = u("qj");
z.p = function(a, b) {
  this.qj = a;
  this.FB = b;
  this.FB || (a = -1 != a.indexOf("?") ? a + "&" : a + "?", a += "XMLCallDate=" + (new Date).getTime().toString());
  this.qs = new vu(a)
};
z.Ov = function() {
  this.Bl.abort()
};
z.getData = function() {
  A.xa()
};
z.wN = function() {
  var a = this.getData();
  Tu[this.qj] = a;
  this.Ak && this.Ak.call(this, a)
};
z.Ak = p;
z.sp = p;
function Uu() {
  Yu.apply(this)
}
A.e(Uu, Yu);
Uu.prototype.getData = function() {
  return this.Bl.Bc ? this.Bl.Bc.responseText : ""
};
function Xu() {
  Yu.apply(this)
}
A.e(Xu, Yu);
Xu.prototype.getData = function() {
  var a;
  var b = this.Bl;
  try {
    a = b.Bc ? b.Bc.responseXML : p
  }catch(c) {
    a = p
  }
  return a
};
function Zu(a) {
  pa.call(this);
  this.N = a;
  this.JF()
}
A.e(Zu, pa);
Zu.prototype.N = p;
Zu.prototype.dj = u("N");
Zu.prototype.UA = p;
function $u(a) {
  Zu.call(this, a);
  av(this)
}
A.e($u, Zu);
z = $u.prototype;
z.JF = function() {
  this.UA = 2
};
z.F = p;
z.D = p;
z.fb = u("D");
z.dt = p;
z.k = p;
z.n = u("k");
z.Nk = p;
z.ey = u("Nk");
z.ce = function(a, b, c) {
  this.F = a;
  this.k = c.Ca();
  this.D = new W(a);
  this.dt = new W(a);
  this.Nk = new W(a);
  this.D.ia(this.dt);
  this.D.ia(this.Nk);
  this.kp || (this.kp = new W(a), this.lp = new ye, this.D.ia(this.kp));
  b.ia ? b.ia(this.D) : b.appendChild(this.D.G);
  ku(this, this.N.uG)
};
z.jf = p;
function av(a) {
  a.jf = new Qu;
  a.jf.sp = function(b, c) {
    a.VN.call(a, c.message)
  }
}
z.Wo = q;
z.refresh = function() {
  this.fd || (this.Wo = j, this.hb.ca().refresh())
};
z.ax = function() {
  !this.fd && this.hb && this.hb.ax()
};
z.Od = function(a, b) {
  this.fd || this.hb.ca().Od(a, b)
};
z.Lj = function(a, b, c) {
  this.fd || this.hb.ca().Lj(a, b, c)
};
z.Ck = function(a, b) {
  this.fd || this.hb.ca().Ck(a, b)
};
z.Ok = function(a, b, c) {
  this.fd || this.hb.ca().Ok(a, b, c)
};
z.Pk = function(a, b, c) {
  this.fd || this.hb.ca().Pk(a, b, c)
};
z.nk = function(a, b, c) {
  this.fd || this.hb.ca().nk(a, b, c)
};
z.uj = function(a, b, c) {
  this.fd || this.hb.ca().wK(a, b, c)
};
z.Mj = function(a) {
  this.fd || this.hb.ca().Mj(a)
};
z.Nj = function(a, b) {
  this.fd || this.hb.ca().Nj(a, b)
};
z.Dk = function(a) {
  this.fd || this.hb.ca().Dk(a)
};
z.Qk = function(a, b) {
  this.fd || this.hb.ca().Qk(a, b)
};
z.Jk = function(a, b) {
  this.fd || this.hb.ca().Jk(a, b)
};
z.ok = function(a, b) {
  this.fd || this.hb.ca().ok(a, b)
};
z.mk = function(a, b) {
  this.fd || this.hb.ca().mk(a, b)
};
z.Ik = function(a, b) {
  this.fd || this.hb.ca().Ik(a, b)
};
z.tj = p;
z.Yu = j;
z.mw = p;
z.Hi = function(a, b, c) {
  this.tj = b;
  this.Yu = c != p && c != h ? c : j;
  this.mw = a;
  this.jf.Ov();
  Ru(this.jf, a);
  if(0 == this.jf.Tg.length) {
    this.Oh(Tu[a])
  }else {
    var d = this;
    bv(this);
    this.jf.Ak = function() {
      d.AN.call(d)
    };
    this.jf.load()
  }
};
z.AN = function() {
  this.Oh(Tu[this.mw])
};
z.Du = p;
z.Jn = function(a) {
  this.Du = a;
  this.jf.Ov();
  Ru(this.jf, a);
  if(0 == this.jf.Tg.length) {
    this.xj(Tu[a])
  }else {
    var b = this;
    bv(this);
    this.jf.Ak = function() {
      b.vN.call(b)
    };
    this.jf.load()
  }
};
z.vN = function() {
  this.xj(Tu[this.Du])
};
z.Hc = p;
z.Oh = function(a) {
  this.Gk(Jb(a))
};
z.xj = function(a) {
  this.Gk(eval("(" + a + ")"))
};
z.Gk = function(a) {
  this.tj && (a = lc(a, this.tj));
  this.Hc = a;
  ku(this, this.N.SI);
  try {
    this.qv()
  }catch(b) {
    cv(this.N, b), ku(this, b.toString())
  }
};
z.ft = p;
z.qv = function() {
  if(!this.Hc) {
    return this.as(), q
  }
  if(this.ft) {
    var a = 1 == this.ft ? "chart" : "gauge";
    if(!C(this.Hc, a + "s")) {
      var b = this.Hc, c = {}, d = {};
      H(c, a + "s", d);
      H(d, a, b);
      this.Hc = c
    }
  }
  return j
};
function dv(a) {
  a.oH(a.Hc);
  0 == a.jf.Tg.length ? a.aK() : (ku(a, a.N.DJ), a.jf.Ak = function() {
    a.aK.call(a)
  }, a.jf.load())
}
z.aK = function() {
  ev(this);
  var a = this.k.Ca();
  this.D.qb(a.x, a.y);
  this.hb.ce(this.dt, a);
  this.hb.aJ() ? this.hb.zp() : 1 == this.UA && uu(this.N.hb);
  this.MH();
  this.hb.qa();
  this.LH()
};
z.Rp = p;
z.kp = p;
z.lp = p;
z.qx = p;
function ku(a, b, c, d) {
  c !== j && ze(a.lp);
  ev(a);
  a.qx = b;
  a.lp.g({format:b, rotation:d});
  a.lp.uc(a.F, b);
  a.kp.ia(a.lp.ic(a.F));
  b = a.lp.n();
  a.kp.qb(a.k.x + (a.k.width - b.width) / 2, a.k.y + (a.k.height - b.height) / 2)
}
function ev(a) {
  a.kp && a.kp.clear();
  a.qx = p
}
z.as = function() {
  var a = this.N.NE, b = a && C(a, "text") ? J(a, "text") : this.N.VJ;
  this.lp.g(this.N.NE);
  ku(this, b, j, F(a, "rotation"))
};
function bv(a, b) {
  b || (b = a.N.CJ);
  ku(a, b)
}
z.VN = function(a) {
  ku(this, "Error: " + a)
};
z.hb = p;
z.clear = function() {
  this.hb = p;
  this.D != p && this.D.clear()
};
z.Wa = function(a) {
  this.k = a;
  this.qx && ku(this, this.qx, j);
  this.hb && this.hb.Wa(a)
};
function ju(a, b) {
  this.bC = b;
  this.Rp = a.Rp;
  $u.call(this, a)
}
A.e(ju, $u);
z = ju.prototype;
z.JF = function() {
  this.UA = 1
};
z.bC = p;
z.DK = t("D");
z.fb = u("D");
z.ce = function(a, b, c) {
  ju.f.ce.call(this, a, b, c)
};
z.qv = function() {
  ju.f.qv.call(this) && dv(this)
};
z.oH = function() {
  this.fd = q;
  this.hb = new au(this.N, this);
  this.hb.sl(this.F);
  lf(this, this.hb, this.Hc)
};
z.as = function() {
  this.hb.DF();
  ju.f.as.call(this)
};
z.MH = function() {
  this.Wo || cv(this.N, new Ge("dashboardViewRefresh", this.bC.getName()))
};
z.LH = function() {
  this.Fx()
};
z.Fx = function() {
  var a = this.N, b = this.bC.getName();
  this.Wo ? cv(a, new Ge("dashboardViewRefresh", b)) : cv(a, new Ge("dashboardViewDraw", b));
  this.Wo = q;
  a.wC++
};
function fv() {
  $u.call(this, this);
  this.Rp = new tf;
  this.Oa = new Ad(20);
  this.sq = {};
  this.Xw = {};
  this.Yu = q
}
A.e(fv, $u);
z = fv.prototype;
z.JF = function() {
  this.UA = 0
};
z.ce = function(a, b, c) {
  fv.f.ce.call(this, a, b, c)
};
z.FA = p;
function gv(a) {
  a.FA = p;
  if(C(a.Hc, "templates")) {
    var b = F(a.Hc, "templates");
    C(b, "path") && (a.FA = J(b, "path"), Wu(a.jf, a.FA));
    uf(a.Rp, I(b, "template"))
  }
  0 == a.jf.Tg.length ? dv(a) : (ku(a, a.N.EJ), a.jf.Ak = function() {
    a.zN.call(a)
  }, a.jf.load())
}
z.zN = function() {
  var a = Kb(Tu[this.FA]);
  uf(this.Rp, I(a, "template"));
  dv(this)
};
z.Gk = function(a) {
  C(a, "anychart") && (a = F(a, "anychart"));
  if(this.Yu) {
    var b = this.F;
    kd(b.hl);
    b.HD.clear();
    b.cr.clear();
    b.FD.clear();
    b.hk.clear();
    this.sq = {};
    this.Xw = {};
    this.Rp.clear();
    this.dt.clear();
    this.Nk.clear();
    ev(this);
    if(this.fd) {
      for(var c in this.kw) {
        this.kw[c].ax()
      }
    }else {
      this.ax()
    }
  }
  fv.f.Gk.call(this, a);
  this.Yu = j
};
z.Oa = p;
z.iq = p;
z.NE = p;
z.qv = function() {
  if(fv.f.qv.call(this)) {
    if(this.iq = C(this.Hc, "margin") && !this.iq ? F(this.Hc, "margin") : $b(this.iq, F(this.Hc, "margin"), "margin")) {
      H(this.Hc, "margin", this.iq), this.Oa.g(this.iq)
    }
    this.sx = new mh;
    this.XJ = new oh;
    if(C(this.Hc, "settings")) {
      var a = F(this.Hc, "settings");
      if(C(a, "locale")) {
        var b = F(a, "locale");
        C(b, "date_time_format") && this.sx.g(F(b, "date_time_format"));
        C(b, "number_format") && this.XJ.g(F(b, "number_format"))
      }
      C(a, "no_data") && (a = F(a, "no_data"), C(a, "label") && (this.NE = F(a, "label")))
    }
    this.sx.p();
    C(this.Hc, "gauges") && (hv(this, I(F(this.Hc, "gauges"), "gauge"), 2), this.ft = 2);
    C(this.Hc, "charts") && (hv(this, I(F(this.Hc, "charts"), "chart"), 1), this.ft = 1);
    gv(this)
  }
};
z.fd = p;
z.oH = function() {
  C(this.Hc, "dashboard") && C(F(this.Hc, "dashboard"), "view") ? (this.fd = j, this.wC = 0, this.hb = new tu(this), this.hb.Wr(this.Oa), this.hb.sl(this.F), this.hb.g(F(F(this.Hc, "dashboard"), "view"))) : (this.fd = q, this.hb = new au(this, this), this.hb.Wr(this.Oa), this.hb.sl(this.F), lf(this, this.hb, this.Hc))
};
z.sq = p;
z.Xw = p;
function hv(a, b, c) {
  for(var d = b.length, f = 0;f < d;f++) {
    var g = b[f];
    if(g && C(g, "name")) {
      var k = a, l = F(g, "name"), n = c;
      k.sq[l] = g;
      k.Xw[l] = n
    }
  }
}
z.MH = function() {
  this.Wo || cv(this, new Fe("anychartRender"))
};
z.LH = function() {
  this.hb && (this.fd ? (0 == this.hb.bm.length || this.hb.bm.length <= this.wC) && this.Fx() : this.Fx())
};
z.Fx = function() {
  this.Wo ? cv(this, new Fe("anychartRefresh")) : cv(this, new Fe("anychartDraw"));
  this.Wo = q
};
z.wC = p;
function cv(a, b) {
  A.I.dispatchEvent(a, b)
}
z.sx = p;
z.Gd = u("sx");
z.XJ = p;
z.mb = function() {
  return this.hb.mb()
};
z.Np = function(a, b) {
  if(this.fd) {
    var c = Jn(this, a);
    c && c.Hi(b)
  }
};
function Jn(a, b) {
  if(!a.fd) {
    return p
  }
  var c = a.hb.kw[b];
  return!c || !c.hb || !c.hb.ca() ? p : c
}
z.Gs = function(a, b, c) {
  (a = Jn(this, a)) && a.Ik(b, c)
};
z.xs = function(a, b) {
  var c = Jn(this, a);
  c && c.Mj(b)
};
z.Es = function(a, b) {
  var c = Jn(this, a);
  c && c.Dk(b)
};
z.ys = function(a, b, c) {
  (a = Jn(this, a)) && a.Nj(b, c)
};
z.Js = function(a, b, c) {
  (a = Jn(this, a)) && a.Qk(b, c)
};
z.Hs = function(a, b, c) {
  (a = Jn(this, a)) && a.Jk(b, c)
};
z.vs = function(a, b, c) {
  (a = Jn(this, a)) && a.Od(b, c)
};
z.ws = function(a, b, c, d) {
  (a = Jn(this, a)) && a.Lj(b, c, d)
};
z.Ds = function(a, b, c) {
  (a = Jn(this, a)) && a.Ck(b, c)
};
z.Is = function(a, b, c, d) {
  (a = Jn(this, a)) && a.Ok(b, c, d)
};
z.Cs = function(a) {
  (a = Jn(this, a)) && a.refresh()
};
z.Bs = function(a, b, c) {
  (a = Jn(this, a)) && a.ok(b, c)
};
z.As = function(a, b, c, d) {
  var f = Jn(this, a);
  f && f.nk(a, b, c, d)
};
z.zs = function(a, b, c) {
  (a = Jn(this, a)) && a.mk(b, c)
};
z.Fs = function(a, b, c, d) {
  (a = Jn(this, a)) && a.uj(b, c, d)
};
z.ps = function(a, b, c, d) {
  (a = Jn(this, a)) && a.Pk(b, c, d)
};
z.os = function(a, b) {
  if(b instanceof Document) {
    b = Kb(b)
  }else {
    if("string" == typeof b && "<" == b.charAt(0)) {
      b = Jb(b)
    }else {
      if(!(b instanceof Object)) {
        return
      }
    }
  }
  for(var c = b, d = 0, f = this.Hc, g = a.length, k;d < g;) {
    k = a.charAt(d);
    var l, n;
    switch(k) {
      case "/":
        d++;
        break;
      case "[":
        k = a.substring(d, a.indexOf("]", d));
        d += k.length + 1;
        if(!A.isArray(f)) {
          continue
        }
        k = k.substring(1);
        f = f[k];
        break;
      case "(":
        k = a.substring(d, a.indexOf(")", d));
        d += k.length + 1;
        k = k.substring(1);
        l = k.split("=");
        k = l[0];
        var m = l[1];
        l = p;
        for(n in f) {
          f[n][k] != h && f[n][k] == m && (l == p ? l = f[n] : (A.isArray(l) || (l = [l]), l.push(f[n])))
        }
        f = l;
        break;
      default:
        if("." == k && d < a.length - 1 && "." == a.charAt(d + 1)) {
          l = iv(a, d);
          d = l[1];
          f = l = f.nQ(l[0].substr(2));
          break
        }
        l = iv(a, d);
        d = l[1];
        k = l[0];
        f = A.isArray(f) ? f[0][k] : f[k]
    }
  }
  f != p && (c = Uk(c), ac(f, c, f), c.text && (f.text = c.text), c.format && (f.text = c.format));
  this.Gk(this.Hc)
};
function iv(a, b) {
  for(var c = a.charAt(b), d = "", f = j, g = a.length;b < g && f;) {
    d += c, b++, c = a.charAt(b), f = a.charAt(b), f = "[" == f || "]" == f || "(" == f || ")" == f || "/" == f ? q : !("." == f && b < a.length - 1 && "." == a.charAt(b + 1))
  }
  return[d, b]
}
z.XA = p;
z.uG = p;
z.SI = p;
z.YH = q;
z.CJ = p;
z.DJ = p;
z.EJ = p;
z.VJ = p;
z.DF = t("VJ");
function jv() {
}
A.Gg("anychart.render.svg.SVGRenderer", jv);
z = jv.prototype;
z.F = p;
z.N = p;
z.oa = p;
z.Zs = x(j);
z.Ha = function(a) {
  if(A.userAgent.Jj) {
    var b = document.getElementsByTagName("head")[0], c = document.createElement("meta");
    c.name = "anychart.com";
    c.content = "IE=Edge";
    c["http-equiv"] = "X-UA-Compatible";
    b.appendChild(c)
  }
  this.ko() && (this.oa = a, this.F = new $c, this.N = new fv, this.N.uG = this.oa.messages.waitingForData, this.N.DF(this.oa.messages.noData), this.N.EJ = this.oa.messages.loadingTemplates, this.N.DJ = this.oa.messages.loadingResources, this.N.CJ = this.oa.messages.loadingConfig, this.N.SI = this.oa.messages.init, this.oa.watermark && (this.N.XA = this.oa.watermark), A.I.nb(this.N, "anychartDraw", this.TH, q, this), A.I.nb(this.N, "anychartRender", this.rK, q, this), A.I.nb(this.N, "anychartError", 
  this.ZH, q, this), A.I.nb(this.N, "anychartRefresh", this.mK, q, this), A.I.nb(this.N, "dashboardViewDraw", this.UH, q, this), A.I.nb(this.N, "dashboardViewRender", this.sK, q, this), A.I.nb(this.N, "dashboardViewRefresh", this.nK, q, this), A.I.nb(this.F.Ic(), ka.qw, this.Mz, q, this), A.I.nb(window, ka.qw, this.Mz, q, this), A.I.nb(this.F.Ic(), ka.Xn, this.MJ, q, this), A.I.nb(this.F.Ic(), ka.fm, this.LJ, q, this), A.I.nb(this.F.Ic(), ka.Ns, this.OJ, q, this), A.I.nb(this.N, "pointClick", this.Yg, 
  q, this), A.I.nb(this.N, "pointMouseOver", this.Yg, q, this), A.I.nb(this.N, "pointMouseOut", this.Yg, q, this), A.I.nb(this.N, "pointMouseDown", this.Yg, q, this), A.I.nb(this.N, "pointMouseUp", this.Yg, q, this), A.I.nb(this.N, "pointSelect", this.Yg, q, this), A.I.nb(this.N, "pointDeselect", this.Yg, q, this), A.I.nb(this.N, "multiplePointsSelect", this.RJ, q, this), this.oa.visible || this.ku())
};
z.ZJ = s();
z.$J = s();
z.Ic = function(a) {
  var b = new XMLSerializer, c = this.F.Ic(), c = b.serializeToString(c);
  if(a) {
    var a = "", d, f, g, k, l, n, b = 0, c = c.replace(/\r\n/g, "\n");
    f = "";
    for(g = 0;g < c.length;g++) {
      k = c.charCodeAt(g), 128 > k ? f += String.fromCharCode(k) : (127 < k && 2048 > k ? f += String.fromCharCode(k >> 6 | 192) : (f += String.fromCharCode(k >> 12 | 224), f += String.fromCharCode(k >> 6 & 63 | 128)), f += String.fromCharCode(k & 63 | 128))
    }
    for(c = f;b < c.length;) {
      d = c.charCodeAt(b++), f = c.charCodeAt(b++), g = c.charCodeAt(b++), k = d >> 2, d = (d & 3) << 4 | f >> 4, l = (f & 15) << 2 | g >> 6, n = g & 63, isNaN(f) ? l = n = 64 : isNaN(g) && (n = 64), a = a + wc().charAt(k) + wc().charAt(d) + wc().charAt(l) + wc().charAt(n)
    }
  }else {
    a = c
  }
  return a
};
z.show = function() {
  this.F.Ic().setAttribute("visibility", "visible")
};
z.ku = function() {
  this.F.Ic().setAttribute("visibility", "hidden")
};
function kv() {
  window.console && window.console.log("Feature not supported yet")
}
z.ko = function() {
  return window.SVGAngle != h
};
z.write = function(a) {
  if(this.ko()) {
    var b = this.F.Ic();
    this.N.YH = this.oa.enabledChartMouseEvents;
    b.setAttribute("width", this.oa.width);
    b.setAttribute("height", this.oa.height);
    a.appendChild(b);
    document.body.appendChild(this.F.Jr);
    this.k = this.lD();
    this.N.ce(this.F, this.F.Ic(), this.k);
    return j
  }
};
z.lD = function() {
  var a, b, c = this.F.Ic();
  a = c.clientWidth;
  b = c.clientHeight;
  a || (a = /^([0-9]+\%?)/.exec(this.oa.width)[1], a = Mb(a) ? c.parentNode.clientWidth * Nb(a) : a);
  b || (b = /^([0-9]+\%?)/.exec(this.oa.height)[1], b = Mb(b) ? c.parentNode.clientHeight * Nb(b) : b);
  return new P(0, 0, a, b)
};
z.OA = s();
z.Mp = function(a, b) {
  this.F.Ic().setAttribute("width", a);
  this.F.Ic().setAttribute("height", b);
  this.Wa(a, b)
};
z.remove = function() {
  this.F.Ic().parentNode && (this.F.Ic().parentNode.removeChild(this.F.Ic()), A.I.ed(this.N, "anychartDraw", this.TH, q, this), A.I.ed(this.N, "anychartRender", this.rK, q, this), A.I.ed(this.N, "anychartError", this.ZH, q, this), A.I.ed(this.N, "anychartRefresh", this.mK, q, this), A.I.ed(this.N, "dashboardViewDraw", this.UH, q, this), A.I.ed(this.N, "dashboardViewRender", this.sK, q, this), A.I.ed(this.N, "dashboardViewRefresh", this.nK, q, this), A.I.ed(this.F.Ic(), ka.qw, this.Mz, q, this), A.I.ed(window, 
  ka.qw, this.Mz, q, this), A.I.ed(this.F.Ic(), ka.Xn, this.MJ, q, this), A.I.ed(this.F.Ic(), ka.fm, this.LJ, q, this), A.I.ed(this.F.Ic(), ka.Ns, this.OJ, q, this), A.I.ed(this.N, "pointClick", this.Yg, q, this), A.I.ed(this.N, "pointMouseOver", this.Yg, q, this), A.I.ed(this.N, "pointMouseOut", this.Yg, q, this), A.I.ed(this.N, "pointMouseDown", this.Yg, q, this), A.I.ed(this.N, "pointMouseUp", this.Yg, q, this), A.I.ed(this.N, "pointSelect", this.Yg, q, this), A.I.ed(this.N, "pointDeselect", this.Yg, 
  q, this), A.I.ed(this.N, "multiplePointsSelect", this.RJ, q, this))
};
z.clear = function() {
  var a = this.N;
  a.fd || a.hb.ca().clear()
};
z.refresh = function() {
  this.N.refresh()
};
z.os = function(a, b) {
  this.N.os(a, b)
};
z.cA = function(a) {
  bv(this.N, a)
};
z.Ox = function() {
  return this.N.mb()
};
z.TH = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"draw"})
};
z.rK = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"render"})
};
z.mK = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"refresh"})
};
z.ZH = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"error", errorCode:a.errorCode, errorMessage:a.errorMessage})
};
z.UH = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"drawView", view:a.QA})
};
z.sK = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"renderView", view:a.QA})
};
z.nK = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"refreshView", view:a.QA})
};
z.Mz = function() {
  var a = this.lD();
  if(this.k.width != a.width || this.k.height != a.height) {
    this.k = a.Ca(), this.Wa(this.k.width, this.k.height), this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"resize"})
  }
};
z.MJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseMove", mouseX:a.clientX, mouseY:a.clientY})
};
z.LJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseDown", mouseX:a.clientX, mouseY:a.clientY})
};
z.OJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseUp", mouseX:a.clientX, mouseY:a.clientY})
};
z.Yg = function(a) {
  if(a.j) {
    var b = bo(a.j)
  }
  this.oa.dispatchEvent({type:a.Jb(), data:b, mouseX:a.PJ, mouseY:a.QJ})
};
z.RJ = function(a) {
  if(a && 0 != a.Fe()) {
    for(var b = {points:[]}, c = a.Fe(), d = 0;d < c;d++) {
      b.points.push(bo(a.Ga(d)))
    }
    this.oa.dispatchEvent({type:a.Jb(), data:b})
  }
};
z.Wa = function(a, b) {
  if(this.N && a && !(0 > a || !b || 0 > b || !this.N)) {
    this.k.width = a, this.k.height = b, this.N.Wa(this.k)
  }
};
z.Hi = function(a) {
  this.N.Hi(a)
};
z.Oh = function(a) {
  this.N.Oh(a)
};
z.xj = function(a) {
  this.N.Gk(a)
};
z.Jn = function(a) {
  this.N.Jn(a)
};
z.Gv = function(a) {
  this.N.Oh(a)
};
z.kA = function(a) {
  this.N.Hi(a)
};
z.Np = function(a, b) {
  this.N.Np(a, b, p)
};
z.gA = function(a, b) {
  var c = this.N;
  c.fd && (c = Jn(c, a)) && c.Oh(b)
};
z.Od = function(a, b) {
  this.N.Od(a, b)
};
z.Lj = function(a, b, c) {
  this.N.Lj(a, b, c)
};
z.Ck = function(a, b) {
  this.N.Ck(a, b)
};
z.Ok = function(a, b, c) {
  this.N.Ok(a, b, c)
};
z.Pk = function() {
  kv()
};
z.nk = function(a, b, c) {
  this.N.nk(a, b, c)
};
z.uj = function(a, b, c) {
  this.N.uj(a, b, c)
};
z.NA = function() {
  kv()
};
z.vA = function() {
  kv()
};
z.Mj = function(a) {
  this.N.Mj(a)
};
z.Nj = function(a, b) {
  this.N.Nj(a, b)
};
z.Dk = function(a) {
  this.N.Dk(a)
};
z.Qk = function(a, b) {
  this.N.Qk(a, b)
};
z.Jk = function(a, b) {
  this.N.Jk(a, b)
};
z.ok = function(a, b) {
  this.N.ok(a, b)
};
z.mk = function(a, b) {
  this.N.mk(a, b)
};
z.rA = function() {
  kv()
};
z.Ik = function(a, b) {
  this.N.Ik(a, b)
};
z.Gs = function(a, b, c) {
  this.N.Gs(a, b, c)
};
z.xs = function(a, b) {
  this.N.xs(a, b)
};
z.Es = function(a, b) {
  this.N.Es(a, b)
};
z.ys = function(a, b, c) {
  this.N.ys(a, b, c)
};
z.Js = function(a, b, c) {
  this.N.Js(a, b, c)
};
z.Hs = function(a, b, c) {
  this.N.Hs(a, b, c)
};
z.vs = function(a, b, c) {
  this.N.vs(a, b, c)
};
z.ws = function(a, b, c, d) {
  this.N.ws(a, b, c, d)
};
z.Ds = function(a, b, c) {
  this.N.Ds(a, b, c)
};
z.Is = function(a, b, c, d) {
  this.N.Is(a, b, c, d)
};
z.VA = function(a) {
  (a = Jn(this.N, a)) && (a.fd || a.hb.ca().clear())
};
z.Cs = function(a) {
  this.N.Cs(a)
};
z.Bs = function(a, b, c) {
  this.N.Bs(a, b, c)
};
z.As = function(a, b, c, d) {
  this.N.As(a, b, c, d)
};
z.zs = function(a, b, c) {
  this.N.zs(a, b, c)
};
z.Fs = function(a, b, c, d) {
  this.N.Fs(a, b, c, d)
};
z.ps = function(a, b, c, d) {
  this.N.ps(a, b, c, d)
};
z.Zq = function() {
  kv()
};
z.ay = function() {
  kv()
};
z.Hz = function() {
  kv()
};
z.Uz = function() {
  kv()
};
z.Vz = function() {
  kv()
};
z.Wz = function() {
  kv()
};
z.Xz = function() {
  kv()
};
z.scrollTo = function() {
  kv()
};
z.SA = function() {
  kv()
};
z.TA = function() {
  kv()
};
z.hy = function() {
  kv()
};
z.iy = function() {
  kv()
};
z.fy = function() {
  kv()
};
z.gy = function() {
  kv()
};
z.lA = function() {
  kv()
};
z.mA = function() {
  kv()
};
z.nA = function() {
  kv()
};
z.hA = function() {
  kv()
};
z.iA = function() {
  kv()
};
z.jA = function() {
  kv()
};
z.Aw = function() {
  kv()
};
})()
