//
//  PagesPopOverViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/16/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardViewController.h"

@class  DashboardViewController;

@interface PagesPopOverViewController : UITableViewController{
    NSArray *dashboards;
    DashboardViewController *delegate;
    NSIndexPath *selectedIdx;
}

@property (nonatomic, assign) NSArray *dashboards;
@property (nonatomic, assign) DashboardViewController *delegate;
@property (nonatomic, retain) NSIndexPath *selectedIdx;

@end
