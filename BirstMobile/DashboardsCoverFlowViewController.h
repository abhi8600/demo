//
//  DashboardsCoverFlowViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/16/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dashboard.h"
#import "Page.h"
#import "TKCoverflowView.h"
#import "TKCoverflowCoverView.h"
#import "DashboardViewController.h"

@class DashboardViewController;

@interface DashboardsCoverFlowViewController : UIViewController<TKCoverflowViewDelegate,TKCoverflowViewDataSource>{
    NSArray *dashboards;
    TKCoverflowView *coverFlow;
    UILabel *description;
    DashboardViewController *delegate;
}

- (id) initWithFrame:(CGRect)frame dashboards:(NSArray*)dashs;

@property (nonatomic, assign) NSArray *dashboards;
@property (nonatomic, retain) TKCoverflowView *coverFlow;
@property (nonatomic, retain) UILabel *description;
@property (nonatomic, assign) DashboardViewController *delegate;
@end
