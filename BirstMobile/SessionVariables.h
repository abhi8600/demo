//
//  SessionVariables.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/18/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SessionVariables : NSObject {
    NSMutableDictionary *sessionVars;
}

@property (nonatomic, retain) NSMutableDictionary *sessionVars;

+ (SessionVariables*) instance;

@end
