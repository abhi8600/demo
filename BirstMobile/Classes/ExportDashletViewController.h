//
//  ExportDashletViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/14/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaximizedDashletViewController.h"

@class MaximizedDashletViewController;

@interface ExportDashletViewController : UITableViewController{
    MaximizedDashletViewController *maxVC;
}

@property (nonatomic, assign) MaximizedDashletViewController *maxVC;
@end
