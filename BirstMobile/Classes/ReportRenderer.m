//
//  ReportRenderer.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/15/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "ReportRenderer.h"
#import "TBXML.h"
#import "Util.h"
#import "ColumnTextCell.h"
#import "ChartView.h"
#import "Settings.h"
#import "Log.h"
#import "Notification.h"
#import "PromptGroupButton.h"
#import "PromptGroupCheckBox.h"

#define CELLTAG @"net.sf.jasperreports.crosstab.cell.type"

@implementation ReportRenderer

@synthesize data, tbxml, dashletCtrl, pageNo, numPages, childViews, numCharts, disableDrilling;
@synthesize editableTextBoxes, checkBoxes;

/*
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		NSLog(@"init with frame");
        // Initialization code.
		self.opaque = YES;
		self.clearsContextBeforeDrawing = YES;
    }
    return self;
}

- (id) init {
	self = [super init];
	if (self) {
		NSLog(@"init");
		// Initialization code.
		self.opaque = YES;
		self.clearsContextBeforeDrawing = YES;
	}
	return self;
}
*/

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tapRecognizer];
        [tapRecognizer release];

        UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)];
        [self addGestureRecognizer:pinchRecognizer];
        [pinchRecognizer release];
        
        self.childViews = [[[NSMutableArray alloc]init]autorelease];
        self.editableTextBoxes = [[[NSMutableDictionary alloc]init]autorelease];
        self.checkBoxes = [[[NSMutableDictionary alloc]init]autorelease];
    }
    return self;
}

-(void)handlePinch:(UIPinchGestureRecognizer *)pinch{
    if(pinch.state == UIGestureRecognizerStateBegan){
        pinchScaleStart = pinch.scale;
    }
    
    if(pinch.state == UIGestureRecognizerStateEnded){
        if(pinch.scale > pinchScaleStart){
            NSLog(@"handlePinch(), maximize dashlet");
            [[NSNotificationCenter defaultCenter] postNotificationName:MAXIMIZE_DASHLET object:self];
        }else{
            NSLog(@"handlePinch() minimize dashlet");
            [[NSNotificationCenter defaultCenter] postNotificationName:MINIMIZE_DASHLET object:self];
        }
    }
}

/*
- (id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tapRecognizer];
        [tapRecognizer release];
        
        childViews = [[NSMutableArray alloc]init];
    }
    return self;
}*/

-(void)handleTap:(UITapGestureRecognizer*)tapEvent{
    if(disableDrilling){
        return;
    }
    
    CGPoint p = [tapEvent locationInView:self];
    UIView *subView = [self hitTest:p withEvent:nil];
    if((subView != self) &&[subView respondsToSelector:@selector(handleTap:)]){
        [subView performSelector:@selector(handleTap:) withObject:tapEvent];
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
	if (self.data != nil) {
		[Log debug:@"Rendering.."];
        
        if(!self.tbxml){
            TBXML *t = [[TBXML alloc] initWithXMLString:self.data];
            self.tbxml = t;
            [t release];
        }
        
        if ([self.childViews count] > 0){
            for (UIView *cv in self.childViews) {
                [cv removeFromSuperview];
            }
            [self.childViews removeAllObjects];
        }
        
		TBXMLElement *root = self.tbxml.rootXMLElement;
		TBXMLElement *report = [Util traverseFindElement:root target:@"report"];
        
		if (report != nil) {
            reportDimension = CGSizeMake(0, 0);
			[self renderChildren:report xOrigin:0 yOrigin:0 pivotTag:None];
            
            CGSize size = self.contentSize;
            float offset = 5;
            if(reportDimension.width > size.width){
                size = CGSizeMake(reportDimension.width + offset, size.height);
            }
            if(reportDimension.height > size.height){
                size = CGSizeMake(size.width, reportDimension.height + offset);
            }
            self.contentSize = size;
		}
        
		[self maximizeSingleChart:rect];
        
        if(self.numCharts == 0){
            [[NSNotificationCenter defaultCenter] postNotificationName:RENDERED object:nil];
        }
	}
}

- (void) maximizeSingleChart:(CGRect)rect{
    //Is it a single chart subview, if so, blow it up
    //subview count == 3 (2 scrollers + 1 chart), index #2 is chart
    if ([[self subviews] count] <= 3) {
        self.contentSize = rect.size;
        for (UIView *v in [self subviews]) {
            if ([v isKindOfClass:[UIWebViewWrapper class]]) {
                UIWebViewWrapper *cv = (UIWebViewWrapper*) v;
                [cv resize:rect];
                return;
            }
        }
    } 
}

- (void) doneJS:(UIWebViewWrapper *)sender{
    renderedJSCount++;
    
    if(renderedJSCount >= self.numCharts){
        NSLog(@"All charts rendered, rendered count %d number charts %d", renderedJSCount, self.numCharts);
        [[NSNotificationCenter defaultCenter] postNotificationName:RENDERED object:nil];
    }
}

- (void) disableAllDrilling:(BOOL)disableDrill {
    for (UIView *v in [self subviews]) {
        if ([v isKindOfClass:[UIWebViewWrapper class]]) {
            UIWebViewWrapper *cv = (UIWebViewWrapper*) v;
            cv.disableDrilling = disableDrill;
        }
    }
    self.disableDrilling = disableDrill;
}

- (void) parse:(NSString *)xml{
    
    //Remove all subviews
    [self.childViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.childViews removeAllObjects];
    
    //temp - fix drilldown xml formatting errors
    xml = [self tempFixDrillAttributes:xml];
	self.data = xml;
    
    TBXML *t = [[TBXML alloc] initWithXMLString:self.data];
    self.tbxml = t;
    [t release];
    
    TBXMLElement *root = self.tbxml.rootXMLElement;
    
    TBXMLElement *dataReport = [Util traverseFindElement:root target:@"data"];
    TBXMLElement *report = [Util traverseFindElement:dataReport target:@"report"];
    
    if (report != nil) {
        NSString *c = [TBXML valueOfAttributeNamed:@"color" forElement:report];
        if(c != nil){
            NSString *bgColor = [NSString stringWithFormat:@"0x%@", c];
            self.backgroundColor = [Util colorWithHexString:bgColor];
        }else{
            self.backgroundColor = [UIColor clearColor];
        }
        NSString *nHeight = [TBXML valueOfAttributeNamed:@"height" forElement:report];
        NSString *nWidth = [TBXML valueOfAttributeNamed:@"width" forElement:report];
        float height = [nHeight floatValue];
        float width = [nWidth floatValue];
        if(height > 0 && width > 0){
            self.contentSize = CGSizeMake(width, height);
        }
        
        //Find number of pages.
        TBXMLElement *sibling = dataReport->nextSibling;
        if(sibling){
            do{
                NSString *tagName = [TBXML elementName:sibling];
                NSString *tagValue = [TBXML textForElement:sibling];
                if ([@"numberOfPages" isEqualToString:tagName]) {
                    numPages = [tagValue intValue];
                }else if([@"page" isEqualToString:tagName]){
                    pageNo = [tagValue intValue];
                    [self renderPagingSection];
                }
            }while((sibling = sibling->nextSibling));
        }
    }
	[self setNeedsDisplay];
}

- (void) renderPagingSection{
    if(numPages > 1){
        [self.dashletCtrl createPagingSection];
        
        int pageNumber = pageNo + 1;
        self.dashletCtrl.pagingPageNo.text = [NSString stringWithFormat:@"%d / %d", pageNumber, numPages];
        
    }
}

/*
 <text x="16" y="16" width="163" height="25" color="#0000FF" backcolor="#FFFFFF" bold="false" italic="false" face="Arial" size="12" value="1995" alignment="left" link="javascript:AdhocContent.DrillThru('/private/sfoo@birst.com/1900', 'Drill Down', 'Time.Year=1995','&lt;DrillTo>&lt;FromDimension>Time&lt;/FromDimension>&lt;FromColumn>Year&lt;/FromColumn>&lt;DrillColumns>&lt;DrillColumn>&lt;Dimension>Time&lt;/Dimension>&lt;Column>Year/Quarter&lt;/Column>&lt;/DrillColumn>&lt;/DrillColumns>&lt;/DrillTo>')">
 */
- (NSString*) tempFixDrillAttributes:(NSString *)xml{
    NSArray *tags = [NSArray arrayWithObjects:@"DrillTo", @"FromDimension", @"FromColumn", @"DrillColumn", @"DrillColumns", @"Dimension", @"Column", nil];
    
    for (int i = 0; i < [tags count]; i++) {
        NSString *tag = [tags objectAtIndex:i];
        
        NSString *ltgt = [NSString stringWithFormat:@"&lt;%@>", tag];
        NSString *ltgtNot = [NSString stringWithFormat:@"&lt;%@&gt;", tag];

        NSString *ltslashgt = [NSString stringWithFormat:@"&lt;/%@>&lt;", tag];
        NSString *ltslashgtNot = [NSString stringWithFormat:@"&lt;/%@&gt;&lt;", tag];
        
        xml = [xml stringByReplacingOccurrencesOfString:ltgt withString:ltgtNot];
        xml = [xml stringByReplacingOccurrencesOfString:ltslashgt withString:ltslashgtNot];
    };
    xml = [xml stringByReplacingOccurrencesOfString:@"&lt;/DrillTo>" withString:@"&lt;/DrillTo&gt;"];
    return xml;
}

- (void) renderChildren:(TBXMLElement *)i xOrigin:(float)x yOrigin:(float)y pivotTag:(PivotTextTagType)pivotTag{
	if (i != nil) {
		TBXMLElement *child = i->firstChild;
		if(child != nil){
            CGRect rect;
			do {
                
				NSString *name = [TBXML elementName:child];
                
                NSLog(@"renderChild: %@", name);
                
				if ([@"text" isEqualToString:name]) {
					rect = [self renderText:child xOrigin:x yOrigin:y parent:i pivotTag:pivotTag];
				}else if ([@"frame" isEqualToString:name]) {
                    [self renderFrame:child xOrigin:x yOrigin:y];
				}else if ([@"chart" isEqualToString:name]){
					rect = [self renderChart:child xOrigin:x yOrigin:y];
				}else if ([@"gauge" isEqualToString:name]){
                    rect = [self renderChart:child xOrigin:x yOrigin:y];
                }else if ([@"image" isEqualToString:name]){
					rect = [self renderImage:child xOrigin:x yOrigin:y];
				}else if ([@"editabletextbox" isEqualToString:name]){
                    rect = [self renderEditableTextBox:child xOrigin:x yOrigin:y];
                }else if ([@"button" isEqualToString:name]){
                    rect = [self renderButton:child xOrigin:x yOrigin:y];
                }else if ([@"checkbox" isEqualToString:name]){
                    rect = [self renderCheckBox:child xOrigin:x yOrigin:y];
                }
                
                //calculate report size
                float width = rect.origin.x + rect.size.width;
                float height = rect.origin.y + rect.size.height;
                if (width > reportDimension.width) {
                    reportDimension.width = width;
                }
                if(height > reportDimension.height){
                    reportDimension.height = height;
                }
			} while ((child = child->nextSibling));
		}
	}
}

- (CGRect) renderEditableTextBox:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
    CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:0.0 parentWidth:0.0];
    
    NSString *name = [TBXML valueOfAttributeNamed:@"name" forElement:i];
    NSString *value = [TBXML valueOfAttributeNamed:@"value" forElement:i];
    
    UITextField *txtField = [[UITextField alloc]initWithFrame:frame];
    txtField.delegate = self;
    txtField.borderStyle = UITextBorderStyleLine;
    txtField.text = value;
    [self addChild:txtField];
    
    NSMutableArray *list = [editableTextBoxes objectForKey:name];
    if(list == Nil){
        list = [NSMutableArray arrayWithObject:txtField];
    }else{
        [list addObject:txtField];
    }
    [editableTextBoxes setObject:list forKey:name];
    
    [txtField release];
    
    return frame;
}

-(void) animateTextField: (UITextField *) textField up: (BOOL) up
{
    CGRect textFieldRect = [self.dashletCtrl.pageRenderCtrl.view convertRect:textField.bounds fromView:self];
    NSLog(@"y = %f", textFieldRect.origin.y);
    CGFloat yPos = textFieldRect.origin.y;
    
    if(yPos < 300){
        return;
    }
    int txtPosition = 768 - yPos;
    const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.dashletCtrl.pageRenderCtrl.view.frame = CGRectOffset( self.dashletCtrl.pageRenderCtrl.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    [theTextField resignFirstResponder];
    return YES;
}

- (CGRect) renderCheckBox:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
    CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:0.0 parentWidth:0.0];
    
    NSString *name = [TBXML valueOfAttributeNamed:@"name" forElement:i];
    NSString *value = [TBXML valueOfAttributeNamed:@"value" forElement:i];
    
    PromptGroupCheckBox *chkbox = [[PromptGroupCheckBox alloc] initWithFrame:frame];
    chkbox.transform = CGAffineTransformMakeScale(0.50, 0.50);
    chkbox.value = value;
    chkbox.on = [value isEqualToString:@"true"];
    [self addChild:chkbox];
    
    NSMutableArray *list = [editableTextBoxes objectForKey:name];
    if(list == Nil){
        list = [NSMutableArray arrayWithObject:chkbox];
    }else{
        [list addObject:chkbox];
    }
    [self.checkBoxes setObject:list forKey:name];
    
    [chkbox release];
    
    return frame;
}

- (CGRect) renderButton:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
    CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:0.0 parentWidth:0.0];
    
    PromptGroupButton *bttn = [PromptGroupButton buttonWithType:UIButtonTypeRoundedRect];
    bttn.frame = frame;
    bttn.renderer = self;
    
    [bttn parseXml:i];
    
    [bttn sizeToFit];
    
    [self addChild:bttn];
    [bttn release];
    
    return frame;
}

-(void)onButtonClick{
    
}

- (CGRect) renderImage:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
	CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:0.0 parentWidth:0.0];
    
    //Prepend server url
    NSString *url = [TBXML textForElement:i];
    NSString *serverUrl = [Settings sanitizePort:@"/SMIWeb" newPort:6103];
    url = [NSString stringWithFormat:@"%@/%@", serverUrl, url];
    NSURL *nsurl = [NSURL URLWithString:url];
    
    //Fetch image data, create the image view
    NSData *imageData = [NSData dataWithContentsOfURL:nsurl];
    UIImage *image = [UIImage imageWithData:imageData];
    
    //Check the size
    CGSize fSize = frame.size;
    CGSize iSize = image.size;
    if (fSize.width == 0 && iSize.width > 0) {
        fSize.width = iSize.width;
    }
    if(fSize.height == 0 && iSize.height > 0){
        fSize.height = iSize.height;
    }
    frame.size = fSize;
    
    //Display
    UIImageView  *imageView = [[UIImageView alloc]initWithFrame:frame];
    imageView.image = image;
    [self addChild:imageView];
    [imageView release];
    
    return frame;
}

- (CGRect) renderChart:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
	CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:0.0 parentWidth:0.0];
	//ChartView *c = [[ChartView alloc] initWithXml:i frame:frame];
    UIWebViewWrapper *c = [[UIWebViewWrapper alloc] initWithXml:i frame:frame];
    c.disableDrilling = self.disableDrilling;
    c.delegate = self;
	[self addChild:c];
    [c release];
    
    self.numCharts++;
    
    return frame;
}

- (void) renderFrame:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO{
	float x = [[TBXML valueOfAttributeNamed:@"x" forElement:i] floatValue];
	float y = [[TBXML valueOfAttributeNamed:@"y" forElement:i] floatValue];
	
	[self renderChildren:i xOrigin:(xO + x) yOrigin:(yO + y) pivotTag:[self getCellTagType:i]];
}						 

- (CGRect) renderText:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO parent:(TBXMLElement*)parent pivotTag:(PivotTextTagType)pivotTag{
    
    //Parent height and width compensation should only be for certain pivot data.
    float parentWidth = 0.0;
    float parentHeight = 0.0;
   
    if(pivotTag != None && pivotTag != CrosstabHeader){
        parentHeight = [[TBXML valueOfAttributeNamed:@"height" forElement:parent] floatValue];
        parentWidth = [[TBXML valueOfAttributeNamed:@"width" forElement:parent] floatValue];
        
        //Are we the last one in a list of text tags?
        TBXMLElement *prevSib = i->previousSibling;
        if(!i->nextSibling && prevSib){
            //We are the last in texts
            if([[TBXML elementName:prevSib] isEqualToString:@"text"]){
                float ourHeight = [[TBXML valueOfAttributeNamed:@"height" forElement:i] floatValue];
                float ourY = [[TBXML valueOfAttributeNamed:@"y" forElement:i] floatValue];
                parentHeight = ourHeight + (parentHeight - (ourY + ourHeight)); 
            }
        }
    }
    
	CGRect frame = [self createFrame:i xOrigin:xO yOrigin:yO parentHeight:parentHeight parentWidth:parentWidth];
    
	ColumnTextCell *label = [[ColumnTextCell alloc] initWithXML:i frame:frame];
    
	[self addChild:label];
	[label release];
    return frame;
}

- (CGRect) createFrame:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO parentHeight:(float)parentHeight parentWidth:(float)parentWidth{
    
	float width = [[TBXML valueOfAttributeNamed:@"width" forElement:i] floatValue];
	float height = [[TBXML valueOfAttributeNamed:@"height" forElement:i] floatValue];
	
    if(height < parentHeight){
        height = parentHeight;
    }
    
    if(width < parentWidth){
        width = parentWidth;
    }
    
	float x = [[TBXML valueOfAttributeNamed:@"x" forElement:i] floatValue];
	float y = [[TBXML valueOfAttributeNamed:@"y" forElement:i] floatValue];
	
	return CGRectMake(xO + x, yO + y, width, height);
}

- (PivotTextTagType) getCellTagType:(TBXMLElement *)el{
    if(el){
        
        TBXMLElement *tag = el->firstChild;
        if(tag && [[TBXML elementName:tag] isEqualToString:CELLTAG]){
            NSString *value = [TBXML textForElement:tag];
            if([@"Data" isEqualToString:value]){
                return Data;
            }else if ([@"RowHeader" isEqualToString:value]){
                return RowHeader;
            }else if ([@"ColumnHeader" isEqualToString:value]){
                return ColumnHeader;
            }else if ([@"CrosstabHeader" isEqualToString:value]){
                return CrosstabHeader;
            }
        }
    }
    return None;
}

- (void) addChild:(UIView *)v{
    [self.childViews addObject:v];
    [self addSubview:v];
}

- (void)dealloc {
    //[Log debug:@"ReportRenderer dealloc"];
    NSLog(@"ReportRenderer dealloc");
	[data release];
    [tbxml release];
    
    for (UIView* v in childViews) {
        if([v isKindOfClass:[UIWebViewWrapper class]]){
            UIWebViewWrapper *wvw = (UIWebViewWrapper*)v;
            wvw.delegate = nil;
        }
    }
    [childViews release];
    
    [super dealloc];
}


@end
