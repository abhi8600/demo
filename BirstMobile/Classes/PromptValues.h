//
//  PromptValues.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/28/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PromptValues : NSObject<NSCopying> {
	NSString *label;
	NSString *value;
    int index;
}

@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, assign) int index;

-(void) copyLabel:(NSString *)l;
- (void) copyValue:(NSString *)v;

@end
