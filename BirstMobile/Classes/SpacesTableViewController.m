//
//  SpacesTableViewController.m
//  Birst
//
//  Created by Seeneng Foo on 3/3/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "SpacesTableViewController.h"
#import "TBXML.h"
#import "Settings.h"
#import "NSStringCategory.h"
#import "BirstWS.h"

@implementation SpacesTableViewController

@synthesize spaces, rootCtrl, detailCtrl, spaceDetailCtrl, activity, dashsCtrl, reqPendingCtrl, networkQueue, targetSpaceName, targetSpaceId;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Network queue which handles all our http requests
    //Set to 1 to force only one requests at a time
    self.networkQueue = [BirstWS createNetworkQueueMaxOne:self finishSel:@selector(onRequestFinished:) failSel:@selector(requestFailed:)];

	 
    // Uncomment the following line to preserve selection between presentations.
	//self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) onRequestFinished:(ASIHTTPRequest*)request{
    switch (request.tag) {
        case SpaceDetail:
            [self onGetSpaceDetailsFinished:request];
            break;
        case DashboardModule:
            [self onGetDashboardModFinished:request];
            break;
        case DashboardList:
            [self stopAnimateRequest];
            [self onGetDashboardListFinished:request];
            break;
        default:
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.detailCtrl setRightBarButtonAsLogin];
	[self.detailCtrl clearSubviews];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark -
#pragma mark Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self spaces] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	Space *space = [spaces objectAtIndex:indexPath.row];
	NSString *imageName = ([space.isOwner isEqualToString:@"true"]) ? @"SpaceIconGreen" : @"SpaceIconRed";
	NSString *path = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
	UIImage *image = [UIImage imageWithContentsOfFile:path];
	
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.textLabel.text = [space.name stringByUrlDecode];
	cell.detailTextLabel.text = [space.owner stringByUrlDecode];
	cell.imageView.image = image;
	
    return cell;
}

- (void) parse:(NSString*)xml{
	self.title = @"Spaces";
	if (spaces == nil) {
		spaces = [[NSMutableArray alloc]init];
	}else {
		[spaces removeAllObjects];
	}

	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *body = [TBXML childElementNamed:@"soap:Body" parentElement:root];
		if (body != nil) {
			TBXMLElement *response = [TBXML childElementNamed:@"GetAllSpacesResponse" parentElement:body];
			if (response != nil) {
				TBXMLElement *result = [TBXML childElementNamed:@"GetAllSpacesResult" parentElement:response];
				if (result != nil) {
					TBXMLElement *spaceList = [TBXML childElementNamed:@"SpacesList" parentElement:result];
					if (spaceList != nil) {
						TBXMLElement *firstSummary = [TBXML childElementNamed:@"SpaceSummary" parentElement:spaceList];
						if (firstSummary != nil) {
							do {
								Space *obj = [[Space alloc]init];
								
								TBXMLElement *space = [TBXML childElementNamed:@"SpaceName" parentElement:firstSummary];
								obj.name = [TBXML textForElement:space];
								
								TBXMLElement *spaceId = [TBXML childElementNamed:@"SpaceID" parentElement:firstSummary];
								obj.spaceId = [TBXML textForElement:spaceId];
								
								TBXMLElement *owner = [TBXML childElementNamed:@"OwnerUsername" parentElement:firstSummary];
								obj.owner = [TBXML textForElement:owner];
								
								TBXMLElement *isOwner = [TBXML childElementNamed:@"Owner" parentElement:firstSummary];
								obj.isOwner = [TBXML textForElement:isOwner];
								
								[spaces addObject:obj];
								[obj release];
							} while ((firstSummary = firstSummary->nextSibling));
						}
					}
				}//result != nil
			}
		}
	}
    [tbxml release];
	
	[self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void) startAnimateRequest{
    if (!self.activity) {
        //Activity on the row
        NSIndexPath *idx = [self.tableView indexPathForSelectedRow];
        self.activity = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        [self.activity startAnimating];
    
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:idx];
        [cell setAccessoryView:self.activity];
        
        //The bigger activity
        self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
        self.reqPendingCtrl.message = @"Loading space...";
        self.reqPendingCtrl.view.frame = self.detailCtrl.view.bounds;
        [self.detailCtrl.view addSubview:self.reqPendingCtrl.view];
    }
}

- (void) stopAnimateRequest {
    if (self.activity) {
        [self.activity stopAnimating];
        [activity release];
        self.activity = nil;
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        [cell setAccessoryView:nil];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        //The whole screen
        if(reqPendingCtrl){
            [reqPendingCtrl.view removeFromSuperview];
            [reqPendingCtrl release];
            reqPendingCtrl = nil;
        }
    }
}

#pragma mark -
#pragma mark Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"selected row %d", indexPath.row);
    
    //Do nothing if we're still processing
    if (self.activity && [self.activity isAnimating]) {
        return;
    }
    
	Space *space = [spaces objectAtIndex:indexPath.row];
	if (space != nil) {
        [self startAnimateRequest];
        
        self.targetSpaceId = space.spaceId;
        self.targetSpaceName = space.name;
        
        ASIHTTPRequest *request = [BirstWS requestWithAdmin:@"GetSpaceDetails"];
        request.tag = SpaceDetail;
		NSString *body = [NSString stringWithFormat:@"<tns:spaceID>%@</tns:spaceID><tns:spaceName>%@</tns:spaceName>", space.spaceId, space.name];
        [BirstWS requestFormat:request body:body];
        [self.networkQueue addOperation:request];
	}
}


- (void)onGetSpaceDetailsFinished:(ASIHTTPRequest *)request {
    
    //NSURL *url = [Settings getNSURL:@"DashboardModule.aspx"];
    NSURL *url = [Settings getDashboardModulePageUrl];
    ASIHTTPRequest* dashRequest = [ASIHTTPRequest requestWithURL:url];
    dashRequest.tag = DashboardModule;
    [self.networkQueue addOperation:dashRequest];
    
}

//Parse response from getSpaceDetails() webservice call
//display the space details view in the details section
- (void)parseSpaceDetails:(NSString *)xml{
	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *results = [Util traverseFindElement:root target:@"GetSpaceDetailsResult"];
		if (results != nil) {
			SpaceDetailsView *ctrl = [[[SpaceDetailsView alloc] init] autorelease];
			ctrl.rootCtrl = rootCtrl;
			ctrl.detailCtrl = detailCtrl;
			ctrl.spacesCtrl = self;
			
			TBXMLElement *el = results->firstChild;
			if (el) {
				do {
					NSString *name = [TBXML elementName:el];
					NSString *value = [TBXML textForElement:el];
					if ([name isEqualToString:@"Error"]) {
						//TODO - error
					}else if ([name isEqualToString:@"SpaceName"]) {
						ctrl.tName = value;
					}else if ([name isEqualToString:@"OwnerUsername"]) {
						ctrl.tOwner = value;
					}else if ([name isEqualToString:@"LastUpload"]){
						ctrl.tLastProcessed = value;
					}else if ([name isEqualToString:@"SpaceSize"]){
						ctrl.tSize = value;
					}
				} while ((el = el->nextSibling));
			}
			[detailCtrl setNewView:ctrl];
		}
	}
    [tbxml release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self stopAnimateRequest];
    
	NSError *error = [request error];
	NSLog(@"requestFailed: %@", error);

	[Util requestFailedAlert:error];
}


- (void) onGetDashboardModFinished:(ASIHTTPRequest *)request{
    NSString *response = [Util getResponse:request];
    
    //Figure out if we're using a 4.0 version, hence we can use optimized
    //getDashbardListFromDB
    NSRange range = [response rangeOfString:@"smiroot=&v=4"];
    NSString *op =  (range.length > 0) ? @"getDashboardListCached" :@"getDashbardListFromDB";
    
    ASIHTTPRequest *dashRequest = [BirstWS requestWithDashboard:op];
    [BirstWS requestFormat:dashRequest body:@""];
    dashRequest.tag = DashboardList;
    [self.networkQueue addOperation:dashRequest];
}

- (void)onGetDashboardListFinished:(ASIHTTPRequest *)request{
    [self stopAnimateRequest];
	NSString *response = [request responseString];
    
    //Format
    NSMutableString *mStr = [NSMutableString stringWithString:response];
    [mStr replaceOccurrencesOfString:@"&lt;" withString:@"<" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mStr length]) ];
    [mStr replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mStr length]) ];
	[mStr replaceOccurrencesOfString:@"&quot;" withString:@"\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mStr length]) ];
    [mStr replaceOccurrencesOfString:@"&apos;" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mStr length]) ];
    [mStr replaceOccurrencesOfString:@"&amp;" withString:@"&" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mStr length]) ];
    
	//TODO - verify response
	self.dashsCtrl = [[[DashboardsViewController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
	self.dashsCtrl.detailCtrl = self.detailCtrl;
	
	NSIndexPath *idx = [self.tableView indexPathForSelectedRow];
	NSLog(@"getDashboardList row index %d", idx.row);
	
    self.dashsCtrl.title = self.targetSpaceName;
    self.dashsCtrl.spaceId = self.targetSpaceId;
    self.dashsCtrl.spaceName = self.targetSpaceName;
    
	[self.navigationController pushViewController:self.dashsCtrl animated:YES];
	[self.dashsCtrl parse:mStr]; 		
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    NSLog(@"SpacesTableViewController dealloc");
    
    [self stopAnimateRequest];
    
	[spaceDetailCtrl release];
	[spaces release];
    [dashsCtrl release];
    
    [networkQueue reset];
	[networkQueue release];
    
    [targetSpaceId release];
    [targetSpaceName release];
    
    [super dealloc];
}


@end

