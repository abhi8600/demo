//
//  LoginSettingsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 7/12/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "LoginSettingsViewController.h"
#import "Login.h"
#import "ASIHTTPRequest.h"
#import "ServerSettingsEditViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation LoginSettingsViewController

@synthesize username, password,rememberPassword, login, toggleEditServers, settings, serverListings, cellTextField, addedNewServer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.login useSimpleOrangeStyle]; 
    
    //Add gradient to background
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    UIColor *top = [Util colorWithRGBHex:0x000000];
    UIColor *bottom = [Util colorWithRGBHex:0x666666];
    gradient.colors = [NSArray arrayWithObjects:(id)[top CGColor], (id)[bottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    self.title = @"Login";

    self.username.text = [Settings username];
    self.password.text = [Settings password];
    
    self.settings = [[Settings instance] settings];
    
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    [ctr addObserver:self selector:@selector(onKeyboardShows:) name:UIKeyboardDidShowNotification object:nil];
    [ctr addObserver:self selector:@selector(onKeyboardHides:) name:UIKeyboardWillHideNotification object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self toggleControlsInteraction];
}

- (void) toggleControlsInteraction
{
    BOOL enabled = ![Settings loggedIn];
    self.username.enabled = self.password.enabled = self.rememberPassword.enabled = self.serverListings.userInteractionEnabled = enabled;
    self.username.alpha = self.password.alpha = (enabled) ? 1 : 0.3;
    self.toggleEditServers.enabled = enabled;
    
    BOOL rememberPass = [Settings rememberPassword];
    [rememberPassword setOn:rememberPass animated:YES];
    
    NSString *loginTitle = (enabled) ? @"Login" : @"Logout";
    [self.login setTitle:loginTitle forState:UIControlStateNormal];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard notifications

-(void) onKeyboardShows:(id)sender{
    NSLog(@"keyboard will show");
    if([self.cellTextField isFirstResponder]){
        self.view.frame = CGRectMake(0, -200, self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void) onKeyboardHides:(id)sender{
    NSLog(@"keyboard will hide");
    if([self.cellTextField isFirstResponder]){
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
}

#pragma mark - UIPickerView delegate operations

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[self.settings objectForKey:@"servers"] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *servers = [self.settings objectForKey:@"servers"];
    return [servers objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 450;
}

#pragma mark - Login operations

- (IBAction)doLogin:(id)sender{
    if([Settings loggedIn]){
        //Do logout
        
        [self showPendingRequest:@"Logging out..."];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[Settings logoutUrl]];
        [Settings addCSRFHeader:request];
        [request startSynchronous];
        [self removePendingRequest];
        
        [Settings setLoggedIn:NO];
        [Settings setRedirectedServer:nil];
        
        [self toggleControlsInteraction];
        
        //Refresh left side bar
        if([self.leftSidebarViewController.spaces count] > 0){
            [self.leftSidebarViewController.spaces removeAllObjects];
        }
        [self.leftSidebarViewController.tableView reloadData];
        [self.leftSidebarViewController setLastSelectedIndexPath];
        
    }else{
        NSString *passToSave = (self.rememberPassword.on) ? password.text : @" ";
        [Settings setRememberPassword:self.rememberPassword.on];
        [Settings setUsername:username.text password:passToSave];
        
        [Login performLogin:username.text password:password.text startAnimate:@selector(startAnimateLogin) endAnimate:@selector(endAnimateLogin) delegate:self loginSuccess:@selector(onLoginSuccess) loginFail:@selector(onLoginFail:)]; 
    }
}

- (IBAction)editServers:(id)sender {
    BOOL toggle = !self.serverListings.editing;
    [self.serverListings setEditing:toggle animated:YES];
    [self.serverListings reloadData];
}

- (void) startAnimateLogin{
    [self showPendingRequest:@"Logging in..."];
}

- (void) endAnimateLogin{
    [self removePendingRequest];
}

- (void) onLoginSuccess{
    [self removePendingRequest];
    [self toggleControlsInteraction];
    [self displaySpaces:self.leftSidebarViewController row:0];
}

- (void) onLoginFail:(ASIHTTPRequest*)request{
    [self removePendingRequest];
}

#pragma mark - Left side bar delegate operations

- (NSIndexPath *) lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController{
    return [NSIndexPath indexPathForRow:IDX_LOGIN inSection:IDX_HDR_MAIN];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // # of servers + edit 
    int count = [[self.settings objectForKey:@"servers"] count];
    return (tableView.editing == YES) ? count + 1: count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    static NSString *EditCellIdentifier = @"EditCell";
    
    UITableViewCell *cell;
    
    NSArray *servers = [self.settings objectForKey:@"servers"];
    BOOL isAddRow = (tableView.editing == YES && indexPath.row >= [servers count]);
   
    if (!isAddRow) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        
        cell.textLabel.text = [servers objectAtIndex:indexPath.row];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:EditCellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:EditCellIdentifier] autorelease];
            
            CGRect f = CGRectMake(40, 1, cell.bounds.size.width - 65, 32);
            self.cellTextField = [[[UITextField alloc]initWithFrame:f] autorelease];
            //[self.cellTextField setEnabled:YES];
            self.cellTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            self.cellTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.cellTextField.borderStyle = UITextBorderStyleRoundedRect;
            self.cellTextField.delegate = self;
            [self.cellTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [cell addSubview:self.cellTextField];
        }
        self.cellTextField.text = @"";
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14];

    if ([cell.textLabel.text isEqualToString:[Settings selectedServer]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
	
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *servers = [self.settings objectForKey:@"servers"];
    if (indexPath.row == [servers count]) {
        return UITableViewCellEditingStyleInsert;
    }
    
    return UITableViewCellEditingStyleDelete;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *servers = [self.settings objectForKey:@"servers"];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView beginUpdates];
        
        [servers removeObjectAtIndex:indexPath.row];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [tableView endUpdates];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        if (self.addedNewServer) {
            [self.cellTextField resignFirstResponder];
            
            [tableView beginUpdates];
            [servers addObject:self.addedNewServer];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView endUpdates];
            
            self.addedNewServer = nil;
            self.cellTextField.text = nil;
        }
    } 
    [Settings saveSavedSettings:self.settings];
}

- (void)textFieldDidChange:(UITextField *)textField{
    self.addedNewServer = textField.text;
}

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *currentServer = [Settings selectedServer];
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *selectedServer = selectedCell.textLabel.text;
    
    if (![currentServer isEqualToString:selectedServer]) {
        [Settings setServer:selectedServer];
        [tableView reloadData];
    }
}




@end
