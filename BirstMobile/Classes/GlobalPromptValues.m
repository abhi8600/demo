//
//  GlobalPromptValues.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/3/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "GlobalPromptValues.h"

static GlobalPromptValues *_inst;

@implementation GlobalPromptValues

- (id) init{
    self = [super init];
    if(self){
        keyValues = [[NSMutableDictionary alloc]init];   
    }
    return self;
}



+ (GlobalPromptValues *) instance{
    if(!_inst){
        _inst = [[GlobalPromptValues alloc]init];
    }
    return _inst;
}

- (NSString *) getValue:(NSString *)key{
    return [keyValues valueForKey:key];
}

- (void) setValue:(id)value key:(NSString*)key{
    [keyValues setObject:value forKey:key];
}

- (void) reset{
    [keyValues removeAllObjects];
}

- (void) dealloc{
    [keyValues release];
    [super dealloc];
}

@end
