//
//  Prompt.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/23/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Prompt.h"
#import "PromptValues.h"
#import "SessionVariables.h"
#import "Util.h"
#import "Log.h"
#import "GlobalPromptValues.h"

@implementation Prompt

@synthesize tagValues, selectedValues, drillValue, isParentPrompt, ref, parents, isUpdateByGlobalVars;

- (id) init{
    self = [super init];
    if (self) {
        isParentPrompt = NO;
        isUpdateByGlobalVars = NO;
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        self.tagValues = dict;
        [dict release];
    }
    return self;
}

-(NSString*) name{
	return [Util xmlUnescape:[self.tagValues objectForKey:@"VisibleName"]];
}

-(PromptType) type{
	NSString *tp = [self.tagValues objectForKey:@"PromptType"];
	if ([@"List" isEqualToString:tp]) {
		return List;
	}else if ([@"Value" isEqualToString:tp]){
		return Value;
	}else if ([@"Query" isEqualToString:tp]){
        return Query;
    }else if ([@"CheckBox" isEqualToString:tp]){
        return CheckBox;
    }
    
    //Todo - Slider
    
	return Query;
}

-(BOOL) isVariableToUpdate{
    return [[self.tagValues objectForKey:@"variableToUpdate"] isEqualToString:@"true"];
}

-(BOOL) isMultiSelect{
	NSString *ms = [self.tagValues objectForKey:@"multiSelectType"];
	return ([@"OR" isEqualToString:ms] || [@"AND" isEqualToString:ms]);
}

-(BOOL) isDate{
    NSString *clz = [self.tagValues objectForKey:@"Class"];
    return [clz isEqualToString:@"Date"];
}

-(BOOL) isDateTime{
    NSString *clz = [self.tagValues objectForKey:@"Class"];
    return [clz isEqualToString:@"DateTime"];
}
 
-(BOOL) isInvisible{
    NSString *iv = [self.tagValues objectForKey:@"Invisible"];
    return (iv != nil) ? [iv isEqualToString:@"true"] : NO;
}

-(NSString*)parameterName{
	return [self.tagValues objectForKey:@"ParameterName"];
}

-(NSString*)defaultOption{
	NSString *defaultOpt = [self.tagValues objectForKey:@"DefaultOption"];
	if (defaultOpt) {
		//TODO - session variables, date parsing
		NSRange range = [defaultOpt rangeOfString:@"V{"];
		if (range.length > 0) {
			SessionVariables *sess = [SessionVariables instance];
            NSString *value = [sess.sessionVars valueForKey:defaultOpt];
            if (value) {
                if ([self isDate]) {
                    NSArray *comps = [value componentsSeparatedByString:@" "];
                    if ([comps count] > 0) {
                        return [comps objectAtIndex:0];
                    }
                }
                return value;
            }
		}
	}
	
	return defaultOpt;
}

-(NSString *)defaultOptionSessionVar{
    NSString *defaultOpt = [self.tagValues objectForKey:@"DefaultOption"];
	if (defaultOpt) {
		//TODO - session variables, date parsing
		NSRange range = [defaultOpt rangeOfString:@"V{"];
		if (range.length > 0) {
            return defaultOpt;
        }
    }
    return nil;
}

-(BOOL) isDefaultOptionSessionVar{
    NSString *defaultOpt = [self.tagValues objectForKey:@"DefaultOption"];
    if(defaultOpt){
        NSRange range = [defaultOpt rangeOfString:@"V{"];
        return (range.length > 0);
    }
    
    return NO;
}

-(BOOL) addNoSelectionEntry{
	NSString *add = [self.tagValues objectForKey:@"addNoSelectionEntry"];
	return [add isEqualToString:@"true"];
}

-(NSString *) allText{
	NSString *text = [self.tagValues objectForKey:@"text"];
	if (!text || [text isEqualToString:@""]) {
		text = @"All";
	}
				  
	return text;
}

-(NSArray *) parentPrompt{
    return self.parents;
}

-(NSString *)operator{
	return [self.tagValues objectForKey:@"Operator"];
}

- (NSMutableArray *)labelValues{
	if (!labelValues) {
		labelValues = [[NSMutableArray alloc] init];
    }
	return labelValues;
}

- (NSString *) columnName{
    return [self.tagValues objectForKey:@"ColumnName"];
}

-(void) parse:(TBXMLElement *)p{
	if (p && [@"Prompt" isEqualToString:[TBXML elementName:p]]) {
		TBXMLElement *node = p->firstChild;
		if (node) {
			if ([self.tagValues count] > 0) {
				[self.tagValues removeAllObjects];
			}

			do {
				NSString *name = [TBXML elementName:node];
				NSString *value = [TBXML textForElement:node];
                if ([name isEqualToString:@"ListValues"]) {
                    [self parseListValues:node->firstChild];
                }else if ([name isEqualToString:@"selectedValues"]){
                    [self parseSelectedValues:node->firstChild];
                }else if ([name isEqualToString:@"Parents"]){
                    [self parseParent:node->firstChild];
                }
				[self.tagValues setObject:value forKey:name];
                //[name release];
                //[value release];
			} while ((node = node->nextSibling));
		}
        
        self.ref = [TBXML valueOfAttributeNamed:@"ref" forElement:p];
        
    }
}

//Parse the user defined labels - values for query type LIST
-(void) parseListValues:(TBXMLElement *)listValue{
    if (listValue) {
        int i = 0;
        if(self.addNoSelectionEntry){
            PromptValues *v = [[[PromptValues alloc]init]autorelease];
            v.label = self.allText;
            v.value = @"NO_FILTER";
            [self.labelValues addObject:v];
            i++;
        }

        PromptValues *defaultOptionMatched = nil;        
        do{
            
            TBXMLElement *firstChild = listValue->firstChild;
            TBXMLElement *secondChild = firstChild->nextSibling;
            if (firstChild && secondChild) {
              
                NSString* firstChildValue = [TBXML textForElement:firstChild];
                NSString* secondChildValue = [TBXML textForElement:secondChild];
                
                PromptValues *v = [[PromptValues alloc]init];
            
                if([[TBXML elementName:firstChild] isEqualToString:@"Label"]){
                    v.label = firstChildValue;
                    v.value = secondChildValue;
                }else{
                    v.label = secondChildValue;
                    v.value = firstChildValue;
                }
                               
                if (!defaultOptionMatched && [self matchDefaultOptionOrSelectedValue:v.value]) {
                    defaultOptionMatched = v;
                    v.index = i;
                }
                
                [self.labelValues addObject:v];
                [v release];
                i++;
            }
        }while((listValue = listValue->nextSibling));
        
        if (!self.selectedValues) {
            self.selectedValues = [[[NSMutableArray alloc]init] autorelease];
        }
        
        
        if (defaultOptionMatched) {
            [self.selectedValues addObject:defaultOptionMatched];
        }else{
            if ([self.labelValues count] > 0) {
                PromptValues *v = [self.labelValues objectAtIndex:0];
                v.index = 0;
                [self.selectedValues addObject: v];
            }
        }
        
    }
}

-(void) parseSelectedValues:(TBXMLElement *)selectedValue{
    if(selectedValue){
        NSMutableArray *values = [[NSMutableArray alloc]init];
        do{
            NSString *v= [TBXML textForElement:selectedValue];
            PromptValues *pv = [[PromptValues alloc]init];
            pv.label = pv.value = v;
            [values addObject:pv];
            [pv release];
        }while((selectedValue = selectedValue->nextSibling));
        self.selectedValues = values;
        [values release];
    }
}

- (void) parseParent:(TBXMLElement *)p{
    if(p){
        NSMutableArray *pts = [[NSMutableArray alloc]init];
        do{
            NSString *pVal = [TBXML textForElement:p];
            [pts addObject:pVal];
        }while ((p = p->nextSibling));
        self.parents = pts;
        [pts release];
    }
}

//Reset the selected value of a LIST prompt to default option or the first
- (void) listValueRefresh{
    if (self.type == List) {
        int count = [self.labelValues count];
        if (count > 0) {
            if (!self.selectedValues) {
                self.selectedValues = [[[NSMutableArray alloc]init] autorelease];
            }
            
            NSString *defaultValue = self.defaultOption;
            PromptValues *defaultOptionMatched = nil;

            for (int i = 0; i < count; i++) {
                PromptValues *v = [self.labelValues objectAtIndex:i];
                if (!defaultOptionMatched && [defaultValue isEqualToString:v.value]) {
                    defaultOptionMatched = v;
                    v.index = i;
                }
            }
            
            if (defaultOptionMatched) {
                [self.selectedValues addObject:defaultOptionMatched];
            }else{
                if ([self.labelValues count] > 0 && [self.selectedValues count] == 0) {
                    PromptValues *v = [self.labelValues objectAtIndex:0];
                    v.index = 0;
                    [self.selectedValues addObject: v];
                }
            }
        }
        
    }
}

-(MatchType) matchDefaultOptionOrSelectedValue:(NSString*) value{
	int numSelectedValues = [self.selectedValues count];
	if(numSelectedValues > 0){
		for(int i = 0; i < numSelectedValues; i++){
			PromptValues *v = [self.selectedValues objectAtIndex:i];
			if([value isEqualToString:v.value]){
				return MatchSelectedValue;
			}
		}
	}else {
		if([value isEqualToString:self.defaultOption]){
			return MatchDefaultOption;
		}
	}
	
	return NoMatch;
}

-(void) parseValues:(TBXMLElement *)values{
    if(self.type == List){
        return;
    }else if(self.type == Value){
        //Selected values was already set, likely via drill to dashboards
        if([self.selectedValues count] > 0){
            return;
        }
        
        //Value won't have values, we check if the default option applies
        NSString *defaultOpt = self.defaultOption;
        if([defaultOpt length] > 0){
            if (!self.selectedValues) {
                self.selectedValues = [[[NSMutableArray alloc]init] autorelease];
            }else{
                if([self.selectedValues count] > 0){
                    [self.selectedValues removeAllObjects];
                }
            }
            PromptValues *v = [[PromptValues alloc] init];
            v.label = v.value = self.defaultOption;
            [self.selectedValues addObject:v];
            [v release];
            return;
        }
    }
    
	if ([self.labelValues count] > 0) {
		[self.labelValues removeAllObjects];
	}
	
    int i = 0;
    
	if(self.addNoSelectionEntry){
		PromptValues *v = [[PromptValues alloc]init];
		v.label = self.allText;
		v.value = @"NO_FILTER";
		[self.labelValues addObject:v];
        [v release];
        i++;
	}
    
    PromptValues *defaultOptionMatched = nil;
    BOOL foundEqual = NO;
	for (; values != nil; values = values->nextSibling){
		TBXMLElement *lab = [TBXML childElementNamed:@"Label" parentElement:values];
		TBXMLElement *val = [TBXML childElementNamed:@"Value" parentElement:values];
		if (lab && val) {
			PromptValues *v = [[PromptValues alloc]init];
			v.label = [TBXML textForElement:lab];
			v.value = [TBXML textForElement:val];
            
            if (!foundEqual){ 
				MatchType t = [self matchDefaultOptionOrSelectedValue:v.value];
				switch (t) {
					case MatchDefaultOption:
						defaultOptionMatched = v;
					case MatchSelectedValue:
						v.index = i;
						foundEqual = YES;
						break;
					default:
						break;
				}
            }
            
			[self.labelValues addObject:v];
            [v release];
		}
        i++;
	}
    
    if (self.selectedValues == nil) {
        NSMutableArray *r = [[NSMutableArray alloc]init];
        self.selectedValues = r;
        [r release];
    }
    
    //Only update if the selected values was not set by global variables.
    if(!self.isUpdateByGlobalVars){
        if (defaultOptionMatched) {
            NSArray* globSels = [NSArray arrayWithObject:[defaultOptionMatched copy]];
            if(self.isVariableToUpdate){
                GlobalPromptValues *gpv = [GlobalPromptValues instance];
                [gpv setValue:globSels key:self.parameterName];
            }
            [self.selectedValues addObject:defaultOptionMatched];
        }else{
            if ([self.labelValues count] > 0 && foundEqual == NO) {
                PromptValues *v = [self.labelValues objectAtIndex:0];
                v.index = 0;
                [self.selectedValues removeAllObjects];
                [self.selectedValues addObject: v];
            }
        }
    }
    self.isUpdateByGlobalVars = NO;
}

-(BOOL) setSelectedValue:(NSString *)value force:(BOOL)force{
	if(force){
		NSMutableArray *mta = [[NSMutableArray alloc]init];
		self.selectedValues = mta;
		[mta release];
		return [self setSingleSelectedValue:value];
	}
	
    switch (self.type) {
        case Value: 
        {
			return [self setSingleSelectedValue:value];	
        }
        case List:
        case Query:
        {
            for(int i = 0; i < [self.labelValues count]; i++){
                PromptValues *v = [self.labelValues objectAtIndex:i];
                if ([v.value isEqualToString:value]) {
                    [self.selectedValues removeAllObjects];
                    [self.selectedValues addObject:v];
                    return YES;
                }
            }
        }
        default:
            break;
    }
    return NO;        
}

-(BOOL)setSingleSelectedValue:(NSString*) value{
	[self.selectedValues removeAllObjects];
	PromptValues *v = [[PromptValues alloc]init];
	v.label = v.value = value;
	[self.selectedValues addObject:v];
	[v release];
	return YES;
}

-(void) appendToString:(NSMutableString*)root addSelectedValues:(BOOL)setSelectVals{
	NSArray *keys = [self.tagValues allKeys];
	[root appendString:@"<Prompt>"];
	for (int i = 0; i < [keys count]; i++) {
		NSString *k = [keys objectAtIndex:i];
		NSString *v = [self.tagValues objectForKey:k];
		
		[root appendString:[NSString stringWithFormat:@"<%@>%@</%@>", k, v, k]];
	}
	
    int numParents = [self.parents count];
    if(numParents > 0){
        [root appendString:@"<Parents>"];
        for(int y = 0; y < numParents; y++){
            [root appendString:[NSString stringWithFormat:@"<Parent>%@</Parent>", [self.parents objectAtIndex:y]]];
        }
        [root appendString:@"</Parents>"];
    }
    
	if (setSelectVals) {
		[self appendSelectedValues:root];
	}
	
	[root appendString:@"</Prompt>"];
}

-(void)appendSelectedValues:(NSMutableString*)root{
	int count = [self.selectedValues count];
    
    //default option for prompt of type value
    if (count <= 0 && self.type == Value && self.defaultOption) {
    //if (count == 0){
        if (!self.selectedValues) {
            self.selectedValues = [[[NSMutableArray alloc]init] autorelease];
        }
        PromptValues *v = [[[PromptValues alloc]init]autorelease];
        if ([self.defaultOption length] == 0 ) {
            v.label = self.allText;
            v.value = @"NO_FILTER";
        }else{
            v.label = v.value = self.defaultOption;
        }
        [self.selectedValues addObject:v];
        count++;
    }
    
	if (count > 0) {
		[root appendString:@"<selectedValues>"];
		for (int i = 0; i < count; i++) {
			PromptValues *pv = [self.selectedValues objectAtIndex:i];
			NSString *selVal = [NSString stringWithFormat:@"<selectedValue>%@</selectedValue>", pv.value];
            [Log debug:@"Prompt %@ selectValue = %@", self.name, pv.value];
			[root appendString:selVal];
		}
		[root appendString:@"</selectedValues>"];
	}
}

-(NSString*) toXmlString{
	NSMutableString *str = [[[NSMutableString alloc]init] autorelease];
	[self appendToString:str addSelectedValues:YES];
	return str;
}

-(NSString*) summary{
	NSMutableString *str;
	int count = [self.selectedValues count];
	if (count > 0) {
        NSString* op = [Util xmlUnescape:self.operator];
		str = [NSMutableString stringWithFormat:@"%@ ", op];
		for (int i = 0; i < count; i++) {
			PromptValues *pv = [self.selectedValues objectAtIndex:i];
			if (i + 1 < count) {
				[str appendString:[NSString stringWithFormat:@"%@, ", pv.label]];
			}else {
				[str appendString:pv.label];
			}

		}
	}else {
		str = [NSMutableString stringWithString:self.allText];
	}
	return str;
}

-(BOOL) expandIntoButtons{
    return [[self.tagValues objectForKey:@"ExpandIntoButtons"] isEqualToString:@"true"];
}

//Reset the selected values and for query type LIST, apply the default option / first value
-(void)clearSelectedValues{
    if (selectedValues) {
        [selectedValues release];
        selectedValues = nil;
    }
    
    //reset drill value
    self.drillValue = nil;

    [self listValueRefresh];
}

-(void)dealloc{
    [selectedValues release];
	[tagValues release];
	[labelValues release];
	[super dealloc];
}

@end
