//
//  PromptGroupButton.h
//  BirstMobile
//
//  Created by See Neng Foo on 10/7/13.
//
//

#import <UIKit/UIKit.h>
#import "ReportRenderer.h"
#import "TBXML.h"

@interface PromptGroupButton : UIButton{
    ReportRenderer *renderer;
    NSMutableArray *options;
    NSString *operationName;
}

@property (assign) ReportRenderer *renderer;
@property (retain) NSMutableArray *options;
@property (retain) NSString *operationName;

- (void) parseXml:(TBXMLElement *)xml;
- (void) onButtonClick;

@end
