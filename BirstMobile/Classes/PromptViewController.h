//
//  PromptViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/23/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prompt.h"

@interface PromptViewController : UIViewController<UIPickerViewDelegate, UITableViewDelegate> {
	Prompt *prompt;
	UIViewController* promptCtrl;
	BOOL setValue;
    CGSize promptCtrlSize;
}

@property (nonatomic, assign) Prompt* prompt;
@property (nonatomic, retain) UIViewController* promptCtrl;

-(void)setValueToPrompt;
-(void)apply;
-(void)cancel;
@end
