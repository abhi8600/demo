#import <UIKit/UIKit.h>
#import "tdgchart.h"
#import "TBXML.h"

typedef struct {
	int	nMajorType;
	int nMinorType;
} TDG_GraphPresetInfo;

typedef struct {
	int16_t nSeriesID;
	BOOL bValue;
} TDG_BOOLEAN16;

typedef struct _TDG_SerDepINT16 {
	int16_t nSeriesID;
	int16_t nValue;
} TDG_SerDepINT16;

typedef struct _TDG_GridStepValues {
	BOOL bUseGridStep;
	float fGridStep;
} TDG_GridStepValuesInfo;

typedef struct _TDG_GridLines {
	BOOL bIsLog;
	BOOL bMajorHashManual;
	BOOL bMinorHashManual;
	int16_t nMajorHashStyle;
	int16_t nMinorHashStyle;
	int16_t nMajorHashCount;
	int16_t nMinorHashCount;
} TDG_GridLinesInfo;

typedef struct _TDG_GridLinesOrd {
	int16_t nMajorHashStyle;
	int16_t nMinorHashStyle;
	int16_t nMinorHashCount;
	int16_t nSuperHashStyle;
	int16_t nSuperHashCount;
} TDG_GridLinesOrdInfo;

typedef struct {
	int16_t red;      //0...65535
	int16_t green;    //0...65535
	int16_t blue;     //0...65535
} RGB16;

typedef struct {
    int16_t nXoff;     /* virtual x offset */
    int16_t nYoff;     /* virtual y offset */
    uint16_t nRed;     /* RGB values */
    uint16_t nGreen;   /* RGB values */
    uint16_t nBlue;    /* RGB values */
} DropShadowInstRec;

typedef struct {
    int16_t nSeriesID;
    int16_t nValue;
} SliceMove;

@interface ChartView : UIView {
	TDGChart* chart;
	NSString *xAxisTitle;
	NSString *yAxisTitle;
    NSMutableDictionary *seriesAttributes;
    BOOL supportedChart;
}

-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f;

@property(nonatomic, retain) TDGChart* chart;
@property(nonatomic, retain) NSString *xAxisTitle;
@property(nonatomic, retain) NSString *yAxisTitle;
@property (nonatomic, readonly) NSMutableDictionary *seriesAttributes;

- (void)parse:(TBXMLElement *)xml;
- (void) parsePoints:(TBXMLElement *)p name:(NSString*)name seriesIndex:(int)seriesIndex;
- (void) parsePointsPie:(TBXMLElement *)p name:(NSString*)name;
- (void)resize:(CGRect)f;

- (NSString *)titleText:(TBXMLElement *)child target:(NSString*)t;
- (UIColor*) parseRgbColor:(NSString*)str;
- (NSArray *) startEndGradientColor:(NSString*)str percentDiff:(float)percentDiff secondAlpha:(float)secondAlpha;
- (UIColor *) getBackgroundColor:(TBXMLElement *)el;
- (void) parseAxes:(TBXMLElement *)axes;
- (void) parseLegend:(TBXMLElement *)legend;
- (void)handleTap:(UITapGestureRecognizer*)tapEvent;
- (void) parsePointAttribute:(TBXMLElement *)attr series:(int)series group:(int)group;
- (void) parseChartSettings:(TBXMLElement *)settings;
- (void) parseSeries:(TBXMLElement *)series;
- (void) displayUnsupportedMessage;
- (void)displayScreenMessage:(NSString*)message;
- (void) parseLabelSettings:(TBXMLElement*)el;
- (void) parseFont:(TBXMLElement*)font isAxisX:(BOOL)isAxisX formatOrdinal:(TDGTextFormat*)ordinal formatY:(TDGTextFormat*)y;
- (void)parseFormat:(TBXMLElement *)xml isAxisX:(BOOL)isAxisY;
@end
