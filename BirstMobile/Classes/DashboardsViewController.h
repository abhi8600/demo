//
//  DashboardsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "TBXML.h"
#import	"Dashboard.h"
#import "Util.h"
#import "PagesViewController.h"
#import "DetailViewController.h"

@class PagesViewController;

@interface DashboardsViewController : UITableViewController {
	NSMutableArray *dashboards;
    NSMutableArray *invisibleDashboards;
	DetailViewController *detailCtrl;
    PagesViewController *pagesCtrl;
    NSDictionary *drillToAttributes;
    NSString *spaceId;
    NSString *spaceName;
    BOOL setAsDefaultBttn;
    NSUInteger selectedIndex;
    NSUInteger pageSelectedIndex;
}

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, assign) NSUInteger pageSelectedIndex;
@property (nonatomic, assign) BOOL setAsDefaultBttn;
@property (nonatomic, retain) NSString *spaceId;
@property (nonatomic, retain) NSString *spaceName;
@property (nonatomic, assign) DetailViewController *detailCtrl;
@property (nonatomic, retain) NSMutableArray *dashboards;
@property (nonatomic, retain) NSMutableArray *invisibleDashboards;
@property (nonatomic, retain) PagesViewController *pagesCtrl;

-(void) parse:(NSString *)xml;
-(void) listPages:(NSUInteger)index;
-(void) initClearDashboards;

@end
