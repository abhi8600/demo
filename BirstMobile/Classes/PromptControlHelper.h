//
//  PromptControlHelper.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Prompt.h"

@interface PromptControlHelper : NSObject {
    
}

+ (void) parentPromptValueChanged:(id)promptCtrl prompt:(Prompt*)prompt ;
@end
