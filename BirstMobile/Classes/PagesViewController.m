//
//  PagesViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "PagesViewController.h"
#import "Page.h"
#import "PageRenderViewController.h"
#import "LoginViewController.h"
#import "Util.h"
#import "BirstWS.h"
#import "Notification.h"

@implementation PagesViewController

@synthesize selectedIndex, pages, invisiblePages, dashCtrl; //detailCtrl;

#pragma mark -
#pragma mark View lifecycle

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    NSLog(@"PagesViewController: initWithNibName");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.selectedIndex = -1;
    }
    return self;
}

- (void)viewDidLoad {
    NSLog(@"PagesViewController: viewDidLoad");
    
    [super viewDidLoad];
	
	/* Let the background show */

	self.tableView.backgroundView = nil;
	self.tableView.opaque = NO;
	
	/*
	//self.tableView.alpha = 0.8;
	self.tableView.opaque = NO;
	 */
	
	/*
	UIImageView *imgView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green-grunge.png"]] autorelease];
	imgView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
	self.tableView.backgroundView = imgView;
	//self.tableView.backgroundColor = [UIColor clearColor];
	 */
	
	[self.tableView reloadData];

    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    if(self.selectedIndex != -1 && [pages count] > self.selectedIndex){
        NSIndexPath *idx = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
        [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }
    
}


- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"PagesViewController: viewWillAppear");
    [super viewWillAppear:animated];
    
   }


- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"PagesViewController: viewDidAppear");
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    NSLog(@"PagesViewController: viewWillDisappear");
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"PagesViewController: viewDidAppear");
    [super viewDidDisappear:animated];
}


-(void) displayPage:(NSString*)page drillAttributes:(DrillAttributes *)dAttrs{
    for (int i = 0; i < [pages count]; i++) {
        Page *p = [pages objectAtIndex:i];
        if ([p.name isEqualToString:page]) {
            p.drillAttributes = dAttrs;
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionMiddle];
            [self popupPage:path];
            return;
        }
    }
    
    //Check the invisibles
    [self displayInvisiblePage:page drillAttributes:dAttrs];
}

- (void)displayInvisiblePage:(NSString *)page drillAttributes:(DrillAttributes *)dAttrs{
    for (int i = 0; i < [invisiblePages count]; i++){
        Page *p = [invisiblePages objectAtIndex:i];
        if ([p.name isEqualToString:page]) {
            p.drillAttributes = dAttrs;
            [self showPage:p];
            return;
        }
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *v = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 44)] autorelease];
	UILabel *l = [[[UILabel alloc] initWithFrame:CGRectMake(45, 0, tableView.frame.size.width, 44)] autorelease];
	l.opaque = NO;
	l.backgroundColor = [UIColor clearColor];
	l.textColor = [UIColor whiteColor];
	l.text = @"Pages";
	[v addSubview:l];
	return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 44.0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [pages count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	//cell.backgroundView = nil;
	//cell.backgroundColor = [UIColor grayColor];
	//cell.opaque = NO;
    //cell.alpha = 0.7;
	
	Page *page = [pages objectAtIndex:indexPath.row];
	
	cell.textLabel.text = [Util xmlUnescape:page.name];
	
    // Configure the cell...
    
    return cell;
}

// Title for the section
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	return @"Pages";
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.row;
    [self popupPage:indexPath];
}

- (void)popupPage:(NSIndexPath *)indexPath{
    [self showPage:[pages objectAtIndex:indexPath.row]];
}

- (void)showPage:(Page*)page{
    if(page.isPartial){
        NSString *body = 
        [NSString stringWithFormat:@"<ns:uuid>%@</ns:uuid><ns:pagePath>%@</ns:pagePath>", page.uuid, [page getPath]];
        
        __block ASIHTTPRequest* req = [BirstWS requestWithDashboard:@"getPage"];
        [BirstWS requestNSDashFormat:req body:body];
        
        //Success, parse the full page xml
        [req setCompletionBlock:^{
            NSString *pageXml = [Util getResponse:req];
            TBXML *tbxml = [TBXML tbxmlWithXMLString:pageXml];
            TBXMLElement *root = tbxml.rootXMLElement;
            TBXMLElement *pageEl = [Util traverseFindElement:root target:@"Page"];
            if(pageEl){
                [page parse:pageEl];
                [self showPageRenderViewController:page];
            }
        }];
        
        [req setFailedBlock:^{
            [Util requestFailedAlert:[req error]];
        }];
        
        [req startSynchronous];
    }else{
        //Already have full page information, just display
        [self showPageRenderViewController:page];
    }
		 
}

- (void)showPageRenderViewController:(Page*)page{
    [[NSNotificationCenter defaultCenter] postNotificationName:REMOVE_BACKGROUND_IMAGE object:nil];
     
    PageRenderViewController *renderCtrl = [[[PageRenderViewController alloc]init] autorelease];
	renderCtrl.page = page;
	renderCtrl.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.dashCtrl presentModalViewController:renderCtrl animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    NSLog(@"PagesViewController: didReceiveMemoryWarning retain count = %d", [self retainCount]);

    // Relinquish ownership any cached data, images, etc. that aren't in use.ƒ
}

- (void)viewDidUnload {
    NSLog(@"PagesViewController: viewWDidUnload");
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    NSLog(@"PagesViewController: dealloc %@", self);
    [pages release];
    [invisiblePages release];
    [super dealloc];
}


@end

