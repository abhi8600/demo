//
//  FlexFieldController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prompt.h"
#import "PageRenderViewController.h"

@interface FlexFieldController : UIViewController {
	Prompt *prompt;
	UITextField *input;
    UIDatePicker *datePicker;
    UIDatePicker *timePicker;
    NSString *origValue;
    BOOL renderedInDashlet;
    PageRenderViewController *pgCtrl;
}

@property (nonatomic, assign) Prompt *prompt;
@property (nonatomic, retain) UITextField *input;
@property (nonatomic, retain) UIDatePicker *datePicker;
@property (nonatomic, retain) UIDatePicker *timePicker;
@property (assign) BOOL renderedInDashlet;
@property (assign) PageRenderViewController *pgCtrl;
@property (copy) NSString *origValue;

- (NSMutableArray*)getSelectedValues;
- (void)dateChanged:(UIDatePicker*)picker;
- (void)timeChanged:(UIDatePicker*)picker;
- (void)useDefaultOption;
- (void)keyboardShown:(NSNotification *)notif;
- (void)keyboardHidden:(NSNotification *)notif;
- (void)inDashletApply;

@end
