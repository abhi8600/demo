//
//  Dashboard.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"
#import "Util.h"
#import "Page.h"


@interface Dashboard : NSObject {
	NSString *name;
	NSMutableArray *pages;
    NSMutableArray *invisiblePages;
}

@property (nonatomic, readonly) BOOL invisible;
@property (nonatomic,retain) NSString *name;
@property (nonatomic,readonly) NSMutableArray *pages;
@property (nonatomic,readonly) NSMutableArray *invisiblePages;

- (void) parse:(TBXMLElement *)xml;
- (Page*) findPageByName:(NSString*) pageName;
@end
