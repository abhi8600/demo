//
//  ColumnSelectorsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/20/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "ColumnSelectorsViewController.h"
#import "TBXML.h"
#import "Util.h"

@interface ColumnSelectorsViewController ()

@end

@implementation ColumnSelectorsViewController

@synthesize maxVC, tbxml, columnSelectors;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.columnSelectors = [[[NSMutableArray alloc]init] autorelease];
    int rowCount = 0;
    NSString *selectorXml = [maxVC.renderer.dashletCtrl selectorXmlFromFile];
    self.tbxml = [[[TBXML alloc] initWithXMLString:selectorXml]autorelease];
    TBXMLElement *root = self.tbxml.rootXMLElement;
    if(root){
        TBXMLElement *colSelectors = [Util traverseFindElement:root target:@"ColumnSelectors"];
        
        while (colSelectors && [[TBXML elementName:colSelectors] isEqualToString:@"ColumnSelectors"])
        {
            ColumnSelectors *cols = [[ColumnSelectors alloc]initSelectors];
           
            TBXMLElement *child = colSelectors->firstChild;
            do{
                NSString *name = [TBXML elementName:child];
                if([name isEqualToString:@"Selected"]){
                    cols.selectedValue = [TBXML textForElement:child];
                    cols.selectedElement = child;
                }else if ([name isEqualToString:@"Column"]){
                    cols.label = [TBXML textForElement:child];
                }else if ([name isEqualToString:@"ColumnSelector"]){
                    ColumnSelector *col = [[ColumnSelector alloc] init];
                    
                    TBXMLElement *el = child->firstChild;
                    while(el != nil){
                        NSString *tagName = [TBXML elementName:el];
                        NSString *tagValue = [TBXML textForElement:el];
                        if([tagName isEqualToString:@"Label"]){
                            col.label = tagValue;
                        }else if ([tagName isEqualToString:@"Dimension"]){
                            col.dimension = tagValue;
                        }else if ([tagName isEqualToString:@"Column"]){
                            col.column = tagValue;
                        }else if([tagName isEqualToString:@"ID"]){
                            col.ID = tagValue;
                        }
                        
                        el = el->nextSibling;
                    }
                   
                    [cols.selectors addObject:col];
                    [col release];
                    
                    rowCount++;
                }
            }while((child = child->nextSibling));
            
            [self.columnSelectors addObject:cols];
            [cols release];
            
            rowCount++;
            colSelectors = colSelectors->nextSibling;
        }
    }

    self.contentSizeForViewInPopover = (rowCount > 0) ? CGSizeMake(200, rowCount * 44) : CGSizeMake(200, 42);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.columnSelectors count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ColumnSelectors *cols = [self.columnSelectors objectAtIndex:section];
    return [cols.selectors count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    ColumnSelectors *cols = [self.columnSelectors objectAtIndex:section];
    return cols.label;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    ColumnSelectors *cols = [self.columnSelectors objectAtIndex:indexPath.section];
    
    ColumnSelector *c = [cols.selectors objectAtIndex:indexPath.row];
    if([[c dimensionColumn] isEqualToString:cols.selectedValue]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cols.selectedRow = indexPath.row;
	}else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
    
    cell.textLabel.text = c.label;

    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ColumnSelectors *cols = [self.columnSelectors objectAtIndex:indexPath.section];
    
    if(indexPath.row == cols.selectedRow){
        return;
    }
    
    ColumnSelector *sel = [cols.selectors objectAtIndex:indexPath.row];
    NSString *dimCol = [sel dimensionColumn];
    
    const char* t = [dimCol UTF8String];
    cols.selectedElement->text = (char *)t;

    NSString *xml = [Util toXmlString:self.tbxml.rootXMLElement output:[NSMutableString stringWithString:@""]];
    
    [self.maxVC columnSelectorChange:xml];
    
}

-(void) dealloc{
    [tbxml release];
    [columnSelectors release];
    [super dealloc];
}

@end

#pragma  mark - ColumnSelectors

@implementation ColumnSelectors

@synthesize selectedElement, label, selectedValue, selectedRow, selectors;

-(id) initSelectors{
    self = [super init];
    self.selectors = [[[NSMutableArray alloc]init] autorelease];
    return self;
}

-(void) dealloc{
    [label release];
    [selectors release];
    
    [super dealloc];
}

@end


#pragma  mark - ColumnSelector

@implementation ColumnSelector

@synthesize label, dimension, column, ID;

-(NSString *) dimensionColumn{
    if([self.ID length] > 0){
        return self.ID;
    }
    
    if([self.dimension length] > 0){
        return [NSString stringWithFormat:@"%@.%@", self.dimension, self.column];
    }
    return self.column;
}

-(void) dealloc{
    [label release];
    [dimension release];
    [column release];
    
    [super dealloc];
}

@end