//
//  DemoDashboardViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/29/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "PageRenderViewController.h"
#import "DashboardsCoverFlowViewController.h"
#import "PagesPopOverViewController.h"


typedef enum {
    RQ_DemoDashboard,
    RQ_DashboardModule,
    RQ_DashboardList,
    RQ_DashboardSettings,
}DashboardRequestTags;


@class PagesPopOverViewController;
@class DashboardsCoverFlowViewController;

@interface DashboardViewController : MainViewController<UIPopoverControllerDelegate>{
    BOOL isDemoDashboard;
    
    NSUInteger selectedIndex;
    NSUInteger pageSelectedIndex;
    NSString *spaceId;
    NSString *spaceName;
    NSMutableArray *dashboards;
    NSMutableArray *invisibleDashboards;
    
    PageRenderViewController *pageRenderViewController;
    UIPopoverController *popOverViewController;
    Dashboard *renderedDashboard;
    Page *renderedPage;
    
    DashboardsCoverFlowViewController *dashboardsCoverFlowController;
    PagesPopOverViewController *pagesPopOver;
    UIColor *backgroundColor;
}

@property (nonatomic, assign) BOOL isDemoDashboard;

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, assign) NSUInteger pageSelectedIndex;
@property (nonatomic, retain) NSString *spaceId;
@property (nonatomic, retain) NSString *spaceName;
@property (nonatomic, retain) NSMutableArray *dashboards;
@property (nonatomic, retain) NSMutableArray *invisibleDashboards;
@property (nonatomic, retain) PageRenderViewController *pageRenderViewController;
@property (nonatomic, retain) DashboardsCoverFlowViewController *dashboardsCoverFlowController;
@property (nonatomic, retain) UIPopoverController *popOverViewController;
@property (nonatomic, retain) Dashboard *renderedDashboard;
@property (nonatomic, retain) Page *renderedPage;
@property (nonatomic, retain) PagesPopOverViewController *pagesPopOver;
@property (retain) UIColor *backgroundColor;

-(void) parseDashboardList:(NSString *)xml;
-(void) initClearDashboards;

- (void) onRequestFinished:(ASIHTTPRequest*)request;
- (void) onRequestFailed:(ASIHTTPRequest *)request;

- (void) onGetDashboardModule:(ASIHTTPRequest *)request;
- (void) onGetDashboardList:(ASIHTTPRequest *)request;

- (void) parseDashboardList:(NSString *)xml;
- (void) refreshPage;
- (void) saveOffline;
- (void) applyPrompts;

- (void) showPagesPopOver;
- (void) showDashboardsCoverFlow;

- (void) onMaximizeDashlet:(NSNotification *)notification;
- (void) onDrillToDashboard:(NSNotification *)notification;
- (void) displayPage:(Dashboard*)dashboard page:(Page *)page screenshot:(BOOL)screenshot isSpaceScreenshot:(BOOL)isSpaceScreenshot;
@end
