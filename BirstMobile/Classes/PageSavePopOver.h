//
//  PageSavePopOver.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/4/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageRenderViewController.h"

@interface PageSavePopOver : UITableViewController {
	PageRenderViewController *pRender;
}

@property (nonatomic, assign) PageRenderViewController *pRender;

@end
