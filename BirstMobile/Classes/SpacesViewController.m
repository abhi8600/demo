//
//  SpacesViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 7/5/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "SpacesViewController.h"
#import "Settings.h"
#import "BirstWS.h"
#import "ASIHTTPRequest.h"
#import "TBXML.h"
#import "Space.h"
#import "UIViewController+JTRevealSidebarV2.h"
#import "DashboardViewController.h"
#import "Login.h"

@implementation SpacesViewController

@synthesize detail, initialSpaceIdx;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Request callbacks

- (void)onLoginSuccess:(ASIHTTPRequest *)request{
    [self dismissLogin];
    [self getAllSpaces];
}

- (void) getAllSpaces{
    [Settings setLoginMode:spaceListing];
    
    ASIHTTPRequest *spRequest = [BirstWS requestWithAdmin:@"GetAllSpaces"];
    spRequest.tag = GetAllSpaces;
    [BirstWS requestFormat:spRequest body:@""];
    [self.networkQueue addOperation:spRequest];   
}

- (void)onGetAllSpaces:(ASIHTTPRequest *)request {
	NSString *responseString = [request responseString];
    
    //TODO - check response
	NSLog(@"getAllSpaces response: %@", responseString);
    
    //Parse spaces
    [self parseAllSpaces:responseString];
	
}

- (void)onGetSpaceDetail:(ASIHTTPRequest *)request {
    NSString *responseString = [request responseString];
    NSLog(@"getSpaceDetail response: %@", responseString);
    
    NSString *errMsg = [Util checkForError:responseString];
    if(errMsg != nil){
        [Util alert:[NSString stringWithFormat:@"Unable to retrieve space details: %@", errMsg] title:@"Space details request error"];
        return;
    }

    if(self.detail != nil){
        [self.detail.view removeFromSuperview];
        [detail release];
        detail = nil;
    }
    
   [self parseSpaceDetailSetSpace:responseString];
    
}

- (void) onRequestFinished:(ASIHTTPRequest*)request {
    [self removePendingRequest];
    
    switch (request.tag){
        case GetAllSpaces:
            [self onGetAllSpaces:request];
            break;
        case GetSpaceDetail:
            [self onGetSpaceDetail:request];
            break;
        default:
            break;
    }
}

- (void) parseAllSpaces:(NSString*)xml{
	self.title = @"Spaces";
	
    NSMutableArray *spaces = [[NSMutableArray alloc]init];
    
	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *body = [TBXML childElementNamed:@"soap:Body" parentElement:root];
		if (body != nil) {
			TBXMLElement *response = [TBXML childElementNamed:@"GetAllSpacesResponse" parentElement:body];
			if (response != nil) {
				TBXMLElement *result = [TBXML childElementNamed:@"GetAllSpacesResult" parentElement:response];
				if (result != nil) {
                    TBXMLElement *error = [TBXML childElementNamed:@"Error" parentElement:result];
                    if(error != nil){
                        TBXMLElement *errorType = [TBXML childElementNamed:@"ErrorType" parentElement:error];
                        if(errorType != nil){
                            NSString *errTypeStr = [TBXML textForElement:errorType];
                            int errValue = [errTypeStr intValue];
                            if(errValue != 0){
                                
                            }
                        }
                    }
					TBXMLElement *spaceList = [TBXML childElementNamed:@"SpacesList" parentElement:result];
					if (spaceList != nil) {
						TBXMLElement *firstSummary = [TBXML childElementNamed:@"SpaceSummary" parentElement:spaceList];
						if (firstSummary != nil) {
							do {
								Space *obj = [[Space alloc]init];
								[obj parse:firstSummary];
								[spaces addObject:obj];
								[obj release];
							} while ((firstSummary = firstSummary->nextSibling));
						}
					}
				}//result != nil
			}
		}
	}
    [tbxml release];
	
    if([spaces count] > 0){
        [self revealLeftSideBar:self];
        self.leftSidebarViewController.spaces = spaces;
        
        [self.leftSidebarViewController.tableView reloadData];
        
        //TODO: Get the designated selected space
        //      For now, show the first one
        [self.leftSidebarViewController.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:IDX_HDR_SPACE ] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self getSpaceDetailAt:0];
    }
    [spaces release];
}

-(void)parseSpaceDetailSetSpace:(NSString *)xml{
    TBXML *tbxml = [TBXML tbxmlWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *body = [TBXML childElementNamed:@"soap:Body" parentElement:root];
		if (body != nil) {
			TBXMLElement *response = [TBXML childElementNamed:@"GetSpaceDetailsResponse" parentElement:body];
			if (response != nil) {
				TBXMLElement *result = [TBXML childElementNamed:@"GetSpaceDetailsResult" parentElement:response];
				if (result != nil) {
                    
                    Space *sp = [[Space alloc]init];
                    [sp parse:result];
                    
                    self.detail = [[[SpaceDetailViewController alloc]init] autorelease];
                    self.detail.space = sp;
                    [sp release];
                    
                    self.detail.view.frame = self.view.frame;
                    self.detail.parentViewCtrl = self;
                    [self.view addSubview:detail.view];
                    [Settings setSpaceId:self.detail.space.spaceId];
                    
                    return;
							
				}//result != nil
			}
		}
	}	
}
    
-(void) displayDashboard{
    DashboardViewController *db = [[DashboardViewController alloc]init];
    db.isDemoDashboard = NO;
    [self displaySubView:self.leftSidebarViewController mainVC:db revealSideBar:NO];
    [db release];
}

#pragma mark - Clicked on a space

- (void)sidebarViewController:(SidebarViewController *)sidebarViewController didSelectObject:(NSObject *)object atIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController setRevealedState:JTRevealedStateNo];
    if(indexPath.section == IDX_HDR_SPACE){
        if([Settings loggedIn]){
            if([sidebarViewController.spaces count] > 0){
                [self getSpaceDetailAt:indexPath.row];
            }else{
                [self getAllSpaces];
            }
        }else{
            [self checkLoginGetSpaces];
        }
    }else{
        [super sidebarViewController:sidebarViewController didSelectObject:object atIndexPath:indexPath];
    }
}

- (void)getSpaceDetailAt:(NSInteger)index{
    Space *space = [self.leftSidebarViewController.spaces objectAtIndex:index];
    
    if(space != nil){
        if(self.detail != nil){
            [self.detail.view removeFromSuperview];
            [detail release];
            detail = nil;
        }

        [self showPendingRequest:[NSString stringWithFormat:@"Loading space %@...", space.name]];
        self.title = space.name;
        ASIHTTPRequest *request = [BirstWS requestWithAdmin:@"GetSpaceDetails"];
        request.tag = GetSpaceDetail;
        NSString *body = [NSString stringWithFormat:@"<tns:spaceID>%@</tns:spaceID><tns:spaceName>%@</tns:spaceName>", space.spaceId, space.name];
        [BirstWS requestFormat:request body:body];
        [self.networkQueue addOperation:request];
    }
}

- (void)checkLoginGetSpaces{
    if(![Settings loggedIn]){
        if([Settings isUsernamePasswordNotEmpty]){
            [self showPendingRequest:@"Listing all spaces..."];            
            [Login performLogin:[Settings username] password:[Settings password] startAnimate:nil endAnimate:@selector(removePendingRequest) delegate:self loginSuccess:@selector(getAllSpaces) loginFail:nil];
        }
    }
}

- (void)loginFailed:(id)request
{
    [self displayLogin:self.leftSidebarViewController];
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Spaces";
}

-(void)viewWillAppear:(BOOL)animated{
    if(![Settings loggedIn]){
        if([Settings isUsernamePasswordNotEmpty]){
            [self showPendingRequest:@"Listing all spaces..."];            
            [Login performLogin:[Settings username] password:[Settings password] startAnimate:nil endAnimate:@selector(removePendingRequest) delegate:self loginSuccess:@selector(getAllSpaces) loginFail:@selector(loginFailed:)];
        }else{
            [self displayLogin:self.leftSidebarViewController];
            [self.leftSidebarViewController setLastSelectedIndexPath];
            return;
        }
    }else{
        if(self.initialSpaceIdx >= 0 && [self.leftSidebarViewController.spaces count] > 0){
            [self getSpaceDetailAt:self.initialSpaceIdx];
        }else{
            [self getAllSpaces];
        }
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
