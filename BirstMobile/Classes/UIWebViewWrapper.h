//
//  UIWebViewWrapper.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/24/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Util.h"
#import "TBXML.h"

@interface UIWebViewWrapper : UIView<UIWebViewDelegate>{
    UIWebView *webview;
    NSString *anychartXml;
    id delegate;
    BOOL isDummy;
    BOOL disableDrilling;
    BOOL isNotSupportedType;
}

@property (retain) UIWebView *webview;
@property (retain) NSString *anychartXml;
@property (retain) id delegate;
@property (assign) BOOL disableDrilling;

-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f;
-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f isDummy:(BOOL)dummy;

- (void) resize:(CGRect)f;
-(NSString *) parse:(TBXMLElement *)xml output:(NSMutableString *)output;
- (void)escapeJS:(NSMutableString *)str;

@end

@protocol UIWebViewWrapperDeleage <NSObject>

- (void) doneJS:(id)sender;
- (void) resizeJS;

@end