//
//  Page.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TBXML.h"
#import "Prompt.h"
#import "DrillAttributes.h"
#import "Layout.h"

@interface Page : NSObject {
	NSString *name;
	NSString *uuid;
	NSString *dir;
	NSString *modifiable;
    NSString *bgColor;
    
    BOOL invisible;
    
	NSMutableArray *prompts;
    NSMutableArray *visiblePrompts;
	NSMutableArray *dashlets;
	NSMutableDictionary *promptsHash;
    NSMutableDictionary *parentChildrenPrompts;
    
    //Temporary drill attributes passed from one page to another in drill to dashboard
    DrillAttributes *drillAttributes;
    BOOL requireSessionVars;
     
    BOOL renderedDashletsCached;
	
	//Offline settings
	BOOL isOffline;
	NSString *offlineDir;
    
    //Optimized, partial
    BOOL isPartial;
    
    Layout *layout;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSString *dir;
@property (nonatomic, retain) NSString *modifiable;
@property (nonatomic, retain) NSString *bgColor;
@property (nonatomic, assign) BOOL invisible;
@property (nonatomic, readonly) NSMutableArray *dashlets;
@property (nonatomic, readonly) NSMutableArray *prompts;
@property (nonatomic, readonly) NSMutableArray *visiblePrompts;
@property (nonatomic, readonly) NSMutableDictionary *promptsHash;
@property (nonatomic, retain) DrillAttributes *drillAttributes;
@property (nonatomic, assign) BOOL requireSessionVars;
@property (nonatomic, assign) BOOL renderedDashletsCached;
@property (nonatomic, assign) BOOL isOffline;
@property (nonatomic, assign) NSString *offlineDir;
@property (nonatomic, assign) BOOL isPartial;
@property (nonatomic, retain) Layout *layout;

- (void) parse:(TBXMLElement*)xml;
- (void) recurseFindDashlets:(TBXMLElement *)root;
- (NSMutableArray*) dashlets;
- (void) parsePrompts:(TBXMLElement *)prompts;
- (NSString *) getPromptsXml;
-(NSString *)getPromptsXmlValues:(BOOL)setSelectedValues encode:(BOOL)encode ignoreList:(NSSet*)ignoreList;
- (void) parsePromptValues:(NSString *)values isParentChildren:(BOOL)isParentChildren;
- (void) resetPromptValues;
- (void) useDrillFilters;
- (NSString *) toXml:(NSString *)backgroundColor;
- (void) updateGlobalPromptValues;
- (void) setValuesByGlobalPromptValues;
- (BOOL) isGlobalPromptsInSync;
- (BOOL) hasParentPrompts;
- (NSString*) getParentChildPromptsXmlValues;
- (BOOL) setPromptEqualsDrillFilterValue:(NSString*)filterStr;
- (BOOL) setPromptValueIfEqual:(Prompt*)p value:(NSString*)promptValue;
- (void) addDrillDownPrompts:(NSString *)xml;
- (NSString*) getPath;
@end
