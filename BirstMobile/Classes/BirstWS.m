//
//  BirstWS.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "BirstWS.h"
#import "Settings.h"
#import "Log.h"

//hardcode for now
static NSString *ADMIN = @"/AdminService.asmx";
static NSString *DASHBOARD = @"/SMIWeb/services/Dashboard.DashboardHttpSoap11Endpoint/";
static NSString *ADHOC = @"/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint/";
static NSString *OPERATION = @"/SMIWeb/services/OperationHandler.OperationHandlerHttpSoap11Endpoint/";
static NSString *INIT = @"/SMIWeb/services/ClientInitData.ClientInitDataHttpSoap11Endpoint/";
static NSString *SP_HEAD = 
@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:tns=\"http://www.birst.com\"><SOAP-ENV:Body>";
static NSString *SP_END = @"</SOAP-ENV:Body></SOAP-ENV:Envelope>";

@implementation BirstWS

//@synthesize data, operation, tns, request;

//admin webservice call
+ (ASIHTTPRequest*) requestWithAdmin:(NSString*)op{
	return [BirstWS requestWithUrl:[self sanitizePort:ADMIN newPort:6105] prefix:@"http://www.birst.com/" operation:op];
}

//dashboard webservice call
+ (ASIHTTPRequest*) requestWithDashboard:(NSString*)op{
	return [BirstWS requestWithUrl:[self sanitizePort:DASHBOARD newPort:6103] prefix:@"urn:" operation:op];
}

//adhoc webservice call
+ (ASIHTTPRequest*) requestWithAdhoc:(NSString *)op{
	return [BirstWS requestWithUrl:[self sanitizePort:ADHOC newPort:6103] prefix:@"urn:" operation:op];
}

//adhoc webservice call
+ (ASIHTTPRequest*) requestWithClientInit:(NSString *)op{
	return [BirstWS requestWithUrl:[self sanitizePort:INIT newPort:6103] prefix:@"urn:" operation:op];
}

//Write back
+ (ASIHTTPRequest*) requestWithOperationHandler:(NSString*)op{
	return [BirstWS requestWithUrl:[self sanitizePort:OPERATION newPort:6103] prefix:@"urn" operation:op];
}

+ (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port{
	NSString *server = [Settings server];
	
	//Switch 6105 port to 6103 for development server
	NSRange range = [server rangeOfString:@":6105"];
	if (range.length > 0 && port != 6105) {
		NSString* newUrl = [NSString stringWithFormat:@"%@:%d%@", [server substringToIndex:range.location], port, suffix];
		[Log debug:@"sanitizePort: %@", newUrl];
		return newUrl;
	}
	
	NSString *sUrl = [NSString stringWithFormat:@"%@%@", [Settings server], suffix];
    [Log debug:@"sanitizePort: %@", sUrl];
	return sUrl;
}

+ (ASIHTTPRequest*) requestWithUrl:(NSString*)link prefix:(NSString*)pref operation:(NSString*)op{
    if (!op) {
        op = @"";
    }
    
    NSURL* url = [NSURL URLWithString:link];
    ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:url];
    //temp
    [request addRequestHeader:@"operation" value:op];
    [request addRequestHeader:@"tns" value:pref];
    //required
    [request addRequestHeader:@"SOAPAction" value: [NSString stringWithFormat:@"\"%@%@\"", pref, op]];
    [request addRequestHeader:@"Content-type" value: @"text/xml; charset=utf-8"];
    
    [Settings addCSRFHeader:request];
	
	return request;
}

+ (void) asyncRequest:(ASIHTTPRequest*)request body:(NSString*)body delegate:(id)del{
	[BirstWS asyncRequest:request body:body delegate:del requestFinished:nil requestFailed:nil];
}

+ (void) requestFormat:(ASIHTTPRequest *)request body:(NSString *)body{
    NSString* tns = [request.requestHeaders objectForKey:@"tns"];
    NSString *operation = [request.requestHeaders objectForKey:@"operation"];
	NSString* envelope = [NSString stringWithFormat:@"%@<tns:%@ xmlns:tns=\"%@\">%@</tns:%@>%@", SP_HEAD, operation, tns, body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
}

+ (void) asyncRequest:(ASIHTTPRequest *)request body:(NSString*)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed {
    
    [BirstWS requestFormat:request body:body];
    
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}
	
	[request startAsynchronous];
}

+ (void) asyncRequestNS:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del{
	[BirstWS asyncRequestNS:request body:body delegate:del requestFinished:nil requestFailed:nil];
}

+ (void) requestNSFormat:(ASIHTTPRequest*)request body:(NSString *)body operationName:(NSString *)operationName{
    NSString *url = [NSString stringWithFormat:@"http://%@.WebServices.successmetricsinc.com", operationName];
    
    NSString *operation = [request.requestHeaders objectForKey:@"operation"];
	NSString* envelope = [NSString stringWithFormat:@"%@<ns:%@ xmlns:ns=\"%@\">%@</ns:%@>%@", SP_HEAD, operation, url, body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];

}

+ (void) requestNSDashFormat:(ASIHTTPRequest*)request body:(NSString *)body{
    NSString *operation = [request.requestHeaders objectForKey:@"operation"];
	NSString* envelope = [NSString stringWithFormat:@"%@<ns:%@ xmlns:ns=\"%@\">%@</ns:%@>%@", SP_HEAD, operation, @"http://dashboard.WebServices.successmetricsinc.com", body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
}

+ (void) asyncRequestNSDash:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed{
    
    [BirstWS requestNSDashFormat:request body:body];
    
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}
    
	
	[request startAsynchronous];
}


+ (void) asyncRequestNS:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed{
    NSString *operation = [request.requestHeaders objectForKey:@"operation"];
	NSString* envelope = [NSString stringWithFormat:@"%@<ns:%@ xmlns:ns=\"%@\">%@</ns:%@>%@", SP_HEAD, operation, @"http://adhoc.WebServices.successmetricsinc.com", body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}
    
	
	[request startAsynchronous];
}

+ (ASINetworkQueue*) createNetworkQueueMaxOne:(id)delegate finishSel:(SEL)finishSel failSel:(SEL)failSel{
    return [BirstWS createNetworkQueue:1 delegate:delegate finishSel:finishSel failSel:failSel];
}

+ (ASINetworkQueue*) createNetworkQueue:(int)count delegate:(id)delegate finishSel:(SEL)finishSel failSel:(SEL)failSel{
    ASINetworkQueue *networkQueue = [ASINetworkQueue queue];
    [networkQueue setMaxConcurrentOperationCount:count];
    [networkQueue setDelegate:delegate];
    [networkQueue setRequestDidFinishSelector:finishSel];
    [networkQueue setRequestDidFailSelector:failSel];
    [networkQueue go];
    
    return networkQueue;
}

@end
