//
//  Dashboard.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Dashboard.h"


@implementation Dashboard
@synthesize name;

- (void) parse:(TBXMLElement *)xml{

	if (xml != nil) {
		
		//dashboard values
		self.name = [TBXML valueOfAttributeNamed:@"name" forElement:xml];
		
		//parse pages
        [self.pages removeAllObjects];
        
		TBXMLElement *page = [Util traverseFindElement:xml target:@"Page"];
		if (page != nil) {
			do {
				if ([[TBXML elementName:page] isEqualToString:@"Page"]) {
					Page *p = [[Page alloc] init];
					[p parse:page];
                    if(p.invisible){
                        [self.invisiblePages addObject:p];
                    }else{
                        [self.pages addObject:p];
                    }
                    [p release];
				}
			} while ((page = page->nextSibling));
		}
	}
}

- (NSMutableArray *) pages{
    if (pages == nil) {
        pages = [[NSMutableArray alloc]init];
    }
    return pages;
}

- (NSMutableArray *) invisiblePages{
    if(!invisiblePages){
        invisiblePages = [[NSMutableArray alloc]init];
    }
    return invisiblePages;
}

- (BOOL) invisible{
    return [self.pages count] == 0 && [self.invisiblePages count] > 0;
}

- (Page*) findPageByName:(NSString*) pageName{
    for(int i = 0; i < [pages count]; i++){
        Page* p = [pages objectAtIndex:i];
        if ([pageName isEqualToString:p.name]){
            return p;
        }
    }
     
    for(int i = 0; i < [invisiblePages count]; i++){
        Page *p = [invisiblePages objectAtIndex:i];
        if([pageName isEqualToString:p.name]){
            return p;
        }
    }
    return nil;
}

- (void) dealloc{
    [pages release];
    [invisiblePages release];
    [super dealloc];
}

@end
