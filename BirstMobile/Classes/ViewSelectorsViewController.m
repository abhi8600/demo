//
//  ViewSelectorsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/16/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "ViewSelectorsViewController.h"

@interface ViewSelectorsViewController ()

@end

@implementation ViewSelectorsViewController

@synthesize useFullChartList, selectedChart, maxVC, fullChartList, tableOrChartList, originalChartType;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.fullChartList = [NSArray arrayWithObjects:@"Pie", @"Bar", @"Column",  @"Line", @"table", nil];
    
    self.tableOrChartList = [NSArray arrayWithObjects:@"Composite", @"table", nil];
    
    for(int i = 0; i < [self.fullChartList count] - 1; i++){
        NSString *c = [self.fullChartList objectAtIndex:i];
        if([c isEqualToString:self.originalChartType]){
            self.useFullChartList = YES;
        }
    }
    
    self.contentSizeForViewInPopover = (self.useFullChartList) ? CGSizeMake(200, 210) : CGSizeMake(200, 85);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) dealloc{
    [fullChartList release];
    [tableOrChartList release];
    
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.useFullChartList) ? 5 : 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSString *cellTitle = @"";
    
    if(self.useFullChartList){
        switch (indexPath.row) {
            case 0:
                cellTitle = @"Pie";
                break;
            case 1:
                cellTitle = @"Bar";
                break;
            case 2:
                cellTitle = @"Column";
                break;
            case 3:
                cellTitle = @"Line";
                break;
            case 4:
                cellTitle = @"Table";
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:
                cellTitle = @"Chart";
                break;
            case 1:
                cellTitle = @"Table";
                break;
            default:
                break;
        }
    }
    
    NSString *cellTitleLower = [cellTitle lowercaseString];
    if([cellTitleLower isEqualToString:[self.selectedChart lowercaseString]]){
        selectedRow = indexPath.row;
    }
    
    cell.textLabel.text = cellTitle;
    
    return cell;
}

- (void)viewDidAppear:(BOOL)animated{
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedRow){
        return;
    }
    
    NSString *chart = 
        (self.useFullChartList) ? 
            [self.fullChartList objectAtIndex:indexPath.row] : [self.tableOrChartList objectAtIndex:indexPath.row];

    [self.maxVC chartSelectorChange:chart];
    
}

@end
