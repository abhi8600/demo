//
//  ServerSettingsEditViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 4/14/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "ServerSettingsEditViewController.h"


@implementation ServerSettingsEditViewController

@synthesize settingsCtrl, addedNewServer, cellTextField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [cellTextField release];
    [addedNewServer release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setEditing:YES];
    isDirty = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonSystemItemAdd target:self action:@selector(addServer)];
}

- (void)addServer{
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.contentSizeForViewInPopover = CGSizeMake(300, 250);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
     self.contentSizeForViewInPopover = CGSizeMake(300, 250);
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.settingsCtrl.tableView reloadData];
    if (isDirty) {
        [self.settingsCtrl saveSavedSettings];
        isDirty = NO;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.settingsCtrl.savedSettings objectForKey:@"servers"] count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *EditCellIdentifier = @"EditCell";
    
    UITableViewCell *cell;
    
    // Configure the cell...
    NSArray *servers = [self.settingsCtrl.savedSettings objectForKey:@"servers"];
    if ([servers count] > indexPath.row) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }

        cell.textLabel.text = [servers objectAtIndex:indexPath.row];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:EditCellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    
            CGRect f = CGRectMake(cell.bounds.origin.x + 45, cell.bounds.origin.y + 5, cell.bounds.size.width - 80, cell.bounds.size.height - 10);
            self.cellTextField = [[[UITextField alloc]initWithFrame:f] autorelease];
            //[self.cellTextField setEnabled:YES];
            self.cellTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            self.cellTextField.borderStyle = UITextBorderStyleRoundedRect;
            self.cellTextField.delegate = self;
            [self.cellTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            [cell addSubview:self.cellTextField];
        }
        self.cellTextField.text = @"";
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

- (void)textFieldDidChange:(UITextField *)textField{
    self.addedNewServer = textField.text;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSArray *servers = [self.settingsCtrl.savedSettings objectForKey:@"servers"];
    if ([servers count] - 1 < indexPath.row){
        return UITableViewCellEditingStyleInsert;
    }
    
    return UITableViewCellEditingStyleDelete;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *servers = [self.settingsCtrl.savedSettings objectForKey:@"servers"];

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [servers removeObjectAtIndex:indexPath.row];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        isDirty = YES;
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        if (self.addedNewServer) {
            [self.cellTextField resignFirstResponder];
            
            [servers addObject:self.addedNewServer];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            
            self.addedNewServer = nil;
            self.cellTextField.text = nil;
            isDirty = YES;
             
        }
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
