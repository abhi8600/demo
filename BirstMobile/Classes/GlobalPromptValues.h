//
//  GlobalPromptValues.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/3/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GlobalPromptValues : NSObject {
    NSMutableDictionary *keyValues;
}

- (id) getValue:(NSString*) key;
- (void) setValue:(id)value key:(NSString*)key;
- (void) reset;

+ (GlobalPromptValues *) instance;
@end
