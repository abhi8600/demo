//
//  SessionMaintainer.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

#define POLL_INTERVAL 8*60

@interface SessionMaintainer : NSObject {
    NSTimer *timer;
    ASIHTTPRequest *pollRequest;
}

@property (nonatomic, retain) ASIHTTPRequest *pollRequest;

+ (SessionMaintainer*)instance;

- (void) start;
- (void) stop;
- (BOOL) isPolling;
- (void) poll;
- (void) clearRequest;
- (void) requestFinished:(ASIHTTPRequest *)request;
- (void) requestFailed:(ASIHTTPRequest*)request;

@end
