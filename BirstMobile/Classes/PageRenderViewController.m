    //
//  PageRenderViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//
#import <QuartzCore/QuartzCore.h> //To remove renderInContext() warning
#import "PageRenderViewController.h"
#import "Dashlet.h"
#import "DashletController.h"
#import "Util.h"
#import "PageSavePopOver.h"
#import "Settings.h"
#import "Notification.h"
#import "SessionVariables.h"
#import "BirstWS.h"
#import "Log.h"
#import "SegmentedCheckboxController.h"

@implementation PageRenderViewController

@synthesize page, savePage, promptsCtrl, saveCtrl, initPromptsXml, drillToDashboardNotification, reqPendingCtrl, viewMode, noReportTitle, screenShot, isSpaceScreenshot, dashletPrompts, backgroundColor, getFilterRequest, isParentRequestPending;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        viewMode = Regular;
    }
    return self;
}


- (IBAction) done{
	[self dismissModalViewControllerAnimated:YES];
}

//Event listener for the settings button
- (IBAction) doPrompts{
    [Log debug:@"doPrompts"];
	
    [self dismissSavePopover];
    
	if (self.promptsCtrl != nil && [self.promptsCtrl isPopoverVisible]) {
		[self dismissPromptPopover];
	}else {
		PromptsViewController *ctrl = [[[PromptsViewController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
		ctrl.prompts = page.visiblePrompts;
		ctrl.pgCtrl = self;
		
		UINavigationController *nc = [[[UINavigationController alloc]initWithRootViewController:ctrl]autorelease];
        
        UIPopoverController *pC = [[UIPopoverController alloc] initWithContentViewController:nc];
		self.promptsCtrl = pC;
        [pC release];
        
		nc.delegate = self;
        
		//promptsCtrl.popoverContentSize = CGSizeMake(320, 250);
		[promptsCtrl presentPopoverFromBarButtonItem:prompts permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
}

//Resize the popover controller based on the sizes indicated by each PromptViewController
//as it appears in the navigation stack
- (void) navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
        self.promptsCtrl.popoverContentSize = viewController.contentSizeForViewInPopover;
}

- (IBAction) refresh{
    
	[promptsSvc release];
	promptsSvc = nil;
	 
    self.page.renderedDashletsCached = NO;
	[self.page resetPromptValues];
	[self renderPage];
}

- (IBAction) save{
    [self dismissPromptPopover];
    
	if (self.saveCtrl != nil && [self.saveCtrl isPopoverVisible]) {
		[self dismissSavePopover];
	}else {
		PageSavePopOver	*ctrl = [[[PageSavePopOver alloc] initWithStyle:UITableViewStylePlain] autorelease];
		ctrl.pRender = self;
        
        UIPopoverController *pC = [[UIPopoverController alloc] initWithContentViewController:ctrl];
        self.saveCtrl = pC;
        [pC release];
        
		self.saveCtrl.popoverContentSize = CGSizeMake(300, 42);
		[saveCtrl presentPopoverFromBarButtonItem:savePage permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
	
}

- (void) saveOffline{
	//We must have rendered the dashlets and cached the results
	if([self.page.dashlets count] > 0 && self.page.renderedDashletsCached){
		NSFileManager *filemgr = [NSFileManager defaultManager];
		
		NSString *offlineDir = [Settings offlineDir:filemgr];
		
		NSString *offlinePageDir = [NSString stringWithFormat:@"%@/%@.offline", offlineDir, page.uuid];
		[Log debug:@"writing to offline dir %@", offlinePageDir];
		
		NSError *error;
		
		//TODO: Alert user that we'll delete the previous
		if (![filemgr removeItemAtPath:offlinePageDir error:&error]){
			//Todo - alert here and return
			[Log debug:@"Cannot remove offline directory %@ because %@", offlinePageDir, [error localizedDescription]];
		}
		
	
		if(![filemgr createDirectoryAtPath:offlinePageDir withIntermediateDirectories:YES attributes:nil error: &error]){
			[Log debug:@"cannot create directory %@ because %@", offlinePageDir, [error localizedDescription]];
			return;
		}
		
		//Grab image of the dashlet
		UIGraphicsBeginImageContext(self.view.bounds.size);
        [self.view.layer renderInContext: UIGraphicsGetCurrentContext()];
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		NSData *imgData = UIImagePNGRepresentation(image);
		NSError *err;
		if(![imgData writeToFile:[NSString stringWithFormat:@"%@/cover.png", offlinePageDir] options:NSDataWritingFileProtectionComplete error:&err]){
			[Log debug:@"Image write to directory failed reason: %@", [err localizedDescription]];
		}else {
			NSDate *currDate = [NSDate date];
			NSString *dateStr = [currDate descriptionWithLocale:nil];
			
			//Write the data saving information
			NSMutableDictionary *info = [[[NSMutableDictionary alloc]init]autorelease];

			[Util setDict:info key:@"pageName" value:page.name];
			[Util setDict:info key:@"saveDate" value:dateStr];
			[Util setDict:info key:@"pageUuid" value:page.uuid];
			[Util setDict:info key:@"username" value:[Settings username]];
			[Util setDict:info key:@"space" value:[Settings space] ];
			[Util setDict:info key:@"server" value:[Settings server]];
			
			if(![info writeToFile:[NSString stringWithFormat:@"%@/Info.plist", offlinePageDir] atomically:YES]){
				[Log debug:@"Unable to write Info.plist"];
			}
			
			//NSString *info = [NSString stringWithFormat:@"%@.\nSaved by %@ . %@", page.name, [Settings username], dateStr];
			//[info writeToFile:[NSString stringWithFormat:@"%@/info", offlinePageDir] atomically:YES encoding:NSUnicodeStringEncoding error:nil];
		}
		
		
		//Write the page information
		NSString *pageXml = [page toXml:[Util hexFromUIColor:self.backgroundColor]];
        
		[pageXml writeToFile:[NSString stringWithFormat:@"%@/page.xml", offlinePageDir] atomically:YES encoding:NSUnicodeStringEncoding error:nil];
			
		//Write the report xml
		for (int i = 0; i < [self.page.dashlets count]; i++) {
			Dashlet *dash = [self.page.dashlets objectAtIndex:i];
			NSString *dashPath = [NSString stringWithFormat:@"%@.rendered", [dash pathify]];
			NSString *toPath = [NSString stringWithFormat:@"%@/%@", offlinePageDir, dashPath];
			NSString *fromPath = [Settings dashletReportCachePath:self.page.uuid dashletName:dashPath];
			if (! [filemgr copyItemAtPath:fromPath toPath:toPath error:&err]){
				[Log debug:@"failed to copy file %@ to %@ because %@", fromPath, toPath, [err localizedDescription]];
			}
		}
		
	}else{
		//Warn user that dashlets havent' rendered for us to save offline
		NSString *msg = @"Please wait until all dashlets have rendered before saving offline";
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Cannot save" message:msg delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
		[alert show];
		[alert release];
		
	}
	
	[self dismissSavePopover];
}

- (void) dismissSavePopover{
	if (self.saveCtrl) {
		[saveCtrl dismissPopoverAnimated:YES];
		[saveCtrl release];
		saveCtrl = nil;
	}
}

- (void) dismissPromptPopover{
	if (self.promptsCtrl) {
		[promptsCtrl dismissPopoverAnimated:YES];
		[promptsCtrl release];
		promptsCtrl = nil;
	}
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBar.translucent = NO;
    if ([self respondsToSelector:@selector(extendedLayoutIncludesOpaqueBars)])
        self.extendedLayoutIncludesOpaqueBars = NO;
    

    [Log debug:@"PageRenderViewController viewDidLoad"];

    if(self.noReportTitle){
        reportTitle.hidden = YES;
    }
    
    if(self.viewMode == Offline){
        self.title = [self getPageTitle];
        [self hideBarButton:prompts];
        [self hideBarButton:savePage];
    }else if(self.viewMode == Oem){
        [self hideBarButton:savePage];
    }
    
    numReportFileWrites = 0;
    
    if(!self.page.renderedDashletsCached){
        [self.page resetPromptValues];
    }else{
        //Check that we need to render again
        if (![self.page isGlobalPromptsInSync]) {
            self.page.renderedDashletsCached = NO;
        }
    }
    
    //Parse the drill attributes passed when doing a drill to dashboard
	//We re-render again
    if (self.page.drillAttributes) {
		self.page.renderedDashletsCached = NO;
        [self.page useDrillFilters];
    }
    
    //Global prompt values
    [self.page setValuesByGlobalPromptValues];
    
   
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    
     //To be notified of drill to dashboard events
    [ctr addObserver:self selector:@selector(drillToDashboard:) name:DRILL2DASHBOARD object:nil];
    
    // To be notified of drilldown events
    [ctr addObserver:self selector:@selector(drillDown:) name:DRILLDOWN object:nil];
    
    // To be notified of drill to url
    [ctr addObserver:self selector:@selector(drillToUrl:) name:DRILL2URL object:nil];
    
	//To be notified of parent prompt value changes
    [ctr addObserver:self selector:@selector(parentPromptValueChanged:) name:PARENTPROMPTVALUECHANGED object:nil];
    
    //To be notified of rendered done for screen capture
    [ctr addObserver:self selector:@selector(captureScreenshot) name:RENDERED object:nil];
    
    scrollView.delegate = self;
	scrollView.minimumZoomScale = 1;
	scrollView.maximumZoomScale = 6.0;
    
    self.dashletPrompts = [[[NSMutableArray alloc]init]autorelease];
    
}

- (void)hideBarButton:(UIBarButtonItem*)item{
    [item setTitle:@""];
    [item setStyle:UIBarButtonItemStylePlain];
    [item setEnabled:FALSE];
}

- (void)parentPromptValueChanged:(NSNotification*)notification{
    [self getParentPromptsValues];
}

- (void)drillToDashboard:(NSNotification*)notification{
    //Set the notification for when the view disappears
    self.drillToDashboardNotification = [NSNotification notificationWithName:DISPLAYDASHBOARD object:self userInfo:[notification userInfo]];

    [self dismissModalViewControllerAnimated:YES];
}

- (void)drillDown:(NSNotification *)notification{
    DrillAttributes *drillAttrs = [[notification userInfo] objectForKey:@"drillAttributes"];
    NSString* filters = drillAttrs.filters;
    
    //Existing prompt?
    isDrillDownPromptExist = [self.page setPromptEqualsDrillFilterValue:filters];
       
    //Find the dashlet which invoked the notification and invoke the drilldown call
    NSString *reportPath = [drillAttrs.targetURL stringByReplacingOccurrencesOfString:@"/" withString:@""];
    for(int i = 0; i < [viewCtrls count]; i++){
        DashletController *d = [viewCtrls objectAtIndex:i];
        NSString *dashletReportPath = [d.dashlet.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
        dashletReportPath = [dashletReportPath stringByReplacingOccurrencesOfString:@".jasper" withString:@""];
        dashletReportPath = [dashletReportPath stringByReplacingOccurrencesOfString:@".AdhocReport" withString:@""];
        if ([reportPath isEqualToString:dashletReportPath]) {
            drillDownDashlet = d;
            NSString *reportXml = [Util xmlSimpleEscape:[NSMutableString stringWithString:[d reportXmlFromFile]]];
            NSString *drillCols = [Util xmlSimpleEscape:[NSMutableString stringWithString:drillAttrs.drillBy]];
            NSString *filters = [Util xmlSimpleEscape:[NSMutableString stringWithString:drillAttrs.filters]];
            NSString *body = 
            [NSString stringWithFormat:@"<ns:xmlReport>%@</ns:xmlReport><ns:drillCols>%@</ns:drillCols><ns:filters>%@</ns:filters>", reportXml, drillCols, filters]; 
            @try{
                [self.drillDownSvc asyncRequestNSDash:body delegate:self requestFinished:@selector(onDrillDownFinished:) requestFailed:@selector(onDrillDownFailed:)];
            }@catch(NSException *ex){
                [Log debug:@"Exception in async request in drillDown() %@", [ex reason]];
            }
            return;
        }
    }
}

- (void)drillToUrl:(NSNotification *)notification{
    DrillAttributes *drillAttrs = [[notification userInfo] objectForKey:@"drillAttributes"];
    NSString* urlStr = drillAttrs.targetURL;
    NSURL *url = [NSURL URLWithString:urlStr];
    if(![[UIApplication sharedApplication] openURL:url]){
        NSLog(@"Failed to open url: %@", [url description]);
    }
}

- (void)onDrillDownFinished:(ASIHTTPRequest *)request{
    if(drillDownDashlet){
        NSString *response = [Util getResponse:request];
        if([drillDownDashlet cacheReportXml:response]){
            //For existing prompts, just render using the new report xml
            if(isDrillDownPromptExist){
                [self renderDashlets:NO useCachedReportXml:YES];
            }else{
                //We have to parse and look for the prompt and fill the values, then render
                NSString *promptStr = [Util xmlBlock:response tag:@"Prompts"];
                if(promptStr){
                    [page addDrillDownPrompts:promptStr];
                    [self fillPromptValues:promptStr];
                }
            }
        }
    }
    drillDownDashlet = nil;
    isDrillDownPromptExist = NO;
    
    //TODO : convert all Webservice to use BirstWS
    drillDownSvc = nil;
}

- (void)onDrillDownFailed:(ASIHTTPRequest *)request{
    [self requestFailed:request];
    [drillDownSvc release];
    drillDownSvc = nil;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
	return [views objectAtIndex:0];
}

- (void) updateGlobalPrompts{
    [self.page updateGlobalPromptValues];
}

- (void) renderPage{
	
	if(views){
		[views removeAllObjects];
	}
	
	if(viewCtrls){
		[viewCtrls removeAllObjects];
	}
	
    if(self.dashletPrompts){
        [self.dashletPrompts removeAllObjects];
    }
    
	[Util removeAllSubviews:scrollView];
	
    //Retrieve prompt and session variables if required 
    //and if we had not rendered our page before
	NSArray *pagePrompts = page.prompts; 
	if (!self.page.renderedDashletsCached && [pagePrompts count] > 0) {
        [Log debug:@"Rendering page %@/%@ requires session variable? %d", page.dir, page.name, page.requireSessionVars];
        if (page.requireSessionVars) {
            [self getSessionVars];
        }else{
            [self getPromptsValuesRender];
        }
	}else {
		[self renderDashlets:self.page.renderedDashletsCached useCachedReportXml:NO];
	}
}

- (Webservice *)promptsSvc{
	if (!promptsSvc) {
		promptsSvc = [[Webservice alloc] initWithAdhoc:@"getFilledOptions"];
        [promptsSvc retain];
    }
	
	return promptsSvc;
}

- (Webservice *)filteredPromptsSvc{
    if(!filteredPromptsSvc){
        filteredPromptsSvc = [[Webservice alloc] initWithAdhoc:@"getFilteredFilledOptions"];
        [filteredPromptsSvc retain];
    }
    return filteredPromptsSvc;
}

- (Webservice *)sessionsSvc{
    if (!sessionsSvc) {
        sessionsSvc = [[Webservice alloc] initWithDashboard:@"evaluateExpression"];
       [sessionsSvc retain];
    }
    
    return sessionsSvc;
}

- (Webservice *)drillDownSvc{
    if(!drillDownSvc){
        drillDownSvc = [[Webservice alloc] initWithDashboard:@"drillDown"];
        [drillDownSvc retain];
    }
    return drillDownSvc;
}

- (void)getSessionVars{
    [Log debug:@"getSessionsVar"];
    SessionVariables *sessVars = [SessionVariables instance];
    NSMutableArray *toBeFilled = [[NSMutableArray alloc]init];
    if(sessVars) {
        NSArray *keys = [sessVars.sessionVars allKeys];
        for (int i = 0; i < [keys count]; i++) {
            NSString *k = [keys objectAtIndex:i];
            NSString *v = [sessVars.sessionVars objectForKey:k];
            if (!v || [v isEqualToString:@"NO_FILTER"]) {
                [toBeFilled addObject:k];
            }
        }
    }
    
    if ([toBeFilled count] > 0) {
        NSMutableString *body = [NSMutableString stringWithString:@"<ns:expression>&lt;Expressions&gt;"];
        for (int i = 0; i < [toBeFilled count]; i++) {
            [body appendString:[NSString stringWithFormat:@"&lt;Expression Expression=\"%@\"/&gt;", [toBeFilled objectAtIndex:i]]];
        }
        [body appendString:@"&lt;/Expressions&gt;</ns:expression>"];                       
        
        //Show Request Screen
        self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
        self.reqPendingCtrl.message = @"Retrieving session variable values...";
        self.reqPendingCtrl.view.frame = self.view.bounds;
        [self.view addSubview:self.reqPendingCtrl.view];
        
        //Make request
        [self.sessionsSvc asyncRequestNSDash:body delegate:self requestFinished:@selector(sessionsFinished:) requestFailed:@selector(requestFailed:)];
    }else{
        [self getPromptsValuesRender];
    }
    
    [toBeFilled release];
}

- (void)sessionsFinished:(ASIHTTPRequest*)request{
    NSString *response = [request responseString];
    
    TBXML *tbxml =  [[TBXML alloc]initWithXMLString:response]; 
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *expr = [Util traverseFindElement:root target:@"Expression"];
    if (expr) {
        do{
            NSString *key = [TBXML valueOfAttributeNamed:@"Expression" forElement:expr];
            NSString *value = [TBXML valueOfAttributeNamed:@"Value" forElement:expr];
            if (key && value) {
                SessionVariables *var = [SessionVariables instance];
                [var.sessionVars setValue:value forKey:key];
            }
        }while((expr = expr->nextSibling));
    }
    [tbxml release];
    
    [self removePendingRequestView];
    
    [self getPromptsValuesRender];
    
}

//NOTE - TODO - ALL THESE METHODS COULD BE REPLACED WITH BLOCKS (CLOSURES)

/* 
 * Gets the prompt's query values via getFilledOptions.
 * If the prompts has parent children relationship, we execute another webservice call
 * getFilteredFilledOptions after getFilledOptions
 */
- (void)getPromptsValuesRender{
    if([page hasParentPrompts]){
        [self getPromptsValues:@selector(getParentPromptsValuesRender:) failSel:@selector(promptsValuesRequestFailed:)];   
    }else{
        [self getPromptsValues:@selector(parsePromptsValuesRender:) failSel:@selector(promptsValuesRequestFailed:)];
    }   
}

//Used to populate the values for the prompt created through drilldown
-(void)fillPromptValues:(NSString *)str{
    str = [Util xmlSimpleEscape:[NSMutableString stringWithString:str]];
    [self getPromptsValuesWithStr:str finishedSel:@selector(parsePromptsValuesRenderUseCache:) failSel:@selector(promptsValuesRequestFailed:)];
}

/*
 * Call getFilledOptions
 */
-(void)getPromptsValues:(SEL)finishedSel failSel:(SEL)failSel{
    if (!self.initPromptsXml) {
		self.initPromptsXml = [page getPromptsXml];
	}
    [self getPromptsValuesWithStr:self.initPromptsXml finishedSel:finishedSel failSel:failSel];
}

-(void)getPromptsValuesWithStr:(NSString *)promptStr finishedSel:(SEL)finishedSel failSel:(SEL)failSel{
    
    if(self.reqPendingCtrl){
        [self.getFilterRequest clearDelegatesAndCancel];
    }
    
    [self removePendingRequestView];
    
    //Show Request Screen
	self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
	self.reqPendingCtrl.message = @"Loading dashboard page...";
	self.reqPendingCtrl.view.frame = self.view.bounds;
    [self.view addSubview:self.reqPendingCtrl.view];
    
	NSString *body = [NSString stringWithFormat:@"<ns:prompts>%@</ns:prompts>", promptStr];
    
    ASIHTTPRequest *request = [BirstWS requestWithAdhoc:@"getFilledOptions"];
    self.getFilterRequest = request;
    [BirstWS asyncRequestNS:request body:body delegate:self requestFinished:finishedSel requestFailed:failSel];   
}

/*
 * Parse the prompt values retrieved from getFilledOptions, then
 * execute getFilteredFilledOptions to get the parent - children values 
 * and render the dashlets.
 */
-(void)getParentPromptsValuesRender:(ASIHTTPRequest *)request{
    [self pageParsePromptValues:request isParentChildren:YES];
    [self doGetParentPromptsValues:YES];
}

/*
 * Execute getFilteredFilledOptions but do not render
 */
-(void)getParentPromptsValues{
    [self doGetParentPromptsValues:NO];
}

/*
 * Show the request pending view while retrieving the parent - children values
 * render parameter controls whether we render the dashlets or not after request is
 * finished
 */
-(void)doGetParentPromptsValues:(BOOL)render{
    NSLog(@">>>>>>>**** isParentRequestPending = %d", self.isParentRequestPending);
    if(self.isParentRequestPending){
        NSLog(@"XXXXX Parent request is pending");
        [self.filteredPromptsSvc.request clearDelegatesAndCancel];
        [filteredPromptsSvc release];
        filteredPromptsSvc = nil;
    }
    
    self.isParentRequestPending = YES;
    [self removePendingRequestView];
    
    //Show Request Screen
	self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
	self.reqPendingCtrl.message = @"Loading dashboard page....";
	self.reqPendingCtrl.view.frame = self.view.bounds;
    [self.view addSubview:self.reqPendingCtrl.view];
     
    
    NSString *promptsXml = [self.page getParentChildPromptsXmlValues];
	NSString *body = [NSString stringWithFormat:@"<ns:prompts>%@</ns:prompts>", promptsXml];
    
    SEL selector = (render) ? @selector(parentPromptValuesRequestFinishedRender:) : @selector(parentPromptValuesRequestFinished:);
    
    [self.filteredPromptsSvc asyncRequestNS:body delegate:self requestFinished:selector requestFailed:@selector(parentPromptValuesRequestFailed:)];
    
}

/*
 * Parse the parent - children prompt values after the request.
 * Does NOT render dashlets
 */
- (void)parentPromptValuesRequestFinished:(ASIHTTPRequest *)request{
    [self pageParsePromptValues:request isParentChildren:YES];
    
    self.isParentRequestPending = NO;
    
    [filteredPromptsSvc release];
    filteredPromptsSvc = nil;
}

/*
 * Parse the prompt values
 */
- (void)pageParsePromptValues:(ASIHTTPRequest *)request isParentChildren:(BOOL)isParentChildren{
    [self removePendingRequestView];
    
	NSString *response = [Util getResponse:request];
    
	[Log debug:@"getPromptsValue response: %@", response];
	[page parsePromptValues:response isParentChildren:isParentChildren];
}

/*
 * Parse the parent  - child values, render dashlets
 * Delete the request object
 */
- (void)parentPromptValuesRequestFinishedRender:(ASIHTTPRequest *)request{
    [self pageParsePromptValues:request isParentChildren:YES];
	[self renderDashlets:NO useCachedReportXml:NO];
    
    self.isParentRequestPending = NO;
    [filteredPromptsSvc release];
    filteredPromptsSvc = nil;
}

/*
 * Parse the prompt values, render the dashlets
 */
- (void)parsePromptsValuesRender:(ASIHTTPRequest *)request{
    [self pageParsePromptValues:request isParentChildren:NO];
	[self renderDashlets:NO useCachedReportXml:NO];
}

/*
 * Parse the prompt values, render the dashlets, use cached report xml
 */
- (void)parsePromptsValuesRenderUseCache:(ASIHTTPRequest *)request{
    [self pageParsePromptValues:request isParentChildren:NO];
	[self renderDashlets:NO useCachedReportXml:YES];
}

- (void)parentPromptValuesRequestFailed:(ASIHTTPRequest *)request{
    self.isParentRequestPending = NO;
    [self requestFailed:request];

    [filteredPromptsSvc release];
    filteredPromptsSvc = nil;
}

- (void)promptsValuesRequestFailed:(ASIHTTPRequest *)request{
    [self requestFailed:request];
    
    [promptsSvc release];
    promptsSvc = nil;
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    [self removePendingRequestView];
    
	NSError *error = [request error];
	
	[Log debug:@"requestFailed: %@", [error localizedDescription]];
	
	[Util requestFailedAlert:error];
}

-(void)removePendingRequestView{
    if(reqPendingCtrl){
        [reqPendingCtrl.view removeFromSuperview];
        [reqPendingCtrl release];
        reqPendingCtrl = nil;
    }
}

- (NSString *) getPageTitle{
    if(!page.invisible){
        return [Util xmlUnescape: page.name];
    }

    return @"";
}


- (void)renderDashlets:(BOOL)renderFromFile useCachedReportXml:(BOOL)useCachedReportXml {
	[self dismissPromptPopover];
    [self dismissSavePopover];
    
    //reset number of page render cache writes
    numReportFileWrites = 0;
    
    //reset number of rendered
    numRendered = 0;
    
	int numDashes = [page.dashlets count];
	
	//set title
	if (numDashes > 1) {
        [self titleWithDashlet:[page.dashlets objectAtIndex:0]];
	}else{
        if(!page.invisible){
            reportTitle.text = [Util xmlUnescape: page.name];
        }
	}
    
    if (!views) {
        views = [[NSMutableArray alloc]initWithCapacity:numDashes];
    }else{
        for(int i = [views count] - 1; i >= 0; i--){
            UIView *v = [views objectAtIndex:i];
            [v removeFromSuperview];
        }
        [views removeAllObjects];
    }
    
    if (!viewCtrls){
        viewCtrls = [[NSMutableArray alloc] initWithCapacity:numDashes];
	}else{
        [viewCtrls removeAllObjects];
    }
    
	NSString *promptXml = [self.page getPromptsXmlValues:YES encode:YES ignoreList:nil];
    
    Layout *layout = self.page.layout;
    
    float offset = 0;
    float x = self.view.frame.origin.x;
    float y = offset;
    float height = 705; //self.view.frame.size.height - offset;
    float width = self.view.frame.size.width;
    
    float percentHeight = (layout.percentHeight) ? layout.percentHeight : 100;
    float percentWidth = (layout.percentWidth) ? layout.percentWidth : 100;
    
    float lHeight = height * (percentHeight / 100);
    float lWidth = width * (percentWidth / 100);

    if(self.viewMode == Offline){
        self.backgroundColor = [Util colorWithHexString:self.page.bgColor];
    }
    [self doLayout:self.view layout:layout rect:CGRectMake(x, y, lWidth, lHeight) promptXml:promptXml renderFromFile:renderFromFile useCachedReportXml:useCachedReportXml];
    
   	[Log debug:@"number of dashlets: %d", numDashes];
}

- (UIView *) doDashletLayout:(UIView *)view layout:(Layout*)layout rect:(CGRect)rect promptXml:(NSString*)promptXml renderFromFile:(BOOL)renderFromFile useCachedReportXml:(BOOL)useCachedReportXml{
    
    UIView *v = nil;
    if(layout.isPromptDashlet){
        BOOL doPromptBorder = YES;
        NSArray *prs = self.page.prompts;
        for(int i = 0; i < [prs count]; i++){
            Prompt *p = [prs objectAtIndex:i];
            if([p.ref isEqualToString: layout.promptRef]){
                UIViewController *promptCtrl = nil;
                //Create and set the sizing for different controls
                if(p.type == Value){
                    FlexFieldController *f = [[FlexFieldController alloc]init];
                    f.prompt = p;
                    f.renderedInDashlet = YES;
                    f.pgCtrl = self;
                    promptCtrl = f;
                    [self.dashletPrompts addObject:f];
                    [f release];
                }else if(p.type == List || p.type == Query){
                    if(!p.expandIntoButtons){
                        if (p.isMultiSelect) {
                            QueryListTableController *qlt = [[QueryListTableController alloc]init];
                            qlt.prompt = p;
                            qlt.renderedInDashlet = YES;
                            qlt.pgCtrl = self;
                            promptCtrl = qlt;
                            [self.dashletPrompts addObject:qlt];
                            [qlt release];
                        }else if(!p.isMultiSelect){
                            SingleSelectListPickerController *ssl = [[SingleSelectListPickerController alloc]init];
                            ssl.prompt = p;
                            promptCtrl = ssl;
                            ssl.pgCtrl = self;
                            ssl.backgroundColor = self.backgroundColor;
                            ssl.renderedInDashlet = YES;
                            [self.dashletPrompts addObject:ssl];
                            [ssl release];
                            doPromptBorder = NO;
                        }
                    }else{
                        if(p.isMultiSelect){
                            
                        }else{
                            SegmentedCheckboxController *s = [[SegmentedCheckboxController alloc]init];
                            s.prompt = p;
                            s.renderedInDashlet = YES;
                            s.pgCtrl = self;
                            s.backgroundColor = self.backgroundColor;
                            promptCtrl = s;
                            [self.dashletPrompts addObject:s];
                            [s release];
                        }
                        doPromptBorder = NO;
                    }
                }else if (p.type == CheckBox){
                   
                }else if (p.type == Slider){
                    UIView *bview = [[UIView alloc]initWithFrame:rect];
                    bview.backgroundColor = self.backgroundColor;
                    if(doPromptBorder){
                        bview.layer.borderColor = [UIColor grayColor].CGColor;
                        bview.layer.borderWidth = 0.5;
                    }
                    
                    UILabel *msg = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 44)];
                    msg.backgroundColor = self.backgroundColor;
                    msg.text = @"This prompt type will be supported in the near future";
                    [bview addSubview:msg];
                    [msg release];
                    [view addSubview:bview];
                    [bview release];
                }
                
                if(promptCtrl){
                    v = promptCtrl.view;
                }
                
                if(v){
                    if(doPromptBorder){
                        v.layer.borderColor = [UIColor grayColor].CGColor;
                        v.layer.borderWidth = 0.5;
                    }
                    
                    v.frame = rect;
                    [view addSubview:v];
                }
                break;
            }
        }
    }else if(layout.dashlet != nil){
        Dashlet *dash = layout.dashlet;
        NSLog(@">>> rendering dashlet %@", dash.path);
        DashletController *vc = [[DashletController alloc] init];
        //vc.dashletSize = rect.size;
        vc.dashlet = dash;
        vc.page = page;
		vc.promptXml = ([dash.ignoredPrompts count] > 0) ?  [self.page getPromptsXmlValues:YES encode:YES ignoreList:layout.dashlet.ignoredPrompts] : promptXml;
		vc.pageRenderCtrl = self;
        vc.renderFromFile = renderFromFile;
        vc.useCachedReportXml = useCachedReportXml;
        if(useCachedReportXml){
            [vc readCachedViewSelectorXml];
        }
        
        v = vc.view;
        v.backgroundColor = self.backgroundColor;
        if(dash.hasBorder){
            v.layer.borderColor = [UIColor grayColor].CGColor;
            v.layer.borderWidth = 0.5;
        }
        v.frame = rect;
		[view addSubview:v];
		[views addObject:v];
		[viewCtrls addObject:vc];
        
		[vc release];
    }else{
        NSLog(@">>> rendering frame ***");
        v = [[UIView alloc]initWithFrame:rect];
        v.backgroundColor = self.backgroundColor;
        [view addSubview:v];
        [views addObject:v];
    }
    
    NSLog(@">>> layout: x=%f, y=%f size width=%f,height=%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    
    return v;
}

- (void) doLayout:(UIView *)view layout:(Layout*)layout rect:(CGRect)rect promptXml:(NSString*)promptXml renderFromFile:(BOOL)renderFromFile useCachedReportXml:(BOOL)useCachedReportXml{
    
    UIView *v = [self doDashletLayout:view layout:layout rect:rect promptXml:promptXml renderFromFile:renderFromFile useCachedReportXml:useCachedReportXml];
    
    NSArray *children = layout.layouts;
    
    float xOffset = 0;
    float yOffset = 0;
    
    NSLog(@">>>> Layout parent, width = %f, height = %f, parent = %@", rect.size.width, rect.size.height, v);
    for(int i = 0; i < [children count]; i++){
        Layout *c = [children objectAtIndex:i];
        
        NSLog(@">>>>> layout children %d, percent width = %f, percent height = %f", i, c.percentWidth, c.percentHeight);
        
        float percentHeight = (c.percentHeight <= 100) ? c.percentHeight : 100;
        float percentWidth = (c.percentWidth <= 100) ? c.percentWidth : 100;
        float cHeight = rect.size.height * (percentHeight / 100);
        float cWidth = rect.size.width * (percentWidth / 100);
        
        float x = xOffset;
        float y = yOffset;
       
        CGRect r = CGRectMake(x, y, cWidth, cHeight);
        //NSLog(@">>>>> DoLayout parent %@ child %d", v, i);
        [self doLayout:v layout:c rect:r promptXml:promptXml renderFromFile:renderFromFile useCachedReportXml:useCachedReportXml];
        
        if(layout.direction == Vertical){
            yOffset += cHeight;
        }else{
            xOffset += cWidth;
        }
    }
    
    if(!layout.dashlet && !layout.isPromptDashlet){
        [v release];
    }
}

//set title with dashlet
- (NSString *)titleWithDashlet:(Dashlet*) dash{
    NSString *rTitle = @"";
    
    if(!page.invisible){
        NSString *pg = [Util xmlUnescape:page.name];
        NSString *dashTitle = [Util xmlUnescape:dash.title];
        rTitle = [NSString stringWithFormat:@"%@ / %@", pg, dashTitle];
    }
    
    return rTitle;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender{
	CGFloat pageWidth = scrollView.frame.size.width;
    int pg = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	
	if (pg >= 0 && pg < [page.dashlets count]){
		Dashlet *dash = [page.dashlets objectAtIndex:pg];
        [self titleWithDashlet:dash];
	}
}

- (void)alignSubviews {
	// Position all the content views at their respective page positions
	scrollView.contentSize = CGSizeMake([page.dashlets count] * scrollView.bounds.size.width, scrollView.bounds.size.height);
	
	NSUInteger i = 0;
	for (UIView *v in views) {
		v.frame = CGRectMake(i * scrollView.bounds.size.width, 0, scrollView.bounds.size.width, scrollView.bounds.size.height);
		i++;
	}
}

- (void)dashletCachedRenderedReport:(Dashlet *)dashlet{
    numReportFileWrites++;
    
    if (numReportFileWrites == [self.page.dashlets count]) {
        self.page.renderedDashletsCached = YES;
    }
}

-(void)captureScreenshot{
    numRendered++;
    
    if(self.isSpaceScreenshot && numRendered == [viewCtrls count]){
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
		
        NSString *screenShotDir = [Settings screenshotDir:filemgr];
		
		NSString *screenshotPage = [NSString stringWithFormat:@"%@/%@.png", screenShotDir, page.uuid];
		[Log debug:@"screenshot path = %@", screenshotPage];
		
		NSError *error;
		
		if(![filemgr createDirectoryAtPath:screenShotDir withIntermediateDirectories:YES attributes:nil error: &error]){
			[Log debug:@"cannot create directory %@ because %@", screenShotDir, [error localizedDescription]];
			return;
		}
        
        [Util screenShot:self.view loc:screenshotPage];
        
        if(self.isSpaceScreenshot){
            NSString *spaceFile = [NSString stringWithFormat:@"%@/%@.png", screenShotDir, [Settings spaceId]];
            [filemgr removeItemAtPath:spaceFile error:nil];
            [filemgr copyItemAtPath:screenshotPage toPath:spaceFile error:nil];
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
										 duration:(NSTimeInterval)duration {
	[self alignSubviews];
	scrollView.contentOffset = CGPointMake(currentPage * scrollView.bounds.size.width, 0);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return [Util orientationLockLandscape:interfaceOrientation];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    [Log warn:@"PageRenderViewController() ***********************MEMORY WARNING*************************"];
    // Release any cached data, images, etc. that aren't in use.
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void) viewWillDisappear:(BOOL)animated{
	if (self.saveCtrl) {
		[self.saveCtrl dismissPopoverAnimated:YES];
		[saveCtrl release];
		saveCtrl = nil;
	}
	
	if (self.promptsCtrl){
		[promptsCtrl dismissPopoverAnimated:YES];
		[promptsCtrl release];
		promptsCtrl = nil;
	}

}

- (void) viewDidDisappear:(BOOL)animated{
    [Log debug:@"PageRenderViewController viewDidDisappear"];
    
    if (self.drillToDashboardNotification) {
        //Notify dashboards view controller
        [[NSNotificationCenter defaultCenter] postNotification:self.drillToDashboardNotification];
    }
}


- (void)viewDidUnload {
    [Log debug:@"PageRenderViewController viewDidUnload"];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated  {
    [Log debug:@"PageRenderViewController viewWillAppear"];
    [self renderPage];
}

- (void)dealloc {
    [Log debug:@"PageRenderViewController dealloc"];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
    [page release];
    [initPromptsXml release];
    
    [scrollView release];
	    
    for (DashletController *ctrl in viewCtrls) {
        ctrl.pageRenderCtrl = nil;
        ctrl.dashlet = nil;
    }
    [viewCtrls release];
    viewCtrls = nil;
    
    [views release];
    views = nil;

    [dashletPrompts release];
    
    [sessionsSvc release];
    sessionsSvc = nil;
    
    [promptsSvc release];
    promptsSvc = nil;
    
    [filteredPromptsSvc release];
    filteredPromptsSvc = nil;
    
    [drillDownSvc release];
    drillDownSvc = nil;
    
    [promptsCtrl release];
    [saveCtrl release];
   
    [reqPendingCtrl release];
    
    [super dealloc];
    
    //Add background image
    //[[NSNotificationCenter defaultCenter] postNotificationName:ADD_BACKGROUND_IMAGE object:nil];
}


@end
