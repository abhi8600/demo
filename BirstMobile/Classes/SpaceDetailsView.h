//
//  SpaceDetailsView.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "DetailViewController.h"
#import "SpacesTableViewController.h"

@class SpacesTableViewController;
@class RootViewController;

@interface SpaceDetailsView : UIViewController {
	IBOutlet UILabel *name;
	IBOutlet UILabel *owner;
	IBOutlet UILabel *size;
	IBOutlet UILabel *lastProcessed;
	IBOutlet UILabel *permissions;
	
	IBOutlet UIButton *dashboardBttn;
	IBOutlet UIButton *reportBttn;
	
	NSString *tName;
	NSString *tOwner;
	NSString *tSize;
	NSString *tLastProcessed;
	NSString *tPermission;
	
	SpacesTableViewController *spacesCtrl;
	RootViewController *rootCtrl;
	DetailViewController *detailCtrl;
}

@property (nonatomic, retain) NSString *tName;
@property (nonatomic, retain) NSString *tOwner;
@property (nonatomic, retain) NSString *tSize;
@property (nonatomic, retain) NSString *tLastProcessed;
@property (nonatomic, retain) NSString *tPermissions;

@property (nonatomic, assign) SpacesTableViewController *spacesCtrl;
@property (nonatomic, assign) RootViewController *rootCtrl;
@property (nonatomic, assign) DetailViewController *detailCtrl;

- (IBAction) getDashboards;

@end
