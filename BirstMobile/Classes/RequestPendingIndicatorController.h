//
//  RequestPendingIndicatorController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RequestPendingIndicatorController : UIViewController {
	UIImageView *activity;
	UILabel *msg;
	NSString *message;
    UIColor *parentBgColor;
}

@property (nonatomic, retain) IBOutlet UIImageView *activity;
@property (nonatomic, retain) IBOutlet UILabel *msg;
@property (nonatomic, retain) NSString *message;
@property (assign) UIColor *parentBgColor;

@end
