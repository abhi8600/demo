//
//  Space.m
//  Birst
//
//  Created by Seeneng Foo on 3/4/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Space.h"


/*
 <?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetSpaceDetailsResponse xmlns="http://www.birst.com/"><GetSpaceDetailsResult><Error><ErrorType>0</ErrorType><canRetryLoad>false</canRetryLoad></Error><SpaceName>DnB - SFDC Prod</SpaceName><SpaceID>c95d1891-fb4f-443c-bc0c-a3da37b2245e</SpaceID><Owner>false</Owner><OwnerUsername>salesinsightsupport@dnb.com</OwnerUsername><Administrator>true</Administrator><Adhoc>false</Adhoc><Dashboards>true</Dashboards><QuickDashboard>false</QuickDashboard><EnableAdhoc>false</EnableAdhoc><EnableDashboards>true</EnableDashboards><EnableQuickDashBoards>false</EnableQuickDashBoards><AdminAccess><ManageCatalog>true</ManageCatalog><Publish>true</Publish><UseTemplate>true</UseTemplate><ServicesAdmin>false</ServicesAdmin><CopySpace>true</CopySpace><CustomFormula>true</CustomFormula><ShareSpace>true</ShareSpace><QuartzSchedulerEnabled>true</QuartzSchedulerEnabled><showOldScheduler>true</showOldScheduler></AdminAccess><LastUpload>7/6/2012</LastUpload><SpaceSize>0 records used (0.0B)</SpaceSize><SpaceComments /><SuperUser>true</SuperUser><AvailabilityCode>0</AvailabilityCode></GetSpaceDetailsResult></GetSpaceDetailsResponse></soap:Body></soap:Envelope>
*/

@implementation Space

@synthesize name, spaceId, owner, isOwner, lastUpload, spaceSize, backgroundColor;


-(void)parse:(TBXMLElement *)element{
    self.name = [self getValueForTag:@"SpaceName" element:element];
    self.spaceId = [self getValueForTag:@"SpaceID" element:element];
    self.owner = [self getValueForTag:@"OwnerUsername" element:element];
    self.isOwner = [self getValueForTag:@"Owner" element:element];
    self.lastUpload = [self getValueForTag:@"LastUpload" element:element];
    self.spaceSize = [self getValueForTag:@"SpaceSize" element:element];
}

-(NSString *)getValueForTag:(NSString *)tag element:(TBXMLElement *)element{
    TBXMLElement *tagEl = [TBXML childElementNamed:tag parentElement:element];
    return (tagEl != nil) ? [TBXML textForElement:tagEl] : @"";
}

- (void) dealloc{
	[name release];
	[spaceId release];
	[owner release];
	[isOwner release];
    [lastUpload release];
    [spaceSize release];
    
    [super dealloc];
}
@end
