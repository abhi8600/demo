//
//  SpaceDetailViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 7/9/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "SpaceDetailViewController.h"
#import "BirstWS.h"
#import "Settings.h"

@implementation SpaceDetailViewController

@synthesize openBttn, spaceTitle, spaceDetails, spaceDashboardImg, space, parentViewCtrl, networkQueue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction) openDashboard:(id)sender{
    [self.parentViewCtrl displayDashboard];
}

-(void)dealloc{
    [openBttn release];
    [spaceTitle release];
    [spaceDetails release];
    [spaceDashboardImg release];
    [space release];
    
    [networkQueue reset];
    [networkQueue release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Network queue which handles all our single http requests
    //Set to 1 to force only one requests at a time
    self.networkQueue = [BirstWS createNetworkQueueMaxOne:self finishSel:@selector(onRequestFinished:) failSel:@selector(onRequestFailed:)];
    
    [self.openBttn useBlackStyle];
    
    self.spaceTitle.text = self.space.name;
    self.spaceDetails.text = [NSString stringWithFormat:@"Owner %@\nLast processed %@", self.space.owner, self.space.lastUpload];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *screenShotDir = [Settings screenshotDir:fm];
    NSString *screenShotPath = [NSString stringWithFormat:@"%@/%@.png", screenShotDir, self.space.spaceId];
    
    if([fm fileExistsAtPath:screenShotPath]){
        self.spaceDashboardImg.image = [UIImage imageWithContentsOfFile:screenShotPath];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [networkQueue reset];
    [networkQueue release];
    networkQueue = nil;
}

@end
