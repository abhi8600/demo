//
//  PagesViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RootViewController.h"

#import "DashboardsViewController.h"
#import "DrillAttributes.h"
#import "Page.h"

@class DashboardsViewController;

@interface PagesViewController : UITableViewController {
	NSArray *pages;
    NSArray *invisiblePages;
    DashboardsViewController *dashCtrl;
    NSUInteger selectedIndex;
}

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, retain) NSArray *pages;
@property (nonatomic, retain) NSArray *invisiblePages;
@property (nonatomic, retain) DashboardsViewController *dashCtrl;

-(void) displayPage:(NSString*)page drillAttributes:(DrillAttributes *)dAttrs;
- (void)displayInvisiblePage:(NSString *)page drillAttributes:(DrillAttributes *)dAttrs;
- (void)popupPage:(NSIndexPath *)indexPath;
- (void)showPage:(Page*)page;
- (void)showPageRenderViewController:(Page*)page;
@end
