//
//  ViewSelectorsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/16/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaximizedDashletViewController.h"

@class MaximizedDashletViewController;

@interface ViewSelectorsViewController : UITableViewController{
    BOOL useFullChartList;
    NSString *selectedChart;
    NSString *originalChartType;
    int selectedRow;
    MaximizedDashletViewController *maxVC;
    
    NSArray *fullChartList;
    NSArray *tableOrChartList;
}

@property BOOL useFullChartList;
@property (assign) NSString *selectedChart;
@property (retain) NSString *originalChartType;
@property (assign) MaximizedDashletViewController *maxVC;
@property (retain) NSArray *fullChartList;
@property (retain) NSArray *tableOrChartList;
@end
