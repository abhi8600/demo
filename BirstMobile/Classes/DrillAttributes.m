//
//  DrillAttributes.m
//  BirstMobile
//
//  Created by Seeneng Foo on 4/19/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "DrillAttributes.h"
#import "Util.h"
#import "Notification.h"
#import "NSStringCategory.h"

#define ATTR_TYPE       @"type"
#define ATTR_DRILL_TYPE @"DrillType"
#define ATTR_URL        @"targetURL"
#define ATTR_DASH       @"dash"
#define ATTR_PAGE       @"page"
#define ATTR_FILTERS    @"filters"
#define ATTR_COLS       @"columns"
#define ATTR_DRILLBY    @"drillBy"

@implementation DrillAttributes


- (id) initWithQuery:(NSString *)query{
    if(query){
        NSArray *keyVals = [query componentsSeparatedByString:@"&"];
        for(int i = 0; i < [keyVals count]; i++){
            NSString *kv = [keyVals objectAtIndex:i];
            NSArray *keyValue = [kv componentsSeparatedByString:@"="];
            if([keyValue count] >= 2){
                NSString *key = [keyValue objectAtIndex:0];
                if([key characterAtIndex:0] == '%'){
                    key = [key substringFromIndex:1];
                }
                
                NSString *value = [keyValue objectAtIndex:1];
                value = [value stringByUrlDecode];
                for(int y = 2; y < [keyValue count]; y++){
                    NSString *v = [keyValue objectAtIndex:y];
                    value = [NSString stringWithFormat:@"%@=%@", value, v];
                }
                
                [self.keyValues setValue:value forKey:key];
            }
        }
    }
    return self;
}

- (id) initWithXml:(TBXMLElement *) xml{
    if (xml) {
        do{
            NSString *name = [TBXML valueOfAttributeNamed:@"name" forElement:xml];
            NSString *value = [TBXML textForElement:xml];
            [self.keyValues setValue:value forKey:name];
        }while((xml = xml->nextSibling));
    }

    return self;
}

- (id) initWithLinkString:(NSString *)link{
    if ([link length] > 0){
        NSString *headStr = @"javascript:AdhocContent.DrillThru(";
        NSRange head = [link rangeOfString:headStr];
        if (head.length > 0) {
            NSUInteger headIdx = head.location + [headStr length];
            NSRange tail = [link rangeOfString:@")"];
            if (tail.length > 0 && tail.location > headIdx) {
                NSString *attrs = [link substringWithRange:NSMakeRange(headIdx, [link length] - [headStr length] - 1)];
                if([attrs length] > 0){
                    NSArray *listAttrs = [attrs componentsSeparatedByString:@","];
                    int attrCount = [listAttrs count];
                    if(attrCount >= 4){
                        int idx = 1;
                        if(attrCount > 4){
                            NSString *dash = [self removeApos:[listAttrs objectAtIndex:0]];
                            [self.keyValues setObject:dash forKey:ATTR_DASH];
                            NSString *pg = [self removeApos:[listAttrs objectAtIndex:1]];
                            [self.keyValues setObject:pg forKey:ATTR_PAGE];
                            idx = 2;
                        }else{
                            NSString *url = [self removeApos:[listAttrs objectAtIndex:0]];
                            [self.keyValues setObject:url forKey:ATTR_URL];
                        }
                        
                        NSString *type = [self removeApos:[listAttrs objectAtIndex:idx++]];
                        [self.keyValues setObject:type forKey:ATTR_TYPE];
                        
                        NSString *filters = [self removeApos:[listAttrs objectAtIndex:idx++]];
                        [self.keyValues setObject:filters forKey:ATTR_FILTERS];
                        
                        NSString *drillCols = [self removeApos:[listAttrs objectAtIndex:idx++]];
                        [self parseDrillColumnsInfo:drillCols];
                    }
                }
            }
        }else{
            NSRange httpExists = [[link lowercaseString] rangeOfString:@"http"];
            if(httpExists.length == 0){
                link = [NSString stringWithFormat:@"http://%@", link];
            }
            [self.keyValues setObject:link forKey:ATTR_URL];
            [self.keyValues setObject:@"URL" forKey:ATTR_TYPE];
        }

    }
    return self;    
}

- (void) notify{
    NSString *notifType = (self.type == DrillToDashboard) ? DRILL2DASHBOARD : (self.type == DrillToUrl) ? DRILL2URL : DRILLDOWN;
    
    //Notify
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    NSMutableDictionary *dict = [[[NSMutableDictionary alloc]init] autorelease];
    [dict setObject:self forKey:@"drillAttributes"];
    [ctr postNotificationName:notifType object:self userInfo:dict];
}

- (void) parseDrillColumnsInfo:(NSString *)drillCols{
    TBXML *tbxml = [[TBXML alloc]initWithXMLString:[Util xmlSimpleUnescape:[NSMutableString stringWithString:drillCols]]];
    TBXMLElement* root = tbxml.rootXMLElement;
    if(root){
        TBXMLElement *fromDim = [Util traverseFindElement:root target:@"FromDimension"];
        if(fromDim){
            TBXMLElement *fromColum = [Util traverseFindElement:root target:@"FromColumn"];
            NSString *drillBy = [NSString stringWithFormat:@"%@.%@", [TBXML textForElement:fromDim], [TBXML textForElement:fromColum]];
            [self.keyValues setObject:drillBy forKey:@"drillBy"];
        }
    }
    [tbxml release];
}

- (NSString*) removeApos:(NSString *)str{
    if([str length] > 0){
        NSRange head = [str rangeOfString:@"'"];
        if (head.length > 0) {
            str = [str substringFromIndex:head.location];
            return [str stringByReplacingOccurrencesOfString:@"'" withString:@""];
        }
    }
    return str;
}

- (DrillType) type{
    
	//ATTR_DRILL_TYPE is from xml (charts), ATTR_TYPE is from link string
    NSString *type = [keyValues objectForKey:ATTR_DRILL_TYPE];
	if(!type){
		type = [keyValues objectForKey:ATTR_TYPE];
	}
	
    if ([type isEqualToString:@"Drill To Dashboard"]) {
        return DrillToDashboard;
    }else if([type isEqualToString:@"URL"]){
        return DrillToUrl;
    }else{
        return DrillDown;
    }
    
}

- (NSString *) targetURL{
    return [keyValues objectForKey:ATTR_URL];
}

- (NSString *) drillToDashboard{
    return [keyValues objectForKey:ATTR_DASH];
}

- (NSString *) drillToPage{
    return [keyValues objectForKey:ATTR_PAGE];
}



- (NSString *) valueForAttribute:(NSString*)attr{
    return [keyValues objectForKey:attr];
}

- (NSString *) drillBy{
    return [keyValues objectForKey:ATTR_DRILLBY];
}

- (NSMutableDictionary *) keyValues{
    if (!keyValues) {
        keyValues = [[NSMutableDictionary alloc]init];
    }
    return keyValues;
}

- (NSString *) filters{
    NSString* f = [keyValues objectForKey:ATTR_FILTERS];
    return [Util xmlUnescape: [f stringByUrlDecode]];
}

- (NSString *) columns{
    return [keyValues objectForKey:ATTR_COLS];
}

- (void) dealloc{
    [keyValues release];
    [super dealloc];
}

@end
