//
//  ColumnSelectorsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/20/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaximizedDashletViewController.h"

@interface ColumnSelectors : NSObject{
    TBXMLElement *selectedElement;
    NSString *label;
    NSString *selectedValue;
    int selectedRow;
    
    NSMutableArray *selectors;
}

@property (assign) TBXMLElement *selectedElement;
@property (retain) NSString *label;
@property (retain) NSString *selectedValue;
@property (assign) int selectedRow;
@property (retain) NSMutableArray *selectors;
@property (retain) NSString *ID;

-(id) initSelectors;

@end

@interface ColumnSelector : NSObject{
    NSString *label;
    NSString *dimension;
    NSString *column;
    NSString *ID;
}

@property (retain) NSString *label;
@property (retain) NSString *dimension;
@property (retain) NSString *column;
@property (retain) NSString *ID;

-(NSString *) dimensionColumn;

@end

@interface ColumnSelectorsViewController : UITableViewController{
    MaximizedDashletViewController *maxVC;
    TBXML *tbxml;
    NSMutableArray *columnSelectors;
}

@property (assign) MaximizedDashletViewController *maxVC;

@property (retain) TBXML *tbxml;
@property (retain) NSMutableArray *columnSelectors;

@end

