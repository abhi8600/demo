//
//  DashletController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/15/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Webservice.h"
#import "Page.h"
#import "Dashlet.h"
#import "ReportRenderer.h"
#import "RequestPendingIndicatorController.h"
#import "PageRenderViewController.h"

@class PageRenderViewController;
@class ReportRenderer;

@interface DashletController : UIViewController {
	Webservice *ws;
    Webservice *wsCached;
	Dashlet *dashlet;
	Page *page;
	ReportRenderer *reportRenderer;
	NSString *promptXml;
    NSString *dashletReportXml;
	RequestPendingIndicatorController *reqPendingCtrl;
    PageRenderViewController *pageRenderCtrl;
    BOOL renderFromFile;
    BOOL useCachedReportXml;
    
    //Paging
    UILabel *pagingPageNo;
    UIButton *pagingPrevious;
    UIButton *pagingNext;
    UIButton *pagingFirst;
    UIButton *pagingLast;
    UIView *pagingSection;
    int pageToRender;
    BOOL doScreenshot;
    BOOL isSpaceScreenshot;
    
    //View Selectors
    BOOL includeChartSelector;
    NSString *selectorChartType;
}

@property (nonatomic, assign) Dashlet *dashlet;
@property (nonatomic, assign) Page *page;
@property (readonly, retain) Webservice *ws;
@property (readonly, retain) Webservice *wsCached;
@property (nonatomic, retain) NSString *promptXml;
@property (nonatomic, retain) ReportRenderer *reportRenderer;
@property (nonatomic, retain) RequestPendingIndicatorController *reqPendingCtrl;
@property (nonatomic, assign) PageRenderViewController *pageRenderCtrl;
@property (nonatomic, assign) BOOL renderFromFile;
@property (nonatomic, assign) BOOL useCachedReportXml;
@property (nonatomic, retain) NSString *dashletReportXml;

@property (nonatomic, retain) UILabel* pagingPageNo;
@property (nonatomic, retain) UIButton* pagingPrevious;
@property (nonatomic, retain) UIButton* pagingNext;
@property (nonatomic, retain) UIButton* pagingFirst;
@property (nonatomic, retain) UIButton* pagingLast;
@property (nonatomic, retain) UIView* pagingSection;

@property BOOL includeChartSelector;
@property (retain) NSString *selectorChartType;

- (void)render:(NSString *)reportXml;
- (void)renderReport;
- (void)requestFinished:(ASIHTTPRequest *)request;
- (void)requestFailed:(ASIHTTPRequest *)request;
- (void)removeRequestPendingView;
- (void)cacheRenderedReport:(NSString*)response renderedFromPath:(BOOL)renderedFromPath;
- (NSString *)renderedXmlFromFile;
- (NSString *)reportXmlFromFile;
- (NSString *)readFromFile:(NSString *)suffix;
- (NSString*)selectorXmlFromFile;
- (BOOL)fileCachePageData:(NSString *)path response:(NSString *)response;
- (NSString *)readFileCachePageData:(NSString *)fullPath;
- (void)renderByColumnSelector:(NSString *)selectorXml;
- (void)renderByViewSelector:(NSString *)graph;
- (void)renderUsingReportXml;
+ (NSString *)reportXmlSubstring:(NSString *)response;
- (BOOL)cacheReportXml:(NSString *)response;
- (BOOL)cacheViewSelectorXml:(NSString *)response renderedFromPath:(BOOL)renderedFromPath;
- (void)renderUsingReportPath;
- (void)resetRenderer;
+ (NSString *)getUniqueXmlTagContent:(NSString *)xml tag:(NSString*)tag;
- (ASIFormDataRequest *)getPDFRequest;
- (void)readCachedViewSelectorXml;
- (NSString *)parseCachedViewSelectorXml:(NSString *)selectorsXml;
- (NSString *)readSelectorChartType;

- (void) clickFirstPage:(id)sender;
- (void) clickNextPage:(id)sender;
- (void) clickPrevPage:(id)sender;
- (void) clickLastPage:(id)sender;

- (void) createPagingSection;

@end
