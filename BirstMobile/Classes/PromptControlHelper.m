//
//  PromptControlHelper.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "PromptControlHelper.h"
#import "Notification.h"


@implementation PromptControlHelper

+ (void) parentPromptValueChanged:(id)promptCtrl prompt:(Prompt*)prompt {
    if([promptCtrl respondsToSelector:@selector(getSelectedValues)]){
        NSMutableArray *selectedValues = [promptCtrl performSelector:@selector(getSelectedValues)];
        if ([selectedValues count] > 0) {
            prompt.selectedValues = selectedValues;
        }

        NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
        [ctr postNotificationName:PARENTPROMPTVALUECHANGED object:nil userInfo:nil];
    }
}
@end
