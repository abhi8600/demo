//
//  Settings.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Settings.h"
#import "ASIHTTPRequest.h"

NSString *const ATTR_TGT_SERVER = @"targetServer";

//main dashboard
NSString *const ATTR_MD_SPACE = @"mainDashSpace";
NSString *const ATTR_MD_SPACE_ID = @"mainDashSpaceId";
NSString *const ATTR_MD_USER = @"mainDashUser";
NSString *const ATTR_MD_SERVER = @"mainDashServer";

NSString *const ATTR_USERNAME = @"username";
NSString *const ATTR_PASSWORD = @"password";
NSString *const ATTR_REMEMBER_PASSWORD = @"rememberPassword";

NSString *const SETTINGS_FILE = @"Settings.plist";
NSString *const URL_BIRST = @"https://app.birst.com";
NSString *const URL_BETA = @"https://beta.birst.com";
NSString *const URL_SFOSFOO = @"http://192.168.90.61:6105";

NSString *const ATTR_VER_MAJOR = @"vMajor";
NSString *const ATTR_VER_MINOR = @"vMinor";
NSString *const ATTR_VER_REV = @"vRev";

@implementation Settings

@synthesize loginMode, majorVersion, minorVersion, revisionVersion, csrf;

static NSString *_server = nil;
static NSString *_redirectedServer = nil;
static NSString *_space = nil;
static BOOL _loggedIn = NO;
static BOOL _readOnly = NO;

static Settings *_instance;

+ (Settings*)instance{
    if (!_instance) {
        _instance = [[Settings alloc]init];
        _instance.loginMode = spaceListing;
    }
    return _instance;
}

+ (BOOL) readonly{
    [[Settings instance] settings];
    return _readOnly;
}

// Retrieve the settings parameters from our bundled Settings.plist file or
// the user's Document directory's Settings.plist.
- (NSMutableDictionary *) settings{

    if(!settings){
        NSDictionary *bundled = [Settings loadBundledSettings];
        if (bundled) {
            settings = [NSMutableDictionary dictionaryWithDictionary:bundled];
            
            //Check if settings can be modified
            BOOL readonly = [[bundled objectForKey:@"readonly"] boolValue];
            if (readonly) {
                _readOnly = YES;
            }else{
                //Check for user settings, load that otherwise
                NSMutableDictionary *userDict = [Settings loadUserSavedSettings];
                if(userDict){
                    //Use user's server settings
                    
                    NSArray *servers = [userDict objectForKey:@"servers"];
                    if([servers count] > 0){
                        [settings setObject:servers forKey:@"servers"];
                    }
                    
                    //Use user's default single values
                    NSArray *keys = [NSArray arrayWithObjects:ATTR_MD_USER, ATTR_MD_SERVER, ATTR_MD_SPACE, ATTR_MD_SPACE_ID, ATTR_TGT_SERVER, ATTR_USERNAME, ATTR_PASSWORD, ATTR_REMEMBER_PASSWORD, nil];
                    
                    for (NSString *k in keys) {
                        id value = [userDict objectForKey:k];
                        if(value){
                            [settings setObject:value forKey:k];
                        }
                    }
                    
                }else{
                    settings = [NSMutableDictionary dictionaryWithDictionary:bundled];
                }
            }
        }
        [settings retain];
    }
    
    return settings;
}

+ (NSMutableDictionary *) loadUserSavedSettings{
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:SETTINGS_FILE];
    
    return [Settings loadSettings:plistPath];
}

+ (NSMutableDictionary *) loadSettings:(NSString *)plistPath{
    NSMutableDictionary *settings = nil;
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    
    NSFileManager *mgr = [NSFileManager defaultManager];
    if ([mgr fileExistsAtPath:plistPath]) {
        [Settings upgradeToFileProtectionComplete:plistPath];
        NSData *plistXML = [mgr contentsAtPath:plistPath];
        settings = (NSMutableDictionary *)[NSPropertyListSerialization
                                                     propertyListFromData:plistXML
                                                     mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                                     format:&format
                                                     errorDescription:&errorDesc];
    }
    return settings;
}

+ (void) upgradeToFileProtectionComplete:(NSString *)path{
    NSError *error = nil;
    NSFileManager *mgr = [NSFileManager defaultManager];
    if([mgr fileExistsAtPath:path]){
        NSDictionary *attrs = [mgr attributesOfItemAtPath:path error:&error];
        if(error){
            NSLog(@"upgradeToFileProtectionComplete(): error get file attributes for file %@ : %@", path, [error localizedDescription]);
            error = nil;
        }
        if(![[attrs objectForKey:NSFileProtectionKey] isEqual:NSFileProtectionComplete]){
            attrs = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            [mgr setAttributes:attrs ofItemAtPath:path error:&error];
            if(error){
                NSLog(@"upgradeToFileProtectionComplete(): set file %@ to NSFileProtectionComplete failed: %@", path, [error localizedDescription]);
            }
        }
    }
}

+ (void) saveSavedSettings:(NSMutableDictionary*)settings{
    
    if([Settings readonly]){
        return;
    }
    
    //TODO - error alerts
    
    NSString *errorDesc = nil;
    NSData *data = [NSPropertyListSerialization dataFromPropertyList:settings format:NSPropertyListXMLFormat_v1_0 errorDescription:&errorDesc];
    if (!data) {
        NSLog(@"Error saving file: %@", errorDesc);
    }else{
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:SETTINGS_FILE];
        NSError *error;
        if (![data writeToFile:plistPath options:NSDataWritingFileProtectionComplete error:&error]){
            NSLog(@"Error saving settings file: %@", [error localizedDescription]);
        }
    }
}

// This is the Settings.plist file we include with the application
// for defaults
+ (NSDictionary *) loadBundledSettings{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
    if(path){
        return [Settings loadSettings:path];
    }
    return nil;
}

+ (NSString *)selectedServer{
    if (!_server) {
        NSMutableDictionary *settings = [[Settings instance] settings];        
        NSString *targetServer = [settings valueForKey:ATTR_TGT_SERVER];
        if (targetServer) {
            _server = [targetServer copy];
            return targetServer;
        }else{
            //Use the first in the list to be target server
            NSMutableArray *servers = [settings valueForKey:@"servers"];
            if (servers && [servers count] > 0) {
                _server = [[servers objectAtIndex:0] copy];
                [settings setValue:_server forKey:ATTR_TGT_SERVER];
                [Settings saveSavedSettings:settings];
            }else{
                _server = URL_BIRST;
            }
        }
        
    }
	return _server;
}

+ (NSString *) server{
    if(_redirectedServer){
        return _redirectedServer;
    }
    
    return [Settings selectedServer];
}

+ (void) setRedirectedServer:(NSString*)server{
    _redirectedServer = [server copy];
}

+ (void) setServer:(NSString*)server{
	if (!_readOnly && _server != server) {
		[_server release];
		_server = [server copy];
        
        //Save the settings
        NSMutableDictionary *settings = [[Settings instance] settings];
        [settings setValue:_server forKey:ATTR_TGT_SERVER];
        [Settings saveSavedSettings:settings];
	}
}


+ (NSString *) space{
	return [Settings valueForKey:ATTR_MD_SPACE];
}

+ (void) setSpace:(NSString*)space{
	if(!_readOnly && _space != space){
		[_space release];
		_space = [space copy];
		
		//Save the settings
        NSMutableDictionary *settings = [[Settings instance] settings];
        [settings setValue:_server forKey:ATTR_MD_SPACE];
        [Settings saveSavedSettings:settings];
	}		
}

+ (void) setSpaceId:(NSString*)spaceId{
    //Save the settings
    NSMutableDictionary *settings = [[Settings instance] settings];
    [settings setValue:spaceId forKey:ATTR_MD_SPACE_ID];
    [Settings saveSavedSettings:settings];
}

+ (BOOL) loggedIn{
	return _loggedIn;
}

+ (void) setLoggedIn:(BOOL)loggedIn{
    if(!loggedIn){
        _redirectedServer = nil;
    }
	_loggedIn = loggedIn;
}

+ (BOOL) isDevServer{
	NSRange range = [[Settings server] rangeOfString:@"6105"];
	return (range.length > 0);
}

+ (NSURL *) getNSURL:(NSString*)suffix{
	NSString *server = [NSString stringWithFormat:@"%@/%@", [Settings server], suffix];
	NSLog(@"getNSURL(): %@", server);
	return [NSURL URLWithString:server];
}

+ (void) setUsername:(NSString *)username password:(NSString*)password{
    if([password length] > 0 && [username length] > 0 && ![DEMO_USER isEqualToString:username]){
        NSMutableDictionary *settings = [[Settings instance] settings];
        [settings setValue:username forKey:ATTR_USERNAME];
        [settings setValue:password forKey:ATTR_PASSWORD];
        [Settings saveSavedSettings:settings];
    }
}

+ (NSString*) username{
	return [Settings valueForKey:ATTR_USERNAME];
}

+ (NSString*) password{
	return [Settings valueForKey:ATTR_PASSWORD];
}

+ (BOOL) isUsernamePasswordNotEmpty{
    NSString *usr = [Settings username];
    NSString *pass = [Settings password];
    
    return [usr length] > 0 && [pass length] > 0;
}

+ (NSURL *) logoutUrl{
	NSString *suffix = @"Login.aspx?logout=true";
	if ([Settings isDevServer]) {
		suffix =@"Login.aspx?logout=true";
	}
	return [Settings getNSURL:suffix];
}

+ (NSString *) currentPagesDir:(NSFileManager *)filemgr{
  return [Settings documentDirChild:filemgr subDir:@"CurrentPages"];  
}

+ (NSString *) offlineDir:(NSFileManager *)filemgr{
    return [Settings documentDirChild:filemgr subDir:@"OfflinePages"];
}

+ (NSString *) screenshotDir:(NSFileManager *)filemgr{
    return [Settings documentDirChild:filemgr subDir:@"Screenshots"];
}

+ (NSString *) documentDirChild:(NSFileManager *)filemgr subDir:(NSString *)subDir{
	NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docDir = [dirPaths objectAtIndex:0];
	NSString *dir = [NSString stringWithFormat:@"%@/%@", docDir, subDir];
	[filemgr release];
	return dir;
}

+ (NSString *) dashletReportCachePath:(NSString *)pageUuid dashletName:(NSString*)pathified{
	NSString *rootDir = [Settings currentPagesDir:[NSFileManager defaultManager]];
	return [NSString stringWithFormat:@"%@/%@/%@", rootDir, pageUuid, pathified];
}

+ (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port{
	NSString *server = [Settings server];
	
	//Switch 6105 port to 6103 for development server
	NSRange range = [server rangeOfString:@":6105"];
	if (range.length > 0 && port != 6105) {
		NSString* newUrl = [NSString stringWithFormat:@"%@:%d%@", [server substringToIndex:range.location], port, suffix];
		NSLog(@"sanitizePort: %@", newUrl);
		return newUrl;
	}
	
	NSString *sUrl = [NSString stringWithFormat:@"%@%@", [Settings server], suffix];
	NSLog(@"sanitizePort: %@", sUrl);
	return sUrl;
}

+ (BOOL) showSpaceListings{
    BOOL showSpaceListings = [[[[Settings instance] settings] objectForKey:@"spaceListings"]boolValue];

    NSLog(@"showSpaceListings = %d", showSpaceListings);
    return showSpaceListings;
}

+ (id) valueForKey:(NSString*)key{
    return [[[Settings instance]settings] objectForKey:key];
}

+ (void)setDefaultSpace:(NSString*)username server:(NSString*)server spaceId:(NSString*)spaceId name:(NSString*)name{
    NSMutableDictionary *settings = [[Settings instance]settings];
    
    [settings setObject:server forKey:ATTR_MD_SERVER];
    [settings setObject:username forKey:ATTR_MD_USER];
    [settings setObject:name forKey:ATTR_MD_SPACE];
    [settings setObject:spaceId forKey:ATTR_MD_SPACE_ID];
    
    [Settings saveSavedSettings:settings];
}

+ (NSString*)spaceId{
    return [Settings valueForKey:ATTR_MD_SPACE_ID];
}

+ (NSString*)mainDashUsername{
    return [Settings valueForKey:ATTR_MD_USER];
}

+ (NSString*)mainDashServer{
    return [Settings valueForKey:ATTR_MD_SERVER];
}

+ (void)setLoginMode:(LoginMode)mode{
    Settings *settings = [Settings instance];
    settings.loginMode = mode;
}

+ (void)setDemoMode:(BOOL)demoMode{
    Settings *settings = [Settings instance];
    settings.loginMode = demo;
}

+ (BOOL)demoMode{
    Settings *settings = [Settings instance];
    return settings.loginMode == demo;
}

+ (void)setRememberPassword:(BOOL)remember{
    NSNumber* value = [NSNumber numberWithBool:remember];
    NSMutableDictionary *settings = [[Settings instance]settings];
    [settings setObject:value forKey:ATTR_REMEMBER_PASSWORD];
    [Settings saveSavedSettings:settings];    
}

+ (BOOL) rememberPassword{
    NSNumber *v = [Settings valueForKey:ATTR_REMEMBER_PASSWORD];
    return [v boolValue];
}

+ (NSString*) appVersionString{
    NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* shortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
   return [NSString stringWithFormat:@"%@ (%@)", shortVersion, version];
}

+ (NSString *) getDashboardListVersion:(NSString*)response{
    if(response){
        NSRange range = [response rangeOfString:@"smiroot=&v=4"];
        return (range.length > 0) ? @"getDashboardListCached" : @"getDashbardListFromDB";
    }
    return @"getDashboardListCached";
}

+ (NSURL *) getDashboardModulePageUrl{
    Settings *inst = [Settings instance];
    /*
    if (inst.majorVersion < 5 || (inst.majorVersion == 5 && inst.minorVersion < 2)){
        return [Settings getNSURL:@"DashboardModule.aspx"];
    }else{
     */
        return [Settings getNSURL:@"FlexModule.aspx?birst.module=dashboard"];
    //}
}

+ (void) addCSRFHeader:(ASIHTTPRequest *) request{
    //csrf
    NSString *csrf = [Settings instance].csrf;
    if(csrf){
        [request addRequestHeader:@"X-XSRF-TOKEN" value:csrf];
    }
}

- (void)dealloc{
    [settings release];
    [super dealloc];
}

@end
