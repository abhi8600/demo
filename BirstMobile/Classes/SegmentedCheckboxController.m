//
//  SegmentedCheckboxController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 9/6/12.
//
//

#import "SegmentedCheckboxController.h"
#import "PromptValues.h"
#import "PromptControlHelper.h"

@interface SegmentedCheckboxController ()

@end

@implementation SegmentedCheckboxController

@synthesize prompt, control, label, pgCtrl, renderedInDashlet, backgroundColor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.label = [[[UILabel alloc] initWithFrame:CGRectMake(4, 0, self.view.frame.size.width, 20)]autorelease];
    self.label.font = [UIFont systemFontOfSize:12];
    self.label.text = self.prompt.name;
    
    if(self.backgroundColor){
        self.label.backgroundColor = self.backgroundColor;
        self.view.backgroundColor = self.backgroundColor;
    }
    
    [self.view addSubview:self.label];
    
    NSArray *labelVals = self.prompt.labelValues;
    int count = [labelVals count];
    if(count > 0){
        NSMutableArray *labels = [[NSMutableArray alloc] initWithCapacity:count];
        PromptValues *selected = nil;
        NSArray *selectedValues = self.prompt.selectedValues;
        if([selectedValues count] > 0){
            selected = [selectedValues objectAtIndex:0];
        }
        
        int selectedIdx = -1;
        for(int i = 0; i < count; i++){
            PromptValues *p = [labelVals objectAtIndex:i];
            if(selected){
                if(p.value == selected.value){
                    selectedIdx = i;
                }
            }else{
                if([p.value isEqualToString:self.prompt.defaultOption]){
                    selectedIdx = i;
                }
            }
        
            [labels addObject:p.label];
        }
        
        self.control = [[[UISegmentedControl alloc] initWithItems:labels] autorelease];
        self.control.apportionsSegmentWidthsByContent = YES;
        CGRect f = self.control.frame;
        f.origin.x = 4;
        f.origin.y = 20;
        f.size.height = 30;
        self.control.frame = f;
        [labels release];
        
        if(selectedIdx >= 0){
            self.control.selectedSegmentIndex = selectedIdx;
        }
        
        [self.view addSubview:self.control];
        
        [self.control addTarget:self action:@selector(segmentedControlChanged) forControlEvents:UIControlEventValueChanged];
    }
}

-(void)segmentedControlChanged{
    int idx = self.control.selectedSegmentIndex;
    NSLog(@"selected index %d", idx);
    
    PromptValues *selectedValue = [self.prompt.labelValues objectAtIndex:idx];
    [self.prompt.selectedValues removeAllObjects];
    [self.prompt.selectedValues addObject:selectedValue];
    
    if (prompt.isParentPrompt) {
        [PromptControlHelper parentPromptValueChanged:self prompt:prompt];
    }
    
    if (self.renderedInDashlet){
        [self inDashletApply];
    }
    
}

- (NSMutableArray*)getSelectedValues{
    return self.prompt.selectedValues;
}

- (void)inDashletApply{
    [self.pgCtrl updateGlobalPrompts];
	[self.pgCtrl renderDashlets:NO useCachedReportXml:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc{
    [prompt release];
    [control release];
    [label release];
    [backgroundColor release];
    
    [super dealloc];
}

@end
