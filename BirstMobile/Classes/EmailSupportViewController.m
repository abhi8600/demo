//
//  EmailSupportViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 6/28/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "EmailSupportViewController.h"


@implementation EmailSupportViewController

@synthesize mailViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *supportEmail = [Settings valueForKey:@"supportEmail"];
    NSArray *email = [NSArray arrayWithObject:supportEmail];
    NSString *subject = [Settings valueForKey:@"supportEmailSubject"];
    
    self.mailViewController = [[[MFMailComposeViewController alloc]init] autorelease];
    [self.mailViewController setMailComposeDelegate:self];
    [self.mailViewController setSubject:subject];
    [self.mailViewController setToRecipients:email];
    [self.mailViewController setMessageBody:@"" isHTML:NO];

    UIView *v = self.mailViewController.view;
    v.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:v];
}

- (void) viewWillAppear:(BOOL)animated{
    CGRect r = self.view.bounds;
    r.origin.y = self.navigationController.navigationBar.frame.size.height;
    self.mailViewController.view.frame = r;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    if (result == MFMailComposeResultFailed) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Request failed" message:@"Failed to send email" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }else if (result == MFMailComposeResultCancelled || result == MFMailComposeResultSaved){
        
    }
    
    [self.mailViewController.view removeFromSuperview];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
