    //
//  RequestPendingIndicatorController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "RequestPendingIndicatorController.h"
#import "Util.h"

@implementation RequestPendingIndicatorController

@synthesize activity, msg, message, parentBgColor;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
	if (self.message == nil) {
		self.message = @"Processing request";
	}
    
    if(self.parentBgColor){
        self.msg.textColor = [Util inverseColor:parentBgColor];
    }
    
	self.msg.text = message;
    
	CATransform3D rotationTransform = CATransform3DMakeRotation(1.0f * M_PI, 0, 0, 1.0);
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    
    rotationAnimation.toValue = [NSValue valueWithCATransform3D:rotationTransform];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = CGFLOAT_MAX;     
    
    [self.activity.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];

}

-(void) viewDidAppear:(BOOL)animated{
    if(self.view.superview){
        UIColor* bgColor = self.view.superview.backgroundColor;
        if(bgColor){
            self.msg.textColor = [Util inverseColor:bgColor];
        }else{
            self.msg.textColor = [UIColor whiteColor];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	[self.activity stopAnimating];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
