//
//  RootViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "RootViewController.h"
#import "DetailViewController.h"
#import "Settings.h"
#import "LoginViewController.h"
#import "OfflinePagesViewController.h"
#import "BirstWS.h"
#import "TutorialViewController.h"

@implementation RootViewController

@synthesize detailViewController, networkQueue;

-(LoginViewController*)loginCtrl{
    if(!loginCtrl){
        loginCtrl = [[LoginViewController alloc]init];
        loginCtrl.modalPresentationStyle = UIModalPresentationFormSheet;
        loginCtrl.delegate = self;
    }
    return loginCtrl;
}

#pragma mark -
#pragma mark View lifecycle

- (id) init{
	self = [super initWithStyle:UITableViewStyleGrouped];
	return self;
}

/*
-(id) initWithCoder:(NSCoder *)aDecoder{
	self = [super initWithStyle:UITableViewStyleGrouped];
	return self;
}*/

- (id) initWithStyle:(UITableViewStyle)style{
    initViewAppears = 0;
	self = [super initWithStyle:UITableViewStyleGrouped];
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Network queue which handles all our http requests
    //Set to 1 to force only one requests at a time
    self.networkQueue = [BirstWS createNetworkQueueMaxOne:self finishSel:@selector(onRequestFinished:) failSel:@selector(requestFailed:)];
    
    //This hack makes the navigation controller to start properly calling
    //viewWillAppear, viewDidAppear, etc
	[self.navigationController viewWillAppear:NO];
        
	//[self.tableView initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
	
	self.tableView.opaque = NO;
	self.tableView.backgroundView = nil;
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.separatorColor = [UIColor clearColor];
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;	
	
	self.clearsSelectionOnViewWillAppear = NO;
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
	
	self.detailViewController.rootViewCtrl = self;
}

- (void) onRequestFinished:(ASIHTTPRequest*)request{
    switch (request.tag) {
        case DemoDashboard:
            [self onGetDashboardModFinished:request];
            break;
        case DashboardModule:
            [self onGetDashboardModFinished:request];
            break;
        case SpaceDetail:
            [self onGetSpaceDetailsFinished:request];
            break;
        case DashboardList:
            [self onGetDashboardListFinished:request];
            break;
        case AllSpaces:
            [self onGetAllSpaces:request];
        default:
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"RootView viewWillAppear");
    
    [self.detailViewController setRightBarButtonAsLogin];
    
    //This required for the navigationcontroller hack outlined in viewDidLoad
    //The extra 1 call is bogus
    if(initViewAppears < 2){
        initViewAppears++;
        if(initViewAppears == 2){
            //Select about us at start up
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            [self.detailViewController displayAboutUs]; 
        }
        return;
    }
    
    [self.detailViewController clearSubviews];
    
    [super viewWillAppear:animated];
}

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	//return [Util orientationLockLandscape:interfaceOrientation];
    return YES;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
    // Return the number of sections.
    return 2;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	switch (section) {
		case 0: 
            //Spaces (if shown), Main Dashboards, Offline pages
			return [Settings showSpaceListings] ? 3 : 2;
		case 1:
            //Tutorial, Demo, About Us, Support
			return 4;
		default:
			return 0;
	}
}

// Title for the section
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	switch (section) {
		case 0:
			return @"Explore";
		case 1:
			return @"Help";
		default:
			return @"";
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    
    // Dequeue or create a cell of the appropriate type.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
	NSString *txt = @"";
	NSString *detailTxt = @"";
	
	if (indexPath.section == 0) {
        int index = [Settings showSpaceListings] ? indexPath.row : indexPath.row + 1;
		switch (index) {
			case 0:
				txt = @"Spaces";
				//detailTxt = @"List all spaces and its contents";
                cell.imageView.image = [UIImage imageNamed:@"SpaceIconGreen.png"];
				break;
			case 1:
				txt = [Settings showSpaceListings] ? @"Main dashboard" : @"Dashboards";
				//detailTxt = @"View the main dashboard";
                cell.imageView.image = [UIImage imageNamed:@"MainDashboard.PNG"];
				break;
			case 2:
				txt = @"Offline pages";
				//detailTxt = @"View saved offline pages";
                cell.imageView.image = [UIImage imageNamed:@"OfflinePages.PNG"];
				break;
			default:
				break;
		}
	}else if (indexPath.section == 1) {
		switch (indexPath.row) {
			case 0:
				txt = @"Tutorial";
                cell.imageView.image = [UIImage imageNamed:@"tutorial.PNG"];
				break;
            case 1:
                txt = @"Demo";
                cell.imageView.image = [UIImage imageNamed:@"demo.PNG"];
                break;
			case 2:
				txt = @"About us";
                cell.imageView.image = [UIImage imageNamed:@"aboutus.PNG"];
				break;
			case 3:
				txt = @"Support";
                cell.imageView.image = [UIImage imageNamed:@"support.PNG"];
				break;
			default:
				break;
		}
		
	}
	
    // Configure the cell.
	cell.opaque = NO;
	cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = txt;
	

	cell.textLabel.textColor = [UIColor whiteColor];
	cell.detailTextLabel.text = detailTxt;
	cell.detailTextLabel.textColor = [UIColor whiteColor];
	
    return cell;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *v = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 44)] autorelease];
	UILabel *l = [[[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.frame.size.width, 44)] autorelease];
	l.opaque = NO;
	//l.font = [UIFont fontWithName:@"Baskerville" size:22.0];
	l.backgroundColor = [UIColor clearColor];
	l.textColor = [UIColor whiteColor];
	if (section == 0){
		l.text = @"Explore";
	}else {
		l.text = @"Help";
	}

	[v addSubview:l];
	return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 44.0;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	if (indexPath.section == 0) { // Spaces
        NSLog(@"indexPath = %d", indexPath.row);
        int index = [Settings showSpaceListings] ? indexPath.row : indexPath.row + 1;
        
        Settings *settings = [Settings instance];
        
		//Check for login
		if (index == 0) {
			if ([Settings loggedIn] == NO || settings.loginMode != spaceListing) {
                self.loginCtrl.loginMode = spaceListing;
                self.loginCtrl.onLoginSuccess = @selector(onLoginSuccessGetSpaces:);
                [self presentViewController:loginCtrl animated:YES completion:nil];
				//[self presentModalViewController:loginCtrl animated:YES];
			}else {
				[self onLoginSuccessGetSpaces:nil];
			}
		}else if (index == 1){ // Main Dashboard / Dashboard
            //Check
            if([[Settings spaceId] length] == 0 || [[Settings space] length] == 0){
                [Util alert:@"Please set the default dashboard" title:@"Info"];
                return;
            }
            
            if([Settings loggedIn] == NO){
                [Settings setServer:[Settings mainDashServer]];
                self.loginCtrl.loginMode = mainDashboard;
                self.loginCtrl.onLoginSuccess = @selector(onLoginSuccessGetDashboards:);
				[self presentModalViewController:loginCtrl animated:YES];
            }else{
                
                [self onLoginSuccessGetDashboards:nil];
            }
        }else if (index == 2){ // Offline Pages
			OfflinePagesViewController *oc = [[[OfflinePagesViewController alloc] init] autorelease];
			oc.view.frame = self.view.bounds;
			oc.modalPresentationStyle = UIModalPresentationFullScreen;
			
			UINavigationController *nc = [[[UINavigationController alloc]initWithRootViewController:oc] autorelease];
			nc.navigationBar.barStyle = UIBarStyleBlackTranslucent;
			
			[self presentModalViewController:nc animated:YES];
		}
	}else if (indexPath.section == 1){
        switch (indexPath.row) {
            case 0: { //tutorial screen
                
                TutorialViewController *tutorialCtrl = [[TutorialViewController alloc] init];
                tutorialCtrl.modalPresentationStyle = UIModalPresentationFullScreen;
                
                UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:tutorialCtrl];
                navCtrl.navigationBar.barStyle = UIBarStyleBlackTranslucent;
                navCtrl.view.autoresizesSubviews = YES;
                
                [self presentModalViewController:navCtrl animated:YES];
                [tutorialCtrl release];
                [navCtrl release];
            }
                break;
            case 1: { //demo
                if([Settings loggedIn] == YES && ![Settings demoMode]){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Logout current user" message:@"In order to access the demo space, we will need to logout the current user." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Proceed", nil];
                    [alert show];
                    [alert release];
                }else{
                    [self getDemoDashboard];
                }
            }
                break;
            case 2: { // about us
                [self.detailViewController displayAboutUs];
            }
                break;
            case 3:  // support
                [Email emailSupport:@"" delegate:self]; 
                break;
            default:
                break;
        }
    }
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        //Logout
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[Settings logoutUrl]];
        [request startSynchronous];
        [Settings setLoggedIn:NO];
        
        [self getDemoDashboard];
    }
}

- (void) getDemoDashboard{
    //Demo url
    NSString *demoUrl = [Settings valueForKey:@"demoSpaceUrl"];
    NSString *demoServer = [Settings valueForKey:@"demoServer"];
    if([demoUrl length] > 0 && [demoServer length] > 0){
        [self.detailViewController startAnimateRequest:@"Logging in as demo user.."];
        [Settings setLoginMode:demo];      
        [Settings setServer:demoServer];
        NSURL *url = [NSURL URLWithString:demoUrl];
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        request.tag = DemoDashboard;
        [self.networkQueue addOperation:request];
    }
        
}

- (void)onLoginSuccessGetDashboards:(ASIHTTPRequest *)request{
    [Settings setLoginMode:mainDashboard];
    
    //Check settings
    NSString *spaceId = [Settings spaceId];
    NSString *spaceName = [Settings space];
    
    
    //Get space details, which calls get dashboards
    NSString *body = [NSString stringWithFormat:@"<tns:spaceID>%@</tns:spaceID><tns:spaceName>%@</tns:spaceName>", spaceId, spaceName];
    ASIHTTPRequest *spRequest = [BirstWS requestWithAdmin:@"GetSpaceDetails"];
    spRequest.tag = SpaceDetail;
    [BirstWS requestFormat:spRequest body:body]; 
    [self.networkQueue addOperation:spRequest];
}

- (void)onGetSpaceDetailsFinished:(ASIHTTPRequest*)request{
    NSURL *url = [Settings getNSURL:@"DashboardModule.aspx"];
    ASIHTTPRequest *dbrequest = [ASIHTTPRequest requestWithURL:url];
    dbrequest.tag = DashboardModule;
    [self.networkQueue addOperation:dbrequest];
}

- (void)onGetDashboardModFinished:(ASIHTTPRequest*)request{
    
    //Figure out if we're using a 5.0 version, hence we can use optimized
    //getDashbardListFromDB
    NSString *response = [Util getResponse:request];
    NSRange range = [response rangeOfString:@"smiroot=&v=4"];
    NSString *op = (range.length > 0) ? @"getDashboardListCached" : @"getDashbardListFromDB";
    
    [self.detailViewController startAnimateRequest:@"Retrieving dashboards.."];
    
    //Get the dashboard list
    ASIHTTPRequest *dbRequest = [BirstWS requestWithDashboard:op];
    dbRequest.tag = DashboardList;
    [BirstWS requestFormat:dbRequest body:@""];
    [self.networkQueue addOperation:dbRequest];
}

- (void)onGetDashboardListFinished:(ASIHTTPRequest *)request{
    [self.detailViewController stopAnimateRequest];
    
	NSString *response = [request responseString];
    
    //Format
    NSString *mStr = [Util xmlUnescape:response];
    
	//TODO - verify response
	DashboardsViewController* dashsCtrl = [[[DashboardsViewController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
	dashsCtrl.detailCtrl = self.detailViewController;
    NSString *title = [Settings space];
    //Demo space
    if([Settings demoMode]){
        title = @"Demo";
        [Settings setLoggedIn:YES];
    }
    dashsCtrl.title = title;
    dashsCtrl.spaceId = [Settings spaceId];
    dashsCtrl.spaceName = [Settings space];
    dashsCtrl.setAsDefaultBttn = NO;
    [dashsCtrl parse:mStr];
	[self.navigationController pushViewController:dashsCtrl animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultFailed) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Request failed" message:@"Failed to send email" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

//TODO: The SpacesTableViewController uses similar logic to login. Consolidate them.
//Called when login is successfull
- (void)onLoginSuccessGetSpaces:(ASIHTTPRequest *)request{
    [Settings setLoginMode:spaceListing];
    
    ASIHTTPRequest *spRequest = [BirstWS requestWithAdmin:@"GetAllSpaces"];
    spRequest.tag = AllSpaces;
    [BirstWS requestFormat:spRequest body:@""];
    [self.networkQueue addOperation:spRequest];
}

- (void)onGetAllSpaces:(ASIHTTPRequest *)request
{
	NSString *responseString = [request responseString];
	NSLog(@"getAllSpaces response: %@", responseString);
	
	[loginCtrl dismissModalViewControllerAnimated:YES];
	[loginCtrl release];
	loginCtrl = nil;
	
	SpacesTableViewController *spacesCtrl = [[[SpacesTableViewController alloc] init]autorelease];
	spacesCtrl.rootCtrl = self;
	spacesCtrl.detailCtrl = self.detailViewController;
	
	[self.navigationController pushViewController:spacesCtrl animated:YES];
	[spacesCtrl parse:responseString];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self.detailViewController stopAnimateRequest];
    
	[loginCtrl dismissModalViewControllerAnimated:YES];
	[loginCtrl release];
	loginCtrl = nil;
	 
	NSLog(@"Failed login");
    
	[Settings setLoginMode:spaceListing];
    
	[Util requestFailedAlert:[request error]];
	
}

- (NSMutableDictionary*) parseLoginPageParams:(NSString*)response {
	NSMutableDictionary *keyValues = [[[NSMutableDictionary alloc] init] autorelease];
	
	if(response != nil){
		[self getParamValue:response parameter:@"__VIEWSTATE" dictionary:keyValues];
		[self getParamValue:response parameter:@"__PREVIOUSPAGE" dictionary:keyValues];
		[self getParamValue:response parameter:@"__EVENTVALIDATION" dictionary:keyValues];
		
		[keyValues setObject:@"" forKey:@"__LASTFOCUS"];
		[keyValues setObject:@"" forKey:@"__EVENTTARGET"];
		[keyValues setObject:@"" forKey:@"__EVENTARGUMENT"];
		[keyValues setObject:@"0" forKey:@"LoginForm$LoginButton.x"];
		[keyValues setObject:@"0" forKey:@"LoginForm$LoginButton.y"];
	}
	
	return keyValues;
}

- (BOOL) getParamValue:(NSString*)response parameter:(NSString*)param dictionary:(NSMutableDictionary*)dict {
	if(response != nil){
		NSString* paramValue = [param stringByAppendingString:@"\" value=\""];
		NSRange range = [response rangeOfString:paramValue];
		if(range.length > 0){
			int startIdx = range.location + range.length;
			int numChars = 0;
			for(int i = startIdx; i < [response length]; i++){
				if ([response characterAtIndex:i] == '"') {
					break;
				}
				numChars++;
			}
			NSString* value = [response substringWithRange:NSMakeRange(startIdx, numChars)];
			NSLog(@"set key: %@ value: %@", param, value);
			[dict setObject:value forKey:param];
			return YES;
		}
	}
	return NO;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [loginCtrl release];
    [detailViewController release];
    [networkQueue reset];
    [networkQueue release];
    [super dealloc];
}


@end

