//
//  Settings.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

#define DEMO_USER @"Demo User"

typedef enum{
    demo,
    mainDashboard,
    spaceListing
}LoginMode;


extern NSString *const URL_BIRST;
extern NSString *const URL_SFOSFOO;

@interface Settings : NSObject {
    NSMutableDictionary *settings;
    LoginMode loginMode;
    NSString *csrf;
}

@property (nonatomic, readonly) NSMutableDictionary *settings;
@property (nonatomic, assign) LoginMode loginMode;
@property (nonatomic) int majorVersion;
@property (nonatomic) int minorVersion;
@property (nonatomic) int revisionVersion;
@property (nonatomic, retain) NSString *csrf;

+ (Settings*) instance;
+ (BOOL) readonly;

- (NSMutableDictionary *) settings;
+ (NSMutableDictionary *) loadUserSavedSettings;
+ (NSDictionary *) loadBundledSettings;
+ (NSMutableDictionary *) loadSettings:(NSString *)plistPath;
+ (void) saveSavedSettings:(NSMutableDictionary*)settings;

+ (void) setRedirectedServer:(NSString*)server;
+ (NSString *)selectedServer;
+ (NSString *) server;
+ (void) setServer:(NSString*)server;
+ (NSURL *) getNSURL:(NSString*)suffix;

+ (NSString *) space;
+ (void) setSpace:(NSString*)space;

+ (void) setSpaceId:(NSString*)spaceId;
+ (NSString *)spaceId;

+ (BOOL) loggedIn;
+ (void) setLoggedIn:(BOOL)loggedIn;
+ (BOOL) isDevServer;

+ (NSString *) username;
+ (NSString *) password;
+ (void) setUsername:(NSString *)username password:(NSString*)password;

+ (NSURL *) logoutUrl;
+ (NSString *) offlineDir:(NSFileManager*)fileMgr;
+ (NSString *) screenshotDir:(NSFileManager *)filemgr;
+ (NSString *) currentPagesDir:(NSFileManager *)filemgr;
+ (NSString *) documentDirChild:(NSFileManager *)filemgr subDir:(NSString *)subDir;
+ (NSString *) dashletReportCachePath:(NSString *)pageUuid dashletName:(NSString*)pathified;
+ (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port;
+ (BOOL) showSpaceListings;
+ (id) valueForKey:(NSString*)key;
+ (void)setDefaultSpace:(NSString*)username server:(NSString*)server spaceId:(NSString*)spaceId name:(NSString*)name;
+ (void)setLoginMode:(LoginMode)demoMode;
//+ (void)setDemoMode:(BOOL)demoMode;
+ (BOOL)demoMode;
+ (void)setRememberPassword:(BOOL)remember;
+ (BOOL)rememberPassword;
+ (NSString*) mainDashUsername;
+ (NSString*) mainDashServer;
+ (NSString*) appVersionString;
+ (NSString *) getDashboardListVersion:(NSString*)response;
+ (NSURL *) getDashboardModulePageUrl;
+ (void) upgradeToFileProtectionComplete:(NSString *)path;
+ (BOOL) isUsernamePasswordNotEmpty;


+ (void) addCSRFHeader:(ASIHTTPRequest *) request;
@end
