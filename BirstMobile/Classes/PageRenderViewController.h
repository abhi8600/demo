//
//  PageRenderViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Page.h"
#import "Dashlet.h"
#import "DashletController.h"
#import "PromptsViewController.h"
#import "Webservice.h"
#import "PagesViewController.h"
#import "RequestPendingIndicatorController.h"

#import "FlexFieldController.h"
#import "QueryListTableController.h"
#import "SingleSelectListPickerController.h"

@class DashletController;

typedef enum{
    Regular,
    Offline,
    Oem
}ViewMode;

@interface PageRenderViewController : UIViewController<UIScrollViewDelegate, UINavigationControllerDelegate> {
	IBOutlet UIToolbar *toolbar;
	IBOutlet UIBarButtonItem *prompts;
	IBOutlet UIScrollView *scrollView;
	IBOutlet UIBarButtonItem *savePage;
	IBOutlet UILabel *reportTitle;
	
	Page *page;
	NSMutableArray *viewCtrls;
	NSMutableArray *views;
	int currentPage;
	UIPopoverController *promptsCtrl;
	UIPopoverController *saveCtrl;
	
	NSString *initPromptsXml;
	
    Webservice *sessionsSvc;
	Webservice *promptsSvc;
    Webservice *filteredPromptsSvc;
    Webservice *drillDownSvc;
    
    NSNotification *drillToDashboardNotification;
    
    RequestPendingIndicatorController *reqPendingCtrl;
    
    DashletController *drillDownDashlet;
    BOOL isDrillDownPromptExist;
    int numReportFileWrites;
    
    int numRendered;
    
    ViewMode viewMode; 
    BOOL noReportTitle;
    BOOL screenShot;
    BOOL isSpaceScreenshot;

    NSMutableArray *dashletPrompts;
    ASIHTTPRequest *getFilterRequest;
    
    BOOL isParentRequestPending;
}

@property (nonatomic, retain) Page *page;

@property (nonatomic, assign) IBOutlet UIBarButtonItem *savePage;

@property (nonatomic, retain) UIPopoverController *promptsCtrl;
@property (nonatomic, retain) UIPopoverController *saveCtrl;

@property (nonatomic, retain) NSString *initPromptsXml;
@property (nonatomic, readonly) Webservice *promptsSvc;
@property (nonatomic, readonly) Webservice *sessionsSvc;
@property (nonatomic, readonly) Webservice *filteredPromptsSvc;
@property (nonatomic, readonly) Webservice *drillDownSvc;
@property (nonatomic, assign) ViewMode viewMode;
@property (nonatomic, assign) BOOL noReportTitle;
@property (nonatomic, assign) BOOL screenShot;
@property (nonatomic, assign) BOOL isSpaceScreenshot;
@property (assign) UIColor *backgroundColor;
@property (nonatomic, retain) NSNotification *drillToDashboardNotification;

@property (nonatomic, retain) RequestPendingIndicatorController *reqPendingCtrl;

@property (retain) NSMutableArray *dashletPrompts;
@property (nonatomic, assign) ASIHTTPRequest *getFilterRequest;
@property (nonatomic, assign) BOOL isParentRequestPending;

- (IBAction)done;
- (IBAction)doPrompts;
- (IBAction)refresh;
- (IBAction)save;
- (void)alignSubviews;
- (NSString *)titleWithDashlet:(Dashlet *) dash;
- (void)renderDashlets:(BOOL)renderFromFile useCachedReportXml:(BOOL)useCacheReportXml;
- (void)getPromptsValuesRender;
- (void)getParentPromptsValues;
- (void) renderPage;
- (void)dismissSavePopover;
- (void) dismissPromptPopover;
- (void)saveOffline;
- (void)drillToDashboard:(NSNotification*)notification;
- (void)parentPromptValueChanged:(NSNotification*)notification;
- (void)removePendingRequestView;
- (void)getSessionVars;
- (void)sessionsFinished:(ASIHTTPRequest*)request;
- (void)dashletCachedRenderedReport:(Dashlet *)dashlet;
- (void)updateGlobalPrompts;
- (void)parentPromptValuesRequestFinished:(ASIHTTPRequest *)request;
- (void)parentPromptValuesRequestFailed:(ASIHTTPRequest *)request;
- (void)pageParsePromptValues:(ASIHTTPRequest *)request isParentChildren:(BOOL)isParentChildren;
- (void)getParentPromptsValuesRender:(ASIHTTPRequest *)request;
- (void)doGetParentPromptsValues:(BOOL)render;
- (void)parsePromptsValuesRender:(ASIHTTPRequest *)request;
- (void)parentPromptValuesRequestFinishedRender:(ASIHTTPRequest *)request;
- (void)promptsValuesRequestFailed:(ASIHTTPRequest *)request;
- (void)requestFailed:(ASIHTTPRequest *)request;
- (void)onDrillDownFinished:(ASIHTTPRequest *)request;
- (void)onDrillDownFailed:(ASIHTTPRequest *)request;
- (void)getPromptsValues:(SEL)finishedSel failSel:(SEL)failSel;
-(void)getPromptsValuesWithStr:(NSString *)promptStr finishedSel:(SEL)finishedSel failSel:(SEL)failSel;
- (void)fillPromptValues:(NSString *)str;
- (void)parsePromptsValuesRenderUseCache:(ASIHTTPRequest *)request;
- (void)hideBarButton:(UIBarButtonItem*)item;
- (void)drillToUrl:(NSNotification *)notification;
- (NSString *) getPageTitle;
- (void) doLayout:(UIView *)view layout:(Layout*)layout rect:(CGRect)rect promptXml:(NSString*)promptXml renderFromFile:(BOOL)renderFromFile useCachedReportXml:(BOOL)useCachedReportXml;
-(void)captureScreenshot;

@end
