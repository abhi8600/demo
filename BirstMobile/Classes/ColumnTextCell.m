//
//  ColumnTextCell.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/16/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "ColumnTextCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Util.h"
#import "NSStringCategory.h"

@implementation ColumnTextCell

@synthesize data, drillAttrs;

/*
+(Class)layerClass{
	return [CATiledLayer class];
}*/

- initWithXML:(TBXMLElement *)xml frame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self) {

        self.userInteractionEnabled = YES;
        self.data = xml;

		//background color
		self.backgroundColor = [self colorWithHexString:[self attribute:@"backcolor"]];
		
		//color
		self.textColor = [self colorWithHexString:[self attribute:@"color"]];
		
		//alignment
		NSString *alignment = [self attribute:@"alignment"];
		if (alignment != nil) {
			if ([@"left" isEqualToString:alignment]) {
				self.textAlignment = UITextAlignmentLeft;
			}else if ([@"right" isEqualToString:alignment]){
				self.textAlignment = UITextAlignmentRight;
			}else if ([@"center" isEqualToString:alignment]) {
				self.textAlignment = UITextAlignmentCenter;
			}
		}
		
        _width = [[TBXML valueOfAttributeNamed:@"width" forElement:self.data] floatValue];
        _height = [[TBXML valueOfAttributeNamed:@"height" forElement:self.data] floatValue];
         
		//font
		NSString *face = [self attribute:@"face"];
		NSString *size = [self attribute:@"size"];
		[self setFont:[UIFont fontWithName:face size:[size floatValue]]];
		 
        NSString *txt = [[self attribute:@"value"] stringByDecodingXMLEntities ];
        
        //Do wrapearound if the text doesn't fit
        self.lineBreakMode = UILineBreakModeWordWrap;
        self.numberOfLines = 0;
        
        //value
        self.text = txt;

        //drill attributes
        NSString *link = [TBXML valueOfAttributeNamed:@"link" forElement:xml];
        if([link length] > 0){
            DrillAttributes *t = [[DrillAttributes alloc]initWithLinkString:link];
            self.drillAttrs = t;
            [t release];
        }
	}
	return self;
}

-(NSString *) attribute:(NSString *)attr{
	if (self.data) {
		return [TBXML valueOfAttributeNamed:attr forElement:self.data];
	}
	return nil;
}

-(void)drawTextInRect:(CGRect)rect{
	if (self.data) {
		[super drawTextInRect:[self textRect]];
		[self addBorders:rect];
	}else {
		[super drawTextInRect:rect];
	}
}

- (CGRect) textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines{
    return [self textRect];
}

- (CGRect) textRect{
    CGSize size = self.bounds.size;
    if(_width < size.width || _height < size.height){
        return CGRectMake(self.bounds.origin.x, self.bounds.origin.y, _width, _height);
    }
    return self.bounds;
}

- (void) parse:(TBXMLElement *)xml{
	self.data = xml;
	[self setNeedsDisplay];
}

- (void)addBorders:(CGRect)rect{
	CGContextRef context = UIGraphicsGetCurrentContext();
	
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    
	float lineWidth = -1;
	
	CGFloat x = rect.origin.x;
	CGFloat y = rect.origin.y;
	
	//top border
	if ((lineWidth = [self setBorderColorWidth:context location:@"topBorder"]) != -1){
		CGContextMoveToPoint(context, x, y);
		CGContextAddLineToPoint(context, x + width, y);
		CGContextStrokePath(context);
	}
	
	//right border
	if ((lineWidth = [self setBorderColorWidth:context location:@"rightBorder"]) != -1) {
		CGContextMoveToPoint(context, x + width, y);
		CGContextAddLineToPoint(context, x + width, y + height);
		CGContextStrokePath(context);
	}
	
	//bottom border
	if ((lineWidth = [self setBorderColorWidth:context location:@"bottomBorder"]) != -1) {
		CGContextMoveToPoint(context, x + width, y + height);
		CGContextAddLineToPoint(context, x, y + height);
		CGContextStrokePath(context);		
	}
	
	//left border
	if ((lineWidth = [self setBorderColorWidth:context location:@"leftBorder"]) != -1) {
		CGContextMoveToPoint(context, x, y + height);
		CGContextAddLineToPoint(context, x, y);
		CGContextStrokePath(context);		
	}
	
}

-(float) setBorderColorWidth:(CGContextRef)context location:(NSString*)loc{
	float lineWidth = -1;
	
    if(loc != nil && self.data != nil){
        NSString *bcs = [TBXML valueOfAttributeNamed:[NSString stringWithFormat:@"%@Color", loc] forElement:self.data];
        if (bcs) {
            CGContextSetStrokeColorWithColor(context, [self colorWithHexString:bcs].CGColor);
            lineWidth = [[TBXML valueOfAttributeNamed:[NSString stringWithFormat:@"%@Width", loc] forElement:self.data] floatValue];
            CGContextSetLineWidth(context, lineWidth);
        }
	}
    
	return lineWidth; 
}

-(UIColor *) colorWithHexString:(NSString *)stringToConvert  
{  
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
	
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return nil;  
	
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
	
	// strip # if it appears  
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];  
	
    if ([cString length] != 6) return nil;  
	
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
	
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
	
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
	
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
	
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
}  

-(void)handleTap:(UITapGestureRecognizer*)tapEvent{
    NSLog(@"Column text handleTap()"); 
    [self.drillAttrs notify];
}

-(void) dealloc{
    [drillAttrs release];
    [super dealloc];
}

@end
