//
//  SideBarMain.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/25/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASINetworkQueue.h"
#import "JTRevealSidebarV2Delegate.h"
#import "LoginViewController.h"
#import "SidebarViewController.h"
#import "RequestPendingIndicatorController.h"
#import "UIWebViewWrapper.h"

@interface MainViewController : UIViewController <JTRevealSidebarV2Delegate, UITableViewDelegate, SidebarViewControllerDelegate> {
    CGPoint _containerOrigin;
    SidebarViewController *leftSidebarViewController;
    ASINetworkQueue *networkQueue;
    LoginViewController *loginCtrl;
    RequestPendingIndicatorController *requestPendingCtrl;
    UIWebViewWrapper *dummy;
}

extern BOOL createdDummyChart;

@property (nonatomic, strong) SidebarViewController *leftSidebarViewController;
@property (nonatomic, assign) BOOL checkLogin;
@property (nonatomic, retain) ASINetworkQueue *networkQueue;
@property (nonatomic, retain) LoginViewController *loginCtrl;
@property (nonatomic, retain) RequestPendingIndicatorController *requestPendingCtrl;
@property (nonatomic, retain) UIWebViewWrapper *dummy;

- (void) sessionTimedOut;
- (void) revealLeftSideBar:(id)sender;
- (void) showPendingRequest:(NSString *)message;
- (void) removePendingRequest;
- (void) displayAboutUs:(SidebarViewController*)sidebarViewController;
- (void) displayLogin:(SidebarViewController*)sidebarViewController;
- (void) displayDemoDashboard:(SidebarViewController*)sidebarViewController;
- (void) displayOfflinePages:(SidebarViewController*)sidebarViewController;
- (void) displayEmailSupport:(SidebarViewController*)sidebarViewController;
- (void) displaySpaces:(SidebarViewController*)sidebarViewController row:(NSInteger)row;
- (void) displaySubView:(SidebarViewController*)sidebarViewController mainVC:(MainViewController*)mainVC revealSideBar:(BOOL)revealSideBar;
- (void) onRequestFailed:(ASIHTTPRequest *)request;
- (void) dismissLogin;
- (void) createInitialDummyChart;
@end
