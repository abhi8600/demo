//
//  LoginViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Settings.h"

@interface LoginViewController : UIViewController {
	IBOutlet UITextField *usernameField;
	IBOutlet UITextField *passwordField;
	IBOutlet UISwitch *demoUserSwitch;
	
	IBOutlet UINavigationBar *navBar;
	IBOutlet UIButton *loginBttn;
	IBOutlet UIButton *cancelBttn;
    
    IBOutlet UISwitch *rememberPassword;
    IBOutlet UILabel *serverName;
	
	IBOutlet UIActivityIndicatorView *activity;
	IBOutlet UILabel *loginMessage;
	
    LoginMode loginMode;
    SEL onLoginSuccess;
}

@property (readonly) UITextField *usernameField;
@property (readonly) UITextField *passwordField;
@property (retain) ASIHTTPRequest *request;
@property (nonatomic, assign) SEL onLoginSuccess;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) LoginMode loginMode;

- (IBAction) login;
- (IBAction) cancel;
- (IBAction) remememberPasswordChanged:(id)sender;

- (void) startAnimate;
- (void) endAnimate;

@end

@protocol LoginViewControllerDelegate <NSObject>
@optional
- (void)loginSuccessFull:(LoginViewController *)loginView;

@end


