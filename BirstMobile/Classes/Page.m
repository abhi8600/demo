//
//  Page.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Page.h"
#import "Util.h"
#import	"Dashlet.h"
#import "SessionVariables.h"
#import "GlobalPromptValues.h"
#import "PromptValues.h"
#import "Log.h"
#import "NSStringCategory.h"

@implementation Page

@synthesize name, dir, uuid, modifiable, invisible, requireSessionVars, drillAttributes, renderedDashletsCached, isOffline, offlineDir, isPartial, layout, bgColor;

- (id) init{
	isOffline = NO;
    invisible = NO;
    requireSessionVars = NO;
    
	return [super init];
}

- (void) parse:(TBXMLElement*)xml{
   
    self.renderedDashletsCached = NO;
    
	if (xml) {
        
		self.name = [TBXML valueOfAttributeNamed:@"name" forElement:xml];
		self.dir = [TBXML valueOfAttributeNamed:@"dir" forElement:xml];
		self.uuid = [TBXML valueOfAttributeNamed:@"uuid" forElement:xml];
		self.bgColor = [TBXML valueOfAttributeNamed:@"bgColor" forElement:xml];
        
		//[Log debug:@"Processing page: %@", self.name];
	
        //Partial page (optimized, need to call getPage() later to get full information)
        NSString* partial = [TBXML valueOfAttributeNamed:@"partial" forElement:xml];
        self.isPartial = (partial && [partial isEqualToString:@"true"]);
        
		TBXMLElement *node = xml->firstChild;
		if (node) {
			do {
				NSString *nName = [TBXML elementName:node];
				
				if ([@"Prompts" isEqualToString:nName]) {
					[self parsePrompts:node->firstChild];
				}
				
				if([@"Layout" isEqualToString:nName]){
					[self recurseFindDashlets:node->firstChild];
                    
                    self.layout = [[[Layout alloc]init] autorelease];
                    [self.layout parse:node];
				}
                
                if([@"Invisible" isEqualToString:nName]){
                    self.invisible = [[TBXML textForElement:node] isEqualToString:@"true"];
                }
			} while ((node = node->nextSibling));
		}
	}
	
}

- (NSMutableArray*) dashlets{
	if (dashlets == nil) {
		dashlets = [[NSMutableArray alloc] init];
	}
	return dashlets;
}

- (NSMutableArray *) prompts{
	if (!prompts) {
		prompts = [[NSMutableArray alloc] init];
	}
	return prompts;
}

- (NSMutableArray *) visiblePrompts{
	if (!visiblePrompts) {
		visiblePrompts = [[NSMutableArray alloc] init];
	}
	return visiblePrompts;
}

- (NSMutableDictionary *) promptsHash{
	if (!promptsHash) {
		promptsHash = [[NSMutableDictionary alloc] init];
	}
	return promptsHash;
}

- (BOOL) hasParentPrompts{
    return [parentChildrenPrompts count] > 0;
}

- (void) addDrillDownPrompts:(NSString *)xml{
    TBXML* tbxml = [[TBXML alloc]initWithXMLString:xml];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *p = [Util traverseFindElement:root target:@"Prompt"];
    if(p){
        do{
            Prompt *prompt = [[Prompt alloc]init];
            [prompt parse:p];
            [self.prompts addObject:prompt];
            [self.promptsHash setObject:prompt forKey:prompt.parameterName];
            [prompt release];
        }while((p = p->nextSibling));
    }
    [tbxml release];
}

- (void) parsePrompts:(TBXMLElement *)p{
	if (p) {
		if ([self.prompts count] > 0) {
			[self.prompts removeAllObjects];
		}
		
		if ([self.promptsHash count] > 0) {
			[self.promptsHash removeAllObjects];
		}
		
		do {
			Prompt *prompt = [[Prompt alloc]init];
			[prompt parse:p];
			[self.prompts addObject:prompt];
            
            if (!prompt.isInvisible){
                [self.visiblePrompts addObject:prompt]; 
            }
            
			[self.promptsHash setObject:prompt forKey:prompt.parameterName];
			
            //Session vars
            NSString *defaultOptionSessionName = prompt.defaultOptionSessionVar;
            if(defaultOptionSessionName) {
                self.requireSessionVars = YES;
                [Log debug:@"***** Page %@  name = %@ requires session variable", self, self.name];
                SessionVariables *session = [SessionVariables instance];
                if (![session.sessionVars objectForKey:prompt.defaultOptionSessionVar]) {
                    //temp value
                    [session.sessionVars setValue:@"NO_FILTER" forKey:defaultOptionSessionName]; 
                }
            }
            
            //Parent child prompt
            NSArray *parents = prompt.parentPrompt;
            if ([parents count] > 0) {
                if(!parentChildrenPrompts){
                    parentChildrenPrompts = [[NSMutableDictionary alloc]init];
                }
                for(int i = 0; i < [parents count]; i++){
                    NSString *parent = [parents objectAtIndex:i];
                    NSMutableArray *children = [parentChildrenPrompts objectForKey:parent];
                    if (!children) {
                        children = [[NSMutableArray alloc]initWithObjects:prompt, nil];
                        [parentChildrenPrompts setValue:children forKey:parent];
                        [children release];
                    }else{
                        [children addObject:prompt];
                    }
                }
            }
            
            [prompt release];
			//test
			//[Log debug:[prompt toXmlString]];
		} while ((p = p->nextSibling));
        
        //Mark the parent prompts
        for (NSString *parent in [parentChildrenPrompts allKeys]) {
            Prompt *p = [promptsHash objectForKey:parent];
            p.isParentPrompt = YES;
        }
	}
}

- (void) parsePromptValues:(NSString *)values isParentChildren:(BOOL)isParentChildren{
	[Log debug:@"parsing prompt values"];
	if (values) {
		TBXML *tbxml = [[TBXML alloc]initWithXMLString:values];
		TBXMLElement *root = tbxml.rootXMLElement;
		if (root) {
			TBXMLElement *node = [Util traverseFindElement:root target:@"PromptValue"];
			if (node) {
				do {
					TBXMLElement *promptId = [TBXML childElementNamed:@"PromptID" parentElement:node];
					if (promptId){
                        
                        //Is a parent prompt, no values
                        if(isParentChildren && promptId->nextSibling == nil){
                            continue;
                        }
                        
						NSString *key = [TBXML textForElement:promptId];
						[Log debug:@"Parsing prompt of promptID %@", key];
						Prompt* p = [self.promptsHash objectForKey:key];
						[p parseValues:promptId->nextSibling];
					}
				} while ((node = node->nextSibling));
			}
		}
        [tbxml release];
	}
}

- (void) recurseFindDashlets:(TBXMLElement *)root{
	if (root) {
		do {
			if ([@"dashlet" isEqualToString:[TBXML elementName:root]]) {
				Dashlet *d = [[Dashlet alloc]init];
				[d parse:root];
				
				[self.dashlets addObject:d];
				[d release];
			}

			if (root->firstChild){
				[self recurseFindDashlets:root->firstChild];
			}		
			
		} while ((root = root->nextSibling)); 
	}
}

- (NSString *) getPromptsXml{
	return [self getPromptsXmlValues:NO encode:YES ignoreList:nil];
}

-(NSString *)getPromptsXmlValues:(BOOL)setSelectedValues encode:(BOOL)encode ignoreList:(NSSet*)ignoreList{
    
	NSMutableString *root = [NSMutableString stringWithString:@"<Prompts>"];
	for (int i = 0; i < [self.prompts count]; i++) {
		Prompt *p = [self.prompts objectAtIndex:i];
        if(![ignoreList containsObject:p.name]){
            [p appendToString:root addSelectedValues:setSelectedValues];
        }
	}
	[root appendString:@"</Prompts>"];
	if (encode) {
		root = [Util xmlSimpleEscape:root];
	}
	return root;
}

- (NSString *)getParentChildPromptsXmlValues{
    NSMutableSet *alreadyIn = [[NSMutableSet alloc]init];
    
    NSMutableString *root = [NSMutableString stringWithString:@"<Prompts>"];
    
    for (NSString* key in [parentChildrenPrompts allKeys]) {
        Prompt *p = [self.promptsHash objectForKey:key];
        [p appendToString:root addSelectedValues:YES];
        
        NSArray *children = [parentChildrenPrompts objectForKey:key];
        for(int i = 0; i < [children count]; i++){
            Prompt *c = [children objectAtIndex:i];
            if ([alreadyIn containsObject:c]) {
                continue;
            }
            [c appendToString:root addSelectedValues:YES];
            [alreadyIn addObject:c];
        }
    }
    [alreadyIn release];
    
    [root appendString:@"</Prompts>"];
    root = [Util xmlSimpleEscape:root];
	
	return root;
}

- (void) resetPromptValues{
	for(int i = 0; i < [self.prompts count]; i++){
		Prompt *p = [self.prompts objectAtIndex:i];
		[p clearSelectedValues];
	}
}

// Drill to Dashboard
// Populate the prompts with selected values from the passed drill to attributes
// this should be a one time thing, we remove drill attributes after that
- (void) useDrillFilters{
    if(self.drillAttributes){
        NSString *filters = self.drillAttributes.filters;
    
        if (filters) {
            NSArray *ampersands = [filters componentsSeparatedByString:@"&"];
            for (int i = 0; i < [ampersands count]; i++) {
                NSString *amp = [ampersands objectAtIndex:i];
               
                NSArray *equals = [amp componentsSeparatedByString:@"="];
                if ([equals count] >= 2) {
                    //TODO: parse out encodings see DashletContentBase.as line 264
                    
                    NSString *promptName = [equals objectAtIndex:0];
                    NSString *promptValue = [equals objectAtIndex:1];
                    promptValue = [promptValue stringByUrlDecode];
                    
                    Prompt *p = [self.promptsHash valueForKey:promptName];
                    if (p) {
                        [p setSelectedValue:promptValue force:YES];
                    }else{
                        //Do check on ColumnName
                        for (NSString *key in self.promptsHash){
                            Prompt *p = [self.promptsHash valueForKey:key];
                            if([p.columnName isEqualToString:promptName]){
                                [p setSelectedValue:promptValue force:YES];
                                break;
                            }
                        }
                    }
                }
            }
        }
        self.drillAttributes = nil;
    }
}

// Drill down
// Parse the filter string and set the prompt value if it exists
- (BOOL) setPromptEqualsDrillFilterValue:(NSString*)filterStr{
    NSArray *equals = [filterStr componentsSeparatedByString:@"="];
    if ([equals count] >= 2) {
        NSString *promptName = [equals objectAtIndex:0];
        NSString *promptValue = [equals objectAtIndex:1];
        
        Prompt *p = [self.promptsHash valueForKey:promptName];
        if (p){
            return [self setPromptValueIfEqual:p value:promptValue];
        }else{
            //Ok, check all using the ColumnName
            for(int i = 0; i < [self.prompts count]; i++){
                Prompt *p = [self.prompts objectAtIndex:i];
                if ([p.columnName isEqualToString:promptName]) {
                    return [self setPromptValueIfEqual:p value:promptValue];
                }
            }
        }
    }
    return NO;
}

- (BOOL) setPromptValueIfEqual:(Prompt*)p value:(NSString*)promptValue{
    if ([p.operator isEqualToString:@"="]) {
        [p setSelectedValue:promptValue force:NO];
        return YES;
    }else{
        return NO;
    } 
}

- (NSString *) toXml:(NSString*)backgroundColor{
	NSMutableString *xml = [NSMutableString stringWithFormat:@"<Page dir=\"%@\" uuid=\"%@\" name=\"%@\"" , self.dir, self.uuid, self.name];
    if(backgroundColor){
        [xml appendFormat:@" bgColor=\"%@\">", backgroundColor];
    }else{
        [xml appendString:@">"];
    }
	[xml appendString:[self getPromptsXmlValues:YES encode:NO ignoreList:nil]];
	[xml appendString:[self.layout toXml]];
	[xml appendString:@"</Page>"];
	
	return xml;
}

- (void) updateGlobalPromptValues{
    int numPrompts = [self.prompts count];
    
    for(int i = 0; i < numPrompts; i++){
        Prompt *p = [self.prompts objectAtIndex:i];
        if (!p.isVariableToUpdate) {
            continue;
        }
        
        NSString *key = p.parameterName;
        NSMutableArray *selectedValues = p.selectedValues;
        
        if (key && selectedValues) {
            NSMutableArray *values = [[NSMutableArray alloc]init];
            for (int i = 0; i < [selectedValues count]; i++) {
                PromptValues *v = [[selectedValues objectAtIndex:i] copy];
                [values addObject:v];
                [v release];
            }
            //NSMutableArray *values = [[NSMutableArray alloc]initWithArray:selectedValues copyItems:YES];
            GlobalPromptValues *gpv = [GlobalPromptValues instance]; 
            [gpv setValue:values key:key];
            //[gpv setValue:selectedValues key:key];
            [values release];
        }
    }
}

- (void) setValuesByGlobalPromptValues{
    int numPrompts = [self.prompts count];
    
    for(int i = 0; i < numPrompts; i++){
        Prompt *p = [self.prompts objectAtIndex:i];
        
        NSString *parameterName = p.parameterName;
        GlobalPromptValues *pv = [GlobalPromptValues instance];
        NSMutableArray *value = [pv getValue:parameterName];
        if (value) {
            p.selectedValues = value;
            p.isUpdateByGlobalVars = YES;
        }
    }
}

- (BOOL) isGlobalPromptsInSync{
    int numPrompts = [self.prompts count];
    
    for(int i = 0; i < numPrompts; i++){
        Prompt *p = [self.prompts objectAtIndex:i];
        
        NSString *parameterName = p.parameterName;
        GlobalPromptValues *pv = [GlobalPromptValues instance];
        NSArray *value = [pv getValue:parameterName];
        NSArray *selectedValues = p.selectedValues;
   
        //Found in global prompt
        if (value && [value count] > 0) {
            if (p.isMultiSelect) {
                if ([value count] != [selectedValues count]) {
                    return NO;
                }    
            }
            
            for (int y = 0; y < [value count]; y++) {
                PromptValues *globalValue = [value objectAtIndex:y];
                PromptValues *selected = [selectedValues objectAtIndex:y];
                
                BOOL isEqual = [globalValue.value isEqualToString:selected.value];
                if (!p.isMultiSelect && y == 0) {
                    return isEqual;
                }
                
                if (!isEqual) {
                    return NO;
                }
            }  
        }
        
    }
    
    return YES;
}

- (NSString*)getPath{
    return [NSString stringWithFormat:@"%@/%@.page", self.dir, self.name];
}

- (void) dealloc{
    [prompts release];
    prompts = nil;
    
    [visiblePrompts release];
    visiblePrompts = nil;
    
    [promptsHash release];
    promptsHash = nil;
    
	[name release];
    name = nil;
    
	[dir release];
    dir = nil;
    
	[uuid release];
    uuid = nil;
    
	[modifiable release];
    modifiable = nil;
    
	[dashlets release];
    dashlets = nil;
	
    [parentChildrenPrompts release];
    
    [drillAttributes release];
    [layout release];
    [bgColor release];
    
	[super dealloc];
}
@end

