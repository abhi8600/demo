//
//  Layout.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/6/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dashlet.h"
#import "TBXML.h"

typedef enum{
    Horizontal,
    Vertical
}Direction;

@interface Layout : NSObject{
    NSMutableArray *layouts;
    Dashlet *dashlet;
    float percentWidth;
    float percentHeight;
    Direction direction;
    BOOL isPromptDashlet;
    NSString *promptRef;
}

@property (nonatomic, retain) NSMutableArray *layouts;
@property (nonatomic, retain) Dashlet *dashlet;

@property (nonatomic, assign) float percentWidth;
@property (nonatomic, assign) float percentHeight;
@property (nonatomic, assign) Direction direction;

@property (assign) BOOL isPromptDashlet;
@property (retain) NSString *promptRef;

- (void) parse:(TBXMLElement*)xml;
- (NSString *) toXml;

@end
