//
//  EmailSupportViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/28/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//
#import "MainViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Settings.h"

@interface EmailSupportViewController : MainViewController<MFMailComposeViewControllerDelegate>

@property(nonatomic, retain) MFMailComposeViewController *mailViewController;
@end
