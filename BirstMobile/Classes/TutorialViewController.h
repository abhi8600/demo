//
//  TutorialViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/20/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface TutorialViewController : MainViewController {
    UIWebView *webview; 
}

- (void) close;
@end
