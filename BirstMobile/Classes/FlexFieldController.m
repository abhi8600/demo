    //
//  FlexFieldController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "FlexFieldController.h"
#import "PromptValues.h"

@implementation FlexFieldController

@synthesize prompt, input, datePicker, timePicker, renderedInDashlet, pgCtrl, origValue;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	NSLog(@"FlexFieldController viewDidLoad()");
	
	self.input = [[[UITextField alloc]initWithFrame:CGRectMake(10, 10, 160, 32)] autorelease];
    self.input.backgroundColor = [UIColor whiteColor];
	self.input.borderStyle = UITextBorderStyleLine;
    [self.view addSubview:self.input];
	
    if (prompt.isDate || prompt.isDateTime) {
        self.datePicker = [[[UIDatePicker alloc] init] autorelease];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.frame = CGRectMake(10, 45, 320, 140);
        [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:self.datePicker];
    }
    
    if (prompt.isDateTime) {
        self.timePicker = [[[UIDatePicker alloc]init]autorelease];
        self.timePicker.datePickerMode = UIDatePickerModeTime;
        self.timePicker.frame = CGRectMake(10, 210, 320, 140);
        [self.timePicker addTarget:self action:@selector(timeChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:self.timePicker];
    }
    
    if (prompt.selectedValues && [prompt.selectedValues count] > 0) {
		PromptValues *pv = [prompt.selectedValues objectAtIndex:0];
        if (pv.value) {
            if([pv.value isEqualToString:@"NO_FILTER"]){
                self.input.placeholder = prompt.allText;
                [self useDefaultOption];
            }else{
                self.input.text = pv.value;
                if (prompt.isDate) {
                    NSDateFormatter *fmt = [[[NSDateFormatter alloc] init] autorelease];
                    [fmt setDateFormat:@"yyyy-MM-dd"];
                    NSDate *fmtDate = [fmt dateFromString:pv.value];
                    if (fmtDate) {
                        self.datePicker.date = fmtDate;
                    }
                }else if (prompt.isDateTime){
                    NSDateFormatter *fmt = [[[NSDateFormatter alloc]init]autorelease];
                    [fmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *fmtTime = [fmt dateFromString:pv.value];
                    if (fmtTime) {
                        self.datePicker.date = fmtTime;
                    }
                }
            }
        }
	}else{
        [self useDefaultOption];
    }
    
    //Check for keyboard shown / dismissed
    if(self.renderedInDashlet){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardDidHideNotification object:nil];
    }
}

-(void)keyboardShown:(NSNotification *)notif{
    self.origValue = self.input.text;
}

-(void)keyboardHidden:(NSNotification *)notif{
    if(![origValue isEqualToString:self.input.text]){
        [self inDashletApply];
    }
}

- (void)inDashletApply{
    NSMutableArray *selectedValues = [self getSelectedValues];
    if ([selectedValues count] > 0) {
        self.prompt.selectedValues = selectedValues;
    }
    [self.pgCtrl updateGlobalPrompts];
	[self.pgCtrl renderDashlets:NO useCachedReportXml:YES];
}

-(void) useDefaultOption{
    if([self.prompt.defaultOption length] > 0){
        self.input.text = self.prompt.defaultOption;
        if (prompt.isDate) {
            NSDateFormatter *fmt = [[[NSDateFormatter alloc] init] autorelease];
            [fmt setDateFormat:@"yyyy-MM-dd"];
            NSDate *fmtDate = [fmt dateFromString:self.prompt.defaultOption];
            if (fmtDate) {
                self.datePicker.date = fmtDate;
            }
        }else if (prompt.isDateTime){
            NSDateFormatter *fmt = [[[NSDateFormatter alloc]init]autorelease];
            [fmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *fmtTime = [fmt dateFromString:self.prompt.defaultOption];
            if (fmtTime) {
                self.datePicker.date = fmtTime;
            }
        }
    }
}

- (void)dateChanged:(UIDatePicker *)picker{
    NSDateFormatter *fmt = [[[NSDateFormatter alloc] init] autorelease];
    [fmt setDateFormat:@"yyyy-MM-dd"];
    self.input.text = [fmt stringFromDate:picker.date];
}

- (void)timeChanged:(UIDatePicker *)picker{
    NSDateFormatter *fmt = [[[NSDateFormatter alloc]init]autorelease];
    [fmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.input.text = [fmt stringFromDate:picker.date];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [input release];
    [datePicker release];
    
    if(self.renderedInDashlet){
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    [super dealloc];
}

- (NSMutableArray*)getSelectedValues{
    PromptValues *pv = [[[PromptValues alloc]init]autorelease];
    
	NSString *txt = self.input.text;
	if (txt && ([txt length] > 0)) {
		pv.label = pv.value = txt;
	}else{
        pv.label = self.prompt.allText;
        pv.value = @"NO_FILTER";
    }
    
    return [NSMutableArray arrayWithObject:pv];
}

@end
