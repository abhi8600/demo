//
//  Webservice.h
//  Birst
//
//  Created by Seeneng Foo on 3/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface Webservice : NSObject {
	NSURL* url;
	ASIFormDataRequest* request;
	NSString* data;
	NSString* operation;
	NSString *tns;
}

@property(retain) NSString *data;
@property(nonatomic, retain) NSString *operation;
@property(nonatomic, retain) NSString *tns;
@property(readonly, retain) ASIFormDataRequest*request;

- (Webservice*) initWithAdmin:(NSString*)op;
- (Webservice*) initWithDashboard:(NSString*)op;
- (Webservice*) initWithUrl:(NSString*)link prefix:(NSString*)pref operation:(NSString*)op;
- (Webservice*) initWithAdhoc:(NSString *)op;

- (NSString*) syncRequest:(NSString*)body;
- (void) asyncRequest:(NSString*)body delegate:(id)del;
- (void) asyncRequest:(NSString*)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
- (void) asyncRequestNS:(NSString *)body delegate:(id)del;
- (void) asyncRequestNS:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
- (void) asyncRequestNSDash:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
- (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port;

@end
