    //
//  SingleSelectListPickerController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "SingleSelectListPickerController.h"
#import "PromptValues.h"
#import "PromptControlHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation SingleSelectListPickerController

@synthesize picker, prompt, selectedValue, label, renderedInDashlet, pgCtrl, bttn, labelValue, popOver, backgroundColor;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	[super loadView];
}*/

- (NSMutableArray*)getSelectedValues{
    if (self.selectedValue) {
        return [NSMutableArray arrayWithObject:self.selectedValue];
    }
    
    return [[[NSMutableArray alloc]init] autorelease];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.labelValue.frame = CGRectMake(4, 21, self.view.frame.size.width - 50, 28);
    self.bttn.frame = CGRectMake(self.view.frame.size.width - 46, 21, 20, 28);
    [self.bttn.layer setBorderColor:[UIColor grayColor].CGColor];
    [self.bttn.layer setBorderWidth:1];
    
    self.contentSizeForViewInPopover = CGSizeMake(320, 160); //Set your own size
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {	
	return [prompt.labelValues count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	PromptValues *pv = [prompt.labelValues objectAtIndex:row];
	
	if (pv == self.selectedValue) {
		[thePickerView selectRow:row inComponent:component animated:YES];
	}
	return pv.label;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	return 320;
}

-(void)pickerView:(UIPickerView*)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	self.selectedValue = [prompt.labelValues objectAtIndex:row];
    if (prompt.isParentPrompt) {
        [PromptControlHelper parentPromptValueChanged:self prompt:prompt];
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
    if(self.renderedInDashlet){
        self.label = [[[UILabel alloc] initWithFrame:CGRectMake(4, 0, self.view.frame.size.width, 20)]autorelease];
        self.label.font = [UIFont systemFontOfSize:12];
        self.label.text = self.prompt.name;
        
        if(self.backgroundColor){
            self.view.backgroundColor = self.backgroundColor;
            self.label.backgroundColor = self.backgroundColor;
        }
        
        [self.view addSubview:self.label];
        
        self.labelValue = [[[UITextField alloc] initWithFrame:CGRectMake(4, 21, 50, 20)] autorelease];
        self.labelValue.enabled = NO;
        self.labelValue.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.labelValue setBorderStyle:UITextBorderStyleLine];
        self.labelValue.font = [UIFont systemFontOfSize:12];
        [self.view addSubview:self.labelValue];
        if([self.prompt.selectedValues count] > 0){
            PromptValues *pv = [self.prompt.selectedValues objectAtIndex:0];
            self.labelValue.text = [NSString stringWithFormat:@" %@", pv.label];
        }
        
        self.bttn = [[[UIButton alloc] initWithFrame:CGRectMake(2, 21, 58, 20)]autorelease];
        [self.bttn setTitle:@"V" forState:UIControlStateNormal];
        self.bttn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.bttn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.bttn addTarget:self action:@selector(showPopOver) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.bttn];
    }else{
        self.picker = [[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 320, 162)]autorelease];
        self.picker.delegate = self;
        self.picker.showsSelectionIndicator = YES;
        
        //selected value
        if (prompt.selectedValues && [prompt.selectedValues count] > 0) {
            self.selectedValue = [prompt.selectedValues objectAtIndex:0];
            [self.picker selectRow:self.selectedValue.index inComponent:0 animated:YES];
        }
        
        [self.view addSubview:self.picker];
    }
}

- (void) showPopOver{
    self.qCtrl = [[[QueryListTableController alloc] init]autorelease];
    self.qCtrl.prompt = self.prompt;
    self.qCtrl.pgCtrl = self.pgCtrl;
    self.qCtrl.renderedInDashlet = YES;
    self.qCtrl.sslCtrl = self;
    
    self.qCtrl.contentSizeForViewInPopover = CGSizeMake(350, [self.prompt.labelValues count] * 50);
    
    self.popOver = [[[UIPopoverController alloc] initWithContentViewController:self.qCtrl]autorelease];
    [self.popOver presentPopoverFromRect:CGRectMake(0, 40, 50, 50) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[picker release];
    [label release];
    [labelValue release];
    [bttn release];
    [qCtrl release];
    [popOver release];
    
    [super dealloc];
}


@end
