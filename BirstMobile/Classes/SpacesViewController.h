//
//  SpacesViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/5/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "SpaceDetailViewController.h"
#import "Space.h"

typedef enum{
    GetAllSpaces,
    GetSpaceDetail
}ASITag;

@class SpaceDetailViewController;

@interface SpacesViewController : MainViewController{
    SpaceDetailViewController *detail;
    NSInteger initialSpaceIdx;
}

@property (nonatomic, retain) SpaceDetailViewController *detail;
@property (nonatomic, assign) NSInteger initialSpaceIdx;

- (void) getAllSpaces;
- (void) parseAllSpaces:(NSString*)xml;
- (void) parseSpaceDetailSetSpace:(NSString *)xml;
- (void) getSpaceDetailAt:(NSInteger)index;
- (void) displayDashboard;
- (void)checkLoginGetSpaces;
- (void)loginFailed:(id)request;
@end
