//
//  UIWebViewWrapper.m
//  BirstMobile
//
//  Created by Seeneng Foo on 7/24/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "UIWebViewWrapper.h"
#import "Settings.h"
#import <QuartzCore/QuartzCore.h>
#import "NSStringCategory.h"
#import "DrillAttributes.h"


@implementation UIWebViewWrapper

@synthesize webview, anychartXml, delegate, disableDrilling;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f{
    return [self initWithXml:xml frame:f isDummy:NO];
}

-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f isDummy:(BOOL)dummy
{
    if( (self = [super initWithFrame:f]) ){
        isDummy = dummy;
        
        NSString *html = (isDummy) ? @"anychart-dummy" : @"anychart-html5";
        
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:html ofType:@"html"] isDirectory:NO];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
        
        self.webview = [[[UIWebView alloc] initWithFrame:f] autorelease];
        //self.webview.scalesPageToFit = YES;
        self.webview.frame = CGRectMake(0, 0, f.size.width, f.size.height);
        self.webview.userInteractionEnabled = YES;
        self.webview.opaque = NO;
        self.webview.backgroundColor = [UIColor clearColor];
        self.webview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.webview.delegate = self;
        self.webview.scrollView.bounces = NO;
        
        /*
        self.webview.layer.borderColor = [UIColor blackColor].CGColor;
        self.webview.layer.borderWidth = 1.0f;
        */
        
        [self.webview loadRequest:request];
        [self addSubview:self.webview];
       
        if(xml != nil){
            TBXMLElement *el = [Util traverseFindElement:xml target:@"anychart"];
            if(el != nil){
                NSMutableString *data = [[NSMutableString alloc] init];
                self.anychartXml = [self parse:el output:data];
                [data release];
            }
        }
    }
    
    return self;
}

- (void) dealloc
{
    NSLog(@"****UIWebViewWrapper dealloc %@", self);
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [webview stopLoading];
    [webview removeFromSuperview];
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    webview.delegate = nil;
    [webview release];
 
    [anychartXml release];
    
    [self removeFromSuperview];
    
    [super dealloc];
}

- (void) resize:(CGRect)f{
    self.frame = f;
    self.webview.frame = f;
    [self resizeJS];
}

// Serialize the anychart xml to a string we'd use later for 
// JS execution
- (NSString *) parse:(TBXMLElement *)xml output:(NSMutableString *)output
{
    if(xml != nil){
        NSString *tag = [TBXML elementName:xml];
        
        //no_data is not supported in AnyChart HTML5 v6.0.9
        if([tag isEqualToString:@"no_data"]){
            return nil;
        }
        
        BOOL isChartTag = [tag isEqualToString:@"chart"];
        
        TBXMLAttribute *attr = xml->firstAttribute;
        if(attr != nil){
            [output appendFormat:@"<%@ ", tag];
            do{
                NSString *attrName = [NSString stringWithCString:attr->name encoding:NSUTF8StringEncoding];
                
                NSMutableString *attrValue = [NSMutableString stringWithCString:attr->value encoding:NSUTF8StringEncoding];
                
                //Check certain unsupported plot type
                if(isChartTag && [attrName isEqualToString:@"plot_type"]){
                    if([@"UMap" isEqualToString:attrValue] ||
                       [@"Radar" isEqualToString:attrValue] ||
                       [@"Map" isEqualToString:attrValue]){
                        isNotSupportedType = YES;
                        return nil;
                    }
                }
                
                [self escapeJS:attrValue];
                [output appendFormat:@"%@=\"%@\" ", attrName, attrValue];
            }while( (attr = attr->next) );
            [output appendString:@">"];
        }else{
            [output appendFormat:@"<%@>", tag];
        }
        
        NSMutableString *text = [NSMutableString stringWithString:[TBXML textForElement:xml]];
        [self escapeJS:text];
        [output appendString:text];
        
        if(xml->firstChild){
            [self parse:xml->firstChild output:output];
        }
        [output appendFormat:@"</%@>", tag];
        
        if(xml->nextSibling){
            [self parse:xml->nextSibling output:output];
        }
        
    }
    
    return output;
}

//Called once UIWebView is ready for JS execution and after it'd loaded its url doc.
//Not called if there is no url doc
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    if(isDummy){
        if([delegate respondsToSelector:@selector(doneJS:)]){
            [delegate doneJS:self];
        }  
    }else{
        [self executeJS];
    }
}

-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *query =  [[request URL] absoluteString];
    NSLog(@"passed chart data: %@", query);
    NSLog(@"query string = %@", [[request URL]query]);
    NSRange r = [query rangeOfString:@"birst://q?"];
    if(r.length > 0){
        if(!self.disableDrilling){
            NSString *query = [[request URL] query];
            DrillAttributes *attr = [[DrillAttributes alloc] initWithQuery:query];
            [attr notify];
            [attr release];
        }
        return NO;
    }
    
    return YES;
}

-(void) resizeJS
{
    if(!isNotSupportedType){
        NSString *jsCode = @"chart.remove(); chart.width = window.innerWidth - 15; chart.height = window.innerHeight - 15; chart.write();";
        [self.webview stringByEvaluatingJavaScriptFromString:jsCode];
    }
}

-(void) executeJS
{
    //If we're an invalid type
    if(isNotSupportedType){
        NSString *jsMsgCode = @"document.getElementById('initMsg').innerHTML = 'The report contains features that are not yet supported.'";
        [self.webview stringByEvaluatingJavaScriptFromString:jsMsgCode];
        return;
    }
    
    NSString *jsCode = [NSString stringWithFormat:@"var chartData = '%@'; AnyChart.renderingType = anychart.RenderingType.SVG_ONLY; var chart = new AnyChart(); chart.width = window.innerWidth - 15; chart.height = window.innerHeight - 15; chart.addEventListener('pointMouseOver', onPointMouseOver); chart.setData(chartData); chart.write();", self.anychartXml];
    
    //NSLog(@"JS code = \n %@", jsCode);
    
    NSLog(@"executing JS code");

    [self.webview stringByEvaluatingJavaScriptFromString:jsCode];
    
    NSLog(@"done executing JS code");
    
    if([delegate respondsToSelector:@selector(doneJS:)]){
        [delegate doneJS:self];
    }
    
}

//Sanitize the string for inclusion into Javascript
- (void)escapeJS:(NSMutableString *)str{
    NSRange r = NSMakeRange(0, [str length]);
    [str replaceOccurrencesOfString:@"\\" withString:@"\\\\" options:NSLiteralSearch range:r];
    [str replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSLiteralSearch range:r];
    [str replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSLiteralSearch range:r];
    [str replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSLiteralSearch range:r];
    [str replaceOccurrencesOfString:@"\'" withString:@"\\'" options:NSLiteralSearch range:r];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
