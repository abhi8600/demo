//
//  ColumnTextCell.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/16/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TBXML.h"
#import "DrillAttributes.h"

@interface ColumnTextCell : UILabel {
	TBXMLElement *data;
    DrillAttributes *drillAttrs;
    float _width;
    float _height;
}

@property TBXMLElement *data;
@property (nonatomic, retain) DrillAttributes *drillAttrs;

- initWithXML:(TBXMLElement *)xml frame:(CGRect)frame;
- (void) parse:(TBXMLElement *)xml;
- (void) addBorders:(CGRect)rect;
- (float) setBorderColorWidth:(CGContextRef)context location:(NSString*)loc;
- (UIColor *) colorWithHexString:(NSString *)stringToConvert;
- (NSString *) attribute:(NSString *)attr;
- (void) handleTap:(UITapGestureRecognizer*)tapEvent;
- (CGRect) textRect;
@end
