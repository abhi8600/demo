//
//  AboutUsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/27/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface AboutUsViewController : MainViewController{
    UIWebView *webview;
    UILabel *version;
}

@property (retain) UIWebView *webview;

@property (retain) IBOutlet UILabel *version;

-(NSIndexPath *)lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController;

@end
