//
//  NSStringCategory.h
//  BirstMobile
//
//  Created by Seeneng Foo on 6/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (NSStringCategory)
- (NSString*)stringByUrlDecode;
- (NSString *)stringByDecodingXMLEntities;
@end
