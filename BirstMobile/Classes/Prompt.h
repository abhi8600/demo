//
//  Prompt.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/23/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"

typedef enum {
	List,
	Value,
	Query,
	Slider,
	CheckBox
} PromptType;

typedef enum{
	MatchDefaultOption,
	MatchSelectedValue,
	NoMatch
}MatchType;

@interface Prompt : NSObject {
	NSMutableDictionary *tagValues; 
	NSMutableArray *labelValues;
	NSMutableArray *selectedValues;
    NSMutableArray *parents;
    NSString *drillValue;
    BOOL isParentPrompt;
    BOOL isUpdateByGlobalVars;
}

@property (nonatomic, retain) NSMutableDictionary *tagValues;
@property (nonatomic, readonly) NSMutableArray *labelValues;
@property (nonatomic, retain) NSMutableArray *parents;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) PromptType type;
@property (nonatomic, readonly) BOOL isMultiSelect;
@property (nonatomic, readonly) NSString *parameterName;
@property (nonatomic, readonly) NSString *defaultOption;
@property (nonatomic, readonly) BOOL addNoSelectionEntry;
@property (nonatomic, readonly) NSString *allText;
@property (nonatomic, readonly) NSString *operator;
@property (nonatomic, readonly) NSString *summary;
@property (nonatomic, readonly) NSString *columnName;
@property (nonatomic, readonly) BOOL isDate;
@property (nonatomic, readonly) BOOL isDateTime;
@property (nonatomic, readonly) BOOL isVariableToUpdate;
@property (nonatomic, readonly) BOOL isDefaultOptionSessionVar;
@property (nonatomic, readonly) BOOL isInvisible;
@property (nonatomic, readonly) NSString* defaultOptionSessionVar;
@property (nonatomic, readonly) NSArray *parentPrompt;
@property (nonatomic, readonly) BOOL expandIntoButtons;
@property (nonatomic, retain) NSString *drillValue;
@property (nonatomic, retain) NSString* ref;
@property (nonatomic, assign) BOOL isParentPrompt;
@property (nonatomic, assign) BOOL isUpdateByGlobalVars;
@property (nonatomic, retain) NSMutableArray *selectedValues;

-(void) parse:(TBXMLElement *)p;
-(NSString*) toXmlString;
-(void) appendToString:(NSMutableString*)root addSelectedValues:(BOOL)setSelectVals;
-(void) parseValues:(TBXMLElement *)values;
-(void) appendSelectedValues:(NSMutableString*)root;
-(void) parseListValues:(TBXMLElement *)listValues;
-(void) parseSelectedValues:(TBXMLElement *)selectedValue;
-(void) clearSelectedValues;
-(void) listValueRefresh;
-(MatchType) matchDefaultOptionOrSelectedValue:(NSString*) value;
-(BOOL) setSelectedValue:(NSString *)value force:(BOOL)force;
-(BOOL) setSingleSelectedValue:(NSString*) value;
@end
