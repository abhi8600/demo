//
//  DashletController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/15/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "DashletController.h"
#import "Util.h"
#import "Settings.h"
#import "Log.h"

#define REPORT_XML_HEAD_ROOT @"<com.successmetricsinc.adhoc.AdhocReport"
#define REPORT_XML_TAIL_ROOT @"</com.successmetricsinc.adhoc.AdhocReport>"

#define RENDERED_REPORT_FILE    1
#define RENDERED_REPORT_XML     2

@implementation DashletController

@synthesize dashlet, page, ws, wsCached, reportRenderer, promptXml, reqPendingCtrl, pageRenderCtrl, renderFromFile, useCachedReportXml, dashletReportXml;
@synthesize pagingPageNo, pagingFirst, pagingLast, pagingNext, pagingPrevious, pagingSection;

@synthesize includeChartSelector, selectorChartType;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

- (id)init{
    self.renderFromFile = NO;
    return [super init];
}

- (Webservice *) ws{
	if (ws == nil) {
		ws = [[Webservice alloc] initWithAdhoc:@"getDashboardRenderedReport"];
        ws.request.tag = RENDERED_REPORT_FILE; 
        [ws retain];
	}
	
	return ws;
}

- (Webservice *) wsCached{
	if (ws == nil) {
		ws = [[Webservice alloc] initWithAdhoc:@"getDashboardRenderedReportFromXML"];
        ws.request.tag = RENDERED_REPORT_XML;
        [ws retain];
	}
	
	return ws;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
    pageToRender = 0;
    
    self.reportRenderer = [[[ReportRenderer alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)]autorelease];
    self.reportRenderer.contentSize = self.view.frame.size;
    self.reportRenderer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.reportRenderer.autoresizesSubviews = YES;
    self.reportRenderer.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.reportRenderer];
    self.reportRenderer.dashletCtrl = self;
    
	[self renderReport];
}

- (void)createPagingSection {
    if(self.pagingSection != nil){
        [self.pagingSection removeFromSuperview];
    }
    
    UIView *pV = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 90, 670, 222, 31)];
    self.pagingSection = pV;
    [pV release];
    
    self.pagingSection.backgroundColor = [UIColor grayColor];
    self.pagingSection.alpha = 0.5;
    //self.pagingSection.hidden = YES;
    [self.view addSubview:self.pagingSection];
    
    self.pagingFirst = [[[UIButton alloc]initWithFrame:CGRectMake(10, 0, 54, 28)] autorelease];
    [self.pagingFirst setTitle:@"<<" forState:UIControlStateNormal];
    [self.pagingFirst setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.pagingFirst addTarget:self action:@selector(clickFirstPage:) forControlEvents:UIControlEventTouchUpInside];
    [self.pagingSection addSubview:pagingFirst];
    
    self.pagingPrevious = [[[UIButton alloc]initWithFrame:CGRectMake(40, 0, 54, 28)] autorelease];
    [self.pagingPrevious setTitle:@"<" forState:UIControlStateNormal];
    [self.pagingPrevious setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.pagingPrevious addTarget:self action:@selector(clickPrevPage:) forControlEvents:UIControlEventTouchUpInside];
    [self.pagingSection addSubview:pagingPrevious];
    
    self.pagingNext = [[[UIButton alloc] initWithFrame:CGRectMake(150, 0, 29, 31)] autorelease];
    [self.pagingNext setTitle:@">" forState:UIControlStateNormal];
    [self.pagingNext addTarget:self action:@selector(clickNextPage:) forControlEvents:UIControlEventTouchUpInside];
    [self.pagingSection addSubview:pagingNext];
    
    self.pagingLast = [[[UIButton alloc]initWithFrame:CGRectMake(170, 0, 54, 28)] autorelease];
    [self.pagingLast setTitle:@">>" forState:UIControlStateNormal];
    [self.pagingLast setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.pagingLast addTarget:self action:@selector(clickLastPage:) forControlEvents:UIControlEventTouchUpInside];
    [self.pagingSection addSubview:self.pagingLast];
    
    self.pagingPageNo = [[[UILabel alloc]initWithFrame:CGRectMake(100, 5, 102, 21)] autorelease];
    self.pagingPageNo.textColor = [UIColor whiteColor];
    self.pagingPageNo.backgroundColor = [UIColor clearColor];
    [self.pagingSection addSubview:self.pagingPageNo];
}

- (void)renderReport {
    if (self.renderFromFile) {
        [Log debug:@"renderReport rendering from file"];
        NSString *renderData = [self renderedXmlFromFile];
        [reportRenderer parse:renderData];
    }else{
        self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
        self.reqPendingCtrl.message = @"Rendering...";
        self.reqPendingCtrl.parentBgColor = self.view.backgroundColor;
        self.reqPendingCtrl.view.frame = self.view.bounds;
        [self.view addSubview:self.reqPendingCtrl.view];
	
        if (self.useCachedReportXml) {
            [self renderUsingReportXml];
        }else{
            [self renderUsingReportPath];
        }
    }
}

- (void)renderByColumnSelector:(NSString *)selectorXml{
    [self cacheViewSelectorXml:selectorXml renderedFromPath:NO];
    [self renderUsingReportXml];
}

- (void)renderByViewSelector:(NSString *)graph{
    
    BOOL isTable = [graph isEqualToString:@"table"];
    
    NSString *selectors = [self selectorXmlFromFile];
    NSString *chartTypeRegex = @"<ChartType>.*</ChartType>";
    NSRange r = [selectors rangeOfString:chartTypeRegex options:NSRegularExpressionSearch];
    if(r.length > 0){
        NSString *newChartType = (isTable) ? @"<ChartType></ChartType>" : [NSString stringWithFormat:@"<ChartType>%@</ChartType>", graph];
        NSString *newXml = [selectors stringByReplacingCharactersInRange:r withString:newChartType];
        [self cacheViewSelectorXml:newXml renderedFromPath:NO];
    }

    NSString *reportXml = [self reportXmlFromFile];
    TBXML *tbxml = [TBXML tbxmlWithXMLString:reportXml];
    TBXMLElement *root = tbxml.rootXMLElement;
    
    //Show or not show table in report xml
    TBXMLElement *includeTable = [Util traverseFindElement:root target:@"IncludeTable"];
    if(includeTable){
        NSString *onOff = (isTable) ? @"true" : @"false";
        const char* t = [onOff UTF8String];
        includeTable->text = (char *)t;
    }
    
    //Change the chart section
    TBXMLElement *defaultChartIndex = [Util traverseFindElement:root target:@"DefaultChartIndex"];
    if(defaultChartIndex){
        NSString *chartIndex = [TBXML textForElement:defaultChartIndex];
        
        TBXMLElement *entities = [Util traverseFindElement:root target:@"Entities"];
        if(entities){
            NSString *chartTag = @"com.successmetricsinc.adhoc.AdhocChart";
            TBXMLElement *chart = [Util traverseFindElement:entities target:chartTag];
            do{
                TBXMLElement *reportIndex = [Util findSibling:chart->firstChild target:@"ReportIndex"];
                
                //We have a chart
                if(reportIndex != nil && [[TBXML textForElement:reportIndex] isEqualToString:chartIndex]){
    
                    TBXMLElement *showInReport = [Util findSibling:chart->firstChild target:@"showInReport"];
                    
                    //Turn off or on the chart display
                    if(showInReport){
                        NSString *onOff = (isTable) ? @"false" : @"true";
                        showInReport->text = (char *)[onOff UTF8String];
                    }
                    
                    //Not table, set the chart type in the chart xml definition
                    if(!isTable && ![graph isEqualToString:@"Composite"]){
                        TBXMLElement *chartCharts = [Util traverseFindElement:chart->firstChild target:@"Charts"];
                        if(chartCharts){
                            TBXMLElement *childChart = [Util traverseFindElement:chartCharts->firstChild target:@"Chart"];
                            if(childChart){
                                TBXMLElement *chartType = [Util traverseFindElement:childChart->firstChild target:@"ChartType"];
                                if(chartType){
                                    chartType->text = (char *)[graph UTF8String];
                                }
                            }
                        }
                    }
                }else{
                    //TODO - add a new chart
                    //Set the default categories and series
                }
            }while((chart = [Util findSibling:chart target:chartTag]));
        }
    }
    NSMutableString *string = [[NSMutableString alloc]init];
    NSString *xml = [Util toXmlString:root output:string];
    [self cacheReportXml:xml];
    [self renderUsingReportXml];
    [string release];
}

- (void)renderUsingReportXml{
    [self render:[self reportXmlFromFile]];
}

- (void)render:(NSString *)reportXml{
   
    NSString *viewSelectors = [self selectorXmlFromFile];
    if(!viewSelectors){
        viewSelectors = @"&lt;Selectors/&gt;";
    }else{
        viewSelectors = [Util xmlSimpleEscape:[NSMutableString stringWithString:viewSelectors]];
    }
    
    if(reportXml){
        reportXml = [Util xmlSimpleEscape:[NSMutableString stringWithString:reportXml]];
        
        NSString *body = [NSString stringWithFormat:@"<ns:xmlData>%@</ns:xmlData><ns:prompts>%@</ns:prompts><ns:page>%d</ns:page><ns:dashletPrompts>%@</ns:dashletPrompts><ns:applyAllPrompts>true</ns:applyAllPrompts><ns:pagePath>%@</ns:pagePath><ns:guid>%@</ns:guid>", reportXml, self.promptXml, pageToRender, viewSelectors, [self.page getPath], self.page.uuid];
        
        @try {
            [self.wsCached asyncRequestNS:body delegate:self]; 
        }
        @catch (NSException *exception) {
           NSLog(@"Exception thrown in renderUsingReportXml() %@", [exception reason]);
        }
        
    }else{
        [self renderUsingReportPath];
    }
}

- (void)renderUsingReportPath{
    NSString *body = [NSString stringWithFormat:@"<ns:name>%@</ns:name><ns:prompts>%@</ns:prompts><ns:page>%d</ns:page><ns:applyAllPrompts>true</ns:applyAllPrompts><ns:pagePath>%@</ns:pagePath><ns:guid>%@</ns:guid>", self.dashlet.path, self.promptXml, pageToRender, [self.page getPath], self.page.uuid];
    
    @try {
        [self.ws asyncRequestNS:body delegate:self]; 
    }
    @catch (NSException *exception) {
        NSLog(@"Exception thrown in renderUsingReportPath() %@", [exception reason] );
    }
    
}

- (NSString *) renderedXmlFromFile{
    NSString *dashPath = [NSString stringWithFormat:@"%@.rendered", [self.dashlet pathify]];

    NSString *fullPath;
    if (self.page.isOffline) {
        fullPath = [NSString stringWithFormat:@"%@/%@", self.page.offlineDir, dashPath];
    }else{
        fullPath = [Settings dashletReportCachePath:self.page.uuid dashletName:dashPath];
    }
    
    return [self readFileCachePageData:fullPath];
}

- (NSString *)reportXmlFromFile{
    return [self readFromFile:@".report.xml"];
}

- (NSString*)selectorXmlFromFile{
    return [self readFromFile:@".selectors.xml"];
}

- (NSString *)readFromFile:(NSString *)suffix{
    NSString *path = [NSString stringWithFormat:@"%@%@", [self.dashlet pathify], suffix];
    NSString *fullPath = [Settings dashletReportCachePath:self.page.uuid dashletName:path];
    return [self readFileCachePageData:fullPath];
}

+ (NSString *)getUniqueXmlTagContent:(NSString *)xml tag:(NSString*)tag{
    NSString *startTag = [NSString stringWithFormat:@"<%@", tag];
    NSString *endTag = [NSString stringWithFormat:@"</%@>", tag];
    
    NSRange head = [xml rangeOfString:startTag];
    if(head.length > 0){
        NSRange tail = [xml rangeOfString:endTag];
        if (tail.length > 0 && tail.location > head.location) {
            return [xml substringWithRange:NSMakeRange(head.location, (tail.location + [endTag length]) - head.location)];
        }
    }
    return nil;
}

- (NSString *)readFileCachePageData:(NSString *)fullPath{
    
    NSError *err;
    NSString *renderData = [NSString stringWithContentsOfFile:fullPath encoding:NSUnicodeStringEncoding error:&err];
    [Settings upgradeToFileProtectionComplete:fullPath];
    return renderData;
}

/* delegate for webservice asyncRequest */
- (void)requestFinished:(ASIHTTPRequest *)request {
	[self removeRequestPendingView];
	
	NSString *response = [Util getResponse:request];
    
	[Log debug:@"rendered report: %@", response];
    
    [reportRenderer parse:response];
    
    //Write out to file
    if(response){
        [self cacheRenderedReport:response renderedFromPath:ws.request.tag == RENDERED_REPORT_FILE];
    }
    
    [ws release];
	ws = nil;
}

-(void) resetRenderer{
    [self.reportRenderer disableAllDrilling:NO];
    
    CGRect r = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.reportRenderer.frame = r;
    [self.view addSubview:self.reportRenderer];
    [self.reportRenderer renderPagingSection];
    [self.reportRenderer maximizeSingleChart:r];
}

- (void)cacheRenderedReport:(NSString*)response renderedFromPath:(BOOL)renderedFromPath{
    NSString *dashPath = [self.dashlet pathify];
    NSString *fullResponsePath = [NSString stringWithFormat:@"%@.rendered", dashPath]; 
    
    //Cache everything
    if ([self fileCachePageData:fullResponsePath response:response]) {
        //Notify our parent render controller
        [self.pageRenderCtrl dashletCachedRenderedReport:self.dashlet];
    }

    //Also cache the report xml
    [self cacheReportXml:response];
    
    //Cache the selector
    [self cacheViewSelectorXml:response renderedFromPath:renderedFromPath];
}

- (BOOL)cacheReportXml:(NSString *)response{
    //Now find the subtring for the report xml
    NSString *reportXml = [DashletController reportXmlSubstring:response];
    if(reportXml){
        return [self fileCachePageData:[NSString stringWithFormat:@"%@.report.xml", [self.dashlet pathify]] response:reportXml];
    }
    return NO;
}

- (BOOL)cacheViewSelectorXml:(NSString *)response renderedFromPath:(BOOL)renderedFromPath{
    //Now find the subtring for the report xml
    NSString *selectorsXml = [DashletController getUniqueXmlTagContent:response tag:@"Selectors"];
    if(selectorsXml){
        NSString *chartType = [self parseCachedViewSelectorXml:selectorsXml];
        if(renderedFromPath && chartType != nil){
            [self fileCachePageData:[NSString stringWithFormat:@"%@.selector-chart-type.xml", [self.dashlet pathify]] response:chartType];
        }
        return [self fileCachePageData:[NSString stringWithFormat:@"%@.selectors.xml", [self.dashlet pathify]] response:selectorsXml];
    }
    return NO;
}

-(void)readCachedViewSelectorXml{
     NSString *xml = [self readFromFile:@".selectors.xml"];
    [self parseCachedViewSelectorXml:xml];
}

-(NSString *)readSelectorChartType{
    return [self readFromFile:@".selector-chart-type.xml"];
}

- (NSString *)parseCachedViewSelectorXml:(NSString *)selectorsXml{
    NSString *chartType = nil;
    if(selectorsXml){
        TBXML *tbxml = [TBXML tbxmlWithXMLString:selectorsXml];
        TBXMLElement *root = tbxml.rootXMLElement;
        if([[TBXML elementName:root] isEqualToString:@"Selectors"]){
            TBXMLElement *child = root->firstChild;
            do{
                NSString *elementName = [TBXML elementName:child];
                NSString *value = [TBXML textForElement:child];
                if([elementName isEqualToString:@"IncludeChartSelector"]){
                    self.includeChartSelector = [value isEqualToString:@"true"];
                }else if ([elementName isEqualToString:@"ChartType"]){
                    if([value length] == 0){
                        value = @"table";
                    }
                    self.selectorChartType = value;
                    chartType = value;
                }
            }while((child = child->nextSibling));
        } 
    }
    return chartType;
}

+ (NSString *)reportXmlSubstring:(NSString *)response{
    //Now find the subtring for the report xml
    NSRange head = [response rangeOfString:REPORT_XML_HEAD_ROOT];
    if(head.length > 0){
        NSRange tail = [response rangeOfString:REPORT_XML_TAIL_ROOT];
        if (tail.length > 0 && tail.location > head.location) {
            return [response substringWithRange:NSMakeRange(head.location, (tail.location + [REPORT_XML_TAIL_ROOT length]) - head.location)];
        }
    }
    return nil;
}

//Write string to file, unique per subdirectory based on page uuid
- (BOOL)fileCachePageData:(NSString *)path response:(NSString*)response{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *rootDir = [Settings currentPagesDir:filemgr];
    
    NSString *subDir = [NSString stringWithFormat:@"%@/%@", rootDir, self.page.uuid];
    
    NSError *error;
    if(![filemgr fileExistsAtPath:subDir]){
        if(![filemgr createDirectoryAtPath:subDir withIntermediateDirectories:YES attributes:nil error: &error]){
			NSLog(@"cannot create directory %@ because %@", subDir, [error localizedDescription]);
			return NO;
		}
    }
    
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", subDir, path];
    
    [Log debug:@"Writing response data to %@", fullPath];
    
    NSError *err;
    if (![response writeToFile:fullPath atomically:YES encoding:NSUnicodeStringEncoding error:&err]){
        NSLog(@"Unable to write to file because %@", [err localizedDescription]);
        return NO;
    }

    [Settings upgradeToFileProtectionComplete:fullPath];
    return YES;
}

- (ASIFormDataRequest *)getPDFRequest{
    
    NSURL *url = [Settings getNSURL:@"SMIWeb/ExportServlet.jsp"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    NSString *reportXml = [self reportXmlFromFile];
    [request setPostValue:reportXml forKey:@"birst.reportXML"];
    [request setPostValue:self.dashlet.title forKey:@"birst.reportName"];
    [request setPostValue:@"pdf" forKey:@"birst.exportType"];
    
    NSString *prompts = [self.promptXml length] == 0 ? @"<Prompts/>" : [Util xmlSimpleUnescape: [NSMutableString stringWithString:self.promptXml]];
    [request setPostValue:prompts forKey:@"birst.prompts"];
    
    NSString *selectorXml = [self selectorXmlFromFile];
    if(selectorXml){
        [request setPostValue:[self selectorXmlFromFile] forKey:@"birst.dashletParams"];
    }
    
    //TODO - figure which is not to apply
    [request setPostValue:@"true" forKey:@"birst.applyAllPrompts"];
    
    return request;
}
- (void)requestFailed:(ASIHTTPRequest *)request {
	[self removeRequestPendingView];
	
	NSError *error = [request error];
	[Log debug:@"requestFailed: %@", [error description]];
	[ws release];
	ws = nil;
	
	[Util requestFailedAlert:error];
}

- (void)removeRequestPendingView{
    if (reqPendingCtrl) {
        [reqPendingCtrl.view removeFromSuperview];
        [reqPendingCtrl release];
        reqPendingCtrl = nil;  
    }
}

- (void) clickFirstPage:(id)sender{
    if(self.reportRenderer.numPages > 1){
        pageToRender = 0;
        self.renderFromFile = NO;
        [self renderReport];
    }
}

- (void) clickNextPage:(id)sender{
    [Log debug:@"doNextPage()"];
    int nextPage = self.reportRenderer.pageNo + 1;
    if(nextPage < self.reportRenderer.numPages){
        pageToRender = nextPage;
        self.renderFromFile = NO;
        [self renderReport];
    }
}

- (void) clickPrevPage:(id)sender{
    [Log debug:@"doPrevPage()"];
    if(self.reportRenderer.pageNo > 0){
        pageToRender = self.reportRenderer.pageNo - 1;
        self.renderFromFile = NO;
        [self renderReport];
    }
}

- (void) clickLastPage:(id)sender{
    if(self.reportRenderer.numPages > 1){
        pageToRender = self.reportRenderer.numPages - 1;
        self.renderFromFile = NO;
        [self renderReport];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [reportRenderer release];
    self.reportRenderer = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    NSLog(@"DashletController dealloc, reportRenderer count = %d", [reportRenderer retainCount]);
    
    [ws release];
    [wsCached release];
    
    [promptXml release];
    
    [reqPendingCtrl release];
    
    reportRenderer.dashletCtrl = nil;
    
    BOOL hasCharts = reportRenderer.numCharts > 0;
    [reportRenderer release];
    
    //Hack for now. I can't find why renderers with charts must be released twice.
    if(hasCharts){
        [reportRenderer release];
    }
	[super dealloc];
}

@end
