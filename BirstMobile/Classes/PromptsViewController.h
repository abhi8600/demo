//
//  PromptsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/22/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageRenderViewController.h"

@class PageRenderViewController;

@interface PromptsViewController : UITableViewController {
	NSArray *prompts;
	PageRenderViewController *pgCtrl;
}

@property (nonatomic, assign) NSArray *prompts;
@property (nonatomic, assign) PageRenderViewController *pgCtrl;

- (void)apply;

@end
