//
//  Space.h
//  Birst
//
//  Created by Seeneng Foo on 3/4/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"

@interface Space : NSObject {
	NSString* name;
	NSString* spaceId;
	NSString* owner;
	NSString* isOwner;
    NSString* lastUpload;
    NSString* spaceSize;
    UIColor* backgroundColor;
}

@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* spaceId;
@property (nonatomic, copy) NSString* owner;
@property (nonatomic, copy) NSString* isOwner;
@property (nonatomic, copy) NSString* lastUpload;
@property (nonatomic, copy) NSString* spaceSize;

@property (retain) UIColor *backgroundColor;

-(void)parse:(TBXMLElement *)element;
-(NSString *)getValueForTag:(NSString *)tag element:(TBXMLElement *)element;

@end
