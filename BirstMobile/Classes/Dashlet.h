//
//  Dashlet.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/14/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TBXML.h"

@interface Dashlet : NSObject {
	NSString *path;
	NSString *title;
	BOOL enablePdf;
	BOOL enablePpt;
	BOOL enableExcel;
    BOOL enableColumSelectors;
    BOOL enableViewSelector;
    BOOL hasBorder;
	NSMutableSet *ignoredPrompts;
}

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, assign) BOOL enablePdf;
@property (nonatomic, assign) BOOL enablePpt;
@property (nonatomic, assign) BOOL enableExcel;
@property (nonatomic, assign) BOOL enableColumnSelectors;
@property (nonatomic, assign) BOOL enableViewSelector;
@property (nonatomic, retain) NSMutableSet *ignoredPrompts;
@property (assign) BOOL hasBorder;

- (void) parse:(TBXMLElement*)xml;
- (NSString *) pathify;
- (NSString *) toXml;
- (void) parseIgnoredPrompts:(TBXMLElement*)xml;
@end
