//
//  SettingsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@class DetailViewController;

@interface SettingsViewController : UITableViewController {
	DetailViewController *detailCtrl;
    NSMutableDictionary *savedSettings;
    int editSection;
}

@property (nonatomic, assign) DetailViewController *detailCtrl;
@property (nonatomic, retain) NSMutableDictionary *savedSettings;

- (void) loadSavedSettings;
- (void) saveSavedSettings;

@end
