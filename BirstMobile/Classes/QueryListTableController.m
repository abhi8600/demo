//
//  QueryListTableController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "QueryListTableController.h"
#import "PromptValues.h"
#import "PromptControlHelper.h"

@implementation QueryListTableController


@synthesize prompt, renderedInDashlet, pgCtrl, sslCtrl;

#pragma mark -
#pragma mark Initialization

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
	self.clearsSelectionOnViewWillAppear = NO;
    
    self.selects = [NSMutableSet setWithArray:self.prompt.selectedValues];
}


- (void)viewWillAppear:(BOOL)animated {
	//self.view.frame = CGRectMake(-40, 0, 320, 160);
	//self.navigationController.contentSizeForViewInPopover = CGSizeMake(320, 160); //Set your own size
	//self.contentSizeForViewInPopover = CGSizeMake(320, 160);
	[super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.prompt.selectedValues count] > 0){
        PromptValues *first = [self.prompt.selectedValues objectAtIndex:0];
        int firstIdx = first.index;
        int numLabelValues = [self.prompt.labelValues count];
         
        if (first && (numLabelValues > 0) && firstIdx < numLabelValues) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:firstIdx inSection:0];
            [self.tableView  scrollToRowAtIndexPath:indexPath  atScrollPosition:UITableViewScrollPositionTop animated:YES];
            if(!self.prompt.isMultiSelect){
                [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewRowAnimationTop];
            }
        }
	}
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [prompt.labelValues count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        if(self.renderedInDashlet){
            cell.textLabel.font = [UIFont systemFontOfSize:12];
        }
    }
    
    // Configure the cell...
	PromptValues *pv = [prompt.labelValues objectAtIndex:indexPath.row];
    cell.textLabel.text = pv.label;
	
    if(prompt.isMultiSelect){
        PromptValues *selectedPV = [self selectedHasValue:pv.value];
        if (selectedPV != nil) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }

    return cell;
}

- (PromptValues *) selectedHasValue:(NSString *)value{
    if([self.selects count] > 0){
        NSArray *pvs = [self.selects allObjects];
        for(int i = 0; i < [pvs count]; i++){
            PromptValues *p = [pvs objectAtIndex:i];
            if([p.value isEqualToString:value]){
                return p;
            }
        }
    }
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.prompt.name ? self.prompt.name : @"";
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (NSMutableSet *) selects{
	if (!selects) {
		selects = [[[NSMutableSet alloc]init]autorelease];
		[selects retain];
	}
	return selects;
}

- (void) setSelects:(NSMutableSet *)s{
	if (s == selects) {
		return;
	}
	
	if (selects) {
		[selects release];
	}
	selects = [s retain];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	PromptValues *pv = [self.prompt.labelValues objectAtIndex:indexPath.row];
    
    if (self.prompt.addNoSelectionEntry) {
        if([pv.value isEqualToString:@"NO_FILTER"]){
            [self.selects removeAllObjects];
        }else{
            /* Do we need this?
            PromptValues *firstValue = [self.prompt.labelValues objectAtIndex:0];
            if(firstValue != nil){
                PromptValues *p = [self selectedHasValue:firstValue.value];
                if(p != nil){
                    [self.selects removeObject:p];
                }
            }*/
        }
    }
    
    if(self.prompt.isMultiSelect){
        //Remove no filter
        PromptValues *noFilter = [self selectedHasValue:@"NO_FILTER"];
        if(noFilter != nil){
            [self.selects removeObject:noFilter];
        }
        
        PromptValues *p = [self selectedHasValue:pv.value];
        if (p != nil) {
            pv.index = 0;
            [self.selects removeObject:p];
        }else {
            pv.index = indexPath.row;
            [self.selects addObject:pv];
        }
    }else{
        pv.index = indexPath.row;
        [self.selects removeAllObjects];
        [self.selects addObject:pv];
    }
    
	[self.tableView reloadData];
    
    if (prompt.isParentPrompt) {
        [PromptControlHelper parentPromptValueChanged:self prompt:prompt];
    }else{
        if (self.renderedInDashlet){
            [self inDashletApply];
        }
    }
}

- (void)inDashletApply{
    NSMutableArray *selectedValues = [self getSelectedValues];
    self.prompt.selectedValues = selectedValues;
    
    [self.pgCtrl updateGlobalPrompts];
	[self.pgCtrl renderDashlets:NO useCachedReportXml:YES];
    
    [self.sslCtrl.popOver dismissPopoverAnimated:YES];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [selects release];
    [super dealloc];
}

- (NSMutableArray*)getSelectedValues{
	return [NSMutableArray arrayWithArray:[self.selects allObjects]];
}

@end

