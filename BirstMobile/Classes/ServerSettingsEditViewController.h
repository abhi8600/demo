//
//  ServerSettingsEditViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/14/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"

@interface ServerSettingsEditViewController : UITableViewController <UITextFieldDelegate> {
    SettingsViewController *settingsCtrl;
    UITextField *cellTextField;
    NSString *newServer;
    BOOL isDirty;
}

@property (nonatomic, assign) SettingsViewController *settingsCtrl;
@property (nonatomic, retain) UITextField *cellTextField;
@property (nonatomic, copy) NSString *addedNewServer;

- (void)addServer;
- (void)textFieldDidChange:(UITextField *)textField;
@end
