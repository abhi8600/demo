//
//  SegmentedCheckboxController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 9/6/12.
//
//

#import <UIKit/UIKit.h>
#import "Prompt.h"
#import "PageRenderViewController.h"

@interface SegmentedCheckboxController : UIViewController{
    Prompt *prompt;
    UISegmentedControl *control;
    UILabel *label;
    PageRenderViewController *pgCtrl;
    BOOL renderedInDashlet;
    UIColor *backgroundColor;
}

@property (retain) UIColor *backgroundColor;
@property (retain) Prompt *prompt;
@property (retain) UISegmentedControl *control;
@property (retain) UILabel *label;
@property (assign) PageRenderViewController *pgCtrl;
@property (assign) BOOL renderedInDashlet;

-(void)segmentedControlChanged;
-(void)inDashletApply;
- (NSMutableArray*)getSelectedValues;

@end
