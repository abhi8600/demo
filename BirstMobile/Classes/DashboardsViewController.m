//
//  DashboardsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "DashboardsViewController.h"
#import "Notification.h"
#import "DrillAttributes.h"
#import "GlobalPromptValues.h"
#import "Settings.h"

@implementation DashboardsViewController

@synthesize selectedIndex, pageSelectedIndex, detailCtrl, pagesCtrl, dashboards, spaceId, spaceName, setAsDefaultBttn, invisibleDashboards;

#pragma mark -
#pragma mark Initialization


- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        self.pageSelectedIndex = -1;
        self.selectedIndex = -1;
        setAsDefaultBttn = YES;
    }
    return self;
}



#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
	self.clearsSelectionOnViewWillAppear = NO;
 
    if(setAsDefaultBttn){
        [self.detailCtrl setRightBarButton:@"Set as default" action:@selector(setAsDefault) target:self];	
	}
    
	self.tableView.opaque = NO;
	self.tableView.backgroundView = nil;
	self.tableView.backgroundColor = [UIColor clearColor];
	

	NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    [ctr addObserver:self selector:@selector(displayDashboard:) name:DISPLAYDASHBOARD object:nil];
}

- (void) setAsDefault{
    NSString *user = [Settings username];
    NSString *server = [Settings server];
    [Settings setDefaultSpace:user server:server spaceId:spaceId name:spaceName];
    [Util alert:@"This dashboard will be the main dashboard" title:@"Info"];
}

- (void) displayDashboard:(NSNotification *)notification{
    DrillAttributes *drillAttrs = [[notification userInfo] objectForKey:@"drillAttributes"];
    NSString *dashboard = drillAttrs.targetURL;
    if (dashboard) {
        //If the pages view was unloaded during low memory, show it again
        if(!self.pagesCtrl){
            [self listPages:self.selectedIndex];
        }
        
        NSArray *dashPage = [dashboard componentsSeparatedByString:@"_"];
        if ([dashPage count] == 2) {
            NSString *dashName = [dashPage objectAtIndex:0];
            NSString *pageName = [dashPage objectAtIndex:1];
            
            NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
            
            //Search in regular dashboards
            for (int i = 0; i < [self.dashboards count]; i++) {
                Dashboard *dash = [self.dashboards objectAtIndex:i];
                if ([dash.name isEqualToString:dashName]) {
                    //We currently select this dashboard row, just select the page list
                    //and display the page
                    if (selected.row == i){
                        if (self.pagesCtrl) {
                            [self.pagesCtrl displayPage:pageName drillAttributes:drillAttrs];
                        }
                    }else{
                        //TODO - actually list the pages, then select the page, then display
                        //Invoke the method for clicking on the dashboard row
                        NSIndexPath *idx = [NSIndexPath indexPathForRow:i inSection:0];
                        [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
                        [self listPages:idx.row];
                        [self.pagesCtrl displayPage:pageName drillAttributes:drillAttrs];
                    }
                    return;
                }
            }
            
            
            //Search in the invisibles
            for(int i = 0; i < [self.invisibleDashboards count]; i++){ 
                Dashboard *dash = [self.invisibleDashboards objectAtIndex:i];
                if ([dash.name isEqualToString:dashName]) {
                    Page* p = [dash findPageByName:pageName];
                    if(p){
                        [self.pagesCtrl showPage:p];
                    }
                    return;
                }
            }
        }
    }//if (dashboard
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *v = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 44)] autorelease];
    
	UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.frame.size.width, 44)];
	l.opaque = NO;
	l.backgroundColor = [UIColor clearColor];
	l.textColor = [UIColor whiteColor];
	l.text = @"Dashboards";
	[v addSubview:l];
    [l release];
    
	return v;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 44.0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return [Util orientationLockLandscape:interfaceOrientation];
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.dashboards count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
	Dashboard *dash = [self.dashboards objectAtIndex:indexPath.row];
	cell.textLabel.text = [Util xmlUnescape:dash.name];
    // Configure the cell...
    
    return cell;
}

// Title for the section
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	return @"Dashboards";
}

-(void) parse:(NSString *)xml{
	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *dash = [Util traverseFindElement:root target:@"Dashboard"];
		if (dash != nil) {
            
			[self initClearDashboards];
            
            //Clear out the global prompt values
            [[GlobalPromptValues instance]reset];
            
			do {
				Dashboard *d = [[Dashboard alloc]init];
				[d parse:dash];
                if(d.invisible){
                    [self.invisibleDashboards addObject:d];
                }else{
                    [self.dashboards addObject:d];
                }
                [d release];
			} while ((dash = dash->nextSibling));
            
			[self.tableView reloadData];
			
			//Select the first row
			if ([self.dashboards count] > 0) {
				NSIndexPath *ip = [NSIndexPath indexPathForRow: 0 inSection:0];
				[self.tableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionTop];
				[self listPages:ip.row];
			}
		}
	}
    [tbxml release];
}

- (void)initClearDashboards{
    if (!self.dashboards){
        NSMutableArray *m = [[NSMutableArray alloc]init];
        self.dashboards = m;
        [m release];
    }else{
        [self.dashboards removeAllObjects];
    }
    
    if(!self.invisibleDashboards){
        NSMutableArray *i = [[NSMutableArray alloc]init];
        self.invisibleDashboards = i;
        [i release];
    }else{
        [self.invisibleDashboards removeAllObjects];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void) listPages:(NSUInteger)index{
    self.selectedIndex = index;
    
	Dashboard *dash = [self.dashboards objectAtIndex:index];
	if (dash != nil) {
        PagesViewController *p = [[PagesViewController alloc] initWithStyle:UITableViewStyleGrouped];
		self.pagesCtrl = p;
        [p release];
        
        NSLog(@"PagesViewController retainCount = %d", [self.pagesCtrl retainCount]);
        self.pagesCtrl.selectedIndex = self.pageSelectedIndex;
		self.pagesCtrl.pages = dash.pages;
        self.pagesCtrl.invisiblePages = dash.invisiblePages;
		self.pagesCtrl.dashCtrl = self;
        
		[detailCtrl setNewView:self.pagesCtrl];
        
	}
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.pageSelectedIndex = -1;
	[self listPages:indexPath.row];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    NSLog(@"DashboardsViewController didReceivedMemoryWarning");
    
    //Remove the pages view
    if(pagesCtrl){
        NSLog(@"releasing pagesCtrl %@ retainCount %d", pagesCtrl, [pagesCtrl retainCount]);
        self.pageSelectedIndex = pagesCtrl.selectedIndex;
        [pagesCtrl.view removeFromSuperview];
        [pagesCtrl release];
        pagesCtrl = nil;
    }
    
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    NSLog(@"DashboardsViewController viewDidUnload");
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"DashboardsViewController viewWillAppear");
    if(!self.pagesCtrl && selectedIndex != -1){
        NSIndexPath *idx = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
        [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        [self listPages:self.selectedIndex];
    }
}

- (void)dealloc {
    [dashboards release];
    dashboards = nil;

    [pagesCtrl release];
    pagesCtrl = nil;
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    [super dealloc];
}


@end

