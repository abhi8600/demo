    //
//  LoginViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "Settings.h"
#import "Webservice.h"
#import "Util.h"
#import "Login.h"

@implementation LoginViewController

@synthesize usernameField, passwordField, request, onLoginSuccess, delegate, loginMode;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        self.loginMode = spaceListing;
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"Login";    
}

- (IBAction) login{
	NSLog(@"Login()");
	
	NSString* username = usernameField.text;
	NSString* password = passwordField.text;

    [Settings setUsername:username password:password];
    
	[Login performLogin:username password:password startAnimate:@selector(startAnimate) endAnimate:@selector(endAnimate) delegate:self loginSuccess:@selector(loginSuccess) loginFail:nil];
}

- (IBAction) remememberPasswordChanged:(id)sender{
    UISwitch *s = (UISwitch*)sender;
    [Settings setRememberPassword:s.on];
}

- (void) startAnimate{
     [activity setHidden:YES];
     [activity startAnimating];
     loginMessage.text = @"Sending request. Please wait.";
     [loginMessage setHidden:NO];
}

- (void) endAnimate{
    [activity stopAnimating];
	[loginMessage setHidden:YES];
}

-(void) loginSuccess{
    [self dismissModalViewControllerAnimated:YES];
    if(delegate && self.onLoginSuccess){
        [delegate performSelector:onLoginSuccess];
    }
}

- (IBAction) cancel{
	[self dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString *username = (self.loginMode == mainDashboard) ? [Settings mainDashUsername] : [Settings username];
    NSString *password = [Settings password];
    BOOL rememberPass = [Settings rememberPassword];
    [rememberPassword setOn:rememberPass animated:YES];
    serverName.text = [Settings selectedServer];
    
    if([username length] > 0){
        usernameField.text = username;
        if(rememberPass && [password length] > 0){
            passwordField.text = password;
            [loginBttn becomeFirstResponder];
        }else{
            [passwordField becomeFirstResponder];
        }
    }else{
        [usernameField becomeFirstResponder];
    }
}

- (void)dealloc {
    [super dealloc];
}


@end
