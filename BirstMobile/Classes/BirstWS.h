//
//  BirstWS.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/11/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"

@interface BirstWS : NSObject {
    
}

+ (ASIHTTPRequest*) requestWithAdmin:(NSString*)op;
+ (ASIHTTPRequest*) requestWithDashboard:(NSString*)op;
+ (ASIHTTPRequest*) requestWithAdhoc:(NSString *)op;
+ (ASIHTTPRequest*) requestWithClientInit:(NSString *)op;
+ (ASIHTTPRequest*) requestWithOperationHandler:(NSString*)op;

+ (ASIHTTPRequest*) requestWithUrl:(NSString*)link prefix:(NSString*)pref operation:(NSString*)op;
+ (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port;
+ (void) asyncRequest:(ASIHTTPRequest*)request body:(NSString*)body delegate:(id)del;
+ (void) asyncRequest:(ASIHTTPRequest *)request body:(NSString*)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
+ (void) asyncRequestNS:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del;
+ (void) asyncRequestNSDash:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
+ (void) asyncRequestNS:(ASIHTTPRequest*)request body:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed;
+ (void) requestNSDashFormat:(ASIHTTPRequest*)request body:(NSString *)body;
+ (void) requestNSFormat:(ASIHTTPRequest*)request body:(NSString *)body operationName:(NSString *)operationName;
+ (void) requestFormat:(ASIHTTPRequest *)request body:(NSString *)body;
+ (ASINetworkQueue*) createNetworkQueueMaxOne:(id)delegate finishSel:(SEL)finishSel failSel:(SEL)failSel;
+ (ASINetworkQueue*) createNetworkQueue:(int)count delegate:(id)delegate finishSel:(SEL)finishSel failSel:(SEL)failSel;
@end
