//
//  MaximizedDashletViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/13/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportRenderer.h"
#import "ExportDashletViewController.h"
#import "ASIHTTPRequest.h"
#import "ViewSelectorsViewController.h"
#import "ColumnSelectorsViewController.h"

@class ExportDashletViewController;

@interface MaximizedDashletViewController : UIViewController<UIDocumentInteractionControllerDelegate>{
    ReportRenderer *renderer;
    UIPopoverController *popOver;
    UIBarButtonItem *saveBttn;
    UIBarButtonItem *viewBttn;
    UIBarButtonItem *colBttn;
    BOOL didRendered;
    UIDocumentInteractionController *docCtrl;
}

@property (nonatomic, assign) ReportRenderer *renderer;
@property (nonatomic, retain) UIPopoverController *popOver;
@property (nonatomic, retain) UIDocumentInteractionController *docCtrl;

-(void) getPdf;
-(void) chartSelectorChange:(NSString *)chartType;
-(void) columnSelectorChange:(NSString *)xml;
-(void) maximizeChart;
-(void) onMinimize;
@end
