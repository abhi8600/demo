    //
//  OfflinePagesViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 4/4/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "OfflinePagesViewController.h"
#import "Settings.h"
#import "Page.h"
#import "TBXML.h"
#import "PageRenderViewController.h"
#import "Util.h"
#import "UIViewController+JTRevealSidebarV2.h"

@implementation OfflinePagesViewController

@synthesize description, tabBar, options, displayPage;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = @"Offline Pages";
	
/*
	UIBarButtonItem *close = [[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(dismissPopup)]autorelease];
	self.navigationItem.leftBarButtonItem = close;
  */  
    UIBarButtonItem *option = [[[UIBarButtonItem alloc] initWithTitle:@"Option" style:UIBarButtonItemStylePlain target:self action:@selector(optionPopup)] autorelease];
    self.navigationItem.rightBarButtonItem = option;
    
	self.view.backgroundColor = [UIColor blackColor];
	
    //NSLog(@"viewDidLoad() offlinepagesview %d", self);
	
}


- (void) viewWillAppear:(BOOL)animated{
   [self renderCoverflow]; 
}

- (void) renderCoverflow{
    //We've rendered already
    if(coverflow){
        return;
    }
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
	NSString *offlineDir = [Settings offlineDir:filemgr];
	
	if ([filemgr fileExistsAtPath:offlineDir]){
		NSArray *dirContents = [filemgr contentsOfDirectoryAtPath:offlineDir error:NULL];
		NSArray *offlines = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.offline'"]];
		
		if (!covers) {
			covers = [[NSMutableArray alloc]init];
			info = [[NSMutableArray	alloc]init];
		}
		
		for (int i = 0; i < [offlines count]; i++) {
			NSString *subDir = [NSString stringWithFormat:@"%@/%@", offlineDir, [offlines objectAtIndex:i]];
			
			UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/cover.png", subDir]];
			[covers addObject:img];
			
			NSMutableDictionary *infoPlist = 
				[[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%@/Info.plist", subDir]];
			[infoPlist setObject:subDir forKey:@"rootDir"];
			[info addObject:infoPlist];
			
			[infoPlist release];
		}
	}else {
		if (!covers) {
			covers = [[NSMutableArray alloc]init];
		}
	}
	
	
	CGRect r = self.view.bounds;
	r.origin.y = 45;
	r.size.height = r.size.height - 90;
	NSLog(@"cover rect x = %f y = %f width = %f height = %f", r.origin.x, r.origin.y, r.size.width, r.size.height);
	coverflow = [[TKCoverflowView alloc] initWithFrame:r];
	coverflow.backgroundColor = [UIColor blackColor];
	coverflow.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	coverflow.coverflowDelegate = self;
	coverflow.dataSource = self;
	[self.view addSubview:coverflow];
	coverflow.coverSpacing = 200;
	coverflow.coverSize = CGSizeMake(600, 500);
	[coverflow setNumberOfCovers:[covers count]];
	
    CGRect loc = CGRectMake(0, r.size.height + 10, r.size.width, 50);
    self.description = [[[UILabel alloc]initWithFrame:loc] autorelease];
	self.description.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.description.backgroundColor = [UIColor blackColor];
	self.description.textAlignment = UITextAlignmentCenter;
    self.description.textColor = [UIColor whiteColor];
	self.description.text = @"No saved offline pages";
	[self.view addSubview:self.description];     
}

- (void) deleteAll{
    if (self.options != nil && [self.options isPopoverVisible]) {
		[self.options dismissPopoverAnimated:YES];
		[options release];
		options = nil;
	}
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
	NSString *offlineDir = [Settings offlineDir:filemgr];
	[filemgr removeItemAtPath:offlineDir error:nil];
    
    coverflow.hidden = YES;
    self.description.text = @"No saved offline pages";
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void) rightButtonSelected:(id)sender{
    UISegmentedControl *target = (UISegmentedControl *)sender;
    int idx = [target selectedSegmentIndex];
    switch (idx) {
        case 0:
            NSLog(@"Options");
            break;
        case 1:
            NSLog(@"Close");
            [self dismissModalViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}

- (void) dismissPopup{
	[self dismissModalViewControllerAnimated:YES];
}

- (void) optionPopup{
    if (self.options != nil && [self.options isPopoverVisible]) {
		[self.options dismissPopoverAnimated:YES];
		[options release];
		options = nil;
	}else {
		OfflineOptionsViewController *opt = [[[OfflineOptionsViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
        opt.offlinePagesCtrl = self;
        self.options = [[[UIPopoverController alloc] initWithContentViewController:opt] autorelease];
		self.options.popoverContentSize = CGSizeMake(300, 40);
		[self.options presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
}

- (NSString*) formatDescription:(int)index{
	NSDictionary *p = [info objectAtIndex:index];
	return [NSString stringWithFormat:@"%@\nSaved by %@ at %@", [p objectForKey:@"pageName"], [p objectForKey:@"username"], [p objectForKey:@"saveDate"]];
}

- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	if ([covers count] > 0) {
		[coverflow bringCoverAtIndexToFront:0 animated:YES];
		self.description.text = [self formatDescription:0];
	}
}

- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasBroughtToFront:(int)index{
    if(index >= 0){
        self.description.text = [self formatDescription:index];
    }else{
        self.description.text = @"No saved offline pages";
    }
}

- (TKCoverflowCoverView*) coverflowView:(TKCoverflowView*)coverflowView coverAtIndex:(int)index{
	
	TKCoverflowCoverView *cover = [coverflowView dequeueReusableCoverView];
	
	if(cover == nil){
		cover = [[[TKCoverflowCoverView alloc] initWithFrame:CGRectMake(0, 0, 600, 500)] autorelease]; // 224
		cover.baseline = 100;
	}
	UIImage *image = [covers objectAtIndex:index%[covers count]];
	cover.image = image;
	
	return cover;	
}

- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasDoubleTapped:(int)index{
	NSDictionary *dict = [info objectAtIndex:index];
	NSString *rootDir = [dict objectForKey:@"rootDir"];

	NSError *error;
	NSString *xml = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/page.xml", rootDir] encoding:NSUnicodeStringEncoding error:&error];
    
	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml]; 
	self.displayPage = [[[Page alloc]init]autorelease];
	[self.displayPage parse:tbxml.rootXMLElement];
    [tbxml release];
    
	self.displayPage.isOffline = YES;
	self.displayPage.offlineDir = rootDir;
	self.displayPage.renderedDashletsCached = YES;
	
	PageRenderViewController *renderCtrl = [[[PageRenderViewController alloc]init] autorelease];
	renderCtrl.page = self.displayPage;
    renderCtrl.viewMode = Offline;
    [self.navigationController pushViewController:renderCtrl animated:YES];
	//renderCtrl.modalPresentationStyle = UIModalPresentationFullScreen;
	//[self presentModalViewController:renderCtrl animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    //return YES;
    return [Util orientationLockLandscape:interfaceOrientation];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [coverflow release];
    [covers release];
    covers = nil;
    
    [info release];
    info = nil;
    
    
    [tabBar release];
    [description release];
    [options release];
    [displayPage release];
    
    [super dealloc];
}


@end
