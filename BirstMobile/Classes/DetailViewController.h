//
//  DetailViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
#import "RequestPendingIndicatorController.h"

@interface DetailViewController : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate> {
    UIPopoverController *popoverController;
    UIToolbar *toolbar;
    IBOutlet UIView *mainView;
    id detailItem;
    UILabel *detailDescriptionLabel;
	UIPopoverController *settingsCtrl;
	IBOutlet UIBarButtonItem *settingsBttn;
	
	UIViewController *scrollViewSubView;
	UIViewController *rootViewCtrl;
	RequestPendingIndicatorController *reqPendingCtrl;
}

@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) id detailItem;
@property (nonatomic, retain) IBOutlet UILabel *detailDescriptionLabel;
@property (nonatomic, readonly) UIView *mainView;
@property (nonatomic, retain) UIPopoverController *settingsCtrl;
@property (nonatomic, retain) id scrollViewSubView;
@property (nonatomic, assign) UIViewController *rootViewCtrl;
@property (nonatomic, retain) RequestPendingIndicatorController *reqPendingCtrl;


- (void) toggleSettings;
- (void) setNewView:(UIViewController *)ctrl;
- (void)clearSubviews;
- (void) loggedOutClearScreen;
- (void)requestPendingShow:(NSNotification *) notification;
- (void)requestPendingEnd:(NSNotification *) notification;
- (void) displayAboutUs;
- (void) removeSubViews;
- (void) displayTutorialPdf;
- (void)setRightBarButtonAsLogin;
- (void)setRightBarButton:(NSString *)title action:(SEL)action target:(id)target;
- (void) startAnimateRequest:(NSString*)message;
- (void) stopAnimateRequest;
@end
