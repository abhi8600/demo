//
//  SpaceDetailViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/9/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Space.h"
#import "SpacesViewController.h"
#import "GradientButton.h"

@class SpacesViewController;

@interface SpaceDetailViewController : UIViewController{
    GradientButton *openBttn;
    UILabel *spaceTitle;
    UITextView *spaceDetails;
    UIImageView *spaceDashboardImg;
    SpacesViewController *parentViewCtrl;
    Space *space;
    ASINetworkQueue* networkQueue;
}

@property (nonatomic, retain) IBOutlet GradientButton *openBttn;
@property (nonatomic, retain) IBOutlet UILabel *spaceTitle;
@property (nonatomic, retain) IBOutlet UITextView *spaceDetails;
@property (nonatomic, retain) IBOutlet UIImageView *spaceDashboardImg;
@property (nonatomic, retain) Space *space;
@property (nonatomic, assign) SpacesViewController *parentViewCtrl;
@property (nonatomic, retain) ASINetworkQueue *networkQueue;

- (IBAction) openDashboard:(id)sender;

@end
