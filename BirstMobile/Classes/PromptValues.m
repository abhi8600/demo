//
//  PromptValues.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/28/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "PromptValues.h"


@implementation PromptValues

@synthesize label, value, index;

-(id) copyWithZone:(NSZone*)zone{
    PromptValues *p = [[PromptValues allocWithZone:zone]init];
    [p copyLabel:label];
    [p copyValue:value];
    p.index = index;
    return p;
}

-(void) copyLabel:(NSString *)l{
    if(label){
        [label release];
    }
    label = [l copy];
}

- (void) copyValue:(NSString *)v{
    if(value){
        [value release];
    }
    value = [v copy];
}

-(void) dealloc{
	[label release];
	[value release];
	[super dealloc];
}
@end
