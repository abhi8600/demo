//
//  SideBarMain.m
//  BirstMobile
//
//  Created by Seeneng Foo on 6/25/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "JTRevealSidebarV2Delegate.h"
#import "UINavigationItem+JTRevealSidebarV2.h"
#import "UIViewController+JTRevealSidebarV2.h"
#import "JTRevealSidebarV2Delegate.h"
#import "SidebarViewController.h"
#import "Settings.h"
#import "LoginViewController.h"

#import "AboutUsViewController.h"
#import "OfflinePagesViewController.h"
#import "EmailSupportViewController.h"
#import "DashboardViewController.h"
#import "SpacesViewController.h"
#import "LoginSettingsViewController.h"
#import "Email.h"
#import "BirstWS.h"
#import "Notification.h"

@implementation MainViewController

BOOL createdDummyChart = NO;

@synthesize leftSidebarViewController, checkLogin, networkQueue, loginCtrl, requestPendingCtrl, dummy;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) showPendingRequest:(NSString *)message{
    [self removePendingRequest];
    
    RequestPendingIndicatorController *rp = [[RequestPendingIndicatorController alloc]init];
    self.requestPendingCtrl = rp;
    [rp release];
    
    self.requestPendingCtrl.message = message;
    
    self.requestPendingCtrl.view.frame = self.view.frame;
    [self.view addSubview:self.requestPendingCtrl.view];
}

-(void) removePendingRequest{
    if(self.requestPendingCtrl != nil){
        [self.requestPendingCtrl.view removeFromSuperview];
        [requestPendingCtrl release];
        requestPendingCtrl = nil;
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.translucent = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!createdDummyChart){
        [self createInitialDummyChart];
    }
    
    self.title = @"Birst Mobile";
    self.view.backgroundColor = [UIColor blackColor];
    
    //Left bar button
    self.navigationItem.leftBarButtonItem = [[ [UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ButtonMenu.png"] style:UIBarButtonItemStyleBordered target:self action: @selector(revealLeftSideBar:)] autorelease];
    
    self.navigationItem.revealSidebarDelegate = self;

    //Network queue which handles all our single http requests
    //Set to 1 to force only one requests at a time
    self.networkQueue = [BirstWS createNetworkQueueMaxOne:self finishSel:@selector(onRequestFinished:) failSel:@selector(onRequestFailed:)];
   
    //Add listener to session timeouts
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionTimedOut) name:SESSION_TIMEOUT object:nil];
}

- (void)sessionTimedOut{
    [self displayLogin:self.leftSidebarViewController];
    [self.leftSidebarViewController.tableView reloadData];
}

- (void)createInitialDummyChart {

    if(self.dummy == nil){
        UIWebViewWrapper *w = [[UIWebViewWrapper alloc] initWithXml:nil frame:CGRectMake(0, 0, 0, 0)];
        self.dummy = w;
        [w release];
        
        self.dummy.hidden = YES;
        [self.view addSubview:dummy];
        NSLog(@"** Created dummy chart **");
    }
    
    createdDummyChart = YES;
}

- (void)onRequestFailed:(ASIHTTPRequest *)request{
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    self.leftSidebarViewController = nil;
    
    [networkQueue reset];
    [networkQueue release];
    networkQueue = nil;
}

- (void)dealloc{
    NSLog(@"MainViewController dealloc");
    
    [networkQueue reset];
    [networkQueue release];
    
    [leftSidebarViewController release];
    [dummy release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Login popup
-(LoginViewController*)loginCtrl{
    if(!loginCtrl){
        loginCtrl = [[LoginViewController alloc]init];
        loginCtrl.modalPresentationStyle = UIModalPresentationFormSheet;
        loginCtrl.delegate = self;
        loginCtrl.onLoginSuccess = @selector(onLoginSuccess:);
    }
    return loginCtrl;
}

-(void)dismissLogin{
    if(self.loginCtrl){
        [self.loginCtrl dismissModalViewControllerAnimated:NO];
    }
}

#pragma mark - Left side bar operations

- (void)revealLeftSideBar:(id)sender{
    [self.navigationController toggleRevealState:JTRevealedStateLeft];
}

- (UIView *)viewForLeftSidebar {
    // Use applicationViewFrame to get the correctly calculated view's frame
    // for use as a reference to our sidebar's view 
    CGRect viewFrame = self.navigationController.applicationViewFrame;
    UITableViewController *controller = self.leftSidebarViewController;
    if ( ! controller) {
        SidebarViewController *sb = [[SidebarViewController alloc] initWithStyle:UITableViewStyleGrouped];
        self.leftSidebarViewController = sb;
        [sb release];
        self.leftSidebarViewController.sidebarDelegate = self;
        controller = self.leftSidebarViewController;
    }
    controller.view.frame = CGRectMake(0, viewFrame.origin.y, 270, viewFrame.size.height);
    controller.view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    return controller.view;
}

- (void)sidebarViewController:(SidebarViewController *)sidebarViewController didSelectObject:(NSObject *)object atIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case IDX_HDR_MAIN:
            switch (indexPath.row) {
                case IDX_ABOUT:
                    [self displayAboutUs:sidebarViewController];
                    break;
                case IDX_LOGIN:
                    [self displayLogin:sidebarViewController];
                    break;
                case IDX_OFFLINE:
                    [self displayOfflinePages:sidebarViewController];
                    break;
                case IDX_DEMO:
                    [Settings setLoggedIn:NO];
                    [sidebarViewController.tableView reloadData];
                    [self displayDemoDashboard:sidebarViewController];
                    break;
                case IDX_SUPPORT:
                    //[Email emailSupport:@"" delegate:self];
                    [self displayEmailSupport:sidebarViewController];
                    break;
                default:
                    break;
            }
            break;
        case IDX_HDR_SPACE:
            [self displaySpaces:sidebarViewController row:indexPath.row];
            break;
        default:
            break;
    }

}

#pragma mark - Main view controllers

- (void) displayAboutUs:(SidebarViewController*)sidebarViewController{
    AboutUsViewController *ab = [[AboutUsViewController alloc] init];
    [self displaySubView:sidebarViewController mainVC:ab revealSideBar:NO];
    [ab release];
}

- (void) displayLogin:(SidebarViewController*)sidebarViewController{
    LoginSettingsViewController *lv = [[LoginSettingsViewController alloc] init];
    [self displaySubView:sidebarViewController mainVC:lv revealSideBar:NO];
    [lv release];
}

- (void) displayDemoDashboard:(SidebarViewController*)sidebarViewController{
    DashboardViewController *db = [[DashboardViewController alloc]init];
    db.isDemoDashboard = YES;
    [self displaySubView:sidebarViewController mainVC:db revealSideBar:NO];
    [db release];
}

- (void) displayOfflinePages:(SidebarViewController*)sidebarViewController{
    OfflinePagesViewController *op = [[OfflinePagesViewController alloc]init];
    [self displaySubView:sidebarViewController mainVC:op revealSideBar:NO];
    [op release];
}

- (void)displayEmailSupport:(SidebarViewController*)sidebarViewController{
    EmailSupportViewController *em = [ [EmailSupportViewController alloc] init];
    [self displaySubView:sidebarViewController mainVC:em revealSideBar:NO];
    [em release];
}

- (void)displaySpaces:(SidebarViewController*)sidebarViewController row:(NSInteger)row{
    SpacesViewController *sp = [[SpacesViewController alloc] init];
    sp.initialSpaceIdx = row;
    [self displaySubView:sidebarViewController mainVC:sp revealSideBar:YES];
    [sp release];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultFailed) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Request failed" message:@"Failed to send email" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void) displaySubView:(SidebarViewController*)sidebarViewController mainVC:(MainViewController*)mainVC revealSideBar:(BOOL)revealSideBar {
    if(!revealSideBar){
        [self.navigationController setRevealedState:JTRevealedStateNo];
    }
    
    mainVC.leftSidebarViewController = sidebarViewController;
    sidebarViewController.sidebarDelegate = mainVC;
    
    [self.navigationController setViewControllers:[NSArray arrayWithObject:mainVC] animated:NO];
}


@end
