//
//  Login.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/20/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Login.h"
#import "Settings.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Util.h"

@implementation Login

+ (NSMutableDictionary*) parseLoginPageParams:(NSString*)response {
	NSMutableDictionary *keyValues = [[[NSMutableDictionary alloc] init] autorelease];
	
	if(response != nil){
		[Login getParamValue:response parameter:@"__VIEWSTATE" dictionary:keyValues];
		[Login getParamValue:response parameter:@"__EVENTVALIDATION" dictionary:keyValues];
		
        [self getParamValue:response parameter:@"__PREVIOUSPAGE" dictionary:keyValues];
        
        [keyValues setObject:@"" forKey:@"__LASTFOCUS"];
        [keyValues setObject:@"" forKey:@"__EVENTTARGET"];
        [keyValues setObject:@"" forKey:@"__EVENTARGUMENT"];
        [keyValues setObject:@"0" forKey:@"LoginForm2$LoginButton.x"];
        [keyValues setObject:@"0" forKey:@"LoginForm2$LoginButton.y"];
        [keyValues setObject:@"Login" forKey:@"LoginForm2$LoginButton"];
	}
	
	return keyValues;
}

+ (BOOL) getParamValue:(NSString*)response parameter:(NSString*)param dictionary:(NSMutableDictionary*)dict {
	if(response != nil){
		NSString* paramValue = [param stringByAppendingString:@"\" value=\""];
		NSRange range = [response rangeOfString:paramValue];
		if(range.length > 0){
			int startIdx = range.location + range.length;
			int numChars = 0;
			for(int i = startIdx; i < [response length]; i++){
				if ([response characterAtIndex:i] == '"') {
					break;
				}
				numChars++;
			}
			NSString* value = [response substringWithRange:NSMakeRange(startIdx, numChars)];
			NSLog(@"set key: %@ value: %@", param, value);
			[dict setObject:value forKey:param];
			return YES;
		}
	}
	return NO;
}

+ (void) performLogin:(NSString*)username password:(NSString*)password startAnimate:(SEL)startAnimate endAnimate:(SEL)endAnimate delegate:(id)delegate loginSuccess:(SEL)loginSuccess loginFail:(SEL)loginFail{
   
	
	if(username == nil || [username length] == 0){
		NSLog(@"username empty");
		return;
	}
	
	if(password == nil || [password length] == 0){
		NSLog(@"password empty");
		return;
	}
	
	NSLog(@"Username %@ password %@", username, password);
	NSLog(@"Login button");
	
    NSURL *url = [Settings getNSURL:[Login getLoginPageUrl]];
	
    if(delegate && startAnimate){
        [delegate performSelector:startAnimate];
    }
    
	__block ASIHTTPRequest *req = [ASIHTTPRequest requestWithURL:url];
    [req setCompletionBlock:^{
        NSError *error = [req error];
        if(!error){
            NSString* response = [req responseString];
            NSLog(@"Response is \n %@", response);
            NSMutableDictionary* formKeyValues = [Login parseLoginPageParams:response];
            
            //Send login request
            NSString *page = [self getLoginPageUrl];
            NSLog(@"page = %@", page);
            ASIFormDataRequest *fRequest = [ASIFormDataRequest requestWithURL:[Settings getNSURL:page]];
            NSArray *keys = [formKeyValues allKeys];
            for(NSString *key in keys){
                [fRequest addPostValue:[formKeyValues objectForKey:key] forKey:key];
            }
            [fRequest addPostValue:username forKey:@"LoginForm2$UserName"];
            [fRequest addPostValue:password forKey:@"LoginForm2$Password"];
            
            //mobile
          //  if (![Settings isDevServer]) {
          //      [fRequest addPostValue:@"Login" forKey:@"LoginForm$LoginButton"];
          //  }
            
            [fRequest startSynchronous];
            NSError *err = [fRequest error];
            if(!err){
                NSString *response = [fRequest responseString];
                NSLog(@"Login response is \n %@", response);
                
                NSString* errMsg;
                
                NSArray*cookies = [fRequest requestCookies];
                
                //Find the CSRF cookie for use in all requests later
                for (NSHTTPCookie *c in cookies){
                    if ([c.name isEqualToString:@"XSRF-TOKEN"]) {
                        NSLog(@"Setting csrf %@ = %@", c.name, c.value);
                        [Settings instance].csrf = c.value;
                        break;
                    }
                }
                
                LoginReturnCode rc = [Login verifyLogin:fRequest];
                switch(rc){
                    case Success:
                        [Settings setUsername:username password:password];
                        [Settings setLoggedIn:YES];
                        
                        if([delegate respondsToSelector:loginSuccess]){
                            [delegate performSelector:loginSuccess];
                        }
                        return;
                    case Locked:
                        errMsg = @"Login has been locked after too many unsuccessful attempts, please contact customerservice@birst.com to unlock";
                        break;
                    case NotLicensed:
                        errMsg = @"You are not licensed for the Birst Mobile application, please contact customerservice@birst.com to acquire the license";
                        break;
                    case UnsuccessFul:
                        errMsg = @"Login attempt was not successful. Please try again";
                        break;
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login failed" message:errMsg delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                if([delegate respondsToSelector:loginFail]){
                    [delegate performSelector:loginFail withObject:fRequest];
                }
            }else {
                if([delegate respondsToSelector:loginFail]){
                    [delegate performSelector:loginFail withObject:fRequest];
                }
            }
            if(endAnimate != nil){
                [delegate performSelector:endAnimate];
            }
        }

    }];
    
    [req setFailedBlock:^{
        if([delegate respondsToSelector:loginFail]){
            [delegate performSelector:loginFail withObject:req];
        }
        NSError *err = [req error];
        NSLog(@"login request failed: %@", [err localizedDescription]);
        [Util requestFailedAlert:err];
    }];
    
	[req startAsynchronous];
}


+(LoginReturnCode) verifyLogin:(ASIHTTPRequest*)request{
    NSString *response = [request responseString];
    NSRange range = [response rangeOfString:@"DefaultHome?v="];
    
    if(range.length == 0){
        range = [response rangeOfString:@"/swf/Dashboards?v="];
    }
    
    if(range.length > 0){
        //Find our version
        NSUInteger idx = range.location + range.length;
        NSError* err = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\d+)\\." options:NSRegularExpressionCaseInsensitive error:&err];
        NSArray* versions = [regex matchesInString:response options:0 range:NSMakeRange(idx , 15)];
        if([versions count] > 2){
            Settings* setting = [Settings instance];
            int i = 0;
            for (NSTextCheckingResult *v in versions) {
                NSString *match = [response substringWithRange:[v rangeAtIndex:1]];
                switch(i){
                    case 0: setting.majorVersion = [match intValue];
                        break;
                    case 1: setting.minorVersion = [match intValue];
                        break;
                    case 2: setting.revisionVersion = [match intValue];
                        break;
                }
                i++;
            }
        }
        
        //Check if the server has been redirected
        NSURL *currentUrl = request.url;
        NSURL *originalUrl = request.originalURL;
        NSString *currentHost = [currentUrl host];
        NSString *originalHost = [originalUrl host];
        
        //Set the redirected url
        if(! [currentHost isEqualToString:originalHost]){
            NSString *redirectUrl = [NSString stringWithFormat:@"%@://%@", [currentUrl scheme], currentHost];
            NSNumber *port = [currentUrl port];
            if(port != nil){
                int portNum = [port intValue];
                if(portNum != 443){
                    redirectUrl = [NSString stringWithFormat:@"%@:%d", redirectUrl, portNum];
                }
            }
            [Settings setRedirectedServer:redirectUrl];
            NSLog(@"redirectUrl = %@", redirectUrl);
        }
        
        return Success;
    }else{
    
        //The errors
        range = [response rangeOfString:@"Login has been locked"];
        if(range.length > 0){
            return Locked;
        }
        
        range = [response rangeOfString:@"You are not licensed"];
        if(range.length > 0){
            return NotLicensed;
        }
        
        range = [response rangeOfString:@"Login attempt was not successful"];
        if(range.length > 0){
            return UnsuccessFul;
        }
    
        return UnsuccessFul;
    }
}


+ (NSString *) getLoginPageUrl{
    return @"Login.aspx";
}

@end
