//
//  SettingsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "SettingsViewController.h"
#import "Settings.h"
#import "ASIHTTPRequest.h"
#import "ServerSettingsEditViewController.h"
#import "Settings.h"
#import "LoginViewController.h"

@implementation SettingsViewController

#pragma mark -
#pragma mark View lifecycle

@synthesize detailCtrl, savedSettings;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Login";
    
    self.navigationController.contentSizeForViewInPopover = CGSizeMake(300, 100);
    
    [self loadSavedSettings];
	// Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void) loadSavedSettings{
    self.savedSettings = [[Settings instance]settings];
}

- (void) saveSavedSettings{
    //TODO - error alerts
    [Settings saveSavedSettings:self.savedSettings];    
}

- (NSMutableDictionary *)defaultSettings{
    NSMutableDictionary *defaults = [[[NSMutableDictionary alloc]init] autorelease];
    NSMutableArray *servers = [NSMutableArray arrayWithObject:@"https://app.birst.com"];
    [defaults setValue:servers forKey:@"servers"];
    return defaults;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     self.navigationController.contentSizeForViewInPopover = CGSizeMake(300, 100);
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:{
            // # of servers + edit 
            int editRow = [Settings readonly] ? 0 : 1;
            return [[self.savedSettings objectForKey:@"servers"] count] + editRow; 
        }
        case 1:
        case 2:
            return 1;
        default:
            return 0;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"Servers";
        case 1:
            return @"Login";
        case 2:
            return @"Version";
        default:
            return @"";
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
	}
	
	if (indexPath.section == 0) {
        NSArray *servers = [self.savedSettings objectForKey:@"servers"];
        NSString *rowName;
        if ([servers count] == indexPath.row) {
            rowName = @"Edit servers";
        }else{
            NSArray *servers = [self.savedSettings objectForKey:@"servers"];
            rowName = [servers objectAtIndex:indexPath.row];
        }
       
        cell.textLabel.text = rowName;
        
		if ([cell.textLabel.text isEqualToString:[Settings selectedServer]]) {
			cell.accessoryType = UITableViewCellAccessoryCheckmark;
		}else {
			cell.accessoryType = UITableViewCellAccessoryNone;
		}
	}else if (indexPath.section == 1) {
		if ([Settings loggedIn]) {
            NSString *username = [Settings demoMode] ? DEMO_USER : [Settings username];
			cell.textLabel.text = [NSString stringWithFormat:@"Logout %@", username];
		}else {
			cell.textLabel.text = @"Click to login";
		}
	}else if (indexPath.section == 2){
        NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSString* shortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", shortVersion, version];
    }
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editSection == indexPath.section) {
        return YES;
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        NSArray *servers = [self.savedSettings objectForKey:@"servers"];
        if (indexPath.row == [servers count]) {
            return UITableViewCellEditingStyleInsert;
        }
    }
    
    return UITableViewCellEditingStyleDelete;
}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
        NSArray *servers = [self.savedSettings objectForKey:@"servers"];
        if ([servers count] == indexPath.row) {
            ServerSettingsEditViewController *sctrl = [[[ServerSettingsEditViewController alloc]init] autorelease];
            sctrl.settingsCtrl = self;
            [self.navigationController pushViewController:sctrl animated:YES];
        }else{
            NSString *currentServer = [Settings selectedServer];
            UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
            NSString *selectedServer = selectedCell.textLabel.text;
        
            if (![currentServer isEqualToString:selectedServer]) {
                [Settings setServer:selectedServer];
                [tableView reloadData];
            }
        }
	}else if (indexPath.section == 1){
		 if ([Settings loggedIn]) {
			 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[Settings logoutUrl]];
			 [request startSynchronous];
			 [Settings setLoggedIn:NO];
             [Settings setRedirectedServer:nil];
             [Settings setLoginMode:spaceListing];
			 [self.detailCtrl loggedOutClearScreen];
		}else {
			LoginViewController* loginCtrl = [[[LoginViewController alloc]init]autorelease];
			loginCtrl.modalPresentationStyle = UIModalPresentationFormSheet;
			[self presentModalViewController:loginCtrl animated:YES]; 
			
			//Close the popover
			[self.detailCtrl toggleSettings];
		}

	}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [savedSettings release];
    [super dealloc];
}


@end

