    //
//  PromptViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/23/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "PromptViewController.h"
#import "FlexFieldController.h"
#import "QueryListTableController.h"
#import "SingleSelectListPickerController.h"
#import "Notification.h"
#import "PromptsViewController.h"
#import "Log.h"

@implementation PromptViewController

@synthesize prompt, promptCtrl;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[Log debug:@"viewWillAppear %@", self];
}

-(void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	[Log debug:@"viewWillDisappear %@", self];
	[self setValueToPrompt];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[Log debug:@"viewDidLoad %@", self];
	
	setValue = YES;
    //self.view.backgroundColor = [UIColor blackColor];
	
	UIBarButtonItem *apply = [[UIBarButtonItem alloc] initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(apply)];
	self.navigationItem.rightBarButtonItem = apply;
	[apply release];
	
	if (prompt) {
		self.title = prompt.name;
		
        //Create and set the sizing for different controls
		if(prompt.type == Value){
			self.promptCtrl = [[[FlexFieldController alloc]init]autorelease];
            if (prompt.isDate) {
                promptCtrlSize = CGSizeMake(340, 210);
            }else if(prompt.isDateTime){
                promptCtrlSize = CGSizeMake(340, 380);
            }else{
                promptCtrlSize = CGSizeMake(320, 45);
            }
		}else if(prompt.type == List || prompt.type == Query){
			if (prompt.isMultiSelect) {
				self.promptCtrl = [[[QueryListTableController alloc]init]autorelease];
                promptCtrlSize = CGSizeMake(320, 220);
			}else{
				self.promptCtrl = [[[SingleSelectListPickerController alloc]init]autorelease];
                promptCtrlSize = CGSizeMake(320, 170);
			}
		}
		
		if (self.promptCtrl) {
            if ([self.promptCtrl respondsToSelector:@selector(setPrompt:)]) {
                [self.promptCtrl performSelector:@selector(setPrompt:) withObject:prompt];
            }
            
            self.contentSizeForViewInPopover = promptCtrlSize;
            
            //Size ourself and the prompt subview
            CGRect f = self.view.frame;
            f.size = promptCtrlSize;
            self.view.frame = f;
            
            UIView *v = self.promptCtrl.view;
            v.frame = self.view.bounds;
            [self.view addSubview:v];
		}
	}	
}

- (void)setValueToPrompt{
    if (setValue) {
		[Log debug:@"Setting prompt value"];
        NSMutableArray *selectedValues = [self.promptCtrl performSelector:@selector(getSelectedValues)];
        if ([selectedValues count] > 0) {
            self.prompt.selectedValues = selectedValues;
        }
	} 
}

- (void)apply{
    //Look for the PromptsViewController before us
    
    NSArray *vcs = self.navigationController.viewControllers;
    for(int i = 0; i < [vcs count]; i++){
        UIViewController *v = [vcs objectAtIndex:i];
        if ([v isKindOfClass:[PromptsViewController class]]) {
            [self setValueToPrompt];
            [v performSelector:@selector(apply)];
        }
    }
}

- (void)cancel{
	setValue = NO;
	[self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [promptCtrl release];
    [super dealloc];
}


@end
