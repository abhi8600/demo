//
//  Layout.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/6/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "Layout.h"

@implementation Layout

@synthesize layouts, dashlet, percentWidth, percentHeight, direction, isPromptDashlet, promptRef;


- (void) parse:(TBXMLElement*)xml{
    if(xml != nil){
        //Sizing
        NSString *sz = [TBXML valueOfAttributeNamed:@"percentWidth" forElement:xml];
        self.percentWidth = [sz floatValue];
        sz = [TBXML valueOfAttributeNamed:@"percentHeight" forElement:xml];
        self.percentHeight = [sz floatValue];
        
        //Direction
        NSString *direct = [TBXML valueOfAttributeNamed:@"direction" forElement:xml];
        self.direction = [direct isEqualToString:@"vertical"] ? Vertical : Horizontal;
        
        TBXMLElement *node = xml->firstChild;
        if (node != nil) {
            do {
                NSString *elName = [TBXML elementName:node];
                if([elName isEqualToString:@"dashlet"]){
                    self.dashlet = [[[Dashlet alloc] init] autorelease];
                    [self.dashlet parse:node];
                }else if ([elName isEqualToString:@"Layout"]){
                    Layout *l = [[Layout alloc] init];
                    [l parse:node];
                    if(self.layouts == nil){
                        self.layouts = [[[NSMutableArray alloc] init ] autorelease];              
                    }
                    [self.layouts addObject:l];
                    [l release];
                }else if ([elName isEqualToString:@"Prompt"]){
                    NSString *ref = [TBXML valueOfAttributeNamed:@"ref" forElement:node];
                    if([ref length] > 0){
                        self.isPromptDashlet = YES;
                        self.promptRef = ref;
                    }
                }
            } while ((node = node->nextSibling));
        }
    }
}

- (NSString *) toXml{
    NSMutableString *xml = [NSMutableString stringWithFormat:@"<Layout percentWidth=\"%f\" percentHeight=\"%f\" direction=\"%@\">", self.percentWidth, self.percentHeight, self.direction == Vertical ? @"vertical" : @"horizontal"];
    if(self.dashlet){
        [xml appendString:[self.dashlet toXml]];
    }
    if(self.isPromptDashlet){
        [xml appendString:[NSString stringWithFormat:@"<Prompt ref=\"%@\"/>", self.promptRef]];
    }
    for(int i = 0; i < [self.layouts count]; i++){
        Layout *l = [self.layouts objectAtIndex:i];
        [xml appendString: [l toXml]];
    }
    [xml appendString:@"</Layout>"];
    return xml;
}

- (void) dealloc{
    [dashlet release];
    [layouts release];
    [promptRef release];
    [super dealloc];
}
@end
