//
//  Dashlet.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/14/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Dashlet.h"

@implementation Dashlet

@synthesize path, title, enablePdf, enablePpt, enableExcel, enableColumnSelectors, enableViewSelector, ignoredPrompts, hasBorder;

- (void) parse:(TBXMLElement*)xml{
    self.hasBorder = YES;
    
	TBXMLElement *node = xml->firstChild;
	if (node != nil) {
		do {
			NSString *nodeName = [TBXML elementName:node];
			NSString *value = [TBXML textForElement:node];
			if ([nodeName isEqualToString:@"Path"]) {
				self.path = value;
			}else if ([nodeName isEqualToString:@"Title"]) {
				self.title = value;
            }else if ([nodeName isEqualToString:@"EnablePDF"]){
                self.enablePdf = [value isEqualToString:@"true"];
            }else if ([nodeName isEqualToString:@"EnableSelectorsCtrl"]){
                self.enableColumnSelectors = [value isEqualToString:@"true"];
			}else if ([nodeName isEqualToString:@"EnableViewSelector"]){
                self.enableViewSelector = [value isEqualToString:@"true"];
            }else if ([@"ignorePromptList" isEqualToString:nodeName]){
                [self parseIgnoredPrompts:node];
            }else if ([nodeName isEqualToString:@"ViewMode"]){
                self.hasBorder = ![value isEqualToString:@"headerBorderless"];
            }
		} while ((node = node->nextSibling));
		//NSLog(@"parsed dashlet %@", self.path);

	}
}

- (NSString *) pathify{
    NSMutableString *str = [NSMutableString stringWithString:self.path];
    [str replaceOccurrencesOfString:@"/" withString:@"_" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length])];
    return str;
}

- (NSString *) toXml{
    NSMutableString *xml = [NSMutableString stringWithFormat:@"<dashlet><Path>%@</Path><Title>%@</Title>", self.path, self.title];
    [xml appendFormat:@"<ViewMode>%@</ViewMode>", (self.hasBorder) ? @"default" : @"headerBorderless"];
    [xml appendString:@"</dashlet>"];
    return xml;
}

- (void) parseIgnoredPrompts:(TBXMLElement*)xml{
    if(xml){
        TBXMLElement *item = xml->firstChild;
        if(item){
            if(!ignoredPrompts){
                ignoredPrompts = [[NSMutableSet alloc]init];
            }
            [ignoredPrompts removeAllObjects];
            
            do{
                NSString *prompt = [TBXML textForElement:item];
                if(prompt){
                    [self.ignoredPrompts addObject:prompt];
                }
            }while((item = item->nextSibling));
        }
    }
}

- (void) dealloc{
	[path release];
	[title release];
	[ignoredPrompts release];
	
	[super dealloc];
}
@end
