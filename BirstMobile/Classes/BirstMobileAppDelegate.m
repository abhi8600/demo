//
//  BirstMobileAppDelegate.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

// #import <BugSense-iOS/BugSenseCrashController.h>

#import "BirstMobileAppDelegate.h"
#import "RootViewController.h"
#import "DetailViewController.h"
#import "ASIHTTPRequest.h"
#import "Settings.h"
#import "SessionMaintainer.h"
#import "Notification.h"
#import "Settings.h"


#import "AboutUsViewController.h"
#import "SpacesViewController.h"
#import "TestFlight.h"
#import "BackgroundViewController.h"

@implementation BirstMobileAppDelegate

@synthesize window, splitViewController, rootViewController, detailViewController, backgroundImage, toBackgroundDate;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions { 
    //Bugsense crash reporter
    //[BugSenseCrashController sharedInstanceWithBugSenseAPIKey:@"2a6d62df"];
    
    //Testflight sdk
    [TestFlight takeOff:@"4a0f6989-9ada-4e87-9daf-658ef8b39c3e"];
    //[TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
     
    [TestFlight passCheckpoint:@"Startup"];
    
    // Change the user-agent such that we can bypass redirection to MobileLogin on 4.4.x servers
    NSString* userAgent = [NSString stringWithFormat:@"BirstMobile %@", [Settings appVersionString]];
    [ASIHTTPRequest setDefaultUserAgentString:userAgent];
    
    // Override point for customization after app launch.
	[ASIHTTPRequest setDefaultTimeOutSeconds:60];
       
    // Add initial background image
    //[self addBackgroundImage];
    
    
    // Add sibebar viewcontroller
    MainViewController *sbMain = nil;
    if([Settings isUsernamePasswordNotEmpty]){
        sbMain = [[SpacesViewController alloc] init];
    }else{
        sbMain = [[AboutUsViewController alloc] init];
    }
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:sbMain];
    navController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [sbMain release];
    
    UINavigationController *container = [[UINavigationController alloc] init];
    [container setNavigationBarHidden:YES animated:NO];
    //[container setViewControllers:[NSArray arrayWithObject:navController] animated:NO];
    //[navController release];
    
    BackgroundViewController *bg = [[BackgroundViewController alloc] init];
    [bg.view addSubview:navController.view];
    
    self.window.rootViewController = bg; //navController; //container;
    [bg release];
    [container release];
    
    [self.window makeKeyAndVisible];

    //Session Poller
    [[SessionMaintainer instance]start];
    
    //Session timeout notification
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    [ctr addObserver:self selector:@selector(logout) name:SESSION_TIMEOUT object:nil];
    
    //UIWebView caching
    int cacheSizeMemory = 1*1024*1024; // 1MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    NSURLCache *sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
    [NSURLCache setSharedURLCache:sharedCache];
    
    return YES;
}

- (void)addBackgroundImage{
    NSLog(@"addBackgroundImage");
    
    //Background image exists
    if(backgroundImage != nil){
        NSLog(@"addBackgroundImage: backgroundImage not nil, return");
        return;
    }
    
    // Past we used [UIColor colorWithPatternImage..} to set the background image. That was a super memory hog.
    NSString *img = [Settings valueForKey:@"backgroundImage"];
    if ([img length] == 0) {
        img = @"BackgroundHexComb";
    }
    
    NSString *imgPath = [[NSBundle mainBundle] pathForResource:img ofType:@"JPG"];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:imgPath];
    if(image){
        UIImageView *v = [[UIImageView alloc] initWithImage:image];
        v.userInteractionEnabled = YES;
        [splitViewController.view insertSubview:v atIndex:0];
        [image release];
        self.backgroundImage = v;
        [v release];
    }    
}

- (void)removeBackgroundImage{
    NSLog(@"removeBackgroundImage");
    
    if(backgroundImage){
        [backgroundImage removeFromSuperview];
        [backgroundImage release];
        backgroundImage = nil;
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    NSLog(@"applicationDidEnterBackground");
    self.toBackgroundDate = [[[NSDate alloc]init]autorelease];
    [[SessionMaintainer instance]stop];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    NSLog(@"applicationWillResignActive");
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive");
    
    //Check if we've gone past the session maintanence time
    if(toBackgroundDate && [Settings loggedIn]){
        NSDate *current = [[NSDate alloc]init];
        NSTimeInterval diff = [current timeIntervalSinceDate:toBackgroundDate];
        [current release];
        [toBackgroundDate release];
        toBackgroundDate = nil;
        
        //logout
        if(diff > POLL_INTERVAL){
            [self logout];
            [[NSNotificationCenter defaultCenter] postNotificationName:SESSION_TIMEOUT object:nil];
            [Util alert:@"The application has logged out due to inactivity. Please login again." title:@"Logged out"];
        }
    }
    [[SessionMaintainer instance]start];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    [Log warn:@"UIApplication memory warning"];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)logout{
    [Settings setLoggedIn:NO];
    [Settings setLoginMode:spaceListing];
    [detailViewController loggedOutClearScreen];  
}

- (void)dealloc {
    [splitViewController release];
    [rootViewController release];
    [detailViewController release];
    [window release];
    [backgroundImage release];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [super dealloc];
}


@end

