//
//  SessionMaintainer.m
//  BirstMobile
//
//  Created by Seeneng Foo on 6/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "SessionMaintainer.h"
#import "Settings.h"
#import "BirstWS.h"



@implementation SessionMaintainer

@synthesize pollRequest;

static SessionMaintainer *_instance = nil;

+ (SessionMaintainer*)instance{
    if(!_instance){
        _instance = [[SessionMaintainer alloc]init];
    }
    return _instance;
}

- (id) init{
    self = [super init];
    if(self){
        
    }
    return self;
}

- (void) start{
    NSLog(@"SessionMaintainer.start()");
    if(!timer){
        timer = [NSTimer scheduledTimerWithTimeInterval:POLL_INTERVAL target:self selector:@selector(poll) userInfo:nil repeats:YES];
    }
}

- (void) stop{
    NSLog(@"SessionMaintainer.stop()");
    if(timer){
        [self clearRequest];
        [timer invalidate];
        timer = nil;
    }
}

- (BOOL) isPolling{
    return (timer != nil);
}
                 
- (void) poll{
    NSLog(@"SessionMaintainer.poll()");
    if([Settings loggedIn]){
        NSLog(@"SessionMaintainer making poll request");
        [self clearRequest];
        
        NSString *server = [NSString stringWithFormat:@"%@/PollSession.aspx", [Settings server]];
        NSURL *url = [NSURL URLWithString:server];
        self.pollRequest = [ASIHTTPRequest requestWithURL:url];
        [pollRequest setDelegate:self];
        [pollRequest setDidFailSelector:@selector(requestFailed:)];
        [pollRequest setDidFinishSelector:@selector(requestFinished:)];
        [pollRequest startAsynchronous];
    }
}

- (void) requestFinished:(ASIHTTPRequest *)request{
    NSLog(@"SessionMaintainer.requestFinished(): \n%@", [request responseString]);
    [self clearRequest];
}

- (void) requestFailed:(ASIHTTPRequest*)request{
    NSLog(@"SessionMaintainer.requestFailed():\n%@", [[request error] localizedDescription]);
}

- (void) clearRequest{
    if(pollRequest){
        [pollRequest clearDelegatesAndCancel];
        [pollRequest release];
        pollRequest = nil;
    }
}

- (void) dealloc{
    [timer release];
    [self clearRequest];
    
    [super dealloc];
}
                 
@end
