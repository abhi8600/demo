//
//  DrillAttributes.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/19/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"

typedef enum{
    DrillDown,
    DrillToDashboard,
    DrillToUrl
}DrillType;

@interface DrillAttributes : NSObject {
    NSMutableDictionary *keyValues;
}

@property (nonatomic, readonly) DrillType type;
@property (nonatomic, readonly) NSString *targetURL;
@property (nonatomic, readonly) NSMutableDictionary *keyValues;
@property (nonatomic, readonly) NSString *filters;
@property (nonatomic, readonly) NSString *drillBy;

- (id) initWithQuery:(NSString *)query;
- (id) initWithXml:(TBXMLElement *) xml;
- (NSString *) valueForAttribute:(NSString*)attr;
- (id) initWithLinkString:(NSString *)link;
- (NSString*) removeApos:(NSString *)str;
- (void) parseDrillColumnsInfo:(NSString *)drillCols;
- (void) notify;
- (NSString *) drillToDashboard;
- (NSString *) drillToPage;

@end
