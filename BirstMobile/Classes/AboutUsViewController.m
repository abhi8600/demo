//
//  AboutUsViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 6/27/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "AboutUsViewController.h"
#import "UIViewController+JTRevealSidebarV2.h"
#import <QuartzCore/QuartzCore.h>

@implementation AboutUsViewController

@synthesize  webview, version;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    UIColor *top = [Util colorWithRGBHex:0x000000];
    UIColor *bottom = [Util colorWithRGBHex:0x666666];
    gradient.colors = [NSArray arrayWithObjects:(id)[top CGColor], (id)[bottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    //Version information
    NSString* bundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* shortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.version.text = [NSString stringWithFormat:@"v%@ (%@)", shortVersion, bundleVersion];
    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma - SidebarViewController delegate operations

-(NSIndexPath *)lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController{
    return [NSIndexPath indexPathForRow:IDX_ABOUT inSection:IDX_HDR_MAIN];
}

@end
