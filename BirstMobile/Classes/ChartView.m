#import "ChartView.h"
#import "Util.h"
#import "TDGSeries.h"
#import "ChartView.h"
#import "Notification.h"
#import "DrillAttributes.h"
#import "Log.h"

#define FONT_MULTIPLICATION_FACTOR 0.65

@implementation ChartView

@synthesize chart, xAxisTitle, yAxisTitle;

-(id)initWithXml:(TBXMLElement*)xml frame:(CGRect)f {
    if ((self = [super initWithFrame:f])) {
		chart = [[TDGChart alloc] init];
        
		chart.bounds = f;
        chart.box.border = nil;
        chart.frame.border.thickness = 0;
        chart.dataManager.useSampleData = NO;
        chart.legend.visible = NO;
        supportedChart = YES;
        
        // 3-D like chart
        //chart.useDepth = YES;
        
		[self parse:xml];
	}
    return self;
}

-(void)resize:(CGRect)f {
	self.frame = f;
	chart.bounds = f;
}

/**
 Tell chart engine to draw in this view.
 */
-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    if (supportedChart) {
        @try {
            [chart draw:UIGraphicsGetCurrentContext()];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception raised while drawing chart %@", [exception reason]);
        }
        @finally {
            
        }
        
    }
}

- (void)dealloc {
    [Log debug:@"ChartView dealloc"];
    [Log debug:@"chart retainCount before release is %d", [chart retainCount]];
    [chart release];
    chart = nil;
    [xAxisTitle release];
    [yAxisTitle release];
    [seriesAttributes release];
    [super dealloc];
}

- (NSMutableDictionary *) seriesAttributes{
    if (!seriesAttributes) {
        seriesAttributes = [[NSMutableDictionary alloc]init];
    }
    
    return seriesAttributes;
}

- (void)parse:(TBXMLElement *)xml{
	if (xml != nil) {
		TBXMLElement *ct = [Util traverseFindElement:xml target:@"charts"];
		if (ct) {
            TBXMLElement *fChart = ct->firstChild;
            NSString *plotType = nil;
			if (fChart && [[TBXML elementName:fChart] isEqualToString:@"chart"]) {
                
                //We don't support more than one chart.
                if (fChart->nextSibling && [[TBXML elementName:fChart->nextSibling] isEqualToString:@"chart"]) {
                    //TODO: Pop up or render the image or write error message in view
                    NSLog(@"More than one chart!");
                    return;
                }
                
				plotType = [TBXML valueOfAttributeNamed:@"plot_type" forElement:fChart];
                
				if ([@"CategorizedVertical" isEqualToString:plotType]) {
                    chart.chartType = BLAChart;
					chart.blaProps.orientation = vertical;
				}else if([@"CategorizedHorizontal" isEqualToString:plotType]){
                    chart.chartType = BLAChart;
                    chart.blaProps.orientation = horizontal;
                }else if([@"Pie" isEqualToString:plotType]){
                    chart.chartType = PieChart;
					/*
                    DropShadowInstRec Shadow;
                    Shadow.nXoff = 100;
                    Shadow.nYoff = 300;
                    Shadow.nRed = 0;
                    Shadow.nGreen = 0;
                    Shadow.nBlue = 0;
                    
                    [chart setValueForProperty:A_AREADROPSHADOW toValue:&Shadow];
                     */
                    
                    TDGPieProperties *props = chart.pieProps;
					props.rotation = 180;
					
                    //Enable showing of the /---- label to each slice
					TDG_BOOLEAN16 feeler;
					feeler.nSeriesID = 0;
					feeler.bValue = YES;
					[chart setValueForProperty:API_SHOW_FEELER toValue:&feeler];
                    
                    //Show the /---- label to all slices
                    TDG_SerDepINT16 dataLabelOpt;
					dataLabelOpt.nSeriesID = -11;
					dataLabelOpt.nValue = 2;
					[chart setValueForProperty:API_SHOW_LBLFEELER  toValue:&dataLabelOpt];

					int showDataText = 0;
					
					//BOOL on = YES;
					//[chart setValueForProperty:kTDG_Prop_Pie_Tilt_On toValue:&on];
                    
					[chart setValueForProperty:A2D_SHOW_DATATEXT toValue:&showDataText];
										
					/*
                     int dataText = 11;
                     [chart setValueForProperty:API_FORMAT_DATATEXT toValue:&dataText];
                     */
					
                    /*
					int dataLabelLoc = 4;
					[chart setValueForProperty:API_PLACE_DATALABEL toValue:&dataLabelLoc];
					[chart setValueForProperty:API_PLACE_VALUELABEL toValue:&dataLabelLoc];
                    */
                    chart.pieProps.labelDisplay = ShowLabel;
                    
					/* Uncomment to set the depth / 3-D like
					int depth = 20;
					[chart setValueForProperty:API_DEPTH toValue:&depth];					
                     */
                }else{
                    //Not supported chart
                    [self displayUnsupportedMessage];
                    
                    //Write to the view that we cannot 
                    return;
                }
                
                //BLAChart settings
                if (chart.chartType == BLAChart) {
                    //smaller grid lines
					chart.numericYAxis.majorGrid.gridLineFormat.thickness = 0.5;
                    chart.numericXAxis.majorGrid.gridLineFormat.color = [UIColor blackColor];
					chart.numericYAxis.majorGrid.tickStyle = outside;
                    chart.blaProps.riserWidth = 32;
                }
                
                //Chart settings
                TBXMLElement *settings = [Util traverseFindElement:ct target:@"chart_settings"];
                [self parseChartSettings:settings];
                
                
                TBXMLElement *plotSettings = [TBXML nextSiblingNamed:@"data_plot_settings" searchFromElement:settings];
                if(plotSettings){
                    
                    //3-D like mode
                    chart.useDepth = [[TBXML valueOfAttributeNamed:@"enable_3d_mode" forElement:plotSettings] isEqualToString:@"True"];
                    
                    //Pie chart uses another setting for depth
                    if(chart.chartType == PieChart && chart.useDepth){
                        int depth = 20;
                        [chart setValueForProperty:API_DEPTH toValue:&depth];
                    }
                    
                    //Parse the label settings
                    [self parseLabelSettings:[Util traverseFindElement:plotSettings target:@"label_settings"]];
                }
                
                //Series information here contains the chart data
                TBXMLElement *series = [Util traverseFindElement:ct target:@"series"];
                if(series){
                    [self parseSeries:series];
                }else{
                    [self displayScreenMessage:@"Query returned no data. Try removing some filters"];
                }
			}   
            
            //TODO: X, Y axis label formatting
           
            //[chart setValueForProperty:A2D_FORMAT_X toValue:&fmt];
            
            //[chart setValueForProperty:A2D_FORMATDTXT_X toValue:&fmt];
            //[chart setValueForProperty:A2D_FORMATDTXT_Y1 toValue:&fmt];
		}else{
            //Could be a gauge chart here
            [self displayUnsupportedMessage];
            return;
        }
	}
}

-(void) displayUnsupportedMessage{
    [self displayScreenMessage:@"This report contains a chart type that is currently not supported"];
}
                     
-(void)displayScreenMessage:(NSString*)message{
    supportedChart = NO;
    
    self.backgroundColor = [UIColor blackColor];
    UILabel *msg = [[UILabel alloc]initWithFrame:self.frame];
    msg.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    msg.textColor = [UIColor whiteColor];
    msg.backgroundColor = [UIColor blackColor];
    msg.textAlignment = UITextAlignmentCenter; 
    msg.text = message;
    [self addSubview:msg];
    [msg release];
}

-(void) parseSeries:(TBXMLElement *)series{
    if (series) {
        int seriesIdx = 0;
        
        NSMutableArray *seriesNames = [[NSMutableArray alloc]init];
        do{
            NSString *name = [TBXML	valueOfAttributeNamed:@"name" forElement:series];
            NSString *type = [TBXML valueOfAttributeNamed:@"type" forElement:series];
            NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:series];
            
            if (chart.chartType == BLAChart) {
                if ([@"Line" isEqualToString:type]) {
                    chart.defaultSeries.format = line;
                    chart.useDepth = NO;
                }else if ([@"Area" isEqualToString:type]){
                    chart.defaultSeries.format = area;
                }
            }
            
            TBXMLElement *point = series->firstChild;
            if (point) {
                switch (chart.chartType) {
                    case PieChart:
                        [self parsePointsPie:point name:name];
                        break;
                    case BLAChart:{
                        TDGSeries *s = [chart seriesAtIndex:seriesIdx];
                        
                        //Gradient colors for BA charts, just color for L (line) charts
                        NSArray *gradColors = [self startEndGradientColor:color percentDiff:0.4 secondAlpha:0.8];
                        if(chart.defaultSeries.format != line && [gradColors count] == 2){
                            TDGGradient *gradient = s.fill.gradient;
                            gradient.format = gradient_linear;
                            gradient.startColor = [gradColors objectAtIndex:1];
                            gradient.endColor = [gradColors objectAtIndex:0];
                            gradient.angle = (chart.blaProps.orientation == horizontal) ? 90 : 45;
                        }else{
                            s.fill.color = [self parseRgbColor:color];
                        }
                        
                        [self parsePoints:point name:name seriesIndex:seriesIdx];
                        [seriesNames addObject:name];
                    }
                    default:
                        break;
                }
            }
            seriesIdx++;
        }while((series = series->nextSibling));
        
        /* Resizing option doesn't work if there is a lot of text. Need to turn it off
         * or wait for 3dgraphics's solution. AnyChart shows a scrollable legend box
        //Resize the legend fonts because they can get too big for a big series
        //and override the name
        if([seriesNames count] > 10){
            chart.legend.labelFormat.fontSize *= 0.1;
        }*/
        
        if (chart.chartType == BLAChart) {
            [chart.dataManager setSeriesLabels:seriesNames];
        }
        [seriesNames release];
    }
}

-(void) parseChartSettings:(TBXMLElement *)settings{
    if (settings) {
        TBXMLElement *sNode = settings->firstChild;
        if (sNode) {
            do {
                NSString *tagName = [TBXML elementName:sNode];
            
                if ([@"chart_background" isEqualToString:tagName]) {
                    UIColor *c = [self getBackgroundColor:sNode];
                    if (c) {
                        chart.box.backgroundColor = c;
                        if (chart.legend.visible) {
                            chart.legend.box.backgroundColor = c;
                        }
                    }else{
                        chart.box.backgroundColor = [UIColor whiteColor];
                    }
                }else if ([@"data_plot_background" isEqualToString:tagName]){
                    UIColor *pBg = [self getBackgroundColor:sNode];
                    if (pBg) {
                        chart.frame.backgroundColor = pBg;
                    }else{
                        chart.frame.backgroundColor = [UIColor whiteColor];
                    }
                }else if ([@"axes" isEqualToString:tagName]) {
                    [self parseAxes:sNode];
                }else if ([@"legend" isEqualToString:tagName]){
                    [self parseLegend:sNode];
                }
            }while ((sNode = sNode->nextSibling));
        }
    }
}

-(void)handleTap:(UITapGestureRecognizer*)tapEvent {
    CGPoint p = [tapEvent locationInView:self];
    TDGDetectionElement* element = [self.chart elementAtPoint:p];
	switch (element.elementType) {
		case kTDG_Title:
			[Log debug:@"Touched title '%@'", self.chart.title.text];
			break;
		case kTDG_Subtitle:
			[Log debug:@"Touched subtitle '%@'", self.chart.subtitle.text];
			break;
		case kTDG_Footnote:
			[Log debug:@"Touched footnote '%@'", self.chart.footnote.text];
			break;
		case kTDG_Legend:
			[Log debug:@"Touched legend"];
			break;
            //	axis is only one pixel, hard to touch
		case kTDG_Ordinal_Axis:
			[Log debug:@"Touched ordinal axis"];
			break;
            //	axis is only one pixel, hard to touch
		case kTDG_Y1_Axis:
			[Log debug:@"Touched y axis"];
			break;
		case kTDG_Riser:
			[Log debug:@"Touched riser series: %d group: %d", element.seriesID, element.groupID];
            if(self.seriesAttributes){
                NSDictionary *groups = [self.seriesAttributes valueForKey:[NSString stringWithFormat:@"%d", element.seriesID]];
                if (groups) {
                    DrillAttributes *attr = [groups valueForKey:[NSString stringWithFormat:@"%d", element.groupID]];
                    [attr notify];
                }
            }
			break;
		default:
			break;
	}
}

-(UIColor *) getBackgroundColor:(TBXMLElement *)background{
	if (background != nil) {
		if([[TBXML valueOfAttributeNamed:@"enabled" forElement:background] isEqualToString:@"True"]){
			TBXMLElement *fill = [Util traverseFindElement:background->firstChild target:@"fill"];
			if (fill) {
				if ([[TBXML valueOfAttributeNamed:@"enabled" forElement:fill] isEqualToString:@"True"]) {
					NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:fill];
					return [self parseRgbColor:color];
				}
			}
		}
	}
	return nil;
}

//parse the data points (Bar)
- (void) parsePoints:(TBXMLElement *)p name:(NSString*)name seriesIndex:(int)seriesIndex{
	chart.title = nil;
	chart.subtitle = nil;
	chart.footnote = nil;
	chart.box.border = nil;
	
	id<TDGDataProtocol> data = chart.dataManager;
	data.useSampleData = NO;
		
	NSMutableArray *columnNames = [[[NSMutableArray alloc]init] autorelease];
	if (p != nil) {
		int i = 0;
		do {
			NSString *tagName = [TBXML elementName:p];
			if ([@"point" isEqualToString:tagName]) {
                
				NSString *name = [TBXML valueOfAttributeNamed:@"name" forElement:p];
                name = [Util xmlUnescape:name];
                if(!name){
                    name = @"";
                }
                [columnNames addObject:name];
                
                NSString *yStr = [TBXML valueOfAttributeNamed:@"y" forElement:p];
				float y = [ yStr floatValue];
				[Log debug:@"point name=%@ y=%f seriesIndex = %d index = %d", name, y, seriesIndex
, i];
				[data setYPoint:seriesIndex index:i data:y];
                
                if (p->firstChild && [@"attributes" isEqualToString:[TBXML elementName:p->firstChild]]) {
                    [self parsePointAttribute:p->firstChild->firstChild series:seriesIndex group:i];
                }
				i++;
			}else if([@"label" isEqualToString:tagName]){
                NSString *enabled = [TBXML valueOfAttributeNamed:@"enabled" forElement:p];
                if([@"true" isEqualToString:[enabled lowercaseString]]){
                    TDGSeries *s = [chart seriesAtIndex:seriesIndex];
                    s.showDataText = YES;
                }
            }
		} while ((p = p->nextSibling));
    
		[data setGroupLabels:columnNames];
		[data setDataPointCount:[columnNames count]];
	}
}


- (void) parsePointAttribute:(TBXMLElement *)attr series:(int)series group:(int)group{
    if (attr) {
        NSString *seriesKey = [NSString stringWithFormat:@"%d", series];
        NSString *groupKey = [NSString stringWithFormat:@"%d", group];
        
       
        NSMutableDictionary *groups = [self.seriesAttributes objectForKey:seriesKey];
        if (!groups) {
            groups = [[[NSMutableDictionary alloc]init] autorelease];
        }
        
        DrillAttributes *drillAttr = [[DrillAttributes alloc] initWithXml:attr];
        [groups setValue:drillAttr forKey:groupKey];
        [self.seriesAttributes setValue:groups forKey:seriesKey];
        [drillAttr release];
    }
}
                        
- (void) parseLabelSettings:(TBXMLElement*)el{
    if(el){
        TBXMLElement *n = [Util traverseFindElement:el target:@"font"];
        if(n){
            NSString *family = [TBXML valueOfAttributeNamed:@"family" forElement:n];
            float size = [[TBXML valueOfAttributeNamed:@"size" forElement:n] floatValue];
            NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:n];
            UIColor *textColor = [self parseRgbColor:color];
            
            TDGTextFormat* fmt = (chart.chartType == PieChart) ? chart.pieProps.sliceTextFormat : chart.dataTextFormat;
            
            fmt.font = family;
            fmt.color = textColor;
            size *= FONT_MULTIPLICATION_FACTOR;
            size = ceil(size);
            fmt.fontSize = size;
            
            if(chart.chartType == PieChart){
                //Check if we have showValue set, that's in <extra_labels>
                for(TBXMLElement *sib = el->nextSibling; sib != nil; sib=sib->nextSibling){
                    if([@"extra_labels" isEqualToString:[TBXML elementName:sib]]){
                        chart.pieProps.labelDisplay = ShowLabelAndValue;
                        chart.pieProps.valueOnSlice = YES;
                        chart.dataTextFormat = fmt;
                        break;
                    }
                }
            }else { // BLAChart
                //int datatext = 1;
                //[chart setValueForProperty:A2D_SHOW_DATATEXT toValue:&datatext];
                
                /* This is suppose to set it on the top of the riser, but doesn't work
                 * See http://www.threedgraphics.com/tdg/products/tools/pgsdk/manual/pg_attr_03.htm#a2d_place
                int showValue = 4;
                [chart setValueForProperty:A2D_PLACE_Y1 toValue:&showValue];
                 */
            }
            
        }
    }
}

//parse the data points (Bar)
- (void) parsePointsPie:(TBXMLElement *)p name:(NSString*)name{
    chart.useDepth = NO;
	chart.title = nil;
	chart.subtitle.visible = NO;
	chart.subtitle.visible = NO;
	chart.footnote.visible = NO;
	chart.legend.visible = NO;
	
	chart.frame.border.color = [UIColor clearColor];
	
	id<TDGDataProtocol> data = chart.dataManager;
	data.useSampleData = NO;
	
	NSMutableArray *columnNames = [[[NSMutableArray alloc]init] autorelease];
	if (p != nil) {
		int i = 0;
		do {
			NSString *tagName = [TBXML elementName:p];
			if ([@"point" isEqualToString:tagName]) {
				NSString *name = [TBXML valueOfAttributeNamed:@"name" forElement:p];
				NSString *y = [TBXML valueOfAttributeNamed:@"y" forElement:p];
				NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:p];
				if(!name){
                    name = @"";
                }
				[Log debug:@"point name=%@ y=%@ color=%@", name, y, color];
				[columnNames addObject:name];			
				[data setYPoint:i index:0 data:[y floatValue]];
				
                //Set the pie color gradients
				TDGSeries *series = [chart seriesAtIndex:i];
				NSArray *gradColors = [self startEndGradientColor:color percentDiff:0.3 secondAlpha:1];
                TDGGradient *gradient = series.fill.gradient;
                gradient.format = gradient_radial;
                gradient.angle = 90;
                gradient.startColor = [gradColors objectAtIndex:0];
                gradient.endColor = [gradColors objectAtIndex:1];
                
                if (p->firstChild && [@"attributes" isEqualToString:[TBXML elementName:p->firstChild]]) {
                    [self parsePointAttribute:p->firstChild->firstChild series:i group:0];
                }
				i++;
			}			
		} while ((p = p->nextSibling));
		
		[data setSeriesLabels:columnNames];
		[data setGroupLabels: [NSArray arrayWithObject: @""]];
		[data setDataPointCount:1];
	}
}

- (void) parseLegend:(TBXMLElement *)legend{
    NSString* enabled = [TBXML valueOfAttributeNamed:@"enabled" forElement:legend];
    BOOL isVisible = [enabled isEqualToString:@"True"];
    self.chart.legend.visible = isVisible;
    
    if(isVisible){
        chart.legend.markerPos = leftOfLabels;
        chart.legend.box.backgroundColor = [UIColor whiteColor];
        TDGGradient* g = chart.legend.box.gradient;
        g.format = gradient_none;
    
        //Positioning is not implemented yet by 3dgraphics
        NSString *position = [TBXML valueOfAttributeNamed:@"position" forElement:legend];
        if ([@"Bottom" isEqualToString:position]) {
            self.chart.legend.legendPos = belowChart;
        }else if ([@"Top" isEqualToString:position]){
            self.chart.legend.legendPos = freeFloating; //? freeFloating is the only other option
        }else if ([@"Left" isEqualToString:position]){
            self.chart.legend.legendPos = leftOfChart;
        }else if ([@"Right" isEqualToString:position]){
            self.chart.legend.legendPos = rightOfChart;
        }
        
        chart.legend.title.visible = NO;
        
        //We cannot set the legend font color and type yet
        
        TBXMLElement *font = [Util traverseFindElement:legend target:@"font"];
        if(font){
            
            NSString *family = [TBXML valueOfAttributeNamed:@"family" forElement:font];
            
            float size = [[TBXML valueOfAttributeNamed:@"size" forElement:font] floatValue];
            //rough conversion to make it smaller and in line with AnyCharts chart labels size
            size *= FONT_MULTIPLICATION_FACTOR;
            size = ceil(size);
            
            NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:font];
            UIColor *textColor = [self parseRgbColor:color];
                       
            TDGTextFormat *fmt = chart.legend.labelFormat;
            fmt.color = textColor;
            fmt.font = family;
            fmt.fontSize = size;
            
        }
        
    }
}

- (void) parseAxes:(TBXMLElement *)axes{
	if (axes != nil) {
        chart.ordinalAxis.title.text = self.xAxisTitle = @"";
        chart.numericYAxis.title.text = self.yAxisTitle = @"";
        
		TBXMLElement *child = axes->firstChild;
		if (child != nil) {
			do {
				NSString *name = [TBXML elementName:child];
                BOOL isAxisX = [@"x_axis" isEqualToString:name];
                BOOL isAxisY = [@"y_axis" isEqualToString:name];
				if ( isAxisX || isAxisY ) {
					TBXMLElement *node = child->firstChild;
					if (node) {
						do {
                            NSString *nodeName = [TBXML elementName:node];
                            if (isAxisY && [@"scale" isEqualToString:nodeName] && chart.chartType == BLAChart) {
                                NSString *mode = [TBXML valueOfAttributeNamed:@"mode" forElement:node];
                                
                                if ([mode isEqualToString:@"Stacked"]){
                                    chart.blaProps.seriesLayout = stacked;
                                }
                            }else if ([@"title" isEqualToString:nodeName]) {
								NSString *enabled = [TBXML valueOfAttributeNamed:@"enabled" forElement:node];
								if (enabled && [enabled isEqualToString:@"True"]) {
									TBXMLElement *n = node->firstChild;
									if (n) {
										do {
											NSString *nodeName = [TBXML elementName:n];
											
											if([@"text" isEqualToString:nodeName]){
												NSString *value = [TBXML textForElement:n];
												if (isAxisX) {
													self.xAxisTitle = value;
													chart.ordinalAxis.title.text = value;	
												}else {
													self.yAxisTitle = value;
													chart.numericYAxis.title.text = value;	
												}
											}else if ([@"font" isEqualToString:nodeName]) {
												[self parseFont:n isAxisX:isAxisX formatOrdinal:chart.ordinalAxis.title.format formatY:chart.numericYAxis.title.format];
											}
										}while ((n = n->nextSibling));
									}
								}
							}else if ([@"labels" isEqualToString:nodeName]){
								NSString *enabled = [TBXML valueOfAttributeNamed:@"enabled" forElement:node];
								if (enabled && [enabled isEqualToString:@"True"]) {
									TBXMLElement *n = node->firstChild;
									if (n) {
										do {
											NSString *nodeName = [TBXML elementName:n];
											
											if ([@"font" isEqualToString:nodeName]) {
												[self parseFont:n isAxisX:isAxisX formatOrdinal:chart.ordinalAxis.labelFormat formatY:chart.numericYAxis.labelFormat];
											}else if ([@"format" isEqualToString:nodeName]){
                                                [self parseFormat:n isAxisX:isAxisY];
                                            }
										}while ((n = n->nextSibling));
									}
								}
								
							}
						} while ((node = node->nextSibling));
					}
				}
			} while ((child = child->nextSibling));
		}
	}	
}

//Parse the label xml format definition
//TODO: when we supoort scatter plots, need to figure out the axis to format
//Refer to http://www.threedgraphics.com/tdg/products/tools/pgsdk/manual/pgsdk_7.htm#formattingnumbers
- (void)parseFormat:(TBXMLElement *)xml isAxisX:(BOOL)isAxisY{
    NSString *fmt = [TBXML textForElement:xml];
    
    //decimals?
    NSRange  range = [fmt rangeOfString:@"numDecimals:2"];
    BOOL useDecimals = range.length > 0;
    
    //thousands separator?
    range = [fmt rangeOfString:@"thousandsSeparator:\\,"];
    BOOL useCommaThousands = range.length > 0;
    
    //$ ?
    range = [fmt rangeOfString:@"$"];
    if(range.length > 0){
        int dollarFmt = 7; // $1000000
        if(useDecimals){ 
            //$1,00,000.00 vs $1000000.00
            dollarFmt = (useCommaThousands) ? 10 : 8; 
        }else{
            //$1,000,000 vs $1000000
            dollarFmt = (useCommaThousands) ? 9 : 7;
        }
        
        if(isAxisY){
            [chart setValueForProperty:A2D_FORMAT_Y1 toValue:&dollarFmt];
        }
    }else{
        int fmt = 1; //general 1000000
        
        //no $$
        if(useDecimals){
            //1,000,000.00 vs 1000000.00
            //BUG - 6, 4 does not work!
            fmt = (useCommaThousands) ? 6 : 4; 
        }else{
            //1,000,000 vs 1000000
            fmt = (useCommaThousands) ? 5 : 1;
        }
        
        if(isAxisY && fmt != 1){
            [chart setValueForProperty:A2D_FORMAT_Y1 toValue:&fmt];
        }
    }
    
    //percent? 
    range = [fmt rangeOfString:@"scale:(0.01)|(%)"];
    if(range.length > 0){
        //BUG - 13, 11 doesn't work, doesn't set decimals and always uses thousand separator ','
        int percentFmtNum = (useDecimals) ? 13 : 11;
        if(isAxisY){
            [chart setValueForProperty:A2D_FORMAT_Y1 toValue:&percentFmtNum];
        }
    }
}

//Parse the font xml definition and set the family, size, color
- (void) parseFont:(TBXMLElement*)font isAxisX:(BOOL)isAxisX formatOrdinal:(TDGTextFormat*)ordinal formatY:(TDGTextFormat*)y {
    NSString *family = [TBXML valueOfAttributeNamed:@"family" forElement:font];
    
    float size = [[TBXML valueOfAttributeNamed:@"size" forElement:font] floatValue];
    //rough conversion to make it smaller and in line with AnyCharts chart labels size
    size *= FONT_MULTIPLICATION_FACTOR;
    size = ceil(size);
    
    NSString *color = [TBXML valueOfAttributeNamed:@"color" forElement:font];
    UIColor *textColor = [self parseRgbColor:color];
    
    if (isAxisX) {
        [Log debug:@"x_axis font = %@ size = %f", family, size];
        ordinal.font = family; //family;
        ordinal.color = textColor;
        ordinal.fontSize = size;
    }else {
        [Log debug:@"y_axis font = %@ size = %@", family, size];
        y.font = family;
        y.color = textColor;
        y.fontSize = size;
    }
}

- (NSString *)titleText:(TBXMLElement *)child target:(NSString*)t{
	TBXMLElement *xTitle = [Util traverseFindElement:child target:@"title"];
	if (xTitle){
		TBXMLElement *titleVal = [Util traverseFindElement:xTitle target:@"text"];
		if (titleVal) {
			return [TBXML textForElement:titleVal];
		}
	}
	return nil;
}

- (UIColor*) parseRgbColor:(NSString*)str{
	if (str) {
		NSCharacterSet *skips = [NSCharacterSet characterSetWithCharactersInString:@"RGB(,gb"];
		NSScanner *scanner = [NSScanner scannerWithString:str];
		[scanner setCharactersToBeSkipped:skips];
		int rgb1 = 0;
		[scanner scanInt:&rgb1];
		int rgb2 = 0;
		[scanner scanInt:&rgb2];
		int rgb3 = 0;
		[scanner scanInt:&rgb3];
		[Log debug:@"parseRgbColor = %d %d %d", rgb1, rgb2, rgb3];
		
		return [UIColor colorWithRed:(rgb1/255.0) green:(rgb2/255.0) blue:(rgb3/255.0) alpha:1.0];
	}
	return [UIColor blackColor];
}

-(NSArray *) startEndGradientColor:(NSString*)str percentDiff:(float)percentDiff secondAlpha:(float)secondAlpha{
    NSArray *colors = nil;
    if (str) {
		NSCharacterSet *skips = [NSCharacterSet characterSetWithCharactersInString:@"RGB(,gb"];
		NSScanner *scanner = [NSScanner scannerWithString:str];
		[scanner setCharactersToBeSkipped:skips];
		int rgb1 = 0;
		[scanner scanInt:&rgb1];
		int rgb2 = 0;
		[scanner scanInt:&rgb2];
		int rgb3 = 0;
		[scanner scanInt:&rgb3];
		[Log debug:@"parseRgbColor = %d %d %d", rgb1, rgb2, rgb3];
		
		UIColor *start = [UIColor colorWithRed:(rgb1/255.0) green:(rgb2/255.0) blue:(rgb3/255.0) alpha:1.0];
        
        //A percentage of the first
        rgb1 *= percentDiff;
        rgb2 *= percentDiff;
        rgb3 *= percentDiff;
        
        UIColor *end = [UIColor colorWithRed:(rgb1/255.0) green:(rgb2/255.0) blue:(rgb3/255.0) alpha:secondAlpha];
        
        colors = [NSArray arrayWithObjects:start, end, nil];
        
	}
    
    return colors;
}

@end
