//
//  LeftSidebarViewController.m
//  JTRevealSidebarDemo
//
//  Created by James Apple Tang on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SidebarViewController.h"
#import "Settings.h"
#import <QuartzCore/QuartzCore.h>
#import "Space.h"
#import "Util.h"

@implementation SidebarViewController

@synthesize searchBar, sidebarDelegate, spaces;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    

    
    self.tableView.opaque = NO;
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [Util colorWithRGBHex:0x2B2C30];
    
    /*
    self.searchBar = [[[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 45)] autorelease];
    self.searchBar.barStyle = UIBarStyleBlack;
    self.searchBar.backgroundColor = [Util colorWithRGBHex:0x2B2C30];
    
    self.tableView.tableHeaderView = self.searchBar;
     */
    
    [self setLastSelectedIndexPath];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setLastSelectedIndexPath];
}

- (void)setLastSelectedIndexPath{
    if ([self.sidebarDelegate respondsToSelector:@selector(lastSelectedIndexPathForSidebarViewController:)]) {
        NSIndexPath *indexPath = [self.sidebarDelegate lastSelectedIndexPathForSidebarViewController:self];
        if(indexPath != nil){
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.searchBar = nil;
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case IDX_HDR_MAIN:
            return 5;
            break;
        case IDX_HDR_SPACE:
            if ([Settings loggedIn]){
                return [self.spaces count];
            }else{
                if([self.spaces count] > 0){
                    [spaces removeAllObjects];
                }
                return 1;
            }
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
    }
    
    cell.imageView.image = nil;
    
    NSString *rowTitle = @"";
    switch (indexPath.section) {
        case IDX_HDR_MAIN:
            switch (indexPath.row) {
                case IDX_ABOUT:
                    rowTitle = @"About";
                    cell.imageView.image = [UIImage imageNamed:@"aboutus.PNG"];
                    break;
                case IDX_LOGIN:
                    rowTitle = @"Login";
                    cell.imageView.image = [UIImage imageNamed:@"login_icon.png"];
                    break;
                case IDX_OFFLINE:
                    rowTitle = @"Offline Pages";
                    cell.imageView.image = [UIImage imageNamed:@"OfflinePages.PNG"];
                    break;
                case IDX_DEMO:
                    rowTitle = @"Demo";
                    cell.imageView.image = [UIImage imageNamed:@"demo.PNG"];
                    break;
                case IDX_SUPPORT:
                    rowTitle = @"Support";
                    cell.imageView.image = [UIImage imageNamed:@"support.PNG"];
                    break;
                default:
                    break;
            }
            break;
        case IDX_HDR_SPACE:
            if([Settings loggedIn] && [self.spaces count] > 0){
                Space *sp = [self.spaces objectAtIndex:indexPath.row];
                rowTitle = [Util xmlUnescape:sp.name];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }else{
                rowTitle = @"Load spaces";
                cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
            }
        default:
            break;
    }
    
    cell.textLabel.font = [cell.textLabel.font fontWithSize:16];
    cell.textLabel.text = rowTitle;
    cell.backgroundColor = [Util colorWithRGBHex:0xF7F7F7];
    
    UIView *bg = [[UIView alloc]initWithFrame:cell.frame];
    bg.backgroundColor = [UIColor blackColor];
    [bg release];
     
    return cell;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    NSString *text = @"";
    
    switch (section) {
        case IDX_HDR_MAIN:
            text = @"Options";
            break;
        case IDX_HDR_SPACE:
            text = @"Spaces";
            break;
        default:
            text = @"";
    }
    
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 10, tableView.bounds.size.width - 10, 18)] autorelease];
    label.text = text;
    label.font = [UIFont boldSystemFontOfSize:17];
    label.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.75];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];

    return headerView;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case IDX_HDR_MAIN:
            return @"Options";
            break;
        case IDX_HDR_SPACE:
            return @"Spaces";
            break;
        default:
            return @"";
            break;
    }
}

-(NSUInteger)supportedInterfaceOrientations{
    return  UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft;
}

- (BOOL)shouldAutorotate {
    
    UIInterfaceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if (orientation==UIInterfaceOrientationPortrait) {
        // do some sh!t
    }
    
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.sidebarDelegate) {
        NSObject *object = [NSString stringWithFormat:@"ViewController%d", indexPath.row];
        [self.sidebarDelegate sidebarViewController:self didSelectObject:object atIndexPath:indexPath];
    }
}

@end
