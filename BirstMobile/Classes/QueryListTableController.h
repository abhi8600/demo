//
//  QueryListTableController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prompt.h"
#import "PageRenderViewController.h"
#import "SingleSelectListPickerController.h"

@class  SingleSelectListPickerController;

@interface QueryListTableController : UITableViewController {
	Prompt *prompt;
	NSMutableSet *selects;
    BOOL renderedInDashlet;
    PageRenderViewController *pgCtrl;
    SingleSelectListPickerController *sslCtrl;
}

@property (assign) BOOL renderedInDashlet;
@property (nonatomic, assign) Prompt *prompt;
@property (nonatomic, retain) NSMutableSet *selects;
@property (nonatomic, assign) PageRenderViewController *pgCtrl;
@property (nonatomic, assign) SingleSelectListPickerController *sslCtrl;

- (NSMutableArray*)getSelectedValues;
- (void) inDashletApply;
- (PromptValues *) selectedHasValue:(NSString *)value;

@end
