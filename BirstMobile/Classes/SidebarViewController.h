//
//  LeftSidebarViewController.h
//  JTRevealSidebarDemo
//
//  Created by James Apple Tang on 12/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//Table header index
#define IDX_HDR_MAIN    0
#define IDX_HDR_SPACE   1

//Table row index
#define IDX_LOGIN   0
#define IDX_OFFLINE 1
#define IDX_DEMO    2
#define IDX_SUPPORT 3
#define IDX_ABOUT   4

@protocol SidebarViewControllerDelegate;

@interface SidebarViewController : UITableViewController{
    NSMutableArray *spaces;
}

@property (nonatomic, assign) id <SidebarViewControllerDelegate> sidebarDelegate;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) NSMutableArray *spaces;

- (void)setLastSelectedIndexPath;

@end

@protocol SidebarViewControllerDelegate <NSObject>

- (void)sidebarViewController:(SidebarViewController *)sidebarViewController didSelectObject:(NSObject *)object atIndexPath:(NSIndexPath *)indexPath;

@optional
- (NSIndexPath *)lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController;

@end
