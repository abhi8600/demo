//
//  SpacesTableViewController.h
//  Birst
//
//  Created by Seeneng Foo on 3/3/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Space.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "DashboardsViewController.h"
#import "RootViewController.h"
#import "DetailViewController.h"
#import "SpaceDetailsView.h"
#import "Util.h"
#import "RequestPendingIndicatorController.h"

@class RootViewController;
@class SpaceDetailsView;
@class  DashboardsViewController;

@interface SpacesTableViewController : UITableViewController {
	NSMutableArray *spaces;
	RootViewController *rootCtrl;
	DetailViewController *detailCtrl;
	SpaceDetailsView *spaceDetailCtrl;
    UIActivityIndicatorView *activity;
    DashboardsViewController *dashsCtrl;
    RequestPendingIndicatorController *reqPendingCtrl;
    ASINetworkQueue *networkQueue;
    
    //Selected Space details
    NSString *targetSpaceName;
    NSString *targetSpaceId;
}

@property (readonly, retain) NSMutableArray* spaces;
@property (nonatomic, assign) RootViewController *rootCtrl;
@property (nonatomic, assign) DetailViewController *detailCtrl;
@property (nonatomic, retain) SpaceDetailsView *spaceDetailCtrl;
@property (nonatomic, retain) UIActivityIndicatorView *activity;
@property (nonatomic, retain) DashboardsViewController *dashsCtrl;
@property (nonatomic, retain) RequestPendingIndicatorController *reqPendingCtrl;
@property (nonatomic, retain) ASINetworkQueue *networkQueue;

@property (nonatomic, retain) NSString *targetSpaceName;
@property (nonatomic, retain) NSString *targetSpaceId;

- (void)parse:(NSString*)xml;
- (void)parseSpaceDetails:(NSString *)xml;
- (void)onGetSpaceDetailsFinished:(ASIHTTPRequest *)request;
- (void)onGetDashboardListFinished:(ASIHTTPRequest *)request;
- (void)startAnimateRequest;
- (void)stopAnimateRequest;
- (void)onRequestFinished:(ASIHTTPRequest*)request;
- (void)onGetDashboardModFinished:(ASIHTTPRequest *)request;
@end
