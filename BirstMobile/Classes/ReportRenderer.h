//
//  ReportRenderer.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/15/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBXML.h"
#import "DashletController.h"
#import "UIWebViewWrapper.h"

@class DashletController;

typedef enum{
    ColumnHeader,
    RowHeader,
    CrosstabHeader,
    Data,
    None
}PivotTextTagType;

@interface ReportRenderer : UIScrollView <UITextFieldDelegate>{
	NSString *data;
    TBXML *tbxml;
    CGSize reportDimension;
    DashletController *dashletCtrl;
    int numPages;
    int pageNo;
    NSMutableArray *childViews;
    NSMutableDictionary *editableTextBoxes;
    NSMutableDictionary *checkBoxes;
    int numCharts;
    int renderedJSCount;
    
    float pinchScaleStart;
    
    BOOL disableDrilling;
}

@property (nonatomic, retain) NSString *data;
@property (nonatomic, retain) TBXML *tbxml;
@property (nonatomic, assign) DashletController *dashletCtrl; 
@property (nonatomic, assign) int pageNo;
@property (nonatomic, assign) int numPages;
@property (nonatomic, assign) int numCharts;
@property (nonatomic, retain) NSMutableArray *childViews;
@property (nonatomic, retain) NSMutableDictionary *editableTextBoxes;
@property (nonatomic, retain) NSMutableDictionary *checkBoxes;

@property (assign) BOOL disableDrilling;

- (void) parse:(NSString *)xml;
- (void) renderChildren:(TBXMLElement *)i xOrigin:(float)x yOrigin:(float)y pivotTag:(PivotTextTagType)pivotTag;
- (CGRect) renderText:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO parent:(TBXMLElement*)parent pivotTag:(PivotTextTagType)pivotTag;
- (void) renderFrame:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) renderChart:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) renderImage:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) renderEditableTextBox:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) renderCheckBox:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) renderButton:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO;
- (CGRect) createFrame:(TBXMLElement *)i xOrigin:(float)xO yOrigin:(float)yO parentHeight:(float)parentHeight parentWidth:(float)parentWidth;
- (void) handleTap:(UITapGestureRecognizer*)tapEvent;
- (void)handlePinch:(UIPinchGestureRecognizer *)pinch;
- (NSString*) tempFixDrillAttributes:(NSString *)xml;
- (void) addChild:(UIView*)v;
- (PivotTextTagType) getCellTagType:(TBXMLElement *)el;
- (void) maximizeSingleChart:(CGRect)rect;
- (void) disableAllDrilling:(BOOL)disableDrill;
- (void) renderPagingSection;

- (void)textFieldDidBeginEditing:(UITextField *)textField;
- (void)textFieldDidEndEditing:(UITextField *)textField;


@end
