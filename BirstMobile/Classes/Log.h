//
//  Log.h
//  BirstMobile
//
//  Created by Seeneng Foo on 9/16/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum{
    Debug,
    Info,
    Warn,
    Error
}LogLevel;

@interface Log : NSObject {
    
}

+ (void) setLogLevel:(LogLevel)level;
+ (LogLevel) logLevel;

+ (void) debug:(NSString *)format, ...;
+ (void) warn:(NSString *)format, ...;

@end
