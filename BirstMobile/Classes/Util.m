//
//  Util.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Util.h"
#import <QuartzCore/QuartzCore.h> //To remove renderInContext() warning
#import "Notification.h"

@implementation Util

// Serialize the anychart xml to a string we'd use later for 
// JS execution
+ (NSString *) toXmlString:(TBXMLElement *)xml output:(NSMutableString *)output
{
    if(xml != nil){
        NSString *tag = [TBXML elementName:xml];
               
        TBXMLAttribute *attr = xml->firstAttribute;
        if(attr != nil){
            [output appendFormat:@"<%@ ", tag];
            do{
                NSString *attrName = [NSString stringWithCString:attr->name encoding:NSUTF8StringEncoding];
                NSMutableString *attrValue = [NSMutableString stringWithCString:attr->value encoding:NSUTF8StringEncoding];
                [output appendFormat:@"%@=\"%@\" ", attrName, attrValue];
            }while( (attr = attr->next) );
            [output appendString:@">"];
        }else{
            [output appendFormat:@"<%@>", tag];
        }
        
        NSMutableString *text = [NSMutableString stringWithString:[TBXML textForElement:xml]];
        [output appendString:text];
        
        if(xml->firstChild){
            [Util toXmlString:xml->firstChild output:output];
        }
        [output appendFormat:@"</%@>", tag];
        
        if(xml->nextSibling){
            [Util toXmlString:xml->nextSibling output:output];
        }
        
    }
    
    return output;
}

+ (TBXMLElement*) traverseFindElement:(TBXMLElement *)element target:(NSString*)ourTarget {
    if(element){
        do {
            if ([ourTarget isEqualToString:[TBXML elementName:element]]) {
                return element;
            }
		
            // if the element has child elements, process them
            if (element->firstChild){
                TBXMLElement *found = [self traverseFindElement:element->firstChild target:ourTarget];
                if (found != nil) {
                    return found;
                }
            }
            // Obtain next sibling element
		
        } while ((element = element->nextSibling)); 
	}
	return nil;
}

+ (TBXMLElement *) findSibling:(TBXMLElement *)element target:(NSString *)ourTarget{
    if(element){
        for(TBXMLElement *sibling = element->nextSibling; sibling != nil; sibling = sibling->nextSibling){
            if([ourTarget isEqualToString:[TBXML elementName:sibling]]){
                return sibling;
            }
        }
    }
	return nil;
}

+ (void) requestFailedAlert:(NSError *)error{
    NSString *errMsg = [error localizedDescription];
    if([errMsg isEqualToString:@"Authentication needed"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:SESSION_TIMEOUT object:nil];
        [self alert:@"The session has timed out. Please login again" title:@"Session timed out"];
    }else{
        [self alert:errMsg title:@"Request failed"];
    }
}

+ (void) alert:(NSString*)message title:(NSString*)title{
	UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

+ (void) popupRequestIndicator:(UIViewController *)caller message:(NSString *)msg{
	RequestPendingIndicatorController *renderCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
	renderCtrl.msg.text = msg;
	renderCtrl.modalPresentationStyle = UIModalPresentationFullScreen;
	[caller presentModalViewController:renderCtrl animated:YES];	
}

+ (BOOL) orientationLockLandscape:(UIInterfaceOrientation)interfaceOrientation{
	 return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

+ (NSMutableString *)xmlSimpleEscape:(NSMutableString*)str {
    /*
	[str replaceOccurrencesOfString:@"&"  withString:@"&amp;"  options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@"'"  withString:@"&#x27;" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@">"  withString:@"&gt;"   options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@"<"  withString:@"&lt;"   options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	*/
    [str replaceOccurrencesOfString:@"&"  withString:@"&amp;"  options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@"'"  withString:@"&apos;" options:
        NSLiteralSearch range:NSMakeRange(0, [str length])];
    [str replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@">"  withString:@"&gt;"   options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	[str replaceOccurrencesOfString:@"<"  withString:@"&lt;"   options:NSLiteralSearch range:NSMakeRange(0, [str length])];
	return str;
}

+ (NSString*) xmlUnescape:(NSString*)str{
    if(str){
        return [Util xmlSimpleUnescape:[NSMutableString stringWithString:str]];
    }
    
    return str;
}

+ (NSMutableString *)xmlSimpleUnescape:(NSMutableString*)str{
    [str replaceOccurrencesOfString:@"&lt;" withString:@"<" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];
    [str replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];
	[str replaceOccurrencesOfString:@"&quot;" withString:@"\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];
    [str replaceOccurrencesOfString:@"&apos;" withString:@"'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];
    [str replaceOccurrencesOfString:@"&amp;" withString:@"&" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];   
    [str replaceOccurrencesOfString:@"&#xA;" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [str length]) ];
    return str;
}


+ (void) startPendingRequest:(NSString *)message sender:(id)snd{
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center postNotificationName:@"startRequest" object:snd userInfo:[NSDictionary dictionaryWithObject:message forKey:@"message"]];
}

+ (void) endPendingRequest:(NSString *)message sender:(id)snd{
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center postNotificationName:@"endRequest" object:snd userInfo:[NSDictionary dictionaryWithObject:message forKey:@"message"]];
}

+ (void) removeAllSubviews:(UIView *)parentView{
	NSArray* views = [parentView subviews];
	for (int i = [views count] - 1; i >= 0; i--) {
		UIView *subView = [views objectAtIndex:i];
		[subView removeFromSuperview];
	}
}

+ (void) setDict:(NSMutableDictionary*)dict key:(id)key value:(id)value{
	if (key && value) {
		[dict setObject:value forKey:key];
	}
}

+ (NSString*) getResponse:(ASIHTTPRequest *)request{
    NSString *response = [request responseString];
    if (!response) {
        //Try ascii encoding, seems that our utf-8 string is invalid
        NSData *data = [request responseData];
        response = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        return [response autorelease];
    }else{
        return response;
    }
    
}

+ (NSString *)xmlBlock:(NSString *)response tag:(NSString*)tag{
    //Now find the subtring for the report xml
    NSString *startTag = [NSString stringWithFormat:@"<%@>", tag];
    NSString *endTag = [NSString stringWithFormat:@"</%@>", tag];
    NSRange head = [response rangeOfString:startTag];
    if(head.length > 0){
        NSRange tail = [response rangeOfString:endTag];
        if (tail.length > 0 && tail.location > head.location) {
            return [response substringWithRange:NSMakeRange(head.location, (tail.location + [endTag length]) - head.location)];
        }
    }
    return nil;
}


+ (UIColor*) parseRgbColor:(NSString*)str{
	if (str) {
		NSCharacterSet *skips = [NSCharacterSet characterSetWithCharactersInString:@"RGB(,gb"];
		NSScanner *scanner = [NSScanner scannerWithString:str];
		[scanner setCharactersToBeSkipped:skips];
		int rgb1 = 0;
		[scanner scanInt:&rgb1];
		int rgb2 = 0;
		[scanner scanInt:&rgb2];
		int rgb3 = 0;
		[scanner scanInt:&rgb3];
		NSLog(@"parseRgbColor = %d %d %d", rgb1, rgb2, rgb3);
		
		return [UIColor colorWithRed:(rgb1/255.0) green:(rgb2/255.0) blue:(rgb3/255.0) alpha:1.0];
	}
	return [UIColor blackColor];
}

+ (UIColor *)colorWithRGBHex:(UInt32)hex {
	int r = (hex >> 16) & 0xFF;
	int g = (hex >> 8) & 0xFF;
	int b = (hex) & 0xFF;
    
	return [UIColor colorWithRed:r / 255.0f
						   green:g / 255.0f
							blue:b / 255.0f
						   alpha:1.0f];
}

// Returns a UIColor by scanning the string for a hex number and passing that to +[UIColor colorWithRGBHex:]
// Skips any leading whitespace and ignores any trailing characters
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
	NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
	unsigned hexNum;
	if (![scanner scanHexInt:&hexNum]) return nil;
	return [Util colorWithRGBHex:hexNum];
}

+ (void) screenShot:(UIView *)uiview loc:(NSString *)loc{
    //Grab image of the dashlet
    UIGraphicsBeginImageContext(uiview.bounds.size);
    [uiview.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imgData = UIImagePNGRepresentation(image);
    NSError *err;
    if(![imgData writeToFile:loc options:NSDataWritingAtomic error:&err]){
        [Log debug:@"Image write to directory failed reason: %@", [err localizedDescription]];
    }
}

+ (NSString *) checkForError:(NSString *)str{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<ErrorCode>(\\d+)</ErrorCode><ErrorMessage>(.*)</ErrorMessage>" options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *result = [regex firstMatchInString:str options:0 range:NSMakeRange(0, [str length])];
    
    if(result){
        NSString *v = [str substringWithRange:[result rangeAtIndex:1]];
        int code = [v intValue];
        if(code != 0){
            NSString *msg = [str substringWithRange:[result rangeAtIndex:2]];
            return (msg != nil && [msg length] > 0) ? msg : v;
        }
    }
   
    return nil;
}

+ (UIColor *) inverseColor:(UIColor *)clr{
    
    CGColorRef oldCGColor = clr.CGColor;
    
    int numberOfComponents = CGColorGetNumberOfComponents(oldCGColor);
    
    // can not invert - the only component is the alpha
    // e.g. self == [UIColor groupTableViewBackgroundColor]
    if (numberOfComponents == 1) {
        return [UIColor colorWithCGColor:oldCGColor];
    }
    
    const CGFloat *oldComponentColors = CGColorGetComponents(oldCGColor);
    CGFloat newComponentColors[numberOfComponents];
    
    int i = numberOfComponents - 1;
    newComponentColors[i] = oldComponentColors[i]; // alpha
    while (--i >= 0) {
        newComponentColors[i] = 1 - oldComponentColors[i];
    }
    
    CGColorRef newCGColor = CGColorCreate(CGColorGetColorSpace(oldCGColor), newComponentColors);
    UIColor *newColor = [UIColor colorWithCGColor:newCGColor];
    CGColorRelease(newCGColor);
    
    return newColor;
}

+ (NSString *) hexFromUIColor:(UIColor *)_color {
    if(!_color){
        return nil;
    }
    
    if (CGColorGetNumberOfComponents(_color.CGColor) < 4) {
        const CGFloat *components = CGColorGetComponents(_color.CGColor);
        _color = [UIColor colorWithRed:components[0] green:components[0] blue:components[0] alpha:components[1]];
    }
    if (CGColorSpaceGetModel(CGColorGetColorSpace(_color.CGColor)) != kCGColorSpaceModelRGB) {
        return [NSString stringWithFormat:@"FFFFFF"];
    }
    return [NSString stringWithFormat:@"%02X%02X%02X", (int)((CGColorGetComponents(_color.CGColor))[0]*255.0), (int)((CGColorGetComponents(_color.CGColor))[1]*255.0), (int)((CGColorGetComponents(_color.CGColor))[2]*255.0)];
}

@end
