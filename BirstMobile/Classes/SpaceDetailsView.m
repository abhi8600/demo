    //
//  SpaceDetailsView.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//


#import "SpaceDetailsView.h"


/**
 * This view is currently NOT in use
 */
 
 
@implementation SpaceDetailsView

@synthesize tName, tOwner, tSize, tLastProcessed, tPermissions, spacesCtrl, rootCtrl, detailCtrl;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (IBAction) getDashboards{
	NSLog(@"getDashboards");
	if (self.spacesCtrl != nil) {
		//[self.spacesCtrl getDashboards];
	}
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	name.text = tName;
	owner.text = tOwner;
	size.text = tSize;
	lastProcessed.text = tLastProcessed;
	permissions.text = tPermission;
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    if ([self respondsToSelector:@selector(extendedLayoutIncludesOpaqueBars)])
        self.extendedLayoutIncludesOpaqueBars = NO;
    
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
