//
//  OfflinePagesViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/4/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKCoverflowCoverView.h"
#import "TKCoverflowView.h"
#import "OfflineOptionsViewController.h"
#import "Page.h"
#import "MainViewController.h"

@interface OfflinePagesViewController :MainViewController <TKCoverflowViewDelegate,TKCoverflowViewDataSource,UIScrollViewDelegate> {
	NSMutableArray *covers;
	NSMutableArray *info;
	TKCoverflowView *coverflow;
	UITabBar *tabBar;
	UILabel *description;
    UIPopoverController *options;
	Page *displayPage;
}

@property (nonatomic, retain) UITabBar *tabBar;
@property (nonatomic, retain) UILabel *description;
@property (nonatomic, retain) UIPopoverController *options;
@property (nonatomic, retain) Page *displayPage;

- (void) dismissPopup;
- (void) rightButtonSelected:(id)sender;
- (void) renderCoverflow;
- (void) deleteAll;
@end
