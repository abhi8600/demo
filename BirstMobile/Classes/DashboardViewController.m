//
//  DemoDashboardViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 6/29/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "DashboardViewController.h"
#import "Settings.h"
#import "BirstWS.h"
#import "Util.h"
#import "Dashboard.h"
#import "GlobalPromptValues.h"
#import "Notification.h"
#import "MaximizedDashletViewController.h"

@implementation DashboardViewController

@synthesize isDemoDashboard;
@synthesize selectedIndex, pageSelectedIndex, dashboards, spaceId, spaceName, invisibleDashboards;
@synthesize pageRenderViewController, popOverViewController, pagesPopOver;
@synthesize renderedDashboard, renderedPage;
@synthesize dashboardsCoverFlowController, backgroundColor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) dealloc
{
    NSLog(@"DashboardViewController dealloc");
    
    [spaceId release];
    [spaceName release];
    [dashboards release];
    [invisibleDashboards release];
    [pageRenderViewController release];
    [dashboardsCoverFlowController release];
    
    if([popOverViewController isPopoverVisible]){
        [popOverViewController dismissPopoverAnimated:NO];
    }
    [popOverViewController release];
    
    [renderedDashboard release];
    [renderedPage release];
    [pagesPopOver release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Right Bar Buttons
    UIBarButtonItem *promptsBttn = [[[UIBarButtonItem alloc] initWithTitle:@"Prompts" style:UIBarButtonItemStyleBordered target:self action:@selector(showPromptsPopOver)] autorelease];
	UIBarButtonItem *savePageBttn = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(saveOffline)] autorelease];
    UIBarButtonItem *refreshBttn = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshPage)] autorelease];
    self.navigationItem.rightBarButtonItems = [[[NSArray alloc] initWithObjects:refreshBttn, savePageBttn, promptsBttn, nil] autorelease];
    
    
    //Left Bar buttons
    UIBarButtonItem *pagesBttn = [[UIBarButtonItem alloc] initWithTitle:@"Dashboards" style:UIBarButtonItemStyleBordered target:self action:@selector(showPagesPopOver)];
    NSArray *leftBttns = [[NSArray alloc] initWithObjects:self.navigationItem.leftBarButtonItem, pagesBttn, nil];
    [pagesBttn release];
    
    self.navigationItem.leftBarButtonItems = leftBttns;
    [leftBttns release];
    
    //Network queue which handles all our single http requests
    //Set to force only two requests at a time
    self.networkQueue = [BirstWS createNetworkQueue:1 delegate:self finishSel:@selector(onRequestFinished:) failSel:@selector(onRequestFailed:)];
    
    NSNotificationCenter *ctr = [NSNotificationCenter defaultCenter];
    
    //To be notified of drill to dashboard events
    [ctr addObserver:self selector:@selector(onDrillToDashboard:) name:DRILL2DASHBOARD object:nil];
    [ctr addObserver:self selector:@selector(onMaximizeDashlet:) name:MAXIMIZE_DASHLET object:nil];
    [ctr addObserver:self selector:@selector(applyPrompts) name:PROMPT_APPLY object:nil];
    
    if(self.isDemoDashboard){
        NSString *demoUrl = [Settings valueForKey:@"demoSpaceUrl"];
        NSString *demoServer = [Settings valueForKey:@"demoServer"];
        if([demoUrl length] > 0 && [demoServer length] > 0){
            //[self.detailViewController startAnimateRequest:@"Logging in as demo user.."];
            [Settings setLoginMode:demo];      
            [Settings setServer:demoServer];
            NSURL *url = [NSURL URLWithString:demoUrl];
            
            [self showPendingRequest:@"Retrieving demo dashboard..."];
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            request.tag = DemoDashboard;
            [self.networkQueue addOperation:request];
        }
    }else{
        [Settings setLoggedIn:mainDashboard];
        NSURL *url = [Settings getDashboardModulePageUrl];
        
        [self showPendingRequest:@"Retrieving dashboards and pages..."];
        ASIHTTPRequest *dbrequest = [ASIHTTPRequest requestWithURL:url];
        [Settings addCSRFHeader:dbrequest];
        dbrequest.tag = DashboardModule;
        [self.networkQueue addOperation:dbrequest];
    }
    
    [self.leftSidebarViewController setLastSelectedIndexPath];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated{
    }

#pragma mark - Dashboard requests and callbacks

- (void) onRequestFinished:(ASIHTTPRequest*)request {
    [self removePendingRequest];
    
    switch (request.tag) {
        case RQ_DemoDashboard:
            [self onGetDashboardModule:request];
            break;
        case RQ_DashboardModule:
            [self onGetDashboardModule:request];
            break;
        case RQ_DashboardList:
            [self onGetDashboardList:request];
            break;
        case RQ_DashboardSettings:
            [self onGetDashboardSettings:request];
            break;
        default:
            break;
    }
}

- (void)onRequestFailed:(ASIHTTPRequest *)request {
    //NSString *response = [request responseString];
    [self removePendingRequest];
    [Util requestFailedAlert:[request error]];
}

-(void) onGetDashboardModule:(ASIHTTPRequest *)request{
    
    //Figure out if we're using a 5.0 version, hence we can use optimized
    //getDashbardListFromDB
    NSString *response = [Util getResponse:request];
    NSRange range = [response rangeOfString:@"smiroot=&v=4"];
    NSString *op = (range.length > 0) ? @"getDashboardListCached" : @"getDashbardListFromDB";
    
    //[self.detailViewController startAnimateRequest:@"Retrieving dashboards.."];
    
    [self showPendingRequest:@"Retrieving dashboards and pages..."];
    
    //Dashboard settings first
    ASIHTTPRequest *spRequest = [BirstWS requestWithClientInit:@"getDashboardSettings"];
    spRequest.tag = RQ_DashboardSettings;
    [BirstWS requestFormat:spRequest body:@""];
    [self.networkQueue addOperation:spRequest]; 
    
    //Get the dashboard list
    ASIHTTPRequest *dbRequest = [BirstWS requestWithDashboard:op];
    dbRequest.tag = RQ_DashboardList;
    [BirstWS requestFormat:dbRequest body:@""];
    [self.networkQueue addOperation:dbRequest];
}

- (void)onGetDashboardSettings:(ASIHTTPRequest *)request{
    NSString *response = [request responseString];
    TBXML *tbxml = [[TBXML alloc] initWithXMLString:response];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *errCode = [Util traverseFindElement:root target:@"ErrorCode"];
    if(errCode){
        NSString *errCodeStr = [TBXML textForElement:errCode];
        if([errCodeStr isEqualToString:@"0"]){
            TBXMLElement *bgColor = [Util traverseFindElement:root target:@"backgroundColor1"];
            if(bgColor != nil){
                NSString *bgColorStr = [TBXML textForElement:bgColor];
                self.backgroundColor = [Util colorWithHexString:bgColorStr];
                NSLog(@"backgroundColor = %@", self.backgroundColor);
            }else{
                //Default background color
                self.backgroundColor = [UIColor whiteColor];
            }
            
        }else{
            //TODO: alert error
        }
    }
    [tbxml release];
}

-(void) onGetDashboardList:(ASIHTTPRequest *)request {
    NSString *response = [request responseString];
    
    //Format
    NSString *mStr = [Util xmlUnescape:response];
    
    NSString *error = [Util checkForError:mStr];
    if(error != nil){
        [Util alert:[NSString stringWithFormat:@"Failed to retrieve dashboards & pages: %@", error] title:@"Request Failed"];
        return;
    }
    
    [self parseDashboardList:mStr];
}

-(void) parseDashboardList:(NSString *)xml{
	TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
	TBXMLElement *root = tbxml.rootXMLElement;
	if(root != nil){
		TBXMLElement *dash = [Util traverseFindElement:root target:@"Dashboard"];
		if (dash != nil) {
            
			[self initClearDashboards];
            
            //Clear out the global prompt values
            [[GlobalPromptValues instance]reset];
            
			do {
				Dashboard *d = [[Dashboard alloc]init];
				[d parse:dash];
                if(d.invisible){
                    [self.invisibleDashboards addObject:d];
                }else{
                    [self.dashboards addObject:d];
                }
                [d release];
			} while ((dash = dash->nextSibling));
            
			//Select the first row
			if ([self.dashboards count] > 0) {
                //Only do screenshots on non demo mode
                BOOL doScreenShot = ![Settings demoMode];
				self.renderedDashboard = [self.dashboards objectAtIndex:0];
                if ([self.renderedDashboard.pages count] > 0){
                    self.renderedPage = [self.renderedDashboard.pages objectAtIndex:0];
                    [self displayPage:self.renderedDashboard page:self.renderedPage screenshot:doScreenShot isSpaceScreenshot:doScreenShot];
                }
			}
		}
	}
    [tbxml release];
}

- (void) onMaximizeDashlet:(NSNotification *)notification{
    ReportRenderer* renderer = notification.object;
    
    NSLog(@"onMaximizedDashlet");
    MaximizedDashletViewController *maxVc = [[MaximizedDashletViewController alloc] init];
    maxVc.renderer = renderer;
    maxVc.title = [self.pageRenderViewController titleWithDashlet:renderer.dashletCtrl.dashlet];
    
    if (self.backgroundColor){
        maxVc.view.backgroundColor = self.backgroundColor;
    }
    
    [self.navigationController pushViewController:maxVc animated:YES];
    [maxVc release];
    
    
}

- (void) onDrillToDashboard:(NSNotification *)notification{
    DrillAttributes *drillAttrs = [[notification userInfo] objectForKey:@"drillAttributes"];
    
    NSString *dashName = nil;
    NSString *pageName = nil;
    
    NSString *dashboard = drillAttrs.targetURL;
    if (dashboard) {
        NSArray *dashPage = [dashboard componentsSeparatedByString:@"_"];
        if([dashPage count] == 1){
            dashPage = [dashboard componentsSeparatedByString:@","];
        }
        if ([dashPage count] == 2) {
            dashName = [dashPage objectAtIndex:0];
            pageName = [dashPage objectAtIndex:1];
            
        }
    }else{
        dashName = drillAttrs.drillToDashboard;
        pageName = drillAttrs.drillToPage;
    }
    
    if(dashName && pageName){
        //Search in regular dashboards
        for (int i = 0; i < [self.dashboards count]; i++) {
            Dashboard *dash = [self.dashboards objectAtIndex:i];
            if ([dash.name isEqualToString:dashName]) {
                for(int y = 0; y < [dash.pages count]; y++){
                    Page *p = [dash.pages objectAtIndex:y];
                    if([p.name isEqualToString:pageName]){
                        p.drillAttributes = drillAttrs;
                        [self displayPage:dash page:p screenshot:NO isSpaceScreenshot:NO];
                    }
                }
                return;
            }
        }
        
        //Search in the invisibles
        for(int i = 0; i < [self.invisibleDashboards count]; i++){ 
            Dashboard *dash = [self.invisibleDashboards objectAtIndex:i];
            if ([dash.name isEqualToString:dashName]) {
                for(int y = 0; y < [dash.pages count]; y++){
                    Page *p = [dash.pages objectAtIndex:y];
                    if([p.name isEqualToString:pageName]){
                        p.drillAttributes = drillAttrs;
                        [self displayPage:dash page:p screenshot:NO isSpaceScreenshot:NO];
                    }
                }
                return;
            }
        }
    }
}

- (void)displayPage:(Dashboard*)dashboard page:(Page *)page screenshot:(BOOL)screenshot isSpaceScreenshot:(BOOL)isSpaceScreenshot{
    
    //Get full page information if required
    if(page.isPartial){
        NSString *body = 
        [NSString stringWithFormat:@"<ns:uuid>%@</ns:uuid><ns:pagePath>%@</ns:pagePath>", page.uuid, [page getPath]];
        
        __block ASIHTTPRequest* req = [BirstWS requestWithDashboard:@"getPage"];
        [BirstWS requestNSDashFormat:req body:body];
        
        //Success, parse the full page xml
        [req setCompletionBlock:^{
            NSString *pageXml = [Util getResponse:req];
            TBXML *tbxml = [TBXML tbxmlWithXMLString:pageXml];
            TBXMLElement *root = tbxml.rootXMLElement;
            TBXMLElement *pageEl = [Util traverseFindElement:root target:@"Page"];
            if(pageEl){
                [page parse:pageEl];
            }
        }];
        
        [req setFailedBlock:^{
            [Util requestFailedAlert:[req error]];
        }];
        
        [req startSynchronous];
    }
    
    if(self.renderedDashboard != dashboard){
        self.renderedDashboard = dashboard;
    }
    if(self.renderedPage != page){
        self.renderedPage = page;
    }
    
    if(pageRenderViewController != nil){
        [pageRenderViewController.view removeFromSuperview];
        [pageRenderViewController release];
        pageRenderViewController = nil;
    }
    
    self.pageRenderViewController = [[[PageRenderViewController alloc]init] autorelease];
    self.pageRenderViewController.page = self.renderedPage;
    self.pageRenderViewController.noReportTitle = YES;
    self.pageRenderViewController.screenShot = screenshot;
    self.pageRenderViewController.isSpaceScreenshot = isSpaceScreenshot;
    self.pageRenderViewController.backgroundColor = self.backgroundColor;
    [self.view addSubview:self.pageRenderViewController.view];
    self.title = [self.pageRenderViewController getPageTitle];

}

- (void)initClearDashboards{
    if (!self.dashboards){
        NSMutableArray *m = [[NSMutableArray alloc]init];
        self.dashboards = m;
        [m release];
    }else{
        [self.dashboards removeAllObjects];
    }
    
    if(!self.invisibleDashboards){
        NSMutableArray *i = [[NSMutableArray alloc]init];
        self.invisibleDashboards = i;
        [i release];
    }else{
        [self.invisibleDashboards removeAllObjects];
    }
}

-(void) applyPrompts{
    [self.popOverViewController dismissPopoverAnimated:NO];
    [self.pageRenderViewController updateGlobalPrompts];
	[self.pageRenderViewController renderDashlets:NO useCachedReportXml:YES];
}

#pragma mark - Left bar buttons operations

- (void) revealLeftSideBar:(id)sender{
    [self.popOverViewController dismissPopoverAnimated:YES];
    [super revealLeftSideBar:sender];
}

- (void) showPagesPopOver{
    [self.popOverViewController dismissPopoverAnimated:YES];
    
    self.pagesPopOver = [[[PagesPopOverViewController alloc]init] autorelease];
    self.pagesPopOver.dashboards = self.dashboards;
    self.pagesPopOver.delegate = self;
    
    self.popOverViewController = [[[UIPopoverController alloc]initWithContentViewController:self.pagesPopOver]autorelease];
    self.popOverViewController.delegate = self;
    
    UIBarButtonItem *pagesBttn = [self.navigationItem.leftBarButtonItems objectAtIndex:1];
    [self.popOverViewController presentPopoverFromBarButtonItem:pagesBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (void) showDashboardsCoverFlow{
    [self.popOverViewController dismissPopoverAnimated:YES];
    
    if(dashboardsCoverFlowController != nil){
        [dashboardsCoverFlowController.view removeFromSuperview];
        [dashboardsCoverFlowController release];
        dashboardsCoverFlowController = nil;
    }else{
        CGRect f = CGRectMake(0, (self.view.frame.size.height / 3) * 2, self.view.frame.size.width, (self.view.frame.size.height / 3));
        self.dashboardsCoverFlowController = [[[DashboardsCoverFlowViewController alloc] initWithFrame:f dashboards:self.dashboards] autorelease];
        self.dashboardsCoverFlowController.delegate = self;
        [self.view addSubview:self.dashboardsCoverFlowController.view];
    }
}

#pragma  mark - Right bar button operations

-(void)showPromptsPopOver{
    [self.popOverViewController dismissPopoverAnimated:YES];
    
    PromptsViewController *ctrl = [[PromptsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    if(self.renderedPage){
        ctrl.prompts = self.renderedPage.visiblePrompts;
        ctrl.pgCtrl = self.pageRenderViewController;
    }
    
    UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:ctrl];
    [ctrl release];
    
    UIPopoverController *pc = [[UIPopoverController alloc] initWithContentViewController:nc];
    [nc release];
    
    self.popOverViewController = pc;
    [pc release];
                                  
    self.popOverViewController.delegate = self;
    
    UIBarButtonItem *promptBttn = [self.navigationItem.rightBarButtonItems objectAtIndex:2];
    [self.popOverViewController presentPopoverFromBarButtonItem:promptBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	
}

-(void)saveOffline{
    [self.popOverViewController dismissPopoverAnimated:YES];
    [self.pageRenderViewController save]; 
}

-(void)refreshPage{
    [self.popOverViewController dismissPopoverAnimated:YES];
    [self.pageRenderViewController refresh];
}

#pragma mark - PopOverController delegate operation

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    //[popoverController release];
    //self.popOverViewController = nil;
}

#pragma mark - Left side bar delegate operations

- (NSIndexPath *) lastSelectedIndexPathForSidebarViewController:(SidebarViewController *)sidebarViewController{
    if([Settings demoMode]){
        return [NSIndexPath indexPathForRow:IDX_DEMO inSection:IDX_HDR_MAIN];
    }
   
    return nil;
}


@end
