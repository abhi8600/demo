//
//  Util.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/10/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TBXML.h"
#import "ASIHTTPRequest.h"
#import "RequestPendingIndicatorController.h"
#import "Log.h"

@interface Util : NSObject {

}

+ (TBXMLElement*) traverseFindElement:(TBXMLElement *)element target:(NSString*)ourTarget;
+ (TBXMLElement *) findSibling:(TBXMLElement *)element target:(NSString *)ourTarget;
+ (void) requestFailedAlert:(NSError *)request;
+ (void) alert:(NSString*)message title:(NSString*)title;
+ (void) popupRequestIndicator:(UIViewController *)caller message:(NSString *)msg;
+ (BOOL) orientationLockLandscape:(UIInterfaceOrientation)interfaceOrientation;
+ (NSMutableString *) xmlSimpleEscape:(NSMutableString*)str;
+ (void) startPendingRequest:(NSString *)message sender:(id)snd;
+ (void) endPendingRequest:(NSString *)message sender:(id)snd;
+ (void) removeAllSubviews:(UIView *)parentView;
+ (void) setDict:(NSMutableDictionary*)dict key:(NSString*)key value:(NSString*)value;
+ (NSString*) getResponse:(ASIHTTPRequest *)request;
+ (UIColor*) parseRgbColor:(NSString*)str;
+ (UIColor *)colorWithRGBHex:(UInt32)hex;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (NSString *)xmlBlock:(NSString *)response tag:(NSString*)tag;
+ (NSMutableString *)xmlSimpleUnescape:(NSMutableString*)str;
+ (NSString*) xmlUnescape:(NSString*)str;
+ (void) screenShot:(UIView *)uiview loc:(NSString *)loc;
+ (NSString *) checkForError:(NSString *)str;
+ (NSString *) toXmlString:(TBXMLElement *)xml output:(NSMutableString *)output;
+ (UIColor *) inverseColor:(UIColor *)clr;
+ (NSString *) hexFromUIColor:(UIColor *)_color;
@end
