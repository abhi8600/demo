//
//  Log.m
//  BirstMobile
//
//  Created by Seeneng Foo on 9/16/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Log.h"

@implementation Log

static LogLevel _logLevel = Debug;

+ (void) setLogLevel:(LogLevel)level{
    _logLevel = level;
}

+ (LogLevel) logLevel{
    return _logLevel;
}

+ (void) debug:(NSString *)format, ...{
    if(_logLevel == Debug){
        va_list argList;
        va_start(argList, format);
        NSLogv(format, argList);
        va_end(argList);
    }
}

+ (void) warn:(NSString *)format, ...{
    if(_logLevel == Warn){
        va_list argList;
        va_start(argList, format);
        NSLogv(format, argList);
        va_end(argList);
    }
}

@end
