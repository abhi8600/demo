//
//  DetailViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "DetailViewController.h"
#import "RootViewController.h"
#import "Settings.h"

@interface DetailViewController ()
@property (nonatomic, retain) UIPopoverController *popoverController;
- (void)configureView;
@end



@implementation DetailViewController

@synthesize toolbar, popoverController, detailItem, detailDescriptionLabel, mainView, settingsCtrl, scrollViewSubView, rootViewCtrl, reqPendingCtrl;

#pragma mark -
#pragma mark Managing the detail item

/*
 When setting the detail item, update the view and dismiss the popover controller if it's showing.
 */
- (void)setDetailItem:(id)newDetailItem {
    if (detailItem != newDetailItem) {
        [detailItem release];
        detailItem = [newDetailItem retain];
        
        // Update the view.
        [self configureView];
    }

    if (self.popoverController != nil) {
        [self.popoverController dismissPopoverAnimated:YES];
    }        
}


- (void)configureView {
    // Update the user interface for the detail item.
    detailDescriptionLabel.text = [detailItem description];   
}

//Event listener for the settings button
- (void) toggleSettings{
	if (self.settingsCtrl != nil && [settingsCtrl isPopoverVisible]) {
		[settingsCtrl dismissPopoverAnimated:YES];
		[settingsCtrl release];
		settingsCtrl = nil;
	}else {

		SettingsViewController *settings = [[[SettingsViewController alloc] initWithStyle:UITableViewStyleGrouped] autorelease];
		settings.tableView.backgroundColor = [UIColor blackColor];
		settings.detailCtrl = self;
        UINavigationController *navCtrl = [[[UINavigationController alloc]initWithRootViewController:settings] autorelease];
		
		self.settingsCtrl = [[[UIPopoverController alloc] initWithContentViewController:navCtrl] autorelease];
		//settingsCtrl.popoverContentSize = CGSizeMake(300, 350);
		[settingsCtrl presentPopoverFromBarButtonItem:settingsBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
}

- (void) loggedOutClearScreen{
	[self.rootViewCtrl.navigationController popToRootViewControllerAnimated:YES];
	[self clearSubviews];
	[settingsCtrl dismissPopoverAnimated:YES];
}

- (void) setNewView:(UIViewController*)ctrl{
	if ([settingsCtrl isPopoverVisible]) {
		[settingsCtrl dismissPopoverAnimated:YES];
	}
	
	[self removeSubViews];
	
	//There is a gap in the landscape orientation when the subview is a uitableview. Try to fix this by setting the frame size.
	UIView *cview = ctrl.view;      
	cview.frame = CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height);
	
	//Add the new view
	[mainView addSubview: cview];
}

- (void) displayAboutUs{
    /*
    NSString *aboutUs = [[[Settings instance]settings] valueForKey:@"aboutUsUrl"];
    NSURL *url = [NSURL URLWithString:aboutUs];
     */
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"aboutUs" ofType:@"JPG"] isDirectory:NO];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self removeSubViews];

    CGRect frame = self.view.bounds;
    UIWebView *webview = [[UIWebView alloc] initWithFrame:frame];
    //webview.scalesPageToFit = YES;
    webview.opaque = NO;
    webview.backgroundColor = [UIColor clearColor];
    webview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [webview loadRequest:request];
    [mainView addSubview:webview];
    [webview release];

}

- (void) displayTutorialPdf{
    
}

- (void) removeSubViews{
    //Remove all the views in our scrollview
	NSArray* views = [mainView subviews];
	for (int i = [views count] - 1; i >= 0; i--) {
		UIView *subView = [views objectAtIndex:i];
		[subView removeFromSuperview];
	}
}

#pragma mark -
#pragma mark Split view support
/*
- (void)splitViewController: (UISplitViewController*)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem*)barButtonItem forPopoverController: (UIPopoverController*)pc {
    //DISABLED for now to lock orientation
    
    barButtonItem.title = @"Menu";
    NSMutableArray *items = [[toolbar items] mutableCopy];
    [items insertObject:barButtonItem atIndex:0];
    [toolbar setItems:items animated:YES];
    [items release];
    self.popoverController = pc; 
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController: (UISplitViewController*)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
    
    NSMutableArray *items = [[toolbar items] mutableCopy];
    [items removeObjectAtIndex:0];
    [toolbar setItems:items animated:YES];
    [items release];
    self.popoverController = nil;
}*/


#pragma mark -
#pragma mark Rotation support

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return [Util orientationLockLandscape:interfaceOrientation];
	//return YES;
}


#pragma mark -
#pragma mark View lifecycle


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
    
    [self setRightBarButtonAsLogin];
	//Listen for notifications on whether to pop up the request pending
	NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
	[center addObserver:self selector:@selector(requestPendingShow:) name:@"startRequest" object:nil];
	[center addObserver:self selector:@selector(requestPendingEnd:) name:@"endRequest" object:nil];
	
}

- (void)setRightBarButtonAsLogin{
    //Set login / settings button
    settingsBttn.title = @"Login";
    settingsBttn.target = self;
    settingsBttn.action = @selector(toggleSettings);
}

- (void)setRightBarButton:(NSString *)title action:(SEL)action target:(id)target{
    settingsBttn.title = title;
    settingsBttn.action = action;
    settingsBttn.target = target;
}
 
- (void)requestPendingShow:(NSNotification*) notification{
	NSLog(@"requestPendingShow");
	if (!self.reqPendingCtrl) {
		self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init]autorelease];
		//self.reqPendingCtrl.modalPresentationStyle = UIModalPresentationFullScreen;
	}
	
	self.reqPendingCtrl.msg.text = [[notification userInfo] objectForKey:@"message"];
	UIView *v = (UIView*)[notification object];
	[v addSubview:self.reqPendingCtrl.view];
	//[vc presentModalViewController:self.reqPendingCtrl animated:YES];	
}

- (void)requestPendingEnd:(NSNotification *) notification{
	NSLog(@"requestPendingEnd");
	if (self.reqPendingCtrl) {
		[self.reqPendingCtrl.view removeFromSuperview];
		//[self.reqPendingCtrl dismissModalViewControllerAnimated:YES];
	}
}

- (void) startAnimateRequest:(NSString*)message{
    if (!self.reqPendingCtrl) {
        self.reqPendingCtrl = [[[RequestPendingIndicatorController alloc]init] autorelease];
        self.reqPendingCtrl.message = message;
        self.reqPendingCtrl.view.frame = self.view.bounds;
        [self.view addSubview:self.reqPendingCtrl.view];
    }else{
        self.reqPendingCtrl.message = message;
    }
    [self clearSubviews];
}

- (void) stopAnimateRequest {
    if (self.reqPendingCtrl) {
        [reqPendingCtrl.view removeFromSuperview];
        [reqPendingCtrl release];
        reqPendingCtrl = nil;
    }
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

- (void)clearSubviews{
	//Remove all the views in our scrollview
	NSArray* views = [mainView subviews];
	for (int i = [views count] - 1; i >= 0; i--) {
		UIView *subView = [views objectAtIndex:i];
		[subView removeFromSuperview];
	}
}

- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.popoverController = nil;
	self.settingsCtrl = nil;
    [super viewDidUnload];
}


#pragma mark -
#pragma mark Memory management

/*
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/

- (void)dealloc {
    [popoverController release];
    [toolbar release];
	
    [detailDescriptionLabel release];
	
	[settingsCtrl release];
    [super dealloc];
}

@end
