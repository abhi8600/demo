//
//  MaximizedDashletViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/13/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MaximizedDashletViewController.h"
#import "Notification.h"

@interface MaximizedDashletViewController ()

@end

@implementation MaximizedDashletViewController

@synthesize renderer, popOver, docCtrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.renderer disableAllDrilling:YES];
   
    NSMutableArray *rightSideButtons = [[NSMutableArray alloc]init];
    Dashlet *d = renderer.dashletCtrl.dashlet;
    
    if(d.enablePdf){
        saveBttn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onSave:)];
        [rightSideButtons addObject:saveBttn];
    }
    
    if(d.enableColumnSelectors){
        colBttn = [[[UIBarButtonItem alloc] initWithTitle:@"Columns" style:UIBarButtonItemStyleBordered target:self action:@selector(onColumnsSelectors:)] autorelease];
        [rightSideButtons addObject:colBttn];
    }

    if(d.enableViewSelector){
       viewBttn = [[UIBarButtonItem alloc] initWithTitle:@"View" style:UIBarButtonItemStyleBordered target:self action:@selector(onViewSelector:)];
        [rightSideButtons addObject:viewBttn];
    }
    
    if([rightSideButtons count] > 0){
        self.navigationItem.rightBarButtonItems = rightSideButtons;
    }
    
    [rightSideButtons release];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onMinimize) name:MINIMIZE_DASHLET object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void) viewDidAppear:(BOOL)animated{
    [self.renderer removeFromSuperview];
    [self maximizeChart];
}

- (void) onMinimize{
    [self.navigationController popViewControllerAnimated:YES];  
}

- (void)maximizeChart{
    float toolbarHeight = self.navigationController.toolbar.frame.size.height;
    CGRect r = CGRectMake(0, toolbarHeight, self.view.frame.size.width, self.view.frame.size.height - toolbarHeight);
    self.renderer.frame = r;
    [self.view addSubview:renderer];
    [self.renderer maximizeSingleChart:CGRectMake(0, 0, r.size.width, r.size.height - toolbarHeight)];
}

- (void) viewWillDisappear:(BOOL)animated{
    [self.renderer.dashletCtrl resetRenderer];
    [self.popOver dismissPopoverAnimated:NO];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void) dealloc{
    [saveBttn release];
    [viewBttn release];
    [popOver release];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    
    [super dealloc];
}

- (void) onViewSelector:(id)sender{
    [self.popOver dismissPopoverAnimated:NO];
    
    ViewSelectorsViewController *colSelCtrl = [[[ViewSelectorsViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
    colSelCtrl.maxVC = self;
    colSelCtrl.originalChartType = [self.renderer.dashletCtrl readSelectorChartType];
    colSelCtrl.selectedChart = self.renderer.dashletCtrl.selectorChartType;
    self.popOver = [[[UIPopoverController alloc] initWithContentViewController:colSelCtrl] autorelease];
    [self.popOver presentPopoverFromBarButtonItem:viewBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) onColumnsSelectors:(id)sender{
    [self.popOver dismissPopoverAnimated:NO];
    ColumnSelectorsViewController *ctrl = [[[ColumnSelectorsViewController alloc]initWithStyle:UITableViewStylePlain]autorelease];
    ctrl.maxVC = self;
    self.popOver = [[[UIPopoverController alloc] initWithContentViewController:ctrl]autorelease];
    [self.popOver presentPopoverFromBarButtonItem:colBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) onSave:(id)sender{
    [self.popOver dismissPopoverAnimated:NO];
    
    ExportDashletViewController	*ctrl = [[[ExportDashletViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
    ctrl.maxVC = self;
    self.popOver = [[[UIPopoverController alloc] initWithContentViewController:ctrl] autorelease];
    self.popOver.popoverContentSize = CGSizeMake(300, 42);
    [self.popOver presentPopoverFromBarButtonItem:saveBttn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) chartSelectorChange:(NSString *)chartType{
    [self.popOver dismissPopoverAnimated:YES];
    [self.renderer.dashletCtrl renderByViewSelector:chartType];
}

- (void) columnSelectorChange:(NSString *)xml {
    [self.popOver dismissPopoverAnimated:YES];
    [self.renderer.dashletCtrl renderByColumnSelector:xml];
}

- (void) getPdf{
    ASIFormDataRequest *request = [renderer.dashletCtrl getPDFRequest];
    [request setDelegate:self];
    [request startSynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    // Use when fetching binary data
    NSData *responseData = [request responseData];
    NSLog(@"getPdf() response length %d", [responseData length]);
    [self saveData:responseData filename:@"file.pdf"];
}

- (void) saveData:(NSData *)data filename:(NSString*)filename{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    NSString *offlineDir = [Settings offlineDir:filemgr];
    
    NSString *offlineDownloadDir = [NSString stringWithFormat:@"%@/download", offlineDir];
    [Log debug:@"writing to download dir %@", offlineDownloadDir];
    
    NSError *error;
    
    //TODO: Alert user that we'll delete the previous
    if (![filemgr removeItemAtPath:offlineDownloadDir error:&error]){
        //Todo - alert here and return
        [Log debug:@"Cannot remove download directory %@ because %@", offlineDownloadDir, [error localizedDescription]];
    }
    
    if(![filemgr createDirectoryAtPath:offlineDownloadDir withIntermediateDirectories:YES attributes:nil error: &error]){
        [Log debug:@"cannot create directory %@ because %@", offlineDownloadDir, [error localizedDescription]];
        return;
    }
    
    NSError *err;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", offlineDownloadDir, filename];
    if(![data writeToFile:filePath options:NSDataWritingAtomic error:&err]){
        [Log debug:@"Download document write to directory failed reason: %@", [err localizedDescription]];
    }
    
    self.docCtrl = [[[UIDocumentInteractionController alloc] init] autorelease];
    [self.docCtrl setURL:[NSURL fileURLWithPath:filePath]];
    self.docCtrl.delegate = self;
    [self.docCtrl presentPreviewAnimated:YES];
    
    [self.popOver dismissPopoverAnimated:YES];
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self.navigationController;
}

- (void) documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller{
    NSLog(@"releasing documentInteractionController");
    self.docCtrl.delegate = nil;
    [docCtrl release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"error");
    NSError *error = [request error];
    NSLog(@"MaximizedDashletViewController.requestFailed(): %@", [error localizedDescription]);
}


@end
