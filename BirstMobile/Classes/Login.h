//
//  Login.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/20/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

typedef enum {
    Locked,
    NotLicensed,
    UnsuccessFul,
    Success
}LoginReturnCode;

@interface Login : NSObject {

}

+ (NSMutableDictionary*) parseLoginPageParams:(NSString*)response;
+ (BOOL) getParamValue:(NSString*)response parameter:(NSString*)param dictionary:(NSMutableDictionary*)dict;
+ (NSString *) getLoginPageUrl;
+ (void) performLogin:(NSString*)username password:(NSString*)password startAnimate:(SEL)startAnimate endAnimate:(SEL)endAnimate delegate:(id)delegate loginSuccess:(SEL)loginSuccess loginFail:(SEL)loginFail;
+ (LoginReturnCode) verifyLogin:(ASIHTTPRequest*)request;

@end
