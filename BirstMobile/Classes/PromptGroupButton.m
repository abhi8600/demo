//
//  PromptGroupButton.m
//  BirstMobile
//
//  Created by See Neng Foo on 10/7/13.
//
//

#import "PromptGroupButton.h"
#import "BirstWS.h"
#import "ASIHTTPRequest.h"
#import "Util.h"
#import "PromptGroupCheckBox.h"

@implementation PromptGroupButton

@synthesize renderer, options, operationName;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) parseXml:(TBXMLElement *)xml{
    NSString *label = [TBXML valueOfAttributeNamed:@"imagePath" forElement:xml];
    [self setTitle:label forState:UIControlStateNormal];
    
    self.operationName = [TBXML valueOfAttributeNamed:@"operationName" forElement:xml];
    
    TBXMLElement *opt = [Util traverseFindElement:xml target:@"Option"];
    if(opt != Nil){
        if(self.options == nil){
            self.options = [[[NSMutableArray alloc] init]autorelease];
        }
        NSString *v = [TBXML textForElement:opt];
        [self.options addObject:v];
        
    }
    
    [self addTarget:self action:@selector(onButtonClick)
   forControlEvents:UIControlEventTouchUpInside];
}

- (void)onButtonClick{
    NSDictionary *txtBoxes = self.renderer.editableTextBoxes;
    NSDictionary *chkBoxes = self.renderer.checkBoxes;
    
    NSMutableString *params = [NSMutableString stringWithFormat:@"<ns:xmlData>&lt;OperationData&gt;&lt;OperationName&gt;%@&lt;/OperationName&gt;", self.operationName];
   
    NSInteger ct = [options count];
    for(int i = 0; i < ct; i++){
        NSString *opt = [options objectAtIndex:i];
        [params appendString:@"&lt;Parameter&gt;"];
        [params appendFormat:@"&lt;ParameterName&gt;%@&lt;/ParameterName&gt;", opt];
        [params appendString:@"&lt;ParameterValues&gt;"];
        
        NSArray *cboxes = [chkBoxes objectForKey:opt];
        for(int y = 0; y < [cboxes count]; y++){
            PromptGroupCheckBox *cb = [cboxes objectAtIndex:y];
            if(cb.on){
                [params appendFormat:@"&lt;ParameterValue>%@&lt;/ParameterValue&gt;", cb.value];
            }
        }
        
        NSArray *boxes = [txtBoxes objectForKey:opt];
        for(int y = 0; y < [boxes count]; y++){
            UITextField *f = [boxes objectAtIndex:y];
            [params appendFormat:@"&lt;ParameterValue>%@&lt;/ParameterValue&gt;", f.text];
            
        }
        [params appendString:@"&lt;/ParameterValues&gt;&lt;/Parameter&gt;"];
        [params appendFormat:@"&lt;PromptXML&gt;%@&lt;/PromptXML&gt;", renderer.dashletCtrl.promptXml];
        [params appendString:@"&lt;/OperationData&gt;</ns:xmlData>"];
    }
    
    NSLog(@"onButtonClick: params = %@", params);
    
    __block ASIHTTPRequest* req =  [BirstWS requestWithOperationHandler:@"performOperation"];
    [BirstWS requestNSFormat:req body:params operationName:@"Operations"];
    
    //Success, parse the full page xml
    [req setCompletionBlock:^{
        NSString *response = [Util getResponse:req];
        NSLog(@"response = %@", response);
    }];
    
    [req setFailedBlock:^{
        [Util requestFailedAlert:[req error]];
    }];
    
    [req startSynchronous];

};

@end
