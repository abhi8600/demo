//
//  Webservice.m
//  Birst
//
//  Created by Seeneng Foo on 3/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Webservice.h"
#import "Settings.h"
#import "Log.h"

//hardcode for now
static NSString *ADMIN = @"/AdminService.asmx";
static NSString *DASHBOARD = @"/SMIWeb/services/Dashboard.DashboardHttpSoap11Endpoint/";
static NSString *ADHOC = @"/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint/";
static NSString *SP_HEAD = 
	@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:tns=\"http://www.birst.com\"><SOAP-ENV:Body>";
static NSString *SP_END = @"</SOAP-ENV:Body></SOAP-ENV:Envelope>";

@implementation Webservice

@synthesize data, operation, tns, request;

//admin webservice call
- (Webservice*) initWithAdmin:(NSString*)op{
	return [self initWithUrl:[self sanitizePort:ADMIN newPort:6105] prefix:@"http://www.birst.com/" operation:op];
}

//dashboard webservice call
- (Webservice*) initWithDashboard:(NSString*)op{
	return [self initWithUrl:[self sanitizePort:DASHBOARD newPort:6103] prefix:@"urn:" operation:op];
}

//adhoc webservice call

- (Webservice*) initWithAdhoc:(NSString *)op{
	return [self initWithUrl:[self sanitizePort:ADHOC newPort:6103] prefix:@"urn:" operation:op];
}

- (NSString*) sanitizePort:(NSString *)suffix newPort:(int)port{
	NSString *server = [Settings server];
	
	//Switch 6105 port to 6103 for development server
	NSRange range = [server rangeOfString:@":6105"];
	if (range.length > 0 && port != 6105) {
		NSString* newUrl = [NSString stringWithFormat:@"%@:%d%@", [server substringToIndex:range.location], port, suffix];
		[Log debug:@"sanitizePort: %@", newUrl];
		return newUrl;
	}
	
	NSString *sUrl = [NSString stringWithFormat:@"%@%@", [Settings server], suffix];
	[Log debug:@"sanitizePort: %@", sUrl];
	return sUrl;
}

- (Webservice*) initWithUrl:(NSString*)link prefix:(NSString*)pref operation:(NSString*)op{
	self = [super init];
	if(self){
		if (!op) {
			op = @"";
		}
		self.operation = op;
		self.tns = pref;
		url = [[NSURL URLWithString:link] retain];
		request = [[ASIFormDataRequest requestWithURL:url] retain];
		[request addRequestHeader:@"SOAPAction" value: [NSString stringWithFormat:@"\"%@%@\"", pref, op]];
		[request addRequestHeader:@"Content-type" value: @"text/xml; charset=utf-8"];
        [Settings addCSRFHeader:request];
        
	}
	return [self autorelease];
}

- (NSString*) syncRequest:(NSString*)body {
	NSString* envelope = [NSString stringWithFormat:@"%@%@%@", SP_HEAD, body, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
	[request startSynchronous];
	NSError *err = [request error];
	if (!err) {
		NSString* response = [request responseString];
		[Log debug:@"response = %@", response];
		return response;
	}
	return [NSString stringWithFormat:@"<ErrorCode>%d</ErrorCode>", err.code];
}

- (void) asyncRequest:(NSString*)body delegate:(id)del{
	[self asyncRequest:body delegate:del requestFinished:nil requestFailed:nil];
}

- (void) asyncRequest:(NSString*)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed {
	NSString* envelope = [NSString stringWithFormat:@"%@<tns:%@ xmlns:tns=\"%@\">%@</tns:%@>%@", SP_HEAD, operation, tns, body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}
	
	[request startAsynchronous];
}

- (void) asyncRequestNS:(NSString *)body delegate:(id)del{
	[self asyncRequestNS:body delegate:del requestFinished:nil requestFailed:nil];
}

- (void) asyncRequestNSDash:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed{
	NSString* envelope = [NSString stringWithFormat:@"%@<ns:%@ xmlns:ns=\"%@\">%@</ns:%@>%@", SP_HEAD, operation, @"http://dashboard.WebServices.successmetricsinc.com", body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}
    
	
	[request startAsynchronous];
}


- (void) asyncRequestNS:(NSString *)body delegate:(id)del requestFinished:(SEL)didFinish requestFailed:(SEL)didFailed{
	NSString* envelope = [NSString stringWithFormat:@"%@<ns:%@ xmlns:ns=\"%@\">%@</ns:%@>%@", SP_HEAD, operation, @"http://adhoc.WebServices.successmetricsinc.com", body, operation, SP_END];
	[Log debug:@"envelope = %@", envelope];
	[request appendPostData:[envelope dataUsingEncoding:NSUTF8StringEncoding]];
	[request setDelegate:del];
	
	if (didFinish) {
		[request setDidFinishSelector:didFinish];
	}
	
	if (didFailed) {
		[request setDidFailSelector:didFailed];
	}

	
	[request startAsynchronous];
}

- (void) dealloc {
	[operation release];
	[tns release];
	[data release];
	[url release];
    [request clearDelegatesAndCancel];
	[request release];
	[super dealloc];
}
@end
