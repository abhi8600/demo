//
//  TutorialViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/20/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "TutorialViewController.h"
#import "UIViewController+JTRevealSidebarV2.h"

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [webview release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    self.title = @"Tutorial";
    
    //Back button to close
    UIBarButtonItem *leftClose = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
    self.navigationItem.leftBarButtonItem = leftClose;
    [leftClose release];
     
    //Black colored background
    self.view.backgroundColor = [UIColor blackColor];
    
    //Offset for the navigation bar
    float offset = 43;
    CGRect rect = self.view.frame;
    rect.size.height -= offset;
    rect.origin.y += offset;

    //Open and read the pdf file
    NSString *manualFile = [[NSBundle mainBundle] pathForResource:@"tutorial" ofType:@"pdf"];
    NSURL *url = [NSURL fileURLWithPath:manualFile];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];

    webview = [[UIWebView alloc]initWithFrame:rect];
    webview.scalesPageToFit = YES;
    webview.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:webview];
    [webview loadRequest:urlRequest];
    
    [super viewDidLoad];
}

- (void) close{
    [self dismissModalViewControllerAnimated:YES];
}

/*
- (void) viewWillAppear:(BOOL)animated{
    //resize, push the view to below
    if (webview){
        webview.frame = self.view.bounds;
    }
    [super viewWillAppear:animated];
}*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
