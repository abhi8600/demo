//
//  LoginSettingsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 7/12/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "SettingsViewController.h"
#import "GradientButton.h"

@interface LoginSettingsViewController : MainViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>{
    UITextField *username;
    UITextField *password;
    UISwitch *rememberPassword;
    GradientButton *login;
    UIButton *toggleEditServers;
    
    NSMutableDictionary *settings;
    SettingsViewController *settingsViewController;
    UITableView *serverListings;
    UITextField *cellTextField;
    NSString *addedNewServer;
    
}

@property (nonatomic, retain) IBOutlet UITextField *username;
@property (nonatomic, retain) IBOutlet UITextField *password;
@property (nonatomic, retain) IBOutlet UISwitch *rememberPassword;
@property (nonatomic, retain) IBOutlet GradientButton *login;
@property (nonatomic, retain) IBOutlet UIButton *toggleEditServers;
@property (nonatomic, retain) IBOutlet UITableView *serverListings;
@property (nonatomic, retain) NSMutableDictionary *settings;
@property (nonatomic, retain) UITextField *cellTextField;
@property (nonatomic, retain) NSString *addedNewServer;

- (void) toggleControlsInteraction;
- (IBAction)doLogin:(id)sender;
- (IBAction)editServers:(id)sender;
- (void) startAnimateLogin;
- (void) endAnimateLogin;
- (void)textFieldDidChange:(UITextField *)textField;
- (void) onKeyboardShows:(id)sender;
- (void) onKeyboardHides:(id)sender;
@end
