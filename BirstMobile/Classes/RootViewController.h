//
//  RootViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/9/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"

#import "Webservice.h"
#import "SpacesTableViewController.h"
#import "Email.h"

@class DetailViewController;

typedef enum {
    DemoDashboard,
    DashboardModule,
    SpaceDetail,
    DashboardList,
    AllSpaces
}RequestTag;

@interface RootViewController : UITableViewController <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>{
    DetailViewController *detailViewController;
	LoginViewController *loginCtrl;
    int initViewAppears;
    ASINetworkQueue *networkQueue;    
}

@property (nonatomic, retain) ASINetworkQueue *networkQueue;
@property (nonatomic, readonly) LoginViewController *loginCtrl;
@property (nonatomic, retain) IBOutlet DetailViewController *detailViewController;

- (NSMutableDictionary*) parseLoginPageParams:(NSString*)response;
- (BOOL) getParamValue:(NSString*)response parameter:(NSString*)param dictionary:(NSMutableDictionary*)dict;
- (void)onGetAllSpaces:(ASIHTTPRequest *)request;
- (void)onGetSpaceDetailsFinished:(ASIHTTPRequest*)request;
- (void)onGetDashboardModFinished:(ASIHTTPRequest*)request;
- (void)onGetDashboardListFinished:(ASIHTTPRequest *)request;
- (void)requestFailed:(ASIHTTPRequest *)request;
- (void)onLoginSuccessGetSpaces:(ASIHTTPRequest *)request;
- (void)onLoginSuccessGetDashboards:(ASIHTTPRequest *)request;
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void)getDemoDashboard;
- (void)onRequestFinished:(ASIHTTPRequest*)request;
@end
