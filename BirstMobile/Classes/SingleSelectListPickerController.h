//
//  SingleSelectListPickerController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 3/24/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prompt.h"
#import "PromptValues.h"
#import "PageRenderViewController.h"
#import "QueryListTableController.h"

@class QueryListTableController;

@interface SingleSelectListPickerController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>{
	UIPickerView *picker;
	Prompt *prompt;
	PromptValues *selectedValue;
    UILabel *label;
    UIButton *bttn;
    UITextField *labelValue;
    BOOL renderedInDashlet;
    PageRenderViewController *pgCtrl;
    UIPopoverController *popOver;
    QueryListTableController *qCtrl;
    UIColor* backgroundColor;
}

@property (nonatomic, retain) UIPickerView *picker;
@property (nonatomic, assign) Prompt *prompt;
@property (nonatomic, assign) PromptValues *selectedValue;
@property (retain) UILabel *label;
@property (retain) UITextField *labelValue;
@property (retain) UIButton *bttn;
@property (assign) BOOL renderedInDashlet;
@property (assign) PageRenderViewController *pgCtrl;
@property (retain) UIPopoverController *popOver;
@property (retain) QueryListTableController *qCtrl;
@property (retain) UIColor *backgroundColor;

- (NSMutableArray*)getSelectedValues;

@end
