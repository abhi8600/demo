#ifndef __PGSDK_API_COLORFILL_H__
#define __PGSDK_API_COLORFILL_H__


#include "types.h"  //INT16 INT32 MEMPTR
#include "color.h"  //RGB16 pg_rgba
#include "space.h"  //fPoint2D

/*
 * PictureRecord
 * WashRecord
 * AdvancedWashRecord
 * GradientRecord
 */

#ifdef _COGNOS2
#  define MAXFILENAME 256
#else
/* 10mar2008jans: changed 64 to 128 since before it was actually 2*64 
	 (one for pictPath and one for dummy that is now removed) */
# define MAXFILENAME 128 
#endif

typedef struct AdvancedWashStruct
{
	RGB16   startColor;
	RGB16   endColor;
	INT16   nOffsetX;
	INT16   nOffsetY;
	INT16   nAngle;        //in degrees
	INT16   nSteps;
	INT16   nScale;
	INT16   nWashType;
} AdvancedWashRecord;


/* 12may2003jans: this is moved from std_sfx.h */
typedef struct PictureRecordStruct
{
	INT16    pictScale;
	INT16    pictFlip;
	INT16    xExt;
	INT16    yExt;
	char     pictName[MAXFILENAME];
	INT16    format;

	/* allow loading pictures directly from memory */
	MEMPTR   buffer;
	INT32    bufsize;

} PictureRecord; 

/* 12may2003jans: this is moved from std_sfx.h */
typedef struct WashRecordStruct
{
	RGB16    startColor;
	RGB16    endColor;
	INT16    startBin;
	INT16    endBin;
	INT16    washType;
	INT16    washDir;
	INT16    washScale;
} WashRecord;


/*
 * Gradient interface
 *
 * There are three kinds of gradients available:
 * - circular
 * - linear (defined by vector)
 * - linear (defined by angle)
 *
 * These gradients may be defined in one of three cooridnate systems
 * - PG_COORD_RELATIVE means that gradient parameters will be computed
 *   relative to the bounding rectangle of object it is applied to.
 *   Widths and points should be in range 0.0 - 1.0, and that value
 *   will be multiplied by the actual size of the object.
 * - PG_COORD_ABSOLUTE_VIRTUAL and PG_COORD_ABSOLUTE_DEVICE are
 *   used to define gradient without considering of the object size,
 *   and will use internal virtual (-32767 - 32768) or device
 *   (user defined by eg. pg_SetSize) scale. These modes are
 *   mostly for internal use, and shouldn't be interesting for
 *   general audience.
 */
//values of points (as p0, p1, center, focal) and distance (rx, ry)
//may be defined in different ways depending on coordinatesType value

enum pg_gradient_type {
	PG_GRADIENT_LINEAR_VECTOR,
	PG_GRADIENT_LINEAR_ANGULAR,
	PG_GRADIENT_OVAL
};

/* PG_GRADIENT_LINEAR_VECTOR
 * Gradient defined by two points. Grade will start at point p0
 * and end on p1, anything before p0 and after p1 will be solid color.
 */
struct pg_GradientLinearVector {
	fPoint2D p0;
	fPoint2D p1;
};

/* PG_GRADIENT_LINEAR_ANGULAR
 * Gradient defined by the angle and a point that defines center 
 * of gradient vector. In this case coordinatesType will matter for
 * the way of how center of gradient vector is computed, but
 * still (regardless of the mode) brect will be used do determine
 * gradient range.
 * This type is left for backward compatibility, and 
 * PG_GRADIENT_LINEAR_VECTOR should be used if possible.
 */
struct pg_GradientLinearAngular {
	INT16 angle;
	fPoint2D center;
};


/* PG_GRADIENT_OVAL
 * Gradient defined by the focal point where the gradient starts
 * and the outer ellipse (center, rx, ry) where it stops.
 * Anything outside the ellipse will have solid color.
 * In PG_COORD_RELATIVE mode, the gradient ellipse will be scaled
 * to the shape size, so even if rx==ry, non-square shapes
 * will get ellipse (non-circle) based fill
 * On rx==ry==1.0 shape brect will be filled completely
 */
struct pg_GradientOval {
	fPoint2D  center;
	fPoint2D  focal;
	REAL      rx;
	REAL      ry;
};


enum pg_coordiantes_type {
	PG_COORD_RELATIVE,
	PG_COORD_ABSOLUTE_VIRTUAL,
	PG_COORD_ABSOLUTE_DEVICE
};

struct pg_GradientStop {
	struct pg_rgba color;     //RGB16 + alpha (0-transparent to 255-opaque)
	INT16   offset;    //0 to PG_GRADIENT_END
};

#define PG_GRADIENT_MAX_STOPS   10
#define PG_GRADIENT_END   10000
/* fixed size of color/offset table is a bit wasteful, but LayerStruct 
	 is not allocated for every color instance, and when set to 10 elements 
	 it is still smaller than PictureRecord */
typedef struct GradientStruct //enum SFXLayerType -> GradientLayer
{
	enum pg_gradient_type type;
	enum pg_coordiantes_type coordinatesType;

	INT16 nSteps;             //number of steps
	struct pg_GradientStop stop[PG_GRADIENT_MAX_STOPS];

	union {
		struct pg_GradientLinearVector  lvector;
		struct pg_GradientLinearAngular langular;
		struct pg_GradientOval          oval;
	} d;
} GradientRecord;




#endif /* __PGSDK_API_COLORFILL_H__ */
