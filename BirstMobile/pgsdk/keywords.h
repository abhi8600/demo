#ifndef __PGSDK_API_KEYWORDS_H__
#define __PGSDK_API_KEYWORDS_H__

//Pointer Modifiers
#ifndef FAR
# define FAR
#endif

#if defined(_WINDOWS)
# define PUBLIC __declspec(dllexport)
#else
# define PUBLIC
# define CALLBACK
#endif

#endif /* __PGSDK_API_KEYWORDS_H__ */
