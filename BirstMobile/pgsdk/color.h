#ifndef __PGSDK_API_COLOR_H__
#define __PGSDK_API_COLOR_H__

#include "types.h"  //UINT16

/************************************************************************
 * RGB16 - Color Triplet Information Record
 ************************************************************************/
typedef struct {
	UINT16 					red;									/* must be 16-bit values! */
	UINT16 					green;
	UINT16 					blue;
} RGB16, *RGB16Ptr;

struct pg_rgba {
	RGB16 c;
	UINT16 alpha; // 0 - transparent to 255 - opaque
};

#endif /* __PGSDK_API_COLOR_H__ */
