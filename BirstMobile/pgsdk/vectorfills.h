#ifndef __PGSDK_API_VECTORFILLS_H__
#define __PGSDK_API_VECTORFILLS_H__

#include "types.h"     //INT16 REAL
#include "colorfill.h" //PictureRecord WashRecord AdvancedWashRecord GradientRecord

/*
 * Vector specific fill structures:
 *   enum VECTOR_TEXT_ALIGN
 *   VectorColour
 *   VectorFill
 *    PatternRecord
 *
 *    [other - imported from general PGSDK]
 *    PictureRecord
 *    WashRecord 
 *    AdvancedWashRecord
 *    GradientRecord
 */

typedef struct VectorColourStruct{
	/* red, blue, green, alpha */
	REAL r;
	REAL g;
	REAL b;
	REAL a;
	/* Note: currently, the alpha channel can only be '0' or '1' (for invisible or opaque, respectively) */
} VectorColour, *VectorColourPtr;


typedef struct PatternRecordStruct
{
	VectorColour color;
	VectorColour bgcolor;
	INT16 pattern_no;
	INT16 transparent;
} PatternRecord;



typedef enum {
	VECTOR_FILL_SOLID,       //VectorColour
	VECTOR_FILL_GRADIENT,    //WashRecord
	VECTOR_FILL_PICTURE,     //PictureRecord
	VECTOR_FILL_PATTERN,     //PatternRecord
	VECTOR_FILL_ADVGRADIENT, //AdvancedWashRecord
	VECTOR_FILL_GRADIENT3,   //GradientRecord

	VECTOR_FILL_MAX

} VectorFillType;


typedef struct VectorFillStruct{
	VectorFillType type;
	union {
		VectorColour color;
		WashRecord gradient;
		PictureRecord picture;
		PatternRecord pattern;
		AdvancedWashRecord advgradient;
		GradientRecord gradient3;  // type == VECTOR_FILL_GRADIENT3 (note: coordinatesType passed will be either PG_COORD_RELATIVE or PG_COORD_ABSOLUTE_DEVICE. No PG_COORD_ABSOLUTE_VIRTUAL)
	} d;
	REAL a; /* alpha value */
} VectorFill, *VectorFillPtr;

enum VECTOR_TEXT_ALIGN {
	VEC_ALIGN_LEFT,
	VEC_ALIGN_RIGHT,
	VEC_ALIGN_CENTER
};



#endif /* __PGSDK_API_VECTORFILLS_H__ */
