#ifndef __API_EXPORT_FORMATS_H__
#define __API_EXPORT_FORMATS_H__

/* Bitmap types available: img/png, img/gif, and img/jpeg */
enum pg_ImageFormat {
	IMG_UNKNOWN, 
	IMG_PNG, 
	IMG_GIF, 
	IMG_JPEG, 
	IMG_BMP, 
	IMG_SVG, 
	IMG_SVGZ, 
	IMG_USER, 
	IMG_RAW_RGBA, 
	IMG_BMP_NOFILEHEADER 
};

#define DevGen_ImageFormat pg_ImageFormat


#endif /* __API_EXPORT_FORMATS_H__ */
