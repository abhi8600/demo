#ifndef __PGSDK_API_SPACE_H__
#define __PGSDK_API_SPACE_H__

#include "types.h"  //INT16 INT32

/*
 * Point2D (INT16)
 * Point3D (IMT16)
 * Point16 
 * Point32 aka Point (UNIX: POINT)
 * Rect16
 * Rect32  aka Rect (UNIX: RECT)
 */

typedef struct _Point2D {
	INT16 					x;
	INT16 					y;
} Point2D, * Point2DPtr;


typedef struct _Point3D {
	INT16 					x;
	INT16 					y;
	INT16 					z;
} Point3D, * Point3DPtr;


typedef struct _Point16 {
	INT16	x;
	INT16	y;
}	Point16, *Point16Ptr;


typedef struct _Point32 {
	INT32	x;
	INT32	y;
}	Point32, *Point32Ptr;

#ifndef WIN32
//typedef Point32 Point;
typedef Point32 POINT;
typedef Point32Ptr LPPOINT;

#endif /* ifndef WIN32 */

#if !defined(__APPLE__) || !defined(PG_PUBLIC_API_HEADER)
//Mac (iOS) has different definition of this, so disable this
//on iOS compilation when included as public API
//(make sure you dont use Point or Rect in PGSDK related APIs)
typedef POINT Point;
#endif

typedef struct _fPoint2D {
	REAL x;
	REAL y;
} fPoint2D;

typedef struct _fPoint3D {
	REAL	x; 
	REAL	y;
	REAL	z;
} fPoint3D;


#define PointX(pPt)           ((pPt)->x)
#define PointY(pPt)           ((pPt)->y)

typedef struct _Rect16 {
	INT16	left;
	INT16	top;
	INT16	right;
	INT16	bottom;
}	Rect16, *Rect16Ptr;	



typedef struct _Rect32 {
	INT32	left;
	INT32	top;
	INT32	right;
	INT32	bottom;
}	Rect32, *Rect32Ptr;	

#ifndef WIN32

typedef Rect32   RECT;
typedef Rect32 * LPRECT;

#endif /* ifndef WIN32 */

#if !defined(__APPLE__) || !defined(PG_PUBLIC_API_HEADER)
//Mac (iOS) has different definition of this, so disable this
//on iOS compilation when included as public API
//(make sure you dont use Point or Rect in PGSDK related APIs)
typedef RECT Rect;
#endif
typedef LPRECT PRECT;

// Polygon (formerly also MacPolygon & _PolyPtr)
typedef struct _PolyPtr {
  int     polySize;
  Rect32  polyBBox;
  Point32 polyPoints[1];
} *PolyPtr;

typedef PolyPtr PolyHandle;


#endif /* __PGSDK_API_SPACE_H__ */
