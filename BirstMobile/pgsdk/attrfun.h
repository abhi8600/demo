#ifndef __API_ATTRFUN_H__
#define __API_ATTRFUN_H__

#include "types.h"
#include "features.h"


const char * pg_get_attr_name(INT16 attrid);
const char * pg_get_attr_type(INT16 attrid);
INT16 pg_get_attr_id(const char *name);
INT16 pg_get_attr_size(INT16 attrid);


#if WANT_DEBUG_SetAttr && defined(PG_PUBLIC_API_HEADER)
#define SetGraphAttr(pGraph, layerID, objectID, seriesID, groupID, attrCode, pData)  SetGraphAttr_check(pGraph, layerID, objectID, seriesID, groupID, attrCode, pData, #pData, sizeof(*pData), __FILE__, __LINE__)
#endif

#endif /* __API_ATTRFUN_H__ */
