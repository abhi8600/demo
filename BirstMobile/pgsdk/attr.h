#ifndef __PGSDK_API_ATTR_H__
#define __PGSDK_API_ATTR_H__

#include "types.h"  //INT16 UINT16

/* this file defines commonly used structures:
 * AreaInstRec LineInstRec FontInstRec 
 * and their dependencies */

/*
 *  AreaInstRec
 *   ColorInstRec
 *   AdvDropShadowInstRec
 *   BevelInstRec
 *    BevelType
 *
 *  LineInstRec
 *   ColorInstRec
 *   GlowInstRec
 *
 *  FontInstRec
 *   ColorInstRec
 *   AdvDropShadowInstRec
 *   FancyBoxRec
 *    FancyBoxShape
 *    FrameType
 */


/************************************************************************
 * ColorInstRec - Color Instance Record
 ************************************************************************/
typedef struct {
	INT16 					useMaytag;		/* flag to enable SFX */
	SFXPtr					hMaytag;		/* handle to washing machine data */
	INT16 					nBin; 			/* palette entry # */
	INT16 					nSpectrumBin; 	/* bin from which RGB values originated */
															 
	UINT16 					nRed; 			/* RGB values */
	UINT16 					nGreen; 		/* RGB values */
	UINT16 					nBlue;			/* RGB values */
	UINT16					nAlpha;			/* Alpha channel (0..255)(255 = opaque, 0 = transparent) 10-20-99/FL */
} ColorInstRec, *ColorInstPtr;

#define PG_INIT_Color_bin_spectr(r, g, b, a, bin, spectrumbin)     { 0, NULL, (bin), (spectrumbin), (r), (g), (b), (a) }
#define PG_INIT_Color(r, g, b, a)      PG_INIT_Color_bin_spectr(r, g, b, a, 0, 255)
#define PG_INIT_Color_NULL             PG_INIT_Color_bin_spectr(0, 0, 0, 0,  0, NOFILL)

#define NOFILL							9999					/* special value indicating transparency */
#define COLORSCHEMESTART		1000					/* start of color scheme range in nSpectrumBin */
#define COLORSCHEMEEND			1999					/* end of color scheme range in nSpectrumBin */

#define IsVisible(pColor) ((pColor) && (pColor)->nAlpha != 0 && ((pColor)->nSpectrumBin != NOFILL || (pColor)->useMaytag ))


/************************************************************************
 * AdvDropShadowInstRec - Advanced Drop Shadow Instance Record
 ************************************************************************/
typedef struct {
	INT16 					nXoff;								/* virtual x offset */
	INT16 					nYoff; 								/* virtual y offset */
	UINT16 					nRed; 								/* RGB values */
	UINT16 					nGreen; 							/* RGB values */
	UINT16 					nBlue;								/* RGB values */
	BOOLEAN16				bSoftShadow;						/*  soft shadow enabled 9-26-00/FL */
	UINT16					nOpacity;							/*  shadow opacity 9-26-00/FL */
	UINT16					nBlur;								/*  fading thickness 9-26-00/FL */
	UINT16					nFade;								/*  fading algorithm 9-26-00/FL */
} AdvDropShadowInstRec, *AdvDropShadowInstPtr;

#define PG_INIT_AdvDropShadow { 0, 0, 0, 0, 0, FALSE, 0, 0, 0 }

/* Possible values for bevel style. */
typedef enum _BevelType {
	PG_BEVEL_NONE,
	PG_BEVEL_SMOOTH_EDGE,
	PG_BEVEL_CHISEL_EDGE,
	PG_BEVEL_DONUT,
	PG_BEVEL_SPHERE
} BevelType;

/************************************************************************
 * BevelInstRec - New bevel effect record
 ************************************************************************/
typedef struct BevelInstStruct {
	INT16		nLightAngle;	/* Light source angle in degrees counter clockwise, 0 = 3 o'clock. */
	INT16		nIntensity;		/* Intensity of bevel effect, percentage. (none)0..100(full). */
	INT16		nDepth;			/* Precentage Length of bevel effect - (none)0...100(maximal). */
	BevelType	nType;			/* Bevel Style - one of BevelStyle above. */
} BevelInstRec, *BevelInstPtr;

#define PG_INIT_Bevel { 0, 0, 0, PG_BEVEL_NONE }

/************************************************************************
 * AreaInstRec - Area Instance Record
 ************************************************************************/
typedef struct AreaInstStruct {
	ColorInstRec			color;		/* standard color/sfx values */
	INT16 					nPattern;	/* interior pattern (0..255) */
	INT16 					nBRed; 		/* pattern's background color (0..65535) */
	INT16 					nBGreen;	/* pattern's background color (0..65535) */
	INT16 					nBBlue;		/* pattern's background color (0..65535) */
	AdvDropShadowInstRec	drop;		/* %%NEW(5)%% DropShadow values DW 10/2/92 */
	BevelInstRec			bevel;		/* 17may06rg - Support for new bevel effects. */
} AreaInstRec, *AreaInstPtr;

#define PG_INIT_Area_bin_spectr(r,g,b,a,bin, spectr)     { PG_INIT_Color_bin_spectr(r,g,b,a, bin, spectr), 0, 0, 0, 0, PG_INIT_AdvDropShadow, PG_INIT_Bevel }
#define PG_INIT_Area(r,g,b,a)     PG_INIT_Area_bin_spectr(r,g,b,a, 0, 255)
#define PG_INIT_Area_NULL         PG_INIT_Area_bin_spectr(0,0,0,0, 0, NOFILL)

/*  possible values for nMode */
#define GLOW_INSIDE		1
#define GLOW_OUTSIDE	2
#define GLOW_INOUT		3

/************************************************************************
 * GlowInstRec - Glowing Instance Record 11-12-99/FL
 ************************************************************************/

typedef struct {
	BOOLEAN16				bGlow;								/* glowing ? */
	INT16 					nThick; 							/* glow line thickness (0..32767) */
	INT16 					nMode; 								/* glow modes (inside, outsde, in&out) */
	UINT16 					nRed; 								/* RGB values */
	UINT16 					nGreen; 							/* RGB values */
	UINT16 					nBlue;								/* RGB values */
} GlowInstRec, *GlowInstPtr;

#define PG_INIT_Glow { FALSE, 0, 0, 0, 0, 0 }


/************************************************************************
 * LineInstRec - Line Instance Record
 ************************************************************************/
typedef struct LineInstStruct {
	ColorInstRec			color;								/* standard color/sfx values */
	INT16 					nThick; 							/* line thickness (0..32767) */
	INT16 					nStyle; 							/* line style (0..255) */
	INT16 					nColor; 							/* line color (0..15) */
	INT16 					nBColor;							/* background color (0..15) */
	GlowInstRec				glow;								/* glowing info 11-12-99/FL */
	INT16					nEndCapAndJoinStyle;				/* EndCap and Miter/Join Style 29jul04DW */
} LineInstRec, *LineInstPtr;

#define PG_INIT_Line_bin_spectr_Thick_Style(r,g,b,a, bin,spectr,thick, style) { PG_INIT_Color_bin_spectr(r,g,b,a,bin,spectr), (thick), (style), 0, 0, PG_INIT_Glow, 0 }
#define PG_INIT_Line_bin_spectr(r,g,b,a,bin,spectr)            PG_INIT_Line_bin_spectr_Thick_Style(r,g,b,a,bin,spectr,0,0)
#define PG_INIT_Line_Thick_Style(r,g,b,a, thick, style) PG_INIT_Line_bin_spectr_Thick_Style(r,g,b,a, 0, 255, thick, style)
#define PG_INIT_Line(r,g,b,a)                           PG_INIT_Line_Thick_Style(r,g,b,a,0,0)
#define PG_INIT_Line_NULL                               { PG_INIT_Color_NULL, 0, 0, 0, 0, PG_INIT_Glow, 0 }

/* 2apr98DW: For COREL, fancy box/frames for legend & text */
typedef enum _FancyBoxShape {
	fbRectangle,
	fbRoundedRect,
	fbOctagon
} FancyBoxShape;

typedef enum _FrameType {
	FrameNone, 
	FrameSingleLine,
	FrameDoubleLine,
	FrameExtrude,
	FrameBevel,
	FrameReverseBevel,
	SmallFrameBevel,
	FrameAdjustableBevel,
	FrameAdjustableReverseBevel
} FrameType;

typedef struct _FancyBoxRec {
	INT16		nShape;
	INT16 		nStyle;
} FancyBoxRec, *FancyBoxPtr;

/* Bits in the nFlags field of FontInstRec, used by DrawLabel */
#define FONT_bBoxIsOK 			  1
#define FONT_bHotelText 		  2
#define FONT_bAntiAliased 	  4
#define FONT_bRotate90  		  8
#define FONT_bRotate180 		 16
#define FONT_bRotate270 		 32
#define FONT_bRotate45  		 64
#define FONT_bRotate315 		128
#define FONT_bRotateCustom 	256

/* Currently supported A_FONTORIENT values. */
#define FONTORIENT_NORMAL					0
#define FONTORIENT_HOTEL					1
#define FONTORIENT_ROTATE_90			2
#define FONTORIENT_ROTATE_180			3
#define FONTORIENT_ROTATE_270			4
#define FONTORIENT_ROTATE_45			5
#define FONTORIENT_ROTATE_315			6
#define FONTORIENT_ROTATE_CUSTOM	7

/* Justification codes for text drawing functions */
#define JUST_LEFT   0
#define JUST_BOTTOM	0
#define JUST_CENTER 1
#define JUST_RIGHT  2
#define JUST_TOP    2
#define JUST_BOTH   3

/************************************************************************
 * FontInstRec - Font Instance Record
 ************************************************************************/
typedef struct {
	ColorInstRec    color;                /* standard color/sfx values */

	INT16           eQuality;             /* stroke, draft or proof */
	INT16           nSize;                /* virtual size (0..32767) */
	INT16           nAngle;               /* axis direction in degrees */
	INT16           justH;                /* horizontal justification control */
	INT16           justV;                /* vertical justification control */

	INT16           textFont;             /* font num */
	INT16           textSize;             /* font size mode */
	INT16           textFace;             /* font style */
	INT16           textMode;             /* font transfer mode */

	INT16           nFlags;               /* flags for rotated, hotel, anti-aliasing, etc */
	INT16           nPitchAndFamily;      /* font pitch and family */
	INT16           nCharSet;             /* font character set */
	AdvDropShadowInstRec	drop;			    /* %%NEW(8)%% DropShadow values DW 10/22/92 */
	INT16           nPattern;             /* 16March98CP: added to support patterns on text */
	INT16           nBRed; 				/* pattern's background color (0..65535) */
	INT16 		  nBGreen;				/* pattern's background color (0..65535) */
	INT16 		  nBBlue;				/* pattern's background color (0..65535) */          

	/* 22apr98DW: Added for COREL - Fancybox for every font/label! */
	FancyBoxRec	  FancyBoxInfo;
	AreaInstRec	  FancyBoxArea;	
	LineInstRec	  FancyBoxLine;

	/* 10feb99DW: Added for McKinsey - Store actual PointSize */
	INT16				nPointSize;
	/* 19feb99DW: Added for McKinsey - Store external Leading factor */
	INT16				nExtLeading;

	/*  font name, replace the nFontIndex and nMBFontIndex which were eliminated 02-13-01/FL */
	LPTSTR		  szFaceName;			

	INT16           nMBPitchAndFamily;     
	INT16           nMBCharSet;            
	INT16           nMBFlags;            
	LPTSTR		      szMBFaceName;			

} FontInstRec, *FontInstPtr;

//#define PG_INIT_Font_RGB_Face(r, g, b, size, face) { PG_INIT_Color_RGB(r, g, b), 

typedef struct {
	INT16 					xULC, yULC; 					/* upper left corner (any INT16) */
	INT16 					xLRC, yLRC; 					/* lower right corner (any INT16) */
} BoxInstRec, *BoxInstPtr;



#endif /* __PGSDK_API_ATTR_H__ */
