#ifndef __PGSDK_API_LIMITS_H__
#define __PGSDK_API_LIMITS_H__

/* 04aug2009jans: PerformBind limits that number to INT16/2. elsewhere limit was set to 16000 so stick to that */
#define PG_MAX_RISERS 16000

#endif /* __PGSDK_API_LIMITS_H__ */
