#ifndef __PGSDK_API_ENCODING_H__
#define __PGSDK_API_ENCODING_H__

#include "keywords.h"  //PUBLIC
#include "graphptr.h"  //GraphPtr


/* 18jun2004jans: API & data encoding support */

/* 
 * Encoding modes for SetGraphEncodingMode (GraphPtr->nEncodingMode)
 *
 * Note:
 *   MBCS is Win32 Thick Build only
 *   and what encoding is used depends on Win32 thread locale setting 
 */

enum pg_EncodingModes {
	/* utf8 encoding is used unless (optional feature) iconv encoding is set up */
	PG_ENCODING_UTF8_ICONV   = 0,
	/* utf8 encoding is used */
	PG_ENCODING_UTF8         = 1,
	/* mbcs encoding is used */
	PG_ENCODING_MBCS         = 2,
	/* utf8 encoding is used for data, ucs2 for dynamic DevVec callbacks */
	PG_ENCODING_UTF8_DEVVEC_UCS2 = 3,
	/* mbcs encoding is used for data, ucs2 for dynamic DevVec callbacks */
	PG_ENCODING_MBCS_DEVVEC_UCS2 = 4,

	/* use predefined default value */
	PG_ENCODING_DEFAULT,

	PG_ENCODING_MAX
};

/* Set/Get encoding mode (as above) for the given graph */
void PUBLIC SetGraphEncodingMode(GraphPtr pGraph, enum pg_EncodingModes mode);
enum pg_EncodingModes PUBLIC GetGraphEncodingMode(GraphPtr pGraph);

#endif /* __PGSDK_API_ENCODING_H__ */
