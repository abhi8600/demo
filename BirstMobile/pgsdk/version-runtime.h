#ifndef __PGSDK_API_VERSION_RUNTIME_H__
#define __PGSDK_API_VERSION_RUNTIME_H__

#include "keywords.h"  //PUBLIC
#include "types.h"     //INT16
#include "version.h"   //PG_VERSION_*

/* function to obtain PGSDK version data */
INT16 PUBLIC pg_version_pgsdk(char *buf,int bufsize);
INT16 PUBLIC pg_version_cflags(char *buf,int bufsize);
INT16 PUBLIC pg_version_options(char *buf,int bufsize);
INT16 PUBLIC pg_version_libraries(char *buf,int bufsize);
INT16 PUBLIC pg_version_all(char *buf,int bufsize);
INT16 PUBLIC pg_version_pgsdk_print(void);
INT16 PUBLIC pg_version_cflags_print(void);
INT16 PUBLIC pg_version_options_print(void);
INT16 PUBLIC pg_version_libraries_print(void);
INT16 PUBLIC pg_version_all_print(void);

#define PG_VERSION_MAKE( major, minor, patch ) (((major)*1000 + (minor)) * 1000 + (patch))
#define PG_VERSION PG_VERSION_MAKE(PG_VERSION_MAJOR, PG_VERSION_MINOR, PG_VERSION_PATCH)

#endif /* __PGSDK_API_VERSION_RUNTIME_H__ */
