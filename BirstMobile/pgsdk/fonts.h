#ifndef __API_FONTS_H__
#define __API_FONTS_H__

#include "graphptr.h"

enum pg_FontAntialias {
	PG_FONTANTIALIAS_NEVER,
	PG_FONTANTIALIAS_ALWAYS,
	PG_FONTANTIALIAS_DEFAULT
};

void PUBLIC 
ConvertPointSizeToVirtual(
		DrawEnvPtr pDE, /* Ptr to draw environment. */
		INT16 *nSize, 
		REAL64 fDocHeight 
);

void PUBLIC
ConvertVirtualToPointSize(
		DrawEnvPtr  pDE,  /* Ptr to draw environment. */
		INT16 *nSize, 
		REAL64 fDocHeight 
);

#endif /* __API_FONTS_H__ */
