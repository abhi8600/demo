#ifndef __API_DATA_H__
#define __API_DATA_H__

#include "types.h"

/* 11jul2007jans: define data item classes to be used in GetGraphQDRDisplayValue */


/*
 * General format quantitative data items.
 */

#define GEN_VALUE 0

/*
 * Polar format quantitative data items.
 */

#define POLAR_VALUE_RO 0
#define POLAR_VALUE_TETA	1

/*
 * Series magnitude format quantitative data items.
 */

#define SERMAG_VALUE 0
#define SERMAG_LABEL 1
#define SERMAG_ERR_LOW 2
#define SERMAG_ERR_HIGH 3
#define SERMAG_ERR_MEDIAN 4

/*
 * Spectral map quantitative data items.
 */

#define SPEC_VALUE	0
#define SPEC_LABEL	1

/*
 * Scatter format quantitative data items.
 */

#define SCAT_VALUE_X	0
#define SCAT_VALUE_Y	1
#define SCAT_LABEL		2
#define SCAT_ERR_LOW	3
#define SCAT_ERR_HIGH	4

/*
 * 3D Scatter format quantitative data items.  The order (X, Z, Y) is due
 *	to the need to translate between user coordinates (vertical Z-axis) and 
 *	engine coordinates (vertical Y-axis).
 */

#define SCAT3D_VALUE_X	1
#define SCAT3D_VALUE_Y	2
#define SCAT3D_VALUE_Z	0
#define SCAT3D_LABEL	3

#if _HACK4ROB
#define SCAT3D_VALUE_Q	3
#endif

/*
 * 3D YZ RowLine format quantitative data items.
 */

#define YZ_3D_VALUE_Y	0
#define YZ_3D_VALUE_Z	1

/* TL:09/27/93 created a format for the bubble chart, it used to use the
 ** same as the 3D scatter, but the order didn't make sense to the user.
 ** So I brought new ordering to the bubble charts. They live in harmony
 ** now.
 */

#define BUBBLE_VALUE_X	0
#define BUBBLE_VALUE_Y	1
#define BUBBLE_VALUE_Z	2
#define BUBBLE_LABEL		3

/*
 * Stock market format quantitative data items.
 */

#define STOCK_VALUE_HIGH	0
#define STOCK_VALUE_LOW		1
#define STOCK_VALUE_OPEN	2
#define STOCK_VALUE_CLOSE	3
#define STOCK_VALUE_METRIC1	4
#define STOCK_VALUE_METRIC2	5
#define STOCK_VALUE_METRIC3	6

/*
 * BoxPlot format quantitative data items.
 */

#define HINGE_VALUE_UA	0    
#define HINGE_VALUE_UQ	1    
#define HINGE_VALUE_ME	2    
#define HINGE_VALUE_LQ	3    
#define HINGE_VALUE_LA	4    

/* TL:02/13/94 added the gantt chart */
/*
 * Gantt format quantitative data items.
 */

#define GANTT_START			0
#define GANTT_DURATION	1
#define GANTT_LABEL			2

/* Chart Intelligence: Product Position Analysis data items */
#define PPA_VALUE_X		0
#define PPA_VALUE_Y		1
#define PPA_VALUE_Z		2
#define PPA_VALUE_Q		3
#define PPA_LABEL		4

/* Chart Intelligence: Resource Return data items */
#define RR_VALUE_X		0
#define RR_VALUE_Y		1
#define RR_VALUE_X_MIN	2
#define RR_VALUE_X_MAX	3

/* Chart Intelligence: Time Series data items */
#define TS_VALUE_Y		0
#define TS_VALUE_F		1
#define TS_ERR_LOW		2
#define TS_ERR_HIGH		3

/*
 * CENTROID quantitative data items.
 */

#define CENTROID_X			0
#define CENTROID_X_MAX		1
#define CENTROID_X_MIN		2
#define CENTROID_Y			3
#define CENTROID_Y_MAX		4
#define CENTROID_Y_MIN		5

/* Allow users to retrieve CALCULATED datapoints, not just stored datapoints.
   These include such values as percentages on pie & percent charts, and histogram bucket counts. */
INT16 PUBLIC
GetGraphQDRDisplayValue(  
		GraphPtr	pGraph, 
		INT16		nSeriesID,
		INT16		nGroupID,
		REAL64		*prPercentValue,
		REAL64		*prValue,
		INT16		nItem
);

#endif /* __API_DATA_H__ */
