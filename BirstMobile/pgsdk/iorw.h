#ifndef __PGSDK_API_IORW_H__
#define __PGSDK_API_IORW_H__ 

#include <stdio.h>      //FILE
#include "keywords.h"   //PUBLIC
#include "types.h"

typedef struct IOPATH {
	FILE *					cookie;
	INT16						isRamFile;
	UCHAR *    			pBuffer;
	UCHAR *    			buffer;
	UINT32					nBufSize;
	UINT32					nBufIndex;
} IOPATH, * IOPATHPTR;

#define IO_RD_WR    TRUE  
#define IO_RD_ONLY 	FALSE 

INT16 PUBLIC
IOopen(
		IOPATHPTR				iopath,								/* ptr to the io path */
		LPTSTR					fileName,							/* c string filename */
		INT16				  	vRefNum,							/* volume ref num - used for Macintosh only */
		BOOLEAN16 			rw					  				/* T = read/write, F = read only */
);


INT16 PUBLIC
IOconnect(
		IOPATHPTR				iopath,								/* ptr to the io path */
		FILE *					cookie								/* file cookie */
);


INT16 PUBLIC
IOclose(
		IOPATHPTR				iopath								/* the file id */
);

INT16 PUBLIC
IOgetc(
		IOPATHPTR				iopath
);


LPTSTR PUBLIC  IOgets(
		IOPATHPTR		iopath,
		LPTSTR			buffer,
		INT32				bufsiz
);


INT32 PUBLIC
IOread(
		IOPATHPTR		iopath,
		void *    	buffer,
		INT32				bufsiz
);


INT32 PUBLIC
IOwrite(
		IOPATHPTR		iopath,
		void *    	buffer,
		INT32				bufsiz
);


#define IO_SEEK_BEG	0x0000  //From beginning of the file
#define IO_SEEK_CUR	0x0001  //From the current position
#define IO_SEEK_END 0x0002  //From the end of the file.


INT32 PUBLIC
IOseek(
		IOPATHPTR   ioPath,
		INT32       lBytes,
		INT16       nOrigin  //IO_SEEK_...
);


#endif /* __PGSDK_API_IORW_H__ */
