#ifndef __PGSDK_API_VECTORPRIMS_H__
#define __PGSDK_API_VECTORPRIMS_H__

#include "keywords.h"       //CALLBACK
#include "types.h"          //INT16 INT32 UINT32 REAL
#include "export-formats.h" //enum DevGen_ImageFormat
#include "encoding.h"       //pg_EncodingModes
#include "attr.h"           //AreaInstPtr FontInstPtr LineInstPtr ColorInstPtr
#include "iorw.h"           //IOPATHPTR

#include "vectorfills.h"    //VectorColourPtr VectorFillPtr VECTOR_TEXT_ALIGN

/////////////////////////////////////////////////////////////////
//VectorDrawPrimitives structure:
//(formerly known as StdDrawPrimitives)
//
//The set of primitive functions available to an outside caller are defined
//here. (Internally, gi_prim.h is included in std_draw.h and a member is 
//included in the DrawEnv - right after StdDrawProcs).
//
//For UNIX -  api's accepting an HDC as a first parameter are actually supplied
//        a pointer to the draw environment....when passed this way,
//        the pointer is currently ignored since outside callers don't 
//        generally have knowledge of the DrawEnv structure.
//
//Geoff's note: anywhere there's 'void *context', 'context' is the
//'poChildData' the child passed the Vector device during initialisation.
//If the child needs the device pointer, it should be placed in whatever
//'context' ends up being.
//
// 20jan2003jans : when altering this structure alter also it's instances 
//                 like svgVectorDrawPrimitives in svg/primitives/primitives.c
/////////////////////////////////////////////////////////////////



/* #1 starts a new path */
typedef INT16 (CALLBACK * LPFN_pathStartNew) (
		void *           context,
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #2 - finishes a path and paints it.  If prStroke is non-NULL, the 'line' of
 * the path is painted that colour.  If prFill is non-NULL, the shape is filled
 * with that colour.  Note: this doesn't automatically close the subpath (ie,
 * join the first and last points.) */
typedef INT16 (CALLBACK * LPFN_pathFinishAndPaint) (
		void *           context,
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #3 - Closes the current subpath by joining the current point with the first. */
typedef INT16 (CALLBACK * LPFN_pathCloseSubpath) (
		void *           context
);


/* #4 - finishes a path and paints it.  If prStroke is non-NULL, the 'line' of
 * the path is painted that colour.  If prFill is non-NULL, the shape is filled
 * with that colour.  */
typedef INT16 (CALLBACK * LPFN_pathSetLineWidth) (
		void *           context,
		REAL             width
);

/* #5 - Sets the line dash pattern.  dashArray is an array of integers
 * specificing the lengths of alternating dashes and gaps.  For example: 
 * [ 1 1 ]    X.X.X.X.X.X.X.X.X.X.    pathSetLineDashPattern(0, 2, {1, 1}) 
 * [ 1 2 ]    X..X..X..X..X..X..X.    pathSetLineDashPattern(0, 2, {1, 2}) 
 * [ 1 2 3 4] X..XXX....X..XXX....    pathSetLineDashPattern(0, 4, {1, 2, 3, 4})
 *
 * arrayLength is the length of the array 'dashArray', 
 * and 'startOffset' is the number of steps to skip initially.  Eg: 
 * [ 1 1 ]    .X.X.X.X.X.X.X.X.X.X    pathSetLineDashPattern(1, 2, {1, 1}) 
 * [ 1 2 ]    X..X..X..X..X..X..X.    pathSetLineDashPattern(0, 2, {1, 2}) 
 * [ 1 2 3 4] XXX....X..XXX....X..    pathSetLineDashPattern(3, 4, {1, 2, 3, 4})
 *
 * If you set arrayLength = 0, and dashArray = NULL, this will remove any
 * pattern on the current path.  */
typedef INT16 (CALLBACK * LPFN_pathSetLineDashPattern) (
		void *           context,
		INT32            startOffset,
		INT32            arrayLength,
		REAL *           dashArray                /* type changed in API v2 */
);

/* #6 - Moves the path to (x,y) without drawing anything.  */
typedef INT16 (CALLBACK * LPFN_pathMoveTo) (
		void *           context,
		REAL             x,
		REAL             y
);

/* #7 - Draws a straight line from the current position to (x,y).  */
typedef INT16 (CALLBACK * LPFN_pathLineTo) (
		void *           context,
		REAL             x,
		REAL             y
);

/* #8 - Draws a Cubic B�zier from the current position to (x,y).  The focus
 * points are (f1x, f1y) and (f2x, f2y) for the current point and (x,y)
 * respectively.  */
typedef INT16 (CALLBACK * LPFN_pathBezierTo) (
		void *           context,
		REAL             x,
		REAL             y,
		REAL             f1x,
		REAL             f1y,
		REAL             f2x,
		REAL             f2y
);

/* #9 - Gets the size of the string szText, if it were draw in dFontSize.
 * Returns values in pdWidth and pdHeight.  */

typedef INT16 (CALLBACK * LPFN_getTextSize) (
		void *           context,
		char const *     szText,
		REAL             dFontSize,
		REAL *           pdWidth
);

/* #10 - Gets info for the font with the size dFontSize.  pdAscent, pdDescent,
 * and pdLeading can be NULL if the caller is not interested in them.  */
typedef INT16 (CALLBACK * LPFN_getFontInfo) (
		void *           context,
		REAL             dFontSize,
		REAL *           pdAscent,
		REAL *           pdDescent,
		REAL *           pdLeading
);

/* #11 - Draws the character c at (x,y) at dFontSize and in the colours
 * suggested by prStroke and prFill. dRotation is expressed in degrees (0-360).  */
typedef INT16 (CALLBACK * LPFN_drawString) (
		void *           context,
		char const *     c,
		REAL             x,
		REAL             y,
		REAL             dFontSize,
		REAL             dRotation,
		VectorColourPtr  prStroke,
		VectorColourPtr  prFill
);

/* #12 - currently unused - no need to implement
 * Draws the character c at (x,y) at dFontSize and in the colours
 * suggested by prStroke and prFill.  */
typedef INT16 (CALLBACK * LPFN_drawTextBox) (
		void *           context,
		char const *     c,
		REAL             x,
		REAL             y,
		REAL             width,
		REAL             height,
		enum VECTOR_TEXT_ALIGN justification,
		REAL             dFontSize,
		VectorColourPtr  prStroke,
		VectorColourPtr  prFill
);

/* #13 - Draws the rectangle. */
typedef INT16 (CALLBACK * LPFN_Rectangle) (    /* types changed in API v2 */
		void *           context,     // handle of device context  
		REAL             nLeftRect,   // bounding rectangle's upper-left corner (x)
		REAL             nTopRect,    // bounding rectangle's upper-left corner (y)
		REAL             nRightRect,  // bounding rectangle's lower-right corner (x)
		REAL             nBottomRect, // bounding rectangle's lower-right corner (y)
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #14 - Called on the begining - before any other function is called
 * used to initialize the output, size, format, etc. */
typedef INT16 (CALLBACK * LPFN_DrawStart) ( 
		void *                   pcontext,     // handle of device context  
		enum DevGen_ImageFormat  i_eFormat,
		IOPATHPTR                direct_output,
		REAL                     image_width,
		REAL                     image_height,
		enum pg_EncodingModes    nEncodingMode
);

/* #15 - Called on the begining - before any other function is called
 * used to initialize the output, size, format, etc. */
typedef INT16 (CALLBACK * LPFN_DrawEnd) ( 
		void *           pcontext     // handle of device context  
);

/* #17 - path draw arc function */
/* parametrs as in http://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands */
typedef INT16 (CALLBACK * LPFN_pathArcTo) (   
		void *           pcontext,
		REAL             x,
		REAL             y,
		REAL             rx, 
		REAL             ry,
		REAL             xrotation, 
		INT32            large_arc,
		INT32            positive
);

/* #18 - alternative to pathArcTo and used when pathArcTo is set to NULL
	 x,y - centre of ellipse; startangle,endangle from 0 to 360 (angle 0 is on
	 three o'clock & angle 90 is on 12 o'clock) defines range of arc to draw,
	 clockwise or counter-clockwise */
typedef INT16 (CALLBACK * LPFN_pathArcTo_Alt) (
		void *           context,
		REAL             x,
		REAL             y,
		REAL             rx,
		REAL             ry,
		REAL             startangle,
		REAL             endangle,
		INT32            clockwise
);

/* #24 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetAreaColors) (
		void *           context,
		AreaInstPtr      pArea
);

/* #25 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetLineColors) (
		void *           context,
		LineInstPtr      pLine
);

/* #26 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetFontColors) (
		void *           context,
		FontInstPtr      pFont
);

/* #27 - */
typedef INT16 (CALLBACK * LPFN_SetFont) (
		void *           context,
		char *           lpszFontname,
		REAL             nPointSize,   /* type changed in API v2 */
		FontInstPtr      pFont
);

/* #28 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_Arc) ( /* types changed in API v2 */
		void *           context,     // handle to device context
		REAL             nLeftRect,   // bounding rectangle's upper-left corner (x)
		REAL             nTopRect,    // bounding rectangle's upper-left corner (y)
		REAL             nRightRect,  // bounding rectangle's lower-right corner (x)
		REAL             nBottomRect, // bounding rectangle's lower-right corner (y)
		REAL             nXStartArc,  // first radial ending point
		REAL             nYStartArc,  // first radial ending point
		REAL             nXEndArc,    // second radial ending point
		REAL             nYEndArc,    // second radial ending point
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #29 - information only - implement if you need that information */
/* Pass the nformation about the object the will be drawn aftrwards */
typedef INT16 (CALLBACK * LPFN_StartObject) (
		void *           context,     // handle to device context
		INT16     nObjectID,
		INT16     nSeriesID,
		INT16     nGroupID,
		const char *    comment       //aditional comment (can be NULL)
);

/* #33 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_DrawText) (
		void *           context,     // handle to device context
		char *           lpString,    // pointer to string to draw
		INT32            nCount,      // string length, in characters
		REAL *           lpRect,      // pointer to struct with formatting dimensions
		UINT32           uFormat      // text-drawing flags
);

/* #34 - draws x-y oriented (unrotated) ellipse */
typedef INT16 (CALLBACK * LPFN_Ellipse) ( /* types changed in API v2 */
		void *           context,     // handle to device context  
		REAL             nLeftRect,   // bounding rectangle's upper-left corner (x) 
		REAL             nTopRect,    // bounding rectangle's upper-left corner (y)
		REAL             nRightRect,  // bounding rectangle's lower-right corner (x)
		REAL             nBottomRect, // bounding rectangle's lower-right corner (y)
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #35 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_ExtTextOut) (
		void *           context,     // handle to device context
		REAL             X,           // x-coordinate of reference point
		REAL             Y,           // y-coordinate of reference point
		UINT32           fuOptions,   // text-output options
		const REAL *     lprc,        // optional clipping and/or opaquing rectangle
		char *           lpString,    // points to string
		UINT32           cbCount,     // number of characters in string
		const REAL *     lpDx         // pointer to array of intercharacter spacing values
);

/* #38 - Gets the clipping rectangle. */
typedef INT16 (CALLBACK * LPFN_GetClipRect) ( 
		void *           context,     // handle of device context  
		REAL *           nLeftRect,   // clipping rectangle's upper-left corner (x)
		REAL *           nTopRect,    // clipping rectangle's upper-left corner (y)
		REAL *           nRightRect,  // clipping rectangle's lower-right corner (x)
		REAL *           nBottomRect  // clipping rectangle's lower-right corner (y)
);



/* #39 - currently unused - no need to implement */
typedef VectorColour (CALLBACK * LPFN_GetPixel) (
		void *           context,     // handle to device context
		INT32            XPos,        // x-coordinate of pixel  
		INT32            nYPos        // y-coordinate of pixel
);

/* #40 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_GetTextExtentPoint) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		char *           lpString,    // address of text string
		INT32            cbString,    // number of characters in string
		REAL *           lpSzie       // address of structure for string size
);

/* #42 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_GetTextFace) (
		void *           context,     // handle of device context
		INT32            nCount,      // length of buffer receiving typeface name
		char *           lpFaceName   // buffer receiving typeface name
);

/* #44 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_GetViewportExtEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		REAL *           lpSize       // structure receiving viewport dimensions
);

/* #45 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_GetWindowExtEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		REAL *           lpSize       // structure receiving window extents
);


/* #55 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_RoundRect)  ( /* types changed in API v2 */
		void *           context,     // handle of device context
		REAL             nLeftRect,   // bounding rectangle's upper-left corner
		REAL             nTopRect,    // bounding rectangle's upper-left corner
		REAL             nRightRect,  // bounding rectangle's lower-right corner  
		REAL             nBottomRect, // bounding rectangle's lower-right corner
		REAL             nWidth,      // width of ellipse used to draw rounded corners
		REAL             nHeight,     // height of ellipse used to draw rounded corners
		VectorColourPtr  prStroke,
		VectorFillPtr    prFill
);

/* #58 - Sets the clipping rectangle. */
typedef INT16 (CALLBACK * LPFN_SetClipRect) ( 
		void *           context,     // handle of device context  
		REAL             nLeftRect,   // clipping rectangle's upper-left corner (x)
		REAL             nTopRect,    // clipping rectangle's upper-left corner (y)
		REAL             nRightRect,  // clipping rectangle's lower-right corner (x)
		REAL             nBottomRect  // clipping rectangle's lower-right corner (y)
);


/* #63 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetPixel) ( /* types changed in API v2 */
		void *           context,     // handle to device context
		REAL             X,           // x-coordinate of pixel
		REAL             Y,           // y-coordinate of pixel  
		VectorColourPtr  crColor      // pixel color
);

/* #68 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetViewportOrgEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		INT32            X,           // new x-coordinate of viewport origin
		INT32            Y,           // new y-coordinate of viewport origin  
		REAL *           lpPoint      // structure receiving original origin
);

/* #69 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetViewportExtEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		INT32            nXExtent,    // new horizontal viewport extent
		INT32            nYExtent,    // new vertical viewport extent
		REAL *           lpSize       // original viewport extent
);

/* #70 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetWindowOrgEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		INT32            X,           // new x-coordinate of window origin
		INT32            Y,           // new y-coordinate of window origin  
		REAL *           lpPoint      // structure receiving original origin
);

/* #71 - currently unused - no need to implement */
typedef INT16 (CALLBACK * LPFN_SetWindowExtEx) ( /* types changed in API v2 */
		void *           context,     // handle of device context
		INT32            nXExtent,    // new horizontal window extent
		INT32            nYExtent,    // new vertical window extent
		REAL *           lpSize       // original window extent
);

/* unused -- reserved for future use */
typedef INT16 (CALLBACK * LPFN_uu) (
		void *           context
);


/*
	 For working vector device, it is necessary to implement primitives:
	 #1, #2, #3, #4, #5, #6, #7, #8, #9, #10, #11, #13, #17 or #18, #27, #34
   Also: #38, #58
 */

typedef struct VectorPrimativesStruct {
	/* #1*/ LPFN_pathStartNew       __pathStartNew;
	/* #2*/ LPFN_pathFinishAndPaint __pathFinishAndPaint;
	/* #3*/ LPFN_pathCloseSubpath   __pathCloseSubpath;
	/* #4*/ LPFN_pathSetLineWidth   __pathSetLineWidth;
	/* #5*/ LPFN_pathSetLineDashPattern __pathSetLineDashPattern;
	/* #6*/ LPFN_pathMoveTo         __pathMoveTo;
	/* #7*/ LPFN_pathLineTo         __pathLineTo;
	/* #8*/ LPFN_pathBezierTo       __pathBezierTo;
	/* #9*/ LPFN_getTextSize        __getTextSize;
	/*#10*/ LPFN_getFontInfo        __getFontInfo;
	/*#11*/ LPFN_drawString         __drawString;
	/*#12*/ LPFN_drawTextBox        __drawTextBox;        /* do not implement */
	/*#13*/ LPFN_Rectangle          __Rectangle;
	/*#14*/ LPFN_DrawStart          __DrawStart;          
	/*#15*/ LPFN_DrawEnd            __DrawEnd;            
	/*#16*/ LPFN_uu                 __uu16;               /* do not implement - unused */
	/*#17*/ LPFN_pathArcTo          __pathArcTo;
	/*#18*/ LPFN_pathArcTo_Alt      __pathArcTo_Alt;
	/*#19*/ LPFN_uu                 __uu19;               /* do not implement - unused */
	/*#20*/ LPFN_uu                 __uu20;               /* do not implement - unused */
	/*#21*/ LPFN_uu                 __uu21;               /* do not implement - unused */
	/*#22*/ LPFN_uu                 __uu22;               /* do not implement - unused */
	/*#23*/ LPFN_uu                 __uu23;               /* do not implement - unused */
	/*#24*/ LPFN_SetAreaColors      __SetAreaColors;      /* do not implement */
	/*#25*/ LPFN_SetLineColors      __SetLineColors;      /* do not implement */
	/*#26*/ LPFN_SetFontColors      __SetFontColors;      /* do not implement */
	/*#27*/ LPFN_SetFont            __SetFont;
	/*#28*/ LPFN_Arc                __Arc;                /* do not implement */
	/*#29*/ LPFN_StartObject        __StartObject;
	/*#30*/ LPFN_uu                 __uu30;               /* do not implement - reserved for eventual EndObject */
	/*#31*/ LPFN_uu                 __uu31;               /* do not implement - unused */
	/*#32*/ LPFN_uu                 __uu32;               /* do not implement - unused */
	/*#33*/ LPFN_DrawText           __DrawText;           /* do not implement */
	/*#34*/ LPFN_Ellipse            __Ellipse;
	/*#35*/ LPFN_ExtTextOut         __ExtTextOut;         /* do not implement */
	/*#36*/ LPFN_uu                 __uu36;               /* do not implement - unused */
	/*#37*/ LPFN_uu                 __uu37;               /* do not implement - unused */
	/*#38*/ LPFN_GetClipRect        __GetClipRect;
	/*#39*/ LPFN_GetPixel           __GetPixel;           /* do not implement */
	/*#40*/ LPFN_GetTextExtentPoint __GetTextExtentPoint; /* do not implement */
	/*#41*/ LPFN_uu                 __uu41;               /* do not implement - unused */
	/*#42*/ LPFN_GetTextFace        __GetTextFace;        /* do not implement */
	/*#43*/ LPFN_uu                 __uu43;               /* do not implement - unused */
	/*#44*/ LPFN_GetViewportExtEx   __GetViewportExtEx;   /* do not implement */
	/*#45*/ LPFN_GetWindowExtEx     __GetWindowExtEx;     /* do not implement */
	/*#46*/ LPFN_uu                 __uu46;               /* do not implement - unused */
	/*#47*/ LPFN_uu                 __uu47;               /* do not implement - unused */
	/*#48*/ LPFN_uu                 __uu48;               /* do not implement - unused */
	/*#49*/ LPFN_uu                 __uu49;               /* do not implement - unused */
	/*#50*/ LPFN_uu                 __uu50;               /* do not implement - unused */
	/*#51*/ LPFN_uu                 __uu51;               /* do not implement - unused */
	/*#52*/ LPFN_uu                 __uu52;               /* do not implement - unused */
	/*#53*/ LPFN_uu                 __uu53;               /* do not implement - unused */
	/*#54*/ LPFN_uu                 __uu54;               /* do not implement - unused */
	/*#55*/ LPFN_RoundRect          __RoundRect;          /* do not implement */
	/*#56*/ LPFN_uu                 __uu56;               /* do not implement - unused */
	/*#57*/ LPFN_uu                 __uu57;               /* do not implement - unused */
	/*#58*/ LPFN_SetClipRect        __SetClipRect;
	/*#59*/ LPFN_uu                 __uu59;               /* do not implement - unused */
	/*#60*/ LPFN_uu                 __uu60;               /* do not implement - unused */
	/*#61*/ LPFN_uu                 __uu61;               /* do not implement - unused */
	/*#62*/ LPFN_uu                 __uu62;               /* do not implement - unused */
	/*#63*/ LPFN_SetPixel           __SetPixel;           /* do not implement */
	/*#64*/ LPFN_uu                 __uu64;               /* do not implement - unused */
	/*#65*/ LPFN_uu                 __uu65;               /* do not implement - unused */
	/*#66*/ LPFN_uu                 __uu66;               /* do not implement - unused */
	/*#67*/ LPFN_uu                 __uu67;               /* do not implement - unused */
	/*#68*/ LPFN_SetViewportOrgEx   __SetViewportOrgEx;   /* do not implement */
	/*#69*/ LPFN_SetViewportExtEx   __SetViewportExtEx;   /* do not implement */
	/*#70*/ LPFN_SetWindowOrgEx     __SetWindowOrgEx;     /* do not implement */
	/*#71*/ LPFN_SetWindowExtEx     __SetWindowExtEx;     /* do not implement */
	/*#72*/ LPFN_uu                 __uu72;               /* do not implement - unused */
	/*#73*/ LPFN_uu                 __uu73;               /* do not implement - unused */
} VectorDrawPrimitives, *VectorDrawPrimitivesPtr;


#endif /* __PGSDK_API_VECTORPRIMS_H__ */
