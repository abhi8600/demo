#ifndef __PGSDK_API_TYPES_H__
#define __PGSDK_API_TYPES_H__

#if !defined(_WINDOWS)
typedef char               BYTE;

# ifndef _TCHAR_DEFINED
#  define _TCHAR_DEFINED
typedef char        TCHAR;
typedef char        _TCHAR;
#  define __T(x)      x
# endif
# define _T(x)       __T(x)
#endif

typedef unsigned char      UCHAR;


#ifndef _DEF_INT16
# define _DEF_INT16
typedef short int          INT16; 
#endif

#ifndef _DEF_UINT16
# define _DEF_UINT16
typedef unsigned short int UINT16;
#endif

typedef short int          BOOLEAN16;

/* 17mar2004jans:FIXME what about 64-bit  */
#ifndef _DEF_INT32
# define _DEF_INT32
# define XMD_H /* 13jun2003jans: for jmorecfg.h */
# if !defined (_MSC_VER) ||  _MSC_VER < 1200 /* MSVC 6+ (--> 1200) defines UINT32 */
typedef int                INT32;
# endif
#endif

#ifndef _DEF_UINT32
# define _DEF_UINT32
# if !defined (_MSC_VER) ||  _MSC_VER < 1200 /* MSVC 6+ (--> 1200) defines UINT32 */
typedef unsigned int       UINT32;
# endif
#endif

typedef INT32              FIXED32;

//pointers
typedef BOOLEAN16 *        PBOOLEAN16;
typedef INT16 *            PINT16;
typedef INT32 *            PINT32;
typedef UINT16 *           PUINT16;
typedef UINT32 *           PUINT32;
typedef INT32 *            PFIXED32;

//float
typedef double             REAL;      /* fastest floating point representation */
typedef double             REAL64;    /* 64-bit floating point value */

typedef double *           PREAL;     /* ptr to fastest floating point representation */
typedef double *           PREAL64;   /* ptr to 64-bit floating point value */

#if !defined (_MSC_VER) ||  _MSC_VER < 1200
typedef PREAL64            PINT64;
#endif


#define MAX_REAL ((REAL) 1.7976931348623158e+308)
#define MIN_REAL ((REAL) 2.2250738585072014e-308)

#define MAX_REAL64 ((REAL) 1.7976931348623158e+308)
#define MIN_REAL64 ((REAL) 2.2250738585072014e-308)


//string
//typedef char *                LPSTR;
#ifndef _WINDOWS
typedef char *    LPTSTR;
typedef char *    LPCTSTR;
#endif

//pointer
typedef void *  MEMPTR;
typedef MEMPTR * SFXHDL; //struct tagSFXRecord
typedef struct tagSFXRecord * SFXPtr;
typedef MEMPTR *MEMHDL;
#ifndef _WINDOWS
typedef MEMPTR * HDC; 
typedef MEMPTR  HANDLE; 
#endif

//special
#ifndef _WINDOWS
typedef long int    LCID;
#endif

typedef INT16 PG_UNUSED;

#endif /* __PGSDK_API_TYPES_H__ */
