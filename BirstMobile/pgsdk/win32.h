#ifndef __PGSDK_API_WIN32_H__
#define __PGSDK_API_WIN32_H__

/************************************************************************
 *  StdDraw Port Types
 ************************************************************************/
enum pg_de_port_type {
	DE_PORT_NULL,
	DE_PORT_NORMAL,
	DE_PORT_OFFSCREEN,
	DE_PORT_PICTURE,
	DE_PORT_PIP,
	DE_PORT_PRINTING,
	DE_PORT_COREL_WPG, /*  Draw to Corel WPG (using their GI library) 3-20-98/FL */
	DE_PORT_ENHMETAFILE,/*  For enhanced metafile, we use clipping,  */
	/*  but still draw when not visible 5-28-99/FL */
	/*  1999sep22 PPN: for proper processing of the 2D parts */
	/*  (ie, title text sfx, etc.) of an OpenGL chart. */
	DE_PORT_OPENGL,
	DE_PORT_GLPICTURE,
	DE_PORT_MAX

};

#endif /* __PGSDK_API_WIN32_H__ */
