#ifndef __API_OID_ENUM_H__
#define __API_OID_ENUM_H__

/************************************************************************
 * from CRO_DEFS.H
 ************************************************************************/

#define O_RANGE_BEGIN			1
#define O_RANGE_END				255

#define O2D_RANGE_BEGIN		256
#define O2D_RANGE_END			511

#define O3D_RANGE_BEGIN		512
#define O3D_RANGE_END			767

#define OTX_RANGE_BEGIN		768
#define OTX_RANGE_END			1023

#define OAN_RANGE_BEGIN		1024
#define OAN_RANGE_END			1279

#define O2D_RANGE2_BEGIN	1280
#define O2D_RANGE2_END		1536

/*********************************
 *  C o m m o n   O b j e c t s  *
 *********************************/
typedef enum _O5DType {
	O5D_BACKGROUND = O_RANGE_BEGIN,
	O5D_FRAME,
	O5D_LBLFOOTNOTE,
	O5D_LBLSUBTITLE,
	O5D_LBLTITLE,

	OSG_LEGEND_AREA,
	OSG_LEGEND_LINE,
	OSG_LEGEND_MARKER,
	OSG_LEGEND_TEXT,

	O5D_CHARTMASTER_FRAME,
	O5D_CHARTMASTER_LBLTITLE,
	O5D_CHARTMASTER_LBLSUBTITLE,
	O5D_CHARTMASTER_LBLFOOTNOTE,

	O5D_FRAME_ALT,

	O5DTYPEMAX
} O5DType;
#define O5DCOUNT ((int) O5DTYPEMAX)

#if (O5DTYPEMAX > O_RANGE_END)
#error cro_defs.h: (O5DTYPEMAX > O_RANGE_END)
#endif

/************************************************************************
 * from C2D_DEFS.H
 ************************************************************************/

/*
 * Some 2D graph terms defined:
 *		Group						-	Generally a column of data.
 *		Primary axis		-	The only numeric axis on charts with only one.
 *											Applies to the left hand data set in dual Y charts,
 *											and the Y axis on scatter charts.
 *		Secondary axis	-	Numeric axis scaling the right hand data set for dual Y charts
 *											and the X axis on scatter charts.
 *		Series					-	Generally a row of data.
 *		Surface					- Any rectanglar area of the charting viewspace.
 */

typedef enum _ObjClass2D {
	O2D_AREARISER = O2D_RANGE_BEGIN,			/* Quantitative data representation on an area chart. */
	O2D_CURVE,							/* Quantitative data series interpolation line. */
	O2D_DATALINE,						/* Line connecting data markers of a line chart. */
	O2D_DATAMARKER,					/* Quantitative data representation point. */
	O2D_DATATEXT,
	O2D_DIVBIPOLAR,					/* Line dividing bipolar chart surfaces. */
	O2D_ERRORBAR,						/* */

	O2D_LINR_EXP,
	O2D_LINR_LINE,
	O2D_LINR_LOG,
	O2D_LINR_NATLOG,
	O2D_LINR_NPOLY,
	O2D_LINR_TEXT,

	O2D_MEANLINE,						/* Histogram mean line. */
	O2D_MOVAVGLINE,					/* Quantitative data series moving average line. */

	O2D_O1_BODY,
	O2D_O1_LABEL,
	O2D_O1_MAJOR,
	O2D_O1_MINOR,
	O2D_O1_SUPER,						/* TL:11/17/92 added super grid line for bars */
	O2D_O1_TITLE,

	O2D_O2_BODY,
	O2D_O2_LABEL,
	O2D_O2_MAJOR,
	O2D_O2_MINOR,
	O2D_O2_TITLE,


	O2D_PRESDL_CONN,
	O2D_PRESDL_CONNBRK,
	O2D_PRESDL_STEP,
	O2D_PRESDL_STEPBRK,

	O2D_RISER,							/* Quantitative data representation of a bar chart. */
	O2D_RISERSHADOW,				/* Visual effect for bar data representation. */
	O2D_STDDEVLINE,					/* Histogram standard deviation line(s). */

	O2D_X1_BODY,
	O2D_X1_LABEL,
	O2D_X1_MAJOR,
	O2D_X1_MINOR,
	O2D_X1_TITLE,
	O2D_X1_ZERO,

	O2D_Y1_BODY,
	O2D_Y1_LABEL,
	O2D_Y1_MAJOR,
	O2D_Y1_MINOR,
	O2D_Y1_TITLE,
	O2D_Y1_ZERO,

	O2D_Y2_BODY,
	O2D_Y2_LABEL,
	O2D_Y2_MAJOR,
	O2D_Y2_MINOR,
	O2D_Y2_TITLE,
	O2D_Y2_ZERO,

	OBP_CAT_COLDIVISORS, 
	OBP_CAT_COLHEADAREA, 
	OBP_CAT_COLMARKER,	 
	OBP_CAT_DATAAREA,		 
	OBP_CAT_LBLCELL,		 
	OBP_CAT_LBLCOLHEAD,	 
	OBP_CAT_LBLROWHEAD,	 
	OBP_CAT_LBLROWTITLE, 
	OBP_CAT_ROWDIVISORS, 
	OBP_CAT_ROWHEADAREA, 
	OBP_CAT_SUBJECTAREA, 

	OBP_GRP_COLDIVISORS, 
	OBP_GRP_COLHEADAREA, 
	OBP_GRP_COLMARKER,	 
	OBP_GRP_DATAAREA,		 
	OBP_GRP_LBLCELL,		 
	OBP_GRP_LBLCOLHEAD,	 
	OBP_GRP_LBLROWHEAD,	 
	OBP_GRP_LBLROWTITLE, 
	OBP_GRP_ROWDIVISORS, 
	OBP_GRP_ROWHEADAREA, 
	OBP_GRP_SUBJECTAREA, 

	OBP_BOX,
	OBP_MEDIAN,
	OBP_STEM,
	OBP_TAIL,

	OCT_LBLCONTOUR,				/* Contour levels Label 	*/
	OCT_LBLCONTOURPOINT,	/* Label associated with a Contour Point32	*/
	OCT_LINECONTOUR,			/* Contour Lines*/
	OCT_MRKCONTOURPOINT,	/* Marker for contour point */

	OPI_FEELER,
	OPI_LBLFEELER,
	OPI_LBLPIE,
	OPI_LBLRING,
	OPI_SLICE,
	OPI_SLICECRUST,
	OPI_SLICEFACE,
	OPI_SLICERING,
	OPI_BARCONNECT,				/* TL:05/06/93 line connects pie to bar in pie bar */

	OPL_CIRCLEPOLARAXIS,	/* Circle polar axis. */
	OPL_LBLPOLARAXIS,			/* Polar Axis Label 	*/
	OPL_LINES,						/* Polar Lines. */
	OPL_THICKPOLARAXIS,		/* Polar Axis Thick 	*/

	OSM_RISER,

	OSP_SCALE_MARKER,

	OTC_COLDIVISORS,			/* Lines separating full chart columns. */
	OTC_COLHEADAREA,			/* Surface beneath the column headers of a table chart. */
	OTC_COLMARKER,				/* */
	OTC_DATAAREA,					/* Surface beneath the data text of a table chart. */
	OTC_LBLCELL,					/* Data text of a table chart. */
	OTC_LBLCOLHEAD,				/* Column header text of a table chart. */
	OTC_LBLROWHEAD,				/* Row header text of a table chart. */
	OTC_LBLROWTITLE,			/* Row title text associated with row headers. */
	OTC_ROWDIVISORS,			/* Lines separating full chart rows. */
	OTC_ROWHEADAREA,			/* Surface beneath the row headers of a table chart. */
	OTC_SUBJECTAREA,			/* Surface just above the row header surface area. */
	OBL_QUADRANT_LINE,			/* 28sep94/DW: Add support for BUBBLE CHARTS */

	O2D_MD_LABEL0,				/* Multi-Dimensional Ordinal Axis Label Id's  26dec96DW */				
	O2D_MD_LABEL1,				
	O2D_MD_LABEL2,				
	O2D_MD_LABEL3,				
	O2D_MD_LABEL4,				
	O2D_MD_LABEL5,				
	O2D_MD_LABEL6,				
	O2D_MD_LABEL7,				
	O2D_MD_LABEL8,				
	O2D_MD_LABEL9,				
	O2D_MD_LABEL10,				
	O2D_MD_LABEL11,				
	O2D_MD_LABEL12,				
	O2D_MD_LABEL13,				
	O2D_MD_LABEL14,				
	O2D_MD_LABEL15,				
	O2D_MD_LABEL16,				
	O2D_MD_LABEL17,				
	O2D_MD_LABEL18,				
	O2D_MD_LABEL19,				
	O2D_MD_LABEL20,				
	O2D_MD_LABEL21,				
	O2D_MD_LABEL22,				
	O2D_MD_LABEL23,				
	O2D_MD_LABEL24,				
	O2D_MD_LABEL25,				
	O2D_MD_LABEL26,				
	O2D_MD_LABEL27,				
	O2D_MD_LABEL28,				
	O2D_MD_LABEL29,				
	O2D_MD_LABEL30,				
	O2D_MD_LABEL31,				
	O2D_RISER2,
	O2D_RISER3,
	O2D_SERIES_TITLE,			/* 20mar98DW: Added series title for legends */
	O2D_CONNECT_STACKLINE,		/* 1apr98DW: For COREL */
	O2D_WATERFALL_SERIES_LABEL,	/* 15oct98DW: For Waterfall charts */
	O2D_DATATEXT_STACK_TOTAL,  /* 15oct98DW: For Waterfall charts */
	O2D_DATATEXT_RO1,			/* 30oct98DW: Range Object BEGIN 1 */
	O2D_DATATEXT_RO2,			/* 30oct98DW: Range Object BEGIN 2 */
	O2D_DATATEXT_SO1,			/* 18nov98DW: Split Object END 1 */
	O2D_DATATEXT_SO2,			/* 18nov98DW: Split Object END 2 */
	O2D_DATATEXT_SO3,			/* 18nov98DW: Split Object END 1 */
	O2D_DATATEXT_SO4,			/* 18nov98DW: Split Object END 2 */
	O2D_RISER_RO1,				/* 30oct98DW: Range Object #1 */
	O2D_RISER_RO2,				/* 30oct98DW: Range Object #2 */
	O2D_RISER_SO1,				/* 18nov98DW: Split Object #1 */
	O2D_RISER_SO2,				/* 18nov98DW: Split Object #2 */
	O2D_RISER_SO3,				/* 18nov98DW: Split Object #3 */
	O2D_RISER_SO4,				/* 18nov98DW: Split Object #4 */
#if 0 /* 06nov2007jans: in Cognos build */
	O2D_USERLINE,				/* 10dec97DW: User-defined lines */
#else /* 06nov2007jans: in MS build! */
	O2D_USERLINE1,		/* 10dec97DW: User-defined lines */
	O2D_USERLINE2,		/* 10dec97DW: User-defined lines */
	O2D_USERLINE3,		/* 10dec97DW: User-defined lines */
	O2D_USERLINE4,		/* 10dec97DW: User-defined lines */
#endif
	OPI_LBL_VALUE,		/* 31mar99DW: Independent Value Label for Pie Slice */
	O2D_QUALITYAXIS_LABEL_Y1,	/*14dec99DW: QualityAxis Label */
	O2D_QUALITYAXIS_LABEL_Y2,	/*14dec99DW: QualityAxis Label */
	O2D_QUALITYAXIS_LABEL_X,	/*14dec99DW: QualityAxis Label */
	O2D_QUALITYAXIS_FILL_LOW_Y1,	/*14dec99DW: QualityAxis Low band fill color */
	O2D_QUALITYAXIS_FILL_LOW_Y2,	/*14dec99DW: QualityAxis Low band fill color */
	O2D_QUALITYAXIS_FILL_LOW_X,	/*14dec99DW: QualityAxis Low band fill color */
	O2D_QUALITYAXIS_FILL_MEDIUM_Y1,	/*14dec99DW: QualityAxis Medium band fill color */
	O2D_QUALITYAXIS_FILL_MEDIUM_Y2,	/*14dec99DW: QualityAxis Medium band fill color */
	O2D_QUALITYAXIS_FILL_MEDIUM_X,	/*14dec99DW: QualityAxis Medium band fill color */
	O2D_QUALITYAXIS_FILL_HIGH_Y1,	/*14dec99DW: QualityAxis High band fill color */
	O2D_QUALITYAXIS_FILL_HIGH_Y2,	/*14dec99DW: QualityAxis High band fill color */
	O2D_QUALITYAXIS_FILL_HIGH_X,	/*14dec99DW: QualityAxis High band fill color */
	O2D_DATATEXT_CONNECTLINE,		/*9feb00DW: Connection line between datatext and riser/marker */
	O2D_PROGRESSION_ARROW,			/*9feb00DW: Arrow between markers on a Product Positioning Chart */
	O2D_QUALITYOBJECT_1,			/*8feb00DW: QualityObject on a Product Pos. Chart */
	O2D_QUALITYOBJECT_2,			/*8feb00DW: QualityObject on a Product Pos. Chart */
	O2D_QUALITYOBJECT_3,			/*8feb00DW: QualityObject on a Product Pos. Chart */
	O2D_QUALITYOBJECT_4,			/*8feb00DW: QualityObject on a Product Pos. Chart */
	O2D_QUALITYOBJECT_5,			/*8feb00DW: QualityObject on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LABEL_1,		/*8feb00DW: QualityObject's Legend Label on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LABEL_2,		/*8feb00DW: QualityObject's Legend Label on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LABEL_3,		/*8feb00DW: QualityObject's Legend Label on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LABEL_4,		/*8feb00DW: QualityObject's Legend Label on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LABEL_5,		/*8feb00DW: QualityObject's Legend Label on a Product Pos. Chart */
	O2D_QUALITYOBJECT_LEGEND,		/*10feb00DW: QualityObject Legend location */
	O2D_GAUGE,						/*23feb00DW: Gauge Object */
	O2D_GAUGE_CONTAINER,			/*23feb00DW: Bounding box that contains the Gauge Object */
	O2D_GAUGE_NEEDLE,				/*23feb00DW: Gauge Needle */
	O2D_GAUGE_MAJOR_GRIDLINE,		/*23feb00DW: Major Gridline on Gauge Object */
	O2D_GAUGE_MINOR_GRIDLINE,		/*23feb00DW: Minor Gridline on Gauge Object */
	O2D_GAUGE_LABEL,				/*23feb00DW: Numeric label on Gauge Object */
	O2D_GAUGE_TITLE,				/*23feb00DW: Title on Gauge Object */
	O2D_GAUGE_RANGE,				/*23feb00DW: Range fill on Gauge Object */
	O2D_GAUGE_RANGE_LABEL,			/*23feb00DW: Range Label on Gauge Object */
	O2D_TS_HISTORY_RANGE,			/* 03-28-00/FL Time Series history range */
	O2D_TS_FORECAST_RANGE,			/* 03-28-00/FL Time Series forecast range */
	O2D_TS_HISTORY_HEADERS,			/* 03-28-00/FL Time Series history range header labels */
	O2D_TS_FORECAST_HEADERS,		/* 03-28-00/FL Time Series history range header labels */
	O2D_TS_HISTORY_LABEL,			/* 03-28-00/FL Time Series main history range label */
	O2D_TS_FORECAST_LABEL,			/* 03-28-00/FL Time Series main forecast range label */
	O2D_TS_HISTORY_INFO_BOX,		/* 03-28-00/FL Time Series main history range label */
	O2D_TS_FORECAST_INFO_BOX,		/* 03-28-00/FL Time Series main forecast range label */
	O2D_SCROLL_RIGHT,				/* 03-28-00/FL Scrolling arrow, pointing to right */
	O2D_SCROLL_LEFT,				/* 03-28-00/FL Scrolling arrow, pointing to left */
	O2D_TS_PREDICTIONS,				/* 03-28-00/FL prediction polygon */
	O2D_TS_FORECAST,				/* 03-28-00/FL forecast line */
	O2D_TS_DATA,					/* 03-28-00/FL Time Series data line */

	O2D_EQUALIZER,						/*23feb00DW: EQUALIZER Object */
	O2D_EQUALIZER_CONTAINER,			/*23feb00DW: Bounding box that contains the EQUALIZER Object */
	O2D_EQUALIZER_SLIDER,				/*23feb00DW: EQUALIZER Needle */
	O2D_EQUALIZER_MAJOR_GRIDLINE,		/*23feb00DW: Major Gridline on EQUALIZER Object */
	O2D_EQUALIZER_MINOR_GRIDLINE,		/*23feb00DW: Minor Gridline on EQUALIZER Object */
	O2D_EQUALIZER_LABEL,				/*23feb00DW: Numeric label on EQUALIZER Object */
	O2D_EQUALIZER_TITLE,				/*23feb00DW: Title on EQUALIZER Object */
	O2D_EQUALIZER_SLIDER_NAME,			/*23feb00DW: "Name" underneath each slider  on EQUALIZER Object */
	O2D_RR_TOTAL_RESOURCE_MARKER,		/* 5apr00/SJR indicates the total (X) resource available in R-R chart */

	OSM_METRIC_1,					/*Marker ID for a metric on new stock charts*/
	OSM_METRIC_2,					/*Marker ID for a metric on new stock charts */

	O2D_Y3_BODY,
	O2D_Y3_LABEL,
	O2D_Y3_MAJOR,
	O2D_Y3_MINOR,
	O2D_Y3_TITLE,
	O2D_Y3_ZERO,

	O2D_Y4_BODY,
	O2D_Y4_LABEL,
	O2D_Y4_MAJOR,
	O2D_Y4_MINOR,
	O2D_Y4_TITLE,
	O2D_Y4_ZERO,

	OSM_LEGEND_METRIC_1,					/*Marker ID for a metric on new stock charts*/
	OSM_LEGEND_METRIC_2,					/*Marker ID for a metric on new stock charts */
	O2D_AGGREGATION_LINE,					/* Aggregation Trendline Object ID */
	OSM_RISER_OPEN,
	OSM_RISER_CLOSE,

	OSM_METRIC_3,					/*Marker ID for a metric on new stock charts */
	OSM_LEGEND_METRIC_3,					/*Marker ID for a metric on new stock charts */

	OSM_SPLIT_INDICATOR,			/*Marker ID for a Split arrow on Stock charts */

	OTC_FRAMEAREA,					/* Frame for TableCharts when used in DataTable mode */
	OTC_DATAAREA_2,					/* For ODD-Stripe DataTable entries */

	O2D_Y1_MAJOR_TICK,
	O2D_Y1_MINOR_TICK,
	O2D_Y2_MAJOR_TICK,
	O2D_Y2_MINOR_TICK,
	O2D_Y3_MAJOR_TICK,
	O2D_Y3_MINOR_TICK,
	O2D_Y4_MAJOR_TICK,
	O2D_Y4_MINOR_TICK,

	/* 20nov00DW: This enumerated class is almost full. Put new ObjectID's in ObjClass2D2 instead!! */

	MAX_2D_OBJCLASS
} ObjClass2D;

#if (MAX_2D_OBJCLASS > O2D_RANGE_END)
#error c2d_defs.h: (MAX_2D_OBJCLASS > O2D_RANGE_END)
#endif


typedef enum _ObjClass2D2 {
	O2D_QUALITYAXIS_FILL_SUPERLOW_Y1 = O2D_RANGE2_BEGIN,	/*14dec99DW: QualityAxis SuperLow band fill color */
	O2D_QUALITYAXIS_FILL_SUPERLOW_Y2,	/*14dec99DW: QualityAxis SuperLow band fill color */
	O2D_QUALITYAXIS_FILL_SUPERLOW_X,	/*14dec99DW: QualityAxis SuperLow band fill color */
	O2D_QUALITYAXIS_FILL_SUPERHIGH_Y1,	/*14dec99DW: QualityAxis SuperHigh band fill color */
	O2D_QUALITYAXIS_FILL_SUPERHIGH_Y2,	/*14dec99DW: QualityAxis SuperHigh band fill color */
	O2D_QUALITYAXIS_FILL_SUPERHIGH_X,	/*14dec99DW: QualityAxis SuperHigh band fill color */
	O2D_ALERT0,							/*4jan01DW: Alert Feature object */
	O2D_ALERT1,							/*4jan01DW: Alert Feature object */
	O2D_ALERT2,							/*4jan01DW: Alert Feature object */

	O2D_QUALITYAXISBAND_FILL_LOW_Y1,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_LOW_Y2,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_LOW_X,			/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_MEDIUM_Y1,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_MEDIUM_Y2,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_MEDIUM_X,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_HIGH_Y1,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_HIGH_Y2,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_HIGH_X,		/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERLOW_Y1,	/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERLOW_Y2,	/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERLOW_X,	/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERHIGH_Y1,	/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERHIGH_Y2,	/*01jan01DW: QualityAxis Band Low band fill color */
	O2D_QUALITYAXISBAND_FILL_SUPERHIGH_X,	/*01jan01DW: QualityAxis Band Low band fill color */

	O2D_ORDINAL_BASE_Y2,				/* Ordinal Baseline attached to this Y axis */
	O2D_ORDINAL_BASE_Y3,				/* Ordinal Baseline attached to this Y axis */
	O2D_ORDINAL_BASE_Y4,				/* Ordinal Baseline attached to this Y axis */

	O2D_X1_MAJOR_TICK,					/*  Secondary ticks for X axis */
	O2D_X1_MINOR_TICK,

	O2D_SPLITMARKER,					/* user-defined arbitrary markers on the chart */
	O2D_MORE_SERIES_ENTRIES,			/* indicator that more data is available than is actually being imaged */
	O2D_MORE_GROUP_ENTRIES,				/* indicator that more data is available than is actually being imaged */
	O2D_MINLINE,						/* minimum value for a series line. */
	O2D_MAXLINE,						/* maximum value for a series line. */
	O2D_PARETO_PERCENTAGE,				/* Pareto Cumulative Percentage Line */
	O2D_LEGEND_INDICATOR,				/* LEGEND ONLY BAR/LINE/AREA INDICATOR */
	O2D_WATERFALL_OVERRIDE_FIRST,		/* FIRST Group on Waterfall Chart in Override Mode */
	O2D_WATERFALL_OVERRIDE_NEGATIVE,	/* NEGATIVE Group(s) on Waterfall Chart in Override Mode */
	O2D_WATERFALL_OVERRIDE_TOTAL,		/* TOTAL Group(s) on Waterfall Chart in Override Mode */
#if 0 /* 06nov2007jans: in Cognos build */
	O2D_ALLDATA_LINR_LINE,				/* Regression line for all data on the chart */
	O2D_ALLDATA_COEF,					/* Label for regression line for all data on the chart */
#else /* 06nov2007jans: in MS build! */
	O2D_GAUGE_BORDER,                   /* 18july06rg: Gauge border */
	O2D_GAUGE_NEEDLEBASE,               /* 18july06rg: Gauge needlebase, the little ring in the center of the gauge */
#endif
	O2D_USER_FILL,						/* User-defined rectangular fill on back wall of chart frame */
	O2D_ALLDATA_LINR_LOG,				/* Log Regression line for all data on the chart */
	O2D_ALLDATA_LINR_NATLOG,			/* Natural Log Regression line for all data on the chart */
	O2D_ALLDATA_LINR_EXP,				/* Exponential Regression line for all data on the chart */
	O2D_ALLDATA_LINR_POLY,				/* Polynomial Regression line for all data on the chart */
	O2D_DATA_ABOVE_MAX,					/* indicator that more off-screen data is available than is actually being imaged on this axis */
	O2D_DATA_BELOW_MIN,					/* indicator that more off-screen data is available than is actually being imaged on this axis*/
#if 0 /* 06nov2007jans: in Cognos build */
	O2D_GAUGE_BORDER,                   /* 18july06rg: Gauge border */
	O2D_GAUGE_NEEDLEBASE,               /* 18july06rg: Gauge needlebase, the little ring in the center of the gauge */
#else /* 06nov2007jans: in MS build! */
	O2D_ALLDATA_LINR_LINE,				/* Regression line for all data on the chart */
	O2D_ALLDATA_COEF,					/* Label for regression line for all data on the chart */
#endif

	O2D_GAUGE_NEEDLEBASE_LINE,			/* 17june08rg: Line around the gauge's needle base. */
	O2D_GAUGE_RANGE_LINE,				/* 17june08rg: Line around an individual gauge range area. */

	MAX_2D2_OBJCLASS
} ObjClass2D2;

#if (MAX_2D2_OBJCLASS > O2D_RANGE2_END)
#error can_defs.h: (MAX_2D2_OBJCLASS > O2D_RANGE2_END)
#endif

/************************************************************************
 * from C3D_DEFS.H
 ************************************************************************/

typedef enum _ObjClass3D {
	O3D_COLHEADER = O3D_RANGE_BEGIN,	/* Text associated with a data group (column). */
	O3D_COLHEADER_2D,			/* 2D text version of column header. */
	O3D_COLHEADER_BOX,			/* Optional box behind COLHEADER. */
	O3D_COLTITLE,				/* Title associated with all data groups. */
	O3D_COLTITLE_2D,			/* 2D text version of column title. */
	O3D_COLTITLE_BOX,			/* Optional box behind COLTITLE. */
	O3D_DATALABEL,				/* Text label used mainly for scatter graph. */
	O3D_DATALABEL_BOX,			/* Box for O3D_DATALABEL. */
	O3D_DATAMARKER,				/* 2D Marker used for scatter graphs. */
	O3D_FLOOR1,					/* Front and back faces of graph's floor. */
	O3D_FLOOR2,					/* Left and right faces of graph's floor. */
	O3D_FLOOR3,					/* Top and bottom faces of graph's floor. */
	O3D_FRAMEGRID_X,			/* Tickline along x axis on graph cube / frame.*/
	O3D_FRAMEGRID_Y,			/* Tickline along y axis on graph cube / frame.*/
	O3D_FRAMEGRID_Z,			/* Tickline along z axis on graph cube / frame.*/
	O3D_LASTRISERFACE,		/* Tenth face of a riser. */
	O3D_LEFTWALL1,				/* Front and back faces of graph's left wall. */
	O3D_LEFTWALL2,				/* Left and right faces of graph's left wall. */
	O3D_LEFTWALL3,				/* Top and bottom faces of graph's left wall. */
	O3D_LYHEADER,				/* Text associated with left vertical axis (Y1). */
	O3D_LYHEADER_2D,			/* 2D text version of left header. */
	O3D_LYHEADER_BOX,			/* Optional box behind LYHEADER. */
	O3D_LYTITLE,				/* Title associated with left vertical axis (Y1). */
	O3D_LYTITLE_2D,				/* 2D text version of left title. */
	O3D_LYTITLE_BOX,			/* Optional box behind LYTITLE. */
	O3D_RIGHTWALL1,				/* Front and back faces of graph's right wall. */
	O3D_RIGHTWALL2,				/* Left and right faces of graph's right wall. */
	O3D_RIGHTWALL3,				/* Top and bottom faces of graph's right wall. */
	O3D_RISERFACE1,				/* First face of a riser. */
	O3D_RISERFACE2,				/* Second face of a riser. */
	O3D_RISERFACE3,				/* Third face of a riser. */
	O3D_RISERFACE4,				/* Fourth face of a riser. */
	O3D_RISERFACE5,				/* Fifth face of a riser. */
	O3D_RISERFACE6,				/* Sixth face of a riser. */
	O3D_RISERFACE7,				/* Seventh face of a riser. */
	O3D_RISERFACE8,				/* Eigth face of a riser. */
	O3D_RISERFACE9,				/* Ninth face of a riser. */
	O3D_RISERGRID_X,			/* Tickline along x axis on risers.*/
	O3D_RISERGRID_Y,			/* Tickline along y axis on risers.*/
	O3D_RISERGRID_Z,			/* Tickline along z axis on risers.*/
	O3D_ROWHEADER,				/* Text associated with a data series (row). */
	O3D_ROWHEADER_2D,			/* 2D text version of row header. */
	O3D_ROWHEADER_BOX,			/* Optional box behind ROWHEADER. */
	O3D_ROWTITLE,				/* Title associated with all data series. */
	O3D_ROWTITLE_2D,			/* 2D text version of row title. */
	O3D_ROWTITLE_BOX,			/* Optional box behind ROWTITLE. */
	O3D_RYHEADER,				/* Text associated with right vertical axis (Y2). */
	O3D_RYHEADER_2D,			/* 2D text version of right header. */
	O3D_RYHEADER_BOX,			/* Optional box behind RYTITLE. */
	O3D_RYTITLE,				/* Title for right vertical axis. */
	O3D_RYTITLE_2D,				/* 2D text version of right title. */
	O3D_RYTITLE_BOX,			/* Optional box behind RYTITLE. */
	O3D_SCATTERLINE,			/* Line connecting scatter markers. */
	O3D_TIELINE_XX,				/* Line connecting markers to the left wall. */
	O3D_TIELINE_YY,				/* Line connecting markers to the floor. */
	O3D_TIELINE_ZZ,				/* Line connecting markers to the right wall. */
	O3D_VARIABLEFACE,			/* any face of a variable face marker. 3-20-98/FL */
	O3D_NEGATIVE_FILL,			/* In OpenGL, the transparant fill of the 3D cube for neg.numbers */
	O3D_Y1_BODY,				/* Y1 Body lines on a 3D chart */

	MAX_3D_OBJCLASS
} ObjClass3D;

#if (MAX_3D_OBJCLASS > O3D_RANGE_END)
#error c3d_defs.h: (MAX_3D_OBJCLASS > O3D_RANGE_END)
#endif

typedef enum ObjClassAna
{
	OAN_ARC = OAN_RANGE_BEGIN,		/* Arc annotation object										*/
	OAN_ARROW,										/* Arrow object															*/
	OAN_BALLOON,									/* Balloon object														*/
	OAN_BALLOONPARA,							/* Balloon paragraph												*/
	OAN_DBLTBOXIN,								/* Inside Double Text Box object						*/
	OAN_DBLTBOXOUT,								/* Outside Double Text Box object						*/
	OAN_ELLIPSE,									/* Annotation object.												*/
	OAN_FREEHAND,									/* Arrow object															*/
	OAN_GROUP,										/* "Group" object													  */
	OAN_LINE,											/* Annotation object.												*/
	OAN_POLYGON,									/* Annotation object.												*/
	OAN_POLYLINE,									/* Annotation object.												*/
	OAN_RECTANGLE,								/* Annotation object.												*/
	OAN_ROUNDRECT,								/* Annotation object.												*/
	OAN_TBOXPARA,									/* Text Box paragraph												*/
	OAN_TCHARTBULLET,							/* Text Chart bullet												*/
	OAN_TCHARTPARA,								/* Text Chart paragraph											*/
	OAN_TEXT,											/* Annotation object.												*/
	OAN_TEXTBOX,									/* Text Box object													*/
	OAN_TEXTCHART,								/* Text Chart object												*/
	OAN_THREEDTBOXFACE,						/* Face Of 3D Text Box object								*/
	OAN_THREEDTBOXSIDES,					/* Sides Of 3D Text Box object							*/
	OAN_WEDGE,										/* Annotation object.												*/

	MAX_ANA_OBJCLASS
} ObjClassAna;

#if (MAX_ANA_OBJCLASS > OAN_RANGE_END)
#error can_defs.h: (MAX_ANA_OBJCLASS > OAN_RANGE_END)
#endif


/************************************************************************
 * from CTX_DEFS.H
 ************************************************************************/

typedef enum _ObjClassText
{
	OTX_BOX_TITLE=OTX_RANGE_BEGIN,/* Each element of the text chart has a box.*/
	OTX_BOX_SUBTITLE,							/* Box for text chart's subtitle.           */
	OTX_BOX_FOOTNOTE,							/* Box for text chart's footnote.           */
	OTX_BOX_COLUMN1,							/* Box for text chart's first column.       */
	OTX_BOX_COLUMN2,							/* Box for text chart's second column.      */
	OTX_BOX_COLUMN3,							/* Box for text chart's third column.       */
	OTX_BOX_COLUMN4,							/* Box for text chart's fourth column.      */
	OTX_BOX_FLOATER1,							/* Box for text chart's first floater.      */
	OTX_BOX_FLOATER2,							/* Box for text chart's second floater.     */
	OTX_BOX_FLOATER3,							/* Box for text chart's third floater.      */
	OTX_BOX_FLOATER4,							/* Box for text chart's fourth floater.     */
	OTX_BOX_ANNOTATION,						/* Box for a text annotation object.        */
	OTX_BULLET,										/* Optional bullet for text paragraph.      */
	OTX_FRAME,										/* The text chart lives in this frame.			*/

	MAX_TEXT_OBJCLASS
} 
ObjClassText;

#if (MAX_TEXT_OBJCLASS > OTX_RANGE_END)
#error ctx_defs.h: (MAX_TEXT_OBJCLASS > OTX_RANGE_END)
#endif


#endif /* __API_OID_ENUM_H__ */
