/* Generated file. Will be overwritten. */
/* See Makefile.version for source of the data and Makefile.tdg.rules for generation */

#define PG_VERSION_MAJOR 3
#define PG_VERSION_MINOR 1
#define PG_VERSION_PATCH 64
#define PG_VERSION_EXTRA ""
#define PG_VERSION_INTERNAL "pgsdk-3.1"
/* for resource use */
#define PG_VERSION_RC "3, 1, 64"
#define PG_VERSION_RCSPECIAL "3.1.64"
