#ifndef __PGSDK_API_DEV_VECTOR_API_H__
#define __PGSDK_API_DEV_VECTOR_API_H__

/* Abstract: definitions common to the public API and the implementation.
 * Translation: geoff is lazy and doesn't want to have to maint this in pg32.h, pgunix.h, and dev_vector_p.h
 *
 * Based on alterations to pgsdk Mark Allen did at Cognos.  Pgsdk (c) 2002
 * by ThreeDGraphics.  The adaption was done June 2002 by Geoffrey Oakham
 * from Cognos.  Work done by Cognos is (c) 2002 Cognos, and work done by
 * ThreeDGraphics is (c) 2002 ThreeDGraphics.
 * 
 * (geoff.oakham@cognos.com/g@mbl.ca)
 */

#ifdef __cplusplus 
extern "C" {
#endif
#if 0
}
#endif

//low level vector primitives that needs to be implemented for custom vector format
#include "vectorprims.h"
#include "keywords.h"  //PUBLIC

DrawEnvPtr PUBLIC DevVec_AllocDevice(VectorDrawPrimitivesPtr prVectorPrimatives, void * poChildData);
INT16 PUBLIC DevVec_FreeDevice(DrawEnvPtr io_oDevice);

INT16 PUBLIC DevVec_DrawBegin(DrawEnvPtr prDE);
INT16 PUBLIC DevVec_DrawEnd(DrawEnvPtr prDE);
INT16 PUBLIC DevVec_SetOutputFormat(DrawEnvPtr io_oDevice, enum DevGen_ImageFormat i_eFormat) ;
INT16 PUBLIC DevVec_SetBackgroundTransparency(DrawEnvPtr io_oDevice, INT16 i_nTransparentBackground);
INT16 PUBLIC DevVec_ExportToFile(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, char * i_szFile);
INT16 PUBLIC DevVec_ExportToMemory(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, void ** o_ppmOutput, UINT32 * o_pnSize);
INT16 PUBLIC DevVec_SetSize(DrawEnvPtr io_oDevice, INT32 i_nWidth, INT32 i_nHeight );

INT16 PUBLIC DevVec_SetDrawRectangle(DrawEnvPtr io_oDevice, INT32 i_nLeft, INT32 i_nTop, INT32 i_nRight, INT32 i_nBottom);
INT16 PUBLIC DevVec_PlaceDefaultElements(DrawEnvPtr io_oDevice, GraphPtr pGraph, UINT32 nElement, INT16 nLegendPlacement, UINT16 cxBounds, UINT16 cyBounds );
INT16 PUBLIC DevVec_Draw(DrawEnvPtr io_oDevice, GraphPtr io_pGraph);
INT16 PUBLIC DevVec_SetDrawPrimitives(DrawEnvPtr io_oDevice, VectorDrawPrimitivesPtr prVectorPrimitives, void *poChildData);


typedef INT16 (CALLBACK * DevVec_ImageInitCB) (
		DrawEnvPtr       prDE,
		void *           context,
		enum DevGen_ImageFormat  i_eFormat,
		IOPATHPTR        direct_output
);
//INT16 PUBLIC DevVec_SetPrimitives(DrawEnvPtr io_oDevice, DevVec_ImageInitCB initfun, DevVec_ImageInitCB donefun);


#ifdef __cplusplus 
}
#endif

#endif /* __PGSDK_API_DEV_VECTOR_API_H__ */
