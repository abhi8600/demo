//
//  DashboardsCoverFlowViewController.m
//  BirstMobile
//
//  Created by Seeneng Foo on 7/16/12.
//  Copyright (c) 2012 Birst, Inc. All rights reserved.
//

#import "DashboardsCoverFlowViewController.h"

@implementation DashboardsCoverFlowViewController

@synthesize dashboards, coverFlow, description, delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame dashboards:(NSArray *)dashs{
    
    self = [super init];
    self.view.frame = frame;
    self.dashboards = dashs;
    self.view.backgroundColor = [UIColor grayColor];
    
    CGRect r = frame;
	r.origin.y = 10;
    r.origin.x = (frame.size.width / 2) - 400;
    r.size.width = 800;
    r.size.height -= 40;
    self.coverFlow = [[[TKCoverflowView alloc] initWithFrame:r] autorelease];
    [self.coverFlow setCoverSpacing:100];
    [self.coverFlow setCoverAngle:1];
    self.coverFlow.backgroundColor = [UIColor grayColor];
	//self.coverFlow.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.coverFlow.coverflowDelegate = self;
	self.coverFlow.dataSource = self;
    [self.view addSubview: self.coverFlow];
    [self.coverFlow setNumberOfCovers:[self.dashboards count]];

    CGRect loc = CGRectMake(0, r.size.height, frame.size.width, 50);
    self.description = [[[UILabel alloc]initWithFrame:loc] autorelease];
	self.description.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.description.backgroundColor = [UIColor grayColor];
	self.description.textAlignment = UITextAlignmentCenter;
    self.description.textColor = [UIColor whiteColor];
	self.description.text = @"Description";
	[self.view addSubview:self.description];     

    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) dealloc
{
    [super dealloc];
    [coverFlow release];
    [description release];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Coverflow delegate operations

- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasBroughtToFront:(int)index{
    if(index >= 0){
        Dashboard *dash = [self.dashboards objectAtIndex:index];
        self.description.text = dash.name;
    }else{
        self.description.text = @"No saved offline pages";
    }
}

- (TKCoverflowCoverView*) coverflowView:(TKCoverflowView*)coverflowView coverAtIndex:(int)index{
	
	TKCoverflowCoverView *cover = [coverflowView dequeueReusableCoverView];
	
	if(cover == nil){
		cover = [[[TKCoverflowCoverView alloc] initWithFrame:CGRectMake(0, 0, 200, 100)] autorelease]; // 224
		cover.baseline = 100;
	}
    
    Dashboard *dash = [self.dashboards objectAtIndex:index];
    if( [dash.pages count] > 0){
         NSFileManager *fm = [NSFileManager defaultManager];
        Page *p = [dash.pages objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat:@"%@/%@.png", [Settings screenshotDir:fm], p.uuid];
        if([fm fileExistsAtPath:filename]){
            cover.image = [UIImage imageWithContentsOfFile:filename];
        }else{
            cover.image = [UIImage imageNamed:@"SpaceDashboard.png"];
        }
    }else{
        cover.image = [UIImage imageNamed:@"SpaceDashboard.png"];
    }
    
	return cover;	
}

- (void) coverflowView:(TKCoverflowView*)coverflowView coverAtIndexWasDoubleTapped:(int)index{
    Dashboard *d = [self.dashboards objectAtIndex:index];
    Page *p = [d.pages objectAtIndex:0];
    if(p != nil){
        [delegate displayPage:d page:p screenshot:YES isSpaceScreenshot:NO];
    }
}

@end
