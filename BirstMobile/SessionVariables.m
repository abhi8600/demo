//
//  SessionVariables.m
//  BirstMobile
//
//  Created by Seeneng Foo on 4/18/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "SessionVariables.h"

static SessionVariables *_inst = nil;

@implementation SessionVariables

@synthesize sessionVars;

- (id)init{
    self = [super init];
    if(self) {
        self.sessionVars = [[[NSMutableDictionary alloc]init] autorelease];
    }
    return self;
}

+ (SessionVariables*) instance{
    @synchronized(self){
        if (!_inst) {
            _inst = [[SessionVariables alloc]init];
        }
    }
    return _inst;
}

@end
