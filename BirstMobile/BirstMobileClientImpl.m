//
//  BirstMobileClientImpl.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/6/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "BirstMobileClientImpl.h"
#import "Util.h"
#import "BirstWS.h"
#import "Page.h"
#import "PageRenderViewController.h"
#import "Settings.h"
#import "Log.h"

/* #define DEFAULT_SERVER @"https://app1.birst.com" */
/* #define DEFAULT_SERVER @"https://rc.birst.com" */
/* #define DEFAULT_SERVER @"http://192.168.90.61:6105" */

#define BIRST_DOMAIN @"com.birst.BirstMobileClient"
#define DEFAULT_SERVER @"https://login.bws.birst.com"
#define TOKEN_PAGE @"TokenGenerator.aspx"
#define SSO_PAGE @"SSO.aspx"

@implementation BirstMobileClientImpl

@synthesize client, asyncRequest, username, ssoPasswd, spaceId, token, dashboardList, serverUrl, redirectServerUrl, targetDashboard, targetPage, dashboardListOp;

- (id) initWith:(NSString*)userName ssoPassword:(NSString*)ssoPassword spaceId:(NSString*)space{
    self = [super init];
    if(self){
        self.username = userName;
        self.ssoPasswd = ssoPassword;
        self.spaceId = space;
        self.serverUrl = DEFAULT_SERVER;
        self.redirectServerUrl = DEFAULT_SERVER;
    }
    return self;
}

 
- (void) authenticate{
    
    if(asyncRequest){
        [asyncRequest clearDelegatesAndCancel];
    }
    
    NSString *tokenPage = [NSString stringWithFormat:@"%@/%@", self.serverUrl, TOKEN_PAGE];
    self.asyncRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:tokenPage]];
    [self.asyncRequest setDelegate:self];
    [self.asyncRequest setDidFailSelector:@selector(authenticateFail:)];
    [self.asyncRequest setDidFinishSelector:@selector(authenticateSuccess:)];
    
    [self.asyncRequest setPostValue:self.username forKey:@"username"];
    [self.asyncRequest setPostValue:self.ssoPasswd forKey:@"ssopassword"];
    [self.asyncRequest setPostValue:self.spaceId forKey:@"BirstSpaceId"];
    
    [self.asyncRequest startAsynchronous];
    
}   

- (void) authenticateSuccess:(ASIHTTPRequest*) request{
    NSString *errMsg = nil;
    
    int status = [request responseStatusCode];
    if(status == 200){
        NSString *response = [request responseString];
        
        //Verify
        NSRange r = [response rangeOfString:@"html" options:NSCaseInsensitiveSearch];
        if(r.length > 0){
            errMsg = @"Sign on failed, please check that the single sign on credentials";
        }else{
            self.token = response;
            
            NSString *redirectServer = [[request responseHeaders] objectForKey:@"Birsturl"];
            self.redirectServerUrl = (redirectServer) ? redirectServer : self.serverUrl;

            [Log debug:@"autheticated token: %@", self.token];
            [Log debug:@"redirected server url: %@", self.redirectServerUrl];
            
            [self getSession];
            return;
        }
    }else{
        [Log debug:@"Authenticate error: %d", status];
        errMsg = [NSString stringWithFormat:@"Sign on failed in authentication phase with http status code %d", status];
    }
    
    if(errMsg){
        NSError *err = [self createNSError:status formatStr:errMsg];
        if([self.client.delegate respondsToSelector:@selector(onSignOn:)]){
            [self.client.delegate onSignOn:err];
        }
    }
}

- (void) authenticateFail:(ASIHTTPRequest *) request{
    NSError *error = [request error];    
    if([self.client.delegate respondsToSelector:@selector(onSignOn:)]){
        NSString *errMsg = [NSString stringWithFormat:@"Sign on failed in authentication phase. Reason %@", [error localizedDescription]];
        NSError *err = [self createNSError:[error code] formatStr:errMsg];
        [self.client.delegate onSignOn:err];
    }
    [Log debug:@"authenticated fail: %@", [error description]];
}

- (void) getSession{
    if(asyncRequest){
        [asyncRequest clearDelegatesAndCancel];
    }
    
    NSString *ssoPage = [NSString stringWithFormat:@"%@/%@", self.redirectServerUrl, SSO_PAGE];
    self.asyncRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:ssoPage]];
    
    [self.asyncRequest setDelegate:self];
    [self.asyncRequest setDidFailSelector:@selector(onGetSessionFail:)];
    [self.asyncRequest setDidFinishSelector:@selector(onGetSessionSuccess:)];
    
    [self.asyncRequest setPostValue:self.token forKey:@"BirstSSOToken"];
    [self.asyncRequest setPostValue:@"Dashboard" forKey:@"dashboard"];
    [self.asyncRequest setPostValue:@"Page" forKey:@"page"];
    [self.asyncRequest setPostValue:@"dashboard" forKey:@"module"];
    [self.asyncRequest startAsynchronous];
}

-(void)onGetSessionSuccess:(ASIHTTPRequest *)request{
    NSError *err = nil;
    
    int status = [request responseStatusCode];
    NSString *response = [request responseString];
    if(status == 200){
        self.dashboardListOp = [Settings getDashboardListVersion:response];
        [Log debug:@"sent token, response: %@", response];
    }else{
        [Log debug:@"sent token, error: %d", status];
        
        NSString *errMsg = [NSString stringWithFormat:@"Sign on failed in retrieving session phase with http status code %d", status];
        err = [self createNSError:status formatStr:errMsg];
    }
    
    if([self.client.delegate respondsToSelector:@selector(onSignOn:)]){
        [self.client.delegate onSignOn:err];
    }
}

-(void)onGetSessionFail:(ASIHTTPRequest *)request{
    NSError *error = [request error];    
    [Log debug:@"get session fail: %@", [error description]];
    
    if([self.client.delegate respondsToSelector:@selector(onSignOn:)]){
        NSString *errMsg = [NSString stringWithFormat:@"Sign on failed in retrieving session phase %@", [error description]];
        NSError *err = [self createNSError:[error code] formatStr:errMsg];
        [self.client.delegate onSignOn:err];
    }
}

- (NSError *)createNSError:(NSInteger)code formatStr:(NSString *)errMsg{
    NSDictionary *dict = (errMsg) ? [NSDictionary dictionaryWithObject:errMsg forKey:NSLocalizedDescriptionKey] : nil;
    return [NSError errorWithDomain:BIRST_DOMAIN code:code userInfo:dict];
}

- (void) logout{
    if(asyncRequest){
        [asyncRequest clearDelegatesAndCancel];
    }
    
    NSString *ssoPage = [NSString stringWithFormat:@"%@/%@", self.redirectServerUrl, SSO_PAGE];
    self.asyncRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:ssoPage]];
    
    [self.asyncRequest setDelegate:self];
    [self.asyncRequest setDidFailSelector:@selector(onLogoutFail:)];
    [self.asyncRequest setDidFinishSelector:@selector(onLogoutSuccess:)];
    
    [self.asyncRequest setPostValue:@"true" forKey:@"logout"];
    [self.asyncRequest startAsynchronous];
}

- (void) onLogoutSuccess:(ASIHTTPRequest *)request{
    if([self.client.delegate respondsToSelector:@selector(onSignOut:)]){
        [self.client.delegate onSignOut:nil];
    }
}

- (void) onLogoutFail:(ASIHTTPRequest*)request{
    if([self.client.delegate respondsToSelector:@selector(onSignOut:)]){
        NSError *error = [request error];
        NSString *errMsg = [NSString stringWithFormat:@"Sign out failed. Reason - %@", [error localizedDescription]];
        NSError *err = [self createNSError:[error code] formatStr:errMsg];
        [self.client.delegate onSignOut:err];
    }
}

- (void)getDashboards{
    
}

- (void) getDashboardList:(SEL)success{
    [Settings setServer:self.redirectServerUrl];
    ASIHTTPRequest *dbRequest = [BirstWS requestWithDashboard:self.dashboardListOp];
    [BirstWS asyncRequest:dbRequest body:@"" delegate:self requestFinished:success requestFailed:@selector(onGetDashboardListFailed:)];
}

- (void)onGetDashboardListFinished:(ASIHTTPRequest *)request{
    [self parseDashsXml:request];
}

- (void) showPageRenderViewController:(Page*)page{
        
    //Pass to the delegate
    if([self.client.delegate respondsToSelector:@selector(onPopUpFullScreenDashboard:error:)]){
        
        PageRenderViewController *renderCtrl = [[PageRenderViewController alloc]initWithNibName:@"PageRenderViewController" bundle:nil];
        renderCtrl.page = page;
        renderCtrl.viewMode = Oem;
        renderCtrl.modalPresentationStyle = UIModalPresentationFullScreen;

        [self.client.delegate onPopUpFullScreenDashboard:renderCtrl error:nil];
        
        [renderCtrl release];
    }
    
   
}

- (void) onGetDashboardListPopUpPage:(ASIHTTPRequest *)request {
    [self parseDashsXml:request];
    
	TBXMLElement *root = self.dashboardList.rootXMLElement;
	if(root != nil){
		TBXMLElement *dash = [Util traverseFindElement:root target:@"Dashboard"];
		if (dash != nil) {
            do {
				NSString* name = [TBXML valueOfAttributeNamed:@"name" forElement:dash];
                if([name isEqualToString:self.targetDashboard]){
                    TBXMLElement *page = [Util traverseFindElement:dash target:@"Page"];
                    if (page != nil) {
                        do {
                            NSString *pageName = [TBXML valueOfAttributeNamed:@"name" forElement:page];
                            if([pageName isEqualToString:self.targetPage]){
                                
                                Page *p = [[Page alloc] init];
                                [p parse:page];
                                
                                if(p.isPartial){
                                    NSString *body = 
                                    [NSString stringWithFormat:@"<ns:uuid>%@</ns:uuid><ns:pagePath>%@</ns:pagePath>", p.uuid, [p getPath]];
                                    
                                    __block ASIHTTPRequest* req = [BirstWS requestWithDashboard:@"getPage"];
                                    [BirstWS requestNSDashFormat:req body:body];
                                    
                                    //Success, parse the full page xml
                                    [req setCompletionBlock:^{
                                        NSString *pageXml = [Util getResponse:req];
                                        TBXML *tbxml = [TBXML tbxmlWithXMLString:pageXml];
                                        TBXMLElement *root = tbxml.rootXMLElement;
                                        TBXMLElement *pageEl = [Util traverseFindElement:root target:@"Page"];
                                        if(pageEl){
                                            [p parse:pageEl];
                                            [self showPageRenderViewController:p];
                                        }
                                    }];
                                    
                                    [req setFailedBlock:^{
                                        [Util requestFailedAlert:[req error]];
                                    }];
                                    
                                    [req startSynchronous];
                                    
                                }else{
                                    //Already have full page information, just display
                                    [self showPageRenderViewController:p];
                                }
                                [p release];
                                return;
                            }
                        }while((page = page->nextSibling));
                    }
                }
            } while ((dash = dash->nextSibling));

        }
    }
    
    //Cannot find the dashboard page
    if([self.client.delegate respondsToSelector:@selector(onPopUpFullScreenDashboard:error:)]){
        NSError *err = [self createNSError:-1 formatStr:@"Cannot find the dashboard and page specified"];
        [self.client.delegate onPopUpFullScreenDashboard:nil error:err];
    }
}

- (void) parseDashsXml:(ASIHTTPRequest *)request{
    NSString *response = [request responseString];
    
    //Format
    NSString *xml = [Util xmlUnescape:response];
    TBXML *tbxml = [[TBXML alloc]initWithXMLString:xml];
    self.dashboardList = tbxml;
    [tbxml release];
 
}

- (void)onGetDashboardListFailed:(ASIHTTPRequest *)request{
    NSError *error = [request error];
    if([self.client.delegate respondsToSelector:@selector(onPopUpFullScreenDashboard:error:)]){
        NSString *errMsg = [NSString stringWithFormat:@"Failed to retrieve dashboards list. Reason: %@", [error localizedDescription]];
        NSError *err = [self createNSError:[error code] formatStr:errMsg];
        [self.client.delegate onPopUpFullScreenDashboard:nil error:err];
    }
    [Log debug:@"onGetDashboardListFailed: %@", [error description]];
}

- (void) popupFullScreenDashboardPage:(NSString*)dashboard page:(NSString*)page{
    self.targetDashboard = dashboard;
    self.targetPage = page;
    
    [self getDashboardList:@selector(onGetDashboardListPopUpPage:)];
}

- (void) dealloc{
    [username release];
    [ssoPasswd release];
    [spaceId release];
    
    [asyncRequest clearDelegatesAndCancel];
    [asyncRequest release];
    
    [super dealloc];
}

@end
