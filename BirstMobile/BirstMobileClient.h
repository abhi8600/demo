//
//  BirstMobileClient.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "BirstMobileListener.h"

@class BirstMobileClientImpl;

@interface BirstMobileClient : NSObject {
    BirstMobileClientImpl *impl;
    id<BirstMobileListener> delegate;
}

@property (nonatomic, assign) NSString *serverUrl;
@property (nonatomic, assign) id<BirstMobileListener> delegate;

- (BirstMobileClient*) initWith:(NSString*)username ssoPasswd:(NSString*)ssoPasswd space:(NSString*)space;

- (void) signOn;
- (void) listSpaces;
- (void) listDashboardsAndPages;
- (void) signOut;
- (void) popupFullScreenDashboardPage:(NSString*)dashboard page:(NSString*)page;

@end



