//
//  BirstMobileClient.m
//  BirstMobile
//
//  Created by Seeneng Foo on 8/2/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "BirstMobileClient.h"
#import "BirstMobileClientImpl.h"
#import "Log.h"

@implementation BirstMobileClient

@synthesize delegate;

- (BirstMobileClient*) initWith:(NSString*)username ssoPasswd:(NSString*)ssoPasswd space:(NSString*)space{
    self = [super init];
    if(self){
        impl = [[BirstMobileClientImpl alloc]initWith:username ssoPassword:ssoPasswd spaceId:space];
        impl.client = self;
        
        //Turn off debug logging
        [Log setLogLevel:Error];
    }
    
    return self;
}

- (void) signOn{
    [impl authenticate];
}

- (void) signOut{
    [impl logout];
}

- (void) listSpaces{
    
}

- (void) listDashboardsAndPages{
    
}

- (void) popupFullScreenDashboardPage:(NSString*)dashboard page:(NSString*)page{
    [impl popupFullScreenDashboardPage:dashboard page:page];
}

- (void) setServerUrl:(NSString *)serverUrl{
    impl.serverUrl = serverUrl;
}

- (NSString *) serverUrl{
    return impl.serverUrl;
}

- (void) dealloc{
    [delegate release];
    [impl release];
    [super dealloc];
}

@end
