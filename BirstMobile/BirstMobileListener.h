//
//  BirstMobileListener.h
//  BirstMobile
//
//  Created by Seeneng Foo on 8/12/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol BirstMobileListener <NSObject>

// Callback for [BirstMobileClient signOn]. error parameter will be nil for success.
- (void) onSignOn:(NSError *)error;

// Callback for [BirstMobileClient logout]. error parameter will be nil for success.
- (void) onSignOut:(NSError *)error;

// Callback for [BirstMobileClient popupFullScreenDashboardPage]. error parameter will be nil for success.
- (void) onPopUpFullScreenDashboard:(UIViewController *)pageRenderer error:(NSError*)err;

- (void) onListSpaces;
- (void) onListDashboards;
@end
