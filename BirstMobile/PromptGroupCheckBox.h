//
//  PromptGroupCheckBox.h
//  BirstMobile
//
//  Created by See Neng Foo on 10/11/13.
//
//

#import <UIKit/UIKit.h>

@interface PromptGroupCheckBox : UISwitch{
    NSString *value;
}

@property (nonatomic, retain) NSString *value;
@end
