//
//  Email.h
//  BirstMobile
//
//  Created by Seeneng Foo on 5/17/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface Email : NSObject {
    
}

+ (void) emailSupport:(NSString *)emailBody delegate:(UIViewController *)delegate;

+ (void) email:(NSArray*)toRecipients subject:(NSString *)subject emailBody:(NSString *)emailBody delegate:(UIViewController *)delegate;

@end
