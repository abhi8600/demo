//
//  Email.m
//  BirstMobile
//
//  Created by Seeneng Foo on 5/17/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import "Email.h"
#import "Settings.h"

@implementation Email

+ (void) emailSupport:(NSString *)emailBody delegate:(UIViewController *)delegate{
    NSString *supportEmail = [Settings valueForKey:@"supportEmail"];
    NSArray *email = [NSArray arrayWithObject:supportEmail];
    NSString *subject = [Settings valueForKey:@"supportEmailSubject"];
    [Email email:email subject:subject emailBody:emailBody delegate:delegate];
}

+ (void) email:(NSArray*)toRecipients subject:(NSString *)subject emailBody:(NSString *)emailBody delegate:(UIViewController *)delegate{
    
    if( [MFMailComposeViewController canSendMail] ){
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = (id)delegate;
        
        [picker setSubject:subject];
        
        NSLog(@"picker = %@", picker);
        [picker setToRecipients:toRecipients];
        [picker setMessageBody:emailBody isHTML:NO];
        
        [delegate presentModalViewController:picker animated:YES];
        [picker release];
    }else{
        NSMutableString *recipients = [NSMutableString stringWithString:@"mailto:"];
        for (int i = 0; i < [toRecipients count]; i++) {
            NSString *recipient = [toRecipients objectAtIndex:i];
            if(i == 0){
                [recipients appendString:recipient]; 
            }else{
                [recipients appendFormat:@",%@", recipient];
            }
        }
        [recipients appendFormat:@"&subject=%@", subject];
        NSString *body = [NSString stringWithFormat:@"&body=%@", emailBody];
        NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
        email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
}
@end
