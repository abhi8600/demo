//
//  Notification.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/12/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DRILL2DASHBOARD             @"drillToDash"
#define DRILLDOWN                   @"drillDown"
#define DRILL2URL                   @"drillUrl"
#define DISPLAYDASHBOARD            @"displayDash"
#define PARENTPROMPTVALUECHANGED    @"parentPromptChanged"
#define REMOVE_BACKGROUND_IMAGE     @"removeBackgroundImage"
#define ADD_BACKGROUND_IMAGE        @"addBackgroundImage"
#define SESSION_TIMEOUT             @"LoginSessionTimeout"
#define MAXIMIZE_DASHLET            @"maximizeDashlet"
#define MINIMIZE_DASHLET            @"minimizeDashlet"
#define DISMISS_POPOVER             @"dismissPopover"
#define PROMPT_APPLY                @"promptsApply"
#define RENDERED                    @"rendered"

@interface Notification : NSObject {
}
@end
