//
//  OfflineOptionsViewController.h
//  BirstMobile
//
//  Created by Seeneng Foo on 4/13/11.
//  Copyright 2011 Birst, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfflinePagesViewController.h"

@class OfflinePagesViewController;
@interface OfflineOptionsViewController : UITableViewController {
    OfflinePagesViewController *offlinePagesCtrl;
}

@property (nonatomic, assign) OfflinePagesViewController *offlinePagesCtrl;
@end
