/***********************************************************************
 * 					PRESENTATION GRAPHICS SDK INCLUDE FILE
 * 					ANSI-C UNIX COMPATIBLE VERSION
 *					Copyright(c) 2000 by Three D Graphics, Inc.
 *	Portions copyright 1994, 1995, 1996, 1997, 1998, by Cold Spring Harbor Laboratory. Funded under Grant P41-RR02188 by the National Institutes
 *     of Health. 
 *   Portions copyright 1996, 1997, 1998, by Boutell.Com, Inc. 
 *   GIF decompression code copyright 1990, 1991, 1993, by David Koblas (koblas@netcom.com). 
 *   Non-LZW-based GIF compression code copyright 1998, by Hutchison Avenue Software Corporation (http:/www.hasc.com/, info@hasc.com). 
 *						REV 0.1 - 28may98DW
 *						REV 1.1 - 18jan00DW
 *	BUILD #: 2.2.1.3
 ************************************************************************/
#ifndef __PGSDK_H__
#define __PGSDK_H__

#define PG_PUBLIC_API_HEADER

/* 06aug2003jans: FIXME TB */
#ifndef PGSDK_COMPAT_WANT_IMPLICIT_DEVICE
#define PGSDK_COMPAT_WANT_IMPLICIT_DEVICE 1
#endif

#ifdef WIN32
# ifdef _MSC_VER
#  pragma pack( push, before_pg )
# endif /*  _MSC_VER */
# pragma pack(8)
#endif

#ifdef __cplusplus 
extern "C" {
#endif
#if 0
}
#endif

/************************************************************************
 * from COMDEF.H
 ************************************************************************/

#ifndef __COMDEF__
#define __COMDEF__

#include "pgsdk/keywords.h" //PUBLIC CALLBACK
#include "pgsdk/types.h"    //BYTE INT16 INT32 UINT16 MEMPTR UCHAR
#include "pgsdk/space.h"    //Point2D Point3D Point16 Point32 Rect16 Rect32 
#include "pgsdk/fonts.h"    //enum pg_FontAntialias
#include "pgsdk/data.h"     //data item classes
#include "pgsdk/limits.h"   //data/api ranges

/*
 ** variable length array type
 */
typedef struct {
	UINT32					ulEntrySize;					/* # of bytes in each entry */
	UINT32					ulGrow;							/* # of entries to grow */
	UINT32					ulMax;							/* # of entries alloc'd */
	UINT32					ulCount;						/* # of entries in-use */
	UCHAR					ucbData[1];						/* the actual data */
} VTABLE, * LPVTABLE;

typedef LPVTABLE PVTABLE;

/*
 ** linked list -- item type
 */
typedef struct _LLITEMREC {
	void *			pNext;								/* PTR to next item */
	UINT16					uItemSize;						/* size of this item */

} LLITEMREC, * LLITEMPTR;

/*
 ** linked list -- list type
 */
typedef struct _LLLISTREC {
	INT16 (* lpfnOnAlloc) (		/* PTR to alloc fn */
			struct _LLLISTREC * pList,
			LLITEMPTR			pItem
	);

	void  (* lpfnOnFree) (			/* PTR to free fn */
			struct _LLLISTREC * pList,
			LLITEMPTR			pItem
	);

	UINT16					uListSize;						/* # of bytes in list struct */
	UINT16					uItemCount;						/* # of items in list */
	void *			pHead;								/* PTR to first item */

} LLLISTREC, * LLLISTPTR;

/************************************************************************
 * Common Memory Definitions
 ************************************************************************/
typedef UINT32 			MEMSIZE;

typedef MEMPTR	TDIHDL;	
typedef MEMPTR	TEXTHDL;
typedef INT16 CONTROL;
typedef MEMPTR HVA;
typedef MEMPTR VarArray;
typedef UINT32	VPSIZE;

typedef MEMPTR  TDG_HBRUSH;
typedef MEMPTR  TDG_HMENU;
typedef MEMPTR  TDG_HPALETTE;
typedef MEMPTR  TDG_HPEN;
typedef MEMPTR  TDG_HBITMAP;
typedef MEMPTR  TDG_HCURSOR;
typedef MEMPTR  TDG_HINSTANCE;

/************* PLATFORM SPECIFIC TYPE DECLARATIONS - win32 */
#ifdef WIN32

# define RgnHandle HRGN
typedef HFONT   TDG_HFONT;

/************* PLATFORM SPECIFIC TYPE DECLARATIONS - Unix */
#else

typedef	UINT32	UINT;
typedef MEMPTR  TDG_HFONT;
# ifndef VOID
#  define VOID void
# endif
#define HWND 		HANDLE
typedef MEMPTR	HBRUSH;
typedef MEMPTR	HGLOBAL;
typedef MEMPTR	HRGN;

typedef	INT32	DWORD;
typedef	long  LONG;
typedef DWORD	COLORREF;

/* Region */
typedef struct ThreeDGraphicsRegion {
	short   rgnSize;
	RECT   	rgnBBox;
} ThreeDGraphicsRegion;

typedef ThreeDGraphicsRegion * RgnPtr;
typedef ThreeDGraphicsRegion ** RgnHandle;

#define LF_FACESIZE	    32

#endif /* ifndef (WIN32) */
/************* PLATFORM SPECIFIC TYPE DECLARATIONS - end */

/************************************************************************
 * File I/O Equates
 ************************************************************************/
//typedef int COOKIE;											/* this is a DOS file handle (17may90/ksk) */

/************************************************************************
 * Limit Constants
 ************************************************************************/
#define MIN_INT16 ((INT16) 0x8000)
#define MAX_INT16 ((INT16) 0x7fff)

#define MIN_INT32 ((INT32) 0x80000000)
#define MAX_INT32 ((INT32) 0x7fffffff)

#define MIN_UINT16 ((UINT16) 0x0000)
#define MAX_UINT16 ((UINT16) 0xffff)

#define MIN_UINT32 ((UINT32) 0x00000000)
#define MAX_UINT32 ((UINT32) 0xffffffff)


/************************************************************************
 * Logic
 ************************************************************************/
#define FALSE		0
#define TRUE		1

/************************************************************************
 * Useful Constants
 ************************************************************************/
#define	TWOPI	(3.14159265358979323846 * 2)
#define	PI		(3.14159265358979323846)
#define	PI2		(1.57079632679489661923)
#define	PI4		(0.78539816339744830966)
#define RADIAN 	(57.29577951)

/************************************************************************
 * Useful Macros
 ************************************************************************/
#define ROUND(x) ((int) (((x) >= 0.0) ? (x+0.5) : (x-0.5)))
#define SQR(x) ((x)*(x))
#define FABS(x) (x >= 0.0 ? x : -x)

#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif /* MAX */
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif /* MIN */

#define GETBIT(value,bitnumber) (((long) (value)) & ((0x00000001 << bitnumber)))
#define SETBIT(value,bitnumber) (((long) (value)) | ((0x00000001 << bitnumber)))
#define RESETBIT(value,bitnumber) (((long) (value)) & ((0x00000001 << bitnumber)^(0x00000000)))



/************************************************************************
 * Platform-dependent Equates
 ************************************************************************/

/*  directory separator (platform specific) FL/10-15-96 */
/*  ---------------------------------------------------- */
#if defined(WIN32) && !defined(_WINDU_SOURCE)
#define DIR_SEPARATOR_CH		'\\'
#define DIR_SEPARATOR_STR		"\\"
#define DIR_ALL_FILES_FILTER "*.*"
#else
#define DIR_SEPARATOR_CH		'/'
#define DIR_SEPARATOR_STR		"/"
#define DIR_ALL_FILES_FILTER "*"
#endif

#endif /* ifndef __COMDEF__ */

/************************************************************************
 * PORTWINDOW -- Platform Specific Window Ptr
 ************************************************************************/
typedef unsigned long PORTWINDOW;


typedef void * StoragePtr;
typedef void * _GraphPtr;

/************************************************************************
 *  D e t L i s t
 ************************************************************************/
typedef MEMPTR DetListHdl;

//Include declarations of all functions related to exporting chart to the
//external file (pg_AllocDevice, etc.).  Mostly used on Headless Server.
#include "pgsdk/export.h"

//#defines to find out version of the PGSDK headers (compiled in)
#include "pgsdk/version.h"         //generated file

//functions to find out PGSDK shared library version
#include "pgsdk/version-runtime.h" 

//win32 related constants (PG_DE_PORT_TYPE)
#include "pgsdk/win32.h"

#define VAR_COUNT(v) (INT16)*(PINT32)((PINT64)((MEMPTR)((PINT64)v+1))-1) /* v: removed LockHandle */

#define AllocHandle(size)      _SDK_AllocPtr(size, _T(__FILE__), __LINE__)
#define AllocHandleClear(size) _SDK_AllocPtrClear(size, _T(__FILE__), __LINE__)
#define FreeHandle(h) 				 _xFreePtr(h, _T(__FILE__), __LINE__)

#define AllocPtr(size)      _SDK_AllocPtr(size, _T(__FILE__), __LINE__)
#define AllocPtrClear(size) _SDK_AllocPtrClear(size, _T(__FILE__), __LINE__)
#define ReAllocPtr(ptr,size)      _SDK_ReAllocPtr(ptr,size, _T(__FILE__), __LINE__)
#define FreePtr(p)					_xFreePtr(p, _T(__FILE__), __LINE__)

/* Allocate memory in a platform independent manner */
MEMPTR PUBLIC	
_SDK_AllocHandle(MEMSIZE size, LPTSTR pFileName, int lineNo); /* deprecated */

/* Allocate CLEARED memory in a platform independent manner */
MEMPTR PUBLIC	
_SDK_AllocHandleClear(MEMSIZE size, LPTSTR pFileName, int lineNo); /* deprecated */

/* Resize a previous memory allocation */
MEMPTR PUBLIC 
ReAllocHandle(MEMPTR *handle, MEMSIZE size); /* deprecated */

/* Resize a previous memory allocation; If growing, zero new bytes */
MEMPTR PUBLIC
ReAllocHandleClear(MEMPTR *handle, MEMSIZE size); /* deprecated */

/* Grab and lockdown a particular section of allocated memory*/
MEMPTR PUBLIC			
LockHandle(MEMPTR handle); /* deprecated */

/* Unlock a particular section of memory */
void PUBLIC				
UnlockHandle(MEMPTR handle); /* deprecated */

/* Free  a particular section of allocated memory */
void PUBLIC				
_xFreeHandle(MEMPTR handle, LPTSTR pFileName, int lineNo); /* deprecated */

/* returns the allocated size in bytes of the handle */
MEMSIZE PUBLIC
SizeOfHandle(MEMPTR handle); /* deprecated */

/* Rearranges allocated memory to make sure that size-wanted */
/* are available for future allocations */
MEMSIZE PUBLIC
env_CompactMem(MEMSIZE size_wanted);

/*allocate a chunk of non-relocatable memory and return a pointer to it */
MEMPTR PUBLIC
_SDK_AllocPtr(MEMSIZE size, LPTSTR fileName, int lineNo);

/*allocate a chunk of non-relocatable memory, clear it,  */
/* and return a pointer to it */
MEMPTR PUBLIC
_SDK_AllocPtrClear(MEMSIZE size, LPTSTR fileName, int lineNo);

/*reallocate a chunk of memory and return a pointer to it */
MEMPTR PUBLIC
_SDK_ReAllocPtr(MEMPTR ptr,MEMSIZE size, LPTSTR fileName, int lineNo);

MEMPTR PUBLIC
_SDK_ReAllocPtrClear(MEMPTR oldptr,MEMSIZE size, char * pFileName, int lineNo);

/*free up the memory associated with this pointer */
void PUBLIC	
_xFreePtr(MEMPTR dPtr, LPTSTR pFileName, int lineNo);

/* return the allocated size in bytes of a Ptr */
MEMSIZE PUBLIC
SizeOfPtr(MEMPTR p);

/* Copy to an existing handle the contents of a specific section of memory */
BOOLEAN16 PUBLIC
Ptr2XHandle(MEMPTR sPtr, MEMPTR * HandAdr, MEMSIZE sLength);

/* Create a handle to a section of relocatable memory and copy the */
/* contents of another section of memory into it */
BOOLEAN16 PUBLIC
Ptr2Handle(MEMPTR sPtr, MEMPTR * HandAdr, MEMSIZE sLength);

/* make a copy of a handle */
BOOLEAN16 PUBLIC
CloneHandle(MEMPTR * HandAdr);

/* make a copy of a ptr */
BOOLEAN16 PUBLIC
ClonePtr(MEMPTR * ppData);

/*** end TDGLIB ***/

INT16 PUBLIC SetvarHdlSize
(
		MEMPTR		 *hHandle,									/* Ptr to handle to adjust size of.	 */
		MEMPTR		 *pPtr,   									/* Ptr to pointer to var array.			 */
		INT16 			ItemSize,									/* Size of each item in var array. 	 */
		INT16 			NumItems 									/* How many items in array.        	 */
);

/************************************************************************
 * from STD_DRAW.H
 ************************************************************************/
typedef struct tagTDG_SIZE { /*  siz  */
	INT32 cx; 
	INT32 cy; 
} TDG_SIZE; 

#if defined(WIN32) && !defined(MAKELONG)
#define NOGDICAPMASKS     /* CC_*, LC_*, PC_*, CP_*, TC_*, RC_ */
#define NOVIRTUALKEYCODES /* VK_* */
#define NOWINMESSAGES     /* WM_*, EM_*, LB_*, CB_* */
#define NOWINSTYLES       /* WS_*, CS_*, ES_*, LBS_*, SBS_*, CBS_* */
#define NOSYSMETRICS      /* SM_* */
#define NOMENUS           /* MF_* */
#define NOICONS           /* IDI_* */
#define NOKEYSTATES       /* MK_* */
#define NOSYSCOMMANDS     /* SC_* */
#define NORASTEROPS       /* Binary and Tertiary raster ops */
#define NOSHOWWINDOW      /* SW_* */
#define OEMRESOURCE       /* OEM Resource values */
#define NOATOM            /* Atom Manager routines */
#define NOCLIPBOARD       /* Clipboard routines */
#define NOCOLOR           /* Screen colors */
#define NOCTLMGR          /* Control and Dialog routines */
#define NODRAWTEXT        /* DrawText() and DT_* */
#define NOGDI             /* All GDI defines and routines */
#define NOKERNEL          /* All KERNEL defines and routines */
/*#define NOUSER             All USER defines and routines
 */
#define NOMB              /* MB_* and MessageBox() */
#define NOMEMMGR          /* GMEM_*, LMEM_*, GHND, LHND, associated routines */
#define NOMETAFILE        /* typedef METAFILEPICT */
#define NOMINMAX          /* Macros min(a,b) and max(a,b) */
#define NOMSG             /* typedef MSG and associated routines */
#define NOOPENFILE        /* OpenFile(), OemToAnsi, AnsiToOem, and OF_* */
#define NOSCROLL          /* SB_* and scrolling routines */
#define NOSOUND           /* Sound driver routines */
#define NOTEXTMETRIC      /* typedef TEXTMETRIC and associated routines */
#define NOWH              /* SetWindowsHook and WH_* */
#define NOWINOFFSETS      /* GWL_*, GCL_*, associated routines */
#define NOCOMM            /* COMM driver routines */
#define NOKANJI           /* Kanji support stuff. */
#define NOHELP            /* Help engine interface. */
#define NOPROFILER        /* Profiler interface. */
#include <windows.h>
#endif

/************************************************************************
 * Macros for sub-parts of a rectangle
 ************************************************************************/
#define topLeft(r)	(((Point32 *) &(r))[0])
#define botRight(r)	(((Point32 *) &(r))[1])
#if 0 /* in WIN32 was: */
#define topLeft(r) 	(*(Point32 *) &(r).left)
#define botRight(r)	(*(Point32 *) &(r).right)

#endif

/************************************************************************
 *  Useful Constants
 ************************************************************************/
#define kForceBW		0
#define kUseFontInst	1

#define WANTPATTERNS 0
#define MAXSYSPATTERN 38
#define MAXGRAYPATTERN 64
#define GRAYPATTERNS 256

#define VirtualXMIN 		(-16383)
#define VirtualXMAX 		16383
#define VirtualYMIN 		(-16383)
#define VirtualYMAX 		16383

#define NULLAREA NULL
#define MAXPALETTE 256
#define FONTNAMEMAX 63
#define MAXSTRIPES 4

#define SQUARESIZE 32

#define IGNORE_ME 9999									/* special flag for InsertDetNode */

/************************************************************************
 *  useful constants for passing null info to drawing calls.  these values
 *  are currently outside of the range of possible object id's so that we
 *  can hopefully detect bad sets of parameters (04feb90/ksk).
 ************************************************************************/
#define NULL_OBJECTID (-3)
#define NULL_SERIESID (-3)
#define NULL_GROUPID (-3)
#define NULL_MISCID (-3)



/************************************************************************
 *  StdDraw Bottleneck Proc Types
 ************************************************************************/
enum _StdDrawProcTypes {
	DE_PROCS_DEFAULT,
	DE_PROCS_DETECTION_ONLY,
	DE_PROCS_DRAWING_ONLY,
	DE_PROCS_ADVANCED_3D, /* added for Advanced 3D (10nov97/bgw) */
	DE_PROCS_COREL_WPG	/*  Draw to Corel WPG (using their GI library) 3-20-98/FL */
};


/************************************************************************
 * InstType - Instance Types
 ************************************************************************/
enum _InstType{
	I_AREA,
	I_LINE,
	I_FONT
};


/************************************************************************
 * WashBandTypes -
 ************************************************************************/
enum _WashBandTypes{
	WashBandLong,
	WashBandMed,
	WashBandShort,

	MAX_WASHBANDTYPES
};


/************************************************************************
 * _NodeType -
 ************************************************************************/
enum _NodeType {
	ARROWOBJ,
	BALLOON,
	ELLIPSE,
	FREEHAND,
	PG_GROUP,
	LINE,
	POLYGON2,
	RECTANGLE,
	REGION,
	TEXT,
	TEXTBOX,
	TEXTCHART,
	WEDGE,
	WEDGECRUST,
	WEDGEFACE,
	DBLTBOX,
	THREEDTBOX,
	ARC,
	POLYLINE,
	ROTLABEL,
	HOLLOW_ELLIPSE,
	HOLLOW_RECTANGLE,
	HOLLOW_POLYGON,
	ROUNDRECT,  /* 06nov2007jans: Move ROUNDRECT down here, to match std_draw.h _NodeType def. */
	PATH,

	MAX_NODETYPE
};


/************************************************************************
 * FontQuality -
 ************************************************************************/
enum FontQuality {
	Stroke, Draft, Proof, Exact
};


/************************************************************************
 * FontAlignType -- Types of Justification	(06feb90/ksk)
 ************************************************************************/
enum _FontAlignType {
	ALIGN_LEFT,
	ALIGN_CENTER,
	ALIGN_RIGHT,
	ALIGN_JUSTIFY
};


/************************************************************************
 * PenModeTypes -
 ************************************************************************/
enum _DE_PEN_MODES {
	DE_PEN_NORMAL,
	DE_PEN_XOR
};

/************************************************************************
 * QuickDraw Version - MAXIS
 ************************************************************************/
enum _QD_VERSION {
	QD_ORIGINAL,
	QD_COLOR,
	QD_COLOR32
};

/************************************************************************
 * DE Drawing Modes - MAXIS
 ************************************************************************/
enum _DE_DRAW_MODES {
	DE_DRAW_RGB,
	DE_DRAW_RGBPATTERN,
	DE_DRAW_QDCOLOR,
	DE_DRAW_QDCOLORPATTERN,
	DE_DRAW_MONOCHROME,
	DE_DRAW_OUTLINE
};

/************************************************************************
 * LineInfoRec -
 ************************************************************************/
typedef struct {
	Point32 					p0;
	Point32 					p1;
	INT16 					thickness;
	PolyHandle			hPoly;
} LineInfoRec, *LineInfoPtr;

/************************************************************************
 * PathInfoRec -
 ************************************************************************/
typedef struct {
	struct pg_PathElement *path;
	INT16                 nElements;
	Rect32                  bounds;
} PathInfoRec, *PathInfoPtr;

/************************************************************************
 * CrustInfoRec - Pie Crust Information Record
 ************************************************************************/
typedef struct _CrustInfoRec {
	Rect32						bounds;
	FIXED32					lxOffset;
	FIXED32					lyOffset;
	INT16						xSize;
	INT16						ySize;
	INT16						nStart;
	INT16						nEnd;
	INT16						nHeight;
	INT16						bIsOuterCrust;
	INT16					nPieExtrusionMode;
} CrustInfoRec, * CrustInfoPtr;


/************************************************************************
 * SliceInfoRec - Pie Slice Information Record
 ************************************************************************/
typedef struct _SliceInfoRec {
	Rect32						bounds;
	FIXED32					lxOffset;
	FIXED32					lyOffset;
	INT16						xSize;
	INT16						ySize;
	INT16						nStart;
	INT16						nEnd;
	INT16						nRing;
	struct {
		Rect32					bounds;
		FIXED32				lxOffset;
		FIXED32				lyOffset;
		INT16					xSize;
		INT16					ySize;
	} inner;		

} SliceInfoRec, * SliceInfoPtr;

/* 30apr2004jans: floating point version */
typedef struct _fSliceInfoRec {
	Rect32						bounds;
	FIXED32					lxOffset;
	FIXED32					lyOffset;
	INT16						xSize;
	INT16						ySize;
	INT16						nStart;
	INT16						nEnd;
	INT16						nRing;
	struct {
		Rect32					bounds;
		FIXED32				lxOffset;
		FIXED32				lyOffset;
		INT16					xSize;
		INT16					ySize;
	} inner;		

} fSliceInfoRec, * fSliceInfoPtr;



/************************************************************************
 * WedgeInfoRec - Pie Wedge Information Record
 ************************************************************************/
typedef struct _WedgeInfoRec {
	Rect32		bounds; 		/* bounds rectangle */
	Point32		ptStart;		/* start point */
	Point32		ptEnd;			/* end point */
	REAL		fStartAngle;	/* start angle (in rect radians) */
	REAL		fEndAngle; 		/* end angle	 (in rect radians) */
	INT16 		nRing;			/* ring percentage  */
	INT16 		nHeight;		/* 2.5D crust/ring height */
} WedgeInfoRec, *WedgeInfoPtr;


/************************************************************************
 * PolyInfoRec - Polygon Information Record
 ************************************************************************/
typedef struct {
	PolyHandle			hPoly;
} PolyInfoRec, *PolyInfoPtr;


/************************************************************************
 * RegionInfoRec - Region Information Record
 ************************************************************************/
typedef struct {
	RgnHandle 			hRgn;
} RegionInfoRec, *RegionInfoPtr;


/************************************************************************
 * TextInfoRec - Text Information Record
 ************************************************************************/
typedef struct {
	Rect32						bounds;
	MEMPTR					hText;
} TextInfoRec, *TextInfoPtr;


#include "pgsdk/color.h"  // RGB16, pg_rgba

/************************************************************************
 * DropShadowInstRec - Drop Shadow Instance Record
 ************************************************************************/
typedef struct {
	INT16 					nXoff;		/* virtual x offset */
	INT16 					nYoff; 		/* virtual y offset */
	UINT16 					nRed; 		/* RGB values */
	UINT16 					nGreen; 	/* RGB values */
	UINT16 					nBlue;		/* RGB values */
} DropShadowInstRec, *DropShadowInstPtr;

//AreaInstPtr FontInstPtr AdvDropShadowInstPtr ColorInstPtr BevelInstPtr
//LineInstPtr GlowInstPtr
#include "pgsdk/attr.h"
#include "pgsdk/attrfun.h"
/************************************************************************
 * WashBandRec - Wash Band Record
 ************************************************************************/
typedef struct {
	INT16 					startBin;
	INT16 					endBin;
} WashBandRec, *WashBandPtr;

/************************************************************************
 *  FontMasterRec -- Keeper of font names
 ************************************************************************/
typedef struct _OldFontNameInfo {
	INT16						nFlags;								/*NOSAVE usage flag */
	INT32						oName;								/* offset to name */
} OldFontNameInfo, * OldFontNameInfoPtr;


typedef struct _OldFontMasterRec {
	INT16						fontNameBufLen;
	INT16						fontNameBufFree;
	LPTSTR						fontNameBuf;					/* Courier,0,Helvetica,0,Times,0,0 */

	INT16						fontNameInfoCount;
	OldFontNameInfoPtr	fontNameInfoTbl;

} OldFontMasterRec, * OldFontMasterPtr;

/************************************************************************
 *  FontMasterRec -- New Keeper of font names
 *
 * This record contains the new fontmaster. It keeps the name of the font
 * and information about the font for substitition purpose.
 *
 * Note: the application will use the font specified in
 *       the fontXXSubstitution field. If these fields are empty,
 *       then the font specified in fontname will be used.
 *
 ************************************************************************/

/*%%TDIBEGIN%%*/

typedef struct _FontNameInfo {
#if defined _BO_DBCS_AIX
	char     fontName[LF_FACESIZE];  /* original font name */    
#else
	TCHAR    fontName[LF_FACESIZE];  /* original font name */
#endif
	BYTE     pitchAndFamily;         /* pitch and family (for Windows only) */
	BYTE     charSet;                /* character set (for Windows only) */
	INT16    nFlags;                 /* NOSAVE usage flag */

} FontNameInfo, * FontNameInfoPtr;

typedef struct _FontMasterRec {

	VPSIZE           								fontInfoCount;
	/*%%VARPTR%%*/ FontNameInfoPtr	fontInfoTbl;

} FontMasterRec, * FontMasterPtr;

/*%%TDITRANSFER(FontMasterRec)%%*/

/*%%TDIEND%%*/

typedef struct {
	UINT32					totalSize;						/* current size */
	UINT32					totalFree;						/* free space available */
	UINT32					firstFree;						/* offset to first free */
	UINT32					lastDetNode;					/* offset to last alloc'd detnode */
} DetListInfo, * DetListPtr;

/************************************************************************
 *  D e t N o d e R e f
 ************************************************************************/
typedef struct {
	INT16						layer;								/* layer found in */
	DetListHdl			hDetList;							/* DetList Handle */
	UINT32					offset;								/* offset into handle */
} DetNodeRef, * DetNodeRefPtr;


/************************************************************************
 * StdDraw Function Prototypes
 ************************************************************************/
INT16 PUBLIC					DEGetBackgroundRect	(DrawEnvPtr pDE, Rect32 * pBounds);
INT16 PUBLIC					DEGetClientData			(DrawEnvPtr pDE, void * * pClientData);
INT16 PUBLIC					DEGetDestRect				(DrawEnvPtr pDE, Rect32 * pBounds);
INT16 PUBLIC					DEGetDrawMode			(DrawEnvPtr pDE, INT16 * pIndex);
INT16 PUBLIC					DEGetFrameRect			(DrawEnvPtr pDE, Rect32 * pBounds);
INT16 PUBLIC					DEGetPenMode				(DrawEnvPtr pDE, INT16 * pPenMode);
INT16 PUBLIC					DEGetPortInfo				(DrawEnvPtr pDE, PORTWINDOW * pPortWindow, INT16 * pnPortType);
INT16 PUBLIC					DEGetVirtRect				(DrawEnvPtr pDE, Rect32 * pRect);

INT16 PUBLIC					DESetBackgroundRect	(DrawEnvPtr pDE, Rect32 * pRect);
INT16 PUBLIC					DESetClientData			(DrawEnvPtr pDE, void * pClientData);
INT16 PUBLIC					DESetDestRect				(DrawEnvPtr pDE, Rect32 * pRect);
INT16 PUBLIC					DESetDrawMode			(DrawEnvPtr pDE, INT16 nIndex);
INT16 PUBLIC					DESetFrameRect			(DrawEnvPtr pDE, Rect32 * pRect);
INT16 PUBLIC					DESetPenMode				(DrawEnvPtr pDE, INT16 nPenType);
INT16 PUBLIC					DESetPortInfo				(DrawEnvPtr pDE, PORTWINDOW port, INT16 nPortType);
INT16 PUBLIC					DESetVirtRect				(DrawEnvPtr pDE, Rect32 * pRect);
INT16 PUBLIC					DEGetMeasurements		(DrawEnvPtr	pDE, INT16 * pnHorz, INT16 * pnVert);
INT16 PUBLIC					DESetMeasurements		(DrawEnvPtr	pDE, INT16 pnHorz, INT16 pnVert);
/* --------------------new */
INT16 PUBLIC					DESetOwnerWindowHeight(DrawEnvPtr	pDE, PINT32	pHeight, PINT32 pRatio);
INT16 PUBLIC					DEGetOwnerWindowHeight(DrawEnvPtr	pDE, PINT32	pHeight, PINT32 pRatio);
INT16 PUBLIC					DrawBegin						(DrawEnvPtr pDE);
INT16 PUBLIC					DrawEnd							(DrawEnvPtr pDE);

/* backwards compatability flags */
#if PGSDK_COMPAT_WANT_IMPLICIT_DEVICE
DrawEnvPtr PUBLIC				AllocDrawEnvPtr			(PORTWINDOW port, INT16 nPortType);
INT16 PUBLIC					FreeDrawEnvPtr			(DrawEnvPtr pDE);
INT16 PUBLIC          DESetDrawProcs      (DrawEnvPtr pDE, INT16 nProcType);
INT16 PUBLIC					DEGetDrawProcs			(DrawEnvPtr pDE, INT16 * pnProcType);

#endif

INT16 PUBLIC
DrawArc(
		DrawEnvPtr			pDE,
		LineInstPtr 		pLine,
		Rect32 *			pBounds,
		Point32 *			pStartPt,
		Point32 *			pEndPt,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawEllipse(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pAreaInst,
		LineInstPtr 		pLineInst,
		INT16 					xULC,
		INT16 					yULC,
		INT16 					xLRC,
		INT16 					yLRC,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawLabel(
		DrawEnvPtr			pDE,
		FontInstPtr 		pFontInst,
		BoxInstPtr			pBox,
		LPTSTR				szText,
		INT16 				nObjectID,
		INT16 				nSeriesID,
		INT16 				nGroupID,
		void *			pInfo,
		INT16 				nInfoLen
);

INT16 PUBLIC
DrawLine(
		DrawEnvPtr			pDE,
		LineInstPtr 		pLine,
		INT16 					x0,
		INT16 					y0,
		INT16 					x1,
		INT16 					y1,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawPara(
		DrawEnvPtr			pDE,
		FontInstPtr 		pFontInst,
		BoxInstPtr			pBox,
		LPTSTR				szText,
		INT16 				nObjectID,
		INT16 				nSeriesID,
		INT16 				nGroupID,
		void *			pInfo,
		INT16 				nInfoLen
);

INT16 PUBLIC
DrawPolygon(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		FontInstPtr 		pFont,
		INT16 					nPoints,
		INT16 * 		xyPoint,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawPolyline(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		FontInstPtr 		pFont,
		INT16 					nPoints,
		INT16 * 		xyPoint,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawRect(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pAreaInst,
		LineInstPtr 		pLineInst,
		INT16 					xULC,
		INT16 					yULC,
		INT16 					xLRC,
		INT16 					yLRC,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawRoundedRect(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pAreaInst,
		LineInstPtr 		pLineInst,
		Rect32					 *pRect,
		INT16						xRound,
		INT16						yRound,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawRotLabel(
		DrawEnvPtr			pDE,
		FontInstPtr 		pFontInst,
		BoxInstPtr			pBox,
		LPTSTR				szText,
		INT16				rotation,
		INT16				charTilt,
		INT16 				nObjectID,
		INT16 				nSeriesID,
		INT16 				nGroupID,
		void *			pInfo,
		INT16 				nInfoLen
);


INT16 PUBLIC
DrawSlice(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		Rect32 *			pBounds,
		Point32 *			pStartPt,
		Point32 *			pEndPt,
		INT16						nRing,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawSliceCrust(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		Rect32 *			pBounds,
		Point32 *			pStartPt,
		Point32 *			pEndPt,
		INT16						nHeight,
		INT16						nRing,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawSliceFace(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		Rect32 *			pBounds,
		Point32 *			pStartPt,
		INT16						nHeight,
		INT16						nRing,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawSliceRing(
		DrawEnvPtr			pDE,
		AreaInstPtr 		pArea,
		LineInstPtr 		pLine,
		Rect32 *			pBounds,
		Point32 *			pStartPt,
		Point32 *			pEndPt,
		INT16						nHeight,
		INT16						nRing,
		INT16 					nObjectID,
		INT16 					nSeriesID,
		INT16 					nGroupID,
		void *			pInfo,
		INT16 					nInfoLen
);

INT16 PUBLIC
DrawCharacter(
		DrawEnvPtr		pDE,
		INT16 				x,
		INT16 				y,
		TCHAR 				c
);

INT16 PUBLIC
DrawExtText(
		DrawEnvPtr		pDE,
		INT16 				x,
		INT16 				y,
		UINT16				wOptions,
		Rect32				 *pRect,
		LPTSTR				pText,
		INT16					nChars,
		INT16				 *pnGaps
);

INT16 PUBLIC
SetFont(
		DrawEnvPtr			pDE,
		FontInstPtr			pFont,
		INT16 					rotation,
		INT16 					tilt
);

INT16 PUBLIC
SetFontColors(
		DrawEnvPtr			pDE,
		FontInstPtr			pFont
);

INT16 PUBLIC
SetAreaColors(
		DrawEnvPtr			pDE,
		AreaInstPtr			pArea
);

INT16 PUBLIC
SetLineColors(
		DrawEnvPtr			pDE,
		LineInstPtr			pLine
);

INT16 PUBLIC
GenericDrawFunction(
		DrawEnvPtr			pDE,
		INT16						nFuncID,
		void *			pData1,
		void *			pData2,
		void *			pData3,
		void *			pData4
);

/*  Visibility wizard functions 02-19-97/FL */
void PUBLIC
SetVisibilityMode(	DrawEnvPtr	pDE, 
		INT16 nSeries, 
		INT16 nGroups,
		BOOLEAN16 bVisibilityMode );

INT16 PUBLIC
GetRiserSurface( DrawEnvPtr	pDE, INT16 nIndex, INT16 *pnSurface );
INT16 PUBLIC
GetRiserBounds( DrawEnvPtr	pDE, INT16 nIndex, RECT *pRect );


/************************************************************************
 * from STD_SDMM.H
 ************************************************************************/

INT16 PUBLIC
DERegisterCallbacks
(
		DrawEnvPtr pDE,
		DrawEnvPtr (*pMaybeSwitchDE) 
		(
				DrawEnvPtr			pDEIn,
				INT16						nObjectID,
				INT16						nSeriesID,
				INT16						nGroupID,
				AreaInstPtr			pAreaInst,
				LineInstPtr			pLineInst,
				FontInstPtr			pFontInst,
				INT16 *         pbObjectsSelected,
				size_t          lClientData 
		),

		INT16 (*pDoneWithSwitchedDE) 
		(
				DrawEnvPtr			pDEUsed,
				INT16						nObjectID,
				INT16						nSeriesID,
				INT16						nGroupID,
				AreaInstPtr			pAreaInst,
				LineInstPtr			pLineInst,
				FontInstPtr			pFontInst,
				INT16           pbObjectsSelected,
				size_t          lClientData 
		),
INT16 (*pDetNodeFilterProc)
	(
			DrawEnvPtr			pDE,
			INT16						nNodeType,
			INT16						nObjectID,
			INT16						nSeriesID,
			INT16						nGroupID,
			INT16						nInfoLen,
			void *			pInfo,
			AreaInstPtr 		pArea,
			LineInstPtr 		pLine,
			FontInstPtr 		pFont,
			void *      pData,
			size_t          lClientData 
	),
	size_t lClientData    /*  user defined data */
		);

INT16 PUBLIC
DESetActionProcs(DrawEnvPtr			pDE);


/************************************************************************
 * from COM_FONT.H
 ************************************************************************/

typedef struct {
	INT16	nStructSize;
	BOOLEAN16		bUseGlobalFontInfo;		/* Use font mode in FontInstRec or not. */
	BOOLEAN16		bEnableFontScaling;		/* Allow scaling. N/A if above FALSE. */
	INT16			nGreekingSize;				/* Size at which font greeking will occur. */
} GlobalFontClass, * GlobalFontPtr;


INT16 PUBLIC
DEGetGlobalFontInfo(
		DrawEnvPtr			pDE,				/* Draw enviroment referenced. */
		GlobalFontPtr		pGlobalFont	/* Requested data buffer. */
);

INT16 PUBLIC
DESetGlobalFontInfo(
		DrawEnvPtr			pDE,				/* Draw enviroment referenced. */
		GlobalFontPtr		pGlobalFont	/* Buffer from which to initialize pDE. */
);


/************************************************************************
 * from COM_XLAT.H
 ************************************************************************/

/*
 * General utility routines
 */
void		PUBLIC			XchgINT16				(INT16 *p1, INT16 *p2);


/*
 * Point32 arithmetic
 */
void		PUBLIC			AddPoints				(Point32 *sum, Point32 *p1, Point32 *p2);
void		PUBLIC			SubPoints				(Point32 *dif, Point32 *p1, Point32 *p2);
void		PUBLIC			AbsPoint				(Point32 *p);
void		PUBLIC			ScalePoint			(Point32 *p, Point32 *mul, Point32 *div);


/*
 * Device to Virtual translation
 */
INT16		PUBLIC			dvScaleHeight		(DrawEnvPtr pDE, INT16 height);
INT16		PUBLIC			dvScaleWidth		(DrawEnvPtr pDE, INT16 width);
void		PUBLIC			dvCrust					(DrawEnvPtr pDE, CrustInfoPtr pCrust);
void		PUBLIC			dvPoint					(DrawEnvPtr pDE, Point32 *p);
void		PUBLIC			dvPoly					(DrawEnvPtr pDE, PolyHandle hPoly);
void		PUBLIC			dvRect					(DrawEnvPtr pDE, Rect32 *pRect);
void		PUBLIC			dvScalePoint			(DrawEnvPtr pDE, Point32 *p);
void		PUBLIC			dvSlice					(DrawEnvPtr pDE, SliceInfoPtr pSlice);
void		PUBLIC			dvWedge					(DrawEnvPtr pDE, WedgeInfoPtr pWedge);
void		PUBLIC			dvPoint2D				(DrawEnvPtr pDE, Point2D *p);
void		PUBLIC			dvScalePoint2D	(DrawEnvPtr pDE, Point2D *p);


/*
 * Virtual to Device translation
 */
INT16		PUBLIC			vdScaleHeight		(DrawEnvPtr pDE, INT16 height);
INT16		PUBLIC			vdScaleWidth		(DrawEnvPtr pDE, INT16 width);
void		PUBLIC			vdCrust					(DrawEnvPtr pDE, CrustInfoPtr pCrust);
void		PUBLIC			vdPoint					(DrawEnvPtr pDE, Point32 *p);
void		PUBLIC			vdPoly					(DrawEnvPtr pDE, PolyHandle hPoly);
void		PUBLIC			vdRect					(DrawEnvPtr pDE, Rect32 *r);
void		PUBLIC			vdScalePoint		(DrawEnvPtr pDE, Point32 *p);
void		PUBLIC			vdSlice					(DrawEnvPtr pDE, SliceInfoPtr pSlice);
void		PUBLIC			vdWedge					(DrawEnvPtr pDE, WedgeInfoPtr pWedge);
void		PUBLIC			vdPoint2D				(DrawEnvPtr pDE, Point2D *p);
void		PUBLIC			vdScalePoint2D	(DrawEnvPtr pDE, Point2D *p);

REAL    PUBLIC      vdfScaleHeight  (DrawEnvPtr pDE, INT16 height);
REAL    PUBLIC      vdfScaleWidth   (DrawEnvPtr pDE, INT16 width);


/*
 * Virtual / Physical translation
 */
INT16 	PUBLIC 			pvScaleWidth    (DrawEnvPtr	pDE, INT16 width);
INT16 	PUBLIC 			pvScaleHeight   (DrawEnvPtr	pDE, INT16 height);
INT16 	PUBLIC 			vpScaleWidth    (DrawEnvPtr	pDE, INT16 width);
INT16 	PUBLIC 			vpScaleHeight   (DrawEnvPtr	pDE, INT16 height);

/*
 * Device routines
 */
void		PUBLIC			dSetRect				(DrawEnvPtr pDE, Rect32 *pRect, INT16 left, INT16 top, INT16 right, INT16 bottom);
void		PUBLIC			dCheckRect			(DrawEnvPtr pDE, Rect32 *r);


/*
 * Virtual routines
 */
void		PUBLIC			vSetRect				(DrawEnvPtr pDE, Rect32 *pRect, INT16 left, INT16 top, INT16 right, INT16 bottom);
void		PUBLIC			vCheckRect			(DrawEnvPtr pDE, Rect32 *r);


/*
 *  Utility routines
 */
void		PUBLIC			xSetPt					(Point32 *pPoint, INT16 x, INT16 y);

REAL		PUBLIC			xNormalizeDegrees	(REAL rAngle);
REAL		PUBLIC			xNormalizeRadians	(REAL rAngle);

REAL		PUBLIC			xPt2Degrees			(Point32 *p, Rect32 *pRect);
REAL		PUBLIC			xPt2Radians			(Point32 *p, Rect32 *pRect);

#define xDegrees2Radians(rAngle) 		((rAngle) * PI / 180.0)
#define xRadians2Degrees(rAngle) 		((rAngle) * 180.0 / PI)


/*
 *  Detection Routines
 */
BOOLEAN16	PUBLIC			xPtInEllipse		(Point32 *p, Rect32 *pRect);
BOOLEAN16	PUBLIC			xPtInPoly				(Point32 *p, PolyHandle hPoly);
BOOLEAN16	PUBLIC			xPtInRect				(Point32 *p, Rect32 *pRect);
BOOLEAN16	PUBLIC			xPtInWedge			(Point32 *p, WedgeInfoPtr pWedge);
BOOLEAN16 PUBLIC			xPtInSlice			(Point32 *p, SliceInfoPtr pSlice);


/*
 *	Rectangle Manipulation.
 */
void		PUBLIC			xDefineRect			(Rect32 *r, INT16 left , INT16 top,
		INT16 right, INT16 bottom);
void		PUBLIC			xInsetRect			(Rect32 *r, INT16 x, INT16 y);
void		PUBLIC			xOffsetRect			(Rect32 *r, INT16 x, INT16 y);
void		PUBLIC			xIntersectRect	(Rect32 *r, Rect32 *cr);
BOOLEAN16 PUBLIC			xCompareRects		(Rect32 *r1, Rect32 *r2);
Rect32		PUBLIC			xSumOfRects			(Rect32 *r1, Rect32 *r2);
INT16 	PUBLIC 			xWidthOfRect 		(Rect32 *r);
INT16 	PUBLIC 			xHeightOfRect 	(Rect32 *r);


/*
 *  Type conversion routines.
 */
void		PUBLIC 			xRectToBox			(BoxInstPtr pBox, Rect32 *pRect);
void		PUBLIC 			xBoxToRect			(Rect32 *pRect, BoxInstPtr pBox);
void		PUBLIC			xPointToPoint2D	(Point2D *np2D, Point32 *p);
void		PUBLIC			xPoint2DToPoint	(Point32 *p, Point2D *np2D);



/************************************************************************
 * from STD_POLY.H
 ************************************************************************/

void PUBLIC					dump_PolyHandle		(PolyHandle hPoly);

PolyHandle	PUBLIC	AllocPolyHandle		(INT16 nPoints);
INT16				PUBLIC	AddPolyPoint			(PolyHandle * hPoly, INT16 x, INT16 y, int isFirstPoint);
void				PUBLIC	ClosePolyHandle		(PolyHandle * hPoly);
void				PUBLIC	FreePolyHandle		(PolyHandle hPoly);

PolyPtr			PUBLIC	LockPolyHandle		(PolyHandle hPoly); /* deprecated */
void				PUBLIC	UnlockPolyHandle	(PolyHandle hPoly); /* deprecated */

BOOLEAN16		PUBLIC	ClonePolyHandle		(PolyHandle * phPoly);

INT16				PUBLIC	GetPolyPointCount	(PolyHandle hPoly);
void				PUBLIC	GetPolyBounds			(PolyHandle hPoly, Rect32 * pRect);

INT16				PUBLIC	GetPolyPtrPointCount	(PolyPtr pPoly);
void				PUBLIC	GetPolyPtrBounds			(PolyPtr pPoly, Rect32 * pRect);

PolyHandle 	PUBLIC	Points2Poly(Point32 *ptData,	INT16 nCount);

INT16 PUBLIC
GetPolyPoint(
		PolyHandle *pPolyHdl,
		INT16						nIndex,
		PINT16 					pnX,
		PINT16 					pnY
);



/************************************************************************
 * from COMMON.H
 ************************************************************************/


#define MAXSTRLEN 			256
#define XYZ							3								/* Number of axes in 3D.*/

/************************************************************************
 *  LayerType -- Constants for Each Layer of the Graph
 ************************************************************************/
typedef enum _LayerType {
	ANNOTATION_LAYER,
	GRAPH_LAYER,
	BACKGROUND_LAYER,

	MAX_LAYER
} LayerType;


/*************************
 *  E n g i n e T y p e  *
 *************************/
typedef enum _EngineType {
	ENGINE_ROOT,
	ENGINE_2D,
	ENGINE_PIE,
	ENGINE_3D,
	ENGINE_ANA,
	ENGINE_TEXT,

	MAX_ENGINETYPE
} EngineType;


/************************************************************************
 *  GraphTypes
 ************************************************************************/
typedef enum _GraphType {

	/*
	 * This table is in one to one correspondence with similar
	 * tables in the following files
	 *	sac_data.c	(Data models)
	 *  com_Util.c	(String to chart type mapping)
	 *  Cro_Menu.c	(Chart menu definitions)
	 */

	G_3D_BarType,
	G_3D_ColAreaType,
	G_3D_ColLineType,
	G_3D_ColStepType,
	G_3D_CubeType,
	G_3D_CutCornerType,
	G_3D_DiamondType,
	G_3D_HoneycombType,
	G_3D_ModelType,
	G_3D_OctagonType,
	G_3D_PyramidType,
	G_3D_RowAreaType,
	G_3D_RowLineType,
	G_3D_RowStepType,
	G_3D_ScatterType,
	G_3D_SquareType,
	G_3D_SurfaceType,

	G_Area,
	G_Bars,
	G_BoxPlot,
	G_BoxPlotTC,
	G_Bubble,
	G_ContourPlot,
	G_Gantt,
	G_HArea,
	G_HBars,
	G_HBoxPlot,
	G_HBoxPlotTC,
	G_HBubble,
	G_HContourPlot,
	G_HHistogram,
	G_HLines,
	G_HScatter,
	G_Hinge,
	G_Histogram,
	G_Lines,
	G_MultPie,
	G_OHLClose,
	G_Pie,
	G_PieBar,
	G_Polar,
	G_Radar,
	G_Scatter,
	G_SpecMapCell,
	G_TableChart,
	G_TextChart,
	G_URadarArea, 		 /*  29jul98DW: Radar Absolute Area */
	G_VJapaneseStock,  /*  12nov97DW: new chart but add at end for backwards compatibility */
	G_VStemLeaf, 			 /*  17dec97DW: new chart but add at end for backwards compatibility */
	G_WMultiBar,					/* Corel Multi-Bar chart */
	G_WWaterFall,					/* McKinsey Waterfall chart */
	G_WWaterFall_Horz,		/* McKinsey Waterfall chart in Horizontal Mode */
	G_X3D_FacetedFloater,	/*  3-20-98/FL  */
	G_X3D_FacetedRiser,		/*  3-20-98/FL  */
	G_XBI1_BalanceScoreCard,/*  8mar00DW: Balance Score Card (Gauge) Chart */
	G_XBI1_ProductPosition,	/*  8mar00DW: Product Position Analysis Chart */
	G_XBI1_ResourceReturn,	/*  8mar00DW: Resource Return Chart */
	G_XBI1_TimeSeries,		/*  03-21-00/FL: Time Series Chart */
	G_XBI1_YYYY,			/*  8mar00DW: 4Y Chart */
	G_XBI2_CENTROID,		/*  19may00DW: Centroid Chart */
	G_XRadarAreaStacked, 	/*  6-1-00/FL: Radar Stacked Area */
	G_XY_3D_ColLine,		/*  Ribbon chart with 2 numeric axes 05.23.00/SJR */
	G_XY_3D_RowLine,		/*  Ribbon chart with 2 numeric axes 05.23.00/SJR */
	G_XYZ_3D_Surface,		/*  Surface chart with 3 numeric axes 05.23.00/SJR */
	G_XYZ_3D2_CONTOUR,		/*  27jun00DW: 3D Contour chart for Corel. */
#if 1 /* 06nov2007jans: in MS build, not in Cognos! */
	G_XYZ_FUNNEL,			/* 5jun03DW: Funnel chart imported from CR9 build */
	G_XYZ_GAUGE,			/*	1aug04DW: Gauge Charts */
#endif
	G_XX_HPareto,			/*  13sep02DW: Pareto Charts */
	G_XX_Pareto,			/*  13sep02DW: Pareto Charts */
#if 0 /* 06nov2007jans: in Cognos build! */
	G_XZB_GAUGE,			/*	1aug04DW: Gauge Charts */
#else /* 06nov2007jans: in MS build! */
	G_XZBA_HGAUGE,    /* 20jul2006jans: Horizontal (eg. thermometer) gauge charts */
#endif

	MAX_GRAPHTYPE
} GraphType;


/************************************************************************
 * SubGraphType
 *************************************************************************/
typedef enum _SubGraphType {						/* 2D BLA (bar, line, area) chart style. */
	Absolute,
	Stacked,
	SideBySide,
	Percent,

	NormalPie,
	PropPie,
	RingPie,
	RingPropPie,

	MAX_SUBGRAPHTYPE
} SubGraphType;


/************************************************************************
 * Major Graph Preset Types
 *************************************************************************/
typedef enum _TDG_GRAPH_TYPE {
	TDG_GRAPH_3D,													/* Change Graph Type to 3D */
	TDG_GRAPH_AREA,												/* Change Graph Type to Area */
	TDG_GRAPH_BARS,												/* Change Graph Type to Bar */
	TDG_GRAPH_LINES,											/* Change Graph Type to Line */
	TDG_GRAPH_PIE,												/* Change Graph Type to Pie */
	TDG_GRAPH_SPECIAL,										/* Change Graph Type to Special */
	TDG_GRAPH_TABLE,											/* Change Graph Type to Table */
	TDG_GRAPH_TEXT,												/* Change Graph Type to Textchart */

	MAX_TDG_GRAPH_TYPE

} TDG_GRAPH_TYPE;


/************************************************************************
 * ShapeType -- 2D shapes enumeration.
 *
 * If you change this list, you also need to change the szShape array in
 * comload.c, and also the DrawMarker routine in C2D_MKRS.C.
 ************************************************************************/
typedef enum _ShapeType {
	NoMarker,
	SRectangle,
	NStar45,
	Plus,
	Circle,
	Diamond,
	SpikedX,
	PlainX,
	IsTri1, 															/* isosceles triangles */
	NStarSkewed,
	FatPlus,
	NStar90,
	SoftX,
	PiratePlus,
	FatX,
	Castle,
	IsTri2,
	IsTri3,
	IsTri4,
	RtTri1, 															/* right triangles */
	RtTri2,
	RtTri3,
	RtTri4,
	SEllipse,
	Square,
	Hexagon,
	Pentagon,
	House,
	Pentagram,
	FontMarker,
	BoxedPlus,
	BoxedX,
	Hourglass,
	HourglassTransparent,
	VerticalLine,
	HorizontalLine,
	Asterisk,
	Star5,
	Star6,
	Star8,	
	BevelBox,			      /* 3apr98DW: For Fancy Risers in regular (non OpenGL) mode - test! */
	ReverseBevelBox,		/* 3apr98DW: For Fancy Risers in regular (non OpenGL) mode - test! */
	SlimHorizontalLine, /* 10April98CP: Added new shapes for Corel  */
	SlimVerticalLine,	  /* Similar shapes were polygon based. New ones are line segment based */
	SlimPlus,
	SlimBoxedPlus,
	SlimX,
	SlimBoxedX,
	RotatedHourglass,
	SmallBevelBox,
	CandleStickMarker,
	LineCircleMarker,
	AreaChartMarker,
	AdjustableBevelBox,					/*  adjustable bevels 10-04-00/FL */
	AdjustableReverseBevelBox,			/*  adjustable bevels 10-04-00/FL */
	Indicator_Bar,		/* A marker that represents a Bar Graph */
	Indicator_Area,		/* A marker that represents an Area Graph */
	Indicator_Line		/* A marker that represents a Line Graph */
} ShapeType;

/************************************************************************
 *************************************************************************
 * BEGIN Advanced New-Look (OpenGL) Data Structures (9oct97/bgw)
 *************************************************************************
 *************************************************************************/

typedef enum _ExtRiserType
{
	EXTRISER_NONE,						/* placeholder for start of range			*/
	EXTRISER_PRISM,						/* faceted cylindrical prism					*/
	EXTRISER_PYRAMID_UP,				/* pyramid with point at top					*/
	EXTRISER_PYRAMID_DOWN,				/* pyramid with point at bottom				*/
	EXTRISER_PYRAMID_DOUBLE,			/* pyramid with point at top & bottom	*/
	EXTRISER_PYRAMID_PRISM,				/* cylindrical prism w/ pyramid caps	*/
	EXTRISER_SPHERE,					/* faceted sphere											*/
	EXTRISER_HEMISPHERE,				/* faceted hemisphere									*/
	EXTRISER_HALFPRISM					/* faceted half cylindrical prism					*/
} ExtRiserType;

#define OGLTEXT_GDI				0		/* GDI (Windows) 2D text */
#define OGLTEXT_OUTLINE		1		/* Flat tessellated outline text */
#define OGLTEXT_EXTRUDED	2		/* Extruded tessellated outline text */
#define OGLTEXT_TEXMAP		3		/* Texture-mapped text */

/************************************************************************
 *************************************************************************
 * END Advanced New-Look (OpenGL) Data Structures (9oct97/bgw)
 *************************************************************************
 *************************************************************************/

/*************************************************************************
 *  MarkerLayoutType - for use with attribute A2D_LEGEND_MARKER_ORIENTATION
 **************************************************************************/
typedef enum _MarkerLayoutType {
	MARKER_LEFT,
	MARKER_RIGHT,
	MARKER_CENTERED,
	MARKER_ABOVE,
	MARKER_BELOW,
	MAX_MARKER_LAYOUT
} MarkerLayoutType;

/************************************************************************
 * HashStyleType
 ************************************************************************/
typedef enum _HashStyleType {
	NoHash,
	Grid,
	GridExtended,
	TickIn,
	TickOut,
	TickSpan,
	GridAndTickIn,		/*  2 different selectable objects 11-01-00/FL */
	GridAndTickOut,		/*  2 different selectable objects 11-01-00/FL */
	GridAndTickSpan		/*  2 different selectable objects 11-01-00/FL */
} HashStyleType;


/************************************************************************
 * SideType -- Axis Placement Constants
 ************************************************************************/
typedef enum _SideType {
	LowSide,
	HighSide,
	BothSides,
	NeitherSide
} SideType;


/************************************************************************
 * OrientaionType
 ************************************************************************/
typedef enum _OrientationType {
	HORIZONTAL,
	VERTICAL
} OrientationType;


/************************************************************************
 * DataOrientationType
 ************************************************************************/
typedef enum _DataOrientationType {
	Columns, Rows
} DataOrientationType;


/************************************************************************
 *  GraphByMethods
 ************************************************************************/
typedef enum _GraphByMethods {
	GraphByType,													/* Use main graph type for all cells.*/
	GraphBySeries,												/* Use RiserType for series mapping.*/
	GraphByGroup,													/* Use RiserType for group mapping.*/

	MAX_GraphByMethods
} GraphByMethods;


/************************************************************************
 *  Color Model Constants 12.16.91/SJR
 ************************************************************************/
typedef enum _ColorModelConstants {
	CM_BY_FACE,							/* 3d only, depends on face, not s or g */
	CM_BY_SERIES,
	CM_BY_GROUP,

	CM_BY_ANGLE,						/* 3d only, depends on angle to viewer */
	CM_BY_HEIGHT,						/* depends on length of riser */
	CM_BY_VALUE_X,					/* depends on distance to LEFT WALL (Y axis) */
	CM_BY_VALUE_Y,					/* depends on distance to FLOOR (X-axis) */
	CM_BY_VALUE_Z,					/* 3d only, depends on distance to RIGHT WALL */

	NUM_CM_TYPES
} ColorModelConstants;


/************************************************************************
 * Control values for A2D_SHOW_DATATEXT.
 ************************************************************************/

#define DATATEXT_NONE  0x00		/* Display no datatext.*/
#define DATATEXT_VALUE 0x01		/* Display value(s) of data item(s). */
#define DATATEXT_TEXT  0x02		/* Display unique data item text. */
#define DATATEXT_ABS   0x04   /* Display the absolute value in stacked segments */
#define DATATEXT_TOTAL 0x08		/* total value on top of the stack bar */
#define DATATEXT_ZVALUE 0x10 	/* Display the z-value in bubble charts */
#define DATATEXT_PERCENT 0x20 /* Calculate and show the riser bvalue as a percentage 
																 of the entire stack/group */
#define DATATEXT_YVALUE 0x40 	/* Display the y-value like in scatters or bubble charts */
#define DATATEXT_INTERNAL 0x80 /* Internal flag */

#define DATATEXT_SERIES_DEPENDENT 0x100 /*12nov97DW: Turn ON Series-Dependent mode */
#define DATATEXT_PERCENT_AND_VALUE 0x400 /* Put out Percent and VALUE */
#define DATATEXT_WIDE 0x800              /* Put Wide Labels */

#define DATATEXT_QUALITYOBJECT_MODE 0x200 /*Format Bubble Chart DataLabels in manner consistent with Product Position Chart. */

#define DATATEXT_BOTH     DATATEXT_VALUE | DATATEXT_TEXT
#define DATATEXT_ABSVALUE DATATEXT_VALUE | DATATEXT_ABS
#define DATATEXT_ABSBOTH  DATATEXT_BOTH | DATATEXT_ABS

/************************************************************************
 * Control values for A2D_SCALEEND_??.
 ************************************************************************/

#define SCALEEND_BOTH			0
#define SCALEEND_MINONLY	1
#define SCALEEND_MAXONLY	2
#define SCALEEND_NONE			3

/************************************************************************
 * Control values for ASG_LGND_LAYOUT.
 ************************************************************************/

#define LAYOUT_HORIZONTAL			0
#define LAYOUT_VERTICAL				1
#define LAYOUT_AUTOMATIC			2
#define LAYOUT_PIE_INFO				3
#define LAYOUT_PIE_INFO_DOLLARS	4	
#define LAYOUT_PIE_INFO_PERCENT	5	

/************************************************************************
 * Control A2D_SHOW_??
 ************************************************************************/

#define SHOW_AXISLINE			1
#define SHOW_LABELS				2
#define SHOW_MAJOR_GRIDLINES	4
#define SHOW_MINOR_GRIDLINES	8
#define SHOW_SUPER_GRIDLINES	16
#define SHOW_ALL				31

/************************************************************************
 * Control values for A_FONTSTYLE.
 ************************************************************************/

#define FONTSTYLE_BOLD				1
#define FONTSTYLE_ITALIC			2
#define FONTSTYLE_UNDERLINE		4
#define FONTSTYLE_STRIKEOUT		8

#define FONTSTYLE_FORCE_BOLD	 (FONTSTYLE_BOLD << 5 )
#define FONTSTYLE_FORCE_ITALIC (FONTSTYLE_ITALIC << 5)
#define FONTSTYLE_FORCE_MASK   ((FONTSTYLE_BOLD|FONTSTYLE_ITALIC) << 5)

/************************************************************************
 * Control values for A2D_LABELMODE_O?.
 ************************************************************************/

#define LABELMODE_EVEN				0	/* Evenly space any headers found. */
#define LABELMODE_EXACT				1	/* Place headers exactly as found. */
#define LABELMODE_SKIPMANUAL	2	/* As exact but with skipping. */
#define LABELMODE_SKIPAUTO		3 /* As skip manual but automated. */

/************************************************************************
 * Control values for A2D_LABELWRAPMODE_O1.
 ************************************************************************/

#define LABELWRAP_OFF					0 /* label wrap mode (multi-line) off */
#define LABELWRAP_AUTO				1	/* DGDS wraps labels to number of lines */

/************************************************************************
 * Control values for A2D_GRIDMODEMAJOR_O1 and A2D_GRIDMODEMINOR_O1.
 * TL:11/18/92 added new attributes to control ordinal axis gridlines
 ************************************************************************/

#define GRIDMODE_NORMAL							0 /* major - grid drawn between each group for bars
																				 minor - AxisLookClass.nMinorHash between majors */
#define GRIDMODE_ATSKIPGROUPS				1	/* major - grid drawn at group every 
																				 AxisLookClass.nLabelSkip groups
																				 minor - drawn at each group */
#define GRIDMODE_BETWEENSKIPGROUPS	2	/* major - grid drawn between every 
																				 AxisLookClass.nLabelSkip groups
																				 minor - drawn between each group */

/* %%TDIBEGIN%% */

/************************************************************************
 *  ColorModelStruct	12.16.91/SJR
 ************************************************************************/
typedef struct _ColorModelStruct {
	INT16 					nColorModel;					/* CM_BY_FACE, CM_BY_SERIES, etc. */
	INT16 					nColorDivisions;
	RGB16						washBeginColor;				/* RGB value of start color for washes. */
	RGB16						washEndColor;					/* RGB value of end color for washes. */
} ColorModelStruct, *ColorModelPtr;


/************************************************************************
 * CLevelLookClass -- Contour Level Look Class
 ************************************************************************/
typedef struct
{
	REAL					fContourValue; 	/* Contour level value to be drawn for given surface */
	INT16					nSurfaceID;		 	/* Surface number for this contour level value */
	BOOLEAN16				bIsMinor;			/* TRUE=Minor , FALSE=Major */
} CLevelLookClass, *CLevelLookPtr;


typedef TCHAR STRING[MAXSTRLEN];
/************************************************************************
 * CPointLookClass -- Contour Point32 Look Class
 ************************************************************************/
typedef struct
{
	REAL					x;					/* Contour Point32 : x coordinate */
	REAL					y;					/* Contour Point32 : y coordinate */
	INT16					nColorIndex; 	/* Absolute color index for this contour point */
	STRING				szLabel;			/* Label at given x,y */
}	CPointLookClass, *CPointLookPtr;


/************************************************************************
 * CSurfaceLookClass -- Contour Surface Look Class
 ************************************************************************/
typedef struct
{
	INT16					nLabelSeg;			/* Put a contour level label every nTh segment except: 0=nonem 1=put only one label */
	INT16					nLFormat;			/* Contour level format 0=Auto, 1=Manual, 2=List in CLevelLookClass Array */
	REAL					fLMinAuto;			/* Auto contour level min */
	REAL					fLMaxAuto;			/* Auto contour level max */
	REAL					fLMajorIntAuto;	/* Auto Major contour level interval */
	REAL					fLMinorIntAuto;	/* Auto Minor contour level interval */
	REAL					fLMinManu;			/* Mnual contour level min */
	REAL					fLMaxManu;			/* Mnual contour level max */
	REAL					fLMajorIntManu;	/* Mnual Majro contour level interval */
	REAL					fLMinorIntManu;	/* Mnual Minor contour level interval */
} CSurfaceLookClass, *CSurfaceLookPtr;


/************************************************************************
 * Font3DFields -- 3D Graph Anatomy Constants
 ************************************************************************/
typedef enum _Font3DFields {
	Row_Headers, 														/* text labels along z axis */
	Col_Headers, 														/* text labels along x axis */
	Left_Numbers, 													/* text labels along left y axis */
	Right_Numbers, 													/* text labels along right y axis */
	Row_Title, 															/* text label on left side of base */
	Col_Title, 															/* text label on right side of base */
	Left_Title, 														/* text label on left wall */
	Right_Title, 														/* text label on right wall */

	Max_3DFields
} Font3DFields;

typedef REAL64		fMatrixType[4][XYZ];
typedef INT32			dMatrixType[4][XYZ];

typedef struct {
	INT32						x, y, z;								/* Coordinates of a 3D point in 4-byte ints. */
} dPoint3D;


/************************************************************************
 *  OleLink -- Next and Previous 
 ************************************************************************/

#if defined(TDI_SDK)
typedef HLLNEXT OleLinkNext;
typedef HLLPREV OleLinkPrev;
#else
typedef MEMPTR OleLinkNext;
typedef MEMPTR OleLinkPrev;
#endif


/************************************************************************
 *  OleLink -- Information needed to manage OLE links 
 ************************************************************************/
typedef struct _OleLink {
	OleLinkNext	hNextNode;
	OleLinkPrev	hPrevNode;
	INT16		nStartRow;
	INT16		nStartCol;
	INT16		nEndRow;
	INT16		nEndCol;
	BOOLEAN16	bAutomatic;
	TCHAR		strDisplayName[256];
} OleLinkRec,       * OleLinkPtr; 


/************************************************************************
 * OleLink Handle
 ************************************************************************/
#if defined(_WINDOWS)
/*** start TDGLIB ***/
#if defined(WIN32)
typedef MEMPTR OleLinkHdl;
#else
typedef INT16 OleLinkHdl;
#endif
/*** end TDGLIB ***/
#else
typedef MEMPTR OleLinkHdl;
#endif

/************************************************************************
 * OleLinkMasterRec
 ************************************************************************/
#if defined(TDI_SDK)

typedef HLLTYPEA OleLinkMasterRec;

#else

typedef struct {
	OleLinkHdl				head;
	OleLinkHdl				tail;
} OleLinkMasterRec, *OleLinkMasterPtr;

#endif

/*  basic public inteface to manage OLE links */
/*  ------------------------------------------------------------ */
BOOLEAN16 PUBLIC AddOleLink(GraphPtr pGraph, OleLinkRec OleLink);
BOOLEAN16 PUBLIC RemoveOleLink(GraphPtr pGraph, OleLinkRec OleLink);
BOOLEAN16 PUBLIC FreeAllOleLinks(GraphPtr pGraph);
OleLinkMasterPtr PUBLIC GetOleLinkList(GraphPtr pGraph);

/* For use with attribute A2D_SDDATALINE_TYPE */
#define DATALINE_MARKER				1
#define DATALINE_LINE				2
#define DATALINE_BOTH				(DATALINE_LINE | DATALINE_MARKER)


#define LINRTYPE_NONE		0
#define LINRTYPE_LINEAR		1

/************************************************************************
 * LinRAnalysisClass - Linear regression analysis.
 ************************************************************************/
typedef struct {

	INT16	nLinRType;

	INT16	nCoefficients;
	PREAL	pfCoefficients;


} LinRAnalysisClass, * LinRAnalysisPtr;


/************************************************************************
 * BoundScaleClass - Scale information derived from graph/data binding.
 ************************************************************************/
typedef struct {

	BOOLEAN16						bIsBound;		/* Is data structure valid? */

	REAL							fRawMin;		/* Unprocessed scale minimum. */
	REAL							fRawMax;		/* Unprocessed scale maximum. */
	REAL							fScaleMin;	/* Processed scale minimum. */
	REAL							fScaleMax;	/* Processed scale maximum. */

	BOOLEAN16						bHasZero;		/* Scale has zero values. */
	BOOLEAN16 					bhasNeg;  	/* Scale has negative values. */

	LinRAnalysisClass	lraResults;

} BoundScaleClass, * BoundScalePtr;

/************************************************************************
 * BoundAccessClass - Information derived from graph/data binding.
 ************************************************************************/
typedef struct {

	BOOLEAN16					bIsBound;	/* Is data structure valid? */

	INT16						nSeries;				/* Series count. */
	INT16 					nGroups;				/* Group count. */
	INT16 					nEntries; 			/* Entry (VDM page) count. */

	BOOLEAN16					bExcessiveQDRs;	/* Too many QDRs for detnodes. */

	BoundScaleClass	X1Scale;				/* X1 axis information. */
	BoundScaleClass	Y1Scale;				/* Y1 axis information. */
	BoundScaleClass	Y2Scale;				/* Y2 axis information. */
	BoundScaleClass	Z1Scale;				/* Z1 axis information. */

} BoundAccessClass, * BoundAccessPtr;

/*%%TDIBEGIN%%*/

/************************************************************************
 * ArrowInfoRec - Arrow Information Record
 ************************************************************************/
typedef struct {
	Point32 				p0;
	Point32 				p1;
	REAL					angle;
	INT16					headSides;
	INT16					headIndent;
	INT16					headWidth;
	INT16					tailIndent;
	INT16					tailWidth;
} 							ArrowInfoRec, *ArrowInfoPtr;

/************************************************************************
 * AnaLineInfoRec - Annotation Line Information Record
 ************************************************************************/
typedef struct {
	Point32 				p0;
	Point32 				p1;
	INT16				thickness;
	INT16				arrowAtStart;
	INT16				arrowAtEnd;
	INT16				arrowSize;
} 							AnaLineInfoRec, *AnaLineInfoPtr;


/************************************************************************
 * GroupInfoRec - Group Information Record
 ************************************************************************/
typedef struct {
	INT16					children;
} 							GroupInfoRec, *GroupInfoPtr;


/************************************************************************
 * PolygonInfoRec - Polygon Information Record
 ************************************************************************/
typedef struct {
	BOOLEAN16				bClosed;
	PolyHandle		hPoly;		/*POLYHDL*/
} PolygonInfoRec, *PolygonInfoPtr;



/************************************************************************
 * GTextInfoRec - Generalized Text Box Information Record
 ************************************************************************/
typedef struct {
	TEXTHDL					hText;		/*%%DYNAMIC%%*/
	INT16						leftInd;
	INT16						topInd;
	INT16						rightInd;
	INT16						bottomInd;

	/* ...For Balloons... */
	INT16						hPct;
	INT16						vPct;
	INT16						SpikeWidth;

	/* ...For Bullet Charts... */
	AreaInstPtr			pbArea;			/*INT32*/
	LineInstPtr			pbLine;			/*INT32*/
	INT16						bTopInd;
	INT16						bSides;
	INT16						bulTxtSpc;
	INT16						bShape;

	INT16						thickness;
} GTextInfoRec, *GTextInfoPtr;

#if defined(TDI_SDK)
typedef HLLNEXT AnodeInfoNext;
typedef HLLPREV AnodeInfoPrev;
#else
typedef MEMPTR AnodeInfoNext;
typedef MEMPTR AnodeInfoPrev;
#endif

/************************************************************************
 * AnodeInfoRec
 ************************************************************************/
typedef MEMPTR AnodeInfoHdl;

typedef struct _AnodeInfoRec {

	/*%%TDICHECK(600)%%*/ /*%%NEW(1)%%*/

	CONTROL /*%%NodeType2Union%%*/nNodeType;
	AnodeInfoNext				hNextNode;
	AnodeInfoPrev				hPrevNode;
	INT16								group;								/* group #. 0 if none */
	Rect32								bounds;								/* enclosing rect */
	AreaInstPtr					pArea;	/*INT32*/	/* area instance */
	LineInstPtr					pLine;	/*INT32*/	/* line instance */
	FontInstPtr					pFont;	/*INT32*/	/* font instance */
	INT16								linkSeries;
	INT16								linkGroup;
	Rect32								linkRect;
	union {
		AnaLineInfoRec		line;	/* This is the ANNOTATION Poly info...different...*/
		PolygonInfoRec		poly;	/* This is the ANNOTATION Poly info...different...*/
		WedgeInfoRec			wedge;
		RegionInfoRec			region;
		GTextInfoRec			text;
		ArrowInfoRec			arrow;
		GroupInfoRec			group;
	} info;

	/*%%TDICHECK(601)%%*/ /*%%NEW(1)%%*/

} AnodeInfoRec, *AnodeInfoPtr;

/************************************************************************
 * AnodeMasterRec
 ************************************************************************/
#if defined(TDI_SDK)

typedef HLLTYPEA /*%%AnodeInfoRec%%*/	AnodeMasterRec;

#else

typedef struct {
	AnodeInfoHdl				head;
	AnodeInfoHdl				tail;
} AnodeMasterRec, *AnodeMasterPtr;

#endif

/*%%TDITRANSFER(AnodeMasterRec)%%*/

/*%%TDIEND%%*/

/************************************************************************
 * GeodeInfoRec
 ************************************************************************/
typedef MEMPTR GeodeInfoHdl;

typedef struct {
	INT16		 				nNodeType;
	DetNodeRef			DetRef;					/* 24april91 */
	_GraphPtr				pGraph;
	INT16						layer;
	INT16						attrCode;							/* attribute code */
	Rect32						bounds;								/* enclosing rect */
	union {
		LineInfoRec		line;
		PolyInfoRec		poly;
		WedgeInfoRec	wedge;
		RegionInfoRec	region;
		TextInfoRec		text;
	} info;
} GeodeInfoRec, *GeodeInfoPtr;


/************************************************************************
 * GraphDataInfo -- Per-Graph Data Access Methods and Info
 ************************************************************************/
typedef enum _GraphDataStringType {
	gsTITLE,
	gsSUBTITLE,
	gsFOOTNOTE,
	gsSERIESTITLE,
	gsGROUPSTITLE,
	gsXAXISTITLE,
	gsY1AXISTITLE,
	gsY2AXISTITLE,
	gsZAXISTITLE,
	gsY3AXISTITLE,
	gsY4AXISTITLE,

	MAX_GraphDataStringType
} GraphDataStringType;

typedef
INT16 (CALLBACK *LPFNGetDataCallBack) (
		INT16		nDataPt,	/* Row # of of requested value. */
		INT16		nSeries,	/* Column # of of requested value. */
		REAL64		*rVal,		/* Caller provided buffer for REAL64 assigment. */
		size_t		lClientData	/* client data value */
);

typedef INT16 (CALLBACK *LPFNGetLabelCallBack) (
		INT16		nCol,							/* Column # of of requested label. */
		INT16		nRow,							/* Row # of of requested label. */
		LPTSTR	lpBuffer,					/* Caller provided buffer for string assignment. */
		INT16		nDimBuffer,				/* Buffer length. */
		size_t   lClientData  /* client data value */
);

typedef INT16 (CALLBACK *LPFNGetStringCallBack) (
		INT16			nWhich,								/* Ordinal identifier of requested string. */
		LPTSTR		lpBuffer,							/* Caller provided buffer for string assignment. */
		INT16			nDimBuffer,						/* Buffer length. */
		size_t		lClientData    				/* client data value */
);

typedef INT16 (CALLBACK *LPFNSetPageCallBack) (
		INT16						nPage,								/* New current page. */
		size_t            lClientData            /* client data value */
);

//#define USER_FMT 1 /* 11oct2008jans: allways on from now on */	

/*25may95DW:  improved Callback function that passes Object, series & Group Info */     

#define USER_DEFINED_FMT 60

typedef INT16 (CALLBACK *LPFNRealToStringCallBack) (
		REAL64		rVal,									/* Value to be formatted. */
		LPTSTR		lpResult,							/* Caller provided buffer for string assigment. */
		INT16			nDimBuffer,						/* Buffer length. */
		INT16			nFmtCode,							/* Format conversion identifier. */
		INT16			nObjectID,						/* Object ID */
		INT16			nSeriesID, 						/* Series ID */
		INT16			nGroupID,							/* Group ID */
//#if PG_UNIFORM_REAL_TO_STRING_CALLBACK on for Microstrategy from 3.1.40
		INT32     nMiscID,              /* MiscID */
//#endif
		size_t    lClientData   /* client data value */
);

typedef enum _DataAxisRef {
	gdY1,
	gdY2,
	gdX1,
	gdZ1,
	gdY3,
	gdY4,
	MAX_DataAxisRef
} DataAxisRef;

typedef BOOLEAN16 (CALLBACK *LPFNGetRawLimits) (
		INT16						nWhichAxis,						/* Axis limits requested. */
		REAL64 *		prMin, 								/* Axis raw minimum. */
		REAL64 *		prMax, 								/* Axis raw maximum. */
		size_t			lClientData						/* Client data value. */
);

/* Callback function for when user has a specified a handle for a picture */

typedef INT16 (CALLBACK *LPFNPictureCallBack) (
		size_t         lClientData, /* client data value */
		LPTSTR					lppictName,     /* The string which identifies this handle */
		INT32 *			lphPicture,	 	/* Caller supplied picture handle */
		BOOLEAN16 *	lpbIsAPM,			/* Caller supplied flag. Set TRUE if this is an APM Metafile */
		RECT  *			lpAPMbox	            /* Caller supplied World Coord. Box for APM Metafiles */ 
);

#if 1 /* 20dec96DW: First cut at adding support for multi-dimensional axis */

/* Bitwise flags controlling Multi-Dimensional Label Options */

#define MULTI_FONT		0x0001
#define BRACKETS		0x0002
#define BLOCK_LAYOUT	0x0004

#define	MAX_MD_DIMENSIONS	32

typedef	UINT16 	DimArray[MAX_MD_DIMENSIONS];		/* Definition of a Dimension Array */
typedef PUINT16	DimArrayPtr;			/* Definition of a pointer to a DimArray */

typedef BOOLEAN16 (CALLBACK *LPFNMDGetLabel) (
		DimArrayPtr		pDimArray,							/* Ptr to a DimensionArray */
		INT16			nCurrentDimension,		/* Which dimension is being checked */
		LPTSTR			lpBuffer,							/* Caller provided buffer for string assignment. */
		INT16			nBufferSize, 					/* Buffer length. */
		size_t     lClientData        /* Client data value. */
);

/* WANT_HIERARCHICAL_MDAXIS. Was COGNOS_MD. Here is COGNOS Specific stuff: */
typedef struct {
	UINT16			nChildren;
	TCHAR			szElementName[MAXSTRLEN];	/* %%DYNAMIC%% */
} DimInfoRec, *DimInfoPtr;

typedef struct {
	UINT16			nDimensions;
	DimInfoRec	DimInfoArray[MAX_MD_DIMENSIONS];
} MDInfoRec, *MDInfoPtr;

typedef
INT16 (CALLBACK *LPFNGetCognosMDLabelCallBack) (
		INT16						nCol,									/* Column # of of requested label. */
		MDInfoPtr				lpMDInfo,							/* Caller provided buffer for MD Info */
		size_t				lClientData						/* client data value */
);

/* 21aug07rg: New callback used to work around the global nLevelCount variable problem in activex layer. 
 * lClientData is expected to be the accurate number of nested label dimensions.  MS 281325. */
typedef
INT16 (CALLBACK *LPFNGetActivexMDLabelCallBack) (
		INT16						nCol,									/* Column # of of requested label. */
		MDInfoPtr				lpMDInfo,							/* Caller provided buffer for MD Info */
		size_t						lClientData						/* client data value */
);

#endif

/************************************************************************
 *  Data Interface Mode constants to define the Data Interface
 ************************************************************************/
#define DATAIF_CALLBACK	0	/* use host supplied callbacks for chart data */
#define DATAIF_SENDDATA	1	/* host sends data for storage before charting */
#define	DATAIF_VDM_DM		2	/* Data Manager supplies chart data and/or host 
													 ** sends data for storage */

typedef struct _GraphDataInfo {
	LPFNGetDataCallBack 			lpfnGetData; 			/* returns data for a given row, col */
	LPFNGetLabelCallBack			lpfnGetLabel;			/* returns label for a given row, col */
	LPFNGetStringCallBack			lpfnGetString;		/* returns string based on GraphDataStringType */
	LPFNRealToStringCallBack	lpfnRealToString;	/* convert a value to a string */
	LPFNSetPageCallBack				lpfnSetPage;			/* request to select a page */
	LPFNGetRawLimits					lpfnGetRawLimits;	/* request data limits. */
	LPFNPictureCallBack		lpfnPicture;			/* request a picture handle */

	INT16						nPages;								/* # of of pages accessable thru CallBacks. */
	INT16						nCols;								/* # of of columns accessable thru CallBacks. */
	INT16						nRows;								/* # of of rows accessable thru CallBacks. */
	size_t						lClientData;					/* client-supplied data */
	INT16						nDataIFMode;					/* data interface mode, see constants above */

#if 1 /* 20dec96DW: First cut at adding support for multi-dimensional axis */
	INT16						nDimensionCount;			/* # of dimensions needing labeling (1..32) */
	DimArrayPtr			pDimensionArray;			/* Ptr to an array of 32 UINT16 values */
	DimArrayPtr			pStartElement;				/* Defines which hierarchy element is first to graph */
	DimArrayPtr			pStopElement;					/* Defines which hierarchy element is last to graph */
	LPFNMDGetLabel	lpfnMDGetLabel;				/* Returns a particular data item if it exists */
	BOOLEAN16				bSymmetric;						/* TRUE = Symmetrical data, FALSE = Asymmetric */
#endif

	/* WANT_HIERARCHICAL_MDAXIS. Was COGNOS_MD. specific function callback: */
	LPFNGetCognosMDLabelCallBack lpfnGetCognosMDLabel;

	/* 21aug07rg: New callback used to work around the global nLevelCount variable problem in activex layer. 
	 * lClientData is expected to be the accurate number of nested label dimensions.  MS 281325. */
	LPFNGetActivexMDLabelCallBack lpfnGetActivexMDLabel;

} GraphDataInfo, * GraphDataPtr;

/************************************************************************
 *
 *	Data resizing interface
 *
 * 12-20-99/FL
 ************************************************************************/
typedef struct {

	/*  virtual coordinates of the original mouse position on the click */
	Point32	orgMouseCoord;

	/*  virtual coordinates of the original position of the object before resizing */
	Point32	orgObjCoord;

	/*  original object value(s) (before resizing) */
	REAL64	orgfValues[5];

	/*  current mouse position */
	Point32 ptMousePos;

	/*  suggested object coordinates */
	Point32 ptNewObjCoord;

	/*  suggested data value */
	REAL64 newfValues[5];

	/*  number of datavalue per data point */
	INT16 nValues; /*  1 to 5 */

} ResizeDataStruct, * ResizeDataPtr;

typedef
INT16 (CALLBACK *LPFNResizeData) (
		INT16						nObjectID,									
		INT16						nSeriesID,									
		INT16						nGroupID,									
		ResizeDataStruct			*pResizeData,
		size_t					lClientData						/* client data value */
);

typedef INT16 (CALLBACK *LPFNResizeDataToolTip) (
		INT16						nObjectID,									
		INT16						nSeriesID,									
		INT16						nGroupID,
		ResizeDataStruct			*pResizeData,
		LPTSTR						szToolTip,
		INT16						nszToolTipMaxLen,
		size_t					lClientData						/* client data value */
);

typedef struct _GraphDataResizeInfo {
	LPFNResizeData 			lpfnResizeData;
	LPFNResizeDataToolTip 	lpfnResizeDataToolTip;
	size_t	lClientData;					/* client-supplied data */
} GraphResizeDataInfo, * GraphResizeDataPtr;

/************************************************************************
 *  TDG_ClientInfo -- Information needed by Client (02.27.91/SS)
 ************************************************************************/
typedef struct _TDG_ClientInfo {
	INT16				nClientHeight;
	INT16				nClientWidth;
	INT16				nClientAtX;
	INT16				nClientAtY;
	BoxInstRec	ClientDisplayRect;
	INT16				nClientTemp1;
	INT16				nClientTemp2;
	INT16				nClientTemp3;
	INT16				nClientTemp4;
	TCHAR				szClientTemp1[256];
	TCHAR				szClientTemp2[256];
	MEMPTR				ClientText;			/* an unlimited amount of text */
} TDG_ClientInfo;

/************************************************************************
 *************************************************************************
 * Advanced 3D New-Look (OpenGL) Data Structures (10nov97/bgw)
 *************************************************************************
 *************************************************************************/

/* 
 ** from std_ogl.h... 
 */
INT32 PUBLIC OGL_RegisterPort(INT32 lRef, HDC hPort);
INT16 PUBLIC OGL_InitOffscreen(BOOLEAN16 bRenderToDIB, HDC hPort, INT16 nMaxWidth, INT16 nMaxHeight);
INT16 PUBLIC OGL_BlitOffscreen(HDC hDestPort, Rect32 * rcDest);
HDC PUBLIC OGL_GetOffscreenDC(void);
INT32 PUBLIC OGL_RegisterOffscreenPort(void);
void PUBLIC OGL_BeginFirstPass(void);
BOOLEAN16 PUBLIC OGL_DrawBegin(INT32 lRef, DrawEnvPtr pDE, GraphPtr pGraph);
BOOLEAN16 PUBLIC OGL_DrawEnd(INT32 lRef, DrawEnvPtr pDE, GraphPtr pGraph);
void PUBLIC OGL_SetTessellationDetail(REAL64 fDetail);
REAL64 PUBLIC OGL_GetTessellationDetail();
void PUBLIC OGL_GetChartStatus(GraphPtr pGraph, BOOLEAN16 * pbDoAdv3D, BOOLEAN16 * pbDoAdv3DTitles, BOOLEAN16 * pbIs2D3D);
INT16 PUBLIC OGL_SaveLighting(LPTSTR szPath, GraphPtr pGraph);
INT16 PUBLIC OGL_LoadLighting(LPTSTR szPath, GraphPtr pGraph);
void PUBLIC OGL_SetModelPath(LPTSTR szPath);

/* 
 ** OGL UI tracker defines & functions... 
 */
void PUBLIC OGL_TrackerInit(GraphPtr pGraph, HDC hPort);
void PUBLIC OGL_TrackerDone(GraphPtr pGraph);
void PUBLIC OGL_TrackerBeginDrag(GraphPtr pGraph, INT16 nControlType, INT16 nInitialMouseX, INT16 nInitialMouseY, BOOLEAN16 bPrimaryButton);
void PUBLIC OGL_TrackerUpdateDrag(GraphPtr pGraph, INT16 nMouseX, INT16 nMouseY);
void PUBLIC OGL_TrackerEndDrag(GraphPtr pGraph);
void PUBLIC OGL_TrackerDrawChart(GraphPtr pGraph, HDC hPort, RECT * prcBounds);
void PUBLIC OGL_TrackerUseWorkStruct(GraphPtr pGraph, BOOLEAN16 bUseWorkStrcut);
#define OGLTRACK_BTN1			0			/* id for primary	mouse button				*/
#define OGLTRACK_BTN2			1			/* id for secondary	mouse button			*/
typedef enum _OGLTrackerType
{
	OGLTRACK_NONE,								/* placeholder for start of range			*/
	OGLTRACK_LIGHT1_XYZ,					/* Light1's XYZ position							*/
	OGLTRACK_LIGHT2_XYZ,					/* Light1's XYZ position							*/
	OGLTRACK_LIGHT3_XYZ,					/* Light1's XYZ position							*/
	OGLTRACK_2D3D_XY_DIST					/* XY position and distance for 2D3D	*/
} OGLTrackerType;

/* from common.h... */
/************************************************************************
 * Material3DStruct (Advanced 3D)
 ************************************************************************/
typedef struct {
	REAL64 						fDiffRed;							/* Diffuse shading, Red component						*/
	REAL64 						fDiffGreen;						/* Diffuse shading, Green component					*/
	REAL64 						fDiffBlue;						/* Diffuse shading, Blue component					*/
	REAL64 						fSpecRed;							/* Specular shading, Red component					*/
	REAL64 						fSpecGreen;						/* Specular shading, Green component				*/
	REAL64 						fSpecBlue;						/* Specular shading, Blue component					*/
	REAL64						fShininess;						/* Shininess factor													*/
	REAL64						fTextureTint;					/* How much does native color tint texture	*/
	REAL64						fTextureScale;				/* Scaling of texture (1.0 = normal)				*/
	INT32							nFlags;               /* flags																		*/
} Material3DStruct, * Material3DPtr;

/************************************************************************
 * Light3DStruct (Advanced 3D)
 ************************************************************************/
typedef struct {
	BOOLEAN16					bLightOn;							/* Is this light on?										*/
	REAL64 						fRed;									/* Light color, Red component						*/
	REAL64 						fGreen;								/* Light color, Green component					*/
	REAL64 						fBlue;								/* Light color, Blue component					*/
	REAL64						fDiffuseAmt;					/* Light diffuse illumination	(0.0-1.0)	*/
	REAL64						fSpecularAmt;					/* Light specular illumination (0.0-1.0)*/
	REAL64 						fPositionX;						/* Light position, X axis								*/
	REAL64 						fPositionY;						/* Light position, Y axis								*/
	REAL64 						fPositionZ;						/* Light position, Z axis								*/
	REAL64						fAttenuation;					/* Light distance attenuation						*/
	INT32							nFlags;               /* flags																*/
} Light3DStruct, * Light3DPtr;


/*  BEGIN Added by FL 4-30-98 from pgedit.h */
/*****************************************************************************
 * BLADepthClass - defines depth effect for the bar, line area chart family.
 ******************************************************************************/
#define DEPTHMODE_NONE			0	/* Normal chart. */
#define DEPTHMODE_ISOSHEAR		1	/* Fake 3D effect on risers. */
#define DEPTHMODE_TRUE3D		2	/* True 3D effect on risers. */



enum Axes { 															/* Names of the coordinate axes.	*/
	xx, yy, zz, cc
};

/*  END Added by FL 4-30-98 from pgedit.h */

/************************************************************************
 * from DETECTIV.H
 ************************************************************************/

typedef struct {
	UINT32       offset;     /* offset to next DetNode */
	INT16        nNodeType;  /* DetNode shape */
	INT16        nObjectID;  /* object ID */
	INT16        nSeriesID;  /* series ID or -1 */
	INT16        nGroupID;   /* group  ID or -1 */
	Rect32         detBounds;  /* enclosing rectangle */
	AreaInstPtr  pArea;      /* area instance */
	LineInstPtr  pLine;      /* line instance */
	FontInstPtr  pFont;      /* font instance */
	union {
		CrustInfoRec   crust;
		LineInfoRec    line;
		PolyInfoRec    poly;
		RegionInfoRec  region;
		SliceInfoRec   slice;
		TextInfoRec    text;
		WedgeInfoRec   wedge;
		PathInfoRec    path;
	} info;
	INT16  tagSize;         /* size of special tagging info */
	BYTE   tagInfo[2];      /* where the tag info lives */
} DetNodeRec, * DetNodePtr;



/************************************************************************
 *  M a c r o s
 ************************************************************************/
#define IsValidDetNodeRef(pDetRef) ((pDetRef)->offset != 0)


/************************************************************************
 *  D e t N o d e   F u n c t i o n s
 ************************************************************************/
void PUBLIC
FreeDetNode(DetNodeRefPtr pDetRef);


INT16 PUBLIC
GetDetNodeObjectID(DetNodePtr pNode);


INT16 PUBLIC
XorDetNode(
		DrawEnvPtr			pDE, 
		DetNodePtr			pNode
);


INT16 PUBLIC
XorDetNodeRef(
		DrawEnvPtr			pDE, 
		DetNodeRefPtr		hDetRef
);

DetNodePtr PUBLIC
LockDetNodeRef(DetNodeRefPtr hDetRef);

INT16 PUBLIC
UnlockDetNodeRef(DetNodeRefPtr hDetRef);

DetListHdl PUBLIC
AllocDetListHandle(void);

void PUBLIC
FreeDetListHandle(DetListHdl * phDetList);

DetListHdl PUBLIC
SwapDetListHandle (GraphPtr pGraph, 
		INT16  LayerID, 
		DetListHdl hDetList);

/************************************************************************
 * from COM_SLCT.H
 ************************************************************************/

#define SELECT_NUM_HANDLES 8
/* hit test handle positions */
typedef enum _HandleType{
	HTTopLeft,						/* 0 */
	HTTopRight,						/* 1 */
	HTBottomLeft,					/* 2 */
	HTBottomRight,				/* 3 */
	HTCenterTop,					/* 4 */
	HTCenterRight,				/* 5 */
	HTCenterLeft,					/* 6 */
	HTCenterBottom,				/* 7 */

	MAX_HITTEST_HANDLES
} HandleType;

/*******************
 *  S e l I t e m  *
 *******************/
enum SelItemDataType {
	DETNODE,
	ANODE,
	GEODE,
	GROUPODE
};

#define GROUP_LAYER (MAX_LAYER + 1)

/****************************
 *   S e l L i s t I n f o  *
 ****************************/
enum SelListInfoIndexType {
	SLI_NUMSELECTED,
	SLI_NUMANODESSELECTED,
	SLI_NUMGEODESSELECTED,
	SLI_NUMDETNODESSELECTED,
	SLI_NUMGROUPSSELECTED,
	SLI_NUMMOVABLESELECTED,
	SLI_NUMTEXTBOXSELECTED,
	SLI_NUMNONDELETE,
	SLI_NUMGROUPSATTACHED,
	SLI_NUMSELWITHLINE,
	SLI_NUMSELWITHAREA,
	SLI_NUMSELWITHFONT
};

/**********************************************************************************************
 *   Selection List Relations - Used in Select_AddRelatedItems  and Select_RemoveRelatedItems *
 **********************************************************************************************/
enum SelListRelationIndexType {
	SLR_SAME_OBJECT,
	SLR_SAME_SERIES,
	SLR_SAME_GROUP,
	SLR_SAME_AREAINST,
	SLR_SAME_LINEINST,
	SLR_SAME_FONTINST,
	SLR_SAME_SG,
	SLR_DEFAULT
};

typedef struct SelItemRec {
	struct SelItemRec *	pNext;
	void 					*	pSiblings;					/* The list this guy's in */
	void					*	pChildren;					/* Pointer to child list if 	*/
	/* this is a group, else NULL */

	INT16						nLayer;								/* layer  ID */
	INT16 					nObjectID;						/* object ID */
	INT16 					nSeriesID;						/* series ID */
	INT16 					nGroupID; 						/* group	ID */

	Rect32						rcBounds;							/* outer bounds */
	Rect32						rcLastBounds;				 	/* last bounds before move */
	Point32						p0;									/* start point of line */
	Point32						p1;									/* end point of line */
	INT16						nHandleCount;					/* # of handle rects */
	Rect32						rcHandles[8];					/* always 8 handles for now... */

	INT16						nType;								/* SelItemDataType */
	DetNodeRef			refDetNode;
	union {
		AnodeInfoHdl		hAnode;
		GeodeInfoHdl		hGeode;
	} data;

} SelItemRec, * SelItemPtr;


/************************************************************************
 *  SelListRec -- Selection List Header
 ************************************************************************/
typedef struct _SelListRec {
	INT16 					nItemCount;

	SelItemPtr						pHead;   /* Pointer to first item in this list. */
	SelItemPtr						pGroups;
	SelItemPtr						pParent;		/* Pointer to parent Group Item. 		*/
	SelItemPtr						pParentMarker; 	/* Pointer to parent Group Marker item. */

} SelListRec, * SelListPtr;


SelListPtr PUBLIC			Select_AllocList			(	void );
INT16 PUBLIC					Select_FreeList				( SelListPtr pSelList );
SelItemPtr PUBLIC			Select_GetNextItem(SelItemPtr pCurr, INT16	goIntoGroups);
SelItemPtr PUBLIC			Select_GetFirstItem(SelListPtr pSelList, INT16 goIntoGroups);

INT16 PUBLIC					Select_ClearList			(	SelListPtr pSelList );

INT16 PUBLIC					Select_RebuildItem		(	GraphPtr 				pGraph, 
		SelItemPtr 			pItem	);
INT16 PUBLIC					Select_RebuildList		(	GraphPtr				pGraph,
		SelListPtr			pSelList  );

SelItemPtr PUBLIC			Select_AddItem				(	GraphPtr				pGraph,
		SelListPtr			pSelList,
		DetNodeRefPtr		pDetRef   );

INT16 PUBLIC					Select_RemoveItem			(	SelListPtr			pSelList,
		SelItemPtr			pItem			);

SelItemPtr PUBLIC			Select_SearchList			(	SelListPtr			pSelList,
		DetNodeRefPtr		pDetRef   );

INT16 PUBLIC					Select_IsEqual				(	SelItemPtr			pOne,
		SelItemPtr			pTwo      );

INT16 PUBLIC					Select_CalcHandlesList(	DrawEnvPtr			pDE,
		SelListPtr			pSelList  );

INT16 PUBLIC					Select_CalcHandlesItem(	DrawEnvPtr			pDE,
		SelItemPtr			pItem     );

INT16 PUBLIC					Select_DrawXorList		(DrawEnvPtr			pDE,
		SelListPtr			pSelList  );

INT16 PUBLIC					Select_DrawXorItem		(DrawEnvPtr			pDE,
		SelItemPtr			pItem     );

INT16 PUBLIC					Amigo_Select_DrawXorList		(	GraphPtr pGraph,	DrawEnvPtr			pDE,
		SelListPtr			pSelList  );

INT16 PUBLIC					Amigo_Select_DrawXorItem		(	GraphPtr pGraph,	DrawEnvPtr			pDE,
		SelItemPtr			pItem     );


SelItemPtr PUBLIC			Select_HandleHitTest	(	SelListPtr			pSelList,
		Point32						mousePt,
		INT16					*whichHandle,
		Rect32 				*pItemRect);

void PUBLIC						GetFirstDetNodeRef		(	SelListPtr			pSelList,
		DetNodeRefPtr		pDetRef   );

void PUBLIC						Item2DetRef						(	SelItemPtr			pItem,
		DetNodeRefPtr		pDetRef   );


SelListPtr PUBLIC				Select_DuplicateList		(	SelListPtr			pSelList	);

SelItemPtr PUBLIC
SelectDetNode	(	GraphPtr			pGraph,
		DrawEnvPtr		pDE,
		SelListPtr		pSelList,
		DetNodeRefPtr	pDetRef,
		INT16				append	 	);

INT16 PUBLIC
SelectDetNodesInRect	(	GraphPtr			pGraph,
		INT16			LayerID,
		SelListPtr		pSelList,
		Rect32				*pBounds,
		DrawEnvPtr		pDE,
		INT16				append		);

INT16 PUBLIC
SelectAllDetNodes (
		GraphPtr	pGraph,
		INT16		layerID,
		SelListPtr	pSelList );

INT16 PUBLIC
Select_Group(
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16					compactNow
);

INT16 PUBLIC
Select_UnGroup(
		GraphPtr				pGraph,
		SelListPtr			pSelList
);

INT16 PUBLIC
Select_GetListBounds(
		SelListPtr			pSelList,
		Rect32					*pRect
);

INT16 PUBLIC
Select_GetListInfo(
		SelListPtr			pSelList,
		INT16						nIndex
);

INT16 PUBLIC
Select_IsObjectSelected(
		SelListPtr			pSelList,
		INT16						nObjectID
);

INT16 PUBLIC
Select_AddRelatedItems(	
		GraphPtr			pGraph,				/* Pointer to graph		*/
		SelListPtr		pSelList,			/* SelList to update	*/
		DetNodeRefPtr	pDetRef,			/* Target for search	*/
		INT16					nRelation,		/* Relation to find		*/
		INT16					bSelectTarget,/* Select pDetRef too?*/
		INT16					nObjectFilter	/* Object ID constrain*/
);

INT16 PUBLIC
Select_RemoveRelatedItems(	
		GraphPtr		pGraph,				/* Pointer to graph		*/
		SelListPtr		pSelList,			/* SelList to update	*/
		SelItemPtr		pItem,				/* item to remove */
		INT16			nRelation,		/* Relation to find		*/
		INT16			nObjectFilter	/* Object ID constrain*/
);

/* DW:(11/24/93) Added these next two routines for */
LPPOINT PUBLIC 
CreateSelItemOutline(
		DrawEnvPtr			pDE,
		SelItemPtr			pItem,
		PINT16				  pnPoints);

INT16 PUBLIC
DestroySelItemOutline(
		Point32 *			ptData);

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

INT16 PUBLIC Select_GetSelectedSeriesID( SelListPtr pSelList );
BOOLEAN16 PUBLIC IsMarkerSelected( SelListPtr pSelList );

/*  11-12-98/FL Added these 2 functions */
BOOLEAN16 PUBLIC Select_IsSeriesRelatedObject( int nObjectID );
INT16 PUBLIC Select_AddSeriesRelatedObjects(	GraphPtr		pGraph,				/* Pointer to graph		*/
		SelListPtr		pSelList,			/* SelList to update	*/
		INT16			nObjectID,
		INT16			nSeriesID,			/* series to add */
		BOOLEAN16		bSelectAll);		/* force to select all the series 
																	 related items */

/*  20dec98DW: Added these 2 functions */
INT16 PUBLIC Select_AddGroupsRelatedObjects(	GraphPtr		pGraph,				/* Pointer to graph		*/
		SelListPtr		pSelList,			/* SelList to update	*/
		INT16			nObjectID,
		INT16			nGroupID,			/* group to add */
		BOOLEAN16		bSelectAll);		/* force to select all the group 
																	 related items */


INT16 PUBLIC Select_AddHeightRelatedObjects(	GraphPtr		pGraph,				/* Pointer to graph		*/
		SelListPtr		pSelList,			/* SelList to update	*/
		SelItemPtr		pSelItem );		/* Area to add */


/*  END Added by FL 4-30-98 from pgedit.h */

/*  Select all the data objects present in the chart  */
INT16 PUBLIC Select_SelectAllSeriesRelatedObjects(	GraphPtr			pGraph,
		SelListPtr			pSelList,
		BOOLEAN16				bLegendMarkers );


/************************************************************************
 * from COM_RNGE.H
 ************************************************************************/

typedef struct {

	/*
	 * Caller sets these variables.
	 */

	REAL	fRawMin;
	REAL	fRawMax;

	BOOLEAN16	bIsLog;

	BOOLEAN16	bManualMajorHash;
	INT16		nMajorHashCount;		/* Manual hash count. */

	INT16		nManualScale;

	/*
	 * Routine sets these variables.
	 */


	INT16 					nMajorHash;
	REAL						fMin;				/* Requires formatting for display. */
	REAL						fMax;				/* Requires formatting for display. */

} ScaleClass, *ScalePtr;

INT16 PUBLIC
DetermineScale(
		ScalePtr	pScale
);


/************************************************************************
 * from COM_GRAF.H
 ************************************************************************/

GraphPtr PUBLIC			AllocGraphPtr			 (void); 										/* get bag for globals */

void PUBLIC					FreeGraphPtr			 (GraphPtr pGraph);					/* give it back */

void PUBLIC					FreeColorInstSFX	 (ColorInstPtr pColor);

BOOLEAN16 PUBLIC
FindNextDetNode(
		DrawEnvPtr     pDE,
		GraphPtr       pGraph,   /* ptr to Graph struct */
		Point32          *pMouser, /* in virtual coords */
		DetNodeRefPtr  pDetRef,  /* struct filled upon success */
		BOOLEAN16      bFound    /* FALSE = First Time; TRUE = Subsequent Finds */
);

BOOLEAN16 PUBLIC
FindDetNode(
		DrawEnvPtr     pDE,
		GraphPtr       pGraph,    /* ptr to Graph struct */
		Point32          *pMouser,  /* in virtual coords */
		DetNodeRefPtr  pDetRef    /* struct filled upon success */
);

BOOLEAN16 PUBLIC
FindDetNodeEx(
		DrawEnvPtr     pDE,
		GraphPtr       pGraph,       /* ptr to Graph struct */
		Point32          *pMouser,     /* in virtual coords */
		DetNodeRefPtr  pDetRef,      /* struct filled upon success */
		INT16          nExcludeLayer
);

void PUBLIC					FreeAllDetNodes		 (GraphPtr pGraph);

INT16 PUBLIC				GetDetNode				 (GraphPtr				pGraph,
		INT16						objectID,
		INT16						seriesID,
		INT16						groupID, 
		INT16 					caseID,
		DetNodeRefPtr 	pDetRef );


INT16 PUBLIC
GetNextDetNode(
		GraphPtr			pGraph,
		INT16					objectID,
		INT16					seriesID,
		INT16					groupID,
		INT16					caseID,
		DetNodeRefPtr	pDetRef
);

INT16 PUBLIC												/* NEW FUNCTION: 12/6/92 DW */
GetRiserDetNodeLimit(
		GraphPtr			pGraph
);

VOID PUBLIC													/* NEW FUNCTION: 12/6/92 DW */
SetRiserDetNodeLimit(
		GraphPtr			pGraph,
		INT16					MaxNum
);

/************************************************************************
 * from COM_DGRF.H
 ************************************************************************/

BOOLEAN16 Toggle4DFlag(void);

INT16 PUBLIC
IsGraphBackgroundVisible(GraphPtr pGraph);

INT16 PUBLIC
DrawBackgroundLayer(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph
);


INT16 PUBLIC
DrawGraphLayer(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph
);


INT16 PUBLIC
DrawAnnotationLayer(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph
);


INT16 PUBLIC
DryRunTheGraph(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		INT16						wantGraph,
		INT16						wantBackground,
		INT16						wantAnnotation
);


INT16 PUBLIC
DrawTheGraph(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		short						wantGraph,
		short						wantBackground,
		short						wantAnnotation
);


/************************************************************************
 * from COM_UTIL.H
 ************************************************************************/

/* return current spreadsheet page num */
INT32 PUBLIC
GetGraphPage(GraphPtr pGraph);

BOOLEAN16	PUBLIC	IsObjectATextBox
(
		INT16						nObjID								/* Object's ID number.    					*/
);

/* return engine class */
INT16 PUBLIC
GetGraphEngineClass(GraphPtr pGraph, INT16 layerID, SelItemPtr pItem);

/* return numeric graphtype index */
INT16 PUBLIC
GetGraphTypeIndex(GraphPtr pGraph);

/* return current graph type name */
LPTSTR PUBLIC 
GetGraphTypeName(GraphPtr pGraph);

/* return numeric value for name */
INT16 PUBLIC
GetGraphTypeIndexFromName(LPTSTR name);

/* returns name for value */
LPTSTR PUBLIC 
GetGraphTypeNameFromIndex(INT16 idx);

WashBandPtr PUBLIC
GetGraphWashBandTbl(GraphPtr pGraph);

INT16 PUBLIC
SetGraphDataInfo(
		GraphPtr				pGraph,								/* ptr to valid graph */
		GraphDataPtr		pGraphData						/* ptr to caller supplied struct */
);

void PUBLIC
GetGraphDataInfo(
		GraphPtr				pGraph,								/* ptr to valid graph */
		GraphDataPtr		pGraphData						/* ptr to caller supplied struct */
);

/*  Supply hook function called when data is beeing dragged 12-20-99/FL */
INT16 PUBLIC
SetGraphResizeDataInfo(
		GraphPtr				pGraph,								/* ptr to valid graph */
		GraphResizeDataPtr		pGraphDataResize					/* ptr to caller supplied struct */
);

void PUBLIC
GetGraphResizeDataInfo(
		GraphPtr				pGraph,								/* ptr to valid graph */
		GraphResizeDataPtr		pGraphDataResize					/* ptr to caller supplied struct */
);

/*  BEGIN Added (14feb99/bgw) from com_util.h for color schemes */

INT16 PUBLIC
Scheme_UpdateColorInst(ColorInstPtr pColor, ColorInstRec * pColorScheme);

INT16 PUBLIC
Scheme_SetEntry
(
		GraphPtr	pGraph,
		INT16			nEntry,
		UINT16		nRed,
		UINT16		nGreen,
		UINT16		nBlue
);

INT16 PUBLIC
Scheme_GetEntry
(
		GraphPtr	pGraph,
		INT16			nEntry,
		UINT16 *	pnRed,
		UINT16 *	pnGreen,
		UINT16 *	pnBlue
);

/*  END Added (14feb99/bgw) from com_util.h for color schemes */

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

INT16 PUBLIC GetGraphOrientation( GraphPtr pGraph );
INT16 PUBLIC GetDataFormatID(	GraphPtr pGraph );
INT16 PUBLIC IsDualYAxes(	GraphPtr pGraph );
INT16 PUBLIC GetGraphSubType( GraphPtr pGraph	);
BOOLEAN16 PUBLIC IsBipolar( GraphPtr pGraph	);

/*  END Added by FL 4-30-98 from pgedit.h */

/************************************************************************
 * from DLLFLOAT.H
 ************************************************************************/

void CALLBACK 		ConvertFltToStr				(double, LPTSTR);

#include "pgsdk/iorw.h"  //IOPATHPTR IO*


/************************************************************************
 * from tdi_graf.h
 ************************************************************************/

/*** start TDGLIB ***/

#define REQUEST_LOOK				 	0x0001	/* Graph visual characteristics. */
#define REQUEST_TCLOOK				0x0002	/* Table chart visual characteristics. */
#define REQUEST_DLOOK					0x0004	/* Data related characteristics. */
#define REQUEST_TEXTOBJECT		0x0008	/* Text charts. */
#define REQUEST_ANODE					0x0010	/* Annotation. */
#define REQUEST_CELL					0x0020	/* Graph data. */
#define REQUEST_FONTMASTER		0x0040	/* Graph fonts. */
#define REQUEST_BACKGROUND		0x0080	/* Background area instance. TL:11/17/92 w/MR: */

/*
 * BEGIN OF
 * supported request flags for the new TIFF File format
 * based upon the flags defined above these
 *
 */
#define REQUEST_TIFFTHUMBNAIL	0x0100	/* TIFF Thumbnail */
#define REQUEST_PICTURES		0x0400	/* Pictures stored in file 01-21-99/FL */
#define REQUEST_TDL						0x04DF 	/* Look of the Graph only */
#define REQUEST_TDT						0x045F	/* Full chart tag aggregation except CELL and
																				 DLOOK (Template) */ 
#define REQUEST_TDC						0x04FF	/* Look of the Graph and VDM Data */
#define REQUEST_TDF						0x05FF	/* Look of the Graph, VDM Data and TIFF Thumbnail */
#define REQUEST_THUMBFILEDESCRIPTION	0x0600	/* Thumbnail File Description */
/*
 * END OF 
 * supported request flags for the new TIFF File format
 *
 */

/* TL:11/23/92 added read/write requests to support layers */
#define REQUEST_TDCBACKGROUND	0x0080
#define REQUEST_TDCGRAPH			0x046F
#define REQUEST_TDCANNOTATION 0x0010

/* TL:11/23/92 added read/write requests to support layers */
#define REQUEST_TDLBACKGROUND	0x0080
#define REQUEST_TDLGRAPH			0x044F
#define REQUEST_TDLANNOTATION 0x0010

#define REQUEST_3D3						0x04FF		/* Full chart tag aggregation. */
#define REQUEST_3DL						0x040DF		/* Look only chart tag aggregation. */
#define REQUEST_3DT	0x005F							/* Full chart tag aggregation except CELL and 
																					 DLOOK (Template) */
#define REQUEST_3DF						0x05FF		/* Look of the Graph, VDM Data and TIFF Thumbnail */

#define REQUEST_COLORSCHEME_ONLY		0x1000		/* Load only the color scheme info 6-2-00/FL  */



/************************************************************************
 * from load.h
 ************************************************************************/

#define FILEFMT_BINARY 					1		/*  NORMAL FLAG */
#define FILEFMT_ASCII 					2		/*  DEBUG FLAG */

#define TERMINATOR_NO 					0		/*  NORMAL FLAG */
#define TERMINATOR_YES 					1		/*  DEBUG FLAG */

INT16 PUBLIC 
Load_TIFFGraph (
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		LPTSTR		pszFileName		/* Ptr to a "C" style string filename */
);

INT16 PUBLIC 
Load_TIFFGraphDefault (
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest		/* As defined tdi_graf.h */
);

INT16 PUBLIC
Load_TIFFGraphStored (
		GraphPtr  pGraph,     /* Ptr to a preallocated zeroed Graph structure */
		UINT32    ulRequest,   /* As defined tdi_graf.h */
		int n
);

void PUBLIC
Load_GraphSamplePreset(
		GraphPtr pGraph,
		INT16     preset
);

INT16 PUBLIC
Save_TIFFGraph (
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator,	/* Whether to add terminators after each record */
		LPTSTR		pszFileName		/* Ptr to a "C" style string filename */
);

/*****************************************************************************
 *  !NAME!
 *     Save_TIFFGraphPath
 *
 *  DESCRIPTION:
 *		Saves a graph structure into a binary or ascii file.   The file
 *		is already opened by the time this routine is called.  ie. pIOPathFile
 *		contains valid data.  We assume the Graph structure has been 
 *		ZEROED and preallocated.
 *
 *
 *  RETURNS:
 *
 ******************************************************************************/
INT16 PUBLIC
Save_TIFFGraphPath(
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator		/* Whether to add terminators after each record */
);

/*****************************************************************************
 *  !NAME!
 *     Save_TIFFGraphPathTrim
 *
 *  DESCRIPTION:
 *		Like above function, but gives option to trim array elements > 64.
 *		Useful for creating templates from charts of large datasets where
 *		normal save might create an extremely large .3tf file on disk.
 *
 *  RETURNS:
 *
 ******************************************************************************/
INT16 PUBLIC
Save_TIFFGraphPathTrim(
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator,	/* Whether to add terminators after each record */
		BOOLEAN16	bWantTrim		/* Control whether we trim excess vararray elements */	
);

/* Set the minimum and maximum number of Series Info that will be stored for any graph when Save_TIFFGraphPathTrim() is called. */
void PUBLIC
SetSaveSeriesMinMax(
		INT16	nMinSeries,
		INT16	nMaxSeries
);

/* Get the minimum and maximum number of Series Info that will be stored for any graph when Save_TIFFGraphPathTrim() is called. */
void PUBLIC
GetSaveSeriesMinMax(
		PINT16	pnMinSeries,
		PINT16	pnMaxSeries
);

/*****************************************************************************
 *  !NAME!
 *     Load_TIFFGraphPath
 *
 *  DESCRIPTION:
 *		Loads a graph structure with the proper information.  The file
 *		is already opened by the time this routine is called.  ie. pIOPathFile
 *		contains valid data.
 *
 *  RETURNS:
 *		An Error Code as defined in com_errs.h for files.
 *
 ******************************************************************************/
INT16 PUBLIC
Load_TIFFGraphPath(
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest		/* As defined tdi_graf.h */
);

/* Clone a GraphPtr using TIFF loader/saver engine.  */
GraphPtr PUBLIC
Clone_TIFFGraph(
		GraphPtr    pGraph	        /* a pointer to our graph object  */
);

#define MAXATTRIBUTES 32			/* max # of attribues allowed in a list */

//#include "pgsdk/oidenum.h" 

#include "pgsdk/attrenum.h" //enum pg_Attribute



/************************************************************************
 * from CRO_ATTR.H
 ************************************************************************/

/*
 *  Graph Preset Type Info
 */
typedef struct {
	INT16			nMajorType;
	INT16			nMinorType;
} TDG_GraphPresetInfo, *TDG_GraphPresetPtr;

typedef struct {
	INT16			nSeriesID;
	RGB16			rgb;
} TDG_SeriesColorInfo, *TDG_SeriesColorPtr;


/************************************************************************
 * from COM_ATTR.H
 ************************************************************************/

INT16 PUBLIC				GetGraphAttrSL			(	GraphPtr	 			pGraph, 
		SelListPtr			pSelList,
		INT16						attrCode,
		void *			pData     );

INT16 PUBLIC				SetGraphAttrSL			(	GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16						attrCode,
		void * 			pData			);

INT16 PUBLIC				GetGraphAttrSI			(	GraphPtr	 			pGraph, 
		SelItemPtr			pItem,
		INT16						attrCode,
		void *			pData     );

INT16 PUBLIC				SetGraphAttrSI			(	GraphPtr				pGraph,
		SelItemPtr			pItem,
		INT16						attrCode,
		void * 			pData			);

INT16 PUBLIC				ExecGraphFuncSL			(	DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16						attrCode,
		void * 			pData			);

INT16 PUBLIC				GetGraphAttr				(	GraphPtr				pGraph,
		INT16						nLayerID,
		INT16						nObjectID,
		INT16						nSeriesID,
		INT16						nGroupID,
		INT16						attrCode,
		void *			pData			);

INT16 PUBLIC        SetGraphAttr        (
    GraphPtr        pGraph,
    INT16           nLayerID,
    INT16           nObjectID,
    INT16           nSeriesID,
    INT16           nGroupID,
    INT16           attrCode,
    void *      pData     );

INT16 PUBLIC
GetAttrSize(
		INT16 attrCode
);

INT16 PUBLIC        SetGraphAttr_check       (
    GraphPtr        pGraph,
    INT16           nLayerID,
    INT16           nObjectID,
    INT16           nSeriesID,
    INT16           nGroupID,
    INT16           attrCode,
    void *      pData,
    const char *    pDataStr,
    INT16           pDataSize,
    const char *    file,
    int             line);

INT16 PUBLIC				ExecGraphFunc				(	DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		INT16						nLayerID,
		INT16						nObjectID,
		INT16						nSeriesID,
		INT16						nGroupID,
		INT16						attrCode,
		void *			pData			);


INT16 PUBLIC				GetDetNodeAttr			(	GraphPtr				pGraph,
		INT16						nLayerID,
		DetNodeRefPtr		pDetRef,
		INT16						attrCode,
		void *			pData			);

INT16 PUBLIC				SetDetNodeAttr			(	GraphPtr				pGraph,
		INT16						nLayerID,
		DetNodeRefPtr		pDetRef,
		INT16						attrCode,
		void *			pData			);

INT16 PUBLIC				ExecDetNodeFunc			(	GraphPtr				pGraph,
		INT16						nLayerID,
		DetNodeRefPtr		pDetRef,
		INT16						attrCode,
		void *			pData			);

INT16 PUBLIC 				AttrCode2EngineClass	(	INT16 nAttr			);
INT16 PUBLIC				Object2EngineClass		(	INT16 nObjID		);

INT16 PUBLIC
GetObjectMobility(
		GraphPtr		pGraph,
		INT16 			objID 								
);


INT16 PUBLIC
SetBoundsCommon(
		GraphPtr				pGraph,
		INT16 					attrCode,
		BoxInstPtr			pNewBounds
);


/************************************************************************
 *  1998July13 PPN: Advanced Number Format Types
 ************************************************************************/
typedef enum _AdvFormatTypes {
	NoFormat,		
	GeneralFormat,		
	NumberFormat,	
	ScientificFormat,	
	PercentFormat,	
	FractionFormat,	
	CurrencyFormat,	
	DateFormat,	
	TimeFormat,
	DateTimeFormat,
	ZipCodeFormat,		/*   ex. input: 90067, output: 90067 */
	ZipCodePlus4Format,	/*   ex. input: 900675908, output: 90067-5908 */
	SSNFormat,			/*   ex. input: 220450867, output: 220-45-0867 */
	PhoneFormat,		/*   ex. input: 3105533313, output: (310) 553-3313 */
	TextFormat,
	CustomFormat		/*   Added 6-18-99/FL  */
} AdvFormatTypes;

/************************************************************************
 *  1998July13 PPN: Advanced Number Format Structure
 ************************************************************************/
/*  The following structure members have same meaning as Win32 LCTYPE constants */
typedef struct _AdvFormatStruct {

	AdvFormatTypes		formatType;					

	UINT 		iLZero;				   /* Leading zero (0=no, 1=yes)  Win32 */
	UINT 		iDigits;			   /* # of precision digits       Win32 */
	UINT 		iGrouping;			   /* # of group digits           Win32 */
	UINT 		iNegNumber;			   /* negative number format code Win32 */
	_TCHAR 		szScientificSymbol[3]; /* scientific notation symbol */
	_TCHAR 		szDecimal[5];		   /* Decimal symbol				 Win32 */
	_TCHAR 		szThousand[5];		   /* Thousand separator symbol   Win32 */

	UINT 		iCurrDigits;		   /* # of precision digits		 Win32 */
	UINT 		iMonGrouping;		   /* # of group digits           Win32 */
	UINT 		iNegCurr;			   /* negative $ format code		 Win32 */
	UINT 		iPosCurr;			   /* positive $ format code		 Win32 */
	_TCHAR 		szCurrency[5];		   /* currency symbol			 Win32 */
	_TCHAR 		szMonDecimalSep[5];	   /* $ Decimal symbol			 Win32 */
	_TCHAR 		szMonThousandSep[5];   /* $ Thousand separator symbol Win32 */

	_TCHAR 		szTimeFormat[32];	   /* time format string			 Win32 */
	_TCHAR 		szDateFormat[32];	   /* date format string			 Win32 */

	_TCHAR 		szPrefixText[20];	   /* text you want prepended to output */
	_TCHAR 		szPostfixText[20];	   /* text you want appended to output */

	_TCHAR 		szNumColor[20];	   /* positive color name: red, green, etc. */
	_TCHAR 		szNegColor[20];	   /* negative color name: red, green, etc. */
	_TCHAR 		szZeroColor[20];   /* zero color name: red, green, etc. */

	BOOLEAN16	bPrefixPercentSymbol; /* =true if percent sign infront of number, false otherwise */
	double		dScaleFactor;		  /* factor to reduce number, ie, 1000 Kilo, 1000000 Mega  */
	BOOLEAN16	bNoNegSign;			  /* =true if do not wish to display negative sign on negative numbers, false otherwise */
	//LCID      localeID;             /* Locale ID  */


	_TCHAR 		szCustomFormat[256];	/*  custom number format		Win32	added 	6-18-99/FL */

} AdvFormatStruct, *AdvFormatPtr;


/************************************************************************
 * from STD_FMT.H
 ************************************************************************/

#define USER_FMT_ID			60	/*95jun1DW:  Arbitrarily decide User defined Num formats are this
															value or higher */

short PUBLIC
StdFormat(
		REAL         value,
		AdvFormatPtr pFormat,
		short        nFormat,
		_TCHAR *     szFormattedString,
		LCID         localeID
);

//9apr04DW: StdFormat returns some values as UTF8. But our UI likes things as MBCS so give it a conversion routine.
short PUBLIC
MBCS_StdFormat(
		REAL			value,		
		AdvFormatPtr	pFormat,	
		short			nFormat,	
		TCHAR			*szFormattedString,
		LCID			localeID
);

void PUBLIC GetAdvFormat( short nFormat, AdvFormatPtr pAdvFormat, UINT32 localID );

/*  fm 3/12/01 */
void PUBLIC SetAdvFormat( short *nFormat,AdvFormatPtr pAdvFormat );


double PUBLIC PackDate(short nDay, short nMonth, short nYear,
		short nHour, short nMin, short nSec );

void PUBLIC UnPackDateTime(short nDay, short nMonth, short nYear,
		short nHour, short nMin, short nSec, double dDate );

void PUBLIC InitAdvFormat( AdvFormatPtr pAdvFormat, UINT32 localID );

BOOLEAN16 PUBLIC ReplaceString ( _TCHAR *szDest, _TCHAR *szSrc, _TCHAR *szOldStr, _TCHAR *szNewStr );

/************************************************************************
 * from C2D_ATTR.H
 ************************************************************************/

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

typedef struct _TDG_TableStat {
	BOOLEAN16              bTitle;
	BOOLEAN16              bSubTitle;
	BOOLEAN16              bFootnote;
} TDG_TableStatInfo, * TDG_TableStatPtr;


/*  END Added by FL 4-30-98 from pgedit.h */

/************************************************************************
 *  GridLines Structure
 ************************************************************************/
typedef struct _TDG_GridLines {
	BOOLEAN16 				bIsLog; 							/* log flag 							*/
	BOOLEAN16 				bMajorHashManual; 		/* major hash manual flag */
	BOOLEAN16 				bMinorHashManual; 		/* minor hash manual flag */
	INT16 					nMajorHashStyle;			/* major hash style 			*/
	INT16 					nMinorHashStyle;			/* minor hash style 			*/
	INT16 					nMajorHashCount;			/* major hash count 			*/
	INT16 					nMinorHashCount;			/* minor hash count 			*/
} TDG_GridLinesInfo, * TDG_GridLinesPtr;


/************************************************************************
 *  GridLines Structure
 ************************************************************************/
typedef struct _TDG_GridLinesOrd {
	INT16 					nMajorHashStyle;			/* major hash style 			*/
	INT16 					nMinorHashStyle;			/* minor hash style 			*/
	INT16 					nMinorHashCount;			/* minor hash count 			*/
	/* TL:11/17/92 added super hash lines for every nSuperHashCount major grid lines */
	INT16						nSuperHashStyle;			/* super hash style				*/
	INT16						nSuperHashCount;			/* super hash every major hash count */
} TDG_GridLinesOrdInfo, * TDG_GridLinesOrdPtr;

/************************************************************************
 * Scale Values Structure
 ************************************************************************/
typedef struct _TDG_ScaleValues {
	INT16						nManualScale; 				/*	0-auto,
																						1-manual,
																						2-manual minimum, auto maximum,
																						3-auto minimum, manual maximum */
	REAL						fManualScaleMax;			/* Manual scale maximum. */
	REAL						fManualScaleMin;			/* Manual scale minimum. */
} TDG_ScaleValuesInfo, * TDG_ScaleValuesPtr;

/************************************************************************
 * Grid Step Values Structure
 ************************************************************************/
typedef struct _TDG_GridStepValues {
	BOOLEAN16					bUseGridStep;				/* use grid step */
	REAL						fGridStep;					/* major grid step */
} TDG_GridStepValuesInfo, * TDG_GridStepValuesPtr;

/************************************************************************
 * Histogram Buckets Structure
 ************************************************************************/
typedef struct _TDG_HistogramBuckets {
	BOOLEAN16 				bManual;							/* manual flag				*/
	INT16 					nBuckets; 						/* number of buckets	*/
} TDG_HistogramBucketsInfo, * TDG_HistogramBucketsPtr;


/************************************************************************
 * Table Divisions Structure
 ************************************************************************/
typedef struct _TDG_TableDivs {
	INT16 					StripeStyle;					/* None, vertical or horiziontal stripes. */
	INT16 					nLineSkip;						/* Stripes skipped between lines. */
	INT16 					nColorSkip; 					/* Row or columns per color. */
	INT16 					nActiveStripes; 			/* Color count. */
	BOOLEAN16 				bExtendedStripes; 		/* Stripes extended into header region? */
} TDG_TableDivsInfo, * TDG_TableDivsPtr;

/************************************************************************
 * SplitY Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_GroupSplitYND {
	INT16	nGroupID;
	BOOLEAN16	bSplitY;
} TDG_GroupSplitYNDInfo, * TDG_GroupSplitYNDPtr;

/*  23sep98/jst: added to support single call access to error bar values */
/************************************************************************
 * Series Dependant Error Bar Values Specific Structure
 ************************************************************************/
typedef struct _TDG_SerDepErrorBarValue {
	INT16				nSeriesID;
	INT16				nYorXErrorBar;			/* 0 = Get/Set Y Values, 1 = Get/Set X Values */
	INT16				nMode;
	INT16				nType;
	REAL64				fErrorBarPlus;			/* Value of Data Point32 PLUS this value is Error Line */
	REAL64				fErrorBarMinus;			/* Value of Data Point32 MINUS this value is Error Line */
	INT16				nHatSize;
	INT16				nStdDevMult;			/* Standard Deviation Multiplier */
} TDG_SerDepErrorBarValue, * TDG_SerDepErrorBarValuePtr;

/************************************************************************
 * Series Dependant INT16 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_SerDepINT16 {
	INT16	nSeriesID;
	INT16	nValue;
} TDG_SerDepINT16, * TDG_SerDepINT16Ptr;

/************************************************************************
 * Series Dependant BOOLEAN16 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_SerDepBOOLEAN16 {
	INT16	nSeriesID;
	BOOLEAN16	bValue;
} TDG_SerDepBOOLEAN16, * TDG_SerDepBOOLEAN16Ptr;

/************************************************************************
 * Group Dependant INT16 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_GroupDepINT16 {
	INT16	nGroupID;
	INT16	nValue;
} TDG_GroupDepINT16, * TDG_GroupDepINT16Ptr;

/************************************************************************
 * Group Dependant BOOLEAN16 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_GroupDepBOOLEAN16 {
	INT16	nGroupID;
	BOOLEAN16	bValue;
} TDG_GroupDepBOOLEAN16, * TDG_GroupDepBOOLEAN16Ptr;

/************************************************************************
 * Table Stripe Dependant BOOLEAN16 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_StripeDepBOOLEAN16 {
	INT16		nStripeID;
	BOOLEAN16	bValue;
} TDG_StripeDepBOOLEAN16, * TDG_StripeDepBOOLEAN16Ptr;

/************************************************************************
 * Series Dependent REAL64 Non-Detection Node Specific Structure
 ************************************************************************/
typedef struct _TDG_SerDepREAL64 {
	INT16	nSeriesID;
	REAL64	fValue;
} TDG_SerDepREAL64, * TDG_SerDepREAL64Ptr;

#define SERCON_USESELLIST	-10
#define SERCON_DOALL			-11


/************************************************************************
 * from COM_ERRS.H
 ************************************************************************/

/************************************************************************
 *  Base Values for All Error Messages
 ************************************************************************/
#define E00_BASE	0x0000								/* Common errors */
#define ELS_BASE  0x0100								/* LoadSave errors */
#define E2D_BASE	0x0200								/* 2D engine errors */
#define E3D_BASE  0x0300								/* 3D engine errors */
#define EPI_BASE  0x0400								/* Pie engine errors */
#define EAN_BASE  0x0500								/* Annotation engine errors */
#define EAC_BASE  0x0600								/* Std Access errors */
#define EDR_BASE  0x0700								/* Std Draw errors */
#define EFX_BASE  0x0800								/* SFX errors */
#define ETX_BASE  0x0900								/* Text Chart engine errors */
#define ETF_BASE	0x0A00								/* Tiff/3DF file errors */
#define ETDI_BASE	0x0B00								/* TypeDef Imaging errors. */


/************************************************************************
 * Error Classes:
 *		STATUS		- not an error, return of status
 *		WARNING		- recoverable error, no user intervention
 *		CONFIRM		- recoverable error, needs user yay or nay
 *		SPECIAL		- recoverable error, needs user to make a multiple-choice
 *		FATAL			- oh well...
 ************************************************************************/
typedef enum _ERROR_CLASSES {
	ERRCLASS_STATUS,
	ERRCLASS_WARNING,
	ERRCLASS_FATAL,
	ERRCLASS_CONFIRM,
	ERRCLASS_SPECIAL
} ERROR_CLASSES;


/************************************************************************
 * System-Wide Error Codes
 ************************************************************************/
typedef enum _E00_ERROR_CODES {
	E00_OK,											/* STATUS: Indicates successful function return result */

	E00_DISK_FULL,							/* WARNING: insufficient disk space */
	E00_FATAL_ERROR,						/* FATAL: Internal error */
	E00_FILE_CLOSE_ERROR,				/* WARNING: file close error */
	E00_FILE_NOT_FOUND,					/* WARNING: file not found */
	E00_FILE_READ_ERROR,				/* WARNING: file read error */
	E00_FILE_SEEK_ERROR,				/* WARNING: file seek error */
	E00_INVALID_NULL_ARG,				/* WARNING: NULL arg not valid here */
	E00_MEM_ERROR,							/* WARNING: out of memory */
	E00_NOT_IMPLEMENTED,				/* WARNING: Function not implemented */
	E00_RANGE_ERROR,						/* WARNING: Out of range error. */
	E00_USER_BREAK,							/* STATUS: User interrupted function by pressing escape */
	E00_USER_CANCEL,						/* STATUS: User canceled from dialog */

	E00_SINGLE_SERIES,					/* WARNING: Multiple series required. */
	E00_SINGLE_GROUP,						/* WARNING: Multiple groups required. */
	E00_NOT_SPLITY_DATA,				/* WARNING: Nothing assigned to Split Y axis, assigning half arbitrarily. */
	E00_NO_DATA,								/* WARNING: No data found. */
	E00_SERIESID_ERROR,					/* STATUS: Illegal series address reference. */
	E00_INVALID_STRUCT_SIZE,		/* ERROR: Passed structure wrong size. */
	E00_DOMAIN_ERROR,						/* ERROR: Illegal value. */
	E00_DDE_INIT,								/* DDE initialization failure. */
	E00_FILE_WRITE_ERROR,
	E00_FILE_OPEN_ERROR,				/* File is already open from another appl. */
	E00_FILE_NOTEXCEL,					/* Non-excel file opened as Excel */
	E00_FILE_NOTTEXT,						/* Non-TEXT file opened as TEXT */
	E00_SMARTFILL_NOMATCH,			/* SmartFill couldn't match the text */
	E00_SMARTFILL_BADCELL,			/* Wrong type of cell for SmartFill */
	E00_FILE_DELETE_ERROR,			/* Error deleting a file */
	E00_FILE_INVALIDTYPE_ERROR,	/* Error reading a file */
	E00_INVALID_PARAMETER,

	E00_MAX_ERROR
} E00_ERROR_CODES;

#define ReportError(p,id,class,lData) xReportError(p,id,class,lData,(LPTSTR)__FILE__,(INT16)__LINE__)

INT32 PUBLIC
xReportError(
		void 	*pContext,		/* Error context information (pGraph, pDE, etc.) */
		INT16		nErrorID,		/* Error code (Exx_yyyy) */
		INT16		nErrorClass,	/* Error class. */
		INT32		lData,			/* add'l info such as filename, etc. */
		LPTSTR		pFile,			/* offending filename */
		INT16		nLine			/* offending line number */
);


typedef INT16 ( *LPFNErrorCallBack) 
	(
		void 		*pContext,		/* Error context information (pGraph, pDE, etc.) */
		INT16		nErrorID,		/* Error code (Exx_yyyy) */
		INT16		nErrorClass,	/* Error class. */
		INT32		lData,			/* add'l info such as filename, etc. */
		LPTSTR		pFile,			/* offending filename */
		INT16		nLine			/* offending line number */
		);


		INT16 PUBLIC
		SetErrorCallBack(
				LPFNErrorCallBack	lpfnErrorCB
		);


		/************************************************************************
		 * from C2D_ERRS.H
		 ************************************************************************/

	typedef enum _E2D_ERROR_CODES {
			E2D_GEN_ERROR = E2D_BASE,

			E2D_MEMERR,					/* WARNING: Meory allocation problem. */
			E2D_ERRBSTACKED,			/* WARNING: Error bars not supported by stacked chart types. */
			E2D_LEGENDCOLOR,			/* CONFIRM: Legend box not applicable to group colored area chart. */
			E2D_PERCENTLOG,			/* WARNING: Logarithmic scale not applicable to percent chart. */
			E2D_NEGLOG,					/* WARNING: Negative values not possible on logarithmic scale. */
			E2D_ZEROLOG,				/* WARNING: Zero value requires infinite range with logarithmic scale. */
			E2D_CONTOURBSPL,			/* WARNING:	Less than two number of vertices on BSplines. */
			E2D_CONTOURLOG,			/* WARNING:	Zero or negative values require infinite range with logarithmic scale. */
			E2D_CONTOURSTEP,			/* WARNING:	Zero or negative intervals are not applicable. */
			E2D_CONTOURLEVELS,		/* WARNING:	Zero or negative number of Contour Levels are not applicable. */
			E2D_CONTOURSURFACE,		/* WARNING:	Zero or negative number of Contour Surfaces are not applicable. */
			E2D_CONTOURPOINTS,		/* WARNING:	Negative number of Contour Points are not applicable. */
			ETC_DC_OVERFLOW,			/* Current table chart overflows DC - set to all autofit. */
			E2D_BADORIENTATION,		/* Horz orientation in a vert-only chart or vice versa */
			E2D_RR_BADMINMAX,			/* One or more min/max values was invalid. */
			E2D_MAX_ERRORS

		} E2D_ERROR_CODES;


/************************************************************************
 * from C3D_ERRS.H
 ************************************************************************/

typedef enum _E3D_ErrorCodes {

	/************************** I N T E R N A L   E R R O R S *******************/
	E3D_CODE_NUMTOPOLINES = E3D_BASE,	
	/* WARNING:
		 More topo lines (grids, contours...) were
		 generated than predicted. This error is a
		 sign of a corrupted .3D3 file and "should"
		 never occur in real life.
	 */
	E3D_CODE_ODDTOPOPOINTS,			/* WARNING:
																 This error indicates an internal program
																 bug - helpful during development, but, like
																 the error above, "should" never occur.
															 */
	E3D_CODE_BADCOLORMAP,				/* FATAL:
																 Invalid color mapping code (trapped when 
																 calculating area and line ID bins).			  */
	E3D_CODE_BADOBJID,					/* FATAL:
																 Invalid object ID, probably out of range.	*/


	/***************** M A C - S P E C I F I C   E R R O R S ********************/
	E3D_MAC_GDEVICE,						/* WARNING:
																 A Mac-specific error indicating that the 
																 program was unable to obtain graph device
																 information from the O/S. This one's not
																 our fault.						 
															 */

	/************* W I N D O W S - S P E C I F I C   E R R O R S ****************/
	E3D_WIN_COLORTABLE,					/* WARNING:
																 WINDOWS didn't let us allocate a color table
																 probably due to memory shortage.						*/
	E3D_WIN_PIPPIX,  						/* WARNING:
																 WINDOWS didn't let us allocate an offscreen
																 picture handle.														*/

	/************************** M E M O R Y  E R R O R S ************************/
	E3D_MEM_3DSTRINGALLOC,			/* WARNING:
																 Unable to allocate memory for a text string.
																 Usual reason is lack of memory, as is the 
																 case for all the _MEM_ error codes below.
															 */
	/* WARNING:
		 Unable to allocate mem for...							*/
	E3D_MEM_AUTOFIT,						/* ... autofit struct. 											  */
	E3D_MEM_FRAME,							/* ... graph frame info structure.						*/
	E3D_MEM_LABELINFO,					/* ... array of header strings.								*/
	E3D_MEM_LINEALLOC,					/* ... buffer for grid-, topo- etc. lines.		*/
	E3D_MEM_MATRIXSORT,					/* ... struct for sorting cells in matrix-type
																 graphs (ordinal x & z axes) = non-scatters.*/
	E3D_MEM_NEWPOLY,						/* ... copy of polygon for clipping purposes.	*/
	E3D_MEM_OBJECT,							/* ... 3D object definition structure.				*/
	E3D_MEM_PIXELBATCH,					/* ... pixel buffer for 3D raytracing.				*/
	E3D_MEM_RAYSTRUCT,					/* ... info struct for 3D raytracing.					*/
	E3D_MEM_SCATTERCLOUDS,			/* ... list of data point location in scatter
																 graphs (used to improve rotation speed).		*/
	E3D_MEM_SCATTERSORT,				/* ... struct for sorting marker points by
																 distance from viewer in scatter graphs.		*/
	E3D_MEM_SINTABLE,						/* ... table of 90 precalc'ed sine values.		*/
	E3D_MEM_STROKESTRINGALLOC,	/* ... table of strings for ATM type text.		*/
	E3D_MEM_TOPOENDPOINTS,			/* ... list of endpoints of topo lines.				*/
	E3D_MEM_TOPOLINELIST,				/* ... list of topolines themselves.					*/

	/*************************** R A N G E  E R R O R S *************************/
	/* WARNING:
		 OUT-OF-RANGE error for...									*/
	E3D_RANGE_ATTR,							/* ... object attribute.											*/
	E3D_RANGE_GRAPHTYPE_1,			/* ... invalid graph type for matrix graphs.	*/
	E3D_RANGE_GRAPHTYPE_2,			/* ... reserved for invalid scatter type.			*/


	/**************************** D A T A  E R R O R S **************************/
	E3D_DATA_SCATTERPOINT,			/* No or bad data for scatter point.      		*/


	/******************* A L L - P U R P O S E  E R R O R ***********************/
	E3D_GEN_ERROR,							/* FIXME FIXME 07dec90/ksk 
																 YOU fix me, Keith! What does this mean? 
															 */

	E3D_MAX_ERRORS

}	E3D_ErrorCodes;



/************************************************************************
 * from CAC_ERRS.H
 ************************************************************************/

typedef enum _EacErrorCodes {
	EAC_FIRST = EAC_BASE,

	EAC_INVALIDSERIES,	 	/* FATAL: Series address out of range. */
	EAC_INVALIDGROUPS,	 	/* FATAL: Group address out of range. */
	EAC_INVALIDENTRY,		 	/* FATAL: Entry address out of range. */
	EAC_BUFFEROVERFLOW,		/* FATAL: Insuffient space in buffer. */
	EAC_UNKNOWNCHART,			/* FATAL: Chart type not found in data structures. */
	EAC_NULLITEM,					/* STATUS: Attempt was made to query an empty item. */
	EAC_GETSTORAGE,				/* FATAL: Storage device was unable to retrieve data. */
	EAC_PUTSTORAGE,				/* FATAL: Storage device was unable to store data. */
	EAC_EMPTYRANGE,				/* FATAL: Operation failed due to lack of data. */
	EAC_NOTIMPLEMENTED,		/* FATAL: Function not implemented in this version. */
	EAC_INVALIDITEM,			/* FATAL: Item not valid in context used. */
	EAC_EMPTYDATAFORMAT,	/* FATAL: No data items active in node. */
	EAC_NULL_DATARANGE,		/* Data range undefined. */
	EAC_NO_DATA,					/* Insufficient data to form any risers. */
	EAC_TOOFEW_SERIES,		/* At least two series required for this graph. */
	EAC_TOOFEW_GROUPS,		/* At least two groups required for this graph. */
	EAC_NEG_LOG,
	EAC_ZERO_LOG,
	EAC_INVALIDPAGE,	 		/* FATAL: Page address out of range. */

	EAC_MAX_ERRORS

} EacErrorCodes;


/************************************************************************
 * from CAN_ERRS.H
 ************************************************************************/

typedef enum _EAN_ErrorCodes {
	EAN_MEM_NEWANODE = EAN_BASE,	/* ALERT: Error creating anode. Probably Mem. */
	EAN_MEM_GROUP,						/* ALERT: Error grouping anodes. Probably Mem. */
	EAN_MEM_DUPLICATE,				/* ALERT: Error duplicating. Probably Mem. */

	EAN_SETTEXT,						/* ALERT: Invalid text handle encountered
														 or out of mem. while setting text. */
	EAN_BADGRAPH,						/* FATAL: Invalid GraphPtr passed to engine */
	EAN_BADATTR,						/* ALERT: Attribute passed was not valid */
	EAN_BADANODE,						/* ALERT: An Invalid Annotation handle was passed */
	EAN_NOANODES,						/* ALERT: The selection list contained no 
														 annotations to act upon. */
	EAN_NODETNODEFOUND,				/* ALERT: No DetNode was found for given Anode */

	EAN_NOLINKOBJECT,					/* ALERT: Hot-Link found no graph object
															 to link to in the selection list  */
	EAN_DATAINVALID,					/* FATAL: Attr codes are menu info is invalid */
	EAN_MAXERR							/* Last annotation error code... */
} EAN_ErrorCodes;


/************************************************************************
 * from SDR_ERRS.H
 ************************************************************************/

typedef enum _EDR_ERROR_CODES {
	EDR_INVALID_DETNODEREF = EDR_BASE,
	EDR_INVALID_DETNODEPTR,
	EDR_ITEM_NOT_FOUND,
	EDR_MISSING_SFX_FILE,
	EDR_NULL_AREAINST,
	EDR_NULL_FONTINST,
	EDR_NULL_LINEINST,
	EDR_CREATE,
	EDR_COMBINE,
	EDR_SELECT,

	EDR_MAX_ERRORS
} EDR_ERROR_CODES;


/************************************************************************
 * from SFX_ERRS.H
 ************************************************************************/

typedef enum _EFX_ErrorCodes {
	EFX_MEM_NEWSFX = EFX_BASE,		/* WARNING: Error creating SFXHandle. Probably Mem. */

	EFX_BAD_PICT,									/* WARNING: Error opening picture file */
	EFX_BAD_PATH,									/* WARNING: TDGPATH not set to pict's path */
	EFX_UNKNOWN_PICT,							/* WARNING: Unrecognized picture format */
	EFX_BAD_EXTENTS,							/* WARNING: Metafile extents are zero'd */
	EFX_NULL_SFX,									/* WARNING: A NULL SFXHandle was passed */
	EFX_COLOR_RANGE,							/* WARNING: An out-of-range palette index was ref'd */

	EFX_OPEN_PRESETS,							/* WARNING: Error opening preset file */
	EFX_WRITE_PRESETS,						/* WARNING: Error writing preset file */
	EFX_READ_PRESETS,							/* WARNING: Error reading preset file */
	EFX_NO_PRESET,								/* WARNING: No preset by that name exists */

	EFX_NOLAYERS,									/* WARNING: No layers were drawn in SFX */

	EFX_MAXERR										/* Last SFX error code... */
} EFX_ErrorCodes;


/************************************************************************
 * from ANODE.H
 ************************************************************************/
void PUBLIC				FreeAllAnodes(GraphPtr pGraph);

/************************************************************************
 * from STD_SFX.H
 ************************************************************************/
void PUBLIC						MakeVirginSFXFile			(	void );

/************************************************************************
 * from COM_RAMF.H
 ************************************************************************/

typedef struct _RamFile {
	MEMSIZE					bufSize;
	MEMSIZE					bufWritten;
	MEMPTR					hBuffer;
	UCHAR  *		pBuffer;
	UCHAR  *		pCurrent;
} RamFileRec, * RamFilePtr;


RamFilePtr PUBLIC	AllocRamFile		(MEMSIZE initialSize);
void PUBLIC			FreeRamFile			(RamFilePtr pRamFile);

LPTSTR PUBLIC  		GetRamFileName		(RamFilePtr pRamFile);
RamFilePtr PUBLIC	XlateRamFileName	(LPTSTR pName);

LPTSTR PUBLIC  		GetRamFileBuffer	(RamFilePtr pRamFile);
void PUBLIC			SetRamFileSize		(RamFilePtr pRamFile, UINT32 lBytes);

INT16 PUBLIC		RFgetc				(RamFilePtr pRamFile);
INT16 PUBLIC		RFwrite				(RamFilePtr pRamFile, LPTSTR buf, MEMSIZE buflen);
void PUBLIC			RFrewind			(RamFilePtr pRamFile);
INT32 PUBLIC		RFseek				(RamFilePtr pRamFile, INT32 lBytes, INT16 nOrigin);


/************************************************************************
 * from INITTDG.H
 ************************************************************************/
INT16 PUBLIC			InitTDGLIB		(void);
INT16 PUBLIC			FiniTDGLIB		(void);

/************************************************************************
 * from COM_UNDO.H
 ************************************************************************/
#ifndef _MAPINFO	/*  Already defined in MapInfo headers, removed to avoid conflicts 1-29-99/FL */
void PUBLIC				ClearUndo(GraphPtr pGraph);
#endif /*  _MAPINFO */

BOOLEAN16 PUBLIC		PerformUndo(DrawEnvPtr pDE, GraphPtr pGraph);

#define CANNOT_UNDO_OR_REDO				0
#define CAN_UNDO_CANNOT_REDO			1
#define CAN_REDO_CANNOT_UNDO			2
#define CAN_UNDO_AND_CAN_REDO			3

INT16 PUBLIC			GetUndoStatus(GraphPtr pGraph);
void PUBLIC				SetUndoStatus(GraphPtr pGraph, INT16 newValue);

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

BOOLEAN16 PUBLIC IsUndoListEmpty(GraphPtr pGraph);

/*  END Added by FL 4-30-98 from pgedit.h */

/*  aug00DW: Anchor mode of Undo for COREL. */
BOOLEAN16 PUBLIC
AnchorUndo(
		DrawEnvPtr		pDE,
		GraphPtr		pGraph
);

BOOLEAN16 PUBLIC
AnchorRedo(
		DrawEnvPtr		pDE,
		GraphPtr		pGraph
);

INT16 PUBLIC
AnchorDrop(
		GraphPtr	pGraph,
		INT16		nID,
		LPTSTR		pszHint
);

void PUBLIC
AnchorClear(
		GraphPtr	pGraph
);

/* 28aug00DW: For COREL, a function to query info about the top anchor on the undo stack. */
/* Returns TRUE if an anchor was found, otherwise FALSE */
BOOLEAN16 PUBLIC
AnchorGetInfo(
		GraphPtr	pGraph,
		PINT16		pnID,			/* return User-specified ID value */
		PBOOLEAN16	pbAnchorOnTop,	/* return TRUE if anchor was topmost item on undo stack */
		LPTSTR		pszHint		/* copies user-specified "hint" string into this buffer */
);

/* 28aug00DW: For COREL, a function to set the # of anchor-items to store on the undo-stack */
/* Any items over this limit are automatically deleted from the bottom of the stack. */
/* System default is 16 */
INT16 PUBLIC
AnchorSetMaximumLevel(
		GraphPtr		pGraph,
		INT16			nLevels
);

/************************************************************************
 * from C3D_WIRE.H
 ************************************************************************/

void 	PUBLIC	DrawWireframe	(DrawEnvPtr pDE, GraphPtr pGraph, INT16 nPixels);
BOOLEAN16 PUBLIC	CheckWireframe	(GraphPtr pGraph);
void PUBLIC		ClearWireframe	(GraphPtr pGraph);


/************************************************************************
 * from COM_UPDT.H
 ************************************************************************/

typedef struct UpdateItemRec {
	struct UpdateItemRec *	pNext;

	INT16 					nObjectID;						/* object ID */
	INT16 					nSeriesID;						/* series ID or -1 */
	INT16 					nGroupID; 						/* group	ID or -1 */

	Rect32						rcBounds;

} UpdateItemRec, * UpdateItemPtr;


UpdateItemPtr PUBLIC
Update_GetFirstItem(GraphPtr pGraph);

UpdateItemPtr PUBLIC
Update_GetNextItem(UpdateItemPtr pItem);

void PUBLIC
Update_GetInvalRect(	UpdateItemPtr 	pItem,
		Rect32				*invalRect);

INT16 PUBLIC
Update_ClearList(GraphPtr pGraph);


/************************************************************************
 * from CSL_TOOL.H
 ************************************************************************/

void PUBLIC
MoveSelectedItem(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelItemPtr			pSelItem,
		Rect32				*newBounds
);

INT16 PUBLIC
AlignSelList_Top(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16					alignToFirst
);

INT16 PUBLIC
AlignSelList_Bottom(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16					alignToFirst
);

INT16 PUBLIC
AlignSelList_Left(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16					alignToFirst
);

INT16 PUBLIC
AlignSelList_Right(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16					alignToFirst
);

INT16 PUBLIC
JoinToFirst(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList
);


INT16 PUBLIC
JoinHoriz(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16				stretchVert
);

INT16 PUBLIC
JoinVert(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16				stretchHoriz
); 

INT16 PUBLIC
MakeSameSize(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16				horiz,
		INT16				vert
);

INT16 PUBLIC
SizeToPicture(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList
);

INT16 PUBLIC
AlignInsideEdges(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16						alignH,
		INT16						alignV
);

INT16 PUBLIC
CenterOnPage(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16						centerH,
		INT16						centerV
);

INT16 PUBLIC
AlignSelList_Center(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16				horiz,
		INT16				vert
);

void PUBLIC
UndoSelListMove(
		DrawEnvPtr			pDE,
		GraphPtr				pGraph,
		SelListPtr			pSelList
);

INT16 PUBLIC
EncompassLineThickness(
		SelItemPtr			pItem,
		Rect32						*bounds
);


/************************************************************************
 * from CAN_TOOL.H
 ************************************************************************/

/***************************************************************************
 *
 * Placement codes for UnpackAndSelectAnodes
 *
 ***************************************************************************/

typedef enum _UnpackPlacementCodes {
	UPC_SAME_PLACE,			 				/* Unpacks into same virtual location */
	UPC_OFFSET_XY,							/* Unpacks offset by 10 DEST pixels		*/
	UPC_CENTER,									/* Unpacks into center of current DEST*/

	MAX_PLACEMENT_CODE
} UnpackPlacementCodes;

/***************************************************************************
 *
 * Annotation Functions for Selection Lists
 *
 ***************************************************************************/

int 		PUBLIC BringForward	(GraphPtr pGraph, SelListPtr pSelList);

int 		PUBLIC Bring2Front	(GraphPtr pGraph, SelListPtr pSelList);

int 		PUBLIC SendBackward	(GraphPtr pGraph, SelListPtr pSelList);

int 		PUBLIC Send2Back		(GraphPtr pGraph, SelListPtr pSelList);

int 		PUBLIC Remove			(		GraphPtr pGraph,
		SelListPtr pSelList, 
		BOOLEAN16 all					);

int			PUBLIC RemoveItem(GraphPtr pGraph, SelItemPtr pSelList);

/***************************************************************************
 *
 * Annotation Object Creation - Return much more useful AnodeID (02/95DW)
 *
 ***************************************************************************/

INT16			 PUBLIC CreateAnode(
		GraphPtr			pGraph,
		INT16				aType,
		Rect32 * 		bounds,
		MEMPTR			hText,
		PolyHandle		hPoly,
		WedgeInfoPtr 	pWedge,
		Point32				startPt,
		Point32				endPt);


/***************************************************************************
 *
 * Annotation Object and List Drawing
 *
 ***************************************************************************/

INT16 	PUBLIC DrawAnAnode(
		DrawEnvPtr		pDE,
		GraphPtr			pGraph,
		INT16				anodeID);

INT16 	PUBLIC DrawAllAnodes(
		DrawEnvPtr		pDE,
		GraphPtr			pGraph);


/***************************************************************************
 *
 * ID Number, Z-Layer, and AnodeInfoHdl Conversion Tools
 *
 ***************************************************************************/

INT16 			PUBLIC AnodeID2ZLayer	(GraphPtr pGraph, INT16 anodeID);

INT16 			PUBLIC ZLayer2AnodeID	(GraphPtr pGraph, INT16 zLayer);

INT16 			PUBLIC DetRef2AnodeID	(DetNodeRefPtr pDetRef);

void 				PUBLIC AnodeID2DetRef	(GraphPtr pGraph, INT16 anodeID, DetNodeRefPtr pDetRef);

INT16				PUBLIC AnodeHdl2ID		(AnodeInfoHdl hNode);

AnodeInfoHdl 	PUBLIC ID2AnodeHdl		(GraphPtr gPtr, INT16 anodeID);


/***************************************************************************
 *
 * General Annotation Tools
 *
 ***************************************************************************/

INT16 	PUBLIC GetRectAnodeInfo(
		GraphPtr			pGraph,
		INT16				anodeID,
		Rect32				*bounds);

void 		PUBLIC SetRectAnodeInfo(
		GraphPtr			pGraph,
		INT16				anodeID,
		Rect32				*bounds);

BOOLEAN16 	PUBLIC IsAnode(DetNodePtr pDetNode);

INT16 		PUBLIC Duplicate(DrawEnvPtr pDE, GraphPtr pGraph, SelListPtr pSelList);

INT16 PUBLIC
SelectAnode_All(
		GraphPtr				pGraph,
		SelListPtr			pSelList
);

SelItemPtr PUBLIC
SelectAnode_AddItem(
		GraphPtr				pGraph,
		SelListPtr			pSelList,
		INT16						anodeID
);

INT16 PUBLIC
ChangeXanTextboxID(	
		GraphPtr	pGraph,
		INT16 		oldID, 
		INT16 		newID
);

INT16 PUBLIC
SetLineItemPoints(
		SelItemPtr			pItem,
		Point32						p0,
		Point32						p1,
		Rect32						*bounds
);

INT16 PUBLIC
CreatePictureAnode(	GraphPtr pGraph, 
		DrawEnvPtr pDE, 
		INT16 x, 
		INT16 y, 
		LPTSTR szPictName, 
		INT32 hdl,
		INT16 xExt,
		INT16 yExt,
		Rect32 *bounds,
		INT16	*pResized);


AnodeInfoHdl PUBLIC
GetAnodeFromTextboxID(GraphPtr pGraph, INT16 nBox);


/************************************************************************
 * from STD_ACSS.H
 ************************************************************************/

INT16	PUBLIC
GetRawLimits(
		GraphPtr		 	G,				 			/* Full graph pointer. */
		INT16					nAxisReq,				/* Axis requested (see DataAxisRef enum). */
		REAL64 *	pfRawMin,				/* Raw axis minimum. */
		REAL64 *	pfRawMax				/* Raw axis maximum. */
);

INT16 PUBLIC
GetSGCount(
		GraphPtr		 	G,				 			/* Environment pointer. */
		BOOLEAN16			 	bAbsolute,			/* Absolute number of series? */
		PINT16		 		pnSeries,				/* Set to number of series. */
		PINT16		 		pnGroups				/* Set to number of groups. */
);

INT16 PUBLIC
GetSeriesHeaderPos(
		GraphPtr 				G,								/* Environment pointer. */
		INT16	 					nSeriesAddress,		/* Series address. */
		PINT16					pnRow,						/* Set to SSI row address. */
		PINT16					pnCol							/* Set to SSI columnn address. */
);

INT16	PUBLIC
GetSeriesHeaderString(
		GraphPtr	G,				/* Full Graph pointer. */
		INT16		s,				/* Series header requested. */
		LPTSTR		szBuffer	/* Must be at least MAXSTRLEN long. */
);

INT16	PUBLIC
GetGroupHeaderString(
		GraphPtr	G,				/* Full Graph pointer. */
		INT16			s,				/* Group header requested. */
		LPTSTR			szBuffer	/* Must be at least MAXSTRLEN long. */
);

INT16 PUBLIC
GetGroupHeaderPos(
		GraphPtr 				G,								/* Environment pointer. */
		INT16		 				nGroupsAddress,		/* Group address. */
		PINT16					pnRow,						/* Set to SSI row address. */
		PINT16					pnCol							/* Set to SSI columnn address. */
);


INT16	PUBLIC
AccGraphDataStatus(
		GraphPtr	G						/* Graph whose data status is to be returned. */
);

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

INT16 PUBLIC
GetSGCount(
		GraphPtr		 	G,				 			/* Environment pointer. */
		BOOLEAN16			bAbsolute,			/* Absolute number of series? */
		PINT16		 		pnSeries,				/* Set to number of series. */
		PINT16		 		pnGroups				/* Set to number of groups. */
);

INT16 PUBLIC
BindColors(
		GraphPtr			G,							/* Colors allocated to G. */
		INT16				 	nColorsNeeded		/* Number of colors needed for this graph. */
);

/*  END Added by FL 4-30-98 from pgedit.h */


/************************************************************************
 * from C3D_ATTR.H
 ************************************************************************/

#define kSpecifiedRiser 0
#define kSpecifiedSeries 1
#define kSpecifiedGroup 2
#define kSpecifiedAll 3

/************************************************************************
 * TDG_GridLines3D - Common Attribute Gridline Values Structure
 ************************************************************************/
typedef struct {
	INT16				Axis;									/* x,y or z (0,1,2)	*/
	INT16				HashStyle;						/* <0:solid, 0:none, >0:ticks */
} TDG_GridLines3D, * TDG_GridLines3DPtr;


/************************************************************************
 * TDG_CustomView - Common Attribute Custom View Values Structure
 ************************************************************************/
typedef struct {
	INT16				FunctionCode;					/* 0..35 */
	INT16				StepSize;							/* 0..MAX_INT */
} TDG_CustomView, * TDG_CustomViewPtr;


/************************************************************************
 * TDG_AxisDivs- Common Attribute Axis Divisions Structure
 ************************************************************************/
typedef struct {
	BOOLEAN16			bManualMajorHash;			/* 0: FALSE */
	INT16				nMajorHash;						/* 1.. */
} TDG_AxisDivs, * TDG_AxisDivsPtr;


/************************************************************************
 * TDG_ViewStruct - Common Attribute Viewing Scene Values Structure
 ************************************************************************/
typedef struct {
	fMatrixType	ViewMatrix;						/* Transformation matrix.*/
	dPoint3D		ViewerPosition;				/* Viewer position in 3D.*/
	dPoint3D		GraphSize;						/* Graph cube size.*/
	dPoint3D		WallThickness;				/* Graph cube wall thicknesses.*/
	INT16				xPan;									/* Horizontal displacement.*/
	INT16				yPan;									/* Vertical displacement.*/
} TDG_ViewStruct, * TDG_ViewStructPtr;


/************************************************************************
 * TDG_RiserThick - Common Attribute Riser Thickness Values Structure
 ************************************************************************/
typedef struct {
	INT16				xThick;								/* 0..200 */
	INT16				yThick;								/* 0..200 */
	INT16				zThick;								/* 0..200 */
} TDG_RiserThick, * TDG_RiserThickPtr;


/************************************************************************
 * TDG_SeriesType - Common Attribute to change graph type for one series.
 ************************************************************************/
typedef struct {
	INT16				Series;								/* Actually "Series" or "Group" */
	INT16				GraphType;						/* Really "Graph" or "Marker" type */
} TDG_SeriesType, * TDG_SeriesTypePtr;

/************************************************************************
 * TDG_VisualizeType - 3D Attribute parameter block for Transparency, Supression and Glow.
 ************************************************************************/
typedef struct {
	INT16						nVisualizeType;				/* TRANSPARENCY = 1, SUPPRESSION = 2, GLOW = 4 (binary flags) */
	INT16						nSeries;
	INT16						nGroup;
	INT16						nPercent;							/* % shown (0 = fully suppressed) */
	BOOLEAN16					bPerformUndo;					/* Are we removing an item? */
	INT16						nOperation;						/* How to interpret series and group and/or action to perform */
	RGB16						glowColor;						/* Color of glowing outline */
} TDG_VisualizeType, * TDG_VisualizePtr;

/************************************************************************
 * from CB2VDM.H
 ************************************************************************/

/*  BEGIN Added by FL 4-30-98 from pgedit.h */

typedef enum _CellType { CELL_REAL, CELL_STRING, CELL_EMPTY } CellType;

CellType PUBLIC GetCellType(GraphPtr pGraph, INT16 nRow, INT16 nCol);

/*  END Added by FL 4-30-98 from pgedit.h */

INT16 PUBLIC InitCB2VDM(GraphPtr pGraph);	/* translate a CALLBACK chart into a SEND_DATA chart. */

BOOLEAN16 PUBLIC SetGraphStrings(void *pGraph);
BOOLEAN16 PUBLIC SetGraphLabels(void *pGraph);
BOOLEAN16 PUBLIC SetGraphData(void *pGraph);

BOOLEAN16 PUBLIC SetGraphTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphSubTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphFootNote(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphSeriesTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphGroupsTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphY1AxisTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphY2AxisTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphSeriesLabel(void *pGraph, INT16 nRow, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphGroupsLabel(void *pGraph, INT16 nCol, LPTSTR szValue);
/* To indicate NULL data, pass a NULL ptr as the parameter prValue; */
BOOLEAN16 PUBLIC _SetGraphRowColData(void *pGraph, INT16 nRow, INT16 nCol, REAL64 *prValue);
BOOLEAN16 PUBLIC SetGraphRowColData(void *pGraph, INT16 nRow, INT16 nCol, REAL64 rValue);
BOOLEAN16 PUBLIC _SetGraphRowColPageData(GraphPtr pGraph, INT16 nRow, INT16 nCol, INT16 nPage, REAL64 *prValue);
BOOLEAN16 PUBLIC SetGraphY3AxisTitle(void *pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC SetGraphY4AxisTitle(void *pGraph, LPTSTR szValue);

BOOLEAN16 PUBLIC GetGraphTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphSubTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphFootNote(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphSeriesTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphGroupsTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphY1AxisTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphY2AxisTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphSeriesLabel(GraphPtr pGraph, INT16 nRow, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphGroupsLabel(GraphPtr pGraph, INT16 nCol, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphRowColData(GraphPtr pGraph, INT16 nRow, INT16 nCol, REAL64 *prValue);
BOOLEAN16 PUBLIC GetGraphRowColPageData(GraphPtr pGraph, INT16 nRow, INT16 nCol, INT16 nPage, REAL64 *prValue);
BOOLEAN16 PUBLIC GetGraphY3AxisTitle(GraphPtr pGraph, LPTSTR szValue);
BOOLEAN16 PUBLIC GetGraphY4AxisTitle(GraphPtr pGraph, LPTSTR szValue);

#if 1
/* 8jan96DW: Routines to process Multi-dimensional Labels in VDM mode */

BOOLEAN16 PUBLIC SetGraphMDGroupsLabel(GraphPtr pGraph, 
		INT16 nCurDimension, 	/* Current Dimension being processed */
		DimArrayPtr	pDimArray,	/* Ptr to a DimensionArray */
		LPTSTR lpValue);		/*  string pointer */

BOOLEAN16 PUBLIC GetGraphMDGroupsLabel(GraphPtr pGraph, 
		INT16 nCurDimension, 	/* Current Dimension being processed */
		DimArrayPtr	pDimArray,	/* Ptr to a DimensionArray */
		LPTSTR	lpValue);		/*  string pointer */
#endif


void PUBLIC GenerateDXF(DrawEnvPtr pDE, GraphPtr pGraph, LPTSTR dxf_name);

/*******************************************************************************
 * SetUndoMode
 *
 *	DESCRIPTION:	Sets the Undo Mode for this instance of the library
 *					0 = No Undo information saved
 *					1 = Last attribute change only saved (default)
 *					2 = All attribute changes saved (create Undo Stack) 
 *
 *	RETURNS:		None
 *
 *******************************************************************************/

void PUBLIC SetUndoMode(INT16 nUndoMode);                                                 

/* FUNCTION TO FORCE CREATION OF INTERNAL RISER INFO MEMORY: For
	 cases where you wish to modify a riser attribute BEFORE it is ever
	 drawn on-screen */
INT16 PUBLIC
CreateRisers(
		GraphPtr    pGraph,
		INT16       nCount);

/* For SEND_DATA or DM method of data storage.  Get the current internal
	 spreadsheet's range of cells to graph */

/* 15dec10rg: data range starting location - necessary to correctly use Get|SetDataRange */
#define DATARANGEROW			5
#define DATARANGECOL			3

INT16 PUBLIC GetDataRange(
		GraphPtr pGraph,			/* graph pointer. */
		INT16    *prULC,        /* Start Row */  
		INT16    *pcULC,        /* Start Col */
		INT16    *prLRC,        /* Stop Row */
		INT16    *pcLRC         /* Stop Col */
);

/* For SEND_DATA or DM method of data storage.  Set the current internal
	 spreadsheet's range of cells to graph.  Not needed in regular
	 usage, but occassionally useful for viewing subsets of data stored
	 inside the graph object. NOTE: SetGraphData() will reset these
	 parameters */

INT16 PUBLIC
SetDataRange(
		GraphPtr pGraph,			/* graph pointer. */
		INT16    *prULC,        /* Start Row */  
		INT16    *pcULC,        /* Start Col */
		INT16    *prLRC,        /* Stop Row */
		INT16    *pcLRC         /* Stop Col */
);

/*  03-21-00/FL Added data range for time series charts */
INT16 PUBLIC GetTimeSeriesDataRange(
		GraphPtr pGraph,			/* graph pointer. */
		INT16    *pnHistoryStart,    /* History start */  
		INT16    *pnHistoryEnd,      /* History end */
		INT16    *pnForecastStart,   /* Forecast Start */
		INT16    *pnForecastEnd      /* Forecast End */
);

INT16 PUBLIC
SetTimeSeriesDataRange(
		GraphPtr pGraph,			/* graph pointer. */
		INT16    nHistoryStart,    /* History start */  
		INT16    nHistoryEnd,      /* History end */
		INT16    nForecastStart,   /* Forecast Start */
		INT16    nForecastEnd      /* Forecast End */
);

/* For TABLECHARTS in SEND_DATA method, here's a way to set string values
	 directly into the internal data matrix */

BOOLEAN16 PUBLIC
SetGraphRowColString(
		GraphPtr 	pGraph,			/* graph pointer */
		INT16		nRow,			/* series number for the data */
		INT16   	nCol,			/* groups number for the data */
		LPTSTR		lpValue			/* string pointer */
);

/* For TABLECHARTS in SEND_DATA method, here's a way to get string values
	 directly from the internal data matrix */

BOOLEAN16 PUBLIC
GetGraphRowColString(
		GraphPtr 	pGraph,			/* graph pointer */
		INT16		nRow,			/* series number for the data */
		INT16   	nCol,			/* groups number for the data */
		LPTSTR		lpValue			/* string pointer */
);

/*
	 Function to Set the Ascii-Text String of an existing Text-Annotation  
RETURNS:	0 = SUCCESS, NON-ZERO = FAILURE
 */
INT16 PUBLIC
SetAnnotationText( 
		GraphPtr	pGraph,
		DrawEnvPtr	pDE,		/* current draw env. */
		INT16		anodeID,	/* Which Annotation */
		LPTSTR 		psNewText   /* Ptr to New Text String */
);

/*
	 Function to Get the Ascii-Text String of an existing Text-Annotation. Copies
	 the Current Annotation Text into string pointed to by pText.  Will copy up to
	 nLength bytes.  If pText = NULL, function will return length of Current Annotation
	 Text (including NULL byte at end of string) into *pnLength. 
RETURNS:	0 = SUCCESS, NON-ZERO = FAILURE
 */
INT16 PUBLIC
GetAnnotationText(
		GraphPtr			pGraph,
		INT16				anodeID,     /* Which Annotation */
		LPTSTR 				pText,       /* Result COPIED into here */
		INT16				*pnLength    /* Max. # of bytes to copy */
);

/*12jun95DW:  Routine to set user-defined widths & heights for cells in our TABLE CHARTS
	Assumes that ATC_UNIFORMROWS and/or ATC_UNIFORMCOLS have been set to 2 (User-defined mode) */
INT16 PUBLIC
SetTableChartCellDimensions(
		GraphPtr 				pGraph,				/* Which Graph */
		INT16					nRow,				/* If Row >= 0, Set HEIGHT */
		INT16					nCol,				/* If Col >= 0, Set WIDTH */
		PINT16					pnValue			 	/* Value */
);

/*12jun95DW:  Routine to get user-defined widths & heights for cells in our TABLE CHARTS */
INT16 PUBLIC
GetTableChartCellDimensions(
		GraphPtr 				pGraph,				/* Which Graph */
		INT16					nRow,				/* If Row >= 0, Get HEIGHT */
		INT16					nCol,				/* IfCol >= 0, Get WIDTH */
		PINT16					pnValue			 	/* Value */
);

/*23apr96DW:  Routine to force Detnode generation in any portmode*/
INT16 PUBLIC
DEForceDetNodes(
		DrawEnvPtr			pDE,
		BOOLEAN16				bMode	/* TRUE = Force Detnode Generation */
);

/*
	 this callback converts utf8 string into set of glyph indexes
	 and sets the flags for each char
	 - normal character will be CHAR_NORMAL type
	 - spaces has (CHAR_SPACE | CHAR_NL_POSSIBLE) flags
	 - if wrapping is possible AFTRER char, set CHAR_NL_POSSIBLE flag
   - to force new line, last preceding non-space character 
	   must have CHAR_NL_FORCE flag
	 - some characters may have CHAR_NL_PREFERED flag set. This selects
	   where engine should wrap. But if, because of maxWidth limit, 
		 string has to be wrapped before CHAR_NL_PREFERED, all following
		 CHAR_NL_PREFERED flags are ignored (used as CHAR_NL_POSSIBLE)
	 - table may contain characters that are not printed (as '\n' for example) 
	   with CHAR_IGNORE flag
*/


typedef
INT16
(CALLBACK *CB_GetGlyphsAndFlags) (
		void *     poChildData,
		char *     acFontName,       // IN: font face name
		double     fontsize,         // IN: font size
		INT16      nFlags,           // IN: font flags
		char *     string,           // IN: utf8 string
		INT32 *    noglyphs,         // IN/OUT: size of glyphs arrays/no of glyphs returned
		double     maxWidth,         // IN: max length of line in points
		INT32 *    glyphs,           //OUT: table of glyphs
		INT32 *    charFlags         //OUT: table of flags for every glyph above
);


INT16 PUBLIC
DevBmp_InitFTCallbacks(
		DrawEnvPtr              io_oDevice,
		CB_GetGlyphsAndFlags    GetGlyphsAndFlagsCB,
		void *                  poChildData
);


/* label character flags */

//this is regular character
#define CHAR_NORMAL         0x00        // ------00
//this is space character (will be skipped if and beginning or end of line)
#define CHAR_SPACE          0x01        // ------01
//this character is ignored
#define CHAR_IGNORE         0x02        // ------10

//is wrapping AFTER current character:
#define CHAR_NL_POSSIBLE    0x04        // ---001--
#define CHAR_NL_PREFERED    0x0c        // ---011--
#define CHAR_NL_FORCE       0x1c        // ---111--
#define CHAR_NL             0x1c        // ---111--

//define direction (for BiDi strings)
#define CHAR_LEFT_TO_RIGHT  0x00        // -01-----
#define CHAR_NEUTRAL        0x20        // -00-----
#define CHAR_RIGHT_TO_LEFT  0x40        // -10-----
#define CHAR_DIRECTION      0x60        // -11-----


/* Export function for saving charts as GIF files */
/* 10may96DW */

#define MAXPAL 256
typedef short int					 TDG_WORD;					/* Name conflict under WINDOWS. */


typedef struct GDPALETTE {
	TDG_WORD		nEntries;				
	RGB16		pal[MAXPAL];	
} GDPALETTE, *LPGDPALETTE;

/* bitmap device prototypes */
DrawEnvPtr PUBLIC					DevBmp_AllocDevice		();
INT16 PUBLIC						DevBmp_FreeDevice		(DrawEnvPtr poDE);
INT16 PUBLIC						DevBmp_SetSize			(DrawEnvPtr io_oDevice, int i_nWidth, int i_nHeight);
INT16 PUBLIC						DevBmp_SetPalette		(DrawEnvPtr io_oDevice, LPGDPALETTE palettePtr);
INT16 PUBLIC            DevBmp_SetUseRGBpalette(DrawEnvPtr io_oDevice, BOOLEAN16 i_bUseRGBpalette); /*  i_bUseRGBpalette: TRUE = 24Bit. FALSE = 8Bit */
INT16 PUBLIC						DevBmp_SetBackgroundTransparency(DrawEnvPtr io_oDevice, INT16 i_nTransparentBackground);
INT16 PUBLIC						DevBmp_SetOutputFormat		(DrawEnvPtr io_oDevice, enum DevGen_ImageFormat i_eFormat);
INT16 PUBLIC						DevBmp_SetProgressiveFormat	(DrawEnvPtr io_oDevice, BOOLEAN16 i_bProgressiveFormat);
INT16 PUBLIC						DevBmp_SetQuality		(DrawEnvPtr io_oDevice, INT16 i_nQuality);
INT16 PUBLIC						DevBmp_ExportToFile		(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, char * i_szFile);
INT16 PUBLIC						DevBmp_ExportToMemory		(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, void ** io_ppmOutputPointer, UINT32 * o_pnOutputSize);
INT16 PUBLIC						DevBmp_FreeMemory		(void * io_pmBuffer);

DrawEnvPtr PUBLIC DevWin_AllocDevice(void);
INT16 PUBLIC DevWin_FreeDevice(DrawEnvPtr io_oDevice);
INT16 PUBLIC DevWin_SetSize(DrawEnvPtr io_oDevice, int i_nWidth, int i_nHeight); 
INT16 PUBLIC DevWin_SetBackgroundTransparency(DrawEnvPtr io_oDevice, INT16 i_nTransparentBackground);
INT16 PUBLIC DevWin_SetProgressiveFormat(DrawEnvPtr io_oDevice, BOOLEAN16 i_bProgressiveFormat); 
INT16 PUBLIC DevWin_SetQuality(DrawEnvPtr io_oDevice, INT16 i_nQuality);
INT16 PUBLIC DevWin_SetPalette(DrawEnvPtr io_oDevice, LPGDPALETTE palettePtr);
INT16 PUBLIC DevWin_SetUseRGBpalette(DrawEnvPtr io_oDevice, BOOLEAN16 i_bUseRGBpalette);
INT16 PUBLIC DevWin_SetOutputFormat(DrawEnvPtr io_oDevice, enum DevGen_ImageFormat i_eFormat);
INT16 PUBLIC DevWin_ExportToFile(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, char * i_szFile);
INT16 PUBLIC DevWin_ExportToMemory(GraphPtr i_graphPtr, DrawEnvPtr io_oDevice, MEMPTR * o_ppmOutput, UINT32 * o_pnSize);
INT16 PUBLIC DevWin_FreeMemory (void * io_pmBuffer);

#if PGSDK_COMPAT_WANT_IMPLICIT_DEVICE

INT16 PUBLIC
ExportGIF(
		GraphPtr				pGraph,							/* foreground graph */
		LPTSTR					szOutFile, 						/* Name of GIF file to be written */
		UINT16					cxBounds,					 	/* in pixels */
		UINT16					cyBounds,					 	/* in pixels */
		INT16    				nTransparent,					/* Unpainted pixels transparent? */
		BOOLEAN16				bInterlaced,					/* GIF image interlaced? */
		LPGDPALETTE				pPalette,						/* External Palette */
		BOOLEAN16				bUseLZWCompression				/* TRUE = Uses LZW compression. FALSE = Run-Length encoded */
);

/*  This function saves the graph as a .GIF file and stores the .GIF file */
/*  in memory.  The address of the .GIF file in memory is returned in ppMemAd. */
/*  The size of the .GIF file (in bytes) is returned in *pnSize.   */
/*  WARNING!!! - This function ALLOCATES MEMORY!  When you are through with */
/*  the memory image of the GIF file, you should free it with a  */
/*  FreePtr() call. Good for CGI applets that need to pipe things to StdOut. */

INT16 PUBLIC
ExportGIFtoMemory(
		GraphPtr				pGraph,							/* foreground graph */
		MEMPTR					*ppMemAd, 						/* Returns address of File in Memory */
		UINT32					*pnSize,						/* Returns size of File in Memory */
		UINT16					cxBounds,					 	/* in pixels */
		UINT16					cyBounds,					 	/* in pixels */
		INT16    				nTransparent,					/* Unpainted pixels transparent? */
		BOOLEAN16				bInterlaced,					/* GIF image interlaced? */
		LPGDPALETTE				pPalette,						/* External Palette */
		BOOLEAN16				bUseLZWCompression				/* TRUE = Uses LZW compression. FALSE = Run-Length encoded */
);


/*  two functions for saving charts as JPEG files */
INT16 PUBLIC
ExportJPEG(GraphPtr               graphPtr,
		LPTSTR                 file,
		UINT16                 imgWidth,
		UINT16                 imgHeight,
		BOOLEAN16      transBackground,
		LPGDPALETTE    palettePtr);
/* 3dec01DW: Export a JPEG file to Ramfile memory */
INT16 PUBLIC
ExportJPEGToMemory(GraphPtr		  graphPtr,			
		MEMPTR*		  memAddress,		
		UINT32*		  fileSize,		
		UINT16		  imgWidth,		
		UINT16		  imgHeight,		
		BOOLEAN16	  transBackground,  
		LPGDPALETTE	  palettePtr);


/*  two functions for saving charts as PNG files */
/*  Stuart Easterling   Feb 23, 2000 */

/*  This function saves the graph as a .PNG file and stores the .PNG file */
/*  in memory.  The address of the .PNG file in memory is returned in ppMemAd. */
/*  The size of the .PNG file (in bytes) is returned in *pnSize.   */
/*  WARNING!!! - This function ALLOCATES MEMORY!  When you are through with */
/*  the memory image of the PNG file, you should free it with a  */
/*  FreePtr() call. Good for CGI applets that need to pipe things to StdOut. */

INT16 PUBLIC 
ExportPNGToMemory (GraphPtr		  graphPtr,			/* foreground graph */
		MEMPTR*		  memAddress,		/* returns address of PNG file in memory */
		UINT32*		  fileSize,			/* size of PNG file in memory */
		UINT16		  imgWidth,			/* image width in pixels */
		UINT16		  imgHeight,		/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr);		/* external palette */

INT16 PUBLIC 
ExportPNG (GraphPtr		  graphPtr,				/* foreground graph */
		LPTSTR		  file,					/* file to write PNG to */
		UINT16		  imgWidth,				/* image width in pixels */
		UINT16		  imgHeight,			/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr);			/* external palette */

/* this is version of ExportPNGToMemory that uses RGB color model
 * instead of palette mode. Transparency is added in additional
 * alpha channel. Better look, but more memory & larger image file. 
 * All parameters exactly the same as in ExportPNGToMemory, palettePtr ignored */
INT16 PUBLIC 
ExportPNGToMemory_24 (GraphPtr		  graphPtr,			/* foreground graph */
		MEMPTR*		  memAddress,		/* returns address of PNG file in memory */
		UINT32*		  fileSize,			/* size of PNG file in memory */
		UINT16		  imgWidth,			/* image width in pixels */
		UINT16		  imgHeight,		/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr);		/* external palette */

/* this is version of 24-bit PNG Export that also supports antialiased primitives */
INT16 PUBLIC 
ExportPNGToMemory_24_AA (GraphPtr		  graphPtr,			/* foreground graph */
		MEMPTR*		  memAddress,		/* returns address of PNG file in memory */
		UINT32*		  fileSize,			/* size of PNG file in memory */
		UINT16		  imgWidth,			/* image width in pixels */
		UINT16		  imgHeight,		/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr,			/* external palette */
		BOOLEAN16		bAntiAlias);		/* AntiAlias control flag */	

/* this is version of ExportPNG that uses RGB color model
 * instead of palette mode. Transparency is added in additional
 * alpha channel. Better look, but more memory & larger image file. 
 * All parameters exactly the same as in ExportPNGToMemory, palettePtr ignored */
INT16 PUBLIC 
ExportPNG_24 (GraphPtr		  graphPtr,				/* foreground graph */
		LPTSTR		  file,					/* file to write PNG to */
		UINT16		  imgWidth,				/* image width in pixels */
		UINT16		  imgHeight,			/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr);			/* external palette */


/* this is version of 24-bit PNG Export that also supports antialiased primitives */
INT16 PUBLIC 
ExportPNG_24_AA (GraphPtr		  graphPtr,	/* foreground graph */
		LPTSTR		  file,					/* file to write PNG to */
		UINT16		  imgWidth,				/* image width in pixels */
		UINT16		  imgHeight,			/* image height in pixels */
		INT16			  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr,			/* external palette */
		BOOLEAN16		bAntiAlias);		/* AntiAlias control flag */

/*  two functions for saving charts as JPEG files */
INT16 PUBLIC
ExportJPEG(GraphPtr graphPtr,
		LPTSTR      file,
		UINT16      imgWidth,
		UINT16      imgHeight,
		BOOLEAN16   transBackground,
		LPGDPALETTE palettePtr);

/* 3dec01DW: Export a JPEG file to Ramfile memory */
INT16 PUBLIC
ExportJPEGToMemory(GraphPtr graphPtr,			
		MEMPTR*     memAddress,    
		UINT32*     fileSize,    
		UINT16      imgWidth,    
		UINT16      imgHeight,    
		BOOLEAN16   transBackground,  
		LPGDPALETTE palettePtr);

INT16 PUBLIC
ExportSVG(
		GraphPtr                        pGraph,
		LPTSTR                          file,
		UINT16                          cxBounds,
		UINT16                          cyBounds,
		INT16 		                      nTransparent
);

INT16 PUBLIC
ExportSVGZ(
		GraphPtr                        pGraph,
		LPTSTR                          file,
		UINT16                          cxBounds,
		UINT16                          cyBounds,
		INT16 		                      nTransparent
);

/* Save a chart as a Windows-style DIB (.BMP) file on disk. */
INT16 PUBLIC
ExportBMP(
		GraphPtr			pGraph,					/* foreground graph */
		LPTSTR				szOutFile,				/* Name of BMP file to be written */
		UINT16				cxBounds,				/* in pixels */
		UINT16				cyBounds);				/* in pixels */

/* Export a BMP file (with BITMAPFILEHADER) to memory */
INT16 PUBLIC
ExportBMPToMemory (
		GraphPtr       pGraph,        /* foreground graph */
		MEMPTR         *ppMemAd,      /* returns address of BMP file in memory */
		UINT32         *pnSize,       /* size of BMP file in memory */
		UINT16         cxBounds,      /* image width in pixels */
		UINT16         cyBounds       /* image height in pixels */
);

/*  This function saves the graph as a .BMP file and stores the .BMP file */
/*  into a RamFile.  Use function GetRamFileBuffer(*ppRamFile) to retrieve the actual bytes. */
/*  The size of the .BMP file (in bytes) is returned in *pnSize.   */
/*  WARNING!!! - This function ALLOCATES MEMORY!  When you are through with */
/*  the Ramfile image of the BMP file, you should free it with a  */
/*  FreeRamFile() call. Good for CGI applets that need to pipe things to StdOut. */

/*  NOTE: Uses Ramfiles instead of memory pointer for increased creation/output speed. */

/*  NOTE: You can simulate an lpbi = LockHandle(hDIB) call by setting bWantFileHeader = FALSE */
/*        This means the byte-stream will start with the BIMAPINFOHEADER information. */
#if 1 /* NOTE: Headless server library uses ExportBMPToMemory instead (bWantFileHeader is always true there)*/
INT16 PUBLIC
ExportBMPtoRamFile(
		GraphPtr				pGraph,					/* foreground graph */
		RamFilePtr				*ppRamFile,				/* Returns Ramfile Ptr */
		UINT32					*pnSize,				/* Returns size of BMP */
		UINT16					cxBounds,			 	/* in pixels */
		UINT16					cyBounds,				/* in pixels */
		BOOLEAN16				bWantFileHeader			/* TRUE = File stream starts with BITMAPFILEHADER info */
);

#endif

/* 15jan02DW: A version of JPG export that has a settable 'quality' parameter */
INT16 PUBLIC
ExportJPEG_Quality(GraphPtr		  graphPtr,			
		LPTSTR		  file,		
		UINT16		  imgWidth,		
		UINT16		  imgHeight,		
		BOOLEAN16	  transBackground,  
		LPGDPALETTE	  palettePtr,
		INT16			nQuality	/* Set <0..100> or use '-1' as default */
);

/* 15jan02DW: A version of JPG export that has a settable 'quality' parameter */
INT16 PUBLIC
ExportJPEGToMemory_Quality(GraphPtr		  graphPtr,			
		MEMPTR*		  memAddress,		
		UINT32*		  fileSize,		
		UINT16		  imgWidth,		
		UINT16		  imgHeight,		
		BOOLEAN16	  transBackground,  
		LPGDPALETTE	  palettePtr,
		INT16			nQuality	/* Set <0..100> or use '-1' as default */
);


/*  Function to set unpainted background pixels to be a passed-in GIF file. */
/*   */
INT16	PUBLIC
gloSetBackgroundGIF(
		LPTSTR	szFile, /* Name of GIF file to be used. NULL = Free global background memory */
		INT16	nMode);	/* 0 = TILE this image on background, 1 = STRETCH this image to fit background */

#endif /* PGSDK_COMPAT_WANT_IMPLICIT_DEVICE */


/*****************************************************************************
 * !NAME!
 *   GetWireFrameBounds
 *
 * DESCRIPTION:	Access routine to allow caller to get the bounding rect
 *								of the wireframe in virtual coordinates as it appears on screen
 *
 * RETURNS:			The bounding Rect32 in virtual coordinates
 *
 * NOTES:				Written by DW, 16jul96
 *
 ******************************************************************************/
BOOLEAN16 PUBLIC GetWireFrameBounds
(
		GraphPtr		pGraph,	 	/* Pointer to graph structure.	 */
		Rect32 *			pRect		/* Ptr to Bounding Rect32. in virt. coord.*/
);

/* 1aug96DW:  New legend enhancements for A2D_SHOW_LEGEND */
#define AUTOSIZE_LEGEND 4
#define AUTOCENTER_LEGEND 16
#define SAVE_ORIGINAL_LEGEND 32
#define FORCE_LEGEND 64
#define ADJUST_FRAME 8
#define ADJUST_FRAME_LEFT 128

INT16 PUBLIC                    DESetUpdateRGN          (DrawEnvPtr pDE, HRGN hUpdateRgn);

/* Handy routine to Remove all VISUALIZE effects from a chart. */
void PUBLIC
RestoreVisualizeGraph(GraphPtr	pGraph);

/* Set the physical size of the Graph in your Document. */
/* Units are in hiMetrics */
VOID PUBLIC
SetGraphDocSize(
		GraphPtr			pGraph,
		REAL					Width,	/* Default = 10160, which is 4 inches */
		REAL					Height	/* Default = 7620, which is 3 inches */
);

/* Get the physical size of the Graph in your Document. */
/* Units are in hiMetrics */
VOID PUBLIC
GetGraphDocSize(
		GraphPtr			pGraph,
		REAL					*pWidth,
		REAL					*pHeight
);

/*  1999Feb11 PPN: CGraph still needs the following functions */
/* #ifndef _MAPINFO	 */ /*  Already defined in MapInfo headers, removed to avoid conflicts 1-29-99/FL */

#define UNIT_INCHES		0
#define	UNIT_CM				1
#define UNIT_PIXELS		2
#define UNIT_HIMETRICS		3		/*  1/100 of mm */

VOID PUBLIC
SetGraphResolutionUnit(
		GraphPtr			pGraph,
		INT16					nUnit
);

INT16 PUBLIC
GetGraphResolutionUnit(
		GraphPtr			pGraph
);

/* #endif  */ /*  _MAPINFO */

/* 16jan02DW: Set the DPI assumed by this graph when imaged  (Default is 96 dpi) */
VOID PUBLIC
SetGraphDPIResolution(
		GraphPtr			pGraph,
		INT16				nDPI
);

/* 16jan02DW: Get the DPI assumed by this graph when imaged */
INT16 PUBLIC
GetGraphDPIResolution(
		GraphPtr			pGraph
);


/* Public routine our customers use to Pre-calculate the curves for a chart */
INT16 PUBLIC
PreCalcCurves(
		GraphPtr				G,
		INT16					nAllSeries,
		INT16					nAllGroups
);

/* Public routine our customers use to set current scroll position on scrolled charts */
INT16	PUBLIC
SetScrollPosition(
		GraphPtr				G,
		INT16					nGroup
);

/* Public routines our customers use to turn OFF the precalc feature. */
INT16	PUBLIC
StopPreCalc(
		GraphPtr		G
);

/* 12may98DW: Definitions of Advanced Curvefits for use with A2D_ADVANCE_CURVEFITS */
#define BQUADRATICREGRESS			1
#define BMODIFIEDHYPERBOLIC			2
#define BRATIONAL					4
#define BLOGQUADRATIC				8
#define BHYPERBOLIC					16
#define BMODIFIEDEXPONENTIAL		32

/* 1Jul98DW: Routines to set some TrueType Default font parameters on UNIX machines */
void PUBLIC			GloSetDefaultFontName(LPTSTR pszDefaultFontName);
LPTSTR PUBLIC		GloGetDefaultFontName();
void PUBLIC			GloSetDefaultFontPath(LPTSTR pszDefaultFontPath);
LPTSTR PUBLIC		GloGetDefaultFontPath();

/*#if HAVE_ICONV*/ /* 26mar2002jans: uncomment if needed */
/* 17dec2001jans: dealing with different input encodings: see sux_iconv*/
typedef MEMSIZE (*lenfunction)(const void *str);
typedef MEMSIZE (*termstrfunction)(void *ptr);
int SetInputEncoding(GraphPtr pGraph,const char *inputEncoding,lenfunction pGetBytesStrLen,termstrfunction pTerminateString);
/* 30dec2001jans: String termination builtin fuctions */
MEMSIZE TerminateSingleByteNULL(char *ptr);
MEMSIZE TerminateDoubleByteNULL(INT16 *ptr);
/* #endif */

/*  18aug00DW: */
/*  TRUE (DEFAULT) = Allow TrueType Font rendering on Headless server if TrueType library is available */
/*  FALSE = Use internal bitmap fonts for rendering regardless of avialability of TrueType */
void PUBLIC			GloSetTrueTypeMode(BOOLEAN16	bAllowTrueType);
BOOLEAN16 PUBLIC	GloGetTrueTypeMode();


/*  1999aug17 PPN: For getting VDM page number */
/*  allowing charts to share a single page of data */
INT16	PUBLIC
GetCurrentPage(GraphPtr pGraph);

/*  1999aug17 PPN: For setting VDM page number */
/*  allowing charts to share a single page of data */
/*  returns old page number. */
INT16 PUBLIC
SetCurrentPage(GraphPtr pGraph, int npage);


/* -----------------------------------------------------------------------  */
/*  Special Effect section */
/*  Merged by CP on 11/March/98 from PG_FX.H */
/* -----------------------------------------------------------------------  */

//#define MAXFXFILENAME 64
//#define MAXFILENAME 128 /* 30mar2007jans: value 128 seems to be absolutely wrong */
#define MAX_EFFECT_NAME 32
#define MAX_WASHLAYER 6

enum SfxType {	WASH_FX = 3,
	PICTURE_FX = 4,
	USER_PICTURE_FX = 6,
	BRUSH_FX = 13,
	ADVANCED_WASH_FX = 14,
	GRADIENT_FX = 15
};

/*  BEGIN Added by FL 4-30-98 from pgedit.h */
/***********************************************************************
 *
 * Preset categories
 *
 ************************************************************************/

enum SFXCategoryEnum {
	SFX_PICTURES,
	SFX_SPECIAL,
	SFX_WASHES,
	SFX_DIRECT,

	MAX_SFXCategoryEnum
};

/* WashRecord, PictureRecord moved to dev_vector_api.h */



/***********************************************************************
 *
 * Enumerated constants for various SFX parameters
 *
 ************************************************************************/

enum SFXLayerType {	
	NilLayer,
	LightenLayer,
	DarkenLayer,
	WashLayer,
	PictureLayer,
	VanillaLayer,
	TextureLayer,
	PickupLayer,
	BlurLayer,
	BlendLayer,
	SpecialLayer,
	ColorizeLayer,
	ClipArtLayer,
	BrushLayer,				/* TL:12/03/93 added special effect */
	AdvancedWashLayer,		/*  24mar1998PN: Added more powerful washes for COREL */
	GradientLayer,    //GradientRecord
	Picture2Layer,
	MAX_SFXLAYERTYPE

};

/***********************************************************************
 *
 * Handles for SFXRecord and SFXPresetList
 *
 ************************************************************************/

typedef MEMPTR SFXHandle;
typedef MEMPTR SFXPresetHdl;

/*  END Added by FL 4-30-98 from pgedit.h */


/* TL:12/03/93 added brush special effect.
 ** No way to save the brush in the TDL file.
 ** Can only be used at run time.
 */
typedef struct
{
	HBRUSH	hBrush;
	INT16		nBrushMode;
} BrushRecord;


#include "pgsdk/dev_vector_api.h"

/***********************************************************************
 *
 * Structure of a DGDS FX record
 *
 ************************************************************************/

typedef struct 
{
	INT16             effect;
	union
	{
		WashRecord			wash;
		PictureRecord		picture;
		BrushRecord			brush;
		AdvancedWashRecord	advwash;
		GradientRecord gradient;
	} info;
} FXStruct, *FXPtr;

typedef struct 
{
	INT16		pictScale;
	INT16		pictFlip;
	INT16		xExt;
	INT16		yExt;
	TCHAR		pictName[MAXFILENAME];
	INT16		format;
	INT32		hdl;
} TextureRecord;

typedef struct 
{
	INT16		lightenPct;
} LightenRecord;

typedef struct
{
	INT16		darkenPct;
} DarkenRecord;

typedef struct 
{
	INT16		red;
	INT16		green;
	INT16		blue;
	INT16		percent;
} ColorizeRecord;

typedef struct 
{
	INT16		blurRadius;
} BlurRecord;

typedef struct 
{
	MEMPTR		hPip;
} PickupRecord;

typedef struct 
{
	RGB16		rgbColor;
} VanillaRecord;


/***********************************************************************
 *
 * Structure of a single layer, with a union of the above structs 
 *
 ************************************************************************/

typedef struct 
{
	CONTROL /*%%EffectType2Union%%*/	effectType;
	INT16 						offsetX;
	INT16 						offsetY;
	INT16							user1;
	INT16							user2;
	INT16							reserved[8];
	union
	{
		WashRecord			wash;
		TextureRecord		texture;
		PictureRecord		picture;
		LightenRecord		lighten;
		DarkenRecord		darken;
		BlurRecord			blur;
		PickupRecord		pickup;
		ColorizeRecord	colorize;
		VanillaRecord		vanilla;
		BrushRecord			brush;			/* TL:12/03/93 added brush special effect */
		AdvancedWashRecord	advwash;		/* 24mar1998PN: Added for COREL */
		GradientRecord	    gradient; /* 24apr2006jans: support for multistep washes */
	} info;
} LayerStruct, *LayerPtr;

/***********************************************************************
 *
 * Structure of a multi-layer special effect
 *
 ************************************************************************/

typedef struct tagSFXRecord
{
	INT16 				version;
	INT16 				layerCount;
	LayerStruct 	aLayer[MAX_WASHLAYER];
} SFXRecord;//, *SFXPtr;

/***********************************************************************
 *
 * Structures for SFX preset files
 *
 ************************************************************************/

typedef struct 
{
	TCHAR effectName[MAX_EFFECT_NAME];
	SFXRecord fx;
}	SFXPSElem;

typedef struct 
{
	INT16 numPresets;
	SFXPSElem presets[1];
}	SFXPresetList, *SFXPresetPtr;


/*  Exported functions */
/*  ------------------- */
SFXPtr PUBLIC NewSFXHandle(LPTSTR name);
SFXPresetHdl PUBLIC	LoadSFXPresets(LPTSTR szEffectPath, INT16 nFile);
void PUBLIC IsolateSFXHandle(SFXHandle hSFX);
INT16 PUBLIC PresetName2Num(LPTSTR szEffectPath, INT16 nFile, LPTSTR sfxName);
INT16 PUBLIC GetSFXPreset(LPTSTR szEffectPath, INT16 nFile, SFXHandle hSFX, 
		INT16 sfxNum,LPTSTR sfxName);
INT16 PUBLIC SaveSFX(LPTSTR szEffectPath, INT16 nFile, SFXHandle hSFX, LPTSTR name);
void PUBLIC DeleteSFX(LPTSTR szEffectPath, INT16 nFile, LPTSTR name);

/*  END Added by FL 4-30-98 from pgedit.h */


/***********************************************************************
 *
 * Enumerated constants for various SFX parameters
 *
 ************************************************************************/


enum WashTypes {												
	NilWashType,			/* 0  */
	WashUpLeft,	 			/* 1  */
	WashUp,		 				/* 2  */
	WashUpRight, 			/* 3  */
	WashLeft,	 				/* 4  */
	WashRight, 	 			/* 5  */
	WashDownLeft,			/* 6  */
	WashDown,	 				/* 7  */
	WashDownRight,		/* 8  */
	WashOval,					/* 9  */
	WashZoomRect,			/* 10 */
	WashSpecial1,			/* 11 */
	WashSpecial2,			/* 12 */
	WashSpecial3,			/* 13 */
	WashLinear				/* 14 */
};

enum BrushModes {
	BRUSHHANDLE
};

typedef enum _FlipTypes {
	NullFlip,
	FlipH,
	FlipV,
	FlipHV
} FlipTypes;

typedef enum _PictureScalingTypes {
	ScaleNone,
	Scale2Fit,
	Scale2Frame,
	Scale2Background,
	Tiled,
	Tiled2Frame,
	Tiled2Background,
	BrickHorizontal,
	BrickVertical,
	Stack,
	ScaleProportional
} PictureScalingTypes;

/* Set the User Picture Callback Function */

INT16 PUBLIC
SetPictureCallBack(
		LPFNPictureCallBack	lpfnPictureCB
);


/* -----------------------------------------------------------------------  */
/*  Graph Type section  */
/*  Merged by CP on 11/March/98 from GTYPES.H */
/* -----------------------------------------------------------------------  */
/* DEFINITION OF SUPPORTED SUB_GRAPH TYPES FOR DGDS  (7/10/92 DW) */

#define THREE_D_BARS                        0       
#define THREE_D_CUBE                        1
#define THREE_D_CUTCORNER                   2
#define THREE_D_DIAMOND                     3
#define THREE_D_OCTAGON                     4
#define THREE_D_PYRAMID                     5
#define THREE_D_SQUARE                      6
#define THREE_D_ROW_AREA                    7
#define THREE_D_ROW_LINE                    8
#define THREE_D_ROW_STEP                    9
#define THREE_D_COL_AREA                    10
#define THREE_D_COL_LINE                    11
#define THREE_D_COL_STEP                    12
#define THREE_D_HONEYCOMB                   13
#define THREE_D_MODEL                       14
#define THREE_D_SURFACE                     15
#define THREE_D_SCATTER                     16
#define THREE_D_CONTOUR_SURFACE             22


#define AREA_ABSOLUTE                       0
#define AREA_ABSOLUTE_DUALY                 1
#define AREA_ABSOLUTE_DUALY_BIPOLAR         3
#define AREA_STACKED                        4
#define AREA_STACKED_DUALY                  5
#define AREA_STACKED_DUALY_BIPOLAR          7
#define AREA_PERCENT                        12  


#define BAR_ABSOLUTE                        0
#define BAR_ABSOLUTE_DUALY                  1
#define BAR_ABSOLUTE_BIPOLAR                2
#define BAR_ABSOLUTE_DUALY_BIPOLAR          3
#define BAR_STACKED                         4
#define BAR_STACKED_DUALY                   5
#define BAR_STACKED_BIPOLAR                 6
#define BAR_STACKED_DUALY_BIPOLAR           7
#define BAR_SIDEBYSIDE                      8
#define BAR_SIDEBYSIDE_DUALY                9
#define BAR_SIDEBYSIDE_DUALY_BIPOLAR        11
#define BAR_PERCENT                         12


#define LINE_ABSOLUTE                       0
#define LINE_ABSOLUTE_DUALY                 1
#define LINE_ABSOLUTE_DUALY_BIPOLAR         3
#define LINE_STACKED                        4
#define LINE_STACKED_DUALY                  5
#define LINE_STACKED_DUALY_BIPOLAR          7
#define LINE_PERCENT                        12  

#define PIE_SINGLE                          0
#define PIE_SINGLE_RING                     1
#define PIE_MULTIPLE                        2
#define PIE_MULTIPLE_PROPORTIONAL           3
#define PIE_MULTIPLE_RING                   4
#define PIE_BAR                             6

#define SPECIAL_HISTOGRAM                   0
#define SPECIAL_SPECTRAL                    1
#define SPECIAL_SCATTER                     3
#define SPECIAL_SCATTER_DUALY               4
#define SPECIAL_HILO                        5
#define SPECIAL_HILO_DUALY                  6
#define SPECIAL_BOXPLOT                     8
#define SPECIAL_GANTT                       19
#define SPECIAL_POLAR                       2
#define SPECIAL_RADAR_ABSOLUTE_LINE         12
#define SPECIAL_RADAR_STACKED_LINE          13
#define SPECIAL_RADAR_ABSOLUTE_LINE_DUALY   14
#define SPECIAL_RADAR_STACKED_LINE_DUALY    15
#define SPECIAL_BUBBLE                      17
#define SPECIAL_RADAR_ABSOLUTE_AREA         20
#define SPECIAL_JAPANESE_STOCK              21
#define SPECIAL_STEMLEAF                    22
#define SPECIAL_MULTIBAR                    23
#define SPECIAL_WATERFALL                   24
#define SPECIAL_WATERFALL_HORIZONTAL        25
/* 8mar00DW: Chart Intelligence ChartTypes (Only available in Chart Intelligence build) */
#define SPECIAL_CI_BALANCE_SCORECARD        26
#define SPECIAL_CI_PRODUCT_POSITION         27
#define SPECIAL_CI_RESOURCE_RETURN          28
#define SPECIAL_CI_TIME_SERIES				29
#define SPECIAL_CI_4Y				        30
#define SPECIAL_CI_CENTROID			        31
#define SPECIAL_RADAR_STACKED_AREA			32
#define SPECIAL_PARETO						33
#define SPECIAL_PARETO_WITH_PERCENT			34
#if 0 /* 06nov2007jans: in Cognos build */
#define SPECIAL_GAUGE						35
#else /* 06nov2007jans: in MS build! */
#define SPECIAL_FUNNEL                      35
#define SPECIAL_GAUGE                       36
#endif
#define SPECIAL_RADAR_ABSOLUTE_AREA_DUALY   37
#define SPECIAL_RADAR_STACKED_AREA_DUALY    38

#define TABLE_REGULAR                       0

/*  BEGIN Added by FL 4-30-98 from pgedit.h */
/************************************************************************
 * from STD_TIFF.H
 ************************************************************************/

/************************************************************************
 * TiffHeaderRec -- TIFF File Header Record
 ************************************************************************/
typedef struct {
	UINT16					uFormat;							/* byte order */
	UINT16					uVersion;							/* must always be 42. (really!) */
	UINT32					ulDirOffset;					/* offset to first directory */
} TiffHeaderRec, * TiffHeaderPtr;

/************************************************************************
 * TiffOutputRec -- TIFF Output File Information
 ************************************************************************/
typedef struct {
	IOPATHPTR				iop;								/* file reference */
	UINT32					ulStart;							/* starting offset */
	UINT32					ulNext;								/* next available data offset */

	TiffHeaderRec			hdr;								/* header */
	PVTABLE					vtDir;								/* IFD dir entries */

} TiffOutputRec, * TiffOutputPtr;


/************************************************************************
 * TiffInputRec -- TIFF Input File Record
 ************************************************************************/
typedef struct {
	IOPATHPTR				iop;									/* file reference */
	INT16						bHeaderInited;				/* header has been read */
	INT16						bNeedSwap;						/* TRUE if byte swap needed */

	TiffHeaderRec		hdr;									/* header */
	PVTABLE				vtDir;								/* IFD dir entries */

} TiffInputRec, * TiffInputPtr;

/* Page Setup Tags 
 * ----------------
 */
#define TIFFX_VIEW_SIZE									0xC004
#define TIFFX_PIX_PER_UNIT							0xC005
#define TIFFX_PAGE_DIMENSIONS						0xC006
#define TIFFX_PAGE_MARGINS							0xC007
#define TIFFX_MEASURING_UNITS						0xC008
#define TIFFX_ORIENTATION								0xC009
#define TIFFX_PAPER_SIZE								0xC00A
#define TIFFX_PAGE_POSITION							0xC00B
#define TIFFX_PAGE_FRAME_STATE					0xC00C


/* TIFF type constants
 * --------------------
 */
#define TIFF_TYPE_UNKNOWN		0
#define TIFF_TYPE_BYTE			1
#define TIFF_TYPE_ASCII			2
#define TIFF_TYPE_SHORT			3
#define TIFF_TYPE_LONG			4
#define TIFF_TYPE_RATIONAL		5

/*
 ** TIFF tag constants
 */
#define TIFF_NEWSUBFILETYPE              254
#define TIFF_OLDSUBFILETYPE              255
#define TIFF_IMAGEWIDTH                  256
#define TIFF_IMAGELENGTH                 257
#define TIFF_BITSPERSAMPLE               258
#define TIFF_COMPRESSION                 259

#define TIFF_PHOTOMETRICINTERPRETATION   262
#define TIFF_THRESHHOLDING							263
#define TIFF_CELLWIDTH									264
#define TIFF_CELLLENGTH									265
#define TIFF_FILLORDER									266

#define TIFF_DOCUMENTNAME								269
#define TIFF_IMAGEDESCRIPTION						270
#define TIFF_MAKE												271
#define TIFF_MODEL											272
#define TIFF_STRIPOFFSETS								273
#define TIFF_ORIENTATION								274

#define TIFF_SAMPLESPERPIXEL						277
#define TIFF_ROWSPERSTRIP								278
#define TIFF_STRIPBYTECOUNTS						279
#define TIFF_MINSAMPLEVALUE							280
#define TIFF_MAXSAMPLEVALUE							281
#define TIFF_XRESOLUTION								282
#define TIFF_YRESOLUTION								283
#define TIFF_PLANARCONFIGURATION				284
#define TIFF_PAGENAME										285
#define TIFF_XPOSITION									286
#define TIFF_YPOSITION									287
#define TIFF_FREEOFFSETS								288
#define TIFF_FREEBYTECOUNTS							289
#define	TIFF_GRAYUNIT										290
#define	TIFF_GRAYCURVE									291

#define TIFF_RESOLUTIONUNIT							296
#define	TIFF_PAGENUMBER									297

#define	TIFF_COLORRESPONSECURVES				301

#define	TIFF_SOFTWARE										305
#define	TIFF_DATETIME										306

#define TIFF_ARTIST											315
#define TIFF_HOSTCOMPUTER								316

#define TIFF_PREDICTOR									317
#define	TIFF_WHITEPOINT									318
#define	TIFF_PRIMARYCHROMATICITIES			319
#define	TIFF_COLORMAP										320

#define TIFF_HIGHSHADOW									321
#define TIFF_TILEWIDTH									322
#define TIFF_TILELENGTH									323
#define TIFF_TILEOFFSETS								324
#define TIFF_TILEBYTECOUNTS							325
#define TIFF_KIDS												330

TiffInputPtr PUBLIC		Tiff_AllocInputInfo			( IOPATHPTR				iop 			);
void PUBLIC						Tiff_FreeInputInfo			( TiffInputPtr		pInput 		);       
TiffOutputPtr PUBLIC	Tiff_AllocOutputInfo		( IOPATHPTR				iop 			);
void PUBLIC						Tiff_FreeOutputInfo			( TiffOutputPtr		pOutput 	);       

INT16 PUBLIC					Tiff_ReadScalarTag			(	TiffInputPtr		pInput,
		UINT16					uTag,
		UINT32 *		pulValue	);

INT16 PUBLIC					Tiff_ReadArrayTag				(	TiffInputPtr		pInput,
		UINT16					uTag,
		void * *ppData,
		UINT32 *		pulMaxData	);
INT16 PUBLIC					Tiff_WriteEntry					( TiffOutputPtr		pOutput,
		UINT16					uTag,
		UINT16					uType,
		UINT32					ulCount,
		void *			pData  		);
INT16 PUBLIC					Tiff_WriteDir						( TiffOutputPtr pInfo );
INT16 PUBLIC					F3DF_WriteDateTime			( TiffOutputPtr pTiffOut );

#define VERSION_3DF 					"3DF.0003 18apr95/dw"
#define DDE_FILE_SENTINEL			"DDELINK"

#define THUMBNAIL_WIDTH		80
#define THUMBNAIL_HEIGHT	60

INT16 PUBLIC
F3DF_ReadThumbnail(
		TiffInputPtr		pInput,								/* input file info */
		void *			pPixRef								/* returned PictHandle/DIB-handle */
);

INT16 PUBLIC
F3DF_ReadDescription(
		TiffInputPtr	pInput,			/* input file info */
		LPTSTR 			szDescription,	/* caller-supplied buffer */
		INT16			nMaxBuf			/* max buffer size */
);

INT16 PUBLIC
F3DF_ReadGraph(
		TiffInputPtr		pInput,								/* input file info */
		GraphPtr				pGraph,								/* destination graph */
		BOOLEAN16				bWantData							/* TRUE to load data */
);

INT16 PUBLIC
F3DF_WriteGraph(
		TiffOutputPtr		pOutput,									/* already open file */
		GraphPtr				pGraph, 									/* graph to be saved */
		INT16						nRequest
);

INT16 PUBLIC
Save_TIFFGraph (
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator,	/* Whether to add terminators after each record */
		LPTSTR 		pszFileName		/* Ptr to a "C" style string filename */
);
INT16 PUBLIC
Save_TIFFGraphPath (
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator		/* Whether to add terminators after each record */
);

INT16 PUBLIC
ThumbWIN_LoadThumbnail (
		LPTSTR 			pszFileName,	/* Ptr to a "C" style string filename */
		void *		pvPixRef
);

INT16 PUBLIC
ThumbWIN_LoadDescription (
		LPTSTR  	pszFileName,		/*  IN: File Specification record Sys 7 type */
		LPTSTR 		pszDescription,		/*  OUT: Description of file */
		size_t		nSizeOfDescription	/*  IN: Size of the description buffer */
);

INT16 PUBLIC
ThumbWIN_SetFileDescriptionIntoGraph(
		GraphPtr		pGraph,				/* IN & OUT: Ptr to the Graph  */
		LPTSTR 			pszDescription		/* IN: The text description to set  */
);

INT16 PUBLIC ThumbWIN_UpdateInGraph(GraphPtr, INT16, INT16);

INT16 PUBLIC ThumbWIN_GetThumbnailFromGraph(
		GraphPtr		pGraph,				/* IN & OUT: Ptr to the Graph  */
		HANDLE *		phThumbnail 
);	

INT16 PUBLIC
ThumbWIN_GetFileDescriptionFromGraph(
		GraphPtr		pGraph,				/* IN & OUT: Ptr to the Graph  */
		LPTSTR 			pszDescription		/* OUT: The text description in the graph */
);


/************************************************************************
 * from DIB.H
 ************************************************************************/

BOOLEAN16 PUBLIC DibBlt	( HDC hdc, 
		int x0, 
		int y0, 
		int dx, 
		int dy, 
		HANDLE hdib, 
		int x1, 
		int y1, 
		LONG rop );

/************************************************************************
 * from C3D_VUIF.H
 ************************************************************************/

typedef enum _CustomViewCodes {

	/*  0 */	CV_Turn_X_Minus,
	/*  1 */	CV_Turn_Y_Minus,
	/*  2 */	CV_Turn_Z_Minus,
	/*  3 */	CV_Turn_X_Plus,
	/*  4 */	CV_Turn_Y_Plus,
	/*  5 */	CV_Turn_Z_Plus,

	/*  6 */	CV_Move_X_Minus,
	/*  7 */	CV_Move_Y_Minus,
	/*  8 */	CV_Move_Z_Minus,
	/*  9 */	CV_Move_X_Plus,
	/* 10 */	CV_Move_Y_Plus,
	/* 11 */	CV_Move_Z_Plus,

	/* 12 */	CV_Size_X_Minus,
	/* 13 */	CV_Size_Y_Minus,
	/* 14 */	CV_Size_Z_Minus,
	/* 15 */	CV_Size_X_Plus,
	/* 16 */	CV_Size_Y_Plus,
	/* 17 */	CV_Size_Z_Plus,

	/* 18 */	CV_Wall_X_Minus,
	/* 19 */	CV_Wall_Y_Minus,
	/* 20 */	CV_Wall_Z_Minus,
	/* 21 */	CV_Wall_X_Plus,
	/* 22 */	CV_Wall_Y_Plus,
	/* 23 */	CV_Wall_Z_Plus,

	/* 24 */	CV_Pan_X_Minus,
	/* 25 */	CV_Pan_Y_Minus,
	/* 26 */	CV_Relative,
	/* 27 */	CV_Pan_X_Plus,
	/* 28 */	CV_Pan_Y_Plus,
	/* 29 */	CV_Absolute,

	/* 30 */	CV_Zoom_Plus,
	/* 31 */	CV_Focus_Minus,
	/* 32 */	CV_StepSize,
	/* 33 */	CV_Zoom_Minus,
	/* 34 */	CV_Focus_Plus,
	/* 35 */	CV_InitMatrix,

	MAX_CUSTOMVIEWCODES

} CustomViewCode;

void PUBLIC		SaveMatrix		(GraphPtr pGraph, TDG_ViewStructPtr pDst);
void PUBLIC		RestoreMatrix	(GraphPtr pGraph, TDG_ViewStructPtr pSrc);
void PUBLIC		Customize3DView	(GraphPtr pGraph, INT16 Code, INT16 StepSize);

INT16 PUBLIC LoadMatrixFile(LPTSTR szPath, GraphPtr pGraph);
INT16 PUBLIC SaveMatrixFile(LPTSTR szPath, GraphPtr pGraph);

void PUBLIC		GetViewAngle3D		(TDG_ViewStructPtr pViewStruct, int idx);
INT16 PUBLIC	MatchViewAngles		(TDG_ViewStructPtr pViewStruct);
INT16 PUBLIC	MatchXPan			    (INT16	XPan);

/************************************************************************
 * from COM_2D.H
 ************************************************************************/

typedef enum _DrawBaseType {										/* Numeric axis riser base location. */
	FromZero, FromMin
} DrawBaseType;

/*  other functions... */

BOOLEAN16 PUBLIC IsTextChart(GraphPtr pGraph);
void PUBLIC GetGraphType( GraphPtr pGraph, INT16 *pnType, LPTSTR szTypeName );
void PUBLIC SetGraphType( GraphPtr pGraph, INT16 nType, LPTSTR szTypeName );
void PUBLIC GetScaleBoundaries( GraphPtr pGraph, REAL *pfXMin, REAL *pfXMax,
		REAL *pfYMin, REAL *pfYMax, REAL *pfZMin, 
		REAL *pfZMax );

/*	Codes returned by the engines' GetAttr and SetAttr routines.
 * -------------------------------------------------------------
 */
typedef enum _AttrStatusType { 	
	NO_UNDO = -2,	/* attribute changed, no undo but redraw */
	NO_ACTION, 		/* attribute changed, but no action necessary */
	NO_MATCH, 		/* attribute not found */
	IS_CHANGED, 	/* attribute changed, redraw all the view entirelly */
	CUSTOM_VIEW,	/* mac stuff, can't remove... */
	NO_REDRAW, 		/* attribute changed, no redraw */
	UPDATE_ME 		/* attribut changed, update only selected objects */
} AttrStatusType;


/************************************************************************
 * from CAN_CPP.H
 ************************************************************************/

/*****************************************
 *  P a c k e d A n o d e I n f o R e c  *
 *****************************************/
typedef struct _PackedAnodeInfoRec {
	AnodeInfoRec			anode;
	AreaInstRec				ainst;
	LineInstRec				linst;
	FontInstRec				finst;

#if WANT_SFX
	SFXRecord					areaSFX;
	SFXRecord					lineSFX;
	SFXRecord					fontSFX;
#else
	INT16						areaSFX; /* Dummy space... */
	INT16						lineSFX; /* Dummy space... */
	INT16						fontSFX; /* Dummy space... */
#endif
	INT16							auxType;
	MEMSIZE						auxBytes;
} PackedAnodeInfoRec, * PackedAnodeInfoPtr;
typedef MEMPTR PackedAnodeInfoHdl;

/*****************************************
 * 				A n o d e A r r a y R e c			 *
 *****************************************/
typedef struct {
	INT16									numAnodes;
	PackedAnodeInfoRec		anodes[1];

} AnodeArrayRec, * AnodeArrayPtr;
typedef MEMPTR AnodeArrayHdl;

AnodeArrayPtr	PUBLIC
PackSelectedAnodes(
		GraphPtr			pGraph,									/* Pointer to Mr. Graph 			*/
		SelListPtr		pSelList,								/* Pointer to selection list 	*/
		INT16					PreserveGrouping				/* Pointer to space for return value	*/
);

INT16 PUBLIC
UnpackAndSelectAnodes(
		DrawEnvPtr		pDE,										/* Pointer to Draw Environment		*/
		GraphPtr			pGraph, 								/* Pointer to graph 							*/
		SelListPtr		pSelList,								/* Pointer to selection list 			*/
		AnodeArrayPtr	pAnodeArray,						/* Pointer to packed anodes				*/
		INT16					placement								/* Code for placement of new guys */
);


/* Set the resources handle if the SDK is used as a lib
 */
void PUBLIC SetSDKHandle( HANDLE hRes );

/* if TRUE, clips labels
 */
void PUBLIC SetLabelClipping(BOOLEAN16 bWantLabelClipping );

void PUBLIC GloSetMinAutofitSize(INT16 nLowLimit);
void PUBLIC GloSetMaxAutofitSize(INT16 nHighLimit);

/************************************************************************
 * from SAC_DATA.H (COM_ACSS.H ???)
 ************************************************************************/

typedef enum _DataItemName {
	LABEL = 1,
	VALUE,
	VALUE_ERR_HIGH,
	VALUE_ERR_LOW,
	VALUE_CLOSE,
	VALUE_HIGH,
	VALUE_LOW,
	VALUE_OPEN,
	VALUE_X,
	VALUE_Y,
	VALUE_Z,
	VALUE_Q,
	VALUE_X_MIN,
	VALUE_X_MAX,
	VALUE_F,
	VALUE_METRIC1,
	VALUE_METRIC2,
	VALUE_METRIC3,
	VALUE_ERR_MEDIAN
} DataItemName;

INT16 PUBLIC
GetNbDataPointFormat( GraphPtr pGraph );

INT16 PUBLIC
GetDataPointFormat( GraphPtr pGraph, INT16 nIndex );


/************************************************************************
 * from WIN_SHCT.H
 ************************************************************************/

BOOLEAN16 PUBLIC ResolveShortcut(HWND hWnd, LPCTSTR pszShortcutFile, LPTSTR pszPath, int cchPath);
void BrowseForFolder(HWND hwnd, char * szFolderPath, char * szPrompt);

/************************************************************************
 * from GALLERY.H
 ************************************************************************/

INT16 PUBLIC ChangeGraphType(
		GraphPtr					pGraph,
		INT16							nIndex
);

INT16 PUBLIC
GetGraphTypeIndex2(
		GraphPtr		pGraph
);

INT16 PUBLIC 
GetNbGraphTypeIndexes();

PUBLIC const char * pg_GetGraphTypeName(INT16 nMegaIndex);

/************************************************************************
 * from C3D_TOOL.H
 ************************************************************************/

enum {
	NOT_AUTOSHADED,
	CUBE_FACE,
	RISER_FACE
};

INT16	PUBLIC Categorize3DObject( INT16 nObjID );
void RestoreVisualizeGraph( GraphPtr	pGraph );

/*  END Added by FL 4-30-98 from pgedit.h */

/* 28apr98DW: For COREL, a routine to calculate a box that will fit around Title, Subtitle or Footnote. */
/* Returns a PTR to a valid BoxInst if success, NULL if failure. */
BOOLEAN16 PUBLIC FindBoxForText(
		DrawEnvPtr	pDE,
		GraphPtr	pGraph,
		INT16		nObjectID,
		LPTSTR		pText,
		BoxInstPtr	pBounds
);

/*  6-11-98/FL For corel, set the initialized corel GI pointer. */
void PUBLIC
SetGIBasePointer(
		DrawEnvPtr	pDE,
		void *	pGIBASE
);

/************************************************************************
 *  Legend Placement enum - for use with function PlaceDefaultElements()
 ************************************************************************/
typedef enum _LegendPlacementType {
	LEGEND_PLACEMENT_RIGHT,
	LEGEND_PLACEMENT_LEFT,
	LEGEND_PLACEMENT_BOTTOM,
	LEGEND_PLACEMENT_AUTO,	/* Use current legend position to decide which option (left,right,bottom,free) to use. */
	MAX_LEGEND_PLACEMENT
} LegendPlacementType;


/* 11jun98DW: Support for DEFAULT ELEMENT POSITIONS */
#define EFRAME		0x0001		/* Default Frame position */
#define ETITLE		0x0002		/* Default Title position */
#define ESUBTITLE	0x0004		/* Default SubTitle position */
#define EFOOTNOTE	0x0008		/* Default Footnote position */
#define EY1TITLE	0x0010		/* Default Y1 Axistitle position */
#define EY2TITLE	0x0020		/* Default Y2 Axistitle position */
#define EXTITLE		0x0040		/* Default X Axistitle position */
#define EMOVABLELABELS	0x0080	/* Restore Movable labels to base positions */
#define ELEGEND		0x0100		/* Recalc size of Legend */
#define EY1LABEL	0x0200		/* Recalc Y1 Labels to fit */
#define EY2LABEL	0x0400		/* Recalc Y2 Labels to fit */
#define EXLABEL		0x0800		/* Recalc X1/O1 Labels to fit */
#define EDEFAULTLABELSIZE 0x1000/* Resize label to 10 pt. while putting in its default location */
#define EDATATEXT	0x2000		/* Recalc data labels to fit */
#define EMAXIMIZEFRAME 0x4000	/* When items are turned off, make frame gobble up empty space */
#define EPIELABEL	0x8000		/* Fit pie labels on pie charts */
#define ECLEARHILITES 0x10000	/* Clear ALL hilite information */
#define E3DCUBE		0x20000		/* Fit labels on 3D so they all fit. Adjust Cube so that it is entirely onscreen. */
#define EDATAMARKERS 0x40000	/* Resize & adjust data markers so they look nice */
#define E3DDEFAULTCUBE 0x80000	/* Reset 3D Cube to a default size & position */
#define EDATATABLE	0x100000	/* Place & Size DataTable (if shown) */
#define EPIE		0x200000	/* Place, size, layout Pies in ChartFrame (if shown) */
#define E25D		0x400000	/* Set Fake3D (2.5D) depth and angle to default value (45 degree angle) */
#define ECENTER_TITLES_TO_FRAME		0x800000	/* Center Title & Subtitle over Chart Frame */
#define EKEEPFLAGS 0x1000000 /* 29mar2006jans: keep roatation/alignment/size font flags. Unused in this build  */
//#define EKEEPGROUPSKIP 0x2000000 /* 03apr2007jans: do not change user group labels skip factor. Unused in this build [Cognos]*/

/**********************************************************************************
 *	NAMED:
 *		PlaceDefaultElements
 *	DESCRIPTION:
 *		Multi-Purpose function designed to place any 2D chart element into
 *		a nice "Default Position"
 *	RETURNS:
 *		E00_OK upon success, otherwise standard E00_XXX error message.
 *	NOTES:
 *		2Mar99DW: Major Rewrite
 *	
 ************************************************************************************/

INT16 PUBLIC 
PlaceDefaultElements(
		DrawEnvPtr	pDE,				/* IN: Ptr to Draw Env. */
		GraphPtr	pGraph,				/* IN: Ptr to Graph */
		UINT32		nElement,			/* IN: OR Flags - */
		INT16		nLegendPlacement	/* IN: Pass in one of the LegendPlacementType ENUMS */
);


/*  retreive dll version, stored in the resources 7-21-98/FL */
BOOLEAN16 PUBLIC GetDLLVersion( TCHAR * szVersion );


/************************************************************************
 * WaterFallRec - Waterfall Chart Specific Data (30jul98DW)
 ************************************************************************/
typedef enum _WGroupType {
	W_NORMAL,
	W_SUBTOTAL,
	W_TOTAL,
	W_EXTRA
} WGroupType;

/*  Function for GridControl's use.  Tells engine what each row is currently */
/*  set to:  W_NORMAL, W_SUBTOTAL, W_TOTAL, W_EXTRA  */
/*  Returns:  Previous setting for that group. */
INT16 PUBLIC
SetWaterFallGroupType (
		GraphPtr		G,
		INT16			g,	/* 0..NumGroups */
		INT16			nGroupType	/* W_NORMAL, W_SUBTOTAL, W_TOTAL, W_EXTRA */
);

/* Function for Grid usage:  Return the current WaterFall GroupInfo for a particular row. */
/*  Returns One of the following: W_NORMAL, W_SUBTOTAL, W_TOTAL, W_EXTRA */
INT16 PUBLIC
GetWaterFallGroupType (
		GraphPtr		G,
		INT16			g	/* 0..NumGroups */
);

enum _RangeObjectTypes {
	BEGIN_RANGE_1,
	BEGIN_RANGE_2,
	END_RANGE_1,
	END_RANGE_2
};

/* For use in Grid Control.  Sets the value of a Range Object. Put the value to be stored */
/* into the REAL64 variable pointed to by pfValue. To CLEAR the cell (ie DELETE or "empty"),  */
/* simply pass a NULL ptr into the pfValue parameter. Returns TRUE if the function succeeded */
/* and FALSE if the function failed. */
BOOLEAN16	PUBLIC
SetWaterFallRangeObjectValue (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		INT16			g,			/* 0..NumGroups */
		PREAL64			pfValue,	/* Pointer to a REAL64 that has value to be stored. */
		INT16			nRangeObjectID /* One of the _RangeObjectTypes defined above. */
);

/* For use in Grid Control.  Retrieve the value of a Range Object. if there is NOT a value  */
/* defined (ie "empty cell") then this function returns FALSE. Otherwise it returns TRUE */
/* and the value is put into *pfValue. */
BOOLEAN16	PUBLIC
GetWaterFallRangeObjectValue (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		INT16			g,			/* 0..NumGroups */
		PREAL64			pfValue,	/* Pointer to a REAL64 that has value to be stored. */
		INT16			nRangeObjectID /* One of the _RangeObjectTypes. */
);

enum _SplitObjectTypes {
	SPLIT_1,
	SPLIT_2,
	SPLIT_3,
	SPLIT_4
};

BOOLEAN16	PUBLIC
SetWaterFallSplitObjectValue (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		INT16			g,			/* 0..NumGroups */
		PREAL64			pfValue,	/* Pointer to a REAL64 that has value to be stored. */
		INT16			nSplitObjectID /* One of the _SplitObjectTypes. */
);

BOOLEAN16	PUBLIC
GetWaterFallSplitObjectValue (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		INT16			g,			/* 0..NumGroups */
		PREAL64			pfValue,	/* Pointer to a REAL64 that has value to be stored. */
		INT16			nSplitObjectID /* One of the _SplitObjectTypes. */
);

BOOLEAN16 PUBLIC
FoundSplitValue(
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		INT16			g			/* 0..NumGroups */
);

BOOLEAN16	PUBLIC
SetWaterFallSplitObjectLabel (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		LPTSTR			psz,		/* Pointer to a string that has value to be stored. */
		INT16			nSplitObjectID /* One of the _SplitObjectTypes. */
);

BOOLEAN16	PUBLIC
GetWaterFallSplitObjectLabel (
		GraphPtr		G,			/* GraphPtr */
		INT16			s,			/* 0..NumSeries */
		LPTSTR			psz,		/* Pointer to a string that has value to be retrieved. */
		INT16			nSplitObjectID /* One of the _SplitObjectTypes. */
);


BOOLEAN16	PUBLIC
GetVDMHighWaterMark(
		GraphPtr 	pGraph,										
		PINT16	pnMaxRow, 
		PINT16	pnMaxCol
);

BOOLEAN16	PUBLIC
GetVDMString(
		GraphPtr 	pGraph,	
		INT16	nRow,
		INT16	nCol,
		LPTSTR	pString
);

BOOLEAN16	PUBLIC
GetVDMReal(
		GraphPtr 	pGraph,	
		INT16		nRow,
		INT16		nCol,
		REAL64	* pReal64
);

BOOLEAN16	PUBLIC
GetVDMEmptyCell(
		GraphPtr 	pGraph,	
		INT16		nRow,
		INT16		nCol
);

BOOLEAN16	PUBLIC
SetVDMHighWaterMark(
		GraphPtr 	pGraph,										
		INT16	nMaxRow, 
		INT16	nMaxCol
);

BOOLEAN16	PUBLIC
SetVDMString(
		GraphPtr 	pGraph,	
		INT16	nRow,
		INT16	nCol,
		LPTSTR	pString
);

BOOLEAN16	PUBLIC
SetVDMReal(
		GraphPtr 	pGraph,	
		INT16	nRow,
		INT16	nCol,
		REAL64	* pReal64
);

BOOLEAN16	PUBLIC
SetVDMEmptyCell(
		GraphPtr 	pGraph,	
		INT16		nRow,
		INT16		nCol
);


/************************************************************************
 *  ExcelRange -- Information needed by Amigo in Excel 
 ************************************************************************/
typedef struct _TDG_ExcelRange {
	INT16		nStartRow;
	INT16		nStartCol;
	INT16		nEndRow;
	INT16		nEndCol;
	INT16   nSheetIndex;
} TDG_ExcelRange,  * TDG_ExcelRangePtr; 

/*** end TDGLIB ***/


BOOLEAN16	PUBLIC
GetExcelRange(GraphPtr pGraph,
		TDG_ExcelRange* pExcelRange);

BOOLEAN16	PUBLIC
SetExcelRange(GraphPtr pGraph,
		TDG_ExcelRange* pExcelRange);


/* 99jan5DW: Function to create a hilite item for each SelItem in a selection list. */
INT16	PUBLIC
CreateHighLightsFromSelList(
		GraphPtr	pGraph,
		SelListPtr	pSelList
);

BOOLEAN16	PUBLIC
IsObjectHiLiteable(
		INT16	nObjectID
);

void PUBLIC
FreeUnusedHiLites(
		GraphPtr	pGraph
);

void PUBLIC
AddHighLight(
		GraphPtr			G,
		INT16				s,
		INT16				g,
		INT16				nObjectID,
		AreaInstPtr			pSrcArea,
		LineInstPtr			pSrcLine,
		FontInstPtr			pSrcFont
);

//3may05DW: For MicroStrategy 216981, add support for Per-series font hiliting for datatext labels
void PUBLIC
AddSeriesFontHighLight( GraphPtr G, 
						INT16	nSeriesID,
						INT16	nObjectID);

/* 16apr99DW: Granular function that will clear a specific hilite record if it is not being used. */
/* returns TRUE if it cleared a record, otherwise FALSE. */
BOOLEAN16	PUBLIC
ClearUnusedHiLite(
		GraphPtr	pGraph,
		INT16		nObjectID,
		INT16		nSeriesID,
		INT16		nGroupID
);

/**********************************************************************************
 *	NAMED:
 *		ClearHighLight
 *	DESCRIPTION:
 *		Removes particular Hilites from the Hilite list.  Must pass valid nObjectID.
 *		May use nSeriesID & nGroupID to further discriminate. OR can use 
 *		nSeriesID == IGNORE_ME and/or nGroupID == IGNORE_ME to remove ALL objects
 *		of a given type.
 *		  EXAMPLE: nObjectID = O2D_DATATEXT, nSeriesID == nGroupID == IGNORE_ME 
 *			would remove ALL hilited O2D_DATATEXT items.
 *	RETURNS: TRUE if it removed anything.
 *	NOTES: 
 *		12Mar99DW: First draft
 ************************************************************************************/
BOOLEAN16	PUBLIC
ClearHighLight(
		GraphPtr	pGraph,		/* IN: GraphPtr */
		INT16		nObjectID,	/* IN: ObjectID of item to remove */
		INT16		nSeriesID,	/* IN: SeriesID of item to remove. Use IGNORE_ME to match ANY seriesID */
		INT16		nGroupID	/* IN: GroupID of item to remove. Use IGNORE_ME to match ANY groupID */
);

/**********************************************************************************
 *	NAMED:
 *		ClearQdrHighLight
 *	DESCRIPTION:
 *		Removes all QDR objects Hilites from the Hilite list. 
 *	RETURNS: TRUE if it removed anything.
 *	NOTES: 
 *		4-16-99/FL: Copied from ClearHighLight
 ************************************************************************************/
BOOLEAN16	PUBLIC
ClearQdrHighLight(
		GraphPtr	pGraph		/* IN: GraphPtr */
);

void	PUBLIC
FreeAllHighLights(
		GraphPtr	pGraph
);

enum pg_hl_mode {
	PG_HL_MOVED = 1,     /* unused for pg_ValidHighLightExists */
	PG_HL_AREA = 2,
	PG_HL_LINE = 4,
	PG_HL_FONT = 8,
	PG_HL_MARKER = 16,   /* unused for pg_ValidHighLightExists */
	PG_HL_SIZE = 32,     /* unused for pg_ValidHighLightExists */
	PG_HL_FORMAT = 64,
	PG_HL_ALPHA = 128    /* unused for pg_ValidHighLightExists */
};


/* Is the element subject to highlight in one of modes (HL_AREA, HL_LINE, HL_FONT, HL_FORMAT) */
BOOLEAN16 PUBLIC pg_ValidHighLightExists(
		GraphPtr    pGraph,
		INT16       s,
		INT16       g,
		INT16       nObjectID,
		enum pg_hl_mode nMode
);

typedef enum _CallbackStorageMode {
	PG_CALLBACK_STOP,
	PG_CALLBACK_RECORD,
	PG_CALLBACK_PLAY
} CallbackStorageMode;

typedef enum _CallbackTypes {
	PG_REAL_TO_STRING = 0x0001,
	PG_MD_LABEL       = 0x0002
} CallbackTypes;

//Set the CallbackStorage Playback/Record/Stop mode.
INT16	PUBLIC
SetCallbackStorageMode(
   		GraphPtr			G,
		CallbackStorageMode	nMode
);

BOOLEAN16 PUBLIC
CallbacksStoredInGraph(
		GraphPtr pGraph,
		INT16    *nCallbacksStored
);

/**********************************************************************************
 *	NAMED:
 *		GetLegendLayoutInformation
 *	DESCRIPTION:
 *		Function provides all needed info for creating a sizing/moving
 *		XOR outline for use by higher-level routines.
 *	RETURNS:
 *		TRUE upon success. If it needed to fix-up pLegendBox, it returns FALSE.
 *		Puts calculated Element width & height into *pnElementWidth & *pnElementHeight
 *		Puts calculated Start Point32 of first legend element into *pStartPoint
 *		Puts legend border margins into *pnLeftRightMargin and *pnBottomMargin
 *	NOTES:
 *		2Mar99DW: First draft
 *		5Mar99DW: 2nd draft
 ************************************************************************************/
BOOLEAN16 PUBLIC
GetLegendLayoutInformation(
		DrawEnvPtr		pDE,			/* IN: Ptr to Draw Environment */
		GraphPtr		G,				/* IN: Ptr to Graph Object */
		BoxInstPtr		pLegendBox,		/* IN: Ptr to Legend Box */
		PINT16			pnElementWidth,		/* OUT: Width of Legend Element */
		PINT16			pnElementHeight,	/* OUT: Height of Legend Element */
		PINT16			pnLeftRightMargin,	/* OUT: Legend Border margin left & right  */
		PINT16			pnBottomMargin,		/* OUT: Legend Border margin at bottom of chart. */
		Point32			*pStartPoint		/* OUT: StartPoint of first legend element */
);

/**********************************************************************************
 *	NAMED:
 *		RecalcLegend
 *	DESCRIPTION:
 *		Adjusts bounds & row/col count of legend.
 *		Should be called whenever a legend element changes sizes:
 *			a) More/Fewer Series (Carlos DM)
 *			b) Different Strings in Series (Carlos DM)
 *			c) OSG_LEGEND_TEXT font changes (size, name, style)
 *	RETURNS:	E00_OK if successful, otherwise E00_XX error code.
 *	NOTES:
 *		5Mar99DW: First draft
 ************************************************************************************/
INT16 PUBLIC
RecalcLegend(
		DrawEnvPtr		pDE,			/* IN: Ptr to Draw Environment */
		GraphPtr		G				/* IN: Ptr to Graph Object */
);


/***************************************************************************************
 *	NAMED:
 *		AdjustToKeepChartOnScreen
 *	DESCRIPTION:
 *		Shrinks and moves the chart frame so that it and all its labels are
 *		guaranteed to be entirely onscreen 
 *	RETURNS:
 *		TRUE if it needed to adjust the frame, otherwise FALSE.
 *	NOTES:
 *		9Mar99DW: First draft
 *		Should be called after any change to an Axis label!
 ****************************************************************************************/

BOOLEAN16	PUBLIC
AdjustToKeepChartOnScreen(
		DrawEnvPtr	pDE,		/* IN: Draw Env. Ptr. */
		GraphPtr	pGraph		/* IN: Graph Ptr. */
);

/* 1dec99DW: For Healtheon, a routine to set custom gridline colors for particular x-axis gridlines */
INT16	PUBLIC
SetXGridOverrideColor(
		GraphPtr	pGraph,
		INT16		nGroupID,
		BOOLEAN16	bOverride,
		RGB16		*pColor
);

/***1999mar09 PPN********************************************************
 *	!NAME!
 * 	MBCExists
 *
 *	DESCRIPTION: checks if the input string contains multi-byte characters
 *				 returns true if it does, false otherwise.
 *
 ************************************************************************/

BOOLEAN16 PUBLIC			/*  OUT: True if string contains MBCS, False otherwise */
MBCExists(
		LPCTSTR		lpszText,	/*  IN: String to be tested */
		int			nLength		/*  IN: string length in bytes */
);


/***1999mar29 CC********************************************************
 *	!NAME!
 * 	ReadJAVAScriptChartFile
 *
 *	DESCRIPTION: Read graph as JavaScript code that is compatible with
 *				 our PERSPECTIVE FOR JAVA product. 
 *
 ************************************************************************/
/*  */
/*  Error Codes  */
/*  */
#define kREADJAVASCRIPTFILE_ERRORNONE 0; 
#define kREADJAVASCRIPTFILE_ERROROPENINGFILE 100 
#define kREADJAVASCRIPTFILE_ERRORFEATURENOTENABLED 200 

PUBLIC INT16 
ReadJAVAScriptChartFile(
		GraphPtr	pGraph,			/*  IN: Ptr to the graph structure  */
		DrawEnvPtr	pDE,			/*  IN: Ptr to the draw envir if any   */
		LPTSTR		pszFullPathName	/*  IN: Full pathname  */
);

/***1999mar29 CC********************************************************
 *	!NAME!
 * 	ExportGraphToJavaScript
 *
 *	DESCRIPTION: Saves graph as JavaScript code that is compatible with
 *				 our PERSPECTIVE FOR JAVA product.
 *
 ************************************************************************/
/*  */
/*  Error Codes  */
/*  */
#define kWRITEJAVASCRIPTFILE_ERRORNONE 0	
#define kWRITEJAVASCRIPTFILE_ERROROPENINGFILE 100 
#define kWRITEJAVASCRIPTFILE_ERRORFEATURENOTENABLED 200 

PUBLIC INT16 
WriteJAVAScriptChartFile(
		GraphPtr	pGraph,				/*  IN: Ptr to the graph structure  */
		LPTSTR		pszOutFullPathName,	/*  IN: Output to full pathname  */
		BOOLEAN16	bExportData,		/*  IN: Whether to export data or not, TRUE or FALSE    */
		BOOLEAN16	bOutputAsHTML		/*  IN: Output into HTML fmt with Header/Trailer, TRUE or FALSE  */
);


/************************************************************************
 * Control values for A2D_SERIES_LABELS_ON_CHART
 ************************************************************************/

#define LABELS_ON_LEFT					1 
#define LABELS_ON_RIGHT					2 
#define LABELS_ON_LEFT_AND_RIGHT		3 


/************************************************************************
 * Control values for API_PLACE_DATALABEL and API_PLACE_VALUELABEL
 ************************************************************************/
#define DTXT_CENTER			0
#define DTXT_OUT_MIN		1
#define DTXT_IN_MIN			2
#define DTXT_IN_MAX			3
#define DTXT_OUT_MAX		4
#define DTXT_OUT_PIE_STACK	5
#define DTXT_IN_PIE_SMART  6
#define DTXT_OUT_PIE_SMART_RADIAL  7
#define DTXT_OUT_PIE_SMART_STACKED 8



/* 21apr99DW_FL:  */
/* Return TRUE if this chart can use the ENTIRE ordinal axis to draw the risers */
/* Return FALSE if this chart needs a little space on right/left to draw the risers */
BOOLEAN16	PUBLIC
CanUseEntireOrdAxis(
		GraphPtr	G
);

/* For any axis, return the Min & Max that will be drawn on the scale. Also return the  */
/* # of gridlines that will be drawn. */
INT16	PUBLIC
GetAxisScaleMinMax (
		GraphPtr	pGraph,
		DataAxisRef	nAxisID,
		REAL64		*pfMin,
		REAL64		*pfMax,
		PINT16		pnGridlines
);

/*  test the object code and return TRUE if it is a data label */
BOOLEAN16 IsDataText( INT16 nObjectID );


/*  test if any visualize feature is used 6-2-99/FL */
BOOLEAN16 PUBLIC 
AreVisualizeEffectUsed( GraphPtr pGraph, INT32 nState );

/**********************************************************************************
 *	NAMED:
 *		ClearMovableHighLightsByObjID
 *	DESCRIPTION:
 *		Removes movable objects Hilites from the Hilite list that match the passed
 *		in Object ID. 
 *	RETURNS: TRUE if it removed anything.
 *	NOTES: 
 *		8-23-99/DW: Copied from ClearHighLight
 ************************************************************************************/
void	PUBLIC
ClearMovableHighLightsByObjID(
		GraphPtr	pGraph,	/* IN: GraphPtr */
		INT16		nObjectID
);

/* 15sep99DW: Routine that adjusts the location of rotated axis titles so that any change */
/* to their string keeps them in the same position. */
INT16	PUBLIC
FixupAxisTitleLocation(
		DrawEnvPtr	pDE,
		GraphPtr	pGraph,						/*  graph pointer */
		INT16		nObjectID,
		LPTSTR		lpValue					/*  string pointer */
);

/*  swap the selection list between 2 environments 9-24-99/FL */
void PUBLIC SwapDetList(
		DrawEnvPtr	pDE1,
		DrawEnvPtr	pDE2 
);

/*  check the validity of a detlist 9-28-99/FL */
BOOLEAN16 PUBLIC CheckDetList(
		DrawEnvPtr	pDE
);

/*  offscreen drawing helpers 9-24-99/FL */
void PUBLIC ClearOffscreenFlag( GraphPtr pGraph ); 
void PUBLIC DirtyOffscreenFlag( GraphPtr pGraph ); 
BOOLEAN16 PUBLIC NeedOffscreenUpdate( GraphPtr pGraph ); 

/************************************************************************
 *  Legend Placement enum - for use with A2D_LEGEND_LOCK
 ************************************************************************/
typedef enum _DefaultLegendPlacementType {
	LEGEND_FREE_FLOAT,
	LEGEND_DEFAULT_RIGHT,
	LEGEND_DEFAULT_LEFT,
	LEGEND_DEFAULT_BOTTOM,
	MAX_DEFAULT_LEGEND_PLACEMENT
} DefaultLegendPlacementType;

/* 11-10-99/FL: Definitions for A2D_MODE_SERIES (Stacked bar charts only) */
#define SERIES_NORMAL	0
#define SERIES_TOP		1
#define SERIES_BOTTOM	2


/*  save an image of the graph in any file format and copy it to memory 11-30-99/FL */
INT16 PUBLIC SaveImageToMemory(	GraphPtr pGraph,
		INT16 nWidth,			/*  width in pixels */
		INT16 nHeight,			/*  height in pixels */
		INT16 nFormat,
		INT16 nBitDepth,		/*  image depth */
		INT16 nResolution,
		HGLOBAL * pMemory,
		UINT32 * pSize
);

/*  File types provided by LEADTOOL */
/*  File types used for saveImage  */
typedef enum  _FileType {
	EXPORT_FILE_JPEG, 
	EXPORT_FILE_PNG, 
	EXPORT_FILE_PCT, 
	EXPORT_FILE_PCX, 
	EXPORT_FILE_PSD, 
	EXPORT_FILE_TGA, 
	EXPORT_FILE_TIF, 
	EXPORT_FILE_BMP
} FileType;

/*  save an image of the graph in any file format and copy it to file 11-30-99/FL */
INT16 PUBLIC SaveImageToFile(	GraphPtr pGraph,
		INT16 nWidth,			/*  width in pixels */
		INT16 nHeight,			/*  height in pixels */
		INT16 nFormat,
		INT16 nDepth,			/*  image depth */
		INT16 nResolution,
		LPTSTR szFilename 
);

/*  Object on screen resizing interface 12-10-99/FL */
void PUBLIC StartResizeDataObject(	DrawEnvPtr pDE,
		GraphPtr pGraph, 
		POINT point, 
		INT16 nResizeDataObjectID, 
		INT16 nResizeDataSeriesID, 
		INT16 nResizeDataGroupID,
		INT16 nMode );				/*  unused yet, but specify  */
/*  which data point is beeing changed */
BOOLEAN16 PUBLIC ResizingDataObject( DrawEnvPtr pDE, 
		GraphPtr pGraph, 
		Point32 point,
		LPTSTR szToolTip,
		INT16 nToolTipMaxLen );
void PUBLIC EndResizeDataObject( GraphPtr pGraph, DrawEnvPtr pDE );

void PUBLIC Get2DAreaGroupIDFromCoord( GraphPtr pGraph, 
		Point32 point, 
		INT16 * pnGroupID );
/*  get a object (riser etc...) value from its objectID, series and group */
void PUBLIC GetDataValueFromIDs(	GraphPtr pGraph,
		INT16 nObjectID,
		INT16 nSeriesID,
		INT16 nGroupID,
		REAL64 * pfData, /*  up to 5 values in the array */
		INT16 * pnError, /*  up to 5 error code in the array */
		INT16 * pnData ); /*  number of values returned */

/*  update cell in the VDM based on the values passed as parameter */
void PUBLIC UpdateChartFromData( GraphPtr pGraph, 
		INT16 nObjectID,
		INT16 nSeriesID,
		INT16 nGroupID,
		REAL64 * pfData,
		BOOLEAN16 * pbUse,	/*  up to 5 values (specify which to set) */
		INT16 nData );

/*  return a coordinate given objectID, seriesID, groupID and data value */
void PUBLIC CoordFromDataValue(	DrawEnvPtr pDE,
		GraphPtr pGraph,
		INT16 nObjectID,
		INT16 nSeriesID,
		INT16 nGroupID,
		REAL64 * pfData,			/*  3 reals, data value */
		INT16 nDataValues,
		Point32 * pCoord );		/*  coordinates returned */

void PUBLIC SetMoveHandle( DrawEnvPtr pDE, BOOLEAN16 bMove );

/************************************************************************
 * QualityAxisLabelAlignment
 ************************************************************************/
typedef enum _QualityAxisLabelAlignment {
	QA_CENTER,
	QA_INNERMAX,
	QA_OUTERMIN
} QualityAxisLabelAlignment;

/************************************************************************
 * QualityAxisLabelPosition
 ************************************************************************/
typedef enum _QualityAxisLabelPosition {
	QA_ONAXIS,
	QA_FRAMESIDE,
	QA_OUTSIDEAXIS
} QualityAxisLabelPosition;

/*******************************************************************************
 * TDG_QualityAxisInfo - Information for Quality Axis on charts (like bubble)
 * Use with attributes A2D_QUALITYAXIS_Y1, A2D_QUALITYAXIS_Y2, A2D_QUALITYAXIS_Y1
 ********************************************************************************/
typedef struct _TDG_QualityAxisInfo{
	BOOLEAN16		bShowLowerLabel;		/*  TRUE = Show Label.  FALSE = Don't show label */
	BOOLEAN16		bShowMediumLabel;		/*  TRUE = Show Label.  FALSE = Don't show label */
	BOOLEAN16		bShowHigherLabel;		/*  TRUE = Show Label.  FALSE = Don't show label */
	REAL64			QALowerSplitValue;		/*  Value of line to be imaged. */
	REAL64			QAHigherSplitValue;		/*  Value of line to be imaged. */
	BOOLEAN16		bShowLowerSplit;		/*  FALSE = OFF, TRUE = ON */
	BOOLEAN16		bShowHigherSplit;		/*  FALSE = OFF, TRUE = ON */
	BOOLEAN16		bUserDefinedLowerSplitValue;	/*  FALSE = Engine calculates value. TRUE = User calculates value. */
	BOOLEAN16		bUserDefinedHigherSplitValue;	/*  FALSE = Engine calculates value. TRUE = User calculates value. */
	TCHAR			szLowerLabel[64];		/*  Label for Segment */
	TCHAR			szMediumLabel[64];		/*  Label for Segment */
	TCHAR			szHigherLabel[64];		/*  Label for Segment */
	BOOLEAN16		bSmoothBlend;			/*  TRUE = Blend low/med/high area colors as band */
	BOOLEAN16		bShowQA;				/*  TRUE = Show Quality Info on Axis		FALSE = Don't display */
	BOOLEAN16		bLowArrow;				/*  TRUE = Draw Arrow at end of "low" axis position */
	BOOLEAN16		bHighArrow;				/*  TRUE = Draw Arrow at end of "high" axis position */
	INT16			nThickness;				/*  Virtual Thickness of the band. */
	INT16			nAxisOffset;			/*  Virtual offset of the QualityAxis from chart frame; */
	INT16			nLabelAlignment;		/*  (QA_CENTER, QA_INNERMAX, QA_OUTERMIN) */
	INT16			nLabelPosition;			/*  (QA_ONAXIS, QA_FRAMESIDE,QA_OUTSIDEAXIS) */
	BOOLEAN16		bQALabelMode;			/*  TRUE = Show NO regular gridlines and only High/Low Axis Labels. FALSE = Normal. */
	INT16			nSide;					/*  LowSide, HighSide, BothSides */

	BOOLEAN16		bShowSuperLowerLabel;		/*  TRUE = Show Label.  FALSE = Don't show label */
	BOOLEAN16		bShowSuperHigherLabel;		/*  TRUE = Show Label.  FALSE = Don't show label */
	REAL64			QASuperLowerSplitValue;		/*  Value of line to be imaged. */
	REAL64			QASuperHigherSplitValue;		/*  Value of line to be imaged. */
	BOOLEAN16		bShowSuperLowerSplit;		/*  FALSE = OFF, TRUE = ON */
	BOOLEAN16		bShowSuperHigherSplit;		/*  FALSE = OFF, TRUE = ON */
	BOOLEAN16		bUserDefinedSuperLowerSplitValue;	/*  FALSE = Engine calculates value. TRUE = User calculates value. */
	BOOLEAN16		bUserDefinedSuperHigherSplitValue;	/*  FALSE = Engine calculates value. TRUE = User calculates value. */
	TCHAR			szSuperLowerLabel[64];		/*  Label for Segment */
	TCHAR			szSuperHigherLabel[64];		/*  Label for Segment */
	REAL64			QAUpperBounds;				/*  Upper Bounds of entire QA */
	REAL64			QALowerBounds;				/*  Lower Bounds of entire QA */
	BOOLEAN16		bShowBands;					/*  Show Quality Bands across Frame. FALSE (Default) = NO BANDS. TRUE = BANDS */

} TDG_QualityAxisInfo,  *TDG_QualityAxisInfoPtr;

/**********************************************************************************
 * TDG_QualityObjectInfo - Information for Quality Objects on Product Position Chart
 * Use with functions SetQualityObjectInfo/GetQualityObjectInfo
 ***********************************************************************************/
typedef struct _TDG_QualityObjectInfo{
	BOOLEAN16	bDefined;						/*  TRUE = This Object is defined & usable. FALSE = Do not show or use. */
	TCHAR		szQualityObjectLabel[128];		/*  Label for Quality Object Legend. */
} TDG_QualityObjectInfo,  *TDG_QualityObjectInfoPtr;

/* 9feb00DW: Function to set label and state of a given QualityObject.  */
/* Used by Product Positioning Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
SetQualityObjectInfo(
		GraphPtr				pGraph,
		INT16					nIndex,					/* Valid range is <1..5> */
		TDG_QualityObjectInfoPtr	pQualityObjectInfo
);
/* 9feb00DW: Function to get label and state of a given QualityObject.  */
/* Used by Product Positioning Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
GetQualityObjectInfo(
		GraphPtr				pGraph,
		INT16					nIndex,					/* Valid range is <1..5> */
		TDG_QualityObjectInfoPtr	pQualityObjectInfo
);

/* 23feb00DW: Function to set value of the gauge.  */
/* Used by Balance Score Card Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
SetGaugeNeedleValue(
		GraphPtr				pGraph,
		PREAL64					pfValue
);

/* 23feb00DW: Function to set value of the gauge.  */
/* Used by Balance Score Card Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
GetGaugeNeedleValue(
		GraphPtr				pGraph,
		PREAL64					pfValue
);

/* 23mar00DW: Function to set value of a slider on the Equalizer object. */
/* Used by Balance Score Card Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
SetEqualizerSliderValue(
		GraphPtr				pGraph,
		INT16					nSliderIndex,
		PREAL64					pfValue
);

/* 23mar00DW: Function to gset value of a slider on the Equalizer object. */
/* Used by Balance Score Card Charts. Returns standard E00_ errorcode. */
INT16 PUBLIC
GetEqualizerSliderValue(
		GraphPtr				pGraph,
		INT16					nSliderIndex,
		PREAL64					pfValue
);

#include "pgsdk/chart-gauge.h"

/************************************************************************
 * TDG_OldGSliderRec - Information for Equalizer Object on Charts
 *	created: 23mar00DW:
 ************************************************************************/
typedef struct _TDG_OldGSliderRec{
	BOOLEAN16		bShow;				/*  TRUE = Show this range on the gauge. FALSE = Do not show this range.  */
	TCHAR			szLabel[64];		/*  Label for Gauge Range. */
	REAL64			fGaugeNeedleValue;
	REAL64			fGaugeMax;
	REAL64			fGaugeMin;
	REAL64			fGaugeMajorInterval;
	REAL64			fGaugeMinorInterval;
	BOOLEAN16		bShowGaugeMajorInterval;
	BOOLEAN16		bShowGaugeMinorInterval;
	BOOLEAN16		bAutoScaleGaugeIntervals;
} TDG_OldGSliderRec, *TDG_OldGSliderPtr;

#define MAX_OLDSLIDERS 5

/*******************************************************************************************
 * TDG_OldEqualizerObjectRec - Information for Equalizer Objects on charts (like Balance Scorecard)
 *	created: 23feb00DW: Use with A2D_EQUALIZER_INFO
 ********************************************************************************************/
typedef struct _TDG_OldEqualizerObjectRec{
	TDG_OldGSliderRec	Slider[MAX_OLDSLIDERS];
	BOOLEAN16		bShow;						/*  TRUE = Show this gauge on the chart.  FALSE = Do not show gauge. */
	TCHAR			szGaugeLabel[128];			/*  Label for Gauge Object. */
	INT16			nThickness;
	INT16			nGaugeStyle;
	INT16			nNeedleStyle;
	BOOLEAN16		bShowGaugeLabel;
	BOOLEAN16		bShowIntervalLabels;
	BOOLEAN16		bShowSliderNames;
} TDG_OldEqualizerObjectRec, *TDG_OldEqualizerObjectPtr;

/*******************************************************************************************
 * _TDG_ResourceReturnRec - Information needed by Resource Return charts
 *	created: 6apr00SJR:
 ********************************************************************************************/
typedef struct _TDG_ResourceReturnRec{
	REAL64			fTotalResource;
	BOOLEAN16		bRRIgnoreMinimumValues;
	BOOLEAN16		bRRAllocateEntireResource;
	BOOLEAN16		bRRAutoOptimize;

} TDG_ResourceReturnRec, *TDG_ResourceReturnPtr;

#if PGSDK_COMPAT_WANT_IMPLICIT_DEVICE
/**********************************************************************************
 *	NAMED:
 *		HS_PlaceDefaultElements
 *	DESCRIPTION:
 *		Multi-Purpose function designed to place any 2D chart element into
 *		a nice "Default Position"
 *	RETURNS:
 *		E00_OK upon success, otherwise standard E00_XXX error message.
 *	NOTES:
 *		6May99DW: Headless server version of PG32.H function PlaceDefaultElements
 *	
 ************************************************************************************/

/* Headless-Server wrappers for PlaceDefaultElements */

/* bitmap device */
INT16 PUBLIC 
HS_PlaceDefaultElements(
		GraphPtr	pGraph,				/* IN: Ptr to Graph */
		UINT32		nElement,			/* IN: OR Flags - */
		INT16		nLegendPlacement,	/* IN: Pass in one of the LegendPlacementType ENUMS */
		UINT16		cxBounds,		 	/* in pixels */
		UINT16		cyBounds		 	/* in pixels */
);
#endif /* PGSDK_COMPAT_WANT_IMPLICIT_DEVICE */

/*  6jan99DW: Function to do work of DryRunTheGraph when using gd library (headless-server builds) */
/*  allocates and returns a valud DrawEnvPtr that can be used for Selection List and Detnode functions */
/*  that require such a parameter */
DrawEnvPtr PUBLIC 
HS_DryRunTheGraph(
		GraphPtr	pGraph,				/* IN: Ptr to Graph */
		INT16		wantGraph,			/* =0 to disable drawing of graph      layer */
		INT16		wantBackground,		/* =0 to disable drawing of background layer */
		INT16		wantAnnotation,		/* =0 to disable drawing of annotation layer */
		UINT16		cxBounds,		 	/* in pixels */
		UINT16		cyBounds		 	/* in pixels */
);


/* 12jun03DW: Headless Server version of RecalcLegend() */
DrawEnvPtr PUBLIC 
HS_RecalcLegend(
		GraphPtr	pGraph,				/* IN: Ptr to Graph */
		UINT16		cxBounds,		 	/* in pixels */
		UINT16		cyBounds		 	/* in pixels */
);

/* 10feb00DW: Utility Routine to Disable/Enable PlaceDefaultElements Logic.   */
/* 	TRUE = Enable PlaceDefaultElements (DEFAULT).  FALSE = PlaceDefaultElements does nothing. */
/* 	RETURNS: Previous boolean value.  */
BOOLEAN16	PUBLIC
EnablePlaceDefaultElements(
		BOOLEAN16	bEnable
);

/*  copy callbacks stored in pDE */
PUBLIC void
CopyCallbacks( 
		DrawEnvPtr		pDEDest,			/* destination DrawEnvPtr */
		DrawEnvPtr		pDEOrg			/* origin DrawEnvPtr */
);

/*  set the oversampling factor for printing bitmaps (fix printed line styles) */
PUBLIC void
SetOverSamplingFactor(
		DrawEnvPtr		pDE,			/* DrawEnvPtr */
		REAL64			fOverSampling
);

PUBLIC REAL64
GetOverSamplingFactor(
		DrawEnvPtr		pDE			/* DrawEnvPtr */
);

/*  03-29-00/FL erase all the forcasted data in a time series chart in the range or touching the range */
PUBLIC void ClearTimeSeriesForecast( GraphPtr pGraph, int nStart, int nStop );

/*  03-29-00/FL erase the entire VDM */
PUBLIC void ClearAllData( GraphPtr pGraph );

/*  04-06-00/FL Usefull functions */
BOOLEAN16 PUBLIC IsY1AxisUsed(GraphPtr pGraph);
BOOLEAN16 PUBLIC IsY2AxisUsed(GraphPtr pGraph);
BOOLEAN16 PUBLIC IsY3AxisUsed(GraphPtr pGraph);
BOOLEAN16 PUBLIC IsY4AxisUsed(GraphPtr pGraph);
BOOLEAN16 PUBLIC IsXAxisUsed(GraphPtr pGraph);
BOOLEAN16 PUBLIC IsO1AxisUsed(GraphPtr pGraph);

INT16 PUBLIC
OptimizeResourceData(
		DrawEnvPtr		pDE,					/* Ptr to the Draw Environment. */
		GraphPtr			G 						/* graph pointer. */
);

BOOLEAN16 PUBLIC
TestOptimizeMinMax(
		GraphPtr			G 						/* graph pointer. */
);


/* For use with attribute ASM_SYMBOL_STYLE: */
typedef enum _StockSymbolStyles {
	DEFAULT_STYLE, 												
	VERTICAL_LINE, 												
	CANDLESTICK, 												
	LINE_CHART, 												
	AREA_CHART, 													
	LINE_CIRCLE,
	DOT,
	AREA_CHART2,
	MAX_STOCKSYMBOLSTYLES
} StockSymbolStyles;

/* For use with attribute ASM_METRICX */
typedef enum _StockMetricStyles {
	METRIC_VERTICAL_LINE, 												
	METRIC_LINE_CHART, 												
	METRIC_AREA_CHART, 													
	METRIC_BAR_CHART, 	
	METRIC_DOT,
	METRIC_AREA_CHART2,
	MAX_STOCKMETRICSTYLES
} StockMetricStyles;

/************************************************************************
 * TDG_StockMetricInfo - Information for Metric Lines on a Stock Chart
 *	created: 23mar00DW: Use with attribute ASM_METRIC1, ASM_METRIC2
 *			6-15-00/FL Added ASM_METRIC3
 ************************************************************************/
typedef struct _TDG_StockMetricInfo{
	BOOLEAN16 	bShow;						
	INT16		nDisplayStyle;		/* from StockMetricStyles enum */
	TCHAR		szMetricLabel[64];	/*  Label for Metric */
} TDG_StockMetricInfo, *TDG_StockMetricInfoPtr;

/************************************************************************
 * TDG_MultiYInfo - Information for MultiY Charts
 *	created: 23mar00DW:  Use with attribute A2D_MULTI_Y_INFO
 ************************************************************************/
typedef struct _TDG_MultiYInfo{
	INT16		nY1Start;	/* Where on Frame to Start drawing Y1 axis (0...100) */
	INT16		nY1Stop;	/* Where on Frame to Stop drawing Y1 axis (0...100) */
	INT16		nY2Start;	/* Where on Frame to Start drawing Y2 axis (0...100) */
	INT16		nY2Stop;	/* Where on Frame to Stop drawing Y2 axis (0...100) */
	INT16		nY3Start;	/* Where on Frame to Start drawing Y3 axis (0...100) */
	INT16		nY3Stop;	/* Where on Frame to Stop drawing Y3 axis (0...100) */
	INT16		nY4Start;	/* Where on Frame to Start drawing Y4 axis (0...100) */
	INT16		nY4Stop;	/* Where on Frame to Stop drawing Y4 axis (0...100) */
	BOOLEAN16 	bUseY2;		/* TRUE = Chart can use Y2 axis. FALSE = Chart does not use this axis.					 */
	BOOLEAN16 	bUseY3;		/* TRUE = Chart can use Y3 axis. FALSE = Chart does not use this axis.				 */
	BOOLEAN16 	bUseY4;		/* TRUE = Chart can use Y4 axis. FALSE = Chart does not use this axis.				 */
} TDG_MultiYInfo, *TDG_MultiYInfoPtr;

/********************************************************************************
 * TDG_ChartMasterInfo - Information for ChartMaster used by PlaceDefaultElements
 *	created: 24oct00DW:  Use with attribute A2D_CHARTMASTER_INFO
 *********************************************************************************/
typedef struct _TDG_ChartMasterInfo{
	INT16		nTitleFontHeight;		/* Height in Virtual Units of Title. */
	INT16		nSubTitleFontHeight;	/* Height in Virtual Units of SubTitle . */
	INT16		nFootnoteFontHeight;	/* Height in Virtual Units of Footnote labels. */
	INT16		nChartNormalFontHeight;	/* Height in Virtual Units of most labels on Chart */
	INT16		nChartSmallFontHeight;	/* Height in Virtual Units of DataText labels on Chart */
	INT16		nTitleJust;				/* Horizontal Justification of Title (ALIGN_RIGHT, ALIGN_CENTER, ALIGN_LEFT) */
	INT16		nSubtitleJust;			/* Horizontal Justification of SubTitle (ALIGN_RIGHT, ALIGN_CENTER, ALIGN_LEFT) */
	INT16		nFootnoteJust;			/* Horizontal Justification of Footnote (ALIGN_RIGHT, ALIGN_CENTER, ALIGN_LEFT) */
} TDG_ChartMasterInfo, * TDG_ChartMasterInfoPtr;

/* 23may00DW: Override bShowLine in UserLineRec to do more as follows: */
#define USERLINE_SHOW_LINE	 0x01	/* Display the actual line of the UserLine */
#define USERLINE_SHOW_LABEL  0x02	/* Display the actual value of the UserLine  */
#define USERLINE_LINK_XY	 0x04	/* Userline draws from x/y intersection out to X & Y-Axis (instead of full length) */

/* 22may00DW: For CENTROID Chart */
typedef
BOOLEAN16 (CALLBACK *LPFNPointCloudCallBack) (
		INT16					nIdx,									
		PREAL64					prX, 							/* X-Axis Value. */
		PREAL64					prY, 							/* Y-Axis Value */
		size_t					lClientData						/* client data value */
);

INT16 PUBLIC
SetPointCloudBack(
		GraphPtr	pGraph,
		LPFNPointCloudCallBack	lpfnPointCloudCB
);

INT16 PUBLIC
SetPointCloudCount(
		GraphPtr	pGraph,
		INT16	nCount
);

typedef enum _WidgetObjects {
	W_GRAPH, 												
	W_GAUGE, 												
	W_SLIDER 												
} WidgetObjects;


INT16 PUBLIC 
ExportWidgetPNGToMemory (GraphPtr		  graphPtr,			/* foreground graph */
		MEMPTR* 		  memAddress,		/* returns address of PNG file in memory */
		UINT32* 		  fileSize,			/* size of PNG file in memory */
		UINT16		  imgWidth,			/* image width in pixels */
		UINT16		  imgHeight,		/* image height in pixels */
		BOOLEAN16	  transBackground,  /* tranparent background? */
		LPGDPALETTE	  palettePtr,		/* external palette */
		INT16			nWidget);		/* Graph, Gauge, Slider, etc...*/



INT16 PUBLIC 
ExportWidgetPNG (GraphPtr		  graphPtr,				/* foreground graph */
		LPTSTR 		  file,					/* file to write PNG to */
		UINT16		  imgWidth,				/* image width in pixels */
		UINT16		  imgHeight,			/* image height in pixels */
		BOOLEAN16	  transBackground,		/* tranparent background? */
		LPGDPALETTE	  palettePtr,			/* external palette */
		INT16         nWidget);			/* Graph, Gauge, Slider, etc...*/


/************************************************************************
 * Series Dependent Aggregation Non-Detection Node Specific Structure
 * for use with attribute A2D_AGGREGATION_INFO_SD
 ************************************************************************/
typedef enum _SeriesPeriod {
	SERIES_DAYS,
	SERIES_WEEKS,
	SERIES_MONTHS,
	SERIES_QUARTERS,
	SERIES_YEARS
} SeriesPeriod;

typedef enum _AggregationPeriod {
	AGG_WEEKS,
	AGG_MONTHS,
	AGG_QUARTERS,
	AGG_YEARS
} AggregationPeriod;

typedef enum _AggregationFunctions {
	AGG_SUM,
	AGG_AVG,
	AGG_STD,
	AGG_STDS,
	AGG_MIN,
	AGG_MAX,
	AGG_VAR,
	AGG_VARS,
	AGG_LINEAR_REGRESSION_NATURAL_LOG_CURVE,
	AGG_LINEAR_REGRESSION,
	AGG_LINEAR_REGRESSION_NATURAL_POLYNOMIAL_CURVE,
	AGG_LINEAR_REGRESSSION_EXPONENTIAL_CURVE,
	AGG_LINEAR_REGRESSION_LOG_CURVE,
	AGG_MOVING_AVERAGE_CURVE,
	AGG_QUADRATIC_REGRESSION_CURVE,
	AGG_MODIFIED_HYPERBOLIC_CURVE,
	AGG_RATIONAL_CURVE,
	AGG_LOG_QUADRATIC_REGRESSION_CURVE,
	AGG_HYPERBOLIC_CURVE,
	AGG_MODIFIED_EXPONENTIAL_CURVE
} AggregationFunctions;

typedef struct _TDG_SerDepAggregation {
	INT16		nSeriesID;			/* SeriesID to use.  SERCON_DOALL applies this to all series. */
	BOOLEAN16	bWantAggregation;	/* TRUE = Do aggregation for this Series. */
	INT16		nSeriesPeriod;		/* Use SeriesPeriod enum above */
	INT16		nAggregationPeriod;	/* Use AggregationPeriod enum above */
	INT16		nFunction;			/* Use AggregationFunctions enum above */
	BOOLEAN16	bShowAsTrendLine;	/* FALSE = Show as proxy for the series. TRUE = Show as trendline on top of series. */
} TDG_SerDepAggregation, * TDG_SerDepAggregationPtr;

void PUBLIC InitAreas(GraphPtr pGraph); /*  copy the background area to all areas */
void PUBLIC InitFonts(GraphPtr pGraph); /*  copy the title font to all fonts */
void PUBLIC InitLines(GraphPtr pGraph); /*  copy the background line to all lines */

#if WANT_CANVAS
/************************************************************************
 * Canvas API BEGIN	 
 ************************************************************************/

typedef void * CanvasItemObjPtr;
typedef void * CanvasObjPtr;
typedef void * GaugeObjPtr;
typedef void * EqualizerObjPtr;

typedef enum _CanvasObjectEnum { 
	GRAPH_OBJECT,			/*  the object is a GraphPtr */
	GAUGE_OBJECT,			/*  the object is a gauge */
	EQUALIZER_OBJECT,		/*  the object is set of sliders */
	ILLEGAL_OBJECT
} CanvasObjectEnum;


/*  Possible mode that they Canvas Item can interact with Data */
/*  -- for future use -- */
typedef enum _DataModeEnum { 
	LINKED,			  /*  the object is linked to Excel spreadsheet */
	REFERENCE,			  /*  the object is get its data from another object */
	CALLLBACK,	      /*  the object uses call back functions to get its data */
	OWN_VALUE             /*  the object uses its own data values   */
} DataModeEnum;


/*  Create / Delete Gauges and Equalizers */
PUBLIC GaugeObjPtr AllocGaugeObjPtr( CanvasObjPtr pCanvasObj, GaugeStyle nStyle );
PUBLIC void FreeGaugeObjPtr( GaugeObjPtr );
PUBLIC EqualizerObjPtr AllocEqualizerObjPtr(CanvasObjPtr pCanvasObj);
PUBLIC void FreeEqualizerObjPtr( EqualizerObjPtr );

/*  Add/Get Graph to/from CanvasObj */
PUBLIC CanvasItemObjPtr AddGraphToCanvas( CanvasObjPtr pCanvasObj, GraphPtr pGraph );
PUBLIC INT16 GetGraphFromItem( CanvasItemObjPtr pItem, GraphPtr * ppGraph );
PUBLIC GraphPtr * GetGraphRefFromItem( CanvasItemObjPtr pItem );

/*  Add/Get Gauge to/from CanvasObj */
PUBLIC CanvasItemObjPtr AddGaugeToCanvas( CanvasObjPtr pCanvasObj, GaugeObjPtr pGauge );
PUBLIC GaugeObjPtr GetGaugeFromItem( CanvasItemObjPtr pItem );

/*  Add/Get Equalizer to/from CanvasObj */
PUBLIC CanvasItemObjPtr AddEqualizerToCanvas( CanvasObjPtr pCanvasObj, EqualizerObjPtr pEqualizer );
PUBLIC EqualizerObjPtr GetEqualizerFromItem( CanvasItemObjPtr pItem );

PUBLIC BOOLEAN16 RemoveItemFromCanvas( CanvasObjPtr pCanvasObj, CanvasItemObjPtr pItem );

/*  CanvasObj allocation */
PUBLIC CanvasObjPtr AllocCanvas();
PUBLIC void FreeCanvas( CanvasObjPtr pCanvasObj );

/*  Drawing  */
PUBLIC INT16 DrawCanvas( DrawEnvPtr pDE, CanvasObjPtr pCanvasObj );
PUBLIC INT16 DrawCanvasItem( DrawEnvPtr pDE, CanvasObjPtr pCanvasObj, CanvasItemObjPtr pItem );

/*  positionning */
PUBLIC INT16 SetCanvasItemRect( CanvasItemObjPtr pItem, Rect32 rPos );
PUBLIC INT16 GetCanvasItemRect( CanvasItemObjPtr pItem, Rect32 * prPos );

/*  other */
PUBLIC BOOLEAN16 HitTest( CanvasObjPtr pCanvasObj, CanvasItemObjPtr * ppItem, Point32 pt );
PUBLIC CanvasObjectEnum GetCanvasItemType( CanvasItemObjPtr pItem );
PUBLIC BOOLEAN16 HitOnEqualizerThumbTest( DrawEnvPtr pDE, CanvasItemObjPtr pItem, Point32 pt, INT16 * pnSlider );

/*  get the CanvasObj items */
PUBLIC CanvasItemObjPtr GetFirstCanvasItem( CanvasObjPtr pCanvasObj );
PUBLIC CanvasItemObjPtr GetNextCanvasItem( CanvasItemObjPtr pItem );
PUBLIC INT16 GetCanvasCount( CanvasObjPtr pCanvasObj );
PUBLIC CanvasItemObjPtr GetCanvasItem( CanvasObjPtr pCanvasObj, INT16 nIndex );

PUBLIC INT32 GetMemberID( CanvasItemObjPtr pItem );
PUBLIC void SetMemberID( CanvasItemObjPtr pItem, INT32 nID );

/*  interface to get/set information when the item is linked */
PUBLIC GetCanvasItemLinkedRange( CanvasItemObjPtr pItem, INT16* pStartRow, INT16* pEndRow,  INT16* pStartCol, INT16* pEndCol);
PUBLIC SetCanvasItemLinkedRange( CanvasItemObjPtr pItem, INT16 nStartRow, INT16 nEndRow,  INT16 nStartCol, INT16 EndCol);

PUBLIC DataModeEnum GetCanvasItemDataMode(CanvasItemObjPtr pItem);
PUBLIC SetCanvasItemDataMode(CanvasItemObjPtr pItem, DataModeEnum nMode);

PUBLIC LPTSTR GetCanvasItemWorkbookName(CanvasItemObjPtr pItem);
PUBLIC SetCanvasItemWorkbookName(CanvasItemObjPtr pItem, LPTSTR  szWorkbookName);

PUBLIC INT16 GetCanvasItemSheetIndex(CanvasItemObjPtr pItem);
PUBLIC void SetCanvasItemSheetIndex(CanvasItemObjPtr pItem, INT16 nIndex);

PUBLIC BOOLEAN16 GetCanvasItemLinked(CanvasItemObjPtr pItem);
PUBLIC void SetCanvasItemLinked(CanvasItemObjPtr pItem, BOOLEAN16 bLinked);



/*  Equalizer Movement */
PUBLIC BOOLEAN16 StartMovingEqualizerThumb( DrawEnvPtr pDE, EqualizerObjPtr pEqualizer, Point32 point, INT16 nSlider );
PUBLIC BOOLEAN16 IsEqualizerThumbMoving( EqualizerObjPtr pEqualizerObj );
PUBLIC BOOLEAN16 MoveEqualizerThumb( DrawEnvPtr pDE, EqualizerObjPtr pEqualizerObj, Point32 point );
PUBLIC BOOLEAN16 EndMovingEqualizerThumb( DrawEnvPtr pDE, EqualizerObjPtr pEqualizerObj, Point32 point );
PUBLIC BOOLEAN16 GetSliderThumbPos( DrawEnvPtr pDE, EqualizerObjPtr pEqualizerObj, Rect32 * prThumb );



INT16 PUBLIC
Save_TIFFCanvas (
		CanvasObjPtr	pCanvas,	/* Ptr to a canvas to save */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator,	/* Whether to add terminators after each record */
		LPTSTR		pszFileName		/* Ptr to a "C" style string filename */
);

INT16 PUBLIC
Save_TIFFCanvasPath (
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		CanvasObjPtr	pCanvas,		/* Ptr to Canvas structure to save */
		UINT32		ulRequest,		/* As defined tdi_graf.h, is passed to graph saving */
		INT16		nFileFmt,		/* FILEFMT_BINARY or FILEFMT_ASCII as defined in tags.h */
		INT16		bTerminator,	/* Whether to add terminators after each record */
		BOOLEAN16	bWantTrim);		/* Control wheter we trim excess vararray elements */	

INT16 PUBLIC
Load_TIFFCanvas (
		CanvasObjPtr pCanvas,			/* Ptr to a preallocated zeroed Graph structure */
		UINT32		ulRequest,		/* As defined tdi_graf.h */
		LPTSTR		pszFileName		/* Ptr to a "C" style string filename */
);

INT16 PUBLIC
Load_TIFFCanvasPath (
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		CanvasObjPtr pCanvas,		/* Ptr to a preallocated canvas */
		UINT32		ulRequest		/* As defined tdi_graf.h */
);

#endif /* WANT_CANVAS */

/* -----------------------------------------------------------------------  */
/*  Gauge API */
/* -----------------------------------------------------------------------  */

typedef struct _GaugeLineInst
{
	INT16	nRed;
	INT16	nGreen;
	INT16	nBlue;
	INT16	nAlpha;
	INT16	nStyle;
	INT16	nThick;
} GaugeLineInstRec, * GaugeLineInstPtr;

typedef struct {
	INT16 					nXoff;								/* virtual x offset */
	INT16 					nYoff; 								/* virtual y offset */
	UINT16 					nRed; 								/* RGB values */
	UINT16 					nGreen; 							/* RGB values */
	UINT16 					nBlue;								/* RGB values */
} GaugeDropShadowInstRec, *GaugeDropShadowInstPtr;

typedef struct _GaugeAreaInst
{
	INT16	nRed;
	INT16	nGreen;
	INT16	nBlue;
	INT16	nAlpha;
	INT16	nPattern;
	GaugeDropShadowInstRec drop;
} GaugeAreaInstRec, * GaugeAreaInstPtr;

typedef struct _GaugeFancyBoxRec {
	INT16		nShape;
	INT16 		nStyle;
} GaugeFancyBoxRec, *GaugeFancyBoxPtr;

typedef struct _GaugeFontInst
{
	INT16				nSize;                /* virtual size (0..32767) */
	INT16				nAngle;               /* axis direction in degrees */
	INT16				justH;                /* horizontal justification control */
	INT16				justV;                /* vertical justification control */
	INT16				nPitchAndFamily;      /* %%DYNAMIC%% font pitch and family */
	INT16				nCharSet;             /* %%DYNAMIC%% font character set */
	TCHAR				szFaceName[128];		/*  font name */
	GaugeDropShadowInstRec	drop;			    /* %%NEW(8)%% DropShadow values DW 10/22/92 */
	INT16				nPattern;             /* 16March98CP: added to support patterns on text */
	GaugeFancyBoxRec	FancyBoxInfo;
	GaugeAreaInstRec	FancyBoxArea;	
	GaugeLineInstRec	FancyBoxLine;
	INT16				nPointSize;				/*  Store actual PointSize */
	INT16				nExtLeading;			/*  Store external Leading factor */
	INT16				nMBCFontIndex;			/*  Store multi-byte font name index */

	INT16				nRed;					/*  Font color */
	INT16				nGreen;
	INT16				nBlue;
	INT16				nAlpha; 

} GaugeFontInstRec, * GaugeFontInstPtr;

/************************************************************************
 *  Copy of 1998July13 PPN: Advanced Number Format Structure 
 *  Since the AdvFormatStruct loading and function uses pGraph, we need 
 *  to copy this structure and save it with a different tag. 
 *  This way, it become independant of pGraph which we don't have when
 *  we save a gauge or an equalizer
 ************************************************************************/
typedef struct _GaugeAdvFormatStruct {

	AdvFormatTypes		formatType;					

	UINT 				iLZero;
	UINT 				iDigits;
	UINT 				iGrouping;
	UINT 				iNegNumber;
	_TCHAR 				szScientificSymbol[3];
	_TCHAR 				szDecimal[5];
	_TCHAR 				szThousand[5];

	UINT 				iCurrDigits;
	UINT 				iMonGrouping;
	UINT 				iNegCurr;
	UINT 				iPosCurr;
	_TCHAR 				szCurrency[5];
	_TCHAR 				szMonDecimalSep[5];
	_TCHAR 				szMonThousandSep[5];

	_TCHAR 				szTimeFormat[32];
	_TCHAR 				szDateFormat[32];

	_TCHAR 				szPrefixText[20];
	_TCHAR 				szPostfixText[20];

	_TCHAR 				szNumColor[20];
	_TCHAR 				szNegColor[20];
	_TCHAR 				szZeroColor[20];

	BOOLEAN16				bPrefixPercentSymbol;
	double				dScaleFactor;
	BOOLEAN16				bNoNegSign;
	LCID				localeID;

	_TCHAR 				szCustomFormat[256];		/*  custom format added 6-18-99/FL */

} GaugeAdvFormatStruct, *GaugeAdvFormatPtr;

#if 0 /* 04nov2001jans */
/************************************************************************
 * TDG_GRangeRec - Information for Ranges on Gauges
 ************************************************************************/
typedef struct _TDG_GRangeRec{
	BOOLEAN16		bShowRange;				/*  TRUE = Show this range on the gauge. FALSE = Do not show this range.  */
	TCHAR			sRangeLabel[64];		/*  Label for Gauge Range. */
	REAL64			fRangeStart;			/*  Start of this range on a gauge */
	REAL64			fRangeEnd;				/*  End of this range on a gauge */
	GaugeAreaInstRec RangeArea;
} TDG_GRangeRec, *TDG_GRangePtr;

/**********************************************************************************
 * TDG_GaugeInfo - Information for Gauge Objects 
 ***********************************************************************************/
typedef struct _TDG_GaugeInfo{
	GaugeLineInstRec MajorGridLine;			/*  Information about the line used to draw major gridlines */
	GaugeLineInstRec MinorGridLine;			/*  Information about the line used to draw minor gridlines */
	TDG_GRangeRec	Range[MAX_GAUGE_RANGES];	/*  Range information */
	BOOLEAN16		bShow;						/*  TRUE = Show this gauge on the chart.  FALSE = Do not show gauge. */
	INT16			nStartAngle;				/*  Start angle for circular gauges */
	INT16			nStopAngle;					/*  Stop angle for circular gauges */
	BOOLEAN16		bClockwise;					/*  Direction of numbers on the gauge. */
	enum pg_GaugeRangePosition nRangePosition;	/*  Control where the gauge range is positioned. */
	INT16			nSeriesAttach;				/*  ID of the series attached to this gauge. */
	INT16			nThickness;					/*  Thickness of the gauge (in virtual units) */
	INT16			nNeedleStyle;				/*  Needle style.  0 = Default. */
	REAL64			fGaugeMax;					/*  Maximum raw value on the gauge. */
	REAL64			fGaugeMin;					/*  Minimem raw value on the gauge. */
	REAL64			fGaugeMajorInterval;		/*  "step size" of numeric scale on the gauge */
	REAL64			fGaugeMinorInterval;		/*  "step size" of minor gridlines between each major gridline. */
	BOOLEAN16		bShowGaugeMajorInterval;	/*  TRUE = Show Major Gridlines on gauge. FALSE = No Show. */
	BOOLEAN16		bShowGaugeMinorInterval;	/*  TRUE = Show Minor Gridlines on gauge. FALSE = No Show. */
	BOOLEAN16		bAutoScaleGaugeIntervals;	/*  TRUE = Engine generates numeric scale. FALSE = use fGaugeMin, fGaugeMax. */
	BOOLEAN16		bShowGaugeLabel;			/*  TRUE = Show Gauge title string.   FALSE = No Show. */
	BOOLEAN16		bShowIntervalLabels;		/*  TRUE = Show Numeric scale value.  FALSE = No Show. */
	BOOLEAN16		bShowRangeLabels;			/*  TRUE = Show Range Labels (if ranges are on). FALSE = Don't show range labels. */
	GaugeAreaInstRec	GaugeFill;				/*  color of the gauge (behind the range or in the back) */
	GaugeLineInstRec	GaugeBorder;			/*  border line of the gauge (behind the range or in the back) */
	GaugeAreaInstRec	GaugeContainerFill;
	GaugeLineInstRec	GaugeContainerBorder;
	GaugeFancyBoxRec	GaugeContainerFancyBox;

	GaugeAdvFormatStruct	AdvStdFormatInterval;	/*  Numeric Format code for the Interval labels.  */

	GaugeAreaInstRec		ValueIndicatorFill;		/*  color of the value object (needle, thum ) */
	GaugeLineInstRec		ValueIndicatorLine;		/*  color of the border line of the value object */
	OrientationType			nOrientation;			/*  gauge orientation */
	BOOLEAN16				bRectangleLedShape;		/*  LED as rectangle or ellipse */
	BOOLEAN16				bDepthEffect;			/*  use depth effect */
	BOOLEAN16				bScaleBelowLeft;		/*  scale on the left (bottom) */
	BOOLEAN16				bShowRange;				/*  make all ranges visible */
	INT16					nLinearArrowThickness;	/*  thickness of the thumb in the linear gauge */
	BOOLEAN16				bShowTitle;
	GaugeFontInstRec		ScaleFont;				/*  information about the font used to draw the scales */
	INT16					nGaugeStyle;

	/*  Gauge Title */
	_TCHAR					szGaugeTitle[128];		/*  Label for Gauge Object. */
	GaugeFontInstRec		TitleFont;

	REAL64					fGaugeNeedleValue;		/*  current gauge value */


} TDG_GaugeInfoRec,  *TDG_GaugeInfoPtr;
#endif

#if WANT_CANVAS

PUBLIC INT16 SetGaugeInfo( CanvasObjPtr pCanvasObj, GaugeObjPtr pGauge, TDG_GaugeInfoPtr pInfo );
PUBLIC INT16 GetGaugeInfo( CanvasObjPtr pCanvasObj, GaugeObjPtr pGauge, TDG_GaugeInfoPtr pInfo );

#endif 
#if 0 /* 04nov2001jans */
#define MAX_SLIDERS 10

/************************************************************************
 * TDG_GSliderRec - Information for Slider on Equalizer
 * WARNING: THIS STRUCTURE IS CALLED IN THE LOADING/SAVING OF CANVAS.
 * LOOK AT UPDATING THE LOADER/SAVER IF YOU NEED TO CHANGE IT. 7-7-00/FL
 ************************************************************************/
typedef struct _TDG_GSliderRec{
	BOOLEAN16		bShow;				/*  TRUE = Show this range on the gauge. FALSE = Do not show this range.  */
	TCHAR			szLabel[64];		/*  Label for Gauge Range. */
	REAL64			fGaugeNeedleValue;
	REAL64			fGaugeMax;
	REAL64			fGaugeMin;
	REAL64			fGaugeMajorInterval;
	REAL64			fGaugeMinorInterval;
	BOOLEAN16		bShowGaugeMajorInterval;
	BOOLEAN16		bShowGaugeMinorInterval;
	BOOLEAN16		bAutoScaleGaugeIntervals;
	GaugeAdvFormatStruct	AdvStdFormatInterval;	/* Numeric Format code for the Interval labels.  */
} TDG_GSliderRec, *TDG_GSliderPtr;

/**********************************************************************************
 * TDG_EqualizerInfo - Information for Equalizer Objects 
 ***********************************************************************************/

/*  FUNCTION CALLED WHEN THE EQUALIZER IS BEEING MOVED */
typedef
INT16 (CALLBACK *LPFNEqualizerThumbMovedCallBack) (
		INT16		nSlider,
		REAL64		fNewValue,
		BOOLEAN16   bEndMove,
		void *		pUserData
);


typedef struct _TDG_EqualizerInfo{
	GaugeLineInstRec		MajorGridLine;
	GaugeLineInstRec		MinorGridLine;
	TDG_GSliderRec		Slider[MAX_SLIDERS];
	BOOLEAN16		bShow;						/*  TRUE = Show this gauge on the chart.  FALSE = Do not show gauge. */
	TCHAR			szGaugeLabel[128];			/*  Label for Gauge Object. */
	INT16			nThickness;
	INT16			nGaugeStyle;
	INT16			nNeedleStyle;
	BOOLEAN16		bShowGaugeLabel;
	BOOLEAN16		bShowIntervalLabels;
	GaugeFontInstRec	ScaleFont;
	GaugeFontInstRec	SliderLabelFont;
	GaugeFontInstRec	SliderTitleFont;
	GaugeAreaInstRec	GaugeFill;
	GaugeAreaInstRec	GaugeContainerFill;
	GaugeLineInstRec	GaugeBorder;
	GaugeLineInstRec	GaugeContainerBorder;
	GaugeFancyBoxRec	GaugeContainerFancyBox;
	BOOLEAN16			bShowSliderNames;
	GaugeAreaInstRec	ThumbIndicatorFill;
	GaugeLineInstRec	ThumbIndicatorLine;

} TDG_EqualizerInfoRec, *TDG_EqualizerInfoPtr;

PUBLIC INT16 SetEqualizerInfo( CanvasObjPtr pCanvasObj, EqualizerObjPtr pEqualizer, TDG_EqualizerInfoPtr pInfo );
PUBLIC INT16 GetEqualizerInfo( CanvasObjPtr pCanvasObj, EqualizerObjPtr pEqualizer, TDG_EqualizerInfoPtr pInfo );

/************************************************************************
 * Canvas API END	 
 ************************************************************************/
#endif

/************************************************************************
 *  Graph Info BEGIN (Information as what the template could be used for
 *	and what part of the graph was saved for limited file)
 ************************************************************************/

#define TEMPLATE_OUTPUT_ONSCREEN			0x00000001
#define TEMPLATE_OUTPUT_WEB					0x00000002
#define TEMPLATE_OUTPUT_REAL_TIME			0x00000004
#define TEMPLATE_OUTPUT_BLACK_WHITE			0x00000008
#define TEMPLATE_OUTPUT_COLOR_OVERHEADS		0x00000010

/*  Graph status/info accessor function (filter flags defined in tdi_graf.h) */
PUBLIC void GetGraphOutputInfo( GraphPtr pGraph, UINT32 * pnOutput );
PUBLIC void SetGraphOutputInfo( GraphPtr pGraph, UINT32 nOutput );

/*  saving filter  */

#define SAVEFILTER_SAVEALL			0x00000000	/*  special case */
#define SAVEFILTER_FONTS			0x00000001
#define SAVEFILTER_LINE				0x00000002
#define SAVEFILTER_GRAPHTYPE		0x00000004
#define SAVEFILTER_COLOR			0x00000008

/*  Graph status/info accessor function (filter flags defined in tdi_graf.h) */
PUBLIC void GetGraphSaveFilterInfo( GraphPtr pGraph, UINT32 * pnFilter );
PUBLIC void SetGraphSaveFilterInfo( GraphPtr pGraph, UINT32 nFilter );

typedef struct _TDG_GraphFileHeaderInfo {
	INT16		nRevision;			/* Revision */
	INT16 		nSubRevision;		/* SubRevision */
	INT16 		nByteOrder;			/* Byte Order, BYTEORDER_MOTOROLA or BYTEORDER_INTEL */
	INT16		bSwapBytes;			/* Should we swap bytes when loading */
	INT16 		nFileFmt;			/* File Format - FILEFMT_ASCII or FILEFMT_BINARY */
	INT16		bTerminator;		/* Do we have a terminator after each TIFF record? */
	UINT32		ulRequestFlag;		/* As defined in tdi_graf.h */
	UINT32		nOutputType;		/* Kind of output the graph can be used for (B/W, Web...)*/
	UINT32		nSavedInfo;			/* What was saved in this files (like request, but lower level) */
} TDG_GraphFileHeaderInfo, * TDG_GraphFileHeaderInfoPtr;


INT16 PUBLIC
Load_TIFFGraphHeader (
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		LPTSTR		pszFileName,		/* Ptr to a "C" style string filename */
		TDG_GraphFileHeaderInfoPtr pHeader
);

INT16 PUBLIC
Load_TIFFGraphHeaderPath (
		IOPATHPTR	pIOPathFile,	/* Ptr to the IO Path of the file as defined in smc_iorw.c */
		GraphPtr	pGraph,			/* Ptr to a preallocated zeroed Graph structure */
		TDG_GraphFileHeaderInfoPtr pHeader
);

/* 27jul00DW: Utility function for use in THEME creation. */
/* Copies attributes from the first data series (like border color, datatext format, dropshadow info) */
/* into all of the other allocated data series.   */
/* Does NOT copy foreground color/pattern/gradient info or MarkerShape or DataLine style. */
PUBLIC void PropagateFirstSeriesInfo( GraphPtr pGraph);

/************************************************************************
 *  Graph Info END 
 ************************************************************************/

/*  apply a color scheme (3li file) and call PlaceDefaultElement */
PUBLIC INT16 ApplyLook( DrawEnvPtr pDE, 
		GraphPtr pGraph, 
		LPTSTR szColorScheme, 
		INT32 nPlaceDefaultElemFlags );

/*  apply a color scheme (3li file) and call PlaceDefaultElement */
/*  a specific file is called based on the current graph chart type, */
/*  to get different look for different charts. 01-09-01/FL */
/*
	 PUBLIC INT16 ApplyLookCognos( DrawEnvPtr pDE, 
	 GraphPtr pGraph, 
	 LPTSTR szFilename, 
	 LPCTSTR szScriptPath,
	 INT32 nPlaceDefaultElemFlags );
 */
/* apply a look to multiple graph 11-08-01/FL */
PUBLIC INT16 ApplyLookToMultipleGraphs( GraphPtr * ppGraph, /*  list of graph */
		INT16 * pWidth, /*  list of graph width */
		INT16 * pHeight, /*  list of graph */
		INT16 nGraphs,	/*  number of graphs */
		LPTSTR szLookPath, /*  filename (with path) of the look file */
		INT32 nPlaceDefaultElemFlags ); /*  PDE flags to apply  */


/*  For COREL, callbacks to register RTF data used by our TIFF Loader/Saver */
typedef BOOLEAN16 (CALLBACK *GLPFNGetRTFBlock) (
		size_t					lClientData,				/* client data value */
		MEMPTR *				ppMem,						/* pointer to buffer address. */
		INT16 *					pnSize						/* pointer to size of the buffer */
);

typedef BOOLEAN16 (CALLBACK *GLPFNSetRTFBlock) (
		size_t					lClientData,			/* client data value */
		MEMPTR					pMem,						/* pointer to buffer */
		INT16 					nSize					/*  size of the buffer */
);

INT16 PUBLIC
RegisterCallback_CorelRTFLoad (
		GLPFNSetRTFBlock	My_SetCallback,
		INT32							cookie
);

INT16 PUBLIC
RegisterCallback_CorelRTFSave (
		GLPFNGetRTFBlock	My_GetCallback,
		INT32							cookie
);
/*****************************************************************************
 *  !NAME!
 *     Apply_TIFF_ColorScheme
 *
 *  DESCRIPTION: Takes the COLORSCHEME information from one chart and injects it
 *			it into another chart.
 *		
 *
 *  RETURNS:
 *		E00_OK upon success, otherwise E00_XXX error code.
 *
 *		17jul00DW: Written for use by COREL
 ******************************************************************************/

INT16 PUBLIC
Apply_TIFF_ColorScheme(
		GraphPtr      pFromGraph,		/* Graph that will provide ColorScheme */
		GraphPtr      pToGraph      /* Graph that will accept ColorScheme */
);

/************************************************************************
 * SplitMarker - SplitMarkers for Stock charts 22apr02DW
 ************************************************************************/
typedef struct _SplitMarkerRec{
	REAL64				fYValue;			/*  Y Value of line to be imaged. */
	REAL64				fXValue;			/*  X Value of line to be imaged. */
	BOOLEAN16			bShow;				/*  FALSE = OFF, TRUE = ON */
	INT16				nShape;				/*  Any Marker Shape (IsTri1, ...) */
	RGB16				rgb;				/*  Standard RGB16 structure */
	INT16				nAxisID;			/*  0 = Y1, 1 = Y2 */
	UINT16				nSplitMarkerSize;	/*  Virtual Size of Marker */
	TCHAR				szSplitMarkerLabel[256];	/*  Label for SplitMarker */
} SplitMarkerRec,  *SplitMarkerPtr;

typedef struct _SplitStructRec{
	INT16			nCount;
	UINT16			nSplitMarkerLabelMode;	/* 0 = No Label. 1 = Label above marker 2 = Label in Legend */
	SplitMarkerPtr	pSplitMarker;
} SplitStructRec,  *SplitStructPtr;

typedef enum _StoreCurveType {
	LINEAR_REGRESSION_NATURAL_LOG_CURVE,
	LINEAR_REGRESSION,
	LINEAR_REGRESSION_NATURAL_POLYNOMIAL_CURVE,
	LINEAR_REGRESSSION_EXPONENTIAL_CURVE,
	LINEAR_REGRESSION_LOG_CURVE,
	MOVING_AVERAGE_CURVE,
	QUADRATIC_REGRESSION_CURVE,
	MODIFIED_HYPERBOLIC_CURVE,
	RATIONAL_CURVE,
	LOG_QUADRATIC_REGRESSION_CURVE,
	HYPERBOLIC_CURVE,
	MODIFIED_EXPONENTIAL_CURVE,
	MAX_CURVE
} StoreCurveType;

typedef struct _XYVal {
	REAL64		x;
	REAL64		y;
	BOOLEAN16	bPlot;
} XYVal, * XYValPtr;


BOOLEAN16 PUBLIC
CalculateCurvePoints(
		GraphPtr		G,
		INT16			nSeries, /* Which Series to Query */
		INT16			nCurveType, /* What kind of Curve - use StoreCurveType above */
		XYValPtr		*pValues,	/* Return: a Pointer to an array of x/y values */
		PINT16			pCount		/* Return # of Points in list */
);

/*  Get/Set bevel relative depth and angle 10-08-00/FL */
REAL64 PUBLIC GetBevelDepth( GraphPtr pGraph );
void PUBLIC SetBevelDepth( GraphPtr pGraph, REAL64 fDepth );
REAL64 PUBLIC GetBevelAngle( GraphPtr pGraph );
void PUBLIC SetBevelAngle( GraphPtr pGraph, REAL64 fDepth );

/*  Set the flag for the data item gradient to follow the graph orientation */
void PUBLIC SetGradientOrientationFlag( GraphPtr pGraph, BOOLEAN16 bGradientFollowOrientation );
BOOLEAN16 PUBLIC GetGradientOrientationFlag( GraphPtr pGraph );

/*  Get/Set the line tickness in line charts (only when polygon lines are used) */
void PUBLIC SetLineThickness( GraphPtr pGraph, INT16 nLineThickness );
INT16 PUBLIC GetLineThickness( GraphPtr pGraph );

/*  set and get the distance between the labels and the frame */
void PUBLIC SetLabelsDistance( GraphPtr pGraph, Rect32 rLabelDistance );
Rect32 PUBLIC GetLabelsDistance( GraphPtr pGraph );

/*  distance between the graph axes and borders and the frame 10-27-00/FL */
void PUBLIC SetFrameInset( GraphPtr pGraph, Rect32 rFrameInset );
Rect32 PUBLIC GetFrameInset( GraphPtr pGraph );

/*  lock the O1 label size to be the same size as the group Halloween00/FL */
void PUBLIC LockO1LabelSize( GraphPtr pGraph, BOOLEAN16 bLock );
BOOLEAN16 PUBLIC IsO1LabelSizeLocked( GraphPtr pGraph );

/*  draws a flat frame with in 2.5D charts 01-04-01/FL */
void PUBLIC IgnoreFrameDepth( GraphPtr pGraph, BOOLEAN16 bIgnore );
BOOLEAN16 PUBLIC IsFrameDepthIgnored( GraphPtr pGraph );

/* 21feb08rg: Get/Set a few internal legend details. */
void PUBLIC SetLegendDetails(
	GraphPtr   pGraph,
	INT16      nLegendRows,
	BOOLEAN16  bLockedLegend,
	INT16      nFancyWidthInset, 
	INT16      nFancyHeightInset, 
	INT16      nLegendElementWidth, 
	INT16      nLegendElementHeight
);

void PUBLIC GetLegendDetails(
	GraphPtr   pGraph,
	INT16      *nLegendRows,
	BOOLEAN16  *bLockedLegend,
	INT16      *nFancyWidthInset, 
	INT16      *nFancyHeightInset, 
	INT16      *nLegendElementWidth, 
	INT16      *nLegendElementHeight
);

#define MAX_ALERTS	3

typedef BOOLEAN16 (CALLBACK *LPFNAlertCallBack) (
		size_t				lClientData,			/* client data value */
		INT16				nSeriesID,				/* SeriesID of the Data Marker being evaluated */
		INT16				nGroupID,				/* GroupID of the Data Marker being evaluated */
		PREAL64				pValue1,				/* Primary Data Value. */
		PREAL64				pValue2,				/* Secondary Data Value (NULL if not used) */
		PREAL64				pValue3,				/* Secondary Data Value (NULL if not used) */
		PREAL64				pValue4,				/* Secondary Data Value (NULL if not used) */
		LPTSTR				lpAlertTextBuffer,		/* Caller provided buffer for string assignment. */
		INT16				nDimBuffer,				/* Buffer length. */
		PINT16				pPlaceHorz,				/* Pointer to an INT16 which returns the Horizontal placement */
		PINT16				pPlaceVert,				/* Pointer to an INT16 which returns the Vertical placement */
		PINT16				pAlertLevel				/* Pointer to an INT16 which returns the Alert Level (0..MAX_ALERTS) to use */
);

INT16 PUBLIC
RegisterCallback_Alert (
		GraphPtr			pGraph, 
		LPFNAlertCallBack	My_AlertCallback
);

void PUBLIC
DebugQualityObjects(
		BOOLEAN16	bDebug	/* TRUE to debug QualityObjects. FALSE (Default) for normal operations. */
);

/*21nov01DW: Pie extrusion 'modes' so we can support both standard 'extrude down' and new 'extrude up-and-right' */
/*				Use with attribute API_EXTRUSION_MODE */
typedef enum _PieExtrusions {
	EXTRUDE_DOWN,    /* Default */
	EXTRUDE_UP_RIGHT
} PieExtrusions;


/* 13feb02DW: A function to set the maximum number of glyphs to image for group labels. 
	 Any glyphs beyond this mark are truncated. Default is 48. Range is 1...255. Also allows you to
	 set the string that indicates that truncation has occurred (default = '...') */
void PUBLIC
SetGroupLabelTruncation(
		GraphPtr	G,
		UINT16		nMaxGlyphs,			/* default = 48 */
		LPTSTR		szTruncationString /* default =  '...'. Can not be longer than 15 bytes */

);

/* 13feb02DW: A function to get the maximum number of glyphs to image for group labels. 
	 Any characters beyond this mark are truncated. Default is 48. Also returns the current
	 string being used as the 'indicator' that truncation has occurred (default = '...'). */
void PUBLIC
GetGroupLabelTruncation(
		GraphPtr	G,
		PUINT16		pnMaxGlyphs,			
		LPTSTR		szTruncationString /* will not be longer than 15 bytes */
);


/* 13feb02DW: A function to set the maximum number of characters to image for series labels.
	 Any glyphs beyond this mark are truncated. Default is 48. Range is 1...255. Also allows you to
	 set the string that indicates that truncation has occurred (default = '...') */
void PUBLIC
SetSeriesLabelTruncation(
		GraphPtr	G,
		UINT16		nMaxGlyphs,			/* default = 48 */
		LPTSTR		szTruncationString /* default =  '...'. Can not be longer than 15 bytes */
);

/* 13feb02DW: A function to get the maximum number of glyphs to image for series labels. 
	 Any characters beyond this mark are truncated. Default is 48.  Also returns the current
	 string being used as the 'indicator' that truncation has occurred (default = '...'). */
void PUBLIC
GetSeriesLabelTruncation(
		GraphPtr	G,
		PUINT16		pnMaxGlyphs,			
		LPTSTR		szTruncationString /* will not be longer than 15 bytes */
);

/* valid values for A2D_MOREDATA_INDICATOR attribute */
#define	MOREDATA_ABOVE	0x0001
#define	MOREDATA_BELOW	0x0002

/* 25apr02DW: Utility function to set 'permissions' related to the EXLABEL parameter of PlaceDefaultElements().
	 If the permission is FALSE, then PDE() will never choose that mode as a solution.  If the permission is TRUE, 
	 then PDE is allowed choose that mode as a solution (but may or may not do so depending upon its logic sequence) */
void PUBLIC
PDE_SetGroupLabelPermissions(
		BOOLEAN16	bAllowRotate45,
		BOOLEAN16	bAllowRotate90,
		BOOLEAN16	bAllowStagger,
		BOOLEAN16	bAllowSkip,
		BOOLEAN16	bAllowTruncation,
		BOOLEAN16	bAllowWordWrapOnRotatedLabels,
		BOOLEAN16	bAllowWordWrapOnNormallabels /* Always TRUE for now */
);

/* 9oct03DW: Extended Version of the above function that also has a flag for maintaining the Font Rotation
	 property of the X-Axis Labels so that PlaceDefaultElements won't obliterate it */
void PUBLIC
PDE_SetGroupLabelPermissionsEx(
		BOOLEAN16	bAllowRotate45,
		BOOLEAN16	bAllowRotate90,
		BOOLEAN16	bAllowStagger,
		BOOLEAN16	bAllowSkip,
		BOOLEAN16	bAllowTruncation,
		BOOLEAN16	bAllowWordWrapOnRotatedLabels,
		BOOLEAN16	bAllowWordWrapOnNormallabels, /* Always TRUE for now */
		BOOLEAN16	bMaintainExistingFontRotationProperty 
);

/* 25apr02DW: Utility function to get 'permissions' related to the EXLABEL parameter of PlaceDefaultElements()*/
void PUBLIC
PDE_GetGroupLabelPermissions(
		PBOOLEAN16	pbAllowRotate45,
		PBOOLEAN16	pbAllowRotate90,
		PBOOLEAN16	pbAllowStagger,
		PBOOLEAN16	pbAllowSkip,
		PBOOLEAN16	pbAllowTruncation,
		PBOOLEAN16	pbAllowWordWrapOnRotatedLabels,
		PBOOLEAN16	pbAllowWordWrapOnNormallabels /* Always TRUE for now */
);

/* 25apr02DW: Utility function to set 'permissions' related to the ELEGEND parameter of PlaceDefaultElements().
	 If the permission is FALSE, then PDE() will never choose that mode as a solution.  If the permission is TRUE, 
	 then PDE is allowed choose that mode as a solution (but may or may not do so depending upon its logic sequence) */
void PUBLIC
PDE_SetLegendLabelPermissions(
		BOOLEAN16	bAllowTruncation,
		BOOLEAN16	bAllowWordWrap,
		BOOLEAN16	bAllowFontSizeReduction
);

/* 25apr02DW: Utility function to get 'permissions' related to the ELEGEND parameter of PlaceDefaultElements()*/
void PUBLIC
PDE_GetLegendLabelPermissions(
		PBOOLEAN16	pbAllowTruncation,
		PBOOLEAN16	pbAllowWordWrap,
		PBOOLEAN16	pbAllowFontSizeReduction
);

/* Get the number of Series Memory Objects that currently exist in this chart*/
INT16 PUBLIC
GetSeriesMemoryObjectCount(
		GraphPtr	G
);

/* Set locale/language for GSUB processing of font objects */
int PUBLIC SetDefaultGSUBlocale(
		GraphPtr pGraph, 
		const char *locale
);

/* Set locale/language for GSUB processing of font objects via lang/country*/
int PUBLIC SetDefaultGSUBlanguage(
		GraphPtr pGraph, 
		const char *language,
		const char *country
);

/*************************************************************************************************
 * LegendIndicators - Special entries at bottom of legend that show bar/line/area 'symbol' + label 
 *	Purpose: Allows "slice & dice" legend on bar/line style legend (18oct02DW)
 *************************************************************************************************/
typedef struct _LIndicatorRec{
	INT16				nShape;					/*  Indicator Shape */
	RGB16				rgb;					/*  Standard RGB16 structure */
	TCHAR				szIndicatorLabel[256];	/*  Label for SplitMarker */
} LIndicatorRec,  *LIndicatorPtr;

typedef struct _LegendIndicatorRec{
	INT16			nCount;
	LIndicatorPtr	pLIndicatorArray;
} LegendIndicatorRec,  *LegendIndicatorPtr;

/* 09mar2003jans: from esckey.h */
/* this callback function should return non-zero value if USER BREAK is requested */
typedef INT16 ( CALLBACK *LPFNForceCancellationCallBack) (
		INT32 lContext      /* Error context information (pGraph, pDE, etc.) */
);

INT16 PUBLIC SetForceCancellationCallBack(
		LPFNForceCancellationCallBack lpfnForceCancelCB
);


/************************************************************************
 * TDG_GRangeRec - Information for Ranges on Gauges
 *	created: 23feb00DW:
 ************************************************************************/
typedef struct _TDG_GRangeRec{
	BOOLEAN16		bShowRange;				// TRUE = Show this range on the gauge. FALSE = Do not show this range. 
	REAL64			fRangeStart;			// Start of this range on a gauge
	REAL64			fRangeEnd;				// End of this range on a gauge
} TDG_GRangeRec,	*TDG_GRangePtr;

#define MAX_GAUGE_RANGES 100
/**********************************************************************************
 * TDG_GaugeInfo - Information for Gauge Objects on Balance Score Card charts
 * Use with attribute A2D_GAUGE_INFO
 ***********************************************************************************/
typedef struct _TDG_GaugeInfo{
	INT16			nStartAngle;				// Start angle for circular gauges
	INT16			nStopAngle;					// Stop angle for circular gauges
	INT16			nThickness;					// Thickness of the gauge (in virtual units)
	INT16			nGaugeStyle;				// Gauge style. 0 = CIRCULAR.
	INT16			nNeedleStyle;				// Needle style.  0 = Default.

	TDG_GRangeRec	Range[MAX_GAUGE_RANGES];	// Range information

	BOOLEAN16		bPercentageMode;			// 28nov07DW: When TRUE, values in .Range[] are percentages (0.0 to 1.0)
} TDG_GaugeInfo,  *TDG_GaugeInfoPtr;

typedef struct _UserFillRec{
	REAL64	fStartX;
	REAL64	fStopX;
	REAL64	fStartY;
	REAL64	fStopY;

	BOOLEAN16	bShow;
	BOOLEAN16	bXPercentageMode;
	BOOLEAN16	bYPercentageMode;

	AreaInstRec area;
	LineInstRec line;

} UserFillRec,  *UserFillPtr;

//22feb02DW: Remap every font in the pGraph so that it will have a font that actually is viewable in the current locale.
//ie. "Arial" will become "MS GOTHIC" on a Japanese machine.
INT16	PUBLIC
SetAllFontsDefaultMBCSIndex
(
		GraphPtr	pGraph
);

//2feb05DW: Function to perform equivalent of A_FONTNAME on ALL fonts in the chart,
// including Highlight/exception nodes.
INT16	PUBLIC
SetAllFonts_FontName
(
		GraphPtr	pGraph,
		FontNameInfoPtr pFontNameInfo
);

/* set the specific locale used for drawing (Windows only) */
/* set LCID to -1 to use default system locale */
void PUBLIC SetGraphLocale( GraphPtr pGraph, UINT32 lcid );
UINT32 PUBLIC GetGraphLocale( GraphPtr pGraph );


// stores the graph within the DE. Should not be called except in specific cases.
INT16 PUBLIC 
DESetGraphData(
		DrawEnvPtr	pDE,
		GraphPtr	pGraph
);

/* Returns TRUE if any of the curvefit or trendlines are visible on the selected nSeries */
BOOLEAN16 PUBLIC
IsTrendVisible( GraphPtr pGraph, INT16 nSeries );

//ErrorBar codes
#define kFIXED 0
#define kPERCENT 1
#define kSTDDEV 2
#define kSTDERR 3
#define kCUSTOM 4
#define kPOPULATIONSTDDEV 5
#define	kFROMDATA 6

#define kBOTHLEGS 0
#define kPLUSLEG 1
#define kMINUSLEG 2

#define kMAXHATSIZE 2000

#define kNORMAL 0
#define kSCATTER 1
#define kBUBBLE 2

#define kPARALLELAXIS	0
#define kORTHOGONALAXIS	1


/* 14jan04DW: It is extremely complicated to set the A2D_XXX errorbar flags to do the relatively simple task of just
	 showing data-driven error bars (thanks Joe Terry wherever you are :-(  ).  So I've written a little 'helper' function
	 that can be called to make this task very easy.  */
BOOLEAN16 PUBLIC
ShowDataDrivenErrorBarsOnYAxis(
		GraphPtr	pGraph,
		BOOLEAN16	bShowErrorBars,
		BOOLEAN16	bShowMedianValue
);

/* 14jan04DW: Complimentary function to the one above. Get visibility state information for error bars.   */
BOOLEAN16 PUBLIC
GetDataDrivenErrorBarsOnYAxisVisibility(
		GraphPtr	pGraph
);

/* 10feb04DW: Return TRUE if the ErrorBar Median value Circle is set to be visible */
BOOLEAN16 PUBLIC
ErrorBarMedianValueIsVisible(
		GraphPtr	pGraph
);

/*1mar04DW: Functions to set/get a Boolean "Do PlaceDefaultElements" flag. This flag does
	nothing in the engine, but IS integrated/exposed in the UI as a way for our customers to know
	that people using our UI in their application want to enable PDE behavior */
void PUBLIC
SetPDE_ExternalFlag(
		GraphPtr	pGraph,
		BOOLEAN16	bFlag
);


BOOLEAN16 PUBLIC
GetPDE_ExternalFlag(
		GraphPtr	pGraph
);


//FONT ALIAS
//PUBLIC (API) FUNCTIONS

/* This add an alias, to _user_ fontname alias table.
	 Key is (szFontName,pnFontFamily,pnCharSet), but alias can be generalized
	 by exchanging any of key fields with NULL. Aliases the better match
	 (less general) are taken before these more general.
	 Default value can be defined with key (NULL,NULL,NULL) - it matches 
	 everything (but with the lowest priority).

	 Adding _any_ alias disables buildin table.
 */
INT16 PUBLIC
HS_gloAddFontFileAlias(
		LPTSTR	szFontName,        //key: face name of the font (eg. "Arial")
		BYTE   *pnFontFamily,      //key:
		BYTE   *pnCharSet,         //key:
		LPTSTR	szNormalAlias,     //filename for regular font (eg. "arial.ttf")
		LPTSTR	szBoldAlias,       //filename for bold font (eg. "arialbd.ttf")
		LPTSTR	szItalicAlias,     //filename for italic font (eg. "ariali.ttf")
		LPTSTR	szBoldItalicAlias  //filename for bold italic font (eg. "arialbi.ttf")
);

/* This function return array of fonts, that are used in graph
	 but cannot be resolved using _user_ fontname alias table.
	 If there is no such font, 0 & NULL are returned.
	 Note #1: array returned in out_unresolved_table has to be freed with FreePtr
	 Note #2: if there is no _user_ fontname alias table, default - builtin
	 is used. But this fuction behaves as none of fonts resolve.
 */
INT16 PUBLIC
HS_GetUnresolvedFontAlias(
		GraphPtr         pGraph,
		INT32           *out_nUnresolved,     //number of returned entries
		FontNameInfoPtr *out_unresolved_table,//array of unresolvable fonts
		BOOLEAN16        useWildcardFont //sould we consider entries with NULL font name
);

/* As above but return array of fonts that do resolve */
INT16 PUBLIC
HS_GetResolvedFontAlias(
		GraphPtr         pGraph,
		INT32           *out_nResolved,
		FontNameInfoPtr *out_resolved_table,
		BOOLEAN16        useWildcardFont //sould we consider entries with NULL font name
);

/* search for an alias (or the best matching one). 
	 Will return the best matching alias.
	 First 3 params (key) are in/out, next 4 (filenames) are out only.
 */
BOOLEAN16 PUBLIC
HS_gloLookupFontFileAlias(
		const TCHAR  **szFontName,        //key: face name of the font (eg. "Arial")
		const BYTE   **pnFontFamily,      //key:
		const BYTE   **pnCharSet,         //key:
		const TCHAR  **szNormalAlias,     //filename for regular font (eg. "arial.ttf")
		const TCHAR  **szBoldAlias,       //filename for bold font (eg. "arialbd.ttf")
		const TCHAR  **szItalicAlias,     //filename for italic font (eg. "ariali.ttf")
		const TCHAR  **szBoldItalicAlias  //filename for bold italic font (eg. "arialbi.ttf")
);
// END FONT ALIAS API

/* 2sep04DW: Utility function that sees if any SeriesDependent datatext flags are
	set for any series in the chart. Returns TRUE if even one of them is set, otherwise FALSE */
BOOLEAN16 PUBLIC IsAnySeriesDependentDataTextVisible(GraphPtr pGraph);

INT16	PUBLIC
ConvertPointSizesToVirtual(
		GraphPtr	pGraph, 
		DrawEnvPtr	pDE
);

INT16	PUBLIC
ConvertVirtualSizesToPoint(
		GraphPtr	pGraph, 
		DrawEnvPtr	pDE
);

BOOLEAN16 PUBLIC	IsGanttChart( GraphPtr pGraph );
BOOLEAN16 PUBLIC	IsGaugeGraph( GraphPtr pGraph );

INT16 PUBLIC
ValueIsNewEngine(
	INT16 nValue
);

void PUBLIC
ConvertOldPieLabelToNew(
	INT16 *nValue
);

void PUBLIC
ConvertNewPieLabelToOld(
	INT16 *nValue
);

void PUBLIC setPieLabelPlacementRules(
		GraphPtr G,
	INT16     iShrinkLabelPriority,     //Number of font sizes to try  (1 to 8)
	INT16     iTruncateLabelPriority,   //Start Truncateing after Font size N, (1 to number of Fontsizes)  setting to 0 will truncate on every font size
	INT16     iMoveingLabelPriority,    //How aggessive should we be about moveing labels (0 to 5)
	BOOLEAN16 iShrinkPieRadiusPriority, //Shrink Radius before shrinking fonts (True or False)
	INT16     iLabelPaddingPriority,    //How much paddeing sould we give label 0 to 5							   
	BOOLEAN16 iSliceReorderPriority,     //Enable Slice Reordering (True or False)
	INT16     iLabelInsidePiePriority
);

/* OR flags for Set/GetGraphHiLitePermissions */
#define HL_QDR				0x0001			/* Risers and Markers */
#define HL_O1_LABELS		0x0002			/* O1 Labels */
#define HL_Y1_LABELS		0x0004			/* Y1 Labels */
#define HL_Y2_LABELS		0x0008			/* Y2 Labels */
#define HL_X1_LABELS		0x0010			/* X1 Labels */
#define HL_PIE_FEELER_LABELS	0x0020		/* Pie Feeler Labels */
#define HL_PIE_GROUP_LABELS	0x0040			/* Pie Group Labels */
#define HL_LEGEND_LABELS	0x0080			/* Legend Labels */
#define HL_Y_LABELS_3D		0x0100			/* Y axis labels on 3D chart */
#define HL_ROW_LABELS_3D	0x0200			/* Series labels on 3D chart */
#define HL_COL_LABELS_3D	0x0400			/* Group labels on 3D chart */
#define HL_DATATEXT_LABELS	0x0800			/* Group labels on 3D chart */

//Allows user to get which chart objects will be "hiliteable"
void PUBLIC GetGraphHiLitePermissions( GraphPtr G, PUINT16	pnPermissions );

//Allows user to set which chart objects will be "hiliteable"
void PUBLIC SetGraphHiLitePermissions( GraphPtr G, UINT16	nPermissions );


#ifdef __cplusplus
}
#endif

#ifdef WIN32
# ifdef _MSC_VER
#  pragma pack( pop, before_pg )
# else
#  pragma pack()
# endif /*  _MSC_VER */
#endif

#endif /* ifndef __PGSDK_H__ */

