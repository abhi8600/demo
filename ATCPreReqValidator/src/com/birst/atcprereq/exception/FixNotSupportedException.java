package com.birst.atcprereq.exception;

@SuppressWarnings("serial")
public class FixNotSupportedException extends Exception
{
	public FixNotSupportedException(String string)
	{
		super(string);
	}
}