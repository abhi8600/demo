package com.birst.atcprereq.impl;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public class OracleToolsStep extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(OracleToolsStep.class);

	public OracleToolsStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean isOracleToolsExists = true;
		try
		{
			File psToolsFile = new File(preReqConfBean.getPsToolFilePath());
			if (psToolsFile.exists())
			{
				Process killProcess = Runtime.getRuntime().exec("taskkill /F /IM psexec*");
				killProcess.waitFor();
				Process psToolsProcess = Runtime.getRuntime().exec("cmd /c " + preReqConfBean.getPsToolFilePath());
				logger.debug("Started PsTools successfully " + psToolsProcess);
				Thread.sleep(60000);
			}
			else
			{
				throw new FileNotFoundException(preReqConfBean.getPsToolFilePath());
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in validate step for Pre-req ", e1);
			isOracleToolsExists = false;
		}
		return isOracleToolsExists;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		throw new FixNotSupportedException("PsTools.bat could not be found. Please check configuration and try again ");
	}

}
