package com.birst.atcprereq.impl;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public class PentahoStartup extends ValidatorStep {

	private static Logger logger = Logger.getLogger(PentahoStartup.class);
	
	public PentahoStartup(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}
	
	@Override
	public boolean validate() {
		boolean isPentahoExists = true;
		try
		{
			File pentahoStartupBat = new File(preReqConfBean.getPentahoStartupBat());
			File pentahoShutdownBat = new File(preReqConfBean.getPentahoShutdownBat());
			if (pentahoStartupBat.exists() && pentahoShutdownBat.exists())
			{
				Process shutdownProcess = Runtime.getRuntime().exec("cmd /c \"" + preReqConfBean.getPentahoShutdownBat()+"\""); 
				shutdownProcess.waitFor();
				Thread.sleep(120000);
				Process startupProcess = Runtime.getRuntime().exec("cmd /c \"" + preReqConfBean.getPentahoStartupBat()+"\""); 
				startupProcess.waitFor();
				logger.debug("Started Pentaho successfully ");
				//Thread.sleep(60000);
			}
			else
			{
				throw new FileNotFoundException(preReqConfBean.getPsToolFilePath());
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in validate step for Pre-req ", e1);
			isPentahoExists = false;
		}
		return isPentahoExists;
	}

	@Override
	public void fix() throws FixNotSupportedException {
		throw new FixNotSupportedException("start-pentaho.bat or stop-pentaho.bat could not be found. Please check configuration and try again ");
	}

}
