package com.birst.atcprereq.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;
import com.birst.atcprereq.util.DBTypeConstants;

public class CheckATCUsersStep extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(CheckATCUsersStep.class);

	public CheckATCUsersStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean isUserExist = true;
		try
		{
			String dbType = preReqConfBean.getDbType();
			String userName = "atc@birst.com";
			if (preReqConfBean.getAtcUser()!=null && !preReqConfBean.getAtcUser().isEmpty())
			{
				userName = preReqConfBean.getAtcUser();
			}
			else
			{
				if (dbType.equalsIgnoreCase(DBTypeConstants.DBTYPE_PADB))
				{
					userName = "atc_padb@birst.com";
				}
				else if (dbType.equalsIgnoreCase(DBTypeConstants.DBTYPE_IB))
				{
					userName = "atc_ib@birst.com";
				}
				else if (dbType.equalsIgnoreCase(DBTypeConstants.DBTYPE_ORACLE))
				{
					userName = "atc@birst.com";
				}
				else if (dbType.equalsIgnoreCase(DBTypeConstants.DBTYPE_REDSHIFT))
				{
					userName = "atc_redshift@birst.com";
				}
			}
			logger.debug("ATC User to look for " + userName);
			isUserExist = checkUserExists(userName);
		}
		catch (Exception e)
		{
			isUserExist = false;
			logger.error("Exception in validate of CheckATCUsers ", e);
		}
		return isUserExist;
	}

	private boolean checkUserExists(String userName) throws Exception
	{
		boolean isUserExist = true;
		Connection con = null;
		PreparedStatement prepareStatement = null;
		ResultSet rs = null;
		try
		{
			String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			if (preReqConfBean.getAdminDbType().equalsIgnoreCase("ORACLE"))
			{
				driverClassName = "oracle.jdbc.driver.OracleDriver";
			}
			Class.forName(driverClassName);
			String connectionUrl = preReqConfBean.getAdminDbUrlMSSQL();
			if (preReqConfBean.getAdminDbType().equalsIgnoreCase("ORACLE"))
			{
				connectionUrl = preReqConfBean.getAdminDbUrlOracle();
			}
			logger.debug("ConnectionUrl for verifying user(s) " + connectionUrl);
			con = DriverManager.getConnection(connectionUrl,preReqConfBean.getAdminUserName(),preReqConfBean.getAdminPassword());
			logger.debug("Connected to the database");

			String SQL = "SELECT USERNAME FROM DBO.USERS WHERE LOWER(USERNAME)=?";
			prepareStatement = con.prepareStatement(SQL);
			prepareStatement.setString(1, userName.toLowerCase());

			rs = prepareStatement.executeQuery();

			if (!rs.next())
			{
				logger.debug("User " + userName + " not found.");
				isUserExist = false;
			}
			else
			{
				preReqConfBean.setAtcUser(userName);
			}

		}
		catch (Exception e)
		{
			logger.error("Exception in checkingUserExists ", e);
			throw e;
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
			if (prepareStatement != null)
			{
				prepareStatement.close();
			}
			if (con != null)
			{
				con.close();
			}
		}
		return isUserExist;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		throw new FixNotSupportedException("ATC User does not exist. Please create the user ");
	}

}