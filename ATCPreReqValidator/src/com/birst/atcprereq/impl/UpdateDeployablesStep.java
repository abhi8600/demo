package com.birst.atcprereq.impl;

import java.beans.DesignMode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public class UpdateDeployablesStep extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(UpdateDeployablesStep.class);

	public UpdateDeployablesStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean isValid = false;
		try
		{

		}
		catch (Exception ex)
		{
			isValid = false;
		}
		return isValid;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		try
		{
			ProcessBuilder processBuilder = new ProcessBuilder(preReqConfBean.getMergeConfigLocation(), preReqConfBean.getSavedConfigLocation(), preReqConfBean.getQaLocation(),
					preReqConfBean.getPerfEngineLocation(), preReqConfBean.getChartServiceLocation());
			logger.debug("Command " + preReqConfBean.getMergeConfigLocation() + " " + preReqConfBean.getSavedConfigLocation() + " " + preReqConfBean.getQaLocation() + " "
					+ preReqConfBean.getPerfEngineLocation() + " " + preReqConfBean.getChartServiceLocation());
			processBuilder.start();
			String sourceLocationConfFiles = preReqConfBean.getSavedConfigLocation();
			String qaDestLocation = preReqConfBean.getQaLocation() + File.separator + "bin";
			copyFile(sourceLocationConfFiles, qaDestLocation, "delspace_dynamic_queries.bat",false,true);
			copyFile(sourceLocationConfFiles, qaDestLocation, "deletespacequery.txt",false,true);
			copyFile(sourceLocationConfFiles, qaDestLocation, "removedirquery.txt",false,true);
			
			String findBugsSourceLocation = preReqConfBean.getFindBugsLocation();
			copyFile(findBugsSourceLocation, qaDestLocation, "FindBugs",true,false);
		}
		catch (Exception ex1)
		{
			logger.error("Exception in fixing of UpdateDeployables ", ex1);
			throw new FixNotSupportedException("MergeConfig failed. Please check your config and try again");
		}
	}

	private void copyFile(String sourceLocation,String destLocation,String fileToBeCopied,boolean isFolder,boolean replace) throws IOException
	{
		try
		{
			File sourceFile = new File(sourceLocation + File.separator + fileToBeCopied);
			File destFile = new File(destLocation + File.separator + fileToBeCopied);
			
			if (isFolder && ((!destFile.exists()) || replace))
			{
				FileUtils.copyDirectory(sourceFile, destFile);
			}
			else
			{
				FileUtils.copyFile(sourceFile, destFile);
			}
			
			/*if (!destFile.exists())
			{
				destFile.createNewFile();
			}

			FileChannel source = null;
			FileChannel destination = null;

			try
			{
				source = new FileInputStream(sourceFile).getChannel();
				destination = new FileOutputStream(destFile).getChannel();
				destination.transferFrom(source, 0, source.size());
			}
			finally
			{
				if (source != null)
				{
					source.close();
				}
				if (destination != null)
				{
					destination.close();
				}
			}*/
		}
		catch (Exception e)
		{
			logger.error("File " + fileToBeCopied + " could not be copied");
		}
	}

}
