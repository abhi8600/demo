package com.birst.atcprereq.impl;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;
import org.olap4j.OlapConnection;
import org.olap4j.OlapWrapper;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public class LiveAccessChecksStep extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(LiveAccessChecksStep.class);

	public LiveAccessChecksStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean isLiveAccessConnections = true;
		try
		{
			// For Hyperion - Start
			Connection hyperionConn = null;
			try
			{
				Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
				hyperionConn = DriverManager.getConnection(preReqConfBean.getLiveAcsHypConnectString(), preReqConfBean.getLiveAcsHypUsername(), preReqConfBean.getLiveAcsHypPassword());
				OlapConnection connection = (OlapConnection) hyperionConn;
				OlapWrapper wrapper = (OlapWrapper) connection;
				OlapConnection olapConnection = wrapper.unwrap(OlapConnection.class);
				// OlapWrapper wrapper = (OlapWrapper) hyperionConn;
				// OlapConnection olapConnection =
				// wrapper.unwrap(OlapConnection.class);
				logger.debug("Connection to hyperion verified before running live access" + olapConnection.getCatalog());
			}
			catch (Exception hypEx)
			{
				logger.error("Exception in connection to Hyperion ", hypEx);
				isLiveAccessConnections = false;
				preReqConfBean.setFixHyperion(true);
			}
			finally
			{
				if (hyperionConn != null && !hyperionConn.isClosed())
				{
					hyperionConn.close();
				}
			}
			// For Hyperion - End
			// For Oracle - Start
			Connection orclConnection = null;
			try
			{
				Class.forName("oracle.jdbc.OracleDriver");
				orclConnection = DriverManager.getConnection(preReqConfBean.getLiveAcsOrclConnectString(), preReqConfBean.getLiveAcsOrclUsername(), preReqConfBean.getLiveAcsOrclPassword());
				logger.debug("Connection to oracle verified before running live access");
			}
			catch (Exception oraEx)
			{
				logger.error("Exception in connection to oracle ", oraEx);
				isLiveAccessConnections = false;
				preReqConfBean.setFixOrcl(true);
			}
			finally
			{
				if (orclConnection != null && !orclConnection.isClosed())
				{
					orclConnection.close();
				}
			}
			// For Oracle - End
			// For MYSQL - Start
			Connection mySQLConnection = null;
			try
			{
				Class.forName("com.mysql.jdbc.Driver");
				mySQLConnection = DriverManager.getConnection(preReqConfBean.getLiveAcsMySQLConnectString(), preReqConfBean.getLiveAcsMySQLUsername(), preReqConfBean.getLiveAcsMySQLPassword());
				logger.debug("Connection to MYSQL verified before running live access");
			}
			catch (Exception oraEx)
			{
				logger.error("Exception in connection to mySQL ", oraEx);
				isLiveAccessConnections = false;
				preReqConfBean.setFixMYSQL(true);
			}
			finally
			{
				if (mySQLConnection != null && !mySQLConnection.isClosed())
				{
					mySQLConnection.close();
				}
			}
			// For MYSQL - End
		}
		catch (Exception e1)
		{
			logger.error("Exception in validating live access connections ", e1);
			isLiveAccessConnections = false;
		}
		return isLiveAccessConnections;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		try
		{
			if (preReqConfBean.isFixOrcl())
			{
				Process startOrcl = Runtime.getRuntime().exec("net start " + preReqConfBean.getLiveAcsOrclServiceName());
				startOrcl.waitFor();
			}
		}
		catch (Exception ex1)
		{
			logger.debug("Exception in LiveAccessChecksStep ", ex1);
			throw new FixNotSupportedException("Fix for checking live access steps failed.Oracle could not be started ");
		}
	}

}
