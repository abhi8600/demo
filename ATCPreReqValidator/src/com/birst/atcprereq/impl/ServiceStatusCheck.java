package com.birst.atcprereq.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public class ServiceStatusCheck extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(ServiceStatusCheck.class);

	public ServiceStatusCheck(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean isServiceRunning = false;
		try
		{
			logger.info("Query service status for "+preReqConfBean.getServiceName());
			String serviceStatus = getServiceStatus("sc query " + preReqConfBean.getServiceName());
			if (serviceStatus != null && serviceStatus.length() > 0)
			{
				String STATE_PREFIX = "STATE              : ";
				int ix = serviceStatus.indexOf(STATE_PREFIX);
				if (ix >= 0)
				{
					// compare status number to one of the states
					String stateStr = serviceStatus.substring(ix + STATE_PREFIX.length(), ix + STATE_PREFIX.length() + 1);
					int state = Integer.parseInt(stateStr);
					if (state == 4)
					{
						isServiceRunning = true;
					}
				}
			}
			logger.debug("Service running "+isServiceRunning);
		}
		catch (Exception e1)
		{
			logger.error("Exception in validate step for Pre-req ", e1);
			isServiceRunning = false;
		}
		return isServiceRunning;
	}

	private String getServiceStatus(String command) throws Exception
	{
		String serviceStatus = "";
		String cmdOutput = "";
		Process process = Runtime.getRuntime().exec(command);

		// Get input streams
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		//BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		// Read command standard output
		logger.debug("Standard output: ");
		while ((cmdOutput = stdInput.readLine()) != null)
		{
			logger.debug(cmdOutput);
			if (cmdOutput.indexOf("STATE") != -1)
			{
				serviceStatus = cmdOutput;
			}
		}

		/*// Read command errors
		System.out.println("Standard error: ");
		while ((cmdOutput = stdError.readLine()) != null)
		{
			System.out.println(cmdOutput);
		}

		System.out.println("Before returning " + cmdOutput);*/
		return serviceStatus;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		try
		{
			logger.debug("Attempting to start service "+preReqConfBean.getServiceName()+ " on local machine");
			logger.debug("net start " + preReqConfBean.getServiceName());
			Process process = Runtime.getRuntime().exec("net start " + preReqConfBean.getServiceName());
			int serviceStart = process.waitFor();
			if (serviceStart != 0)
			{
				throw new Exception();
			}
		}
		catch (Exception ex)
		{
			throw new FixNotSupportedException("Service could not be started ");
		}
	}

	public static void main(String args[])
	{
		ServiceStatusCheck sc = new ServiceStatusCheck(null);
		try
		{
			sc.validate();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
