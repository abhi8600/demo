package com.birst.atcprereq.impl;

import org.apache.log4j.Logger;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class MongoDBConnectionStep extends ValidatorStep
{
	private static Logger logger = Logger.getLogger(MongoDBConnectionStep.class);

	public MongoDBConnectionStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean mongoConnectionCheck = true;
		Mongo mongo = null;
		try
		{
			String mongoHost = preReqConfBean.getMongoHostName();
			int mongoPort = preReqConfBean.getMongoPort();

			mongo = new Mongo(mongoHost, mongoPort);

			DB localDB = mongo.getDB("local");

			DBObject ping = new BasicDBObject("ping", "1");
			localDB.command(ping);

			logger.debug("Connected to MongoDB at " + mongoHost + ":" + mongoPort + " to database " + localDB.getName());

			// mongo.close();

			// List<String> dbNames = mongo.getDatabaseNames();

			// logger.debug("Connected to MongoDB. DB Names " + dbNames);

			// MongoClient mongoClient = new MongoClient("localhost", 27017);
		}
		catch (Exception e)
		{
			logger.error("Exception in connection to MongoDB instance", e);
			mongoConnectionCheck = false;
		}
		finally
		{
			try
			{
				if (mongo != null)
				{
					mongo.close();
				}
			}
			catch (Exception e)
			{
				logger.error("Exception in trying to close mongo Connection " + e);
			}
		}
		return mongoConnectionCheck;
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		throw new FixNotSupportedException("Could not connect to MongoDB instance. Please check host name and port number configuration");
	}

}
