package com.birst.atcprereq.impl;

import java.io.IOException;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.birst.atcprereq.ValidatorStep;
import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;
import com.birst.atcprereq.util.SoapClientUtil;

public class LoginAndCreateSpaceStep extends ValidatorStep
{
	private static final Logger logger = Logger.getLogger(LoginAndCreateSpaceStep.class);

	public LoginAndCreateSpaceStep(PreReqConfBean preReqConfBean)
	{
		super(preReqConfBean);
	}

	@Override
	public boolean validate()
	{
		boolean loginCreateSpace = true;
		SoapClientUtil soapClientUtil = null;
		try
		{
			soapClientUtil = new SoapClientUtil();
			String webServiceUrl = preReqConfBean.getWebServiceUrl();
			String soapEndPoint = "CommandWebService.asmx";
			String methodPayLoad = readFile("config/loginpayload.xml", Charset.defaultCharset());
			logger.debug("methodPayLoad==>" + methodPayLoad);
			methodPayLoad = methodPayLoad.replace("V${Username}", preReqConfBean.getAtcUser());
			methodPayLoad = methodPayLoad.replace("V${Password}", "birst01");
			logger.debug("methodPayLoad modified==>" + methodPayLoad);
			String methodName = "Login";

			String[] httpResponse = soapClientUtil.sendRequest(webServiceUrl, soapEndPoint, true, null, "Soap", methodPayLoad, methodName);
			logger.debug("Result::" + httpResponse[0]);
			logger.debug("Response::" + httpResponse[1]);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(httpResponse[1])));
			doc.getDocumentElement().normalize();

			logger.debug("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("LoginResult");

			String loginToken = null;

			if (nList != null && nList.getLength() > 0)
			{
				Node loginResultNode = nList.item(0);
				loginToken = loginResultNode.getTextContent();
				logger.debug("Your login token is " + loginToken);
			}
			else
			{
				logger.error("Login failed for user " + preReqConfBean.getAtcUser());
				throw new Exception("Login Failed");
			}

			Date today = new Date();
			String newSpaceName = "ATCPreReq_" + today.getTime();

			methodPayLoad = readFile("config/createNewSpacpayload.xml", Charset.defaultCharset());
			logger.debug("methodPayLoad for createSpace==>" + methodPayLoad);
			methodPayLoad = methodPayLoad.replace("V${Token}", loginToken);
			methodPayLoad = methodPayLoad.replace("V${SpaceName}", newSpaceName);
			logger.debug("methodPayLoad modified for createSpace==>" + methodPayLoad);
			methodName = "createNewSpace";

			httpResponse = soapClientUtil.sendRequest(webServiceUrl, soapEndPoint, true, null, "Soap", methodPayLoad, methodName);
			logger.debug("Result::" + httpResponse[0]);
			logger.debug("Response::" + httpResponse[1]);

			doc = dBuilder.parse(new InputSource(new StringReader(httpResponse[1])));
			doc.getDocumentElement().normalize();

			logger.debug("Root element for space creation:" + doc.getDocumentElement().getNodeName());

			nList = doc.getElementsByTagName("createNewSpaceResult");

			String spaceID = null;

			if (nList != null && nList.getLength() > 0)
			{
				Node createSpaceNode = nList.item(0);
				spaceID = createSpaceNode.getTextContent();
				logger.debug("Your new created spaceID is " + spaceID);
			}
			else
			{
				logger.error("Space Creation Failed");
				throw new Exception("Space Creation Failed ");
			}

		}
		catch (Exception e)
		{
			loginCreateSpace = false;
			logger.error("Exception in validate of LoginAndCreateSpace ", e);
		}
		return loginCreateSpace;
	}

	private String readFile(String path, Charset encoding) throws IOException
	{
		try
		{
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return encoding.decode(ByteBuffer.wrap(encoded)).toString();
		}
		catch (Exception e)
		{
			logger.error("Exception in reading file ");
		}
		return "";
	}

	@Override
	public void fix() throws FixNotSupportedException
	{
		throw new FixNotSupportedException("Login and/or create new space failed. Please check configuration and try again");
	}

}
