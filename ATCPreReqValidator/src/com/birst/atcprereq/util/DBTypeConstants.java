package com.birst.atcprereq.util;

public interface DBTypeConstants
{
	public static final String DBTYPE_MSSQL = "MSSQL";
	public static final String DBTYPE_IB = "IB";
	public static final String DBTYPE_PADB = "PADB";
	public static final String DBTYPE_ORACLE = "ORACLE";
	public static final String DBTYPE_REDSHIFT = "REDSHIFT";
}
