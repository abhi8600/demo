package com.birst.atcprereq.util;

import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;

public class SoapClientUtil
{
	private static final Logger logger = Logger.getLogger(SoapClientUtil.class);

	@SuppressWarnings("deprecation")
	public String[] sendRequest(String url, String soapEndPoint, boolean isPostRequest, Map<String, String> parametersMap, String requestType, String soapPayLoad, String soapMethodName)
	{
		String[] requestPostResult = new String[2];
		String strURL = url + "/" + soapEndPoint;
		logger.debug("strURL::" + strURL);
		PostMethod post = new PostMethod(strURL);
		post.setRequestHeader("Host", url.replace("http://", ""));
		post.setRequestHeader("User-Agent", "BirstMobile");

		if ("SOAP".equalsIgnoreCase(requestType))
		{
			RequestEntity entity = new StringRequestEntity(soapPayLoad);
			post.setRequestEntity(entity);
			post.setRequestHeader("SOAPAction", "http://www.birst.com/" + soapMethodName);
			post.setRequestHeader("Content-Type", "text/xml;charset=UTF-8");
		}
		else
		{

			if (!parametersMap.isEmpty())
			{
				Iterator it = parametersMap.entrySet().iterator();
				while (it.hasNext())
				{
					Map.Entry pairs = (Map.Entry) it.next();
					post.addParameter(pairs.getKey().toString(), pairs.getValue().toString());
					it.remove();
				}
			}

			post.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		}

		HttpClient httpclient = new HttpClient();

		// Allow Fiddler2 to capture Eclipse Traffic

		String response = "";
		try
		{
			int result = httpclient.executeMethod(post);
			logger.debug(soapMethodName + "Request PayLoad : " + soapPayLoad);
			logger.debug(soapMethodName + "Response status code: " + result);
			response = post.getResponseBodyAsString();
			logger.debug(soapMethodName + "Response body: " + response);
			requestPostResult[0] = String.valueOf(result);
			requestPostResult[1] = response;

		}
		catch (Exception ex)
		{
			logger.error(ex);
		}
		finally
		{
			post.releaseConnection();
		}
		return requestPostResult;
	}
}
