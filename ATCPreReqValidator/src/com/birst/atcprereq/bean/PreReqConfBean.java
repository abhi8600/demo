package com.birst.atcprereq.bean;

public class PreReqConfBean
{

	private String adminDbType;
	private String adminDbUrlMSSQL;
	private String adminDbUrlOracle;
	private String atcUser;
	private String chartServiceLocation;
	private String dbType;
	private boolean fixHyperion;
	private boolean liveAccessCheck;
	private String mergeConfigLocation;
	private String mongoHostName;
	private int mongoPort;
	private String perfEngineLocation;
	private String qaLocation;
	private String savedConfigLocation;
	private String webServiceUrl;
	private String liveAcsHypConnectString;
	private String liveAcsHypUsername;
	private String liveAcsHypPassword;
	private String liveAcsOrclConnectString;
	private String liveAcsOrclUsername;
	private String liveAcsOrclPassword;
	private String liveAcsMySQLConnectString;
	private String liveAcsMySQLUsername;
	private String liveAcsMySQLPassword;
	private boolean fixOrcl;
	private boolean fixMYSQL;
	private String psToolFilePath;
	private String liveAcsOrclServiceName;
	private String emailToList;
	private String emailSubPrefix;
	private String emailFrom;
	private String emailServer;
	private String hostName;
	private String serviceName;
	private String adminUserName;
	private String adminPassword;
	private String findBugsLocation;
	private String serviceNames;
	private String pentahoStartupBat;
	private String pentahoShutdownBat;
	private boolean startPentaho;

	public String getServiceNames() {
		return serviceNames;
	}

	public void setServiceNames(String serviceNames) {
		this.serviceNames = serviceNames;
	}

	public String getFindBugsLocation() {
		return findBugsLocation;
	}

	public void setFindBugsLocation(String findBugsLocation) {
		this.findBugsLocation = findBugsLocation;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public String getAdminDbType()
	{
		return adminDbType;
	}

	public String getAdminDbUrlMSSQL()
	{
		return adminDbUrlMSSQL;
	}

	public String getAdminDbUrlOracle()
	{
		return adminDbUrlOracle;
	}

	public String getAtcUser()
	{
		return atcUser;
	}

	public String getChartServiceLocation()
	{
		return chartServiceLocation;
	}

	public String getDbType()
	{
		return dbType;
	}

	public String getMergeConfigLocation()
	{
		return mergeConfigLocation;
	}

	public String getMongoHostName()
	{
		return mongoHostName;
	}

	public int getMongoPort()
	{
		return mongoPort;
	}

	public String getPerfEngineLocation()
	{
		return perfEngineLocation;
	}

	public String getQaLocation()
	{
		return qaLocation;
	}

	public String getSavedConfigLocation()
	{
		return savedConfigLocation;
	}

	public String getWebServiceUrl()
	{
		return webServiceUrl;
	}

	public boolean isFixHyperion()
	{
		return fixHyperion;
	}

	public boolean isLiveAccessCheck()
	{
		return liveAccessCheck;
	}

	public void setAdminDbType(String adminDbType)
	{
		this.adminDbType = adminDbType;
	}

	public void setAdminDbUrlMSSQL(String adminDbUrlMSSQL)
	{
		this.adminDbUrlMSSQL = adminDbUrlMSSQL;
	}

	public void setAdminDbUrlOracle(String adminDbUrlOracle)
	{
		this.adminDbUrlOracle = adminDbUrlOracle;
	}

	public void setAtcUser(String atcUser)
	{
		this.atcUser = atcUser;
	}

	public void setChartServiceLocation(String chartServiceLocation)
	{
		this.chartServiceLocation = chartServiceLocation;
	}

	public void setDbType(String dbType)
	{
		this.dbType = dbType;
	}

	public void setFixHyperion(boolean fixHyperion)
	{
		this.fixHyperion = fixHyperion;
	}

	public void setLiveAccessCheck(boolean liveAccessCheck)
	{
		this.liveAccessCheck = liveAccessCheck;
	}

	public void setMergeConfigLocation(String mergeConfigLocation)
	{
		this.mergeConfigLocation = mergeConfigLocation;
	}

	public void setMongoHostName(String mongoHostName)
	{
		this.mongoHostName = mongoHostName;
	}

	public void setMongoPort(int mongoPort)
	{
		this.mongoPort = mongoPort;
	}

	public void setPerfEngineLocation(String perfEngineLocation)
	{
		this.perfEngineLocation = perfEngineLocation;
	}

	public void setQaLocation(String qaLocation)
	{
		this.qaLocation = qaLocation;
	}

	public void setSavedConfigLocation(String savedConfigLocation)
	{
		this.savedConfigLocation = savedConfigLocation;
	}

	public void setWebServiceUrl(String webServiceUrl)
	{
		this.webServiceUrl = webServiceUrl;
	}

	public String getLiveAcsHypConnectString()
	{
		return liveAcsHypConnectString;
	}

	public void setLiveAcsHypConnectString(String liveAcsHypConnectString)
	{
		this.liveAcsHypConnectString = liveAcsHypConnectString;
	}

	public String getLiveAcsHypUsername()
	{
		return liveAcsHypUsername;
	}

	public void setLiveAcsHypUsername(String liveAcsHypUsername)
	{
		this.liveAcsHypUsername = liveAcsHypUsername;
	}

	public String getLiveAcsHypPassword()
	{
		return liveAcsHypPassword;
	}

	public void setLiveAcsHypPassword(String liveAcsHypPassword)
	{
		this.liveAcsHypPassword = liveAcsHypPassword;
	}

	public String getLiveAcsOrclConnectString()
	{
		return liveAcsOrclConnectString;
	}

	public void setLiveAcsOrclConnectString(String liveAcsOrclConnectString)
	{
		this.liveAcsOrclConnectString = liveAcsOrclConnectString;
	}

	public String getLiveAcsOrclUsername()
	{
		return liveAcsOrclUsername;
	}

	public void setLiveAcsOrclUsername(String liveAcsOrclUsername)
	{
		this.liveAcsOrclUsername = liveAcsOrclUsername;
	}

	public String getLiveAcsOrclPassword()
	{
		return liveAcsOrclPassword;
	}

	public void setLiveAcsOrclPassword(String liveAcsOrclPassword)
	{
		this.liveAcsOrclPassword = liveAcsOrclPassword;
	}

	public boolean isFixOrcl()
	{
		return fixOrcl;
	}

	public void setFixOrcl(boolean fixOrcl)
	{
		this.fixOrcl = fixOrcl;
	}

	public String getLiveAcsMySQLConnectString()
	{
		return liveAcsMySQLConnectString;
	}

	public void setLiveAcsMySQLConnectString(String liveAcsMySQLConnectString)
	{
		this.liveAcsMySQLConnectString = liveAcsMySQLConnectString;
	}

	public String getLiveAcsMySQLUsername()
	{
		return liveAcsMySQLUsername;
	}

	public void setLiveAcsMySQLUsername(String liveAcsMySQLUsername)
	{
		this.liveAcsMySQLUsername = liveAcsMySQLUsername;
	}

	public String getLiveAcsMySQLPassword()
	{
		return liveAcsMySQLPassword;
	}

	public void setLiveAcsMySQLPassword(String liveAcsMySQLPassword)
	{
		this.liveAcsMySQLPassword = liveAcsMySQLPassword;
	}

	public boolean isFixMYSQL()
	{
		return fixMYSQL;
	}

	public void setFixMYSQL(boolean fixMYSQL)
	{
		this.fixMYSQL = fixMYSQL;
	}

	public String getPsToolFilePath()
	{
		return psToolFilePath;
	}

	public void setPsToolFilePath(String psToolFilePath)
	{
		this.psToolFilePath = psToolFilePath;
	}

	public String getLiveAcsOrclServiceName()
	{
		return liveAcsOrclServiceName;
	}

	public void setLiveAcsOrclServiceName(String liveAcsOrclServiceName)
	{
		this.liveAcsOrclServiceName = liveAcsOrclServiceName;
	}

	public String getEmailToList()
	{
		return emailToList;
	}

	public void setEmailToList(String emailToList)
	{
		this.emailToList = emailToList;
	}

	public String getEmailSubPrefix()
	{
		return emailSubPrefix;
	}

	public void setEmailSubPrefix(String emailSubPrefix)
	{
		this.emailSubPrefix = emailSubPrefix;
	}

	public String getEmailFrom()
	{
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom)
	{
		this.emailFrom = emailFrom;
	}

	public String getEmailServer()
	{
		return emailServer;
	}

	public void setEmailServer(String emailServer)
	{
		this.emailServer = emailServer;
	}

	public String getHostName()
	{
		return hostName;
	}

	public void setHostName(String hostName)
	{
		this.hostName = hostName;
	}

	public String getServiceName()
	{
		return serviceName;
	}

	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}

	public String getPentahoStartupBat() 
	{
		return pentahoStartupBat;
	}

	public void setPentahoStartupBat(String pentahoStartupBat) 
	{
		this.pentahoStartupBat = pentahoStartupBat;
	}

	public String getPentahoShutdownBat() 
	{
		return pentahoShutdownBat;
	}

	public void setPentahoShutdownBat(String pentahoShutdownBat) 
	{
		this.pentahoShutdownBat = pentahoShutdownBat;
	}

	public boolean isStartPentaho() 
	{
		return startPentaho;
	}

	public void setStartPentaho(boolean startPentaho) 
	{
		this.startPentaho = startPentaho;
	}		
}