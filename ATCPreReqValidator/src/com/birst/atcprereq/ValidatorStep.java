package com.birst.atcprereq;

import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;

public abstract class ValidatorStep
{
	protected PreReqConfBean preReqConfBean;

	public PreReqConfBean getPreReqConfBean()
	{
		return preReqConfBean;
	}

	public void setPreReqConfBean(PreReqConfBean preReqConfBean)
	{
		this.preReqConfBean = preReqConfBean;
	}

	public ValidatorStep(PreReqConfBean preReqConfBean)
	{
		this.preReqConfBean = preReqConfBean;
	}

	public ValidatorStep()
	{

	}

	public abstract boolean validate();

	public abstract void fix() throws FixNotSupportedException;
}