package com.birst.atcprereq;

import java.io.FileInputStream;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.birst.atcprereq.bean.PreReqConfBean;
import com.birst.atcprereq.exception.FixNotSupportedException;
import com.birst.atcprereq.impl.CheckATCUsersStep;
import com.birst.atcprereq.impl.LiveAccessChecksStep;
import com.birst.atcprereq.impl.LoginAndCreateSpaceStep;
import com.birst.atcprereq.impl.MongoDBConnectionStep;
import com.birst.atcprereq.impl.OracleToolsStep;
import com.birst.atcprereq.impl.PentahoStartup;
import com.birst.atcprereq.impl.ServiceStatusCheck;
import com.birst.atcprereq.impl.UpdateDeployablesStep;

public class StartATCPreReqCheck
{
	private static PreReqConfBean preReqConfBean;
	private static Logger logger = Logger.getLogger(StartATCPreReqCheck.class);

	private static void loadConfiguration() throws Exception
	{
		logger.debug("Initializing Configuration");
		preReqConfBean = new PreReqConfBean();
		String filePathForConfig = "config/ATCPreReqSettings.config";
		Properties props = new Properties();
		props.load(new FileInputStream(filePathForConfig));

		preReqConfBean.setSavedConfigLocation(props.getProperty("SAVEDCONFIG_LOC"));
		preReqConfBean.setQaLocation(props.getProperty("QA_DEPLOYMENT_LOC"));
		preReqConfBean.setPerfEngineLocation(props.getProperty("PERFENGINE_DEPLOYMENT_LOC"));
		preReqConfBean.setChartServiceLocation(props.getProperty("CHARTSERVICE_DEPLOYMENT_LOC"));
		preReqConfBean.setMergeConfigLocation(props.getProperty("MERGECONFIG_LOC"));
		preReqConfBean.setDbType(props.getProperty("DBTYPE"));
		preReqConfBean.setAdminDbType(props.getProperty("ADMINDBTYPE"));
		preReqConfBean.setAdminDbUrlMSSQL(props.getProperty("ADMINDBURLMSSQL"));
		preReqConfBean.setAdminDbUrlOracle(props.getProperty("ADMINDBURLORACLE"));
		preReqConfBean.setWebServiceUrl(props.getProperty("WEBSERVICEURL"));
		preReqConfBean.setMongoHostName(props.getProperty("MONGOHOST"));
		preReqConfBean.setMongoPort(Integer.parseInt(props.getProperty("MONGOPORT")));
		preReqConfBean.setLiveAccessCheck(Boolean.valueOf(props.getProperty("LIVEACCESSFLAG")));
		preReqConfBean.setLiveAcsHypConnectString(props.getProperty("LIVEACCESS_HYPERION_CONNECT"));
		preReqConfBean.setLiveAcsHypUsername(props.getProperty("LIVEACCESS_HYPERION_USERNAME"));
		preReqConfBean.setLiveAcsHypPassword(props.getProperty("LIVEACCESS_HYPERION_PASSWORD"));
		preReqConfBean.setLiveAcsOrclConnectString(props.getProperty("LIVEACCESS_ORACLE_CONNECT"));
		preReqConfBean.setLiveAcsOrclUsername(props.getProperty("LIVEACCESS_ORACLE_USERNAME"));
		preReqConfBean.setLiveAcsOrclPassword(props.getProperty("LIVEACCESS_ORACLE_PASSWORD"));
		preReqConfBean.setLiveAcsMySQLConnectString(props.getProperty("LIVEACCESS_MYSQL_CONNECT"));
		preReqConfBean.setLiveAcsMySQLUsername(props.getProperty("LIVEACCESS_MYSQL_USERNAME"));
		preReqConfBean.setLiveAcsMySQLPassword(props.getProperty("LIVEACCESS_MYSQL_PASSWORD"));
		preReqConfBean.setPsToolFilePath(props.getProperty("ORACLE_PSTOOL_FILEPATH"));
		preReqConfBean.setLiveAcsOrclServiceName(props.getProperty("LIVEACCESS_ORACLE_SVCNAME"));
		preReqConfBean.setEmailToList(props.getProperty("EMAIL_TO_LIST"));
		preReqConfBean.setEmailSubPrefix(props.getProperty("EMAIL_SUB_PREF"));
		preReqConfBean.setEmailFrom(props.getProperty("EMAIL_FROM"));
		preReqConfBean.setEmailServer(props.getProperty("EMAIL_SERVER"));
		preReqConfBean.setHostName(props.getProperty("HOST_NAME"));
		preReqConfBean.setServiceNames(props.getProperty("SERVICE_NAMES"));
		preReqConfBean.setAdminUserName(props.getProperty("ADMINUSERNAME"));
		preReqConfBean.setAdminPassword(props.getProperty("ADMINPASSWORD"));
		preReqConfBean.setFindBugsLocation(props.getProperty("FINDBUGS_LOCATION"));
		preReqConfBean.setAtcUser(props.getProperty("ATC_USER"));
		preReqConfBean.setPentahoStartupBat(props.getProperty("PENTAHO_STARTUP_BAT_PATH"));
		preReqConfBean.setPentahoShutdownBat(props.getProperty("PENTAHO_SHUTDOWN_BAT_PATH"));
		preReqConfBean.setStartPentaho(Boolean.valueOf(props.getProperty("PENTAHO_START_FLAG")));
	}

	private static void displayHelp() throws Exception
	{
		StringBuffer lSbHelpText = new StringBuffer("");
		lSbHelpText.append("StartATCPreReqCheck : This utility will check for the basic ATC pre-requisites in the following order\n");
		lSbHelpText.append("1. Check if scheduler service is running and if not start the service \n");
		lSbHelpText.append("2. Close previous instances and start pstools for oracle (Only applicable when admin DB is oracle)\n");
		lSbHelpText.append("3. Update deployables in QA\\bin folder\n");
		lSbHelpText.append("4. Check whether ATC User exists or not in the database specified in config location\n");
		lSbHelpText.append("5. Check login and create space functions by invoking the respective webservices\n");
		lSbHelpText.append("6. Check connection to local mongoDB server\n");
		lSbHelpText.append("7. Check all Live access connections (Only applicable when live access flag is enabled)\n");
		lSbHelpText.append("8. Stop and start Pentaho server (if applicable)\n");
		lSbHelpText.append("If any of the above steps fail, the subsequent steps will not be carried out and utility will exit with error message");
		System.out.println(lSbHelpText);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		StringBuffer lSbReason = new StringBuffer("");
		try
		{
			displayHelp();
			loadConfiguration();

			if (preReqConfBean.getServiceNames()!=null)
			{
				String serviceNames[] = preReqConfBean.getServiceNames().split(",");
				for (String strServiceName : serviceNames)
				{
					preReqConfBean.setServiceName(strServiceName);
					ValidatorStep validationStep01 = new ServiceStatusCheck(preReqConfBean);
					if (!validationStep01.validate())
					{
						logger.debug("Validation of step check for service failed. Trying to fix");
						validationStep01.fix();
					}
				}
			}
			
			if (preReqConfBean.getAdminDbType().equalsIgnoreCase("ORACLE"))
			{
				ValidatorStep validationStep02 = new OracleToolsStep(preReqConfBean);
				if (!validationStep02.validate())
				{
					logger.debug("Validation of step startup Oracle Pstools failed. Trying to fix");
					validationStep02.fix();
				}
			}

			ValidatorStep validationStep03 = new UpdateDeployablesStep(preReqConfBean);
			if (!validationStep03.validate())
			{
				logger.debug("Validation of step update deployables failed. Trying to fix");
				validationStep03.fix();
			}

			ValidatorStep validationStep04 = new CheckATCUsersStep(preReqConfBean);
			if (!validationStep04.validate())
			{
				logger.debug("Validation of step check for users failed. Trying to fix");
				validationStep04.fix();
			}

			ValidatorStep validationStep05 = new LoginAndCreateSpaceStep(preReqConfBean);
			if (!validationStep05.validate())
			{
				logger.debug("Validation of step login and create space failed. Trying to fix");
				validationStep05.fix();
			}

			ValidatorStep validationStep06 = new MongoDBConnectionStep(preReqConfBean);
			if (!validationStep06.validate())
			{
				logger.debug("Validation of step connection to mongoDB instance failed. Trying to fix");
				validationStep06.fix();
			}

			if (preReqConfBean.isLiveAccessCheck())
			{
				ValidatorStep validationStep07 = new LiveAccessChecksStep(preReqConfBean);
				if (!validationStep07.validate())
				{
					logger.debug("Validation of step verify live access connections failed. Trying to fix");
					validationStep07.fix();
				}
			}
			
			if (preReqConfBean.isStartPentaho())
			{
				ValidatorStep validationStep08 = new PentahoStartup(preReqConfBean);
				if (!validationStep08.validate())
				{
					logger.debug("Validation of stop and start Pentaho failed. Trying to fix");
					validationStep08.fix();
				}
			}

			logger.debug("All steps of ATC pre-requisites configuration steps completed. ATC can now be run ");
			sendEmail(true, "");
			System.exit(0);
		}
		catch (FixNotSupportedException fse)
		{
			logger.error("Pre-Requisite check could not be completed due to the following reason " + fse);
			lSbReason.append(fse.getMessage());
			sendEmail(false, lSbReason.toString());
			System.exit(1);
		}
		catch (Exception ex)
		{
			logger.error("Exception occured in pre-requisite checks ",ex);
		}
	}

	private static void sendEmail(boolean isATCPreReqSuccess, String failReason)
	{
		try
		{
			StringBuffer emailBody = new StringBuffer("");
			String from = preReqConfBean.getEmailFrom();
			String to = preReqConfBean.getEmailToList();
			String subject = preReqConfBean.getEmailSubPrefix() + " - " + (isATCPreReqSuccess ? "Success" : "Failed");

			emailBody.append("Hi,<br><br>");
			emailBody.append("ATC pre-requisite check has been completed on " + preReqConfBean.getHostName() + ".<br><br>");

			if (isATCPreReqSuccess)
			{
				emailBody.append("ATC will now begin on this machine");
			}
			else
			{
				emailBody.append("ATC pre-requisite check failed due to the following reason : " + failReason);
			}

			logger.debug("Email body:" + emailBody);

			Session mailSession = null;
			Properties props = new Properties();
			props.put("mail.smtp.host", "192.168.85.11");
			//props.put("mail.smtp.host", "192.168.90.100");
			if (mailSession == null)
			{
				mailSession = javax.mail.Session.getInstance(props, null);
			}

			Message msg = new MimeMessage(mailSession);
			Multipart mp = new MimeMultipart();
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			msg.setReplyTo(new InternetAddress[]
			{ addressFrom });
			String[] recipients = to.split(";");
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++)
			{
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeBodyPart messagePart = new MimeBodyPart();
			messagePart.setContent(emailBody.toString(), "text/html");
			mp.addBodyPart(messagePart);
			msg.setContent(mp);
			Transport.send(msg);
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
	}
}
