﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("Package", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class PackageDetail
    {
        public PackageDetail()
        {
        }

        public Guid ID;
        public string Name;
        public string Description;
        public Guid SpaceID;
        public PStagingTable[] StagingTables;
        public PDimensionTable[] DimensionTables;
        public PMeasureTable[] MeasureTables;
        public PVariable[] Variables;
        public PAggregate[] Aggregates;
        public PCustomSubjectArea[] CustomSubjectAreas;
        public string CreatedBy;
        public string ModifiedBy;
        public DateTime? CreatedDate;
        public DateTime? ModifiedDate;

        public string[] getStagingTableNames()
        {
            return getPackageObjectNames(StagingTables);
        }

        public string[] getDimensionTableNames()
        {
            return getPackageObjectNames(DimensionTables);
        }

        public string[] getMeasureTableNames()
        {
            return getPackageObjectNames(MeasureTables);
        }

        public string[] getVariableNames()
        {
            return getPackageObjectNames(Variables);
        }

        public string[] getAggregateNames()
        {
            return getPackageObjectNames(Aggregates);
        }

        public string[] getCustomSubjectAreaNames()
        {
            return getPackageObjectNames(CustomSubjectAreas);
        }

        public string[] getPackageObjectNames(PackageBaseObject[] packageObjects)
        {
            List<string> responseList = null;
            if (packageObjects != null && packageObjects.Length > 0)
            {
                responseList = new List<string>();
                foreach (PackageBaseObject pObject in packageObjects)
                {
                    responseList.Add(pObject.Name);
                }
            }

            return responseList != null && responseList.Count > 0 ? responseList.ToArray() : null;
        }

        public string[] getPackageObjectDisplayNames(PackageBaseObject[] packageObjects)
        {
            List<string> responseList = null;
            if (packageObjects != null && packageObjects.Length > 0)
            {
                responseList = new List<string>();
                foreach (PackageBaseObject pObject in packageObjects)
                {
                    responseList.Add(pObject.Name);
                }
            }

            return responseList != null && responseList.Count > 0 ? responseList.ToArray() : null;
        }

    }

    // creating separate objects for adding any specific info
    [XmlType(TypeName = "StagingTable")]
    [Serializable]
    public class PStagingTable : PackageBaseObject
    {   
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [XmlType(TypeName = "DimensionTable")]
    [Serializable]
    public class PDimensionTable : PackageBaseObject
    {
        public PDimensionTable() { }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [XmlType(TypeName = "MeasureTable")]
    [Serializable]
    public class PMeasureTable : PackageBaseObject
    {
        public PMeasureTable() { }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [XmlType(TypeName = "Variable")]
    [Serializable]
    public class PVariable : PackageBaseObject
    {
        public PVariable() { }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [XmlType(TypeName = "Aggregate")]
    [Serializable]
    public class PAggregate : PackageBaseObject
    {
        public PAggregate() { }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    
    [XmlType(TypeName = "CustomSubjectArea")]
    [Serializable]
    public class PCustomSubjectArea : PackageBaseObject
    {
        public PCustomSubjectArea() { }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PackageBaseObject : BaseObject
    {
        public string Name;
        public string DisplayName;
        public bool Hide;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(PackageBaseObject).Equals(obj.GetType())) return false;
            PackageBaseObject other = (PackageBaseObject)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(DisplayName, other.DisplayName)) return false;
            if (Hide != other.Hide) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
