﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("ImportedItem", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class ImportedItem
    {
        public string ItemName;
        public ForeignKey[] ForeignKeys;
        public Location[] Locations;

        public string getSerializedString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ItemName);
            sb.Append("|3" + ForeignKeys != null ? ForeignKeys.Length: 0);
            if (ForeignKeys != null)
            {
                foreach (ForeignKey fk in ForeignKeys)
                {
                    sb.Append("|3" + fk.toSerializedString());
                }
            }
            sb.Append("|3" + Locations != null ? Locations.Length : 0);
            if (Locations != null)
            {
                foreach (Location lo in Locations)
                {
                    sb.Append("|3" + lo.toSerializedString());
                }
            }
            return sb.ToString();
        }

        public bool addForeignKey(ForeignKey fk)
        {
            if (ForeignKeys == null)
            {
                ForeignKeys = new ForeignKey[1];
                ForeignKeys[0] = fk;
            }
            else
            {
                List<ForeignKey> curSources = new List<ForeignKey>(ForeignKeys);
                ForeignKey oldfk = null;
                foreach (ForeignKey sfk in ForeignKeys)
                {
                    if (sfk.Source == fk.Source)
                    {
                        oldfk = sfk;
                        break;
                    }
                }
                if (oldfk == null)
                    curSources.Add(fk);
                else
                {
                    if (oldfk.Type == fk.Type)
                        return false;
                    oldfk.Type = fk.Type;
                }
                ForeignKeys = curSources.ToArray();
            }
            return true;
        }

        public bool removeForeignKey(string source)
        {
            if (ForeignKeys == null)
            {
                return false;
            }
            else
            {
                List<ForeignKey> curSources = new List<ForeignKey>(ForeignKeys);
                ForeignKey oldfk = null;
                foreach (ForeignKey sfk in ForeignKeys)
                {
                    if (sfk.Source == source)
                    {
                        oldfk = sfk;
                        break;
                    }
                }
                if (oldfk != null)
                {
                    curSources.Remove(oldfk);
                    ForeignKeys = curSources.ToArray();
                    return true;
                }
            }
            return false;
        }
    }
}

