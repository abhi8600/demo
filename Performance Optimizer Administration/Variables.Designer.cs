namespace Performance_Optimizer_Administration
{
    partial class Variables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Variables));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.variablesTabPage = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.evaluateOnDemandCheckBox = new System.Windows.Forms.CheckBox();
            this.invalidDefaultBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cacheableCheckBox = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.constantResultType = new System.Windows.Forms.RadioButton();
            this.queryResultType = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.varConnectionBox = new System.Windows.Forms.ComboBox();
            this.label180 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label179 = new System.Windows.Forms.Label();
            this.physicalQuery = new System.Windows.Forms.RadioButton();
            this.logicalQuery = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.multipleColumns = new System.Windows.Forms.RadioButton();
            this.label178 = new System.Windows.Forms.Label();
            this.singleValue = new System.Windows.Forms.RadioButton();
            this.multiValue = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.warehouseButton = new System.Windows.Forms.RadioButton();
            this.label177 = new System.Windows.Forms.Label();
            this.repositoryVariable = new System.Windows.Forms.RadioButton();
            this.sessionVariable = new System.Windows.Forms.RadioButton();
            this.variableQuery = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.variableMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.variablesTabPage.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.variableMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.variablesTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // variablesTabPage
            // 
            this.variablesTabPage.AutoScroll = true;
            this.variablesTabPage.Controls.Add(this.groupBox27);
            this.variablesTabPage.Location = new System.Drawing.Point(4, 22);
            this.variablesTabPage.Name = "variablesTabPage";
            this.variablesTabPage.Size = new System.Drawing.Size(844, 707);
            this.variablesTabPage.TabIndex = 20;
            this.variablesTabPage.Text = "Variables";
            this.variablesTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.label3);
            this.groupBox27.Controls.Add(this.evaluateOnDemandCheckBox);
            this.groupBox27.Controls.Add(this.invalidDefaultBox);
            this.groupBox27.Controls.Add(this.label2);
            this.groupBox27.Controls.Add(this.cacheableCheckBox);
            this.groupBox27.Controls.Add(this.panel2);
            this.groupBox27.Controls.Add(this.varConnectionBox);
            this.groupBox27.Controls.Add(this.label180);
            this.groupBox27.Controls.Add(this.panel1);
            this.groupBox27.Controls.Add(this.panel7);
            this.groupBox27.Controls.Add(this.panel6);
            this.groupBox27.Controls.Add(this.variableQuery);
            this.groupBox27.Controls.Add(this.label154);
            this.groupBox27.Location = new System.Drawing.Point(3, 3);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(837, 487);
            this.groupBox27.TabIndex = 18;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Variable Definition";
            // 
            // label3
            // 
            this.label3.AutoEllipsis = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(17, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(814, 32);
            this.label3.TabIndex = 31;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // evaluateOnDemandCheckBox
            // 
            this.evaluateOnDemandCheckBox.AutoSize = true;
            this.evaluateOnDemandCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.evaluateOnDemandCheckBox.Location = new System.Drawing.Point(17, 178);
            this.evaluateOnDemandCheckBox.Name = "evaluateOnDemandCheckBox";
            this.evaluateOnDemandCheckBox.Size = new System.Drawing.Size(153, 20);
            this.evaluateOnDemandCheckBox.TabIndex = 30;
            this.evaluateOnDemandCheckBox.Text = "Evaluate on Demand";
            this.evaluateOnDemandCheckBox.UseVisualStyleBackColor = true;
            // 
            // invalidDefaultBox
            // 
            this.invalidDefaultBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.invalidDefaultBox.Enabled = false;
            this.invalidDefaultBox.Location = new System.Drawing.Point(126, 448);
            this.invalidDefaultBox.Name = "invalidDefaultBox";
            this.invalidDefaultBox.Size = new System.Drawing.Size(197, 20);
            this.invalidDefaultBox.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label2.Location = new System.Drawing.Point(14, 452);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 16);
            this.label2.TabIndex = 28;
            this.label2.Text = "Default if Invalid";
            // 
            // cacheableCheckBox
            // 
            this.cacheableCheckBox.AutoSize = true;
            this.cacheableCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cacheableCheckBox.Location = new System.Drawing.Point(17, 152);
            this.cacheableCheckBox.Name = "cacheableCheckBox";
            this.cacheableCheckBox.Size = new System.Drawing.Size(203, 20);
            this.cacheableCheckBox.TabIndex = 27;
            this.cacheableCheckBox.Text = "Use Cache For Logical Query";
            this.cacheableCheckBox.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.constantResultType);
            this.panel2.Controls.Add(this.queryResultType);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(6, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(398, 32);
            this.panel2.TabIndex = 26;
            // 
            // constantResultType
            // 
            this.constantResultType.AutoSize = true;
            this.constantResultType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.constantResultType.Location = new System.Drawing.Point(240, 6);
            this.constantResultType.Name = "constantResultType";
            this.constantResultType.Size = new System.Drawing.Size(78, 20);
            this.constantResultType.TabIndex = 2;
            this.constantResultType.Text = "Constant";
            this.constantResultType.UseVisualStyleBackColor = true;
            // 
            // queryResultType
            // 
            this.queryResultType.Checked = true;
            this.queryResultType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.queryResultType.Location = new System.Drawing.Point(120, 6);
            this.queryResultType.Name = "queryResultType";
            this.queryResultType.Size = new System.Drawing.Size(104, 20);
            this.queryResultType.TabIndex = 1;
            this.queryResultType.TabStop = true;
            this.queryResultType.Text = "Query";
            this.queryResultType.UseVisualStyleBackColor = true;
            this.queryResultType.CheckedChanged += new System.EventHandler(this.queryResultType_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Result Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // varConnectionBox
            // 
            this.varConnectionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.varConnectionBox.FormattingEnabled = true;
            this.varConnectionBox.Location = new System.Drawing.Point(126, 261);
            this.varConnectionBox.Name = "varConnectionBox";
            this.varConnectionBox.Size = new System.Drawing.Size(197, 21);
            this.varConnectionBox.TabIndex = 25;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label180.Location = new System.Drawing.Point(16, 261);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(75, 16);
            this.label180.TabIndex = 24;
            this.label180.Text = "Connection";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label179);
            this.panel1.Controls.Add(this.physicalQuery);
            this.panel1.Controls.Add(this.logicalQuery);
            this.panel1.Location = new System.Drawing.Point(6, 115);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 32);
            this.panel1.TabIndex = 23;
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label179.Location = new System.Drawing.Point(8, 6);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(79, 20);
            this.label179.TabIndex = 21;
            this.label179.Text = "Query Type";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // physicalQuery
            // 
            this.physicalQuery.AutoSize = true;
            this.physicalQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.physicalQuery.Location = new System.Drawing.Point(240, 6);
            this.physicalQuery.Name = "physicalQuery";
            this.physicalQuery.Size = new System.Drawing.Size(77, 20);
            this.physicalQuery.TabIndex = 22;
            this.physicalQuery.Text = "Physical";
            this.physicalQuery.UseVisualStyleBackColor = true;
            this.physicalQuery.CheckedChanged += new System.EventHandler(this.physicalQuery_CheckedChanged);
            // 
            // logicalQuery
            // 
            this.logicalQuery.Checked = true;
            this.logicalQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.logicalQuery.Location = new System.Drawing.Point(120, 6);
            this.logicalQuery.Name = "logicalQuery";
            this.logicalQuery.Size = new System.Drawing.Size(70, 20);
            this.logicalQuery.TabIndex = 20;
            this.logicalQuery.TabStop = true;
            this.logicalQuery.Text = "Logical";
            this.logicalQuery.UseVisualStyleBackColor = false;
            this.logicalQuery.CheckedChanged += new System.EventHandler(this.logicalQuery_CheckedChanged);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.multipleColumns);
            this.panel7.Controls.Add(this.label178);
            this.panel7.Controls.Add(this.singleValue);
            this.panel7.Controls.Add(this.multiValue);
            this.panel7.Location = new System.Drawing.Point(6, 83);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(546, 32);
            this.panel7.TabIndex = 19;
            // 
            // multipleColumns
            // 
            this.multipleColumns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multipleColumns.Location = new System.Drawing.Point(360, 6);
            this.multipleColumns.Name = "multipleColumns";
            this.multipleColumns.Size = new System.Drawing.Size(129, 20);
            this.multipleColumns.TabIndex = 20;
            this.multipleColumns.Text = "MultipleColumns";
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.Location = new System.Drawing.Point(8, 6);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(108, 20);
            this.label178.TabIndex = 18;
            this.label178.Text = "Query Result";
            this.label178.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // singleValue
            // 
            this.singleValue.Checked = true;
            this.singleValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.singleValue.Location = new System.Drawing.Point(120, 6);
            this.singleValue.Name = "singleValue";
            this.singleValue.Size = new System.Drawing.Size(88, 20);
            this.singleValue.TabIndex = 16;
            this.singleValue.TabStop = true;
            this.singleValue.Text = "Value";
            // 
            // multiValue
            // 
            this.multiValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiValue.Location = new System.Drawing.Point(240, 6);
            this.multiValue.Name = "multiValue";
            this.multiValue.Size = new System.Drawing.Size(88, 20);
            this.multiValue.TabIndex = 17;
            this.multiValue.Text = "Multivalue";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.warehouseButton);
            this.panel6.Controls.Add(this.label177);
            this.panel6.Controls.Add(this.repositoryVariable);
            this.panel6.Controls.Add(this.sessionVariable);
            this.panel6.Location = new System.Drawing.Point(6, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(479, 32);
            this.panel6.TabIndex = 18;
            // 
            // warehouseButton
            // 
            this.warehouseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warehouseButton.Location = new System.Drawing.Point(360, 6);
            this.warehouseButton.Name = "warehouseButton";
            this.warehouseButton.Size = new System.Drawing.Size(101, 20);
            this.warehouseButton.TabIndex = 19;
            this.warehouseButton.Text = "Warehouse";
            // 
            // label177
            // 
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.Location = new System.Drawing.Point(8, 6);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(108, 20);
            this.label177.TabIndex = 18;
            this.label177.Text = "Variable Type";
            this.label177.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // repositoryVariable
            // 
            this.repositoryVariable.Checked = true;
            this.repositoryVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryVariable.Location = new System.Drawing.Point(120, 6);
            this.repositoryVariable.Name = "repositoryVariable";
            this.repositoryVariable.Size = new System.Drawing.Size(104, 20);
            this.repositoryVariable.TabIndex = 16;
            this.repositoryVariable.TabStop = true;
            this.repositoryVariable.Text = "Repository";
            this.repositoryVariable.CheckedChanged += new System.EventHandler(this.repositoryVariable_CheckedChanged);
            // 
            // sessionVariable
            // 
            this.sessionVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sessionVariable.Location = new System.Drawing.Point(240, 6);
            this.sessionVariable.Name = "sessionVariable";
            this.sessionVariable.Size = new System.Drawing.Size(78, 20);
            this.sessionVariable.TabIndex = 17;
            this.sessionVariable.Text = "Session";
            // 
            // variableQuery
            // 
            this.variableQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.variableQuery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.variableQuery.Location = new System.Drawing.Point(126, 296);
            this.variableQuery.Multiline = true;
            this.variableQuery.Name = "variableQuery";
            this.variableQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.variableQuery.Size = new System.Drawing.Size(705, 146);
            this.variableQuery.TabIndex = 15;
            this.variableQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.variableQuery_KeyDown);
            this.variableQuery.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.variableQuery_KeyPress);
            // 
            // label154
            // 
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(16, 296);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(64, 32);
            this.label154.TabIndex = 14;
            this.label154.Text = "Query/ Constant";
            // 
            // variableMenuStrip
            // 
            this.variableMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newVariableToolStripMenuItem,
            this.removeVariableToolStripMenuItem,
            this.upToolStripMenuItem,
            this.downToolStripMenuItem});
            this.variableMenuStrip.Name = "variableMenuStrip";
            this.variableMenuStrip.ShowImageMargin = false;
            this.variableMenuStrip.Size = new System.Drawing.Size(138, 92);
            // 
            // newVariableToolStripMenuItem
            // 
            this.newVariableToolStripMenuItem.Name = "newVariableToolStripMenuItem";
            this.newVariableToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.newVariableToolStripMenuItem.Text = "New Variable";
            this.newVariableToolStripMenuItem.Click += new System.EventHandler(this.addVariable_Click);
            // 
            // removeVariableToolStripMenuItem
            // 
            this.removeVariableToolStripMenuItem.Name = "removeVariableToolStripMenuItem";
            this.removeVariableToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.removeVariableToolStripMenuItem.Text = "Remove Variable";
            this.removeVariableToolStripMenuItem.Click += new System.EventHandler(this.removeVariable_Click);
            // 
            // upToolStripMenuItem
            // 
            this.upToolStripMenuItem.Name = "upToolStripMenuItem";
            this.upToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.upToolStripMenuItem.Text = "Up";
            this.upToolStripMenuItem.Click += new System.EventHandler(this.upToolStripMenuItem_Click);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.downToolStripMenuItem_Click);
            // 
            // Variables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "Variables";
            this.Text = "Variables";
            this.tabControl.ResumeLayout(false);
            this.variablesTabPage.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.variableMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage variablesTabPage;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.RadioButton singleValue;
        private System.Windows.Forms.RadioButton multiValue;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.RadioButton repositoryVariable;
        private System.Windows.Forms.RadioButton sessionVariable;
        private System.Windows.Forms.ContextMenuStrip variableMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeVariableToolStripMenuItem;
        private System.Windows.Forms.RadioButton logicalQuery;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.RadioButton physicalQuery;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton multipleColumns;
        private System.Windows.Forms.ToolStripMenuItem upToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton constantResultType;
        private System.Windows.Forms.RadioButton queryResultType;
        private System.Windows.Forms.RadioButton warehouseButton;
        private System.Windows.Forms.CheckBox cacheableCheckBox;
        private System.Windows.Forms.CheckBox evaluateOnDemandCheckBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox invalidDefaultBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox varConnectionBox;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.TextBox variableQuery;
        private System.Windows.Forms.Label label154;
    }
}