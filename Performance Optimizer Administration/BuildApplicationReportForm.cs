using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class BuildApplicationReportForm : Form
    {
        private List<BuildAction> buildActions;

        public BuildApplicationReportForm(List<BuildAction> buildActions)
        {
            InitializeComponent();
            this.buildActions = buildActions;
            InitializeDataTable(buildActions, false);
        }

        public void InitializeDataTable(List<BuildAction> buildActions, bool verbose)
        {
            operationsDataGridView.Rows.Clear();
            if (buildActions != null)
            {
                foreach (BuildAction ba in buildActions)
                {
                    if (ba.Type == MessageType.BASIC || verbose)
                    {
                        operationsDataGridView.Rows.Add(new string[] { ba.Message });
                    }
                }
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            string postBuildRem = ((MainAdminForm)this.Owner).buildPropertiesModule.getPostBuildReminder();
            this.Close();
            if (postBuildRem != null && postBuildRem.Length > 0)
            {
                PostBuildReminderDialog dg = new PostBuildReminderDialog(postBuildRem);
                dg.Show();
            }
        }

        private void verbose_CheckedChanged(object sender, EventArgs e)
        {
            if (verbose.Checked)
            {
                InitializeDataTable(buildActions, true);
            }
            else
            {
                InitializeDataTable(buildActions, false);
            }
        }
    }
}