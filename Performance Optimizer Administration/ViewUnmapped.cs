﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class ViewUnmappedForm : Form
    {
        MainAdminForm ma;
        public ViewUnmappedForm(MainAdminForm ma, bool showDimensionAttributes)
        {
            this.ma = ma;
            InitializeComponent();
            setup(showDimensionAttributes);
        }

        private void setup(bool showDimensionAttributes)
        {
            if (showDimensionAttributes)
            {
                this.Text = "Unmapped Dimensional Attributes";
                Dictionary<string, List<string>> unmappedDimCols = getUnmappedDimensionColumns();
                Dictionary<string, List<string>> aggMappedDimCols = getAggMappedDimensionColumns();
                foreach (string dim in unmappedDimCols.Keys)
                {
                    List<string> aggMappedColList = null;
                    if(aggMappedDimCols.ContainsKey(dim))
                    {
                        aggMappedColList = aggMappedDimCols[dim];
                    }
                    bool first = true;
                    foreach (string col in unmappedDimCols[dim])
                    {
                        //Ignore columns that are mapped in aggregates
                        if (aggMappedColList != null && aggMappedColList.Contains(col))
                        {
                            continue;
                        }
                        if (first)
                        {
                            unmappedGrid.Rows.Add(dim, col);
                            first = false;
                        }
                        else
                        {
                            unmappedGrid.Rows.Add("", col);
                        }
                    }
                }
            }
            else
            {
                this.Text = "Unmapped Measures";
                unmappedGrid.Columns["Dimension"].Visible = false;
                unmappedGrid.Columns["Attribute"].HeaderText = "Measure";
                unmappedGrid.Width = unmappedGrid.Width - 140;
                this.Width = this.Width - 140;
                closeButton.Location = new Point(closeButton.Location.X - 140, closeButton.Location.Y);
                List<string> unmappedMeasures = getUnmappedMeasures();
                List<string> aggMappedMeasures = getAggMappedMeasures();
                foreach (string um in unmappedMeasures)
                {
                    if (aggMappedMeasures != null && aggMappedMeasures.Contains(um))
                    {
                        continue;
                    }
                    unmappedGrid.Rows.Add("", um);
                }
            }
        }

        private List<string> getUnmappedMeasures()
        {
            List<string> unmappedMeasures = new List<string>();
            foreach (DataRow dr in ma.measureColumnsTable.Rows)
            {
                List<DataRow> results = Util.selectRows(ma.measureColumnMappings, new string[][] { 
                    new string[] { "ColumnName", dr["ColumnName"].ToString() } });
                if (!((bool)dr["Derived"]) && results.Count == 0)
                {
                    unmappedMeasures.Add((string)dr["ColumnName"]);
                }
            }
            return unmappedMeasures;
        }

        private Dictionary<string, List<string>> getUnmappedDimensionColumns()
        {
            Dictionary<string, List<string>> unmappedDimCols = new Dictionary<string, List<string>>();
            foreach (DataRow dr in ma.dimensionColumns.Rows)
            {
                List<DataRow> result = Util.selectRows(ma.dimensionColumnMappings, new string[][] { new string[] { "ColumnName", dr["ColumnName"].ToString() } });
                if (result.Count == 0)
                {
                    List<string> colList = null;
                    if (!unmappedDimCols.ContainsKey((string)dr["DimensionName"]))
                    {
                        colList = new List<string>();
                        unmappedDimCols.Add((string)dr["DimensionName"], colList);
                    }
                    colList = unmappedDimCols[(string)dr["DimensionName"]];
                    colList.Add((string)dr["ColumnName"]);
                }
            }
            return unmappedDimCols;
        }

        private List<string> getAggMappedMeasures()
        {
            List<string> aggMappedMeasures = new List<string>();
            Aggregate[] aggList = ma.aggModule.getAggregateList();
            if (aggList != null)
            {
                foreach (Aggregate agg in aggList)
                {
                    foreach (AggTransformation tr in agg.Transformations)
                    {
                        if (tr.Dimension != null)
                        {
                            //Ignore dimension column transformations
                            continue;
                        }
                        aggMappedMeasures.Add(tr.ColumnName);
                    }
                }
            }
            return aggMappedMeasures;
        }

        private Dictionary<string, List<string>> getAggMappedDimensionColumns()
        {
            Dictionary<string, List<string>> aggMappedDimCols = new Dictionary<string, List<string>>();
            Aggregate[] aggList = ma.aggModule.getAggregateList();
            if (aggList != null)
            {
                foreach (Aggregate agg in aggList)
                {
                    foreach (AggTransformation tr in agg.Transformations)
                    {
                        if (tr.Dimension == null)
                        {
                            continue;
                        }
                        List<string> colList = null;
                        if (!aggMappedDimCols.ContainsKey(tr.Dimension))
                        {
                            colList = new List<string>();
                            aggMappedDimCols.Add(tr.Dimension, colList);
                        }
                        colList = aggMappedDimCols[tr.Dimension];
                        colList.Add(tr.ColumnName);
                    }
                }
            }
            return aggMappedDimCols;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
