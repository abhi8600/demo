using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class MeasureGrainForm : Form
    {
        private MainAdminForm ma = null;
        private string measureColumnName = null;
        private List<MeasureGrain> mappedMeasureGrains = new List<MeasureGrain>();
        private List<MeasureGrain> unmappedMeasureGrains = new List<MeasureGrain>();
        public MeasureGrainForm(MainAdminForm ma, string measureColumnName, List<MeasureGrain> mappedMeasureGrains, List<MeasureGrain> unmappedMeasureGrains)
        {
            this.ma = ma;
            this.measureColumnName = measureColumnName;
            this.mappedMeasureGrains = mappedMeasureGrains;
            this.unmappedMeasureGrains = unmappedMeasureGrains;
            InitializeComponent();
            InitializeDataTables(mappedMeasureGrains, unmappedMeasureGrains);
        }

        public List<MeasureGrain> getUnmappedMeasureGrains()
        {
            return unmappedMeasureGrains;
        }

        private void InitializeDataTables(List<MeasureGrain> mappedMeasureGrains, List<MeasureGrain> unmappedMeasureGrains)
        {
            measureGrainDataGridView.Rows.Clear();
            refreshMeasureGrainDataGrid(mappedMeasureGrains, unmappedMeasureGrains);

            List<String> dimensionsWithLevels = new List<string>();
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            levelListBox.Items.Clear();
            foreach (Hierarchy h in hlist)
            {
                List<string> llist = new List<string>();
                dimensionsWithLevels.Add(h.DimensionName);
                h.getChildLevelNames(llist);
                foreach (string s in llist)
                {
                    levelListBox.Items.Add(new ListViewItem(new string[] { h.DimensionName, s }));
                }
            }
        }

        private void refreshMeasureGrainDataGrid(List<MeasureGrain> mappedMeasureGrains, List<MeasureGrain> unmappedMeasureGrains)
        {
            measureGrainDataGridView.Rows.Clear();
            foreach (MeasureGrain mg in mappedMeasureGrains)
            {
                addMeasureGrainToDataGrid(mg);
            }

            foreach (MeasureGrain mg in unmappedMeasureGrains)
            {
                addMeasureGrainToDataGrid(mg);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            ListView.CheckedIndexCollection checkedIndices = levelListBox.CheckedIndices;
            if (checkedIndices.Count <= 0)
            {
                MessageBox.Show("You must select atleast one dimension/level in order to add a new measure grain.", "Error");
                return;
            }
            MeasureGrain mgToBeAdded = new MeasureGrain();
            mgToBeAdded.measureColumnName = measureColumnName;
            Int16 index = 0;
            if (unmappedMeasureGrains != null && unmappedMeasureGrains.Count > 0)
            {
                string lastUnmappedMeasureTableName = unmappedMeasureGrains[unmappedMeasureGrains.Count - 1].measureTableName;
                string indexStr = lastUnmappedMeasureTableName.Substring(10, lastUnmappedMeasureTableName.Length - 11);
                index = (Int16)new Int16Converter().ConvertFromString(indexStr);
            }
            mgToBeAdded.measureTableName = "<UNMAPPED "+(index+1)+">";
            mgToBeAdded.measureTableGrains = new MeasureTableGrain[checkedIndices.Count];
            int mtgIdx = 0;
            for(int idx=0; idx < levelListBox.Items.Count; idx++)
            {
                if (checkedIndices.Contains(idx))
                {
                    MeasureTableGrain mtg = new MeasureTableGrain();
                    mtg.DimensionName = levelListBox.Items[idx].SubItems[0].Text;
                    mtg.DimensionLevel = levelListBox.Items[idx].SubItems[1].Text;
                    mgToBeAdded.measureTableGrains[mtgIdx] = mtg;
                    mtgIdx++;
                }
            }
            foreach (MeasureGrain mg in mappedMeasureGrains)
            {
                if (mg.atSameGrain(mgToBeAdded))
                {
                    MessageBox.Show("Grain already exists for measure in measure table: " + mg.measureTableName);
                    return;
                }
            }
            foreach (MeasureGrain mg in unmappedMeasureGrains)
            {
                if (mg.atSameGrain(mgToBeAdded))
                {
                    MessageBox.Show("Grain already exists for measure in measure table: " + mg.measureTableName);
                    return;
                }
            }
            unmappedMeasureGrains.Add(mgToBeAdded);
            addMeasureGrainToDataGrid(mgToBeAdded);
            MessageBox.Show("Grain added as "+mgToBeAdded.measureTableName+" measure table.");
        }

        private void addMeasureGrainToDataGrid(MeasureGrain mg)
        {
            measureGrainDataGridView.Rows.Add(new String[] { mg.measureTableName, mg.measureTableGrains[0].DimensionName, mg.measureTableGrains[0].DimensionLevel });
            if (mg.measureTableGrains.Length > 1)
            {
                for (int i = 1; i < mg.measureTableGrains.Length; i++)
                {
                    measureGrainDataGridView.Rows.Add(new String[] { "", mg.measureTableGrains[i].DimensionName, mg.measureTableGrains[i].DimensionLevel });
                }
            }
        }

        private void levelListBox_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked == true)
            {
                string dim = e.Item.SubItems[0].Text;
                foreach (ListViewItem lvi in levelListBox.Items)
                {
                    if (lvi != e.Item && lvi.SubItems[0].Text == dim)
                        lvi.Checked = false;
                }
            }
        }

        private void measureGrainDataGridView_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (measureGrainDataGridView.SelectedRows.Count == 1)
            {
                DataGridViewRow dr = measureGrainDataGridView.SelectedRows[0];
                String measureTableName = (string)dr.Cells["measureTable"].Value;
                if ((measureTableName != null) && (measureTableName.StartsWith("<UNMAPPED ")))
                {
                    removeToolStripMenuItem.Visible = true;
                }
                else
                {
                    removeToolStripMenuItem.Visible = false;
                }
            }
            else
            {
                removeToolStripMenuItem.Visible = false;
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataGridViewRow dr = measureGrainDataGridView.SelectedRows[0];
            String measureTableName = (string)dr.Cells["measureTable"].Value;
            MeasureGrain selectedMeasureGrain = null;
            foreach (MeasureGrain mg in unmappedMeasureGrains)
            {
                if (mg.measureTableName.Equals(measureTableName))
                {
                    selectedMeasureGrain = mg;
                    break;
                }
            }
            if (selectedMeasureGrain != null)
            {
                unmappedMeasureGrains.Remove(selectedMeasureGrain);
                refreshMeasureGrainDataGrid(mappedMeasureGrains, unmappedMeasureGrains);
            }
        }

        private void measureGrainDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            measureGrainDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
    }
}