using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class UsersAndGroups : Form, RepositoryModule, RenameNode
    {
        private MainAdminForm ma;
        private string moduleName = "Users and Groups";
        private string categoryName = "Business Modeling";
        public List<ACLItem> accessList;
        public List<Group> groupsList;
        public List<User> usersList;

        public UsersAndGroups(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            this.accessList = new List<ACLItem>();
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (groupsList != null)
            {
                r.Groups = (Group[])groupsList.ToArray();
            }
             if (usersList != null)
            {
                r.Users = new User[usersList.Count];
                for (int i = 0; i < usersList.Count; i++) r.Users[i] = usersList[i].clone();
            }
            r.ACL = accessList.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            User adminUser = null;
            selNode = null;
            if (r != null && r.Users != null)
            {
                usersList = new List<User>(r.Users);
                foreach (User u in r.Users)
                {
                    // handle old style password encryption and convert to new style
                    if ((u.Password != null) && !u.Password.StartsWith("$sha-1$"))
                    {
                        if (u.Password != "")
                            u.Password = MainAdminForm.decrypt(u.Password, MainAdminForm.SecretPassword);
                        u.Password = MainAdminForm.digest(u.Password);
                    }
                    if (u.Username == "Administrator")
                    {
                        adminUser = u;
                    }
                }
            }
            else
            {
                usersList = new List<User>();
            }
            if (adminUser == null)
            {
                User u = new User();
                u.Username = "Administrator";
                u.Fullname = "Administrator";
                adminUser = u;
                usersList.Add(u);
            }
            bool hasAdmin = false;
            if (r != null && r.Groups != null)
            {
                groupsList = new List<Group>(r.Groups);
                foreach (Group g in r.Groups)
                {
                    if (g.Name == "Administrators")
                    {
                        hasAdmin = true;
                        bool found = false;
                        foreach (String s in g.Usernames)
                            if (s == "Administrator")
                            {
                                found = true;
                                g.InternalGroup = true; // backwards compatibility
                                break;
                            }
                        if (!found)
                        {
                            string[] curList = g.Usernames;
                            g.Usernames = new string[curList.Length + 1];
                            for (int i = 0; i < curList.Length; i++) g.Usernames[i] = curList[i];
                            g.Usernames[g.Usernames.Length - 1] = "Administrator";
                        }
                    }
                }
            } else
                groupsList = new List<Group>();
            if (!hasAdmin)
            {
                Group g = new Group();
                g.Name = "Administrators";
                g.Usernames = new string[] { "Administrator" };
                g.Filters = new string[] { };
                g.InternalGroup = true;
                groupsList.Insert(0, g);
            }
            accessList.Clear();
            if (r != null && r.ACL != null)
                accessList.AddRange(r.ACL);
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;
        TreeNode groupsNode;
        TreeNode usersNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            TreeNode tn = new TreeNode("Groups");
            tn.ImageIndex = 14;
            tn.SelectedImageIndex = 14;
            rootNode.Nodes.Add(tn);
            groupsNode = tn;
            tn = new TreeNode("Users");
            tn.ImageIndex = 14;
            tn.SelectedImageIndex = 14;
            rootNode.Nodes.Add(tn);
            usersNode = tn;
            if (groupsList != null)
                foreach (Group g in groupsList)
                {
                    tn = new TreeNode(g.Name);
                    tn.ImageIndex = 14;
                    tn.SelectedImageIndex = 14;
                    groupsNode.Nodes.Add(tn);
                }
            if (usersList != null)
                foreach (User u in usersList)
                {
                    tn = new TreeNode(u.Username);
                    tn.ImageIndex = 14;
                    tn.SelectedImageIndex = 14;
                    usersNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();

            selNode = node;
            if (selNode == groupsNode)
            {
                ma.mainTree.ContextMenuStrip = groupsMenuStrip;
                removeGroupToolStripMenuItem.Visible = false;
                return false;
            }
            else if (selNode == usersNode)
            {
                ma.mainTree.ContextMenuStrip = usersMenuStrip;
                removeUserToolStripMenuItem.Visible = false;
                return false;
            }
            if (selNode.Parent == groupsNode)
            {
                ma.mainTree.ContextMenuStrip = groupsMenuStrip;
                removeGroupToolStripMenuItem.Visible = true;
                groupsGroupBox.Visible = true;
                usersGroupBox.Visible = false;
                changeGroupIndex();
                return true;
            }
            else if (selNode.Parent == usersNode)
            {
                ma.mainTree.ContextMenuStrip = usersMenuStrip;
                groupsGroupBox.Visible = false;
                usersGroupBox.Visible = true;
                removeUserToolStripMenuItem.Visible = true;
                changeUserIndex();
                return true;
            }
            return false;
        }

        public void setup()
        {
            ArrayList checkedFilters = new ArrayList();
            foreach (string s in groupFilterListBox.CheckedItems)
            {
                checkedFilters.Add(s);
            }
            QueryFilter.sortedListBox(groupFilterListBox, ma.filterMod.filterList, (string[])checkedFilters.ToArray(typeof(string)));
           
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return usersAndGroupsTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
            updateUser();
            updateGroup();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        private Group getGroup()
        {
            Group g = new Group();
            g.Name = selNode.Text;
            g.Usernames = new string[groupUserList.CheckedItems.Count];
            for (int i = 0; i < groupUserList.CheckedItems.Count; i++) g.Usernames[i] = groupUserList.CheckedItems[i].SubItems[0].Text;
            List<string> mflist = new List<string>();
            foreach (string s in groupFilterListBox.CheckedItems) mflist.Add(s);
            g.Filters = (string[])mflist.ToArray();
            g.InternalGroup = internalGroup.Checked;
            return (g);
        }

        private User getUser()
        {
            User u = new User();
            u.Username = selNode.Text;

            // don't redigest a password that is already digested
            if (userPasswordBox.Text.StartsWith("$sha-1$"))
            {
                u.Password = userPasswordBox.Text;
            }
            else
            {
                u.Password = MainAdminForm.digest(userPasswordBox.Text);
            }
            u.Fullname = fullNameBox.Text;
            return (u);
        }

        private void addGroupButton_Click(object sender, System.EventArgs e)
        {
            Group g = new Group();
            g.Name = "New Group";
            g.Usernames = new string[0];
            foreach (Group g2 in groupsList)
            {
                if (g.Name == g2.Name) return;
            }
            groupsList.Add(g);
            TreeNode tn = new TreeNode(g.Name);
            tn.ImageIndex = 14;
            tn.SelectedImageIndex = 14;
            groupsNode.Nodes.Add(tn);
            g.InternalGroup = false;
        }

        private void updateGroup()
        {
            if (selNode == null) return;
            if (selNode == groupsNode && selNode == usersNode) return;
            if (selNode.Parent == groupsNode)
            {
                int index = groupsNode.Nodes.IndexOf(selNode);
                Group g = getGroup();
                groupsList[index] = g;
                selNode.Text = g.Name;
            }
        }

        private void removeGroupButton_Click(object sender, System.EventArgs e)
        {
            if (selNode == groupsNode && selNode == usersNode) return;
            if (selNode.Parent == groupsNode)
            {
                int index = groupsNode.Nodes.IndexOf(selNode);
                for (int i = 0; i < accessList.Count; i++)
                {
                    ACLItem item = accessList[i];
                    if (item.Group == selNode.Text)
                    {
                        accessList.RemoveAt(i);
                        i--;
                    }
                }
                groupsList.RemoveAt(index);
                selNode.Remove();
            }
        }

        private void changeGroupIndex()
        {
            if (selNode == null || selNode == groupsNode || selNode == usersNode) return;
            if (selNode.Parent == groupsNode)
            {
                int index = groupsNode.Nodes.IndexOf(selNode);
                Group g = groupsList[index];
                internalGroup.Checked = g.InternalGroup;
                
                List<string> sUsersList = new List<String>();
                for (int i = 0; i < usersList.Count; i++) sUsersList.Add(usersList[i].Username);
                Util.sortedListView(groupUserList, sUsersList, g.Usernames);
                
                QueryFilter.sortedListBox(groupFilterListBox, ma.filterMod.filterList, g.Filters);
                
            }
        }

        private void addUserButton_Click(object sender, System.EventArgs e)
        {
            User u = new User();
            u.Fullname = "New User - Full Name";
            u.Username = "New User";
            foreach (User u2 in usersList)
            {
                if (u.Username == u2.Username) return;
            }
            usersList.Add(u);
            TreeNode tn = new TreeNode(u.Username);
            tn.ImageIndex = 14;
            tn.SelectedImageIndex = 14;
            usersNode.Nodes.Add(tn);
        }

        private void updateUser()
        {
            if (selNode == null) return;
            if (selNode == groupsNode && selNode == usersNode) return;
            if (selNode.Parent == usersNode)
            {
                int index = usersNode.Nodes.IndexOf(selNode);
                User u = getUser();
                usersList[index] = u;
                selNode.Text = u.Username;
            }
        }

        private void removeUserButton_Click(object sender, System.EventArgs e)
        {
            if (selNode == groupsNode && selNode == usersNode) return;
            if (selNode.Parent == usersNode)
            {
                if (selNode.Text == "Administrator") return;
                int index = usersNode.Nodes.IndexOf(selNode);
                usersList.RemoveAt(index);
                selNode.Remove();
            }
        }

        private void changeUserIndex()
        {
            if (selNode == null || selNode == groupsNode || selNode == usersNode) return;
            if (selNode.Parent == usersNode)
            {
                int index = usersNode.Nodes.IndexOf(selNode);
                User u = usersList[index];
                userPasswordBox.Text = u.Password;
                fullNameBox.Text = u.Fullname;
            }
        }

        private void setPriviliges_Click(object sender, EventArgs e)
        {
            if (selNode.Text.Length > 0)
            {
                SetPriviliges sp = new SetPriviliges(ma, selNode.Text, accessList);
                sp.ShowDialog();
                List<ACLItem> list = sp.getAccessList();
                if (list != null)
                {

                    // Create new list without entries for current group
                    List<ACLItem> newList = new List<ACLItem>();
                    foreach (ACLItem curItem in accessList)
                    {
                        if (curItem.Group != selNode.Text)
                        {
                            newList.Add(curItem);
                        }
                    }
                    accessList = newList;
                    // Add new ones
                    foreach (ACLItem item in list)
                    {
                        accessList.Add(item);
                    }
                }
            }
        }

        public string getAdminUsername()
        {
            return ("Administrator");
        }

        public string getAdminPassword()
        {
            foreach (User u in usersList)
            {
                if (u.Username == "Administrator")
                    return (u.Password);
            }
            return (null);
        }

        private void groupBoxUsers_Leave(object sender, EventArgs e)
        {
            updateUser();
        }

        private void groupBoxGroups_Leave(object sender, EventArgs e)
        {
            updateGroup();
        }


        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == groupsNode)
            {
                string oldName = e.Node.Text;
                string newName = e.Label;
                e.Node.Text = e.Label;
                foreach (ACLItem item in accessList)
                {
                    if (item.Group == oldName) item.Group = newName;
                }
            }
            else if (e.Node.Parent == usersNode)
            {
                string oldName = e.Node.Text;
                if (oldName == "Administrator")
                {
                    e.CancelEdit = true;
                    return;
                }
                string newName = e.Label;
                e.Node.Text = e.Label;
                foreach (Group g in groupsList)
                {
                    if (g.Usernames != null && g.Usernames.Length > 0)
                    for (int i = 0; i < g.Usernames.Length; i++)
                    {
                        if (g.Usernames[i] == oldName) g.Usernames[i] = newName;
                    }
                }
            }
        }

        #endregion

        private void UserAndGroups_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }
    }
}