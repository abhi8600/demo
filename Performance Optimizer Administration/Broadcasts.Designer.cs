namespace Performance_Optimizer_Administration
{
    partial class Broadcasts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.broadcastTabPage = new System.Windows.Forms.TabPage();
            this.queryTextBox = new System.Windows.Forms.TextBox();
            this.labelQuery = new System.Windows.Forms.Label();
            this.textBoxBody = new System.Windows.Forms.TextBox();
            this.labelBody = new System.Windows.Forms.Label();
            this.textBoxReport = new System.Windows.Forms.TextBox();
            this.labelReport = new System.Windows.Forms.Label();
            this.textBoxSubject = new System.Windows.Forms.TextBox();
            this.labelSubject = new System.Windows.Forms.Label();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.labelFrom = new System.Windows.Forms.Label();
            this.broadcastContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newBroadcastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeBroadcastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.broadcastTabPage.SuspendLayout();
            this.broadcastContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.broadcastTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // broadcastTabPage
            // 
            this.broadcastTabPage.AutoScroll = true;
            this.broadcastTabPage.Controls.Add(this.queryTextBox);
            this.broadcastTabPage.Controls.Add(this.labelQuery);
            this.broadcastTabPage.Controls.Add(this.textBoxBody);
            this.broadcastTabPage.Controls.Add(this.labelBody);
            this.broadcastTabPage.Controls.Add(this.textBoxReport);
            this.broadcastTabPage.Controls.Add(this.labelReport);
            this.broadcastTabPage.Controls.Add(this.textBoxSubject);
            this.broadcastTabPage.Controls.Add(this.labelSubject);
            this.broadcastTabPage.Controls.Add(this.textBoxFrom);
            this.broadcastTabPage.Controls.Add(this.labelFrom);
            this.broadcastTabPage.Location = new System.Drawing.Point(4, 22);
            this.broadcastTabPage.Name = "broadcastTabPage";
            this.broadcastTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.broadcastTabPage.Size = new System.Drawing.Size(844, 707);
            this.broadcastTabPage.TabIndex = 0;
            this.broadcastTabPage.Text = "Broadcasts";
            this.broadcastTabPage.UseVisualStyleBackColor = true;
            // 
            // queryTextBox
            // 
            this.queryTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.queryTextBox.Location = new System.Drawing.Point(102, 171);
            this.queryTextBox.Multiline = true;
            this.queryTextBox.Name = "queryTextBox";
            this.queryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.queryTextBox.Size = new System.Drawing.Size(600, 200);
            this.queryTextBox.TabIndex = 9;
            this.queryTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.queryTextBox_validating);
            // 
            // labelQuery
            // 
            this.labelQuery.AutoSize = true;
            this.labelQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQuery.Location = new System.Drawing.Point(43, 171);
            this.labelQuery.Name = "labelQuery";
            this.labelQuery.Size = new System.Drawing.Size(44, 16);
            this.labelQuery.TabIndex = 8;
            this.labelQuery.Text = "Query";
            // 
            // textBoxBody
            // 
            this.textBoxBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxBody.Location = new System.Drawing.Point(102, 388);
            this.textBoxBody.Multiline = true;
            this.textBoxBody.Name = "textBoxBody";
            this.textBoxBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBody.Size = new System.Drawing.Size(600, 200);
            this.textBoxBody.TabIndex = 7;
            this.textBoxBody.KeyDown += new System.Windows.Forms.KeyEventHandler(this.broadcastBody_KeyDown);
            this.textBoxBody.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.broadcastBody_KeyPress);
            this.textBoxBody.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxBody_Validating);
            // 
            // labelBody
            // 
            this.labelBody.AutoSize = true;
            this.labelBody.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBody.Location = new System.Drawing.Point(43, 388);
            this.labelBody.Name = "labelBody";
            this.labelBody.Size = new System.Drawing.Size(40, 16);
            this.labelBody.TabIndex = 6;
            this.labelBody.Text = "Body";
            // 
            // textBoxReport
            // 
            this.textBoxReport.Location = new System.Drawing.Point(102, 127);
            this.textBoxReport.Name = "textBoxReport";
            this.textBoxReport.Size = new System.Drawing.Size(600, 20);
            this.textBoxReport.TabIndex = 5;
            this.textBoxReport.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxReport_Validating);
            // 
            // labelReport
            // 
            this.labelReport.AutoSize = true;
            this.labelReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReport.Location = new System.Drawing.Point(43, 131);
            this.labelReport.Name = "labelReport";
            this.labelReport.Size = new System.Drawing.Size(49, 16);
            this.labelReport.TabIndex = 4;
            this.labelReport.Text = "Report";
            // 
            // textBoxSubject
            // 
            this.textBoxSubject.Location = new System.Drawing.Point(102, 82);
            this.textBoxSubject.Name = "textBoxSubject";
            this.textBoxSubject.Size = new System.Drawing.Size(600, 20);
            this.textBoxSubject.TabIndex = 3;
            this.textBoxSubject.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxSubject_Validating);
            // 
            // labelSubject
            // 
            this.labelSubject.AutoSize = true;
            this.labelSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubject.Location = new System.Drawing.Point(43, 86);
            this.labelSubject.Name = "labelSubject";
            this.labelSubject.Size = new System.Drawing.Size(53, 16);
            this.labelSubject.TabIndex = 2;
            this.labelSubject.Text = "Subject";
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Location = new System.Drawing.Point(102, 37);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(600, 20);
            this.textBoxFrom.TabIndex = 1;
            this.textBoxFrom.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxFrom_Validating);
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFrom.Location = new System.Drawing.Point(43, 41);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(39, 16);
            this.labelFrom.TabIndex = 0;
            this.labelFrom.Text = "From";
            // 
            // broadcastContextMenuStrip
            // 
            this.broadcastContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBroadcastToolStripMenuItem,
            this.removeBroadcastToolStripMenuItem,
            this.upToolStripMenuItem,
            this.downToolStripMenuItem});
            this.broadcastContextMenuStrip.Name = "broadcastContextMenuStrip";
            this.broadcastContextMenuStrip.Size = new System.Drawing.Size(176, 92);
            // 
            // newBroadcastToolStripMenuItem
            // 
            this.newBroadcastToolStripMenuItem.Name = "newBroadcastToolStripMenuItem";
            this.newBroadcastToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.newBroadcastToolStripMenuItem.Text = "Add Broadcast";
            this.newBroadcastToolStripMenuItem.Click += new System.EventHandler(this.newBroadcastToolStripMenuItem_Click);
            // 
            // removeBroadcastToolStripMenuItem
            // 
            this.removeBroadcastToolStripMenuItem.Name = "removeBroadcastToolStripMenuItem";
            this.removeBroadcastToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.removeBroadcastToolStripMenuItem.Text = "Remove Broadcast";
            this.removeBroadcastToolStripMenuItem.Click += new System.EventHandler(this.removeBroadcastToolStripMenuItem_Click);
            // 
            // upToolStripMenuItem
            // 
            this.upToolStripMenuItem.Name = "upToolStripMenuItem";
            this.upToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.upToolStripMenuItem.Text = "Up";
            this.upToolStripMenuItem.Click += new System.EventHandler(this.upToolStripMenuItem_Click);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.downToolStripMenuItem_Click);
            // 
            // Broadcasts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "Broadcasts";
            this.Text = "Broadcasts";
            this.tabControl.ResumeLayout(false);
            this.broadcastTabPage.ResumeLayout(false);
            this.broadcastTabPage.PerformLayout();
            this.broadcastContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage broadcastTabPage;
        private System.Windows.Forms.Label labelBody;
        private System.Windows.Forms.TextBox textBoxReport;
        private System.Windows.Forms.Label labelReport;
        private System.Windows.Forms.TextBox textBoxSubject;
        private System.Windows.Forms.Label labelSubject;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.TextBox textBoxBody;
        private System.Windows.Forms.ContextMenuStrip broadcastContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newBroadcastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeBroadcastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.Label labelQuery;
        private System.Windows.Forms.TextBox queryTextBox;

    }
}