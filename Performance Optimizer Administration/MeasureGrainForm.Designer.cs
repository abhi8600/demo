namespace Performance_Optimizer_Administration
{
    partial class MeasureGrainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ok = new System.Windows.Forms.Button();
            this.measureGrainDataGridView = new System.Windows.Forms.DataGridView();
            this.measureTable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dimension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measureGrainMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancel = new System.Windows.Forms.Button();
            this.levelListBox = new System.Windows.Forms.ListView();
            this.dimNameCol = new System.Windows.Forms.ColumnHeader();
            this.levelColumn = new System.Windows.Forms.ColumnHeader();
            this.addButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.measureGrainDataGridView)).BeginInit();
            this.measureGrainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ok
            // 
            this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ok.Location = new System.Drawing.Point(223, 525);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 3;
            this.ok.Text = "OK";
            this.ok.UseVisualStyleBackColor = true;
            // 
            // measureGrainDataGridView
            // 
            this.measureGrainDataGridView.AllowUserToAddRows = false;
            this.measureGrainDataGridView.AllowUserToDeleteRows = false;
            this.measureGrainDataGridView.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureGrainDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.measureGrainDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureGrainDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.measureTable,
            this.dimension,
            this.level});
            this.measureGrainDataGridView.ContextMenuStrip = this.measureGrainMenu;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.measureGrainDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.measureGrainDataGridView.Location = new System.Drawing.Point(13, 40);
            this.measureGrainDataGridView.Name = "measureGrainDataGridView";
            this.measureGrainDataGridView.ReadOnly = true;
            this.measureGrainDataGridView.Size = new System.Drawing.Size(609, 218);
            this.measureGrainDataGridView.TabIndex = 2;
            this.measureGrainDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.measureGrainDataGridView_CellBeginEdit);
            this.measureGrainDataGridView.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureGrainDataGridView_RowEnter);
            // 
            // measureTable
            // 
            this.measureTable.HeaderText = "Measure Table";
            this.measureTable.Name = "measureTable";
            this.measureTable.ReadOnly = true;
            this.measureTable.Width = 250;
            // 
            // dimension
            // 
            this.dimension.HeaderText = "Dimension";
            this.dimension.Name = "dimension";
            this.dimension.ReadOnly = true;
            this.dimension.Width = 200;
            // 
            // level
            // 
            this.level.HeaderText = "Level";
            this.level.Name = "level";
            this.level.ReadOnly = true;
            // 
            // measureGrainMenu
            // 
            this.measureGrainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem});
            this.measureGrainMenu.Name = "measureGrainMenu";
            this.measureGrainMenu.Size = new System.Drawing.Size(125, 26);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // cancel
            // 
            this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel.Location = new System.Drawing.Point(353, 525);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 4;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            // 
            // levelListBox
            // 
            this.levelListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.levelListBox.CheckBoxes = true;
            this.levelListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dimNameCol,
            this.levelColumn});
            this.levelListBox.FullRowSelect = true;
            this.levelListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.levelListBox.Location = new System.Drawing.Point(13, 317);
            this.levelListBox.Name = "levelListBox";
            this.levelListBox.Size = new System.Drawing.Size(468, 178);
            this.levelListBox.TabIndex = 5;
            this.levelListBox.UseCompatibleStateImageBehavior = false;
            this.levelListBox.View = System.Windows.Forms.View.Details;
            this.levelListBox.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.levelListBox_ItemChecked);
            // 
            // dimNameCol
            // 
            this.dimNameCol.Text = "Dimension";
            this.dimNameCol.Width = 300;
            // 
            // levelColumn
            // 
            this.levelColumn.Text = "Level";
            this.levelColumn.Width = 150;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(506, 390);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 6;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Existing grains";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(431, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "To add new grains to the measure, select a combination of dimension/level and cli" +
                "ck Add.";
            // 
            // MeasureGrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 587);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.levelListBox);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.measureGrainDataGridView);
            this.Name = "MeasureGrainForm";
            this.Text = "MeasureGrainForm";
            ((System.ComponentModel.ISupportInitialize)(this.measureGrainDataGridView)).EndInit();
            this.measureGrainMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.DataGridView measureGrainDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn measureTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn dimension;
        private System.Windows.Forms.DataGridViewTextBoxColumn level;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.ListView levelListBox;
        private System.Windows.Forms.ColumnHeader dimNameCol;
        private System.Windows.Forms.ColumnHeader levelColumn;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip measureGrainMenu;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
    }
}