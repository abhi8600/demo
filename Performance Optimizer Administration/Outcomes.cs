using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Outcomes : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Outcomes";
        string categoryName = "Business Modeling";
        List<Outcome> outcomeList;
        Dictionary<Pattern, List<OutcomeFactor>> valuesList = new Dictionary<Pattern, List<OutcomeFactor>>();
        OutcomeFactor[] factors = null;
        ClassifierSettings cSettings;

        public Outcomes(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            outcomeList = new List<Outcome>();
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (outcomeList != null)
            {
                r.Outcomes = (Outcome[])outcomeList.ToArray();
            }
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Outcomes != null)
            {
                outcomeList = new List<Outcome>(r.Outcomes);
            }
            else
            {
                outcomeList = new List<Outcome>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (Outcome o in outcomeList)
            {
                TreeNode tn = new TreeNode(o.Name);
                tn.ImageIndex = 19;
                tn.SelectedImageIndex = 19;
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            if (outcomeList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = outcomeMenuStrip;
                removeOutcomeToolStripMenuItem.Visible = false;
                return false;
            }
            if (selNode != null)
                implicitSave();
            selIndex = rootNode.Nodes.IndexOf(node);
            if (selIndex >= 0)
            {
                this.selNode = node;
                indexChanged(selIndex);
                ma.mainTree.ContextMenuStrip = outcomeMenuStrip;
                removeOutcomeToolStripMenuItem.Visible = true;
            }
            return true;
        }

        public void setup()
        {
            List<SuccessModel> smlist = ma.smodels.modelList;
            if (smlist != null)
            {
                outcomeModel.Items.Clear();
                foreach (SuccessModel sm in smlist)
                {
                    outcomeModel.Items.Add(sm.Name);
                }
            }

            foreach (QueryFilter f in ma.filterMod.filterList)
            {
                trainingFilterComboBox.Items.Add(f.Name);
                scoringFilterComboBox.Items.Add(f.Name);
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return outcomesTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (Outcome o in outcomeList)
            {
                if (match)
                {
                    if (o.TargetMeasure == find) o.TargetMeasure = replace;
                    if (o.ClassMeasure == find) o.ClassMeasure = replace;
                    if (o.MaxMeasure == find) o.MaxMeasure = replace;
                    if (o.MinMeasure == find) o.MinMeasure = replace;
                }
                else
                {
                    o.TargetMeasure = o.TargetMeasure.Replace(find, replace);
                    o.ClassMeasure = o.ClassMeasure.Replace(find, replace);
                    o.MaxMeasure = o.MaxMeasure.Replace(find, replace);
                    o.MinMeasure = o.MinMeasure.Replace(find, replace);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (Outcome o in outcomeList)
            {
                if (match)
                {
                    if (o.TargetMeasure == find) results.Add("Outcome: " + o.Name + " - Target measure");
                    if (o.ClassMeasure == find) results.Add("Outcome: " + o.Name + " - Class measure");
                    if (o.MaxMeasure == find) results.Add("Outcome: " + o.Name + " - Max measure");
                    if (o.MinMeasure == find) results.Add("Outcome: " + o.Name + " - Min measure");
                }
                else
                {
                    if (o.TargetMeasure != null && o.TargetMeasure.IndexOf(find) >= 0) results.Add("Outcome: " + o.Name + " - Target measure");
                    if (o.ClassMeasure != null && o.ClassMeasure.IndexOf(find) >= 0) results.Add("Outcome: " + o.Name + " - Class measure");
                    if (o.MaxMeasure != null && o.MaxMeasure.IndexOf(find) >= 0) results.Add("Outcome: " + o.Name + " - Max measure");
                    if (o.MinMeasure != null && o.MinMeasure.IndexOf(find) >= 0) results.Add("Outcome: " + o.Name + " - Min measure");
                }
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateOutcome();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        private Outcome getOutcome()
        {
            Outcome o = new Outcome();
            if (selNode != null)
                o.Name = selNode.Text;
            else
                o.Name = "New Outcome";
            o.OutcomeGroupName = outcomeGroup.Text;
            o.OutcomeGroupTableName = outcomeGroupTable.Text;
            o.SuccessModelName = outcomeModel.Text;
            o.IterationAnalysisValue = outcomeIterationValue.Text;
            o.IterationModelValue = outcomeModelIterationValue.Text;
            o.OutputTableName = outcomeTableName.Text;
            if (nameFilterBox.Text.Length > 0)
                o.NameFilter = nameFilterBox.Text;
            o.TargetMeasure = targetOverrideBox.Text.Length>0?targetOverrideBox.Text:null;
            o.ClassMeasure = classMeasureBox.Text;
            o.EstimateClass = estimateClass.Checked;
            o.EstimateValue = estimateValue.Checked;
            o.IncludeAllNonzero = includeAllNonzero.Checked;
            o.BiasToUniformClass = biasUniform.Checked;
            o.EstimateValueInClass = estimateValueInClass.Checked;
            o.EstimateDifferenceFromActual = estimateDifferenceFromActual.Checked;
            o.DropExistingTable = dropOutcomeTable.Checked;
            o.UseMinEstimatedValue = useMinOutcomeVal.Checked;
            o.ExcludeIfBelowMinValue = excludeBelowMin.Checked;
            o.RelativeMinValue = minOutcomeRelative.Checked;
            o.MinRelativeToMeasure = minOutcomeRelMeasure.Checked;
            o.MinMeasure = minOutcomeMeasure.Text;
            o.UseMaxEstimatedValue = useMaxOutcomeVal.Checked;
            try
            {
                o.MinEstimatedValue = Double.Parse(minOutcomeVal.Text);
                o.MaxEstimatedValue = Double.Parse(maxOutcomeVal.Text);
            }
            catch (Exception)
            {
                o.MinEstimatedValue = 0;
                o.MaxEstimatedValue = 0;
            }
            o.ExcludeIfAboveMaxValue = excludeAboveMax.Checked;
            o.RelativeMaxValue = maxOutcomeRelative.Checked;
            o.MaxRelativeToMeasure = maxOutcomeRelMeasure.Checked;
            o.MaxMeasure = maxOutcomeMeasure.Text;
            o.OutputOnlyClass = outputOnlyClass.Checked;
            o.OutputBuckets = bucketOutcome.Checked;
            o.OutputBucketType = bucketOutcomeValue.Checked ? 0 : (bucketOutcomeDifference.Checked ? 1 : 2);
            o.Buckets = (MeasureBucket[])outcomeBucketList.Tag;
            o.RankFactors = rankFactors.Checked;
            o.NumFactorsToRank = Int32.Parse(numToRank.Text);
            List<OutcomeFactor> ofList = new List<OutcomeFactor>();
            foreach (List<OutcomeFactor> list in valuesList.Values)
            {
                ofList.AddRange(list);
            }

            foreach (DataGridViewRow dr in factorDataView.Rows)
            {
                if ((string)dr.Cells["BaseValue"].Value != "Current" || (string)dr.Cells["TestValue"].Value != "Current")
                {
                    if ((string)dr.Cells["BaseValue"].Value != "Specific Value" && (string)dr.Cells["TestValue"].Value != "Specific Value")
                    {
                        OutcomeFactor of = new OutcomeFactor((Pattern)dr.Tag);
                        if ((string)dr.Cells["BaseValue"].Value == "Current")
                            of.Base = OutcomeFactor.SettingType.Current;
                        else if ((string)dr.Cells["BaseValue"].Value == "Average")
                            of.Base = OutcomeFactor.SettingType.Average;
                        else if ((string)dr.Cells["BaseValue"].Value == "Zero")
                            of.Base = OutcomeFactor.SettingType.Zero;
                        if ((string)dr.Cells["TestValue"].Value == "Current")
                            of.Test = OutcomeFactor.SettingType.Current;
                        else if ((string)dr.Cells["TestValue"].Value == "Average")
                            of.Test = OutcomeFactor.SettingType.Average;
                        else if ((string)dr.Cells["TestValue"].Value == "Zero")
                            of.Test = OutcomeFactor.SettingType.Zero;
                        ofList.Add(of);
                    }
                }
            }
            o.Factors = ofList.ToArray();

            List<ClassificationRule> crList = new List<ClassificationRule>();
            foreach (DataGridViewRow dr in ruleGrid.Rows)
            {
                ClassificationRule rule = new ClassificationRule();
                rule.Priority = (string) dr.Cells["Priority"].Value;
                rule.Filter = (string) dr.Cells["Filter"].Value;
                rule.Explanation = (string) dr.Cells["Explanation"].Value;
                if (rule.Priority == null) break;
                crList.Add(rule);
            }
            o.RuleSet = crList.ToArray();

            crList = new List<ClassificationRule>();
            foreach (DataGridViewRow dr in exclusionRulesGrid.Rows)
            {
                ClassificationRule rule = new ClassificationRule();
                rule.Priority = (string)dr.Cells["PriorityExclusion"].Value;
                rule.Filter = (string)dr.Cells["FilterExclusion"].Value;
                rule.Explanation = (string)dr.Cells["ExplanationExclusion"].Value;
                if (rule.Priority == null) break;
                crList.Add(rule);
            }
            o.ExclusionRuleSet = crList.ToArray();
            o.Classifiers = cSettings;
            o.TrainingFilter = trainingFilterComboBox.Text;
            o.ScoringFilter = scoringFilterComboBox.Text;
            return (o);
        }

        private void addOutcome_Click(object sender, System.EventArgs e)
        {
            Outcome o = getOutcome();
            if (outcomeList == null)
            {
                outcomeList = new List<Outcome>();
            }
            outcomeList.Add(o);
            o.Name = "New Outcome";
            o.OutcomeGroupName = outcomeGroup.Text;
            o.OutcomeGroupTableName = outcomeGroupTable.Text;
            TreeNode tn = new TreeNode(o.Name);
            tn.ImageIndex = 19;
            tn.SelectedImageIndex = 19;
            rootNode.Nodes.Add(tn);
        }

        private void updateOutcome()
        {
            if (selNode == null || selIndex < 0 || selIndex >= outcomeList.Count) return;
            Outcome o = getOutcome();
            outcomeList[selIndex] = o;
            selNode.Text = o.Name;
        }

        private void removeOutcome_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            outcomeList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void indexChanged(int index)
        {
            if (index < 0) return;
            Outcome o = outcomeList[index];
            outcomeGroup.Text = o.OutcomeGroupName;
            outcomeGroupTable.Text = o.OutcomeGroupTableName;
            outcomeModel.Text = o.SuccessModelName;
            outcomeIterationValue.Text = o.IterationAnalysisValue;
            outcomeModelIterationValue.Text = o.IterationModelValue;
            outcomeTableName.Text = o.OutputTableName;
            if (o.NameFilter != null && o.NameFilter.Length > 0)
                nameFilterBox.Text = o.NameFilter;
            else
                nameFilterBox.Text = "";
            estimateClass.Checked = o.EstimateClass;
            estimateValue.Checked = o.EstimateValue;
            includeAllNonzero.Checked = o.IncludeAllNonzero;
            biasUniform.Checked = o.BiasToUniformClass;
            estimateValueInClass.Checked = o.EstimateValueInClass;
            estimateDifferenceFromActual.Checked = o.EstimateDifferenceFromActual;
            targetOverrideBox.Text = o.TargetMeasure == null ? "" : o.TargetMeasure;
            classMeasureBox.Text = o.ClassMeasure;
            dropOutcomeTable.Checked = o.DropExistingTable;
            useMinOutcomeVal.Checked = o.UseMinEstimatedValue;
            minOutcomeVal.Text = o.MinEstimatedValue.ToString();
            excludeBelowMin.Checked = o.ExcludeIfBelowMinValue;
            minOutcomeRelative.Checked = o.RelativeMinValue;
            minOutcomeRelMeasure.Checked = o.MinRelativeToMeasure;
            minOutcomeMeasure.Text = o.MinMeasure;
            useMaxOutcomeVal.Checked = o.UseMaxEstimatedValue;
            maxOutcomeVal.Text = o.MaxEstimatedValue.ToString();
            excludeAboveMax.Checked = o.ExcludeIfAboveMaxValue;
            maxOutcomeRelative.Checked = o.RelativeMaxValue;
            maxOutcomeRelMeasure.Checked = o.MaxRelativeToMeasure;
            maxOutcomeMeasure.Text = o.MaxMeasure;
            outputOnlyClass.Checked = o.OutputOnlyClass;
            bucketOutcome.Checked = o.OutputBuckets;
            bucketOutcomeValue.Checked = o.OutputBucketType == 0;
            bucketOutcomeDifference.Checked = o.OutputBucketType == 1;
            bucketOutcomePctDiff.Checked = o.OutputBucketType == 2;
            outcomeBucketList.Tag = o.Buckets;
            outcomeBucketList.Items.Clear();
            numToRank.Text = o.NumFactorsToRank.ToString();
            rankFactors.Checked = o.RankFactors;
            if (o.Buckets != null)
                foreach (MeasureBucket mb in o.Buckets)
                {
                    outcomeBucketList.Items.Add(new ListViewItem(new string[] { mb.Name, mb.Min, mb.Max }));
                }
            factors = o.Factors;
            setSuccessModelOptions();

            ruleGrid.Rows.Clear();
            if (o.RuleSet != null)
            {
                foreach (ClassificationRule rule in o.RuleSet)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewTextBoxCell pr = new DataGridViewTextBoxCell();
                    pr.Value = rule.Priority;
                    row.Cells.Add(pr);
                    DataGridViewComboBoxCell filter = new DataGridViewComboBoxCell();
                    foreach (QueryFilter f in ma.filterMod.filterList)
                    {
                        filter.Items.Add(f.Name);
                    }
                    filter.Value = rule.Filter;
                    row.Cells.Add(filter);
                    DataGridViewTextBoxCell exp = new DataGridViewTextBoxCell();
                    exp.Value = rule.Explanation;
                    row.Cells.Add(exp);
                    ruleGrid.Rows.Add(row);
                }
            }

            exclusionRulesGrid.Rows.Clear();
            if (o.ExclusionRuleSet != null)
            {
                foreach (ClassificationRule rule in o.ExclusionRuleSet)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewTextBoxCell pr = new DataGridViewTextBoxCell();
                    pr.Value = rule.Priority;
                    row.Cells.Add(pr);
                    DataGridViewComboBoxCell filter = new DataGridViewComboBoxCell();
                    foreach (QueryFilter f in ma.filterMod.filterList)
                    {
                        filter.Items.Add(f.Name);
                    }
                    filter.Value = rule.Filter;
                    row.Cells.Add(filter);
                    DataGridViewTextBoxCell exp = new DataGridViewTextBoxCell();
                    exp.Value = rule.Explanation;
                    row.Cells.Add(exp);
                    exclusionRulesGrid.Rows.Add(row);
                }
            }
            cSettings = o.Classifiers;
            trainingFilterComboBox.Text = o.TrainingFilter;
            scoringFilterComboBox.Text = o.ScoringFilter;
        }

        private SuccessModel getSuccessModel(string name)
        {
            List<SuccessModel> smlist = ma.smodels.modelList;
            if (smlist != null)
            {
                foreach (SuccessModel sm in smlist)
                {
                    if (sm.Name == name)
                    {
                        return (sm);
                    }
                }
            }
            return (null);
        }

        private SuccessModel getCurrentSuccessModel()
        {
            if (outcomeModel.Text.Length > 0)
            {
                classMeasureBox.Items.Clear();
                minOutcomeMeasure.Items.Clear();
                maxOutcomeMeasure.Items.Clear();
                return (getSuccessModel(outcomeModel.Text));
            }
            return (null);
        }

        private void setSuccessModelOptions()
        {
            SuccessModel sm = getCurrentSuccessModel();
            factorDataView.Rows.Clear();
            if (sm != null)
            {
                List<Pattern> plist = sm.getPatterns(ma.smodels.modelList);
                List<DataGridViewRow> rlist = new List<DataGridViewRow>();
                valuesList.Clear();
                foreach (Pattern p in plist)
                {
                    if (p.Type == Pattern.MEASUREONLY)
                    {
                        targetOverrideBox.Items.Add(p.Measure);
                        classMeasureBox.Items.Add(p.Measure);
                        minOutcomeMeasure.Items.Add(p.Measure);
                        maxOutcomeMeasure.Items.Add(p.Measure);
                    }
                    bool found = false;
                    if (factors != null)
                    {
                        // build up valuesList
                        foreach (OutcomeFactor of in factors)
                        {
                            if (of.matchesPattern(p))
                            {
                                List<OutcomeFactor> oflist = null;
                                if (!valuesList.ContainsKey(p))
                                {
                                    oflist = new List<OutcomeFactor>();
                                    valuesList[p] = oflist;
                                }
                                else
                                    oflist = valuesList[p];
                                oflist.Add(of);
                            }
                        }
                        foreach (OutcomeFactor of in factors)
                        {
                            if (of.matchesPattern(p))
                            {
                                found = true;
                                factorDataView.Rows.Add(of.getRow());
                                int row = factorDataView.Rows.Count - 1;
                                factorDataView.Rows[row].Tag = p;

                                if (valuesList.ContainsKey(p))
                                {

                                    if (factorDataView.Rows[row].Cells[1].Value.Equals("Specific Value"))
                                    {
                                        string lstbase = getOutcomeListString(valuesList[p], false);
                                        factorDataView.Rows[row].Cells[2].Value = lstbase;
                                    }
                                    if (factorDataView.Rows[row].Cells[3].Value.Equals("Specific Value"))
                                    {
                                        string lsttest = getOutcomeListString(valuesList[p], true);
                                        factorDataView.Rows[row].Cells[4].Value = lsttest;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (!found)
                    {
                        factorDataView.Rows.Add(new Object[] { OutcomeFactor.getName(p), "Current", null, "Current", null });
                        factorDataView.Rows[factorDataView.Rows.Count - 1].Tag = p;
                    }
                }
            }
        }

        private void outcomeModel_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            setSuccessModelOptions();
        }

        private void addOutcomeBucket_Click(object sender, System.EventArgs e)
        {
            MeasureBucket mb = new MeasureBucket();
            mb.Name = outcomeBucketName.Text;
            mb.Max = outcomeBucketMax.Text;
            mb.Min = outcomeBucketMin.Text;
            outcomeBucketList.Items.Add(new ListViewItem(new string[] { mb.Name, mb.Min, mb.Max }));
            if (outcomeBucketList.Tag == null)
            {
                outcomeBucketList.Tag = new MeasureBucket[] { mb };
            }
            else
            {
                ArrayList list = new ArrayList((MeasureBucket[])outcomeBucketList.Tag);
                list.Add(mb);
                outcomeBucketList.Tag = list.ToArray(typeof(MeasureBucket));
            }
        }

        private void removeOutcomeBucket_Click(object sender, System.EventArgs e)
        {
            if (outcomeBucketList.SelectedIndices.Count == 0) return;
            ArrayList list = new ArrayList((MeasureBucket[])outcomeBucketList.Tag);
            list.RemoveAt(outcomeBucketList.SelectedIndices[0]);
            outcomeBucketList.Tag = list.ToArray(typeof(MeasureBucket));
            outcomeBucketList.Items.Remove(outcomeBucketList.SelectedItems[0]);
        }

        private string getOutcomeListString(List<OutcomeFactor> oflist, bool test)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < oflist.Count; i++)
            {
                if (i > 0) sb.Append(',');
                sb.Append(oflist[i].ToString(test));
            }
            return (sb.ToString());
        }

        private void factorDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == factorDataView.Columns["BaseValues"].Index)
            {
                DataGridViewRow dr = factorDataView.CurrentRow;
                if (dr.Cells["BaseValue"].Value.ToString() == "Specific Value")
                {
                    Pattern p = (Pattern)dr.Tag;
                    OutcomeValues ov = new OutcomeValues(p, valuesList.ContainsKey(p) ? valuesList[p] : null, false);
                    ov.ShowDialog();
                    List<OutcomeFactor> oflist = ov.getFactors();
                    foreach (OutcomeFactor of in oflist)
                    {
                        of.Base = OutcomeFactor.getSetting((string)dr.Cells["BaseValue"].Value);
                        of.Test = OutcomeFactor.getSetting((string)dr.Cells["TestValue"].Value);
                    }
                    dr.Cells[2].Value = getOutcomeListString(oflist, false);
                    valuesList[p] = oflist;
                }
            }
            else if (e.ColumnIndex == factorDataView.Columns["TestValues"].Index)
            {
                DataGridViewRow dr = factorDataView.CurrentRow;
                if (dr.Cells["TestValue"].Value.ToString() == "Specific Value")
                {
                    Pattern p = (Pattern)dr.Tag;
                    OutcomeValues ov = new OutcomeValues(p, valuesList.ContainsKey(p) ? valuesList[p] : null, true);
                    ov.ShowDialog();
                    List<OutcomeFactor> oflist = ov.getFactors();
                    foreach (OutcomeFactor of in oflist)
                    {
                        of.Base = OutcomeFactor.getSetting((string)dr.Cells["BaseValue"].Value);
                        of.Test = OutcomeFactor.getSetting((string)dr.Cells["TestValue"].Value);
                    }
                    dr.Cells[4].Value = getOutcomeListString(oflist, true);
                    valuesList[p] = oflist;
                }
            }
        }

        private void currentBase_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["BaseValue"].Value = "Current";
            }
        }

        private void averageBase_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["BaseValue"].Value = "Average";
            }
        }

        private void zeroBase_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["BaseValue"].Value = "Zero";
            }
        }

        private void currentTest_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["TestValue"].Value = "Current";
            }
        }

        private void averageTest_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["TestValue"].Value = "Average";
            }
        }

        private void zeroTest_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in factorDataView.SelectedCells)
            {
                cell.OwningRow.Cells["TestValue"].Value = "Zero";
            }
        }

        private void outcomesTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }


        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                e.Node.Text = e.Label;
            }
        }

        #endregion

        private void classifierSettings_Click(object sender, EventArgs e)
        {
            BestFitClassifierDialog bfcd = new BestFitClassifierDialog(ma, cSettings == null ? ma.modelMod.classifierDialog.getClassifierSettings() : cSettings, true);
            bfcd.ShowDialog();
            if (bfcd.isDefault)
                cSettings = null;
            else
                cSettings = bfcd.getClassifierSettings();
        }

        private void ruleGrid_rowEntered(object sender, DataGridViewCellEventArgs e)
        {
            foreach (QueryFilter f in ma.filterMod.filterList)
            {
                ((DataGridViewComboBoxCell) ruleGrid.Rows[e.RowIndex].Cells["Filter"]).Items.Add(f.Name);
            }
        }

        private void exclusionRuleGrid_rowEntered(object sender, DataGridViewCellEventArgs e)
        {
            foreach (QueryFilter f in ma.filterMod.filterList)
            {
                ((DataGridViewComboBoxCell)exclusionRulesGrid.Rows[e.RowIndex].Cells["FilterExclusion"]).Items.Add(f.Name);
            }
        }

        private void ruleGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            ruleGrid.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }
    }
}