using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class Scripts : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        private string moduleName = "Script Groups";
        private string categoryName = "Warehouse";
        private List<ScriptGroup> scriptGroupList;
        private int imageIndex = 30;

        public Scripts(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            scriptGroupList = new List<ScriptGroup>(0);
        }


        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.ScriptGroups = (ScriptGroup[])scriptGroupList.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.ScriptGroups != null)
            {
                scriptGroupList = new List<ScriptGroup>(r.ScriptGroups);
            }
            else
            {
                scriptGroupList = new List<ScriptGroup>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public List<ScriptGroup> getScriptGroupList()
        {
            return scriptGroupList;
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (ScriptGroup sg in scriptGroupList)
            {
                TreeNode tn = new TreeNode(sg.Name);
                tn.ImageIndex = imageIndex;
                tn.SelectedImageIndex = imageIndex;
                if (sg.Scripts != null)
                {
                    foreach (Script s in sg.Scripts)
                    {
                        TreeNode sn = new TreeNode(s.Name);
                        sn.ImageIndex = imageIndex;
                        sn.SelectedImageIndex = imageIndex;
                        tn.Nodes.Add(sn);
                    }
                }
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode selNode;
        int selIndex;

        private void setScreen(bool isVisible)
        {
            scriptLabel.Visible = isVisible;
            scriptTextBox.Visible = isVisible;
        }

        public bool selectedNode(TreeNode node)
        {
            ma.mainTree.ContextMenuStrip = scriptContextMenuStrip;
           
            if (scriptGroupList.Count == 0 || node.Parent.Parent == null)
            {
                setScreen(false);
                scriptUpToolStripMenuItem.Visible = false;
                scriptDownToolStripMenuItem.Visible = false;
                addScriptGroupToolStripMenuItem.Visible = true;
                addScriptToolStripMenuItem.Visible = false;
                removeScriptGroupToolStripMenuItem.Visible = false;
                removeScriptToolStripMenuItem.Visible = false;
                return true;
            }
            else if (node.Parent == rootNode)
            {
                implicitSave();
                selIndex = rootNode.Nodes.IndexOf(node);
                selNode = node;
                indexChanged();
                setScreen(false);
                scriptUpToolStripMenuItem.Visible = false;
                scriptDownToolStripMenuItem.Visible = false;
                addScriptGroupToolStripMenuItem.Visible = true;
                addScriptToolStripMenuItem.Visible = true;
                removeScriptGroupToolStripMenuItem.Visible = scriptGroupList.Count > 0;
                removeScriptToolStripMenuItem.Visible = false;
                return true;
            }
            else if (node.Parent.Parent == rootNode)
            {
                implicitSave();
                selIndex = node.Parent.Nodes.IndexOf(node);
                selNode = node;
                indexChanged();
                setScreen(true);
                scriptUpToolStripMenuItem.Visible = selNode.Parent.Nodes.Count > 1;
                scriptUpToolStripMenuItem.Enabled = (selIndex > 0);
                scriptDownToolStripMenuItem.Visible = selNode.Parent.Nodes.Count > 1;
                scriptDownToolStripMenuItem.Enabled = (selNode.Parent.LastNode != selNode);
                addScriptGroupToolStripMenuItem.Visible = false;
                addScriptToolStripMenuItem.Visible = true;
                removeScriptGroupToolStripMenuItem.Visible = false;
                removeScriptToolStripMenuItem.Visible = selNode.Parent.Nodes.Count > 0;
                return true;
            }

            return false; 
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return scriptTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {

        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (turnOffimplicitSave) return;
            updateScript();
            updateScriptGroup();
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                scriptGroupList[selIndex].Name = e.Label;
            }
            else if (e.Node.Parent.Parent == rootNode)
            {
                TreeNode sgNode = selNode.Parent;
                int index = rootNode.Nodes.IndexOf(sgNode);
                selNode.Text = e.Label;
                scriptGroupList[index].Scripts[selIndex].Name = e.Label;
            }
        }

        #endregion

        private Script getScript()
        {
            Script s = new Script();
            if (selNode == null || selNode.Parent == null || selNode.Parent.Parent == null || selNode.Parent.Parent != rootNode) return null;
            s.Name = selNode.Text;
            s.Text = scriptTextBox.Text;
            return s;
        }
        private ScriptGroup getScriptGroup()
        {
            ScriptGroup sg = new ScriptGroup();
            if (selNode == null || selNode.Parent == null || selNode.Parent != rootNode) return null;

            int index = rootNode.Nodes.IndexOf(selNode);
            sg.Name = selNode.Text;
            sg.Scripts = scriptGroupList[index].Scripts;
            return sg;
        }


        private void indexChanged()
        {
            if (selIndex < 0) return;
            if (selNode == null) return;
            if (selNode.Parent == rootNode)
            {
            }
            else if (selNode.Parent.Parent == rootNode)
            {
                TreeNode pNode = selNode.Parent;
                int index = pNode.Parent.Nodes.IndexOf(pNode);
                Script s = scriptGroupList[index].Scripts[selIndex];
                scriptTextBox.Text = s.Text;
            }
        }

        private void updateScript()
        {
            if (selNode == null || selIndex < 0) return;
            if (selNode.Parent == null || selNode.Parent.Parent == null || selNode.Parent.Parent != rootNode) return;
            Script s = getScript();
            int sgindex = selNode.Parent.Parent.Nodes.IndexOf(selNode.Parent);
            int sindex = selNode.Parent.Nodes.IndexOf(selNode);
            scriptGroupList[sgindex].Scripts[sindex] = s;
            selNode.Text = s.Name;
        }

        private void updateScriptGroup()
        {
            if (selNode == null || selIndex < 0) return;
            if (selNode.Parent == null || selNode.Parent != rootNode) return;
            ScriptGroup sg = getScriptGroup();
            scriptGroupList[selIndex] = sg;
            selNode.Text = sg.Name;
        }

        private bool keyhandled;
        private void scriptTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyhandled)
            {
                e.Handled = true;
                keyhandled = false;
            }
        }

        private void scriptTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                scriptTextBox.SelectAll();
                keyhandled = true;
            }
        }



        private void addScriptGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScriptGroup sg = new ScriptGroup();
            sg.Name = "New Script Group";
            sg.Scripts = new Script[0];
            foreach (ScriptGroup sg2 in scriptGroupList)
            {
                if (sg.Name == sg2.Name) return;
            }
            scriptGroupList.Add(sg);
            TreeNode tn = new TreeNode(sg.Name);
            tn.ImageIndex = imageIndex;
            tn.SelectedImageIndex = imageIndex;
            rootNode.Nodes.Add(tn);

        }

        private void addScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Script s = new Script();
            s.Name = "New Script";
            Script[] scripts;
            List<Script> newScriptList;

            // get the scripts array for the group
            if (selNode.Parent == rootNode)
            {
                scripts = scriptGroupList[selIndex].Scripts;
            }
            else
            {
                TreeNode pNode = selNode.Parent;
                int pIndex = rootNode.Nodes.IndexOf(pNode);
                scripts = scriptGroupList[pIndex].Scripts;
            }

            if (scripts != null)
            {
                foreach (Script s2 in scripts)
                {
                    if (s2.Name == s.Name) return;
                }
                newScriptList = new List<Script>(scripts);
            }
            else
            {
                newScriptList = new List<Script>();
            }
            
            newScriptList.Add(s);
            TreeNode tn = new TreeNode(s.Name);
            tn.ImageIndex = imageIndex;
            tn.SelectedImageIndex = imageIndex;

            if (selNode.Parent == rootNode)
            {
                scriptGroupList[selIndex].Scripts = (Script[])newScriptList.ToArray();
                selNode.Nodes.Add(tn);
            }
            else
            {
                TreeNode pNode = selNode.Parent;
                int pIndex = rootNode.Nodes.IndexOf(pNode);
                scriptGroupList[pIndex].Scripts = (Script[])newScriptList.ToArray();
                pNode.Nodes.Add(tn);
            } 
        }

        private void removeScriptGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            scriptGroupList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void removeScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent == null || selNode.Parent.Parent == null || selNode.Parent.Parent != rootNode) return;
            turnOffimplicitSave = true;
            TreeNode pNode = selNode.Parent;
            int pindex = pNode.Parent.Nodes.IndexOf(pNode);
            List<Script> newScriptList = new List<Script>(scriptGroupList[pindex].Scripts);
            newScriptList.RemoveAt(selIndex);
            scriptGroupList[pindex].Scripts = (Script[])newScriptList.ToArray();
            selNode.Remove();
            turnOffimplicitSave = false;

        }

        private void scriptDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent == null || selNode.Parent.Parent == null || selNode.Parent.Parent != rootNode) return;
            if (selIndex == selNode.Parent.Nodes.Count - 1) return;

            turnOffimplicitSave = true;

            Script s = getScript();
            int sIndex = selIndex;
            TreeNode pNode = selNode.Parent;
            TreeNode node = pNode.Nodes[sIndex];
            int pIndex = rootNode.Nodes.IndexOf(pNode);

            pNode.Nodes.RemoveAt(sIndex);
            pNode.Nodes.Insert(sIndex + 1, node);

            List<Script> newScriptList = new List<Script>(scriptGroupList[pIndex].Scripts);
            newScriptList.RemoveAt(sIndex);
            newScriptList.Insert(sIndex + 1, s);
            scriptGroupList[pIndex].Scripts = (Script[])newScriptList.ToArray();

            ma.mainTree.SelectedNode = node;
            turnOffimplicitSave = false;

        }

        private void scriptUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent == null || selNode.Parent.Parent == null || selNode.Parent.Parent != rootNode) return;
            if (selIndex == 0) return;

            turnOffimplicitSave = true;

            Script s = getScript();
            int sIndex = selIndex;
            TreeNode pNode = selNode.Parent;
            TreeNode node = pNode.Nodes[sIndex];
            int pIndex = rootNode.Nodes.IndexOf(pNode);

            pNode.Nodes.RemoveAt(sIndex);
            pNode.Nodes.Insert(sIndex - 1, node);

            List<Script> newScriptList = new List<Script>(scriptGroupList[pIndex].Scripts);
            newScriptList.RemoveAt(sIndex);
            newScriptList.Insert(sIndex - 1, s);
            scriptGroupList[pIndex].Scripts = (Script[])newScriptList.ToArray();

            ma.mainTree.SelectedNode = node;
            turnOffimplicitSave = false;

        }
    }
}