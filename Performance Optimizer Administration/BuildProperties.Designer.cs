﻿namespace Performance_Optimizer_Administration
{
    partial class BuildPropertiesModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scriptTextBox = new System.Windows.Forms.TextBox();
            this.scriptLabel = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.postBuildReminderTabPage = new System.Windows.Forms.TabPage();
            this.postBuildReminderTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.postBuildReminderTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // scriptTextBox
            // 
            this.scriptTextBox.Location = new System.Drawing.Point(29, 63);
            this.scriptTextBox.Multiline = true;
            this.scriptTextBox.Name = "scriptTextBox";
            this.scriptTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.scriptTextBox.Size = new System.Drawing.Size(787, 400);
            this.scriptTextBox.TabIndex = 1;
            // 
            // scriptLabel
            // 
            this.scriptLabel.AutoSize = true;
            this.scriptLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scriptLabel.Location = new System.Drawing.Point(29, 25);
            this.scriptLabel.Name = "scriptLabel";
            this.scriptLabel.Size = new System.Drawing.Size(137, 16);
            this.scriptLabel.TabIndex = 0;
            this.scriptLabel.Text = "Type your script here:";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.postBuildReminderTabPage);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 1;
            // 
            // postBuildReminderTabPage
            // 
            this.postBuildReminderTabPage.AutoScroll = true;
            this.postBuildReminderTabPage.Controls.Add(this.postBuildReminderTextBox);
            this.postBuildReminderTabPage.Controls.Add(this.label1);
            this.postBuildReminderTabPage.Location = new System.Drawing.Point(4, 22);
            this.postBuildReminderTabPage.Name = "postBuildReminderTabPage";
            this.postBuildReminderTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.postBuildReminderTabPage.Size = new System.Drawing.Size(844, 707);
            this.postBuildReminderTabPage.TabIndex = 0;
            this.postBuildReminderTabPage.Text = "Post-Build Reminder";
            this.postBuildReminderTabPage.UseVisualStyleBackColor = true;
            // 
            // postBuildReminderTextBox
            // 
            this.postBuildReminderTextBox.Location = new System.Drawing.Point(29, 63);
            this.postBuildReminderTextBox.Multiline = true;
            this.postBuildReminderTextBox.Name = "postBuildReminderTextBox";
            this.postBuildReminderTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.postBuildReminderTextBox.Size = new System.Drawing.Size(787, 400);
            this.postBuildReminderTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type your post-build reminder here:";
            // 
            // BuildPropertiesModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "BuildPropertiesModule";
            this.Text = "Build Properties";
            this.tabControl.ResumeLayout(false);
            this.postBuildReminderTabPage.ResumeLayout(false);
            this.postBuildReminderTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox scriptTextBox;
        private System.Windows.Forms.Label scriptLabel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage postBuildReminderTabPage;
        private System.Windows.Forms.TextBox postBuildReminderTextBox;
        private System.Windows.Forms.Label label1;
    }
}