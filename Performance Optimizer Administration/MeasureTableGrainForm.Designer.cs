namespace Performance_Optimizer_Administration
{
    partial class MeasureTableGrainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.close = new System.Windows.Forms.Button();
            this.measureTableGrainDataGridView = new System.Windows.Forms.DataGridView();
            this.dimension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.measureTableGrainDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // close
            // 
            this.close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.close.Location = new System.Drawing.Point(175, 289);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 1;
            this.close.Text = "Close";
            this.close.UseVisualStyleBackColor = true;
            // 
            // measureTableGrainDataGridView
            // 
            this.measureTableGrainDataGridView.AllowUserToAddRows = false;
            this.measureTableGrainDataGridView.AllowUserToDeleteRows = false;
            this.measureTableGrainDataGridView.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureTableGrainDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.measureTableGrainDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureTableGrainDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dimension,
            this.level});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.measureTableGrainDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.measureTableGrainDataGridView.Location = new System.Drawing.Point(26, 36);
            this.measureTableGrainDataGridView.Name = "measureTableGrainDataGridView";
            this.measureTableGrainDataGridView.ReadOnly = true;
            this.measureTableGrainDataGridView.Size = new System.Drawing.Size(394, 231);
            this.measureTableGrainDataGridView.TabIndex = 0;
            // 
            // dimension
            // 
            this.dimension.HeaderText = "Dimension";
            this.dimension.Name = "dimension";
            this.dimension.ReadOnly = true;
            this.dimension.Width = 200;
            // 
            // level
            // 
            this.level.HeaderText = "Level";
            this.level.Name = "level";
            this.level.ReadOnly = true;
            this.level.Width = 150;
            // 
            // MeasureTableGrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 343);
            this.Controls.Add(this.close);
            this.Controls.Add(this.measureTableGrainDataGridView);
            this.Name = "MeasureTableGrainForm";
            this.Text = "Measure Table Grains";
            ((System.ComponentModel.ISupportInitialize)(this.measureTableGrainDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button close;
        private System.Windows.Forms.DataGridView measureTableGrainDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dimension;
        private System.Windows.Forms.DataGridViewTextBoxColumn level;
    }
}