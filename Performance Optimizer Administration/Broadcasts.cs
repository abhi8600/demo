using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class Broadcasts : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        private string moduleName = "Broadcasts";
        private string categoryName = "System";
        private List<Broadcast> broadcastList;
        private int imageIndex = 29;

        public Broadcasts(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            broadcastList = new List<Broadcast>(0);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.Broadcasts = (Broadcast[])broadcastList.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Broadcasts != null)
            {
                broadcastList = new List<Broadcast>(r.Broadcasts);
            }
            else
            {
                broadcastList = new List<Broadcast>();
            } 
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public List<Broadcast> getBroadcastList()
        {
            return broadcastList;
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (Broadcast bc in broadcastList)
            {
                TreeNode tn = new TreeNode(bc.Name);
                tn.ImageIndex = imageIndex;
                tn.SelectedImageIndex = imageIndex;
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode selNode;
        int selIndex;

        private void setScreen(bool isVisible)
        {
            this.textBoxBody.Visible = isVisible;
            this.textBoxFrom.Visible = isVisible;
            this.textBoxReport.Visible = isVisible;
            this.textBoxSubject.Visible = isVisible;
            this.labelBody.Visible = isVisible;
            this.labelFrom.Visible = isVisible;
            this.labelReport.Visible = isVisible;
            this.labelSubject.Visible = isVisible;
            this.queryTextBox.Visible = isVisible;
            this.labelQuery.Visible = isVisible;
        }

        public bool selectedNode(TreeNode node)
        {
            if (broadcastList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = broadcastContextMenuStrip;
                setScreen(false);
                upToolStripMenuItem.Visible = false;
                downToolStripMenuItem.Visible = false;
                removeBroadcastToolStripMenuItem.Visible = false;
                return true;
            }

            implicitSave();

            selIndex = rootNode.Nodes.IndexOf(node);
            selNode = node;
            indexChanged();
            setScreen(true);
            ma.mainTree.ContextMenuStrip = broadcastContextMenuStrip;
            upToolStripMenuItem.Visible = broadcastList.Count > 1;
            downToolStripMenuItem.Visible = broadcastList.Count > 1;
            removeBroadcastToolStripMenuItem.Visible = true;
            return true;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return broadcastTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateBroadcast();
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                broadcastList[selIndex].Name = e.Label;
            }
        }

        #endregion

        private Broadcast getBroadcast()
        {
            Broadcast bc = new Broadcast();
            bc.Name = selNode.Text;
            bc.From = textBoxFrom.Text;
            bc.Subject = textBoxSubject.Text;
            bc.Body = textBoxBody.Text;
            bc.Report = textBoxReport.Text;
            bc.Query = queryTextBox.Text;
            return bc;
        }


        private void indexChanged()
        {
            if (selIndex < 0) return;
            Broadcast bc = broadcastList[selIndex];
            textBoxFrom.Text = bc.From;
            textBoxSubject.Text = bc.Subject;
            textBoxBody.Text = bc.Body;
            textBoxReport.Text = bc.Report;
            queryTextBox.Text = bc.Query;
        }

        private bool broadcastIsValid(Broadcast bc)
        {
            if (bc.From == null || bc.From.Trim() == "") return false;
            if (bc.Subject == null || bc.Subject.Trim() == "") return false;
            if (bc.Query == null || bc.Query.Trim() == "") return false;
            if ((bc.Body == null || bc.Body.Trim() == "") && (bc.Report == null || bc.Body.Trim() == "")) return false;
            return true;
        }

        private void updateBroadcast()
        {
            if (selIndex < 0 || selIndex >= broadcastList.Count || selNode == null) return;
            Broadcast bc = getBroadcast();
            if (bc == null || !broadcastIsValid(bc)) return;
            broadcastList[selIndex] = bc;
            selNode.Text = bc.Name;
        }

        private bool keyhandled;
        private void broadcastBody_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyhandled)
            {
                e.Handled = true;
                keyhandled = false;
            }
        }

        private void broadcastBody_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                textBoxBody.SelectAll();
                keyhandled = true;
            }
        }

        private void newBroadcastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (broadcastList == null)
            {
                broadcastList = new List<Broadcast>();
            }
            Broadcast bc = new Broadcast();
            //bc.Name = "New Broadcast";
            foreach (Broadcast bc2 in broadcastList)
            {
                if (bc.Name == bc2.Name) return;
            }
            broadcastList.Add(bc);
            TreeNode tn = new TreeNode(bc.Name);
            tn.ImageIndex = imageIndex;
            tn.SelectedImageIndex = imageIndex;
            rootNode.Nodes.Add(tn);
        }

        private void removeBroadcastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            broadcastList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void upToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex > 0)
            {
                turnOffimplicitSave = true;
                TreeNode node = rootNode.Nodes[selIndex];
                rootNode.Nodes.RemoveAt(selIndex);
                rootNode.Nodes.Insert(selIndex - 1, node);
                Broadcast bc = broadcastList[selIndex];
                broadcastList.RemoveAt(selIndex);
                broadcastList.Insert(selIndex - 1, bc);
                ma.mainTree.SelectedNode = node;
                turnOffimplicitSave = false;
            }
        }

        private void downToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < broadcastList.Count - 1)
            {
                turnOffimplicitSave = true;
                TreeNode node = rootNode.Nodes[selIndex];
                rootNode.Nodes.RemoveAt(selIndex);
                rootNode.Nodes.Insert(selIndex + 1, node);
                Broadcast bc = broadcastList[selIndex];
                broadcastList.RemoveAt(selIndex);
                broadcastList.Insert(selIndex + 1, bc);
                ma.mainTree.SelectedNode = node;
                turnOffimplicitSave = false;
            }
        }

        private void textBoxFrom_Validating(object sender, CancelEventArgs e)
        {
            if (textBoxFrom.Text == null || textBoxFrom.Text.Trim() == "")
            {
                e.Cancel = true;
                MessageBox.Show(this, "Who is sending this broadcast?", "Broadcast: Error in From field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxSubject_Validating(object sender, CancelEventArgs e)
        {
            if (textBoxSubject.Text == null || textBoxSubject.Text.Trim() == "")
            {
                e.Cancel = true;
                MessageBox.Show(this, "What is the subject of this broadcast?", "Broadcast: Error in Subject field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxReport_Validating(object sender, CancelEventArgs e)
        {
            if ((textBoxBody.Text == null || textBoxBody.Text.Trim() == "") &&
                  (textBoxReport.Text == null || textBoxReport.Text.Trim() == ""))
            {
                e.Cancel = true;
                MessageBox.Show(this, "You must either specify a report or come up with a message for this broadcast.", "Broadcast: Error in Report/Body fields", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxBody_Validating(object sender, CancelEventArgs e)
        {
            if ((textBoxBody.Text == null || textBoxBody.Text.Trim() == "") &&
                  (textBoxReport.Text == null || textBoxReport.Text.Trim() == ""))
            {
                e.Cancel = true;
                MessageBox.Show(this, "You must either specify a report or come up with a message for this broadcast.", "Broadcast: Error in Report/Body fields", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void queryTextBox_validating(object sender, CancelEventArgs e)
        {
            if (queryTextBox.Text == null || queryTextBox.Text.Trim() == "")
            {
                e.Cancel = true;
                MessageBox.Show(this, "You must either specify a query for this broadcast.", "Broadcast: Error in Query field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}