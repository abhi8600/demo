﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("ForeignKey", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class ForeignKey
    {
        /*
         * Foreign key join types:
         * 0 - Inner Join
         * 1 - Left Outer Join
         * 2 - Right Outer Join
         * 3 - Full Outer Join
         */
        public const int INNER_JOIN = 0;
        public const int LEFT_OUTER_JOIN = 1;
        public const int RIGHT_OUTER_JOIN = 2;
        public const int FULL_OUTER_JOIN = 3;
        public string Source;
        public int Type;
        public string Condition;
        public bool Invalid;
        public bool Redundant;

        public static ForeignKey findKey(ForeignKey[] list, string name)
        {
            if (list == null)
                return null;
            foreach (ForeignKey fk in list)
            {
                if (fk.Source == name)
                    return fk;
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj is System.DBNull) return false;
            if (!typeof(ForeignKey).Equals(obj.GetType())) return false;
            ForeignKey other = (ForeignKey)obj;
            if (Source != other.Source) return false;
            if (Type != other.Type) return false;
            if (Condition != other.Condition) return false;
            if (Invalid != other.Invalid) return false;
            if (Redundant != other.Redundant) return false;
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string getDisplayString()
        {
            StringBuilder result = new StringBuilder("Foreign Key: [");
            result.Append(Source + ",");
            switch (Type)
            {
                case INNER_JOIN:
                    result.Append("Inner Join");
                    break;
                case LEFT_OUTER_JOIN:
                    result.Append("Left Outer Join");
                    break;
                case RIGHT_OUTER_JOIN:
                    result.Append("Right Outer Join");
                    break;
                case FULL_OUTER_JOIN:
                    result.Append("Full Outer Join");
                    break;
            }
            result.Append(',' + Condition + "," + (Invalid ? "Invalid" : "Valid") + (Redundant ? ",Redundant" : ""));
            result.Append(']');
            return result.ToString();
        }
        
        public string toSerializedString()
        {
            return Source + "|2" + Type + "|2" + Invalid + "|2" + Redundant + "|2" + (Condition != null && Condition.Length > 0 ? Condition : "");
        }

        public static ForeignKey getFromSerializedString(string s)
        {
            string[] parts = s.Split(new string[] {"|2"}, StringSplitOptions.None);
            if (parts.Length == 4)
            {
                ForeignKey fk = new ForeignKey();
                fk.Source = parts[0];
                bool.TryParse(parts[2], out fk.Invalid);
                bool.TryParse(parts[3], out fk.Redundant);
                if (int.TryParse(parts[1], out fk.Type))
                    return fk;
            }
            if (parts.Length == 5)
            {
                ForeignKey fk = new ForeignKey();
                fk.Source = parts[0];
                bool.TryParse(parts[2], out fk.Invalid);
                bool.TryParse(parts[3], out fk.Redundant);
                fk.Condition = parts[4];
                if (int.TryParse(parts[1], out fk.Type))
                    return fk;
            }
            return null;
        }

        public static string getStringFromArray(ForeignKey[] fkeys)
        {
            if (fkeys.Length == 0)
                return null;
            StringBuilder sb = new StringBuilder();
            foreach (ForeignKey fk in fkeys)
            {
                if (sb.Length > 0)
                    sb.Append('\r');
                sb.Append(fk.toSerializedString());
            }
            return sb.ToString();
        }

        public static ForeignKey[] getArrayFromString(string s)
        {
            string[] keys = s.Split('\n');
            List<ForeignKey> result = new List<ForeignKey>();
            {
                foreach (string key in keys)
                {
                    ForeignKey fk = ForeignKey.getFromSerializedString(key);
                    if (fk != null)
                        result.Add(fk);
                }
            }
            return result.ToArray();
        }
    }
}
