﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Performance_Optimizer_Administration
{
    [Serializable]
    public class HierarchyLevel
    {
        public string HierarchyName;
        public string LevelName;

        public HierarchyLevel()
        {
        }

        public HierarchyLevel(string hierarchyName, string levelName)
        {
            this.HierarchyName = hierarchyName;
            this.LevelName = levelName;
        }
    }
}
