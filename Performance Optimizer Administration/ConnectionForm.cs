using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class ConnectionForm : Form, RepositoryModule, RenameNode
    {
        private string moduleName = "Database Connection";
        private string categoryName = "Business Modeling";
        private AppConfig appconfig;
        UsersAndGroups usersandgroups;
        public List<DatabaseConnection> connectionList;
        TreeNode rootNode;
        MainAdminForm ma;

        public ConnectionForm(MainAdminForm ma, AppConfig appconfig, UsersAndGroups usersandgroups)
        {
            InitializeComponent();
            this.ma = ma;
            this.appconfig = appconfig;
            this.usersandgroups = usersandgroups;
        }

        public static void setConnectInfo(DatabaseConnection cp, string dbservername, string port, string dbdatabase)
        {
            if (cp.Driver == "com.mysql.jdbc.Driver")
            {
                cp.ConnectString = "jdbc:mysql://" + dbservername;
                if (port != null && port.Length > 0)
                    cp.ConnectString = cp.ConnectString + ':' + port;
                if (dbdatabase != null && dbdatabase.Length > 0)
                    cp.ConnectString = cp.ConnectString + '/' + dbdatabase;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "com.microsoft.jdbc.sqlserver.SQLServerDriver")
            {
                // SQL Server 2000
                string dbtext = "";
                if (dbdatabase.Length > 0)
                    dbtext = ";databaseName=" + dbdatabase;

                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                cp.ConnectString = "jdbc:microsoft:sqlserver://" + dbservername + portText + dbtext;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            {
                string dbtext = "";
                if (dbdatabase.Length > 0)
                    dbtext = ";databaseName=" + dbdatabase;

                String portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                cp.ConnectString = "jdbc:sqlserver://" + dbservername + portText + dbtext;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "sun.jdbc.odbc.JdbcOdbcDriver")
            {
                cp.ConnectString = "jdbc:odbc:" + dbservername;
            }
            else if (cp.Driver == "oracle.jdbc.OracleDriver")
            {
                cp.ConnectString = "jdbc:oracle:thin:@" + dbservername + ":" + (port != null && port.Length > 0 ? port : "1521") + ":" + dbdatabase;
            }
            else if (cp.Driver == "GoogleAnalytics")
            {
                cp.ConnectString = dbdatabase;
            }
            else if (cp.Driver == "memdb.jdbcclient.DBDriver")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;

                cp.ConnectString = "jdbc:memdb:" + dbservername + portText;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "com.vertica.Driver")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;

                cp.ConnectString = "jdbc:vertica://" + dbservername + portText + "/" + dbdatabase;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "org.postgresql.Driver")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;

                cp.ConnectString = "jdbc:postgresql://" + dbservername + portText + "/" + dbdatabase;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "org.apache.hive.jdbc.HiveDriver") // impala and new hive? XXX
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                else
                    portText = ":21050";

                // jdbc:hive2://myhost.example.com:21050/;auth=noSasl (no authentication)
                // XXX kerberos is jdbc:hive2://myhost.example.com:21050/;principal=impala/myhost.example.com@H2.EXAMPLE.COM
                cp.ConnectString = "jdbc:hive2://" + dbservername + portText + "/;auth=noSasl"; 
                cp.Database = null; // no database for Impala
            }
            else if (cp.Driver == "org.postgresql.Driver.Redshift")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;

                cp.ConnectString = "jdbc:postgresql://" + dbservername + portText + "/" + dbdatabase + "?tcpKeepAlive=true";
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "com.sybase.jdbc4.jdbc.SybDriver")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                else
                    portText = ":2638"; // default value

                cp.ConnectString = "jdbc:sybase:Tds:" + dbservername + portText;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "com.paraccel.Driver")
            {
                string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                else
                    portText = ":5439"; // default value

                cp.ConnectString = "jdbc:paraccel://" + dbservername + portText + "/" + dbdatabase; ;
                cp.Database = dbdatabase;
            }
            else if (cp.Driver == "org.olap4j.driver.xmla.XmlaOlap4jDriver")
            {
                cp.ConnectString = dbservername;
            }
            else if (cp.Driver == "com.sap.db.jdbc.Driver")
            {
                 string portText = "";
                if (port.Length > 0)
                    portText = ":" + port;
                else
                    portText = ":30015"; // port is 3<instance number>15 (<instance number> is 2 digits, first instance is 00)

                cp.ConnectString = "jdbc:sap://" + dbservername + portText; // optional: /?autocommit=false
                cp.Database = null; // no database for hana, each Hana instance is a single database, like Redshift (even though Redshift has a database name)
            }
            else if (cp.Driver == "none")
            {
                cp.ConnectString = "";
            }
        }

        public static string getConnectionType(string driverName, string dbType)
        {
            if (driverName == "com.mysql.jdbc.Driver")
            {
                if (dbType != null && dbType.Equals("Infobright"))
                    return "Infobright";
                return "MySQL";
            }
            else if (driverName == "memdb.jdbcclient.DBDriver")
                return "MemDB";
            else if (driverName == "com.microsoft.jdbc.sqlserver.SQLServerDriver")
                return "MS SQL Server 2000";
            else if (driverName == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            {
                if (dbType != null && dbType.Equals("MSSQL 2012 Column Store"))
                    return "MSSQL 2012 Column Store";
                if (dbType != null && dbType.Equals("MS SQL Server 2014"))
                    return "MS SQL Server 2014";
                return "MS SQL Server 2005";
            }
            else if (driverName == "sun.jdbc.odbc.JdbcOdbcDriver")
                return dbType;
            else if (driverName == "GoogleAnalytics")
                return "Google Analytics";
            else if (driverName == "oracle.jdbc.OracleDriver")
                return "Oracle 11g";
            else if (driverName == "org.olap4j.driver.xmla.XmlaOlap4jDriver")
                return dbType;
            else if (driverName == "com.vertica.Driver")
                return "Vertica";
            else if (driverName == "org.postgresql.Driver")
                return "PostgreSQL";
            else if (driverName == "org.postgresql.Driver.Redshift")
                return "Redshift";
            else if (driverName == "com.teradata.jdbc.TeraDriver")
                return "Teradata";
            else if (driverName == "com.sybase.jdbc4.jdbc.SybDriver")
                return "Sybase IQ";
            else if (driverName == "com.paraccel.Driver")
                return "ParAccel";
            else if (driverName == "org.apache.hadoop.hive.jdbc.HiveDriver") // XXX old hive connection info
            	return "Hive/Hadoop";
            else if (driverName == "org.apache.hive.jdbc.HiveDriver")
                return "Impala";
            else if (driverName == "com.sap.db.jdbc.Driver")
                return "SAP Hana";
            else if (driverName == "none")
                return "None";
            return dbType;
        }

        #region RepositoryModule Members

        private void setConnectionParameters(DatabaseConnection cp)
        {
            setConnectionDriver(cp);
            setConnectInfo(cp, dbServerName.Text, dbPortBox.Text, dbDatabase.Text);
        }

        public static void setConnectionDriver(DatabaseConnection cp)
        {
            if (cp.Type == "MySQL")
            {
                cp.Driver = "com.mysql.jdbc.Driver";
            }
            else if (cp.Type == "MemDB" || cp.Type == "In-Memory Database")
            {
                cp.Driver = "memdb.jdbcclient.DBDriver";
            }
            else if (cp.Type == "Infobright")
            {
                cp.Driver = "com.mysql.jdbc.Driver";
            }
            else if (cp.Type == "Microsoft SQL Server")
            {
                cp.Driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2000")
            {
                cp.Driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2005")
            {
                cp.Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2008")
            {
                cp.Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2012")
            {
                cp.Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2014")
            {
                cp.Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            else if (cp.Type == "MS SQL Server 2012 Column Store")
            {
                cp.Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            else if (cp.Type == "Oracle ODBC" || cp.Type == "DB2 ODBC" || cp.Type == "MySQL ODBC" || cp.Type == "MSSQL Server ODBC")
            {
                cp.Driver = "sun.jdbc.odbc.JdbcOdbcDriver";
            }
            else if (cp.Type == "Microsoft Excel ODBC")
            {
                cp.Driver = "sun.jdbc.odbc.JdbcOdbcDriver";
            }
            else if (cp.Type == "Siebel Analytics ODBC")
            {
                cp.Driver = "sun.jdbc.odbc.JdbcOdbcDriver";
            }
            else if (cp.Type == "Impala")
            {
                cp.Driver = "org.apache.hive.jdbc.HiveDriver";
            }
            else if (cp.Type == "Google Analytics")
            {
                cp.Driver = "GoogleAnalytics";
            }
            else if (cp.Type == "Oracle 11g")
            {
                cp.Driver = "oracle.jdbc.OracleDriver";
            }
            else if (cp.Type == "Sybase IQ")
            {
                cp.Driver = "com.sybase.jdbc4.jdbc.SybDriver";
            }
            else if (cp.Type == "ParAccel")
            {
                cp.Driver = "com.paraccel.Driver";
            }
            else if (cp.Type == "Vertica")
            {
                cp.Driver = "com.vertica.Driver";
            }
            else if (cp.Type == "PostgreSQL")
            {
                cp.Driver = "org.postgresql.Driver";
            }
            else if (cp.Type == "Redshift")
            {
                cp.Driver = "org.postgresql.Driver.Redshift";
            }
            else if (cp.Type == "Hive/Hadoop")
            {
                cp.Driver = "org.apache.hadoop.hive.jdbc.HiveDriver"; // XXX old driver
            }
            else if (cp.Type == "Microsoft Analysis Services (XMLA)" || cp.Type == "Pentaho (Mondrian) Analysis Services (XMLA)" || cp.Type == "Hyperion Essbase (XMLA)" || cp.Type == "SAP BW (XMLA)")
            {
                cp.Driver = "org.olap4j.driver.xmla.XmlaOlap4jDriver";
            }
            else if (cp.Type == "SAP Hana")
            {
                cp.Driver = "com.sap.db.jdbc.Driver";
            }
            else if (cp.Type == "None")
            {
                cp.Driver = "none";
            }            
        }

        private DatabaseConnection getConnectionParameters()
        {
            DatabaseConnection cp = new DatabaseConnection();
            cp.Type = dbType.Text;
            cp.UserName = dbUserName.Text;
            cp.Password = MainAdminForm.encrypt(dbPassword.Text, MainAdminForm.SecretPassword);
            if (dbSchema.Text.Length > 0)
                cp.Schema = dbSchema.Text;
            try
            {
                cp.ConnectionPoolSize = Int32.Parse(connectionPoolBox.Text);
            }
            catch (FormatException)
            {
                cp.ConnectionPoolSize = 25;
            }
            try
            {
                cp.MaxConnectionThreads = Int32.Parse(maxConnectionThreads.Text);
            }
            catch (FormatException)
            {
                cp.MaxConnectionThreads = 2;
            }
            cp.UnicodeDatabase = unicodeDatabaseCheckBox.Checked;
            cp.useCompression = compressedCheckBox.Checked;
            cp.Realtime = realtimeCB.Checked;
            if (cp.Realtime)
            {
                cp.VisibleName = selNode.Text;
                cp.Name = connectionNameBox.Text;
            }
            else
                cp.Name = selNode.Text;

            cp.ID = idText.Text;
            setConnectionParameters(cp);
            return (cp);
        }

        public void setRepository(Repository r)
        {
            if (connectionList != null)
            {
                r.Connections = (DatabaseConnection[])(connectionList.ToArray());
            }
        }

        public void updateFromRepository(Repository r)
        {
            //Modeled Relationships
            selIndex = -1;
            if (r != null && r.Connections != null)
            {
                connectionList = new List<DatabaseConnection>(r.Connections);
            }
            else
            {
                connectionList = new List<DatabaseConnection>();
                DatabaseConnection cp = new DatabaseConnection();
                cp.Name = "Default Connection";
                connectionList.Add(cp);

            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (connectionList != null)
                foreach (DatabaseConnection cp in connectionList)
                {
                    TreeNode tn = null;
                    if (cp.Realtime)
                        tn = new TreeNode(cp.VisibleName);
                    else
                        tn = new TreeNode(cp.Name);
                    tn.ImageIndex = 13;
                    tn.SelectedImageIndex = 13;
                    rootNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();

            selIndex = rootNode.Nodes.IndexOf(node);
            selNode = node;
            ma.mainTree.ContextMenuStrip = connectionMenu;
            if (node.Parent.Parent == null)
            {
                removeConnectionToolStripMenuItem.Visible = false;
                return false;
            }
            removeConnectionToolStripMenuItem.Visible = true;
            if (selIndex >= 0)
                indexChanged(selIndex);
            return true;
        }

        private void indexChanged(int index)
        {
            DatabaseConnection cp = connectionList[index];
            if (cp.Driver == "com.mysql.jdbc.Driver")
            {
                int pos1 = cp.ConnectString.IndexOf("//") + 2;
                int pos2 = cp.ConnectString.IndexOf(":", pos1);
                int pos3 = cp.ConnectString.IndexOf("/", pos1);
                if (pos2 >= 0)
                {
                    dbServerName.Text = cp.ConnectString.Substring(pos1, pos2 - pos1);
                    if (pos3 >= 0)
                    {
                        dbPortBox.Text = cp.ConnectString.Substring(pos2 + 1, pos3 - pos2 - 1);
                    }
                    else
                    {
                        dbPortBox.Text = cp.ConnectString.Substring(pos2 + 1);
                    }
                }
                else
                {
                    if (pos3 >= 0)
                    {
                        dbServerName.Text = cp.ConnectString.Substring(pos1, pos3 - pos1);
                    }
                    else
                    {
                        dbServerName.Text = cp.ConnectString.Substring(pos1);
                    }
                    dbPortBox.Text = "";
                }
                if (pos3 >= 0)
                    dbDatabase.Text = cp.ConnectString.Substring(pos3 + 1);
                else
                    dbDatabase.Text = "";
            }
            else if (cp.Driver == "memdb.jdbcclient.DBDriver")
            {
                int pos1 = cp.ConnectString.IndexOf("memdb:") + 8;
                int pos2 = cp.ConnectString.IndexOf(":", pos1);
                if (pos2 >= 0)
                {
                    dbServerName.Text = cp.ConnectString.Substring(pos1, pos2 - pos1);
                    dbPortBox.Text = cp.ConnectString.Substring(pos2 + 1);
                }
                dbDatabase.Text = "";
            }
            else if ((cp.Driver == "com.microsoft.sqlserver.jdbc.SQLServerDriver") ||
                     (cp.Driver == "com.microsoft.jdbc.sqlserver.SQLServerDriver"))
            {
                int pos1 = cp.ConnectString.IndexOf("//") + 2;
                int pos2 = cp.ConnectString.IndexOf(":", pos1);
                int pos3 = cp.ConnectString.IndexOf("databaseName=", pos1);
                int pos4 = cp.ConnectString.IndexOf(";");
                if (pos4 < 0) pos4 = cp.ConnectString.Length;
                if (pos2 >= 0)
                    dbPortBox.Text = cp.ConnectString.Substring(pos2 + 1, pos4 - pos2 - 1);
                else dbPortBox.Text = "";
                dbServerName.Text = cp.ConnectString.Substring(pos1, (pos2 >= 0 ? pos2 : pos4) - pos1);
                if (pos3 >= 0)
                    dbDatabase.Text = cp.ConnectString.Substring(pos3 + 13);
                else
                    dbDatabase.Text = "";
            }
            else if (cp.Driver == "sun.jdbc.odbc.JdbcOdbcDriver")
            {
                int pos1 = cp.ConnectString.IndexOf(":");
                int pos2 = cp.ConnectString.IndexOf(":", pos1 + 1);
                dbServerName.Text = cp.ConnectString.Substring(pos2 + 1);
            }
            else if (cp.Driver == "com.sybase.jdbc4.jdbc.SybDriver")
            {
                int pos1 = cp.ConnectString.IndexOf("sybase:Tds:") + "sybase:Tds:".Length;
                int pos2 = cp.ConnectString.IndexOf(":", pos1);
                if (pos2 > 0)
                {
                    dbServerName.Text = cp.ConnectString.Substring(pos1, pos2 - pos1);
                    dbPortBox.Text = cp.ConnectString.Substring(pos2 + 1);
                }
                else
                {
                    dbServerName.Text = cp.ConnectString.Substring(pos1);
                    dbPortBox.Text = "";
                }
                dbDatabase.Text = "";
            }
            else if (cp.Driver == "GoogleAnalytics")
            {
                dbDatabase.Text = cp.ConnectString;
            }          

            dbType.Text = cp.Type;
            dbUserName.Text = cp.UserName;
            if (cp.Password != null)
            {
                dbPassword.Text = MainAdminForm.decrypt(cp.Password, MainAdminForm.SecretPassword);
            }
            if (cp.Schema != null)
                dbSchema.Text = cp.Schema;
            connectionPoolBox.Text = (cp.ConnectionPoolSize == 0) ? "25" : cp.ConnectionPoolSize.ToString();
            maxConnectionThreads.Text = (cp.MaxConnectionThreads == 0) ? "2" : cp.MaxConnectionThreads.ToString();
            unicodeDatabaseCheckBox.Checked = cp.UnicodeDatabase;
            compressedCheckBox.Checked = cp.useCompression;
            realtimeCB.Checked = cp.Realtime;
            if (cp.Realtime)
                connectionNameBox.Enabled = true;
            else
                connectionNameBox.Enabled = false;
            connectionNameBox.Text = cp.Name;
            idText.Text = cp.ID;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return connectionTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateConnection();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        private void dbDatabase_DropDown(object sender, EventArgs e)
        {
            List<string[]> catalogs = Server.getCatalogs(null, appconfig.ServerConnectionURL, usersandgroups.getAdminUsername(), usersandgroups.getAdminPassword()); 
            dbDatabase.Items.Clear();
            if (catalogs != null)
                foreach (string[] s in catalogs) dbDatabase.Items.Add(s[1]);
        }

        private void dbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dbType.Text == "ODBC" || dbType.Text == "Microsoft Excel ODBC" || dbType.Text == "Siebel Analytics ODBC")
            {
                dbDatabase.Enabled = false;
                dbPortBox.Enabled = false;
            }
            else
            {
                dbPortBox.Enabled = true;
                dbDatabase.Enabled = true;
            }
        }

        private void updateConnection_Click(object sender, EventArgs e)
        {
            updateConnection();
        }

        private void updateConnection()
        {
            if (selIndex < 0 || selIndex >= connectionList.Count || selNode == null) return;
            DatabaseConnection cp = getConnectionParameters();
            if (cp.Realtime)
                selNode.Text = cp.VisibleName;
            else
                selNode.Text = cp.Name;
            connectionList[selIndex] = cp;
        }

        private void addConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseConnection cp = new DatabaseConnection();
            connectionList.Add(cp);
            TreeNode tn = new TreeNode(cp.Name);
            tn.ImageIndex = 13;
            tn.SelectedImageIndex = 13;
            rootNode.Nodes.Add(tn);

        }

        private void removeConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Cannot remove the last connection
            if (connectionList.Count <= 1) return;
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            connectionList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void connectionTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        public DatabaseConnection findConnection(string name)
        {
            if (connectionList != null)
            {
                foreach (DatabaseConnection dc in connectionList)
                {
                    if (dc.Name == name)
                        return dc;
                }
            }
            return null;
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                e.Node.Text = e.Label;
            }
        }

        #endregion
    }
}