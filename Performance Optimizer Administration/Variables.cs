using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Variables : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Variables";
        string categoryName = "Business Modeling";
        private VariableList variableList;
        private string repositoryDirectory;
        private Variable[] repositoryVariables;

        public Variables(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            variableList = new VariableList(ma.getLogger());
        }

        public void updateRepositoryDirectory(string d)
        {
            if (repositoryDirectory != d)
            {
                repositoryDirectory = d;
                variableList.updateRepositoryDirectory(d, ma);
            }

        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (variableList != null && variableList.variableList.Count > 0)
            {
                r.Variables = variableList.Variables;
            }
        }

        public Boolean updateGuids()
        {
            return variableList.updateGuids();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Variables != null)
            {
                repositoryVariables = r.Variables;
            }
            else
            {
                repositoryVariables = new Variable[0];
            }
            variableList.variableList.Clear();
            variableList.variableList.AddRange(repositoryVariables);
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            
            if (variableList.variableList != null)
                foreach (Variable v in variableList.variableList)
                {
                    TreeNode tn = new TreeNode(v.Name);
                    tn.ImageIndex = 15;
                    tn.SelectedImageIndex = 15;
                    rootNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            if (variableList.variableList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = variableMenuStrip;
                upToolStripMenuItem.Visible = false;
                downToolStripMenuItem.Visible = false;
                removeVariableToolStripMenuItem.Visible = false;
                return false;
            }

            implicitSave();

            selIndex = rootNode.Nodes.IndexOf(node);
            selNode = node;
            indexChanged();
            ma.mainTree.ContextMenuStrip = variableMenuStrip;
            upToolStripMenuItem.Visible = true;
            downToolStripMenuItem.Visible = true;
            removeVariableToolStripMenuItem.Visible = true;
            return true;
        }

        public void setup()
        {
            if (!repositoryVariable.Checked && !sessionVariable.Checked && !warehouseButton.Checked) repositoryVariable.Checked = true;
            if (!queryResultType.Checked && !constantResultType.Checked) queryResultType.Checked = true;
            if (!constantResultType.Checked && !singleValue.Checked && !multiValue.Checked && !multipleColumns.Checked) singleValue.Checked = true;
            if (!constantResultType.Checked && !logicalQuery.Checked && !physicalQuery.Checked) logicalQuery.Checked = true;
            varConnectionBox.Enabled = !constantResultType.Checked;
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return variablesTabPage;
            }
            set
            {
            }
        }
        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (turnOffimplicitSave) return;
            updateVariable();
        }

        public void setRepositoryDirectory(string d)
        {
            variableList.updateRepositoryDirectory(d, ma);
        }
        #endregion


        private Variable getVariable()
        {
            Variable v = new Variable();
            v.Name = selNode.Text;
            v.Query = variableQuery.Text;
            v.Type = sessionVariable.Checked ? Variable.VariableType.Session : warehouseButton.Checked ? Variable.VariableType.Warehouse : Variable.VariableType.Repository;
            v.Session = sessionVariable.Checked;
            if (v.Type == Variable.VariableType.Session && !v.Session)
                v.Type = Variable.VariableType.Repository; // backward compatibility
            v.MultiValue = multiValue.Checked;
            v.Constant = constantResultType.Checked;
            v.LogicalQuery = logicalQuery.Checked;
            v.PhysicalQuery = physicalQuery.Checked;
            v.EvaluateOnDemand = evaluateOnDemandCheckBox.Checked;
            if (v.Type == Variable.VariableType.Repository)
            {
                v.Cacheable = cacheableCheckBox.Checked;
            }
            else
            {
                v.Cacheable = true;
            }
            if (!v.Constant)
            {
                v.Connection = varConnectionBox.Text;
                v.DefaultIfInvalid = invalidDefaultBox.Text;
            }
            return (v);
        }

        private void addVariable_Click(object sender, System.EventArgs e)
        {
            Variable v = new Variable();
            v.Name = "New Variable";
            foreach (Variable v2 in variableList.variableList)
            {
                if (v.Name == v2.Name) return;
            }
            v.Type = Variable.VariableType.Repository;
            variableList.variableList.Add(v);
            TreeNode tn = new TreeNode(v.Name);
            tn.ImageIndex = 15;
            tn.SelectedImageIndex = 15;
            rootNode.Nodes.Add(tn);
        }

        private void updateVariable()
        {
            if (selIndex < 0 || selIndex >= variableList.variableList.Count || selNode == null) return;
            Variable v = getVariable();
            variableList.variableList[selIndex] = v;
            selNode.Text = v.Name;
        }

        private void removeVariable_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            variableList.variableList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void indexChanged()
        {
            if (selIndex < 0) return;
            Variable v = variableList.variableList[selIndex];
            variableQuery.Text = v.Query;
            sessionVariable.Checked = v.Session;
            if (!v.Session && v.Type != Variable.VariableType.Session)
            {
                if (v.Type == Variable.VariableType.Warehouse)
                    warehouseButton.Checked = true;
                else if (v.Type == Variable.VariableType.Repository)
                    repositoryVariable.Checked = true;
            }
            multiValue.Checked = v.MultiValue;
            multipleColumns.Checked = v.MultipleColumns;
            singleValue.Checked = !v.Constant && !v.MultiValue && !v.MultipleColumns;
            constantResultType.Checked = v.Constant;
            queryResultType.Checked = !v.Constant;
            logicalQuery.Checked = v.LogicalQuery;
            physicalQuery.Checked = v.PhysicalQuery;
            evaluateOnDemandCheckBox.Checked = v.EvaluateOnDemand;
            if (v.Type == Variable.VariableType.Repository)
            {
                cacheableCheckBox.Checked = v.Cacheable;
            }
            else
            {
                cacheableCheckBox.Checked = true;
            }
            varConnectionBox.Items.Clear();
            foreach (DatabaseConnection dc in ma.connection.connectionList)
            {
                varConnectionBox.Items.Add(dc.Name);
            }
            varConnectionBox.Text = v.Connection;
            if (!v.Constant)
            {
                invalidDefaultBox.Text = v.DefaultIfInvalid;
                invalidDefaultBox.Enabled = true;
            }
            else
                invalidDefaultBox.Enabled = false;
        }

        private void upToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex > 0)
            {
                turnOffimplicitSave = true;
                TreeNode node = rootNode.Nodes[selIndex];
                rootNode.Nodes.RemoveAt(selIndex);
                rootNode.Nodes.Insert(selIndex - 1, node);
                Variable v = variableList.variableList[selIndex];
                variableList.variableList.RemoveAt(selIndex);
                variableList.variableList.Insert(selIndex - 1, v);
                ma.mainTree.SelectedNode = node;
                turnOffimplicitSave = false;
            }
        }

        private void downToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < variableList.variableList.Count - 1)
            {
                turnOffimplicitSave = true;
                TreeNode node = rootNode.Nodes[selIndex];
                rootNode.Nodes.RemoveAt(selIndex);
                rootNode.Nodes.Insert(selIndex + 1, node);
                Variable v = variableList.variableList[selIndex];
                variableList.variableList.RemoveAt(selIndex);
                variableList.variableList.Insert(selIndex + 1, v);
                ma.mainTree.SelectedNode = node;
                turnOffimplicitSave = false;
            }
        }

        private void queryResultType_CheckedChanged(object sender, EventArgs e)
        {
            if (queryResultType.Checked)
            {
                sessionVariable.Enabled = true;
                warehouseButton.Enabled = true;
                singleValue.Enabled = true;
                multiValue.Enabled = true;
                multipleColumns.Enabled = true;
                if (!singleValue.Checked && !multiValue.Checked && !multipleColumns.Checked) singleValue.Checked = true;
                physicalQuery.Enabled = true;
                logicalQuery.Enabled = true;
                if (!physicalQuery.Checked && !logicalQuery.Checked) logicalQuery.Checked = true;
                varConnectionBox.Enabled = true;
                cacheableCheckBox.Enabled = true;
                invalidDefaultBox.Enabled = true;
                evaluateOnDemandCheckBox.Enabled = true;
            }
            else
            {
                repositoryVariable.Enabled = true;
                sessionVariable.Enabled = false;
                warehouseButton.Enabled = false;
                singleValue.Enabled = false;
                multiValue.Enabled = false;
                multipleColumns.Enabled = false;
                singleValue.Checked = false;
                multiValue.Checked = false;
                multipleColumns.Checked = false;
                physicalQuery.Enabled = false;
                logicalQuery.Enabled = false;
                physicalQuery.Checked = false;
                logicalQuery.Checked = false;
                varConnectionBox.SelectedIndex = -1;
                varConnectionBox.Enabled = false;
                cacheableCheckBox.Enabled = false;
                invalidDefaultBox.Enabled = false;
                evaluateOnDemandCheckBox.Enabled = false;
                evaluateOnDemandCheckBox.Checked = false;
            }
        }

        private bool keyhandled;
        private void variableQuery_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyhandled)
            {
                e.Handled = true;
                keyhandled = false;
            }
        }

        private void variableQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                variableQuery.SelectAll();
                keyhandled = true;
            }
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                variableList.variableList[selIndex].Name = e.Label;
            }
        }

        #endregion

        private void repositoryVariable_CheckedChanged(object sender, EventArgs e)
        {
            if (repositoryVariable.Checked)
            {
                if ((selIndex != -1) && (selIndex < variableList.variableList.Count))
                {
                    cacheableCheckBox.Checked = variableList.variableList[selIndex].Cacheable;
                }
                cacheableCheckBox.Enabled = true;
                evaluateOnDemandCheckBox.Enabled = true;
            }
            else
            {
                cacheableCheckBox.Checked = true;
                cacheableCheckBox.Enabled = false;
                evaluateOnDemandCheckBox.Enabled = false;
                evaluateOnDemandCheckBox.Checked = false;
            }
        }

        private void physicalQuery_CheckedChanged(object sender, EventArgs e)
        {
            if (physicalQuery.Checked)
            {
                cacheableCheckBox.Checked = false;
                cacheableCheckBox.Enabled = false;
            }
            else
            {
                cacheableCheckBox.Enabled = true;
            }
        }

        private void logicalQuery_CheckedChanged(object sender, EventArgs e)
        {
            if (logicalQuery.Checked)
            {
               
                cacheableCheckBox.Enabled = true;
            }
            else
            {
                cacheableCheckBox.Checked = false;
                cacheableCheckBox.Enabled = false;
            }
        }
        public bool isConnectionInUse(String name)
        {
            if (variableList.variableList.Count > 0)
            {
                string lname = name.ToLower();
                foreach (Variable v in variableList.variableList)
                {
                    if (v != null && v.Connection != null && v.Connection.ToLower() == lname)
                        return true;
                }
            }
            return false;
        }

        internal void updateVariable(Variable var, string username)
        {
            variableList.updateItem(var, username, ma);
        }

        internal void deleteVariable(Variable var)
        {
            variableList.deleteItem(var, ma);
        }

        internal List<Variable> getVariables()
        {
            variableList.refreshIfNecessary(ma);
            if (variableList.variableList.Count == 0 && repositoryVariables != null && repositoryVariables.GetLength(0) > 0)
            {
                foreach (Variable v in repositoryVariables)
                {
                    variableList.variableList.Add(v);
                }
            }
            return variableList.variableList;
        }

		// do not use for most things.   This is only necessary during load time
		public List<Variable> getCurrentVariables()
		{
			return variableList.variableList;
		}
    }
}