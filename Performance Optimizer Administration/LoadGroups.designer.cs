namespace Performance_Optimizer_Administration
{
    partial class LoadGroups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.loadGroupsPage = new System.Windows.Forms.TabPage();
            this.loadGroupGridView = new System.Windows.Forms.DataGridView();
            this.DimensionNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperatorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loadGroupsPanel = new System.Windows.Forms.Panel();
            this.operatorLabel = new System.Windows.Forms.Label();
            this.operatorComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.valueLabel = new System.Windows.Forms.Label();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.columnComboBox = new System.Windows.Forms.ComboBox();
            this.columnLabel = new System.Windows.Forms.Label();
            this.dimensionLabel = new System.Windows.Forms.Label();
            this.dimensionComboBox = new System.Windows.Forms.ComboBox();
            this.loadGroupsTabControl = new System.Windows.Forms.TabControl();
            this.loadGroupMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newLoadGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLoadGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGroupsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadGroupGridView)).BeginInit();
            this.loadGroupsPanel.SuspendLayout();
            this.loadGroupsTabControl.SuspendLayout();
            this.loadGroupMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadGroupsPage
            // 
            this.loadGroupsPage.AutoScroll = true;
            this.loadGroupsPage.Controls.Add(this.loadGroupGridView);
            this.loadGroupsPage.Controls.Add(this.loadGroupsPanel);
            this.loadGroupsPage.Location = new System.Drawing.Point(4, 22);
            this.loadGroupsPage.Name = "loadGroupsPage";
            this.loadGroupsPage.Padding = new System.Windows.Forms.Padding(3);
            this.loadGroupsPage.Size = new System.Drawing.Size(844, 707);
            this.loadGroupsPage.TabIndex = 0;
            this.loadGroupsPage.Text = "Load Groups";
            this.loadGroupsPage.UseVisualStyleBackColor = true;
            // 
            // loadGroupGridView
            // 
            this.loadGroupGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.loadGroupGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.loadGroupGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.loadGroupGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DimensionNameColumn,
            this.ColumnNameColumn,
            this.OperatorColumn,
            this.ValueColumn});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.loadGroupGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.loadGroupGridView.Location = new System.Drawing.Point(3, 184);
            this.loadGroupGridView.Name = "loadGroupGridView";
            this.loadGroupGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.loadGroupGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.loadGroupGridView.Size = new System.Drawing.Size(838, 551);
            this.loadGroupGridView.TabIndex = 0;
            this.loadGroupGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.loadGroupGridView_CellBeginEdit);
            // 
            // DimensionNameColumn
            // 
            this.DimensionNameColumn.DataPropertyName = "DimensionName";
            this.DimensionNameColumn.HeaderText = "Dimension";
            this.DimensionNameColumn.Name = "DimensionNameColumn";
            this.DimensionNameColumn.ReadOnly = true;
            this.DimensionNameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DimensionNameColumn.Width = 200;
            // 
            // ColumnNameColumn
            // 
            this.ColumnNameColumn.DataPropertyName = "DimensionColumn";
            this.ColumnNameColumn.HeaderText = "Column";
            this.ColumnNameColumn.Name = "ColumnNameColumn";
            this.ColumnNameColumn.ReadOnly = true;
            this.ColumnNameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnNameColumn.Width = 200;
            // 
            // OperatorColumn
            // 
            this.OperatorColumn.DataPropertyName = "Operator";
            this.OperatorColumn.HeaderText = "Operator";
            this.OperatorColumn.Name = "OperatorColumn";
            this.OperatorColumn.ReadOnly = true;
            this.OperatorColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ValueColumn
            // 
            this.ValueColumn.DataPropertyName = "Value";
            this.ValueColumn.HeaderText = "Value";
            this.ValueColumn.Name = "ValueColumn";
            this.ValueColumn.ReadOnly = true;
            this.ValueColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ValueColumn.Width = 400;
            // 
            // loadGroupsPanel
            // 
            this.loadGroupsPanel.Controls.Add(this.operatorLabel);
            this.loadGroupsPanel.Controls.Add(this.operatorComboBox);
            this.loadGroupsPanel.Controls.Add(this.label2);
            this.loadGroupsPanel.Controls.Add(this.label1);
            this.loadGroupsPanel.Controls.Add(this.removeButton);
            this.loadGroupsPanel.Controls.Add(this.addButton);
            this.loadGroupsPanel.Controls.Add(this.valueLabel);
            this.loadGroupsPanel.Controls.Add(this.valueTextBox);
            this.loadGroupsPanel.Controls.Add(this.columnComboBox);
            this.loadGroupsPanel.Controls.Add(this.columnLabel);
            this.loadGroupsPanel.Controls.Add(this.dimensionLabel);
            this.loadGroupsPanel.Controls.Add(this.dimensionComboBox);
            this.loadGroupsPanel.Location = new System.Drawing.Point(3, 0);
            this.loadGroupsPanel.Name = "loadGroupsPanel";
            this.loadGroupsPanel.Size = new System.Drawing.Size(841, 178);
            this.loadGroupsPanel.TabIndex = 9;
            // 
            // operatorLabel
            // 
            this.operatorLabel.AutoSize = true;
            this.operatorLabel.Location = new System.Drawing.Point(468, 66);
            this.operatorLabel.Name = "operatorLabel";
            this.operatorLabel.Size = new System.Drawing.Size(48, 13);
            this.operatorLabel.TabIndex = 20;
            this.operatorLabel.Text = "Operator";
            // 
            // operatorComboBox
            // 
            this.operatorComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.operatorComboBox.FormattingEnabled = true;
            this.operatorComboBox.Items.AddRange(new object[] {
            "<",
            "<=",
            "=",
            ">=",
            ">"});
            this.operatorComboBox.Location = new System.Drawing.Point(473, 90);
            this.operatorComboBox.Name = "operatorComboBox";
            this.operatorComboBox.Size = new System.Drawing.Size(46, 21);
            this.operatorComboBox.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(767, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Load group filter condition is used in the context of persisting and updating agg" +
                "regates only.  The condition is ignored while loading staging tables and warehou" +
                "se.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(620, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Specify filter condition for load groups by selecting a dimension/column cominati" +
                "on using the dropdowns and entering a filter value.";
            // 
            // removeButton
            // 
            this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeButton.Location = new System.Drawing.Point(3, 143);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 16;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addButton
            // 
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Location = new System.Drawing.Point(758, 88);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 15;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // valueLabel
            // 
            this.valueLabel.AutoSize = true;
            this.valueLabel.Location = new System.Drawing.Point(539, 66);
            this.valueLabel.Name = "valueLabel";
            this.valueLabel.Size = new System.Drawing.Size(34, 13);
            this.valueLabel.TabIndex = 14;
            this.valueLabel.Text = "Value";
            // 
            // valueTextBox
            // 
            this.valueTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.valueTextBox.Location = new System.Drawing.Point(539, 90);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.Size = new System.Drawing.Size(197, 20);
            this.valueTextBox.TabIndex = 13;
            // 
            // columnComboBox
            // 
            this.columnComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.columnComboBox.FormattingEnabled = true;
            this.columnComboBox.Location = new System.Drawing.Point(246, 89);
            this.columnComboBox.Name = "columnComboBox";
            this.columnComboBox.Size = new System.Drawing.Size(205, 21);
            this.columnComboBox.TabIndex = 12;
            // 
            // columnLabel
            // 
            this.columnLabel.AutoSize = true;
            this.columnLabel.Location = new System.Drawing.Point(243, 66);
            this.columnLabel.Name = "columnLabel";
            this.columnLabel.Size = new System.Drawing.Size(42, 13);
            this.columnLabel.TabIndex = 11;
            this.columnLabel.Text = "Column";
            // 
            // dimensionLabel
            // 
            this.dimensionLabel.AutoSize = true;
            this.dimensionLabel.Location = new System.Drawing.Point(0, 65);
            this.dimensionLabel.Name = "dimensionLabel";
            this.dimensionLabel.Size = new System.Drawing.Size(56, 13);
            this.dimensionLabel.TabIndex = 10;
            this.dimensionLabel.Text = "Dimension";
            // 
            // dimensionComboBox
            // 
            this.dimensionComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dimensionComboBox.FormattingEnabled = true;
            this.dimensionComboBox.Location = new System.Drawing.Point(3, 88);
            this.dimensionComboBox.Name = "dimensionComboBox";
            this.dimensionComboBox.Size = new System.Drawing.Size(221, 21);
            this.dimensionComboBox.TabIndex = 9;
            this.dimensionComboBox.SelectedIndexChanged += new System.EventHandler(this.dimensionComboBox_SelectedIndexChanged);
            // 
            // loadGroupsTabControl
            // 
            this.loadGroupsTabControl.Controls.Add(this.loadGroupsPage);
            this.loadGroupsTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadGroupsTabControl.Location = new System.Drawing.Point(0, 0);
            this.loadGroupsTabControl.Name = "loadGroupsTabControl";
            this.loadGroupsTabControl.SelectedIndex = 0;
            this.loadGroupsTabControl.Size = new System.Drawing.Size(852, 733);
            this.loadGroupsTabControl.TabIndex = 0;
            // 
            // loadGroupMenuStrip
            // 
            this.loadGroupMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newLoadGroupToolStripMenuItem,
            this.removeLoadGroupToolStripMenuItem});
            this.loadGroupMenuStrip.Name = "sourceFileMenuStrip";
            this.loadGroupMenuStrip.Size = new System.Drawing.Size(183, 48);
            // 
            // newLoadGroupToolStripMenuItem
            // 
            this.newLoadGroupToolStripMenuItem.Name = "newLoadGroupToolStripMenuItem";
            this.newLoadGroupToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.newLoadGroupToolStripMenuItem.Text = "&New Load Group";
            this.newLoadGroupToolStripMenuItem.Click += new System.EventHandler(this.newLoadGroupToolStripMenuItem_Click);
            // 
            // removeLoadGroupToolStripMenuItem
            // 
            this.removeLoadGroupToolStripMenuItem.Name = "removeLoadGroupToolStripMenuItem";
            this.removeLoadGroupToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.removeLoadGroupToolStripMenuItem.Text = "&Remove Load Group";
            this.removeLoadGroupToolStripMenuItem.Click += new System.EventHandler(this.removeLoadGroupToolStripMenuItem_Click);
            // 
            // LoadGroups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.loadGroupsTabControl);
            this.Name = "LoadGroups";
            this.Text = "Load Groups";
            this.loadGroupsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadGroupGridView)).EndInit();
            this.loadGroupsPanel.ResumeLayout(false);
            this.loadGroupsPanel.PerformLayout();
            this.loadGroupsTabControl.ResumeLayout(false);
            this.loadGroupMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage loadGroupsPage;
        private System.Windows.Forms.TabControl loadGroupsTabControl;
        private System.Windows.Forms.ContextMenuStrip loadGroupMenuStrip;
        private System.Windows.Forms.DataGridView loadGroupGridView;
        private System.Windows.Forms.ToolStripMenuItem newLoadGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLoadGroupToolStripMenuItem;
        private System.Windows.Forms.Panel loadGroupsPanel;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label valueLabel;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.ComboBox columnComboBox;
        private System.Windows.Forms.Label columnLabel;
        private System.Windows.Forms.Label dimensionLabel;
        private System.Windows.Forms.ComboBox dimensionComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox operatorComboBox;
        private System.Windows.Forms.Label operatorLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn DimensionNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperatorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueColumn;
    }
}