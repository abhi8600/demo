namespace Performance_Optimizer_Administration
{
    partial class TimeProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeProperties));
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.dayBox = new System.Windows.Forms.CheckBox();
            this.weekBox = new System.Windows.Forms.CheckBox();
            this.monthBox = new System.Windows.Forms.CheckBox();
            this.quarterBox = new System.Windows.Forms.CheckBox();
            this.halfyearBox = new System.Windows.Forms.CheckBox();
            this.yearBox = new System.Windows.Forms.CheckBox();
            this.generateTime = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.noAgg = new System.Windows.Forms.RadioButton();
            this.trailingAgg = new System.Windows.Forms.RadioButton();
            this.trailingX = new System.Windows.Forms.TextBox();
            this.trailingPeriod = new System.Windows.Forms.ComboBox();
            this.toDatePeriod = new System.Windows.Forms.ComboBox();
            this.toDateAgg = new System.Windows.Forms.RadioButton();
            this.label22 = new System.Windows.Forms.Label();
            this.shiftPeriod = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.shiftAmount = new System.Windows.Forms.TextBox();
            this.shiftDir = new System.Windows.Forms.ComboBox();
            this.prefixBox = new System.Windows.Forms.TextBox();
            this.addAnalysis = new System.Windows.Forms.Button();
            this.periodList = new System.Windows.Forms.ListView();
            this.aggHeader = new System.Windows.Forms.ColumnHeader();
            this.shiftHeader = new System.Windows.Forms.ColumnHeader();
            this.prefixHead = new System.Windows.Forms.ColumnHeader();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.partitionFacts = new System.Windows.Forms.CheckBox();
            this.partitionEnd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.partitionStart = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.partitionPeriodBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.determinantDayLbl = new System.Windows.Forms.Label();
            this.firstDayLbl = new System.Windows.Forms.Label();
            this.determinantDayBox = new System.Windows.Forms.ComboBox();
            this.firstDayBox = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(326, 16);
            this.label14.TabIndex = 45;
            this.label14.Text = "Select Desired Time Grains and Aggregations";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(189, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 51;
            this.label17.Text = "Start Date";
            // 
            // startDate
            // 
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDate.Location = new System.Drawing.Point(250, 13);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(91, 20);
            this.startDate.TabIndex = 41;
            this.startDate.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(189, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 53;
            this.label18.Text = "End Date";
            // 
            // endDate
            // 
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDate.Location = new System.Drawing.Point(250, 39);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(91, 20);
            this.endDate.TabIndex = 42;
            this.endDate.Value = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(366, 586);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 43;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(447, 586);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 44;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // dayBox
            // 
            this.dayBox.AutoSize = true;
            this.dayBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dayBox.Location = new System.Drawing.Point(16, 25);
            this.dayBox.Name = "dayBox";
            this.dayBox.Size = new System.Drawing.Size(42, 17);
            this.dayBox.TabIndex = 0;
            this.dayBox.Text = "Day";
            this.dayBox.UseVisualStyleBackColor = true;
            // 
            // weekBox
            // 
            this.weekBox.AutoSize = true;
            this.weekBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.weekBox.Location = new System.Drawing.Point(16, 54);
            this.weekBox.Name = "weekBox";
            this.weekBox.Size = new System.Drawing.Size(52, 17);
            this.weekBox.TabIndex = 1;
            this.weekBox.Text = "Week";
            this.weekBox.UseVisualStyleBackColor = true;
            // 
            // monthBox
            // 
            this.monthBox.AutoSize = true;
            this.monthBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.monthBox.Location = new System.Drawing.Point(16, 83);
            this.monthBox.Name = "monthBox";
            this.monthBox.Size = new System.Drawing.Size(53, 17);
            this.monthBox.TabIndex = 2;
            this.monthBox.Text = "Month";
            this.monthBox.UseVisualStyleBackColor = true;
            // 
            // quarterBox
            // 
            this.quarterBox.AutoSize = true;
            this.quarterBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quarterBox.Location = new System.Drawing.Point(16, 112);
            this.quarterBox.Name = "quarterBox";
            this.quarterBox.Size = new System.Drawing.Size(58, 17);
            this.quarterBox.TabIndex = 3;
            this.quarterBox.Text = "Quarter";
            this.quarterBox.UseVisualStyleBackColor = true;
            // 
            // halfyearBox
            // 
            this.halfyearBox.AutoSize = true;
            this.halfyearBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.halfyearBox.Location = new System.Drawing.Point(16, 141);
            this.halfyearBox.Name = "halfyearBox";
            this.halfyearBox.Size = new System.Drawing.Size(65, 17);
            this.halfyearBox.TabIndex = 4;
            this.halfyearBox.Text = "Half-year";
            this.halfyearBox.UseVisualStyleBackColor = true;
            // 
            // yearBox
            // 
            this.yearBox.AutoSize = true;
            this.yearBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.yearBox.Location = new System.Drawing.Point(16, 170);
            this.yearBox.Name = "yearBox";
            this.yearBox.Size = new System.Drawing.Size(45, 17);
            this.yearBox.TabIndex = 5;
            this.yearBox.Text = "Year";
            this.yearBox.UseVisualStyleBackColor = true;
            // 
            // generateTime
            // 
            this.generateTime.AutoSize = true;
            this.generateTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generateTime.Location = new System.Drawing.Point(15, 12);
            this.generateTime.Name = "generateTime";
            this.generateTime.Size = new System.Drawing.Size(168, 17);
            this.generateTime.TabIndex = 64;
            this.generateTime.Text = "Generate Time Dimension";
            this.generateTime.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 13);
            this.label20.TabIndex = 66;
            this.label20.Text = "Aggregation";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(213, 67);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 65;
            this.label21.Text = "Prefix";
            // 
            // noAgg
            // 
            this.noAgg.AutoSize = true;
            this.noAgg.Checked = true;
            this.noAgg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.noAgg.Location = new System.Drawing.Point(9, 36);
            this.noAgg.Name = "noAgg";
            this.noAgg.Size = new System.Drawing.Size(50, 17);
            this.noAgg.TabIndex = 67;
            this.noAgg.TabStop = true;
            this.noAgg.Text = "None";
            this.noAgg.UseVisualStyleBackColor = true;
            this.noAgg.CheckedChanged += new System.EventHandler(this.noAgg_CheckedChanged);
            // 
            // trailingAgg
            // 
            this.trailingAgg.AutoSize = true;
            this.trailingAgg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trailingAgg.Location = new System.Drawing.Point(9, 65);
            this.trailingAgg.Name = "trailingAgg";
            this.trailingAgg.Size = new System.Drawing.Size(58, 17);
            this.trailingAgg.TabIndex = 68;
            this.trailingAgg.Text = "Trailing";
            this.trailingAgg.UseVisualStyleBackColor = true;
            this.trailingAgg.CheckedChanged += new System.EventHandler(this.trailingAgg_CheckedChanged);
            // 
            // trailingX
            // 
            this.trailingX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trailingX.Location = new System.Drawing.Point(74, 65);
            this.trailingX.Name = "trailingX";
            this.trailingX.Size = new System.Drawing.Size(34, 20);
            this.trailingX.TabIndex = 69;
            this.trailingX.Text = "12";
            this.trailingX.TextChanged += new System.EventHandler(this.trailingX_TextChanged);
            // 
            // trailingPeriod
            // 
            this.trailingPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trailingPeriod.FormattingEnabled = true;
            this.trailingPeriod.Items.AddRange(new object[] {
            "Days",
            "Weeks",
            "Months"});
            this.trailingPeriod.Location = new System.Drawing.Point(114, 64);
            this.trailingPeriod.Name = "trailingPeriod";
            this.trailingPeriod.Size = new System.Drawing.Size(80, 21);
            this.trailingPeriod.TabIndex = 70;
            this.trailingPeriod.Text = "Months";
            this.trailingPeriod.SelectedIndexChanged += new System.EventHandler(this.trailingPeriod_SelectedIndexChanged);
            // 
            // toDatePeriod
            // 
            this.toDatePeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toDatePeriod.FormattingEnabled = true;
            this.toDatePeriod.Items.AddRange(new object[] {
            "Week",
            "Month",
            "Quarter",
            "Half-year",
            "Year"});
            this.toDatePeriod.Location = new System.Drawing.Point(74, 89);
            this.toDatePeriod.Name = "toDatePeriod";
            this.toDatePeriod.Size = new System.Drawing.Size(80, 21);
            this.toDatePeriod.TabIndex = 72;
            this.toDatePeriod.Text = "Year";
            this.toDatePeriod.SelectedIndexChanged += new System.EventHandler(this.toDatePeriod_SelectedIndexChanged);
            // 
            // toDateAgg
            // 
            this.toDateAgg.AutoSize = true;
            this.toDateAgg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toDateAgg.Location = new System.Drawing.Point(9, 94);
            this.toDateAgg.Name = "toDateAgg";
            this.toDateAgg.Size = new System.Drawing.Size(63, 17);
            this.toDateAgg.TabIndex = 71;
            this.toDateAgg.Text = "To Date";
            this.toDateAgg.UseVisualStyleBackColor = true;
            this.toDateAgg.CheckedChanged += new System.EventHandler(this.toDateAgg_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Enabled = false;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(213, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 73;
            this.label22.Text = "Period Shift";
            // 
            // shiftPeriod
            // 
            this.shiftPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shiftPeriod.FormattingEnabled = true;
            this.shiftPeriod.Items.AddRange(new object[] {
            "Week",
            "Month",
            "Quarter",
            "Half-year",
            "Year"});
            this.shiftPeriod.Location = new System.Drawing.Point(259, 35);
            this.shiftPeriod.Name = "shiftPeriod";
            this.shiftPeriod.Size = new System.Drawing.Size(70, 21);
            this.shiftPeriod.TabIndex = 74;
            this.shiftPeriod.Text = "Year";
            this.shiftPeriod.SelectedIndexChanged += new System.EventHandler(this.shiftPeriod_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.shiftAmount);
            this.groupBox1.Controls.Add(this.shiftDir);
            this.groupBox1.Controls.Add(this.prefixBox);
            this.groupBox1.Controls.Add(this.addAnalysis);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.shiftPeriod);
            this.groupBox1.Controls.Add(this.noAgg);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.trailingAgg);
            this.groupBox1.Controls.Add(this.toDatePeriod);
            this.groupBox1.Controls.Add(this.trailingX);
            this.groupBox1.Controls.Add(this.toDateAgg);
            this.groupBox1.Controls.Add(this.trailingPeriod);
            this.groupBox1.Location = new System.Drawing.Point(118, 179);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 151);
            this.groupBox1.TabIndex = 76;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Define Desired Time Period Analysis";
            // 
            // shiftAmount
            // 
            this.shiftAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shiftAmount.Location = new System.Drawing.Point(216, 36);
            this.shiftAmount.Name = "shiftAmount";
            this.shiftAmount.Size = new System.Drawing.Size(37, 20);
            this.shiftAmount.TabIndex = 79;
            this.shiftAmount.TextChanged += new System.EventHandler(this.shiftAmount_TextChanged);
            // 
            // shiftDir
            // 
            this.shiftDir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shiftDir.FormattingEnabled = true;
            this.shiftDir.Items.AddRange(new object[] {
            "Ago",
            "Ahead"});
            this.shiftDir.Location = new System.Drawing.Point(335, 35);
            this.shiftDir.Name = "shiftDir";
            this.shiftDir.Size = new System.Drawing.Size(61, 21);
            this.shiftDir.TabIndex = 78;
            this.shiftDir.Text = "Ago";
            this.shiftDir.SelectedIndexChanged += new System.EventHandler(this.shiftDir_SelectedIndexChanged);
            // 
            // prefixBox
            // 
            this.prefixBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.prefixBox.Location = new System.Drawing.Point(216, 89);
            this.prefixBox.Name = "prefixBox";
            this.prefixBox.Size = new System.Drawing.Size(111, 20);
            this.prefixBox.TabIndex = 77;
            // 
            // addAnalysis
            // 
            this.addAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addAnalysis.Location = new System.Drawing.Point(321, 122);
            this.addAnalysis.Name = "addAnalysis";
            this.addAnalysis.Size = new System.Drawing.Size(75, 23);
            this.addAnalysis.TabIndex = 76;
            this.addAnalysis.Text = "Add";
            this.addAnalysis.UseVisualStyleBackColor = true;
            this.addAnalysis.Click += new System.EventHandler(this.addAnalysis_Click);
            // 
            // periodList
            // 
            this.periodList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.periodList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.aggHeader,
            this.shiftHeader,
            this.prefixHead});
            this.periodList.FullRowSelect = true;
            this.periodList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.periodList.Location = new System.Drawing.Point(118, 376);
            this.periodList.MultiSelect = false;
            this.periodList.Name = "periodList";
            this.periodList.Size = new System.Drawing.Size(404, 140);
            this.periodList.TabIndex = 77;
            this.periodList.UseCompatibleStateImageBehavior = false;
            this.periodList.View = System.Windows.Forms.View.Details;
            // 
            // aggHeader
            // 
            this.aggHeader.Text = "Aggregation";
            this.aggHeader.Width = 124;
            // 
            // shiftHeader
            // 
            this.shiftHeader.Text = "Period Shift";
            this.shiftHeader.Width = 149;
            // 
            // prefixHead
            // 
            this.prefixHead.Text = "Prefix";
            this.prefixHead.Width = 125;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.yearBox);
            this.groupBox2.Controls.Add(this.halfyearBox);
            this.groupBox2.Controls.Add(this.quarterBox);
            this.groupBox2.Controls.Add(this.monthBox);
            this.groupBox2.Controls.Add(this.weekBox);
            this.groupBox2.Controls.Add(this.dayBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(100, 206);
            this.groupBox2.TabIndex = 78;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Time Levels";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 79;
            this.label1.Text = "Selected Time Analysis";
            // 
            // removeButton
            // 
            this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(458, 354);
            this.removeButton.Margin = new System.Windows.Forms.Padding(0);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(64, 19);
            this.removeButton.TabIndex = 80;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // partitionFacts
            // 
            this.partitionFacts.AutoSize = true;
            this.partitionFacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.partitionFacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partitionFacts.Location = new System.Drawing.Point(12, 530);
            this.partitionFacts.Name = "partitionFacts";
            this.partitionFacts.Size = new System.Drawing.Size(189, 17);
            this.partitionFacts.TabIndex = 85;
            this.partitionFacts.Text = "Partition Fact Tables by Time";
            this.partitionFacts.UseVisualStyleBackColor = true;
            // 
            // partitionEnd
            // 
            this.partitionEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.partitionEnd.Location = new System.Drawing.Point(302, 551);
            this.partitionEnd.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.partitionEnd.Name = "partitionEnd";
            this.partitionEnd.Size = new System.Drawing.Size(91, 20);
            this.partitionEnd.TabIndex = 82;
            this.partitionEnd.Value = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(241, 558);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "End Date";
            // 
            // partitionStart
            // 
            this.partitionStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.partitionStart.Location = new System.Drawing.Point(302, 528);
            this.partitionStart.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.partitionStart.Name = "partitionStart";
            this.partitionStart.Size = new System.Drawing.Size(91, 20);
            this.partitionStart.TabIndex = 81;
            this.partitionStart.Value = new System.DateTime(2005, 1, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(241, 532);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 83;
            this.label3.Text = "Start Date";
            // 
            // partitionPeriodBox
            // 
            this.partitionPeriodBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.partitionPeriodBox.FormattingEnabled = true;
            this.partitionPeriodBox.Items.AddRange(new object[] {
            "Week",
            "Month",
            "Year"});
            this.partitionPeriodBox.Location = new System.Drawing.Point(118, 552);
            this.partitionPeriodBox.Name = "partitionPeriodBox";
            this.partitionPeriodBox.Size = new System.Drawing.Size(80, 21);
            this.partitionPeriodBox.TabIndex = 86;
            this.partitionPeriodBox.Text = "Month";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(31, 558);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 87;
            this.label4.Text = "Partition by";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 16);
            this.label5.TabIndex = 88;
            this.label5.Text = "Week Properties";
            // 
            // determinantDayLbl
            // 
            this.determinantDayLbl.AutoSize = true;
            this.determinantDayLbl.Enabled = false;
            this.determinantDayLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.determinantDayLbl.Location = new System.Drawing.Point(9, 87);
            this.determinantDayLbl.Name = "determinantDayLbl";
            this.determinantDayLbl.Size = new System.Drawing.Size(118, 13);
            this.determinantDayLbl.TabIndex = 89;
            this.determinantDayLbl.Text = "Week Determinant Day";
            // 
            // firstDayLbl
            // 
            this.firstDayLbl.AutoSize = true;
            this.firstDayLbl.Enabled = false;
            this.firstDayLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstDayLbl.Location = new System.Drawing.Point(12, 115);
            this.firstDayLbl.Name = "firstDayLbl";
            this.firstDayLbl.Size = new System.Drawing.Size(92, 13);
            this.firstDayLbl.TabIndex = 90;
            this.firstDayLbl.Text = "First Day of Week";
            // 
            // determinantDayBox
            // 
            this.determinantDayBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.determinantDayBox.FormattingEnabled = true;
            this.determinantDayBox.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.determinantDayBox.Location = new System.Drawing.Point(192, 87);
            this.determinantDayBox.Name = "determinantDayBox";
            this.determinantDayBox.Size = new System.Drawing.Size(149, 21);
            this.determinantDayBox.TabIndex = 91;
            // 
            // firstDayBox
            // 
            this.firstDayBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.firstDayBox.FormattingEnabled = true;
            this.firstDayBox.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"});
            this.firstDayBox.Location = new System.Drawing.Point(192, 115);
            this.firstDayBox.Name = "firstDayBox";
            this.firstDayBox.Size = new System.Drawing.Size(149, 21);
            this.firstDayBox.TabIndex = 92;
            // 
            // TimeProperties
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(534, 612);
            this.ControlBox = false;
            this.Controls.Add(this.firstDayBox);
            this.Controls.Add(this.determinantDayBox);
            this.Controls.Add(this.firstDayLbl);
            this.Controls.Add(this.determinantDayLbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.partitionPeriodBox);
            this.Controls.Add(this.partitionFacts);
            this.Controls.Add(this.partitionEnd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.partitionStart);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.periodList);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.generateTime);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.startDate);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label14);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeProperties";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Time Properties";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox dayBox;
        private System.Windows.Forms.CheckBox weekBox;
        private System.Windows.Forms.CheckBox monthBox;
        private System.Windows.Forms.CheckBox quarterBox;
        private System.Windows.Forms.CheckBox halfyearBox;
        private System.Windows.Forms.CheckBox yearBox;
        private System.Windows.Forms.CheckBox generateTime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton noAgg;
        private System.Windows.Forms.RadioButton trailingAgg;
        private System.Windows.Forms.TextBox trailingX;
        private System.Windows.Forms.ComboBox trailingPeriod;
        private System.Windows.Forms.ComboBox toDatePeriod;
        private System.Windows.Forms.RadioButton toDateAgg;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox shiftPeriod;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button addAnalysis;
        private System.Windows.Forms.ListView periodList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColumnHeader aggHeader;
        private System.Windows.Forms.ColumnHeader shiftHeader;
        private System.Windows.Forms.ColumnHeader prefixHead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox prefixBox;
        private System.Windows.Forms.TextBox shiftAmount;
        private System.Windows.Forms.ComboBox shiftDir;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.CheckBox partitionFacts;
        private System.Windows.Forms.DateTimePicker partitionEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker partitionStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox partitionPeriodBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label determinantDayLbl;
        private System.Windows.Forms.Label firstDayLbl;
        private System.Windows.Forms.ComboBox determinantDayBox;
        private System.Windows.Forms.ComboBox firstDayBox;
    }
}