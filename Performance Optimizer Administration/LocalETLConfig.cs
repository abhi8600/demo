﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Performance_Optimizer_Administration
{
    public class LocalETLConfig
    {
        private string localETLLoadGroup;
        private bool useLocalETLConnection;
        private string applicationDirectory;
        private string localETLConnection;

        public LocalETLConfig(string localETLLoadGroup, bool useLocalETLConnection, string applicationDirectory, string localETLConnection)
        {
            this.localETLLoadGroup = localETLLoadGroup;
            this.useLocalETLConnection = useLocalETLConnection;
            this.applicationDirectory = applicationDirectory;
            this.localETLConnection = localETLConnection;
        }

        public string getLocalETLLoadGroup()
        {
            return localETLLoadGroup;
        }

        public bool isUseLocalETLConnection()
        {
            return useLocalETLConnection;
        }

        public string getApplicationDirectory()
        {
            return applicationDirectory;
        }

        public string getLocalETLConnection()
        {
            return localETLConnection;
        }
    }
}
