using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class TransformationForm : Form, ResizeMemoryForm
    {
        MainAdminForm ma;
        private string windowName = "Transformation";

        public TransformationForm(MainAdminForm ma, Transformation st, string[] options, bool isStagingTableTransformation)
        {
            InitializeComponent();
            this.ma = ma;
            resetWindowFromSettings();
            transformSelectBox.Items.Clear();
            transformSelectBox.Items.AddRange(options);
            if (isStagingTableTransformation)
                setTransformationExecutionStage(st == null ? Transformation.AFTER_UNKNOWN_KEYS_REPLACED : st.ExecuteAfter);
            else
                label30.Visible = executeAfterSelectBox.Visible = false;
            stagingFormula.ScrollBars = ScrollBars.Vertical;
            if (st != null)
            {
                if (st.GetType() == typeof(Expression))
                {
                    stagingFormula.Text = ((Expression)st).Formula;
                    transformSelectBox.Text = "Expression";
                }
                else if (st.GetType() == typeof(DateID))
                {
                    this.dateExpressionBox.Text = ((DateID)st).DateExpression;
                    this.dateTypeBox.Text = ((DateID)st).DateType == TimePeriod.PERIOD_DAY ? "Day" : ((DateID)st).DateType == TimePeriod.PERIOD_WEEK ? "Week" : "Month";
                    transformSelectBox.Text = "Date ID";
                }
                else if (st.GetType() == typeof(LogicalColumnDefinition))
                {
                    stagingFormula.Text = ((LogicalColumnDefinition)st).Formula;
                    transformSelectBox.Text = "Logical Column Definition";
                }
                else if (st.GetType() == typeof(TableLookup))
                {
                    lookupExpressionBox.Text = ((TableLookup)st).Formula;
                    lookupTableBox.Text = ((TableLookup)st).LookupTable;
                    joinExpressionBox.Text = ((TableLookup)st).JoinCondition;
                    transformSelectBox.Text = "External Table Lookup";
                }
                else if (st.GetType() == typeof(Procedure))
                {
                    procedureTextBox.Text = ((Procedure)st).Body;
                    procNameBox.Text = ((Procedure)st).Name;
                    transformSelectBox.Text = "Procedure";
                }
                else if (st.GetType() == typeof(JavaTransform))
                {
                    javaTransformName.Text = ((JavaTransform)st).Name;
                    javaClassName.Text = ((JavaTransform)st).ClassName;
                    nameValueTable.Clear();
                    if (((JavaTransform)st).Parameters != null)
                    {
                        foreach (NameValuePair pr in ((JavaTransform) st).Parameters)
                        {
                            nameValueTable.Rows.Add(new object[] { pr.Name, pr.Value });
                        }
                    }
                    transformSelectBox.Text = "Java";
                }
                else if (st.GetType() == typeof(Rank))
                {
                    rankButton.Checked = ((Rank)st).Type == Rank.RankType;
                    ntileButton.Checked = !rankButton.Checked;
                    ntileNum.Text = ((Rank)st).NTileNum.ToString();
                    partitionTable.Clear();
                    if (((Rank)st).PartitionByColumns != null)
                    {
                        foreach (string s in ((Rank)st).PartitionByColumns)
                            partitionTable.Rows.Add(new object[] { s });
                    }
                    orderByTable.Clear();
                    if (((Rank)st).OrderByColumns != null)
                    {
                        for (int i = 0; i < ((Rank)st).OrderByColumns.Length; i++)
                            orderByTable.Rows.Add(new object[] { ((Rank)st).OrderByColumns[i], ((Rank)st).OrderByAscending[i] });
                    }
                    transformSelectBox.Text = "Rank";
                }
                else if (st.GetType() == typeof(Household))
                {
                    HHIDBox.Text = ((Household)st).HHIDColumn;
                    keyColBox.Text = ((Household)st).KeyColumn;
                    AddressBox.Text = ((Household)st).Address;
                    CityBox.Text = ((Household)st).City;
                    StateBox.Text = ((Household)st).State;
                    ZipBox.Text = ((Household)st).Zip;
                    TargetTableBox.Text = ((Household)st).TargetTable;
                    targetKeyBox.Text = ((Household)st).TargetKeyColumn;
                    TargetHHIDBox.Text = ((Household)st).TargetHHIDColumn;
                    TargetAddressBox.Text = ((Household)st).TargetAddress;
                    TargetCityBox.Text = ((Household)st).TargetCity;
                    TargetStateBox.Text = ((Household)st).TargetState;
                    TargetZipBox.Text = ((Household)st).TargetZip;
                    transformSelectBox.Text = "Household";
                }
            }
            else
                transformSelectBox.Text = "None";
        }

        public void setTransformationExecutionStage(int stage)
        {
            switch (stage)
            {
                case Transformation.AFTER_FILES_STAGED:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Files Staged");
                    break;
                case Transformation.AFTER_QUERY_BASED_TABLES_GENERATED:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Query-Based Tables Generated");
                    break;
                case Transformation.AFTER_RANKS_SET:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Ranks Set");
                    break;
                case Transformation.AFTER_CHECKSUMS_UPDATED:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Checksums Updated");
                    break;
                case Transformation.DO_NOT_EXECUTE:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Do Not Execute");
                    break;
                default:
                    executeAfterSelectBox.SelectedIndex = executeAfterSelectBox.Items.IndexOf("Unknown Keys Replaced");
                    break;
            }
        }

        public int getTransformationExecutionStage()
        {
            if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Files Staged"))
                return Transformation.AFTER_FILES_STAGED;
            else if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Query-Based Tables Generated"))
                return Transformation.AFTER_QUERY_BASED_TABLES_GENERATED;
            else if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Unknown Keys Replaced"))
                return Transformation.AFTER_UNKNOWN_KEYS_REPLACED;
            else if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Ranks Set"))
                return Transformation.AFTER_RANKS_SET;
            else if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Checksums Updated"))
                return Transformation.AFTER_CHECKSUMS_UPDATED;
            else if (executeAfterSelectBox.SelectedIndex == executeAfterSelectBox.Items.IndexOf("Do Not Execute"))
                return Transformation.DO_NOT_EXECUTE;
            return Transformation.AFTER_UNKNOWN_KEYS_REPLACED;
        }

        public Transformation getTransformation()
        {
            if (transformSelectBox.Text == "Expression")
            {
                Expression sf = new Expression();
                sf.Formula = stagingFormula.Text;
                sf.ExecuteAfter = getTransformationExecutionStage();
                return (sf);
            }
            else if (transformSelectBox.Text == "Date ID")
            {
                DateID di = new DateID();
                di.DateExpression = dateExpressionBox.Text;
                di.ExecuteAfter = getTransformationExecutionStage();
                if (dateTypeBox.Text == "Day")
                    di.DateType = TimePeriod.PERIOD_DAY;
                else if (dateTypeBox.Text == "Week")
                    di.DateType = TimePeriod.PERIOD_WEEK;
                else
                    di.DateType = TimePeriod.PERIOD_MONTH;
                return (di);
            }
            else if (transformSelectBox.Text == "Logical Column Definition")
            {
                LogicalColumnDefinition lcd = new LogicalColumnDefinition();
                lcd.ExecuteAfter = getTransformationExecutionStage();
                lcd.Formula = stagingFormula.Text;
                return (lcd);
            }
            else if (transformSelectBox.Text == "External Table Lookup")
            {
                TableLookup sl = new TableLookup();
                sl.ExecuteAfter = getTransformationExecutionStage();
                sl.Formula = lookupExpressionBox.Text;
                sl.LookupTable = lookupTableBox.Text;
                sl.JoinCondition = joinExpressionBox.Text;
                return (sl);
            }
            else if (transformSelectBox.Text == "Procedure")
            {
                Procedure sp = new Procedure();
                sp.ExecuteAfter = getTransformationExecutionStage();
                sp.Body = procedureTextBox.Text;
                sp.Name = procNameBox.Text;
                return (sp);
            }
            else if (transformSelectBox.Text == "Java")
            {
                JavaTransform sp = new JavaTransform();
                sp.ExecuteAfter = getTransformationExecutionStage();
                sp.Name = javaTransformName.Text;
                sp.ClassName = javaClassName.Text;
                sp.Parameters = new NameValuePair[nameValueTable.Rows.Count];
                for (int i = 0; i < nameValueTable.Rows.Count; i++)
                    sp.Parameters[i] = new NameValuePair(nameValueTable.Rows[i][0].ToString(), nameValueTable.Rows[i][1].ToString());
 /*
                sp.Parameters[0] = new NameValuePair("melissadata.library", "Dt3Api");
                sp.Parameters[1] = new NameValuePair("melissadata.registration", "DEMO");
                sp.Parameters[2] = new NameValuePair("melissadata.dtloc", "c:/program files (x86)/matchupapi");
                sp.Parameters[3] = new NameValuePair("melissadata.cassloc", "c:/program files (x86)/matchupapi/cassmate");
                sp.Parameters[4] = new NameValuePair("melissadata.matchcode", "Basic Matching");

                sp.Parameters[5] = new NameValuePair("dimensiontable.name", "DW_DM_CUSTOMER");
                sp.Parameters[6] = new NameValuePair("dimensiontable.id", "MATCHID");
                sp.Parameters[7] = new NameValuePair("dimensiontable.matchkey", "MATCHKEY");
                sp.Parameters[8] = new NameValuePair("dimensiontable.clusterkey", "CLUSTERKEY");

                sp.Parameters[9] = new NameValuePair("stagingtable.name", "ST_CUSTOMER");
                sp.Parameters[10] = new NameValuePair("stagingtable.id", "ID");
                sp.Parameters[11] = new NameValuePair("stagingtable.dimensionid", "DIMMATCHID");
                sp.Parameters[12] = new NameValuePair("stagingtable.matchkey", "MATCHKEY");
                sp.Parameters[13] = new NameValuePair("stagingtable.clusterkey", "CLUSTERKEY");
                sp.Parameters[14] = new NameValuePair("stagingtable.zip5", "ZIP5");
                sp.Parameters[15] = new NameValuePair("stagingtable.addr1", "ADDR1");
                sp.Parameters[16] = new NameValuePair("stagingtable.firstname", "FIRSTNAME");
                sp.Parameters[17] = new NameValuePair("stagingtable.lastname", "LASTNAME");
  */
                return (sp);
            }
            else if (transformSelectBox.Text == "Rank")
            {
                Rank rn = new Rank();
                rn.ExecuteAfter = getTransformationExecutionStage();
                rn.Type = rankButton.Checked ? Rank.RankType : Rank.NTileType;
                rn.NTileNum = Int32.Parse(ntileNum.Text);
                rn.PartitionByColumns = new string[partitionTable.Rows.Count];
                for (int i = 0; i < partitionTable.Rows.Count; i++)
                    rn.PartitionByColumns[i] = partitionTable.Rows[i][0].ToString();
                rn.OrderByColumns = new string[orderByTable.Rows.Count];
                rn.OrderByAscending = new bool[orderByTable.Rows.Count];
                for (int i = 0; i < orderByTable.Rows.Count; i++)
                {
                    rn.OrderByColumns[i] = orderByTable.Rows[i][0].ToString();
                    rn.OrderByAscending[i] = (bool)orderByTable.Rows[i][1];
                }
                return (rn);
            }
            else if (transformSelectBox.Text == "Household")
            {
                Household hh = new Household();
                hh.ExecuteAfter = getTransformationExecutionStage();
                hh.HHIDColumn = HHIDBox.Text;
                hh.KeyColumn = keyColBox.Text;
                hh.Address = AddressBox.Text;
                hh.City = CityBox.Text;
                hh.State = StateBox.Text;
                hh.Zip = ZipBox.Text;
                hh.TargetTable = TargetTableBox.Text;
                hh.TargetHHIDColumn = TargetHHIDBox.Text;
                hh.TargetKeyColumn = targetKeyBox.Text;
                hh.TargetAddress = TargetAddressBox.Text;
                hh.TargetCity = TargetCityBox.Text;
                hh.TargetState = TargetStateBox.Text;
                hh.TargetZip = TargetZipBox.Text;
                return (hh);
            }
            return (null);
        }

        private void transformSelectBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            formulaGroup.Visible = false;
            lookupBox.Visible = false;
            procedureBox.Visible = false;
            householdBox.Visible = false;
            rankBox.Visible = false;
            dateIDBox.Visible = false;
            javaTransformBox.Visible = false;

            if (transformSelectBox.Text == "Expression")
            {
                formulaGroup.Visible = true;
            }
            else if (transformSelectBox.Text == "Date ID")
            {
                dateIDBox.Visible = true;
            }
            else if (transformSelectBox.Text == "Logical Column Definition")
            {
                formulaGroup.Visible = true;
            }
            else if (transformSelectBox.Text == "External Table Lookup")
            {
                lookupBox.Visible = true;
            }
            else if (transformSelectBox.Text == "Procedure")
            {
                procedureBox.Visible = true;
            }
            else if (transformSelectBox.Text == "Java")
            {
                javaTransformBox.Visible = true;
            }
            else if (transformSelectBox.Text == "Household")
            {
                householdBox.Visible = true;
            }
            else if (transformSelectBox.Text == "Rank")
            {
                rankBox.Visible = true;
            }
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void TransformationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }

        private void ntileNum_Validating(object sender, CancelEventArgs e)
        {
            int result;
            Int32.TryParse(ntileNum.Text, out result);
            e.Cancel = result == 0;
        }

        public void setColumnLabel(String label)
        {
            if (label != "")
            {
                columnLabel.Text = label;
                columnLabel.Visible = true;
            }            
        }

    }
}