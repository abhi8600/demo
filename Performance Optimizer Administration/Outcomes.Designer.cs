namespace Performance_Optimizer_Administration
{
    partial class Outcomes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Outcomes));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.outcomesTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nameFilterBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.scoringFilterComboBox = new System.Windows.Forms.ComboBox();
            this.trainingFilterLabel = new System.Windows.Forms.Label();
            this.trainingFilterComboBox = new System.Windows.Forms.ComboBox();
            this.outcomeModelIterationValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.classifierSettings = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.exclusionRulesGrid = new System.Windows.Forms.DataGridView();
            this.PriorityExclusion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilterExclusion = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ExplanationExclusion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.exclusionRuleGroup = new System.Windows.Forms.GroupBox();
            this.ruleGrid = new System.Windows.Forms.DataGridView();
            this.Priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Explanation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classificationRulesGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.targetOverrideBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numToRank = new System.Windows.Forms.TextBox();
            this.factorDataView = new System.Windows.Forms.DataGridView();
            this.FactorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseValue = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BaseValues = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TestValue = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TestValues = new System.Windows.Forms.DataGridViewButtonColumn();
            this.outcomeFactorsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setBaseItems = new System.Windows.Forms.ToolStripMenuItem();
            this.currentBase = new System.Windows.Forms.ToolStripMenuItem();
            this.averageBase = new System.Windows.Forms.ToolStripMenuItem();
            this.zeroBase = new System.Windows.Forms.ToolStripMenuItem();
            this.setTestItems = new System.Windows.Forms.ToolStripMenuItem();
            this.currentTest = new System.Windows.Forms.ToolStripMenuItem();
            this.averageTest = new System.Windows.Forms.ToolStripMenuItem();
            this.zeroTest = new System.Windows.Forms.ToolStripMenuItem();
            this.rankFactors = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.minOutcomeVal = new System.Windows.Forms.TextBox();
            this.maxOutcomeVal = new System.Windows.Forms.TextBox();
            this.estimateValue = new System.Windows.Forms.CheckBox();
            this.excludeBelowMin = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.bucketOutcomePctDiff = new System.Windows.Forms.RadioButton();
            this.bucketOutcomeDifference = new System.Windows.Forms.RadioButton();
            this.bucketOutcomeValue = new System.Windows.Forms.RadioButton();
            this.excludeAboveMax = new System.Windows.Forms.CheckBox();
            this.minOutcomeRelative = new System.Windows.Forms.CheckBox();
            this.label42 = new System.Windows.Forms.Label();
            this.includeAllNonzero = new System.Windows.Forms.CheckBox();
            this.label43 = new System.Windows.Forms.Label();
            this.maxOutcomeRelative = new System.Windows.Forms.CheckBox();
            this.addOutcomeBucket = new System.Windows.Forms.Button();
            this.useMinOutcomeVal = new System.Windows.Forms.CheckBox();
            this.outcomeBucketMin = new System.Windows.Forms.TextBox();
            this.useMaxOutcomeVal = new System.Windows.Forms.CheckBox();
            this.outcomeBucketList = new System.Windows.Forms.ListView();
            this.columnHeader36 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader37 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader38 = new System.Windows.Forms.ColumnHeader();
            this.minOutcomeRelMeasure = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.maxOutcomeRelMeasure = new System.Windows.Forms.CheckBox();
            this.removeOutcomeBucket = new System.Windows.Forms.Button();
            this.minOutcomeMeasure = new System.Windows.Forms.ComboBox();
            this.outcomeBucketMax = new System.Windows.Forms.TextBox();
            this.maxOutcomeMeasure = new System.Windows.Forms.ComboBox();
            this.outcomeBucketName = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.bucketOutcome = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.estimateDifferenceFromActual = new System.Windows.Forms.RadioButton();
            this.estimateOutcome = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.estimateValueInClass = new System.Windows.Forms.CheckBox();
            this.classMeasureBox = new System.Windows.Forms.ComboBox();
            this.classLabel = new System.Windows.Forms.Label();
            this.outputOnlyClass = new System.Windows.Forms.CheckBox();
            this.estimateClass = new System.Windows.Forms.CheckBox();
            this.biasUniform = new System.Windows.Forms.CheckBox();
            this.outcomeGroupTable = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.outcomeGroup = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.outcomeModel = new System.Windows.Forms.ComboBox();
            this.outcomeTableName = new System.Windows.Forms.TextBox();
            this.labelBaseModelDataset = new System.Windows.Forms.Label();
            this.dropOutcomeTable = new System.Windows.Forms.CheckBox();
            this.outcomeIterationValue = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.outcomeMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addOutcomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeOutcomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.outcomesTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exclusionRulesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ruleGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.factorDataView)).BeginInit();
            this.outcomeFactorsMenu.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.outcomeMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.outcomesTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 866);
            this.tabControl.TabIndex = 0;
            // 
            // outcomesTabPage
            // 
            this.outcomesTabPage.AutoScroll = true;
            this.outcomesTabPage.Controls.Add(this.groupBox1);
            this.outcomesTabPage.Location = new System.Drawing.Point(4, 22);
            this.outcomesTabPage.Name = "outcomesTabPage";
            this.outcomesTabPage.Size = new System.Drawing.Size(844, 840);
            this.outcomesTabPage.TabIndex = 3;
            this.outcomesTabPage.Text = "Outcomes";
            this.outcomesTabPage.UseVisualStyleBackColor = true;
            this.outcomesTabPage.Leave += new System.EventHandler(this.outcomesTabPage_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nameFilterBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.scoringFilterComboBox);
            this.groupBox1.Controls.Add(this.trainingFilterLabel);
            this.groupBox1.Controls.Add(this.trainingFilterComboBox);
            this.groupBox1.Controls.Add(this.outcomeModelIterationValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.classifierSettings);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.targetOverrideBox);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.outcomeGroupTable);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.outcomeGroup);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.outcomeModel);
            this.groupBox1.Controls.Add(this.outcomeTableName);
            this.groupBox1.Controls.Add(this.labelBaseModelDataset);
            this.groupBox1.Controls.Add(this.dropOutcomeTable);
            this.groupBox1.Controls.Add(this.outcomeIterationValue);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(832, 834);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Outcome Definition";
            // 
            // nameFilterBox
            // 
            this.nameFilterBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nameFilterBox.Location = new System.Drawing.Point(681, 116);
            this.nameFilterBox.Name = "nameFilterBox";
            this.nameFilterBox.Size = new System.Drawing.Size(141, 20);
            this.nameFilterBox.TabIndex = 138;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(678, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 21);
            this.label5.TabIndex = 137;
            this.label5.Text = "Measure Name Filter";
            this.toolTip.SetToolTip(this.label5, "Filter for limiting modeling/the data used for training");
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(419, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 136;
            this.label4.Text = "Scoring Filter";
            this.toolTip.SetToolTip(this.label4, "Filter for limiting modeling/the data used for scoring");
            // 
            // scoringFilterComboBox
            // 
            this.scoringFilterComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.scoringFilterComboBox.Location = new System.Drawing.Point(529, 115);
            this.scoringFilterComboBox.Name = "scoringFilterComboBox";
            this.scoringFilterComboBox.Size = new System.Drawing.Size(143, 21);
            this.scoringFilterComboBox.TabIndex = 135;
            // 
            // trainingFilterLabel
            // 
            this.trainingFilterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trainingFilterLabel.Location = new System.Drawing.Point(419, 89);
            this.trainingFilterLabel.Name = "trainingFilterLabel";
            this.trainingFilterLabel.Size = new System.Drawing.Size(104, 16);
            this.trainingFilterLabel.TabIndex = 134;
            this.trainingFilterLabel.Text = "Modeling Filter";
            this.toolTip.SetToolTip(this.trainingFilterLabel, "Filter for limiting modeling/the data used for training");
            // 
            // trainingFilterComboBox
            // 
            this.trainingFilterComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trainingFilterComboBox.Location = new System.Drawing.Point(529, 89);
            this.trainingFilterComboBox.Name = "trainingFilterComboBox";
            this.trainingFilterComboBox.Size = new System.Drawing.Size(143, 21);
            this.trainingFilterComboBox.TabIndex = 133;
            // 
            // outcomeModelIterationValue
            // 
            this.outcomeModelIterationValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeModelIterationValue.Location = new System.Drawing.Point(186, 85);
            this.outcomeModelIterationValue.Name = "outcomeModelIterationValue";
            this.outcomeModelIterationValue.Size = new System.Drawing.Size(200, 20);
            this.outcomeModelIterationValue.TabIndex = 131;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(170, 16);
            this.label3.TabIndex = 132;
            this.label3.Text = "Iteration Value(s) to Model";
            this.toolTip.SetToolTip(this.label3, "Iteration Value(s) to Model.  Separate multiple values with commas.");
            // 
            // classifierSettings
            // 
            this.classifierSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classifierSettings.Location = new System.Drawing.Point(749, 15);
            this.classifierSettings.Name = "classifierSettings";
            this.classifierSettings.Size = new System.Drawing.Size(75, 51);
            this.classifierSettings.TabIndex = 130;
            this.classifierSettings.Text = "Classifier Settings";
            this.toolTip.SetToolTip(this.classifierSettings, "For outcome-specific classifier settings");
            this.classifierSettings.UseVisualStyleBackColor = true;
            this.classifierSettings.Click += new System.EventHandler(this.classifierSettings_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.exclusionRulesGrid);
            this.groupBox4.Controls.Add(this.exclusionRuleGroup);
            this.groupBox4.Controls.Add(this.ruleGrid);
            this.groupBox4.Controls.Add(this.classificationRulesGroup);
            this.groupBox4.Location = new System.Drawing.Point(10, 558);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(812, 270);
            this.groupBox4.TabIndex = 129;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Rule-based Classification";
            // 
            // exclusionRulesGrid
            // 
            this.exclusionRulesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.exclusionRulesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.exclusionRulesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PriorityExclusion,
            this.FilterExclusion,
            this.ExplanationExclusion});
            this.exclusionRulesGrid.Location = new System.Drawing.Point(17, 154);
            this.exclusionRulesGrid.Name = "exclusionRulesGrid";
            this.exclusionRulesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.exclusionRulesGrid.Size = new System.Drawing.Size(770, 90);
            this.exclusionRulesGrid.StandardTab = true;
            this.exclusionRulesGrid.TabIndex = 129;
            this.toolTip.SetToolTip(this.exclusionRulesGrid, "Rules used to exclude instances from scoring");
            this.exclusionRulesGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.exclusionRuleGrid_rowEntered);
            // 
            // PriorityExclusion
            // 
            this.PriorityExclusion.HeaderText = "Priority";
            this.PriorityExclusion.Name = "PriorityExclusion";
            this.PriorityExclusion.Width = 50;
            // 
            // FilterExclusion
            // 
            this.FilterExclusion.HeaderText = "Filter";
            this.FilterExclusion.Name = "FilterExclusion";
            this.FilterExclusion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FilterExclusion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FilterExclusion.Width = 200;
            // 
            // ExplanationExclusion
            // 
            this.ExplanationExclusion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExplanationExclusion.HeaderText = "Explanation";
            this.ExplanationExclusion.Name = "ExplanationExclusion";
            // 
            // exclusionRuleGroup
            // 
            this.exclusionRuleGroup.Location = new System.Drawing.Point(9, 135);
            this.exclusionRuleGroup.Name = "exclusionRuleGroup";
            this.exclusionRuleGroup.Size = new System.Drawing.Size(790, 120);
            this.exclusionRuleGroup.TabIndex = 131;
            this.exclusionRuleGroup.TabStop = false;
            this.exclusionRuleGroup.Text = "Exclusion Rules";
            // 
            // ruleGrid
            // 
            this.ruleGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ruleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ruleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Priority,
            this.Filter,
            this.Explanation});
            this.ruleGrid.Location = new System.Drawing.Point(18, 33);
            this.ruleGrid.Name = "ruleGrid";
            this.ruleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.ruleGrid.Size = new System.Drawing.Size(770, 90);
            this.ruleGrid.StandardTab = true;
            this.ruleGrid.TabIndex = 128;
            this.toolTip.SetToolTip(this.ruleGrid, "Used to classifiy instances (instead of modeling)");
            this.ruleGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.ruleGrid_CellBeginEdit);
            this.ruleGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ruleGrid_rowEntered);
            // 
            // Priority
            // 
            this.Priority.HeaderText = "Priority";
            this.Priority.Name = "Priority";
            this.Priority.Width = 50;
            // 
            // Filter
            // 
            this.Filter.HeaderText = "Filter";
            this.Filter.Name = "Filter";
            this.Filter.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Filter.Width = 200;
            // 
            // Explanation
            // 
            this.Explanation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Explanation.HeaderText = "Explanation";
            this.Explanation.Name = "Explanation";
            // 
            // classificationRulesGroup
            // 
            this.classificationRulesGroup.Location = new System.Drawing.Point(10, 15);
            this.classificationRulesGroup.Name = "classificationRulesGroup";
            this.classificationRulesGroup.Size = new System.Drawing.Size(790, 120);
            this.classificationRulesGroup.TabIndex = 130;
            this.classificationRulesGroup.TabStop = false;
            this.classificationRulesGroup.Text = "Classification Rules";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(419, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 127;
            this.label2.Text = "Override Target";
            // 
            // targetOverrideBox
            // 
            this.targetOverrideBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetOverrideBox.Location = new System.Drawing.Point(529, 15);
            this.targetOverrideBox.Name = "targetOverrideBox";
            this.targetOverrideBox.Size = new System.Drawing.Size(200, 21);
            this.targetOverrideBox.TabIndex = 126;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.numToRank);
            this.groupBox3.Controls.Add(this.factorDataView);
            this.groupBox3.Controls.Add(this.rankFactors);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.minOutcomeVal);
            this.groupBox3.Controls.Add(this.maxOutcomeVal);
            this.groupBox3.Controls.Add(this.estimateValue);
            this.groupBox3.Controls.Add(this.excludeBelowMin);
            this.groupBox3.Controls.Add(this.panel3);
            this.groupBox3.Controls.Add(this.excludeAboveMax);
            this.groupBox3.Controls.Add(this.minOutcomeRelative);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.includeAllNonzero);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.maxOutcomeRelative);
            this.groupBox3.Controls.Add(this.addOutcomeBucket);
            this.groupBox3.Controls.Add(this.useMinOutcomeVal);
            this.groupBox3.Controls.Add(this.outcomeBucketMin);
            this.groupBox3.Controls.Add(this.useMaxOutcomeVal);
            this.groupBox3.Controls.Add(this.outcomeBucketList);
            this.groupBox3.Controls.Add(this.minOutcomeRelMeasure);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.maxOutcomeRelMeasure);
            this.groupBox3.Controls.Add(this.removeOutcomeBucket);
            this.groupBox3.Controls.Add(this.minOutcomeMeasure);
            this.groupBox3.Controls.Add(this.outcomeBucketMax);
            this.groupBox3.Controls.Add(this.maxOutcomeMeasure);
            this.groupBox3.Controls.Add(this.outcomeBucketName);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.bucketOutcome);
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Location = new System.Drawing.Point(11, 206);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(812, 350);
            this.groupBox3.TabIndex = 125;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Value Estimation";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(235, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 23);
            this.label1.TabIndex = 127;
            this.label1.Text = "Number of Factors to Rank";
            this.toolTip.SetToolTip(this.label1, "Number of factors to rank (0 for all)");
            // 
            // numToRank
            // 
            this.numToRank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numToRank.Location = new System.Drawing.Point(411, 272);
            this.numToRank.Name = "numToRank";
            this.numToRank.Size = new System.Drawing.Size(28, 20);
            this.numToRank.TabIndex = 126;
            this.numToRank.Text = "0";
            this.numToRank.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // factorDataView
            // 
            this.factorDataView.AllowUserToAddRows = false;
            this.factorDataView.AllowUserToDeleteRows = false;
            this.factorDataView.AllowUserToResizeColumns = false;
            this.factorDataView.AllowUserToResizeRows = false;
            this.factorDataView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.factorDataView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.factorDataView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.factorDataView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.factorDataView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.factorDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.factorDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FactorName,
            this.BaseValue,
            this.BaseValues,
            this.TestValue,
            this.TestValues});
            this.factorDataView.ContextMenuStrip = this.outcomeFactorsMenu;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.factorDataView.DefaultCellStyle = dataGridViewCellStyle9;
            this.factorDataView.Location = new System.Drawing.Point(13, 297);
            this.factorDataView.Name = "factorDataView";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.factorDataView.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.factorDataView.RowHeadersVisible = false;
            this.factorDataView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.factorDataView.RowTemplate.DividerHeight = 1;
            this.factorDataView.RowTemplate.Height = 18;
            this.factorDataView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.factorDataView.ShowEditingIcon = false;
            this.factorDataView.Size = new System.Drawing.Size(770, 40);
            this.factorDataView.TabIndex = 125;
            this.factorDataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.factorDataView_CellContentClick);
            // 
            // FactorName
            // 
            this.FactorName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FactorName.HeaderText = "Factor";
            this.FactorName.Name = "FactorName";
            this.FactorName.ReadOnly = true;
            this.FactorName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // BaseValue
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.BaseValue.DefaultCellStyle = dataGridViewCellStyle7;
            this.BaseValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BaseValue.HeaderText = "Base";
            this.BaseValue.Items.AddRange(new object[] {
            "Current",
            "Zero",
            "Average",
            "Specific Value"});
            this.BaseValue.Name = "BaseValue";
            this.BaseValue.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // BaseValues
            // 
            this.BaseValues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BaseValues.HeaderText = "Base Values";
            this.BaseValues.Name = "BaseValues";
            this.BaseValues.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BaseValues.Text = "";
            this.BaseValues.Width = 175;
            // 
            // TestValue
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TestValue.DefaultCellStyle = dataGridViewCellStyle8;
            this.TestValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TestValue.HeaderText = "Test";
            this.TestValue.Items.AddRange(new object[] {
            "Current",
            "Zero",
            "Average",
            "Specific Value"});
            this.TestValue.Name = "TestValue";
            // 
            // TestValues
            // 
            this.TestValues.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TestValues.HeaderText = "Test Values";
            this.TestValues.Name = "TestValues";
            this.TestValues.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TestValues.Text = "";
            this.TestValues.Width = 175;
            // 
            // outcomeFactorsMenu
            // 
            this.outcomeFactorsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setBaseItems,
            this.setTestItems});
            this.outcomeFactorsMenu.Name = "outcomeFactorsMenu";
            this.outcomeFactorsMenu.ShowImageMargin = false;
            this.outcomeFactorsMenu.Size = new System.Drawing.Size(199, 48);
            // 
            // setBaseItems
            // 
            this.setBaseItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currentBase,
            this.averageBase,
            this.zeroBase});
            this.setBaseItems.Name = "setBaseItems";
            this.setBaseItems.Size = new System.Drawing.Size(198, 22);
            this.setBaseItems.Text = "Set Base for Selected Factors";
            // 
            // currentBase
            // 
            this.currentBase.Name = "currentBase";
            this.currentBase.Size = new System.Drawing.Size(117, 22);
            this.currentBase.Text = "Curent";
            this.currentBase.Click += new System.EventHandler(this.currentBase_Click);
            // 
            // averageBase
            // 
            this.averageBase.Name = "averageBase";
            this.averageBase.Size = new System.Drawing.Size(117, 22);
            this.averageBase.Text = "Average";
            this.averageBase.Click += new System.EventHandler(this.averageBase_Click);
            // 
            // zeroBase
            // 
            this.zeroBase.Name = "zeroBase";
            this.zeroBase.Size = new System.Drawing.Size(117, 22);
            this.zeroBase.Text = "Zero";
            this.zeroBase.Click += new System.EventHandler(this.zeroBase_Click);
            // 
            // setTestItems
            // 
            this.setTestItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currentTest,
            this.averageTest,
            this.zeroTest});
            this.setTestItems.Name = "setTestItems";
            this.setTestItems.Size = new System.Drawing.Size(198, 22);
            this.setTestItems.Text = "Set Test for Selected Factors";
            // 
            // currentTest
            // 
            this.currentTest.Name = "currentTest";
            this.currentTest.Size = new System.Drawing.Size(117, 22);
            this.currentTest.Text = "Current";
            this.currentTest.Click += new System.EventHandler(this.currentTest_Click);
            // 
            // averageTest
            // 
            this.averageTest.Name = "averageTest";
            this.averageTest.Size = new System.Drawing.Size(117, 22);
            this.averageTest.Text = "Average";
            this.averageTest.Click += new System.EventHandler(this.averageTest_Click);
            // 
            // zeroTest
            // 
            this.zeroTest.Name = "zeroTest";
            this.zeroTest.Size = new System.Drawing.Size(117, 22);
            this.zeroTest.Text = "Zero";
            this.zeroTest.Click += new System.EventHandler(this.zeroTest_Click);
            // 
            // rankFactors
            // 
            this.rankFactors.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rankFactors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rankFactors.Location = new System.Drawing.Point(10, 268);
            this.rankFactors.Name = "rankFactors";
            this.rankFactors.Size = new System.Drawing.Size(220, 24);
            this.rankFactors.TabIndex = 124;
            this.rankFactors.Text = "Rank Factors Driving Outcome";
            this.toolTip.SetToolTip(this.rankFactors, "Rank a set of factors based on their influence on the outcome");
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 85);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(200, 23);
            this.label37.TabIndex = 97;
            this.label37.Text = "Estimated Value Range Settings";
            this.toolTip.SetToolTip(this.label37, "Restrict the range of estimated outcomes");
            // 
            // minOutcomeVal
            // 
            this.minOutcomeVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minOutcomeVal.Location = new System.Drawing.Point(158, 108);
            this.minOutcomeVal.Name = "minOutcomeVal";
            this.minOutcomeVal.Size = new System.Drawing.Size(72, 20);
            this.minOutcomeVal.TabIndex = 11;
            // 
            // maxOutcomeVal
            // 
            this.maxOutcomeVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxOutcomeVal.Location = new System.Drawing.Point(158, 132);
            this.maxOutcomeVal.Name = "maxOutcomeVal";
            this.maxOutcomeVal.Size = new System.Drawing.Size(72, 20);
            this.maxOutcomeVal.TabIndex = 17;
            // 
            // estimateValue
            // 
            this.estimateValue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estimateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estimateValue.Location = new System.Drawing.Point(10, 18);
            this.estimateValue.Name = "estimateValue";
            this.estimateValue.Size = new System.Drawing.Size(192, 18);
            this.estimateValue.TabIndex = 123;
            this.estimateValue.Text = "Estimate Value of Outcome";
            this.toolTip.SetToolTip(this.estimateValue, "Whether to estimate the value of the target measure");
            // 
            // excludeBelowMin
            // 
            this.excludeBelowMin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.excludeBelowMin.Location = new System.Drawing.Point(238, 108);
            this.excludeBelowMin.Name = "excludeBelowMin";
            this.excludeBelowMin.Size = new System.Drawing.Size(160, 24);
            this.excludeBelowMin.TabIndex = 12;
            this.excludeBelowMin.Text = "Exclude from class if below";
            this.toolTip.SetToolTip(this.excludeBelowMin, "Exclude an individual from predicted class membership if the predicated value of " +
                    "the target measure is below a value");
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.bucketOutcomePctDiff);
            this.panel3.Controls.Add(this.bucketOutcomeDifference);
            this.panel3.Controls.Add(this.bucketOutcomeValue);
            this.panel3.Location = new System.Drawing.Point(10, 182);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(179, 72);
            this.panel3.TabIndex = 119;
            // 
            // bucketOutcomePctDiff
            // 
            this.bucketOutcomePctDiff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bucketOutcomePctDiff.Location = new System.Drawing.Point(8, 48);
            this.bucketOutcomePctDiff.Name = "bucketOutcomePctDiff";
            this.bucketOutcomePctDiff.Size = new System.Drawing.Size(171, 24);
            this.bucketOutcomePctDiff.TabIndex = 2;
            this.bucketOutcomePctDiff.Text = "% Difference From Actual";
            // 
            // bucketOutcomeDifference
            // 
            this.bucketOutcomeDifference.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bucketOutcomeDifference.Location = new System.Drawing.Point(8, 24);
            this.bucketOutcomeDifference.Name = "bucketOutcomeDifference";
            this.bucketOutcomeDifference.Size = new System.Drawing.Size(144, 24);
            this.bucketOutcomeDifference.TabIndex = 1;
            this.bucketOutcomeDifference.Text = "Difference From Actual";
            // 
            // bucketOutcomeValue
            // 
            this.bucketOutcomeValue.Checked = true;
            this.bucketOutcomeValue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bucketOutcomeValue.Location = new System.Drawing.Point(8, 0);
            this.bucketOutcomeValue.Name = "bucketOutcomeValue";
            this.bucketOutcomeValue.Size = new System.Drawing.Size(104, 24);
            this.bucketOutcomeValue.TabIndex = 0;
            this.bucketOutcomeValue.TabStop = true;
            this.bucketOutcomeValue.Text = "Outcome Value";
            // 
            // excludeAboveMax
            // 
            this.excludeAboveMax.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.excludeAboveMax.Location = new System.Drawing.Point(238, 132);
            this.excludeAboveMax.Name = "excludeAboveMax";
            this.excludeAboveMax.Size = new System.Drawing.Size(160, 24);
            this.excludeAboveMax.TabIndex = 18;
            this.excludeAboveMax.Text = "Exclude from class if above";
            this.toolTip.SetToolTip(this.excludeAboveMax, "Exclude an individual from predicted class membership if the predicated value of " +
                    "the target measure is above a value");
            // 
            // minOutcomeRelative
            // 
            this.minOutcomeRelative.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minOutcomeRelative.Location = new System.Drawing.Point(401, 108);
            this.minOutcomeRelative.Name = "minOutcomeRelative";
            this.minOutcomeRelative.Size = new System.Drawing.Size(71, 24);
            this.minOutcomeRelative.TabIndex = 13;
            this.minOutcomeRelative.Text = "Relative";
            this.toolTip.SetToolTip(this.minOutcomeRelative, resources.GetString("minOutcomeRelative.ToolTip"));
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(605, 230);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 23);
            this.label42.TabIndex = 117;
            this.label42.Text = "Max";
            // 
            // includeAllNonzero
            // 
            this.includeAllNonzero.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.includeAllNonzero.Location = new System.Drawing.Point(208, 19);
            this.includeAllNonzero.Name = "includeAllNonzero";
            this.includeAllNonzero.Size = new System.Drawing.Size(216, 16);
            this.includeAllNonzero.TabIndex = 121;
            this.includeAllNonzero.Text = "Include All Non-zero Values";
            this.toolTip.SetToolTip(this.includeAllNonzero, "When sampling to predict the target measure, whether to include all nonzero measu" +
                    "re values in the population. Useful when nonzero values are rare");
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(605, 206);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 23);
            this.label43.TabIndex = 116;
            this.label43.Text = "Min";
            // 
            // maxOutcomeRelative
            // 
            this.maxOutcomeRelative.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.maxOutcomeRelative.Location = new System.Drawing.Point(402, 132);
            this.maxOutcomeRelative.Name = "maxOutcomeRelative";
            this.maxOutcomeRelative.Size = new System.Drawing.Size(70, 24);
            this.maxOutcomeRelative.TabIndex = 19;
            this.maxOutcomeRelative.Text = "Relative";
            this.toolTip.SetToolTip(this.maxOutcomeRelative, resources.GetString("maxOutcomeRelative.ToolTip"));
            // 
            // addOutcomeBucket
            // 
            this.addOutcomeBucket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addOutcomeBucket.Location = new System.Drawing.Point(741, 226);
            this.addOutcomeBucket.Name = "addOutcomeBucket";
            this.addOutcomeBucket.Size = new System.Drawing.Size(64, 24);
            this.addOutcomeBucket.TabIndex = 113;
            this.addOutcomeBucket.Text = "Add";
            this.addOutcomeBucket.Click += new System.EventHandler(this.addOutcomeBucket_Click);
            // 
            // useMinOutcomeVal
            // 
            this.useMinOutcomeVal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.useMinOutcomeVal.Location = new System.Drawing.Point(22, 108);
            this.useMinOutcomeVal.Name = "useMinOutcomeVal";
            this.useMinOutcomeVal.Size = new System.Drawing.Size(128, 24);
            this.useMinOutcomeVal.TabIndex = 10;
            this.useMinOutcomeVal.Text = "Min Estimated Value";
            this.toolTip.SetToolTip(this.useMinOutcomeVal, "Set a minimum on the estimated value");
            // 
            // outcomeBucketMin
            // 
            this.outcomeBucketMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeBucketMin.Location = new System.Drawing.Point(663, 206);
            this.outcomeBucketMin.Name = "outcomeBucketMin";
            this.outcomeBucketMin.Size = new System.Drawing.Size(72, 20);
            this.outcomeBucketMin.TabIndex = 111;
            // 
            // useMaxOutcomeVal
            // 
            this.useMaxOutcomeVal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.useMaxOutcomeVal.Location = new System.Drawing.Point(22, 132);
            this.useMaxOutcomeVal.Name = "useMaxOutcomeVal";
            this.useMaxOutcomeVal.Size = new System.Drawing.Size(136, 24);
            this.useMaxOutcomeVal.TabIndex = 16;
            this.useMaxOutcomeVal.Text = "Max Estimated Value";
            this.toolTip.SetToolTip(this.useMaxOutcomeVal, "Set a maximum on the estimated value");
            // 
            // outcomeBucketList
            // 
            this.outcomeBucketList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeBucketList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38});
            this.outcomeBucketList.FullRowSelect = true;
            this.outcomeBucketList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.outcomeBucketList.Location = new System.Drawing.Point(195, 182);
            this.outcomeBucketList.MultiSelect = false;
            this.outcomeBucketList.Name = "outcomeBucketList";
            this.outcomeBucketList.Size = new System.Drawing.Size(334, 80);
            this.outcomeBucketList.TabIndex = 118;
            this.outcomeBucketList.UseCompatibleStateImageBehavior = false;
            this.outcomeBucketList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Name";
            this.columnHeader36.Width = 179;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Min";
            this.columnHeader37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader37.Width = 75;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Max";
            this.columnHeader38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader38.Width = 75;
            // 
            // minOutcomeRelMeasure
            // 
            this.minOutcomeRelMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minOutcomeRelMeasure.Location = new System.Drawing.Point(478, 108);
            this.minOutcomeRelMeasure.Name = "minOutcomeRelMeasure";
            this.minOutcomeRelMeasure.Size = new System.Drawing.Size(184, 24);
            this.minOutcomeRelMeasure.TabIndex = 14;
            this.minOutcomeRelMeasure.Text = "Relative to Measure (vs. Actual)";
            this.toolTip.SetToolTip(this.minOutcomeRelMeasure, "If checked, the mimimum value is used relative to a specified measure (as opposed" +
                    " to the actual value of the target measure)");
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(605, 182);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(56, 23);
            this.label44.TabIndex = 115;
            this.label44.Text = "Bucket";
            // 
            // maxOutcomeRelMeasure
            // 
            this.maxOutcomeRelMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.maxOutcomeRelMeasure.Location = new System.Drawing.Point(478, 132);
            this.maxOutcomeRelMeasure.Name = "maxOutcomeRelMeasure";
            this.maxOutcomeRelMeasure.Size = new System.Drawing.Size(184, 24);
            this.maxOutcomeRelMeasure.TabIndex = 20;
            this.maxOutcomeRelMeasure.Text = "Relative to Measure (vs. Actual)";
            this.toolTip.SetToolTip(this.maxOutcomeRelMeasure, "If checked, the mimimum value is used relative to a specified measure (as opposed" +
                    " to the actual value of the target measure)");
            // 
            // removeOutcomeBucket
            // 
            this.removeOutcomeBucket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeOutcomeBucket.Location = new System.Drawing.Point(535, 178);
            this.removeOutcomeBucket.Name = "removeOutcomeBucket";
            this.removeOutcomeBucket.Size = new System.Drawing.Size(64, 24);
            this.removeOutcomeBucket.TabIndex = 114;
            this.removeOutcomeBucket.Text = "Remove";
            this.removeOutcomeBucket.Click += new System.EventHandler(this.removeOutcomeBucket_Click);
            // 
            // minOutcomeMeasure
            // 
            this.minOutcomeMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minOutcomeMeasure.Location = new System.Drawing.Point(662, 108);
            this.minOutcomeMeasure.Name = "minOutcomeMeasure";
            this.minOutcomeMeasure.Size = new System.Drawing.Size(144, 21);
            this.minOutcomeMeasure.TabIndex = 15;
            // 
            // outcomeBucketMax
            // 
            this.outcomeBucketMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeBucketMax.Location = new System.Drawing.Point(663, 230);
            this.outcomeBucketMax.Name = "outcomeBucketMax";
            this.outcomeBucketMax.Size = new System.Drawing.Size(72, 20);
            this.outcomeBucketMax.TabIndex = 112;
            // 
            // maxOutcomeMeasure
            // 
            this.maxOutcomeMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.maxOutcomeMeasure.Location = new System.Drawing.Point(662, 132);
            this.maxOutcomeMeasure.Name = "maxOutcomeMeasure";
            this.maxOutcomeMeasure.Size = new System.Drawing.Size(144, 21);
            this.maxOutcomeMeasure.TabIndex = 21;
            // 
            // outcomeBucketName
            // 
            this.outcomeBucketName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeBucketName.Location = new System.Drawing.Point(663, 182);
            this.outcomeBucketName.Name = "outcomeBucketName";
            this.outcomeBucketName.Size = new System.Drawing.Size(132, 20);
            this.outcomeBucketName.TabIndex = 110;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(7, 39);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(143, 21);
            this.label38.TabIndex = 107;
            this.label38.Text = "Estimated Value Type";
            // 
            // bucketOutcome
            // 
            this.bucketOutcome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bucketOutcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bucketOutcome.Location = new System.Drawing.Point(10, 158);
            this.bucketOutcome.Name = "bucketOutcome";
            this.bucketOutcome.Size = new System.Drawing.Size(168, 24);
            this.bucketOutcome.TabIndex = 109;
            this.bucketOutcome.Text = "Output Bucketed Result";
            this.toolTip.SetToolTip(this.bucketOutcome, resources.GetString("bucketOutcome.ToolTip"));
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.estimateDifferenceFromActual);
            this.panel2.Controls.Add(this.estimateOutcome);
            this.panel2.Location = new System.Drawing.Point(14, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(248, 24);
            this.panel2.TabIndex = 108;
            // 
            // estimateDifferenceFromActual
            // 
            this.estimateDifferenceFromActual.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estimateDifferenceFromActual.Location = new System.Drawing.Point(112, -4);
            this.estimateDifferenceFromActual.Name = "estimateDifferenceFromActual";
            this.estimateDifferenceFromActual.Size = new System.Drawing.Size(152, 24);
            this.estimateDifferenceFromActual.TabIndex = 1;
            this.estimateDifferenceFromActual.Text = "Difference From Actual";
            this.toolTip.SetToolTip(this.estimateDifferenceFromActual, "Output the difference of the estimated value from the actual value of the target " +
                    "measure (i.e. predict incremental change)");
            // 
            // estimateOutcome
            // 
            this.estimateOutcome.Checked = true;
            this.estimateOutcome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estimateOutcome.Location = new System.Drawing.Point(0, -4);
            this.estimateOutcome.Name = "estimateOutcome";
            this.estimateOutcome.Size = new System.Drawing.Size(104, 24);
            this.estimateOutcome.TabIndex = 0;
            this.estimateOutcome.TabStop = true;
            this.estimateOutcome.Text = "Outcome Value";
            this.toolTip.SetToolTip(this.estimateOutcome, "Output the estimated value of the target measure");
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.estimateValueInClass);
            this.groupBox2.Controls.Add(this.classMeasureBox);
            this.groupBox2.Controls.Add(this.classLabel);
            this.groupBox2.Controls.Add(this.outputOnlyClass);
            this.groupBox2.Controls.Add(this.estimateClass);
            this.groupBox2.Controls.Add(this.biasUniform);
            this.groupBox2.Location = new System.Drawing.Point(11, 137);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(813, 69);
            this.groupBox2.TabIndex = 124;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Class Estimation";
            // 
            // estimateValueInClass
            // 
            this.estimateValueInClass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estimateValueInClass.Location = new System.Drawing.Point(322, 23);
            this.estimateValueInClass.Name = "estimateValueInClass";
            this.estimateValueInClass.Size = new System.Drawing.Size(216, 24);
            this.estimateValueInClass.TabIndex = 87;
            this.estimateValueInClass.Text = "Estimate Value of Outcome if in Class";
            this.toolTip.SetToolTip(this.estimateValueInClass, "For individuals that are predicted to be in the target class, estimate the value " +
                    "of the target measure");
            // 
            // classMeasureBox
            // 
            this.classMeasureBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.classMeasureBox.Location = new System.Drawing.Point(122, 43);
            this.classMeasureBox.Name = "classMeasureBox";
            this.classMeasureBox.Size = new System.Drawing.Size(176, 21);
            this.classMeasureBox.TabIndex = 8;
            // 
            // classLabel
            // 
            this.classLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classLabel.Location = new System.Drawing.Point(26, 43);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(96, 23);
            this.classLabel.TabIndex = 89;
            this.classLabel.Text = "Class Measure";
            this.toolTip.SetToolTip(this.classLabel, "Target class should be a measure with values 0 (not in class) or 1 (in class)");
            // 
            // outputOnlyClass
            // 
            this.outputOnlyClass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.outputOnlyClass.Location = new System.Drawing.Point(546, 23);
            this.outputOnlyClass.Name = "outputOnlyClass";
            this.outputOnlyClass.Size = new System.Drawing.Size(232, 24);
            this.outputOnlyClass.TabIndex = 106;
            this.outputOnlyClass.Text = "Output Only Estimated Class Members";
            this.toolTip.SetToolTip(this.outputOnlyClass, "Save only individuals that are predicted to be in the class, otherwise save all i" +
                    "ndividuals");
            // 
            // estimateClass
            // 
            this.estimateClass.AccessibleDescription = "";
            this.estimateClass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.estimateClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estimateClass.Location = new System.Drawing.Point(10, 19);
            this.estimateClass.Name = "estimateClass";
            this.estimateClass.Size = new System.Drawing.Size(216, 21);
            this.estimateClass.TabIndex = 122;
            this.estimateClass.Text = "Estimate Class of Outcome";
            this.toolTip.SetToolTip(this.estimateClass, "Estimate whether each individual is a member of a target class or not. Target cla" +
                    "ss should be a measure with values 0 (not in class) or 1 (in class)");
            // 
            // biasUniform
            // 
            this.biasUniform.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.biasUniform.Location = new System.Drawing.Point(322, 47);
            this.biasUniform.Name = "biasUniform";
            this.biasUniform.Size = new System.Drawing.Size(216, 16);
            this.biasUniform.TabIndex = 120;
            this.biasUniform.Text = "Bias Sampling to Uniform Class";
            this.toolTip.SetToolTip(this.biasUniform, resources.GetString("biasUniform.ToolTip"));
            // 
            // outcomeGroupTable
            // 
            this.outcomeGroupTable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeGroupTable.Location = new System.Drawing.Point(529, 62);
            this.outcomeGroupTable.Name = "outcomeGroupTable";
            this.outcomeGroupTable.Size = new System.Drawing.Size(200, 20);
            this.outcomeGroupTable.TabIndex = 86;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(419, 62);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(88, 16);
            this.label36.TabIndex = 109;
            this.label36.Text = "Group Table";
            this.toolTip.SetToolTip(this.label36, "Outcome Group Table");
            // 
            // outcomeGroup
            // 
            this.outcomeGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeGroup.Location = new System.Drawing.Point(529, 39);
            this.outcomeGroup.Name = "outcomeGroup";
            this.outcomeGroup.Size = new System.Drawing.Size(200, 20);
            this.outcomeGroup.TabIndex = 85;
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(419, 39);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(88, 16);
            this.label35.TabIndex = 107;
            this.label35.Text = "Group Name";
            this.toolTip.SetToolTip(this.label35, "Outcome Group Name");
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(8, 39);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(128, 16);
            this.label32.TabIndex = 77;
            this.label32.Text = "Output Table Name";
            // 
            // outcomeModel
            // 
            this.outcomeModel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.outcomeModel.Location = new System.Drawing.Point(158, 13);
            this.outcomeModel.Name = "outcomeModel";
            this.outcomeModel.Size = new System.Drawing.Size(216, 21);
            this.outcomeModel.TabIndex = 3;
            this.toolTip.SetToolTip(this.outcomeModel, "Model Dataset to use to predict this outcome (the Model Dataset contains all inst" +
                    "ance data necessary to predict outcomes)");
            // 
            // outcomeTableName
            // 
            this.outcomeTableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeTableName.Location = new System.Drawing.Point(158, 39);
            this.outcomeTableName.Name = "outcomeTableName";
            this.outcomeTableName.Size = new System.Drawing.Size(140, 20);
            this.outcomeTableName.TabIndex = 5;
            // 
            // labelBaseModelDataset
            // 
            this.labelBaseModelDataset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseModelDataset.Location = new System.Drawing.Point(8, 16);
            this.labelBaseModelDataset.Name = "labelBaseModelDataset";
            this.labelBaseModelDataset.Size = new System.Drawing.Size(144, 23);
            this.labelBaseModelDataset.TabIndex = 80;
            this.labelBaseModelDataset.Text = "Base Model Dataset";
            // 
            // dropOutcomeTable
            // 
            this.dropOutcomeTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dropOutcomeTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropOutcomeTable.Location = new System.Drawing.Point(303, 38);
            this.dropOutcomeTable.Name = "dropOutcomeTable";
            this.dropOutcomeTable.Size = new System.Drawing.Size(119, 20);
            this.dropOutcomeTable.TabIndex = 6;
            this.dropOutcomeTable.Text = "Drop Table";
            // 
            // outcomeIterationValue
            // 
            this.outcomeIterationValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outcomeIterationValue.Location = new System.Drawing.Point(186, 112);
            this.outcomeIterationValue.Name = "outcomeIterationValue";
            this.outcomeIterationValue.Size = new System.Drawing.Size(200, 20);
            this.outcomeIterationValue.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 112);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(170, 16);
            this.label30.TabIndex = 83;
            this.label30.Text = "Iteration Value(s) to Score";
            this.toolTip.SetToolTip(this.label30, "Iteration Value(s) to Score.  Separate multiple values with commas.");
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            this.toolTip.ToolTipTitle = "Outcome Analysis";
            // 
            // outcomeMenuStrip
            // 
            this.outcomeMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addOutcomeToolStripMenuItem,
            this.removeOutcomeToolStripMenuItem});
            this.outcomeMenuStrip.Name = "outcomeMenuStrip";
            this.outcomeMenuStrip.ShowImageMargin = false;
            this.outcomeMenuStrip.Size = new System.Drawing.Size(146, 48);
            // 
            // addOutcomeToolStripMenuItem
            // 
            this.addOutcomeToolStripMenuItem.Name = "addOutcomeToolStripMenuItem";
            this.addOutcomeToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.addOutcomeToolStripMenuItem.Text = "Add Outcome";
            this.addOutcomeToolStripMenuItem.Click += new System.EventHandler(this.addOutcome_Click);
            // 
            // removeOutcomeToolStripMenuItem
            // 
            this.removeOutcomeToolStripMenuItem.Name = "removeOutcomeToolStripMenuItem";
            this.removeOutcomeToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.removeOutcomeToolStripMenuItem.Text = "Remove Outcome";
            this.removeOutcomeToolStripMenuItem.Click += new System.EventHandler(this.removeOutcome_Click);
            // 
            // Outcomes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 866);
            this.Controls.Add(this.tabControl);
            this.Name = "Outcomes";
            this.Text = "Outcomes";
            this.tabControl.ResumeLayout(false);
            this.outcomesTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exclusionRulesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ruleGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.factorDataView)).EndInit();
            this.outcomeFactorsMenu.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.outcomeMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage outcomesTabPage;
        private System.Windows.Forms.TextBox outcomeGroupTable;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox outcomeGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox estimateValue;
        private System.Windows.Forms.CheckBox estimateClass;
        private System.Windows.Forms.CheckBox includeAllNonzero;
        private System.Windows.Forms.CheckBox biasUniform;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton bucketOutcomePctDiff;
        private System.Windows.Forms.RadioButton bucketOutcomeDifference;
        private System.Windows.Forms.RadioButton bucketOutcomeValue;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button addOutcomeBucket;
        private System.Windows.Forms.TextBox outcomeBucketMin;
        private System.Windows.Forms.ListView outcomeBucketList;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button removeOutcomeBucket;
        private System.Windows.Forms.TextBox outcomeBucketMax;
        private System.Windows.Forms.TextBox outcomeBucketName;
        private System.Windows.Forms.CheckBox bucketOutcome;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton estimateDifferenceFromActual;
        private System.Windows.Forms.RadioButton estimateOutcome;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox outputOnlyClass;
        private System.Windows.Forms.ComboBox maxOutcomeMeasure;
        private System.Windows.Forms.ComboBox minOutcomeMeasure;
        private System.Windows.Forms.CheckBox maxOutcomeRelMeasure;
        private System.Windows.Forms.CheckBox minOutcomeRelMeasure;
        private System.Windows.Forms.CheckBox useMaxOutcomeVal;
        private System.Windows.Forms.CheckBox useMinOutcomeVal;
        private System.Windows.Forms.CheckBox maxOutcomeRelative;
        private System.Windows.Forms.CheckBox minOutcomeRelative;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox excludeAboveMax;
        private System.Windows.Forms.CheckBox excludeBelowMin;
        private System.Windows.Forms.TextBox maxOutcomeVal;
        private System.Windows.Forms.TextBox minOutcomeVal;
        private System.Windows.Forms.Label classLabel;
        private System.Windows.Forms.ComboBox classMeasureBox;
        private System.Windows.Forms.CheckBox estimateValueInClass;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox outcomeModel;
        private System.Windows.Forms.TextBox outcomeTableName;
        private System.Windows.Forms.Label labelBaseModelDataset;
        private System.Windows.Forms.CheckBox dropOutcomeTable;
        private System.Windows.Forms.TextBox outcomeIterationValue;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ContextMenuStrip outcomeMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addOutcomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeOutcomeToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox rankFactors;
        private System.Windows.Forms.DataGridView factorDataView;
        private System.Windows.Forms.DataGridViewTextBoxColumn FactorName;
        private System.Windows.Forms.DataGridViewComboBoxColumn BaseValue;
        private System.Windows.Forms.DataGridViewButtonColumn BaseValues;
        private System.Windows.Forms.DataGridViewComboBoxColumn TestValue;
        private System.Windows.Forms.DataGridViewButtonColumn TestValues;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox numToRank;
        private System.Windows.Forms.ContextMenuStrip outcomeFactorsMenu;
        private System.Windows.Forms.ToolStripMenuItem setBaseItems;
        private System.Windows.Forms.ToolStripMenuItem currentBase;
        private System.Windows.Forms.ToolStripMenuItem averageBase;
        private System.Windows.Forms.ToolStripMenuItem zeroBase;
        private System.Windows.Forms.ToolStripMenuItem setTestItems;
        private System.Windows.Forms.ToolStripMenuItem currentTest;
        private System.Windows.Forms.ToolStripMenuItem averageTest;
        private System.Windows.Forms.ToolStripMenuItem zeroTest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox targetOverrideBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView ruleGrid;
        private System.Windows.Forms.DataGridView exclusionRulesGrid;
        private System.Windows.Forms.GroupBox classificationRulesGroup;
        private System.Windows.Forms.GroupBox exclusionRuleGroup;
        private System.Windows.Forms.Button classifierSettings;
        private System.Windows.Forms.TextBox outcomeModelIterationValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label trainingFilterLabel;
        private System.Windows.Forms.ComboBox trainingFilterComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriorityExclusion;
        private System.Windows.Forms.DataGridViewComboBoxColumn FilterExclusion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExplanationExclusion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priority;
        private System.Windows.Forms.DataGridViewComboBoxColumn Filter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Explanation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox scoringFilterComboBox;
        private System.Windows.Forms.TextBox nameFilterBox;
        private System.Windows.Forms.Label label5;
    }
}