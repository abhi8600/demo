using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class GetStagingColumnFilter : Form
    {
        public List<StagingColumnFilterOverride> scFilters;
        MainAdminForm ma;


        public GetStagingColumnFilter(MainAdminForm ma, List<StagingColumnFilterOverride> scFilters)
        {
            this.ma = ma;
            this.scFilters = scFilters;
            InitializeComponent();
            List<StagingFilter> filters = ma.stagingFilterMod.getStagingFilters();
            filterBox.Items.Clear();
            if ((filters != null) && (filters.Count > 0))
            {
                foreach (StagingFilter sf in filters)
                {
                    filterBox.Items.Add(sf.Name);
                }
            }
            fillDimensionBox();
            fillFilterList();
        }

        private void dimensionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedDimension = (string)dimensionBox.SelectedItem;
            levelBox.Items.Clear();
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            List<string> llist = new List<string>();
            foreach (Hierarchy h in hlist)
            {
                if (h.DimensionName != selectedDimension)
                {
                    continue;
                }
                h.getChildLevelNames(llist);
                break;
            }
            if (llist.Count > 0)
            {
                foreach (string level in llist)
                {
                    levelBox.Items.Add(level);
                }
            }
            else
            {
                levelBox.Items.Add("<Implicit>");
            }
            levelBox.SelectedIndex = 0;
        }

        public void fillDimensionBox()
        {
            dimensionBox.Items.Clear();
            foreach (string dim in ma.getDimensionList())
            {
                dimensionBox.Items.Add(dim);
            }
        }

        public void fillFilterList()
        {
            filterList.Items.Clear();

            if ((scFilters != null) && (scFilters.Count > 0))
            {
                foreach (StagingColumnFilterOverride scf in scFilters)
                {
                    filterList.Items.Add(scf.getItem());
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string filter = (string)filterBox.SelectedItem;
            string dimension = (string)dimensionBox.SelectedItem;
            string level = (string)levelBox.SelectedItem;

            if (filter == null || filter == "" || dimension == null || dimension == "" || level == null || level == "")
            {
                MessageBox.Show("Filter, dimension and level must be selected to add a staging column filter.", "Error");
                return;
            }

            StagingColumnFilterOverride obj = new StagingColumnFilterOverride(filter, dimension, level);
            filterList.Items.Add(obj.getItem());
            if (scFilters == null)
            {
                scFilters = new List<StagingColumnFilterOverride>();
            }
            scFilters.Add(obj);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (filterList.SelectedItems.Count > 0)
            {
                foreach (int index in filterList.SelectedIndices)
                {
                    filterList.Items.RemoveAt(index);
                    scFilters.RemoveAt(index);
                }
            }
        }
    }
}