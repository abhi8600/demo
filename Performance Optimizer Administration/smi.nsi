; smi.nsi
;
;--------------------------------

!include "MUI.nsh"

; The name of the installer
Name "Success Metrics Administration Tool"

; The file to write
OutFile "SMI Administration Installer.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\Success Metrics\"

InstallDirRegKey HKCU "Software\Success Metrics" ""

;Vista redirects $SMPROGRAMS to all users without this
RequestExecutionLevel admin

;--------------------------------
;Variables

  Var MUI_TEMP
  Var STARTMENU_FOLDER

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "license.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Success Metrics" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER

  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------

; The stuff to install
Section "SMI Administration Tool" Tool

  SectionIn RO
  
Call IsDotNETInstalled
Pop $0
StrCmp $0 1 +2
MessageBox MB_OK|MB_ICONINFORMATION "The installer was unable to detect the Microsoft .NET Framework. $(^Name) requires the .NET Framework in order to run.$\n$\nPlease click OK to exit Setup."

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "bin\Release\SMI Administration.exe"
  
  ; Write the installation path into the registry
  WriteRegStr HKCU "SOFTWARE\Success Metrics" "" "$INSTDIR"
  WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\Success Metrics" "Success Metrics Administration Tool" "$INSTDIR\Uninstall.exe"

  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application

  CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Success Metrics Administration Tool.lnk" "$INSTDIR\SMI Administration.exe"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Desktop Shortcut" Desktop

  CreateShortCut "$DESKTOP\Success Metrics Admininstration Tool.lnk" "$INSTDIR\SMI Administration.exe"
  
SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_Tool ${LANG_ENGLISH} "Installs the SMI Administration Tool"
  LangString DESC_Desktop ${LANG_ENGLISH} "Creates a Desktop Shortcut"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${Tool} $(DESC_Tool)
    !insertmacro MUI_DESCRIPTION_TEXT ${Desktop} $(DESC_Desktop)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  Delete "$INSTDIR\SMI Administration.exe"
  Delete "$DESKTOP\Success Metrics Admininstration Tool.lnk"

  Delete $INSTDIR\Uninstall.exe
  RMDir "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP

  Delete "$SMPROGRAMS\$MUI_TEMP\Success Metrics Administration Tool.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"

  StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"

  startMenuDeleteLoop:
    ClearErrors
    RMDir $MUI_TEMP
    GetFullPathName $MUI_TEMP "$MUI_TEMP\.."
    
    IfErrors startMenuDeleteLoopDone
  
    StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
  startMenuDeleteLoopDone:

  DeleteRegKey /ifempty HKCU "Software\Success Metrics"

SectionEnd

Function IsDotNETInstalled
  Push $0
  Push $1
  Push $2
  Push $3
  Push $4
  ReadRegStr $4 HKEY_LOCAL_MACHINE "Software\Microsoft\.NETFramework" "InstallRoot"
  # remove trailing back slash
  Push $4
  Exch $EXEDIR
  Exch $EXEDIR
  Pop $4
  # if the root directory doesn't exist .NET is not installed
  IfFileExists $4 0 noDotNET
  StrCpy $0 0
  EnumStart:
  EnumRegKey $2 HKEY_LOCAL_MACHINE "Software\Microsoft\.NETFramework\Policy" $0
  IntOp $0 $0 + 1
  StrCmp $2 "" noDotNET
  StrCpy $1 0
  EnumPolicy:
  EnumRegValue $3 HKEY_LOCAL_MACHINE "Software\Microsoft\.NETFramework\Policy\$2" $1
  IntOp $1 $1 + 1
  StrCmp $3 "" EnumStart
  IfFileExists "$4\$2.$3" foundDotNET EnumPolicy
  noDotNET:
  StrCpy $0 0
  Goto done
  foundDotNET:
  StrCpy $0 1
  done:
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Exch $0
FunctionEnd

