namespace Performance_Optimizer_Administration
{
    partial class Operations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Operations));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.opMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addOpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeOpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addActionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addUpdateStatementMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invalidateCacheMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invalidateDashboardCache = new System.Windows.Forms.ToolStripMenuItem();
            this.emailMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeActionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upActionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downActionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opTabControl = new System.Windows.Forms.TabControl();
            this.opTabPage = new System.Windows.Forms.TabPage();
            this.emailActionPanel = new System.Windows.Forms.Panel();
            this.emailActionReportText = new System.Windows.Forms.TextBox();
            this.emailActionReportLabel = new System.Windows.Forms.Label();
            this.emailActionBodyText = new System.Windows.Forms.TextBox();
            this.emailActionBodyLabel = new System.Windows.Forms.Label();
            this.emailActionSubjectText = new System.Windows.Forms.TextBox();
            this.emailActionSubjectLabel = new System.Windows.Forms.Label();
            this.emailActionToText = new System.Windows.Forms.TextBox();
            this.emailActionToLabel = new System.Windows.Forms.Label();
            this.emailActionFromText = new System.Windows.Forms.TextBox();
            this.emailActionFromLabel = new System.Windows.Forms.Label();
            this.emailActionNameText = new System.Windows.Forms.TextBox();
            this.emailActionNameLabel = new System.Windows.Forms.Label();
            this.EmailActionLabel = new System.Windows.Forms.Label();
            this.updatePanel = new System.Windows.Forms.Panel();
            this.updateNameBox = new System.Windows.Forms.TextBox();
            this.anotherActionNameLabel = new System.Windows.Forms.Label();
            this.sqlStoredProcedure = new System.Windows.Forms.RadioButton();
            this.sqlUpdate = new System.Windows.Forms.RadioButton();
            this.sqlStatementLabel = new System.Windows.Forms.Label();
            this.updateStatement = new System.Windows.Forms.TextBox();
            this.updateTransactionLabel = new System.Windows.Forms.Label();
            this.operationPanel = new System.Windows.Forms.Panel();
            this.parametersLabel = new System.Windows.Forms.Label();
            this.parameterGrid = new System.Windows.Forms.DataGridView();
            this.ParameterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParameterType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Datatype = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.opNameBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.invalidateCachePanel = new System.Windows.Forms.Panel();
            this.invalidateNameBox = new System.Windows.Forms.TextBox();
            this.actionNameLabel = new System.Windows.Forms.Label();
            this.logicalQuery = new System.Windows.Forms.RadioButton();
            this.tableDefinition = new System.Windows.Forms.RadioButton();
            this.invalidateTable = new System.Windows.Forms.ComboBox();
            this.invalidateQuery = new System.Windows.Forms.TextBox();
            this.invalidateCacheLabel = new System.Windows.Forms.Label();
            this.opToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.opMenuStrip.SuspendLayout();
            this.opTabControl.SuspendLayout();
            this.opTabPage.SuspendLayout();
            this.emailActionPanel.SuspendLayout();
            this.updatePanel.SuspendLayout();
            this.operationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parameterGrid)).BeginInit();
            this.invalidateCachePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // opMenuStrip
            // 
            this.opMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addOpMenuItem,
            this.removeOpMenuItem,
            this.addActionMenuItem,
            this.removeActionMenuItem,
            this.upActionMenuItem,
            this.downActionMenuItem});
            this.opMenuStrip.Name = "opMenuStrip";
            this.opMenuStrip.Size = new System.Drawing.Size(176, 136);
            // 
            // addOpMenuItem
            // 
            this.addOpMenuItem.Name = "addOpMenuItem";
            this.addOpMenuItem.Size = new System.Drawing.Size(175, 22);
            this.addOpMenuItem.Text = "Add Operation";
            this.addOpMenuItem.Click += new System.EventHandler(this.addOpButton_Click);
            // 
            // removeOpMenuItem
            // 
            this.removeOpMenuItem.Name = "removeOpMenuItem";
            this.removeOpMenuItem.Size = new System.Drawing.Size(175, 22);
            this.removeOpMenuItem.Text = "Remove Operation";
            this.removeOpMenuItem.Click += new System.EventHandler(this.removeOpButton_Click);
            // 
            // addActionMenuItem
            // 
            this.addActionMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUpdateStatementMenuItem,
            this.invalidateCacheMenuItem,
            this.invalidateDashboardCache,
            this.emailMenuItem});
            this.addActionMenuItem.Name = "addActionMenuItem";
            this.addActionMenuItem.Size = new System.Drawing.Size(175, 22);
            this.addActionMenuItem.Text = "Add Action";
            // 
            // addUpdateStatementMenuItem
            // 
            this.addUpdateStatementMenuItem.Name = "addUpdateStatementMenuItem";
            this.addUpdateStatementMenuItem.Size = new System.Drawing.Size(221, 22);
            this.addUpdateStatementMenuItem.Text = "Update Statement";
            this.addUpdateStatementMenuItem.Click += new System.EventHandler(this.addUpdateStatementMenuItem_Click);
            // 
            // invalidateCacheMenuItem
            // 
            this.invalidateCacheMenuItem.Name = "invalidateCacheMenuItem";
            this.invalidateCacheMenuItem.Size = new System.Drawing.Size(221, 22);
            this.invalidateCacheMenuItem.Text = "Invalidate Query Cache";
            this.invalidateCacheMenuItem.Click += new System.EventHandler(this.invalidateCacheMenuItem_Click);
            // 
            // invalidateDashboardCache
            // 
            this.invalidateDashboardCache.Name = "invalidateDashboardCache";
            this.invalidateDashboardCache.Size = new System.Drawing.Size(221, 22);
            this.invalidateDashboardCache.Text = "Invalidate Dashboard Cache";
            this.invalidateDashboardCache.Click += new System.EventHandler(this.invalidateDashboardCache_Click);
            // 
            // emailMenuItem
            // 
            this.emailMenuItem.Name = "emailMenuItem";
            this.emailMenuItem.Size = new System.Drawing.Size(221, 22);
            this.emailMenuItem.Text = "Email";
            this.emailMenuItem.Click += new System.EventHandler(this.emailMenuItem_Click);
            // 
            // removeActionMenuItem
            // 
            this.removeActionMenuItem.Name = "removeActionMenuItem";
            this.removeActionMenuItem.Size = new System.Drawing.Size(175, 22);
            this.removeActionMenuItem.Text = "Remove Action";
            this.removeActionMenuItem.Click += new System.EventHandler(this.removeActionMenuItem_Click);
            // 
            // upActionMenuItem
            // 
            this.upActionMenuItem.Name = "upActionMenuItem";
            this.upActionMenuItem.Size = new System.Drawing.Size(175, 22);
            this.upActionMenuItem.Text = "Up";
            this.upActionMenuItem.Click += new System.EventHandler(this.upActionMenuItem_Click);
            // 
            // downActionMenuItem
            // 
            this.downActionMenuItem.Name = "downActionMenuItem";
            this.downActionMenuItem.Size = new System.Drawing.Size(175, 22);
            this.downActionMenuItem.Text = "Down";
            this.downActionMenuItem.Click += new System.EventHandler(this.downActionMenuItem_Click);
            // 
            // opTabControl
            // 
            this.opTabControl.Controls.Add(this.opTabPage);
            this.opTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opTabControl.Location = new System.Drawing.Point(0, 0);
            this.opTabControl.Name = "opTabControl";
            this.opTabControl.SelectedIndex = 0;
            this.opTabControl.Size = new System.Drawing.Size(852, 733);
            this.opTabControl.TabIndex = 1;
            // 
            // opTabPage
            // 
            this.opTabPage.AutoScroll = true;
            this.opTabPage.Controls.Add(this.emailActionPanel);
            this.opTabPage.Controls.Add(this.updatePanel);
            this.opTabPage.Controls.Add(this.operationPanel);
            this.opTabPage.Controls.Add(this.invalidateCachePanel);
            this.opTabPage.Location = new System.Drawing.Point(4, 22);
            this.opTabPage.Name = "opTabPage";
            this.opTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.opTabPage.Size = new System.Drawing.Size(844, 707);
            this.opTabPage.TabIndex = 0;
            this.opTabPage.Text = "Operations";
            this.opTabPage.UseVisualStyleBackColor = true;
            this.opTabPage.Leave += new System.EventHandler(this.opTabPage_Leave);
            // 
            // emailActionPanel
            // 
            this.emailActionPanel.Controls.Add(this.emailActionReportText);
            this.emailActionPanel.Controls.Add(this.emailActionReportLabel);
            this.emailActionPanel.Controls.Add(this.emailActionBodyText);
            this.emailActionPanel.Controls.Add(this.emailActionBodyLabel);
            this.emailActionPanel.Controls.Add(this.emailActionSubjectText);
            this.emailActionPanel.Controls.Add(this.emailActionSubjectLabel);
            this.emailActionPanel.Controls.Add(this.emailActionToText);
            this.emailActionPanel.Controls.Add(this.emailActionToLabel);
            this.emailActionPanel.Controls.Add(this.emailActionFromText);
            this.emailActionPanel.Controls.Add(this.emailActionFromLabel);
            this.emailActionPanel.Controls.Add(this.emailActionNameText);
            this.emailActionPanel.Controls.Add(this.emailActionNameLabel);
            this.emailActionPanel.Controls.Add(this.EmailActionLabel);
            this.emailActionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailActionPanel.Location = new System.Drawing.Point(3, 3);
            this.emailActionPanel.Name = "emailActionPanel";
            this.emailActionPanel.Size = new System.Drawing.Size(838, 701);
            this.emailActionPanel.TabIndex = 27;
            this.emailActionPanel.Visible = false;
            // 
            // emailActionReportText
            // 
            this.emailActionReportText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionReportText.Location = new System.Drawing.Point(149, 428);
            this.emailActionReportText.Name = "emailActionReportText";
            this.emailActionReportText.Size = new System.Drawing.Size(600, 20);
            this.emailActionReportText.TabIndex = 41;
            // 
            // emailActionReportLabel
            // 
            this.emailActionReportLabel.AutoSize = true;
            this.emailActionReportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionReportLabel.Location = new System.Drawing.Point(3, 429);
            this.emailActionReportLabel.Name = "emailActionReportLabel";
            this.emailActionReportLabel.Size = new System.Drawing.Size(135, 16);
            this.emailActionReportLabel.TabIndex = 40;
            this.emailActionReportLabel.Text = "Compiled Report File";
            // 
            // emailActionBodyText
            // 
            this.emailActionBodyText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionBodyText.Location = new System.Drawing.Point(150, 202);
            this.emailActionBodyText.Multiline = true;
            this.emailActionBodyText.Name = "emailActionBodyText";
            this.emailActionBodyText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.emailActionBodyText.Size = new System.Drawing.Size(600, 200);
            this.emailActionBodyText.TabIndex = 37;
            // 
            // emailActionBodyLabel
            // 
            this.emailActionBodyLabel.AutoSize = true;
            this.emailActionBodyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionBodyLabel.Location = new System.Drawing.Point(4, 203);
            this.emailActionBodyLabel.Name = "emailActionBodyLabel";
            this.emailActionBodyLabel.Size = new System.Drawing.Size(40, 16);
            this.emailActionBodyLabel.TabIndex = 36;
            this.emailActionBodyLabel.Text = "Body";
            // 
            // emailActionSubjectText
            // 
            this.emailActionSubjectText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionSubjectText.Location = new System.Drawing.Point(149, 160);
            this.emailActionSubjectText.Name = "emailActionSubjectText";
            this.emailActionSubjectText.Size = new System.Drawing.Size(500, 20);
            this.emailActionSubjectText.TabIndex = 35;
            // 
            // emailActionSubjectLabel
            // 
            this.emailActionSubjectLabel.AutoSize = true;
            this.emailActionSubjectLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionSubjectLabel.Location = new System.Drawing.Point(3, 161);
            this.emailActionSubjectLabel.Name = "emailActionSubjectLabel";
            this.emailActionSubjectLabel.Size = new System.Drawing.Size(53, 16);
            this.emailActionSubjectLabel.TabIndex = 34;
            this.emailActionSubjectLabel.Text = "Subject";
            // 
            // emailActionToText
            // 
            this.emailActionToText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionToText.Location = new System.Drawing.Point(150, 120);
            this.emailActionToText.Name = "emailActionToText";
            this.emailActionToText.Size = new System.Drawing.Size(500, 20);
            this.emailActionToText.TabIndex = 33;
            // 
            // emailActionToLabel
            // 
            this.emailActionToLabel.AutoSize = true;
            this.emailActionToLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionToLabel.Location = new System.Drawing.Point(4, 121);
            this.emailActionToLabel.Name = "emailActionToLabel";
            this.emailActionToLabel.Size = new System.Drawing.Size(25, 16);
            this.emailActionToLabel.TabIndex = 32;
            this.emailActionToLabel.Text = "To";
            // 
            // emailActionFromText
            // 
            this.emailActionFromText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionFromText.Location = new System.Drawing.Point(150, 85);
            this.emailActionFromText.Name = "emailActionFromText";
            this.emailActionFromText.Size = new System.Drawing.Size(500, 20);
            this.emailActionFromText.TabIndex = 31;
            // 
            // emailActionFromLabel
            // 
            this.emailActionFromLabel.AutoSize = true;
            this.emailActionFromLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionFromLabel.Location = new System.Drawing.Point(4, 86);
            this.emailActionFromLabel.Name = "emailActionFromLabel";
            this.emailActionFromLabel.Size = new System.Drawing.Size(39, 16);
            this.emailActionFromLabel.TabIndex = 30;
            this.emailActionFromLabel.Text = "From";
            // 
            // emailActionNameText
            // 
            this.emailActionNameText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emailActionNameText.Location = new System.Drawing.Point(151, 45);
            this.emailActionNameText.Name = "emailActionNameText";
            this.emailActionNameText.Size = new System.Drawing.Size(500, 20);
            this.emailActionNameText.TabIndex = 29;
            this.emailActionNameText.Visible = false;
            // 
            // emailActionNameLabel
            // 
            this.emailActionNameLabel.AutoSize = true;
            this.emailActionNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailActionNameLabel.Location = new System.Drawing.Point(5, 46);
            this.emailActionNameLabel.Name = "emailActionNameLabel";
            this.emailActionNameLabel.Size = new System.Drawing.Size(85, 16);
            this.emailActionNameLabel.TabIndex = 28;
            this.emailActionNameLabel.Text = "Action Name";
            this.emailActionNameLabel.Visible = false;
            // 
            // EmailActionLabel
            // 
            this.EmailActionLabel.AutoSize = true;
            this.EmailActionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailActionLabel.Location = new System.Drawing.Point(4, 16);
            this.EmailActionLabel.Name = "EmailActionLabel";
            this.EmailActionLabel.Size = new System.Drawing.Size(42, 16);
            this.EmailActionLabel.TabIndex = 0;
            this.EmailActionLabel.Text = "Email";
            // 
            // updatePanel
            // 
            this.updatePanel.AutoScroll = true;
            this.updatePanel.Controls.Add(this.updateNameBox);
            this.updatePanel.Controls.Add(this.anotherActionNameLabel);
            this.updatePanel.Controls.Add(this.sqlStoredProcedure);
            this.updatePanel.Controls.Add(this.sqlUpdate);
            this.updatePanel.Controls.Add(this.sqlStatementLabel);
            this.updatePanel.Controls.Add(this.updateStatement);
            this.updatePanel.Controls.Add(this.updateTransactionLabel);
            this.updatePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatePanel.Location = new System.Drawing.Point(3, 3);
            this.updatePanel.Name = "updatePanel";
            this.updatePanel.Size = new System.Drawing.Size(838, 701);
            this.updatePanel.TabIndex = 18;
            this.updatePanel.Visible = false;
            // 
            // updateNameBox
            // 
            this.updateNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.updateNameBox.Location = new System.Drawing.Point(134, 24);
            this.updateNameBox.Name = "updateNameBox";
            this.updateNameBox.Size = new System.Drawing.Size(227, 20);
            this.updateNameBox.TabIndex = 26;
            this.updateNameBox.Visible = false;
            // 
            // anotherActionNameLabel
            // 
            this.anotherActionNameLabel.AutoSize = true;
            this.anotherActionNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anotherActionNameLabel.Location = new System.Drawing.Point(5, 25);
            this.anotherActionNameLabel.Name = "anotherActionNameLabel";
            this.anotherActionNameLabel.Size = new System.Drawing.Size(85, 16);
            this.anotherActionNameLabel.TabIndex = 25;
            this.anotherActionNameLabel.Text = "Action Name";
            this.anotherActionNameLabel.Visible = false;
            // 
            // sqlStoredProcedure
            // 
            this.sqlStoredProcedure.AutoSize = true;
            this.sqlStoredProcedure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sqlStoredProcedure.Location = new System.Drawing.Point(147, 69);
            this.sqlStoredProcedure.Name = "sqlStoredProcedure";
            this.sqlStoredProcedure.Size = new System.Drawing.Size(158, 20);
            this.sqlStoredProcedure.TabIndex = 20;
            this.sqlStoredProcedure.Text = "Stored Procedure Call";
            this.opToolTip.SetToolTip(this.sqlStoredProcedure, resources.GetString("sqlStoredProcedure.ToolTip"));
            this.sqlStoredProcedure.UseVisualStyleBackColor = true;
            // 
            // sqlUpdate
            // 
            this.sqlUpdate.AutoSize = true;
            this.sqlUpdate.Checked = true;
            this.sqlUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sqlUpdate.Location = new System.Drawing.Point(8, 69);
            this.sqlUpdate.Name = "sqlUpdate";
            this.sqlUpdate.Size = new System.Drawing.Size(134, 20);
            this.sqlUpdate.TabIndex = 19;
            this.sqlUpdate.TabStop = true;
            this.sqlUpdate.Text = "Update Statement";
            this.opToolTip.SetToolTip(this.sqlUpdate, "Executes a prepared statement with parameters. E.g. UPDATE TAB1 SET NAME=$P{NAME}" +
                    " WHERE ID=$P{ID}");
            this.sqlUpdate.UseVisualStyleBackColor = true;
            // 
            // sqlStatementLabel
            // 
            this.sqlStatementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sqlStatementLabel.Location = new System.Drawing.Point(4, 94);
            this.sqlStatementLabel.Name = "sqlStatementLabel";
            this.sqlStatementLabel.Size = new System.Drawing.Size(124, 18);
            this.sqlStatementLabel.TabIndex = 17;
            this.sqlStatementLabel.Text = "SQL Statement";
            // 
            // updateStatement
            // 
            this.updateStatement.Location = new System.Drawing.Point(8, 115);
            this.updateStatement.Multiline = true;
            this.updateStatement.Name = "updateStatement";
            this.updateStatement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.updateStatement.Size = new System.Drawing.Size(788, 142);
            this.updateStatement.TabIndex = 16;
            // 
            // updateTransactionLabel
            // 
            this.updateTransactionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateTransactionLabel.Location = new System.Drawing.Point(4, 3);
            this.updateTransactionLabel.Name = "updateTransactionLabel";
            this.updateTransactionLabel.Size = new System.Drawing.Size(143, 23);
            this.updateTransactionLabel.TabIndex = 15;
            this.updateTransactionLabel.Text = "Update Transaction";
            // 
            // operationPanel
            // 
            this.operationPanel.AutoScroll = true;
            this.operationPanel.Controls.Add(this.parametersLabel);
            this.operationPanel.Controls.Add(this.parameterGrid);
            this.operationPanel.Controls.Add(this.opNameBox);
            this.operationPanel.Controls.Add(this.nameLabel);
            this.operationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.operationPanel.Location = new System.Drawing.Point(3, 3);
            this.operationPanel.Name = "operationPanel";
            this.operationPanel.Size = new System.Drawing.Size(838, 701);
            this.operationPanel.TabIndex = 17;
            // 
            // parametersLabel
            // 
            this.parametersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.parametersLabel.Location = new System.Drawing.Point(4, 31);
            this.parametersLabel.Name = "parametersLabel";
            this.parametersLabel.Size = new System.Drawing.Size(78, 23);
            this.parametersLabel.TabIndex = 19;
            this.parametersLabel.Text = "Parameters";
            // 
            // parameterGrid
            // 
            this.parameterGrid.AllowUserToResizeColumns = false;
            this.parameterGrid.AllowUserToResizeRows = false;
            this.parameterGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.parameterGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.parameterGrid.CausesValidation = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.parameterGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.parameterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.parameterGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ParameterName,
            this.ParameterType,
            this.Datatype});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.parameterGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.parameterGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.parameterGrid.Location = new System.Drawing.Point(88, 31);
            this.parameterGrid.Name = "parameterGrid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.parameterGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.parameterGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.parameterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.parameterGrid.ShowCellErrors = false;
            this.parameterGrid.ShowEditingIcon = false;
            this.parameterGrid.ShowRowErrors = false;
            this.parameterGrid.Size = new System.Drawing.Size(511, 226);
            this.parameterGrid.TabIndex = 18;
            this.parameterGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.parameterGrid_CellBeginEdit);
            // 
            // ParameterName
            // 
            this.ParameterName.HeaderText = "Parameter Name";
            this.ParameterName.Name = "ParameterName";
            // 
            // ParameterType
            // 
            this.ParameterType.FillWeight = 40F;
            this.ParameterType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParameterType.HeaderText = "ParameterType";
            this.ParameterType.Items.AddRange(new object[] {
            "Single-value",
            "Multi-value"});
            this.ParameterType.Name = "ParameterType";
            this.ParameterType.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Datatype
            // 
            this.Datatype.FillWeight = 40F;
            this.Datatype.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Datatype.HeaderText = "Data Type";
            this.Datatype.Items.AddRange(new object[] {
            "Varchar",
            "Number",
            "Integer",
            "Date",
            "DateTime",
            "Float"});
            this.Datatype.Name = "Datatype";
            // 
            // opNameBox
            // 
            this.opNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.opNameBox.Location = new System.Drawing.Point(88, 3);
            this.opNameBox.Name = "opNameBox";
            this.opNameBox.Size = new System.Drawing.Size(204, 20);
            this.opNameBox.TabIndex = 16;
            this.opNameBox.Visible = false;
            // 
            // nameLabel
            // 
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(4, 3);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(55, 23);
            this.nameLabel.TabIndex = 15;
            this.nameLabel.Text = "Name";
            this.nameLabel.Visible = false;
            // 
            // invalidateCachePanel
            // 
            this.invalidateCachePanel.Controls.Add(this.invalidateNameBox);
            this.invalidateCachePanel.Controls.Add(this.actionNameLabel);
            this.invalidateCachePanel.Controls.Add(this.logicalQuery);
            this.invalidateCachePanel.Controls.Add(this.tableDefinition);
            this.invalidateCachePanel.Controls.Add(this.invalidateTable);
            this.invalidateCachePanel.Controls.Add(this.invalidateQuery);
            this.invalidateCachePanel.Controls.Add(this.invalidateCacheLabel);
            this.invalidateCachePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.invalidateCachePanel.Location = new System.Drawing.Point(3, 3);
            this.invalidateCachePanel.Name = "invalidateCachePanel";
            this.invalidateCachePanel.Size = new System.Drawing.Size(838, 701);
            this.invalidateCachePanel.TabIndex = 19;
            this.invalidateCachePanel.Visible = false;
            // 
            // invalidateNameBox
            // 
            this.invalidateNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.invalidateNameBox.Location = new System.Drawing.Point(134, 24);
            this.invalidateNameBox.Name = "invalidateNameBox";
            this.invalidateNameBox.Size = new System.Drawing.Size(227, 20);
            this.invalidateNameBox.TabIndex = 24;
            this.invalidateNameBox.Visible = false;
            // 
            // actionNameLabel
            // 
            this.actionNameLabel.AutoSize = true;
            this.actionNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actionNameLabel.Location = new System.Drawing.Point(5, 25);
            this.actionNameLabel.Name = "actionNameLabel";
            this.actionNameLabel.Size = new System.Drawing.Size(85, 16);
            this.actionNameLabel.TabIndex = 23;
            this.actionNameLabel.Text = "Action Name";
            this.actionNameLabel.Visible = false;
            // 
            // logicalQuery
            // 
            this.logicalQuery.AutoSize = true;
            this.logicalQuery.Checked = true;
            this.logicalQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logicalQuery.Location = new System.Drawing.Point(8, 100);
            this.logicalQuery.Name = "logicalQuery";
            this.logicalQuery.Size = new System.Drawing.Size(109, 20);
            this.logicalQuery.TabIndex = 21;
            this.logicalQuery.TabStop = true;
            this.logicalQuery.Text = "Logical Query";
            this.logicalQuery.UseVisualStyleBackColor = true;
            // 
            // tableDefinition
            // 
            this.tableDefinition.AutoSize = true;
            this.tableDefinition.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableDefinition.Location = new System.Drawing.Point(8, 72);
            this.tableDefinition.Name = "tableDefinition";
            this.tableDefinition.Size = new System.Drawing.Size(120, 20);
            this.tableDefinition.TabIndex = 20;
            this.tableDefinition.Text = "Table Definition";
            this.tableDefinition.UseVisualStyleBackColor = true;
            this.tableDefinition.CheckedChanged += new System.EventHandler(this.invalidateCache_RadioButtonsCheckedChanged);
            // 
            // invalidateTable
            // 
            this.invalidateTable.Enabled = false;
            this.invalidateTable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.invalidateTable.FormattingEnabled = true;
            this.invalidateTable.Location = new System.Drawing.Point(134, 71);
            this.invalidateTable.Name = "invalidateTable";
            this.invalidateTable.Size = new System.Drawing.Size(227, 21);
            this.invalidateTable.TabIndex = 18;
            // 
            // invalidateQuery
            // 
            this.invalidateQuery.Location = new System.Drawing.Point(134, 104);
            this.invalidateQuery.Multiline = true;
            this.invalidateQuery.Name = "invalidateQuery";
            this.invalidateQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.invalidateQuery.Size = new System.Drawing.Size(702, 115);
            this.invalidateQuery.TabIndex = 17;
            // 
            // invalidateCacheLabel
            // 
            this.invalidateCacheLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invalidateCacheLabel.Location = new System.Drawing.Point(4, 0);
            this.invalidateCacheLabel.Name = "invalidateCacheLabel";
            this.invalidateCacheLabel.Size = new System.Drawing.Size(170, 25);
            this.invalidateCacheLabel.TabIndex = 15;
            this.invalidateCacheLabel.Text = "Invalidate Cache";
            // 
            // opToolTip
            // 
            this.opToolTip.AutoPopDelay = 30000;
            this.opToolTip.InitialDelay = 500;
            this.opToolTip.IsBalloon = true;
            this.opToolTip.ReshowDelay = 100;
            // 
            // Operations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.opTabControl);
            this.Name = "Operations";
            this.Text = "Operation";
            this.opMenuStrip.ResumeLayout(false);
            this.opTabControl.ResumeLayout(false);
            this.opTabPage.ResumeLayout(false);
            this.emailActionPanel.ResumeLayout(false);
            this.emailActionPanel.PerformLayout();
            this.updatePanel.ResumeLayout(false);
            this.updatePanel.PerformLayout();
            this.operationPanel.ResumeLayout(false);
            this.operationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parameterGrid)).EndInit();
            this.invalidateCachePanel.ResumeLayout(false);
            this.invalidateCachePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip opMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addOpMenuItem;
        private System.Windows.Forms.TabControl opTabControl;
        private System.Windows.Forms.TabPage opTabPage;
        private System.Windows.Forms.TextBox opNameBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.ToolStripMenuItem removeOpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addActionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upActionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downActionMenuItem;
        private System.Windows.Forms.Panel updatePanel;
        private System.Windows.Forms.Label updateTransactionLabel;
        private System.Windows.Forms.Panel operationPanel;
        private System.Windows.Forms.ToolStripMenuItem addUpdateStatementMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invalidateCacheMenuItem;
        private System.Windows.Forms.Panel invalidateCachePanel;
        private System.Windows.Forms.Label invalidateCacheLabel;
        private System.Windows.Forms.RadioButton tableDefinition;
        private System.Windows.Forms.ComboBox invalidateTable;
        private System.Windows.Forms.TextBox invalidateQuery;
        private System.Windows.Forms.Label sqlStatementLabel;
        private System.Windows.Forms.TextBox updateStatement;
        private System.Windows.Forms.RadioButton logicalQuery;
        private System.Windows.Forms.Label parametersLabel;
        private System.Windows.Forms.DataGridView parameterGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParameterName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ParameterType;
        private System.Windows.Forms.DataGridViewComboBoxColumn Datatype;
        private System.Windows.Forms.RadioButton sqlStoredProcedure;
        private System.Windows.Forms.RadioButton sqlUpdate;
        private System.Windows.Forms.ToolTip opToolTip;
        private System.Windows.Forms.TextBox updateNameBox;
        private System.Windows.Forms.Label anotherActionNameLabel;
        private System.Windows.Forms.TextBox invalidateNameBox;
        private System.Windows.Forms.Label actionNameLabel;
        private System.Windows.Forms.ToolStripMenuItem invalidateDashboardCache;
        private System.Windows.Forms.ToolStripMenuItem removeActionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailMenuItem;
        private System.Windows.Forms.Panel emailActionPanel;
        private System.Windows.Forms.TextBox emailActionNameText;
        private System.Windows.Forms.Label emailActionNameLabel;
        private System.Windows.Forms.Label EmailActionLabel;
        private System.Windows.Forms.TextBox emailActionToText;
        private System.Windows.Forms.Label emailActionToLabel;
        private System.Windows.Forms.TextBox emailActionFromText;
        private System.Windows.Forms.Label emailActionFromLabel;
        private System.Windows.Forms.TextBox emailActionBodyText;
        private System.Windows.Forms.Label emailActionBodyLabel;
        private System.Windows.Forms.TextBox emailActionSubjectText;
        private System.Windows.Forms.Label emailActionSubjectLabel;
        private System.Windows.Forms.TextBox emailActionReportText;
        private System.Windows.Forms.Label emailActionReportLabel;
    }
}