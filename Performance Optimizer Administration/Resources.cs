using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Resources : Form, RepositoryModule
    {
        string moduleName = "Resources";
        string categoryName = "System";
        private Resource[] defaults = {
            new Resource("Performance ID Key","Schema","Performance","Name of target id column", "ID"),
            new Resource("Performance Iteration Key","Schema","Performance","Name of iteration id column", "ITERATION"),
            new Resource("Performance Flag Column","Schema","Performance","Name of measure flag column", "FLAG"),
            new Resource("Performance Measure Name Column","Schema","Performance","Name of measure name column", "NAME"),
            new Resource("Performance Category Name Column","Schema","Performance","Name of category name column", "CAT"),
            new Resource("Performance Measure Value Column","Schema","Performance","Name of measure value column (M)(S)(O)", "VAL"),
            new Resource("Performance Peer Value Column","Schema","Performance","Name of peer measure value colum(M), optimized target value(O)", "PEERVAL"),
            new Resource("Performance Percentile Column","Schema","Performance","Name of measure peer percentile column(M), weighted average percentile(C), overall average percentile(S)", "PTILE"),
            new Resource("Performance Measure Impact Column","Schema","Performance","Name of measure incremental impact column(M), weighted average impact of measures in category(C)", "IMPACT"),
            new Resource("Performance Peer Difference Impact Column","Schema","Performance","Name of column measuring impact of difference from peers(M), unexplained variance by model(S)", "PEER_DIFF_IMPACT"),
            new Resource("Performance Opportunity Column","Schema","Performance","Name of column with opportunity text associated with measure(O)", "OPPORTUNITY"),

            new Resource("Pattern Dimension","Schema","Patterns","Dimension name for mapped pattern table", "Pattern"),
            new Resource("Pattern Column Name","Schema","Patterns","Logical column name of pattern column", "Pattern"),
            new Resource("Iteration Column Name","Schema","Patterns","Logical column name of iteration column", "Iteration"),
            new Resource("Measure Used Column Name","Schema","Patterns","Logical column name of measure used column","Measure Used"),
            new Resource("Measure Rank Column Name","Schema","Patterns","Logical column name of measure rank column", "Measure Rank"),
            new Resource("Category Filter Column Name","Schema","Patterns","Logical column name of category filter column", "Category Filter"),
            new Resource("Pattern Score Column Name","Schema","Patterns","Logical column name of pattern score column", "Pattern Score"),
            
            new Resource("Pattern Target ID","Schema","Patterns","Column name for target key", "ID"),
            new Resource("Pattern Iteration ID","Schema","Patterns","Column name for iteration key", "IT"),
            new Resource("Pattern Segment","Schema","Patterns","Column name for iteration key", "SEGMENT"),
            new Resource("Pattern Measure Name","Schema","Patterns","Column name for measure name column", "MEASURE"),
            new Resource("Pattern Measure Used","Schema","Patterns","Column name for measure used column", "MEASURE_USED"),
            new Resource("Pattern Segment Measure","Schema","Patterns","Column name for segment measure column", "SEGMENT_MEASURE"),
            new Resource("Pattern First Dimension","Schema","Patterns","Column name for column with name of first dimension of breakouts", "DIMENSION1"),
            new Resource("Pattern First Attribute","Schema","Patterns","Column name for column with first attribute used in breakouts", "ATTRIBUTE1"),
            new Resource("Pattern Second Dimension","Schema","Patterns","Column name for column with name of second dimension of breakouts", "DIMENSION2"),
            new Resource("Pattern Second Attribute","Schema","Patterns","Column name for column with second attribute used in breakouts", "ATTRIBUTE2"),
            new Resource("Pattern Performance Category","Schema","Patterns","Column name for performance category column", "PERF_CATEGORY"),
            new Resource("Pattern Format","Schema","Patterns","Column name for column with format of measures in breaktous", "FORMAT"),
            new Resource("Pattern Rank","Schema","Patterns","Column name for rank of pattern for target id", "RANK"),
            new Resource("Pattern Measure Rank","Schema","Patterns","Column name for rank of pattern for measure", "MEASURE_RANK"),
            new Resource("Pattern Score","Schema","Patterns","Column name for score of pattern", "SCORE"),
            new Resource("Pattern Pattern","Schema","Patterns","Column name for pattern", "PATTERN"),
            new Resource("Pattern Reference Value","Schema","Patterns","Column name for pattern flag", "REFERENCE"),
            new Resource("Pattern First Category","Schema","Patterns","Column name for first measure category", "CATEGORY1"),
            new Resource("Pattern Second Category","Schema","Patterns","Column name for second measure category", "CATEGORY2"),
            new Resource("Pattern Value","Schema","Patterns","Column name value of breakout item", "VAL"),
            new Resource("Pattern Reference Value","Schema","Patterns","Column name for pattern flag", "REFERENCE"),
            new Resource("Pattern Category Filter","Schema","Patterns","Column name for filter to select for individual category segment", "CATFILTER"),

            new Resource("Outcome Name","Schema","Outcomes","Column name for name of outcome", "OUTCOME"),
            new Resource("Outcome Target ID","Schema","Outcomes","Column name for id of target", "ID"),
            new Resource("Outcome Iteration ID","Schema","Outcomes","Column name for id of iteration", "ITERATION"),
            new Resource("Outcome Actual Value","Schema","Outcomes","Column name for actual value of outcome", "VAL"),
            new Resource("Outcome Class Probability","Schema","Outcomes","Column name for predicted class probability", "CLASSPROB"),
            new Resource("Outcome Predicted Value","Schema","Outcomes","Column name for predicated value of outcome", "PREDVAL"),
            new Resource("Outcome Predicted Bucket","Schema","Outcomes","Column name for predicted bucket of outcome", "BUCKET"),
            new Resource("Outcome Predicted Class","Schema","Outcomes","Column name for predicted class", "CLASS"),
            new Resource("Outcome Factor","Schema","Outcomes","Column name for the name of a given outcome factor", "FACTOR"),
            new Resource("Outcome Factor Value","Schema","Outcomes","Column name for the current value of an outcome factor", "FACTORVAL"),
            new Resource("Outcome Factor Base Value","Schema","Outcomes","Column name for base value of an outcome factor", "FACTORBASE"),
            new Resource("Outcome Factor Test Value","Schema","Outcomes","Column name for test value of an outcome factor", "FACTORTEST"),
            new Resource("Outcome Factor Impact Value","Schema","Outcomes","Column name for the impact of an outcome factor test", "FACTORIMP"),

            new Resource("Target Key ID","Schema","Target Segments","Column name of success model target key name column", "KEYID"),
            new Resource("Target ID","Schema","Target Segments","Column name of success model id key name column", "ID"),
            new Resource("Target Iteration","Schema","Target Segments","Column name of success model iteration key name column", "ITERATION"),
            new Resource("Target Pattern","Schema","Target Segments","Column name of pattern name column", "PATTERN"),
            new Resource("Target Message","Schema","Target Segments","Column name of message column", "MESSAGE"),
            new Resource("Target Score","Schema","Target Segments","Column name of target score column", "SCORE"),
            new Resource("Target Filter","Filter","Target Segments","Name of filter applied to Target segments", "Target Filter"),
            new Resource("Green Ball","Image","Conditional Format","","20x20:green.gif"),
            new Resource("Yellow Ball","Image","Conditional Format","","20x20:yellow.gif"),
            new Resource("Red Ball","Image","Conditional Format","","20x20:red.gif"),
            new Resource("Blue Ball","Image","Conditional Format","","20x20:blue.gif"),
            new Resource("Positive","Image","Conditional Format","","19x19:pos_unk.jpg"),
            new Resource("Negative","Image","Conditional Format","","19x19:neg_unk.jpg"),
            new Resource("Moderate","Image","Conditional Format","","19x19:mod_unk.jpg")
        };

        public Resources()
        {
            InitializeComponent();
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            ArrayList list = new ArrayList();
            foreach (DataRow dr in StringsTable.Rows)
            {
                Resource re = new Resource();
                re.Name = (string) dr["Name"];
                re.Type = (string) dr["Type"];
                re.Area = (string) dr["Area"];
                re.Value = (string) dr["Value"];
                re.Description = (string)dr["Description"];
                list.Add(re);
            }
            r.Resources = (Resource[]) list.ToArray(typeof(Resource));
        }

        public void updateFromRepository(Repository r)
        {
            StringsTable.Rows.Clear();
            foreach (Resource re in defaults)
            {
                DataRow dr = StringsTable.NewRow();
                dr["Name"] = re.Name;
                dr["Type"] = re.Type;
                dr["Area"] = re.Area;
                dr["Value"] = re.Value;
                dr["Description"] = re.Description;
                StringsTable.Rows.Add(dr);
            }
            if (r != null && r.Resources != null)
                foreach (Resource re in r.Resources)
                {
                    DataRow[] rows = StringsTable.Select("Name='" + re.Name + "'");
                    if (rows.Length == 0)
                    {
                        DataRow dr = StringsTable.NewRow();
                        dr["Name"] = re.Name;
                        dr["Type"] = re.Type;
                        dr["Area"] = re.Area;
                        dr["Value"] = re.Value;
                        dr["Description"] = re.Description;
                        StringsTable.Rows.Add(dr);
                    }
                    else
                    {
                        rows[0]["Name"] = re.Name;
                        rows[0]["Type"] = re.Type;
                        rows[0]["Area"] = re.Area;
                        rows[0]["Value"] = re.Value;
                        rows[0]["Description"] = re.Description;
                    }
                }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
            return;
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return resourceTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        private void recourceDataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (resourceDataSet.Tables["StringsTable"] != null)
            {
                DataColumn dc = resourceDataSet.Tables["StringsTable"].Columns[e.ColumnIndex];
                if (resourceDataGrid[e.ColumnIndex, e.RowIndex].Value == System.DBNull.Value && !dc.AllowDBNull && dc.DefaultValue != null)
                {
                    resourceDataGrid[e.ColumnIndex, e.RowIndex].Value = dc.DefaultValue;
                }
            }
        }
    }
}