namespace Performance_Optimizer_Administration
{
    partial class DefineSubstitutions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefineSubstitutions));
            this.tableList = new System.Windows.Forms.ListView();
            this.inheritedName = new System.Windows.Forms.ColumnHeader();
            this.newName = new System.Windows.Forms.ColumnHeader();
            this.addTable = new System.Windows.Forms.Button();
            this.removeTable = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.originalNameBox = new System.Windows.Forms.TextBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.newNameBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableList
            // 
            this.tableList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.inheritedName,
            this.newName});
            this.tableList.FullRowSelect = true;
            this.tableList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tableList.Location = new System.Drawing.Point(6, 12);
            this.tableList.MultiSelect = false;
            this.tableList.Name = "tableList";
            this.tableList.Size = new System.Drawing.Size(529, 97);
            this.tableList.TabIndex = 0;
            this.tableList.UseCompatibleStateImageBehavior = false;
            this.tableList.View = System.Windows.Forms.View.Details;
            this.tableList.SelectedIndexChanged += new System.EventHandler(this.tableList_SelectedIndexChanged);
            // 
            // inheritedName
            // 
            this.inheritedName.Text = "Inherited Formula";
            this.inheritedName.Width = 250;
            // 
            // newName
            // 
            this.newName.Text = "New Formula";
            this.newName.Width = 250;
            // 
            // addTable
            // 
            this.addTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addTable.Location = new System.Drawing.Point(561, 12);
            this.addTable.Name = "addTable";
            this.addTable.Size = new System.Drawing.Size(119, 23);
            this.addTable.TabIndex = 1;
            this.addTable.Text = "Add Substitution";
            this.addTable.UseVisualStyleBackColor = true;
            this.addTable.Click += new System.EventHandler(this.addTable_Click);
            // 
            // removeTable
            // 
            this.removeTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeTable.Location = new System.Drawing.Point(561, 41);
            this.removeTable.Name = "removeTable";
            this.removeTable.Size = new System.Drawing.Size(119, 23);
            this.removeTable.TabIndex = 2;
            this.removeTable.Text = "Remove";
            this.removeTable.UseVisualStyleBackColor = true;
            this.removeTable.Click += new System.EventHandler(this.removeTable_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Inherited Formula";
            // 
            // originalNameBox
            // 
            this.originalNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.originalNameBox.Location = new System.Drawing.Point(6, 34);
            this.originalNameBox.Name = "originalNameBox";
            this.originalNameBox.Size = new System.Drawing.Size(168, 20);
            this.originalNameBox.TabIndex = 4;
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKButton.Location = new System.Drawing.Point(524, 197);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 10;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(605, 197);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 11;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.newNameBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.originalNameBox);
            this.groupBox1.Location = new System.Drawing.Point(6, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(674, 76);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Substitution";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(261, 52);
            this.label3.TabIndex = 7;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "New Formula";
            // 
            // newNameBox
            // 
            this.newNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.newNameBox.Location = new System.Drawing.Point(204, 34);
            this.newNameBox.Name = "newNameBox";
            this.newNameBox.Size = new System.Drawing.Size(168, 20);
            this.newNameBox.TabIndex = 6;
            // 
            // DefineSubstitutions
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(686, 226);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.removeTable);
            this.Controls.Add(this.addTable);
            this.Controls.Add(this.tableList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DefineSubstitutions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define Column Substitutions";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefineSubstitutions_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView tableList;
        private System.Windows.Forms.ColumnHeader inheritedName;
        private System.Windows.Forms.ColumnHeader newName;
        private System.Windows.Forms.Button addTable;
        private System.Windows.Forms.Button removeTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox newNameBox;
        private System.Windows.Forms.TextBox originalNameBox;
        private System.Windows.Forms.Label label3;
    }
}