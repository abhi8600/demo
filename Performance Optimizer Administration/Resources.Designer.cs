namespace Performance_Optimizer_Administration
{
    partial class Resources
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resourceDataGrid = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resourceDataSet = new System.Data.DataSet();
            this.StringsTable = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.resourceTabPage = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.resourceDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourceDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StringsTable)).BeginInit();
            this.tabControl.SuspendLayout();
            this.resourceTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // resourceDataGrid
            // 
            this.resourceDataGrid.AutoGenerateColumns = false;
            this.resourceDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resourceDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.areaDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn,
            this.Description});
            this.resourceDataGrid.DataMember = "StringsTable";
            this.resourceDataGrid.DataSource = this.resourceDataSet;
            this.resourceDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resourceDataGrid.Location = new System.Drawing.Point(3, 3);
            this.resourceDataGrid.Name = "resourceDataGrid";
            this.resourceDataGrid.RowTemplate.Height = 18;
            this.resourceDataGrid.Size = new System.Drawing.Size(931, 455);
            this.resourceDataGrid.TabIndex = 0;
            this.resourceDataGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.recourceDataGrid_CellValueChanged);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 200;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.Width = 150;
            // 
            // areaDataGridViewTextBoxColumn
            // 
            this.areaDataGridViewTextBoxColumn.DataPropertyName = "Area";
            this.areaDataGridViewTextBoxColumn.HeaderText = "Area";
            this.areaDataGridViewTextBoxColumn.Name = "areaDataGridViewTextBoxColumn";
            this.areaDataGridViewTextBoxColumn.Width = 150;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.Width = 300;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.Width = 500;
            // 
            // resourceDataSet
            // 
            this.resourceDataSet.DataSetName = "NewDataSet";
            this.resourceDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.StringsTable});
            // 
            // StringsTable
            // 
            this.StringsTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5});
            this.StringsTable.TableName = "StringsTable";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Name";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Type";
            this.dataColumn2.DefaultValue = "Schema";
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "Area";
            this.dataColumn3.DefaultValue = "";
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "Value";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Description";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.resourceTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(945, 487);
            this.tabControl.TabIndex = 1;
            // 
            // resourceTabPage
            // 
            this.resourceTabPage.Controls.Add(this.resourceDataGrid);
            this.resourceTabPage.Location = new System.Drawing.Point(4, 22);
            this.resourceTabPage.Name = "resourceTabPage";
            this.resourceTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.resourceTabPage.Size = new System.Drawing.Size(937, 461);
            this.resourceTabPage.TabIndex = 0;
            this.resourceTabPage.Text = "resourceTabPage";
            this.resourceTabPage.UseVisualStyleBackColor = true;
            // 
            // Resources
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 487);
            this.Controls.Add(this.tabControl);
            this.Name = "Resources";
            this.Text = "Resources";
            ((System.ComponentModel.ISupportInitialize)(this.resourceDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourceDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StringsTable)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.resourceTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView resourceDataGrid;
        private System.Data.DataSet resourceDataSet;
        private System.Data.DataTable StringsTable;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage resourceTabPage;
        private System.Data.DataColumn dataColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn areaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
    }
}