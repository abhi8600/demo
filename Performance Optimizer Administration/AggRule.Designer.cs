namespace Performance_Optimizer_Administration
{
    partial class AggRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AggRule));
            this.label1 = new System.Windows.Forms.Label();
            this.baseAggRuleBox = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.dimViewColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ruleViewColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aggRuleDataset = new System.Data.DataSet();
            this.aggRuleTable = new System.Data.DataTable();
            this.dimAggCol = new System.Data.DataColumn();
            this.dimAggRule = new System.Data.DataColumn();
            this.okButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggRuleDataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggRuleTable)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Base Rule";
            // 
            // baseAggRuleBox
            // 
            this.baseAggRuleBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.baseAggRuleBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.baseAggRuleBox.FormattingEnabled = true;
            this.baseAggRuleBox.Items.AddRange(new object[] {
            "SUM",
            "AVG",
            "COUNT",
            "COUNT DISTINCT",
            "MIN",
            "MAX",
            "STDEV"});
            this.baseAggRuleBox.Location = new System.Drawing.Point(74, 9);
            this.baseAggRuleBox.Name = "baseAggRuleBox";
            this.baseAggRuleBox.Size = new System.Drawing.Size(177, 21);
            this.baseAggRuleBox.TabIndex = 1;
            // 
            // dataGridView
            // 
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dimViewColumn,
            this.ruleViewColumn});
            this.dataGridView.DataMember = "AggRules";
            this.dataGridView.DataSource = this.aggRuleDataset;
            this.dataGridView.Location = new System.Drawing.Point(15, 54);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView.Size = new System.Drawing.Size(245, 123);
            this.dataGridView.TabIndex = 2;
            // 
            // dimViewColumn
            // 
            this.dimViewColumn.DataPropertyName = "Dimension";
            this.dimViewColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dimViewColumn.HeaderText = "Dimension";
            this.dimViewColumn.Name = "dimViewColumn";
            this.dimViewColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dimViewColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ruleViewColumn
            // 
            this.ruleViewColumn.DataPropertyName = "Rule";
            this.ruleViewColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ruleViewColumn.HeaderText = "Rule";
            this.ruleViewColumn.Items.AddRange(new object[] {
            "SUM",
            "AVG",
            "COUNT",
            "COUNT DISTINCT",
            "MIN",
            "MAX",
            "STDEV"});
            this.ruleViewColumn.Name = "ruleViewColumn";
            this.ruleViewColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ruleViewColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // aggRuleDataset
            // 
            this.aggRuleDataset.DataSetName = "NewDataSet";
            this.aggRuleDataset.Tables.AddRange(new System.Data.DataTable[] {
            this.aggRuleTable});
            // 
            // aggRuleTable
            // 
            this.aggRuleTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dimAggCol,
            this.dimAggRule});
            this.aggRuleTable.TableName = "AggRules";
            // 
            // dimAggCol
            // 
            this.dimAggCol.AllowDBNull = false;
            this.dimAggCol.ColumnName = "Dimension";
            // 
            // dimAggRule
            // 
            this.dimAggRule.AllowDBNull = false;
            this.dimAggRule.ColumnName = "Rule";
            this.dimAggRule.DefaultValue = "SUM";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.okButton.Location = new System.Drawing.Point(15, 183);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Dimension-specific Rules";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(185, 183);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // AggRule
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(273, 212);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.baseAggRuleBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AggRule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define Aggregation Rule";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggRuleDataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggRuleTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox baseAggRuleBox;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Data.DataSet aggRuleDataset;
        private System.Data.DataTable aggRuleTable;
        private System.Data.DataColumn dimAggCol;
        private System.Data.DataColumn dimAggRule;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn dimViewColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn ruleViewColumn;
    }
}