using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    interface RenameNode
    {
        void rename(NodeLabelEditEventArgs e);
    }
}
