namespace Performance_Optimizer_Administration
{
    partial class BuildApplicationReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeButton = new System.Windows.Forms.Button();
            this.verbose = new System.Windows.Forms.CheckBox();
            this.operationsDataGridView = new System.Windows.Forms.DataGridView();
            this.OperationText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.operationsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Location = new System.Drawing.Point(545, 391);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // verbose
            // 
            this.verbose.AutoSize = true;
            this.verbose.Location = new System.Drawing.Point(12, 391);
            this.verbose.Name = "verbose";
            this.verbose.Size = new System.Drawing.Size(65, 17);
            this.verbose.TabIndex = 3;
            this.verbose.Text = "Verbose";
            this.verbose.UseVisualStyleBackColor = true;
            this.verbose.CheckedChanged += new System.EventHandler(this.verbose_CheckedChanged);
            // 
            // operationsDataGridView
            // 
            this.operationsDataGridView.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.operationsDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.operationsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.operationsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.operationsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operationsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OperationText});
            this.operationsDataGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this.operationsDataGridView.Location = new System.Drawing.Point(0, 0);
            this.operationsDataGridView.Name = "operationsDataGridView";
            this.operationsDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.operationsDataGridView.Size = new System.Drawing.Size(632, 385);
            this.operationsDataGridView.TabIndex = 5;
            // 
            // OperationText
            // 
            this.OperationText.HeaderText = "Operation";
            this.OperationText.Name = "OperationText";
            this.OperationText.ReadOnly = true;
            this.OperationText.Width = 500;
            // 
            // BuildApplicationReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 426);
            this.Controls.Add(this.operationsDataGridView);
            this.Controls.Add(this.verbose);
            this.Controls.Add(this.closeButton);
            this.Name = "BuildApplicationReportForm";
            this.Text = "Build Application - Operations Performed";
            ((System.ComponentModel.ISupportInitialize)(this.operationsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.CheckBox verbose;
        private System.Windows.Forms.DataGridView operationsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperationText;
    }
}