﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("PackageImport", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class PackageImport
    {
        public Guid SpaceID;
        public Guid PackageID;
        public string PackageName;
        public ImportedItem[] Items;

        public void setItems(List<string> itemNames)
        {
            List<ImportedItem> iilist = new List<ImportedItem>();
            // Make sure the list of imported items is complete, remove any dead ones
            foreach (string name in itemNames)
            {
                ImportedItem iitem = null;
                if (Items != null)
                    foreach (ImportedItem ii in Items)
                    {
                        if (ii.ItemName == name)
                        {
                            iitem = ii;
                            break;
                        }
                    }
                if (iitem == null)
                {
                    iitem = new ImportedItem();
                    iitem.ItemName = name;
                }
                iilist.Add(iitem);
            }
            Items = iilist.ToArray();
        }

        public ImportedItem getImportedItem(string itemName, bool add)
        {
            ImportedItem iitem = null;
            if (Items != null)
                foreach (ImportedItem ii in Items)
                {
                    if (ii.ItemName == itemName)
                    {
                        iitem = ii;
                        break;
                    }
                }
            if (iitem != null || !add)
                return iitem;
            iitem = new ImportedItem();
            iitem.ItemName = itemName;
            List<ImportedItem> iilist = new List<ImportedItem>();
            if (Items != null)
                foreach (ImportedItem sit in Items)
                    iilist.Add(sit);
            iilist.Add(iitem);
            Items = iilist.ToArray();
            return iitem;
        }

        public void updatePackageImport(MainAdminForm maf)
        {
            if (maf.Imports == null)
                return;
            for(int i = 0; i < maf.Imports.Length; i++)
            {
                PackageImport pi = maf.Imports[i];
                if (pi.PackageName == PackageName && pi.SpaceID == SpaceID)
                {
                    maf.Imports[i] = this;
                    break;
                }
            }
        }
    }
}