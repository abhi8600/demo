﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Performance_Optimizer_Administration
{
    public class TimeZoneUtil
    {
        private static Dictionary<string, string> mapTZ = null;

        public static List<TimeZoneInfo> getSystemTimeZones()
        {
            if (mapTZ == null)//fill up the map for the first time
            {
                mapTZ = new Dictionary<string, string>();
                mapTZ.Add("AUS Central Standard Time", "Australia/Darwin");
                mapTZ.Add("AUS Eastern Standard Time", "Australia/Sydney");
                mapTZ.Add("Afghanistan Standard Time", "Asia/Kabul");
                mapTZ.Add("Alaskan Standard Time", "America/Anchorage");
                mapTZ.Add("Arab Standard Time", "Asia/Riyadh");
                mapTZ.Add("Arabian Standard Time", "Asia/Dubai");
                mapTZ.Add("Arabic Standard Time", "Asia/Baghdad");
                mapTZ.Add("Argentina Standard Time", "America/Buenos_Aires");
                mapTZ.Add("Armenian Standard Time", "Asia/Yerevan");
                mapTZ.Add("Atlantic Standard Time", "America/Halifax");
                mapTZ.Add("Azerbaijan Standard Time", "Asia/Baku");
                mapTZ.Add("Azores Standard Time", "Atlantic/Azores");
                mapTZ.Add("Canada Central Standard Time", "America/Regina");
                mapTZ.Add("Cape Verde Standard Time", "Atlantic/Cape_Verde");
                mapTZ.Add("Caucasus Standard Time", "Asia/Tbilisi");
                mapTZ.Add("Cen. Australia Standard Time", "Australia/Adelaide");
                mapTZ.Add("Central America Standard Time", "America/Guatemala");
                mapTZ.Add("Central Asia Standard Time", "Asia/Dhaka");
                mapTZ.Add("Central Brazilian Standard Time", "America/Manaus");
                mapTZ.Add("Central Europe Standard Time", "Europe/Budapest");
                mapTZ.Add("Central European Standard Time", "Europe/Warsaw");
                mapTZ.Add("Central Pacific Standard Time", "Pacific/Guadalcanal");
                mapTZ.Add("Central Standard Time", "America/Chicago");
                mapTZ.Add("Central Standard Time (Mexico)", "America/Mexico_City");
                mapTZ.Add("China Standard Time", "Asia/Shanghai");
                mapTZ.Add("Dateline Standard Time", "Etc/GMT+12");
                mapTZ.Add("E. Africa Standard Time", "Africa/Nairobi");
                mapTZ.Add("E. Australia Standard Time", "Australia/Brisbane");
                mapTZ.Add("E. Europe Standard Time", "Europe/Minsk");
                mapTZ.Add("E. South America Standard Time", "America/Sao_Paulo");
                mapTZ.Add("Eastern Standard Time", "America/New_York");
                mapTZ.Add("Egypt Standard Time", "Africa/Cairo");
                mapTZ.Add("Ekaterinburg Standard Time", "Asia/Yekaterinburg");
                mapTZ.Add("FLE Standard Time", "Europe/Kiev");
                mapTZ.Add("Fiji Standard Time", "Pacific/Fiji");
                mapTZ.Add("GMT Standard Time", "Europe/London");
                mapTZ.Add("GTB Standard Time", "Europe/Istanbul");
                mapTZ.Add("Georgian Standard Time", "Etc/GMT-3");
                mapTZ.Add("Greenland Standard Time", "America/Godthab");
                mapTZ.Add("Greenwich Standard Time", "Africa/Casablanca");
                mapTZ.Add("Hawaiian Standard Time", "Pacific/Honolulu");
                mapTZ.Add("India Standard Time", "Asia/Calcutta");
                mapTZ.Add("Iran Standard Time", "Asia/Tehran");
                mapTZ.Add("Israel Standard Time", "Asia/Jerusalem");
                mapTZ.Add("Jordan Standard Time", "Asia/Amman");
                mapTZ.Add("Korea Standard Time", "Asia/Seoul");
                mapTZ.Add("Mexico Standard Time", "America/Mexico_City");
                mapTZ.Add("Mexico Standard Time 2", "America/Chihuahua");
                mapTZ.Add("Mid-Atlantic Standard Time", "Atlantic/South_Georgia");
                mapTZ.Add("Middle East Standard Time", "Asia/Beirut");
                mapTZ.Add("Montevideo Standard Time", "America/Montevideo");
                mapTZ.Add("Mountain Standard Time", "America/Denver");
                mapTZ.Add("Mountain Standard Time (Mexico)", "America/Chihuahua");
                mapTZ.Add("Myanmar Standard Time", "Asia/Rangoon");
                mapTZ.Add("N. Central Asia Standard Time", "Asia/Novosibirsk");
                mapTZ.Add("Namibia Standard Time", "Africa/Windhoek");
                mapTZ.Add("Nepal Standard Time", "Asia/Katmandu");
                mapTZ.Add("New Zealand Standard Time", "Pacific/Auckland");
                mapTZ.Add("Newfoundland Standard Time", "America/St_Johns");
                mapTZ.Add("North Asia East Standard Time", "Asia/Irkutsk");
                mapTZ.Add("North Asia Standard Time", "Asia/Krasnoyarsk");
                mapTZ.Add("Pacific SA Standard Time", "America/Santiago");
                mapTZ.Add("Pacific Standard Time", "America/Los_Angeles");
                mapTZ.Add("Pacific Standard Time (Mexico)", "America/Tijuana");
                mapTZ.Add("Romance Standard Time", "Europe/Paris");
                mapTZ.Add("Russian Standard Time", "Europe/Moscow");
                mapTZ.Add("SA Eastern Standard Time", "Etc/GMT+3");
                mapTZ.Add("SA Pacific Standard Time", "America/Bogota");
                mapTZ.Add("SA Western Standard Time", "America/La_Paz");
                mapTZ.Add("SE Asia Standard Time", "Asia/Bangkok");
                mapTZ.Add("Samoa Standard Time", "Pacific/Apia");
                mapTZ.Add("Singapore Standard Time", "Asia/Singapore");
                mapTZ.Add("South Africa Standard Time", "Africa/Johannesburg");
                mapTZ.Add("Sri Lanka Standard Time", "Asia/Colombo");
                mapTZ.Add("Taipei Standard Time", "Asia/Taipei");
                mapTZ.Add("Tasmania Standard Time", "Australia/Hobart");
                mapTZ.Add("Tokyo Standard Time", "Asia/Tokyo");
                mapTZ.Add("Tonga Standard Time", "Pacific/Tongatapu");
                mapTZ.Add("US Eastern Standard Time", "Etc/GMT+5");
                mapTZ.Add("US Mountain Standard Time", "America/Phoenix");
                mapTZ.Add("Venezuela Standard Time", "America/Caracas");
                mapTZ.Add("Vladivostok Standard Time", "Asia/Vladivostok");
                mapTZ.Add("W. Australia Standard Time", "Australia/Perth");
                mapTZ.Add("W. Central Africa Standard Time", "Africa/Lagos");
                mapTZ.Add("W. Europe Standard Time", "Europe/Berlin");
                mapTZ.Add("West Asia Standard Time", "Asia/Karachi");
                mapTZ.Add("West Pacific Standard Time", "Pacific/Port_Moresby");
                mapTZ.Add("Yakutsk Standard Time", "Asia/Yakutsk");
            }

            List<TimeZoneInfo> lstTz = new List<TimeZoneInfo>(TimeZoneInfo.GetSystemTimeZones());
            List<TimeZoneInfo> newLst = new List<TimeZoneInfo>();
            foreach (TimeZoneInfo tz in lstTz)
            {
                string value = null;
                if (mapTZ.TryGetValue(tz.Id, out value))
                    newLst.Add(tz);
            }
            newLst.Sort(timeZoneComparator);
            return newLst;
        }

        private static int timeZoneComparator(TimeZoneInfo tz1, TimeZoneInfo tz2)
        {
            int diff = tz1.BaseUtcOffset.CompareTo(tz2.BaseUtcOffset);
            if (diff != 0)
                return diff;
            else
                return (tz1.DisplayName.CompareTo(tz2.DisplayName));            
        }
    }
}
