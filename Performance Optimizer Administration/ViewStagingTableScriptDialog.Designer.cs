﻿namespace Performance_Optimizer_Administration
{
    partial class ViewStagingTableScriptDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.lblScript = new System.Windows.Forms.Label();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.lblInputQuery = new System.Windows.Forms.Label();
            this.txtInputQuery = new System.Windows.Forms.TextBox();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.lblOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(265, 495);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblScript
            // 
            this.lblScript.AutoSize = true;
            this.lblScript.Location = new System.Drawing.Point(-1, 196);
            this.lblScript.Name = "lblScript";
            this.lblScript.Size = new System.Drawing.Size(34, 13);
            this.lblScript.TabIndex = 11;
            this.lblScript.Text = "Script";
            // 
            // txtScript
            // 
            this.txtScript.Location = new System.Drawing.Point(2, 212);
            this.txtScript.Multiline = true;
            this.txtScript.Name = "txtScript";
            this.txtScript.ReadOnly = true;
            this.txtScript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtScript.Size = new System.Drawing.Size(608, 277);
            this.txtScript.TabIndex = 10;
            // 
            // lblInputQuery
            // 
            this.lblInputQuery.AutoSize = true;
            this.lblInputQuery.Location = new System.Drawing.Point(-1, 9);
            this.lblInputQuery.Name = "lblInputQuery";
            this.lblInputQuery.Size = new System.Drawing.Size(62, 13);
            this.lblInputQuery.TabIndex = 13;
            this.lblInputQuery.Text = "Input Query";
            // 
            // txtInputQuery
            // 
            this.txtInputQuery.Location = new System.Drawing.Point(2, 25);
            this.txtInputQuery.Multiline = true;
            this.txtInputQuery.Name = "txtInputQuery";
            this.txtInputQuery.ReadOnly = true;
            this.txtInputQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInputQuery.Size = new System.Drawing.Size(608, 76);
            this.txtInputQuery.TabIndex = 14;
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(2, 120);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(608, 73);
            this.txtOutput.TabIndex = 16;
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(-1, 104);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(39, 13);
            this.lblOutput.TabIndex = 15;
            this.lblOutput.Text = "Output";
            // 
            // ViewStagingTableScriptDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 522);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.txtInputQuery);
            this.Controls.Add(this.lblInputQuery);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblScript);
            this.Controls.Add(this.txtScript);
            this.Name = "ViewStagingTableScriptDialog";
            this.Text = "ViewStagingTableScriptDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.TextBox txtScript;
        private System.Windows.Forms.Label lblInputQuery;
        private System.Windows.Forms.TextBox txtInputQuery;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label lblOutput;
    }
}