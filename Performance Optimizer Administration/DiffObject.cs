﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Performance_Optimizer_Administration
{
    public class DiffObject
    {
        public DiffObject(string label)
        {
            this.label = label;
        }

        public DiffObject(string label, string baseObjectVersion, string compareObjectVersion)
        {
            this.label = label;
            this.baseObjectVersion = baseObjectVersion;
            this.compareObjectVersion = compareObjectVersion;
        }

        public DiffObject(string label, int baseObjectVersion, int compareObjectVersion)
        {
            this.label = label;
            this.baseObjectVersion = baseObjectVersion.ToString();
            this.compareObjectVersion = compareObjectVersion.ToString();
        }

        public DiffObject(string label, bool baseObjectVersion, bool compareObjectVersion)
        {
            this.label = label;
            this.baseObjectVersion = baseObjectVersion.ToString();
            this.compareObjectVersion = compareObjectVersion.ToString();
        }

        public DiffObject(string label, string[] baseObjectVersion, string[] compareObjectVersion)
        {
            this.label = label;
            this.baseObjectVersion = Util.getDisplayString(baseObjectVersion);
            this.compareObjectVersion = Util.getDisplayString(compareObjectVersion);
        }

        public DiffObject(string label, bool[] baseObjectVersion, bool[] compareObjectVersion)
        {
            this.label = label;
            this.baseObjectVersion = Util.getDisplayString(baseObjectVersion);
            this.compareObjectVersion = Util.getDisplayString(compareObjectVersion);
        }

        public string label;
        public string baseObjectVersion;
        public string compareObjectVersion;

        public string getSerializedString()
        {
            return label + "|" + baseObjectVersion + "|" + compareObjectVersion;
        }

        public static string getBaseOnlyExists()
        {
            return "Object|Exists|Does not exist";
        }

        public static string getCompareOnlyExists()
        {
            return "Object|Does not exist|Exists";
        }
    }
}