namespace Performance_Optimizer_Administration
{
    partial class Scripts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.scriptTabPage = new System.Windows.Forms.TabPage();
            this.scriptTextBox = new System.Windows.Forms.TextBox();
            this.scriptLabel = new System.Windows.Forms.Label();
            this.scriptContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addScriptGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeScriptGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scriptUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scriptDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.scriptTabPage.SuspendLayout();
            this.scriptContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.scriptTabPage);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // scriptTabPage
            // 
            this.scriptTabPage.AutoScroll = true;
            this.scriptTabPage.Controls.Add(this.scriptTextBox);
            this.scriptTabPage.Controls.Add(this.scriptLabel);
            this.scriptTabPage.Location = new System.Drawing.Point(4, 22);
            this.scriptTabPage.Name = "scriptTabPage";
            this.scriptTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.scriptTabPage.Size = new System.Drawing.Size(844, 707);
            this.scriptTabPage.TabIndex = 0;
            this.scriptTabPage.Text = "Scripts";
            this.scriptTabPage.UseVisualStyleBackColor = true;
            // 
            // scriptTextBox
            // 
            this.scriptTextBox.Location = new System.Drawing.Point(29, 63);
            this.scriptTextBox.Multiline = true;
            this.scriptTextBox.Name = "scriptTextBox";
            this.scriptTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.scriptTextBox.Size = new System.Drawing.Size(787, 400);
            this.scriptTextBox.TabIndex = 1;
            this.scriptTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.scriptTextBox_KeyDown);
            this.scriptTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.scriptTextBox_KeyPress);
            // 
            // scriptLabel
            // 
            this.scriptLabel.AutoSize = true;
            this.scriptLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scriptLabel.Location = new System.Drawing.Point(29, 25);
            this.scriptLabel.Name = "scriptLabel";
            this.scriptLabel.Size = new System.Drawing.Size(137, 16);
            this.scriptLabel.TabIndex = 0;
            this.scriptLabel.Text = "Type your script here:";
            // 
            // scriptContextMenuStrip
            // 
            this.scriptContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addScriptGroupToolStripMenuItem,
            this.removeScriptGroupToolStripMenuItem,
            this.addScriptToolStripMenuItem,
            this.removeScriptToolStripMenuItem,
            this.scriptUpToolStripMenuItem,
            this.scriptDownToolStripMenuItem});
            this.scriptContextMenuStrip.Name = "scriptContextMenuStrip";
            this.scriptContextMenuStrip.Size = new System.Drawing.Size(187, 136);
            // 
            // addScriptGroupToolStripMenuItem
            // 
            this.addScriptGroupToolStripMenuItem.Name = "addScriptGroupToolStripMenuItem";
            this.addScriptGroupToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.addScriptGroupToolStripMenuItem.Text = "Add Script Group";
            this.addScriptGroupToolStripMenuItem.Click += new System.EventHandler(this.addScriptGroupToolStripMenuItem_Click);
            // 
            // removeScriptGroupToolStripMenuItem
            // 
            this.removeScriptGroupToolStripMenuItem.Name = "removeScriptGroupToolStripMenuItem";
            this.removeScriptGroupToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.removeScriptGroupToolStripMenuItem.Text = "Remove Script Group";
            this.removeScriptGroupToolStripMenuItem.Click += new System.EventHandler(this.removeScriptGroupToolStripMenuItem_Click);
            // 
            // addScriptToolStripMenuItem
            // 
            this.addScriptToolStripMenuItem.Name = "addScriptToolStripMenuItem";
            this.addScriptToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.addScriptToolStripMenuItem.Text = "Add Script";
            this.addScriptToolStripMenuItem.Click += new System.EventHandler(this.addScriptToolStripMenuItem_Click);
            // 
            // removeScriptToolStripMenuItem
            // 
            this.removeScriptToolStripMenuItem.Name = "removeScriptToolStripMenuItem";
            this.removeScriptToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.removeScriptToolStripMenuItem.Text = "Remove Script";
            this.removeScriptToolStripMenuItem.Click += new System.EventHandler(this.removeScriptToolStripMenuItem_Click);
            // 
            // scriptUpToolStripMenuItem
            // 
            this.scriptUpToolStripMenuItem.Name = "scriptUpToolStripMenuItem";
            this.scriptUpToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.scriptUpToolStripMenuItem.Text = "Up";
            this.scriptUpToolStripMenuItem.Click += new System.EventHandler(this.scriptUpToolStripMenuItem_Click);
            // 
            // scriptDownToolStripMenuItem
            // 
            this.scriptDownToolStripMenuItem.Name = "scriptDownToolStripMenuItem";
            this.scriptDownToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.scriptDownToolStripMenuItem.Text = "Down";
            this.scriptDownToolStripMenuItem.Click += new System.EventHandler(this.scriptDownToolStripMenuItem_Click);
            // 
            // Scripts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "Scripts";
            this.Text = "Scripts";
            this.tabControl.ResumeLayout(false);
            this.scriptTabPage.ResumeLayout(false);
            this.scriptTabPage.PerformLayout();
            this.scriptContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage scriptTabPage;
        private System.Windows.Forms.TextBox scriptTextBox;
        private System.Windows.Forms.Label scriptLabel;
        private System.Windows.Forms.ContextMenuStrip scriptContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addScriptGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeScriptGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scriptUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scriptDownToolStripMenuItem;

    }
}