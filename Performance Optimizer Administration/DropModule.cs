using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    interface DropModule
    {
        void doDrop(TreeNode sourceNode, TreeNode targetNode);
        DragDropEffects startDrag(TreeNode tn);
    }
}
