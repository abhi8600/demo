using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Performance_Optimizer_Administration
{
    public partial class DefineSubstitutions : Form, ResizeMemoryForm
    {
        public List<ColumnSubstitution> subList;
        private MainAdminForm ma;
        private string windowName = "ColumnSubstitutions";

        public DefineSubstitutions(MainAdminForm ma, List<ColumnSubstitution> subList)
        {
            InitializeComponent();
            this.ma = ma;
            this.subList = subList;
            resetWindowFromSettings();
            if (this.subList == null)
            {
                this.subList = new List<ColumnSubstitution>();
                subList = this.subList;
            }
            foreach(ColumnSubstitution cs in subList)
            {
                ListViewItem lvi = new ListViewItem(new string[] { cs.Original, cs.New });
                tableList.Items.Add(lvi);
            }
        }

        private void addTable_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(new string[] { originalNameBox.Text, newNameBox.Text});
            tableList.Items.Add(lvi);
        }

        private void removeTable_Click(object sender, EventArgs e)
        {
            if (tableList.SelectedIndices.Count == 1)
            {
                tableList.Items.RemoveAt(tableList.SelectedIndices[0]);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            subList = new List<ColumnSubstitution>();
            for (int i = 0; i < tableList.Items.Count; i++)
            {
                ColumnSubstitution cs = new ColumnSubstitution();
                ListViewItem lvi = tableList.Items[i];
                cs.Original = lvi.SubItems[0].Text;
                cs.New = lvi.SubItems[1].Text;
                subList.Add(cs);
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

        }

        private void tableList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tableList.SelectedItems.Count!=1) return;
            originalNameBox.Text = tableList.SelectedItems[0].SubItems[0].Text;
            newNameBox.Text = tableList.SelectedItems[0].SubItems[1].Text;
        }

        private void DefineSubstitutions_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members
    }
}