namespace Performance_Optimizer_Administration
{
    partial class ServerProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.serverTabPage = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.disableCachingForDimensionsBox = new System.Windows.Forms.CheckBox();
            this.disableMeasureTableCacheBox = new System.Windows.Forms.CheckBox();
            this.ProxyDBPasswordTextBox = new System.Windows.Forms.TextBox();
            this.ProxyDBUserNameTextBox = new System.Windows.Forms.TextBox();
            this.ProxyDBConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.scriptQueryTimeoutBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.maxOutputRowsBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.maxInputRowsBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.maxScriptStatementsBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.allowDimensionalCrossjoins = new System.Windows.Forms.CheckBox();
            this.DisplayTimeZoneComboBox = new System.Windows.Forms.ComboBox();
            this.ProcessingTimeZoneComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.snowflakeSize = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.generateUniqueKeys = new System.Windows.Forms.CheckBox();
            this.stagingEncryptionKeyTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.genSABox = new System.Windows.Forms.CheckBox();
            this.maxQueryTimeTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.mapNullsToZeroCheckBox = new System.Windows.Forms.CheckBox();
            this.customerMailHostTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.maxRecords = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maxCacheEntries = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cachePath = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.appPath = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.serverTabPage.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.serverTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // serverTabPage
            // 
            this.serverTabPage.AutoScroll = true;
            this.serverTabPage.Controls.Add(this.groupBox19);
            this.serverTabPage.Location = new System.Drawing.Point(4, 22);
            this.serverTabPage.Name = "serverTabPage";
            this.serverTabPage.Size = new System.Drawing.Size(844, 707);
            this.serverTabPage.TabIndex = 18;
            this.serverTabPage.Text = "Server Properties";
            this.serverTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.disableCachingForDimensionsBox);
            this.groupBox19.Controls.Add(this.disableMeasureTableCacheBox);
            this.groupBox19.Controls.Add(this.ProxyDBPasswordTextBox);
            this.groupBox19.Controls.Add(this.ProxyDBUserNameTextBox);
            this.groupBox19.Controls.Add(this.ProxyDBConnectionStringTextBox);
            this.groupBox19.Controls.Add(this.scriptQueryTimeoutBox);
            this.groupBox19.Controls.Add(this.label10);
            this.groupBox19.Controls.Add(this.maxOutputRowsBox);
            this.groupBox19.Controls.Add(this.label11);
            this.groupBox19.Controls.Add(this.maxInputRowsBox);
            this.groupBox19.Controls.Add(this.label12);
            this.groupBox19.Controls.Add(this.maxScriptStatementsBox);
            this.groupBox19.Controls.Add(this.label13);
            this.groupBox19.Controls.Add(this.allowDimensionalCrossjoins);
            this.groupBox19.Controls.Add(this.DisplayTimeZoneComboBox);
            this.groupBox19.Controls.Add(this.ProcessingTimeZoneComboBox);
            this.groupBox19.Controls.Add(this.label9);
            this.groupBox19.Controls.Add(this.label8);
            this.groupBox19.Controls.Add(this.snowflakeSize);
            this.groupBox19.Controls.Add(this.label7);
            this.groupBox19.Controls.Add(this.generateUniqueKeys);
            this.groupBox19.Controls.Add(this.stagingEncryptionKeyTextBox);
            this.groupBox19.Controls.Add(this.label6);
            this.groupBox19.Controls.Add(this.genSABox);
            this.groupBox19.Controls.Add(this.maxQueryTimeTextBox);
            this.groupBox19.Controls.Add(this.label5);
            this.groupBox19.Controls.Add(this.mapNullsToZeroCheckBox);
            this.groupBox19.Controls.Add(this.customerMailHostTextBox);
            this.groupBox19.Controls.Add(this.label3);
            this.groupBox19.Controls.Add(this.maxRecords);
            this.groupBox19.Controls.Add(this.label4);
            this.groupBox19.Controls.Add(this.maxCacheEntries);
            this.groupBox19.Controls.Add(this.label1);
            this.groupBox19.Controls.Add(this.cachePath);
            this.groupBox19.Controls.Add(this.label50);
            this.groupBox19.Controls.Add(this.appPath);
            this.groupBox19.Controls.Add(this.label39);
            this.groupBox19.Location = new System.Drawing.Point(6, 8);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(832, 586);
            this.groupBox19.TabIndex = 17;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Server Properties";
            // 
            // disableCachingForDimensionsBox
            // 
            this.disableCachingForDimensionsBox.AutoSize = true;
            this.disableCachingForDimensionsBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.disableCachingForDimensionsBox.Location = new System.Drawing.Point(552, 49);
            this.disableCachingForDimensionsBox.Name = "disableCachingForDimensionsBox";
            this.disableCachingForDimensionsBox.Size = new System.Drawing.Size(255, 17);
            this.disableCachingForDimensionsBox.TabIndex = 73;
            this.disableCachingForDimensionsBox.Text = "Disable Caching for Generated Dimension Tables";
            this.disableCachingForDimensionsBox.UseVisualStyleBackColor = true;
            // 
            // disableMeasureTableCacheBox
            // 
            this.disableMeasureTableCacheBox.AutoSize = true;
            this.disableMeasureTableCacheBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.disableMeasureTableCacheBox.Location = new System.Drawing.Point(552, 25);
            this.disableMeasureTableCacheBox.Name = "disableMeasureTableCacheBox";
            this.disableMeasureTableCacheBox.Size = new System.Drawing.Size(247, 17);
            this.disableMeasureTableCacheBox.TabIndex = 72;
            this.disableMeasureTableCacheBox.Text = "Disable Caching for Generated Measure Tables";
            this.disableMeasureTableCacheBox.UseVisualStyleBackColor = true;
            // 
            // ProxyDBPasswordTextBox
            // 
            this.ProxyDBPasswordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProxyDBPasswordTextBox.Location = new System.Drawing.Point(315, 504);
            this.ProxyDBPasswordTextBox.Name = "ProxyDBPasswordTextBox";
            this.ProxyDBPasswordTextBox.PasswordChar = '*';
            this.ProxyDBPasswordTextBox.Size = new System.Drawing.Size(296, 20);
            this.ProxyDBPasswordTextBox.TabIndex = 71;
            this.ProxyDBPasswordTextBox.Visible = false;
            // 
            // ProxyDBUserNameTextBox
            // 
            this.ProxyDBUserNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProxyDBUserNameTextBox.Location = new System.Drawing.Point(315, 478);
            this.ProxyDBUserNameTextBox.Name = "ProxyDBUserNameTextBox";
            this.ProxyDBUserNameTextBox.PasswordChar = '*';
            this.ProxyDBUserNameTextBox.Size = new System.Drawing.Size(296, 20);
            this.ProxyDBUserNameTextBox.TabIndex = 70;
            this.ProxyDBUserNameTextBox.Visible = false;
            // 
            // ProxyDBConnectionStringTextBox
            // 
            this.ProxyDBConnectionStringTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProxyDBConnectionStringTextBox.Location = new System.Drawing.Point(315, 455);
            this.ProxyDBConnectionStringTextBox.Name = "ProxyDBConnectionStringTextBox";
            this.ProxyDBConnectionStringTextBox.PasswordChar = '*';
            this.ProxyDBConnectionStringTextBox.Size = new System.Drawing.Size(296, 20);
            this.ProxyDBConnectionStringTextBox.TabIndex = 69;
            this.ProxyDBConnectionStringTextBox.Visible = false;
            // 
            // scriptQueryTimeoutBox
            // 
            this.scriptQueryTimeoutBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scriptQueryTimeoutBox.Location = new System.Drawing.Point(211, 521);
            this.scriptQueryTimeoutBox.Name = "scriptQueryTimeoutBox";
            this.scriptQueryTimeoutBox.Size = new System.Drawing.Size(56, 20);
            this.scriptQueryTimeoutBox.TabIndex = 67;
            this.scriptQueryTimeoutBox.Text = "0";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 521);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(197, 23);
            this.label10.TabIndex = 68;
            this.label10.Text = "Script Query Timeout";
            // 
            // maxOutputRowsBox
            // 
            this.maxOutputRowsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxOutputRowsBox.Location = new System.Drawing.Point(211, 498);
            this.maxOutputRowsBox.Name = "maxOutputRowsBox";
            this.maxOutputRowsBox.Size = new System.Drawing.Size(56, 20);
            this.maxOutputRowsBox.TabIndex = 65;
            this.maxOutputRowsBox.Text = "0";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 498);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 23);
            this.label11.TabIndex = 66;
            this.label11.Text = "Max Output Records";
            // 
            // maxInputRowsBox
            // 
            this.maxInputRowsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxInputRowsBox.Location = new System.Drawing.Point(211, 475);
            this.maxInputRowsBox.Name = "maxInputRowsBox";
            this.maxInputRowsBox.Size = new System.Drawing.Size(56, 20);
            this.maxInputRowsBox.TabIndex = 63;
            this.maxInputRowsBox.Text = "0";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 475);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 23);
            this.label12.TabIndex = 64;
            this.label12.Text = "Max Input Records";
            // 
            // maxScriptStatementsBox
            // 
            this.maxScriptStatementsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxScriptStatementsBox.Location = new System.Drawing.Point(211, 452);
            this.maxScriptStatementsBox.Name = "maxScriptStatementsBox";
            this.maxScriptStatementsBox.Size = new System.Drawing.Size(56, 20);
            this.maxScriptStatementsBox.TabIndex = 61;
            this.maxScriptStatementsBox.Text = "0";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 452);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(182, 23);
            this.label13.TabIndex = 62;
            this.label13.Text = "Max Script Statements";
            // 
            // allowDimensionalCrossjoins
            // 
            this.allowDimensionalCrossjoins.AutoSize = true;
            this.allowDimensionalCrossjoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowDimensionalCrossjoins.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.allowDimensionalCrossjoins.Location = new System.Drawing.Point(11, 313);
            this.allowDimensionalCrossjoins.Name = "allowDimensionalCrossjoins";
            this.allowDimensionalCrossjoins.Size = new System.Drawing.Size(207, 20);
            this.allowDimensionalCrossjoins.TabIndex = 60;
            this.allowDimensionalCrossjoins.Text = "Allow Dimensional Cross Joins";
            this.allowDimensionalCrossjoins.UseVisualStyleBackColor = true;
            // 
            // DisplayTimeZoneComboBox
            // 
            this.DisplayTimeZoneComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DisplayTimeZoneComboBox.FormattingEnabled = true;
            this.DisplayTimeZoneComboBox.Location = new System.Drawing.Point(209, 414);
            this.DisplayTimeZoneComboBox.Name = "DisplayTimeZoneComboBox";
            this.DisplayTimeZoneComboBox.Size = new System.Drawing.Size(296, 21);
            this.DisplayTimeZoneComboBox.TabIndex = 59;
            this.DisplayTimeZoneComboBox.DropDown += new System.EventHandler(this.TimeZoneComboBox_DropDown);
            // 
            // ProcessingTimeZoneComboBox
            // 
            this.ProcessingTimeZoneComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProcessingTimeZoneComboBox.FormattingEnabled = true;
            this.ProcessingTimeZoneComboBox.Location = new System.Drawing.Point(209, 381);
            this.ProcessingTimeZoneComboBox.Name = "ProcessingTimeZoneComboBox";
            this.ProcessingTimeZoneComboBox.Size = new System.Drawing.Size(296, 21);
            this.ProcessingTimeZoneComboBox.TabIndex = 58;
            this.ProcessingTimeZoneComboBox.DropDown += new System.EventHandler(this.TimeZoneComboBox_DropDown);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 414);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(182, 23);
            this.label9.TabIndex = 56;
            this.label9.Text = "Display Time Zone";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 381);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(182, 23);
            this.label8.TabIndex = 55;
            this.label8.Text = "Processing Time Zone";
            // 
            // snowflakeSize
            // 
            this.snowflakeSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.snowflakeSize.Location = new System.Drawing.Point(211, 143);
            this.snowflakeSize.Name = "snowflakeSize";
            this.snowflakeSize.Size = new System.Drawing.Size(56, 20);
            this.snowflakeSize.TabIndex = 53;
            this.snowflakeSize.Text = "3";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 23);
            this.label7.TabIndex = 54;
            this.label7.Text = "Max Snowflake Size";
            // 
            // generateUniqueKeys
            // 
            this.generateUniqueKeys.AutoSize = true;
            this.generateUniqueKeys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateUniqueKeys.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.generateUniqueKeys.Location = new System.Drawing.Point(11, 287);
            this.generateUniqueKeys.Name = "generateUniqueKeys";
            this.generateUniqueKeys.Size = new System.Drawing.Size(677, 20);
            this.generateUniqueKeys.TabIndex = 52;
            this.generateUniqueKeys.Text = "Generate unique surrogate key names (appends a generated number to prevent collis" +
    "ion with source columns)";
            this.generateUniqueKeys.UseVisualStyleBackColor = true;
            // 
            // stagingEncryptionKeyTextBox
            // 
            this.stagingEncryptionKeyTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.stagingEncryptionKeyTextBox.Location = new System.Drawing.Point(209, 348);
            this.stagingEncryptionKeyTextBox.Name = "stagingEncryptionKeyTextBox";
            this.stagingEncryptionKeyTextBox.PasswordChar = '*';
            this.stagingEncryptionKeyTextBox.Size = new System.Drawing.Size(296, 20);
            this.stagingEncryptionKeyTextBox.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(182, 23);
            this.label6.TabIndex = 51;
            this.label6.Text = "Staging Encryption Key";
            // 
            // genSABox
            // 
            this.genSABox.AutoSize = true;
            this.genSABox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.genSABox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.genSABox.Location = new System.Drawing.Point(11, 261);
            this.genSABox.Name = "genSABox";
            this.genSABox.Size = new System.Drawing.Size(509, 20);
            this.genSABox.TabIndex = 49;
            this.genSABox.Text = "Generate a complete subject area automatically (overrides defined subject areas)";
            this.genSABox.UseVisualStyleBackColor = true;
            // 
            // maxQueryTimeTextBox
            // 
            this.maxQueryTimeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxQueryTimeTextBox.Location = new System.Drawing.Point(211, 120);
            this.maxQueryTimeTextBox.Name = "maxQueryTimeTextBox";
            this.maxQueryTimeTextBox.Size = new System.Drawing.Size(56, 20);
            this.maxQueryTimeTextBox.TabIndex = 47;
            this.maxQueryTimeTextBox.Text = "-1";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(197, 23);
            this.label5.TabIndex = 48;
            this.label5.Text = "Max Query Time (seconds)";
            // 
            // mapNullsToZeroCheckBox
            // 
            this.mapNullsToZeroCheckBox.AutoSize = true;
            this.mapNullsToZeroCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mapNullsToZeroCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.mapNullsToZeroCheckBox.Location = new System.Drawing.Point(11, 235);
            this.mapNullsToZeroCheckBox.Name = "mapNullsToZeroCheckBox";
            this.mapNullsToZeroCheckBox.Size = new System.Drawing.Size(135, 20);
            this.mapNullsToZeroCheckBox.TabIndex = 46;
            this.mapNullsToZeroCheckBox.Text = "Map Nulls To Zero";
            this.mapNullsToZeroCheckBox.UseVisualStyleBackColor = true;
            // 
            // customerMailHostTextBox
            // 
            this.customerMailHostTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customerMailHostTextBox.Location = new System.Drawing.Point(211, 200);
            this.customerMailHostTextBox.Name = "customerMailHostTextBox";
            this.customerMailHostTextBox.Size = new System.Drawing.Size(296, 20);
            this.customerMailHostTextBox.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 23);
            this.label3.TabIndex = 32;
            this.label3.Text = "Customer Mail Host";
            // 
            // maxRecords
            // 
            this.maxRecords.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxRecords.Location = new System.Drawing.Point(211, 97);
            this.maxRecords.Name = "maxRecords";
            this.maxRecords.Size = new System.Drawing.Size(56, 20);
            this.maxRecords.TabIndex = 27;
            this.maxRecords.Text = "100000";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 23);
            this.label4.TabIndex = 28;
            this.label4.Text = "Max Records";
            // 
            // maxCacheEntries
            // 
            this.maxCacheEntries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxCacheEntries.Location = new System.Drawing.Point(211, 74);
            this.maxCacheEntries.Name = "maxCacheEntries";
            this.maxCacheEntries.Size = new System.Drawing.Size(56, 20);
            this.maxCacheEntries.TabIndex = 3;
            this.maxCacheEntries.Text = "1000";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 23);
            this.label1.TabIndex = 26;
            this.label1.Text = "Max Cache Entries";
            // 
            // cachePath
            // 
            this.cachePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cachePath.Location = new System.Drawing.Point(211, 48);
            this.cachePath.Name = "cachePath";
            this.cachePath.Size = new System.Drawing.Size(296, 20);
            this.cachePath.TabIndex = 1;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(8, 48);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(112, 23);
            this.label50.TabIndex = 24;
            this.label50.Text = "Cache Path";
            // 
            // appPath
            // 
            this.appPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.appPath.Location = new System.Drawing.Point(211, 24);
            this.appPath.Name = "appPath";
            this.appPath.Size = new System.Drawing.Size(296, 20);
            this.appPath.TabIndex = 0;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(8, 24);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(112, 23);
            this.label39.TabIndex = 11;
            this.label39.Text = "Application Path";
            // 
            // ServerProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "ServerProperties";
            this.Text = "ServerProperties";
            this.tabControl.ResumeLayout(false);
            this.serverTabPage.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage serverTabPage;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox maxCacheEntries;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox maxRecords;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox customerMailHostTextBox;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox mapNullsToZeroCheckBox;
        public System.Windows.Forms.TextBox cachePath;
        public System.Windows.Forms.TextBox appPath;
        public System.Windows.Forms.TextBox maxQueryTimeTextBox;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.CheckBox genSABox;
        private System.Windows.Forms.TextBox stagingEncryptionKeyTextBox;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.CheckBox generateUniqueKeys;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox snowflakeSize;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox ProcessingTimeZoneComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox DisplayTimeZoneComboBox;
        public System.Windows.Forms.CheckBox allowDimensionalCrossjoins;
        public System.Windows.Forms.TextBox scriptQueryTimeoutBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox maxOutputRowsBox;
        public System.Windows.Forms.TextBox maxInputRowsBox;
        public System.Windows.Forms.TextBox maxScriptStatementsBox;
        public System.Windows.Forms.TextBox ProxyDBConnectionStringTextBox;
        public System.Windows.Forms.TextBox ProxyDBUserNameTextBox;
        public System.Windows.Forms.TextBox ProxyDBPasswordTextBox;
        public System.Windows.Forms.CheckBox disableCachingForDimensionsBox;
        public System.Windows.Forms.CheckBox disableMeasureTableCacheBox;
    }
}