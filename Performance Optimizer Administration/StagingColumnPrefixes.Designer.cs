namespace Performance_Optimizer_Administration
{
    partial class StagingColumnPrefixes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StagingColumnPrefixes));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.columnPrefixesLbl = new System.Windows.Forms.Label();
            this.columnPrefixesBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(61, 120);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(50, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(210, 120);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(50, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // columnPrefixesLbl
            // 
            this.columnPrefixesLbl.AutoSize = true;
            this.columnPrefixesLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.columnPrefixesLbl.Location = new System.Drawing.Point(12, 18);
            this.columnPrefixesLbl.Name = "columnPrefixesLbl";
            this.columnPrefixesLbl.Size = new System.Drawing.Size(293, 16);
            this.columnPrefixesLbl.TabIndex = 5;
            this.columnPrefixesLbl.Text = "Enter a comma-separated list of column prefixes";
            // 
            // columnPrefixesBox
            // 
            this.columnPrefixesBox.Location = new System.Drawing.Point(15, 50);
            this.columnPrefixesBox.Name = "columnPrefixesBox";
            this.columnPrefixesBox.Size = new System.Drawing.Size(290, 20);
            this.columnPrefixesBox.TabIndex = 6;
            this.columnPrefixesBox.TextChanged += new System.EventHandler(this.columnPrefixesBox_TextChanged);
            // 
            // StagingColumnPrefixes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 155);
            this.ControlBox = false;
            this.Controls.Add(this.columnPrefixesBox);
            this.Controls.Add(this.columnPrefixesLbl);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StagingColumnPrefixes";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Staging Column Prefixes";
            this.TopMost = true;
            this.Leave += new System.EventHandler(this.StagingColumnPrefixes_Leave);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StagingColumnPrefixes_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label columnPrefixesLbl;
        private System.Windows.Forms.TextBox columnPrefixesBox;
    }
}