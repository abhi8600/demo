namespace Performance_Optimizer_Administration
{
    partial class Performance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Performance));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.performanceTabPage = new System.Windows.Forms.TabPage();
            this.goalMeasure = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.referencePopulationBox = new System.Windows.Forms.ComboBox();
            this.classifierSettings = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.perfSearchPattern = new System.Windows.Forms.ComboBox();
            this.label140 = new System.Windows.Forms.Label();
            this.perfSearchPatterns = new System.Windows.Forms.CheckBox();
            this.optBox = new System.Windows.Forms.GroupBox();
            this.minTargetChange = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.targetProb = new System.Windows.Forms.RadioButton();
            this.targetImp = new System.Windows.Forms.RadioButton();
            this.targetProbability = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.dampenFactor = new System.Windows.Forms.TextBox();
            this.label157 = new System.Windows.Forms.Label();
            this.targetAccuracy = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.numIterations = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.minTarget = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.perfOptimize = new System.Windows.Forms.CheckBox();
            this.targetImprovement = new System.Windows.Forms.TextBox();
            this.numOptimizeMeasures = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label175 = new System.Windows.Forms.Label();
            this.perfIterateModelValue = new System.Windows.Forms.TextBox();
            this.label174 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.perfIterateAnalysisValue = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.perfIterateDefaultAttribute = new System.Windows.Forms.ComboBox();
            this.perfIterateDim = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.performanceFilterListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.perfOutputFile = new System.Windows.Forms.TextBox();
            this.perfDropTable = new System.Windows.Forms.CheckBox();
            this.standardRelButton = new System.Windows.Forms.RadioButton();
            this.logRelButton = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.impactPercent = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.perfOutputTable = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.selectedMeasures = new System.Windows.Forms.ListView();
            this.nameHeader = new System.Windows.Forms.ColumnHeader();
            this.catHeader = new System.Windows.Forms.ColumnHeader();
            this.bucketHeader = new System.Windows.Forms.ColumnHeader();
            this.blockHeader = new System.Windows.Forms.ColumnHeader();
            this.coverHeader = new System.Windows.Forms.ColumnHeader();
            this.numCoverHeader = new System.Windows.Forms.ColumnHeader();
            this.baseHeader = new System.Windows.Forms.ColumnHeader();
            this.optHeader = new System.Windows.Forms.ColumnHeader();
            this.setMeasureAttributesButton = new System.Windows.Forms.Button();
            this.Selected = new System.Windows.Forms.GroupBox();
            this.label180 = new System.Windows.Forms.Label();
            this.optimizeMeasure = new System.Windows.Forms.ComboBox();
            this.label171 = new System.Windows.Forms.Label();
            this.baseCaseMeasure = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.numToCover = new System.Windows.Forms.TextBox();
            this.coverGroup = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.blockGroup = new System.Windows.Forms.TextBox();
            this.setPerfAttributes = new System.Windows.Forms.Button();
            this.numPerfBuckets = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.removeBusMeasure = new System.Windows.Forms.Button();
            this.addBusMeasure = new System.Windows.Forms.Button();
            this.availableMeasures = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl.SuspendLayout();
            this.performanceTabPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.optBox.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.Selected.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.performanceTabPage);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 711);
            this.tabControl.TabIndex = 0;
            // 
            // performanceTabPage
            // 
            this.performanceTabPage.AutoScroll = true;
            this.performanceTabPage.Controls.Add(this.goalMeasure);
            this.performanceTabPage.Controls.Add(this.label5);
            this.performanceTabPage.Controls.Add(this.label4);
            this.performanceTabPage.Controls.Add(this.referencePopulationBox);
            this.performanceTabPage.Controls.Add(this.classifierSettings);
            this.performanceTabPage.Controls.Add(this.groupBox2);
            this.performanceTabPage.Controls.Add(this.optBox);
            this.performanceTabPage.Controls.Add(this.groupBox21);
            this.performanceTabPage.Controls.Add(this.groupBox18);
            this.performanceTabPage.Controls.Add(this.groupBox15);
            this.performanceTabPage.Controls.Add(this.groupBox6);
            this.performanceTabPage.Location = new System.Drawing.Point(4, 22);
            this.performanceTabPage.Name = "performanceTabPage";
            this.performanceTabPage.Size = new System.Drawing.Size(844, 685);
            this.performanceTabPage.TabIndex = 6;
            this.performanceTabPage.Text = "Performance";
            this.performanceTabPage.UseVisualStyleBackColor = true;
            // 
            // goalMeasure
            // 
            this.goalMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.goalMeasure.Location = new System.Drawing.Point(600, 78);
            this.goalMeasure.Name = "goalMeasure";
            this.goalMeasure.Size = new System.Drawing.Size(200, 21);
            this.goalMeasure.TabIndex = 77;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(422, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 23);
            this.label5.TabIndex = 76;
            this.label5.Text = "Optimization Goal";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(422, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 23);
            this.label4.TabIndex = 75;
            this.label4.Text = "Reference Population";
            // 
            // referencePopulationBox
            // 
            this.referencePopulationBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.referencePopulationBox.Location = new System.Drawing.Point(600, 45);
            this.referencePopulationBox.Name = "referencePopulationBox";
            this.referencePopulationBox.Size = new System.Drawing.Size(200, 21);
            this.referencePopulationBox.TabIndex = 75;
            this.toolTip.SetToolTip(this.referencePopulationBox, "Reference Population");
            // 
            // classifierSettings
            // 
            this.classifierSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.classifierSettings.Location = new System.Drawing.Point(425, 118);
            this.classifierSettings.Name = "classifierSettings";
            this.classifierSettings.Size = new System.Drawing.Size(111, 23);
            this.classifierSettings.TabIndex = 51;
            this.classifierSettings.Text = "Classifier Settings";
            this.classifierSettings.UseVisualStyleBackColor = true;
            this.classifierSettings.Click += new System.EventHandler(this.classifierSettings_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.perfSearchPattern);
            this.groupBox2.Controls.Add(this.label140);
            this.groupBox2.Controls.Add(this.perfSearchPatterns);
            this.groupBox2.Location = new System.Drawing.Point(624, 531);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(216, 141);
            this.groupBox2.TabIndex = 52;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Related Success Patterns";
            this.toolTip.SetToolTip(this.groupBox2, "Related Success Patterns can be found for each metric generated during optimizati" +
                    "on. Each pattern will be the most significant one for that metric (or category o" +
                    "f metric)");
            // 
            // perfSearchPattern
            // 
            this.perfSearchPattern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.perfSearchPattern.Location = new System.Drawing.Point(8, 64);
            this.perfSearchPattern.Name = "perfSearchPattern";
            this.perfSearchPattern.Size = new System.Drawing.Size(200, 21);
            this.perfSearchPattern.TabIndex = 0;
            this.toolTip.SetToolTip(this.perfSearchPattern, "Pattern search definition to use - each optimized measure is passed to this searc" +
                    "h definition in order to find the best patterns associated with that measure.");
            // 
            // label140
            // 
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(8, 40);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(168, 23);
            this.label140.TabIndex = 74;
            this.label140.Text = "Pattern Search Definition";
            // 
            // perfSearchPatterns
            // 
            this.perfSearchPatterns.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfSearchPatterns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.perfSearchPatterns.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.perfSearchPatterns.Location = new System.Drawing.Point(8, 16);
            this.perfSearchPatterns.Name = "perfSearchPatterns";
            this.perfSearchPatterns.Size = new System.Drawing.Size(204, 24);
            this.perfSearchPatterns.TabIndex = 1;
            this.perfSearchPatterns.Text = "Find Related Success Patterns";
            // 
            // optBox
            // 
            this.optBox.Controls.Add(this.minTargetChange);
            this.optBox.Controls.Add(this.label47);
            this.optBox.Controls.Add(this.targetProb);
            this.optBox.Controls.Add(this.targetImp);
            this.optBox.Controls.Add(this.targetProbability);
            this.optBox.Controls.Add(this.label172);
            this.optBox.Controls.Add(this.dampenFactor);
            this.optBox.Controls.Add(this.label157);
            this.optBox.Controls.Add(this.targetAccuracy);
            this.optBox.Controls.Add(this.label156);
            this.optBox.Controls.Add(this.numIterations);
            this.optBox.Controls.Add(this.label155);
            this.optBox.Controls.Add(this.minTarget);
            this.optBox.Controls.Add(this.label148);
            this.optBox.Controls.Add(this.perfOptimize);
            this.optBox.Controls.Add(this.targetImprovement);
            this.optBox.Controls.Add(this.numOptimizeMeasures);
            this.optBox.Controls.Add(this.label46);
            this.optBox.Location = new System.Drawing.Point(624, 264);
            this.optBox.Name = "optBox";
            this.optBox.Size = new System.Drawing.Size(216, 261);
            this.optBox.TabIndex = 51;
            this.optBox.TabStop = false;
            this.optBox.Text = "Optimize";
            // 
            // minTargetChange
            // 
            this.minTargetChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minTargetChange.Location = new System.Drawing.Point(160, 231);
            this.minTargetChange.Name = "minTargetChange";
            this.minTargetChange.Size = new System.Drawing.Size(48, 20);
            this.minTargetChange.TabIndex = 8;
            this.minTargetChange.Text = "0";
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(8, 231);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(144, 16);
            this.label47.TabIndex = 61;
            this.label47.Text = "Min % Target Change:";
            this.toolTip.SetToolTip(this.label47, resources.GetString("label47.ToolTip"));
            // 
            // targetProb
            // 
            this.targetProb.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetProb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.targetProb.Location = new System.Drawing.Point(16, 104);
            this.targetProb.Name = "targetProb";
            this.targetProb.Size = new System.Drawing.Size(120, 20);
            this.targetProb.TabIndex = 60;
            this.targetProb.Text = "% Probability:";
            this.toolTip.SetToolTip(this.targetProb, "Seek an improvement with a given estimated probability. The higher the probabilit" +
                    "y the lower the target goal.");
            // 
            // targetImp
            // 
            this.targetImp.Checked = true;
            this.targetImp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetImp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.targetImp.Location = new System.Drawing.Point(16, 80);
            this.targetImp.Name = "targetImp";
            this.targetImp.Size = new System.Drawing.Size(136, 20);
            this.targetImp.TabIndex = 59;
            this.targetImp.TabStop = true;
            this.targetImp.Text = "% Improvement:";
            this.toolTip.SetToolTip(this.targetImp, "Goal % improvement in target measure");
            // 
            // targetProbability
            // 
            this.targetProbability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetProbability.Location = new System.Drawing.Point(160, 104);
            this.targetProbability.Name = "targetProbability";
            this.targetProbability.Size = new System.Drawing.Size(48, 20);
            this.targetProbability.TabIndex = 2;
            this.targetProbability.Text = "50";
            // 
            // label172
            // 
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.Location = new System.Drawing.Point(8, 64);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(64, 16);
            this.label172.TabIndex = 56;
            this.label172.Text = "Target:";
            // 
            // dampenFactor
            // 
            this.dampenFactor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dampenFactor.Location = new System.Drawing.Point(160, 207);
            this.dampenFactor.Name = "dampenFactor";
            this.dampenFactor.Size = new System.Drawing.Size(48, 20);
            this.dampenFactor.TabIndex = 7;
            this.dampenFactor.Text = "0";
            // 
            // label157
            // 
            this.label157.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(8, 207);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(144, 16);
            this.label157.TabIndex = 54;
            this.label157.Text = "Dampening Factor";
            this.toolTip.SetToolTip(this.label157, resources.GetString("label157.ToolTip"));
            // 
            // targetAccuracy
            // 
            this.targetAccuracy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetAccuracy.Location = new System.Drawing.Point(160, 183);
            this.targetAccuracy.Name = "targetAccuracy";
            this.targetAccuracy.Size = new System.Drawing.Size(48, 20);
            this.targetAccuracy.TabIndex = 53;
            this.targetAccuracy.Text = "0";
            // 
            // label156
            // 
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(8, 183);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(144, 16);
            this.label156.TabIndex = 52;
            this.label156.Text = "Target Accuracy";
            this.toolTip.SetToolTip(this.label156, resources.GetString("label156.ToolTip"));
            // 
            // numIterations
            // 
            this.numIterations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numIterations.Location = new System.Drawing.Point(160, 159);
            this.numIterations.Name = "numIterations";
            this.numIterations.Size = new System.Drawing.Size(48, 20);
            this.numIterations.TabIndex = 6;
            this.numIterations.Text = "0";
            // 
            // label155
            // 
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(8, 159);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(144, 16);
            this.label155.TabIndex = 50;
            this.label155.Text = "Number of Iterations";
            this.toolTip.SetToolTip(this.label155, resources.GetString("label155.ToolTip"));
            // 
            // minTarget
            // 
            this.minTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minTarget.Location = new System.Drawing.Point(160, 136);
            this.minTarget.Name = "minTarget";
            this.minTarget.Size = new System.Drawing.Size(48, 20);
            this.minTarget.TabIndex = 3;
            this.minTarget.Text = "0";
            // 
            // label148
            // 
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.Location = new System.Drawing.Point(8, 136);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(144, 16);
            this.label148.TabIndex = 48;
            this.label148.Text = "Min Target Value:";
            this.toolTip.SetToolTip(this.label148, "Minimum absolute target value");
            // 
            // perfOptimize
            // 
            this.perfOptimize.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfOptimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.perfOptimize.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.perfOptimize.Location = new System.Drawing.Point(8, 16);
            this.perfOptimize.Name = "perfOptimize";
            this.perfOptimize.Size = new System.Drawing.Size(200, 21);
            this.perfOptimize.TabIndex = 9;
            this.perfOptimize.Text = "Generate Optimizations";
            this.toolTip.SetToolTip(this.perfOptimize, "Generate an optimized list of measures to achieve a given change in the target me" +
                    "asure");
            // 
            // targetImprovement
            // 
            this.targetImprovement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetImprovement.Location = new System.Drawing.Point(160, 80);
            this.targetImprovement.Name = "targetImprovement";
            this.targetImprovement.Size = new System.Drawing.Size(48, 20);
            this.targetImprovement.TabIndex = 1;
            this.targetImprovement.Text = "25";
            // 
            // numOptimizeMeasures
            // 
            this.numOptimizeMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numOptimizeMeasures.Location = new System.Drawing.Point(160, 40);
            this.numOptimizeMeasures.Name = "numOptimizeMeasures";
            this.numOptimizeMeasures.Size = new System.Drawing.Size(48, 20);
            this.numOptimizeMeasures.TabIndex = 0;
            this.numOptimizeMeasures.Text = "6";
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(8, 40);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(160, 16);
            this.label46.TabIndex = 42;
            this.label46.Text = "# Measures To Optimize:";
            this.toolTip.SetToolTip(this.label46, "The number of performance measures allowed to change to reach the target value");
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.label175);
            this.groupBox21.Controls.Add(this.perfIterateModelValue);
            this.groupBox21.Controls.Add(this.label174);
            this.groupBox21.Controls.Add(this.label45);
            this.groupBox21.Controls.Add(this.perfIterateAnalysisValue);
            this.groupBox21.Controls.Add(this.label118);
            this.groupBox21.Controls.Add(this.label117);
            this.groupBox21.Controls.Add(this.perfIterateDefaultAttribute);
            this.groupBox21.Controls.Add(this.perfIterateDim);
            this.groupBox21.Controls.Add(this.label116);
            this.groupBox21.Location = new System.Drawing.Point(8, 160);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(400, 104);
            this.groupBox21.TabIndex = 50;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Iterate Samples of Target Level Over Other Attribute";
            // 
            // label175
            // 
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.Location = new System.Drawing.Point(288, 80);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(104, 16);
            this.label175.TabIndex = 42;
            this.label175.Text = "(leave blank for all)";
            // 
            // perfIterateModelValue
            // 
            this.perfIterateModelValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perfIterateModelValue.Location = new System.Drawing.Point(288, 32);
            this.perfIterateModelValue.Name = "perfIterateModelValue";
            this.perfIterateModelValue.Size = new System.Drawing.Size(104, 20);
            this.perfIterateModelValue.TabIndex = 1;
            // 
            // label174
            // 
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.Location = new System.Drawing.Point(232, 56);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(56, 16);
            this.label174.TabIndex = 40;
            this.label174.Text = "Score";
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(224, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(72, 16);
            this.label45.TabIndex = 39;
            this.label45.Text = "Values To:";
            // 
            // perfIterateAnalysisValue
            // 
            this.perfIterateAnalysisValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perfIterateAnalysisValue.Location = new System.Drawing.Point(288, 56);
            this.perfIterateAnalysisValue.Name = "perfIterateAnalysisValue";
            this.perfIterateAnalysisValue.Size = new System.Drawing.Size(104, 20);
            this.perfIterateAnalysisValue.TabIndex = 3;
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.Location = new System.Drawing.Point(232, 32);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(56, 16);
            this.label118.TabIndex = 37;
            this.label118.Text = "Model";
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(8, 32);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(72, 16);
            this.label117.TabIndex = 32;
            this.label117.Text = "Dimension";
            // 
            // perfIterateDefaultAttribute
            // 
            this.perfIterateDefaultAttribute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfIterateDefaultAttribute.Location = new System.Drawing.Point(80, 56);
            this.perfIterateDefaultAttribute.Name = "perfIterateDefaultAttribute";
            this.perfIterateDefaultAttribute.Size = new System.Drawing.Size(136, 21);
            this.perfIterateDefaultAttribute.TabIndex = 2;
            // 
            // perfIterateDim
            // 
            this.perfIterateDim.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfIterateDim.Location = new System.Drawing.Point(80, 32);
            this.perfIterateDim.Name = "perfIterateDim";
            this.perfIterateDim.Size = new System.Drawing.Size(136, 21);
            this.perfIterateDim.TabIndex = 0;
            // 
            // label116
            // 
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(8, 56);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(72, 16);
            this.label116.TabIndex = 33;
            this.label116.Text = "Attribute";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label9);
            this.groupBox18.Controls.Add(this.performanceFilterListBox);
            this.groupBox18.Location = new System.Drawing.Point(416, 160);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(424, 104);
            this.groupBox18.TabIndex = 49;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Filters";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(280, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 56);
            this.label9.TabIndex = 42;
            this.label9.Text = "Note: Filter is usually a period filter";
            // 
            // performanceFilterListBox
            // 
            this.performanceFilterListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.performanceFilterListBox.CheckOnClick = true;
            this.performanceFilterListBox.Location = new System.Drawing.Point(16, 16);
            this.performanceFilterListBox.Name = "performanceFilterListBox";
            this.performanceFilterListBox.Size = new System.Drawing.Size(256, 77);
            this.performanceFilterListBox.TabIndex = 43;
            this.performanceFilterListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Performance_MouseMove);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label3);
            this.groupBox15.Controls.Add(this.perfOutputFile);
            this.groupBox15.Controls.Add(this.perfDropTable);
            this.groupBox15.Controls.Add(this.standardRelButton);
            this.groupBox15.Controls.Add(this.logRelButton);
            this.groupBox15.Controls.Add(this.label11);
            this.groupBox15.Controls.Add(this.label16);
            this.groupBox15.Controls.Add(this.impactPercent);
            this.groupBox15.Controls.Add(this.label15);
            this.groupBox15.Controls.Add(this.perfOutputTable);
            this.groupBox15.Location = new System.Drawing.Point(8, 32);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(400, 128);
            this.groupBox15.TabIndex = 37;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Performance Analysis Properties";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 23);
            this.label3.TabIndex = 48;
            this.label3.Text = "Output File Name";
            // 
            // perfOutputFile
            // 
            this.perfOutputFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perfOutputFile.Location = new System.Drawing.Point(136, 20);
            this.perfOutputFile.Name = "perfOutputFile";
            this.perfOutputFile.Size = new System.Drawing.Size(140, 20);
            this.perfOutputFile.TabIndex = 47;
            // 
            // perfDropTable
            // 
            this.perfDropTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfDropTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.perfDropTable.Location = new System.Drawing.Point(280, 47);
            this.perfDropTable.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.perfDropTable.Name = "perfDropTable";
            this.perfDropTable.Size = new System.Drawing.Size(118, 24);
            this.perfDropTable.TabIndex = 2;
            this.perfDropTable.Text = "Drop Table";
            // 
            // standardRelButton
            // 
            this.standardRelButton.Checked = true;
            this.standardRelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.standardRelButton.Location = new System.Drawing.Point(144, 95);
            this.standardRelButton.Name = "standardRelButton";
            this.standardRelButton.Size = new System.Drawing.Size(136, 24);
            this.standardRelButton.TabIndex = 3;
            this.standardRelButton.TabStop = true;
            this.standardRelButton.Text = "Standard Relationship";
            // 
            // logRelButton
            // 
            this.logRelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.logRelButton.Location = new System.Drawing.Point(16, 95);
            this.logRelButton.Name = "logRelButton";
            this.logRelButton.Size = new System.Drawing.Size(112, 24);
            this.logRelButton.TabIndex = 32;
            this.logRelButton.Text = "Log Relationship";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 23);
            this.label11.TabIndex = 15;
            this.label11.Text = "Output Table Name";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(184, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 23);
            this.label16.TabIndex = 31;
            this.label16.Text = "%";
            // 
            // impactPercent
            // 
            this.impactPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.impactPercent.Location = new System.Drawing.Point(136, 71);
            this.impactPercent.Name = "impactPercent";
            this.impactPercent.Size = new System.Drawing.Size(48, 20);
            this.impactPercent.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 23);
            this.label15.TabIndex = 29;
            this.label15.Text = "Metric Impact Test";
            // 
            // perfOutputTable
            // 
            this.perfOutputTable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perfOutputTable.Location = new System.Drawing.Point(136, 47);
            this.perfOutputTable.Name = "perfOutputTable";
            this.perfOutputTable.Size = new System.Drawing.Size(140, 20);
            this.perfOutputTable.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.selectedMeasures);
            this.groupBox6.Controls.Add(this.setMeasureAttributesButton);
            this.groupBox6.Controls.Add(this.Selected);
            this.groupBox6.Controls.Add(this.addBusMeasure);
            this.groupBox6.Controls.Add(this.availableMeasures);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(8, 264);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(608, 408);
            this.groupBox6.TabIndex = 36;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Selected Business Measures to Analyze Against Performance";
            // 
            // selectedMeasures
            // 
            this.selectedMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectedMeasures.CheckBoxes = true;
            this.selectedMeasures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.catHeader,
            this.bucketHeader,
            this.blockHeader,
            this.coverHeader,
            this.numCoverHeader,
            this.baseHeader,
            this.optHeader});
            this.selectedMeasures.FullRowSelect = true;
            this.selectedMeasures.GridLines = true;
            this.selectedMeasures.HideSelection = false;
            this.selectedMeasures.Location = new System.Drawing.Point(8, 219);
            this.selectedMeasures.Name = "selectedMeasures";
            this.selectedMeasures.ShowItemToolTips = true;
            this.selectedMeasures.Size = new System.Drawing.Size(592, 176);
            this.selectedMeasures.TabIndex = 33;
            this.selectedMeasures.UseCompatibleStateImageBehavior = false;
            this.selectedMeasures.View = System.Windows.Forms.View.Details;
            this.selectedMeasures.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.selectedMeasures_ColumnClick);
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "(Model) Measure";
            this.nameHeader.Width = 175;
            // 
            // catHeader
            // 
            this.catHeader.Text = "Category";
            this.catHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.catHeader.Width = 120;
            // 
            // bucketHeader
            // 
            this.bucketHeader.Text = "#Buckets";
            this.bucketHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bucketHeader.Width = 75;
            // 
            // blockHeader
            // 
            this.blockHeader.Text = "Block Group";
            this.blockHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.blockHeader.Width = 90;
            // 
            // coverHeader
            // 
            this.coverHeader.Text = "Cover Group";
            this.coverHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.coverHeader.Width = 90;
            // 
            // numCoverHeader
            // 
            this.numCoverHeader.Text = "# Coverage";
            this.numCoverHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numCoverHeader.Width = 75;
            // 
            // baseHeader
            // 
            this.baseHeader.Text = "Base Case";
            this.baseHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.baseHeader.Width = 75;
            // 
            // optHeader
            // 
            this.optHeader.Text = "Optimize";
            this.optHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.optHeader.Width = 75;
            // 
            // setMeasureAttributesButton
            // 
            this.setMeasureAttributesButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setMeasureAttributesButton.Location = new System.Drawing.Point(472, 32);
            this.setMeasureAttributesButton.Name = "setMeasureAttributesButton";
            this.setMeasureAttributesButton.Size = new System.Drawing.Size(128, 24);
            this.setMeasureAttributesButton.TabIndex = 1;
            this.setMeasureAttributesButton.Text = "Set Measure Attributes";
            this.setMeasureAttributesButton.Click += new System.EventHandler(this.setMeasureAttributesButton_Click);
            // 
            // Selected
            // 
            this.Selected.Controls.Add(this.label180);
            this.Selected.Controls.Add(this.optimizeMeasure);
            this.Selected.Controls.Add(this.label171);
            this.Selected.Controls.Add(this.baseCaseMeasure);
            this.Selected.Controls.Add(this.label54);
            this.Selected.Controls.Add(this.numToCover);
            this.Selected.Controls.Add(this.coverGroup);
            this.Selected.Controls.Add(this.label52);
            this.Selected.Controls.Add(this.label49);
            this.Selected.Controls.Add(this.blockGroup);
            this.Selected.Controls.Add(this.setPerfAttributes);
            this.Selected.Controls.Add(this.numPerfBuckets);
            this.Selected.Controls.Add(this.label132);
            this.Selected.Controls.Add(this.removeBusMeasure);
            this.Selected.Location = new System.Drawing.Point(288, 64);
            this.Selected.Name = "Selected";
            this.Selected.Size = new System.Drawing.Size(312, 144);
            this.Selected.TabIndex = 44;
            this.Selected.TabStop = false;
            this.Selected.Text = "Modify Selected Measures";
            // 
            // label180
            // 
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.Location = new System.Drawing.Point(8, 62);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(72, 16);
            this.label180.TabIndex = 54;
            this.label180.Text = "Optimize";
            // 
            // optimizeMeasure
            // 
            this.optimizeMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.optimizeMeasure.Items.AddRange(new object[] {
            "True",
            "False"});
            this.optimizeMeasure.Location = new System.Drawing.Point(80, 62);
            this.optimizeMeasure.Name = "optimizeMeasure";
            this.optimizeMeasure.Size = new System.Drawing.Size(72, 21);
            this.optimizeMeasure.TabIndex = 2;
            // 
            // label171
            // 
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.Location = new System.Drawing.Point(8, 40);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(72, 16);
            this.label171.TabIndex = 52;
            this.label171.Text = "Base Case";
            // 
            // baseCaseMeasure
            // 
            this.baseCaseMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.baseCaseMeasure.Items.AddRange(new object[] {
            "True",
            "False"});
            this.baseCaseMeasure.Location = new System.Drawing.Point(80, 40);
            this.baseCaseMeasure.Name = "baseCaseMeasure";
            this.baseCaseMeasure.Size = new System.Drawing.Size(72, 21);
            this.baseCaseMeasure.TabIndex = 1;
            // 
            // label54
            // 
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(160, 64);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(72, 16);
            this.label54.TabIndex = 50;
            this.label54.Text = "# to Cover";
            // 
            // numToCover
            // 
            this.numToCover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numToCover.Location = new System.Drawing.Point(240, 64);
            this.numToCover.Name = "numToCover";
            this.numToCover.Size = new System.Drawing.Size(64, 20);
            this.numToCover.TabIndex = 5;
            // 
            // coverGroup
            // 
            this.coverGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.coverGroup.Location = new System.Drawing.Point(240, 40);
            this.coverGroup.Name = "coverGroup";
            this.coverGroup.Size = new System.Drawing.Size(64, 20);
            this.coverGroup.TabIndex = 4;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(160, 40);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(88, 16);
            this.label52.TabIndex = 46;
            this.label52.Text = "Cover Group";
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(160, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 16);
            this.label49.TabIndex = 45;
            this.label49.Text = "Block Group";
            // 
            // blockGroup
            // 
            this.blockGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blockGroup.Location = new System.Drawing.Point(240, 16);
            this.blockGroup.Name = "blockGroup";
            this.blockGroup.Size = new System.Drawing.Size(64, 20);
            this.blockGroup.TabIndex = 3;
            // 
            // setPerfAttributes
            // 
            this.setPerfAttributes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setPerfAttributes.Location = new System.Drawing.Point(94, 95);
            this.setPerfAttributes.Name = "setPerfAttributes";
            this.setPerfAttributes.Size = new System.Drawing.Size(64, 41);
            this.setPerfAttributes.TabIndex = 43;
            this.setPerfAttributes.Text = "Set Attributes";
            this.setPerfAttributes.Click += new System.EventHandler(this.setMeasureAttributesButton_Click);
            // 
            // numPerfBuckets
            // 
            this.numPerfBuckets.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numPerfBuckets.Location = new System.Drawing.Point(80, 16);
            this.numPerfBuckets.Name = "numPerfBuckets";
            this.numPerfBuckets.Size = new System.Drawing.Size(48, 20);
            this.numPerfBuckets.TabIndex = 0;
            this.numPerfBuckets.Text = "0";
            // 
            // label132
            // 
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(8, 16);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(80, 16);
            this.label132.TabIndex = 40;
            this.label132.Text = "# Buckets:";
            // 
            // removeBusMeasure
            // 
            this.removeBusMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeBusMeasure.Location = new System.Drawing.Point(8, 95);
            this.removeBusMeasure.Name = "removeBusMeasure";
            this.removeBusMeasure.Size = new System.Drawing.Size(80, 41);
            this.removeBusMeasure.TabIndex = 26;
            this.removeBusMeasure.Text = "Remove Measures";
            this.removeBusMeasure.Click += new System.EventHandler(this.removeBusMeasure_Click);
            // 
            // addBusMeasure
            // 
            this.addBusMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addBusMeasure.Location = new System.Drawing.Point(288, 32);
            this.addBusMeasure.Name = "addBusMeasure";
            this.addBusMeasure.Size = new System.Drawing.Size(120, 24);
            this.addBusMeasure.TabIndex = 0;
            this.addBusMeasure.Text = "Add Measure To List";
            this.addBusMeasure.Click += new System.EventHandler(this.addBusMeasure_Click);
            // 
            // availableMeasures
            // 
            this.availableMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availableMeasures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.availableMeasures.FullRowSelect = true;
            this.availableMeasures.GridLines = true;
            this.availableMeasures.HideSelection = false;
            this.availableMeasures.Location = new System.Drawing.Point(8, 32);
            this.availableMeasures.Name = "availableMeasures";
            this.availableMeasures.ShowItemToolTips = true;
            this.availableMeasures.Size = new System.Drawing.Size(274, 160);
            this.availableMeasures.TabIndex = 32;
            this.availableMeasures.UseCompatibleStateImageBehavior = false;
            this.availableMeasures.View = System.Windows.Forms.View.Details;
            this.availableMeasures.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.availableMeasures_ColumnClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Measure";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Category";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 105;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(8, 200);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(208, 16);
            this.label23.TabIndex = 24;
            this.label23.Text = "Performance Business Measures";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(8, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(192, 16);
            this.label24.TabIndex = 23;
            this.label24.Text = "Available Business Measures";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // Performance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(852, 711);
            this.Controls.Add(this.tabControl);
            this.Name = "Performance";
            this.Text = "Performance";
            this.tabControl.ResumeLayout(false);
            this.performanceTabPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.optBox.ResumeLayout(false);
            this.optBox.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.Selected.ResumeLayout(false);
            this.Selected.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage performanceTabPage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox perfSearchPattern;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.CheckBox perfSearchPatterns;
        private System.Windows.Forms.GroupBox optBox;
        private System.Windows.Forms.TextBox minTargetChange;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.RadioButton targetProb;
        private System.Windows.Forms.RadioButton targetImp;
        private System.Windows.Forms.TextBox targetProbability;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.TextBox dampenFactor;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TextBox targetAccuracy;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.TextBox numIterations;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.TextBox minTarget;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.CheckBox perfOptimize;
        private System.Windows.Forms.TextBox targetImprovement;
        private System.Windows.Forms.TextBox numOptimizeMeasures;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.TextBox perfIterateModelValue;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox perfIterateAnalysisValue;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ComboBox perfIterateDefaultAttribute;
        private System.Windows.Forms.ComboBox perfIterateDim;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox performanceFilterListBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.CheckBox perfDropTable;
        private System.Windows.Forms.RadioButton standardRelButton;
        private System.Windows.Forms.RadioButton logRelButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox impactPercent;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox perfOutputTable;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button setMeasureAttributesButton;
        private System.Windows.Forms.GroupBox Selected;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.ComboBox optimizeMeasure;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.ComboBox baseCaseMeasure;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox numToCover;
        private System.Windows.Forms.TextBox coverGroup;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox blockGroup;
        private System.Windows.Forms.Button setPerfAttributes;
        private System.Windows.Forms.TextBox numPerfBuckets;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Button removeBusMeasure;
        private System.Windows.Forms.Button addBusMeasure;
        private System.Windows.Forms.ListView availableMeasures;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader catHeader;
        private System.Windows.Forms.ColumnHeader bucketHeader;
        private System.Windows.Forms.ColumnHeader blockHeader;
        private System.Windows.Forms.ColumnHeader coverHeader;
        private System.Windows.Forms.ColumnHeader numCoverHeader;
        private System.Windows.Forms.ColumnHeader baseHeader;
        private System.Windows.Forms.ColumnHeader optHeader;
        public System.Windows.Forms.ListView selectedMeasures;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button classifierSettings;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox perfOutputFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox referencePopulationBox;
        public System.Windows.Forms.ComboBox goalMeasure;
        private System.Windows.Forms.Label label5;
    }
}