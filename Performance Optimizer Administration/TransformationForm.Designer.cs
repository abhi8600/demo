namespace Performance_Optimizer_Administration
{
    partial class TransformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransformationForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.transformSelectBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.formulaGroup = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.stagingFormula = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.lookupBox = new System.Windows.Forms.GroupBox();
            this.joinExpressionBox = new System.Windows.Forms.TextBox();
            this.lookupExpressionBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lookupTableBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.procedureBox = new System.Windows.Forms.GroupBox();
            this.procNameBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.procedureTextBox = new System.Windows.Forms.TextBox();
            this.householdBox = new System.Windows.Forms.GroupBox();
            this.targetKeyBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.keyColBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.TargetZipBox = new System.Windows.Forms.TextBox();
            this.TargetStateBox = new System.Windows.Forms.TextBox();
            this.TargetCityBox = new System.Windows.Forms.TextBox();
            this.TargetAddressBox = new System.Windows.Forms.TextBox();
            this.TargetHHIDBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TargetTableBox = new System.Windows.Forms.TextBox();
            this.ZipBox = new System.Windows.Forms.TextBox();
            this.StateBox = new System.Windows.Forms.TextBox();
            this.CityBox = new System.Windows.Forms.TextBox();
            this.AddressBox = new System.Windows.Forms.TextBox();
            this.HHIDBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rankBox = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.orderByGrid = new System.Windows.Forms.DataGridView();
            this.columnDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ascending = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.transformSet = new System.Data.DataSet();
            this.partitionTable = new System.Data.DataTable();
            this.dataColumn2 = new System.Data.DataColumn();
            this.orderByTable = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.nameValueTable = new System.Data.DataTable();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.label23 = new System.Windows.Forms.Label();
            this.partitionGrid = new System.Windows.Forms.DataGridView();
            this.columnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ntileButton = new System.Windows.Forms.RadioButton();
            this.rankButton = new System.Windows.Forms.RadioButton();
            this.ntileNum = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.dateIDBox = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dateTypeBox = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.dateExpressionBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.javaTransformBox = new System.Windows.Forms.GroupBox();
            this.nameValuePairGrid = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.javaClassName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.javaTransformName = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.columnDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ascendingDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.columnLabel = new System.Windows.Forms.Label();
            this.transformationPanel = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.executeAfterSelectBox = new System.Windows.Forms.ComboBox();
            this.formulaGroup.SuspendLayout();
            this.lookupBox.SuspendLayout();
            this.procedureBox.SuspendLayout();
            this.householdBox.SuspendLayout();
            this.rankBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderByGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partitionTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderByTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nameValueTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.partitionGrid)).BeginInit();
            this.dateIDBox.SuspendLayout();
            this.javaTransformBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameValuePairGrid)).BeginInit();
            this.transformationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // transformSelectBox
            // 
            this.transformSelectBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transformSelectBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.transformSelectBox.FormattingEnabled = true;
            this.transformSelectBox.Items.AddRange(new object[] {
            "None",
            "Staging Table Expression",
            "External Table Lookup",
            "Date ID"});
            this.transformSelectBox.Location = new System.Drawing.Point(95, 36);
            this.transformSelectBox.Name = "transformSelectBox";
            this.transformSelectBox.Size = new System.Drawing.Size(184, 21);
            this.transformSelectBox.TabIndex = 0;
            this.transformSelectBox.SelectedIndexChanged += new System.EventHandler(this.transformSelectBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Transformation";
            // 
            // formulaGroup
            // 
            this.formulaGroup.Controls.Add(this.label5);
            this.formulaGroup.Controls.Add(this.stagingFormula);
            this.formulaGroup.Location = new System.Drawing.Point(12, 63);
            this.formulaGroup.Name = "formulaGroup";
            this.formulaGroup.Size = new System.Drawing.Size(970, 582);
            this.formulaGroup.TabIndex = 2;
            this.formulaGroup.TabStop = false;
            this.formulaGroup.Text = "Specify Formula/Expression";
            // 
            // label5
            // 
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(799, 41);
            this.label5.TabIndex = 1;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // stagingFormula
            // 
            this.stagingFormula.AcceptsReturn = true;
            this.stagingFormula.AcceptsTab = true;
            this.stagingFormula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.stagingFormula.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stagingFormula.Location = new System.Drawing.Point(6, 60);
            this.stagingFormula.Multiline = true;
            this.stagingFormula.Name = "stagingFormula";
            this.stagingFormula.Size = new System.Drawing.Size(958, 516);
            this.stagingFormula.TabIndex = 0;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(826, 661);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(907, 661);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // lookupBox
            // 
            this.lookupBox.AutoSize = true;
            this.lookupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.lookupBox.Controls.Add(this.joinExpressionBox);
            this.lookupBox.Controls.Add(this.lookupExpressionBox);
            this.lookupBox.Controls.Add(this.label6);
            this.lookupBox.Controls.Add(this.label4);
            this.lookupBox.Controls.Add(this.lookupTableBox);
            this.lookupBox.Controls.Add(this.label3);
            this.lookupBox.Controls.Add(this.label2);
            this.lookupBox.Location = new System.Drawing.Point(12, 63);
            this.lookupBox.Name = "lookupBox";
            this.lookupBox.Size = new System.Drawing.Size(970, 400);
            this.lookupBox.TabIndex = 5;
            this.lookupBox.TabStop = false;
            this.lookupBox.Text = "External Table Lookup";
            // 
            // joinExpressionBox
            // 
            this.joinExpressionBox.AcceptsReturn = true;
            this.joinExpressionBox.AcceptsTab = true;
            this.joinExpressionBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.joinExpressionBox.Location = new System.Drawing.Point(6, 255);
            this.joinExpressionBox.Multiline = true;
            this.joinExpressionBox.Name = "joinExpressionBox";
            this.joinExpressionBox.Size = new System.Drawing.Size(958, 126);
            this.joinExpressionBox.TabIndex = 6;
            // 
            // lookupExpressionBox
            // 
            this.lookupExpressionBox.AcceptsReturn = true;
            this.lookupExpressionBox.AcceptsTab = true;
            this.lookupExpressionBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupExpressionBox.Location = new System.Drawing.Point(6, 105);
            this.lookupExpressionBox.Multiline = true;
            this.lookupExpressionBox.Name = "lookupExpressionBox";
            this.lookupExpressionBox.Size = new System.Drawing.Size(958, 131);
            this.lookupExpressionBox.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Location = new System.Drawing.Point(3, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(961, 31);
            this.label6.TabIndex = 8;
            this.label6.Text = "References to columns should be fully qualified physical names, e.g. TABLENAME.Co" +
                "lumnName.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Join Expression";
            // 
            // lookupTableBox
            // 
            this.lookupTableBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupTableBox.Location = new System.Drawing.Point(82, 55);
            this.lookupTableBox.Name = "lookupTableBox";
            this.lookupTableBox.Size = new System.Drawing.Size(184, 20);
            this.lookupTableBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lookup Table";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lookup Expression";
            // 
            // procedureBox
            // 
            this.procedureBox.Controls.Add(this.procNameBox);
            this.procedureBox.Controls.Add(this.label8);
            this.procedureBox.Controls.Add(this.label7);
            this.procedureBox.Controls.Add(this.procedureTextBox);
            this.procedureBox.Location = new System.Drawing.Point(12, 63);
            this.procedureBox.Name = "procedureBox";
            this.procedureBox.Size = new System.Drawing.Size(970, 582);
            this.procedureBox.TabIndex = 3;
            this.procedureBox.TabStop = false;
            this.procedureBox.Text = "Procedure";
            // 
            // procNameBox
            // 
            this.procNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.procNameBox.Location = new System.Drawing.Point(106, 13);
            this.procNameBox.Name = "procNameBox";
            this.procNameBox.Size = new System.Drawing.Size(161, 20);
            this.procNameBox.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Location = new System.Drawing.Point(6, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(799, 39);
            this.label8.TabIndex = 2;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Procedure Name";
            // 
            // procedureTextBox
            // 
            this.procedureTextBox.AcceptsReturn = true;
            this.procedureTextBox.AcceptsTab = true;
            this.procedureTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.procedureTextBox.CausesValidation = false;
            this.procedureTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.procedureTextBox.HideSelection = false;
            this.procedureTextBox.Location = new System.Drawing.Point(6, 78);
            this.procedureTextBox.MaxLength = 65535;
            this.procedureTextBox.Multiline = true;
            this.procedureTextBox.Name = "procedureTextBox";
            this.procedureTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.procedureTextBox.Size = new System.Drawing.Size(958, 498);
            this.procedureTextBox.TabIndex = 0;
            this.procedureTextBox.WordWrap = false;
            // 
            // householdBox
            // 
            this.householdBox.Controls.Add(this.targetKeyBox);
            this.householdBox.Controls.Add(this.label22);
            this.householdBox.Controls.Add(this.keyColBox);
            this.householdBox.Controls.Add(this.label21);
            this.householdBox.Controls.Add(this.TargetZipBox);
            this.householdBox.Controls.Add(this.TargetStateBox);
            this.householdBox.Controls.Add(this.TargetCityBox);
            this.householdBox.Controls.Add(this.TargetAddressBox);
            this.householdBox.Controls.Add(this.TargetHHIDBox);
            this.householdBox.Controls.Add(this.label16);
            this.householdBox.Controls.Add(this.label17);
            this.householdBox.Controls.Add(this.label18);
            this.householdBox.Controls.Add(this.label19);
            this.householdBox.Controls.Add(this.label20);
            this.householdBox.Controls.Add(this.TargetTableBox);
            this.householdBox.Controls.Add(this.ZipBox);
            this.householdBox.Controls.Add(this.StateBox);
            this.householdBox.Controls.Add(this.CityBox);
            this.householdBox.Controls.Add(this.AddressBox);
            this.householdBox.Controls.Add(this.HHIDBox);
            this.householdBox.Controls.Add(this.label10);
            this.householdBox.Controls.Add(this.label15);
            this.householdBox.Controls.Add(this.label14);
            this.householdBox.Controls.Add(this.label13);
            this.householdBox.Controls.Add(this.label9);
            this.householdBox.Controls.Add(this.label11);
            this.householdBox.Controls.Add(this.label12);
            this.householdBox.Location = new System.Drawing.Point(12, 63);
            this.householdBox.Name = "householdBox";
            this.householdBox.Size = new System.Drawing.Size(970, 582);
            this.householdBox.TabIndex = 6;
            this.householdBox.TabStop = false;
            this.householdBox.Text = "Householding";
            this.householdBox.Visible = false;
            // 
            // targetKeyBox
            // 
            this.targetKeyBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetKeyBox.Location = new System.Drawing.Point(416, 237);
            this.targetKeyBox.Name = "targetKeyBox";
            this.targetKeyBox.Size = new System.Drawing.Size(148, 20);
            this.targetKeyBox.TabIndex = 32;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(300, 240);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Reference Key Column";
            // 
            // keyColBox
            // 
            this.keyColBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.keyColBox.Location = new System.Drawing.Point(416, 62);
            this.keyColBox.Name = "keyColBox";
            this.keyColBox.Size = new System.Drawing.Size(148, 20);
            this.keyColBox.TabIndex = 30;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(300, 65);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 13);
            this.label21.TabIndex = 29;
            this.label21.Text = "Key Column";
            // 
            // TargetZipBox
            // 
            this.TargetZipBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetZipBox.Location = new System.Drawing.Point(119, 335);
            this.TargetZipBox.Name = "TargetZipBox";
            this.TargetZipBox.Size = new System.Drawing.Size(148, 20);
            this.TargetZipBox.TabIndex = 28;
            // 
            // TargetStateBox
            // 
            this.TargetStateBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetStateBox.Location = new System.Drawing.Point(119, 310);
            this.TargetStateBox.Name = "TargetStateBox";
            this.TargetStateBox.Size = new System.Drawing.Size(148, 20);
            this.TargetStateBox.TabIndex = 27;
            // 
            // TargetCityBox
            // 
            this.TargetCityBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetCityBox.Location = new System.Drawing.Point(119, 285);
            this.TargetCityBox.Name = "TargetCityBox";
            this.TargetCityBox.Size = new System.Drawing.Size(148, 20);
            this.TargetCityBox.TabIndex = 26;
            // 
            // TargetAddressBox
            // 
            this.TargetAddressBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetAddressBox.Location = new System.Drawing.Point(119, 260);
            this.TargetAddressBox.Name = "TargetAddressBox";
            this.TargetAddressBox.Size = new System.Drawing.Size(148, 20);
            this.TargetAddressBox.TabIndex = 25;
            // 
            // TargetHHIDBox
            // 
            this.TargetHHIDBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetHHIDBox.Location = new System.Drawing.Point(119, 236);
            this.TargetHHIDBox.Name = "TargetHHIDBox";
            this.TargetHHIDBox.Size = new System.Drawing.Size(148, 20);
            this.TargetHHIDBox.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 313);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "State";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 338);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Zip";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 288);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "City";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(3, 239);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Household ID Column";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 263);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "Address";
            // 
            // TargetTableBox
            // 
            this.TargetTableBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TargetTableBox.Location = new System.Drawing.Point(119, 205);
            this.TargetTableBox.Name = "TargetTableBox";
            this.TargetTableBox.Size = new System.Drawing.Size(148, 20);
            this.TargetTableBox.TabIndex = 18;
            // 
            // ZipBox
            // 
            this.ZipBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ZipBox.Location = new System.Drawing.Point(119, 161);
            this.ZipBox.Name = "ZipBox";
            this.ZipBox.Size = new System.Drawing.Size(148, 20);
            this.ZipBox.TabIndex = 17;
            // 
            // StateBox
            // 
            this.StateBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StateBox.Location = new System.Drawing.Point(119, 136);
            this.StateBox.Name = "StateBox";
            this.StateBox.Size = new System.Drawing.Size(148, 20);
            this.StateBox.TabIndex = 16;
            // 
            // CityBox
            // 
            this.CityBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CityBox.Location = new System.Drawing.Point(119, 111);
            this.CityBox.Name = "CityBox";
            this.CityBox.Size = new System.Drawing.Size(148, 20);
            this.CityBox.TabIndex = 15;
            // 
            // AddressBox
            // 
            this.AddressBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AddressBox.Location = new System.Drawing.Point(119, 86);
            this.AddressBox.Name = "AddressBox";
            this.AddressBox.Size = new System.Drawing.Size(148, 20);
            this.AddressBox.TabIndex = 14;
            // 
            // HHIDBox
            // 
            this.HHIDBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HHIDBox.Location = new System.Drawing.Point(119, 62);
            this.HHIDBox.Name = "HHIDBox";
            this.HHIDBox.Size = new System.Drawing.Size(148, 20);
            this.HHIDBox.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 208);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Reference Table";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 139);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "State";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Zip";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "City";
            // 
            // label9
            // 
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Location = new System.Drawing.Point(3, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(961, 26);
            this.label9.TabIndex = 8;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Household ID Column";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Address";
            // 
            // rankBox
            // 
            this.rankBox.Controls.Add(this.label24);
            this.rankBox.Controls.Add(this.orderByGrid);
            this.rankBox.Controls.Add(this.label23);
            this.rankBox.Controls.Add(this.partitionGrid);
            this.rankBox.Controls.Add(this.ntileButton);
            this.rankBox.Controls.Add(this.rankButton);
            this.rankBox.Controls.Add(this.ntileNum);
            this.rankBox.Controls.Add(this.label34);
            this.rankBox.Location = new System.Drawing.Point(12, 63);
            this.rankBox.Name = "rankBox";
            this.rankBox.Size = new System.Drawing.Size(970, 582);
            this.rankBox.TabIndex = 33;
            this.rankBox.TabStop = false;
            this.rankBox.Text = "Rank/NTile";
            this.rankBox.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(297, 93);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 13);
            this.label24.TabIndex = 38;
            this.label24.Text = "Order By Columns";
            // 
            // orderByGrid
            // 
            this.orderByGrid.AutoGenerateColumns = false;
            this.orderByGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderByGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnDataGridViewTextBoxColumn1,
            this.Ascending});
            this.orderByGrid.DataMember = "OrderByTable";
            this.orderByGrid.DataSource = this.transformSet;
            this.orderByGrid.Location = new System.Drawing.Point(300, 112);
            this.orderByGrid.Name = "orderByGrid";
            this.orderByGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.orderByGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.orderByGrid.Size = new System.Drawing.Size(277, 150);
            this.orderByGrid.TabIndex = 37;
            // 
            // columnDataGridViewTextBoxColumn1
            // 
            this.columnDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnDataGridViewTextBoxColumn1.DataPropertyName = "Column";
            this.columnDataGridViewTextBoxColumn1.HeaderText = "Column";
            this.columnDataGridViewTextBoxColumn1.Name = "columnDataGridViewTextBoxColumn1";
            // 
            // Ascending
            // 
            this.Ascending.DataPropertyName = "Ascending";
            this.Ascending.HeaderText = "Ascending";
            this.Ascending.Name = "Ascending";
            this.Ascending.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Ascending.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Ascending.Width = 65;
            // 
            // transformSet
            // 
            this.transformSet.DataSetName = "NewDataSet";
            this.transformSet.Tables.AddRange(new System.Data.DataTable[] {
            this.partitionTable,
            this.orderByTable,
            this.nameValueTable});
            // 
            // partitionTable
            // 
            this.partitionTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn2});
            this.partitionTable.TableName = "PartitionTable";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Column";
            // 
            // orderByTable
            // 
            this.orderByTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn3});
            this.orderByTable.TableName = "OrderByTable";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Column";
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "Ascending";
            this.dataColumn3.DataType = typeof(bool);
            this.dataColumn3.DefaultValue = true;
            // 
            // nameValueTable
            // 
            this.nameValueTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn4,
            this.dataColumn5});
            this.nameValueTable.TableName = "NameValueTable";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Name";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Value";
            this.dataColumn5.ColumnName = "Value";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 93);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 13);
            this.label23.TabIndex = 36;
            this.label23.Text = "Partition By Columns";
            // 
            // partitionGrid
            // 
            this.partitionGrid.AutoGenerateColumns = false;
            this.partitionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.partitionGrid.ColumnHeadersVisible = false;
            this.partitionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnDataGridViewTextBoxColumn});
            this.partitionGrid.DataMember = "PartitionTable";
            this.partitionGrid.DataSource = this.transformSet;
            this.partitionGrid.Location = new System.Drawing.Point(10, 112);
            this.partitionGrid.Name = "partitionGrid";
            this.partitionGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.partitionGrid.Size = new System.Drawing.Size(240, 150);
            this.partitionGrid.TabIndex = 35;
            // 
            // columnDataGridViewTextBoxColumn
            // 
            this.columnDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnDataGridViewTextBoxColumn.DataPropertyName = "Column";
            this.columnDataGridViewTextBoxColumn.HeaderText = "Column";
            this.columnDataGridViewTextBoxColumn.Name = "columnDataGridViewTextBoxColumn";
            // 
            // ntileButton
            // 
            this.ntileButton.AutoSize = true;
            this.ntileButton.Location = new System.Drawing.Point(67, 45);
            this.ntileButton.Name = "ntileButton";
            this.ntileButton.Size = new System.Drawing.Size(50, 17);
            this.ntileButton.TabIndex = 34;
            this.ntileButton.Text = "NTile";
            this.ntileButton.UseVisualStyleBackColor = true;
            // 
            // rankButton
            // 
            this.rankButton.AutoSize = true;
            this.rankButton.Checked = true;
            this.rankButton.Location = new System.Drawing.Point(10, 45);
            this.rankButton.Name = "rankButton";
            this.rankButton.Size = new System.Drawing.Size(51, 17);
            this.rankButton.TabIndex = 33;
            this.rankButton.TabStop = true;
            this.rankButton.Text = "Rank";
            this.rankButton.UseVisualStyleBackColor = true;
            // 
            // ntileNum
            // 
            this.ntileNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ntileNum.Location = new System.Drawing.Point(119, 45);
            this.ntileNum.Name = "ntileNum";
            this.ntileNum.Size = new System.Drawing.Size(47, 20);
            this.ntileNum.TabIndex = 13;
            this.ntileNum.Text = "100";
            this.ntileNum.Validating += new System.ComponentModel.CancelEventHandler(this.ntileNum_Validating);
            // 
            // label34
            // 
            this.label34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label34.Location = new System.Drawing.Point(3, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(961, 26);
            this.label34.TabIndex = 8;
            this.label34.Text = "References to other columns in target table or lookup table should be fully quali" +
                "fied (e.g. tablename.\"column name\").";
            // 
            // dateIDBox
            // 
            this.dateIDBox.AutoSize = true;
            this.dateIDBox.Controls.Add(this.label26);
            this.dateIDBox.Controls.Add(this.dateTypeBox);
            this.dateIDBox.Controls.Add(this.label25);
            this.dateIDBox.Controls.Add(this.dateExpressionBox);
            this.dateIDBox.Controls.Add(this.label27);
            this.dateIDBox.Location = new System.Drawing.Point(12, 63);
            this.dateIDBox.Name = "dateIDBox";
            this.dateIDBox.Size = new System.Drawing.Size(970, 249);
            this.dateIDBox.TabIndex = 9;
            this.dateIDBox.TabStop = false;
            this.dateIDBox.Text = "Transform a Date into a Date ID";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 86);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 10;
            this.label26.Text = "Date Type";
            // 
            // dateTypeBox
            // 
            this.dateTypeBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dateTypeBox.FormattingEnabled = true;
            this.dateTypeBox.Items.AddRange(new object[] {
            "Day",
            "Week",
            "Month"});
            this.dateTypeBox.Location = new System.Drawing.Point(90, 81);
            this.dateTypeBox.Name = "dateTypeBox";
            this.dateTypeBox.Size = new System.Drawing.Size(104, 21);
            this.dateTypeBox.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(3, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(961, 31);
            this.label25.TabIndex = 8;
            this.label25.Text = "Enter the date to be transformed and type type of ID it should be transformed to." +
                " This column should be a static value or a variable. This transformation does no" +
                "t apply to underlying columns.";
            // 
            // dateExpressionBox
            // 
            this.dateExpressionBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dateExpressionBox.Location = new System.Drawing.Point(90, 55);
            this.dateExpressionBox.Name = "dateExpressionBox";
            this.dateExpressionBox.Size = new System.Drawing.Size(184, 20);
            this.dateExpressionBox.TabIndex = 4;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(84, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Date Expression";
            // 
            // javaTransformBox
            // 
            this.javaTransformBox.Controls.Add(this.nameValuePairGrid);
            this.javaTransformBox.Controls.Add(this.javaClassName);
            this.javaTransformBox.Controls.Add(this.label29);
            this.javaTransformBox.Controls.Add(this.javaTransformName);
            this.javaTransformBox.Controls.Add(this.label28);
            this.javaTransformBox.Location = new System.Drawing.Point(12, 63);
            this.javaTransformBox.Name = "javaTransformBox";
            this.javaTransformBox.Size = new System.Drawing.Size(970, 582);
            this.javaTransformBox.TabIndex = 34;
            this.javaTransformBox.TabStop = false;
            this.javaTransformBox.Text = "Java Transform";
            // 
            // nameValuePairGrid
            // 
            this.nameValuePairGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.nameValuePairGrid.AutoGenerateColumns = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nameValuePairGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.nameValuePairGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.nameValuePairGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn});
            this.nameValuePairGrid.DataMember = "NameValueTable";
            this.nameValuePairGrid.DataSource = this.transformSet;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.nameValuePairGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.nameValuePairGrid.Location = new System.Drawing.Point(19, 89);
            this.nameValuePairGrid.Name = "nameValuePairGrid";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.nameValuePairGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.nameValuePairGrid.Size = new System.Drawing.Size(945, 487);
            this.nameValuePairGrid.TabIndex = 8;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 300;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.Width = 600;
            // 
            // javaClassName
            // 
            this.javaClassName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.javaClassName.Location = new System.Drawing.Point(116, 52);
            this.javaClassName.Name = "javaClassName";
            this.javaClassName.Size = new System.Drawing.Size(497, 20);
            this.javaClassName.TabIndex = 7;
            // 
            // label29
            // 
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Location = new System.Drawing.Point(16, 55);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(94, 17);
            this.label29.TabIndex = 6;
            this.label29.Text = "Class Name";
            // 
            // javaTransformName
            // 
            this.javaTransformName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.javaTransformName.Location = new System.Drawing.Point(116, 22);
            this.javaTransformName.Name = "javaTransformName";
            this.javaTransformName.Size = new System.Drawing.Size(497, 20);
            this.javaTransformName.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Location = new System.Drawing.Point(16, 25);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 17);
            this.label28.TabIndex = 4;
            this.label28.Text = "Name";
            // 
            // columnDataGridViewTextBoxColumn2
            // 
            this.columnDataGridViewTextBoxColumn2.DataPropertyName = "Column";
            this.columnDataGridViewTextBoxColumn2.HeaderText = "Column";
            this.columnDataGridViewTextBoxColumn2.Name = "columnDataGridViewTextBoxColumn2";
            // 
            // ascendingDataGridViewCheckBoxColumn
            // 
            this.ascendingDataGridViewCheckBoxColumn.DataPropertyName = "Ascending";
            this.ascendingDataGridViewCheckBoxColumn.HeaderText = "Ascending";
            this.ascendingDataGridViewCheckBoxColumn.Name = "ascendingDataGridViewCheckBoxColumn";
            // 
            // columnLabel
            // 
            this.columnLabel.AutoSize = true;
            this.columnLabel.Location = new System.Drawing.Point(9, 9);
            this.columnLabel.Name = "columnLabel";
            this.columnLabel.Size = new System.Drawing.Size(45, 13);
            this.columnLabel.TabIndex = 35;
            this.columnLabel.Text = "Column:";
            this.columnLabel.Visible = false;
            // 
            // transformationPanel
            // 
            this.transformationPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.transformationPanel.Controls.Add(this.label30);
            this.transformationPanel.Controls.Add(this.executeAfterSelectBox);
            this.transformationPanel.Location = new System.Drawing.Point(6, 28);
            this.transformationPanel.Name = "transformationPanel";
            this.transformationPanel.Size = new System.Drawing.Size(985, 627);
            this.transformationPanel.TabIndex = 36;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(344, 10);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Execute After";
            // 
            // executeAfterSelectBox
            // 
            this.executeAfterSelectBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.executeAfterSelectBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.executeAfterSelectBox.FormattingEnabled = true;
            this.executeAfterSelectBox.Items.AddRange(new object[] {
            "Files Staged",
            "Query-Based Tables Generated",
            "Unknown Keys Replaced",
            "Ranks Set",
            "Checksums Updated",
            "Do Not Execute"});
            this.executeAfterSelectBox.Location = new System.Drawing.Point(430, 7);
            this.executeAfterSelectBox.Name = "executeAfterSelectBox";
            this.executeAfterSelectBox.Size = new System.Drawing.Size(184, 21);
            this.executeAfterSelectBox.TabIndex = 2;
            // 
            // TransformationForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(994, 696);
            this.Controls.Add(this.procedureBox);
            this.Controls.Add(this.lookupBox);
            this.Controls.Add(this.javaTransformBox);
            this.Controls.Add(this.householdBox);
            this.Controls.Add(this.formulaGroup);
            this.Controls.Add(this.dateIDBox);
            this.Controls.Add(this.rankBox);
            this.Controls.Add(this.columnLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.transformSelectBox);
            this.Controls.Add(this.transformationPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TransformationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transformation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TransformationForm_FormClosing);
            this.formulaGroup.ResumeLayout(false);
            this.formulaGroup.PerformLayout();
            this.lookupBox.ResumeLayout(false);
            this.lookupBox.PerformLayout();
            this.procedureBox.ResumeLayout(false);
            this.procedureBox.PerformLayout();
            this.householdBox.ResumeLayout(false);
            this.householdBox.PerformLayout();
            this.rankBox.ResumeLayout(false);
            this.rankBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderByGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partitionTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderByTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nameValueTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.partitionGrid)).EndInit();
            this.dateIDBox.ResumeLayout(false);
            this.dateIDBox.PerformLayout();
            this.javaTransformBox.ResumeLayout(false);
            this.javaTransformBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameValuePairGrid)).EndInit();
            this.transformationPanel.ResumeLayout(false);
            this.transformationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox transformSelectBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox formulaGroup;
        private System.Windows.Forms.TextBox stagingFormula;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox lookupBox;
        private System.Windows.Forms.TextBox joinExpressionBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lookupTableBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox lookupExpressionBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox procedureBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox procedureTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox procNameBox;
        private System.Windows.Forms.GroupBox householdBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TargetZipBox;
        private System.Windows.Forms.TextBox TargetStateBox;
        private System.Windows.Forms.TextBox TargetCityBox;
        private System.Windows.Forms.TextBox TargetAddressBox;
        private System.Windows.Forms.TextBox TargetHHIDBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TargetTableBox;
        private System.Windows.Forms.TextBox ZipBox;
        private System.Windows.Forms.TextBox StateBox;
        private System.Windows.Forms.TextBox CityBox;
        private System.Windows.Forms.TextBox AddressBox;
        private System.Windows.Forms.TextBox HHIDBox;
        private System.Windows.Forms.TextBox targetKeyBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox keyColBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox rankBox;
        private System.Windows.Forms.TextBox ntileNum;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.RadioButton ntileButton;
        private System.Windows.Forms.RadioButton rankButton;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView partitionGrid;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridView orderByGrid;
        private System.Data.DataSet transformSet;
        private System.Data.DataTable partitionTable;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataTable orderByTable;
        private System.Data.DataColumn dataColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnDataGridViewTextBoxColumn;
        private System.Data.DataColumn dataColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Ascending;
        private System.Windows.Forms.GroupBox dateIDBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox dateExpressionBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox dateTypeBox;
        private System.Windows.Forms.GroupBox javaTransformBox;
        private System.Windows.Forms.TextBox javaClassName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox javaTransformName;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridView nameValuePairGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ascendingDataGridViewCheckBoxColumn;
        private System.Data.DataTable nameValueTable;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label columnLabel;
        private System.Windows.Forms.Panel transformationPanel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox executeAfterSelectBox;
    }
}