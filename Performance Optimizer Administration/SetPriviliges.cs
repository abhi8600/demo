using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Performance_Optimizer_Administration
{
    public partial class SetPriviliges : Form, ResizeMemoryForm
    {
        private MainAdminForm ma;
        private string windowName = "Privileges";
        private string groupName;
        private List<ACLItem> resultList;
        private String[] defaultTags = new String[]
            {
                "Dashboard", "Manage", "NewDashboard", "EditDashboard", "Adhoc", "AdvancedFilter", "MaintainCatalog",
                "ResetPassword", "CreateUser", "DeleteUser", "EnableUser", "DisableUser", "ChangePassword", "DemoMode",
                "PremiumAdHoc", "DashboardDelete", "EnableDownload"
            };
        public SetPriviliges(MainAdminForm ma, string groupName, IEnumerable accessList)
        {
            InitializeComponent();
            this.ma = ma;
            ACL.Rows.Clear();
            this.groupName = groupName;
            resultList = new List<ACLItem>();
            foreach (String s in defaultTags)
            {
                DataRow dr = ACL.NewRow();
                dr["Group"] = groupName;
                dr["Tag"] = s;
                if (groupName.Equals("Administrators"))
                {
                    dr["Access"] = true;
                }
                ACL.Rows.Add(dr);
            }
            foreach (ACLItem item in accessList)
            {
                if (item.Group == groupName)
                {
                    DataRow[] rows = ACL.Select("Group='" + groupName + "' AND Tag='" + item.Tag + "'");
                    if (rows.Length > 0)
                    {
                        rows[0]["Access"] = item.Access;
                    }
                    else
                    {
                        DataRow dr = ACL.NewRow();
                        dr["Group"] = groupName;
                        dr["Tag"] = item.Tag;
                        dr["Access"] = item.Access;
                        ACL.Rows.Add(dr);
                    }
                }
            }
            this.resetWindowFromSettings();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            DataRow[] rows = ACL.Select("Group='" + groupName + "' AND Tag IS NOT NULL");
            foreach (DataRow row in rows)
            {
                ACLItem item = new ACLItem();
                item.Group = (string) row["Group"];
                item.Tag = (string) row["Tag"];
                try
                {
                    item.Access = (bool)row["Access"];
                    resultList.Add(item);
                }
                catch (Exception)
                {
                }
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            resultList = null;
            this.Close();
        }

        public List<ACLItem> getAccessList()
        {
            return (resultList);
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void SetPriviliges_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }
    }
}