﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using log4net;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("PackageList2", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class AllPackages
    {
        public const int PACKAGE_FORMAT_OLD = 1;
        public const int PACKAGE_FORMAT_NEW = 2;

        // To signal the format change of packages.xml
        [XmlIgnore]
        public int version; 

        public PackageDetail[] Packages;
        private static ILog logger;

        public static AllPackages load(string directory)
        {
            string path = Path.Combine(directory, "Packages.xml");
            if (!File.Exists(path))
                return null;
            StreamReader sreader = null;
            XmlReader xreader = null;
            AllPackages response = null;
            try
            {
                sreader = new StreamReader(path, Encoding.UTF8);
                xreader = Util.getSecureXMLReader(sreader);
                // If packages.xml is in new format
                XmlSerializer serializer = new XmlSerializer(typeof(AllPackages));
                response = (AllPackages)serializer.Deserialize(xreader);
                response.version = PACKAGE_FORMAT_NEW;
            }
            catch (System.InvalidOperationException)
            {
                if (logger != null)
                {
                    logger.Warn("Problem in loading packages.xml in new format. Trying old format");
                }
                response = fetchOldFormatPackages(directory);
                if (response != null)
                {
                    response.version = PACKAGE_FORMAT_OLD;
                }
            }
            finally
            {
                if (xreader != null)
                    xreader.Close();
                if (sreader != null)
                    sreader.Close();
            }
            return response;
        }

        public void save(string spaceDirectory)
        {
            save(spaceDirectory, null);
        }

        public void save(string spaceDirectory, string packageFileName)
        {
            TextWriter writer = null;
            XmlSerializer serializer = null;
            try
            {
                version = PACKAGE_FORMAT_NEW;
                if (packageFileName == null)
                {
                    packageFileName = "packages.xml";   
                }

                string packageFilePath = Path.Combine(spaceDirectory, packageFileName);
                writer = new StreamWriter(packageFilePath);
                serializer = new XmlSerializer(typeof(AllPackages));
                serializer.Serialize(writer, this);
            }
            finally
            {
                if(writer != null)
                {
                    writer.Close();
                }
            }
        }

        public PackageDetail getPackage(string name)
        {
            if (Packages != null && Packages.Length > 0)
            {
                foreach (PackageDetail p in Packages)
                    if (p.Name == name)
                        return p;
            }
            return null;
        }

        public PackageDetail getPackage(Guid packageID)
        {
            if (Packages != null && Packages.Length > 0)
            {
                foreach (PackageDetail p in Packages)
                {
                    if (p.ID == packageID)
                    {
                        return p;
                    }
                }
            }
            return null;
        }

        private static AllPackages fetchOldFormatPackages(string directory)
        {
            AllPackages response = null;
            try
            {
                PackageList packageListOld = PackageList.load(directory);
                if (packageListOld != null && packageListOld.Packages != null)
                {
                    response = new AllPackages();
                    List<PackageDetail> packageDetailsList = new List<PackageDetail>();
                    foreach (Package packageOldFormat in packageListOld.Packages)
                    {
                        PackageDetail packageDetail = formatOldToNewPackageFormat(packageOldFormat);
                        if (packageDetail != null)
                        {
                            packageDetailsList.Add(packageDetail);
                        }
                    }

                    response.Packages = packageDetailsList.ToArray();
                }
            }
            catch (Exception ex)
            {
                if (logger != null)
                {
                    logger.Error("Error in loading packages.xml in old format", ex);
                }
            }
            return response;
        }

        private static PackageDetail formatOldToNewPackageFormat(Package packageOldFormat)
        {
            PackageDetail response = null;
            if (packageOldFormat != null)
            {
                response = new PackageDetail();
                response.ID = packageOldFormat.ID;
                response.SpaceID = packageOldFormat.SpaceID; 
                response.Name = packageOldFormat.Name;
                response.Description = packageOldFormat.Description;
                response.CreatedBy = packageOldFormat.CreatedBy;
                response.CreatedDate = packageOldFormat.CreatedDate;
                response.ModifiedBy = packageOldFormat.ModifiedBy;
                response.ModifiedDate = packageOldFormat.ModifiedDate;
                string[] stagingTables = packageOldFormat.StagingTables;
                string[] dimensionTables = packageOldFormat.DimensionTables;
                string[] dimensionTableDisplayNames = packageOldFormat.DimensionTableDisplayNames;
                string[] measureTables = packageOldFormat.MeasureTables;
                string[] measureTableDisplayNames = packageOldFormat.MeasureTableDisplayNames;
                string[] variables = packageOldFormat.Variables;

                if (stagingTables != null && stagingTables.Length > 0)
                {
                    List<PStagingTable> pStagingTablesList = new List<PStagingTable>();
                    foreach (string stagingTableName in stagingTables)
                    {
                        PStagingTable pStagingTable = new PStagingTable();
                        pStagingTable.Name = stagingTableName;
                        pStagingTablesList.Add(pStagingTable);
                    }
                    response.StagingTables = pStagingTablesList.ToArray();
                }
                if (variables != null && variables.Length > 0)
                {
                    List<PVariable> pVariablesList = new List<PVariable>();
                    foreach (string variableName in variables)
                    {
                        PVariable pVariable = new PVariable();
                        pVariable.Name = variableName;
                        pVariablesList.Add(pVariable);
                    }
                    response.Variables = pVariablesList.ToArray();
                }

                if (dimensionTables != null && dimensionTables.Length > 0
                    && dimensionTableDisplayNames != null && dimensionTableDisplayNames.Length > 0)
                {
                    if (dimensionTables.Length == dimensionTableDisplayNames.Length)
                    {
                        List<PDimensionTable> pDimensionTablesList = new List<PDimensionTable>();
                        for (int i = 0; i < dimensionTables.Length; i++)
                        {
                            PDimensionTable pDimTable = new PDimensionTable();
                            pDimTable.Name = dimensionTables[i];
                            pDimTable.DisplayName = dimensionTableDisplayNames[i];
                            pDimensionTablesList.Add(pDimTable);
                        }
                        response.DimensionTables = pDimensionTablesList.ToArray();
                    }
                    else
                    {
                        if (logger != null)
                        {
                            logger.Warn("Old PackageFormat : Skipping Dimension Tables: Number of dimtable names and display names do not match ");
                        }
                    }

                }

                if (measureTables != null && measureTables.Length > 0
                    && measureTableDisplayNames != null && measureTableDisplayNames.Length > 0)
                {
                    if (measureTables.Length == measureTableDisplayNames.Length)
                    {
                        List<PMeasureTable> pMeasureTablesList = new List<PMeasureTable>();
                        for (int i = 0; i < measureTables.Length; i++)
                        {
                            PMeasureTable pMeasureTable = new PMeasureTable();
                            pMeasureTable.Name = measureTables[i];
                            pMeasureTable.DisplayName = measureTableDisplayNames[i];
                            pMeasureTablesList.Add(pMeasureTable);
                        }
                        response.MeasureTables = pMeasureTablesList.ToArray();
                    }
                    else
                    {
                        if (logger != null)
                        {
                            logger.Warn("Old PackageFormat : Skipping Measure Tables: Number of measure table names and display names do not match ");
                        }
                    }

                }
            }
            return response;
        }

        public static void setLogger(ILog log)
        {
            logger = log;
        }

        public static ILog getLogger()
        {
            return logger;
        }
    }
}
