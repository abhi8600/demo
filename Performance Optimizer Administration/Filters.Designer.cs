namespace Performance_Optimizer_Administration
{
    partial class Filters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.filterTabPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.operatorBox = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.leftFilterListViewLabel = new System.Windows.Forms.Label();
            this.leftFilterListView = new System.Windows.Forms.ListView();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.label6 = new System.Windows.Forms.Label();
            this.columnButton = new System.Windows.Forms.RadioButton();
            this.leftFilterButton = new System.Windows.Forms.RadioButton();
            this.availFilterColumns = new System.Windows.Forms.ListView();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.selectLeftPredicate = new System.Windows.Forms.Button();
            this.filterColumn = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.selectFilterColumn = new System.Windows.Forms.Button();
            this.leftFilterPredicate = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rightFilterListViewLabel = new System.Windows.Forms.Label();
            this.rightFilterListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.selectRightPredicate = new System.Windows.Forms.Button();
            this.rightFilterPredicate = new System.Windows.Forms.TextBox();
            this.operandBox = new System.Windows.Forms.TextBox();
            this.filterMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.filterTabPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.filterMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.filterTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // filterTabPage
            // 
            this.filterTabPage.Controls.Add(this.groupBox16);
            this.filterTabPage.Location = new System.Drawing.Point(4, 22);
            this.filterTabPage.Name = "filterTabPage";
            this.filterTabPage.Size = new System.Drawing.Size(844, 707);
            this.filterTabPage.TabIndex = 15;
            this.filterTabPage.Text = "Filters";
            this.filterTabPage.UseVisualStyleBackColor = true;
            this.filterTabPage.Leave += new System.EventHandler(this.filterTabPage_Leave);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.cancelButton);
            this.groupBox16.Controls.Add(this.okButton);
            this.groupBox16.Controls.Add(this.label7);
            this.groupBox16.Controls.Add(this.operatorBox);
            this.groupBox16.Controls.Add(this.panel4);
            this.groupBox16.Controls.Add(this.panel5);
            this.groupBox16.Location = new System.Drawing.Point(8, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(832, 565);
            this.groupBox16.TabIndex = 41;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Filter Properties";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(748, 536);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 64;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Visible = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(667, 536);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 63;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Visible = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(366, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 23);
            this.label7.TabIndex = 52;
            this.label7.Text = "Operator";
            // 
            // operatorBox
            // 
            this.operatorBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.operatorBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.operatorBox.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            "<>",
            ">=",
            "<=",
            "LIKE",
            "NOT LIKE",
            "IN",
            "NOT IN"});
            this.operatorBox.Location = new System.Drawing.Point(350, 43);
            this.operatorBox.Name = "operatorBox";
            this.operatorBox.Size = new System.Drawing.Size(91, 21);
            this.operatorBox.TabIndex = 51;
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.leftFilterListViewLabel);
            this.panel4.Controls.Add(this.leftFilterListView);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.columnButton);
            this.panel4.Controls.Add(this.leftFilterButton);
            this.panel4.Controls.Add(this.availFilterColumns);
            this.panel4.Controls.Add(this.selectLeftPredicate);
            this.panel4.Controls.Add(this.filterColumn);
            this.panel4.Controls.Add(this.label106);
            this.panel4.Controls.Add(this.selectFilterColumn);
            this.panel4.Controls.Add(this.leftFilterPredicate);
            this.panel4.Location = new System.Drawing.Point(6, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(333, 448);
            this.panel4.TabIndex = 61;
            // 
            // leftFilterListViewLabel
            // 
            this.leftFilterListViewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftFilterListViewLabel.Location = new System.Drawing.Point(24, 90);
            this.leftFilterListViewLabel.Name = "leftFilterListViewLabel";
            this.leftFilterListViewLabel.Size = new System.Drawing.Size(120, 23);
            this.leftFilterListViewLabel.TabIndex = 65;
            this.leftFilterListViewLabel.Text = "Available Filters";
            // 
            // leftFilterListView
            // 
            this.leftFilterListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leftFilterListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.leftFilterListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.leftFilterListView.HideSelection = false;
            this.leftFilterListView.Location = new System.Drawing.Point(24, 114);
            this.leftFilterListView.MultiSelect = false;
            this.leftFilterListView.Name = "leftFilterListView";
            this.leftFilterListView.ShowItemToolTips = true;
            this.leftFilterListView.Size = new System.Drawing.Size(304, 97);
            this.leftFilterListView.TabIndex = 64;
            this.leftFilterListView.UseCompatibleStateImageBehavior = false;
            this.leftFilterListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 180;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 23);
            this.label6.TabIndex = 63;
            this.label6.Text = "Left Predicate";
            // 
            // columnButton
            // 
            this.columnButton.Checked = true;
            this.columnButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.columnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnButton.Location = new System.Drawing.Point(8, 227);
            this.columnButton.Name = "columnButton";
            this.columnButton.Size = new System.Drawing.Size(112, 24);
            this.columnButton.TabIndex = 62;
            this.columnButton.TabStop = true;
            this.columnButton.Text = "Filter Column";
            this.columnButton.CheckedChanged += new System.EventHandler(this.columnButton_CheckedChanged);
            // 
            // leftFilterButton
            // 
            this.leftFilterButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.leftFilterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftFilterButton.Location = new System.Drawing.Point(8, 32);
            this.leftFilterButton.Name = "leftFilterButton";
            this.leftFilterButton.Size = new System.Drawing.Size(88, 24);
            this.leftFilterButton.TabIndex = 61;
            this.leftFilterButton.Text = "Filter";
            this.leftFilterButton.CheckedChanged += new System.EventHandler(this.leftfilterButton_CheckedChanged);
            // 
            // availFilterColumns
            // 
            this.availFilterColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availFilterColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader24,
            this.columnHeader25});
            this.availFilterColumns.FullRowSelect = true;
            this.availFilterColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.availFilterColumns.HideSelection = false;
            this.availFilterColumns.Location = new System.Drawing.Point(24, 307);
            this.availFilterColumns.MultiSelect = false;
            this.availFilterColumns.Name = "availFilterColumns";
            this.availFilterColumns.ShowItemToolTips = true;
            this.availFilterColumns.Size = new System.Drawing.Size(304, 136);
            this.availFilterColumns.TabIndex = 49;
            this.availFilterColumns.UseCompatibleStateImageBehavior = false;
            this.availFilterColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Column Name";
            this.columnHeader24.Width = 150;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Type";
            this.columnHeader25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader25.Width = 100;
            // 
            // selectLeftPredicate
            // 
            this.selectLeftPredicate.Enabled = false;
            this.selectLeftPredicate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectLeftPredicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectLeftPredicate.Location = new System.Drawing.Point(248, 32);
            this.selectLeftPredicate.Name = "selectLeftPredicate";
            this.selectLeftPredicate.Size = new System.Drawing.Size(56, 20);
            this.selectLeftPredicate.TabIndex = 59;
            this.selectLeftPredicate.Text = "Select";
            this.selectLeftPredicate.Click += new System.EventHandler(this.selectLeftPredicate_Click);
            // 
            // filterColumn
            // 
            this.filterColumn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filterColumn.Location = new System.Drawing.Point(24, 251);
            this.filterColumn.Name = "filterColumn";
            this.filterColumn.ReadOnly = true;
            this.filterColumn.Size = new System.Drawing.Size(280, 20);
            this.filterColumn.TabIndex = 57;
            // 
            // label106
            // 
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(24, 283);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(120, 23);
            this.label106.TabIndex = 55;
            this.label106.Text = "Available Columns";
            // 
            // selectFilterColumn
            // 
            this.selectFilterColumn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectFilterColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectFilterColumn.Location = new System.Drawing.Point(248, 227);
            this.selectFilterColumn.Name = "selectFilterColumn";
            this.selectFilterColumn.Size = new System.Drawing.Size(56, 20);
            this.selectFilterColumn.TabIndex = 56;
            this.selectFilterColumn.Text = "Select";
            this.selectFilterColumn.Click += new System.EventHandler(this.selectFilterColumn_Click);
            // 
            // leftFilterPredicate
            // 
            this.leftFilterPredicate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leftFilterPredicate.Location = new System.Drawing.Point(24, 56);
            this.leftFilterPredicate.Multiline = true;
            this.leftFilterPredicate.Name = "leftFilterPredicate";
            this.leftFilterPredicate.ReadOnly = true;
            this.leftFilterPredicate.Size = new System.Drawing.Size(280, 20);
            this.leftFilterPredicate.TabIndex = 60;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.rightFilterListViewLabel);
            this.panel5.Controls.Add(this.rightFilterListView);
            this.panel5.Controls.Add(this.label151);
            this.panel5.Controls.Add(this.label150);
            this.panel5.Controls.Add(this.label105);
            this.panel5.Controls.Add(this.selectRightPredicate);
            this.panel5.Controls.Add(this.rightFilterPredicate);
            this.panel5.Controls.Add(this.operandBox);
            this.panel5.Location = new System.Drawing.Point(452, 19);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(360, 476);
            this.panel5.TabIndex = 62;
            // 
            // rightFilterListViewLabel
            // 
            this.rightFilterListViewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightFilterListViewLabel.Location = new System.Drawing.Point(24, 90);
            this.rightFilterListViewLabel.Name = "rightFilterListViewLabel";
            this.rightFilterListViewLabel.Size = new System.Drawing.Size(120, 23);
            this.rightFilterListViewLabel.TabIndex = 70;
            this.rightFilterListViewLabel.Text = "Available Filters";
            // 
            // rightFilterListView
            // 
            this.rightFilterListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rightFilterListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.rightFilterListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.rightFilterListView.HideSelection = false;
            this.rightFilterListView.Location = new System.Drawing.Point(24, 114);
            this.rightFilterListView.MultiSelect = false;
            this.rightFilterListView.Name = "rightFilterListView";
            this.rightFilterListView.ShowItemToolTips = true;
            this.rightFilterListView.Size = new System.Drawing.Size(304, 97);
            this.rightFilterListView.TabIndex = 69;
            this.rightFilterListView.UseCompatibleStateImageBehavior = false;
            this.rightFilterListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 180;
            // 
            // label151
            // 
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(8, 252);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(88, 16);
            this.label151.TabIndex = 68;
            this.label151.Text = "Expression";
            // 
            // label150
            // 
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.Location = new System.Drawing.Point(8, 32);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(88, 16);
            this.label150.TabIndex = 67;
            this.label150.Text = "Filter";
            // 
            // label105
            // 
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(8, 8);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(112, 23);
            this.label105.TabIndex = 66;
            this.label105.Text = "Right Predicate";
            // 
            // selectRightPredicate
            // 
            this.selectRightPredicate.Enabled = false;
            this.selectRightPredicate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectRightPredicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectRightPredicate.Location = new System.Drawing.Point(280, 32);
            this.selectRightPredicate.Name = "selectRightPredicate";
            this.selectRightPredicate.Size = new System.Drawing.Size(56, 20);
            this.selectRightPredicate.TabIndex = 62;
            this.selectRightPredicate.Text = "Select";
            this.selectRightPredicate.Click += new System.EventHandler(this.selectRightPredicate_Click);
            // 
            // rightFilterPredicate
            // 
            this.rightFilterPredicate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rightFilterPredicate.Location = new System.Drawing.Point(24, 56);
            this.rightFilterPredicate.Multiline = true;
            this.rightFilterPredicate.Name = "rightFilterPredicate";
            this.rightFilterPredicate.ReadOnly = true;
            this.rightFilterPredicate.Size = new System.Drawing.Size(312, 20);
            this.rightFilterPredicate.TabIndex = 63;
            // 
            // operandBox
            // 
            this.operandBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.operandBox.Location = new System.Drawing.Point(24, 276);
            this.operandBox.Multiline = true;
            this.operandBox.Name = "operandBox";
            this.operandBox.Size = new System.Drawing.Size(312, 184);
            this.operandBox.TabIndex = 53;
            // 
            // filterMenuStrip
            // 
            this.filterMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFilterToolStripMenuItem,
            this.removeFilterToolStripMenuItem});
            this.filterMenuStrip.Name = "filterMenuStrip";
            this.filterMenuStrip.ShowImageMargin = false;
            this.filterMenuStrip.Size = new System.Drawing.Size(122, 48);
            // 
            // newFilterToolStripMenuItem
            // 
            this.newFilterToolStripMenuItem.Name = "newFilterToolStripMenuItem";
            this.newFilterToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.newFilterToolStripMenuItem.Text = "New Filter";
            this.newFilterToolStripMenuItem.Click += new System.EventHandler(this.addFilter_Click);
            // 
            // removeFilterToolStripMenuItem
            // 
            this.removeFilterToolStripMenuItem.Name = "removeFilterToolStripMenuItem";
            this.removeFilterToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.removeFilterToolStripMenuItem.Text = "Remove Filter";
            this.removeFilterToolStripMenuItem.Click += new System.EventHandler(this.removeFilter_Click);
            // 
            // Filters
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(868, 650);
            this.Name = "Filters";
            this.Text = "Filters";
            this.tabControl.ResumeLayout(false);
            this.filterTabPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.filterMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage filterTabPage;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox operatorBox;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton columnButton;
        private System.Windows.Forms.RadioButton leftFilterButton;
        private System.Windows.Forms.ListView availFilterColumns;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.Button selectLeftPredicate;
        private System.Windows.Forms.TextBox filterColumn;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Button selectFilterColumn;
        private System.Windows.Forms.TextBox leftFilterPredicate;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Button selectRightPredicate;
        private System.Windows.Forms.TextBox rightFilterPredicate;
        private System.Windows.Forms.TextBox operandBox;
        private System.Windows.Forms.ContextMenuStrip filterMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem removeFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFilterToolStripMenuItem;
        private System.Windows.Forms.ListView leftFilterListView;
        private System.Windows.Forms.ListView rightFilterListView;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label leftFilterListViewLabel;
        private System.Windows.Forms.Label rightFilterListViewLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
    }
}