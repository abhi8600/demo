using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Xml;

namespace Performance_Optimizer_Administration
{
    public class Util
    {
        public const int MODEL_NONE = 0;
        public const int MODEL_REFERENCE_POPULATIONS = 1;
        public const int MODEL_PERFORMANCE = 2;
        public const int MODEL_SUCCESS_MODELS = 3;
        public static string GLOBAL_TIMEZONE_DEFAULT = "Server Local Time";
        public static string DISPLAY_TIMEZONE_DEFAULT = "Same As Processing Time Zone";
        public static string SOURCE_TIMEZONE_DEFAULT = "Same As Processing Time Zone";
        public static string NEW_QUERY_LANGUAGE_UNSUPPORTED_CHARS = "[^\\w:$#%\\-&!^@,;></ \u0080-\uFFCF]";
        public static string OLD_QUERY_LANGUAGE_UNSUPPORTED_CHARS = "[^\\w\\.@\\-_$#%*&:;/<>,=+| ]";

        internal class UseList
        {
            public string name;
            bool isUsed = false;
            public UseList(string name, bool isUsed)
            {
                this.name = name;
                this.isUsed = isUsed;
            }
            public static int compareTo(UseList One, UseList Two)
            {
                if (One == null && Two == null) return 0;
                if (One != null && Two == null) return -1;
                if (One == null && Two != null) return 1;
                if (One.isUsed && !Two.isUsed) return -1;
                if (!One.isUsed && Two.isUsed) return 1;
                return One.name.CompareTo(Two.name);
            }
        }

        /**
         * get safe XML parser (no entity replacement, no external entities)
         */
        public static XmlReader getSecureXMLReader(StreamReader strm)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ProhibitDtd = true;
            settings.MaxCharactersFromEntities = 30;
            settings.ValidationType = ValidationType.None;
            settings.XmlResolver = null;
            XmlReader reader = XmlReader.Create(strm, settings);
            return reader;
        }

        /**
         * WARNING - this renaming must match the renaming in Util.replaceWithPhysicalString on the Java side
         * - if you get SQL errors about bad column names when updating a dimension table from a staging table, it is probably because these two methods have drifted apart
         */
        public static string generatePhysicalName(string colName)
        {
            return generatePhysicalName(colName, int.MaxValue); //Don't have builder version so pass a lower value to preserve older behavior.
        }

        public static string generatePhysicalName(string colName, int builderVersion)
        {
            return generatePhysicalName(colName, builderVersion, false, 0);
        }
        public static string generatePhysicalName(string colName, int builderVersion, bool isStagingTable, int maxLength)
        {      
            if (colName.Length == 0)
                return colName;
            char c = colName[0];
            if (c >= '0' && c <= '9')
            {
                colName = 'N' + colName;
            }

            string p1 = colName.Trim().Replace('$', 'D').Replace('#', 'N');
            string p2 = Regex.Replace(p1, @"[^\w\u0080-\uFFCF]", "_");

            if (ApplicationBuilder.ContainsNonASCIICharacters(p2))
            {
                p2 = ApplicationBuilder.getMD5Hash(ApplicationBuilder.SqlGenType.Default, p2, 1000, builderVersion); // XXX HACK (max length and builder version to make sure we don't do anything else)
            }
            else if (isStagingTable == true && maxLength > 0 && p2.Length > maxLength && builderVersion>=24)
            {
                p2 = ApplicationBuilder.getMD5Hash(ApplicationBuilder.SqlGenType.Default, p2, maxLength, builderVersion); // XXX HACK (max length and builder version to make sure we don't do anything else)                                    
            }
                  
            return p2;
        }

        public static List<string> sortListAsUsed(List<string> list, string[] listOfUsed)
        {
            if (list == null) return null;
            if (listOfUsed == null || listOfUsed.Length == 0)
            {
                list.Sort();
                return list;
            }
            List<string> lListOfUsed = new List<string>(listOfUsed);
            List<UseList> uList = new List<UseList>();
            for (int i = 0; i < list.Count; i++)
            {
                UseList UF = new UseList(list[i], lListOfUsed.Contains(list[i]) ? true : false);
                uList.Add(UF);
            }
            uList.Sort(UseList.compareTo);
            List<string> sList = new List<string>();
            for (int i = 0; i < uList.Count; i++)
            {
                sList.Add(uList[i].name);
            }
            return sList;
            
        }
        public static void sortedListBox(CheckedListBox c, List<string> list, string[] listOfUsed)
        {
            List<string> sortedList = sortListAsUsed(list, listOfUsed);
            List<string> sListOfUsed = listOfUsed != null ? new List<string>(listOfUsed) : new List<string>();
            c.Items.Clear();
            foreach (string s in sortedList)
            {
                if (s!= null && !c.Items.Contains(s)) c.Items.Add(s);
                if (s!= null && sListOfUsed.Contains(s))
                {
                    c.SetItemChecked(c.Items.Count - 1, true);
                }
            }
            return;
        }
        public static void sortedListView(ListView c, List<string> list, string[] listOfUsed)
        {
            List<string> sortedList = sortListAsUsed(list, listOfUsed);
            List<string> sListOfUsed = listOfUsed != null ? new List<string>(listOfUsed) : new List<string>();
            c.Items.Clear();
            foreach (string s in sortedList)
            {
                if (s != null && !c.Items.Contains(new ListViewItem(s))) c.Items.Add(s);
                if (s != null && sListOfUsed.Contains(s))
                {
                    c.Items[c.Items.Count - 1].Checked = true;
                }
            }
            return;
        }

        public static List<DataRow> selectRows(DataTable table, object[][] criteria)
        {
            return (selectRows(table, criteria, true));
        }

        public static List<DataRow> selectRows(DataTable table, object[][] criteria, bool caseSensitive)
        {
            int[] indices = new int[criteria.Length];
            for (int i = 0; i < criteria.Length; i++)
                indices[i] = table.Columns.IndexOf(criteria[i][0].ToString());
            List<DataRow> result = new List<DataRow>();
            foreach (DataRow dr in table.Rows)
            {
                bool match = true;
                for (int i = 0; i < criteria.Length; i++)
                {
                    if (caseSensitive)
                    {
                        if (!dr[indices[i]].Equals(criteria[i][1]))
                        {
                            match = false;
                            break;
                        }
                    }
                    else
                    {
                        if (!dr[indices[i]].ToString().Equals(criteria[i][1].ToString(), 
                            StringComparison.OrdinalIgnoreCase))
                        {
                            match = false;
                            break;
                        }
                    }
                }
                if (match)
                    result.Add(dr);
            }
            return (result);
        }

        public static string[] splitString(string s, char delim)
        {
            List<string> l = new List<string>();
            bool quote = false;
            bool lastEscape = false;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '"' && !lastEscape)
                {
                    sb.Append('"');
                    quote = !quote;
                }
                else if (!quote)
                {
                    if (s[i] == delim)
                    {
                        l.Add(sb.ToString());
                        sb = new StringBuilder();
                    }
                    else
                        sb.Append(s[i]);
                }
                else
                {
                    if (lastEscape && s[i] == '\n')
                    {
                        sb[sb.Length - 1] = '\n';
                    }
                    else
                        sb.Append(s[i]);
                }
                if (s[i] == '\\' && !lastEscape)
                    lastEscape = true;
                else
                    lastEscape = false;
            }
            l.Add(sb.ToString());
            return (l.ToArray());
        }

        public static bool equalsArray(string[] arr1, string[] arr2)
        {
            if (arr1 == null && arr2 != null)
                return false;
            if (arr1 != null && arr2 == null)
                return false;
            if (arr1.Length != arr2.Length)
                return false;
            foreach (string s in arr1)
            {
                bool found = false;
                foreach (string s2 in arr2)
                {
                    if (s == s2)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }

        public static String getAuthenticationToken(String serverURL, String repUsername)
        {
            String url = serverURL + "/TokenGeneratorServlet?userName=" + repUsername + "&sharedToken=narf";
            WebRequest wreq = WebRequest.Create(url);
            WebResponse wres = wreq.GetResponse();
            byte[] buff = new byte[200];
            int readCount = wres.GetResponseStream().Read(buff, 0, 200);
            StringBuilder token = new StringBuilder(200);
            for (int i = 0; i < readCount; i++)
                token.Append((char)buff[i]);
            return token.ToString();
        }

        public static void MouseMove(object sender, MouseEventArgs e, ToolTip toolTip)
        {
            if (sender is CheckedListBox)
            {
                int width = ((CheckedListBox)sender).Width;
                System.Drawing.Graphics g = ((CheckedListBox)sender).CreateGraphics();
                System.Drawing.Font font = ((CheckedListBox)sender).Font;

                int ttIndex = ((CheckedListBox)sender).IndexFromPoint(((CheckedListBox)sender).PointToClient(Control.MousePosition));
                if (ttIndex > -1)
                {
                    int newWidth = (int)g.MeasureString(((CheckedListBox)sender).Items[ttIndex].ToString(), font).Width;
                    int height = ((CheckedListBox)sender).Items.Count * (int)g.MeasureString(((CheckedListBox)sender).Items[ttIndex].ToString(), font).Height;
                    int vscrollbarwidth = height < ((CheckedListBox)sender).Height ? 1 : SystemInformation.VerticalScrollBarWidth;
                    if (width < newWidth + (vscrollbarwidth * 2))
                        toolTip.SetToolTip((CheckedListBox)sender, ((CheckedListBox)sender).Items[ttIndex].ToString());
                    else
                        toolTip.RemoveAll();
                }
            }
            else if (sender is ListBox)
            {
                int width = ((ListBox)sender).Width;
                System.Drawing.Graphics g = ((ListBox)sender).CreateGraphics();
                System.Drawing.Font font = ((ListBox)sender).Font;

                int ttIndex = ((ListBox)sender).IndexFromPoint(((ListBox)sender).PointToClient(Control.MousePosition));
                if (ttIndex > -1)
                {
                    int newWidth = (int)g.MeasureString(((ListBox)sender).Items[ttIndex].ToString(), font).Width;
                    int height = ((ListBox)sender).Items.Count * (int)g.MeasureString(((ListBox)sender).Items[ttIndex].ToString(), font).Height;
                    int vscrollbarwidth = height < ((ListBox)sender).Height ? 1 : SystemInformation.VerticalScrollBarWidth;
                    if (width < newWidth + (vscrollbarwidth))
                        toolTip.SetToolTip((ListBox)sender, ((ListBox)sender).Items[ttIndex].ToString());
                    else
                        toolTip.RemoveAll();
                }
            }
        }

        /* While trying to parse a string into datetime, if you use tryParse, it uses current culture's datetime formats only. i.e. for "en-US",
         * it wont successfully parse values in d/M/y format. This method tries to parse strings using the US as well as UK cultures making sure
         * that both M/d/y and d/M/y formats are interpreted as datetime.
         */
        public static bool tryDateTimeParse(string s, out DateTime dt)
        {
            bool parsed = false;
            parsed = DateTime.TryParse(s, out dt);
            if (!parsed)
            {
                //Remember current culture
                CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                //Make sure we try parsing d/M/y as well as M/d/y
                if (currentCulture.Name == "en-US")
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                }
                parsed = DateTime.TryParse(s, out dt);
                Thread.CurrentThread.CurrentCulture = currentCulture;
            }
            if (!parsed)
            {
                //check if datetime format is "yyyy-MM-dd'T'H:mm:ss" or "yyyy/MM/dd'T'H:mm:ss"
                //"yyyy-MM-dd'T'H:mm:ss" format is required for Securian
                //when .txt is converted from .xlsx "yyyy-MM-dd'T'H:mm:ss" is written as "yyyy/MM/dd'T'H:mm:ss"
                //therefore, trying to parse using both formats as a special case.
                //also allowing for small 't' instead of 'T'
                string[] formats = { "yyyy'/'MM'/'dd'T'H':'mm':'ss", "yyyy'-'MM'-'dd'T'H':'mm':'ss",
                                     "yyyy'/'MM'/'dd't'H':'mm':'ss", "yyyy'-'MM'-'dd't'H':'mm':'ss" };
                parsed = DateTime.TryParseExact(s, formats, null, System.Globalization.DateTimeStyles.None, out dt);
            }
            return parsed;
        }

        // deal with locking issues for move
        public static void SafeMove(string from, string to)
        {
            int cnt = 0;
            while (true)
            {
                try
                {
                    File.Delete(to);
                    File.Move(from, to);
                    break;
                }
                catch (IOException ex)
                {
                    // from could currently be locked due to another process reading it (lets hope its not a nother process writing it...)
                    cnt++;
                    if (cnt > 10)
                        throw ex;
                    Thread.Sleep(500);
                }
            }
        }

        public static StreamReader getStreamReader(string fileName)
        {
            StreamReader reader = null;
            int count = 0;
            bool retry = false;
            do
            {
                try
                {
                    count++;
                    reader = new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read));
                    retry = false;
                }
                catch (IOException ex)
                {
                    // if the ioexception occurs, retry few seconds before giving up.
                    // Exception might be because of concurrent reading/writing of the repository_dev.xml
                    retry = count <= 5 ? true : false;
                    if (!retry)
                    {
                        // if number of retries have exhausted throw the exception back to the calling program
                        throw ex;
                    }
                    System.Threading.Thread.Sleep(2000);
                }
            } while (retry);
            return reader;
        }

        public static string getBinaryExtension()
        {
            return ".abin" + Repository.RepositoryWriteVersion;
        }

        public static string getSerializedString(string[] arrayofstring)
        {
            if (arrayofstring == null)
                return null;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < arrayofstring.Length; i++)
            {
                if (i > 0)
                    sb.Append("|1");
                sb.Append(arrayofstring[i]);
            }
            return sb.ToString();
        }

        public static bool tryParseInt(int builderVersion, string s, out long val)
        {
            if(builderVersion >= Repository.IntToLongBuilderVersion)
            {
                return Int64.TryParse(s, out val); 
            }
            else
            {
                int valInt;
                bool success = Int32.TryParse(s, out valInt);
                val = valInt;
                return success;
            }            
        }        

        public static string getDisplayString(string[] stringarr)
        {
            if (stringarr == null)
                return "";
            StringBuilder sb = new StringBuilder("[");
            for (int i = 0; i < stringarr.Length; i++)
            {
                if (i > 0)
                    sb.Append(',');
                sb.Append(stringarr[i]);
            }
            sb.Append(']');
            return sb.ToString();
        }

        public static string getDisplayString(bool[] boolarr)
        {
            if (boolarr == null)
                return "";
            StringBuilder sb = new StringBuilder("[");
            for (int i = 0; i < boolarr.Length; i++)
            {
                if (i > 0)
                    sb.Append(',');
                sb.Append(boolarr[i].ToString());
            }
            sb.Append(']');
            return sb.ToString();
        }

        public static DimensionColumn getDimensionColumn(DimensionTable dt, String columnName)
        {            
            if (dt.DimensionColumns != null)
            {
                foreach (DimensionColumn dc in dt.DimensionColumns)
                {
                    if (dc.ColumnName.Equals(columnName))
                    {
                        return dc;
                    }
                }
            }
            return null;
        }
    }
}
