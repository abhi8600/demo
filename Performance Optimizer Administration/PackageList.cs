﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("PackageList", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class PackageList
    {
        public Package[] Packages;

        public static PackageList load(string directory)
        {
            string path = Path.Combine(directory, "Packages.xml");
            if (!File.Exists(path))
                return null;
            StreamReader sreader = null;
            XmlReader xreader = null;
            try
            {
                sreader = new StreamReader(path, Encoding.UTF8);
                xreader = Util.getSecureXMLReader(sreader);
                XmlSerializer serializer = new XmlSerializer(typeof(PackageList));
                PackageList pl = (PackageList)serializer.Deserialize(xreader);
                return pl;
            }
            catch (System.InvalidOperationException ex)
            {
                if (AllPackages.getLogger() != null)
                {
                    AllPackages.getLogger().Error("Error in loading packages file in old format ", ex);
                }
            }
            finally
            {
                if (xreader != null)
                    xreader.Close();
                if (sreader != null)
                    sreader.Close();
            }
            return null;
        }

        public void save(string spaceDirectory)
        {
            TextWriter writer = null;
            XmlSerializer serializer = null;
            try
            {
                writer = new StreamWriter(spaceDirectory + "\\packages.xml");
                serializer = new XmlSerializer(typeof(PackageList));
                serializer.Serialize(writer, this);
            }
            finally
            {
                if(writer != null)
                {
                    writer.Close();
                }
            }
        }

        public Package getPackage(string name)
        {
            if (Packages != null && Packages.Length > 0)
            {
                foreach (Package p in Packages)
                    if (p.Name == name)
                        return p;
            }
            return null;
        }

        public Package getPackage(Guid packageID)
        {
            if (Packages != null && Packages.Length > 0)
            {
                foreach (Package p in Packages)
                {
                    if (p.ID == packageID)
                    {
                        return p;
                    }
                }
            }
            return null;
        }
    }
}
