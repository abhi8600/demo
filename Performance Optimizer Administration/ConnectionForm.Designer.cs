namespace Performance_Optimizer_Administration
{
    partial class ConnectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.connectionTabPage = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.compressedCheckBox = new System.Windows.Forms.CheckBox();
            this.connectionNameBox = new System.Windows.Forms.TextBox();
            this.lblConnectionName = new System.Windows.Forms.Label();
            this.idText = new System.Windows.Forms.TextBox();
            this.realtimeCB = new System.Windows.Forms.CheckBox();
            this.unicodeDatabaseCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dbSchema = new System.Windows.Forms.TextBox();
            this.dbDatabase = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.dbType = new System.Windows.Forms.ComboBox();
            this.maxConnectionThreads = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.dbPortBox = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.connectionPoolBox = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.dbServerName = new System.Windows.Forms.TextBox();
            this.dbUserName = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.dbPassword = new System.Windows.Forms.TextBox();
            this.connectionMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.connectionTabPage.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.connectionMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.connectionTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // connectionTabPage
            // 
            this.connectionTabPage.AutoScroll = true;
            this.connectionTabPage.Controls.Add(this.groupBox7);
            this.connectionTabPage.Location = new System.Drawing.Point(4, 22);
            this.connectionTabPage.Name = "connectionTabPage";
            this.connectionTabPage.Size = new System.Drawing.Size(844, 707);
            this.connectionTabPage.TabIndex = 12;
            this.connectionTabPage.Text = "Connection";
            this.connectionTabPage.UseVisualStyleBackColor = true;
            this.connectionTabPage.Leave += new System.EventHandler(this.connectionTabPage_Leave);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.compressedCheckBox);
            this.groupBox7.Controls.Add(this.connectionNameBox);
            this.groupBox7.Controls.Add(this.lblConnectionName);
            this.groupBox7.Controls.Add(this.idText);
            this.groupBox7.Controls.Add(this.realtimeCB);
            this.groupBox7.Controls.Add(this.unicodeDatabaseCheckBox);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.dbSchema);
            this.groupBox7.Controls.Add(this.dbDatabase);
            this.groupBox7.Controls.Add(this.label59);
            this.groupBox7.Controls.Add(this.dbType);
            this.groupBox7.Controls.Add(this.maxConnectionThreads);
            this.groupBox7.Controls.Add(this.label53);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Controls.Add(this.dbPortBox);
            this.groupBox7.Controls.Add(this.label87);
            this.groupBox7.Controls.Add(this.label135);
            this.groupBox7.Controls.Add(this.connectionPoolBox);
            this.groupBox7.Controls.Add(this.label94);
            this.groupBox7.Controls.Add(this.label86);
            this.groupBox7.Controls.Add(this.dbServerName);
            this.groupBox7.Controls.Add(this.dbUserName);
            this.groupBox7.Controls.Add(this.label84);
            this.groupBox7.Controls.Add(this.label85);
            this.groupBox7.Controls.Add(this.dbPassword);
            this.groupBox7.Location = new System.Drawing.Point(8, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(772, 279);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Connection Properties";
            // 
            // compressedCheckBox
            // 
            this.compressedCheckBox.AutoSize = true;
            this.compressedCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compressedCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.compressedCheckBox.Location = new System.Drawing.Point(9, 251);
            this.compressedCheckBox.Name = "compressedCheckBox";
            this.compressedCheckBox.Size = new System.Drawing.Size(102, 20);
            this.compressedCheckBox.TabIndex = 53;
            this.compressedCheckBox.Text = "Compressed";
            this.compressedCheckBox.UseVisualStyleBackColor = true;
            // 
            // connectionNameBox
            // 
            this.connectionNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.connectionNameBox.Location = new System.Drawing.Point(193, 58);
            this.connectionNameBox.Name = "connectionNameBox";
            this.connectionNameBox.Size = new System.Drawing.Size(208, 20);
            this.connectionNameBox.TabIndex = 51;
            // 
            // lblConnectionName
            // 
            this.lblConnectionName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectionName.Location = new System.Drawing.Point(7, 58);
            this.lblConnectionName.Name = "lblConnectionName";
            this.lblConnectionName.Size = new System.Drawing.Size(180, 23);
            this.lblConnectionName.TabIndex = 52;
            this.lblConnectionName.Text = "ConnectionName";
            // 
            // idText
            // 
            this.idText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.idText.Location = new System.Drawing.Point(193, 182);
            this.idText.Name = "idText";
            this.idText.Size = new System.Drawing.Size(168, 20);
            this.idText.TabIndex = 50;
            this.idText.Visible = false;
            // 
            // realtimeCB
            // 
            this.realtimeCB.AutoSize = true;
            this.realtimeCB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.realtimeCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.realtimeCB.Location = new System.Drawing.Point(9, 225);
            this.realtimeCB.Name = "realtimeCB";
            this.realtimeCB.Size = new System.Drawing.Size(78, 20);
            this.realtimeCB.TabIndex = 49;
            this.realtimeCB.Text = "Realtime";
            this.realtimeCB.UseVisualStyleBackColor = true;
            // 
            // unicodeDatabaseCheckBox
            // 
            this.unicodeDatabaseCheckBox.AutoSize = true;
            this.unicodeDatabaseCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.unicodeDatabaseCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F);
            this.unicodeDatabaseCheckBox.Location = new System.Drawing.Point(10, 199);
            this.unicodeDatabaseCheckBox.Name = "unicodeDatabaseCheckBox";
            this.unicodeDatabaseCheckBox.Size = new System.Drawing.Size(138, 20);
            this.unicodeDatabaseCheckBox.TabIndex = 47;
            this.unicodeDatabaseCheckBox.Text = "Unicode Database";
            this.unicodeDatabaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 23);
            this.label1.TabIndex = 27;
            this.label1.Text = "Schema (optional)";
            // 
            // dbSchema
            // 
            this.dbSchema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dbSchema.Location = new System.Drawing.Point(193, 156);
            this.dbSchema.Name = "dbSchema";
            this.dbSchema.Size = new System.Drawing.Size(168, 20);
            this.dbSchema.TabIndex = 26;
            // 
            // dbDatabase
            // 
            this.dbDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dbDatabase.FormattingEnabled = true;
            this.dbDatabase.Location = new System.Drawing.Point(427, 15);
            this.dbDatabase.Name = "dbDatabase";
            this.dbDatabase.Size = new System.Drawing.Size(340, 21);
            this.dbDatabase.TabIndex = 16;
            this.dbDatabase.DropDown += new System.EventHandler(this.dbDatabase_DropDown);
            // 
            // label59
            // 
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(458, 112);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(160, 20);
            this.label59.TabIndex = 25;
            this.label59.Text = "Maximum Threads";
            // 
            // dbType
            // 
            this.dbType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dbType.Items.AddRange(new object[] {
            "MS SQL Server 2000",
            "MS SQL Server 2005",
            "MS SQL Server 2008",
            "MS SQL Server 2012",
            "MS SQL Server 2014",
            "Microsoft SQL Server ODBC",
            "MySQL",
            "MySQL ODBC",
            "Oracle 11g",
            "Oracle ODBC",
            "DB2 ODBC",
            "Infobright",
            "Google Analytics",
            "Microsoft Excel ODBC",
            "Vertica",
            "SAP Hana",
            "Teradata",
            "PostgreSQL",
            "MemDB",
            "Sybase IQ",
            "ParAccel",
            "Hive/Hadoop",
            "Impala",
            "Redshift",
            "None"});
            this.dbType.Location = new System.Drawing.Point(117, 14);
            this.dbType.Name = "dbType";
            this.dbType.Size = new System.Drawing.Size(168, 21);
            this.dbType.TabIndex = 1;
            this.dbType.SelectedIndexChanged += new System.EventHandler(this.dbType_SelectedIndexChanged);
            // 
            // maxConnectionThreads
            // 
            this.maxConnectionThreads.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxConnectionThreads.Location = new System.Drawing.Point(633, 132);
            this.maxConnectionThreads.Name = "maxConnectionThreads";
            this.maxConnectionThreads.Size = new System.Drawing.Size(56, 20);
            this.maxConnectionThreads.TabIndex = 23;
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(7, 15);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(104, 23);
            this.label53.TabIndex = 7;
            this.label53.Text = "Database Type";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(503, 132);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 23);
            this.label20.TabIndex = 24;
            this.label20.Text = "Connection";
            // 
            // dbPortBox
            // 
            this.dbPortBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dbPortBox.Location = new System.Drawing.Point(633, 90);
            this.dbPortBox.Name = "dbPortBox";
            this.dbPortBox.Size = new System.Drawing.Size(56, 20);
            this.dbPortBox.TabIndex = 18;
            // 
            // label87
            // 
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(303, 16);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(131, 21);
            this.label87.TabIndex = 13;
            this.label87.Text = "Database/Catalog";
            // 
            // label135
            // 
            this.label135.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(457, 90);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(168, 23);
            this.label135.TabIndex = 17;
            this.label135.Text = "Database Port";
            // 
            // connectionPoolBox
            // 
            this.connectionPoolBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.connectionPoolBox.Location = new System.Drawing.Point(633, 58);
            this.connectionPoolBox.Name = "connectionPoolBox";
            this.connectionPoolBox.Size = new System.Drawing.Size(56, 20);
            this.connectionPoolBox.TabIndex = 6;
            // 
            // label94
            // 
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(457, 58);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(168, 23);
            this.label94.TabIndex = 16;
            this.label94.Text = "User Connection Pool Size";
            // 
            // label86
            // 
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(7, 133);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(88, 23);
            this.label86.TabIndex = 10;
            this.label86.Text = "Password";
            // 
            // dbServerName
            // 
            this.dbServerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dbServerName.Location = new System.Drawing.Point(193, 87);
            this.dbServerName.Name = "dbServerName";
            this.dbServerName.Size = new System.Drawing.Size(208, 20);
            this.dbServerName.TabIndex = 2;
            // 
            // dbUserName
            // 
            this.dbUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dbUserName.Location = new System.Drawing.Point(193, 110);
            this.dbUserName.Name = "dbUserName";
            this.dbUserName.Size = new System.Drawing.Size(168, 20);
            this.dbUserName.TabIndex = 4;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(7, 87);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(180, 23);
            this.label84.TabIndex = 5;
            this.label84.Text = "Server Name/Address/DSN";
            // 
            // label85
            // 
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(7, 110);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(80, 23);
            this.label85.TabIndex = 9;
            this.label85.Text = "Username";
            // 
            // dbPassword
            // 
            this.dbPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dbPassword.Location = new System.Drawing.Point(193, 133);
            this.dbPassword.Name = "dbPassword";
            this.dbPassword.PasswordChar = '*';
            this.dbPassword.Size = new System.Drawing.Size(168, 20);
            this.dbPassword.TabIndex = 5;
            // 
            // connectionMenu
            // 
            this.connectionMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConnectionToolStripMenuItem,
            this.removeConnectionToolStripMenuItem});
            this.connectionMenu.Name = "successModelMenu";
            this.connectionMenu.ShowImageMargin = false;
            this.connectionMenu.Size = new System.Drawing.Size(158, 48);
            // 
            // addConnectionToolStripMenuItem
            // 
            this.addConnectionToolStripMenuItem.Name = "addConnectionToolStripMenuItem";
            this.addConnectionToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.addConnectionToolStripMenuItem.Text = "Add Connection";
            this.addConnectionToolStripMenuItem.Click += new System.EventHandler(this.addConnectionToolStripMenuItem_Click);
            // 
            // removeConnectionToolStripMenuItem
            // 
            this.removeConnectionToolStripMenuItem.Name = "removeConnectionToolStripMenuItem";
            this.removeConnectionToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.removeConnectionToolStripMenuItem.Text = "Remove Connection";
            this.removeConnectionToolStripMenuItem.Click += new System.EventHandler(this.removeConnectionToolStripMenuItem_Click);
            // 
            // ConnectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "ConnectionForm";
            this.Text = "Connection";
            this.tabControl.ResumeLayout(false);
            this.connectionTabPage.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.connectionMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage connectionTabPage;
        private System.Windows.Forms.ComboBox dbType;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox dbPortBox;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox connectionPoolBox;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox dbServerName;
        private System.Windows.Forms.TextBox dbUserName;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox dbPassword;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox maxConnectionThreads;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox dbDatabase;
        private System.Windows.Forms.ContextMenuStrip connectionMenu;
        private System.Windows.Forms.ToolStripMenuItem addConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeConnectionToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dbSchema;
        private System.Windows.Forms.CheckBox unicodeDatabaseCheckBox;
        private System.Windows.Forms.CheckBox realtimeCB;
        private System.Windows.Forms.TextBox idText;
        private System.Windows.Forms.TextBox connectionNameBox;
        private System.Windows.Forms.Label lblConnectionName;
        private System.Windows.Forms.CheckBox compressedCheckBox;
    }
}