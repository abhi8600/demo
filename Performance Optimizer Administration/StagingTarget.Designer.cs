namespace Performance_Optimizer_Administration
{
    partial class StagingTarget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StagingTarget));
            this.targetListBox = new System.Windows.Forms.CheckedListBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // targetListBox
            // 
            this.targetListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetListBox.CheckOnClick = true;
            this.targetListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.targetListBox.FormattingEnabled = true;
            this.targetListBox.Location = new System.Drawing.Point(0, 0);
            this.targetListBox.Name = "targetListBox";
            this.targetListBox.Size = new System.Drawing.Size(151, 122);
            this.targetListBox.TabIndex = 2;
            this.targetListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.targetListBox_ItemCheck);
            this.targetListBox.MouseLeave += new System.EventHandler(this.targetListBox_MouseLeave);
            this.targetListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.targetListBox_KeyDown);
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(37, 131);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(50, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(97, 131);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(50, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // StagingTarget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(155, 155);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.targetListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StagingTarget";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Targets for Staging Column";
            this.Leave += new System.EventHandler(this.StagingTarget_Leave);
            this.MouseLeave += new System.EventHandler(this.StagingTarget_Leave);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StagingTarget_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.CheckedListBox targetListBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
    }
}