using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Performance : Form, RepositoryModule
    {
        List<QueryFilter> filterList;
        MainAdminForm ma;
        string moduleName = "Performance";
        string categoryName = "Business Modeling";
        ClassifierSettings cSettings;

        public Performance(MainAdminForm ma)
        {
            this.ma = ma;
            InitializeComponent();
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            PerformanceModel model = new PerformanceModel();
            if (r.PerformanceModels == null || r.PerformanceModels.Length == 0)
            {
                r.PerformanceModels = new PerformanceModel[1];
            }
            r.PerformanceModels[0] = model;

            model.Name = "Default Performance Model";
            model.ModelFile = perfOutputFile.Text;
            model.OptimizationGoal = goalMeasure.Text;
            model.PerformanceOutputTable = perfOutputTable.Text;
            model.DropExistingTable = perfDropTable.Checked;
            try
            {
                model.PerformanceImpactPercent = double.Parse(impactPercent.Text);
            }
            catch (Exception)
            {
                model.PerformanceImpactPercent = 5.0;
            }
            model.TargetType = targetImp.Checked ? 0 : 1;
            try
            {
                model.NumMeasuresToOptimize = Int32.Parse(numOptimizeMeasures.Text);
                model.MinTargetImprovement = Double.Parse(minTargetChange.Text);
                model.TargetImprovement = Double.Parse(targetImprovement.Text);
                model.TargetProbability = Double.Parse(targetProbability.Text);
                model.TargetMinimum = Int32.Parse(minTarget.Text);
                model.NumIterations = Double.Parse(numIterations.Text);
                model.TargetAccuracy = Double.Parse(targetAccuracy.Text);
                model.DampeningFactor = Double.Parse(dampenFactor.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Parsing Performance Settings");
                model.NumMeasuresToOptimize = 5;
                model.MinTargetImprovement = 2.5;
                model.TargetImprovement = 25;
                model.TargetProbability = 50;
                model.TargetMinimum = 0;
                model.NumIterations = 1;
                model.TargetAccuracy = 100;
                model.DampeningFactor = 0;
            }
            model.Measures = new PerformanceMeasure[selectedMeasures.Items.Count];
            // Ensure always saved in the same order
            SortedList slist = new SortedList();
            for (int count = 0; count < selectedMeasures.Items.Count; count++)
            {
                ListViewItem lvi = selectedMeasures.Items[count];
                if (!slist.Contains(lvi.SubItems[0].Text))
                    slist.Add(lvi.SubItems[0].Text, lvi);
            }
            for (int count = 0; count < slist.Count; count++)
            {
                ListViewItem lvi = (ListViewItem)slist.GetByIndex(count);
                PerformanceMeasure pm = new PerformanceMeasure();
                model.Measures[count] = pm;
                pm.Name = lvi.SubItems[0].Text;
                try
                {
                    pm.Bucket = Int32.Parse(lvi.SubItems[2].Text);
                }
                catch (Exception) { }
                pm.BlockGroup = lvi.SubItems[3].Text;
                pm.CoverGroup = lvi.SubItems[4].Text;
                pm.ModelFlag = lvi.Checked;
                pm.BaseCaseMeasure = (Boolean.TrueString == lvi.SubItems[6].Text);
                pm.Optimize = (Boolean.TrueString == lvi.SubItems[7].Text);
                try
                {
                    pm.NumInCoverGroup = Int32.Parse(lvi.SubItems[5].Text);
                }
                catch (Exception) { }
            }
            ArrayList pfList = new ArrayList();
            foreach (string s in performanceFilterListBox.CheckedItems) if (!pfList.Contains(s)) pfList.Add(s);
            model.PerformanceFilters = (string[])pfList.ToArray(typeof(string));
            model.PerformanceIterationDimension = perfIterateDim.Text;
            model.PerformanceIterationDefaultAttribute = perfIterateDefaultAttribute.Text;
            model.PerformanceIterationModelValue = perfIterateModelValue.Text;
            model.PerformanceIterationAnalysisValue = perfIterateAnalysisValue.Text;
            model.PerformanceLogRelationship = logRelButton.Checked;
            model.Optimize = perfOptimize.Checked;
            model.SearchPatterns = perfSearchPatterns.Checked;
            model.SearchPatternDefinition = perfSearchPattern.Text;
            model.ReferencePopulation = referencePopulationBox.Text;
            model.Classifiers = cSettings;
        }

        public void updateFromRepositoryFinal(Repository r)
        {
            if (r == null) return;

            if (r.PerformanceModels == null || r.PerformanceModels.Length == 0)
            {
                r.PerformanceModels = new PerformanceModel[1];
                r.PerformanceModels[0] = r.Performance;
            }
            PerformanceModel model = r.PerformanceModels[0];

            //Setup based on measures added
            if (r != null && model.Measures != null)
            {
                List<string>[] lists = ma.getMeasureList(null);
                List<string> mList = lists[0];
                List<string> catList = lists[1];
                for (int count = 0; count < model.Measures.Length; count++)
                {
                    PerformanceMeasure pm = model.Measures[count];
                    string s = pm.Name;
                    int index = mList.IndexOf(s);
                    string bg = "";
                    string cg = "";
                    if (pm.BlockGroup != null)
                        bg = pm.BlockGroup;
                    if (pm.CoverGroup != null)
                        cg = pm.CoverGroup;
                    string cat = "";
                    if ((index >= 0) && (index < catList.Count))
                    {
                        cat = (string)catList[index];
                    }

                    ListViewItem newlvi = new ListViewItem(
                        new string[] { s, cat, pm.Bucket.ToString(), bg, cg, pm.NumInCoverGroup.ToString(), pm.BaseCaseMeasure.ToString(), pm.Optimize.ToString() });
                    newlvi.Checked = pm.ModelFlag;
                    selectedMeasures.Items.Add(newlvi);
                }
            }
        }
        public void updateFromRepository(Repository r)
        {
            if (r == null) return;

            if (r.PerformanceModels == null || r.PerformanceModels.Length == 0)
            {
                r.PerformanceModels = new PerformanceModel[1];
                r.PerformanceModels[0] = r.Performance;
            }
            PerformanceModel model = r.PerformanceModels[0];
            if (r != null && model != null)
            {
                perfOutputTable.Text = model.PerformanceOutputTable;
                perfOutputFile.Text = model.ModelFile;
                perfDropTable.Checked = model.DropExistingTable;
                impactPercent.Text = model.PerformanceImpactPercent.ToString();
                selectedMeasures.Items.Clear();
                perfIterateDim.Text = model.PerformanceIterationDimension;
                perfIterateDefaultAttribute.Text = model.PerformanceIterationDefaultAttribute;
                perfIterateModelValue.Text = model.PerformanceIterationModelValue;
                perfIterateAnalysisValue.Text = model.PerformanceIterationAnalysisValue;
                logRelButton.Checked = model.PerformanceLogRelationship;
                numOptimizeMeasures.Text = model.NumMeasuresToOptimize.ToString();
                targetImp.Checked = (model.TargetType == 0);
                targetProb.Checked = (model.TargetType == 1);
                targetImprovement.Text = model.TargetImprovement.ToString();
                targetProbability.Text = model.TargetProbability.ToString();
                minTarget.Text = model.TargetMinimum.ToString();
                minTargetChange.Text = model.MinTargetImprovement.ToString();
                numIterations.Text = model.NumIterations.ToString();
                targetAccuracy.Text = model.TargetAccuracy.ToString();
                dampenFactor.Text = model.DampeningFactor.ToString();
                perfOptimize.Checked = model.Optimize;
                perfSearchPatterns.Checked = model.SearchPatterns;
                perfSearchPattern.Text = model.SearchPatternDefinition;
                referencePopulationBox.Text = model.ReferencePopulation;
                goalMeasure.Text = model.OptimizationGoal;
                cSettings = model.Classifiers;
            }
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
            if (r == null) return;

            PerformanceModel model = null;
            if (r.PerformanceModels == null)
            {
                model = new PerformanceModel();
            }
            else
            {
                model = r.PerformanceModels[0];
            }
            List<QueryFilter> sortedList = QueryFilter.sortListAsUsed(list, model.PerformanceFilters);
            this.filterList = sortedList;
            if (r != null && model.PerformanceFilters != null)
            {
                performanceFilterListBox.Tag = new ArrayList(model.PerformanceFilters);
                foreach (QueryFilter f in sortedList)
                {
                    performanceFilterListBox.Items.Add(f.Name);
                    if (((ArrayList)performanceFilterListBox.Tag).Contains(f.Name))
                        performanceFilterListBox.SetItemChecked(performanceFilterListBox.Items.Count - 1, true);
                }
            }
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void setup()
        {
            goalMeasure.Items.Clear();
            List<string> list2 = ma.getMeasureList(null)[0];
            foreach (string s in list2)
            {
                if (s != null && !goalMeasure.Items.Contains(s))
                    goalMeasure.Items.Add(s);
            }
            availableMeasures.BeginUpdate();
            performanceFilterListBox.BeginUpdate();
            if (ma.dimMeasureLists[goalMeasure.Text] == null)
            {
                availableMeasures.Items.Clear();
                List<string>[] lists = ma.getMeasureList(ma.targetDimension.Text);
                List<string> mlist = lists[0];
                List<string> catList = lists[1];
                for (int i = 0; i < catList.Count; i++)
                {
                    string name = (string)mlist[i];
                    string category = (string)catList[i];
                    ListViewItem lvi = new ListViewItem(new string[] { name, category });
                    availableMeasures.Items.Add(lvi);
                }
                availableMeasures.Sorting = SortOrder.Ascending;
                // Go through existing performance measures and update categories if necessary
                foreach (ListViewItem lvi in selectedMeasures.Items)
                {
                    string name = lvi.SubItems[0].Text;
                    int index = mlist.IndexOf(name);
                    if (index >= 0)
                    {
                        lvi.SubItems[1].Text = (string)catList[index];
                    }
                    else
                    {
                        // Bad measure - so need to remove
                        selectedMeasures.Items.Remove(lvi);
                    }
                }
            }
            ArrayList checkedFilters = new ArrayList();
            foreach (string s in performanceFilterListBox.CheckedItems)
            {
                checkedFilters.Add(s);
            }

            performanceFilterListBox.Items.Clear();
            filterList = QueryFilter.sortListAsUsed(filterList, (string[])checkedFilters.ToArray(typeof(string)));
            if (filterList != null)
                QueryFilter.sortedListBox(performanceFilterListBox, filterList, (string[])checkedFilters.ToArray(typeof(string)));
               
            List<string> dlist = ma.getDimensionList();
            perfIterateDefaultAttribute.Items.Clear();
            perfIterateDim.Items.Clear();
            foreach (string d in dlist)
            {
                perfIterateDim.Items.Add(d);
            }
            setPerfIterateAttributes();
            // Populate search pattern definitions
            perfSearchPattern.Items.Clear();
            foreach (PatternSearchDefinition psd in ma.patterns.definitions)
            {
                perfSearchPattern.Items.Add(psd.Name);
            }
            // Populate reference populations
            referencePopulationBox.Items.Clear();
            foreach (ReferencePopulation refPop in ma.refPopModule.refPopList)
            {
                referencePopulationBox.Items.Add(refPop.PopulationName);
            }
            availableMeasures.EndUpdate();
            performanceFilterListBox.EndUpdate();
        }

        public string ModuleName
        {
            get
            {
                return (moduleName);
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return (categoryName);
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return (this.performanceTabPage);
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            for (int count = 0; count < selectedMeasures.Items.Count; count++)
            {
                ListViewItem lvi = selectedMeasures.Items[count];
                if (match)
                {
                    if (lvi.SubItems[0].Text == find) lvi.SubItems[0].Text = replace;
                }
                else
                {
                    lvi.SubItems[0].Text = lvi.SubItems[0].Text.Replace(find, replace);
                }
            }
            if (match)
            {
                if (perfIterateDefaultAttribute.Text == find) perfIterateDefaultAttribute.Text = replace;
            }
            else
            {
                perfIterateDefaultAttribute.Text = perfIterateDefaultAttribute.Text.Replace(find, replace);
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            for (int count = 0; count < selectedMeasures.Items.Count; count++)
            {
                ListViewItem lvi = selectedMeasures.Items[count];
                if (match)
                {
                    if (lvi.SubItems[0].Text == find) results.Add("Performance - Measure");
                }
                else
                {
                    if (lvi.SubItems[0].Text.IndexOf(find) >= 0) results.Add("Performance - Measure");
                }
            }
            if (match)
            {
                if (perfIterateDefaultAttribute.Text == find) results.Add("Performance - Iterate attribute");
            }
            else
            {
                if (perfIterateDefaultAttribute.Text.IndexOf(find) >= 0) results.Add("Performance - Iterate attribute");
            }
        }

        public void implicitSave()
        {
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        private void selectedMeasures_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            selectedMeasures.ListViewItemSorter = new MainAdminForm.Listcomparer(e.Column);
            selectedMeasures.Sort();
        }

        private void availableMeasures_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            availableMeasures.ListViewItemSorter = new MainAdminForm.Listcomparer(e.Column);
            availableMeasures.Sort();
        }

        private void setPerfAttributes_Click(object sender, System.EventArgs e)
        {
            selectedMeasures.BeginUpdate();
            foreach (ListViewItem lvi in selectedMeasures.SelectedItems)
            {
                if (numPerfBuckets.Text.Length > 0)
                    lvi.SubItems[2].Text = numPerfBuckets.Text;
                if (blockGroup.Text.Length > 0)
                    lvi.SubItems[3].Text = blockGroup.Text;
                if (coverGroup.Text.Length > 0)
                    lvi.SubItems[4].Text = coverGroup.Text;
                if (numToCover.Text.Length > 0)
                    lvi.SubItems[5].Text = numToCover.Text;
                if (baseCaseMeasure.Text.Length > 0)
                    lvi.SubItems[6].Text = baseCaseMeasure.Text;
                if (optimizeMeasure.Text.Length > 0)
                    lvi.SubItems[7].Text = optimizeMeasure.Text;
            }
            selectedMeasures.EndUpdate();
        }

        private void addBusMeasure_Click(object sender, System.EventArgs e)
        {
            ListViewItem[] lviList = new ListViewItem[availableMeasures.SelectedItems.Count];
            for (int count = 0; count < availableMeasures.SelectedItems.Count; count++)
            {
                lviList[count] = availableMeasures.SelectedItems[count];
            }
            foreach (ListViewItem lvi in lviList)
            {
                availableMeasures.Items.Remove(lvi);
                String baseStr = baseCaseMeasure.Text;
                if (baseStr.Length == 0) baseStr = "False";
                String optStr = optimizeMeasure.Text;
                if (optStr.Length == 0) optStr = "True";
                ListViewItem newlvi = new ListViewItem(
                    new string[] {lvi.SubItems[0].Text,lvi.SubItems[1].Text,numPerfBuckets.Text,
									 blockGroup.Text,coverGroup.Text,numToCover.Text, baseStr, optStr});
                newlvi.Checked = true;
                selectedMeasures.Items.Add(newlvi);
            }
        }

        /*
         * Set attributes for a given measure
         * */
        private void setMeasureAttributesButton_Click(object sender, System.EventArgs e)
        {
            if (selectedMeasures.SelectedIndices.Count == 0) return;
            string measure = selectedMeasures.SelectedItems[0].SubItems[0].Text;
            MeasureAttributeForm maf = new MeasureAttributeForm(measure);
            for (int ccount = 0; ccount < ma.measureColumnsTable.Rows.Count; ccount++)
            {
                DataRow dr = ma.measureColumnsTable.Rows[ccount];
                if (((string)dr["ColumnName"]) == measure)
                {
                    if ((dr["Opportunity"] != System.DBNull.Value) && (((string)dr["Opportunity"]).Length > 0))
                        maf.textBox.Text = (string)dr["Opportunity"];
                    if ((dr["Minimum"] != System.DBNull.Value) && (((string)dr["Minimum"]).Length > 0))
                        maf.minBox.Text = (string)dr["Minimum"];
                    if ((dr["Maximum"] != System.DBNull.Value) && (((string)dr["Maximum"]).Length > 0))
                        maf.maxBox.Text = (string)dr["Maximum"];
                    if ((dr["MinTarget"] != System.DBNull.Value) && (((string)dr["MinTarget"]).Length > 0))
                        maf.minTargetBox.Text = (string)dr["MinTarget"];
                    if ((dr["MaxTarget"] != System.DBNull.Value) && (((string)dr["MaxTarget"]).Length > 0))
                        maf.maxTargetBox.Text = (string)dr["MaxTarget"];
                    if ((dr["MinImprovement"] != System.DBNull.Value) && (((string)dr["MinImprovement"]).Length > 0))
                        maf.minImpBox.Text = (string)dr["MinImprovement"];
                    if ((dr["MaxImprovement"] != System.DBNull.Value) && (((string)dr["MaxImprovement"]).Length > 0))
                        maf.maxImpBox.Text = (string)dr["MaxImprovement"];
                    if ((dr["ValidMeasure"] != System.DBNull.Value) && (((string)dr["ValidMeasure"]).Length > 0))
                        maf.validBox.Text = (string)dr["ValidMeasure"];
                    if ((dr["AttributeWeightMeasure"] != System.DBNull.Value) && (((string)dr["AttributeWeightMeasure"]).Length > 0))
                        maf.weightBox.Text = (string)dr["AttributeWeightMeasure"];
                    if ((dr["AnalysisMeasure"] != System.DBNull.Value) && (((string)dr["AnalysisMeasure"]).Length > 0))
                        maf.analysisMeasureBox.Text = (string)dr["AnalysisMeasure"];
                    if ((dr["SegmentMeasure"] != System.DBNull.Value) && (((string)dr["SegmentMeasure"]).Length > 0))
                        maf.segmentMeasureBox.Text = (string)dr["SegmentMeasure"];
                    if ((dr["Format"] != System.DBNull.Value) && (((string)dr["Format"]).Length > 0))
                        maf.formatBox.Text = (string)dr["Format"];
                    maf.impDirBox.Text = dr["Improve"].ToString();
                    maf.enforceBox.Checked = (bool)dr["EnforceLimits"];
                }
            }
            DialogResult diar = maf.ShowDialog();
            if (diar == DialogResult.OK)
            {
                for (int ccount = 0; ccount < ma.measureColumnsTable.Rows.Count; ccount++)
                {
                    DataRow dr = ma.measureColumnsTable.Rows[ccount];
                    if (((string)dr["ColumnName"]) == measure)
                    {
                        dr["Opportunity"] = maf.textBox.Text;
                        dr["Minimum"] = maf.minBox.Text;
                        dr["Maximum"] = maf.maxBox.Text;
                        dr["MinTarget"] = maf.minTargetBox.Text;
                        dr["MaxTarget"] = maf.maxTargetBox.Text;
                        dr["MinImprovement"] = maf.minImpBox.Text;
                        dr["MaxImprovement"] = maf.maxImpBox.Text;
                        dr["ValidMeasure"] = maf.validBox.Text;
                        dr["AttributeWeightMeasure"] = maf.weightBox.Text;
                        dr["AnalysisMeasure"] = maf.analysisMeasureBox.Text;
                        dr["SegmentMeasure"] = maf.segmentMeasureBox.Text;
                        if (maf.impDirBox.Text.Length > 0)
                        {
                            try
                            {
                                dr["Improve"] = Int32.Parse(maf.impDirBox.Text);
                            }
                            catch (Exception) { }
                        }
                        if (maf.formatBox.Text.Length > 0)
                            dr["Format"] = maf.formatBox.Text;
                        dr["EnforceLimits"] = maf.enforceBox.Checked;
                    }
                }
            }
        }

        private void removeBusMeasure_Click(object sender, System.EventArgs e)
        {
            ListViewItem[] lviList = new ListViewItem[selectedMeasures.SelectedItems.Count];
            for (int count = 0; count < selectedMeasures.SelectedItems.Count; count++)
            {
                lviList[count] = selectedMeasures.SelectedItems[count];
            }
            foreach (ListViewItem lvi in lviList)
            {
                selectedMeasures.Items.Remove(lvi);
                availableMeasures.Items.Add(new ListViewItem(
                    new string[] { lvi.SubItems[0].Text, lvi.SubItems[1].Text }));
            }
            setup();
        }
        private void setPerfIterateAttributes()
        {
            List<string> clist = ma.getDimensionColumnList(perfIterateDim.Text);
            perfIterateDefaultAttribute.Items.Clear();
            foreach (string c in clist)
            {
                perfIterateDefaultAttribute.Items.Add(c);
            }
        }

        private void perfIterateDim_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            setPerfIterateAttributes();
        }

        private void classifierSettings_Click(object sender, EventArgs e)
        {
            BestFitClassifierDialog bfcd = new BestFitClassifierDialog(ma, cSettings == null ? 
                ma.modelMod.classifierDialog.getClassifierSettings() : cSettings, true);
            bfcd.ShowDialog();
            if (bfcd.isDefault)
                cSettings = null;
            else
                cSettings = bfcd.getClassifierSettings();
        }

        private void Performance_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

    }
}