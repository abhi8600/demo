using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Performance_Optimizer_Administration
{
	/// <summary>
	/// Summary description for ImportTable.
	/// </summary>
	public class ImportTable : System.Windows.Forms.Form
	{
        public ImportTableResult result = new ImportTableResult();
        private string serverURL;
        public System.Windows.Forms.RadioButton dimensionButton;
        public RadioButton factButton;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private TreeView treeView;
        private ComboBox importAsBox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private List<string> measureTableDefinitions;
        private List<string> dimensionTableDefinitions;
        private string username;
        private RadioButton bothButton;
        private string password;

        public ImportTable(string serverURL, string username, string password, List<string> measureTableDefinitions, List<string> dimensionTableDefinitions)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.username = username;
            this.password = password;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            this.serverURL = serverURL;
            // Get the catalogs available
            try
            {
                List<string[]> catalogs = Server.getCatalogs(null, serverURL, username, password);
                if (catalogs != null)
                {
                    treeView.Nodes.Clear();
                    foreach (string[] catalogResult in catalogs)
                    {
                            List<String> tables = Server.getTables(catalogResult[0], serverURL, catalogResult[1], username, password);
                            if (tables != null)
                            {
                                TreeNode node = new TreeNode(catalogResult[1]);
                                node.Tag = catalogResult[0];
                                treeView.Nodes.Add(node);
                                    foreach (String s in tables)
                                    {
                                        TreeNode tableNode = new TreeNode(s);
                                        node.Nodes.Add(tableNode);
                                    }
                            }
                   
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString());
            }
            if (measureTableDefinitions != null || dimensionTableDefinitions != null)
            {
                importAsBox.Items.Clear();
                if (measureTableDefinitions != null)
                    foreach (string s in measureTableDefinitions)
                        if (s != null) importAsBox.Items.Add(s);
                if (dimensionTableDefinitions != null)
                    foreach (string s in dimensionTableDefinitions)
                        if (s != null) importAsBox.Items.Add(s);
            }
            this.measureTableDefinitions = measureTableDefinitions;
            this.dimensionTableDefinitions = dimensionTableDefinitions;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportTable));
            this.dimensionButton = new System.Windows.Forms.RadioButton();
            this.factButton = new System.Windows.Forms.RadioButton();
            this.OKButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.treeView = new System.Windows.Forms.TreeView();
            this.importAsBox = new System.Windows.Forms.ComboBox();
            this.bothButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // dimensionButton
            // 
            this.dimensionButton.Checked = true;
            this.dimensionButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimensionButton.Location = new System.Drawing.Point(178, 246);
            this.dimensionButton.Name = "dimensionButton";
            this.dimensionButton.Size = new System.Drawing.Size(80, 24);
            this.dimensionButton.TabIndex = 3;
            this.dimensionButton.TabStop = true;
            this.dimensionButton.Text = "Dimension";
            // 
            // factButton
            // 
            this.factButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.factButton.Location = new System.Drawing.Point(252, 245);
            this.factButton.Name = "factButton";
            this.factButton.Size = new System.Drawing.Size(50, 24);
            this.factButton.TabIndex = 4;
            this.factButton.Text = "Fact";
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Enabled = false;
            this.OKButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKButton.Location = new System.Drawing.Point(178, 278);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(283, 278);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Import As Table Definition";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 246);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "Import As Definition Type";
            // 
            // treeView
            // 
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(7, 7);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(351, 198);
            this.treeView.TabIndex = 9;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // importAsBox
            // 
            this.importAsBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.importAsBox.FormattingEnabled = true;
            this.importAsBox.Location = new System.Drawing.Point(178, 221);
            this.importAsBox.Name = "importAsBox";
            this.importAsBox.Size = new System.Drawing.Size(180, 21);
            this.importAsBox.TabIndex = 10;
            this.importAsBox.SelectionChangeCommitted += new System.EventHandler(this.importAsBox_SelectionChangeCommitted);
            this.importAsBox.SelectedIndexChanged += new System.EventHandler(this.importAsBox_SelectedIndexChanged);
            // 
            // bothButton
            // 
            this.bothButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bothButton.Location = new System.Drawing.Point(308, 246);
            this.bothButton.Name = "bothButton";
            this.bothButton.Size = new System.Drawing.Size(50, 24);
            this.bothButton.TabIndex = 11;
            this.bothButton.Text = "Both";
            // 
            // ImportTable
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(370, 309);
            this.Controls.Add(this.bothButton);
            this.Controls.Add(this.importAsBox);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.factButton);
            this.Controls.Add(this.dimensionButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Table";
            this.ResumeLayout(false);

		}
		#endregion

        private void OKButton_Click(object sender, System.EventArgs e)
        {
            result.ConnectionName = (string)treeView.SelectedNode.Parent.Tag;
            result.CatalogName = treeView.SelectedNode.Parent.Text;
            result.TableName = treeView.SelectedNode.Text;
            result.ImportAsName = importAsBox.Text;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback +=
                delegate(object senderObj, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
                SMI.CommandProcessor cps = new SMI.CommandProcessor();
                String token = Util.getAuthenticationToken(serverURL, username);
                cps.Url = serverURL + "/services/CommandProcessor.CommandProcessorHttpSoap11Endpoint?ssoUser=" + token;
                string schema = cps.getTableSchema(username, password, result.ConnectionName, result.CatalogName, result.TableName);
                if (schema == null)
                {
                    throw new Exception("No schema returned");
                }
                if (schema.StartsWith("ERROR:"))
                {
                    throw new Exception(schema);
                }
                string line = null;
                Boolean done = false;
                result.ColumnNames = new ArrayList();
                result.ColumnTypes = new ArrayList();
                while (!done)
                {
                    int idx = schema.IndexOf('\n');
                    if (idx >= 0)
                    {
                        line = schema.Substring(0, schema.IndexOf('\n'));
                    }
                    else
                    {
                        line = schema;
                        done = true;
                    }
                    schema = schema.Substring(schema.IndexOf('\n') + 1);

                    int p1 = line.IndexOf('\t');
                    int p2 = line.IndexOf('\t', p1 + 1);
                    int p3 = line.IndexOf('\t', p2 + 1);
                    if ((p1 < 0) || (p2 < 0) || (p3 < 0)) MessageBox.Show(this, "Error reading schema from server: " + line);
                    else
                    {
                        string s1 = line.Substring(0, p1);
                        string s2 = line.Substring(p1 + 1, p2 - p1 - 1);
                        string s3 = line.Substring(p2 + 1, p3 - p2 - 1);
                        result.ColumnNames.Add(s2);
                        result.ColumnTypes.Add(Int32.Parse(s3));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString());
                result.TableName = null;
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView.SelectedNode.Level == 1)
            {
                OKButton.Enabled = true;
            }
        }

        private void importAsBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
        }

        private void importAsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (string s in measureTableDefinitions)
            {
                if (importAsBox.Text == s)
                {
                    factButton.Checked = true;
                    return;
                }
            }
            foreach (string s in dimensionTableDefinitions)
                if (importAsBox.Text == s)
                {
                    dimensionButton.Checked = true;
                    return;
                }
        }
	}

    public class ImportTableResult
    {
        public string ConnectionName;
        public string CatalogName;
        public string TableName;
        public string ImportAsName;
        public ArrayList ColumnNames;
        public ArrayList ColumnTypes;
    }
}
