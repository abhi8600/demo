using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Hierarchies : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Hierarchies";
        string categoryName = "Dimensional Mapping";
        Repository r;
        HierarchyList hierarchies;

        public Hierarchies(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            hierarchies = new HierarchyList(ma.getLogger());
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.Hierarchies = hierarchies.hierarchies;
        }

        public Boolean updateGuids()
        {
            return hierarchies.updateGuids();
        }

        public void updateFromRepository(Repository r)
        {
            selNode = null;
            if (rootNode != null)
                rootNode.Nodes.Clear();
        }

        public void updateFromRepositoryFinal(Repository r)
        {
            this.r = r;
            if (r != null)
            {
                StringBuilder sb = new StringBuilder();
                bool firstT = true;
                foreach (Hierarchy h in r.Hierarchies)
                {
                    if (h.DimensionKeyLevel == null || h.DimensionKeyLevel.Trim().Equals(""))
                    {
                        if (firstT)
                        {
                            firstT = false;
                            sb.Append(h.DimensionName);
                        }
                        else
                        {
                            sb.Append(", "+h.DimensionName);
                        }
                    }
                }
                string missingDimKeyLevels = sb.ToString();
                if (!missingDimKeyLevels.Equals(""))
                {
                    ma.reportMessage(null, null, "Blank dimension key level detected for following dimensional hierarchies: " + missingDimKeyLevels + " - Please fix.", 
                        DialogResult.OK, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            if (rootNode != null)
                this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            bool addToVariableList = false;
            if (hierarchies == null || hierarchies.getCount() == 0)
            {
                addToVariableList = true;
            }
            if (r != null && r.Hierarchies != null)
            {
                for (int i = 0; i < r.Hierarchies.Length; i++)
                {
                    TreeNode n = new TreeNode(r.Hierarchies[i].Name);
                    n.Tag = r.Hierarchies[i];
                    rootNode.Nodes.Add(n);
                    n.ImageIndex = 7;
                    n.SelectedImageIndex = 7;
                    addNodes(n, r.Hierarchies[i].Children);
                    if (addToVariableList)
                    {
                        hierarchies.updateItemWithoutLock(r.Hierarchies[i], null, null, ma);
                    }
                }
            }
        }

        public void replaceHierarchy(Hierarchy h)
        {
            replaceHierarchy(0, h);
        }

        public void replaceHierarchy(int index, Hierarchy h)
        {
            hierarchies.updateItem(h, "", ma);
            this.r = ma.getOpenedRepository();
            if (this.r != null)
                this.r.Hierarchies = hierarchies.hierarchies;
            setupNodes(rootNode);
        }

        public void replaceHierarchy(Hierarchy oldH, Hierarchy newH)
        {
            hierarchies.Remove(oldH);
            hierarchies.updateItem(newH, "", ma);
            this.r = ma.getOpenedRepository();
            if (this.r != null)
                this.r.Hierarchies = hierarchies.hierarchies;
            setupNodes(rootNode);
        }

        public void clearNodes()
        {
            rootNode.Nodes.Clear();
            hierarchies.variableList.Clear();
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            Repository r = ma.getOpenedRepository();
            r.Hierarchies = new Hierarchy[0];
            if (ma.isInUberTransaction() == false)
                hierarchies.save(r);
            this.r = r;
            ma.setOpenedRepository(r);
        }

        TreeNode selNode;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();
            this.selNode = node;
            if (rootNode.Nodes.Count == 0 || node.Parent.Parent == null || node.Parent.Parent.Parent == null)
                insertLevelAboveToolStripMenuItem.Visible = false;
            else
                insertLevelAboveToolStripMenuItem.Visible = true;
            if (rootNode.Nodes.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = hiearchyMenu;
                newLevelToolStripMenuItem.Visible = false;
                removeToolStripMenuItem.Visible = false;
                return false;
            }
            ma.mainTree.ContextMenuStrip = hiearchyMenu;
            selectNode();
            newLevelToolStripMenuItem.Visible = true;
            removeToolStripMenuItem.Visible = true;
            return true;
        }

        public void setup()
        {
            setHierarchyLists();
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return hierarchyTabPage;
            }
            set
            {
            }
        }

        private void replaceLevelColumn(Level l, string find, string replace, bool match)
        {
            for (int i = 0; i < l.ColumnNames.Length; i++)
            {
                if (match)
                {
                    if (l.ColumnNames[i] == find) l.ColumnNames[i] = replace;
                }
                else
                {
                    l.ColumnNames[i] = l.ColumnNames[i].Replace(find, replace);
                }
            }
            foreach (Level cl in l.Children) replaceLevelColumn(cl, find, replace, match);
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (TreeNode n in rootNode.Nodes)
            {
                Hierarchy h = (Hierarchy)n.Tag;
                foreach (Level l in h.Children) replaceLevelColumn(l, find, replace, match);
            }
        }

        private void findLevelColumn(string hierarchy, Level l, List<string> results, string find, bool match)
        {
            for (int i = 0; i < l.ColumnNames.Length; i++)
            {
                if (match)
                {
                    if (l.ColumnNames[i] == find) results.Add("Hierarchy: " + hierarchy + ", Level: " + l.Name);
                }
                else
                {
                    if (l.ColumnNames[i].IndexOf(find) >= 0) results.Add("Hierarchy: " + hierarchy + ", Level: " + l.Name);
                }
            }
            foreach (Level cl in l.Children) findLevelColumn(hierarchy, cl, results, find, match);
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (TreeNode n in rootNode.Nodes)
            {
                Hierarchy h = (Hierarchy)n.Tag;
                foreach (Level l in h.Children) findLevelColumn(h.Name, l, results, find, match);
            }
        }

        public void implicitSave()
        {
            updateHierarchyLevel(selNode);
        }

        public void setRepositoryDirectory(string d)
        {
            hierarchies.updateRepositoryDirectory(d, ma);
        }
        #endregion
        private void setHierarchyLists()
        {
            hierarchyDimension.Items.Clear();
            List<string> dlist = ma.getDimensionList();
            foreach (string s in dlist) hierarchyDimension.Items.Add(s);
            hierarchyExclusionFilter.Items.Clear();
            hierarchyNoMeasureFilter.Items.Clear();
            List<QueryFilter> flist = ma.filterMod.filterList;
            foreach (QueryFilter f in flist)
            {
                hierarchyExclusionFilter.Items.Add(f.Name);
                hierarchyNoMeasureFilter.Items.Add(f.Name);
            }
        }

        public List<Hierarchy> getHierarchies()
        {
            List<Hierarchy> hList = new List<Hierarchy>();
            foreach (TreeNode n in rootNode.Nodes)
            {
                hList.Add((Hierarchy)n.Tag);
            }
            return (hList);
        }

        public Hierarchy getHierarchy(string name)
        {
            foreach (TreeNode n in rootNode.Nodes)
            {
                if (((Hierarchy)n.Tag).Name == name)
                    return ((Hierarchy)n.Tag);
            }
            return (null);
        }

        public List<Hierarchy> getLatestHierarchies()
        {
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.refreshIfNecessary(ma);
            return hierarchies.variableList;
        }

        public Hierarchy getLatestHierarchy(string name)
        {
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.refreshIfNecessary(ma);
            foreach (Hierarchy h in hierarchies.variableList)
            {
                if (h.Name == name)
                    return h;
            }
            return null;
        }

        public void updateHierarchy(Hierarchy h)
        {
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.updateItem(h, "", ma);
            this.r = ma.getOpenedRepository();
            if (this.r != null)
                this.r.Hierarchies = hierarchies.hierarchies;
            setupNodes(rootNode);
        }

        public Level getNextGeneratedTableLevel(Hierarchy h, Level l)
        {
            if (l.GenerateDimensionTable)
                return (l);
            if (l.Children != null)
                foreach (Level cl in l.Children)
                {
                    Level gl = getNextGeneratedTableLevel(h, cl);
                    if (gl != null)
                        return (gl);
                }
            if (l.SharedChildren != null)
                foreach (string s in l.SharedChildren)
                {
                    Level cl = h.findLevel(s);
                    Level gl = getNextGeneratedTableLevel(h, cl);
                    if (gl != null)
                        return (gl);
                }
            return (null);
        }

        public void addHierarchy(Hierarchy h)
        {
            TreeNode n = new TreeNode(h.Name);
            n.Tag = h;
            rootNode.Nodes.Add(n);
            n.ImageIndex = 7;
            n.SelectedImageIndex = 7;
            addNodes(n, h.Children);
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.updateItem(h, "", ma);
        }

        public TreeNode newHierarchy(Hierarchy h)
        {
            TreeNode tn = new TreeNode(h.Name);
            tn.ImageIndex = 7;
            tn.SelectedImageIndex = 7;
            tn.Tag = h;
            rootNode.Nodes.Add(tn);
            rootNode.Expand();
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.updateItem(h, "", ma);
            return (tn);
        }

        private void newHierarchyMenuItem_Click(object sender, System.EventArgs e)
        {
            Hierarchy h = new Hierarchy();
            h.Name = "New Hierarchy";
            h.DimensionName = "";
            h.Children = new Level[0];
            h.DimensionKeyLevel = "";
            newHierarchy(h);
            setHierarchyLists();
            hierarchyPanel.Visible = true;
            levelPanel.Visible = false;
        }


        private void newHierarchyButton_Click(object sender, System.EventArgs e)
        {
            newHierarchyMenuItem_Click(sender, e);
        }

        private void updateHierarchy()
        {
            if (selNode != null)
            {
                if (selNode.Parent == rootNode)
                {
                    Hierarchy h = (Hierarchy)selNode.Tag;
                    h.DimensionName = hierarchyDimension.Text;
                    h.ExclusionFilter = hierarchyExclusionFilter.Text;
                    h.NoMeasureFilter = hierarchyNoMeasureFilter.Text;
                    h.DimensionKeyLevel = dimKeyLevelBox.Text;
                    h.Hidden = hiddenBox.Checked;
                    h.Locked = webLocked.Checked;
                    if (sourceGroupBox.Text.Length > 0)
                        h.SourceGroups = sourceGroupBox.Text;
                    else
                        h.SourceGroups = null;
                    if ((h.DimensionKeyLevel == null || h.DimensionKeyLevel.Trim().Equals("")) && h.Children.Length>0)
                    {
                        Level l = h.Children[0];
                        while (l.Children.Length > 0)
                            l = l.Children[0];
                        h.DimensionKeyLevel = l.Name;                      
                    }
                }
            }
        }

        private Hierarchy curHierarchy;

        private void selectNode()
        {
            TreeNode n = selNode;
            if (selNode != null)
            {
                if (selNode.Parent == rootNode)
                {
                    hierarchyPanel.Visible = true;
                    levelPanel.Visible = false;
                    Hierarchy h = (Hierarchy)n.Tag;
                    hierarchyDimension.Text = h.DimensionName;
                    hierarchyExclusionFilter.Text = h.ExclusionFilter;
                    hierarchyNoMeasureFilter.Text = h.NoMeasureFilter;
                    hiddenBox.Checked = h.Hidden;
                    webLocked.Checked = h.Locked;
                    if (h.SourceGroups != null)
                        sourceGroupBox.Text = h.SourceGroups;
                    else
                        sourceGroupBox.Text = "";
                    setHierarchyLists();
                    curHierarchy = h;
                    List<Level> levelList = setSharedLevelColumns(null, h);
                    sharedLevel.Items.AddRange(levelList.ToArray());
                    sharedLevelToolStripMenuItem.Visible = sharedLevel.Items.Count > 0;
                    addSharedLevel.Visible = false;
                    List<string> llist = new List<string>();
                    h.getChildLevelNames(llist);
                    dimKeyLevelBox.Items.Clear();
                    foreach (string s in llist)
                        dimKeyLevelBox.Items.Add(s);
                    dimKeyLevelBox.Text = h.DimensionKeyLevel;
                }
                else
                {
                    levelPanel.Visible = true;
                    hierarchyPanel.Visible = false;
                    Level l = (Level)n.Tag;
                    bool isSharedLevel = false;
                    //Shared level
                    if (l == null)
                    {
                        Hierarchy h = null;
                        TreeNode curNode = n;
                        //Traverse up the tree strucutre to find the hierarchy node
                        while (h == null)
                        {
                            if ((curNode.Parent != null) && (typeof(Hierarchy).IsInstanceOfType(curNode.Parent.Tag)))
                            {
                                h = (Hierarchy)curNode.Parent.Tag;
                            }
                            else
                            {
                                curNode = curNode.Parent;
                            }
                        }

                        if (h != null)
                        {
                            l = h.findLevel(n.Text);
                        }
                        isSharedLevel = true;
                    }
                    setEnabledForLevelPanelComponents(!isSharedLevel);
                    if (l != null)
                    {
                        cardinalityBox.Text = l.Cardinality.ToString();
                        generateDimTableCheckBox.Checked = l.GenerateDimensionTable;
                        degenerateTable.Checked = l.Degenerate;
                        if (l.SCDType == 2)
                            typeIISCD.Checked = true;
                        else
                            typeIISCD.Checked = false;
                        generateCurrentCheckBox.Checked = l.GenerateInheritedCurrentDimTable;
                        generateCurrentCheckBox.Enabled = typeIISCD.Checked;
                        levelColumns.Items.Clear();
                        while (n.Parent != rootNode) n = n.Parent;
                        Hierarchy h = (Hierarchy)n.Tag;
                        List<string> sortedColList = getAvailableColumnsForLevel(ma, h, l);
                        foreach (string s in sortedColList)
                        {
                            int index = Array.IndexOf<string>(l.ColumnNames, s);
                            ListViewItem lvi = null;
                            if (index >= 0)
                            {
                                string hideMark = l.HiddenColumns == null ? "" : (l.HiddenColumns.Length <= index ? "" : (l.HiddenColumns[index] ? "X" : ""));
                                lvi = new ListViewItem(new string[] { s, hideMark });
                                lvi.Checked = true;
                            }
                            else
                                lvi = new ListViewItem(new string[] { s, "" });
                            levelColumns.Items.Add(lvi);
                        }
                        keyListBox.Items.Clear();
                        if (l.Keys != null)
                        {
                            keyListBox.Items.AddRange(l.Keys);
                        }
                        curHierarchy = h;
                        List<Level> levelList = setSharedLevelColumns(l, h);
                        sharedLevel.Items.AddRange(levelList.ToArray());
                        sharedLevelToolStripMenuItem.Visible = sharedLevel.Items.Count > 0;
                        addSharedLevel.Visible = false;
                    }
                    //Assigning values to checkbox "Generate Dimension Table" might have enabled the other two checkboxes
                    //For shared level, make sure those other two checkboxes are disabled
                    if (isSharedLevel)
                    {
                        degenerateTable.Enabled = false;
                        typeIISCD.Enabled = false;
                        generateCurrentCheckBox.Enabled = false;
                    }
                }
            }
        }

        public List<string> getAvailableColumnsForLevel(MainAdminForm ma, Hierarchy h, Level l)
        {
            List<string> colList = ma.getDimensionColumnList(h.DimensionName);
            List<string> usedList = new List<string>();
            foreach (Level cl in h.Children)
                getUsedColumnsNotInLevel(cl, l, usedList);
            List<string> sortedColList = Util.sortListAsUsed(colList, l.ColumnNames);
            List<string> result = new List<string>();
            foreach (string s in sortedColList)
            {
                if (!usedList.Contains(s))
                {
                    result.Add(s);
                }
            }
            return (result);
        }

        public void getUsedColumnsNotInLevel(Level startLevel, Level l, List<string> usedList)
        {
            if (startLevel != l)
                foreach (string s in startLevel.ColumnNames)
                    if (Array.IndexOf<string>(l.ColumnNames, s) < 0) usedList.Add(s);
            foreach (Level newLevel in startLevel.Children)
                getUsedColumnsNotInLevel(newLevel, l, usedList);
        }

        public void removeHierarchy(Hierarchy h)
        {
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.deleteItem(h, ma);
            Repository r = ma.getOpenedRepository();
            if (this.r == null)
                this.r = r;
            if (this.r != null)
            {
                this.r.Hierarchies = hierarchies.hierarchies;
            }
            this.setupNodes(rootNode);
        }

        public void removeHierarchyWithoutRefresh(Hierarchy h)
        {
            hierarchies.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            hierarchies.Remove(h);
            Repository r = ma.getOpenedRepository();
            if (this.r == null)
                this.r = r;
            if (this.r != null)
            {
                this.r.Hierarchies = hierarchies.hierarchies;
            }
            this.setupNodes(rootNode);
        }

        private void deleteHierarchyButton_Click(object sender, System.EventArgs e)
        {
            TreeNode n = selNode;
            if (n != null)
            {
                if (n.Parent == rootNode)
                {
                    n.Remove();
                }
            }
        }

        public TreeNode newLevel(TreeNode parent, Level l, string sharedLevelName)
        {
            if (parent != null)
            {
                string name = sharedLevelName;
                TreeNode nn = new TreeNode(name);
                nn.ImageIndex = 7;
                nn.SelectedImageIndex = 7;
                parent.Nodes.Add(nn);
                if (sharedLevelName == null)
                {
                    nn.Tag = l;
                    if (parent.Parent == rootNode)
                    {
                        Hierarchy h = (Hierarchy)parent.Tag;
                        Level[] newlist = new Level[h.Children.Length + 1];
                        for (int i = 0; i < h.Children.Length; i++) newlist[i] = h.Children[i];
                        newlist[h.Children.Length] = l;
                        h.Children = newlist;
                    }
                    else
                    {
                        Level plevel = (Level)parent.Tag;
                        Level[] newlist = new Level[plevel.Children.Length + 1];
                        for (int i = 0; i < plevel.Children.Length; i++) newlist[i] = plevel.Children[i];
                        newlist[plevel.Children.Length] = l;
                        plevel.Children = newlist;
                    }
                    nn.Text = l.Name;
                }
                else
                {
                    nn.ForeColor = Color.Blue;
                    nn.Tag = null;
                    Level plevel = (Level)parent.Tag;

                    string[] newlist = null;
                    if (plevel.SharedChildren == null)
                    {
                        newlist = new string[1];
                        newlist[0] = sharedLevelName;
                    }
                    else
                    {
                        newlist = new string[plevel.SharedChildren.Length + 1];
                        for (int i = 0; i < plevel.SharedChildren.Length; i++) newlist[i] = plevel.SharedChildren[i];
                        newlist[plevel.SharedChildren.Length] = sharedLevelName;
                    }
                    plevel.SharedChildren = newlist;
                }
                return (nn);
            }
            return (null);
        }

        private Level getBlankLevel()
        {
            Level l = new Level();
            l.Name = "New Level";
            l.Cardinality = 1;
            l.Children = new Level[0];
            l.ColumnNames = new string[0];
            return (l);
        }

        private void newLevel(string sharedLevelName)
        {
            TreeNode n = selNode;
            if (n != null)
            {
                levelPanel.Visible = true;
                hierarchyPanel.Visible = false;
                Level l = getBlankLevel();
                TreeNode nn = newLevel(n, l, sharedLevelName);
                n.Expand();
                /*if (sharedLevelName == null)
                {
                    cardinalityBox.Text = l.Cardinality.ToString();
                    generateDimTableCheckBox.Checked = l.generateDimensionTable;
                } */
                levelPanel.Visible = true;
                ma.mainTree.SelectedNode = nn;
            }
        }

        private void newLevelMenuItem_Click(object sender, System.EventArgs e)
        {
            newLevel(null);
        }

        private void newLevelButton_Click(object sender, System.EventArgs e)
        {
            newLevelMenuItem_Click(sender, e);
        }

        private void updateLevel()
        {
            TreeNode n = selNode;
            if (n != null && n != rootNode && n.Parent != rootNode && n.Parent != null)
            {
                Level l = (Level)n.Tag;
                if (l == null)
                {
                    // shared level?
                    return;
                }
                l.Cardinality = Int32.Parse(cardinalityBox.Text);
                l.GenerateDimensionTable = generateDimTableCheckBox.Checked;
                l.SCDType = typeIISCD.Checked ? 2 : 1;
                l.Degenerate = degenerateTable.Checked;
                l.GenerateInheritedCurrentDimTable = generateCurrentCheckBox.Checked;
                l.ColumnNames = new string[levelColumns.CheckedItems.Count];
                l.HiddenColumns = new bool[levelColumns.CheckedItems.Count];
                for (int i = 0; i < levelColumns.CheckedItems.Count; i++)
                {
                    ListViewItem lvi = levelColumns.CheckedItems[i];
                    l.ColumnNames[i] = (string)lvi.SubItems[0].Text;
                    l.HiddenColumns[i] = lvi.SubItems[1].Text == "X";
                }
                l.Keys = new LevelKey[keyListBox.Items.Count];
                for (int i = 0; i < keyListBox.Items.Count; i++)
                {
                    l.Keys[i] = (LevelKey)keyListBox.Items[i];
                }
            }
        }

        public void clearSelectedNode()
        {
            selNode = null;
        }

        public List<string> getDimensionLevelNames(string dimension)
        {
            List<string> result = new List<string>();
            List<Level> lresult = getDimensionLevels(dimension);
            foreach (Level l in lresult)
                if (l != null && !result.Contains(l.Name))
                    result.Add(l.Name);
            return (result);
        }

        public List<string> getDimensionLevelNames(Hierarchy h)
        {
            List<string> result = new List<string>();
            List<Level> list = getDimensionLevels(h);
            if (list != null && list.Count > 0)
            {
                foreach (Level l in list)
                    if (l != null && !result.Contains(l.Name))
                        result.Add(l.Name);
            }
            return result;
        }

        public List<Level> getDimensionLevels(string dimension)
        {
            List<Level> lresult = new List<Level>();
            getDimensionLevels(dimension, lresult);
            return (lresult);
        }

        public Hierarchy getDimensionHierarchy(string dimension)
        {
            foreach (Hierarchy h in getLatestHierarchies())
            {
                if (h.DimensionName == dimension)
                    return h;
            }
            return (null);
        }

        private void getDimensionLevels(string dimension, List<Level> list)
        {
            if (dimension != null)
            {
                Hierarchy h = getDimensionHierarchy(dimension);
                if (h != null && h.Children != null)
                {
                    foreach (Level l in h.Children)
                    {
                        list.Add(l);
                        l.getChildLevels(h, list);
                    }
                }
            }
            return;
        }

        private List<Level> getDimensionLevels(Hierarchy h)
        {
            List<Level> list = new List<Level>();
            if (h != null && h.Children != null)
            {
                foreach (Level l in h.Children)
                {
                    list.Add(l);
                    l.getChildLevels(h, list);
                }
            }
            return list;
        }

        public string getDimensionKeyLevel(string dimension)
        {
            if (dimension != null)
            {
                foreach (TreeNode tn in rootNode.Nodes)
                {
                    Hierarchy h = (Hierarchy)tn.Tag;
                    if (h.DimensionName == dimension) return h.DimensionKeyLevel;
                }
            }
            return null;
        }

        public List<Level> setSharedLevelColumns(Level curLevel, Hierarchy h)
        {
            List<Level> levelList = new List<Level>();
            sharedLevel.Items.Clear();
            if (curHierarchy.Children != null)
            {
                foreach (Level l in curHierarchy.Children)
                {
                    getLevelList(l, levelList);
                }
            }
            if (curLevel != null)
            {
                levelList.Remove(curLevel);
                List<Level> ancestorList = new List<Level>();
                curHierarchy.findAncestors(curLevel.Name, ancestorList);
                foreach (Level al in ancestorList)
                {
                    levelList.Remove(al);
                }
                foreach (Level l in curLevel.Children)
                {
                    levelList.Remove(l);
                }
                if (curLevel.SharedChildren != null)
                    foreach (string s in curLevel.SharedChildren)
                    {
                        int i = 0;
                        for (; i < levelList.Count; i++)
                        {
                            if (levelList[i].Name == s)
                                break;
                        }
                        if (i < levelList.Count) levelList.RemoveAt(i);
                    }
            }
            else if (h != null)
            {
                if (h.Children != null)
                {
                    foreach (Level l in h.Children) levelList.Remove(l);
                }
                if (h.SharedChildren != null)
                    foreach (string s in h.SharedChildren)
                    {
                        int i = 0;
                        for (; i < levelList.Count; i++)
                        {
                            if (levelList[i].Name == s)
                                break;
                        }
                        if (i < levelList.Count) levelList.RemoveAt(i);
                    }
            }
            return (levelList);
        }

        private void getLevelList(Level l, List<Level> levelList)
        {
            levelList.Add(l);
            foreach (Level cl in l.Children)
            {
                getLevelList(cl, levelList);
            }
        }

        private void levelColumns_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (levelColumns.SelectedIndices.Count == 0) return;
            ListViewItem lvi = levelColumns.SelectedItems[0];
        }

        private void removeLevelColumn_Click(object sender, System.EventArgs e)
        {
            levelColumns.Items.Remove(levelColumns.SelectedItems[0]);
        }

        private Hierarchy getHiearchy(TreeNode node)
        {
            while (node.Parent != null && node.Parent != rootNode)
            {
                node = node.Parent;
            }
            if (node.Parent != null)
            {
                return (Hierarchy)node.Tag;
            }
            return null;
        }

        // build the new list of children for the parent of the current level that is being deleted.
        // User may choose to move the children of deleted node to the parent (making them at the sibling level of
        // node that is being deleted)
        private Level[] buildNewSiblingsList(Level[] siblings, Level cl, bool saveChildren)
        {
            int newSiblingCount = siblings.Length;
            if (cl != null)
                newSiblingCount--; // since level to be deleted is not a shared child, reduce the count as it gets deleted.
            if (saveChildren)
                newSiblingCount += cl.Children.Length; // children to be saved as siblings
            Level[] newsiblings = new Level[newSiblingCount];
            int i = 0;
            foreach (Level child in siblings)
            {
                if (child != cl)
                    newsiblings[i++] = child;
                else
                {
                    if (saveChildren)
                    {
                        foreach (Level grandchild in cl.Children)
                            newsiblings[i++] = grandchild;
                    }
                }
            }
            return newsiblings;

        }

        private DialogResult promptForSavingChildren(Level cl, string parentName, bool toHier)
        {
            if (cl != null && (cl.Children.Length > 0 ||
                (cl.SharedChildren != null && cl.SharedChildren.Length > 0)))
            {
                if (toHier)
                    return MessageBox.Show("Do you want to assign the children of the " + cl.Name + " level directly to the " + parentName + " hierarchy?", "Remove Level", MessageBoxButtons.YesNoCancel);
                else
                    return MessageBox.Show("Do you want to assign the children of the " + cl.Name + " level to the " + parentName + " level?", "Remove Level", MessageBoxButtons.YesNoCancel);
            }
            return DialogResult.None; // nothing to do as there are no children

        }

        private string[] removeItemFromArray(string[] strArray, string itemToRemove) {
            if (strArray == null || strArray.Length == 0)
                return strArray;
            int idx = -1;
            for (int i=0; i<strArray.Length; i++) {
                if (strArray[i].Equals(itemToRemove)) 
                    idx = i;
            }
            if (idx < 0) 
                return strArray; // nothing to remove.
            string[] newArray = new string[strArray.Length - 1];
            int j = 0;
            for (int i=0; i<strArray.Length; i++) {
                if (i != idx) 
                    newArray[j++] = strArray[i];
            }
            return newArray;
        }

        private void removeHierarchyItem_Click(object sender, System.EventArgs e)
        {
            TreeNode n = selNode;
            TreeNode nParent = n.Parent;
            DialogResult saveTheChildren = DialogResult.None;
            if (n != null)
            {
                if (nParent == rootNode)
                {
                    Hierarchy h = getHiearchy(n);
                    n.Remove();
                    removeHierarchy(h);
                }
                else
                {
                    Level cl = (Level)n.Tag;
                    Hierarchy hier = getHiearchy(n);
                    Hashtable nodesSharedByOthers = null;
                    bool parentIsHierarchy = (nParent.Parent == rootNode); // if the parent is root node, then it is hierarchy
                    if (cl == null)
                    {
                        // It is  a shared node, remove it from parent's shared report.
                        if (parentIsHierarchy)
                            ((Hierarchy)nParent.Tag).SharedChildren = removeItemFromArray(((Hierarchy)nParent.Tag).SharedChildren, n.Text);
                        else
                            ((Level)nParent.Tag).SharedChildren = removeItemFromArray(((Level)nParent.Tag).SharedChildren, n.Text);
                    }
                    else
                    {
                        saveTheChildren = promptForSavingChildren(cl, nParent.Text, parentIsHierarchy);
                        if (saveTheChildren == DialogResult.Cancel) return;
                        nodesSharedByOthers = getNodesThatAreShared(hier, cl, (saveTheChildren == DialogResult.Yes));
                        if (nodesSharedByOthers.Count > 0)
                        {
                            if (MessageBox.Show("Level(s) you have chosen to remove is used as shared level, do you want to continue? \n If you continue, all the shared usage of this will also be removed.", "Confirm Remove shared", MessageBoxButtons.YesNo) == DialogResult.No)
                                return;
                        }
                        removeSharedLevels(nodesSharedByOthers);
                        // Children are there, either user has chosen to move them up or delete.
                        Level[] siblings = (parentIsHierarchy) ? ((Hierarchy)nParent.Tag).Children : ((Level)nParent.Tag).Children;
                        Level[] newsiblings = buildNewSiblingsList(siblings, cl, (saveTheChildren == DialogResult.Yes));
                        if (parentIsHierarchy)
                            ((Hierarchy)nParent.Tag).Children = newsiblings;
                        else
                            ((Level)nParent.Tag).Children = newsiblings;
                            // Check if there are nodes that are shared by others                        
                    }
                    int ix = nParent.Nodes.IndexOf(n);
                    nParent.Nodes.Remove(n);
                    if (saveTheChildren == DialogResult.Yes)
                    {
                        foreach (TreeNode c in n.Nodes)
                            nParent.Nodes.Insert(ix++, c);
                    }
                    if (nodesSharedByOthers != null)
                    {
                        deleteTreeNodesForSharedLevels(rootNode, nodesSharedByOthers);
                    }
                }
            }
        }

        private void deleteTreeNodesForSharedLevels(TreeNode node, Hashtable nodesSharedByOthers)
        {
            List<TreeNode> nodesToRemove = new List<TreeNode>();
            foreach (TreeNode nd in node.Nodes)
            {
                if (nd.Tag == null && nodesSharedByOthers.ContainsKey(nd.Text))
                    nodesToRemove.Add(nd);
                // recursively call this 
                deleteTreeNodesForSharedLevels(nd, nodesSharedByOthers);
            }
            // remove those that need to be deleted
            foreach (TreeNode nd in nodesToRemove)
                node.Nodes.Remove(nd);
        }

        // REmove the sharedchildren entry for those that are getting deleted
        private void removeSharedLevels(Hashtable nodesSharedByOthers)
        {
            IDictionaryEnumerator mapEnumerator = nodesSharedByOthers.GetEnumerator();
            while (mapEnumerator.MoveNext())
            {
                // remove the shared entries from it's parents
                List<Level> parents = (List<Level>)mapEnumerator.Entry.Value;
                foreach (Level p in parents) 
                    p.SharedChildren = removeItemFromArray(p.SharedChildren, mapEnumerator.Entry.Key.ToString());            
            }

        }

        // Get all the nodes that are shared by others in the subtree starting from (including) 'cl'
        // return a map where the key is the name of the level and value is the parents (List<Level> )
        private Hashtable getNodesThatAreShared(Hierarchy hier, Level cl, bool ignoreChildren)
        {
            Hashtable sharedLevelsToBeDeleted = new Hashtable();
            List<Level> nodes = (ignoreChildren)? new List<Level>() : cl.getChildLevels(hier, true);
            nodes.Add(cl);
            foreach (Level node in nodes)
            {
                List<Level> parents = hier.getParents(node.Name);
                if (parents.Count > 1)
                {
                    sharedLevelsToBeDeleted.Add(node.Name, parents);
                }
            }
            return sharedLevelsToBeDeleted;
        }

        private void addNodes(TreeNode n, Level[] levels)
        {
            if (levels == null)
                return;
            foreach (Level lv in levels)
            {
                TreeNode nn = new TreeNode(lv.Name);
                nn.ImageIndex = 7;
                nn.SelectedImageIndex = 7;
                nn.Tag = lv;
                n.Nodes.Add(nn);
                addNodes(nn, lv.Children);
                if (lv.SharedChildren != null)
                    addSharedNodes(nn, lv.SharedChildren);
            }
        }

        private void addSharedNodes(TreeNode n, string[] levels)
        {
            foreach (string lv in levels)
            {
                TreeNode nn = new TreeNode(lv);
                nn.ImageIndex = 7;
                nn.SelectedImageIndex = 7;
                nn.ForeColor = Color.Blue;
                n.Nodes.Add(nn);
            }
        }


        private void newKeyButton_Click(object sender, EventArgs e)
        {
            DefineKeyForm dkf = new DefineKeyForm(new LevelKey(), ma, curHierarchy.DimensionName);
            DialogResult dr = dkf.ShowDialog();
            if (dr == DialogResult.OK)
            {
                LevelKey key = dkf.LevelKey;
                keyListBox.Items.Add(key);
            }
        }

        private void editKeyButton_Click(object sender, EventArgs e)
        {
            if (keyListBox.SelectedItem != null)
            {
                DefineKeyForm dkf = new DefineKeyForm((LevelKey)keyListBox.SelectedItem, ma, curHierarchy.DimensionName);
                DialogResult dr = dkf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    keyListBox.Items[keyListBox.SelectedIndex] = dkf.LevelKey;
                }
            }
        }

        private void removeKeyButton_Click(object sender, EventArgs e)
        {
            keyListBox.Items.Remove(keyListBox.SelectedItem);
        }

        private void keyListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            LevelKey lk;
            if (e.Index >= 0)
            {
                lk = (LevelKey)keyListBox.Items[e.Index];
                if (lk != null)
                {
                    Brush b = Brushes.Black;
                    Brush bH = SystemBrushes.HighlightText;
                    Font drawFont = e.Font;
                    Font newDrawFont = new Font(drawFont.FontFamily, drawFont.Size, FontStyle.Bold);

                    Rectangle rect1 = e.Bounds;
                    rect1.X += 20;
                    Rectangle rect2 = e.Bounds;
                    rect2.Width = rect2.Height;
                    rect2.X += 2;
                    Bitmap bm = CompiledResources.Key;
                    e.DrawBackground();
                    e.Graphics.DrawImage(bm, rect2);
                    e.Graphics.FillRectangle(Brushes.White, new Rectangle(0, rect2.Y, 2, rect2.Height));
                    if ((e.State & DrawItemState.Selected) > 0)
                        e.Graphics.DrawString(lk.ToString(), newDrawFont, bH, rect1);
                    else
                        e.Graphics.DrawString(lk.ToString(), drawFont, b, rect1);
                    e.DrawFocusRectangle();
                }
            }
        }

        private void addSharedLevel_Click(object sender, EventArgs e)
        {
            newLevel(sharedLevel.Text);
        }

        private void sharedLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            addSharedLevel.Visible = sharedLevel.Text.Length > 0;
        }

        private void hierarchyTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        private void updateHierarchyLevel(TreeNode node)
        {
            if (node != null && node != rootNode)
            {
                if (node.Parent == rootNode)
                    updateHierarchy();
                else
                    updateLevel();
            }
        }



        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (selNode == null) return;
            if (selNode != e.Node || e.Node.Tag == null) return;
            if (typeof(Hierarchy).IsInstanceOfType(selNode.Tag))
            {
                ((Hierarchy)selNode.Tag).Name = e.Label;
            }
            else if (typeof(Level).IsInstanceOfType(selNode.Tag))
            {
                ((Level)selNode.Tag).Name = e.Label;
            }
        }

        #endregion

        private void toggleHidden_Click(object sender, EventArgs e)
        {
            if (levelColumns.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in levelColumns.SelectedItems)
                {
                    //if (lvi.Checked)
                    //{
                        if (lvi.SubItems[1].Text == "")
                            lvi.SubItems[1].Text = "X";
                        else
                            lvi.SubItems[1].Text = "";
                    //}
                }
            }
        }

        private void insertLevelAboveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selNode != null && selNode.Parent != rootNode)
            {
                TreeNode n = selNode;
                levelPanel.Visible = true;
                hierarchyPanel.Visible = false;
                Level l = getBlankLevel();
                TreeNode nn = newLevel(selNode.Parent, l, null);
                cardinalityBox.Text = l.Cardinality.ToString();
                generateDimTableCheckBox.Checked = l.GenerateDimensionTable;
                degenerateTable.Checked = l.Degenerate;
                typeIISCD.Checked = l.SCDType == 2 ? true : false;
                generateCurrentCheckBox.Checked = l.GenerateInheritedCurrentDimTable;
                Level cl = (Level)n.Tag;
                List<Level> llist = null;
                if (typeof(Level).IsInstanceOfType(n.Parent.Tag))
                {
                    Level pl = (Level)n.Parent.Tag;
                    llist = new List<Level>(pl.Children);
                    llist.Remove(cl);
                    n.Parent.Nodes.Remove(n);
                    pl.Children = llist.ToArray();
                }
                else
                {
                    Hierarchy h = (Hierarchy)n.Parent.Tag;
                    llist = new List<Level>(h.Children);
                    llist.Remove(cl);
                    n.Parent.Nodes.Remove(n);
                    h.Children = llist.ToArray();
                }
                nn.Nodes.Add(n);
                llist = new List<Level>(l.Children);
                llist.Add(cl);
                l.Children = llist.ToArray();
                nn.Expand();
                ma.mainTree.SelectedNode = nn;
            }
        }

        private void generateDimTableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (generateDimTableCheckBox.Checked)
            {
                degenerateTable.Enabled = true;
                typeIISCD.Enabled = true;
            }
            else
            {
                degenerateTable.Enabled = false;
                typeIISCD.Enabled = false;
            }
        }

        private void degenerateTable_CheckedChanged(object sender, EventArgs e)
        {
            if (degenerateTable.Checked)
                typeIISCD.Checked = false;
        }

        private void typeIISCD_CheckedChanged(object sender, EventArgs e)
        {
            if (typeIISCD.Checked)
            {
                degenerateTable.Checked = false;
                generateCurrentCheckBox.Enabled = true;
            }
            else
            {
                generateCurrentCheckBox.Enabled = false;
            }
        }

       

        private void levelColumns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            ListViewItem lvi = e.Item;
            levelColumns.SelectedItems.Clear();
            int idx = levelColumns.Items.IndexOf(lvi);
            levelColumns.Items[idx].Selected = true;
        }

        private void setEnabledForLevelPanelComponents(bool enabled)
        {
            cardinalityBox.Enabled = enabled;
            generateDimTableCheckBox.Enabled = enabled;
            degenerateTable.Enabled = enabled;
            typeIISCD.Enabled = enabled;
            levelColumns.Enabled = enabled;
            keyListBox.Enabled = enabled;
            toggleHidden.Enabled = enabled;
            newKeyButton.Enabled = enabled;
            editKeyButton.Enabled = enabled;
            removeKeyButton.Enabled = enabled;
        }
    }
}