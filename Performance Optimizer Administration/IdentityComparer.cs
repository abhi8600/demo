﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Performance_Optimizer_Administration
{
    public class IdentityComparer<T> : IEqualityComparer<T>
    where T : class
    {
        public bool Equals(T x, T y)
        {
            return Object.ReferenceEquals(x,y);
        }

        public int GetHashCode(T x)
        {
            return x.GetHashCode();
        }
    }

}
