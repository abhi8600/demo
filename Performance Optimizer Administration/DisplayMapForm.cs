using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class DisplayMapForm : Form
    {
        DisplayMap dm;
        public DisplayMapForm(DisplayMap dm)
        {
            if (dm == null)
                this.dm = new DisplayMap();
            else
                this.dm = dm;
            dm = this.dm;
            InitializeComponent();
            mapTable.Rows.Clear();
            bucketTable.Rows.Clear();
            bucketGridView.DataSource = bucketTable;
            dataTypeBox.Text = dm.DataType == null ? "Varchar" : dm.DataType;
            if (dm.Type == DisplayMap.TYPE_QUERY)
            {
                queryButton.Checked = true;
                queryBox.Text = dm.Query;
            }
            else if (dm.Type == DisplayMap.TYPE_LOOKUP)
            {
                lookupButton.Checked = true;
                lookupTableBox.Text = dm.LookupTable;
                lookupBox.Text = dm.LookupExpression;
                lookupJoinBox.Text = dm.LookupJoinColumn;
                lookupFilterBox.Text = dm.LookupFilterExpression;
                outerJoinBox.Checked = dm.LookupOuterJoin;
            }
            else
            {
                if (dm.Type == DisplayMap.TYPE_MAP)
                    staticValueButton.Checked = true;
                else bucketButton.Checked = true;
                for (int i = 0; i < dm.MappedValues.Length; i++)
                {
                    if (dm.Type == DisplayMap.TYPE_MAP)
                    {
                        DataRow dr = mapTable.NewRow();
                        dr[0] = dm.Keys[i];
                        dr[1] = dm.MappedValues[i];
                        mapTable.Rows.Add(dr);
                    }
                    else
                    {
                        DataRow dr = bucketTable.NewRow();
                        dr[0] = dm.MappedValues[i];
                        dr[1] = dm.Buckets[i][0];
                        dr[2] = dm.Buckets[i][1];
                        bucketTable.Rows.Add(dr);
                    }
                }
            }
            defaultBox.Text = this.dm.Default;
        }

        public DisplayMap getMap()
        {
            if (dm == null)
                return (null);
            dm.Default = defaultBox.Text;
            if (queryButton.Checked)
            {
                dm.Type = DisplayMap.TYPE_QUERY;
                dm.Query = queryBox.Text;
            }
            else if (staticValueButton.Checked)
            {
                dm.Type = DisplayMap.TYPE_MAP;
                dm.Keys = new string[mapTable.Rows.Count];
                dm.MappedValues = new string[mapTable.Rows.Count];
                for (int i = 0; i < mapTable.Rows.Count; i++)
                {
                    dm.Keys[i] = (string)mapTable.Rows[i][0];
                    dm.MappedValues[i] = (string)mapTable.Rows[i][1];
                }
            }
            else if (lookupButton.Checked)
            {
                dm.Type = DisplayMap.TYPE_LOOKUP;
                dm.LookupTable = lookupTableBox.Text;
                dm.LookupExpression = lookupBox.Text;
                dm.LookupJoinColumn = lookupJoinBox.Text;
                dm.LookupFilterExpression = lookupFilterBox.Text;
                dm.LookupOuterJoin = outerJoinBox.Checked;
            }
            else
            {
                dm.Type = DisplayMap.TYPE_BUCKET;
                dm.MappedValues = new string[bucketTable.Rows.Count];
                dm.Buckets = new double[bucketTable.Rows.Count][];
                for (int i = 0; i < bucketTable.Rows.Count; i++)
                {
                    dm.MappedValues[i] = (string)bucketTable.Rows[i][0];
                    dm.Buckets[i] = new double[2];
                    dm.Buckets[i][0] = (double)bucketTable.Rows[i][1];
                    dm.Buckets[i][1] = (double)bucketTable.Rows[i][2];
                }
            }
            return (dm);
        }

        private void delMapButton_Click(object sender, EventArgs e)
        {
            dm = null;
            this.Close();
        }
    }
}