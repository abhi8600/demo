using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class ServerProperties : Form, RepositoryModule
    {
        string moduleName = "Server Properties";
        string categoryName = "System";
        public string Collation;
        public string HistoricalReplacementValue;
        public long BulkDeleteBatchSize;
        public bool UseNewIncrementalDeleteOnIB;
        public long IBIncrementalDeleteThresholdValue;
        public int MaxThreadsInETL;

        public ServerProperties()
        {
            InitializeComponent();
            this.ProcessingTimeZoneComboBox.Text = "";
            this.DisplayTimeZoneComboBox.Text = "";
            this.ProcessingTimeZoneComboBox.Items.Add(Util.GLOBAL_TIMEZONE_DEFAULT);
            this.DisplayTimeZoneComboBox.Items.Add(Util.DISPLAY_TIMEZONE_DEFAULT);
            List<TimeZoneInfo> sysTZ = TimeZoneUtil.getSystemTimeZones();
            this.ProcessingTimeZoneComboBox.Items.AddRange(sysTZ.ToArray());
            this.DisplayTimeZoneComboBox.Items.AddRange(sysTZ.ToArray());                        
        }

        private void TimeZoneComboBox_DropDown(object sender, System.EventArgs e)
        {
            ComboBox senderComboBox = (ComboBox)sender;
            int width = senderComboBox.DropDownWidth;
            Graphics g = senderComboBox.CreateGraphics();
            Font font = senderComboBox.Font;
            int vertScrollBarWidth =
                (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;

            int newWidth;
            System.Windows.Forms.ComboBox.ObjectCollection cmbLst = ((ComboBox)sender).Items;
            for (int i = 1; i < cmbLst.Count; i++)
            {
                TimeZoneInfo s = (TimeZoneInfo)cmbLst[i];
                newWidth = (int)g.MeasureString(s.DisplayName, font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth;
                }
            }
            senderComboBox.DropDownWidth = width;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (r.ServerParameters == null) r.ServerParameters = new ServerParameters();
            r.ServerParameters.ApplicationPath = appPath.Text;
            r.ServerParameters.CachePath = cachePath.Text;
            r.ServerParameters.CustomerMailHost = customerMailHostTextBox.Text;
            r.ServerParameters.MapNullToZero = mapNullsToZeroCheckBox.Checked;
            r.GenerateSubjectArea = genSABox.Checked;
            r.ServerParameters.GenerateUniqueKeys = generateUniqueKeys.Checked;
            r.ServerParameters.AllowDimensionalCrossjoins = allowDimensionalCrossjoins.Checked;
            r.ServerParameters.StagingEncryptionKey = MainAdminForm.encrypt(stagingEncryptionKeyTextBox.Text, MainAdminForm.SecretPassword);
            r.ServerParameters.ProxyDBConnectionString = ProxyDBConnectionStringTextBox.Text;
            r.ServerParameters.ProxyDBUserName = MainAdminForm.encrypt(ProxyDBUserNameTextBox.Text, MainAdminForm.SecretPassword);
            r.ServerParameters.ProxyDBPassword = MainAdminForm.encrypt(ProxyDBPasswordTextBox.Text, MainAdminForm.SecretPassword);
            r.ServerParameters.DisableCachingForGeneratedMeasureTables = disableMeasureTableCacheBox.Checked;
            r.ServerParameters.DisableCachingForGeneratedDimensionTables = disableCachingForDimensionsBox.Checked;
            try
            {
                r.ServerParameters.MaxCacheEntries = Int32.Parse(maxCacheEntries.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxCacheEntries = 1000;
            }
            try
            {
                r.ServerParameters.MaxQueryTime = Int32.Parse(maxQueryTimeTextBox.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxQueryTime = -1;
            }
            try
            {
                r.ServerParameters.MaxRecords = Int32.Parse(maxRecords.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxRecords = 10000;
            }
            try
            {
                r.ServerParameters.MaxSnowflakeSize = Int32.Parse(snowflakeSize.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxSnowflakeSize = -1;
            }
            try
            {
                r.ServerParameters.MaxScriptStatements = Int64.Parse(maxScriptStatementsBox.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxScriptStatements = 0;
            }
            try
            {
                r.ServerParameters.MaxInputRows = Int32.Parse(maxInputRowsBox.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxInputRows = 0;
            }
            try
            {
                r.ServerParameters.MaxOutputRows = Int32.Parse(maxOutputRowsBox.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.MaxOutputRows = 0;
            }
            try
            {
                r.ServerParameters.ScriptQueryTimeout = Int32.Parse(scriptQueryTimeoutBox.Text);
            }
            catch (Exception)
            {
                r.ServerParameters.ScriptQueryTimeout = 0;
            }

            if (ProcessingTimeZoneComboBox.SelectedIndex > 0)
                r.ServerParameters.ProcessingTimeZone = ((TimeZoneInfo)ProcessingTimeZoneComboBox.Items[ProcessingTimeZoneComboBox.SelectedIndex]).Id;
            else
                r.ServerParameters.ProcessingTimeZone = ProcessingTimeZoneComboBox.Text;
            if (DisplayTimeZoneComboBox.SelectedIndex > 0)
                r.ServerParameters.DisplayTimeZone = ((TimeZoneInfo)DisplayTimeZoneComboBox.Items[DisplayTimeZoneComboBox.SelectedIndex]).Id;
            else
                r.ServerParameters.DisplayTimeZone = DisplayTimeZoneComboBox.Text;
            r.ServerParameters.Collation = Collation;
            r.ServerParameters.MaxThreadsInETL = MaxThreadsInETL;
            if (UseNewIncrementalDeleteOnIB)
            {
                r.ServerParameters.DisableNewIncrementalDeleteOnIB = UseNewIncrementalDeleteOnIB;
            }
            if (BulkDeleteBatchSize > 0)
            {
                r.ServerParameters.BulkDeleteBatchSize = BulkDeleteBatchSize;
            }
            else
            {
                r.ServerParameters.BulkDeleteBatchSize = 1000000;
            }
            if (IBIncrementalDeleteThresholdValue > 0)
            {
                r.ServerParameters.IBIncrementalDeleteThresholdValue = IBIncrementalDeleteThresholdValue;
            }
            else
            {
                r.ServerParameters.IBIncrementalDeleteThresholdValue = 1000000;
            }
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.ServerParameters != null)
            {
                appPath.Text = r.ServerParameters.ApplicationPath;
                cachePath.Text = r.ServerParameters.CachePath;
                customerMailHostTextBox.Text = r.ServerParameters.CustomerMailHost;
                maxCacheEntries.Text = r.ServerParameters.MaxCacheEntries.ToString();
                maxRecords.Text = r.ServerParameters.MaxRecords.ToString();
                maxQueryTimeTextBox.Text = r.ServerParameters.MaxQueryTime.ToString();
                maxScriptStatementsBox.Text = r.ServerParameters.MaxScriptStatements.ToString();
                maxInputRowsBox.Text = r.ServerParameters.MaxInputRows.ToString();
                maxOutputRowsBox.Text = r.ServerParameters.MaxOutputRows.ToString();
                scriptQueryTimeoutBox.Text = r.ServerParameters.ScriptQueryTimeout.ToString();
                mapNullsToZeroCheckBox.Checked = r.ServerParameters.MapNullToZero;
                generateUniqueKeys.Checked = r.ServerParameters.GenerateUniqueKeys;
                allowDimensionalCrossjoins.Checked = r.ServerParameters.AllowDimensionalCrossjoins;
                stagingEncryptionKeyTextBox.Text = MainAdminForm.decrypt(r.ServerParameters.StagingEncryptionKey, MainAdminForm.SecretPassword);
                ProxyDBConnectionStringTextBox.Text = r.ServerParameters.ProxyDBConnectionString;
                ProxyDBUserNameTextBox.Text = MainAdminForm.decrypt(r.ServerParameters.ProxyDBUserName, MainAdminForm.SecretPassword);
                ProxyDBPasswordTextBox.Text = MainAdminForm.decrypt(r.ServerParameters.ProxyDBPassword, MainAdminForm.SecretPassword);
                disableMeasureTableCacheBox.Checked = r.ServerParameters.DisableCachingForGeneratedMeasureTables;
                disableCachingForDimensionsBox.Checked = r.ServerParameters.DisableCachingForGeneratedDimensionTables;
                snowflakeSize.Text = r.ServerParameters.MaxSnowflakeSize.ToString();
                genSABox.Checked = r.GenerateSubjectArea;
                if (r.ServerParameters.ProcessingTimeZone != null && !r.ServerParameters.ProcessingTimeZone.Equals("") && !r.ServerParameters.ProcessingTimeZone.Equals(Util.GLOBAL_TIMEZONE_DEFAULT))
                {
                    TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(r.ServerParameters.ProcessingTimeZone);
                    ProcessingTimeZoneComboBox.SelectedIndex = ProcessingTimeZoneComboBox.FindStringExact(tz.DisplayName);
                }
                else
                    ProcessingTimeZoneComboBox.SelectedItem = ProcessingTimeZoneComboBox.Items[0];
                if (r.ServerParameters.DisplayTimeZone != null && !r.ServerParameters.DisplayTimeZone.Equals("") &&
                    !r.ServerParameters.DisplayTimeZone.Equals(Util.DISPLAY_TIMEZONE_DEFAULT) && !r.ServerParameters.DisplayTimeZone.Equals(Util.GLOBAL_TIMEZONE_DEFAULT)) 
                {
                    TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(r.ServerParameters.DisplayTimeZone);
                    DisplayTimeZoneComboBox.SelectedIndex = DisplayTimeZoneComboBox.FindStringExact(tz.DisplayName);
                }
                else
                    DisplayTimeZoneComboBox.SelectedItem = DisplayTimeZoneComboBox.Items[0];
                Collation = r.ServerParameters.Collation;
                BulkDeleteBatchSize = r.ServerParameters.BulkDeleteBatchSize;
                UseNewIncrementalDeleteOnIB = r.ServerParameters.DisableNewIncrementalDeleteOnIB;
                IBIncrementalDeleteThresholdValue = r.ServerParameters.IBIncrementalDeleteThresholdValue;
                MaxThreadsInETL = r.ServerParameters.MaxThreadsInETL;
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return serverTabPage;
            }
            set
            {
            }
        }


        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

    }
}