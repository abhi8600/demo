using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    interface RepositoryModule
    {
        void setRepository(Repository r);
        void updateFromRepository(Repository r);
        void updateFromRepositoryFinal(Repository r);
        void setFilters(Repository r, List<QueryFilter> list);
        void setup();
        void setupNodes(TreeNode rootNode);
        bool selectedNode(TreeNode node);
        void replaceColumn(string find, string replace, bool match);
        void findColumn(List<string> results, string find, bool match);
        void implicitSave();
        void setRepositoryDirectory(string d);
        string ModuleName
        {
            get;
            set;
        }
        string CategoryName
        {
            get;
            set;
        }
        TabPage TabPage
        {
            get;
            set;
        }
    }
}
