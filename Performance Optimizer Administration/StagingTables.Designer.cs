namespace Performance_Optimizer_Administration
{
    partial class StagingTables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.stagingTablePage = new System.Windows.Forms.TabPage();
            this.stagingPanel = new System.Windows.Forms.Panel();
            this.btnViewSnapshotDeleteKeys = new System.Windows.Forms.Button();
            this.btnViewScript = new System.Windows.Forms.Button();
            this.scdDateNameBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cksumGroupBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.incrementalSnapshotFact = new System.Windows.Forms.CheckBox();
            this.btnViewQuery = new System.Windows.Forms.Button();
            this.sourceGroupsBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.webReadOnly = new System.Windows.Forms.CheckBox();
            this.webOptional = new System.Windows.Forms.CheckBox();
            this.persistBox = new System.Windows.Forms.CheckBox();
            this.transformListBox = new System.Windows.Forms.ListBox();
            this.queryBox = new System.Windows.Forms.TextBox();
            this.queryButton = new System.Windows.Forms.RadioButton();
            this.fileButton = new System.Windows.Forms.RadioButton();
            this.downTransformButton = new System.Windows.Forms.Button();
            this.upTransformButton = new System.Windows.Forms.Button();
            this.loadGroupsListBox = new System.Windows.Forms.ListView();
            this.LoadGroupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.setFilterButton = new System.Windows.Forms.Button();
            this.editTransformButton = new System.Windows.Forms.Button();
            this.removeTransformButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.addTransformButton = new System.Windows.Forms.Button();
            this.dataSourceBox = new System.Windows.Forms.ComboBox();
            this.levelListBox = new System.Windows.Forms.ListView();
            this.dimNameCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.levelColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.levelFilterColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.stagingTableGridView = new System.Windows.Forms.DataGridView();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SourceFileColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Transformation = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TargetTypes = new System.Windows.Forms.DataGridViewButtonColumn();
            this.NaturalKey = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TableKey = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GenerateTimeDimension = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Index = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ExcludeFromChecksum = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GrainInfo = new System.Windows.Forms.DataGridViewButtonColumn();
            this.UnknownValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stagingColumnContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setTargetTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setGrainInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.stagingTableMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newStagingTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeStagingTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateFromSourceFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.stagingTablePage.SuspendLayout();
            this.stagingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stagingTableGridView)).BeginInit();
            this.stagingColumnContextMenu.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.stagingTableMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // stagingTablePage
            // 
            this.stagingTablePage.AutoScroll = true;
            this.stagingTablePage.Controls.Add(this.stagingPanel);
            this.stagingTablePage.Controls.Add(this.stagingTableGridView);
            this.stagingTablePage.Location = new System.Drawing.Point(4, 22);
            this.stagingTablePage.Name = "stagingTablePage";
            this.stagingTablePage.Padding = new System.Windows.Forms.Padding(3);
            this.stagingTablePage.Size = new System.Drawing.Size(916, 707);
            this.stagingTablePage.TabIndex = 0;
            this.stagingTablePage.Text = "Staging Tables";
            this.stagingTablePage.UseVisualStyleBackColor = true;
            // 
            // stagingPanel
            // 
            this.stagingPanel.Controls.Add(this.btnViewSnapshotDeleteKeys);
            this.stagingPanel.Controls.Add(this.btnViewScript);
            this.stagingPanel.Controls.Add(this.scdDateNameBox);
            this.stagingPanel.Controls.Add(this.label6);
            this.stagingPanel.Controls.Add(this.cksumGroupBox);
            this.stagingPanel.Controls.Add(this.label5);
            this.stagingPanel.Controls.Add(this.incrementalSnapshotFact);
            this.stagingPanel.Controls.Add(this.btnViewQuery);
            this.stagingPanel.Controls.Add(this.sourceGroupsBox);
            this.stagingPanel.Controls.Add(this.label2);
            this.stagingPanel.Controls.Add(this.webReadOnly);
            this.stagingPanel.Controls.Add(this.webOptional);
            this.stagingPanel.Controls.Add(this.persistBox);
            this.stagingPanel.Controls.Add(this.transformListBox);
            this.stagingPanel.Controls.Add(this.queryBox);
            this.stagingPanel.Controls.Add(this.queryButton);
            this.stagingPanel.Controls.Add(this.fileButton);
            this.stagingPanel.Controls.Add(this.downTransformButton);
            this.stagingPanel.Controls.Add(this.upTransformButton);
            this.stagingPanel.Controls.Add(this.loadGroupsListBox);
            this.stagingPanel.Controls.Add(this.label4);
            this.stagingPanel.Controls.Add(this.setFilterButton);
            this.stagingPanel.Controls.Add(this.editTransformButton);
            this.stagingPanel.Controls.Add(this.removeTransformButton);
            this.stagingPanel.Controls.Add(this.label3);
            this.stagingPanel.Controls.Add(this.addTransformButton);
            this.stagingPanel.Controls.Add(this.dataSourceBox);
            this.stagingPanel.Controls.Add(this.levelListBox);
            this.stagingPanel.Controls.Add(this.label1);
            this.stagingPanel.Location = new System.Drawing.Point(0, 3);
            this.stagingPanel.Name = "stagingPanel";
            this.stagingPanel.Size = new System.Drawing.Size(895, 232);
            this.stagingPanel.TabIndex = 3;
            // 
            // btnViewSnapshotDeleteKeys
            // 
            this.btnViewSnapshotDeleteKeys.Enabled = false;
            this.btnViewSnapshotDeleteKeys.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewSnapshotDeleteKeys.Location = new System.Drawing.Point(385, 201);
            this.btnViewSnapshotDeleteKeys.Name = "btnViewSnapshotDeleteKeys";
            this.btnViewSnapshotDeleteKeys.Size = new System.Drawing.Size(102, 23);
            this.btnViewSnapshotDeleteKeys.TabIndex = 32;
            this.btnViewSnapshotDeleteKeys.Text = "View Delete Keys";
            this.btnViewSnapshotDeleteKeys.UseVisualStyleBackColor = true;
            this.btnViewSnapshotDeleteKeys.Click += new System.EventHandler(this.btnViewSnapshotDeleteKeys_Click);
            // 
            // btnViewScript
            // 
            this.btnViewScript.Enabled = false;
            this.btnViewScript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewScript.Location = new System.Drawing.Point(6, 53);
            this.btnViewScript.Name = "btnViewScript";
            this.btnViewScript.Size = new System.Drawing.Size(92, 23);
            this.btnViewScript.TabIndex = 31;
            this.btnViewScript.Text = "View Script";
            this.btnViewScript.UseVisualStyleBackColor = true;
            this.btnViewScript.Click += new System.EventHandler(this.btnViewScript_Click);
            // 
            // scdDateNameBox
            // 
            this.scdDateNameBox.Enabled = false;
            this.scdDateNameBox.FormattingEnabled = true;
            this.scdDateNameBox.Location = new System.Drawing.Point(791, 191);
            this.scdDateNameBox.Name = "scdDateNameBox";
            this.scdDateNameBox.Size = new System.Drawing.Size(101, 21);
            this.scdDateNameBox.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(792, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "SCD Date Name";
            // 
            // cksumGroupBox
            // 
            this.cksumGroupBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cksumGroupBox.Location = new System.Drawing.Point(639, 190);
            this.cksumGroupBox.Name = "cksumGroupBox";
            this.cksumGroupBox.Size = new System.Drawing.Size(134, 20);
            this.cksumGroupBox.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(636, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Checksum Group";
            // 
            // incrementalSnapshotFact
            // 
            this.incrementalSnapshotFact.AutoSize = true;
            this.incrementalSnapshotFact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.incrementalSnapshotFact.Location = new System.Drawing.Point(227, 204);
            this.incrementalSnapshotFact.Name = "incrementalSnapshotFact";
            this.incrementalSnapshotFact.Size = new System.Drawing.Size(150, 17);
            this.incrementalSnapshotFact.TabIndex = 26;
            this.incrementalSnapshotFact.Text = "Incremental Snapshot Fact";
            this.incrementalSnapshotFact.UseVisualStyleBackColor = true;
            this.incrementalSnapshotFact.CheckedChanged += new System.EventHandler(this.incrementalSnapshotFact_CheckedChanged);
            // 
            // btnViewQuery
            // 
            this.btnViewQuery.Enabled = false;
            this.btnViewQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewQuery.Location = new System.Drawing.Point(318, 1);
            this.btnViewQuery.Name = "btnViewQuery";
            this.btnViewQuery.Size = new System.Drawing.Size(92, 23);
            this.btnViewQuery.TabIndex = 25;
            this.btnViewQuery.Text = "View Query";
            this.btnViewQuery.UseVisualStyleBackColor = true;
            this.btnViewQuery.Click += new System.EventHandler(this.btnViewQuery_Click);
            // 
            // sourceGroupsBox
            // 
            this.sourceGroupsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceGroupsBox.Location = new System.Drawing.Point(497, 190);
            this.sourceGroupsBox.Name = "sourceGroupsBox";
            this.sourceGroupsBox.Size = new System.Drawing.Size(134, 20);
            this.sourceGroupsBox.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(494, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Source Groups";
            // 
            // webReadOnly
            // 
            this.webReadOnly.AutoSize = true;
            this.webReadOnly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.webReadOnly.Location = new System.Drawing.Point(108, 204);
            this.webReadOnly.Name = "webReadOnly";
            this.webReadOnly.Size = new System.Drawing.Size(97, 17);
            this.webReadOnly.TabIndex = 22;
            this.webReadOnly.Text = "Web Read-only";
            this.webReadOnly.UseVisualStyleBackColor = true;
            // 
            // webOptional
            // 
            this.webOptional.AutoSize = true;
            this.webOptional.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.webOptional.Location = new System.Drawing.Point(6, 204);
            this.webOptional.Name = "webOptional";
            this.webOptional.Size = new System.Drawing.Size(88, 17);
            this.webOptional.TabIndex = 21;
            this.webOptional.Text = "Web Optional";
            this.webOptional.UseVisualStyleBackColor = true;
            // 
            // persistBox
            // 
            this.persistBox.AutoSize = true;
            this.persistBox.Checked = true;
            this.persistBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.persistBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.persistBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.persistBox.Location = new System.Drawing.Point(227, 3);
            this.persistBox.Name = "persistBox";
            this.persistBox.Size = new System.Drawing.Size(85, 17);
            this.persistBox.TabIndex = 20;
            this.persistBox.Text = "Persist Query";
            this.persistBox.UseVisualStyleBackColor = true;
            // 
            // transformListBox
            // 
            this.transformListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.transformListBox.FormattingEnabled = true;
            this.transformListBox.HorizontalScrollbar = true;
            this.transformListBox.Location = new System.Drawing.Point(497, 18);
            this.transformListBox.Name = "transformListBox";
            this.transformListBox.Size = new System.Drawing.Size(134, 145);
            this.transformListBox.TabIndex = 6;
            this.transformListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.StagingTables_MouseMove);
            // 
            // queryBox
            // 
            this.queryBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.queryBox.Location = new System.Drawing.Point(132, 26);
            this.queryBox.MaxLength = 65535;
            this.queryBox.Multiline = true;
            this.queryBox.Name = "queryBox";
            this.queryBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.queryBox.Size = new System.Drawing.Size(359, 67);
            this.queryBox.TabIndex = 19;
            // 
            // queryButton
            // 
            this.queryButton.AutoSize = true;
            this.queryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.queryButton.Location = new System.Drawing.Point(132, 3);
            this.queryButton.Name = "queryButton";
            this.queryButton.Size = new System.Drawing.Size(89, 17);
            this.queryButton.TabIndex = 18;
            this.queryButton.Text = "Query Source";
            this.queryButton.UseVisualStyleBackColor = true;
            this.queryButton.CheckedChanged += new System.EventHandler(this.queryButton_CheckedChanged);
            this.queryButton.Click += new System.EventHandler(this.queryButton_Click);
            // 
            // fileButton
            // 
            this.fileButton.AutoSize = true;
            this.fileButton.Checked = true;
            this.fileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fileButton.Location = new System.Drawing.Point(6, 3);
            this.fileButton.Name = "fileButton";
            this.fileButton.Size = new System.Drawing.Size(77, 17);
            this.fileButton.TabIndex = 17;
            this.fileButton.TabStop = true;
            this.fileButton.Text = "File Source";
            this.fileButton.UseVisualStyleBackColor = true;
            this.fileButton.Click += new System.EventHandler(this.fileButton_Click);
            // 
            // downTransformButton
            // 
            this.downTransformButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.downTransformButton.Location = new System.Drawing.Point(639, 140);
            this.downTransformButton.Name = "downTransformButton";
            this.downTransformButton.Size = new System.Drawing.Size(71, 23);
            this.downTransformButton.TabIndex = 16;
            this.downTransformButton.Text = "Down";
            this.downTransformButton.UseVisualStyleBackColor = true;
            this.downTransformButton.Click += new System.EventHandler(this.downTransformButton_Click);
            // 
            // upTransformButton
            // 
            this.upTransformButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upTransformButton.Location = new System.Drawing.Point(639, 111);
            this.upTransformButton.Name = "upTransformButton";
            this.upTransformButton.Size = new System.Drawing.Size(71, 23);
            this.upTransformButton.TabIndex = 15;
            this.upTransformButton.Text = "Up";
            this.upTransformButton.UseVisualStyleBackColor = true;
            this.upTransformButton.Click += new System.EventHandler(this.upTransformButton_Click);
            // 
            // loadGroupsListBox
            // 
            this.loadGroupsListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.loadGroupsListBox.CheckBoxes = true;
            this.loadGroupsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LoadGroupName});
            this.loadGroupsListBox.FullRowSelect = true;
            this.loadGroupsListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.loadGroupsListBox.Location = new System.Drawing.Point(717, 18);
            this.loadGroupsListBox.Name = "loadGroupsListBox";
            this.loadGroupsListBox.ShowItemToolTips = true;
            this.loadGroupsListBox.Size = new System.Drawing.Size(119, 145);
            this.loadGroupsListBox.TabIndex = 14;
            this.loadGroupsListBox.UseCompatibleStateImageBehavior = false;
            this.loadGroupsListBox.View = System.Windows.Forms.View.Details;
            // 
            // LoadGroupName
            // 
            this.LoadGroupName.Text = "Name";
            this.LoadGroupName.Width = 118;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(714, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Load Groups";
            // 
            // setFilterButton
            // 
            this.setFilterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setFilterButton.Location = new System.Drawing.Point(431, 99);
            this.setFilterButton.Name = "setFilterButton";
            this.setFilterButton.Size = new System.Drawing.Size(60, 25);
            this.setFilterButton.TabIndex = 11;
            this.setFilterButton.Text = "Set Filter";
            this.setFilterButton.UseVisualStyleBackColor = true;
            this.setFilterButton.Click += new System.EventHandler(this.setFilterButton_Click);
            // 
            // editTransformButton
            // 
            this.editTransformButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editTransformButton.Location = new System.Drawing.Point(639, 53);
            this.editTransformButton.Name = "editTransformButton";
            this.editTransformButton.Size = new System.Drawing.Size(71, 23);
            this.editTransformButton.TabIndex = 10;
            this.editTransformButton.Text = "Edit";
            this.editTransformButton.UseVisualStyleBackColor = true;
            this.editTransformButton.Click += new System.EventHandler(this.editTransformButton_Click);
            // 
            // removeTransformButton
            // 
            this.removeTransformButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeTransformButton.Location = new System.Drawing.Point(639, 82);
            this.removeTransformButton.Name = "removeTransformButton";
            this.removeTransformButton.Size = new System.Drawing.Size(71, 23);
            this.removeTransformButton.TabIndex = 9;
            this.removeTransformButton.Text = "Remove";
            this.removeTransformButton.UseVisualStyleBackColor = true;
            this.removeTransformButton.Click += new System.EventHandler(this.removeTransformButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(494, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Staging Table Transformations";
            // 
            // addTransformButton
            // 
            this.addTransformButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addTransformButton.Location = new System.Drawing.Point(639, 26);
            this.addTransformButton.Name = "addTransformButton";
            this.addTransformButton.Size = new System.Drawing.Size(71, 23);
            this.addTransformButton.TabIndex = 7;
            this.addTransformButton.Text = "Add";
            this.addTransformButton.UseVisualStyleBackColor = true;
            this.addTransformButton.Click += new System.EventHandler(this.addTransformButton_Click);
            // 
            // dataSourceBox
            // 
            this.dataSourceBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataSourceBox.FormattingEnabled = true;
            this.dataSourceBox.Location = new System.Drawing.Point(6, 25);
            this.dataSourceBox.Name = "dataSourceBox";
            this.dataSourceBox.Size = new System.Drawing.Size(120, 21);
            this.dataSourceBox.TabIndex = 5;
            // 
            // levelListBox
            // 
            this.levelListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.levelListBox.CheckBoxes = true;
            this.levelListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dimNameCol,
            this.levelColumn,
            this.levelFilterColumn});
            this.levelListBox.FullRowSelect = true;
            this.levelListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.levelListBox.Location = new System.Drawing.Point(36, 99);
            this.levelListBox.Name = "levelListBox";
            this.levelListBox.ShowItemToolTips = true;
            this.levelListBox.Size = new System.Drawing.Size(389, 99);
            this.levelListBox.TabIndex = 3;
            this.levelListBox.UseCompatibleStateImageBehavior = false;
            this.levelListBox.View = System.Windows.Forms.View.Details;
            this.levelListBox.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.levelListBox_ItemChecked);
            // 
            // dimNameCol
            // 
            this.dimNameCol.Text = "Dimension";
            this.dimNameCol.Width = 104;
            // 
            // levelColumn
            // 
            this.levelColumn.Text = "Level";
            this.levelColumn.Width = 110;
            // 
            // levelFilterColumn
            // 
            this.levelFilterColumn.Text = "Filter";
            this.levelFilterColumn.Width = 169;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Grain";
            // 
            // stagingTableGridView
            // 
            this.stagingTableGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.stagingTableGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.stagingTableGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stagingTableGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.DataType,
            this.ColWidth,
            this.SourceFileColumn,
            this.Transformation,
            this.TargetTypes,
            this.NaturalKey,
            this.TableKey,
            this.GenerateTimeDimension,
            this.Index,
            this.ExcludeFromChecksum,
            this.GrainInfo,
            this.UnknownValue});
            this.stagingTableGridView.ContextMenuStrip = this.stagingColumnContextMenu;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.stagingTableGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.stagingTableGridView.Location = new System.Drawing.Point(1, 241);
            this.stagingTableGridView.Name = "stagingTableGridView";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.stagingTableGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.stagingTableGridView.Size = new System.Drawing.Size(912, 466);
            this.stagingTableGridView.TabIndex = 0;
            this.stagingTableGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.stagingTableGridView_CellBeginEdit);
            this.stagingTableGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.stagingTableGridView_CellClick);
            this.stagingTableGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.stagingTableGridView_CellContentClick);
            this.stagingTableGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.stagingTableGridView_CellEndEdit);
            this.stagingTableGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.stagingTableGridView_CellFormatting);
            this.stagingTableGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.stagingTableGridView_CellValidated);
            this.stagingTableGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataErrorHandler);
            this.stagingTableGridView.SelectionChanged += new System.EventHandler(this.stagingTableGridView_SelectionChanged);
            this.stagingTableGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.stagingTableGridView_UserDeletedRow);
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "ColumnName";
            this.ColumnName.Frozen = true;
            this.ColumnName.HeaderText = "Column Name";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.Width = 250;
            // 
            // DataType
            // 
            this.DataType.DataPropertyName = "DataType";
            this.DataType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DataType.HeaderText = "Data Type";
            this.DataType.Name = "DataType";
            this.DataType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DataType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DataType.Width = 80;
            // 
            // ColWidth
            // 
            this.ColWidth.DataPropertyName = "Width";
            this.ColWidth.HeaderText = "Width";
            this.ColWidth.Name = "ColWidth";
            this.ColWidth.Width = 40;
            // 
            // SourceFileColumn
            // 
            this.SourceFileColumn.DataPropertyName = "SourceFileColumn";
            this.SourceFileColumn.HeaderText = "Source File Column";
            this.SourceFileColumn.Name = "SourceFileColumn";
            this.SourceFileColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SourceFileColumn.Width = 150;
            // 
            // Transformation
            // 
            this.Transformation.DataPropertyName = "Transformation";
            this.Transformation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Transformation.HeaderText = "Transformation";
            this.Transformation.Name = "Transformation";
            this.Transformation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Transformation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Transformation.Width = 80;
            // 
            // TargetTypes
            // 
            this.TargetTypes.DataPropertyName = "TargetTypes";
            this.TargetTypes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TargetTypes.HeaderText = "Target Types";
            this.TargetTypes.Name = "TargetTypes";
            this.TargetTypes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TargetTypes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // NaturalKey
            // 
            this.NaturalKey.DataPropertyName = "NaturalKey";
            this.NaturalKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NaturalKey.HeaderText = "Natural Key";
            this.NaturalKey.Name = "NaturalKey";
            this.NaturalKey.Width = 50;
            // 
            // TableKey
            // 
            this.TableKey.DataPropertyName = "TableKey";
            this.TableKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TableKey.HeaderText = "Table Key";
            this.TableKey.Name = "TableKey";
            this.TableKey.Width = 50;
            // 
            // GenerateTimeDimension
            // 
            this.GenerateTimeDimension.DataPropertyName = "GenerateTimeDimension";
            this.GenerateTimeDimension.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GenerateTimeDimension.HeaderText = "Analyze Time";
            this.GenerateTimeDimension.Name = "GenerateTimeDimension";
            this.GenerateTimeDimension.Width = 50;
            // 
            // Index
            // 
            this.Index.DataPropertyName = "Index";
            this.Index.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.Width = 50;
            // 
            // ExcludeFromChecksum
            // 
            this.ExcludeFromChecksum.DataPropertyName = "ExcludeFromChecksum";
            this.ExcludeFromChecksum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExcludeFromChecksum.HeaderText = "Exclude From Checksum";
            this.ExcludeFromChecksum.Name = "ExcludeFromChecksum";
            this.ExcludeFromChecksum.Width = 65;
            // 
            // GrainInfo
            // 
            this.GrainInfo.DataPropertyName = "GrainInfo";
            this.GrainInfo.HeaderText = "Grain Info";
            this.GrainInfo.Name = "GrainInfo";
            this.GrainInfo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.GrainInfo.Width = 120;
            // 
            // UnknownValue
            // 
            this.UnknownValue.DataPropertyName = "UnknownValue";
            this.UnknownValue.HeaderText = "UnknownValue";
            this.UnknownValue.Name = "UnknownValue";
            // 
            // stagingColumnContextMenu
            // 
            this.stagingColumnContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setTargetTypeToolStripMenuItem,
            this.setGrainInfoToolStripMenuItem});
            this.stagingColumnContextMenu.Name = "stagingColumnContextMenu";
            this.stagingColumnContextMenu.Size = new System.Drawing.Size(162, 48);
            // 
            // setTargetTypeToolStripMenuItem
            // 
            this.setTargetTypeToolStripMenuItem.Name = "setTargetTypeToolStripMenuItem";
            this.setTargetTypeToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.setTargetTypeToolStripMenuItem.Text = "Set &Target Types";
            this.setTargetTypeToolStripMenuItem.Click += new System.EventHandler(this.setTargetTypeToolStripMenuItem_Click);
            // 
            // setGrainInfoToolStripMenuItem
            // 
            this.setGrainInfoToolStripMenuItem.Name = "setGrainInfoToolStripMenuItem";
            this.setGrainInfoToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.setGrainInfoToolStripMenuItem.Text = "Set &Grain Info";
            this.setGrainInfoToolStripMenuItem.Click += new System.EventHandler(this.setGrainInfoToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.stagingTablePage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(924, 733);
            this.tabControl1.TabIndex = 0;
            // 
            // stagingTableMenuStrip
            // 
            this.stagingTableMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStagingTableToolStripMenuItem,
            this.removeStagingTableToolStripMenuItem,
            this.updateFromSourceFileToolStripMenuItem});
            this.stagingTableMenuStrip.Name = "sourceFileMenuStrip";
            this.stagingTableMenuStrip.Size = new System.Drawing.Size(210, 70);
            // 
            // newStagingTableToolStripMenuItem
            // 
            this.newStagingTableToolStripMenuItem.Name = "newStagingTableToolStripMenuItem";
            this.newStagingTableToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.newStagingTableToolStripMenuItem.Text = "&New Staging Table";
            this.newStagingTableToolStripMenuItem.Click += new System.EventHandler(this.newStagingTableToolStripMenuItem_Click);
            // 
            // removeStagingTableToolStripMenuItem
            // 
            this.removeStagingTableToolStripMenuItem.Name = "removeStagingTableToolStripMenuItem";
            this.removeStagingTableToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.removeStagingTableToolStripMenuItem.Text = "&Remove Staging Table";
            this.removeStagingTableToolStripMenuItem.Click += new System.EventHandler(this.removeStagingTableToolStripMenuItem_Click);
            // 
            // updateFromSourceFileToolStripMenuItem
            // 
            this.updateFromSourceFileToolStripMenuItem.Name = "updateFromSourceFileToolStripMenuItem";
            this.updateFromSourceFileToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.updateFromSourceFileToolStripMenuItem.Text = "&Update From Data Source";
            this.updateFromSourceFileToolStripMenuItem.Click += new System.EventHandler(this.updateFromSourceFileToolStripMenuItem_Click);
            // 
            // StagingTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(924, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "StagingTables";
            this.Text = "StagingTables";
            this.stagingTablePage.ResumeLayout(false);
            this.stagingPanel.ResumeLayout(false);
            this.stagingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stagingTableGridView)).EndInit();
            this.stagingColumnContextMenu.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.stagingTableMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage stagingTablePage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ContextMenuStrip stagingTableMenuStrip;
        private System.Windows.Forms.DataGridView stagingTableGridView;
        private System.Windows.Forms.ToolStripMenuItem newStagingTableToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel stagingPanel;
        private System.Windows.Forms.ColumnHeader levelColumn;
        private System.Windows.Forms.ColumnHeader dimNameCol;
        private System.Windows.Forms.ComboBox dataSourceBox;
        private System.Windows.Forms.Button addTransformButton;
        private System.Windows.Forms.ListBox transformListBox;
        private System.Windows.Forms.Button removeTransformButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button editTransformButton;
        private System.Windows.Forms.ToolStripMenuItem removeStagingTableToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader levelFilterColumn;
        private System.Windows.Forms.ToolStripMenuItem updateFromSourceFileToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView loadGroupsListBox;
        private System.Windows.Forms.ColumnHeader LoadGroupName;
        private System.Windows.Forms.Button downTransformButton;
        private System.Windows.Forms.Button upTransformButton;
        private System.Windows.Forms.Button setFilterButton;
        private System.Windows.Forms.ContextMenuStrip stagingColumnContextMenu;
        private System.Windows.Forms.ToolStripMenuItem setTargetTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setGrainInfoToolStripMenuItem;
        public System.Windows.Forms.ListView levelListBox;
        private System.Windows.Forms.RadioButton queryButton;
        private System.Windows.Forms.RadioButton fileButton;
        private System.Windows.Forms.TextBox queryBox;
        private System.Windows.Forms.CheckBox persistBox;
        private System.Windows.Forms.CheckBox webReadOnly;
        private System.Windows.Forms.CheckBox webOptional;
        private System.Windows.Forms.TextBox sourceGroupsBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnViewQuery;
        private System.Windows.Forms.CheckBox incrementalSnapshotFact;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewComboBoxColumn DataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn SourceFileColumn;
        private System.Windows.Forms.DataGridViewButtonColumn Transformation;
        private System.Windows.Forms.DataGridViewButtonColumn TargetTypes;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NaturalKey;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TableKey;
        private System.Windows.Forms.DataGridViewCheckBoxColumn GenerateTimeDimension;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Index;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ExcludeFromChecksum;
        private System.Windows.Forms.DataGridViewButtonColumn GrainInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnknownValue;
        private System.Windows.Forms.TextBox cksumGroupBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox scdDateNameBox;
        private System.Windows.Forms.Button btnViewScript;
        private System.Windows.Forms.Button btnViewSnapshotDeleteKeys;
    }
}