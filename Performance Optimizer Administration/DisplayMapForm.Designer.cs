namespace Performance_Optimizer_Administration
{
    partial class DisplayMapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DisplayMapForm));
            this.dataSet = new System.Data.DataSet();
            this.mapTable = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.bucketTable = new System.Data.DataTable();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.keyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.defaultBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.delMapButton = new System.Windows.Forms.Button();
            this.staticValueButton = new System.Windows.Forms.RadioButton();
            this.queryButton = new System.Windows.Forms.RadioButton();
            this.bucketButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.bucketGridView = new System.Windows.Forms.DataGridView();
            this.Category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Miin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Max = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lookupBox = new System.Windows.Forms.TextBox();
            this.lookupButton = new System.Windows.Forms.RadioButton();
            this.lookupTableBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lookupJoinBox = new System.Windows.Forms.TextBox();
            this.lookupFilterBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.outerJoinBox = new System.Windows.Forms.CheckBox();
            this.dataTypeBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bucketTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bucketGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSet
            // 
            this.dataSet.DataSetName = "NewDataSet";
            this.dataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.mapTable,
            this.bucketTable});
            // 
            // mapTable
            // 
            this.mapTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.mapTable.TableName = "MapTable";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Key";
            this.dataColumn1.DefaultValue = "";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Value";
            this.dataColumn2.DefaultValue = "";
            // 
            // bucketTable
            // 
            this.bucketTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5});
            this.bucketTable.TableName = "BucketTable";
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "Category";
            this.dataColumn3.DefaultValue = "";
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "Min";
            this.dataColumn4.DataType = typeof(double);
            this.dataColumn4.DefaultValue = 0;
            // 
            // dataColumn5
            // 
            this.dataColumn5.AllowDBNull = false;
            this.dataColumn5.ColumnName = "Max";
            this.dataColumn5.DataType = typeof(double);
            this.dataColumn5.DefaultValue = 0;
            // 
            // dataGridView
            // 
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.keyDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn});
            this.dataGridView.DataMember = "MapTable";
            this.dataGridView.DataSource = this.dataSet;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView.Location = new System.Drawing.Point(12, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView.Size = new System.Drawing.Size(421, 128);
            this.dataGridView.TabIndex = 0;
            // 
            // keyDataGridViewTextBoxColumn
            // 
            this.keyDataGridViewTextBoxColumn.DataPropertyName = "Key";
            this.keyDataGridViewTextBoxColumn.HeaderText = "Key";
            this.keyDataGridViewTextBoxColumn.Name = "keyDataGridViewTextBoxColumn";
            this.keyDataGridViewTextBoxColumn.Width = 189;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.Width = 189;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 425);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Default Value";
            // 
            // defaultBox
            // 
            this.defaultBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.defaultBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.defaultBox.Location = new System.Drawing.Point(111, 425);
            this.defaultBox.Name = "defaultBox";
            this.defaultBox.Size = new System.Drawing.Size(322, 20);
            this.defaultBox.TabIndex = 2;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(647, 460);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(809, 460);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(421, 27);
            this.label2.TabIndex = 5;
            this.label2.Text = "Enter key-value pairs to map data results to specific display values. All values " +
                "will be replaced with their display values.\r\n";
            // 
            // delMapButton
            // 
            this.delMapButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.delMapButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.delMapButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delMapButton.Location = new System.Drawing.Point(728, 460);
            this.delMapButton.Name = "delMapButton";
            this.delMapButton.Size = new System.Drawing.Size(75, 23);
            this.delMapButton.TabIndex = 6;
            this.delMapButton.Text = "Delete Map";
            this.delMapButton.UseVisualStyleBackColor = true;
            this.delMapButton.Click += new System.EventHandler(this.delMapButton_Click);
            // 
            // staticValueButton
            // 
            this.staticValueButton.AutoSize = true;
            this.staticValueButton.Checked = true;
            this.staticValueButton.Location = new System.Drawing.Point(12, 12);
            this.staticValueButton.Name = "staticValueButton";
            this.staticValueButton.Size = new System.Drawing.Size(87, 17);
            this.staticValueButton.TabIndex = 7;
            this.staticValueButton.TabStop = true;
            this.staticValueButton.Text = "Static Values";
            this.staticValueButton.UseVisualStyleBackColor = true;
            // 
            // queryButton
            // 
            this.queryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.queryButton.AutoSize = true;
            this.queryButton.Location = new System.Drawing.Point(463, 12);
            this.queryButton.Name = "queryButton";
            this.queryButton.Size = new System.Drawing.Size(90, 17);
            this.queryButton.TabIndex = 8;
            this.queryButton.Text = "Logical Query";
            this.queryButton.UseVisualStyleBackColor = true;
            // 
            // bucketButton
            // 
            this.bucketButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bucketButton.AutoSize = true;
            this.bucketButton.Location = new System.Drawing.Point(9, 195);
            this.bucketButton.Name = "bucketButton";
            this.bucketButton.Size = new System.Drawing.Size(64, 17);
            this.bucketButton.TabIndex = 10;
            this.bucketButton.Text = "Buckets";
            this.bucketButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoEllipsis = true;
            this.label3.Location = new System.Drawing.Point(9, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(424, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Enter ranges of values to define buckets.";
            // 
            // bucketGridView
            // 
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bucketGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.bucketGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.bucketGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.bucketGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bucketGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.bucketGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bucketGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Category,
            this.Miin,
            this.Max});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bucketGridView.DefaultCellStyle = dataGridViewCellStyle7;
            this.bucketGridView.Location = new System.Drawing.Point(12, 233);
            this.bucketGridView.Name = "bucketGridView";
            this.bucketGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bucketGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.bucketGridView.Size = new System.Drawing.Size(421, 167);
            this.bucketGridView.TabIndex = 94;
            // 
            // Category
            // 
            this.Category.DataPropertyName = "Category";
            this.Category.FillWeight = 200F;
            this.Category.HeaderText = "Category Name";
            this.Category.Name = "Category";
            this.Category.Width = 189;
            // 
            // Miin
            // 
            this.Miin.DataPropertyName = "Min";
            this.Miin.HeaderText = "Minimum Value (>=)";
            this.Miin.Name = "Miin";
            this.Miin.Width = 95;
            // 
            // Max
            // 
            this.Max.DataPropertyName = "Max";
            this.Max.HeaderText = "Maximum Value (<)";
            this.Max.Name = "Max";
            this.Max.Width = 94;
            // 
            // queryBox
            // 
            this.queryBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.queryBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.queryBox.Location = new System.Drawing.Point(463, 65);
            this.queryBox.Multiline = true;
            this.queryBox.Name = "queryBox";
            this.queryBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.queryBox.Size = new System.Drawing.Size(421, 63);
            this.queryBox.TabIndex = 95;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoEllipsis = true;
            this.label4.Location = new System.Drawing.Point(459, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(425, 27);
            this.label4.TabIndex = 96;
            this.label4.Text = "Enter a logical query that defines the mapping. The query must contain two column" +
                "s in its results. The first column is the key, the second the value.";
            // 
            // lookupBox
            // 
            this.lookupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lookupBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupBox.Location = new System.Drawing.Point(463, 275);
            this.lookupBox.Multiline = true;
            this.lookupBox.Name = "lookupBox";
            this.lookupBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lookupBox.Size = new System.Drawing.Size(421, 38);
            this.lookupBox.TabIndex = 98;
            // 
            // lookupButton
            // 
            this.lookupButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lookupButton.AutoSize = true;
            this.lookupButton.Location = new System.Drawing.Point(462, 195);
            this.lookupButton.Name = "lookupButton";
            this.lookupButton.Size = new System.Drawing.Size(91, 17);
            this.lookupButton.TabIndex = 97;
            this.lookupButton.Text = "Table Lookup";
            this.lookupButton.UseVisualStyleBackColor = true;
            // 
            // lookupTableBox
            // 
            this.lookupTableBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lookupTableBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupTableBox.Location = new System.Drawing.Point(539, 219);
            this.lookupTableBox.Name = "lookupTableBox";
            this.lookupTableBox.Size = new System.Drawing.Size(200, 20);
            this.lookupTableBox.TabIndex = 101;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoEllipsis = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(460, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 100;
            this.label6.Text = "Lookup Table";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoEllipsis = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(460, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(424, 37);
            this.label7.TabIndex = 102;
            this.label7.Text = "Lookup Expression (value to be looked up; column references must be qualified by " +
                "physical table name: e.g. Table.column)";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(460, 326);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 13);
            this.label5.TabIndex = 104;
            this.label5.Text = "Join Column (reference column to look up)";
            // 
            // lookupJoinBox
            // 
            this.lookupJoinBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lookupJoinBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupJoinBox.Location = new System.Drawing.Point(671, 324);
            this.lookupJoinBox.Name = "lookupJoinBox";
            this.lookupJoinBox.Size = new System.Drawing.Size(213, 20);
            this.lookupJoinBox.TabIndex = 103;
            // 
            // lookupFilterBox
            // 
            this.lookupFilterBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lookupFilterBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lookupFilterBox.Location = new System.Drawing.Point(463, 362);
            this.lookupFilterBox.Multiline = true;
            this.lookupFilterBox.Name = "lookupFilterBox";
            this.lookupFilterBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lookupFilterBox.Size = new System.Drawing.Size(421, 38);
            this.lookupFilterBox.TabIndex = 105;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(460, 346);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(369, 13);
            this.label8.TabIndex = 106;
            this.label8.Text = "Lookup Filter Expression (filter to be applied to join condition; qualify columns" +
                ")";
            // 
            // outerJoinBox
            // 
            this.outerJoinBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.outerJoinBox.AutoEllipsis = true;
            this.outerJoinBox.Location = new System.Drawing.Point(787, 220);
            this.outerJoinBox.Name = "outerJoinBox";
            this.outerJoinBox.Size = new System.Drawing.Size(84, 17);
            this.outerJoinBox.TabIndex = 107;
            this.outerJoinBox.Text = "Outer Join";
            this.outerJoinBox.UseVisualStyleBackColor = true;
            // 
            // dataTypeBox
            // 
            this.dataTypeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataTypeBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataTypeBox.FormattingEnabled = true;
            this.dataTypeBox.Items.AddRange(new object[] {
            "Number",
            "Integer",
            "Varchar",
            "Date",
            "DateTime",
            "Float"});
            this.dataTypeBox.Location = new System.Drawing.Point(544, 425);
            this.dataTypeBox.Name = "dataTypeBox";
            this.dataTypeBox.Size = new System.Drawing.Size(121, 21);
            this.dataTypeBox.TabIndex = 109;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(460, 425);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 17);
            this.label9.TabIndex = 110;
            this.label9.Text = "Data Type";
            // 
            // DisplayMapForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(895, 495);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dataTypeBox);
            this.Controls.Add(this.outerJoinBox);
            this.Controls.Add(this.lookupFilterBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lookupJoinBox);
            this.Controls.Add(this.lookupTableBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lookupBox);
            this.Controls.Add(this.lookupButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.queryBox);
            this.Controls.Add(this.bucketGridView);
            this.Controls.Add(this.bucketButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.queryButton);
            this.Controls.Add(this.staticValueButton);
            this.Controls.Add(this.delMapButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.defaultBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DisplayMapForm";
            this.Text = "Map Displayed Results";
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bucketTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bucketGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Data.DataSet dataSet;
        private System.Data.DataTable mapTable;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox defaultBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button delMapButton;
        private System.Windows.Forms.RadioButton staticValueButton;
        private System.Windows.Forms.RadioButton queryButton;
        private System.Windows.Forms.RadioButton bucketButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Category;
        private System.Windows.Forms.DataGridViewTextBoxColumn Miin;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max;
        private System.Data.DataTable bucketTable;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Windows.Forms.TextBox queryBox;
        private System.Windows.Forms.DataGridView bucketGridView;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lookupBox;
        private System.Windows.Forms.RadioButton lookupButton;
        private System.Windows.Forms.TextBox lookupTableBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox lookupJoinBox;
        private System.Windows.Forms.TextBox lookupFilterBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox outerJoinBox;
        private System.Windows.Forms.ComboBox dataTypeBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn keyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
    }
}