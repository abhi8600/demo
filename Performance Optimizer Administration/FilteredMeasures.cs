using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class FilteredMeasures : Form, ResizeMemoryForm
    {
        MainAdminForm ma = null;
        FilteredMeasureDefinition fmd = null;
        private string windowName = "FilteredMeasures";
        public FilteredMeasures(MainAdminForm ma, FilteredMeasureDefinition fmd)
        {
            this.ma = ma;
            if (fmd == null)
                this.fmd = new FilteredMeasureDefinition();
            else
                this.fmd = fmd;
            InitializeComponent();
            filterTable.Clear();
            resetWindowFromSettings();
            if (fmd != null)
                for (int i = 0; i < fmd.Names.Length; i++)
                {
                    DataRow dr = filterTable.NewRow();
                    dr[0] = fmd.Names[i];
                    dr[1] = fmd.Filters[i];
                    if (fmd.Defaults != null && fmd.Defaults[i] != null)
                        dr[2] = fmd.Defaults[i];
                    filterTable.Rows.Add(dr);
                }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 2)
                return;
            Filters fs = new Filters(ma);
            fs.setupDimensionFilters();
            fs.filterList = ma.filterMod.filterList;
            fs.setupLists();
            fs.setDialog();
            QueryFilter qf = null;
            if (dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != System.DBNull.Value)
                qf = (QueryFilter)dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
            if (qf != null)
                fs.showFilter(qf);
            DialogResult dr = fs.ShowDialog();
            if (dr == DialogResult.OK)
            {
                qf = fs.getQueryFilter();
                dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = qf;
            }
        }

        public FilteredMeasureDefinition getFilteredMeasureDefinition()
        {
            if (fmd.Names.Length == 0)
                return (null);
            return (fmd);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            fmd.Names = new string[filterTable.Rows.Count];
            fmd.Filters = new QueryFilter[filterTable.Rows.Count];
            fmd.Defaults = new string[filterTable.Rows.Count];
            for (int i = 0; i < fmd.Names.Length; i++)
            {
                fmd.Names[i] = (string) filterTable.Rows[i][0];
                fmd.Filters[i] = (QueryFilter) filterTable.Rows[i][1];
                if (filterTable.Rows[i][2] != System.DBNull.Value)
                    fmd.Defaults[i] = (string) filterTable.Rows[i][2];
            }
            saveWindowSettings();
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            saveWindowSettings();
            this.Close();
        }

        private void FilteredMeasures_Load(object sender, EventArgs e)
        {


        }
        private void FilteredMeasure_FormClosing(object sender, FormClosingEventArgs e)
        {
            //TODO: Not really getting called, need to check
            saveWindowSettings();
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

    }
}