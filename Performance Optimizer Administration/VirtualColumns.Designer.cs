namespace Performance_Optimizer_Administration
{
    partial class VirtualColumns
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label6;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VirtualColumns));
            this.DefaultCategoryName = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.virtualColumnsTabPage = new System.Windows.Forms.TabPage();
            this.groupBoxVirtualColumns = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NullCategoryName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bucketGridView = new System.Windows.Forms.DataGridView();
            this.Category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Miin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Max = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.virtualColumnFilterBox = new System.Windows.Forms.CheckedListBox();
            this.bucketMeasure = new System.Windows.Forms.RadioButton();
            this.pivotLevel = new System.Windows.Forms.ComboBox();
            this.pivotPercentBox = new System.Windows.Forms.CheckBox();
            this.pivotMeasure = new System.Windows.Forms.RadioButton();
            this.pivotDimension = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.columnTableName = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.columnMeasure = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.vcMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newVirtualColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeVirtualColumnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            label6 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.virtualColumnsTabPage.SuspendLayout();
            this.groupBoxVirtualColumns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bucketGridView)).BeginInit();
            this.vcMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            label6.Location = new System.Drawing.Point(25, 528);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(565, 32);
            label6.TabIndex = 99;
            label6.Text = resources.GetString("label6.Text");
            // 
            // DefaultCategoryName
            // 
            this.DefaultCategoryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DefaultCategoryName.Location = new System.Drawing.Point(182, 267);
            this.DefaultCategoryName.Name = "DefaultCategoryName";
            this.DefaultCategoryName.Size = new System.Drawing.Size(235, 20);
            this.DefaultCategoryName.TabIndex = 94;
            this.DefaultCategoryName.TextChanged += new System.EventHandler(this.DefaultCategoryName_TextChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.virtualColumnsTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(783, 674);
            this.tabControl.TabIndex = 0;
            // 
            // virtualColumnsTabPage
            // 
            this.virtualColumnsTabPage.AutoScroll = true;
            this.virtualColumnsTabPage.Controls.Add(this.groupBoxVirtualColumns);
            this.virtualColumnsTabPage.Location = new System.Drawing.Point(4, 22);
            this.virtualColumnsTabPage.Name = "virtualColumnsTabPage";
            this.virtualColumnsTabPage.Size = new System.Drawing.Size(775, 648);
            this.virtualColumnsTabPage.TabIndex = 9;
            this.virtualColumnsTabPage.Text = "Virtual Columns";
            this.virtualColumnsTabPage.UseVisualStyleBackColor = true;
            this.virtualColumnsTabPage.Leave += new System.EventHandler(this.virtualColumnsTabPage_Leave);
            // 
            // groupBoxVirtualColumns
            // 
            this.groupBoxVirtualColumns.Controls.Add(label6);
            this.groupBoxVirtualColumns.Controls.Add(this.label3);
            this.groupBoxVirtualColumns.Controls.Add(this.label2);
            this.groupBoxVirtualColumns.Controls.Add(this.NullCategoryName);
            this.groupBoxVirtualColumns.Controls.Add(this.label4);
            this.groupBoxVirtualColumns.Controls.Add(this.label1);
            this.groupBoxVirtualColumns.Controls.Add(this.DefaultCategoryName);
            this.groupBoxVirtualColumns.Controls.Add(this.label5);
            this.groupBoxVirtualColumns.Controls.Add(this.bucketGridView);
            this.groupBoxVirtualColumns.Controls.Add(this.virtualColumnFilterBox);
            this.groupBoxVirtualColumns.Controls.Add(this.bucketMeasure);
            this.groupBoxVirtualColumns.Controls.Add(this.pivotLevel);
            this.groupBoxVirtualColumns.Controls.Add(this.pivotPercentBox);
            this.groupBoxVirtualColumns.Controls.Add(this.pivotMeasure);
            this.groupBoxVirtualColumns.Controls.Add(this.pivotDimension);
            this.groupBoxVirtualColumns.Controls.Add(this.label55);
            this.groupBoxVirtualColumns.Controls.Add(this.columnTableName);
            this.groupBoxVirtualColumns.Controls.Add(this.label61);
            this.groupBoxVirtualColumns.Controls.Add(this.columnMeasure);
            this.groupBoxVirtualColumns.Controls.Add(this.label56);
            this.groupBoxVirtualColumns.Controls.Add(this.label62);
            this.groupBoxVirtualColumns.Controls.Add(this.label58);
            this.groupBoxVirtualColumns.Location = new System.Drawing.Point(8, 32);
            this.groupBoxVirtualColumns.Name = "groupBoxVirtualColumns";
            this.groupBoxVirtualColumns.Size = new System.Drawing.Size(767, 592);
            this.groupBoxVirtualColumns.TabIndex = 93;
            this.groupBoxVirtualColumns.TabStop = false;
            this.groupBoxVirtualColumns.Text = "Column Definition";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(418, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 13);
            this.label3.TabIndex = 98;
            this.label3.Text = "Name to use when measure value is null";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(418, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 13);
            this.label2.TabIndex = 98;
            this.label2.Text = "Name to use when measure is not in any bucket";
            // 
            // NullCategoryName
            // 
            this.NullCategoryName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NullCategoryName.Location = new System.Drawing.Point(182, 293);
            this.NullCategoryName.Name = "NullCategoryName";
            this.NullCategoryName.Size = new System.Drawing.Size(235, 20);
            this.NullCategoryName.TabIndex = 96;
            this.NullCategoryName.TextChanged += new System.EventHandler(this.NullCategoryName_TextChanged);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 18);
            this.label4.TabIndex = 97;
            this.label4.Text = "Buckets";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 293);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 23);
            this.label1.TabIndex = 97;
            this.label1.Text = "NULL Category Name";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 23);
            this.label5.TabIndex = 95;
            this.label5.Text = "Default Category Name";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // bucketGridView
            // 
            this.bucketGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.bucketGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.bucketGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bucketGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Category,
            this.Miin,
            this.Max});
            this.bucketGridView.Location = new System.Drawing.Point(28, 337);
            this.bucketGridView.Name = "bucketGridView";
            this.bucketGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.bucketGridView.Size = new System.Drawing.Size(572, 188);
            this.bucketGridView.TabIndex = 93;
            this.bucketGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.bucketGridView_RowsAdded);
            this.bucketGridView.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.bucketGridView_RowValidated);
            // 
            // Category
            // 
            this.Category.DataPropertyName = "Category";
            this.Category.FillWeight = 200F;
            this.Category.HeaderText = "Category Name";
            this.Category.Name = "Category";
            // 
            // Miin
            // 
            this.Miin.DataPropertyName = "Min";
            this.Miin.HeaderText = "Minimum Value (>=)";
            this.Miin.Name = "Miin";
            // 
            // Max
            // 
            this.Max.DataPropertyName = "Max";
            this.Max.HeaderText = "Maximum Value (<)";
            this.Max.Name = "Max";
            // 
            // virtualColumnFilterBox
            // 
            this.virtualColumnFilterBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.virtualColumnFilterBox.CheckOnClick = true;
            this.virtualColumnFilterBox.Location = new System.Drawing.Point(412, 17);
            this.virtualColumnFilterBox.Name = "virtualColumnFilterBox";
            this.virtualColumnFilterBox.Size = new System.Drawing.Size(352, 107);
            this.virtualColumnFilterBox.TabIndex = 4;
            this.virtualColumnFilterBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.VirtualColumns_MouseMove);
            // 
            // bucketMeasure
            // 
            this.bucketMeasure.Checked = true;
            this.bucketMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bucketMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bucketMeasure.Location = new System.Drawing.Point(9, 240);
            this.bucketMeasure.Name = "bucketMeasure";
            this.bucketMeasure.Size = new System.Drawing.Size(192, 24);
            this.bucketMeasure.TabIndex = 9;
            this.bucketMeasure.TabStop = true;
            this.bucketMeasure.Text = "Bucketed Measure Attribute";
            // 
            // pivotLevel
            // 
            this.pivotLevel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pivotLevel.Location = new System.Drawing.Point(137, 202);
            this.pivotLevel.Name = "pivotLevel";
            this.pivotLevel.Size = new System.Drawing.Size(224, 21);
            this.pivotLevel.TabIndex = 7;
            this.pivotLevel.SelectedIndexChanged += new System.EventHandler(this.pivotLevel_SelectedIndexChanged);
            this.pivotLevel.TextChanged += new System.EventHandler(this.pivotLevel_TextChanged);
            // 
            // pivotPercentBox
            // 
            this.pivotPercentBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pivotPercentBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pivotPercentBox.Location = new System.Drawing.Point(412, 146);
            this.pivotPercentBox.Name = "pivotPercentBox";
            this.pivotPercentBox.Size = new System.Drawing.Size(128, 24);
            this.pivotPercentBox.TabIndex = 8;
            this.pivotPercentBox.Text = "Percent of Total";
            this.pivotPercentBox.CheckedChanged += new System.EventHandler(this.pivotPercentBox_CheckedChanged);
            // 
            // pivotMeasure
            // 
            this.pivotMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pivotMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pivotMeasure.Location = new System.Drawing.Point(9, 146);
            this.pivotMeasure.Name = "pivotMeasure";
            this.pivotMeasure.Size = new System.Drawing.Size(184, 24);
            this.pivotMeasure.TabIndex = 5;
            this.pivotMeasure.Text = "Pivoted Measure";
            // 
            // pivotDimension
            // 
            this.pivotDimension.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pivotDimension.Location = new System.Drawing.Point(137, 178);
            this.pivotDimension.Name = "pivotDimension";
            this.pivotDimension.Size = new System.Drawing.Size(224, 21);
            this.pivotDimension.TabIndex = 6;
            this.pivotDimension.SelectedIndexChanged += new System.EventHandler(this.pivotDimension_SelectedIndexChanged);
            this.pivotDimension.TextChanged += new System.EventHandler(this.pivotDimension_TextChanged);
            // 
            // label55
            // 
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(25, 202);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(120, 23);
            this.label55.TabIndex = 71;
            this.label55.Text = "Pivot Level";
            // 
            // columnTableName
            // 
            this.columnTableName.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.columnTableName.Location = new System.Drawing.Point(126, 22);
            this.columnTableName.Name = "columnTableName";
            this.columnTableName.Size = new System.Drawing.Size(224, 21);
            this.columnTableName.TabIndex = 2;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(25, 178);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(120, 23);
            this.label61.TabIndex = 91;
            this.label61.Text = "Pivot Dimension";
            // 
            // columnMeasure
            // 
            this.columnMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.columnMeasure.Location = new System.Drawing.Point(126, 46);
            this.columnMeasure.Name = "columnMeasure";
            this.columnMeasure.Size = new System.Drawing.Size(224, 21);
            this.columnMeasure.TabIndex = 3;
            // 
            // label56
            // 
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(6, 46);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(112, 23);
            this.label56.TabIndex = 0;
            this.label56.Text = "Base Measure";
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(361, 17);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(56, 23);
            this.label62.TabIndex = 0;
            this.label62.Text = "Filters:";
            // 
            // label58
            // 
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 22);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(112, 23);
            this.label58.TabIndex = 0;
            this.label58.Text = "Table Name";
            // 
            // vcMenuStrip
            // 
            this.vcMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newVirtualColumnToolStripMenuItem,
            this.removeVirtualColumnToolStripMenuItem});
            this.vcMenuStrip.Name = "vcMenuStrip";
            this.vcMenuStrip.ShowImageMargin = false;
            this.vcMenuStrip.Size = new System.Drawing.Size(176, 48);
            // 
            // newVirtualColumnToolStripMenuItem
            // 
            this.newVirtualColumnToolStripMenuItem.Name = "newVirtualColumnToolStripMenuItem";
            this.newVirtualColumnToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.newVirtualColumnToolStripMenuItem.Text = "New Virtual Column";
            this.newVirtualColumnToolStripMenuItem.Click += new System.EventHandler(this.addColumn_Click);
            // 
            // removeVirtualColumnToolStripMenuItem
            // 
            this.removeVirtualColumnToolStripMenuItem.Name = "removeVirtualColumnToolStripMenuItem";
            this.removeVirtualColumnToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.removeVirtualColumnToolStripMenuItem.Text = "Remove Virtual Column";
            this.removeVirtualColumnToolStripMenuItem.Click += new System.EventHandler(this.removeColumn_Click);
            // 
            // VirtualColumns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 674);
            this.Controls.Add(this.tabControl);
            this.Name = "VirtualColumns";
            this.Text = "VirtualColumns";
            this.tabControl.ResumeLayout(false);
            this.virtualColumnsTabPage.ResumeLayout(false);
            this.groupBoxVirtualColumns.ResumeLayout(false);
            this.groupBoxVirtualColumns.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bucketGridView)).EndInit();
            this.vcMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage virtualColumnsTabPage;
        private System.Windows.Forms.ComboBox pivotLevel;
        private System.Windows.Forms.RadioButton bucketMeasure;
        private System.Windows.Forms.RadioButton pivotMeasure;
        private System.Windows.Forms.ComboBox columnTableName;
        private System.Windows.Forms.ComboBox pivotDimension;
        private System.Windows.Forms.ComboBox columnMeasure;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.GroupBox groupBoxVirtualColumns;
        private System.Windows.Forms.CheckedListBox virtualColumnFilterBox;
        private System.Windows.Forms.CheckBox pivotPercentBox;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.ContextMenuStrip vcMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newVirtualColumnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeVirtualColumnToolStripMenuItem;
        private System.Windows.Forms.DataGridView bucketGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Category;
        private System.Windows.Forms.DataGridViewTextBoxColumn Miin;
        private System.Windows.Forms.DataGridViewTextBoxColumn Max;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DefaultCategoryName;
        private System.Windows.Forms.TextBox NullCategoryName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolTip toolTip;
    }
}