using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class TimeProperties : Form
    {
        public TimeProperties(TimeDefinition td)
        {
            InitializeComponent();
            if (td != null)
            {
                dayBox.Checked = td.Day;
                weekBox.Checked = td.Week;
                monthBox.Checked = td.Month;
                quarterBox.Checked = td.Quarter;
                halfyearBox.Checked = td.Halfyear;
                yearBox.Checked = td.Year;
                startDate.Value = td.StartDate;
                endDate.Value = td.EndDate;
                determinantDayBox.Text = td.WeekDeterminantDay;
                firstDayBox.Text = td.FirstDayOfWeek;
                generateTime.Checked = td.GenerateTimeDimension;
                partitionFacts.Checked = td.PartitionFacts;
                partitionStart.Value = td.PartitionStart < partitionStart.MinDate ? partitionStart.MinDate : td.PartitionStart;
                partitionEnd.Value = td.PartitionEnd < partitionEnd.MinDate ? partitionEnd.MinDate : td.PartitionEnd;
                partitionPeriodBox.Text = td.PartitionPeriod != null ? td.PartitionPeriod : "Month";
                periodList.Items.Clear();
                if (td.Periods != null)
                    foreach (TimePeriod tp in td.Periods)
                    {
                        periodList.Items.Add(tp.getItem());
                    }
            }
        }

        public TimeDefinition getTimeDefinition()
        {
            TimeDefinition td = new TimeDefinition();
            td.Day = dayBox.Checked;
            td.Week = weekBox.Checked;
            td.Month = monthBox.Checked;
            td.Quarter = quarterBox.Checked;
            td.Halfyear = halfyearBox.Checked;
            td.Year = yearBox.Checked;
            td.StartDate = startDate.Value;
            td.EndDate = endDate.Value;
            td.WeekDeterminantDay = determinantDayBox.Text;
            td.FirstDayOfWeek = firstDayBox.Text;
            td.GenerateTimeDimension = generateTime.Checked;
            td.PartitionFacts = partitionFacts.Checked;
            td.PartitionStart = partitionStart.Value;
            td.PartitionEnd = partitionEnd.Value;
            td.PartitionPeriod = partitionPeriodBox.Text;
            List<TimePeriod> tplist = new List<TimePeriod>();
            foreach (ListViewItem lvi in periodList.Items)
                tplist.Add((TimePeriod)lvi.Tag);
            td.Periods = tplist.ToArray();
            return (td);
        }

        private void addAnalysis_Click(object sender, EventArgs e)
        {
            if (prefixBox.Text == null || prefixBox.Text == "")
            {
                MessageBox.Show("Please select aggregation rule and/or period shift to add time analysis entry.", "Error");
                return;
            }
            TimePeriod tp = new TimePeriod();
            tp.AggregationType = noAgg.Checked ? TimePeriod.AGG_TYPE_NONE :
                (trailingAgg.Checked ? TimePeriod.AGG_TYPE_TRAILING : TimePeriod.AGG_TYPE_TO_DATE);
            try
            {
                tp.AggregationNumPeriods = Int32.Parse(trailingX.Text);
            }
            catch (Exception)
            {
                tp.AggregationNumPeriods = 1;
            }
            if (tp.AggregationType == TimePeriod.AGG_TYPE_TRAILING)
                tp.AggregationPeriod = tp.getPeriod(trailingPeriod.Text);
            else if (tp.AggregationType == TimePeriod.AGG_TYPE_TO_DATE)
                tp.AggregationPeriod = tp.getPeriod(toDatePeriod.Text);
            try
            {
                tp.ShiftAmount = Int32.Parse(shiftAmount.Text);
                tp.ShiftPeriod = tp.getPeriod(shiftPeriod.Text);
                tp.ShiftAgo = shiftDir.Text == "Ago";
            }
            catch (Exception)
            {
                tp.ShiftAmount = 0;
            }
            tp.Prefix = prefixBox.Text;
            periodList.Items.Add(tp.getItem());
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (periodList.SelectedItems.Count > 0)
            {
                foreach (int index in periodList.SelectedIndices)
                    periodList.Items.RemoveAt(index);
            }
        }

        private void updatePrefix()
        {
            string prefix = "";
            if (shiftAmount.Text != "" && shiftAmount.Text != "0")
            {
                if (shiftAmount.Text != "1")
                    prefix += shiftAmount.Text;
                if (shiftDir.Text == "Ago")
                {
                    if (shiftPeriod.Text == "Week")
                        prefix += "WAGO ";
                    else if (shiftPeriod.Text == "Month")
                        prefix += "MAGO ";
                    else if (shiftPeriod.Text == "Quarter")
                        prefix += "QAGO ";
                    else if (shiftPeriod.Text == "Half-year")
                        prefix += "HYAGO ";
                    else if (shiftPeriod.Text == "Year")
                        prefix += "YAGO ";
                }
                else
                {
                    if (shiftPeriod.Text == "Week")
                        prefix += "NW ";
                    else if (shiftPeriod.Text == "Month")
                        prefix += "NM ";
                    else if (shiftPeriod.Text == "Quarter")
                        prefix += "NQ ";
                    else if (shiftPeriod.Text == "Half-year")
                        prefix += "NHY ";
                    else if (shiftPeriod.Text == "Year")
                        prefix += "NY ";
                }
            }
            if (trailingAgg.Checked)
            {
                prefix += "T";
                if (trailingX.Text == "12")
                    prefix += "T";
                else prefix += trailingX.Text;
                if (trailingPeriod.Text == "Months")
                    prefix += "M";
                else if (trailingPeriod.Text == "Days")
                    prefix += "D";
                else if (trailingPeriod.Text == "Weeks")
                    prefix += "W";
                else if (trailingPeriod.Text == "Quarters")
                    prefix += "Q";
                else if (trailingPeriod.Text == "Half-years")
                    prefix += "HY";
                else if (trailingPeriod.Text == "Years")
                    prefix += "Y";
            }
            else if (toDateAgg.Checked)
            {
                if (toDatePeriod.Text == "Month")
                    prefix += "MTD";
                else if (toDatePeriod.Text == "Week")
                    prefix += "WTD";
                else if (toDatePeriod.Text == "Quarter")
                    prefix += "QTD";
                else if (toDatePeriod.Text == "Half-year")
                    prefix += "HYTD";
                else if (toDatePeriod.Text == "Year")
                    prefix += "YTD";
            }
            prefixBox.Text = prefix.Trim();
        }

        private void trailingX_TextChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void trailingPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void toDatePeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void shiftAmount_TextChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void shiftPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void shiftDir_SelectedIndexChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void noAgg_CheckedChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void trailingAgg_CheckedChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }

        private void toDateAgg_CheckedChanged(object sender, EventArgs e)
        {
            updatePrefix();
        }
    }
}