﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class PostBuildReminderDialog : Form
    {
        public PostBuildReminderDialog(string postBuildRem)
        {
            InitializeComponent();
            postBuildReminderTextBox.Text = postBuildRem;
            postBuildReminderTextBox.SelectionStart = 0;
            postBuildReminderTextBox.SelectionLength = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
