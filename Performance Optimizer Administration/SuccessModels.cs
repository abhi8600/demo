using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace Performance_Optimizer_Administration
{
    public partial class SuccessModels : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Model Datasets";
        string categoryName = "Business Modeling";
        public List<SuccessModel> modelList;
        TreeNode rootNode;

        public SuccessModels(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (modelList != null)
            {
                r.SuccessModels = (SuccessModel[])(modelList.ToArray());
            }
        }

        public void updateFromRepository(Repository r)
        {
            //Modeled Relationships
            selIndex = -1;
            selNode = null;
            
            availPatternMeasures.Items.Clear();
            if (r != null && r.SuccessModels != null)
            {
                modelList = new List<SuccessModel>(r.SuccessModels);
                foreach (SuccessModel sm in modelList)
                {
                    if (sm.PartitionAttribute == null) sm.PartitionAttribute = "";
                }
            }
            else
            {
                modelList = new List<SuccessModel>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (modelList != null)
                foreach (SuccessModel sm in modelList)
                {
                    TreeNode tn = new TreeNode(sm.Name);
                    tn.ImageIndex = 17;
                    tn.SelectedImageIndex = 17;
                    rootNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;
        int selIndex = -1;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();

            if (modelList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = successModelMenu;
                removeSuccessModelToolStripMenuItem.Visible = false;
                return false;
            }
            selIndex = rootNode.Nodes.IndexOf(node);
            selNode = node;
            if (selIndex >= 0)
                indexChanged(selIndex);
            ma.mainTree.ContextMenuStrip = successModelMenu;
            removeSuccessModelToolStripMenuItem.Visible = true;
            return true;
        }

        public void setup()
        {
            targetMeasureBox.BeginUpdate();
            availPatternAttribute1.BeginUpdate();
            availPatternAttribute2.BeginUpdate();
            inheritSuccessModel.BeginUpdate();
            inheritSuccessModel.Items.Clear();
            availPatternAttribute1.Items.Clear();
            availPatternAttribute2.Items.Clear();
            foreach (SuccessModel sm in modelList)
            {
                if (sm.Name != rootNode.Nodes[selIndex].Text)
                {
                    inheritSuccessModel.Items.Add(sm.Name);
                }
            }
            if (availPatternMeasures.Items.Count == 0 || ma.curMeasureList == null)
            {
                availPatternMeasures.BeginUpdate();
                availPatternMeasures.Items.Clear();
                List<string> mlist = ma.getMeasureList(null)[0];
                foreach (string s in mlist)
                {
                    if (!availPatternMeasures.Items.Contains(s))
                        availPatternMeasures.Items.Add(s);
                    if (!targetMeasureBox.Items.Contains(s))
                        targetMeasureBox.Items.Add(s);
                }
                if (mlist.Count > 0)
                    availPatternMeasures.Sorted = true;
                availPatternMeasures.EndUpdate();
            }

            List<string> dlist = ma.getDimensionList();
            targetDimBox.Items.Clear();
            targetLevelBox.Items.Clear();
            availAttributeListBox.Items.Clear();
            iterateDimBox.Items.Clear();
            iterateDimBox.Items.Add("None");
            foreach (string d in dlist)
            {
                List<string> clist = ma.getDimensionColumnList(d);
                targetDimBox.Items.Add(d);
                iterateDimBox.Items.Add(d);
                foreach (string c in clist)
                {
                    string name = d + ":" + c;
                    if (!availPatternAttribute1.Items.Contains(name))
                    {
                        availPatternAttribute1.Items.Add(name);
                        availPatternAttribute2.Items.Add(name);
                    }
                }
            }
            setModelTargetLevels(modelList[selIndex]);
            ArrayList checkedFilters = new ArrayList();
            foreach (string s in modelFilterBox.CheckedItems)
            {
                checkedFilters.Add(s);
            }
            QueryFilter.sortedListBox(modelFilterBox, ma.filterMod.filterList, (string[])checkedFilters.ToArray(typeof(string)));
           
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
            inheritSuccessModel.EndUpdate();
            targetMeasureBox.EndUpdate();
            availPatternAttribute1.EndUpdate();
            availPatternAttribute2.EndUpdate();
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return modelTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (SuccessModel sm in modelList)
            {
                if (match)
                {
                    for (int i = 0; i < sm.Attributes.Length; i++)
                        if (sm.Attributes[i] == find) sm.Attributes[i] = replace;
                    if (sm.BreakAttribute != null && sm.BreakAttribute == find) sm.BreakAttribute = replace;
                    if (sm.PartitionAttribute != null && sm.PartitionAttribute == find) sm.PartitionAttribute = replace;
                    if (sm.IterateLevel != null && sm.IterateLevel == find) sm.IterateLevel = replace;
                    if (sm.Measure != null && sm.Measure == find) sm.Measure = replace;
                    if (sm.TargetLevel != null && sm.TargetLevel == find) sm.TargetLevel = replace;
                    if (sm.Patterns != null)
                    {
                        foreach (Pattern p in sm.Patterns)
                        {
                            if (p.Attribute1 != null && p.Attribute1 == find) p.Attribute1 = replace;
                            if (p.Attribute2 != null && p.Attribute2 == find) p.Attribute2 = replace;
                            if (p.Measure != null && p.Measure == find) p.Measure = replace;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < sm.Attributes.Length; i++)
                        sm.Attributes[i] = sm.Attributes[i].Replace(find, replace);
                    if (sm.BreakAttribute != null)
                        sm.BreakAttribute = sm.BreakAttribute.Replace(find, replace);
                    if (sm.PartitionAttribute != null)
                        sm.PartitionAttribute = sm.PartitionAttribute.Replace(find, replace);
                    if (sm.IterateLevel != null)
                        sm.IterateLevel = sm.IterateLevel.Replace(find, replace);
                    if (sm.Measure != null)
                        sm.Measure = sm.Measure.Replace(find, replace);
                    if (sm.Patterns != null)
                    foreach (Pattern p in sm.Patterns)
                    {
                        if (p.Attribute1 != null)
                            p.Attribute1 = p.Attribute1.Replace(find, replace);
                        if (p.Attribute2 != null)
                            p.Attribute2 = p.Attribute2.Replace(find, replace);
                        if (p.Measure != null)
                            p.Measure = p.Measure.Replace(find, replace);
                    }
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (SuccessModel sm in modelList)
            {
                if (match)
                {
                    for (int i = 0; i < sm.Attributes.Length; i++)
                        if (sm.Attributes[i] == find) results.Add("Success Model: " + sm.Name + " - Attribute");
                    if (sm.BreakAttribute != null && sm.BreakAttribute == find) results.Add("Success Model: " + sm.Name + " - Break attribute");
                    if (sm.PartitionAttribute != null && sm.PartitionAttribute == find) results.Add("Success Model: " + sm.Name + " - Partition attribute");
                    if (sm.IterateLevel != null && sm.IterateLevel == find) results.Add("Success Model: " + sm.Name + " - Iterate level");
                    if (sm.Measure != null && sm.Measure == find) results.Add("Success Model: " + sm.Name + " - Measure");
                    if (sm.TargetLevel != null && sm.TargetLevel == find) results.Add("Success Model: " + sm.Name + " - Target level");
                    if (sm.Patterns != null)
                    foreach (Pattern p in sm.Patterns)
                    {
                        if (p.Attribute1 != null && p.Attribute1 == find) results.Add("Success Model: " + sm.Name + " - Pattern attribute 1");
                        if (p.Attribute2 != null && p.Attribute2 == find) results.Add("Success Model: " + sm.Name + " - Pattern attribute 2");
                        if (p.Measure != null && p.Measure == find) results.Add("Success Model: " + sm.Name + " - Pattern measure");
                    }
                }
                else
                {
                    for (int i = 0; i < sm.Attributes.Length; i++)
                        if (sm.Attributes[i].IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Attribute");
                    if (sm.BreakAttribute != null && sm.BreakAttribute.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Break attribute");
                    if (sm.PartitionAttribute != null && sm.PartitionAttribute.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Partition attribute");
                    if (sm.IterateLevel != null && sm.IterateLevel.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Iterate level");
                    if (sm.Measure != null && sm.Measure.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Measure");
                    if (sm.TargetLevel != null && sm.TargetLevel.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Target level");
                    if (sm.Patterns != null)
                    foreach (Pattern p in sm.Patterns)
                    {
                        if (p.Attribute1 != null && p.Attribute1.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Pattern attribute 1");
                        if (p.Attribute2 != null && p.Attribute2.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Pattern attribute 2");
                        if (p.Measure != null && p.Measure.IndexOf(find) >= 0) results.Add("Success Model: " + sm.Name + " - Pattern measure");
                    }
                }
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateModel();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        public void setModelTargetLevels(SuccessModel sm)
        {
            List<string> clist = ma.getDimensionColumnList(targetDimBox.Text);
            targetLevelBox.Items.Clear();

            if (sm != null && sm.Attributes != null)
            {
                for (int i = 0; i < sm.Attributes.Length; i++)
                {
                    if (!clist.Contains(sm.Attributes[i]))
                    {
                        clist.Add(sm.Attributes[i]);
                    }
                }
            }

            Util.sortedListBox(availAttributeListBox, clist, sm.Attributes);
            
            foreach (string c in clist)
            {
                if (!targetLevelBox.Items.Contains(c))
                    targetLevelBox.Items.Add(c);
                if (!breakAttribute.Items.Contains(c))
                    breakAttribute.Items.Add(c);
                if (!partitionAttributeBox.Items.Contains(c))
                    partitionAttributeBox.Items.Add(c);
            }
        }
        private void selectPerformanceMeasures_Click(object sender, System.EventArgs e)
        {
            availPatternMeasures.SelectedIndex = -1;
            foreach (ListViewItem lvi in ma.perf.selectedMeasures.Items)
            {
                string name = lvi.SubItems[0].Text;
                for (int i = 0; i < availPatternMeasures.Items.Count; i++)
                {
                    if (availPatternMeasures.Items[i].ToString() == name)
                    {
                        availPatternMeasures.SetSelected(i, true);
                        break;
                    }
                }
            }
        }

        private SuccessModel getSuccessModel()
        {
            SuccessModel sm = new SuccessModel();
            if (selIndex < 0 || rootNode.Nodes.Count == 0)
                sm.Name = "New Model Dataset";
            else
                sm.Name = rootNode.Nodes[selIndex].Text;
            sm.Measure = targetMeasureBox.Text;
            sm.TargetDimension = targetDimBox.Text;
            sm.TargetLevel = targetLevelBox.Text;
            sm.IterateDimension = iterateDimBox.Text;
            sm.IterateLevel = iterateLevelBox.Text;
            sm.BreakModel = oneModelButton.Checked ? 0 :
                (clusterBreakButton.Checked ? 1 : 2);
            sm.BreakAttribute = breakAttribute.Text;
            sm.PartitionAttribute = partitionAttributeBox.Text != null ? partitionAttributeBox.Text : "";
            sm.Attributes = new string[availAttributeListBox.CheckedItems.Count];
            sm.InheritFromModel = inheritSuccessModel.Text;
            sm.GroupMeasureOnly = groupMeasureOnly.Checked;
            sm.Runnable = runnableCheckBox.Checked;
            sm.CreateAggregates = aggregateCheckBox.Checked;
            for (int i = 0; i < availAttributeListBox.CheckedItems.Count; i++)
            {
                sm.Attributes[i] = (string)availAttributeListBox.CheckedItems[i];
            }
            sm.Patterns = new Pattern[selectedPatterns.Items.Count];
            for (int i = 0; i < selectedPatterns.Items.Count; i++)
            {
                ListViewItem lvi = selectedPatterns.Items[i];
                sm.Patterns[i] = new Pattern();
                sm.Patterns[i].Measure = (string)lvi.SubItems[0].Text;
                sm.Patterns[i].Type = Pattern.MEASUREONLY;
                if ((lvi.SubItems.Count > 1) && (lvi.SubItems[1].Text.Length > 0))
                {
                    sm.Patterns[i].Type = Pattern.ONEATTRIBUTE;
                    sm.Patterns[i].Dimension1 = (string)lvi.SubItems[1].Text;
                    sm.Patterns[i].Attribute1 = (string)lvi.SubItems[2].Text;
                }
                if ((lvi.SubItems.Count > 3) && (lvi.SubItems[3].Text.Length > 0))
                {
                    sm.Patterns[i].Type = Pattern.TWOATTRIBUTE;
                    sm.Patterns[i].Dimension2 = (string)lvi.SubItems[3].Text;
                    sm.Patterns[i].Attribute2 = (string)lvi.SubItems[4].Text;
                }
                sm.Patterns[i].Proportion =
                    Boolean.Parse(lvi.SubItems[5].Text);
                sm.Patterns[i].Actionable =
                    Boolean.Parse(lvi.SubItems[6].Text);
                sm.Patterns[i].Model =
                    Boolean.Parse(lvi.SubItems[7].Text);
                try
                {
                    sm.Patterns[i].MaxPatterns =
                        Int32.Parse(lvi.SubItems[8].Text);
                }
                catch (Exception)
                {
                    sm.Patterns[i].MaxPatterns = 10;
                }
            }
            ArrayList mflist = new ArrayList();
            foreach (string s in modelFilterBox.CheckedItems) mflist.Add(s);
            sm.Filters = (string[])(mflist).ToArray(typeof(string));
            sm.FilterNullCategories = filterNullValuesBox.Checked;
            return (sm);
        }

        private void indexChanged(int index)
        {
            selectedPatterns.BeginUpdate();
            modelFilterBox.BeginUpdate();
            availAttributeListBox.BeginUpdate();
            inheritSuccessModel.BeginUpdate();
            inheritSuccessModel.Items.Clear();
            foreach (SuccessModel ism in modelList)
            {
                if (ism.Name != rootNode.Nodes[selIndex].Text)
                {
                    inheritSuccessModel.Items.Add(ism.Name);
                }
            }
            SuccessModel sm = modelList[index];
            targetMeasureBox.Text = sm.Measure;
            targetDimBox.Text = sm.TargetDimension;
            targetLevelBox.Text = sm.TargetLevel;
            iterateDimBox.Text = sm.IterateDimension;
            iterateLevelBox.Text = sm.IterateLevel;
            inheritSuccessModel.Text = sm.InheritFromModel;
            groupMeasureOnly.Checked = sm.GroupMeasureOnly;
            runnableCheckBox.Checked = sm.Runnable;
            aggregateCheckBox.Checked = sm.CreateAggregates;
            switch (sm.BreakModel)
            {
                case 0:
                    oneModelButton.Checked = true;
                    break;
                case 1:
                    clusterBreakButton.Checked = true;
                    break;
                case 2:
                    attributeBreakButton.Checked = true;
                    break;
            }
            breakAttribute.Text = sm.BreakAttribute;
            partitionAttributeBox.Text = sm.PartitionAttribute != null ? sm.PartitionAttribute : "";
            selectedPatterns.Items.Clear();
            if (sm.Patterns != null)
            {
                foreach (Pattern p in sm.Patterns)
                {
                    ListViewItem lvi = new ListViewItem(new string[] { p.Measure, p.Dimension1, p.Attribute1, p.Dimension2, p.Attribute2, p.Proportion.ToString(), p.Actionable.ToString(), p.Model.ToString(), p.MaxPatterns.ToString() });
                    selectedPatterns.Items.Add(lvi);
                }
            }
            QueryFilter.sortedListBox(modelFilterBox, ma.filterMod.filterList, sm.Filters);
            
            filterNullValuesBox.Checked = sm.FilterNullCategories;
            setModelTargetLevels(sm);
            setIterateLevels();
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
            inheritSuccessModel.EndUpdate();
            modelFilterBox.EndUpdate();
            availAttributeListBox.EndUpdate();
            selectedPatterns.EndUpdate();
        }

        private void addModel_Click(object sender, System.EventArgs e)
        {
            SuccessModel sm = new SuccessModel();
            sm.Name = "New Model Dataset";
            if (modelList == null)
            {
                modelList = new List<SuccessModel>();
            }
            modelList.Add(sm);
            TreeNode tn = new TreeNode(sm.Name);
            tn.ImageIndex = 17;
            tn.SelectedImageIndex = 17;
            rootNode.Nodes.Add(tn);
        }

        private void updateModel()
        {
            if (selNode == null || selIndex < 0 || selIndex >= modelList.Count || selNode.Parent != rootNode) return;
            SuccessModel sm = getSuccessModel();
            selNode.Text = sm.Name;
            modelList[selIndex] = sm;
        }

        private void removeModel_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            modelList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }


        private void selectFirstAttributes_Click(object sender, System.EventArgs e)
        {
            availPatternAttribute1.SelectedIndex = -1;
            foreach (ListViewItem lvi in selectedPatterns.Items)
            {
                if (lvi.SubItems[3].Text.Length == 0)
                {
                    string dim = lvi.SubItems[1].Text;
                    string col = lvi.SubItems[2].Text;
                    for (int i = 0; i < availPatternAttribute1.Items.Count; i++)
                    {
                        if (availPatternAttribute1.Items[i].ToString() == dim + ":" + col)
                        {
                            availPatternAttribute1.SetSelected(i, true);
                            break;
                        }
                    }
                }
            }
        }

        private void selectSecondAttributes_Click(object sender, System.EventArgs e)
        {
            availPatternAttribute2.SelectedIndex = -1;
            foreach (ListViewItem lvi in selectedPatterns.Items)
            {
                if (lvi.SubItems[3].Text.Length == 0)
                {
                    string dim = lvi.SubItems[1].Text;
                    string col = lvi.SubItems[2].Text;
                    for (int i = 0; i < availPatternAttribute2.Items.Count; i++)
                    {
                        if (availPatternAttribute2.Items[i].ToString() == dim + ":" + col)
                        {
                            availPatternAttribute2.SetSelected(i, true);
                            break;
                        }
                    }
                }
            }
        }

        private void addMeasureOnlyButton_Click(object sender, System.EventArgs e)
        {
            foreach (string m in availPatternMeasures.SelectedItems)
            {
                bool found = false;
                foreach (ListViewItem lvi in selectedPatterns.Items)
                {
                    if ((lvi.SubItems[0].Text == m) && (lvi.SubItems[1].Text.Length == 0) && (lvi.SubItems[3].Text.Length == 0))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ListViewItem lvi = new ListViewItem(new string[] { m, null, null, null, null, "False", "False", "True", "1" });
                    selectedPatterns.Items.Add(lvi);
                }
            }
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
        }

        private void addAttribButton_Click(object sender, System.EventArgs e)
        {
            selectedPatterns.BeginUpdate();
            foreach (string m in availPatternMeasures.SelectedItems)
            {
                foreach (string a1 in availPatternAttribute1.SelectedItems)
                {
                    int p = a1.IndexOf(':');
                    string d = a1.Substring(0, p);
                    string c = a1.Substring(p + 1);
                    bool found = false;
                    foreach (ListViewItem lvi in selectedPatterns.Items)
                    {
                        if ((lvi.SubItems[0].Text == m) && (lvi.SubItems[1].Text == d) && (lvi.SubItems[2].Text == c) && (lvi.SubItems[3].Text.Length == 0))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        ListViewItem lvi = new ListViewItem(new string[] { m, d, c, null, null, "False", "True", "True", "10" });
                        selectedPatterns.Items.Add(lvi);
                    }
                }
            }
            selectedPatterns.EndUpdate();
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
        }

        private void add2AttribButton_Click(object sender, System.EventArgs e)
        {
            foreach (string m in availPatternMeasures.SelectedItems)
            {
                foreach (string a1 in availPatternAttribute1.SelectedItems)
                {
                    foreach (string a2 in availPatternAttribute2.SelectedItems)
                    {
                        int p = a1.IndexOf(':');
                        string d1 = a1.Substring(0, p);
                        string c1 = a1.Substring(p + 1);
                        p = a2.IndexOf(':');
                        string d2 = a2.Substring(0, p);
                        string c2 = a2.Substring(p + 1);
                        bool found = false;
                        foreach (ListViewItem lvi in selectedPatterns.Items)
                        {
                            if ((lvi.SubItems[0].Text == m) && (lvi.SubItems[1].Text == d1) && (lvi.SubItems[2].Text == c1) && (lvi.SubItems[3].Text == d2) && (lvi.SubItems[4].Text == c2))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            ListViewItem lvi = new ListViewItem(new string[] { m, d1, c1, d2, c2, "False", "True", "True", "15" });
                            selectedPatterns.Items.Add(lvi);
                        }
                    }
                }
            }
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
        }

        private void setPatternFlags_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem lvi in selectedPatterns.SelectedItems)
            {
                lvi.SubItems[5].Text = proportionFlag.Checked.ToString();
                lvi.SubItems[6].Text = actionableFlag.Checked.ToString();
                lvi.SubItems[7].Text = modelFlag.Checked.ToString();
                lvi.SubItems[8].Text = maxPatterns.Text;
            }
        }

        private void removeAttribButton_Click(object sender, System.EventArgs e)
        {
            while (selectedPatterns.SelectedItems.Count > 0)
            {
                selectedPatterns.Items.Remove(selectedPatterns.SelectedItems[0]);
            }
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
        }

        private void filterValidPatterns_Click(object sender, System.EventArgs e)
        {
            // Make sure each joins with the appropriate dimensions and the target dimension and iteration dimension
            for (int i = 0; i < selectedPatterns.Items.Count; i++)
            {
                ListViewItem lvi = selectedPatterns.Items[i];
                string measure = lvi.SubItems[0].Text;
                List<String> list = null;
                bool removed = false;
                // See if it's a derived measure
                if (!ma.checkDerived(measure))
                {
                    // Check it joins with the target dimension
                    if (targetDimBox.Text.Length > 0)
                    {
                        string dimension = targetDimBox.Text;
                        string attribute = targetLevelBox.Text;
                        list = ma.checkJoin(measure, dimension, attribute);
                        if ((list != null) && (list.Count == 0))
                        {
                            selectedPatterns.Items.RemoveAt(i--);
                            removed = true;
                        }
                    }
                    // Check it joins with the iteration dimension
                    if (!removed && (iterateDimBox.Text.Length > 0) && (iterateDimBox.Text != "None"))
                    {
                        string dimension = iterateDimBox.Text;
                        string attribute = iterateLevelBox.Text;
                        list = ma.checkJoin(measure, dimension, attribute);
                        if ((list != null) && (list.Count == 0))
                        {
                            selectedPatterns.Items.RemoveAt(i--);
                            removed = true;
                        }
                    }
                    // Check to see if it joins to the first attribute
                    if (!removed && (lvi.SubItems.Count > 1) && (lvi.SubItems[1].Text.Length > 0))
                    {
                        string dimension = lvi.SubItems[1].Text;
                        string attribute = lvi.SubItems[2].Text;
                        list = ma.checkJoin(measure, dimension, attribute);
                        if ((list != null) && (list.Count == 0))
                        {
                            selectedPatterns.Items.RemoveAt(i--);
                            removed = true;
                        }
                    }
                    // Check to see if it joins to the second attribute
                    if (!removed && (lvi.SubItems.Count > 3) && (lvi.SubItems[3].Text.Length > 0) && (list.Count > 0))
                    {
                        string dimension = lvi.SubItems[3].Text;
                        string attribute = lvi.SubItems[4].Text;
                        List<String> list2 = ma.checkJoin(measure, dimension, attribute);
                        if ((list2 != null) && (list2.Count == 0))
                        {
                            selectedPatterns.Items.RemoveAt(i--);
                        }
                        else if (list != null)
                        {
                            bool found = false;
                            foreach (string s in list2)
                            {
                                if (list.Contains(s))
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                                selectedPatterns.Items.RemoveAt(i--);
                        }
                    }
                }
            }
            totalPatternLabel.Text = "Total Count: " + selectedPatterns.Items.Count;
        }
        private void iterateDimBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            setIterateLevels();
        }

        private void setIterateLevels()
        {
            List<string> clist = ma.getDimensionColumnList(iterateDimBox.Text);
            iterateLevelBox.Items.Clear();
            foreach (string c in clist)
            {
                iterateLevelBox.Items.Add(c);
            }
        }
        private void selectedPatterns_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            selectedPatterns.ListViewItemSorter = new MainAdminForm.Listcomparer(e.Column);
            selectedPatterns.Sort();
        }
        private void exportModelButton_Click(object sender, System.EventArgs e)
        {
            if (ma.exportSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(ma.exportSaveFileDialog.FileName, false);
                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to Open File");
                    return;
                }
                SuccessModel sm = modelList[selIndex];
                writer.WriteLine("Success Model: " + sm.Name);
                writer.WriteLine("Target Measure: " + sm.Measure);
                writer.WriteLine("Target Dimension/Level: " + sm.TargetDimension + "/" + sm.TargetLevel);
                writer.WriteLine("Attributes:");
                foreach (string s in sm.Attributes)
                {
                    writer.WriteLine("\t" + s);
                }
                writer.WriteLine("Pattern Breakouts:");
                if (sm.Patterns != null)
                foreach (Pattern p in sm.Patterns)
                {
                    if (p.Type == 0)
                    {
                        writer.WriteLine(p.Measure + "\t" + p.Proportion + "\t" + p.MaxPatterns);
                    }
                    else if (p.Type == 1)
                    {
                        writer.WriteLine(p.Measure + "\t" + p.Dimension1 + "." + p.Attribute1 + "\t" + p.Proportion + "\t" + p.MaxPatterns);
                    }
                    else if (p.Type == 2)
                    {
                        writer.WriteLine(p.Measure + "\t" + p.Dimension1 + "." + p.Attribute1 + "\t" + p.Dimension2 + "." + p.Attribute2 + "\t" + p.Proportion + "\t" + p.MaxPatterns);
                    }
                }
                writer.Close();
            }
        }

        private void targetDimBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setModelTargetLevels(modelList[selIndex]);
        }

        private void modelTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                modelList[e.Node.Index].Name = e.Label;
                e.Node.Text = e.Label;
            }
        }

        #endregion

        private void selectedPatterns_ColumnClick_1(object sender, ColumnClickEventArgs e)
        {
            selectedPatterns.ListViewItemSorter = new MainAdminForm.Listcomparer(e.Column);
            selectedPatterns.Sort();
        }

        private void SuccessModels_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);            
        }
    }
}