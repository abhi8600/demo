namespace Performance_Optimizer_Administration
{
    partial class ReferencePopulations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.referenceTabPage = new System.Windows.Forms.TabPage();
            this.levelTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.targetDimension = new System.Windows.Forms.ComboBox();
            this.successPatternTableTextBox = new System.Windows.Forms.TextBox();
            this.successPatternTableLabel = new System.Windows.Forms.Label();
            this.performanceTablelabel = new System.Windows.Forms.Label();
            this.performanceTableTextBox = new System.Windows.Forms.TextBox();
            this.filtersGroupBox = new System.Windows.Forms.GroupBox();
            this.referenceFilterListBox = new System.Windows.Forms.CheckedListBox();
            this.label83 = new System.Windows.Forms.Label();
            this.referenceDimensionLabel = new System.Windows.Forms.Label();
            this.peeringAttributesGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.peerAvailableColumns = new System.Windows.Forms.ListView();
            this.availColNameHeader = new System.Windows.Forms.ColumnHeader();
            this.availTypeHeader = new System.Windows.Forms.ColumnHeader();
            this.peerWeight = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.perfPeerAdd = new System.Windows.Forms.Button();
            this.patternPeerAdd = new System.Windows.Forms.Button();
            this.performancePeeringGroupBox = new System.Windows.Forms.GroupBox();
            this.perfPeerAttributes = new System.Windows.Forms.ListView();
            this.attribColumnHeader = new System.Windows.Forms.ColumnHeader();
            this.typeHeader = new System.Windows.Forms.ColumnHeader();
            this.weightHeader = new System.Windows.Forms.ColumnHeader();
            this.perfPeerRemove = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.numPerfPeers = new System.Windows.Forms.TextBox();
            this.successPatternPeeringGroupBox = new System.Windows.Forms.GroupBox();
            this.successMeasure = new System.Windows.Forms.ComboBox();
            this.patternPeerAttributes = new System.Windows.Forms.ListView();
            this.scolNameHeader = new System.Windows.Forms.ColumnHeader();
            this.scolTypeHeader = new System.Windows.Forms.ColumnHeader();
            this.scolWeightHeader = new System.Windows.Forms.ColumnHeader();
            this.patternPeerRemove = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numSuccessPeers = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.refPopulationsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.referenceTabPage.SuspendLayout();
            this.filtersGroupBox.SuspendLayout();
            this.peeringAttributesGroupBox.SuspendLayout();
            this.performancePeeringGroupBox.SuspendLayout();
            this.successPatternPeeringGroupBox.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.refPopulationsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // referenceTabPage
            // 
            this.referenceTabPage.AutoScroll = true;
            this.referenceTabPage.Controls.Add(this.levelTextBox);
            this.referenceTabPage.Controls.Add(this.label2);
            this.referenceTabPage.Controls.Add(this.targetDimension);
            this.referenceTabPage.Controls.Add(this.successPatternTableTextBox);
            this.referenceTabPage.Controls.Add(this.successPatternTableLabel);
            this.referenceTabPage.Controls.Add(this.performanceTablelabel);
            this.referenceTabPage.Controls.Add(this.performanceTableTextBox);
            this.referenceTabPage.Controls.Add(this.filtersGroupBox);
            this.referenceTabPage.Controls.Add(this.referenceDimensionLabel);
            this.referenceTabPage.Controls.Add(this.peeringAttributesGroupBox);
            this.referenceTabPage.Controls.Add(this.performancePeeringGroupBox);
            this.referenceTabPage.Controls.Add(this.successPatternPeeringGroupBox);
            this.referenceTabPage.Location = new System.Drawing.Point(4, 22);
            this.referenceTabPage.Name = "referenceTabPage";
            this.referenceTabPage.Size = new System.Drawing.Size(844, 707);
            this.referenceTabPage.TabIndex = 8;
            this.referenceTabPage.Text = "Reference Populations";
            this.referenceTabPage.UseVisualStyleBackColor = true;
            // 
            // levelTextBox
            // 
            this.levelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.levelTextBox.Location = new System.Drawing.Point(578, 8);
            this.levelTextBox.Name = "levelTextBox";
            this.levelTextBox.Size = new System.Drawing.Size(196, 20);
            this.levelTextBox.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(434, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 23);
            this.label2.TabIndex = 55;
            this.label2.Text = "Dimension Level:";
            // 
            // targetDimension
            // 
            this.targetDimension.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetDimension.Location = new System.Drawing.Point(214, 10);
            this.targetDimension.Name = "targetDimension";
            this.targetDimension.Size = new System.Drawing.Size(208, 21);
            this.targetDimension.TabIndex = 54;
            // 
            // successPatternTableTextBox
            // 
            this.successPatternTableTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.successPatternTableTextBox.Location = new System.Drawing.Point(578, 42);
            this.successPatternTableTextBox.Name = "successPatternTableTextBox";
            this.successPatternTableTextBox.Size = new System.Drawing.Size(196, 20);
            this.successPatternTableTextBox.TabIndex = 52;
            // 
            // successPatternTableLabel
            // 
            this.successPatternTableLabel.AutoSize = true;
            this.successPatternTableLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successPatternTableLabel.Location = new System.Drawing.Point(434, 44);
            this.successPatternTableLabel.Name = "successPatternTableLabel";
            this.successPatternTableLabel.Size = new System.Drawing.Size(144, 16);
            this.successPatternTableLabel.TabIndex = 51;
            this.successPatternTableLabel.Text = "Success Pattern Table";
            // 
            // performanceTablelabel
            // 
            this.performanceTablelabel.AutoSize = true;
            this.performanceTablelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.performanceTablelabel.Location = new System.Drawing.Point(22, 44);
            this.performanceTablelabel.Name = "performanceTablelabel";
            this.performanceTablelabel.Size = new System.Drawing.Size(124, 16);
            this.performanceTablelabel.TabIndex = 50;
            this.performanceTablelabel.Text = "Performance Table";
            // 
            // performanceTableTextBox
            // 
            this.performanceTableTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.performanceTableTextBox.Location = new System.Drawing.Point(152, 42);
            this.performanceTableTextBox.Name = "performanceTableTextBox";
            this.performanceTableTextBox.Size = new System.Drawing.Size(151, 20);
            this.performanceTableTextBox.TabIndex = 49;
            // 
            // filtersGroupBox
            // 
            this.filtersGroupBox.Controls.Add(this.referenceFilterListBox);
            this.filtersGroupBox.Controls.Add(this.label83);
            this.filtersGroupBox.Location = new System.Drawing.Point(417, 361);
            this.filtersGroupBox.Name = "filtersGroupBox";
            this.filtersGroupBox.Size = new System.Drawing.Size(386, 240);
            this.filtersGroupBox.TabIndex = 48;
            this.filtersGroupBox.TabStop = false;
            this.filtersGroupBox.Text = "Filters";
            // 
            // referenceFilterListBox
            // 
            this.referenceFilterListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.referenceFilterListBox.CheckOnClick = true;
            this.referenceFilterListBox.Location = new System.Drawing.Point(16, 16);
            this.referenceFilterListBox.Name = "referenceFilterListBox";
            this.referenceFilterListBox.Size = new System.Drawing.Size(348, 182);
            this.referenceFilterListBox.TabIndex = 43;
            this.referenceFilterListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ReferencePopulations_MouseMove);
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(8, 208);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(344, 16);
            this.label83.TabIndex = 42;
            this.label83.Text = "Note: Comparison filter is usually a period filter";
            // 
            // referenceDimensionLabel
            // 
            this.referenceDimensionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referenceDimensionLabel.Location = new System.Drawing.Point(8, 8);
            this.referenceDimensionLabel.Name = "referenceDimensionLabel";
            this.referenceDimensionLabel.Size = new System.Drawing.Size(200, 23);
            this.referenceDimensionLabel.TabIndex = 4;
            this.referenceDimensionLabel.Text = "Reference Target Dimension:";
            // 
            // peeringAttributesGroupBox
            // 
            this.peeringAttributesGroupBox.Controls.Add(this.label1);
            this.peeringAttributesGroupBox.Controls.Add(this.peerAvailableColumns);
            this.peeringAttributesGroupBox.Controls.Add(this.peerWeight);
            this.peeringAttributesGroupBox.Controls.Add(this.label40);
            this.peeringAttributesGroupBox.Controls.Add(this.perfPeerAdd);
            this.peeringAttributesGroupBox.Controls.Add(this.patternPeerAdd);
            this.peeringAttributesGroupBox.Location = new System.Drawing.Point(8, 361);
            this.peeringAttributesGroupBox.Name = "peeringAttributesGroupBox";
            this.peeringAttributesGroupBox.Size = new System.Drawing.Size(396, 240);
            this.peeringAttributesGroupBox.TabIndex = 45;
            this.peeringAttributesGroupBox.TabStop = false;
            this.peeringAttributesGroupBox.Text = "Add/Remove Peering Attributes";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 23);
            this.label1.TabIndex = 48;
            this.label1.Text = "Available Columns";
            // 
            // peerAvailableColumns
            // 
            this.peerAvailableColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.peerAvailableColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.availColNameHeader,
            this.availTypeHeader});
            this.peerAvailableColumns.FullRowSelect = true;
            this.peerAvailableColumns.GridLines = true;
            this.peerAvailableColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.peerAvailableColumns.HideSelection = false;
            this.peerAvailableColumns.Location = new System.Drawing.Point(16, 40);
            this.peerAvailableColumns.MultiSelect = false;
            this.peerAvailableColumns.Name = "peerAvailableColumns";
            this.peerAvailableColumns.ShowItemToolTips = true;
            this.peerAvailableColumns.Size = new System.Drawing.Size(252, 184);
            this.peerAvailableColumns.TabIndex = 47;
            this.peerAvailableColumns.UseCompatibleStateImageBehavior = false;
            this.peerAvailableColumns.View = System.Windows.Forms.View.Details;
            // 
            // availColNameHeader
            // 
            this.availColNameHeader.Text = "Column Name";
            this.availColNameHeader.Width = 150;
            // 
            // availTypeHeader
            // 
            this.availTypeHeader.Text = "Type";
            this.availTypeHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.availTypeHeader.Width = 80;
            // 
            // peerWeight
            // 
            this.peerWeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.peerWeight.Location = new System.Drawing.Point(284, 64);
            this.peerWeight.Name = "peerWeight";
            this.peerWeight.Size = new System.Drawing.Size(64, 20);
            this.peerWeight.TabIndex = 46;
            this.peerWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(284, 40);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 23);
            this.label40.TabIndex = 45;
            this.label40.Text = "Weight";
            // 
            // perfPeerAdd
            // 
            this.perfPeerAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfPeerAdd.Location = new System.Drawing.Point(284, 96);
            this.perfPeerAdd.Name = "perfPeerAdd";
            this.perfPeerAdd.Size = new System.Drawing.Size(104, 34);
            this.perfPeerAdd.TabIndex = 37;
            this.perfPeerAdd.Text = "Add to Performance";
            this.perfPeerAdd.Click += new System.EventHandler(this.perfPeerAdd_Click);
            // 
            // patternPeerAdd
            // 
            this.patternPeerAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.patternPeerAdd.Location = new System.Drawing.Point(284, 144);
            this.patternPeerAdd.Name = "patternPeerAdd";
            this.patternPeerAdd.Size = new System.Drawing.Size(104, 34);
            this.patternPeerAdd.TabIndex = 35;
            this.patternPeerAdd.Text = "Add to Success Patterns";
            this.patternPeerAdd.Click += new System.EventHandler(this.patternPeerAdd_Click);
            // 
            // performancePeeringGroupBox
            // 
            this.performancePeeringGroupBox.Controls.Add(this.perfPeerAttributes);
            this.performancePeeringGroupBox.Controls.Add(this.perfPeerRemove);
            this.performancePeeringGroupBox.Controls.Add(this.label17);
            this.performancePeeringGroupBox.Controls.Add(this.numPerfPeers);
            this.performancePeeringGroupBox.Location = new System.Drawing.Point(8, 97);
            this.performancePeeringGroupBox.Name = "performancePeeringGroupBox";
            this.performancePeeringGroupBox.Size = new System.Drawing.Size(396, 256);
            this.performancePeeringGroupBox.TabIndex = 46;
            this.performancePeeringGroupBox.TabStop = false;
            this.performancePeeringGroupBox.Text = "Performance Peering";
            // 
            // perfPeerAttributes
            // 
            this.perfPeerAttributes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perfPeerAttributes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.attribColumnHeader,
            this.typeHeader,
            this.weightHeader});
            this.perfPeerAttributes.FullRowSelect = true;
            this.perfPeerAttributes.GridLines = true;
            this.perfPeerAttributes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.perfPeerAttributes.Location = new System.Drawing.Point(16, 24);
            this.perfPeerAttributes.MultiSelect = false;
            this.perfPeerAttributes.Name = "perfPeerAttributes";
            this.perfPeerAttributes.ShowItemToolTips = true;
            this.perfPeerAttributes.Size = new System.Drawing.Size(352, 112);
            this.perfPeerAttributes.TabIndex = 23;
            this.perfPeerAttributes.UseCompatibleStateImageBehavior = false;
            this.perfPeerAttributes.View = System.Windows.Forms.View.Details;
            // 
            // attribColumnHeader
            // 
            this.attribColumnHeader.Text = "Column Name";
            this.attribColumnHeader.Width = 180;
            // 
            // typeHeader
            // 
            this.typeHeader.Text = "Type";
            this.typeHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.typeHeader.Width = 80;
            // 
            // weightHeader
            // 
            this.weightHeader.Text = "Weight";
            this.weightHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.weightHeader.Width = 65;
            // 
            // perfPeerRemove
            // 
            this.perfPeerRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.perfPeerRemove.Location = new System.Drawing.Point(264, 144);
            this.perfPeerRemove.Name = "perfPeerRemove";
            this.perfPeerRemove.Size = new System.Drawing.Size(104, 24);
            this.perfPeerRemove.TabIndex = 38;
            this.perfPeerRemove.Text = "Remove Selected";
            this.perfPeerRemove.Click += new System.EventHandler(this.perfPeerRemove_Click);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 200);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(192, 23);
            this.label17.TabIndex = 6;
            this.label17.Text = "Number of Performance Peers";
            // 
            // numPerfPeers
            // 
            this.numPerfPeers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numPerfPeers.Location = new System.Drawing.Point(224, 200);
            this.numPerfPeers.Name = "numPerfPeers";
            this.numPerfPeers.Size = new System.Drawing.Size(100, 20);
            this.numPerfPeers.TabIndex = 7;
            this.numPerfPeers.Text = "100";
            this.numPerfPeers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // successPatternPeeringGroupBox
            // 
            this.successPatternPeeringGroupBox.Controls.Add(this.successMeasure);
            this.successPatternPeeringGroupBox.Controls.Add(this.patternPeerAttributes);
            this.successPatternPeeringGroupBox.Controls.Add(this.patternPeerRemove);
            this.successPatternPeeringGroupBox.Controls.Add(this.label22);
            this.successPatternPeeringGroupBox.Controls.Add(this.label18);
            this.successPatternPeeringGroupBox.Controls.Add(this.numSuccessPeers);
            this.successPatternPeeringGroupBox.Location = new System.Drawing.Point(417, 97);
            this.successPatternPeeringGroupBox.Name = "successPatternPeeringGroupBox";
            this.successPatternPeeringGroupBox.Size = new System.Drawing.Size(386, 256);
            this.successPatternPeeringGroupBox.TabIndex = 47;
            this.successPatternPeeringGroupBox.TabStop = false;
            this.successPatternPeeringGroupBox.Text = "Success Pattern Peering";
            // 
            // successMeasure
            // 
            this.successMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.successMeasure.Location = new System.Drawing.Point(195, 200);
            this.successMeasure.Name = "successMeasure";
            this.successMeasure.Size = new System.Drawing.Size(184, 21);
            this.successMeasure.TabIndex = 39;
            // 
            // patternPeerAttributes
            // 
            this.patternPeerAttributes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.patternPeerAttributes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.scolNameHeader,
            this.scolTypeHeader,
            this.scolWeightHeader});
            this.patternPeerAttributes.FullRowSelect = true;
            this.patternPeerAttributes.GridLines = true;
            this.patternPeerAttributes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.patternPeerAttributes.Location = new System.Drawing.Point(11, 24);
            this.patternPeerAttributes.MultiSelect = false;
            this.patternPeerAttributes.Name = "patternPeerAttributes";
            this.patternPeerAttributes.ShowItemToolTips = true;
            this.patternPeerAttributes.Size = new System.Drawing.Size(352, 112);
            this.patternPeerAttributes.TabIndex = 30;
            this.patternPeerAttributes.UseCompatibleStateImageBehavior = false;
            this.patternPeerAttributes.View = System.Windows.Forms.View.Details;
            // 
            // scolNameHeader
            // 
            this.scolNameHeader.Text = "Column Name";
            this.scolNameHeader.Width = 180;
            // 
            // scolTypeHeader
            // 
            this.scolTypeHeader.Text = "Type";
            this.scolTypeHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.scolTypeHeader.Width = 80;
            // 
            // scolWeightHeader
            // 
            this.scolWeightHeader.Text = "Weight";
            this.scolWeightHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.scolWeightHeader.Width = 65;
            // 
            // patternPeerRemove
            // 
            this.patternPeerRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.patternPeerRemove.Location = new System.Drawing.Point(259, 144);
            this.patternPeerRemove.Name = "patternPeerRemove";
            this.patternPeerRemove.Size = new System.Drawing.Size(104, 24);
            this.patternPeerRemove.TabIndex = 36;
            this.patternPeerRemove.Text = "Remove Selected";
            this.patternPeerRemove.Click += new System.EventHandler(this.patternPeerRemove_Click);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 200);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(208, 23);
            this.label22.TabIndex = 19;
            this.label22.Text = "Success Measure";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 176);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(216, 23);
            this.label18.TabIndex = 8;
            this.label18.Text = "Number of Success Pattern Peers";
            // 
            // numSuccessPeers
            // 
            this.numSuccessPeers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numSuccessPeers.Location = new System.Drawing.Point(279, 176);
            this.numSuccessPeers.Name = "numSuccessPeers";
            this.numSuccessPeers.Size = new System.Drawing.Size(100, 20);
            this.numSuccessPeers.TabIndex = 9;
            this.numSuccessPeers.Text = "100";
            this.numSuccessPeers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.referenceTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(852, 733);
            this.tabControl1.TabIndex = 0;
            // 
            // refPopulationsMenu
            // 
            this.refPopulationsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.refPopulationsMenu.Name = "refPopulationsMenu";
            this.refPopulationsMenu.Size = new System.Drawing.Size(234, 48);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.addToolStripMenuItem.Text = "New Reference Population";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.removeToolStripMenuItem.Text = "Remove Reference Population";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // ReferencePopulations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "ReferencePopulations";
            this.Text = "ReferencePopulations";
            this.referenceTabPage.ResumeLayout(false);
            this.referenceTabPage.PerformLayout();
            this.filtersGroupBox.ResumeLayout(false);
            this.peeringAttributesGroupBox.ResumeLayout(false);
            this.peeringAttributesGroupBox.PerformLayout();
            this.performancePeeringGroupBox.ResumeLayout(false);
            this.performancePeeringGroupBox.PerformLayout();
            this.successPatternPeeringGroupBox.ResumeLayout(false);
            this.successPatternPeeringGroupBox.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.refPopulationsMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage referenceTabPage;
        private System.Windows.Forms.GroupBox filtersGroupBox;
        private System.Windows.Forms.CheckedListBox referenceFilterListBox;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.ComboBox successMeasure;
        private System.Windows.Forms.ListView patternPeerAttributes;
        private System.Windows.Forms.ColumnHeader scolNameHeader;
        private System.Windows.Forms.ColumnHeader scolTypeHeader;
        private System.Windows.Forms.ColumnHeader scolWeightHeader;
        private System.Windows.Forms.ListView perfPeerAttributes;
        private System.Windows.Forms.ColumnHeader attribColumnHeader;
        private System.Windows.Forms.ColumnHeader typeHeader;
        private System.Windows.Forms.ColumnHeader weightHeader;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox numSuccessPeers;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox numPerfPeers;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label referenceDimensionLabel;
        private System.Windows.Forms.GroupBox peeringAttributesGroupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView peerAvailableColumns;
        private System.Windows.Forms.ColumnHeader availColNameHeader;
        private System.Windows.Forms.ColumnHeader availTypeHeader;
        private System.Windows.Forms.TextBox peerWeight;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button perfPeerAdd;
        private System.Windows.Forms.Button patternPeerAdd;
        private System.Windows.Forms.Button perfPeerRemove;
        private System.Windows.Forms.Button patternPeerRemove;
        private System.Windows.Forms.GroupBox performancePeeringGroupBox;
        private System.Windows.Forms.GroupBox successPatternPeeringGroupBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ContextMenuStrip refPopulationsMenu;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.TextBox performanceTableTextBox;
        private System.Windows.Forms.TextBox successPatternTableTextBox;
        private System.Windows.Forms.Label successPatternTableLabel;
        private System.Windows.Forms.Label performanceTablelabel;
        public System.Windows.Forms.ComboBox targetDimension;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox levelTextBox;
        private System.Windows.Forms.ToolTip toolTip;

    }
}