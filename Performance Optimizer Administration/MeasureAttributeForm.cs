using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
	/// <summary>
	/// Summary description for StringForm.
	/// </summary>
	public class MeasureAttributeForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button OKbutton;
		private System.Windows.Forms.Button cancelButton;
		public System.Windows.Forms.TextBox textBox;
		private System.Windows.Forms.Label message;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.TextBox minBox;
		public System.Windows.Forms.TextBox maxBox;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.TextBox maxTargetBox;
		private System.Windows.Forms.Label label3;
		public System.Windows.Forms.TextBox minTargetBox;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.TextBox maxImpBox;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.TextBox minImpBox;
		private System.Windows.Forms.Label label6;
		public System.Windows.Forms.TextBox validBox;
		private System.Windows.Forms.Label label7;
		public System.Windows.Forms.CheckBox enforceBox;
		public System.Windows.Forms.TextBox weightBox;
		private System.Windows.Forms.Label label8;
		public System.Windows.Forms.TextBox impDirBox;
		private System.Windows.Forms.Label label9;
		public System.Windows.Forms.TextBox formatBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		public System.Windows.Forms.TextBox analysisMeasureBox;
		public System.Windows.Forms.TextBox segmentMeasureBox;
        private System.Windows.Forms.Label label12;
        private ToolTip toolTip;
        private IContainer components;

		public MeasureAttributeForm(string measure)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.Text = "Set Measure Attributes - "+measure;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasureAttributeForm));
            this.OKbutton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.minBox = new System.Windows.Forms.TextBox();
            this.maxBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maxTargetBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.minTargetBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maxImpBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.minImpBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.validBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.enforceBox = new System.Windows.Forms.CheckBox();
            this.weightBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.impDirBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.formatBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.analysisMeasureBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.segmentMeasureBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // OKbutton
            // 
            this.OKbutton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKbutton.Location = new System.Drawing.Point(176, 352);
            this.OKbutton.Name = "OKbutton";
            this.OKbutton.Size = new System.Drawing.Size(75, 24);
            this.OKbutton.TabIndex = 0;
            this.OKbutton.Text = "OK";
            this.OKbutton.Click += new System.EventHandler(this.OKbutton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(264, 352);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 24);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // message
            // 
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(0, 312);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(160, 16);
            this.message.TabIndex = 2;
            this.message.Text = "Opportunity Message";
            // 
            // textBox
            // 
            this.textBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox.Location = new System.Drawing.Point(0, 328);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(344, 20);
            this.textBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Minimum";
            this.toolTip.SetToolTip(this.label1, "Indicates the minimum value of the measure for display purposes (not internal cal" +
                    "culation)");
            // 
            // minBox
            // 
            this.minBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minBox.Location = new System.Drawing.Point(160, 56);
            this.minBox.Name = "minBox";
            this.minBox.Size = new System.Drawing.Size(184, 20);
            this.minBox.TabIndex = 5;
            // 
            // maxBox
            // 
            this.maxBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxBox.Location = new System.Drawing.Point(160, 80);
            this.maxBox.Name = "maxBox";
            this.maxBox.Size = new System.Drawing.Size(184, 20);
            this.maxBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Maximum";
            this.toolTip.SetToolTip(this.label2, "Indicates the maximum value of the measure for display purposes (not internal cal" +
                    "culation)\r\n");
            // 
            // maxTargetBox
            // 
            this.maxTargetBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxTargetBox.Location = new System.Drawing.Point(160, 128);
            this.maxTargetBox.Name = "maxTargetBox";
            this.maxTargetBox.Size = new System.Drawing.Size(184, 20);
            this.maxTargetBox.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Maximum Target";
            this.toolTip.SetToolTip(this.label3, "Indicates the maximum allowable value for a target measure.\r\n");
            // 
            // minTargetBox
            // 
            this.minTargetBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minTargetBox.Location = new System.Drawing.Point(160, 104);
            this.minTargetBox.Name = "minTargetBox";
            this.minTargetBox.Size = new System.Drawing.Size(184, 20);
            this.minTargetBox.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Minimum Target";
            this.toolTip.SetToolTip(this.label4, "Indicates the minimum allowable value for a target measure.");
            // 
            // maxImpBox
            // 
            this.maxImpBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxImpBox.Location = new System.Drawing.Point(160, 176);
            this.maxImpBox.Name = "maxImpBox";
            this.maxImpBox.Size = new System.Drawing.Size(184, 20);
            this.maxImpBox.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "Maximum Improvement";
            this.toolTip.SetToolTip(this.label5, "Indicates the maximum improvement to be targeted (usually some maximum value is a" +
                    "ctually achievable)\r\n");
            // 
            // minImpBox
            // 
            this.minImpBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minImpBox.Location = new System.Drawing.Point(160, 152);
            this.minImpBox.Name = "minImpBox";
            this.minImpBox.Size = new System.Drawing.Size(184, 20);
            this.minImpBox.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Minimum Improvement";
            this.toolTip.SetToolTip(this.label6, "Indicates the minimum meaningful improvement to be targeted (usually some minimum" +
                    " value is needed to be a meaningful goal)");
            // 
            // validBox
            // 
            this.validBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.validBox.Location = new System.Drawing.Point(160, 200);
            this.validBox.Name = "validBox";
            this.validBox.Size = new System.Drawing.Size(184, 20);
            this.validBox.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Valid Measure";
            this.toolTip.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            // 
            // enforceBox
            // 
            this.enforceBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.enforceBox.Location = new System.Drawing.Point(8, 296);
            this.enforceBox.Name = "enforceBox";
            this.enforceBox.Size = new System.Drawing.Size(192, 16);
            this.enforceBox.TabIndex = 18;
            this.enforceBox.Text = "Enforce Min/Max Limits in Query";
            this.toolTip.SetToolTip(this.enforceBox, "Force display limits on query results. This ensures that all data for calculation" +
                    "s will fall within a given range.");
            // 
            // weightBox
            // 
            this.weightBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.weightBox.Location = new System.Drawing.Point(160, 224);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(184, 20);
            this.weightBox.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 224);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Attribute Weight Measure";
            this.toolTip.SetToolTip(this.label8, resources.GetString("label8.ToolTip"));
            // 
            // impDirBox
            // 
            this.impDirBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.impDirBox.Location = new System.Drawing.Point(161, 8);
            this.impDirBox.Name = "impDirBox";
            this.impDirBox.Size = new System.Drawing.Size(39, 20);
            this.impDirBox.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Improvement Direction";
            this.toolTip.SetToolTip(this.label9, resources.GetString("label9.ToolTip"));
            // 
            // formatBox
            // 
            this.formatBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.formatBox.Location = new System.Drawing.Point(160, 32);
            this.formatBox.Name = "formatBox";
            this.formatBox.Size = new System.Drawing.Size(184, 20);
            this.formatBox.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(0, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 16);
            this.label10.TabIndex = 23;
            this.label10.Text = "Format";
            this.toolTip.SetToolTip(this.label10, "Indicates the formatting to be applied to the measure (Java Decimal Format)");
            // 
            // analysisMeasureBox
            // 
            this.analysisMeasureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.analysisMeasureBox.Location = new System.Drawing.Point(160, 248);
            this.analysisMeasureBox.Name = "analysisMeasureBox";
            this.analysisMeasureBox.Size = new System.Drawing.Size(184, 20);
            this.analysisMeasureBox.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 248);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(160, 16);
            this.label11.TabIndex = 25;
            this.label11.Text = "Analysis Measure";
            this.toolTip.SetToolTip(this.label11, resources.GetString("label11.ToolTip"));
            // 
            // segmentMeasureBox
            // 
            this.segmentMeasureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.segmentMeasureBox.Location = new System.Drawing.Point(160, 272);
            this.segmentMeasureBox.Name = "segmentMeasureBox";
            this.segmentMeasureBox.Size = new System.Drawing.Size(184, 20);
            this.segmentMeasureBox.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 272);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(160, 16);
            this.label12.TabIndex = 27;
            this.label12.Text = "Segment Measure";
            this.toolTip.SetToolTip(this.label12, resources.GetString("label12.ToolTip"));
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // MeasureAttributeForm
            // 
            this.AcceptButton = this.OKbutton;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(354, 383);
            this.ControlBox = false;
            this.Controls.Add(this.segmentMeasureBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.analysisMeasureBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.formatBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.impDirBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.weightBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.enforceBox);
            this.Controls.Add(this.validBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maxImpBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.minImpBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.maxTargetBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.minTargetBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maxBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.minBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.message);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKbutton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MeasureAttributeForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Measure Attributes";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void OKbutton_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}
	}
}
