using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class SubjectAreas : Form, RepositoryModule
    {
        MainAdminForm ma;
        Repository r;
        string moduleName = "Subject Areas";
        string categoryName = "Business Modeling";
        List<SubjectArea> salist;

        public SubjectAreas(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        public List<SubjectArea> getSubjectAreas()
        {
            return (salist);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (salist != null)
            {
                r.SubjectAreas = (SubjectArea[])salist.ToArray();
            }
        }

        public void updateFromRepository(Repository r)
        {
            selNode = null;
            this.r = r;
            if (r != null)
            {
                salist = new List<SubjectArea>(r.SubjectAreas);
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (salist != null)
            {
                foreach (SubjectArea sa in salist)
                {
                    addSubjectAreaToListView(sa);
                }
            }
            else
            {
                salist = new List<SubjectArea>();
            }
        }

        TreeNode selNode = null;

        public bool selectedNode(TreeNode node)
        {
            this.selNode = node;
            if (rootNode.Nodes.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = saContextMenu;
                removeToolStripMenuItem.Visible = false;
                upToolStripMenuItem.Visible = false;
                downToolStripMenuItem.Visible = false;
                saAccessMenuItem.Visible = false;
                pruneBadToolStripMenuItem.Visible = false;
                addSubjectAreaToolStripMenuItem.Visible = true;
                addFolderToolStripMenuItem.Visible = false;
                return false;
            }
            ma.mainTree.ContextMenuStrip = saContextMenu;
            removeToolStripMenuItem.Visible = true;
            upToolStripMenuItem.Visible = true;
            downToolStripMenuItem.Visible = true;
            saAccessMenuItem.Visible = true;
            pruneBadToolStripMenuItem.Visible = true;
            addSubjectAreaToolStripMenuItem.Visible = true;
            addFolderToolStripMenuItem.Visible = true;
            return true;
        }

        public void setup()
        {
            TreeNode lastNode = null;
            if (ma.lastNode != null)
            {
                lastNode = ma.lastNode;
                if (lastNode == rootNode)
                {
                    lastNode = null;
                }
            }
            if (lastNode == null || ma.lastPNode.Text != this.moduleName)
            {
                availableSubjectAreaColumns.BeginUpdate();
                availableSubjectAreaColumns.Items.Clear();
                availableSubjectAreaColumns.ListViewItemSorter = new MainAdminForm.Listcomparer(0, 1);
                List<string> dimList = ma.getDimensionList();
                foreach (string dim in dimList)
                {
                    List<string> columnList = ma.getDimensionColumnList(dim);
                    foreach (string col in columnList)
                    {
                        ListViewItem lvi = new ListViewItem(
                            new string[] { dim, col });
                        availableSubjectAreaColumns.Items.Add(lvi);
                    }
                }
                List<string> mList = ma.getMeasureList(null)[0];
                ListViewItem[] lvilist = new ListViewItem[mList.Count];
                int i = 0;
                foreach (string m in mList)
                {
                    ListViewItem lvi = new ListViewItem(
                        new string[] { "Measure", m });
                    lvilist[i++] = lvi;
                }
                availableSubjectAreaColumns.Items.AddRange(lvilist);
                availableSubjectAreaColumns.EndUpdate();
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return subjectAreaTabPage;
            }
            set
            {
            }
        }

        private void replaceNode(TreeNode tn, string find, string replace, bool match)
        {
            Object o = tn.Tag;
            if (o != null && o.GetType() == typeof(Column))
            {
                Column c = (Column)o;
                if (match)
                {
                    if (c.Name == find)
                    {
                        c.Name = replace;
                        tn.Text = c.Name;
                        tn.Name = c.Name;
                    }
                }
                else
                {
                    c.Name = c.Name.Replace(find, replace);
                    tn.Name = tn.Name.Replace(find, replace);
                }
            }
            else
                foreach (TreeNode child in tn.Nodes)
                    replaceNode(child, find, replace, match);
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            TreeNode tn = rootNode;
            replaceNode(tn, find, replace, match);
        }

        private void findNode(string sa, string folder, TreeNode tn, List<string> results, string find, bool match)
        {
            Object o = tn.Tag;
            if (o != null && o.GetType() == typeof(Column))
            {
                Column c = (Column)o;
                if (match)
                {
                    if (c.Name == find) results.Add("Subject Area: " + sa + ", Folder: " + folder);
                }
                else
                {
                    if (c.Name.IndexOf(find) >= 0) results.Add("Subject Area: " + sa + ", Folder: " + folder);
                }
            }
            else
            {
                if (o != null)
                {
                    if (o.GetType() == typeof(SubjectArea)) sa = ((SubjectArea)o).Name;
                    else if (o.GetType() == typeof(Folder)) folder = ((Folder)o).Name;
                }
                foreach (TreeNode child in tn.Nodes)
                    findNode(sa, folder, child, results, find, match);
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            TreeNode tn = rootNode;
            findNode(null, null, tn, results, find, match);
        }

        public void implicitSave()
        {
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        private void addSubjectAreaFolder(TreeNode tn, Folder f)
        {
            TreeNode newnode = new TreeNode(f.Name);
            newnode.Tag = f;
            newnode.ImageIndex = 6;
            newnode.SelectedImageIndex = 6;
            tn.Nodes.Add(newnode);
            if (f.Subfolders != null)
                foreach (Folder sf in f.Subfolders)
                {
                    addSubjectAreaFolder(newnode, sf);
                }
            if (f.Columns != null)
                foreach (Column c in f.Columns)
                {
                    TreeNode cnode = new TreeNode(c.Name);
                    cnode.ImageIndex = 6;
                    cnode.SelectedImageIndex = 6;
                    cnode.Tag = c;
                    newnode.Nodes.Add(cnode);
                }
        }

        private void addSubjectAreaToListView(SubjectArea sa)
        {
            TreeNode tn = new TreeNode(sa.Name);
            tn.Tag = sa;
            tn.ImageIndex = 6;
            tn.SelectedImageIndex = 6;
            rootNode.Nodes.Add(tn);
            foreach (Folder f in sa.Folders)
            {
                addSubjectAreaFolder(tn, f);
            }
        }
        private void addSAFolder_Click(object sender, System.EventArgs e)
        {
            if (selNode == null) return;
            Object o = selNode.Tag;
            if (o.GetType() == typeof(SubjectArea))
            {
                SubjectArea sa = (SubjectArea)o;
                Folder f = new Folder();
                f.Name = folderStripBox.Text;
                ArrayList list = new ArrayList(sa.Folders);
                list.Add(f);
                sa.Folders = (Folder[])list.ToArray(typeof(Folder)); ;
                TreeNode tn = new TreeNode(f.Name);
                tn.ImageIndex = 6;
                tn.SelectedImageIndex = 6;
                tn.Tag = f;
                selNode.Nodes.Add(tn);
            }
            else if (o.GetType() == typeof(Folder))
            {
                Folder pf = (Folder)o;
                Folder f = new Folder();
                f.Name = folderStripBox.Text;
                ArrayList list = null;
                if (pf.Subfolders == null)
                {
                    list = new ArrayList();
                }
                else
                {
                    list = new ArrayList(pf.Subfolders);
                }
                list.Add(f);
                pf.Subfolders = (Folder[])list.ToArray(typeof(Folder)); ;
                TreeNode tn = new TreeNode(f.Name);
                tn.Tag = f;
                selNode.Nodes.Add(tn);
            }
            selNode.Expand();
        }

        private void addSAColumns_Click(object sender, System.EventArgs e)
        {
            if (selNode == null) return;
            Object o = selNode.Tag;
            if (o.GetType() == typeof(Folder))
            {
                Folder pf = (Folder)o;
                ArrayList list = null;
                if (pf.Columns == null)
                {
                    list = new ArrayList();
                }
                else
                {
                    list = new ArrayList(pf.Columns);
                }
                foreach (ListViewItem lvi in availableSubjectAreaColumns.SelectedItems)
                {
                    Column c = new Column();
                    if (lvi.SubItems[0].Text != "Measure") c.Dimension = lvi.SubItems[0].Text;
                    c.Name = lvi.SubItems[1].Text;
                    bool found = false;
                    foreach (Column lc in list)
                    {
                        if ((c.Dimension == lc.Dimension) && (c.Name == lc.Name))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        list.Add(c);
                        TreeNode tn = new TreeNode(c.Name);
                        tn.ImageIndex = 6;
                        tn.SelectedImageIndex = 6;
                        tn.Tag = c;
                        selNode.Nodes.Add(tn);
                    }
                }
                pf.Columns = (Column[])list.ToArray(typeof(Column));
                selNode.Expand();
            }
        }

        private void removeSATreeNode(TreeNode tn)
        {
            Object o = tn.Tag;
            if (o.GetType() == typeof(SubjectArea))
            {
                salist.Remove((SubjectArea)o);
            }
            else if (o.GetType() == typeof(Folder))
            {
                Object p = tn.Parent.Tag;
                if (p.GetType() == typeof(Folder))
                {
                    ArrayList list = new ArrayList(((Folder)p).Subfolders);
                    list.Remove(o);
                    ((Folder)p).Subfolders = (Folder[])list.ToArray(typeof(Folder));
                }
                else
                {
                    ArrayList list = new ArrayList(((SubjectArea)p).Folders);
                    list.Remove(o);
                    ((SubjectArea)p).Folders = (Folder[])list.ToArray(typeof(Folder));
                }
            }
            else
            {
                Object p = tn.Parent.Tag;
                ArrayList list = new ArrayList(((Folder)p).Columns);
                list.Remove(o);
                ((Folder)p).Columns = (Column[])list.ToArray(typeof(Column));
            }
            tn.Remove();
        }

        private void removeSASelected_Click(object sender, System.EventArgs e)
        {
            removeSATreeNode(selNode);
        }

        private void pruneSANode(TreeNode tn, List<string> measureList)
        {
            Object o = tn.Tag;
            if (o.GetType() == typeof(SubjectArea))
            {
                foreach (TreeNode ctn in tn.Nodes)
                {
                    pruneSANode(ctn, measureList);
                }
            }
            else if (o.GetType() == typeof(Folder))
            {
                for (int i = 0; i < tn.Nodes.Count; i++)
                {
                    TreeNode ctn = tn.Nodes[i];
                    Object co = ctn.Tag;
                    if (co.GetType() == typeof(Column))
                    {
                        Column c = (Column)co;
                        if (c.Dimension == null)
                        {
                            if (!measureList.Contains(c.Name))
                            {
                                removeSATreeNode(ctn);
                                i--;
                            }
                        }
                        else
                        {
                            bool found = false;
                            foreach (DimensionTable dt in ma.dimensionTablesList)
                            {
                                if (dt.DimensionName == c.Dimension)
                                {
                                     foreach (DimensionColumn dc in dt.DimensionColumns)
                                    {
                                        if (dc.ColumnName == c.Name)
                                         {
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                                if (found) break;
                            }
                            /*
                             * If not found, see if columns are inherited
                             */
                            if (!found)
                            {
                                foreach (DimensionTable dt in ma.dimensionTablesList)
                                {
                                    if (dt.InheritTable != null && dt.DimensionName == c.Dimension)
                                    {
                                        int inh = ma.findDimensionTableIndex(dt.InheritTable);
                                        if (inh > -1)
                                        {
                                            DimensionTable idt = ma.dimensionTablesList[inh];
                                            foreach (DimensionColumn dc in idt.DimensionColumns)
                                            {
                                                if (dt.InheritPrefix + dc.ColumnName == c.Name)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else if (dt.VirtualMeasuresInheritTable != null && dt.DimensionName == c.Dimension)
                                    {
                                        int vmit = ma.findDimensionTableIndex(dt.VirtualMeasuresInheritTable);
                                        if (vmit > -1)
                                        {
                                            DimensionTable vdt = ma.dimensionTablesList[vmit];
                                            foreach (DimensionColumn dc in vdt.DimensionColumns)
                                            {
                                                if (dt.VirtualMeasuresPrefix + dc.ColumnName == c.Name)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (found) break;
                                }
                            }
                            if (!found)
                            {
                                if (ma.vcolumns.vclist != null)
                                    foreach (VirtualColumn vc in ma.vcolumns.vclist.virtualColumns)
                                    {
                                        if (vc.Type != VirtualColumn.TYPE_PIVOT)
                                        {
                                            foreach (DimensionTable dt in ma.dimensionTablesList)
                                            {
                                                if (vc.TableName == dt.TableName)
                                                {
                                                    if ((vc.Name == c.Name) && (dt.DimensionName == c.Dimension))
                                                    {
                                                        found = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                            }
                            if (!found)
                            {
                                removeSATreeNode(ctn);
                                i--;
                            }
                        }
                    }
                    else
                    {
                        pruneSANode(ctn, measureList);
                    }
                }
            }
        }

        private void pruneSAbadColumns_Click(object sender, System.EventArgs e)
        {
            List<string> mlist = ma.getMeasureList(null)[0];
            foreach (TreeNode tn in rootNode.Nodes)
            {
                pruneSANode(tn, mlist);
            }
        }

        private void upSASelected_Click(object sender, System.EventArgs e)
        {
            Object o = selNode.Tag;
            if (o.GetType() == typeof(SubjectArea)) return;
            Object p = selNode.Parent.Tag;
            if (p.GetType() == typeof(Folder))
            {
                if (o.GetType() == typeof(Column))
                {
                    ArrayList list = new ArrayList(((Folder)p).Columns);
                    int index = list.IndexOf(o);
                    if (index > 0)
                    {
                        list.RemoveAt(index);
                        list.Insert(index - 1, o);
                    }
                    ((Folder)p).Columns = (Column[])list.ToArray(typeof(Column));
                }
                else
                {
                    ArrayList list = new ArrayList(((Folder)p).Subfolders);
                    int index = list.IndexOf(o);
                    if (index > 0)
                    {
                        list.RemoveAt(index);
                        list.Insert(index - 1, o);
                    }
                    ((Folder)p).Subfolders = (Folder[])list.ToArray(typeof(Folder));
                }
            }
            else
            {
                ArrayList list = new ArrayList(((SubjectArea)p).Folders);
                int index = list.IndexOf(o);
                if (index > 0)
                {
                    list.RemoveAt(index);
                    list.Insert(index - 1, o);
                }
                ((SubjectArea)p).Folders = (Folder[])list.ToArray(typeof(Folder));
            }
            TreeNode tn = selNode;
            TreeNode ptn = tn.Parent;
            int ind = ptn.Nodes.IndexOf(tn);
            if (ind > 0)
            {
                ptn.Nodes.RemoveAt(ind);
                ptn.Nodes.Insert(ind - 1, tn);
            }
            ma.mainTree.SelectedNode = tn;
        }

        private void downSASelected_Click(object sender, System.EventArgs e)
        {
            Object o = selNode.Tag;
            if (o.GetType() == typeof(SubjectArea)) return;
            Object p = selNode.Parent.Tag;
            if (p.GetType() == typeof(Folder))
            {
                if (o.GetType() == typeof(Column))
                {
                    ArrayList list = new ArrayList(((Folder)p).Columns);
                    int index = list.IndexOf(o);
                    if (index < list.Count - 1)
                    {
                        list.RemoveAt(index);
                        list.Insert(index + 1, o);
                    }
                    ((Folder)p).Columns = (Column[])list.ToArray(typeof(Column));
                }
                else
                {
                    ArrayList list = new ArrayList(((Folder)p).Subfolders);
                    int index = list.IndexOf(o);
                    if (index < list.Count - 1)
                    {
                        list.RemoveAt(index);
                        list.Insert(index + 1, o);
                    }
                    ((Folder)p).Subfolders = (Folder[])list.ToArray(typeof(Folder));
                }
            }
            else
            {
                ArrayList list = new ArrayList(((SubjectArea)p).Folders);
                int index = list.IndexOf(o);
                if (index < list.Count - 1)
                {
                    list.RemoveAt(index);
                    list.Insert(index + 1, o);
                }
                ((SubjectArea)p).Folders = (Folder[])list.ToArray(typeof(Folder));
            }
            TreeNode tn = selNode;
            TreeNode ptn = tn.Parent;
            int ind = ptn.Nodes.IndexOf(tn);
            if (ind < ptn.Nodes.Count - 1)
            {
                ptn.Nodes.RemoveAt(ind);
                ptn.Nodes.Insert(ind + 1, tn);
            }
            ma.mainTree.SelectedNode = tn;
        }

        public bool returnFoundInSA(Column c, TreeNode tn)
        {
            Object o = tn.Tag;
            if ((o.GetType() == typeof(SubjectArea)) || (o.GetType() == typeof(Folder)))
            {
                foreach (TreeNode ctn in tn.Nodes)
                {
                    if (returnFoundInSA(c, ctn)) return (true);
                }
            }
            else if (o.GetType() == typeof(Column))
            {
                Column cc = (Column)o;
                if ((cc.Dimension == c.Dimension) && (cc.Name == c.Name)) return (true);
            }
            return (false);
        }

        private void saHighlight_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem lvi in availableSubjectAreaColumns.Items)
            {
                Column c = new Column();
                if (lvi.SubItems[0].Text != "Measure") c.Dimension = lvi.SubItems[0].Text;
                c.Name = lvi.SubItems[1].Text;
                bool found = false;
                foreach (TreeNode tn in rootNode.Nodes)
                {
                    if (returnFoundInSA(c, tn))
                    {
                        found = true;
                        break;
                    }
                }
                lvi.Selected = !found;
            }
        }

        private void addSAButton_Click(object sender, System.EventArgs e)
        {
            if (salist == null)
            {
                salist = new List<SubjectArea>();
            }
            SubjectArea sa = new SubjectArea();
            sa.Name = this.saToolStripBox.Text;
            sa.Folders = new Folder[0];
            salist.Add(sa);
            TreeNode tn = new TreeNode(sa.Name);
            tn.Tag = sa;
            tn.ImageIndex = 6;
            tn.SelectedImageIndex = 6;
            rootNode.Nodes.Add(tn);
            tn.ExpandAll();
        }

        private void saAccessMenuItem_Click(object sender, EventArgs e)
        {

            Object o = selNode.Tag;
            List<String> selectedGroups = new List<string>();
            if (o.GetType() == typeof(SubjectArea))
            {
                SubjectArea sa = (SubjectArea)o;
                if (sa.Groups != null)
                {
                    foreach (String s in sa.Groups)
                    {
                        selectedGroups.Add(s);
                    }
                }
            }
            else if (o.GetType() == typeof(Folder))
            {
                Folder f = (Folder)o;
                if (f.Groups != null)
                {
                    foreach (String s in f.Groups)
                    {
                        selectedGroups.Add(s);
                    }
                }
            }
            else return;
            List<Group> glist = ma.usersAndGroups.groupsList;
            SelectGroups sg = new SelectGroups(glist, selectedGroups);
            sg.label.Text = "Select Groups with Access";
            DialogResult result = sg.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (o.GetType() == typeof(SubjectArea))
                {
                    SubjectArea sa = (SubjectArea)o;
                    sa.Groups = sg.selectedGroups.ToArray();
                }
                else if (o.GetType() == typeof(Folder))
                {
                    Folder f = (Folder)o;
                    f.Groups = sg.selectedGroups.ToArray();
                }
            }
        }

    }
}