﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class BuildPropertiesModule : Form, RepositoryModule
    {
        MainAdminForm ma;
        private string moduleName = "Build Properties";
        private string categoryName = "Warehouse";        

        public BuildPropertiesModule(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        public string getPostBuildReminder()
        {
            return postBuildReminderTextBox.Text;
        }

        #region RepositoryModule Members

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return postBuildReminderTabPage;
            }
            set
            {
            }
        }

        public void setRepository(Repository r)
        {
            r.BuildProperties = new BuildProperties();
            r.BuildProperties.postBuildReminder = postBuildReminderTextBox.Text;
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.BuildProperties != null && r.BuildProperties.postBuildReminder != null)
                postBuildReminderTextBox.Text = r.BuildProperties.postBuildReminder;
            else
                postBuildReminderTextBox.Text = "";            
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

    }
}
