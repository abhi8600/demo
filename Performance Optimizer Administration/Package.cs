﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Performance_Optimizer_Administration
{
    [XmlRootAttribute("Package", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class Package
    {
        public Guid ID;
        public string Name;
        public string Description;
        public Guid SpaceID;
        public string[] StagingTables;
        public string[] DimensionTables;
        public string[] DimensionTableDisplayNames;
        public string[] MeasureTables;
        public string[] MeasureTableDisplayNames;
        public string[] Variables;
        public string CreatedBy;
        public string ModifiedBy;
        public DateTime? CreatedDate;
        public DateTime? ModifiedDate;   

        public Package()
        {
        }

        public Package(string name, Guid spaceID)
        {
            this.Name = name;
            this.SpaceID = spaceID;
        }
    }
}
