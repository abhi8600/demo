namespace Performance_Optimizer_Administration
{
    partial class SuccessPatterns
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SuccessPatterns));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.patternsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.patternsPerMeasure = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ignoreZeroWeights = new System.Windows.Forms.CheckBox();
            this.dropPSTable = new System.Windows.Forms.CheckBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.patternOutputTable = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.patternNumToSave = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SPreferencePopulationBox = new System.Windows.Forms.ComboBox();
            this.refPercent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.splitSearch = new System.Windows.Forms.RadioButton();
            this.comparisonFilters = new System.Windows.Forms.CheckedListBox();
            this.referenceFilters = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.populationSearch = new System.Windows.Forms.RadioButton();
            this.peerSearch = new System.Windows.Forms.RadioButton();
            this.numTopPeers = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dropSegmentTable = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.segmentOutputTableName = new System.Windows.Forms.TextBox();
            this.processSeparately = new System.Windows.Forms.CheckBox();
            this.segmentNumToSave = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.screenDupMeasures = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.joinMeasureBox = new System.Windows.Forms.ComboBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.targetLevelBox = new System.Windows.Forms.ComboBox();
            this.targetDimBox = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.segmentListMeasures = new System.Windows.Forms.CheckedListBox();
            this.genSegments = new System.Windows.Forms.CheckBox();
            this.patternIterationValue = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.patternDefinitionModel = new System.Windows.Forms.ComboBox();
            this.labelBaseModelDataset = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.patternMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addPatternSearchDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removePatternSearchDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.patternsTabPage.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.patternMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.patternsTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(852, 733);
            this.tabControl1.TabIndex = 0;
            // 
            // patternsTabPage
            // 
            this.patternsTabPage.AutoScroll = true;
            this.patternsTabPage.Controls.Add(this.groupBox23);
            this.patternsTabPage.Location = new System.Drawing.Point(4, 22);
            this.patternsTabPage.Name = "patternsTabPage";
            this.patternsTabPage.Size = new System.Drawing.Size(844, 707);
            this.patternsTabPage.TabIndex = 14;
            this.patternsTabPage.Text = "Success Patterns";
            this.patternsTabPage.UseVisualStyleBackColor = true;
            this.patternsTabPage.Leave += new System.EventHandler(this.patternsTabPage_Leave);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.patternsPerMeasure);
            this.groupBox23.Controls.Add(this.label5);
            this.groupBox23.Controls.Add(this.groupBox3);
            this.groupBox23.Controls.Add(this.groupBox2);
            this.groupBox23.Controls.Add(this.groupBox1);
            this.groupBox23.Controls.Add(this.patternIterationValue);
            this.groupBox23.Controls.Add(this.label133);
            this.groupBox23.Controls.Add(this.patternDefinitionModel);
            this.groupBox23.Controls.Add(this.labelBaseModelDataset);
            this.groupBox23.Location = new System.Drawing.Point(8, 3);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(832, 696);
            this.groupBox23.TabIndex = 73;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Success Pattern Search Definition";
            // 
            // patternsPerMeasure
            // 
            this.patternsPerMeasure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.patternsPerMeasure.Location = new System.Drawing.Point(599, 48);
            this.patternsPerMeasure.Name = "patternsPerMeasure";
            this.patternsPerMeasure.Size = new System.Drawing.Size(36, 20);
            this.patternsPerMeasure.TabIndex = 96;
            this.patternsPerMeasure.Text = "3";
            this.toolTip.SetToolTip(this.patternsPerMeasure, "Determines the number of patterns to find for each measure.");
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(431, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 20);
            this.label5.TabIndex = 97;
            this.label5.Text = "# Patterns/Measure";
            this.toolTip.SetToolTip(this.label5, "Minimum absolute target value");
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ignoreZeroWeights);
            this.groupBox3.Controls.Add(this.dropPSTable);
            this.groupBox3.Controls.Add(this.label136);
            this.groupBox3.Controls.Add(this.label97);
            this.groupBox3.Controls.Add(this.patternOutputTable);
            this.groupBox3.Controls.Add(this.label96);
            this.groupBox3.Controls.Add(this.patternNumToSave);
            this.groupBox3.Location = new System.Drawing.Point(6, 207);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(818, 93);
            this.groupBox3.TabIndex = 95;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Search Results Output";
            // 
            // ignoreZeroWeights
            // 
            this.ignoreZeroWeights.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ignoreZeroWeights.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ignoreZeroWeights.Location = new System.Drawing.Point(5, 68);
            this.ignoreZeroWeights.Name = "ignoreZeroWeights";
            this.ignoreZeroWeights.Size = new System.Drawing.Size(448, 19);
            this.ignoreZeroWeights.TabIndex = 88;
            this.ignoreZeroWeights.Text = "Ignore Segments with Zero Weights for Targets (and generate no segment defintion)" +
                "";
            // 
            // dropPSTable
            // 
            this.dropPSTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dropPSTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropPSTable.Location = new System.Drawing.Point(472, 15);
            this.dropPSTable.Name = "dropPSTable";
            this.dropPSTable.Size = new System.Drawing.Size(128, 24);
            this.dropPSTable.TabIndex = 87;
            this.dropPSTable.Text = "Drop Existing Table";
            // 
            // label136
            // 
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(320, 42);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(168, 24);
            this.label136.TabIndex = 79;
            this.label136.Text = "(<0 means save all patterns)";
            // 
            // label97
            // 
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(2, 19);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(234, 23);
            this.label97.TabIndex = 19;
            this.label97.Text = "Success Pattern Output Table Name";
            // 
            // patternOutputTable
            // 
            this.patternOutputTable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.patternOutputTable.Location = new System.Drawing.Point(242, 19);
            this.patternOutputTable.Name = "patternOutputTable";
            this.patternOutputTable.Size = new System.Drawing.Size(224, 20);
            this.patternOutputTable.TabIndex = 20;
            // 
            // label96
            // 
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(2, 42);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(176, 19);
            this.label96.TabIndex = 23;
            this.label96.Text = "Number of Patterns to Save";
            // 
            // patternNumToSave
            // 
            this.patternNumToSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.patternNumToSave.Location = new System.Drawing.Point(242, 42);
            this.patternNumToSave.Name = "patternNumToSave";
            this.patternNumToSave.Size = new System.Drawing.Size(72, 20);
            this.patternNumToSave.TabIndex = 24;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.SPreferencePopulationBox);
            this.groupBox2.Controls.Add(this.refPercent);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.splitSearch);
            this.groupBox2.Controls.Add(this.comparisonFilters);
            this.groupBox2.Controls.Add(this.referenceFilters);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.populationSearch);
            this.groupBox2.Controls.Add(this.peerSearch);
            this.groupBox2.Controls.Add(this.numTopPeers);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Location = new System.Drawing.Point(6, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(819, 130);
            this.groupBox2.TabIndex = 94;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Type of Pattern Search";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 23);
            this.label7.TabIndex = 106;
            this.label7.Text = "Reference Population";
            // 
            // SPreferencePopulationBox
            // 
            this.SPreferencePopulationBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SPreferencePopulationBox.Location = new System.Drawing.Point(8, 95);
            this.SPreferencePopulationBox.Name = "SPreferencePopulationBox";
            this.SPreferencePopulationBox.Size = new System.Drawing.Size(200, 21);
            this.SPreferencePopulationBox.TabIndex = 105;
            this.toolTip.SetToolTip(this.SPreferencePopulationBox, "Reference Population");
            // 
            // refPercent
            // 
            this.refPercent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.refPercent.Location = new System.Drawing.Point(606, 78);
            this.refPercent.Name = "refPercent";
            this.refPercent.Size = new System.Drawing.Size(36, 20);
            this.refPercent.TabIndex = 104;
            this.refPercent.Text = "50";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(603, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(207, 14);
            this.label6.TabIndex = 103;
            this.label6.Text = "Percent in Reference Population";
            // 
            // splitSearch
            // 
            this.splitSearch.AutoSize = true;
            this.splitSearch.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.splitSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.splitSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitSearch.Location = new System.Drawing.Point(587, 19);
            this.splitSearch.Name = "splitSearch";
            this.splitSearch.Size = new System.Drawing.Size(225, 36);
            this.splitSearch.TabIndex = 100;
            this.splitSearch.Text = "Compare Populations Using Split \r\nBased on Target Metric\r\n";
            this.splitSearch.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.splitSearch.UseVisualStyleBackColor = true;
            // 
            // comparisonFilters
            // 
            this.comparisonFilters.CheckOnClick = true;
            this.comparisonFilters.FormattingEnabled = true;
            this.comparisonFilters.Location = new System.Drawing.Point(397, 58);
            this.comparisonFilters.Name = "comparisonFilters";
            this.comparisonFilters.Size = new System.Drawing.Size(184, 64);
            this.comparisonFilters.TabIndex = 99;
            this.comparisonFilters.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessPatterns_MouseMove);
            // 
            // referenceFilters
            // 
            this.referenceFilters.CheckOnClick = true;
            this.referenceFilters.FormattingEnabled = true;
            this.referenceFilters.Location = new System.Drawing.Point(212, 58);
            this.referenceFilters.Name = "referenceFilters";
            this.referenceFilters.Size = new System.Drawing.Size(179, 64);
            this.referenceFilters.TabIndex = 98;
            this.referenceFilters.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessPatterns_MouseMove);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 13);
            this.label4.TabIndex = 97;
            this.label4.Text = "Filters Defining Comparison Population";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(182, 13);
            this.label3.TabIndex = 95;
            this.label3.Text = "Filters Defining Reference Population";
            // 
            // populationSearch
            // 
            this.populationSearch.AutoSize = true;
            this.populationSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.populationSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.populationSearch.Location = new System.Drawing.Point(212, 19);
            this.populationSearch.Name = "populationSearch";
            this.populationSearch.Size = new System.Drawing.Size(232, 20);
            this.populationSearch.TabIndex = 93;
            this.populationSearch.Text = "Compare Populations Using Filters";
            this.populationSearch.UseVisualStyleBackColor = true;
            // 
            // peerSearch
            // 
            this.peerSearch.AutoSize = true;
            this.peerSearch.Checked = true;
            this.peerSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.peerSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.peerSearch.Location = new System.Drawing.Point(5, 19);
            this.peerSearch.Name = "peerSearch";
            this.peerSearch.Size = new System.Drawing.Size(200, 20);
            this.peerSearch.TabIndex = 92;
            this.peerSearch.TabStop = true;
            this.peerSearch.Text = "Compare Individual vs. Peers";
            this.peerSearch.UseVisualStyleBackColor = true;
            // 
            // numTopPeers
            // 
            this.numTopPeers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numTopPeers.Location = new System.Drawing.Point(159, 40);
            this.numTopPeers.Name = "numTopPeers";
            this.numTopPeers.Size = new System.Drawing.Size(41, 20);
            this.numTopPeers.TabIndex = 78;
            this.numTopPeers.Text = "20";
            this.numTopPeers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 42);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(145, 18);
            this.label19.TabIndex = 77;
            this.label19.Text = "# of Top Peers to Compare";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dropSegmentTable);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.segmentOutputTableName);
            this.groupBox1.Controls.Add(this.processSeparately);
            this.groupBox1.Controls.Add(this.segmentNumToSave);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.screenDupMeasures);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.joinMeasureBox);
            this.groupBox1.Controls.Add(this.label109);
            this.groupBox1.Controls.Add(this.label108);
            this.groupBox1.Controls.Add(this.targetLevelBox);
            this.groupBox1.Controls.Add(this.targetDimBox);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.segmentListMeasures);
            this.groupBox1.Controls.Add(this.genSegments);
            this.groupBox1.Location = new System.Drawing.Point(6, 306);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(819, 384);
            this.groupBox1.TabIndex = 91;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Target Segment Lists";
            // 
            // dropSegmentTable
            // 
            this.dropSegmentTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dropSegmentTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropSegmentTable.Location = new System.Drawing.Point(326, 184);
            this.dropSegmentTable.Name = "dropSegmentTable";
            this.dropSegmentTable.Size = new System.Drawing.Size(128, 24);
            this.dropSegmentTable.TabIndex = 104;
            this.dropSegmentTable.Text = "Drop Existing Table";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(310, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 23);
            this.label2.TabIndex = 102;
            this.label2.Text = "Segment Output Table Name";
            // 
            // segmentOutputTableName
            // 
            this.segmentOutputTableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.segmentOutputTableName.Location = new System.Drawing.Point(502, 167);
            this.segmentOutputTableName.Name = "segmentOutputTableName";
            this.segmentOutputTableName.Size = new System.Drawing.Size(224, 20);
            this.segmentOutputTableName.TabIndex = 103;
            // 
            // processSeparately
            // 
            this.processSeparately.AutoSize = true;
            this.processSeparately.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.processSeparately.Location = new System.Drawing.Point(313, 143);
            this.processSeparately.Name = "processSeparately";
            this.processSeparately.Size = new System.Drawing.Size(154, 17);
            this.processSeparately.TabIndex = 101;
            this.processSeparately.Text = "Process Targets Separately";
            this.toolTip.SetToolTip(this.processSeparately, "Whether or not to process all targets simultaneously or via separate passes/queri" +
                    "es (normally set to false)");
            this.processSeparately.UseVisualStyleBackColor = true;
            // 
            // segmentNumToSave
            // 
            this.segmentNumToSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.segmentNumToSave.Location = new System.Drawing.Point(424, 97);
            this.segmentNumToSave.Name = "segmentNumToSave";
            this.segmentNumToSave.Size = new System.Drawing.Size(100, 20);
            this.segmentNumToSave.TabIndex = 100;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(310, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 23);
            this.label1.TabIndex = 99;
            this.label1.Text = "Number to Save";
            this.toolTip.SetToolTip(this.label1, "Specifies the maximum number of segments to be saved for each entity. Segments wi" +
                    "th the highest pattern search scores will be chosen first.");
            // 
            // screenDupMeasures
            // 
            this.screenDupMeasures.AutoSize = true;
            this.screenDupMeasures.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.screenDupMeasures.Location = new System.Drawing.Point(313, 120);
            this.screenDupMeasures.Name = "screenDupMeasures";
            this.screenDupMeasures.Size = new System.Drawing.Size(155, 17);
            this.screenDupMeasures.TabIndex = 98;
            this.screenDupMeasures.Text = "Screen Duplicate Measures";
            this.toolTip.SetToolTip(this.screenDupMeasures, resources.GetString("screenDupMeasures.ToolTip"));
            this.screenDupMeasures.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(310, 71);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 23);
            this.label29.TabIndex = 96;
            this.label29.Text = "Join Measure";
            this.toolTip.SetToolTip(this.label29, resources.GetString("label29.ToolTip"));
            // 
            // joinMeasureBox
            // 
            this.joinMeasureBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.joinMeasureBox.Location = new System.Drawing.Point(424, 70);
            this.joinMeasureBox.Name = "joinMeasureBox";
            this.joinMeasureBox.Size = new System.Drawing.Size(232, 21);
            this.joinMeasureBox.Sorted = true;
            this.joinMeasureBox.TabIndex = 97;
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(560, 27);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(72, 16);
            this.label109.TabIndex = 95;
            this.label109.Text = "Level";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(424, 27);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(72, 16);
            this.label108.TabIndex = 94;
            this.label108.Text = "Dimension";
            // 
            // targetLevelBox
            // 
            this.targetLevelBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetLevelBox.Location = new System.Drawing.Point(560, 43);
            this.targetLevelBox.Name = "targetLevelBox";
            this.targetLevelBox.Size = new System.Drawing.Size(128, 21);
            this.targetLevelBox.Sorted = true;
            this.targetLevelBox.TabIndex = 93;
            // 
            // targetDimBox
            // 
            this.targetDimBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetDimBox.Location = new System.Drawing.Point(424, 43);
            this.targetDimBox.Name = "targetDimBox";
            this.targetDimBox.Size = new System.Drawing.Size(128, 21);
            this.targetDimBox.Sorted = true;
            this.targetDimBox.TabIndex = 92;
            this.targetDimBox.SelectedIndexChanged += new System.EventHandler(this.targetDimBox_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(310, 44);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 23);
            this.label28.TabIndex = 91;
            this.label28.Text = "Target";
            this.toolTip.SetToolTip(this.label28, resources.GetString("label28.ToolTip"));
            // 
            // segmentListMeasures
            // 
            this.segmentListMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.segmentListMeasures.CheckOnClick = true;
            this.segmentListMeasures.FormattingEnabled = true;
            this.segmentListMeasures.Location = new System.Drawing.Point(6, 43);
            this.segmentListMeasures.Name = "segmentListMeasures";
            this.segmentListMeasures.Size = new System.Drawing.Size(284, 332);
            this.segmentListMeasures.TabIndex = 89;
            this.segmentListMeasures.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessPatterns_MouseMove);
            // 
            // genSegments
            // 
            this.genSegments.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.genSegments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genSegments.Location = new System.Drawing.Point(6, 18);
            this.genSegments.Name = "genSegments";
            this.genSegments.Size = new System.Drawing.Size(350, 19);
            this.genSegments.TabIndex = 90;
            this.genSegments.Text = "Generate segments for selected measures";
            // 
            // patternIterationValue
            // 
            this.patternIterationValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.patternIterationValue.Location = new System.Drawing.Point(599, 24);
            this.patternIterationValue.Name = "patternIterationValue";
            this.patternIterationValue.Size = new System.Drawing.Size(160, 20);
            this.patternIterationValue.TabIndex = 76;
            // 
            // label133
            // 
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(431, 24);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(160, 16);
            this.label133.TabIndex = 75;
            this.label133.Text = "Iteration Value to Analyze";
            // 
            // patternDefinitionModel
            // 
            this.patternDefinitionModel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.patternDefinitionModel.Location = new System.Drawing.Point(158, 23);
            this.patternDefinitionModel.Name = "patternDefinitionModel";
            this.patternDefinitionModel.Size = new System.Drawing.Size(239, 21);
            this.patternDefinitionModel.TabIndex = 25;
            // 
            // labelBaseModelDataset
            // 
            this.labelBaseModelDataset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBaseModelDataset.Location = new System.Drawing.Point(8, 24);
            this.labelBaseModelDataset.Name = "labelBaseModelDataset";
            this.labelBaseModelDataset.Size = new System.Drawing.Size(144, 23);
            this.labelBaseModelDataset.TabIndex = 72;
            this.labelBaseModelDataset.Text = "Base Model Dataset";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // patternMenuStrip
            // 
            this.patternMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPatternSearchDefinitionToolStripMenuItem,
            this.removePatternSearchDefinitionToolStripMenuItem});
            this.patternMenuStrip.Name = "patternMenuStrip";
            this.patternMenuStrip.ShowImageMargin = false;
            this.patternMenuStrip.Size = new System.Drawing.Size(227, 48);
            // 
            // addPatternSearchDefinitionToolStripMenuItem
            // 
            this.addPatternSearchDefinitionToolStripMenuItem.Name = "addPatternSearchDefinitionToolStripMenuItem";
            this.addPatternSearchDefinitionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.addPatternSearchDefinitionToolStripMenuItem.Text = "Add Pattern Search Definition";
            this.addPatternSearchDefinitionToolStripMenuItem.Click += new System.EventHandler(this.addPatternDefinition_Click);
            // 
            // removePatternSearchDefinitionToolStripMenuItem
            // 
            this.removePatternSearchDefinitionToolStripMenuItem.Name = "removePatternSearchDefinitionToolStripMenuItem";
            this.removePatternSearchDefinitionToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.removePatternSearchDefinitionToolStripMenuItem.Text = "Remove Pattern Search Definition";
            this.removePatternSearchDefinitionToolStripMenuItem.Click += new System.EventHandler(this.removePatternDefinition_Click);
            // 
            // SuccessPatterns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "SuccessPatterns";
            this.Text = "SuccessPatterns";
            this.tabControl1.ResumeLayout(false);
            this.patternsTabPage.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.patternMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage patternsTabPage;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.CheckBox ignoreZeroWeights;
        private System.Windows.Forms.CheckBox dropPSTable;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox numTopPeers;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox patternIterationValue;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.ComboBox patternDefinitionModel;
        private System.Windows.Forms.TextBox patternOutputTable;
        private System.Windows.Forms.Label labelBaseModelDataset;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox patternNumToSave;
        private System.Windows.Forms.CheckBox genSegments;
        private System.Windows.Forms.CheckedListBox segmentListMeasures;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.ComboBox targetLevelBox;
        private System.Windows.Forms.ComboBox targetDimBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox joinMeasureBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox screenDupMeasures;
        private System.Windows.Forms.TextBox segmentNumToSave;
        private System.Windows.Forms.CheckBox processSeparately;
        private System.Windows.Forms.CheckBox dropSegmentTable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox segmentOutputTableName;
        private System.Windows.Forms.ContextMenuStrip patternMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addPatternSearchDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removePatternSearchDefinitionToolStripMenuItem;
        private System.Windows.Forms.RadioButton peerSearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton populationSearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox referenceFilters;
        private System.Windows.Forms.CheckedListBox comparisonFilters;
        private System.Windows.Forms.RadioButton splitSearch;
        private System.Windows.Forms.TextBox refPercent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox patternsPerMeasure;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox SPreferencePopulationBox;
    }
}