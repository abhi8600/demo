using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class ModelParameterModule : Form, RepositoryModule
    {
        MainAdminForm ma;
        string moduleName = "Model Parameters";
        string categoryName = "System";
        public BestFitClassifierDialog classifierDialog;

        public ModelParameterModule(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.ModelParameters = new ModelParameters();
            try
            {
                r.ModelParameters.MaxComputationThreads = Int32.Parse(maxComputationThreads.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.MaxComputationThreads = 1;
            }
            r.ModelParameters.BulkLoadDirectory = bulkLoadDirectoryBox.Text;
            try
            {
                r.ModelParameters.MaxClustersPerPattern = Int32.Parse(maxClustersPerPattern.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.MaxClustersPerPattern = 5;
            }
            try
            {
                r.ModelParameters.MaxOneAttributeCategories = Int32.Parse(max1Categories.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.MaxOneAttributeCategories = 10;
            }
            r.ModelParameters.ClusterDensity = probMethodBox.Checked;
            try
            {
                r.ModelParameters.SampleSize = Int32.Parse(searchSample.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.SampleSize = -1;
            }
            try
            {
                r.ModelParameters.MinSamplesPerAttribute = Double.Parse(samplesPerAttribute.Text);
                r.ModelParameters.MaxAttributesPerModel = Int32.Parse(attributesPerModel.Text);
                r.ModelParameters.TargetReductionRate = Double.Parse(targetReduction.Text);
                r.ModelParameters.MaxAttributesPerFold = Int32.Parse(attributesPerFold.Text);
                r.ModelParameters.TargetCorrelationThreshold = Double.Parse(targetCorrelationThreshold.Text);
                r.ModelParameters.CrossCorrelationThreshold = Double.Parse(crossCorrelationThreshold.Text);
                r.ModelParameters.MaxARFFInstances = Int32.Parse(arffLimitTextBox.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.MinSamplesPerAttribute = 1.5;
                r.ModelParameters.MaxAttributesPerModel = 300;
                r.ModelParameters.TargetReductionRate = 0.65;
                r.ModelParameters.MaxAttributesPerFold = 500;
                r.ModelParameters.TargetCorrelationThreshold = 0.96;
                r.ModelParameters.CrossCorrelationThreshold = 0.92;
                r.ModelParameters.MaxARFFInstances = 5000;
            }

            r.ModelParameters.ModelBasedFiltering = decisionTreeFilteringCheckBox.Checked;
            r.ModelParameters.MapNullsToZero = mapNullsToZeroCheckBox.Checked;
            r.ModelParameters.UseOnlyExplanableClassificationModels = explanableModels.Checked;
            r.ModelParameters.UseCrossCorrelation = useCrossCorrelation.Checked;
            r.ModelParameters.UseWekaRemoveUseless = removeUseless.Checked;
            r.ModelParameters.UseRemoveRelated = removeRelatedCheckBox.Checked;
            r.ModelParameters.EnableAttributeSelection = attributeSelectionCheckBox.Checked;

            try
            {
                r.ModelParameters.MaxInstances = Int32.Parse(MaxInstances.Text);
            }
            catch (FormatException)
            {
                r.ModelParameters.MaxInstances = 500000;
            }

            //Optimizer Parameters
            r.OptimizerParameters = new OptimizerParameters();
            r.OptimizerParameters.Method = simplexMethod.Checked ? 0 : 1;
            try
            {
                r.OptimizerParameters.PopulationSize = Int32.Parse(populationSize.Text);
                r.OptimizerParameters.MaxGenerations = Int32.Parse(maxGenerations.Text);
                r.OptimizerParameters.MaxNoChangeGenerations = Int32.Parse(maxNoChange.Text);
                r.OptimizerParameters.MutationRate = Double.Parse(mutationRate.Text);
                r.OptimizerParameters.ReproductionRate = Double.Parse(reproductionRate.Text);
            }
            catch (FormatException)
            {
                r.OptimizerParameters.PopulationSize = 125;
                r.OptimizerParameters.MaxGenerations = 100;
                r.OptimizerParameters.MaxNoChangeGenerations = 12;
                r.OptimizerParameters.MutationRate = 1.5;
                r.OptimizerParameters.ReproductionRate = 1;
            }
            r.ModelParameters.Classifiers = classifierDialog.getClassifierSettings();
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.ModelParameters != null)
            {
                maxComputationThreads.Text = (r.ModelParameters.MaxComputationThreads == 0) ? "1" : r.ModelParameters.MaxComputationThreads.ToString();
                bulkLoadDirectoryBox.Text = r.ModelParameters.BulkLoadDirectory;
                maxClustersPerPattern.Text = (r.ModelParameters.MaxClustersPerPattern == 0) ? "5" : r.ModelParameters.MaxClustersPerPattern.ToString();
                max1Categories.Text = (r.ModelParameters.MaxOneAttributeCategories == 0) ? "10" : r.ModelParameters.MaxOneAttributeCategories.ToString();
                probMethodBox.Checked = r.ModelParameters.ClusterDensity;
                searchSample.Text = (r.ModelParameters.SampleSize == 0) ? "-1" : r.ModelParameters.SampleSize.ToString();
                samplesPerAttribute.Text = r.ModelParameters.MinSamplesPerAttribute == 0 ? "1.5" : r.ModelParameters.MinSamplesPerAttribute.ToString();
                attributesPerModel.Text = r.ModelParameters.MaxAttributesPerModel == 0 ? "300" : r.ModelParameters.MaxAttributesPerModel.ToString();
                targetReduction.Text = r.ModelParameters.TargetReductionRate == 0 ? "0.65" : r.ModelParameters.TargetReductionRate.ToString();
                attributesPerFold.Text = r.ModelParameters.MaxAttributesPerFold == 0 ? "500" : r.ModelParameters.MaxAttributesPerFold.ToString();
                decisionTreeFilteringCheckBox.Checked = r.ModelParameters.ModelBasedFiltering;
                mapNullsToZeroCheckBox.Checked = r.ModelParameters.MapNullsToZero;
                explanableModels.Checked = r.ModelParameters.UseOnlyExplanableClassificationModels;
                useCrossCorrelation.Checked = r.ModelParameters.UseCrossCorrelation;
                removeUseless.Checked = r.ModelParameters.UseWekaRemoveUseless;
                removeRelatedCheckBox.Checked = r.ModelParameters.UseRemoveRelated;
                attributeSelectionCheckBox.Checked = r.ModelParameters.EnableAttributeSelection;
                decisionTreeFilteringCheckBox.Checked = r.ModelParameters.ModelBasedFiltering;
                MaxInstances.Text = (r.ModelParameters.MaxInstances == 0) ? "500000" : r.ModelParameters.MaxInstances.ToString();
                arffLimitTextBox.Text = (r.ModelParameters.MaxARFFInstances == 0) ? "5000" : r.ModelParameters.MaxARFFInstances.ToString();
                targetCorrelationThreshold.Text = r.ModelParameters.TargetCorrelationThreshold == 0 ? "0.96" : r.ModelParameters.TargetCorrelationThreshold.ToString();
                crossCorrelationThreshold.Text = r.ModelParameters.CrossCorrelationThreshold == 0 ? "0.92" : r.ModelParameters.CrossCorrelationThreshold.ToString();
            }
            else
            {
                maxComputationThreads.Text = "1";
                bulkLoadDirectoryBox.Text = "";
                maxClustersPerPattern.Text = "5";
                max1Categories.Text = "10";
                searchSample.Text = "-1";
                samplesPerAttribute.Text = "1.5";
                attributesPerModel.Text = "300";
                targetReduction.Text = "0.65";
                attributesPerFold.Text = "500";
                MaxInstances.Text = "500000";
                arffLimitTextBox.Text = "5000";
                targetCorrelationThreshold.Text = "0.96";
                crossCorrelationThreshold.Text = "0.92";
                useCrossCorrelation.Checked = true;
                removeUseless.Checked = true;
                removeRelatedCheckBox.Checked = true;
                attributeSelectionCheckBox.Checked = true;
                decisionTreeFilteringCheckBox.Checked = true;
            }

            //Optimizer Parameters
            if (r != null && r.OptimizerParameters != null)
            {
                if (r.OptimizerParameters.Method == 0) simplexMethod.Checked = true;
                else if (r.OptimizerParameters.Method == 1) quasiNewtonMethod.Checked = true;
                populationSize.Text = r.OptimizerParameters.PopulationSize.ToString();
                maxGenerations.Text = r.OptimizerParameters.MaxGenerations.ToString();
                maxNoChange.Text = r.OptimizerParameters.MaxNoChangeGenerations.ToString();
                mutationRate.Text = r.OptimizerParameters.MutationRate.ToString();
                reproductionRate.Text = r.OptimizerParameters.ReproductionRate.ToString();
            }
            classifierDialog = new BestFitClassifierDialog(ma, r == null ? null : r.ModelParameters.Classifiers, false);
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return modelParamTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        private void defaultClassifiersButton_Click(object sender, EventArgs e)
        {
            classifierDialog.ShowDialog();
        }
    }
}