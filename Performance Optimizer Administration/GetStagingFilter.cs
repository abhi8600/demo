using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class GetStagingFilter : Form, ResizeMemoryForm
    {
        public string selectedFilter = "";
        private string windowName = "GetStagingFilter";
        MainAdminForm ma;


        public GetStagingFilter(MainAdminForm ma, string selectedFilter)
        {
            this.ma = ma;
            InitializeComponent();
            resetWindowFromSettings();
            List<StagingFilter> filters = ma.stagingFilterMod.getStagingFilters();
            filterBox.Items.Clear();
            if ((filters != null) && (filters.Count > 0))
            {
                foreach (StagingFilter sf in filters)
                {
                    filterBox.Items.Add(sf.Name);
                }
            }
            filterBox.Items.Add("<None>");
            if (filterBox.Items.Contains(selectedFilter))
            {
                filterBox.SelectedIndex = filterBox.Items.IndexOf(selectedFilter);
            }
            if (selectedFilter == null || selectedFilter == "")
            {
                filterBox.SelectedIndex = filterBox.Items.IndexOf("<None>");
            }
        }

        private void filterBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedFilter = (string)filterBox.SelectedItem;
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.Location, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void GetStagingFilter_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }
    }
}