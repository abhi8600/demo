﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using log4net.Repository.Hierarchy;
using log4net;

namespace Performance_Optimizer_Administration
{
    abstract public class SpaceConfigurationFile<T> : BaseObject where T : BaseAuditableObject 
    {
        protected static string DEV_EXTENSION = "_dev";
        protected static string LOCK_EXTENSION = ".lock";
        protected static string CONF_EXTENSION = ".xml";

        protected static string AUDIT = "[AUDIT] ";
        protected static string CREATE = "CREATE ";
        protected static string DELETE = "DELETE ";
        protected static string MODIFY = "MODIFY ";

        [XmlIgnore]
        protected DateTime? modifiedFileDate = null;

        [XmlIgnore]
        protected string repositoryDirectory;

        [XmlIgnore]
        protected ILog logger;

        [XmlIgnore]
        public List<T> variableList;


        public static string getFilename()
        {
            return "repository";
        }

        public SpaceConfigurationFile()
        {
            // necessary for serialization
        }

        public SpaceConfigurationFile(ILog l)
        {
            logger = l;
        }

        public virtual void updateRepositoryDirectory(string d, MainAdminForm maf)
        {
            if (repositoryDirectory != d)
            {
                this.repositoryDirectory = d;
                modifiedFileDate = null;
                refreshIfNecessary(maf);
            }
        }

        public Repository getLatestFromFile(string filename, MainAdminForm maf)
        {
            Repository r = null;
            if (repositoryDirectory == null)
                return null;
            string repositoryFilename = Path.Combine(repositoryDirectory, filename);
            Repository rep = maf.getOpenedRepository();
            if (repositoryFilename != null && File.Exists(repositoryFilename))
            {
                DateTime lastWriteTime = File.GetLastWriteTime(repositoryFilename);
                DateTime? modifiedTime = (modifiedFileDate == null && rep != null) ? rep.fileModifiedTime : modifiedFileDate;
                if (modifiedTime != null &&  lastWriteTime.CompareTo(modifiedTime) <= 0)
                    return rep;

                modifiedFileDate = File.GetLastWriteTime(repositoryFilename);
                StreamReader sreader = null;
                XmlReader xreader = null;
                try
                {
                    sreader = Util.getStreamReader(repositoryFilename);
                    xreader = Util.getSecureXMLReader(sreader);
                    XmlSerializer serializer = new XmlSerializer(Type.GetType("Performance_Optimizer_Administration.Repository"));

                    r = (Repository)serializer.Deserialize(xreader);
                    r.fileModifiedTime = modifiedFileDate;
                    maf.setOpenedRepository(r);
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    if (xreader != null)
                        xreader.Close();
                    if (sreader != null)
                        sreader.Close();
                }
            }
            r = maf.getOpenedRepository();

			// r is now repository
			getFromRepository(r);
            return r;
        }

		protected abstract void getFromRepository(object r);

        protected abstract void setToRepository(object r);

        public void saveToFile(string currentFilename, Repository r)
        {
            // assume that if r is null that we are building a new repository or some such
            // and that we shouldn't try to save now.  It will be explicitly saved later.
            if (r == null)
                return;

            Type type = r.GetType();
            currentFilename = this.repositoryDirectory + "/" + currentFilename;
            TextWriter writer = null;
            bool success = false;
            int count = 0;
            string tname = null;
            do
            {
                try
                {
                    tname = currentFilename + ".temp" + (count > 0 ? count.ToString() : "");
                    writer = new StreamWriter(tname);
                    success = true;
                }
                catch (Exception)
                {
                    System.Threading.Thread.Sleep(1000);
                    count++;
                }
            } while (!success && count < 10);
            if (!success || writer == null)
            {
                throw new Exception("Tried to create a StreamWriter for a temp repository file for " + currentFilename + ", but was unable to (gave up after 10 times)");
            }
            try
            {

                XmlSerializer serializer = new XmlSerializer(type);
                serializer.Serialize(writer, r);
                writer.Close();
                File.Copy(tname, currentFilename, true);
                File.Delete(tname);
                modifiedFileDate = File.GetLastWriteTime(currentFilename);
            }
            catch (Exception)
            {
                throw;
            }
        }


        protected virtual void copyFrom(SpaceConfigurationFile<T> scf)
        {
            variableList = scf.variableList;
            modifiedFileDate = scf.modifiedFileDate;
        }

        public void save(Repository r)
        {
            this.saveToFile(getFilename() + DEV_EXTENSION + ".xml", r);
        }

        public Repository refreshIfNecessary(MainAdminForm maf)
        {
            Repository r = maf.getOpenedRepository();
            if (maf.isInUberTransaction() == false)
            {
                r = this.getLatestFromFile(getFilename() + DEV_EXTENSION + CONF_EXTENSION, maf);

                if (r != null)
                {
                    this.getFromRepository(r);
                }
            }
            return r;
        }

		public void updateItem(T var, string username, MainAdminForm maf)
		{
            updateItem(var, username, var.GetType().Name, maf);
		}

        public static string getLockFileName(string repositoryDirectory)
        {
            return Path.Combine(repositoryDirectory, getFilename() + LOCK_EXTENSION);
        }

        public void updateItem(T var, string username, string label, MainAdminForm maf)
        {
            if (maf.isInUberTransaction() || var.lockThis(getLockFileName(repositoryDirectory)))
            {
                try
                {
                    updateItemWithoutLock(var, username, label, maf);
                    if (maf.isInUberTransaction() == false)
                        save(maf.getOpenedRepository());
                }
                finally
                {
                    var.unlockThis();
                }
            }
        }

        public void isNewOrUpToDate(T var, string label, MainAdminForm maf)
        {
            Repository r = refreshIfNecessary(maf);

            bool hasGuid = true;
            if (var.Guid == null || Guid.Empty == var.Guid)
                hasGuid = false;
            for (int i = 0; i < variableList.Count; i++)
            {
                T v = variableList[i];
                if ((!hasGuid && var.IsEqualTo(v)) ||
                    (hasGuid && var.Guid.Equals(v.Guid)))
                {
                    if (v.LastModifiedDate != null && var.LastModifiedDate == null)
                    {
                        string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername;
                        if (v.LastModifiedDate != null)
                        {
                            message = message + " at " + TimeZoneInfo.ConvertTimeFromUtc((DateTime)v.LastModifiedDate, TimeZoneInfo.Local);
                        }
                        if (logger != null)
                            logger.Error(message);
                        throw new RepositoryElementOutOfDateException(message);
                    }
                    if (v.LastModifiedDate != null)
                    {
                        if (compareTimes(var, v) > 0)
                        {
                            string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername;
                            if (v.LastModifiedDate != null)
                                message = message + " at " + TimeZoneInfo.ConvertTimeFromUtc((DateTime)v.LastModifiedDate, TimeZoneInfo.Local);
                            if (logger != null)
                                logger.Error(message);
                            throw new RepositoryElementOutOfDateException(message);
                        }
                    }
                    break;
                }
            }
        }

        public void updateItemWithoutLock(T var, string username, string label, MainAdminForm maf)
        {
            Repository r = refreshIfNecessary(maf);

            bool found = false;
            bool hasGuid = true;
            if (var.Guid == null || Guid.Empty == var.Guid)
                hasGuid = false;

            for (int i = 0; i < variableList.Count; i++)
            {
                T v = variableList[i];
                if ((!hasGuid && var.IsEqualTo(v)) ||
                    (hasGuid && var.Guid.Equals(v.Guid)))
                {
                    found = true;
                    if (maf.isInUberTransaction() == false && v.LastModifiedDate != null && var.LastModifiedDate == null)
                    {
						string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername;
						if (v.LastModifiedDate != null)
						{
							message = message + " at " + TimeZoneInfo.ConvertTimeFromUtc((DateTime)v.LastModifiedDate, TimeZoneInfo.Local);
						}
						if (logger != null)
                            logger.Error(message);
                        throw new RepositoryElementOutOfDateException(message);
                    }
                    if (maf.isInUberTransaction() == false && v.LastModifiedDate != null)
                    {
                        if (compareTimes(var, v) > 0)
                        {
							string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername;
							if (v.LastModifiedDate != null)
								message = message + " at " + TimeZoneInfo.ConvertTimeFromUtc((DateTime)v.LastModifiedDate, TimeZoneInfo.Local);
							if (logger != null)
                                logger.Error(message);
                            throw new RepositoryElementOutOfDateException(message);
                        }
                    }

                    var.LastModifiedDate = DateTime.UtcNow;
                    var.LastModifiedUsername = username;
                    if (var.Guid == null || var.Guid == Guid.Empty)
                        var.Guid = BaseAuditableObject.generateGuid();

                    // auditLogText = AUDIT + MODIFY + variableList[i].toAuditString();
                    variableList[i] = var;
                    break;
                }
            }

            if (!found)
            {
                // new variable - add to end
                var.LastModifiedDate = DateTime.UtcNow;
                var.LastModifiedUsername = username;
                var.CreatedDate = var.LastModifiedDate;
                var.CreatedUsername = username;
                var.Guid = BaseAuditableObject.generateGuid();

                // auditLogText = AUDIT + CREATE + var.toAuditString();

                variableList.Add(var);
            }

            setToRepository(r);
            maf.setOpenedRepository(r);
            return;
        }

        private long compareTimes(BaseAuditableObject var, BaseAuditableObject v)
        {
            // Flex does not keep all the ticks in the time, so we need to divide by 10000 ticks and do the comparison there. 
            DateTime varLastModifiedDate = var.LastModifiedDate ?? DateTime.MinValue;
            DateTime vLastModifiedDate = v.LastModifiedDate ?? DateTime.MinValue;
            long varLastModifiedTicks = varLastModifiedDate.Ticks / 10000;
            long vLastModifiedTicks = vLastModifiedDate.Ticks / 10000;
            // if variable in latest repository is later than (>) variable passed in
            return (vLastModifiedTicks - varLastModifiedTicks);
        }

        public void deleteItem(T var, MainAdminForm maf) {
			deleteItem(var, var.GetType().Name, maf);
		}

		public void deleteItem(T var, string label, MainAdminForm maf)
        {
            if (maf.isInUberTransaction() || var.lockThis(repositoryDirectory + getFilename() + LOCK_EXTENSION))
            {
                try
                {
                    Repository r = refreshIfNecessary(maf);

                    bool found = false;
                    bool hasGuid = true;
                    if (var.Guid == null || var.Guid == Guid.Empty)
                        hasGuid = false;
                    for (int i = 0; i < variableList.Count; i++)
                    {
                        T v = variableList[i];
                        if ((!hasGuid && var.IsEqualTo(v)) ||
                            (hasGuid && var.Guid.Equals(v.Guid)))
                        {
                            found = true;
                            if (maf.isInUberTransaction() == false && v.LastModifiedDate != null && var.LastModifiedDate == null)
                            {
                                string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername + " at " + v.LastModifiedDate;
                                if (logger != null)
                                    logger.Error(message);
                                throw new RepositoryElementOutOfDateException(message);
                            }
                            if (maf.isInUberTransaction() == false && v.LastModifiedDate != null)
                            {
                                if (compareTimes(var, v) > 0)
                                {
                                    string message = label + " " + var.Name + " has been updated by " + v.LastModifiedUsername + " at " + v.LastModifiedDate;
                                    if (logger != null)
                                        logger.Error(message);
                                    throw new RepositoryElementOutOfDateException(message);
                                }
                            }

                            // auditLogText = AUDIT + DELETE + v.toAuditString();
                            variableList.Remove(v);
                        }
                    }

                    if (found)
                    {
                        setToRepository(r);
                        if (maf.isInUberTransaction() == false)
                            save(r);
                        maf.setOpenedRepository(r);
                    }
                    else
                    {
                        string message = label + " " + var.Name + " could not be deleted as it is not found.";
                        if (logger != null)
                            logger.Error(message);
                    }
                }
                finally
                {
                    var.unlockThis();
                }

            }
        }

        public void Add(T item)
        {
            variableList.Add(item);
        }

        public void RemoveAt(int index)
        {
            variableList.RemoveAt(index);
        }

        public bool Remove(T item)
        {
            return variableList.Remove(item);
        }

        public int getCount()
        {
            return variableList.Count;
        }

        public T replaceItemAt(T item, int index)
        {
            T ret = variableList[index];
            variableList[index] = item;
            return ret;
        }

        public Boolean updateGuids()
        {
            Boolean ret = false;
            foreach (T var in variableList)
            {
                if (var.Guid == null || var.Guid == Guid.Empty)
                {
                    var.Guid = BaseAuditableObject.generateGuid();
                    ret = true;
                }
            }
            return ret;
        }
    }

    public class VariableList : SpaceConfigurationFile<Variable>
    {
        public Variable[] Variables
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
				if (value == null)
					variableList = new List<Variable>();
				else 
					variableList = new List<Variable>(value);
            }
        }


        public VariableList()
        {
            // necessary for serialization
        }

        public VariableList(ILog l) : base(l)
        {
            variableList = new List<Variable>();
        }

		protected override void getFromRepository(object r)
		{
			if (r is Repository)
			{
				Repository rep = (Repository)r;
				this.Variables = rep.Variables;
			}
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.Variables = this.Variables;
            }
        }
    }

    public class VirtualColumnList : SpaceConfigurationFile<VirtualColumn>
    {
        public VirtualColumn[] virtualColumns
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
				if (value == null)
					variableList = new List<VirtualColumn>();
				else 
					variableList= new List<VirtualColumn>(value);
            }
        }


        public VirtualColumnList(ILog l)
            : base(l)
        {
            variableList = new List<VirtualColumn>();
        }

        public VirtualColumnList() { }

		protected override void getFromRepository(object r)
		{
			if (r is Repository)
			{
				Repository rep = (Repository)r;
				virtualColumns = rep.VirtualColumns;
			}
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.VirtualColumns = virtualColumns;
            }
        }
    }

    public class LogicalExpressionList : SpaceConfigurationFile<LogicalExpression>
    {
        public LogicalExpression[] logicalExpressions
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
				if (value == null)
					variableList = new List<LogicalExpression>();
				else
					variableList = new List<LogicalExpression>(value);
            }
        }


        public LogicalExpressionList(ILog l)
            : base(l)
        {
            variableList = new List<LogicalExpression>();
        }

        public LogicalExpressionList() { }

		protected override void getFromRepository(object r)
		{
			if (r is Repository) 
				logicalExpressions = ((Repository)r).Expressions;
		}
        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.Expressions = logicalExpressions;
            }
        }
    }

    public class AggregateList : SpaceConfigurationFile<Aggregate>
    {
        public Aggregate[] aggregates
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
				if (value == null)
					variableList = new List<Aggregate>();
				else
					variableList = new List<Aggregate>(value);
            }
        }

        public AggregateList(ILog l)
            : base(l)
        {
            variableList = new List<Aggregate>();
        }

        public AggregateList() { }

		protected override void  getFromRepository(object r)
		{
			if (r is Repository)
				aggregates = ((Repository)r).Aggregates;
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.Aggregates = aggregates;
            }
        }
    }

    public class StagingTableList : SpaceConfigurationFile<StagingTable>
    {
        public StagingTable[] stagingTables
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
                if (value == null)
                    variableList = new List<StagingTable>();
                else
                    variableList = new List<StagingTable>(value);
            }
        }
        public StagingTableList(ILog l)
            : base(l)
        {
            variableList = new List<StagingTable>();
        }

        public StagingTableList() { }

		protected override void  getFromRepository(object r)
		{
			if (r is Repository)
                stagingTables = ((Repository)r).StagingTables;
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.StagingTables = stagingTables;
            }
        }
    }

    public class SourceFileList : SpaceConfigurationFile<SourceFile>
    {
        public SourceFile[] sourceFiles
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
                if (value == null)
                    variableList = new List<SourceFile>();
                else
                    variableList = new List<SourceFile>(value);
            }
        }
        public SourceFileList(ILog l)
            : base(l)
        {
            variableList = new List<SourceFile>();
        }

        public SourceFileList() { }

		protected override void  getFromRepository(object r)
		{
            if (r is Repository)
                sourceFiles = ((Repository)r).SourceFiles;
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.SourceFiles = sourceFiles;
            }
        }
    }

    public class HierarchyList : SpaceConfigurationFile<Hierarchy>
    {
        public Hierarchy[] hierarchies
        {
            get
            {
                return variableList.ToArray();
            }

            set
            {
                if (value == null)
                    variableList = new List<Hierarchy>();
                else
                    variableList = new List<Hierarchy>(value);
            }
        }
        public HierarchyList(ILog l)
            : base(l)
        {
            variableList = new List<Hierarchy>();
        }

        public HierarchyList() { }

		protected override void  getFromRepository(object r)
		{
            if (r is Repository)
                hierarchies = ((Repository)r).Hierarchies;
		}

        protected override void setToRepository(object r)
        {
            if (r is Repository)
            {
                Repository rep = (Repository)r;
                rep.Hierarchies = hierarchies;
            }
        }
    }

    // class used to refresh the repository and update params - requiresPublish
    public class ParamsList : SpaceConfigurationFile<Hierarchy>
    {
        List<Param> parameters;
        MainAdminForm maf;
        public ParamsList(MainAdminForm maf)
        {
            this.maf = maf;
            this.parameters = new List<Param>();
            this.parameters.Add(new Param("RequiresPublish"));
        }

        
        public void updatedParams(string paramName)
        {
            Param param = getParam(paramName);
            if (param == null)
            {
                param = new Param(paramName);
                param.CreatedDate = DateTime.Now;
                parameters.Add(param);
            }
            param.LastModifiedDate = DateTime.Now;
        }

        private Param getParam(string paramName)
        {
            if(parameters != null && parameters.Count > 0)
            {
                foreach(Param param in parameters)
                {
                    if(param.Name == paramName)
                    {
                        return param;
                    }
                }
            }
            return null;
        }

        public void refreshParams()
        {
            updateRepositoryDirectory(maf.getRepositoryDirectory(), maf);
            Repository r = refreshIfNecessary(maf);
            if (r!= null && parameters != null && parameters.Count > 0)
            {
                foreach (Param param in parameters)
                {
                    if (r.fileModifiedTime != null)
                    {
                        bool update = true;
                        if (param.LastModifiedDate != null)
                        {
                            DateTime d1 = (DateTime)r.fileModifiedTime;
                            DateTime d2 = (DateTime)param.LastModifiedDate;
                            if (d1.CompareTo(d2) < 0)
                            {
                                update = false;
                            }
                        }
                        if (update)
                        {
                            if (param.Name == "RequiresPublish")
                            {
                                maf.RequiresPublish = r.RequiresPublish;
                            }
                        }
                    }
                }
            }
        }

        protected override void getFromRepository(object r)
        {   
        }

        protected override void setToRepository(object r)
        {
        }
    }

    public class Param : BaseAuditableObject
    {
        public Param() { }

        public Param(string name)
        {
            this.Name = name;
        }
        public object value;
        public override string toAuditString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Name ");
            sb.Append(Name);
            return sb.ToString();
        }
    }
}
