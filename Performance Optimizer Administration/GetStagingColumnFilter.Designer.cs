namespace Performance_Optimizer_Administration
{
    partial class GetStagingColumnFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GetStagingColumnFilter));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filterBox = new System.Windows.Forms.ComboBox();
            this.dimensionBox = new System.Windows.Forms.ComboBox();
            this.dimensionLbl = new System.Windows.Forms.Label();
            this.levelBox = new System.Windows.Forms.ComboBox();
            this.levelLbl = new System.Windows.Forms.Label();
            this.dimensionLevelPanel = new System.Windows.Forms.Panel();
            this.addButton = new System.Windows.Forms.Button();
            this.filterList = new System.Windows.Forms.ListView();
            this.stagingFilter = new System.Windows.Forms.ColumnHeader();
            this.dimension = new System.Windows.Forms.ColumnHeader();
            this.level = new System.Windows.Forms.ColumnHeader();
            this.removeButton = new System.Windows.Forms.Button();
            this.dimensionLevelPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(131, 394);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(222, 394);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Staging Filter";
            // 
            // filterBox
            // 
            this.filterBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.filterBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterBox.FormattingEnabled = true;
            this.filterBox.Location = new System.Drawing.Point(12, 37);
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(385, 21);
            this.filterBox.Sorted = true;
            this.filterBox.TabIndex = 4;
            // 
            // dimensionBox
            // 
            this.dimensionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimensionBox.Location = new System.Drawing.Point(10, 85);
            this.dimensionBox.Name = "dimensionBox";
            this.dimensionBox.Size = new System.Drawing.Size(184, 21);
            this.dimensionBox.TabIndex = 109;
            this.dimensionBox.SelectedIndexChanged += new System.EventHandler(this.dimensionBox_SelectedIndexChanged);
            // 
            // dimensionLbl
            // 
            this.dimensionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dimensionLbl.Location = new System.Drawing.Point(8, 61);
            this.dimensionLbl.Name = "dimensionLbl";
            this.dimensionLbl.Size = new System.Drawing.Size(80, 23);
            this.dimensionLbl.TabIndex = 108;
            this.dimensionLbl.Text = "Dimension";
            // 
            // levelBox
            // 
            this.levelBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.levelBox.Location = new System.Drawing.Point(211, 85);
            this.levelBox.Name = "levelBox";
            this.levelBox.Size = new System.Drawing.Size(184, 21);
            this.levelBox.TabIndex = 111;
            // 
            // levelLbl
            // 
            this.levelLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.levelLbl.Location = new System.Drawing.Point(207, 61);
            this.levelLbl.Name = "levelLbl";
            this.levelLbl.Size = new System.Drawing.Size(80, 23);
            this.levelLbl.TabIndex = 110;
            this.levelLbl.Text = "Level";
            // 
            // dimensionLevelPanel
            // 
            this.dimensionLevelPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionLevelPanel.Controls.Add(this.addButton);
            this.dimensionLevelPanel.Controls.Add(this.levelBox);
            this.dimensionLevelPanel.Controls.Add(this.filterBox);
            this.dimensionLevelPanel.Controls.Add(this.levelLbl);
            this.dimensionLevelPanel.Controls.Add(this.dimensionLbl);
            this.dimensionLevelPanel.Controls.Add(this.label1);
            this.dimensionLevelPanel.Controls.Add(this.dimensionBox);
            this.dimensionLevelPanel.Location = new System.Drawing.Point(12, 12);
            this.dimensionLevelPanel.Name = "dimensionLevelPanel";
            this.dimensionLevelPanel.Size = new System.Drawing.Size(404, 156);
            this.dimensionLevelPanel.TabIndex = 112;
            // 
            // addButton
            // 
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Location = new System.Drawing.Point(11, 121);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 112;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // filterList
            // 
            this.filterList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filterList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.stagingFilter,
            this.dimension,
            this.level});
            this.filterList.FullRowSelect = true;
            this.filterList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.filterList.Location = new System.Drawing.Point(12, 235);
            this.filterList.MultiSelect = false;
            this.filterList.Name = "filterList";
            this.filterList.Size = new System.Drawing.Size(404, 140);
            this.filterList.TabIndex = 113;
            this.filterList.UseCompatibleStateImageBehavior = false;
            this.filterList.View = System.Windows.Forms.View.Details;
            // 
            // stagingFilter
            // 
            this.stagingFilter.Text = "Staging Filter";
            this.stagingFilter.Width = 124;
            // 
            // dimension
            // 
            this.dimension.Text = "Dimension";
            this.dimension.Width = 149;
            // 
            // level
            // 
            this.level.Text = "Level";
            this.level.Width = 125;
            // 
            // removeButton
            // 
            this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeButton.Location = new System.Drawing.Point(341, 206);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 114;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // GetStagingColumnFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 429);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.filterList);
            this.Controls.Add(this.dimensionLevelPanel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GetStagingColumnFilter";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GetStagingFilter";
            this.dimensionLevelPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox filterBox;
        private System.Windows.Forms.ComboBox dimensionBox;
        private System.Windows.Forms.Label dimensionLbl;
        private System.Windows.Forms.ComboBox levelBox;
        private System.Windows.Forms.Label levelLbl;
        private System.Windows.Forms.Panel dimensionLevelPanel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListView filterList;
        private System.Windows.Forms.ColumnHeader stagingFilter;
        private System.Windows.Forms.ColumnHeader dimension;
        private System.Windows.Forms.ColumnHeader level;
        private System.Windows.Forms.Button removeButton;
    }
}