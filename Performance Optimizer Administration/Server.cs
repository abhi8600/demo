using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
namespace Performance_Optimizer_Administration
{
    class Server
    {
        private static bool firstaccess = true;

        private static void login()
        {
        }

        public static List<string> getTables(string connection, string serverURL, string catalog, string username, string password)
        {
            ServicePointManager.ServerCertificateValidationCallback += 
            delegate(object senderObj, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            SMI.CommandProcessor cps = new SMI.CommandProcessor();
            try
            {
                String token = Util.getAuthenticationToken(serverURL, username);
                cps.Url = serverURL + "/services/CommandProcessor.CommandProcessorHttpSoap11Endpoint?ssoUser=" + token;                
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to connect to server to retrieve DB tablenames: is your Server URL set correctly?", "GetTables");
                return (new List<string>());
            }
            WaitingDialog wd = null;
            if (firstaccess)
            {
                wd = new WaitingDialog();
                wd.Text = "Please Wait";
                wd.label.Text = "Connecting to server";
                wd.Show();
                wd.Update();
            }
            string tables = null;
            try
            {
                tables = cps.getTables(username, password, connection, catalog);
                if (tables == null)
                {
                    throw new Exception("No tables returned");
                }
                if (tables.Equals("ERROR:com.microsoft.sqlserver.jdbc.SQLServerException: The database name component of the object qualifier must be the name of the current database."))
                {
                    return null;
                }
                if (tables.StartsWith("ERROR:"))
                {
                    throw new Exception(tables);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to connect to server to retrieve tables: " + e.ToString());
                if (wd != null)
                    wd.Close();
                return (new List<string>());
            }
            List<string> results = new List<string>();
            Boolean done = false;
            string line = null;
            while(!done)
            {
                int idx = tables.IndexOf('\n');
                if (idx >= 0)
                {
                    line = tables.Substring(0, tables.IndexOf('\n'));
                }
                else
                {
                    line = tables;
                    done = true;
                }
                tables = tables.Substring(tables.IndexOf('\n') + 1);
                if (!line.StartsWith("ERROR:"))
                {
                    int pos = line.IndexOf('\t');
                    if (pos >= 0)
                        line = line.Substring(pos + 1);
                    results.Add(line);
                }
                else
                {
                    MessageBox.Show("Error connecting to server: " + line.Substring(7));
                    if (wd != null)
                        wd.Close();
                    return (null);
                }
            }
            if (wd != null)
                wd.Close();
            firstaccess = false;
            return (results);
        }

        public static List<string[]> getCatalogs(String connection, string serverURL, string username, string password)
        {
            if (serverURL == null || serverURL == "" || serverURL == "http://")
            {
                MessageBox.Show("No Server URL has been specified");
                return (new List<string[]>());
            }

            ServicePointManager.ServerCertificateValidationCallback +=
            delegate(object senderObj, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            String token = Util.getAuthenticationToken(serverURL, username);
            SMI.CommandProcessor cps = new SMI.CommandProcessor();
            cps.Url = serverURL + "/services/CommandProcessor.CommandProcessorHttpSoap11Endpoint?ssoUser=" + token;

            WaitingDialog wd = null;
            if (firstaccess)
            {
                wd = new WaitingDialog();
                wd.Text = "Please Wait";
                wd.label.Text = "Connecting to server";
                wd.Show();
                wd.Update();
            }
            string catalogs;
            try
            {
                catalogs = cps.getCatalogs(username, password, connection);
                if (catalogs == null)
                {
                    throw new Exception("No catalogs returned");
                }
                if (catalogs.StartsWith("ERROR:"))
                {
                    throw new Exception(catalogs);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to connect to server to retrieve catalogs: "+e.ToString());
                if (wd != null)
                    wd.Close();
                return (new List<string[]>());
            }
            string line = null;
            Boolean done = false;
            List<string[]> results = new List<string[]>();
            while(!done)
            {
                int idx = catalogs.IndexOf('\n');
                if (idx >= 0)
                {
                    line = catalogs.Substring(0, catalogs.IndexOf('\n'));
                }
                else
                {
                    line = catalogs;
                    done = true;
                }
                catalogs = catalogs.Substring(catalogs.IndexOf('\n') + 1);
                if (line.StartsWith("ERROR:"))
                {
                    MessageBox.Show("Error connecting to server: " + line.Substring(7));
                    if (wd != null)
                        wd.Close();
                    return (null);
                }
                int pos = line.IndexOf('\t');
                if (pos >= 0)
                {
                    string[] result = new string[2];
                    result[0] = line.Substring(0, pos);
                    result[1] = line.Substring(pos + 1);
                    results.Add(result);
                }
            }
            if (wd != null)
                wd.Close();
            firstaccess = false;
            return (results);
        }

    }
}
