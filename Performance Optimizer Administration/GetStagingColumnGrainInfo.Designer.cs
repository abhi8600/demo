namespace Performance_Optimizer_Administration
{
    partial class GetStagingColumnGrainInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridView setGrid;
            System.Windows.Forms.DataGridView mapGrid;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GetStagingColumnGrainInfo));
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Set = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SetObject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filterBox = new System.Windows.Forms.ComboBox();
            this.dimensionBox = new System.Windows.Forms.ComboBox();
            this.dimensionLbl = new System.Windows.Forms.Label();
            this.levelBox = new System.Windows.Forms.ComboBox();
            this.levelLbl = new System.Windows.Forms.Label();
            this.dimensionLevelPanel = new System.Windows.Forms.Panel();
            this.nameTypeBox = new System.Windows.Forms.ComboBox();
            this.defaultLabel = new System.Windows.Forms.Label();
            this.defaultBox = new System.Windows.Forms.TextBox();
            this.aggBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.columnNameBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.mapPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.addSetButton = new System.Windows.Forms.Button();
            this.orderedListPanel = new System.Windows.Forms.Panel();
            this.orderedListGrid = new System.Windows.Forms.DataGridView();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aggDataSet = new System.Data.DataSet();
            this.setTable = new System.Data.DataTable();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.itemsTable = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.grainInfoList = new System.Windows.Forms.ListView();
            this.dimension = new System.Windows.Forms.ColumnHeader();
            this.level = new System.Windows.Forms.ColumnHeader();
            this.stagingFilter = new System.Windows.Forms.ColumnHeader();
            this.aggregation = new System.Windows.Forms.ColumnHeader();
            this.columnPrefix = new System.Windows.Forms.ColumnHeader();
            this.removeButton = new System.Windows.Forms.Button();
            this.columnLabel = new System.Windows.Forms.Label();
            setGrid = new System.Windows.Forms.DataGridView();
            mapGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(setGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(mapGrid)).BeginInit();
            this.dimensionLevelPanel.SuspendLayout();
            this.mapPanel.SuspendLayout();
            this.orderedListPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderedListGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // setGrid
            // 
            setGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            setGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            setGrid.ColumnHeadersVisible = false;
            setGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            setGrid.Location = new System.Drawing.Point(528, 31);
            setGrid.Name = "setGrid";
            setGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            setGrid.Size = new System.Drawing.Size(176, 163);
            setGrid.TabIndex = 128;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn2.HeaderText = "Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // mapGrid
            // 
            mapGrid.AllowUserToAddRows = false;
            mapGrid.AllowUserToResizeRows = false;
            mapGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            mapGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            mapGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Set,
            this.dataGridViewTextBoxColumn1,
            this.SetObject});
            mapGrid.Location = new System.Drawing.Point(3, 31);
            mapGrid.Name = "mapGrid";
            mapGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            mapGrid.Size = new System.Drawing.Size(443, 163);
            mapGrid.TabIndex = 127;
            // 
            // Set
            // 
            this.Set.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Set.DataPropertyName = "SetDisplay";
            this.Set.HeaderText = "Set";
            this.Set.Name = "Set";
            this.Set.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Value";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // SetObject
            // 
            this.SetObject.DataPropertyName = "SetObject";
            this.SetObject.HeaderText = "SetObject";
            this.SetObject.Name = "SetObject";
            this.SetObject.ReadOnly = true;
            this.SetObject.Visible = false;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(760, 528);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Location = new System.Drawing.Point(841, 528);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label1.Location = new System.Drawing.Point(3, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Staging Filter";
            // 
            // filterBox
            // 
            this.filterBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.filterBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterBox.FormattingEnabled = true;
            this.filterBox.Location = new System.Drawing.Point(89, 72);
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(310, 21);
            this.filterBox.Sorted = true;
            this.filterBox.TabIndex = 4;
            // 
            // dimensionBox
            // 
            this.dimensionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dimensionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimensionBox.Location = new System.Drawing.Point(89, 5);
            this.dimensionBox.Name = "dimensionBox";
            this.dimensionBox.Size = new System.Drawing.Size(173, 21);
            this.dimensionBox.TabIndex = 109;
            this.dimensionBox.SelectedIndexChanged += new System.EventHandler(this.dimensionBox_SelectedIndexChanged);
            // 
            // dimensionLbl
            // 
            this.dimensionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dimensionLbl.Location = new System.Drawing.Point(3, 6);
            this.dimensionLbl.Name = "dimensionLbl";
            this.dimensionLbl.Size = new System.Drawing.Size(80, 23);
            this.dimensionLbl.TabIndex = 108;
            this.dimensionLbl.Text = "Dimension";
            // 
            // levelBox
            // 
            this.levelBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.levelBox.Location = new System.Drawing.Point(89, 32);
            this.levelBox.Name = "levelBox";
            this.levelBox.Size = new System.Drawing.Size(173, 21);
            this.levelBox.TabIndex = 111;
            // 
            // levelLbl
            // 
            this.levelLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.levelLbl.Location = new System.Drawing.Point(3, 33);
            this.levelLbl.Name = "levelLbl";
            this.levelLbl.Size = new System.Drawing.Size(80, 23);
            this.levelLbl.TabIndex = 110;
            this.levelLbl.Text = "Level";
            // 
            // dimensionLevelPanel
            // 
            this.dimensionLevelPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionLevelPanel.Controls.Add(this.nameTypeBox);
            this.dimensionLevelPanel.Controls.Add(this.defaultLabel);
            this.dimensionLevelPanel.Controls.Add(this.defaultBox);
            this.dimensionLevelPanel.Controls.Add(this.aggBox);
            this.dimensionLevelPanel.Controls.Add(this.label4);
            this.dimensionLevelPanel.Controls.Add(this.columnNameBox);
            this.dimensionLevelPanel.Controls.Add(this.addButton);
            this.dimensionLevelPanel.Controls.Add(this.levelBox);
            this.dimensionLevelPanel.Controls.Add(this.filterBox);
            this.dimensionLevelPanel.Controls.Add(this.levelLbl);
            this.dimensionLevelPanel.Controls.Add(this.dimensionLbl);
            this.dimensionLevelPanel.Controls.Add(this.label1);
            this.dimensionLevelPanel.Controls.Add(this.dimensionBox);
            this.dimensionLevelPanel.Controls.Add(this.mapPanel);
            this.dimensionLevelPanel.Controls.Add(this.orderedListPanel);
            this.dimensionLevelPanel.Location = new System.Drawing.Point(12, 32);
            this.dimensionLevelPanel.Name = "dimensionLevelPanel";
            this.dimensionLevelPanel.Size = new System.Drawing.Size(904, 366);
            this.dimensionLevelPanel.TabIndex = 112;
            // 
            // nameTypeBox
            // 
            this.nameTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nameTypeBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nameTypeBox.Items.AddRange(new object[] {
            "Column Prefix",
            "New Name"});
            this.nameTypeBox.Location = new System.Drawing.Point(413, 6);
            this.nameTypeBox.Name = "nameTypeBox";
            this.nameTypeBox.Size = new System.Drawing.Size(121, 21);
            this.nameTypeBox.TabIndex = 136;
            // 
            // defaultLabel
            // 
            this.defaultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.defaultLabel.Location = new System.Drawing.Point(318, 105);
            this.defaultLabel.Name = "defaultLabel";
            this.defaultLabel.Size = new System.Drawing.Size(89, 22);
            this.defaultLabel.TabIndex = 135;
            this.defaultLabel.Text = "Default Value";
            // 
            // defaultBox
            // 
            this.defaultBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.defaultBox.Location = new System.Drawing.Point(413, 105);
            this.defaultBox.Name = "defaultBox";
            this.defaultBox.Size = new System.Drawing.Size(164, 20);
            this.defaultBox.TabIndex = 134;
            // 
            // aggBox
            // 
            this.aggBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.aggBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aggBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aggBox.FormattingEnabled = true;
            this.aggBox.Items.AddRange(new object[] {
            "",
            "Max",
            "Min",
            "Sum",
            "Avg",
            "Count",
            "Count Distinct",
            "Map",
            "Ordered List"});
            this.aggBox.Location = new System.Drawing.Point(89, 104);
            this.aggBox.Name = "aggBox";
            this.aggBox.Size = new System.Drawing.Size(223, 21);
            this.aggBox.TabIndex = 131;
            this.aggBox.SelectedIndexChanged += new System.EventHandler(this.aggBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 22);
            this.label4.TabIndex = 130;
            this.label4.Text = "Aggregation";
            // 
            // columnNameBox
            // 
            this.columnNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.columnNameBox.Location = new System.Drawing.Point(540, 7);
            this.columnNameBox.Name = "columnNameBox";
            this.columnNameBox.Size = new System.Drawing.Size(357, 20);
            this.columnNameBox.TabIndex = 114;
            // 
            // addButton
            // 
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Location = new System.Drawing.Point(822, 334);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 112;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // mapPanel
            // 
            this.mapPanel.Controls.Add(mapGrid);
            this.mapPanel.Controls.Add(this.label3);
            this.mapPanel.Controls.Add(this.addSetButton);
            this.mapPanel.Controls.Add(setGrid);
            this.mapPanel.Location = new System.Drawing.Point(89, 131);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(808, 197);
            this.mapPanel.TabIndex = 133;
            this.mapPanel.Visible = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(808, 19);
            this.label3.TabIndex = 126;
            this.label3.Text = "Enter sets of values and their corresponding mapped value. Enter a set of values " +
                "on the right and then press \"Add Set\". Then map that set to a value on the left." +
                "";
            // 
            // addSetButton
            // 
            this.addSetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addSetButton.Location = new System.Drawing.Point(710, 31);
            this.addSetButton.Name = "addSetButton";
            this.addSetButton.Size = new System.Drawing.Size(75, 23);
            this.addSetButton.TabIndex = 129;
            this.addSetButton.Text = "Add Set";
            this.addSetButton.UseVisualStyleBackColor = true;
            this.addSetButton.Click += new System.EventHandler(this.addSetButton_Click);
            // 
            // orderedListPanel
            // 
            this.orderedListPanel.Controls.Add(this.orderedListGrid);
            this.orderedListPanel.Controls.Add(this.label2);
            this.orderedListPanel.Location = new System.Drawing.Point(89, 131);
            this.orderedListPanel.Name = "orderedListPanel";
            this.orderedListPanel.Size = new System.Drawing.Size(808, 197);
            this.orderedListPanel.TabIndex = 132;
            this.orderedListPanel.Visible = false;
            // 
            // orderedListGrid
            // 
            this.orderedListGrid.AutoGenerateColumns = false;
            this.orderedListGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.orderedListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderedListGrid.ColumnHeadersVisible = false;
            this.orderedListGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.valueDataGridViewTextBoxColumn});
            this.orderedListGrid.DataMember = "SetTable";
            this.orderedListGrid.DataSource = this.aggDataSet;
            this.orderedListGrid.Location = new System.Drawing.Point(3, 27);
            this.orderedListGrid.Name = "orderedListGrid";
            this.orderedListGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.orderedListGrid.Size = new System.Drawing.Size(280, 124);
            this.orderedListGrid.TabIndex = 125;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            // 
            // aggDataSet
            // 
            this.aggDataSet.DataSetName = "NewDataSet";
            this.aggDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.setTable,
            this.itemsTable});
            // 
            // setTable
            // 
            this.setTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4});
            this.setTable.TableName = "SetTable";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "SetDisplay";
            this.dataColumn2.DefaultValue = "";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "SetObject";
            this.dataColumn3.DataType = typeof(object);
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "Value";
            this.dataColumn4.DefaultValue = "";
            // 
            // itemsTable
            // 
            this.itemsTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1});
            this.itemsTable.TableName = "ItemsTable";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Value";
            this.dataColumn1.DefaultValue = "";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(808, 19);
            this.label2.TabIndex = 124;
            this.label2.Text = "Enter a list of values. Values will be selected from the group in the order in wh" +
                "ich they are listed.";
            // 
            // grainInfoList
            // 
            this.grainInfoList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grainInfoList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dimension,
            this.level,
            this.stagingFilter,
            this.aggregation,
            this.columnPrefix});
            this.grainInfoList.FullRowSelect = true;
            this.grainInfoList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.grainInfoList.HideSelection = false;
            this.grainInfoList.Location = new System.Drawing.Point(12, 404);
            this.grainInfoList.MultiSelect = false;
            this.grainInfoList.Name = "grainInfoList";
            this.grainInfoList.Size = new System.Drawing.Size(673, 147);
            this.grainInfoList.TabIndex = 113;
            this.grainInfoList.UseCompatibleStateImageBehavior = false;
            this.grainInfoList.View = System.Windows.Forms.View.Details;
            this.grainInfoList.SelectedIndexChanged += new System.EventHandler(this.grainInfoList_SelectedIndexChanged);
            // 
            // dimension
            // 
            this.dimension.Text = "Dimension";
            this.dimension.Width = 149;
            // 
            // level
            // 
            this.level.Text = "Level";
            this.level.Width = 125;
            // 
            // stagingFilter
            // 
            this.stagingFilter.Text = "Staging Filter";
            this.stagingFilter.Width = 124;
            // 
            // aggregation
            // 
            this.aggregation.Text = "Aggregation";
            this.aggregation.Width = 135;
            // 
            // columnPrefix
            // 
            this.columnPrefix.Text = "Column Prefix";
            this.columnPrefix.Width = 114;
            // 
            // removeButton
            // 
            this.removeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeButton.Location = new System.Drawing.Point(691, 404);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(59, 23);
            this.removeButton.TabIndex = 114;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // columnLabel
            // 
            this.columnLabel.AutoSize = true;
            this.columnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnLabel.Location = new System.Drawing.Point(12, 9);
            this.columnLabel.Name = "columnLabel";
            this.columnLabel.Size = new System.Drawing.Size(63, 17);
            this.columnLabel.TabIndex = 115;
            this.columnLabel.Text = "Column: ";
            // 
            // GetStagingColumnGrainInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 563);
            this.Controls.Add(this.columnLabel);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.grainInfoList);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.dimensionLevelPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GetStagingColumnGrainInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grain-specific Information";
            ((System.ComponentModel.ISupportInitialize)(setGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(mapGrid)).EndInit();
            this.dimensionLevelPanel.ResumeLayout(false);
            this.dimensionLevelPanel.PerformLayout();
            this.mapPanel.ResumeLayout(false);
            this.orderedListPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.orderedListGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aggDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox filterBox;
        private System.Windows.Forms.ComboBox dimensionBox;
        private System.Windows.Forms.Label dimensionLbl;
        private System.Windows.Forms.ComboBox levelBox;
        private System.Windows.Forms.Label levelLbl;
        private System.Windows.Forms.Panel dimensionLevelPanel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListView grainInfoList;
        private System.Windows.Forms.ColumnHeader stagingFilter;
        private System.Windows.Forms.ColumnHeader dimension;
        private System.Windows.Forms.ColumnHeader level;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.TextBox columnNameBox;
        private System.Windows.Forms.ColumnHeader columnPrefix;
        private System.Windows.Forms.Button addSetButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView orderedListGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox aggBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel mapPanel;
        private System.Windows.Forms.Panel orderedListPanel;
        private System.Windows.Forms.ColumnHeader aggregation;
        private System.Data.DataSet aggDataSet;
        private System.Data.DataTable setTable;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Data.DataTable itemsTable;
        private System.Data.DataColumn dataColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.TextBox defaultBox;
        private System.Windows.Forms.Label defaultLabel;
        private System.Windows.Forms.Label columnLabel;
        private System.Windows.Forms.ComboBox nameTypeBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Set;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SetObject;
    }
}