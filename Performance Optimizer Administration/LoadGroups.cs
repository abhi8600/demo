using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class LoadGroups : Form, RepositoryModule, RenameNode
    {
        string moduleName = "Load Groups";
        string categoryName = "Warehouse";
        MainAdminForm ma;
        TreeNode rootNode;
        public List<LoadGroup> groups = new List<LoadGroup>();

        public LoadGroups(MainAdminForm ma)
        {
            this.ma = ma;
            InitializeComponent();
        }

        public void updateDimensionList()
        {
            dimensionComboBox.Items.Clear();
            List<String> dimList = ma.getDimensionList();
            dimList.Sort();
            foreach (string dim in dimList)
            {
                dimensionComboBox.Items.Add(dim);
            }
        }

        public void updateDimensionColumnList(string selectedDimension)
        {
            columnComboBox.Items.Clear();
            List<string> dimColList = ma.getDimensionColumnList(selectedDimension);
            dimColList.Sort();
            foreach (string col in dimColList)
            {
                columnComboBox.Items.Add(col);
            }
        }

        public List<LoadGroup> getLoadGroups()
        {
            return (groups);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.LoadGroups = groups.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.LoadGroups != null)
                groups = new List<LoadGroup>(r.LoadGroups);
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {
            updateDimensionList();
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (LoadGroup lg in groups)
            {
                TreeNode tn = new TreeNode(lg.Name);
                tn.ImageIndex = 28;
                tn.SelectedImageIndex = 28;
                tn.Tag = lg;
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode curNode = null;

        public bool selectedNode(TreeNode node)
        {
            if (curNode != null) implicitSave();
            ma.mainTree.ContextMenuStrip = loadGroupMenuStrip;
            if (node.Parent.Parent == null)
            {
                loadGroupGridView.Visible = false;
                loadGroupsPanel.Visible = false;
                curNode = null;
                removeLoadGroupToolStripMenuItem.Visible = false;
            }
            else
            {
                if (node.Index < groups.Count)
                {
                    loadGroupGridView.Visible = true;
                    removeLoadGroupToolStripMenuItem.Visible = true;
                    loadGroupsPanel.Visible = true;
                    LoadGroup lg = (LoadGroup)node.Tag;
                    DataTable dat = groups[node.Index].getTable();
                    loadGroupGridView.DataSource = dat;
                    curNode = node;
                }
                else
                    curNode = null;
            }
            return true;
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
            if (curNode != null)
            {
                LoadGroup lg = (LoadGroup)curNode.Tag;
                lg.updateFromDataTable((DataTable)loadGroupGridView.DataSource);
                lg.Name = curNode.Text;
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return loadGroupsPage;
            }
            set
            {
            }
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        private void newLoadGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode tn = new TreeNode("New Load Group");
            tn.ImageIndex = 28;
            tn.SelectedImageIndex = 28;
            rootNode.Nodes.Add(tn);
            LoadGroup lg = new LoadGroup("New Load Group");
            tn.Tag = lg;
            groups.Add(lg);
        }

        private void removeLoadGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode != null && curNode.Parent == rootNode)
            {
                int index = curNode.Index;
                rootNode.Nodes.RemoveAt(index);
                groups.RemoveAt(index);
            }
        }

        private void dimensionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateDimensionColumnList((string)dimensionComboBox.SelectedItem);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)loadGroupGridView.DataSource;
            DataRow dr = dt.NewRow();
            dr["DimensionName"] = (string)dimensionComboBox.SelectedItem;
            dr["DimensionColumn"] = (string)columnComboBox.SelectedItem;
            dr["Operator"] = (string)operatorComboBox.SelectedItem;
            dr["Value"] = valueTextBox.Text;
            dt.Rows.Add(dr);
            dimensionComboBox.Text = columnComboBox.Text = operatorComboBox.Text = valueTextBox.Text = "";
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode && curNode != null)
            {
                curNode.Text = e.Label;
                groups[curNode.Index].Name = e.Label;
            }
        }

        #endregion

        private void removeButton_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRows = loadGroupGridView.SelectedRows;
            if (selectedRows != null && selectedRows.Count > 0)
            {
                loadGroupGridView.Enabled = false;
                foreach (DataGridViewRow row in selectedRows)
                {
                    loadGroupGridView.Rows.Remove(row);
                }
                loadGroupGridView.Enabled = true;
            }
        }

        private void loadGroupGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            loadGroupGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}