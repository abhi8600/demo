namespace Performance_Optimizer_Administration
{
    partial class SuccessModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.modelTabPage = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.selectSecondAttributes = new System.Windows.Forms.Button();
            this.selectFirstAttributes = new System.Windows.Forms.Button();
            this.selectPerformanceMeasures = new System.Windows.Forms.Button();
            this.selectedPatterns = new System.Windows.Forms.ListView();
            this.columnHeader19 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader20 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader21 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader22 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.removeAttribButton = new System.Windows.Forms.Button();
            this.availPatternMeasures = new System.Windows.Forms.ListBox();
            this.availPatternAttribute1 = new System.Windows.Forms.ListBox();
            this.availPatternAttribute2 = new System.Windows.Forms.ListBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.totalPatternLabel = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.modelFlag = new System.Windows.Forms.CheckBox();
            this.label124 = new System.Windows.Forms.Label();
            this.maxPatterns = new System.Windows.Forms.TextBox();
            this.actionableFlag = new System.Windows.Forms.CheckBox();
            this.setPatternFlags = new System.Windows.Forms.Button();
            this.proportionFlag = new System.Windows.Forms.CheckBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.addAttribButton = new System.Windows.Forms.Button();
            this.addMeasureOnlyButton = new System.Windows.Forms.Button();
            this.add2AttribButton = new System.Windows.Forms.Button();
            this.filterValidPatterns = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.oneModelButton = new System.Windows.Forms.RadioButton();
            this.clusterBreakButton = new System.Windows.Forms.RadioButton();
            this.attributeBreakButton = new System.Windows.Forms.RadioButton();
            this.breakAttribute = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.partitionAttributeBox = new System.Windows.Forms.ComboBox();
            this.availAttributeListBox = new System.Windows.Forms.CheckedListBox();
            this.iterateLevelBox = new System.Windows.Forms.ComboBox();
            this.iterateDimBox = new System.Windows.Forms.ComboBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.targetLevelBox = new System.Windows.Forms.ComboBox();
            this.targetDimBox = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.targetMeasureBox = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.aggregateCheckBox = new System.Windows.Forms.CheckBox();
            this.runnableCheckBox = new System.Windows.Forms.CheckBox();
            this.exportModelButton = new System.Windows.Forms.Button();
            this.groupMeasureOnly = new System.Windows.Forms.CheckBox();
            this.label179 = new System.Windows.Forms.Label();
            this.inheritSuccessModel = new System.Windows.Forms.ComboBox();
            this.filterNullValuesBox = new System.Windows.Forms.CheckBox();
            this.modelFilterBox = new System.Windows.Forms.CheckedListBox();
            this.label60 = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.successModelMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newSuccessModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeSuccessModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.modelTabPage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.successModelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.modelTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // modelTabPage
            // 
            this.modelTabPage.AutoScroll = true;
            this.modelTabPage.Controls.Add(this.groupBox9);
            this.modelTabPage.Controls.Add(this.groupBox10);
            this.modelTabPage.Controls.Add(this.groupBox11);
            this.modelTabPage.Location = new System.Drawing.Point(4, 22);
            this.modelTabPage.Name = "modelTabPage";
            this.modelTabPage.Size = new System.Drawing.Size(844, 707);
            this.modelTabPage.TabIndex = 5;
            this.modelTabPage.Text = "Model Datasets";
            this.modelTabPage.UseVisualStyleBackColor = true;
            this.modelTabPage.Leave += new System.EventHandler(this.modelTabPage_Leave);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.selectSecondAttributes);
            this.groupBox9.Controls.Add(this.selectFirstAttributes);
            this.groupBox9.Controls.Add(this.selectPerformanceMeasures);
            this.groupBox9.Controls.Add(this.selectedPatterns);
            this.groupBox9.Controls.Add(this.removeAttribButton);
            this.groupBox9.Controls.Add(this.availPatternMeasures);
            this.groupBox9.Controls.Add(this.availPatternAttribute1);
            this.groupBox9.Controls.Add(this.availPatternAttribute2);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.label14);
            this.groupBox9.Controls.Add(this.label13);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.totalPatternLabel);
            this.groupBox9.Controls.Add(this.groupBox22);
            this.groupBox9.Controls.Add(this.groupBox28);
            this.groupBox9.Controls.Add(this.filterValidPatterns);
            this.groupBox9.Location = new System.Drawing.Point(8, 301);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(832, 398);
            this.groupBox9.TabIndex = 19;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Selected Patterns to Evaluate for Relationships to Outcome";
            // 
            // selectSecondAttributes
            // 
            this.selectSecondAttributes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectSecondAttributes.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectSecondAttributes.Location = new System.Drawing.Point(625, 16);
            this.selectSecondAttributes.Name = "selectSecondAttributes";
            this.selectSecondAttributes.Size = new System.Drawing.Size(104, 16);
            this.selectSecondAttributes.TabIndex = 26;
            this.selectSecondAttributes.Text = "Select Attributes Used";
            this.selectSecondAttributes.Click += new System.EventHandler(this.selectSecondAttributes_Click);
            // 
            // selectFirstAttributes
            // 
            this.selectFirstAttributes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectFirstAttributes.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectFirstAttributes.Location = new System.Drawing.Point(383, 16);
            this.selectFirstAttributes.Name = "selectFirstAttributes";
            this.selectFirstAttributes.Size = new System.Drawing.Size(104, 16);
            this.selectFirstAttributes.TabIndex = 25;
            this.selectFirstAttributes.Text = "Select Attributes Used";
            this.selectFirstAttributes.Click += new System.EventHandler(this.selectFirstAttributes_Click);
            // 
            // selectPerformanceMeasures
            // 
            this.selectPerformanceMeasures.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.selectPerformanceMeasures.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectPerformanceMeasures.Location = new System.Drawing.Point(142, 16);
            this.selectPerformanceMeasures.Name = "selectPerformanceMeasures";
            this.selectPerformanceMeasures.Size = new System.Drawing.Size(104, 16);
            this.selectPerformanceMeasures.TabIndex = 23;
            this.selectPerformanceMeasures.Text = "Select Perf Measures";
            this.selectPerformanceMeasures.Click += new System.EventHandler(this.selectPerformanceMeasures_Click);
            // 
            // selectedPatterns
            // 
            this.selectedPatterns.AllowColumnReorder = true;
            this.selectedPatterns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.selectedPatterns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader32,
            this.columnHeader28});
            this.selectedPatterns.FullRowSelect = true;
            this.selectedPatterns.GridLines = true;
            this.selectedPatterns.HideSelection = false;
            this.selectedPatterns.Location = new System.Drawing.Point(8, 198);
            this.selectedPatterns.Name = "selectedPatterns";
            this.selectedPatterns.ShowItemToolTips = true;
            this.selectedPatterns.Size = new System.Drawing.Size(696, 192);
            this.selectedPatterns.TabIndex = 0;
            this.selectedPatterns.UseCompatibleStateImageBehavior = false;
            this.selectedPatterns.View = System.Windows.Forms.View.Details;
            this.selectedPatterns.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.selectedPatterns_ColumnClick_1);
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Measure";
            this.columnHeader19.Width = 115;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Attribute 1 Dimension";
            this.columnHeader20.Width = 115;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Attribute 1 Column";
            this.columnHeader21.Width = 115;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Attribute 2 Dimension";
            this.columnHeader22.Width = 115;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Attribute 2 Column";
            this.columnHeader23.Width = 115;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Proportion";
            this.columnHeader26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Actionable";
            this.columnHeader27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader27.Width = 65;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Model";
            this.columnHeader32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader32.Width = 65;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Max Num";
            this.columnHeader28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader28.Width = 65;
            // 
            // removeAttribButton
            // 
            this.removeAttribButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeAttribButton.Location = new System.Drawing.Point(712, 230);
            this.removeAttribButton.Name = "removeAttribButton";
            this.removeAttribButton.Size = new System.Drawing.Size(104, 24);
            this.removeAttribButton.TabIndex = 10;
            this.removeAttribButton.Text = "Remove Patterns";
            this.removeAttribButton.Click += new System.EventHandler(this.removeAttribButton_Click);
            // 
            // availPatternMeasures
            // 
            this.availPatternMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availPatternMeasures.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availPatternMeasures.Location = new System.Drawing.Point(8, 32);
            this.availPatternMeasures.Name = "availPatternMeasures";
            this.availPatternMeasures.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.availPatternMeasures.Size = new System.Drawing.Size(238, 145);
            this.availPatternMeasures.TabIndex = 16;
            this.availPatternMeasures.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessModels_MouseMove);
            // 
            // availPatternAttribute1
            // 
            this.availPatternAttribute1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availPatternAttribute1.Location = new System.Drawing.Point(249, 32);
            this.availPatternAttribute1.Name = "availPatternAttribute1";
            this.availPatternAttribute1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.availPatternAttribute1.Size = new System.Drawing.Size(238, 145);
            this.availPatternAttribute1.TabIndex = 14;
            this.availPatternAttribute1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessModels_MouseMove);
            // 
            // availPatternAttribute2
            // 
            this.availPatternAttribute2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availPatternAttribute2.Location = new System.Drawing.Point(490, 32);
            this.availPatternAttribute2.Name = "availPatternAttribute2";
            this.availPatternAttribute2.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.availPatternAttribute2.Size = new System.Drawing.Size(238, 145);
            this.availPatternAttribute2.TabIndex = 15;
            this.availPatternAttribute2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessModels_MouseMove);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(493, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(104, 16);
            this.label25.TabIndex = 8;
            this.label25.Text = "Second Attribute";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(246, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 7;
            this.label14.Text = "First Attribute";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 2;
            this.label13.Text = "Measure";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(8, 181);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(184, 23);
            this.label26.TabIndex = 11;
            this.label26.Text = "Selected Patterns to Evaluate";
            this.toolTip.SetToolTip(this.label26, "Select patterns to be modeled against the target measure. If inheriting patterns " +
                    "from another model, pattern settings here will override those of the parent mode" +
                    "l.");
            // 
            // totalPatternLabel
            // 
            this.totalPatternLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPatternLabel.Location = new System.Drawing.Point(592, 183);
            this.totalPatternLabel.Name = "totalPatternLabel";
            this.totalPatternLabel.Size = new System.Drawing.Size(112, 16);
            this.totalPatternLabel.TabIndex = 18;
            this.totalPatternLabel.Text = "Total Count: 0";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.modelFlag);
            this.groupBox22.Controls.Add(this.label124);
            this.groupBox22.Controls.Add(this.maxPatterns);
            this.groupBox22.Controls.Add(this.actionableFlag);
            this.groupBox22.Controls.Add(this.setPatternFlags);
            this.groupBox22.Controls.Add(this.proportionFlag);
            this.groupBox22.Location = new System.Drawing.Point(712, 262);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(112, 128);
            this.groupBox22.TabIndex = 22;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Set Pattern Attrs";
            // 
            // modelFlag
            // 
            this.modelFlag.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.modelFlag.Location = new System.Drawing.Point(8, 48);
            this.modelFlag.Name = "modelFlag";
            this.modelFlag.Size = new System.Drawing.Size(88, 16);
            this.modelFlag.TabIndex = 25;
            this.modelFlag.Text = "Model";
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.Location = new System.Drawing.Point(8, 72);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(32, 16);
            this.label124.TabIndex = 24;
            this.label124.Text = "Max";
            // 
            // maxPatterns
            // 
            this.maxPatterns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxPatterns.Location = new System.Drawing.Point(40, 72);
            this.maxPatterns.Name = "maxPatterns";
            this.maxPatterns.Size = new System.Drawing.Size(64, 20);
            this.maxPatterns.TabIndex = 23;
            this.maxPatterns.Text = "10";
            // 
            // actionableFlag
            // 
            this.actionableFlag.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.actionableFlag.Location = new System.Drawing.Point(8, 32);
            this.actionableFlag.Name = "actionableFlag";
            this.actionableFlag.Size = new System.Drawing.Size(88, 16);
            this.actionableFlag.TabIndex = 22;
            this.actionableFlag.Text = "Actionable";
            // 
            // setPatternFlags
            // 
            this.setPatternFlags.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setPatternFlags.Location = new System.Drawing.Point(48, 96);
            this.setPatternFlags.Name = "setPatternFlags";
            this.setPatternFlags.Size = new System.Drawing.Size(56, 24);
            this.setPatternFlags.TabIndex = 20;
            this.setPatternFlags.Text = "Set";
            this.setPatternFlags.Click += new System.EventHandler(this.setPatternFlags_Click);
            // 
            // proportionFlag
            // 
            this.proportionFlag.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.proportionFlag.Location = new System.Drawing.Point(8, 16);
            this.proportionFlag.Name = "proportionFlag";
            this.proportionFlag.Size = new System.Drawing.Size(80, 16);
            this.proportionFlag.TabIndex = 21;
            this.proportionFlag.Text = "Proportion";
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.addAttribButton);
            this.groupBox28.Controls.Add(this.addMeasureOnlyButton);
            this.groupBox28.Controls.Add(this.add2AttribButton);
            this.groupBox28.Location = new System.Drawing.Point(733, 24);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(93, 112);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Add Patterns";
            // 
            // addAttribButton
            // 
            this.addAttribButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addAttribButton.Location = new System.Drawing.Point(6, 48);
            this.addAttribButton.Name = "addAttribButton";
            this.addAttribButton.Size = new System.Drawing.Size(80, 24);
            this.addAttribButton.TabIndex = 4;
            this.addAttribButton.Text = "1 Attribute";
            this.addAttribButton.Click += new System.EventHandler(this.addAttribButton_Click);
            // 
            // addMeasureOnlyButton
            // 
            this.addMeasureOnlyButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addMeasureOnlyButton.Location = new System.Drawing.Point(6, 16);
            this.addMeasureOnlyButton.Name = "addMeasureOnlyButton";
            this.addMeasureOnlyButton.Size = new System.Drawing.Size(80, 24);
            this.addMeasureOnlyButton.TabIndex = 19;
            this.addMeasureOnlyButton.Text = "No Attributes";
            this.addMeasureOnlyButton.Click += new System.EventHandler(this.addMeasureOnlyButton_Click);
            // 
            // add2AttribButton
            // 
            this.add2AttribButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.add2AttribButton.Location = new System.Drawing.Point(6, 80);
            this.add2AttribButton.Name = "add2AttribButton";
            this.add2AttribButton.Size = new System.Drawing.Size(80, 24);
            this.add2AttribButton.TabIndex = 12;
            this.add2AttribButton.Text = "2 Attributes";
            this.add2AttribButton.Click += new System.EventHandler(this.add2AttribButton_Click);
            // 
            // filterValidPatterns
            // 
            this.filterValidPatterns.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.filterValidPatterns.Location = new System.Drawing.Point(712, 198);
            this.filterValidPatterns.Name = "filterValidPatterns";
            this.filterValidPatterns.Size = new System.Drawing.Size(104, 24);
            this.filterValidPatterns.TabIndex = 19;
            this.filterValidPatterns.Text = "Filter Valid";
            this.filterValidPatterns.Click += new System.EventHandler(this.filterValidPatterns_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.groupBox1);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.partitionAttributeBox);
            this.groupBox10.Controls.Add(this.availAttributeListBox);
            this.groupBox10.Controls.Add(this.iterateLevelBox);
            this.groupBox10.Controls.Add(this.iterateDimBox);
            this.groupBox10.Controls.Add(this.label109);
            this.groupBox10.Controls.Add(this.label108);
            this.groupBox10.Controls.Add(this.label98);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.targetLevelBox);
            this.groupBox10.Controls.Add(this.targetDimBox);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.label29);
            this.groupBox10.Controls.Add(this.targetMeasureBox);
            this.groupBox10.Location = new System.Drawing.Point(8, 136);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(832, 159);
            this.groupBox10.TabIndex = 24;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Model Target Properties";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.oneModelButton);
            this.groupBox1.Controls.Add(this.clusterBreakButton);
            this.groupBox1.Controls.Add(this.attributeBreakButton);
            this.groupBox1.Controls.Add(this.breakAttribute);
            this.groupBox1.Location = new System.Drawing.Point(614, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 93);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Model Segments";
            this.toolTip.SetToolTip(this.groupBox1, "Useful for segmenting a dataset and building different models for each segment");
            // 
            // oneModelButton
            // 
            this.oneModelButton.Checked = true;
            this.oneModelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.oneModelButton.Location = new System.Drawing.Point(11, 14);
            this.oneModelButton.Name = "oneModelButton";
            this.oneModelButton.Size = new System.Drawing.Size(80, 24);
            this.oneModelButton.TabIndex = 35;
            this.oneModelButton.TabStop = true;
            this.oneModelButton.Text = "One Model";
            // 
            // clusterBreakButton
            // 
            this.clusterBreakButton.Enabled = false;
            this.clusterBreakButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.clusterBreakButton.Location = new System.Drawing.Point(97, 13);
            this.clusterBreakButton.Name = "clusterBreakButton";
            this.clusterBreakButton.Size = new System.Drawing.Size(76, 24);
            this.clusterBreakButton.TabIndex = 36;
            this.clusterBreakButton.Text = "Clusters";
            // 
            // attributeBreakButton
            // 
            this.attributeBreakButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.attributeBreakButton.Location = new System.Drawing.Point(11, 37);
            this.attributeBreakButton.Name = "attributeBreakButton";
            this.attributeBreakButton.Size = new System.Drawing.Size(191, 24);
            this.attributeBreakButton.TabIndex = 37;
            this.attributeBreakButton.Text = "Segment Using Attribute";
            // 
            // breakAttribute
            // 
            this.breakAttribute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.breakAttribute.Location = new System.Drawing.Point(11, 67);
            this.breakAttribute.Name = "breakAttribute";
            this.breakAttribute.Size = new System.Drawing.Size(194, 21);
            this.breakAttribute.Sorted = true;
            this.breakAttribute.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(613, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 15);
            this.label1.TabIndex = 44;
            this.label1.Text = "Partition Dataset Attribute";
            this.toolTip.SetToolTip(this.label1, "If dataset is large (too large to fit into memory) it can be partitioned using a " +
                    "given attribute.");
            // 
            // partitionAttributeBox
            // 
            this.partitionAttributeBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.partitionAttributeBox.Location = new System.Drawing.Point(616, 32);
            this.partitionAttributeBox.Name = "partitionAttributeBox";
            this.partitionAttributeBox.Size = new System.Drawing.Size(208, 21);
            this.partitionAttributeBox.Sorted = true;
            this.partitionAttributeBox.TabIndex = 43;
            // 
            // availAttributeListBox
            // 
            this.availAttributeListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availAttributeListBox.CheckOnClick = true;
            this.availAttributeListBox.FormattingEnabled = true;
            this.availAttributeListBox.Location = new System.Drawing.Point(379, 32);
            this.availAttributeListBox.Name = "availAttributeListBox";
            this.availAttributeListBox.Size = new System.Drawing.Size(208, 122);
            this.availAttributeListBox.TabIndex = 42;
            this.availAttributeListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessModels_MouseMove);
            // 
            // iterateLevelBox
            // 
            this.iterateLevelBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iterateLevelBox.Location = new System.Drawing.Point(224, 77);
            this.iterateLevelBox.Name = "iterateLevelBox";
            this.iterateLevelBox.Size = new System.Drawing.Size(128, 21);
            this.iterateLevelBox.Sorted = true;
            this.iterateLevelBox.TabIndex = 41;
            // 
            // iterateDimBox
            // 
            this.iterateDimBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iterateDimBox.Location = new System.Drawing.Point(88, 77);
            this.iterateDimBox.Name = "iterateDimBox";
            this.iterateDimBox.Size = new System.Drawing.Size(128, 21);
            this.iterateDimBox.Sorted = true;
            this.iterateDimBox.TabIndex = 40;
            this.iterateDimBox.SelectedIndexChanged += new System.EventHandler(this.iterateDimBox_SelectedIndexChanged);
            // 
            // label109
            // 
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(224, 37);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(72, 16);
            this.label109.TabIndex = 39;
            this.label109.Text = "Level";
            // 
            // label108
            // 
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(88, 37);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(72, 16);
            this.label108.TabIndex = 38;
            this.label108.Text = "Dimension";
            // 
            // label98
            // 
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(376, 14);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(168, 25);
            this.label98.TabIndex = 27;
            this.label98.Text = "Target Attributes";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 77);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(80, 23);
            this.label27.TabIndex = 21;
            this.label27.Text = "Iterate Over";
            // 
            // targetLevelBox
            // 
            this.targetLevelBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetLevelBox.Location = new System.Drawing.Point(224, 53);
            this.targetLevelBox.Name = "targetLevelBox";
            this.targetLevelBox.Size = new System.Drawing.Size(128, 21);
            this.targetLevelBox.Sorted = true;
            this.targetLevelBox.TabIndex = 23;
            // 
            // targetDimBox
            // 
            this.targetDimBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetDimBox.Location = new System.Drawing.Point(88, 53);
            this.targetDimBox.Name = "targetDimBox";
            this.targetDimBox.Size = new System.Drawing.Size(128, 21);
            this.targetDimBox.Sorted = true;
            this.targetDimBox.TabIndex = 22;
            this.targetDimBox.SelectedIndexChanged += new System.EventHandler(this.targetDimBox_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(8, 53);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 23);
            this.label28.TabIndex = 20;
            this.label28.Text = "Target";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(8, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 23);
            this.label29.TabIndex = 25;
            this.label29.Text = "Target Measure";
            // 
            // targetMeasureBox
            // 
            this.targetMeasureBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetMeasureBox.Location = new System.Drawing.Point(120, 16);
            this.targetMeasureBox.Name = "targetMeasureBox";
            this.targetMeasureBox.Size = new System.Drawing.Size(232, 21);
            this.targetMeasureBox.Sorted = true;
            this.targetMeasureBox.TabIndex = 26;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.aggregateCheckBox);
            this.groupBox11.Controls.Add(this.runnableCheckBox);
            this.groupBox11.Controls.Add(this.exportModelButton);
            this.groupBox11.Controls.Add(this.groupMeasureOnly);
            this.groupBox11.Controls.Add(this.label179);
            this.groupBox11.Controls.Add(this.inheritSuccessModel);
            this.groupBox11.Controls.Add(this.filterNullValuesBox);
            this.groupBox11.Controls.Add(this.modelFilterBox);
            this.groupBox11.Controls.Add(this.label60);
            this.groupBox11.Location = new System.Drawing.Point(8, 8);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(832, 128);
            this.groupBox11.TabIndex = 36;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Model Attributes";
            // 
            // aggregateCheckBox
            // 
            this.aggregateCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.aggregateCheckBox.Location = new System.Drawing.Point(679, 95);
            this.aggregateCheckBox.Name = "aggregateCheckBox";
            this.aggregateCheckBox.Size = new System.Drawing.Size(130, 16);
            this.aggregateCheckBox.TabIndex = 52;
            this.aggregateCheckBox.Text = "Create Aggregates";
            this.toolTip.SetToolTip(this.aggregateCheckBox, "Should an aggregate be created for this model");
            // 
            // runnableCheckBox
            // 
            this.runnableCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.runnableCheckBox.Location = new System.Drawing.Point(679, 73);
            this.runnableCheckBox.Name = "runnableCheckBox";
            this.runnableCheckBox.Size = new System.Drawing.Size(100, 16);
            this.runnableCheckBox.TabIndex = 51;
            this.runnableCheckBox.Text = "Runnable";
            this.toolTip.SetToolTip(this.runnableCheckBox, "Is this model one that can be run");
            // 
            // exportModelButton
            // 
            this.exportModelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.exportModelButton.Location = new System.Drawing.Point(549, 16);
            this.exportModelButton.Name = "exportModelButton";
            this.exportModelButton.Size = new System.Drawing.Size(75, 20);
            this.exportModelButton.TabIndex = 37;
            this.exportModelButton.Text = "Export";
            this.exportModelButton.Click += new System.EventHandler(this.exportModelButton_Click);
            // 
            // groupMeasureOnly
            // 
            this.groupMeasureOnly.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupMeasureOnly.Location = new System.Drawing.Point(384, 63);
            this.groupMeasureOnly.Name = "groupMeasureOnly";
            this.groupMeasureOnly.Size = new System.Drawing.Size(224, 16);
            this.groupMeasureOnly.TabIndex = 49;
            this.groupMeasureOnly.Text = "Group Measure-only Patterns";
            // 
            // label179
            // 
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.Location = new System.Drawing.Point(376, 87);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(80, 23);
            this.label179.TabIndex = 47;
            this.label179.Text = "Inherit From";
            // 
            // inheritSuccessModel
            // 
            this.inheritSuccessModel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inheritSuccessModel.Location = new System.Drawing.Point(464, 87);
            this.inheritSuccessModel.Name = "inheritSuccessModel";
            this.inheritSuccessModel.Size = new System.Drawing.Size(160, 21);
            this.inheritSuccessModel.Sorted = true;
            this.inheritSuccessModel.TabIndex = 48;
            // 
            // filterNullValuesBox
            // 
            this.filterNullValuesBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.filterNullValuesBox.Location = new System.Drawing.Point(384, 47);
            this.filterNullValuesBox.Name = "filterNullValuesBox";
            this.filterNullValuesBox.Size = new System.Drawing.Size(224, 16);
            this.filterNullValuesBox.TabIndex = 46;
            this.filterNullValuesBox.Text = "Filter to Remove Null Value Categories";
            this.toolTip.SetToolTip(this.filterNullValuesBox, "When expanding out individual values of attributes, whether or not to eliminate t" +
                    "o consider columns where the attribute value is null.");
            // 
            // modelFilterBox
            // 
            this.modelFilterBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modelFilterBox.CheckOnClick = true;
            this.modelFilterBox.Location = new System.Drawing.Point(62, 21);
            this.modelFilterBox.Name = "modelFilterBox";
            this.modelFilterBox.Size = new System.Drawing.Size(290, 92);
            this.modelFilterBox.TabIndex = 44;
            this.modelFilterBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuccessModels_MouseMove);
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(8, 16);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(48, 23);
            this.label60.TabIndex = 45;
            this.label60.Text = "Filters";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // successModelMenu
            // 
            this.successModelMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSuccessModelToolStripMenuItem,
            this.removeSuccessModelToolStripMenuItem});
            this.successModelMenu.Name = "successModelMenu";
            this.successModelMenu.ShowImageMargin = false;
            this.successModelMenu.Size = new System.Drawing.Size(172, 48);
            // 
            // newSuccessModelToolStripMenuItem
            // 
            this.newSuccessModelToolStripMenuItem.Name = "newSuccessModelToolStripMenuItem";
            this.newSuccessModelToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.newSuccessModelToolStripMenuItem.Text = "Add Model Dataset";
            this.newSuccessModelToolStripMenuItem.Click += new System.EventHandler(this.addModel_Click);
            // 
            // removeSuccessModelToolStripMenuItem
            // 
            this.removeSuccessModelToolStripMenuItem.Name = "removeSuccessModelToolStripMenuItem";
            this.removeSuccessModelToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.removeSuccessModelToolStripMenuItem.Text = "Remove Model Dataset";
            this.removeSuccessModelToolStripMenuItem.Click += new System.EventHandler(this.removeModel_Click);
            // 
            // SuccessModels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "SuccessModels";
            this.Text = "SuccessModels";
            this.tabControl.ResumeLayout(false);
            this.modelTabPage.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.successModelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage modelTabPage;
        private System.Windows.Forms.Button exportModelButton;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button selectSecondAttributes;
        private System.Windows.Forms.Button selectFirstAttributes;
        private System.Windows.Forms.Button selectPerformanceMeasures;
        private System.Windows.Forms.ListView selectedPatterns;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.Button removeAttribButton;
        private System.Windows.Forms.ListBox availPatternMeasures;
        private System.Windows.Forms.ListBox availPatternAttribute1;
        private System.Windows.Forms.ListBox availPatternAttribute2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label totalPatternLabel;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.CheckBox modelFlag;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox maxPatterns;
        private System.Windows.Forms.CheckBox actionableFlag;
        private System.Windows.Forms.Button setPatternFlags;
        private System.Windows.Forms.CheckBox proportionFlag;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button addAttribButton;
        private System.Windows.Forms.Button addMeasureOnlyButton;
        private System.Windows.Forms.Button add2AttribButton;
        private System.Windows.Forms.Button filterValidPatterns;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox iterateLevelBox;
        private System.Windows.Forms.ComboBox iterateDimBox;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.RadioButton attributeBreakButton;
        private System.Windows.Forms.RadioButton clusterBreakButton;
        private System.Windows.Forms.RadioButton oneModelButton;
        private System.Windows.Forms.ComboBox breakAttribute;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox targetMeasureBox;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.CheckBox groupMeasureOnly;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.ComboBox inheritSuccessModel;
        private System.Windows.Forms.CheckBox filterNullValuesBox;
        private System.Windows.Forms.CheckedListBox modelFilterBox;
        private System.Windows.Forms.Label label60;
        public System.Windows.Forms.ComboBox targetLevelBox;
        public System.Windows.Forms.ComboBox targetDimBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ContextMenuStrip successModelMenu;
        private System.Windows.Forms.ToolStripMenuItem newSuccessModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSuccessModelToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox availAttributeListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox partitionAttributeBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox aggregateCheckBox;
        private System.Windows.Forms.CheckBox runnableCheckBox;
    }
}