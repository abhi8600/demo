namespace Performance_Optimizer_Administration
{
    partial class DefineTableSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefineTableSource));
            this.tableList = new System.Windows.Forms.ListView();
            this.tableName = new System.Windows.Forms.ColumnHeader();
            this.joinType = new System.Windows.Forms.ColumnHeader();
            this.joinStr = new System.Windows.Forms.ColumnHeader();
            this.addTable = new System.Windows.Forms.Button();
            this.removeTable = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.innerJoin = new System.Windows.Forms.RadioButton();
            this.leftOuterJoin = new System.Windows.Forms.RadioButton();
            this.rightOuterJoin = new System.Windows.Forms.RadioButton();
            this.fullOuterJoin = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.joinClause = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableNameBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.updateTable = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableList
            // 
            this.tableList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tableName,
            this.joinType,
            this.joinStr});
            this.tableList.FullRowSelect = true;
            this.tableList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tableList.HideSelection = false;
            this.tableList.Location = new System.Drawing.Point(6, 12);
            this.tableList.MultiSelect = false;
            this.tableList.Name = "tableList";
            this.tableList.Size = new System.Drawing.Size(529, 97);
            this.tableList.TabIndex = 0;
            this.tableList.UseCompatibleStateImageBehavior = false;
            this.tableList.View = System.Windows.Forms.View.Details;
            this.tableList.SelectedIndexChanged += new System.EventHandler(this.tableList_SelectedIndexChanged);
            // 
            // tableName
            // 
            this.tableName.Text = "Table Name";
            this.tableName.Width = 150;
            // 
            // joinType
            // 
            this.joinType.Text = "Join Type";
            this.joinType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.joinType.Width = 70;
            // 
            // joinStr
            // 
            this.joinStr.Text = "Join Clause";
            this.joinStr.Width = 300;
            // 
            // addTable
            // 
            this.addTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addTable.Location = new System.Drawing.Point(541, 12);
            this.addTable.Name = "addTable";
            this.addTable.Size = new System.Drawing.Size(119, 23);
            this.addTable.TabIndex = 1;
            this.addTable.Text = "Add Table Definition";
            this.addTable.UseVisualStyleBackColor = true;
            this.addTable.Click += new System.EventHandler(this.addTable_Click);
            // 
            // removeTable
            // 
            this.removeTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeTable.Location = new System.Drawing.Point(541, 70);
            this.removeTable.Name = "removeTable";
            this.removeTable.Size = new System.Drawing.Size(119, 23);
            this.removeTable.TabIndex = 2;
            this.removeTable.Text = "Remove Definition";
            this.removeTable.UseVisualStyleBackColor = true;
            this.removeTable.Click += new System.EventHandler(this.removeTable_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Table Name";
            // 
            // innerJoin
            // 
            this.innerJoin.AutoSize = true;
            this.innerJoin.Checked = true;
            this.innerJoin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.innerJoin.Location = new System.Drawing.Point(180, 35);
            this.innerJoin.Name = "innerJoin";
            this.innerJoin.Size = new System.Drawing.Size(70, 17);
            this.innerJoin.TabIndex = 5;
            this.innerJoin.TabStop = true;
            this.innerJoin.Text = "Inner Join";
            this.innerJoin.UseVisualStyleBackColor = true;
            // 
            // leftOuterJoin
            // 
            this.leftOuterJoin.AutoSize = true;
            this.leftOuterJoin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.leftOuterJoin.Location = new System.Drawing.Point(257, 35);
            this.leftOuterJoin.Name = "leftOuterJoin";
            this.leftOuterJoin.Size = new System.Drawing.Size(93, 17);
            this.leftOuterJoin.TabIndex = 6;
            this.leftOuterJoin.Text = "Left Outer Join";
            this.leftOuterJoin.UseVisualStyleBackColor = true;
            // 
            // rightOuterJoin
            // 
            this.rightOuterJoin.AutoSize = true;
            this.rightOuterJoin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rightOuterJoin.Location = new System.Drawing.Point(357, 35);
            this.rightOuterJoin.Name = "rightOuterJoin";
            this.rightOuterJoin.Size = new System.Drawing.Size(100, 17);
            this.rightOuterJoin.TabIndex = 7;
            this.rightOuterJoin.Text = "Right Outer Join";
            this.rightOuterJoin.UseVisualStyleBackColor = true;
            // 
            // fullOuterJoin
            // 
            this.fullOuterJoin.AutoSize = true;
            this.fullOuterJoin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.fullOuterJoin.Location = new System.Drawing.Point(464, 35);
            this.fullOuterJoin.Name = "fullOuterJoin";
            this.fullOuterJoin.Size = new System.Drawing.Size(91, 17);
            this.fullOuterJoin.TabIndex = 8;
            this.fullOuterJoin.Text = "Full Outer Join";
            this.fullOuterJoin.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Join Type";
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKButton.Location = new System.Drawing.Point(504, 277);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 10;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(585, 277);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 11;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // joinClause
            // 
            this.joinClause.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.joinClause.Location = new System.Drawing.Point(6, 73);
            this.joinClause.Multiline = true;
            this.joinClause.Name = "joinClause";
            this.joinClause.Size = new System.Drawing.Size(642, 60);
            this.joinClause.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Join Clause";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableNameBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.joinClause);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.innerJoin);
            this.groupBox1.Controls.Add(this.leftOuterJoin);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rightOuterJoin);
            this.groupBox1.Controls.Add(this.fullOuterJoin);
            this.groupBox1.Location = new System.Drawing.Point(6, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(654, 156);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Table Definition";
            // 
            // tableNameBox
            // 
            this.tableNameBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tableNameBox.FormattingEnabled = true;
            this.tableNameBox.Location = new System.Drawing.Point(6, 35);
            this.tableNameBox.Name = "tableNameBox";
            this.tableNameBox.Size = new System.Drawing.Size(168, 21);
            this.tableNameBox.TabIndex = 15;
            this.tableNameBox.DropDown += new System.EventHandler(this.tableNameBox_DropDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(589, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Join Syntax: Use fully qualified physical table and column names when specifying " +
                "join syntax, e.g.: TAB1.COL1=TAB2.COL2";
            // 
            // updateTable
            // 
            this.updateTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.updateTable.Location = new System.Drawing.Point(541, 41);
            this.updateTable.Name = "updateTable";
            this.updateTable.Size = new System.Drawing.Size(119, 23);
            this.updateTable.TabIndex = 15;
            this.updateTable.Text = "Update Definition";
            this.updateTable.UseVisualStyleBackColor = true;
            this.updateTable.Click += new System.EventHandler(this.updateTable_Click);
            // 
            // DefineTableSource
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(664, 306);
            this.Controls.Add(this.updateTable);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.removeTable);
            this.Controls.Add(this.addTable);
            this.Controls.Add(this.tableList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DefineTableSource";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Define Table Source";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefineTableSource_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView tableList;
        private System.Windows.Forms.ColumnHeader tableName;
        private System.Windows.Forms.ColumnHeader joinType;
        private System.Windows.Forms.Button addTable;
        private System.Windows.Forms.Button removeTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton innerJoin;
        private System.Windows.Forms.RadioButton leftOuterJoin;
        private System.Windows.Forms.RadioButton rightOuterJoin;
        private System.Windows.Forms.RadioButton fullOuterJoin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox joinClause;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader joinStr;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button updateTable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox tableNameBox;
    }
}