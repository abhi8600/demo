using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class GetStagingColumnGrainInfo : Form
    {
        private List<StagingColumnGrainInfo> scGrainInfoList;
        MainAdminForm ma;

        public GetStagingColumnGrainInfo(MainAdminForm ma, List<StagingColumnGrainInfo> scGrainInfoList, string colName)
        {
            this.ma = ma;
            this.scGrainInfoList = scGrainInfoList;
            InitializeComponent();
            ((DataGridView)mapPanel.Controls["mapGrid"]).DataSource = setTable;
            ((DataGridView)mapPanel.Controls["setGrid"]).DataSource = itemsTable;
            populateDropdowns();
            columnLabel.Text = "Column: " + colName;
        }

        public void populateDropdowns()
        {
            List<StagingFilter> filters = ma.stagingFilterMod.getStagingFilters();
            filterBox.Items.Clear();
            if ((filters != null) && (filters.Count > 0))
            {
                foreach (StagingFilter sf in filters)
                {
                    filterBox.Items.Add(sf.Name);
                }
            }
            fillDimensionBox();
            fillGrainInfoList();
        }

        private void dimensionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedDimension = (string)dimensionBox.SelectedItem;
            levelBox.Items.Clear();
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            List<string> llist = new List<string>();
            foreach (Hierarchy h in hlist)
            {
                if (h.DimensionName != selectedDimension)
                {
                    continue;
                }
                h.getChildLevelNames(llist);
                break;
            }
            if (llist.Count > 0)
            {
                foreach (string level in llist)
                {
                    levelBox.Items.Add(level);
                }
            }
            else
            {
                levelBox.Items.Add("<Implicit>");
            }
            levelBox.SelectedIndex = 0;
        }

        public void fillDimensionBox()
        {
            dimensionBox.Items.Clear();
            foreach (string dim in ma.getDimensionList())
            {
                dimensionBox.Items.Add(dim);
            }
        }

        public void fillGrainInfoList()
        {
            grainInfoList.Items.Clear();

            if ((scGrainInfoList != null) && (scGrainInfoList.Count > 0))
            {
                foreach (StagingColumnGrainInfo scg in scGrainInfoList)
                {
                    grainInfoList.Items.Add(scg.getItem());
                }
            }
        }

        public StagingColumnGrainInfoDisplay getStagingColumnGrainInfoDisplay()
        {
            StagingColumnGrainInfoDisplay obj = new StagingColumnGrainInfoDisplay();
            obj.scGrainInfo = scGrainInfoList.ToArray();
            return obj;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string filter = (string)filterBox.SelectedItem;
            string dimension = (string)dimensionBox.SelectedItem;
            string level = (string)levelBox.SelectedItem;
            string columnName = (string)columnNameBox.Text;
            StagingAggregation agg = getAggregation();


            if (dimension == null || dimension == "" || level == null || level == "")
            {
                MessageBox.Show("Dimension and level must be selected to add a staging column grain info entry.", "Error");
                return;
            }
            int nameType = nameTypeBox.Text == "Column Prefix" ? StagingColumnGrainInfo.TYPE_PREFIX : StagingColumnGrainInfo.TYPE_RENAME;
            StagingColumnGrainInfo obj = new StagingColumnGrainInfo(filter, dimension, level, columnName, nameType, agg);
            grainInfoList.Items.Add(obj.getItem());
            if (scGrainInfoList == null)
            {
                scGrainInfoList = new List<StagingColumnGrainInfo>();
            }
            scGrainInfoList.Add(obj);
        }

        private StagingAggregation getAggregation()
        {
            StagingAggregation agg = null;
            if (aggBox.Text.Length > 0)
            {
                agg = new StagingAggregation();
                agg.Type = aggBox.Text;
                agg.Default = defaultBox.Text;
                if (aggBox.Text == "Ordered List")
                {
                    agg.ValueList = new string[setTable.Rows.Count];
                    for (int i = 0; i < setTable.Rows.Count; i++)
                    {
                        agg.ValueList[i] = (string)setTable.Rows[i]["Value"];
                    }
                }
                else if (aggBox.Text == "Map")
                {
                    agg.ValueList = new string[setTable.Rows.Count];
                    agg.SetList = new string[setTable.Rows.Count][];
                    for (int i = 0; i < setTable.Rows.Count; i++)
                    {
                        agg.ValueList[i] = (string)setTable.Rows[i]["Value"];
                        agg.SetList[i] = (string[])setTable.Rows[i]["SetObject"];
                    }
                }
            }
            return (agg);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (grainInfoList.SelectedItems.Count > 0)
            {
                foreach (int index in grainInfoList.SelectedIndices)
                {
                    grainInfoList.Items.RemoveAt(index);
                    scGrainInfoList.RemoveAt(index);
                }
            }
        }

        private void aggBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (aggBox.Text == "Ordered List")
            {
                orderedListPanel.Visible = true;
                mapPanel.Visible = false;
                defaultLabel.Visible = true;
                defaultBox.Visible = true;
            }
            else if (aggBox.Text == "Map")
            {
                orderedListPanel.Visible = false;
                mapPanel.Visible = true;
                defaultLabel.Visible = true;
                defaultBox.Visible = true;
            }
            else
            {
                orderedListPanel.Visible = false;
                mapPanel.Visible = false;
                defaultLabel.Visible = false;
                defaultBox.Visible = false;
            }
        }

        private void grainInfoList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grainInfoList.SelectedIndices.Count == 0) return;
            int index = grainInfoList.SelectedIndices[0];
            StagingColumnGrainInfo scg = scGrainInfoList[index];

            filterBox.Text = scg.StagingFilterName == null ? "" : scg.StagingFilterName;
            dimensionBox.Text = scg.Dimension;
            levelBox.Text = scg.Level;
            columnNameBox.Text = scg.NewName;
            nameTypeBox.Text = (string)nameTypeBox.Items[scg.NewNameType];

            if (scg.Aggregation != null)
            {
                aggBox.Text = scg.Aggregation.Type;
                defaultBox.Text = scg.Aggregation.Default;
                if (aggBox.Text == "Ordered List")
                {
                    setTable.Rows.Clear();
                    for (int i = 0; i < scg.Aggregation.ValueList.Length; i++)
                    {
                        DataRow dr = setTable.NewRow();
                        dr["Value"] = scg.Aggregation.ValueList[i];
                        setTable.Rows.Add(dr);
                    }
                }
                else if (aggBox.Text == "Map")
                {
                    setTable.Rows.Clear();
                    itemsTable.Rows.Clear();
                    for (int i = 0; i < scg.Aggregation.ValueList.Length; i++)
                    {
                        DataRow dr = setTable.NewRow();
                        dr["Value"] = scg.Aggregation.ValueList[i];
                        dr["SetObject"] = scg.Aggregation.SetList[i];
                        dr["SetDisplay"] = getSetDisplay(scg.Aggregation.SetList[i]);
                        setTable.Rows.Add(dr);
                    }
                }
            }
            else
                aggBox.Text = "";
        }

        private string getSetDisplay(string[] set)
        {
            string disp = "";
            foreach (string s in set)
            {
                if (disp.Length > 0) disp += ",";
                disp += s;
            }
            return (disp);
        }

        private void addSetButton_Click(object sender, EventArgs e)
        {
            string[] set = new string[itemsTable.Rows.Count];
            for (int i = 0; i < itemsTable.Rows.Count; i++)
            {
                set[i] = (string)itemsTable.Rows[i]["Value"];
            }
            itemsTable.Rows.Clear();
            DataRow dr = setTable.NewRow();
            dr["SetObject"] = set;
            dr["SetDisplay"] = getSetDisplay(set);
            setTable.Rows.Add(dr);
        }
    }
}