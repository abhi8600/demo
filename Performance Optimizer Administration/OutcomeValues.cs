using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class OutcomeValues : Form
    {
        Pattern p;
        bool test;
        public OutcomeValues(Pattern p, List<OutcomeFactor> flist, bool test)
        {
            InitializeComponent();
            this.outcomeValueGridView.Rows.Clear();
            this.outcomeValueGridView.Columns.Clear();
            this.p = p;
            this.test = test;
            if (p.Type == Pattern.MEASUREONLY)
            {
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns[test ? 0 : 1].Visible = false;
                if (flist != null && flist.Count > 0)
                {
                    this.outcomeValueGridView.Rows.Add(new object[] { flist[0].BaseValue, flist[0].TestValue });
                }
                else
                    this.outcomeValueGridView.Rows.Add(new object[] { 0, 0 });
                this.outcomeValueGridView.AllowUserToAddRows = false;
                this.outcomeValueGridView.AllowUserToDeleteRows = false;
                this.outcomeValueGridView.ShowEditingIcon = false;
                this.outcomeValueGridView.RowHeadersVisible = false;
            }
            else if (p.Type == Pattern.ONEATTRIBUTE)
            {
                this.outcomeValueGridView.Columns.Add(p.Attribute1, p.Attribute1 + "Value");
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns[test ? 1 : 2].Visible = false;
                if (flist != null)
                    foreach (OutcomeFactor of in flist)
                    {
                        this.outcomeValueGridView.Rows.Add(new object[] { of.Attribute1Value, of.BaseValue, of.TestValue });
                    }
            }
            else if (p.Type == Pattern.TWOATTRIBUTE)
            {
                this.outcomeValueGridView.Columns.Add(p.Attribute1, p.Attribute1 + "Value");
                this.outcomeValueGridView.Columns.Add(p.Attribute2, p.Attribute2 + "Value");
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns.Add(p.Measure, p.Measure);
                this.outcomeValueGridView.Columns[this.outcomeValueGridView.Columns.Count - 1].ValueType = typeof(double);
                this.outcomeValueGridView.Columns[test ? 2 : 3].Visible = false;
                if (flist != null)
                    foreach (OutcomeFactor of in flist)
                    {
                        this.outcomeValueGridView.Rows.Add(new object[] { of.Attribute1Value, of.Attribute2Value, of.BaseValue, of.TestValue });
                    }
            }
        }

        public List<OutcomeFactor> getFactors()
        {
            List<OutcomeFactor> oflist = new List<OutcomeFactor>();
            foreach (DataGridViewRow dr in this.outcomeValueGridView.Rows)
            {
                if (dr.Cells[0].Value != null)
                {
                    if (p.Type == Pattern.MEASUREONLY)
                        oflist.Add(new OutcomeFactor(p, Double.Parse(dr.Cells[0].Value.ToString()), Double.Parse(dr.Cells[1].Value.ToString())));
                    else if (p.Type == Pattern.ONEATTRIBUTE)
                        oflist.Add(new OutcomeFactor(p, Double.Parse(dr.Cells[1].Value.ToString()), Double.Parse(dr.Cells[2].Value.ToString()), dr.Cells[0].Value));
                    else if (p.Type == Pattern.TWOATTRIBUTE)
                        oflist.Add(new OutcomeFactor(p, Double.Parse(dr.Cells[2].Value.ToString()), Double.Parse(dr.Cells[3].Value.ToString()), dr.Cells[0].Value, dr.Cells[1].Value));
                }
            }
            return (oflist);
        }

        private void outcomeValueGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            switch (e.Row.Cells.Count)
            {
                case 2:
                    e.Row.Cells[0].Value = 0.0;
                    e.Row.Cells[1].Value = 0.0;
                    break;
                case 3:
                    e.Row.Cells[0].Value = "Val";
                    e.Row.Cells[1].Value = 0.0;
                    e.Row.Cells[2].Value = 0.0;
                    break;
                case 4:
                    e.Row.Cells[0].Value = "Val";
                    e.Row.Cells[1].Value = "Val";
                    e.Row.Cells[2].Value = 0.0;
                    e.Row.Cells[3].Value = 0.0;
                    break;
            }
        }
    }
}