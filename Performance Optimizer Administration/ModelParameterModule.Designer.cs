namespace Performance_Optimizer_Administration
{
    partial class ModelParameterModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.modelParamTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.defaultClassifiersButton = new System.Windows.Forms.Button();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.reproductionRate = new System.Windows.Forms.TextBox();
            this.mutationRate = new System.Windows.Forms.TextBox();
            this.maxNoChange = new System.Windows.Forms.TextBox();
            this.maxGenerations = new System.Windows.Forms.TextBox();
            this.populationSize = new System.Windows.Forms.TextBox();
            this.label170 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.quasiNewtonMethod = new System.Windows.Forms.RadioButton();
            this.simplexMethod = new System.Windows.Forms.RadioButton();
            this.label164 = new System.Windows.Forms.Label();
            this.distanceMethodBox = new System.Windows.Forms.GroupBox();
            this.arffLimitTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.attributeSelectionCheckBox = new System.Windows.Forms.CheckBox();
            this.removeRelatedCheckBox = new System.Windows.Forms.CheckBox();
            this.removeUseless = new System.Windows.Forms.CheckBox();
            this.useCrossCorrelation = new System.Windows.Forms.CheckBox();
            this.crossCorrelationThreshold = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.targetCorrelationThreshold = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.explanableModels = new System.Windows.Forms.CheckBox();
            this.MaxInstances = new System.Windows.Forms.TextBox();
            this.MaxInstanceLabel = new System.Windows.Forms.Label();
            this.mapNullsToZeroCheckBox = new System.Windows.Forms.CheckBox();
            this.decisionTreeFilteringCheckBox = new System.Windows.Forms.CheckBox();
            this.attributesPerFold = new System.Windows.Forms.TextBox();
            this.targetReduction = new System.Windows.Forms.TextBox();
            this.attributesPerModel = new System.Windows.Forms.TextBox();
            this.samplesPerAttribute = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.searchSample = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.probMethodBox = new System.Windows.Forms.RadioButton();
            this.label114 = new System.Windows.Forms.Label();
            this.max1Categories = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.maxClustersPerPattern = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.bulkLoadDirectoryBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.maxComputationThreads = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl.SuspendLayout();
            this.modelParamTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.distanceMethodBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.modelParamTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // modelParamTabPage
            // 
            this.modelParamTabPage.AutoScroll = true;
            this.modelParamTabPage.Controls.Add(this.groupBox1);
            this.modelParamTabPage.Controls.Add(this.groupBox30);
            this.modelParamTabPage.Controls.Add(this.distanceMethodBox);
            this.modelParamTabPage.Controls.Add(this.groupBox20);
            this.modelParamTabPage.Location = new System.Drawing.Point(4, 22);
            this.modelParamTabPage.Name = "modelParamTabPage";
            this.modelParamTabPage.Size = new System.Drawing.Size(844, 707);
            this.modelParamTabPage.TabIndex = 16;
            this.modelParamTabPage.Text = "Model Parameters";
            this.modelParamTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.defaultClassifiersButton);
            this.groupBox1.Location = new System.Drawing.Point(8, 386);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(289, 232);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Best Fit Classifiers";
            // 
            // defaultClassifiersButton
            // 
            this.defaultClassifiersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.defaultClassifiersButton.Location = new System.Drawing.Point(87, 96);
            this.defaultClassifiersButton.Name = "defaultClassifiersButton";
            this.defaultClassifiersButton.Size = new System.Drawing.Size(114, 46);
            this.defaultClassifiersButton.TabIndex = 21;
            this.defaultClassifiersButton.Text = "Best Fit Classifier Default Settings";
            this.defaultClassifiersButton.UseVisualStyleBackColor = true;
            this.defaultClassifiersButton.Click += new System.EventHandler(this.defaultClassifiersButton_Click);
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.reproductionRate);
            this.groupBox30.Controls.Add(this.mutationRate);
            this.groupBox30.Controls.Add(this.maxNoChange);
            this.groupBox30.Controls.Add(this.maxGenerations);
            this.groupBox30.Controls.Add(this.populationSize);
            this.groupBox30.Controls.Add(this.label170);
            this.groupBox30.Controls.Add(this.label169);
            this.groupBox30.Controls.Add(this.label168);
            this.groupBox30.Controls.Add(this.label167);
            this.groupBox30.Controls.Add(this.label166);
            this.groupBox30.Controls.Add(this.label165);
            this.groupBox30.Controls.Add(this.quasiNewtonMethod);
            this.groupBox30.Controls.Add(this.simplexMethod);
            this.groupBox30.Controls.Add(this.label164);
            this.groupBox30.Location = new System.Drawing.Point(303, 386);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(537, 232);
            this.groupBox30.TabIndex = 20;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Optimizer Settings";
            // 
            // reproductionRate
            // 
            this.reproductionRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reproductionRate.Location = new System.Drawing.Point(168, 192);
            this.reproductionRate.Name = "reproductionRate";
            this.reproductionRate.Size = new System.Drawing.Size(40, 20);
            this.reproductionRate.TabIndex = 27;
            this.reproductionRate.Text = "1";
            // 
            // mutationRate
            // 
            this.mutationRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mutationRate.Location = new System.Drawing.Point(168, 168);
            this.mutationRate.Name = "mutationRate";
            this.mutationRate.Size = new System.Drawing.Size(40, 20);
            this.mutationRate.TabIndex = 26;
            this.mutationRate.Text = "1.5";
            // 
            // maxNoChange
            // 
            this.maxNoChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxNoChange.Location = new System.Drawing.Point(168, 144);
            this.maxNoChange.Name = "maxNoChange";
            this.maxNoChange.Size = new System.Drawing.Size(40, 20);
            this.maxNoChange.TabIndex = 25;
            this.maxNoChange.Text = "12";
            // 
            // maxGenerations
            // 
            this.maxGenerations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxGenerations.Location = new System.Drawing.Point(168, 120);
            this.maxGenerations.Name = "maxGenerations";
            this.maxGenerations.Size = new System.Drawing.Size(40, 20);
            this.maxGenerations.TabIndex = 24;
            this.maxGenerations.Text = "100";
            // 
            // populationSize
            // 
            this.populationSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.populationSize.Location = new System.Drawing.Point(168, 96);
            this.populationSize.Name = "populationSize";
            this.populationSize.Size = new System.Drawing.Size(40, 20);
            this.populationSize.TabIndex = 23;
            this.populationSize.Text = "125";
            // 
            // label170
            // 
            this.label170.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.Location = new System.Drawing.Point(16, 96);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(96, 16);
            this.label170.TabIndex = 22;
            this.label170.Text = "Population Size";
            this.label170.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label169
            // 
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.Location = new System.Drawing.Point(16, 192);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(104, 16);
            this.label169.TabIndex = 21;
            this.label169.Text = "Reproduction Rate";
            this.label169.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label168
            // 
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.Location = new System.Drawing.Point(16, 168);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(96, 16);
            this.label168.TabIndex = 20;
            this.label168.Text = "Mutation Rate";
            this.label168.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label167
            // 
            this.label167.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.Location = new System.Drawing.Point(16, 144);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(152, 16);
            this.label167.TabIndex = 19;
            this.label167.Text = "Max No Change Generations";
            this.label167.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label166
            // 
            this.label166.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.Location = new System.Drawing.Point(16, 120);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(96, 16);
            this.label166.TabIndex = 18;
            this.label166.Text = "Max Generations";
            this.label166.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label165
            // 
            this.label165.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.Location = new System.Drawing.Point(8, 80);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(96, 16);
            this.label165.TabIndex = 17;
            this.label165.Text = "Genetic Optimizer";
            // 
            // quasiNewtonMethod
            // 
            this.quasiNewtonMethod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.quasiNewtonMethod.Location = new System.Drawing.Point(8, 56);
            this.quasiNewtonMethod.Name = "quasiNewtonMethod";
            this.quasiNewtonMethod.Size = new System.Drawing.Size(128, 16);
            this.quasiNewtonMethod.TabIndex = 16;
            this.quasiNewtonMethod.Text = "BFGS Quasi Newton";
            // 
            // simplexMethod
            // 
            this.simplexMethod.Checked = true;
            this.simplexMethod.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.simplexMethod.Location = new System.Drawing.Point(8, 40);
            this.simplexMethod.Name = "simplexMethod";
            this.simplexMethod.Size = new System.Drawing.Size(72, 16);
            this.simplexMethod.TabIndex = 15;
            this.simplexMethod.TabStop = true;
            this.simplexMethod.Text = "Simplex";
            // 
            // label164
            // 
            this.label164.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(8, 24);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(160, 16);
            this.label164.TabIndex = 14;
            this.label164.Text = "Nonlinear Optimization Method";
            // 
            // distanceMethodBox
            // 
            this.distanceMethodBox.Controls.Add(this.arffLimitTextBox);
            this.distanceMethodBox.Controls.Add(this.label4);
            this.distanceMethodBox.Controls.Add(this.attributeSelectionCheckBox);
            this.distanceMethodBox.Controls.Add(this.removeRelatedCheckBox);
            this.distanceMethodBox.Controls.Add(this.removeUseless);
            this.distanceMethodBox.Controls.Add(this.useCrossCorrelation);
            this.distanceMethodBox.Controls.Add(this.crossCorrelationThreshold);
            this.distanceMethodBox.Controls.Add(this.label3);
            this.distanceMethodBox.Controls.Add(this.targetCorrelationThreshold);
            this.distanceMethodBox.Controls.Add(this.label2);
            this.distanceMethodBox.Controls.Add(this.explanableModels);
            this.distanceMethodBox.Controls.Add(this.MaxInstances);
            this.distanceMethodBox.Controls.Add(this.MaxInstanceLabel);
            this.distanceMethodBox.Controls.Add(this.mapNullsToZeroCheckBox);
            this.distanceMethodBox.Controls.Add(this.decisionTreeFilteringCheckBox);
            this.distanceMethodBox.Controls.Add(this.attributesPerFold);
            this.distanceMethodBox.Controls.Add(this.targetReduction);
            this.distanceMethodBox.Controls.Add(this.attributesPerModel);
            this.distanceMethodBox.Controls.Add(this.samplesPerAttribute);
            this.distanceMethodBox.Controls.Add(this.label123);
            this.distanceMethodBox.Controls.Add(this.label122);
            this.distanceMethodBox.Controls.Add(this.label121);
            this.distanceMethodBox.Controls.Add(this.label21);
            this.distanceMethodBox.Controls.Add(this.label120);
            this.distanceMethodBox.Controls.Add(this.searchSample);
            this.distanceMethodBox.Controls.Add(this.label119);
            this.distanceMethodBox.Controls.Add(this.label107);
            this.distanceMethodBox.Controls.Add(this.panel1);
            this.distanceMethodBox.Controls.Add(this.label114);
            this.distanceMethodBox.Controls.Add(this.max1Categories);
            this.distanceMethodBox.Controls.Add(this.label113);
            this.distanceMethodBox.Controls.Add(this.maxClustersPerPattern);
            this.distanceMethodBox.Controls.Add(this.label112);
            this.distanceMethodBox.Location = new System.Drawing.Point(8, 88);
            this.distanceMethodBox.Name = "distanceMethodBox";
            this.distanceMethodBox.Size = new System.Drawing.Size(832, 292);
            this.distanceMethodBox.TabIndex = 18;
            this.distanceMethodBox.TabStop = false;
            this.distanceMethodBox.Text = "Outcome Models";
            // 
            // arffLimitTextBox
            // 
            this.arffLimitTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.arffLimitTextBox.Location = new System.Drawing.Point(285, 216);
            this.arffLimitTextBox.Name = "arffLimitTextBox";
            this.arffLimitTextBox.Size = new System.Drawing.Size(56, 20);
            this.arffLimitTextBox.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(250, 16);
            this.label4.TabIndex = 57;
            this.label4.Text = "Maximum Number of Instances in ARFF";
            // 
            // attributeSelectionCheckBox
            // 
            this.attributeSelectionCheckBox.AutoSize = true;
            this.attributeSelectionCheckBox.Checked = true;
            this.attributeSelectionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.attributeSelectionCheckBox.Location = new System.Drawing.Point(403, 148);
            this.attributeSelectionCheckBox.Name = "attributeSelectionCheckBox";
            this.attributeSelectionCheckBox.Size = new System.Drawing.Size(148, 17);
            this.attributeSelectionCheckBox.TabIndex = 56;
            this.attributeSelectionCheckBox.Text = "Enable Attribute Selection";
            this.toolTip1.SetToolTip(this.attributeSelectionCheckBox, "Enable the automatic selection of attributes based upon the criteria specified be" +
                    "low.");
            this.attributeSelectionCheckBox.UseVisualStyleBackColor = true;
            // 
            // removeRelatedCheckBox
            // 
            this.removeRelatedCheckBox.AutoSize = true;
            this.removeRelatedCheckBox.Checked = true;
            this.removeRelatedCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeRelatedCheckBox.Location = new System.Drawing.Point(424, 171);
            this.removeRelatedCheckBox.Name = "removeRelatedCheckBox";
            this.removeRelatedCheckBox.Size = new System.Drawing.Size(128, 17);
            this.removeRelatedCheckBox.TabIndex = 55;
            this.removeRelatedCheckBox.Text = "Use Remove Related";
            this.toolTip1.SetToolTip(this.removeRelatedCheckBox, "Remove all measures that are related to the target measure (by searching through " +
                    "the derived measures).");
            this.removeRelatedCheckBox.UseVisualStyleBackColor = true;
            // 
            // removeUseless
            // 
            this.removeUseless.AutoSize = true;
            this.removeUseless.Checked = true;
            this.removeUseless.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeUseless.Location = new System.Drawing.Point(424, 194);
            this.removeUseless.Name = "removeUseless";
            this.removeUseless.Size = new System.Drawing.Size(237, 17);
            this.removeUseless.TabIndex = 54;
            this.removeUseless.Text = "Use Weka RemoveUseless to filter attributes";
            this.toolTip1.SetToolTip(this.removeUseless, "Remove attributes with zero or very little variability.");
            this.removeUseless.UseVisualStyleBackColor = true;
            // 
            // useCrossCorrelation
            // 
            this.useCrossCorrelation.AutoSize = true;
            this.useCrossCorrelation.Checked = true;
            this.useCrossCorrelation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useCrossCorrelation.Location = new System.Drawing.Point(424, 239);
            this.useCrossCorrelation.Name = "useCrossCorrelation";
            this.useCrossCorrelation.Size = new System.Drawing.Size(207, 17);
            this.useCrossCorrelation.TabIndex = 53;
            this.useCrossCorrelation.Text = "Use Cross Correlation to filter attributes";
            this.toolTip1.SetToolTip(this.useCrossCorrelation, "Check for correlation between attributes and remove them (using Desired Values an" +
                    "d correlated to target to determine what to remove).");
            this.useCrossCorrelation.UseVisualStyleBackColor = true;
            // 
            // crossCorrelationThreshold
            // 
            this.crossCorrelationThreshold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crossCorrelationThreshold.Location = new System.Drawing.Point(611, 257);
            this.crossCorrelationThreshold.Name = "crossCorrelationThreshold";
            this.crossCorrelationThreshold.Size = new System.Drawing.Size(56, 20);
            this.crossCorrelationThreshold.TabIndex = 52;
            this.crossCorrelationThreshold.Text = "0.92";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(454, 259);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 16);
            this.label3.TabIndex = 51;
            this.label3.Text = "Cross Correlation Threshold";
            this.toolTip1.SetToolTip(this.label3, "Attributes correlated to each other with a value greater than this will be remove" +
                    "d.");
            // 
            // targetCorrelationThreshold
            // 
            this.targetCorrelationThreshold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetCorrelationThreshold.Location = new System.Drawing.Point(209, 176);
            this.targetCorrelationThreshold.Name = "targetCorrelationThreshold";
            this.targetCorrelationThreshold.Size = new System.Drawing.Size(56, 20);
            this.targetCorrelationThreshold.TabIndex = 50;
            this.targetCorrelationThreshold.Text = "0.96";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 16);
            this.label2.TabIndex = 49;
            this.label2.Text = "Target Correlation Threshold";
            this.toolTip1.SetToolTip(this.label2, "Attributes with correlation to the target greater than this will be filtered.");
            // 
            // explanableModels
            // 
            this.explanableModels.AutoSize = true;
            this.explanableModels.Location = new System.Drawing.Point(11, 262);
            this.explanableModels.Name = "explanableModels";
            this.explanableModels.Size = new System.Drawing.Size(357, 17);
            this.explanableModels.TabIndex = 48;
            this.explanableModels.Text = "Only Use Classification Models that can be Explained (J48, Ridor, etc.)";
            this.toolTip1.SetToolTip(this.explanableModels, "Only use classification models that can be explained (such as J48 and Ridor)");
            this.explanableModels.UseVisualStyleBackColor = true;
            // 
            // MaxInstances
            // 
            this.MaxInstances.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MaxInstances.Location = new System.Drawing.Point(209, 153);
            this.MaxInstances.Name = "MaxInstances";
            this.MaxInstances.Size = new System.Drawing.Size(56, 20);
            this.MaxInstances.TabIndex = 47;
            // 
            // MaxInstanceLabel
            // 
            this.MaxInstanceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaxInstanceLabel.Location = new System.Drawing.Point(9, 153);
            this.MaxInstanceLabel.Name = "MaxInstanceLabel";
            this.MaxInstanceLabel.Size = new System.Drawing.Size(200, 16);
            this.MaxInstanceLabel.TabIndex = 46;
            this.MaxInstanceLabel.Text = "Maximum Number of Instances";
            // 
            // mapNullsToZeroCheckBox
            // 
            this.mapNullsToZeroCheckBox.AutoSize = true;
            this.mapNullsToZeroCheckBox.Checked = true;
            this.mapNullsToZeroCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mapNullsToZeroCheckBox.Location = new System.Drawing.Point(403, 73);
            this.mapNullsToZeroCheckBox.Name = "mapNullsToZeroCheckBox";
            this.mapNullsToZeroCheckBox.Size = new System.Drawing.Size(114, 17);
            this.mapNullsToZeroCheckBox.TabIndex = 45;
            this.mapNullsToZeroCheckBox.Text = "Map Nulls To Zero";
            this.toolTip1.SetToolTip(this.mapNullsToZeroCheckBox, "Map null-valued measures that use SUM/COUNT aggregration rules to 0.  This is mai" +
                    "nly due to the fact that these fields are not really missing, just bad data.");
            this.mapNullsToZeroCheckBox.UseVisualStyleBackColor = true;
            // 
            // decisionTreeFilteringCheckBox
            // 
            this.decisionTreeFilteringCheckBox.AutoSize = true;
            this.decisionTreeFilteringCheckBox.Checked = true;
            this.decisionTreeFilteringCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.decisionTreeFilteringCheckBox.Location = new System.Drawing.Point(424, 217);
            this.decisionTreeFilteringCheckBox.Name = "decisionTreeFilteringCheckBox";
            this.decisionTreeFilteringCheckBox.Size = new System.Drawing.Size(286, 17);
            this.decisionTreeFilteringCheckBox.TabIndex = 44;
            this.decisionTreeFilteringCheckBox.Text = "Use Model-Based (J48, Ridor) Filtering to filter attributes";
            this.toolTip1.SetToolTip(this.decisionTreeFilteringCheckBox, "Filter attributes based upon the models generating trival trees/rulesets.");
            this.decisionTreeFilteringCheckBox.UseVisualStyleBackColor = true;
            // 
            // attributesPerFold
            // 
            this.attributesPerFold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.attributesPerFold.Location = new System.Drawing.Point(720, 128);
            this.attributesPerFold.Name = "attributesPerFold";
            this.attributesPerFold.Size = new System.Drawing.Size(56, 20);
            this.attributesPerFold.TabIndex = 43;
            // 
            // targetReduction
            // 
            this.targetReduction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetReduction.Location = new System.Drawing.Point(720, 104);
            this.targetReduction.Name = "targetReduction";
            this.targetReduction.Size = new System.Drawing.Size(56, 20);
            this.targetReduction.TabIndex = 42;
            // 
            // attributesPerModel
            // 
            this.attributesPerModel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.attributesPerModel.Location = new System.Drawing.Point(208, 128);
            this.attributesPerModel.Name = "attributesPerModel";
            this.attributesPerModel.Size = new System.Drawing.Size(56, 20);
            this.attributesPerModel.TabIndex = 41;
            // 
            // samplesPerAttribute
            // 
            this.samplesPerAttribute.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.samplesPerAttribute.Location = new System.Drawing.Point(208, 104);
            this.samplesPerAttribute.Name = "samplesPerAttribute";
            this.samplesPerAttribute.Size = new System.Drawing.Size(56, 20);
            this.samplesPerAttribute.TabIndex = 40;
            // 
            // label123
            // 
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.Location = new System.Drawing.Point(400, 128);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(280, 16);
            this.label123.TabIndex = 39;
            this.label123.Text = "Maximum Attributes per Reduction Pass Fold";
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(400, 104);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(320, 16);
            this.label122.TabIndex = 38;
            this.label122.Text = "Target Attribute Reduction Rate per Reduction Pass";
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(8, 128);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(200, 16);
            this.label121.TabIndex = 37;
            this.label121.Text = "Maximum Attributes per Model";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(8, 104);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(200, 16);
            this.label21.TabIndex = 36;
            this.label21.Text = "Minimum Samples per Attribute";
            // 
            // label120
            // 
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(192, 72);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(184, 16);
            this.label120.TabIndex = 35;
            this.label120.Text = "Samples (<0 for no sampling)";
            // 
            // searchSample
            // 
            this.searchSample.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchSample.Location = new System.Drawing.Point(119, 72);
            this.searchSample.Name = "searchSample";
            this.searchSample.Size = new System.Drawing.Size(65, 20);
            this.searchSample.TabIndex = 34;
            // 
            // label119
            // 
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(8, 72);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(92, 23);
            this.label119.TabIndex = 33;
            this.label119.Text = "Sample Size:";
            // 
            // label107
            // 
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(8, 48);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(176, 23);
            this.label107.TabIndex = 32;
            this.label107.Text = "Pattern Assignment Method";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.probMethodBox);
            this.panel1.Location = new System.Drawing.Point(168, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(216, 16);
            this.panel1.TabIndex = 31;
            // 
            // radioButton1
            // 
            this.radioButton1.Checked = true;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.radioButton1.Location = new System.Drawing.Point(16, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(72, 16);
            this.radioButton1.TabIndex = 31;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Distance";
            // 
            // probMethodBox
            // 
            this.probMethodBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.probMethodBox.Location = new System.Drawing.Point(88, 0);
            this.probMethodBox.Name = "probMethodBox";
            this.probMethodBox.Size = new System.Drawing.Size(136, 16);
            this.probMethodBox.TabIndex = 32;
            this.probMethodBox.Text = "Probability Density";
            // 
            // label114
            // 
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(696, 16);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(128, 40);
            this.label114.TabIndex = 28;
            this.label114.Text = "Note: Categories will be ranked by frequency";
            // 
            // max1Categories
            // 
            this.max1Categories.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.max1Categories.Location = new System.Drawing.Point(632, 16);
            this.max1Categories.Name = "max1Categories";
            this.max1Categories.Size = new System.Drawing.Size(56, 20);
            this.max1Categories.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(400, 16);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(240, 23);
            this.label113.TabIndex = 26;
            this.label113.Text = "Max Number of Attribute Categories";
            // 
            // maxClustersPerPattern
            // 
            this.maxClustersPerPattern.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxClustersPerPattern.Location = new System.Drawing.Point(168, 24);
            this.maxClustersPerPattern.Name = "maxClustersPerPattern";
            this.maxClustersPerPattern.Size = new System.Drawing.Size(56, 20);
            this.maxClustersPerPattern.TabIndex = 21;
            // 
            // label112
            // 
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(8, 24);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(160, 23);
            this.label112.TabIndex = 20;
            this.label112.Text = "Max Clusters Per Pattern";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.bulkLoadDirectoryBox);
            this.groupBox20.Controls.Add(this.label1);
            this.groupBox20.Controls.Add(this.label110);
            this.groupBox20.Controls.Add(this.maxComputationThreads);
            this.groupBox20.Location = new System.Drawing.Point(8, 8);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(832, 72);
            this.groupBox20.TabIndex = 17;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Server Properties";
            // 
            // bulkLoadDirectoryBox
            // 
            this.bulkLoadDirectoryBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bulkLoadDirectoryBox.Location = new System.Drawing.Point(295, 43);
            this.bulkLoadDirectoryBox.Name = "bulkLoadDirectoryBox";
            this.bulkLoadDirectoryBox.Size = new System.Drawing.Size(500, 20);
            this.bulkLoadDirectoryBox.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 23);
            this.label1.TabIndex = 23;
            this.label1.Text = "Bulk Load Directory";
            this.toolTip1.SetToolTip(this.label1, "Directory accessible from both the Performance Engine and the SQL Server being us" +
                    "ed.  Only applicable when using the Bulk Load option for scoring.");
            // 
            // label110
            // 
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(16, 16);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(460, 23);
            this.label110.TabIndex = 22;
            this.label110.Text = "Maximum Number of Computation Threads (limited by the number of CPUs)";
            this.toolTip1.SetToolTip(this.label110, "Maximum number of threads to use for compute intenstive tasks.  Limited to less t" +
                    "han or equal to the number of physical CPUs.");
            // 
            // maxComputationThreads
            // 
            this.maxComputationThreads.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxComputationThreads.Location = new System.Drawing.Point(484, 17);
            this.maxComputationThreads.Name = "maxComputationThreads";
            this.maxComputationThreads.Size = new System.Drawing.Size(56, 20);
            this.maxComputationThreads.TabIndex = 21;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 30000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // ModelParameterModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "ModelParameterModule";
            this.Text = "ModelParameters";
            this.tabControl.ResumeLayout(false);
            this.modelParamTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.distanceMethodBox.ResumeLayout(false);
            this.distanceMethodBox.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage modelParamTabPage;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.TextBox reproductionRate;
        private System.Windows.Forms.TextBox mutationRate;
        private System.Windows.Forms.TextBox maxNoChange;
        private System.Windows.Forms.TextBox maxGenerations;
        private System.Windows.Forms.TextBox populationSize;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.RadioButton quasiNewtonMethod;
        private System.Windows.Forms.RadioButton simplexMethod;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.GroupBox distanceMethodBox;
        private System.Windows.Forms.TextBox attributesPerFold;
        private System.Windows.Forms.TextBox targetReduction;
        private System.Windows.Forms.TextBox attributesPerModel;
        private System.Windows.Forms.TextBox samplesPerAttribute;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox searchSample;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton probMethodBox;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox max1Categories;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox maxClustersPerPattern;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox maxComputationThreads;
        private System.Windows.Forms.Button defaultClassifiersButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox mapNullsToZeroCheckBox;
        private System.Windows.Forms.CheckBox decisionTreeFilteringCheckBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox MaxInstances;
        private System.Windows.Forms.Label MaxInstanceLabel;
        private System.Windows.Forms.CheckBox explanableModels;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bulkLoadDirectoryBox;
        private System.Windows.Forms.CheckBox useCrossCorrelation;
        private System.Windows.Forms.TextBox crossCorrelationThreshold;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox targetCorrelationThreshold;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox removeUseless;
        private System.Windows.Forms.CheckBox attributeSelectionCheckBox;
        private System.Windows.Forms.CheckBox removeRelatedCheckBox;
        private System.Windows.Forms.TextBox arffLimitTextBox;
        private System.Windows.Forms.Label label4;
    }
}