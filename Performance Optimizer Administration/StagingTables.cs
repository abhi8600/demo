using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace Performance_Optimizer_Administration
{
    public partial class StagingTables : Form, RepositoryModule, RenameNode, DropModule
    {
        string moduleName = "Staging Tables";
        string categoryName = "Warehouse";
        MainAdminForm ma;
        public TreeNode rootNode;
        private StagingTableList stagingTables;
        StagingTarget starget;

        public class StageLevelInfo
        {
            public string Dimension;
            public string Level;
            public bool Checked;
            public string Filter;
        }

        public StagingTables(MainAdminForm ma)
        {
            stagingTables = new StagingTableList(ma.getLogger());
            InitializeComponent();
            this.ma = ma;
            starget = new StagingTarget();
        }

        public List<StagingTable> getStagingTables()
        {
            List<StagingTable> list = new List<StagingTable>();
            foreach (StagingTable st in stagingTables.variableList)
                if (!st.Disabled)
                    list.Add(st);
            return (list);
        }

        public StagingTable findTable(string name)
        {
            foreach (StagingTable st in getAllStagingTables())
            {
                if (st.Name == name)
                    return st;
            }
            return null;
        }

        public StagingTable findTableFromSourceName(string name)
        {
            foreach (StagingTable st in getAllStagingTables())
            {
                name = name.ToLower();
                if (st.getDisplayName().ToLower() == name)
                    return st;
            }
            return null;
        }

        public List<StagingTable> getAllStagingTables()
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            stagingTables.refreshIfNecessary(ma);
            List<StagingTable> list = new List<StagingTable>(stagingTables.getCount());
            foreach (StagingTable st in stagingTables.variableList)
                    list.Add(st);
            return (list);
        }

        public void save(StagingTable st)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            stagingTables.updateItem(st, "", ma);
        }

        public StagingTable get(String name)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            stagingTables.refreshIfNecessary(ma);
            foreach (StagingTable st in stagingTables.variableList)
            {
                if (st.Name == name)
                    return st;
            }
            return null;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.StagingTables = stagingTables.stagingTables;
        }

        public Boolean updateGuids()
        {
            return stagingTables.updateGuids();
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.StagingTables != null)
                stagingTables.stagingTables = r.StagingTables;
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {
            for (int i = 0; i < stagingTableGridView.Rows.Count; i++)
            {
                DataGridViewRow dr = stagingTableGridView.Rows[i];
                DataGridViewCell dc = dr.Cells["NaturalKey"];
                if (dc.Value != null && (bool) dc.Value)
                {
                    ((DataGridViewCheckBoxCell)dr.Cells["ExcludeFromChecksum"]).ReadOnly = true;
                }
            }            
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (StagingTable st in stagingTables.variableList)
            {
                TreeNode tn = new TreeNode(st.Name);
                tn.ImageIndex = 27;
                tn.SelectedImageIndex = 27;
                tn.Tag = st;
                rootNode.Nodes.Add(tn);
            }
        }

        public TreeNode curNode = null;

        public bool selectedNode(TreeNode node)
        {
            if (curNode != null) implicitSave();
            ma.mainTree.ContextMenuStrip = stagingTableMenuStrip;
            if (node.Parent.Parent == null)
            {
                stagingTableGridView.Visible = false;
                stagingPanel.Visible = false;
                curNode = null;
                newStagingTableToolStripMenuItem.Visible = true;
                removeStagingTableToolStripMenuItem.Visible = false;
                updateFromSourceFileToolStripMenuItem.Visible = false;
            }
            else
            {
                if (node.Index < stagingTables.getCount())
                {
                    stagingTableGridView.Visible = true;
                    stagingPanel.Visible = true;
                    newStagingTableToolStripMenuItem.Visible = false;
                    removeStagingTableToolStripMenuItem.Visible = true;
                    updateFromSourceFileToolStripMenuItem.Visible = true;
                    List<SourceFile> list = ma.sourceFileMod.getSourceFiles();
                    dataSourceBox.Items.Clear();
                    foreach (SourceFile sf in list) dataSourceBox.Items.Add(sf.FileName);
                    setTargetTypes();
                    levelListBox.Items.Clear();
                    StagingTable st = (StagingTable)node.Tag;
                    List<StageLevelInfo> llistitems = getLevelListItems(st);
                    foreach (StageLevelInfo sli in llistitems)
                    {
                        ListViewItem lvi = new ListViewItem(new string[] { sli.Dimension, sli.Level, sli.Filter });
                        lvi.Checked = sli.Checked;
                        levelListBox.Items.Add(lvi);
                    }
                    if (st.SourceType == StagingTable.SOURCE_FILE)
                    {
                        fileButton.Checked = true;
                        dataSourceBox.Text = st.SourceFile;
                        queryButton.Checked = false;
                        queryBox.Text = "";
                        persistBox.Checked = false;
                        btnViewQuery.Enabled = false;
                    }
                    else
                    {
                        fileButton.Checked = false;
                        dataSourceBox.Text = "";
                        queryButton.Checked = true;
                        queryBox.Text = st.Query;
                        persistBox.Checked = st.PersistQuery;
                        btnViewQuery.Enabled = true;
                    }
                    if (st.Script != null)
                        btnViewScript.Enabled = true;
                    else
                        btnViewScript.Enabled = false;
                    List<LoadGroup> lgList = ma.loadGroupMod.getLoadGroups();
                    List<string> slgList = new List<string>();
                    foreach (LoadGroup lg in lgList) slgList.Add(lg.Name);
                    Util.sortedListView(loadGroupsListBox, slgList, st.LoadGroups);

                    webOptional.Checked = st.WebOptional;
                    webReadOnly.Checked = st.WebReadOnly;
                    incrementalSnapshotFact.Checked = st.IncrementalSnapshotFact;

                    if (st.SourceGroups != null && st.SourceGroups.Length > 0)
                        sourceGroupsBox.Text = st.SourceGroups;
                    else
                        sourceGroupsBox.Text = null;

                    if (st.ChecksumGroup != null && st.ChecksumGroup.Length > 0)
                        cksumGroupBox.Text = st.ChecksumGroup;
                    else
                        cksumGroupBox.Text = null;

                    transformListBox.Items.Clear();
                    if (st.Transformations != null)
                        foreach (Transformation stran in st.Transformations)
                        {
                            transformListBox.Items.Add(stran);
                        }
                    curNode = node;
                    StagingTable stable = (StagingTable)curNode.Tag;
                    if (stable != null)
                    {
                        DataTable dt = stable.getTable();
                        stagingTableGridView.DataSource = dt;
                        scdDateNameBox.Text = stable.SCDDateName;
                        setSDCDateNameBox();            
                    }

                    // Update date ID transformations
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Clear();
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Number");
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Integer");
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Varchar");
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date");
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("DateTime");
                    ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Float");
                    if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension)
                    {
                        if (ma.timeDefinition.Day)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Day");
                        if (ma.timeDefinition.Week)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Week");
                        if (ma.timeDefinition.Month)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Month");
                        if (ma.timeDefinition.Quarter)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Quarter");
                        if (ma.timeDefinition.Halfyear)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Half-year");
                        if (ma.timeDefinition.Year)
                            ((DataGridViewComboBoxColumn)stagingTableGridView.Columns["DataType"]).Items.Add("Date ID: Year");
                    }
                }
                else
                    curNode = null;
            }
            return true;
        }

        public List<StageLevelInfo> getLevelListItems(StagingTable st)
        {
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            List<String> dimensionsWithLevels = new List<string>();
            List<StageLevelInfo> result = new List<StageLevelInfo>();
            foreach (Hierarchy h in hlist)
            {
                List<string> llist = new List<string>();
                dimensionsWithLevels.Add(h.DimensionName);
                h.getChildLevelNames(llist);
                foreach (string s in llist)
                {
                    StageLevelInfo sli = new StageLevelInfo();
                    sli.Dimension = h.DimensionName;
                    sli.Level = s;
                    result.Add(sli);
                }
            }
            foreach (Dimension dim in ma.dimensionsList)
            {
                if (!dimensionsWithLevels.Contains(dim.Name))
                {
                    StageLevelInfo sli = new StageLevelInfo();
                    sli.Dimension = dim.Name;
                    sli.Level = "<Implicit>";
                    result.Add(sli);
                }
            }
            if (st.Levels != null)
            {
                foreach (StageLevelInfo sli in result)
                {
                    foreach (string[] l in st.Levels)
                    {
                        if (sli.Dimension == l[0] && sli.Level == l[1])
                        {
                            sli.Checked = true;
                        }
                    }
                    if (st.LevelFilters != null)
                        foreach (string[] f in st.LevelFilters)
                        {
                            if (f[0] == sli.Dimension && f[1] == sli.Level)
                                sli.Filter = f[2];
                        }
                }
            }
            return (result);
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void renameColumn(string oldName, string newName)
        {
            foreach (StagingTable st in stagingTables.variableList)
                st.renameColumn(oldName, newName);
        }

        public void implicitSave()
        {
            if (curNode != null)
            {
                StagingTable st = (StagingTable)curNode.Tag;
                st.updateFromDataTable((DataTable)stagingTableGridView.DataSource);
                st.Name = curNode.Text;
                st.WebReadOnly = webReadOnly.Checked;
                st.WebOptional = webOptional.Checked;
                st.IncrementalSnapshotFact = incrementalSnapshotFact.Checked;
                if (sourceGroupsBox.Text.Length > 0)
                    st.SourceGroups = sourceGroupsBox.Text;
                else
                    st.SourceGroups = null;
                if (cksumGroupBox.Text.Length > 0)
                    st.ChecksumGroup = cksumGroupBox.Text;
                else
                    st.ChecksumGroup = null;

                if (fileButton.Checked)
                {
                    st.SourceType = StagingTable.SOURCE_FILE;
                    st.SourceFile = dataSourceBox.Text;
                }
                else
                {
                    st.SourceType = StagingTable.SOURCE_QUERY;
                    st.Query = queryBox.Text;
                    st.PersistQuery = persistBox.Checked;
                }
                if (scdDateNameBox.Text.Length > 0)
                {
                    st.SCDDateName = scdDateNameBox.Text;
                }
                else
                {
                    st.SCDDateName = null;
                }
                st.Levels = new string[levelListBox.CheckedItems.Count][];
                int count = 0;
                foreach (ListViewItem lvi in levelListBox.CheckedItems)
                {
                    st.Levels[count] = new string[2];
                    st.Levels[count][0] = lvi.SubItems[0].Text;
                    st.Levels[count][1] = lvi.SubItems[1].Text;
                    count++;
                }
                List<string[]> filterList = new List<string[]>();
                foreach (ListViewItem lvi in levelListBox.Items)
                {
                    if (lvi.SubItems.Count > 2 && lvi.SubItems[2].Text.Length > 0)
                    {
                        string[] f = new string[3];
                        f[0] = lvi.SubItems[0].Text;
                        f[1] = lvi.SubItems[1].Text;
                        f[2] = lvi.SubItems[2].Text;
                        filterList.Add(f);
                    }
                }
                st.LevelFilters = filterList.ToArray();
                st.LoadGroups = new string[loadGroupsListBox.CheckedItems.Count];
                count = 0;
                foreach (ListViewItem lvi in loadGroupsListBox.CheckedItems)
                {
                    st.LoadGroups[count] = lvi.Text;
                    count++;
                }
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return stagingTablePage;
            }
            set
            {
            }
        }

        public void setRepositoryDirectory(string d)
        {
            stagingTables.updateRepositoryDirectory(d, ma);
        }
        #endregion

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode && curNode != null)
            {
                curNode.Text = e.Label;
                stagingTables.stagingTables[curNode.Index].Name = e.Label;
            }
        }

        #endregion

        private void newStagingTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode tn = new TreeNode("New Table");
            tn.ImageIndex = 27;
            tn.SelectedImageIndex = 27;
            rootNode.Nodes.Add(tn);
            StagingTable st = new StagingTable("New Table", ma.currentReferenceID++);
            tn.Tag = st;
            stagingTables.Add(st);
        }

        private void dataErrorHandler(object sender, DataGridViewDataErrorEventArgs args)
        {
        }

        public string getStagingNameFromSourceFileName(string filename, bool liveAccess, MainAdminForm maf)
        {
            int dot = filename.LastIndexOf('.');
            if (dot > 0)
            {
                filename = filename.Remove(dot);
            }
            if (liveAccess)
                return filename;
            ApplicationBuilder appBuilderInstance = new ApplicationBuilder(maf);
            int maxTableNameLen = appBuilderInstance.getMaxPhysicalTableNameLength();
            string fname = filename;
            fname = Regex.Replace(filename, @"[^\w\. -_\u0080-\uFFCF]", "");
            if (maf.builderVersion >= 29)
            {
                string stagingTableName = Util.generatePhysicalName(fname, maf.builderVersion, true, maxTableNameLen - 3);
                return Util.generatePhysicalName("ST " + stagingTableName, maf.builderVersion, true, maxTableNameLen);
            }
            else if (maf.builderVersion >= 26)
            {
                string stagingTableName = Util.generatePhysicalName(fname, maf.builderVersion, true, maxTableNameLen);
                return Util.generatePhysicalName("ST " + stagingTableName);
            }
            else
            {
                string stagingTableName = "ST " + fname;
                return Util.generatePhysicalName(stagingTableName, maf.builderVersion, true, maxTableNameLen);
            }
        }
        
        public StagingTable addStagingTable(SourceFile sf, bool unmapColumnsNotPresent, bool liveAccess)
        {
            return addStagingTable(sf, unmapColumnsNotPresent, liveAccess, false);
        }
        public StagingTable addStagingTable(SourceFile sf, bool unmapColumnsNotPresent, bool liveAccess, bool checkDuplicateColumns)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            string stagingTableName = getStagingNameFromSourceFileName(sf.FileName, liveAccess, ma);
            string filename = sf.FileName;
            int dot = filename.LastIndexOf('.');
            if (dot > 0)
            {
                filename = filename.Remove(dot);
            }
            ApplicationBuilder appBuilderInstance = new ApplicationBuilder(ma);
            int maxTableNameLen = appBuilderInstance.getMaxPhysicalTableNameLength();
            string stagingTableLogicalName = stagingTableName;
            if (ApplicationBuilder.ContainsNonASCIICharacters(filename) && ma.builderVersion >= Repository.I18NLogicalNames)
            {
                stagingTableLogicalName = filename;
            }
            else if (maxTableNameLen > 0 && (filename.Length+3) > maxTableNameLen && ma.builderVersion >= 24) //filename.length + 3 is because ST_ is prefixed to staging table name
            {
                stagingTableLogicalName = filename;
            }
            stagingTables.refreshIfNecessary(ma);
            setupNodes(rootNode);
            TreeNode tn = null;
            //Make sure the staging table does not already exist
            string lname = stagingTableName.ToLower();
            foreach (TreeNode stn in rootNode.Nodes)
            {
                if (stn.Text.ToLower() == lname)
                {
                    tn = stn;
                    break;
                }
            }
            if (tn == null)
            {
                tn = new TreeNode(stagingTableName);
                tn.ImageIndex = 27;
                tn.SelectedImageIndex = 27;
                StagingTable st = new StagingTable(stagingTableName, ma.currentReferenceID++);
                st.LogicalName = stagingTableLogicalName;
                st.SourceFile = sf.FileName;
                st.Columns = new StagingColumn[sf.Columns.Length];
                for (int i = 0; i < sf.Columns.Length; i++)
                {
                    st.Columns[i] = new StagingColumn();
                    if (sf.Columns[i].Name != null && sf.Columns[i].Name.Length > appBuilderInstance.getMaxPhysicalColumnNameLength() && ma.builderVersion >= 28)
                    {
                        st.Columns[i].PhysicalName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(ma), sf.Columns[i].Name + ApplicationBuilder.PHYSICAL_SUFFIX, appBuilderInstance.getMaxPhysicalColumnNameLength(), ma.builderVersion);
                        st.Columns[i].Name = sf.Columns[i].Name;
                    }
                    else
                    {
                        st.Columns[i].Name = sf.Columns[i].Name;
                    }
                    st.Columns[i].DataType = sf.Columns[i].DataType;
                    st.Columns[i].Width = sf.Columns[i].Width;
                    st.Columns[i].SourceFileColumn = sf.Columns[i].Name;
                }
                st.LevelFilters = new string[0][];
                st.Levels = new string[0][];
                st.LoadGroups = new string[0];
                st.Transformations = new Transformation[0];
                stagingTables.updateItem(st, "", ma);
                tn.Tag = st;
                rootNode.Nodes.Add(tn);
                ma.mainTree.SelectedNode = tn;
                return (st);
            }
            else
            {
                updateStagingColumnsFromSourceFile((StagingTable)tn.Tag, sf, unmapColumnsNotPresent, checkDuplicateColumns);
                return ((StagingTable)tn.Tag);
            }
        }

        #region DropModule Members

        public void doDrop(TreeNode sourceNode, TreeNode targetNode)
        {
            if (ma.getMainNode(sourceNode).Text == "Staging Tables" && sourceNode.Text != "Staging Tables")
            {
                List<StagingTable> list = stagingTables.variableList;
                int sourceNodeIndex = rootNode.Nodes.IndexOf(sourceNode);
                int targetNodeIndex = 0;
                if (targetNode.Text != "Staging Tables")
                {
                    targetNodeIndex = rootNode.Nodes.IndexOf(targetNode);
                }
                if (sourceNodeIndex == targetNodeIndex)
                    return;
                list[sourceNodeIndex].amIBeingDragged = true;
                rootNode.Nodes.RemoveAt(sourceNodeIndex);
                StagingTable sourceStagingTable = list[sourceNodeIndex];
                list.RemoveAt(sourceNodeIndex);
                rootNode.Nodes.Insert(targetNodeIndex, sourceNode);
                list.Insert(targetNodeIndex, sourceStagingTable);
                list[targetNodeIndex].amIBeingDragged = false;
                if (sourceNodeIndex >= targetNodeIndex)
                    selectedNode(rootNode.Nodes[sourceNodeIndex + 1]);
                else if (sourceNodeIndex < targetNodeIndex)
                    selectedNode(rootNode.Nodes[sourceNodeIndex]);
            }
            else if (ma.getMainNode(sourceNode).Text == "Data Sources" && sourceNode.Text != "Data Sources")
            {
                List<SourceFile> list = ma.sourceFileMod.getSourceFiles();
                foreach (SourceFile sf in list)
                {
                    if (sf.FileName == sourceNode.Text)
                    {
                        addStagingTable(sf, false, false);
                        break;
                    }
                }
            }            
        }

        public DragDropEffects startDrag(TreeNode tn)
        {
            if (curNode == null || tn == null) return (DragDropEffects.None);
            if (curNode.Index != tn.Index) return (DragDropEffects.None);
            if ((tn.Parent != null && tn.Parent.Text == "Staging Tables") || (tn.Parent != null && tn.Parent.Text == "Staging Tables"))
                return (DragDropEffects.Move);
            if ((tn.Parent != null && tn.Parent.Text == "Data Sources") || (tn.Parent != null && tn.Parent.Text == "Data Sources"))
                return (DragDropEffects.Copy);
            return (DragDropEffects.None);            
        }

        #endregion

        private void levelListBox_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked == true)
            {
                string dim = e.Item.SubItems[0].Text;
                foreach (ListViewItem lvi in levelListBox.Items)
                {
                    if (lvi != e.Item && lvi.SubItems[0].Text == dim)
                        lvi.Checked = false;
                }
            }
        }

        public List<string> getTargetTypes()
        {
            List<string> tlist = new List<string>();
            tlist.Add("None");
            tlist.Add("Measure");
            foreach (Dimension d in ma.dimensionsList)
                if (d != null && d.Name != null && d.Name != "" && !tlist.Contains(d.Name)) tlist.Add(d.Name);
            return (tlist);
        }

        private void setTargetTypes()
        {
            starget.targetListBox.Items.Clear();
            foreach (string s in getTargetTypes())
                starget.targetListBox.Items.Add(s);
        }

        private void stagingTableGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex == ((DataGridView)sender).RowCount-1)
                return;
            if (e.ColumnIndex == stagingTableGridView.Columns["Transformation"].Index)
            {
                Object o = stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                TransformationForm stf = new TransformationForm(ma,
                    (o == System.DBNull.Value ? null : (Transformation)o),
                    new string[] { "None", "Expression", "External Table Lookup", "Rank", "Date ID" }, false);
                stf.setColumnLabel("Column: "+(string)stagingTableGridView.Rows[e.RowIndex].Cells["ColumnName"].Value);
                DialogResult dr = stf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = stf.getTransformation();
                }
                stf.Close();
            }
            else if (e.ColumnIndex == stagingTableGridView.Columns["GrainInfo"].Index)
            {
                Object o = stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                StagingColumnGrainInfoDisplay scgDisplay = (o == System.DBNull.Value ? null : (StagingColumnGrainInfoDisplay)o);
                List<StagingColumnGrainInfo> scfList = new List<StagingColumnGrainInfo>();
                if (scgDisplay != null && scgDisplay.scGrainInfo != null && scgDisplay.scGrainInfo.Length > 0)
                {
                    for (int i = 0; i < scgDisplay.scGrainInfo.Length; i++)
                    {
                        scfList.Add(scgDisplay.scGrainInfo[i]);
                    }
                }
                GetStagingColumnGrainInfo scf = new GetStagingColumnGrainInfo(ma, scfList, (string)stagingTableGridView.Rows[e.RowIndex].Cells["ColumnName"].Value);
                DialogResult dr = scf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = scf.getStagingColumnGrainInfoDisplay();
                }
                scf.Close();
            }
            else if (e.ColumnIndex == stagingTableGridView.Columns["TargetTypes"].Index)
            {
                setTargetTypes();
                Rectangle rect = stagingTableGridView.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                Point p = stagingTableGridView.PointToScreen(new Point(rect.Left, rect.Top));
                starget.formatStagingTarget(false, null);
                starget.Left = p.X;
                starget.Top = p.Y;
                starget.Width = rect.Width;
                starget.formatStagingTarget(false, null);
                string type = (string)stagingTableGridView.Rows[e.RowIndex].Cells["DataType"].Value;
                starget.setDataType(type);
                Object o = stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                starget.setCheckedItems(o == System.DBNull.Value ? null : (List<string>)o);
                starget.ShowDialog();
                stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = starget.getCheckedItems();
            }
        }

        private void addTransformButton_Click(object sender, EventArgs e)
        {
            StagingTable st = (StagingTable)curNode.Tag;
            TransformationForm stf = new TransformationForm(ma, new Procedure(), new string[] { "Procedure", "Household", "Java" }, true);
            DialogResult dr = stf.ShowDialog();
            if (dr == DialogResult.OK)
            {
                Transformation[] newstf = new Transformation[st.Transformations == null ? 1 : st.Transformations.Length + 1];
                if (st.Transformations != null)
                    st.Transformations.CopyTo(newstf, 0);
                Transformation newestStf = stf.getTransformation();
                if (newestStf != null)
                {
                    newstf[st.Transformations == null ? 0 : st.Transformations.Length] = newestStf;
                    st.Transformations = newstf;
                    transformListBox.Items.Add(stf.getTransformation());
                }
            }
            stf.Close();
        }

        private void removeTransformButton_Click(object sender, EventArgs e)
        {
            if (transformListBox.SelectedItem != null)
            {
                StagingTable st = (StagingTable)curNode.Tag;
                List<Transformation> newList = new List<Transformation>(st.Transformations);
                newList.RemoveAt(transformListBox.SelectedIndex);
                st.Transformations = newList.ToArray();
                transformListBox.Items.RemoveAt(transformListBox.SelectedIndex);
            }
        }

        private void editTransformButton_Click(object sender, EventArgs e)
        {
            if (transformListBox.SelectedItem == null)
                return;
            StagingTable st = (StagingTable)curNode.Tag;
            Transformation tran = st.Transformations[transformListBox.SelectedIndex];
            TransformationForm stf = new TransformationForm(ma, tran, new string[] { "None", "Procedure", "Household", "Java" }, true);
            DialogResult dr = stf.ShowDialog();
            if (dr == DialogResult.OK)
            {
                st.Transformations[transformListBox.SelectedIndex] = stf.getTransformation();
                transformListBox.Items[transformListBox.SelectedIndex] = stf.getTransformation();
            }
            stf.Close();
        }

        public void removeStagingTable(string name)
        {
            StagingTable stToBeRemoved = null;
            stagingTables.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            stagingTables.refreshIfNecessary(ma);
            foreach (StagingTable st in stagingTables.variableList)
            {
                if (st.Name == name)
                {
                    stToBeRemoved = st;
                    break;
                }
            }
            if (stToBeRemoved != null)
            {
                stagingTables.deleteItem(stToBeRemoved, ma);
            }
            setupNodes(rootNode);
        }

        private void removeStagingTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode != null && curNode.Parent == rootNode)
            {
                removeStagingTable(curNode.Text);
            }
        }

        private void setFilterButton_Click(object sender, EventArgs e)
        {
            if (levelListBox.SelectedItems.Count == 1)
            {
                ListViewItem lvi = levelListBox.SelectedItems[0];
                GetStagingFilter gsf = new GetStagingFilter(ma, (lvi.SubItems.Count > 2) ? lvi.SubItems[2].Text : "");
                DialogResult dr = gsf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    if (lvi.SubItems.Count == 2)
                        lvi.SubItems.Insert(2, new ListViewItem.ListViewSubItem(lvi, gsf.selectedFilter == "<None>" ? "" : gsf.selectedFilter));
                    else
                        lvi.SubItems[2].Text = (string)gsf.selectedFilter == "<None>" ? "" : gsf.selectedFilter;
                }
            }
        }

        private void stagingTableGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (stagingTableGridView.Columns[e.ColumnIndex].Name == "TargetTypes")
            {
                if (e.Value == System.DBNull.Value) return;
                List<string> l = (List<string>)e.Value;
                if (l == null) return;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < l.Count; i++)
                {
                    if (i > 0) sb.Append(',');
                    sb.Append(l[i]);
                }
                e.Value = sb.ToString();
                e.FormattingApplied = true;
            }            
        }

        private void stagingTableGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == stagingTableGridView.Columns["NaturalKey"].Index)
            {
                if (stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null ||
                        stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals(System.DBNull.Value) ||
                        ((bool) stagingTableGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == false))
                {
                    stagingTableGridView.Rows[e.RowIndex].Cells[stagingTableGridView.Columns["ExcludeFromChecksum"].Index].Value = false;
                    stagingTableGridView.Rows[e.RowIndex].Cells[stagingTableGridView.Columns["ExcludeFromChecksum"].Index].ReadOnly = true;
                }
                else
                {
                    stagingTableGridView.Rows[e.RowIndex].Cells[stagingTableGridView.Columns["ExcludeFromChecksum"].Index].ReadOnly = false;
                }                
            }
        }

        private void updateFromSourceFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode != null && curNode.Parent == rootNode)
            {
                StagingTable st = (StagingTable)curNode.Tag;
                string sourceFileName = st.SourceFile;
                if (sourceFileName != null)
                {
                    SourceFile associatedSourceFile = null;
                    TreeNode sourceFilesNode = ma.getMainNode("Data Sources");
                    foreach (TreeNode sfNode in sourceFilesNode.Nodes)
                    {
                        if (sfNode.Text == sourceFileName)
                        {
                            associatedSourceFile = (SourceFile)sfNode.Tag;
                            break;
                        }
                    }
                    if (associatedSourceFile != null)
                    {
                        List<BuildAction> buildActions = updateStagingColumnsFromSourceFile(st, associatedSourceFile, false, false);
                        BuildApplicationReportForm bar = new BuildApplicationReportForm(buildActions);
                        bar.Show(this);
                    }
                }
            }
        }

        private List<BuildAction> updateStagingColumnsFromSourceFile(StagingTable st, SourceFile sf, bool unmapColumnsNotPresent, bool checkDuplicate)
        {
            List<BuildAction> buildActions = new List<BuildAction>();
            if (st.SourceFile == null || st.SourceFile.ToLower() != sf.FileName.ToLower() || sf.Columns == null || sf.Columns.Length == 0)
            {
                return buildActions;
            }

            Dictionary<string, StagingColumn> stagingColumnsBySourceFileColumnName = new Dictionary<string, StagingColumn>();
            Dictionary<string, SourceFile.SourceColumn> sourceFileColumnBySourceFileColumnName = new Dictionary<string, SourceFile.SourceColumn>();

            List<StagingColumn> stagingColumnsToBeRemoved = new List<StagingColumn>();
            List<StagingColumn> stagingColumnsToBeAdded = new List<StagingColumn>();
            List<StagingColumn> stagingColumnsUnmapped = new List<StagingColumn>();

            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.SourceFileColumn != null && !stagingColumnsBySourceFileColumnName.ContainsKey(sc.SourceFileColumn))
                {
                    stagingColumnsBySourceFileColumnName.Add(sc.SourceFileColumn, sc);
                }
            }

            foreach (SourceFile.SourceColumn sfc in sf.Columns)
            {
                sourceFileColumnBySourceFileColumnName.Add(sfc.Name, sfc);
                if (!stagingColumnsBySourceFileColumnName.ContainsKey(sfc.Name))
                {
                    bool found = false;
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.Name == sfc.Name && (sc.Transformations == null || sc.Transformations.Length == 0) &&
                            (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0) && sc.DataType == sfc.DataType)
                        {
                            sc.SourceFileColumn = sfc.Name;
                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        continue;
                    }

                    string scName= sfc.Name;

                    if (checkDuplicate)
                    {
                        int dupIdx = 0;
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.Name == scName)
                            {
                                dupIdx++;
                            }
                        }
                        if (dupIdx > 0)
                        {
                            scName = sfc.Name + " " + dupIdx;
                        }
                    }
                    
                    StagingColumn newsc = new StagingColumn();
                    newsc.Name = scName;
                    newsc.DataType = sfc.DataType;
                    newsc.Width = sfc.Width;
                    newsc.SourceFileColumn = sfc.Name;
                    newsc.TargetTypes = new string[0];
                    stagingColumnsToBeAdded.Add(newsc);
                }
            }

            List<StagingColumn> toBeRemoved = new List<StagingColumn>();
            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.SourceFileColumn == null)
                {
                    //Leave alone the manually added columns that do not map to any source file column
                    continue;
                }
                if (!sourceFileColumnBySourceFileColumnName.ContainsKey(sc.SourceFileColumn))
                {
                    if (unmapColumnsNotPresent)
                    {
                        sc.SourceFileColumn = null;
                        stagingColumnsUnmapped.Add(sc);
                    }
                    else
                        stagingColumnsToBeRemoved.Add(sc);
                }
                else
                {
                    SourceFile.SourceColumn sfc = sourceFileColumnBySourceFileColumnName[sc.SourceFileColumn];
                    if (sc.DataType != sfc.DataType)
                    {
                        bool createdDateID = false;
                        if (ma.timeDefinition != null && sc.NaturalKey && sc.DataType.StartsWith("Date ID"))
                        {
                            if (sfc.DataType == "DateTime" || sfc.DataType == "Date")
                                createdDateID = true;
                            else
                            {
                                foreach (StagingColumn ssc in st.Columns)
                                {
                                    if (ssc == sc || ssc.SourceFileColumn == null)
                                        continue;
                                    if (ssc.SourceFileColumn == sc.SourceFileColumn)
                                    {
                                        // If this column refers to a source column mapped somewhere else then it's illegal
                                        // A datetime changed it's type and needs to be removed
                                        createdDateID = true;
                                        toBeRemoved.Add(sc);
                                        break;
                                    }
                                }
                            }
                        }
                        if (!createdDateID)
                        {
                            sc.DataType = sfc.DataType;
                            buildActions.Add(new BuildAction("Update data type to " + sc.DataType + " for staging column " + sc.Name, MessageType.BASIC));
                        }
                    }
                    if (sc.Width != sfc.Width)
                    {
                        sc.Width = sfc.Width;
                        buildActions.Add(new BuildAction("Update width to " + sc.Width + " for staging column " + sc.Name, MessageType.BASIC));
                    }
                    if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                    {
                        sc.SourceFileColumn = sfc.Name;
                    }
                }
            }
            if (toBeRemoved.Count > 0)
            {
                List<StagingColumn> newlist = new List<StagingColumn>();
                foreach (StagingColumn sc in st.Columns)
                    if (!toBeRemoved.Contains(sc))
                        newlist.Add(sc);
                st.Columns = newlist.ToArray();
            }

            if (stagingColumnsToBeRemoved.Count > 0 || stagingColumnsToBeAdded.Count > 0)
            {
                List<StagingColumn> newStagingColumnList = new List<StagingColumn>(st.Columns);
                foreach (StagingColumn sc in stagingColumnsToBeRemoved)
                {
                    newStagingColumnList.Remove(sc);
                    buildActions.Add(new BuildAction("Removed staging column " + sc.Name + " from staging table " + st.Name, MessageType.BASIC));
                }
                foreach (StagingColumn sc in stagingColumnsUnmapped)
                {
                    buildActions.Add(new BuildAction("Unmapped staging column " + sc.Name + " from staging table " + st.Name, MessageType.BASIC));
                }
                foreach (StagingColumn sc in stagingColumnsToBeAdded)
                {
                    newStagingColumnList.Add(sc);
                    buildActions.Add(new BuildAction("Added staging column " + sc.Name + " to staging table " + st.Name, MessageType.BASIC));
                }
                st.Columns = newStagingColumnList.ToArray();
            }

            // Reorder if Infobright (bulk loader cannot reorder from source file to staging)
            if (ma.connection.connectionList.Count > 0 && ma.connection.connectionList[0].Type == "Infobright")
            {
                List<StagingColumn> newStagingColumnList = new List<StagingColumn>();
                List<StagingColumn> added = new List<StagingColumn>();
                foreach (SourceFile.SourceColumn sfc in sf.Columns)
                {
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.SourceFileColumn == sfc.Name)
                        {
                            newStagingColumnList.Add(sc);
                            added.Add(sc);
                            break;
                        }
                    }
                }
                foreach (StagingColumn sc in st.Columns)
                {
                    if (!added.Contains(sc))
                        newStagingColumnList.Add(sc);
                }
                st.Columns = newStagingColumnList.ToArray();
            }

            DataTable dat = st.getTable();
            stagingTableGridView.DataSource = dat;

            return buildActions;
        }

        private void upTransformButton_Click(object sender, EventArgs e)
        {
            if (transformListBox.SelectedItem == null) return;
            StagingTable st = (StagingTable)curNode.Tag;
            int index = transformListBox.SelectedIndex;
            if (index == 0) return;
            Transformation t = st.Transformations[index];

            List<Transformation> newList = new List<Transformation>(st.Transformations);
            newList.RemoveAt(index);
            newList.Insert(index - 1, t);
            st.Transformations = newList.ToArray();

            transformListBox.Items.RemoveAt(index);
            transformListBox.Items.Insert(index - 1, t);


        }

        private void downTransformButton_Click(object sender, EventArgs e)
        {
            if (transformListBox.SelectedItem == null) return;
            StagingTable st = (StagingTable)curNode.Tag;
            int index = transformListBox.SelectedIndex;
            if (index == (st.Transformations.Length - 1)) return;
            Transformation t = st.Transformations[index];

            List<Transformation> newList = new List<Transformation>(st.Transformations);
            newList.RemoveAt(index);
            newList.Insert(index + 1, t);
            st.Transformations = newList.ToArray();

            transformListBox.Items.RemoveAt(index);
            transformListBox.Items.Insert(index + 1, t);
        }

        private void setTargetTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (stagingTableGridView.SelectedRows == null || stagingTableGridView.SelectedRows.Count == 0)
            {
                return;
            }

            setTargetTypes();
            starget.Left = stagingTableGridView.Location.X;
            starget.Top = stagingTableGridView.Location.Y;
            starget.Width = starget.Height;
            starget.formatStagingTarget(true, ma);
            starget.setCheckedItems(null);
            DialogResult result = starget.ShowDialog();
            if (result == DialogResult.OK)
            {
                List<string> targets = starget.getCheckedItems();
                foreach (DataGridViewRow dr in stagingTableGridView.SelectedRows)
                {
                    dr.Cells["TargetTypes"].Value = targets;
                }
            }
        }

        private void stagingTableGridView_SelectionChanged(object sender, EventArgs e)
        {
            bool visible = stagingTableGridView.SelectedRows != null && stagingTableGridView.SelectedRows.Count > 0;
            setTargetTypeToolStripMenuItem.Visible = visible;
            setGrainInfoToolStripMenuItem.Visible = visible;
        }

        private void setGrainInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (stagingTableGridView.SelectedRows == null || stagingTableGridView.SelectedRows.Count == 0)
            {
                return;
            }

            GetStagingColumnGrainInfo scolGrainInfo = new GetStagingColumnGrainInfo(ma, new List<StagingColumnGrainInfo>(), "Multiple");
            DialogResult result = scolGrainInfo.ShowDialog();
            if (result == DialogResult.OK)
            {
                foreach (DataGridViewRow dr in stagingTableGridView.SelectedRows)
                {
                    dr.Cells["GrainInfo"].Value = scolGrainInfo.getStagingColumnGrainInfoDisplay();
                }
            }
        }

        private void stagingTableGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            stagingTableGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void stagingTableGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            stagingTableGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        public List<StagingTable> getStagingTablesAtLevel(string dimension, string level)
        {
            List<StagingTable> stlist = new List<StagingTable>();
            foreach (StagingTable st in stagingTables.variableList)
            {
                foreach (string[] l in st.Levels)
                    if (l[0] == dimension && l[1] == level)
                        stlist.Add(st);
            }
            return (stlist);
        }

        private void btnViewQuery_Click(object sender, EventArgs e)
        {
            ViewStagingTableQueryDialog dg = new ViewStagingTableQueryDialog(queryBox.Text);
            DialogResult dr = dg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                queryBox.Text = dg.getQuery();
            }
            dg.Close();
        }

        private void queryButton_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
                btnViewQuery.Enabled = true;
            else
                btnViewQuery.Enabled = false;
        }

        private void StagingTables_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

        private void setSDCDateNameBox()
        {
               StagingTable stable = (StagingTable)curNode.Tag;
               List<String> dateColNames = getDateTimeColumnNames();
               if (dateColNames != null && dateColNames.Count > 0)
               {
                    scdDateNameBox.Enabled = true;
                    //prv_value is used when user is already selected a value and new values are added bcoz of date/datetime entries added to grid or user removes/changes a value in scdNameBox(i.e. making "" / unselect value).. should not change current selection
                    //stable.SCDDateName is used when user is switching the staging table nodes..
                    String prv_value = scdDateNameBox.Text;
                    scdDateNameBox.Items.Clear();
                    scdDateNameBox.Items.AddRange(dateColNames.ToArray());                  
                    scdDateNameBox.Text =  prv_value=="" || dateColNames.Contains(prv_value)? prv_value : stable.SCDDateName != null && dateColNames.Contains(stable.SCDDateName)? stable.SCDDateName : "";// dateColNames[0];                    
                }
                else
                {
                    scdDateNameBox.Enabled = false;
                    scdDateNameBox.Text = "";
                }
        }

        private void stagingTableGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (stagingTableGridView.Columns[e.ColumnIndex].Name == "DataType" || stagingTableGridView.Columns[e.ColumnIndex].Name=="SourceFileColumn")
            {
                 setSDCDateNameBox();
            }
        }

        private void stagingTableGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
             setSDCDateNameBox();
        }

        public List<String> getDateTimeColumnNames()
        {
            List<String> columnList = new List<String>();
            StagingTable st = (StagingTable)curNode.Tag;
            foreach (DataGridViewRow row in stagingTableGridView.Rows)
            {
                if (row.Cells["DataType"].Value!=null && (row.Cells["DataType"].Value.Equals("Date") || row.Cells["DataType"].Value.Equals("DateTime")))
                {
                    //File-based and SourceFileColumn is not there .. continue
                    if (fileButton.Checked==true && row.Cells["SourceFileColumn"].Value == System.DBNull.Value)
                    {
                        continue;
                    }
                    columnList.Add((String)row.Cells["ColumnName"].Value);
                }
            }
            return columnList;
        }

        private void fileButton_Click(object sender, EventArgs e)
        {
            setSDCDateNameBox();
        }

        private void queryButton_Click(object sender, EventArgs e)
        {
            setSDCDateNameBox();
        }

        private void btnViewScript_Click(object sender, EventArgs e)
        {
            StagingTable st = (StagingTable)curNode.Tag;
            ViewStagingTableScriptDialog dg = new ViewStagingTableScriptDialog(st.Script);
            dg.ShowDialog();
        }

        private void incrementalSnapshotFact_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                btnViewSnapshotDeleteKeys.Enabled = true;
            else
                btnViewSnapshotDeleteKeys.Enabled = false;
        }

        private void btnViewSnapshotDeleteKeys_Click(object sender, EventArgs e)
        {
            StagingTable st = (StagingTable)curNode.Tag;
            StagingTableSnapshotDeleteKeyForm snapshotDeleteKeyfrm = new StagingTableSnapshotDeleteKeyForm(st.SnapshotDeleteKeys);
            snapshotDeleteKeyfrm.Show();
        }

        public bool removeStagingTable(StagingTable st)
        {
            removeStagingTable(st.Name);
            return true;
        }

        public void updateStagingTable(StagingTable st, string username)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            if (st.TableSource != null)
            {
                if (st.TableSource.Filters != null)
                {
                    List<TableSource.TableSourceFilter> filters = new List<TableSource.TableSourceFilter>();
                    foreach(TableSource.TableSourceFilter tsf in st.TableSource.Filters)
                    {
                        if (tsf.Filter != null && tsf.Filter.Length > 0)
                            filters.Add(tsf);
                    }
                    if (filters.Count == 0)
                        st.TableSource.Filters = null;
                    else
                        st.TableSource.Filters = filters.ToArray();
                }
            }
            stagingTables.updateItem(st, username, "StagingTable", ma);
            setupNodes(rootNode);
        }

        public void updateStagingTableWithoutLock(StagingTable st, string username)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            stagingTables.updateItemWithoutLock(st, username, "StagingTable", ma);
            setupNodes(rootNode);
        }

        public void isNewOrUpToDate(StagingTable st)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            stagingTables.isNewOrUpToDate(st, "StagingTable", ma);
        }
    }
}