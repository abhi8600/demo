using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class StagingTarget : Form, ResizeMemoryForm
    {
        private string dataType;
        private MainAdminForm ma;
        private string windowName = "TargetType";
        private bool fullWindow = false;

        public StagingTarget()
        {
            InitializeComponent();
        }

        public void formatStagingTarget(bool fullwindow, MainAdminForm ma)
        {
            this.fullWindow = fullwindow;
            this.ma = ma;
            okButton.Visible = fullWindow;
            cancelButton.Visible = fullWindow;
            if (fullwindow)
            {
                this.Text = "Set Target Types";
                this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
                this.StartPosition = FormStartPosition.Manual;
                resetWindowFromSettings();
            }
            else
            {
                this.Text = null;
                this.StartPosition = FormStartPosition.Manual;
                this.FormBorderStyle = FormBorderStyle.None;
                this.Height = this.targetListBox.Height + 8;
            }
        }

        private void StagingTarget_Leave(object sender, EventArgs e)
        {
            if (!fullWindow) closeWin();
        }

        private void targetListBox_MouseLeave(object sender, EventArgs e)
        {
            Rectangle rect = new Rectangle(0, 0, targetListBox.Width + 25, targetListBox.Height);
            if (!rect.Contains(targetListBox.PointToClient(Control.MousePosition)) && !fullWindow)
            {
                closeWin();
            }
        }

        private void targetListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !fullWindow)
                closeWin();
        }

        private void closeWin()
        {
            this.Close();
        }

        private void targetListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                for (int i = 1; i < targetListBox.Items.Count; i++)
                {
                    targetListBox.SetItemChecked(i, false);
                }
            }
            else
            {
                targetListBox.SetItemChecked(0, false);
            }
        }

        public void setDataType(string type)
        {
            this.dataType = type;
        }

        public void setCheckedItems(List<string> items)
        {
            if (items == null || items.Count == 0)
            {
                targetListBox.SetItemChecked(0, true);
            }
            if (!targetListBox.Items.Contains("Measure"))
            {
                targetListBox.Items.Insert(1, "Measure");
            }
            for (int i = 0; i < targetListBox.Items.Count; i++)
            {
                if (items == null)
                    targetListBox.SetItemChecked(i, false);
                else if (items.Contains((string)targetListBox.Items[i]))
                    targetListBox.SetItemChecked(i, true);
                else
                    targetListBox.SetItemChecked(i, false);
            }
        }

        public List<string> getCheckedItems()
        {
            List<string> result = new List<string>();
            foreach (string s in targetListBox.CheckedItems)
                if (s != "None") result.Add(s);
            return (result);
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            checkWindowSize();
            base.OnResizeEnd(e);
        }

       

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            if (!fullWindow) return;
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty)
            {
                this.Size = new Size(sz.Width, sz.Height);
            }
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
            else
            {
                this.StartPosition = FormStartPosition.CenterParent;
            }
            checkWindowSize();
        }

        private void checkWindowSize()
        {
            if (this.Height != this.okButton.Location.Y + 55)
                this.Height = this.okButton.Location.Y + 55;
            if (this.Width < this.cancelButton.Location.X + 75)
                this.Width = this.cancelButton.Location.X + 75;

        }

        public void saveWindowSettings()
        {
            if (!fullWindow) return;
            ma.addPopupWindowStatus(windowName, this.Size, this.Location, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void StagingTarget_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fullWindow) saveWindowSettings();
        }

    }
}