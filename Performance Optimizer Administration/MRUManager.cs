using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public interface IMRUClient
    {
        void OpenMRUFile(string fName);
    }

    class MRUManager
    {
        private System.Windows.Forms.Form ownerForm;  // owner form
        private MenuItem menuItemMRU;           // Recent Files menu item
        private MenuItem menuItemParent;        // Recent Files menu item parent
        //private string registryPath;            // Registry path to keep MRU list
        private int maxNumberOfFiles = 10;      // maximum number of files in MRU list
        private int maxDisplayLength = 100;      // maximum length of file name for display
        private string currentDirectory;        // current directory
        private ArrayList mruList;              // MRU list (file names)
        private const string regEntryName = "file";  // entry name to keep MRU (file0, file1...)

        public void Initialize(System.Windows.Forms.Form owner, MenuItem mruItem, string[] MRUArray)
        {
            ownerForm = owner;

            // check if owner form implements IMRUClient interface
            if (!(owner is IMRUClient))
            {
                throw new Exception("MRUManager: Owner form doesn't implement IMRUClient interface");
            }

            // keep reference to MRU menu item
            menuItemMRU = mruItem;

            // keep reference to MRU menu item parent
            try
            {
                menuItemParent = (MenuItem)menuItemMRU.Parent;
            }
            catch
            {
            }

            if (menuItemParent == null)
            {
                throw new Exception("MRUManager: Cannot find parent of MRU menu item");
            }

            mruList = new ArrayList();
            if (MRUArray != null && MRUArray.GetLength(0) > 0)
            {

                foreach (string fName in MRUArray)
                    if (File.Exists(fName)) mruList.Insert(0, fName);
            }


            // Not using registry for now
            // keep Registry path adding MRU key to it
            /* try
             {
                 registryPath = regPath;
                 if (registryPath.EndsWith("\\"))
                     registryPath += "MRU";
                 else
                     registryPath += "\\MRU";
             }
             catch
             {
                 throw new Exception("MRUManager: registryPath not specified");
             } */

            // keep current directory in the time of initialization
            currentDirectory = Directory.GetCurrentDirectory();

            // subscribe to MRU parent Popup event
            menuItemParent.Popup += new EventHandler(this.OnMRUParentPopup);

            // subscribe to owner form Closing event
            ownerForm.Closing += new System.ComponentModel.CancelEventHandler(OnOwnerClosing);

            // load MRU list from Registry
            //LoadMRU();
        }

        private void OnOwnerClosing(object sender, CancelEventArgs e)
        {
            SaveMRU();
        }

        private void OnMRUParentPopup(object sender, EventArgs e)
        {
            if (menuItemMRU.IsParent)
            {
                MainAdminForm ma = (MainAdminForm)ownerForm;
                if (ma.fileOpened) return;
            }
            // remove all child items
            if (menuItemMRU.IsParent)
                menuItemMRU.MenuItems.Clear();

            // Disable menu item if MRU list is empty
            if (mruList == null || mruList.Count == 0)
            {
                menuItemMRU.Enabled = false;
                return;
            }

            // enable menu item and add child items
            menuItemMRU.Enabled = true;

            MenuItem item;
            IEnumerator myEnumerator = mruList.GetEnumerator();

            while (myEnumerator.MoveNext())
            {
                item = new MenuItem(GetDisplayName((string)myEnumerator.Current));

                // subscribe to item's Click event
                item.Click += new EventHandler(this.OnMRUClicked);

                menuItemMRU.MenuItems.Add(item);
            }
        }

        private string GetDisplayName(string fName)
        {
            return GetDisplayName(fName, maxDisplayLength);
        }

        public string GetDisplayName(string fName, int maxLength)
        {
            int d = maxLength / 2;
            return fName.Length <= maxLength ? fName :
                fName.Substring(0, d - 3) + "..." + fName.Substring(fName.Length - d);
        }

        private void OnMRUClicked(object sender, EventArgs e)
        {
            string s;

            try
            {
                MenuItem item = (MenuItem)sender;

                if (item != null)
                {
                    s = (string)mruList[item.Index];

                    // call owner's OpenMRUFile function
                    if (s.Length > 0)
                    {
                        ((IMRUClient)ownerForm).OpenMRUFile(s);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in OnMRUClicked: " + ex.Message);
            }
        }

        public void Add(string fName)
        {
            if (fName == null) return;
            if (mruList == null) mruList = new ArrayList();
            if (mruList.Contains(fName)) mruList.Remove(fName);
            if (mruList.Count == maxNumberOfFiles) mruList.RemoveAt(0);
            if (File.Exists(fName)) mruList.Add(fName);
        }

        public void Remove(string fName)
        {
            if (mruList.Contains(fName)) mruList.Remove(fName);
        }

        public void LoadMRU()
        {
        }

        public void SaveMRU()
        {
        }

        public ArrayList MRUList
        {
            get { return this.mruList; }
        }

    }
}
