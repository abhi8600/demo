using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Aggregates : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Aggregates";
        string categoryName = "Dimensional Mapping";
        private string repositoryDirectory;
        AggregateList aggregateList;

        public Aggregates(MainAdminForm ma)
        {
            aggregateList = new AggregateList(ma.getLogger());
            InitializeComponent();
            this.ma = ma;
        }

        public void addAggregate(Aggregate a)
        {
            aggregateList.Add(a);
        }

        public bool removeAggregate(Aggregate a)
        {
            return aggregateList.Remove(a);
        }

        public void removeAggregateAt(int index)
        {
            aggregateList.RemoveAt(index);
        }

        public void updateAggregate(Aggregate a, string username)
        {
            aggregateList.updateItem(a, username, "Aggregate", ma);
        }

        public void deleteAggregate(Aggregate a)
        {
            aggregateList.deleteItem(a, ma);
        }

        public void updateRepositoryDirectory(string d)
        {
            if (repositoryDirectory != d)
            {
                repositoryDirectory = d;
                aggregateList.updateRepositoryDirectory(d, ma);
            }

        }
        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.Aggregates = aggregateList.aggregates;
        }

        public Boolean updateGuids()
        {
            return aggregateList.updateGuids();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Aggregates != null)
            {
                aggregateList.aggregates = r.Aggregates;
            }
            else
            {
                aggregateList = new AggregateList(ma.getLogger());
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public Aggregate[] getAggregateList()
        {
            aggregateList.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            aggregateList.refreshIfNecessary(ma);
            return aggregateList.aggregates;
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (Aggregate agg in getAggregateList())
            {
                TreeNode tn = new TreeNode(agg.Name);
                tn.ImageIndex = 8;
                tn.SelectedImageIndex = 8;
                rootNode.Nodes.Add(tn);
            }
        }

        public Aggregate findAggregate(String name)
        {
            Aggregate aggregate = null;
            Aggregate[] aggList = getAggregateList();
            if (aggList != null)
            {
                foreach (Aggregate agg in aggList)
                {
                    if (agg.Name == name)
                    {
                        aggregate = agg;
                        break;
                    }
                }
            }
            return aggregate;
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            if (aggregateList.getCount() == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = aggMenuStrip;
                removeAggregateToolStripMenuItem.Visible = false;
                return false;
            }
            implicitSave();
            selIndex = (rootNode.Nodes.IndexOf(node));
            if (selIndex >= 0)
            {
                selNode = node;
                ma.mainTree.ContextMenuStrip = aggMenuStrip;
                removeAggregateToolStripMenuItem.Visible = true;

                List<LoadGroup> lgList = ma.loadGroupMod.getLoadGroups();
                List<string> slgList = new List<string>();
                foreach (LoadGroup lg in lgList) slgList.Add(lg.Name);
                Util.sortedListView(loadGroupsListBox, slgList, getAggregateList()[selIndex].LoadGroups);

                if (ma.connection.connectionList != null)
                {
                    dbConnection.Items.Clear();
                    foreach (DatabaseConnection dc in ma.connection.connectionList)
                    {
                        if (dc.Name != null && !dbConnection.Items.Contains(dc.Name))
                            dbConnection.Items.Add(dc.Name);
                    }
                }
                indexChanged();
            }
            return true;
        }

        public void setup()
        {
            availAggMeasures.Items.Clear();
            prefixesComboBox.Items.Clear();
            availAggMeasures.BeginUpdate();
            if (ma.aggAvailableMeasures != null)
            {
                foreach(string key in ma.aggAvailableMeasures.Keys)
                    prefixesComboBox.Items.Add(key);
            }
            prefixesComboBox.Text = "";
            foreach (string str in ma.aggAvailableMeasures["<blank>"])
            {
                availAggMeasures.Items.Add(new ListViewItem(str, ""));
            }
            setupAvailAggMeasures();
            availAggMeasures.EndUpdate();

            aggDimensionBox.Items.Clear();
            foreach (string dim in ma.getDimensionList())
            {
                aggDimensionBox.Items.Add(dim);
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return aggTabPage;
            }
            set
            {
            }
        }
        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (Aggregate agg in getAggregateList())
            {
                if (agg.Measures != null)
                {
                    for (int i = 0; i < agg.Measures.Length; i++)
                        if (match)
                        {
                            if (agg.Measures[i] == find) agg.Measures[i] = replace;
                        }
                        else agg.Measures[i] = agg.Measures[i].Replace(find, replace);
                }
                if (agg.Columns != null)
                {
                    for (int i = 0; i < agg.Columns.Length; i++)
                        if (match)
                        {
                            if (agg.Columns[i].Column == find) agg.Columns[i].Column = replace;
                        }
                        else agg.Columns[i].Column = agg.Columns[i].Column.Replace(find, replace);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (Aggregate agg in getAggregateList())
            {
                for (int i = 0; i < agg.Measures.Length; i++)
                    if (match)
                    {
                        if (agg.Measures[i] == find) results.Add("Aggregate: " + agg.Name + " - Measure");
                    }
                    else
                    {
                        if (agg.Measures[i].IndexOf(find) >= 0) results.Add("Aggregate: " + agg.Name + " - Measure");
                    }
                for (int i = 0; i < agg.Columns.Length; i++)
                    if (match)
                    {
                        if (agg.Columns[i].Column == find) results.Add("Aggregate: " + agg.Name + " - Level");
                    }
                    else
                    {
                        if (agg.Columns[i].Column.IndexOf(find) >= 0) results.Add("Aggregate: " + agg.Name + " - Level");
                    }

            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateAggregate();
        }

        public void setRepositoryDirectory(string d)
        {
            aggregateList.updateRepositoryDirectory(d, ma);
        }
        #endregion
        private Aggregate getCurrentAggregate()
        {
            Aggregate agg = new Aggregate();
            if (selIndex < 0 || rootNode.Nodes.Count == 0)
                agg.Name = "New Aggregate";
            else
                agg.Name = selNode.Text;
            agg.Connection = dbConnection.Text;
            agg.Query = logicalQueryAggButton.Checked;
            agg.Disabled = aggDisabledCheckBox.Checked;
            agg.OuterJoin = !agg.Query && checkBoxOuterJoin.Checked;
            if (logicalQueryTextBox.Text.Length > 0) agg.LogicalQuery = logicalQueryTextBox.Text;
            if (incrementalFilterTextBox.Text.Length > 0) agg.IncrementalFilter = incrementalFilterTextBox.Text;
            if (buildFilterTextBox.Text.Length > 0) agg.BuildFilter = buildFilterTextBox.Text;
            if (processingGroupTextBox.Text.Length > 0)
            {
                if (agg.SubGroups == null || agg.SubGroups.Length == 0)
                {
                    agg.SubGroups = new string[1];
                    agg.SubGroups[0] = processingGroupTextBox.Text;
                }
                else
                {
                    agg.SubGroups[0] = processingGroupTextBox.Text;
                }
            }
            agg.Measures = new String[selectedAggMeasures.Items.Count];
            for (int count = 0; count < selectedAggMeasures.Items.Count; count++)
            {
                agg.Measures[count] = (string)selectedAggMeasures.Items[count].Text;
            }
            agg.Columns = new AggColumn[aggSelectedColumns.Items.Count];
            for (int i = 0; i < aggSelectedColumns.Items.Count; i++)
            {
                ListViewItem lvi = aggSelectedColumns.Items[i];
                agg.Columns[i] = new AggColumn();
                agg.Columns[i].Dimension = lvi.SubItems[0].Text;
                agg.Columns[i].Column = lvi.SubItems[1].Text;
            }
            List<string> mflist = new List<string>();
            foreach (string s in filterListBox.CheckedItems) mflist.Add(s);
            agg.Filters = (string[])mflist.ToArray();

            agg.Transformations = new AggTransformation[transformList.Items.Count];
            for (int i = 0; i < transformList.Items.Count; i++)
            {
                ListViewItem lvi = transformList.Items[i];
                AggTransformation agt = new AggTransformation();
                int index = lvi.SubItems[0].Text.IndexOf('.');
                if (index > 0)
                {
                    agt.Dimension = lvi.SubItems[0].Text.Substring(0, index);
                    agt.ColumnName = lvi.SubItems[0].Text.Substring(index + 1);
                    agt.Transformations = new Transformation[] { (Transformation)lvi.SubItems[1].Tag };
                }
                else
                {
                    agt.ColumnName = lvi.SubItems[0].Text;
                    agt.Transformations = new Transformation[] { (Transformation)lvi.SubItems[1].Tag };
                }
                agg.Transformations[i] = agt;
            }
            agg.LoadGroups = new string[loadGroupsListBox.CheckedItems.Count];
            int countLoadGroups = 0;
            foreach (ListViewItem lvi in loadGroupsListBox.CheckedItems)
            {
                agg.LoadGroups[countLoadGroups] = lvi.Text;
                countLoadGroups++;
            }

            return (agg);
        }

        private void addAggButton_Click(object sender, System.EventArgs e)
        {
            Aggregate agg = new Aggregate();
            agg.Name = "New Aggregate";
            agg.Measures = new string[0];
            agg.Columns = new AggColumn[0];
            agg.Query = false;
            agg.LogicalQuery = "";
            aggregateList.Add(agg);
            TreeNode tn = new TreeNode(agg.Name);
            tn.ImageIndex = 8;
            tn.SelectedImageIndex = 8;
            rootNode.Nodes.Add(tn);
        }

        private void removeAggButton_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0) return;
            TreeNode n = selNode;
            if (n == null || n.Parent != rootNode) return;
            turnOffimplicitSave = true;
            aggregateList.RemoveAt(selIndex);
            n.Remove();
            turnOffimplicitSave = false;

        }

        private void updateAggregate()
        {
            if (selIndex < 0 || selIndex >= aggregateList.getCount() || selNode == null) return;
            Aggregate agg = getCurrentAggregate();
            aggregateList.variableList[selIndex] = agg;
            selNode.Text = agg.Name;
        }

        private void indexChanged()
        {
            if (selIndex < 0) return;
            Aggregate agg = getAggregateList()[selIndex];
            if (agg.Connection != null && dbConnection.Items.Contains(agg.Connection))
                dbConnection.SelectedItem = agg.Connection;
            logicalQueryAggButton.Checked = agg.Query;
            aggDisabledCheckBox.Checked = agg.Disabled;
            checkBoxOuterJoin.Checked = !agg.Query && agg.OuterJoin;
            columnAggButton.Checked = !agg.Query;
            if (agg.Query && agg.LogicalQuery != null)
                logicalQueryTextBox.Text = agg.LogicalQuery;
            else
                logicalQueryTextBox.Text = null;
            incrementalFilterTextBox.Text = null;
            buildFilterTextBox.Text = null;
            processingGroupTextBox.Text = null;
            if (agg.IncrementalFilter != null)
                incrementalFilterTextBox.Text = agg.IncrementalFilter;
            if (agg.BuildFilter != null)
                buildFilterTextBox.Text = agg.BuildFilter;
            if (agg.SubGroups != null && agg.SubGroups.Length > 0)
                processingGroupTextBox.Text = agg.SubGroups[0];          
            availAggMeasures.Items.Clear();
            prefixesComboBox.Items.Clear();
            selectedAggMeasures.Items.Clear();
            availAggMeasures.BeginUpdate();
            ma.refreshMeasureList(null, false);
            if (agg.Measures != null)
            {
                foreach (string sel in agg.Measures)
                    selectedAggMeasures.Items.Add(new ListViewItem(new string[] { sel, "" }));
            }
            if (ma.aggAvailableMeasures != null)
            {
                foreach (string key in ma.aggAvailableMeasures.Keys)
                    prefixesComboBox.Items.Add(key);
            }
            prefixesComboBox.Text = "";
            foreach (string str in ma.aggAvailableMeasures["<blank>"])
            {
                availAggMeasures.Items.Add(new ListViewItem(str, ""));
            }
            setupAvailAggMeasures();
            availAggMeasures.EndUpdate();
            
            aggSelectedColumns.Items.Clear();

            if (agg.Columns != null)
            {
                foreach (AggColumn ac in agg.Columns)
                {
                    aggSelectedColumns.Items.Add(new ListViewItem(new string[] { ac.Dimension, ac.Column }));
                }
            }

            sortAggSelectedColumns(null);

            QueryFilter.sortedListBox(filterListBox, ma.filterMod.filterList, agg.Filters);
           
            transformList.Items.Clear();
            if (agg.Transformations != null)
                foreach (AggTransformation agt in agg.Transformations)
                {
                    ListViewItem lvi = new ListViewItem(new string[] {
                        agt.Dimension == null ? agt.ColumnName : agt.Dimension + "." + agt.ColumnName,
                        agt.Transformations[0].ToString()});
                    lvi.SubItems[1].Tag = agt.Transformations[0];
                    transformList.Items.Add(lvi);
                }
        }

        private void addAggMeasure_Click(object sender, System.EventArgs e)
        {
            ListViewItem[] lviList = new ListViewItem[availAggMeasures.SelectedItems.Count];
            for (int count = 0; count < availAggMeasures.SelectedItems.Count; count++)
            {
                lviList[count] = availAggMeasures.SelectedItems[count];
            }
            foreach (ListViewItem lvi in lviList)
            {
                availAggMeasures.Items.Remove(lvi);
                selectedAggMeasures.Items.Add(lvi);
            }
        }

        private void removeAggMeasure_Click(object sender, System.EventArgs e)
        {
            ListViewItem[] lviList = new ListViewItem[selectedAggMeasures.SelectedItems.Count];
            for (int count = 0; count < selectedAggMeasures.SelectedItems.Count; count++)
            {
                lviList[count] = selectedAggMeasures.SelectedItems[count];
            }
            foreach (ListViewItem lvi in lviList)
            {
                selectedAggMeasures.Items.Remove(lvi);
                string prefix = prefixesComboBox.Text == "" ? "<blank>" : prefixesComboBox.Text;
                if (ma.aggAvailableMeasures.ContainsKey(prefix) && ma.aggAvailableMeasures[prefix] != null && ma.aggAvailableMeasures[prefix].Contains(lvi.Text))
                    availAggMeasures.Items.Add(lvi.Text);                
            }
            prefixesComboBox_TextChanged(prefixesComboBox, e);
        }

        private void aggDimensionBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            aggColListBox.Items.Clear();
            foreach (string col in ma.getDimensionColumnList(aggDimensionBox.Text))
            {
                aggColListBox.Items.Add(col);
            }
        }

        private void addColumn_Click(object sender, System.EventArgs e)
        {
            if (aggDimensionBox.Text.Length > 0)
            {
                string dimension = aggDimensionBox.Text;
                foreach (string s in aggColListBox.SelectedItems)
                {
                    bool found = false;
                    foreach (ListViewItem lvi in aggSelectedColumns.Items)
                    {
                        if (lvi.SubItems[0].Text == dimension && lvi.SubItems[1].Text == s)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        aggSelectedColumns.Items.Add(new ListViewItem(new string[] { aggDimensionBox.Text, s }));
                        sortAggSelectedColumns(aggDimensionBox.Text);
                    }
                }
            }
        }

        /* 
         * Method void sortAggSelectedColumns(String) sorts the selected columns (2nd column in the list box) for the given dimension.
         * If dimension is supplied as null, the operation is performed on each of the selected dimensions. 
         */
        private void sortAggSelectedColumns(String dimension)
        {
            for (int i = 0; i < aggSelectedColumns.Items.Count; )
            {
                if (dimension != null)
                {
                    while (!aggSelectedColumns.Items[i].SubItems[0].Text.Equals(dimension))
                        i++;
                }

                int j = i + 1;
                List<String> lstStr = new List<String>();
                lstStr.Add(aggSelectedColumns.Items[i].SubItems[1].Text);
                while (j < aggSelectedColumns.Items.Count && aggSelectedColumns.Items[i].SubItems[0].Text.Equals(aggSelectedColumns.Items[j].SubItems[0].Text))
                {
                    lstStr.Add(aggSelectedColumns.Items[j].SubItems[1].Text);
                    j++;
                }
                lstStr.Sort();
                for (int k = i, l = 0; k < j; k++, l++)
                {
                    aggSelectedColumns.Items[k].SubItems[1].Text = lstStr[l];
                }
                i = j;

                if (dimension != null)
                    break;
            }
        }

        private void removeColumn_Click(object sender, System.EventArgs e)
        {
            if (aggSelectedColumns.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in aggSelectedColumns.SelectedItems)
                    aggSelectedColumns.Items.Remove(lvi);
            }
        }

        private bool keyhandled;
        private void logicalQueryTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (keyhandled)
            {
                e.Handled = true;
                keyhandled = false;
            }
        }

        private void logicalQueryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                logicalQueryTextBox.SelectAll();
                keyhandled = true;
            }
        }

        private void groupBoxAggregates_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        private void aggTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                getAggregateList()[selIndex].Name = e.Label;
            }
        }

        #endregion

        private void addTransformation_Click(object sender, EventArgs e)
        {
            LogicalColumnDefinition lcd = new LogicalColumnDefinition();
            string currentAggName = "";
            if (selIndex >= 0)
            {
                currentAggName = getAggregateList()[selIndex].Name;
            }
            if (selectedAggMeasures.SelectedItems.Count > 0)
            {
                for (int i = 0; i < selectedAggMeasures.SelectedItems.Count; i++)
                {
                    lcd.Formula = currentAggName + ".\"" + selectedAggMeasures.SelectedItems[i].Text + "\"";
                    ListViewItem lvi = new ListViewItem(new string[] { 
                    selectedAggMeasures.SelectedItems[i].Text, "Logical Column Definition"});
                    lvi.SubItems[1].Tag = lcd;
                    transformList.Items.Add(lvi);
                }
            }
            else if (aggSelectedColumns.SelectedItems.Count > 0)
            {
                for (int i = 0; i < aggSelectedColumns.SelectedItems.Count; i++)
                {
                    lcd.Formula = currentAggName + ".\"" + aggSelectedColumns.SelectedItems[i].SubItems[1].Text + "\"";
                    ListViewItem lvi = new ListViewItem(new string[] { 
                    aggSelectedColumns.SelectedItems[i].SubItems[0].Text + "." + 
                    aggSelectedColumns.SelectedItems[i].SubItems[1].Text, "Logical Column Definition"});
                    lvi.SubItems[1].Tag = lcd;
                    transformList.Items.Add(lvi);
                }
            }
            else
            {
                MessageBox.Show("Please select at least one measure or dimension column to add.", "Error");
            }
        }

        private void modifyTransformation_Click(object sender, EventArgs e)
        {
            if (transformList.SelectedItems.Count == 1)
            {
                Object o = transformList.SelectedItems[0].SubItems[1].Tag;
                TransformationForm stf = new TransformationForm(ma, (Transformation)o, new string[] { "Logical Column Definition", "Procedure", "Rank" }, false);
                stf.setColumnLabel("Column: " + transformList.SelectedItems[0].SubItems[0].Text);
                DialogResult dr = stf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    Transformation t = stf.getTransformation();
                    transformList.SelectedItems[0].SubItems[1].Tag = t;
                    transformList.SelectedItems[0].SubItems[1].Text = t.ToString();
                }
                stf.Close();
            }
        }

        private void deleteTransformation_Click(object sender, EventArgs e)
        {
            if (transformList.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in transformList.SelectedItems)
                    transformList.Items.Remove(lvi);
            }
        }

        private void selectedAggMeasures_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in aggSelectedColumns.SelectedItems)
                lvi.Selected = false;
        }

        private void aggSelectedColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in selectedAggMeasures.SelectedItems)
                lvi.Selected = false;
        }

        private void columnAggButton_CheckedChanged(object sender, EventArgs e)
        {
            if (columnAggButton.Checked)
            {
                groupBoxColumnsFilters.Visible = true;
                groupBoxLogicalQuery.Visible = false;
                checkBoxOuterJoin.Visible = true;
            }
            else
            {
                groupBoxColumnsFilters.Visible = false;
                groupBoxLogicalQuery.Visible = true;
                checkBoxOuterJoin.Visible = false;
            }
        }

        private void moveUpBtn_Click(object sender, EventArgs e)
        {
            if (transformList.SelectedIndices.Count == 1)
            {
                int selIndex = transformList.SelectedIndices[0];
                if (selIndex > 0)
                {
                    ListViewItem temp = transformList.Items[selIndex];
                    transformList.Items.RemoveAt(selIndex);
                    transformList.Items.Insert(selIndex - 1, temp);
                }
            }
        }

        private void moveDownBtn_Click(object sender, EventArgs e)
        {
            if (transformList.SelectedIndices.Count == 1)
            {
                int selIndex = transformList.SelectedIndices[0];
                if (selIndex < (transformList.Items.Count - 1))
                {
                    ListViewItem temp = transformList.Items[selIndex];
                    transformList.Items.RemoveAt(selIndex);
                    transformList.Items.Insert(selIndex+1, temp);
                }
            }
        }

        private void prefixesComboBox_TextChanged(object sender, EventArgs e)
        {
            availAggMeasures.Items.Clear();
            availAggMeasures.BeginUpdate();
            if (((ComboBox)sender).Text.Equals("") || ((ComboBox)sender).Text.Equals("<blank>"))
            {
                foreach (string str in ma.aggAvailableMeasures["<blank>"])
                {
                    availAggMeasures.Items.Add(new ListViewItem(str, ""));
                }
                setupAvailAggMeasures();
                availAggMeasures.EndUpdate();
                return;
            }

            foreach (string key in ma.aggAvailableMeasures.Keys)
            {
                if (key.StartsWith(((ComboBox)sender).Text, true, System.Globalization.CultureInfo.InvariantCulture))
                {
                    foreach (string str in ma.aggAvailableMeasures[key])
                    {
                        availAggMeasures.Items.Add(new ListViewItem(str, ""));
                    }
                }
            }
            setupAvailAggMeasures();
            availAggMeasures.EndUpdate();
        }

        private void setupAvailAggMeasures()
        {
            for (int i = 0; i < availAggMeasures.Items.Count; i++)
            {
                foreach (ListViewItem lvs in selectedAggMeasures.Items)
                {
                    if (lvs.SubItems[0].Text.Equals(availAggMeasures.Items[i].Text))
                    {
                        availAggMeasures.Items.Remove(availAggMeasures.Items[i]);
                        break;
                    }
                }
            }
        }

        private void Aggregates_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

        public bool isConnectionInUse(string name)
        {
            if (aggregateList.getCount() > 0)
            {
                string lname = name.ToLower();
                foreach (Aggregate agg in getAggregateList())
                {
                    if (agg != null && agg.Connection!= null && agg.Connection.ToLower() == lname)
                        return true;
                }
            }
            return false;
        }
    }
}