using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class Connection : Form, RepositoryModule
    {
        private string moduleName = "Database Connection";
        private string categoryName = "Business Modeling";
        private AppConfig appconfig;
        UsersAndGroups usersandgroups;

        public Connection(AppConfig appconfig, UsersAndGroups usersandgroups)
        {
            InitializeComponent();
            this.appconfig = appconfig;
            this.usersandgroups = usersandgroups;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (dbType.Text == "MySQL")
            {
                r.Connection = new ConnectionParameters();
                r.Connection.Driver = "com.mysql.jdbc.Driver";
                r.Connection.ConnectString = "jdbc:mysql://" + dbServerName.Text + "/" + dbDatabase.Text;
            }
            else if (dbType.Text == "MS SQL Server 2000")
            {
                r.Connection = new ConnectionParameters();
                r.Connection.Driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
                r.Connection.ConnectString = "jdbc:microsoft:sqlserver://" + dbServerName.Text + ":" + dbPortBox.Text + ";databaseName=" + dbDatabase.Text;
            }
            else if (dbType.Text == "ODBC")
            {
                r.Connection = new ConnectionParameters();
                r.Connection.Driver = "sun.jdbc.odbc.JdbcOdbcDriver";
                r.Connection.ConnectString = "jdbc:odbc:" + dbServerName.Text ;
            }
            else if (dbType.Text == "None")
            {
                r.Connection = new ConnectionParameters();
                r.Connection.Driver = "none";
            }
            r.Connection.UserName = dbUserName.Text;
            r.Connection.Password = MainAdminForm.encrypt(dbPassword.Text, MainAdminForm.SecretPassword);
            try
            {
                r.Connection.ConnectionPoolSize = Int32.Parse(connectionPoolBox.Text);
            }
            catch (FormatException)
            {
                r.Connection.ConnectionPoolSize = 20;
            }
            try
            {
                r.Connection.MaxConnectionThreads = Int32.Parse(maxConnectionThreads.Text);
            }
            catch (FormatException)
            {
                r.Connection.MaxConnectionThreads = 4;
            }
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.Connection != null)
            {
                if (r.Connection.Driver == "com.mysql.jdbc.Driver")
                {
                    dbType.Text = "MySQL";
                    int pos1 = r.Connection.ConnectString.IndexOf("//") + 2;
                    int pos2 = r.Connection.ConnectString.IndexOf("/", pos1);
                    dbServerName.Text = r.Connection.ConnectString.Substring(pos1, pos2 - pos1);
                    dbDatabase.Text = r.Connection.ConnectString.Substring(pos2 + 1);
                }
                else if (r.Connection.Driver == "com.microsoft.jdbc.sqlserver.SQLServerDriver")
                {
                    dbType.Text = "MS SQL Server 2000";
                    int pos1 = r.Connection.ConnectString.IndexOf("//") + 2;
                    int pos2 = r.Connection.ConnectString.IndexOf(":", pos1);
                    int pos3 = r.Connection.ConnectString.IndexOf("databaseName=", pos2);
                    dbPortBox.Text = r.Connection.ConnectString.Substring(pos2 + 1, pos3 - pos2 - 2);
                    dbServerName.Text = r.Connection.ConnectString.Substring(pos1, pos2 - pos1);
                    dbDatabase.Text = r.Connection.ConnectString.Substring(pos3 + 13);
                }
                else if (r.Connection.Driver == "sun.jdbc.odbc.JdbcOdbcDriver")
                {
                    dbType.Text = "ODBC";
                    int pos1 = r.Connection.ConnectString.IndexOf(":");
                    int pos2 = r.Connection.ConnectString.IndexOf(":", pos1+1);
                    dbServerName.Text = r.Connection.ConnectString.Substring(pos2+1);
                }
                else if (r.Connection.Driver == "none")
                {
                    dbType.Text = "None";
                }
                dbUserName.Text = r.Connection.UserName;
                if (r.Connection.Password != null)
                {
                    dbPassword.Text = MainAdminForm.decrypt(r.Connection.Password, MainAdminForm.SecretPassword);
                }
                connectionPoolBox.Text = (r.Connection.ConnectionPoolSize == 0) ? "20" : r.Connection.ConnectionPoolSize.ToString();
                maxConnectionThreads.Text = (r.Connection.MaxConnectionThreads == 0) ? "4" : r.Connection.MaxConnectionThreads.ToString();
            }
            else
            {
                connectionPoolBox.Text = "20";
                maxConnectionThreads.Text = "4";
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
        }

        public bool selectedNode(TreeNode node)
        {
            return true;
        }

        public void setup()
        {
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return connectionTabPage;
            }
            set
            {
            }
        }

        #endregion

        private void dbDatabase_DropDown(object sender, EventArgs e)
        {
            List<string> catalogs = Server.getCatalogs(appconfig.ServerConnectionURL, usersandgroups.getAdminUsername(), usersandgroups.getAdminPassword());
            dbDatabase.Items.Clear();
            foreach(string s in catalogs) dbDatabase.Items.Add(s);
        }

        private void dbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dbType.Text == "ODBC")
                dbDatabase.Enabled = false;
            else
                dbDatabase.Enabled = true;
        }
    }
}