﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Performance_Optimizer_Administration
{
    [Serializable]
    public class Location
    {
        public string LoadGroupName;
        public int x;
        public int y;

        public string toSerializedString()
        {
            return LoadGroupName + "|2" + x + "|2" + y;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !typeof(Location).Equals(obj.GetType())) return false;
            Location other = (Location) obj;
            if (LoadGroupName != other.LoadGroupName) return false;
            if (x != other.x) return false;
            if (y != other.y) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
