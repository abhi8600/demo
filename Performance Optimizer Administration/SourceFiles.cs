using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;


namespace Performance_Optimizer_Administration
{
    public partial class SourceFiles : Form, RepositoryModule, RenameNode, DropModule
    {
        private static int NUM_LINES_TO_SCAN = 20000;
        private static int NUM_LINES_TO_SCAN_FOR_DELIMITERS = 1000;
        private static int MAX_LINE_LENGTH = 1000000;
        private static int MIN_NULL_ESCAPES = 10;
        private static int MAX_COLUMN_NAME_WIDTH = 80;
        private static bool ALLOW_FLOATS_AS_KEYS = false;
        public static int ESCAPE_SUBSTRING = 0; // Escape anywhere in the field
        public static int ESCAPE_ALL = 1; // Escape if field exactly matches
        public static int ESCAPE_START = 2; // Escape if field starts with match
        public static int ESCAPE_END = 3; // Escape if field ends with match
        string moduleName = "Data Sources";
        string categoryName = "Warehouse";
        MainAdminForm ma;
        public TreeNode rootNode;
        SourceFileList sourceFiles;
        TreeNode curNode = null;
        SourceFile.SourceColumn[] importColumns;
        public SourceFiles(MainAdminForm ma)
        {
            this.ma = ma;
            sourceFiles = new SourceFileList(ma.getLogger());
            InitializeComponent();
            this.InputTimeZoneComboBox.Text = "";
            this.InputTimeZoneComboBox.Items.Add(Util.SOURCE_TIMEZONE_DEFAULT);
            List<TimeZoneInfo> sysTZ = TimeZoneUtil.getSystemTimeZones();
            this.InputTimeZoneComboBox.Items.AddRange(sysTZ.ToArray());
        }

        private void TimeZoneComboBox_DropDown(object sender, System.EventArgs e)
        {
            ComboBox senderComboBox = (ComboBox)sender;
            int width = senderComboBox.DropDownWidth;
            Graphics g = senderComboBox.CreateGraphics();
            Font font = senderComboBox.Font;
            int vertScrollBarWidth =
                (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;

            int newWidth;
            System.Windows.Forms.ComboBox.ObjectCollection cmbLst = ((ComboBox)sender).Items;
            for (int i = 1; i < cmbLst.Count; i++)
            {
                TimeZoneInfo s = (TimeZoneInfo)cmbLst[i];
                newWidth = (int)g.MeasureString(s.DisplayName, font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth;
                }
            }
            senderComboBox.DropDownWidth = width;
        }

        public List<SourceFile> getSourceFiles()
        {
            sourceFiles.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            sourceFiles.refreshIfNecessary(ma);


            // give each source a guid.  necessary for tree control updates
            Dictionary<String, SourceFile> map = new Dictionary<string, SourceFile>();
            foreach (SourceFile sf in sourceFiles.variableList)
            {
                if ((sf.Guid == null || sf.Guid == Guid.Empty) && !map.ContainsKey(sf.FileName))
                {
                    map.Add(sf.FileName, sf);
                }
            }

            if (map.Count > 0)
            {
                bool previousValue = ma.isInUberTransaction();
                ma.setInUberTransaction(true);

                int i = 0;
                SourceFile lastSourceFile = null;
                foreach (SourceFile sf in map.Values)
                {
                    if (i == map.Count - 1)
                    {
                        lastSourceFile = sf;
                        break;
                    }
                    sourceFiles.updateItem(sf, "", ma);
                    i++;
                }

                ma.setInUberTransaction(previousValue);
                if (lastSourceFile != null)
                {
                    sourceFiles.updateItem(lastSourceFile, "", ma);
                }
            }
            return (sourceFiles.variableList);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.SourceFiles = sourceFiles.sourceFiles;
        }

        public Boolean updateGuids()
        {
            return sourceFiles.updateGuids();
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.SourceFiles != null)
            {
                sourceFiles.sourceFiles = r.SourceFiles;
                //Migrate older repository to new scheme of data upload options
                if (r.Version <= 7)
                {
                    foreach (SourceFile sf in sourceFiles.variableList)
                    {
                        if (sf.SchemaLock)
                        {
                            sf.AllowAddColumns = true;
                            sf.AllowNullBindingRemovedColumns = false;
                            sf.AllowUpcast = false;
                            sf.AllowValueTruncationOnLoad = true;
                            sf.AllowVarcharExpansion = false;
                            sf.FailUploadOnVarcharExpansion = false;                            
                        }
                    }
                }
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {            
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (SourceFile sf in sourceFiles.variableList)
            {
                TreeNode tn = new TreeNode(sf.FileName);
                tn.ImageIndex = 26;
                tn.SelectedImageIndex = 26;
                rootNode.Nodes.Add(tn);
                tn.Tag = sf;
            }
        }

        public bool selectedNode(TreeNode node)
        {
            if (curNode != null) implicitSave();
            ma.mainTree.ContextMenuStrip = sourceFileMenuStrip;
            if (node.Parent.Parent == null)
            {
                curNode = null;
                sourceFilePanel.Visible = false;
                sourceFileGridView.Visible = false;
                newSourceFileStripMenuItem.Visible = true;
                importSourceFileToolStripMenuItem.Visible = true;
                updateSourceFileToolStripMenuItem.Visible = false;
                removeSourceFileToolStripMenuItem.Visible = false;
            }
            else
            {
                newSourceFileStripMenuItem.Visible = false;
                importSourceFileToolStripMenuItem.Visible = false;
                updateSourceFileToolStripMenuItem.Visible = true;
                removeSourceFileToolStripMenuItem.Visible = true;

                if (node.Index < sourceFiles.getCount())
                {
                    SourceFile sourceFile = sourceFiles.sourceFiles[node.Index];
                    if (sourceFile.FormatType == SourceFile.FileFormatType.FixedWidth)
                    {
                        fixedWidthRadioButton.Checked = true;
                        delimitedByRadioButton.Checked = false;
                        delimiterTxt.Text = "";
                    }
                    else
                    {
                        fixedWidthRadioButton.Checked = false;
                        delimitedByRadioButton.Checked = true;
                        if (sourceFile.Separator == "\t")
                        {
                            delimiterTxt.Text = "TAB";
                        }
                        else
                        {
                            delimiterTxt.Text = sourceFile.Separator;
                        }
                    }
                    if (sourceFile.Encoding == null || sourceFile.Encoding.Length == 0)
                        fileEncodingTextBox.Text = "iso-8859-1";
                    else
                        fileEncodingTextBox.Text = sourceFile.Encoding;
                    if (sourceFile.BadColumnValueAction == SourceFile.SKIP_RECORD)
                    {
                        skipRecordRadioButton.Checked = true;
                        replaceWithNullRadioButton.Checked = false;
                    }
                    else
                    {
                        skipRecordRadioButton.Checked = false;
                        replaceWithNullRadioButton.Checked = true;
                    }
                    containsHeaders.Checked = sourceFile.HasHeaders;
                    forceNumColumns.Checked = sourceFile.ForceNumColumns;
                    quoteAfterSeparator.Checked = sourceFile.OnlyQuoteAfterSeparator;
                    ignoreBackslashCheckBox.Checked = sourceFile.IgnoreBackslash;
                    ignoreCarriageReturnCheckBox.Checked = sourceFile.DoNotTreatCarriageReturnAsNewline;
                    ignoreRowsBox.Text = sourceFile.IgnoredRows.ToString();
                    continueIfMissingCheckBox.Checked = sourceFile.ContinueIfMissing;
                    lockFormatBox.Checked = sourceFile.LockFormat;
                    schemaLockBox.Checked = sourceFile.SchemaLock;
                    //SchemaLock Sub-Options
                    allowAddColumnsBox.Checked = sourceFile.AllowAddColumns;
                    allowNullBindingRemovedColumnsBox.Checked = sourceFile.AllowNullBindingRemovedColumns;
                    allowUpcastBox.Checked = sourceFile.AllowUpcast;
                    allowVarcharExpansionRadioButton.Checked = sourceFile.AllowVarcharExpansion;
                    failUploadOnVarcharExpansionRadioButton.Checked = sourceFile.FailUploadOnVarcharExpansion;
                    allowValueTruncationOnLoadRadioButton.Checked = sourceFile.AllowValueTruncationOnLoad;
                    //SchemaLock Sub-Options End
                    customUploadBox.Checked = sourceFile.CustomUpload;
                    if (sourceFile.InputTimeZone != null && !sourceFile.InputTimeZone.Equals("") && !sourceFile.InputTimeZone.Equals(Util.SOURCE_TIMEZONE_DEFAULT))
                    {
                        TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(sourceFile.InputTimeZone);
                        InputTimeZoneComboBox.SelectedIndex = InputTimeZoneComboBox.FindStringExact(tz.DisplayName);
                    }
                    else
                        InputTimeZoneComboBox.SelectedItem = InputTimeZoneComboBox.Items[0];
                    ignoreLastRowsBox.Text = sourceFile.IgnoredLastRows.ToString();
                    sourceFileGridView.DataSource = sourceFile.getTable();
                    importColumns = sourceFile.ImportColumns;
                    searchPatternTextBox.Text = sourceFile.SearchPattern;
                    quoteTextBox.Text = sourceFile.Quote;
                    sourceFilePanel.Visible = true;
                    sourceFileGridView.Visible = true;
                    updateSourceFileToolStripMenuItem.Visible = true;
                    removeSourceFileToolStripMenuItem.Visible = true;
                    minimumRowsBox.Text = sourceFile.MinRows.ToString();
                    curNode = node;
                    SourceFile sf = (SourceFile)curNode.Tag;
                    filterColumnBox.Items.Clear();
                    filterColumnBox.Items.Add("");
                    if (sf != null)
                    {
                        DataTable table = sf.getTable();
                        sourceFileGridView.DataSource = table;
                        foreach (SourceFile.SourceColumn sc in sf.Columns)
                        {
                            filterColumnBox.Items.Add(sc.Name);
                        }
                        if (sf.FilterColumn != null)
                        {
                            filterColumnBox.Text = sf.FilterColumn;
                            rowFilterValue.Text = sf.FilterValue;
                        }
                        else
                        {
                            filterColumnBox.Text = "";
                            rowFilterValue.Text = "";
                        }
                        filterOperatorComboBox.Text = "";
                        if (sf.FilterOperator != null && sf.FilterOperator != "")
                        {
                            filterOperatorComboBox.Text = sf.FilterOperator;
                        }
                        else  if(filterColumnBox.Text != null && filterColumnBox.Text != "")
                        {
                            //Backward compatibility - if there is a filter column specified, set the filter operator to "="
                            filterOperatorComboBox.Text = "=";
                        }
                        escapesTable.Rows.Clear();
                        if (sf.Escapes != null)
                            foreach (string[] esc in sf.Escapes)
                            {
                                DataRow dr = escapesTable.NewRow();
                                dr[0] = esc[0];
                                dr[1] = esc[1];
                                escapesTable.Rows.Add(dr);
                            }
                    }
                }
            }
            return true;
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
            if (curNode != null)
            {
                SourceFile sf = (SourceFile)curNode.Tag;
                if (sf != null)
                {
                    if (fixedWidthRadioButton.Checked)
                    {
                        sf.FormatType = SourceFile.FileFormatType.FixedWidth;
                        sf.Separator = "";
                    }
                    else
                    {
                        sf.FormatType = SourceFile.FileFormatType.Delimited;
                        if (delimiterTxt.Text == "TAB")
                        {
                            sf.Separator = "\t";
                        }
                        else
                        {
                            sf.Separator = delimiterTxt.Text;
                        }
                    }
                    if (escapesTable.Rows.Count > 0)
                    {
                        sf.Escapes = new string[escapesTable.Rows.Count][];
                        for (int i = 0; i < escapesTable.Rows.Count; i++)
                        {
                            sf.Escapes[i] = new string[2];
                            sf.Escapes[i][0] = (string)escapesTable.Rows[i][0];
                            sf.Escapes[i][1] = (string)escapesTable.Rows[i][1];
                            sf.Escapes[i][1].Replace("\\N", "");
                            sf.Escapes[i][1].Replace("\\n", "\n");
                            sf.Escapes[i][1].Replace("\\r", "\r");
                            sf.Escapes[i][1].Replace("\\t", "\t");
                        }
                    }
                    sf.updateFromDataTable((DataTable)sourceFileGridView.DataSource);
                    sf.FileName = curNode.Text;
                    sf.Encoding = fileEncodingTextBox.Text;
                    try
                    {
                        sf.IgnoredRows = Int32.Parse(ignoreRowsBox.Text);
                    }
                    catch (FormatException)
                    {
                        sf.IgnoredRows = 0;
                    }
                    try
                    {
                        sf.IgnoredLastRows = Int32.Parse(ignoreLastRowsBox.Text);
                    }
                    catch (FormatException)
                    {
                        sf.IgnoredLastRows = 0;
                    }
                    try
                    {
                        sf.MinRows = Int32.Parse(minimumRowsBox.Text);
                    }
                    catch (FormatException)
                    {
                        sf.MinRows = 0;
                    }
                    if (InputTimeZoneComboBox.SelectedIndex > 0)
                        sf.InputTimeZone = ((TimeZoneInfo)InputTimeZoneComboBox.Items[InputTimeZoneComboBox.SelectedIndex]).Id;
                    else
                        sf.InputTimeZone = InputTimeZoneComboBox.Text;
                    sf.HasHeaders = containsHeaders.Checked;
                    sf.ForceNumColumns = forceNumColumns.Checked;
                    sf.OnlyQuoteAfterSeparator = quoteAfterSeparator.Checked;
                    sf.IgnoreBackslash = ignoreBackslashCheckBox.Checked;
                    sf.DoNotTreatCarriageReturnAsNewline = ignoreCarriageReturnCheckBox.Checked;
                    sf.BadColumnValueAction = skipRecordRadioButton.Checked ? SourceFile.SKIP_RECORD : SourceFile.REPLACE_WITH_NULL;
                    sf.SearchPattern = searchPatternTextBox.Text;
                    sf.Quote = quoteTextBox.Text;
                    sf.ContinueIfMissing = continueIfMissingCheckBox.Checked;
                    sf.LockFormat = lockFormatBox.Checked;
                    sf.SchemaLock = schemaLockBox.Checked;
                    //SchemaLock Sub-Options
                    sf.AllowAddColumns =allowAddColumnsBox.Checked;
                    sf.AllowNullBindingRemovedColumns = allowNullBindingRemovedColumnsBox.Checked;
                    sf.AllowUpcast = allowUpcastBox.Checked;
                    sf.AllowVarcharExpansion = allowVarcharExpansionRadioButton.Checked;
                    sf.FailUploadOnVarcharExpansion = failUploadOnVarcharExpansionRadioButton.Checked;
                    sf.AllowValueTruncationOnLoad = allowValueTruncationOnLoadRadioButton.Checked;
                    //SchemaLock Sub-Options End
                    sf.CustomUpload = customUploadBox.Checked;
                    if (filterColumnBox.Text.Length == 0)
                    {
                        sf.FilterColumn = null;
                        sf.FilterValue = null;
                        sf.FilterOperator = null;
                    }
                    else
                    {
                        sf.FilterColumn = filterColumnBox.Text;
                        sf.FilterValue = rowFilterValue.Text;
                        sf.FilterOperator = filterOperatorComboBox.Text;
                    }
                    if (!sf.HasHeaders)
                    {
                        sf.ImportColumns = null;
                    }
                }
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return sourceFilePage;
            }
            set
            {
            }
        }

        public void setRepositoryDirectory(string d)
        {
            sourceFiles.updateRepositoryDirectory(d, ma);
        }
        #endregion

        private static double StandardDeviation(double[] data)
        {
            double ret = 0;
            double DataAverage = 0;
            double TotalVariance = 0;
            int Max = 0;

            try
            {
                Max = data.Length;
                if (Max == 0) { return ret; }
                DataAverage = Average(data);
                for (int i = 0; i < Max; i++)
                {
                    TotalVariance += Math.Pow(data[i] - DataAverage, 2);
                }
                ret = Math.Sqrt(SafeDivide(TotalVariance, Max));
            }
            catch (Exception) { throw; }
            return ret;
        }

        private static double Average(double[] data)
        {
            double DataTotal = 0;

            try
            {
                for (int i = 0; i < data.Length; i++)
                {
                    DataTotal += data[i];
                }
                return SafeDivide(DataTotal, data.Length);
            }
            catch (Exception) { throw; }
        }

        private static double SafeDivide(double value1, double value2)
        {
            double ret = 0;

            try
            {
                if ((value1 == 0) || (value2 == 0)) { return ret; }
                ret = value1 / value2;
            }
            catch { }
            return ret;
        }

        private int getCount(string line, char sep)
        {
            int cnt = 0;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == sep) cnt++;
            }
            return cnt;
        }

        public object importFile(string directory, string name, int numLines, bool getStats, bool findKeys, out List<string> errors, out List<string> warnings, out List<string> changes, bool preventChanges, bool ignoreInternalQuotes, string[][] escapes, bool forceNumColumns, string quotechar, bool hasLoadedData, int queryLanguageVersion)
        {
            return (importFile(directory, name, "iso-8859-1", numLines, getStats, findKeys, out errors, out warnings, out changes, preventChanges, ignoreInternalQuotes, escapes, forceNumColumns, quotechar, hasLoadedData, queryLanguageVersion, null));
        }

        public object importFile(string directory, string name, string encoding, int numLines, bool getStats, bool findKeys, out List<string> errors, out List<string> warnings, out List<string> changes, bool preventChanges, bool ignoreInternalQuotes, string[][] escapes, bool forceNumColumns, string quotechar, bool hasLoadedData, int queryLanguageVersion, string separator)
        {
            return (importFile(directory, name, encoding, numLines, getStats, findKeys, true, 0, 0, out errors, out warnings, out changes,
                preventChanges, ignoreInternalQuotes, escapes, forceNumColumns, quotechar, hasLoadedData, queryLanguageVersion, false, separator,false, 0));
        }

        public TreeNode addSourceFile(SourceFile sf)
        {
            TreeNode tn = new TreeNode(sf.FileName);
            tn.Tag = sf;
            rootNode.Nodes.Add(tn);
            sourceFiles.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            sourceFiles.updateItem(sf, "", ma);
            tn.ImageIndex = 26;
            tn.SelectedImageIndex = 26;
            return tn;
        }

        /*
         * Returns either a valid sourcefile or a string containing an error message
         */
        public SourceFile importFile(string directory, string name, string encoding, int numLines, bool getStats, bool findKeys, bool firstRowHeaders, 
            int ignoredRows, int ignoredLastRows, out List<string> errors, out List<string> warnings, out List<string> changes,
            bool preventChanges, bool ignoreInternalQuotes, string[][] escapes, bool forceNumColumns, string quotechar, bool hasLoadedData, int queryLanguageVersion, bool ignoreCarriageReturn, string separator, bool isBrowserUpload, int builderVersion)
        {
            SourceFile sf = sourceFileExists(name);
            bool update = false;
            bool ignoreSeparatorDetection = false;
            TreeNode tn = null;
            if (sf == null)
            {
                sf = new SourceFile();
            }
            else
            {
                update = true;
                string lname = name.ToLower();
                foreach (TreeNode stn in rootNode.Nodes)
                {
                    if (stn.Text.ToLower() == lname)
                    {
                        tn = stn;
                        break;
                    }
                }
            }
            // If the source is questionable and separator has been found, use existing
            sf.Separator = update ? sf.Separator : null;

            if (!isBrowserUpload || !update)
            {
                sf.HasHeaders = firstRowHeaders;
                sf.IgnoredRows = ignoredRows;
                sf.IgnoredLastRows = ignoredLastRows;
                sf.DoNotTreatCarriageReturnAsNewline = ignoreCarriageReturn;
                sf.Quote = quotechar == null ? "\"" : quotechar;
                sf.ForceNumColumns = forceNumColumns;

                if (escapes != null)
                    sf.Escapes = escapes;

                if (encoding != null)
                    sf.Encoding = encoding;
                if (separator != null && separator.Length > 0)
                {
                    sf.Separator = separator;
                    ignoreSeparatorDetection = true;
                }

                sf.OnlyQuoteAfterSeparator = ignoreInternalQuotes || sf.OnlyQuoteAfterSeparator;
            }
            else
            {
                firstRowHeaders = sf.HasHeaders;
            }
            
            sf.FileName = name;

            bool allowOtherOperations = (!hasLoadedData && !preventChanges && !sf.SchemaLock);
            importSourceFile(directory, sf, tn, update, numLines, firstRowHeaders, getStats, findKeys, 
                out errors, out warnings, out changes, sf.LockFormat || preventChanges, sf.SchemaLock,
                allowOtherOperations, allowOtherOperations, queryLanguageVersion, ignoreSeparatorDetection, builderVersion);

            if (!update && errors.Count <=0)
            {
                sourceFiles.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
                sourceFiles.updateItem(sf, "", ma);
                tn = addSourceFile(sf);
                ma.mainTree.SelectedNode = tn;
            }
            return (sf);
        }

        private void importSourceFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            importSourceFileDialog.Title = "Import Data Source";
            DialogResult result = importSourceFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filename = importSourceFileDialog.FileName;
                FileInfo fi = new FileInfo(filename);
                string nodeName = fi.Name;
                if (sourceFileExists(nodeName) != null)
                {
                    MessageBox.Show("Source file definition already exists - " + nodeName, "Error");
                    return;
                }
                List<string> errors;
                List<string> warnings;
                List<string> changes;
                importFile(fi.DirectoryName, fi.Name, NUM_LINES_TO_SCAN, false, false, out errors, out warnings, out changes, false, false, null, false, null, false, 1);
            }
        }

        private void importSourceFile(string directory, SourceFile sf, TreeNode tn, bool update, int numLines, out List<string> errors, out List<string> warnings, out List<string> changes, bool preventChanges, bool schemaLock, int queryLanguageVersion, bool ignoreSeparatorDetection)
    {
            importSourceFile(directory, sf, tn, update, numLines, true, false, false, out errors, out warnings, out changes, preventChanges, schemaLock, false, false, queryLanguageVersion, ignoreSeparatorDetection, 0);
        }

        public static Encoding getEncoding(SourceFile sf)
        {
            if (sf.Encoding == null)
                return null;
            else if (sf.Encoding == "UTF-16" || sf.Encoding == "Unicode")
                return Encoding.Unicode;
            else if (sf.Encoding == "UTF-16LE" || sf.Encoding == "UTF-16(Little Endian)")
                return Encoding.Unicode;
            else if (sf.Encoding == "UTF-16BE" || sf.Encoding == "UTF-16(Big Endian)")
                return Encoding.BigEndianUnicode;
            else 
                return Encoding.GetEncoding(sf.Encoding);
        }

        enum DType { None, Varchar, Date, DateTime, Float, Integer };
        private void importSourceFile(string directory, SourceFile sf, TreeNode tn, bool update, int numLines, bool firstLineHasNames,
            bool getStats, bool findKeys, out List<string> errors, out List<string> warnings, out List<string> changes,
            bool preventChanges, bool schemaLock, bool allowDowncast, bool allowWidthReduction, int queryLanguageVersion, bool ignoreSeparatorDetection, int builderVersion)
        {
            errors = new List<string>();
            warnings = new List<string>();
            changes = new List<string>();
            string fname = Path.Combine(directory, sf.FileName);
            FileInfo fi = new FileInfo(fname);
            fi.Refresh();
            sf.LastUploadDate = fi.LastWriteTime;
            char sep = ',';
            Hashtable separators = new Hashtable();
            separators.Add(',', new List<double>());
            separators.Add('\t', new List<double>());
            separators.Add('|', new List<double>());
            separators.Add(';', new List<double>());
            separators.Add(':', new List<double>());
            separators.Add('!', new List<double>()); // bugzilla #10993

            int count = 0;
            string line = null;
            bool firstPass = true;
            int lineCount = 0;
            StreamReader reader = null;
            Encoding encoding = getEncoding(sf);
            /*
             * If ignoring rows at the end, we need to get the total line count
             */
            if (sf.IgnoredLastRows > 0)
            {
                if (encoding == null)
                    reader = new StreamReader(fname, true);
                else
                    reader = new StreamReader(fname, encoding);
                while ((line = readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns && sf.Columns != null && !(sf.HasHeaders && lineCount == 0) ? sf.Columns.Length - 1 : 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline)) != null)
                {
                    lineCount++;
                }
                reader.Close();
            }
            count = 0;
            if (encoding == null)
                reader = new StreamReader(fname, true);
            else
            	reader = new StreamReader(fname, encoding);                                
            if (sf.Encoding == null)
            {
                sf.Encoding = reader.CurrentEncoding.EncodingName;
                if (sf.Encoding == "Unicode (UTF-8)")
                    sf.Encoding = "UTF-8";
            }
            if (!(update || ignoreSeparatorDetection))//do not perform heuristics
            {
                while ((line = readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns && sf.Columns != null && !(sf.HasHeaders && count == 0) ? sf.Columns.Length - 1 : 0, false, sf.DoNotTreatCarriageReturnAsNewline)) != null && count < NUM_LINES_TO_SCAN_FOR_DELIMITERS)
                {
                    bool skip = false;
                    if (sf.IgnoredRows > 0 && count < sf.IgnoredRows)
                        skip = true;
                    else if (sf.IgnoredLastRows > 0 && (lineCount - count - 1) < sf.IgnoredLastRows)
                        skip = true;
                    count++;
                    if (skip)
                        continue;
                    if (sf.Encoding == null)
                    {
                        sf.Encoding = reader.CurrentEncoding.EncodingName;
                        if (sf.Encoding == "Unicode (UTF-8)")
                            sf.Encoding = "UTF-8";
                    }
                    bool containsSep = false;
                    foreach (char key in separators.Keys)
                    {
                        if (line.IndexOf(key) != -1)
                        {
                            containsSep = true;
                            break;
                        }
                    }
                    if (!containsSep)
                    {
                        continue;
                    }
                    List<char> removalList = new List<char>();
                    foreach (char key in separators.Keys)
                    {
                        int sepCount = getCount(line, key);
                        if (sepCount > 0)
                        {
                            List<double> lst = (List<double>)separators[key];
                            lst.Add((double)sepCount);
                        }
                        else
                        {
                            removalList.Add(key);
                            // could exit early if 0 or 1..
                        }
                    }
                    foreach (char key in removalList)
                    {
                        separators.Remove(key);
                    }
                }
            }
            reader.Close();
            if (!(update || ignoreSeparatorDetection))//do not perform heuristics
            {
                char lastSep = ',';
                int cnt = 0;
                foreach (DictionaryEntry entry in separators)
                {
                    lastSep = (char)entry.Key;
                    cnt++;
                }
                if (cnt == 1)
                {
                    // found one
                    sep = lastSep;
                }
                else if (cnt == 0)
                {
                    // fixed format?
                    errors.Add("No column separators found");
                    return;
                }
                else
                {
                    // no single choice, choose the 'best'
                    double leaststddev = 1000000.0;
                    double correspondingmean = 0.0;
                    bool first = true;
                    foreach (DictionaryEntry entry in separators)
                    {
                        List<double> lst = (List<double>)entry.Value;
                        double[] items = new double[count];
                        lst.CopyTo(items, 0);
                        double mean = Average(items);
                        double stddev = StandardDeviation(items);

                        if (first || (mean > 0 && correspondingmean == 0) ||
                            (mean - correspondingmean) / correspondingmean -
                            (stddev - leaststddev) / correspondingmean > 0)
                        {
                            leaststddev = stddev;
                            correspondingmean = mean;
                            sep = (char)entry.Key;
                        }
                        first = false;
                    }
                }
                sf.Separator = new String(new char[] { sep });
            }
            else
            {
                sep = (sf.Separator != null && sf.Separator.Length > 0) ? sf.Separator[0] : '|';
            }
            // now find the column names, types, and widths
            if (encoding == null)
                reader = new StreamReader(fname);
            else
                reader = new StreamReader(fname, encoding);
            count = 0;
            if (!reader.EndOfStream)
            {
                string[] colnames = null;
                /* 
                 * Clean up the column names, trim leading/trailing spaces
                 * 
                 * Ensure no duplicates (add numbers)
                 */
                Dictionary<string, object>[] uniqueValues = null;
                int ind = sf.FileName.IndexOf('.');
                string prefix = ind < 0 ? sf.FileName : sf.FileName.Substring(0, ind);
                string[] dtypeStrings = null;
                DType[] dtypes = null;
                bool[] parsedvalues = null;
                int[] widths = null;
                int linesRead = 0;
                if (sf.IgnoredRows > 0)
                {
                    int skipCount = 0;
                    while (skipCount++ < sf.IgnoredRows && !reader.EndOfStream)
                    {
                        line = readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns && sf.Columns != null && !(sf.HasHeaders && linesRead == 0) ? sf.Columns.Length - 1 : 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
                        linesRead++;
                    }
                    count = skipCount;
                }
                int[] escapeTypes = null;
                string[][] escapes = null;
                if (sf.Escapes != null)
                {
                    escapeTypes = new int[sf.Escapes.Length];
                    escapes = new string[sf.Escapes.Length][];
                    for (int i = 0; i < sf.Escapes.Length; i++)
                    {
                        escapes[i] = new string[2];
                        // If the escape starts and ends with a '^', escape only the entire field
                        if (sf.Escapes[i][0][0] == '^' && sf.Escapes[i][0][sf.Escapes[i][0].Length - 1] == '^')
                        {
                            escapeTypes[i] = ESCAPE_ALL;
                            escapes[i][0] = sf.Escapes[i][0].Substring(1, sf.Escapes[i][0].Length - 2);
                        }
                        // If the escape starts with a '^', escape only the start
                        else if (sf.Escapes[i][0][0] == '^')
                        {
                            escapeTypes[i] = ESCAPE_START;
                            escapes[i][0] = sf.Escapes[i][0].Substring(1);
                        }
                        // If the escape ends with a '^', escape only the end
                        else if (sf.Escapes[i][0][sf.Escapes[i][0].Length - 1] == '^')
                        {
                            escapeTypes[i] = ESCAPE_END;
                            escapes[i][0] = sf.Escapes[i][0].Substring(0, sf.Escapes[i][0].Length - 1);
                        }
                        // Escape anywhere in the field
                        else
                        {
                            escapeTypes[i] = ESCAPE_SUBSTRING;
                            escapes[i][0] = sf.Escapes[i][0];
                        }
                        escapes[i][1] = sf.Escapes[i][1];
                    }
                }
                bool quoteSet = sf.Quote != null;

                if (firstLineHasNames && !reader.EndOfStream)
                {
                    line = readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns && sf.Columns != null && !(sf.HasHeaders && linesRead == 0) ? sf.Columns.Length - 1 : 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
                    linesRead++;
                    count++;
                    colnames = line.Split(sep);
                    Dictionary<string, object> nameList = new Dictionary<string, object>(colnames.Length);
                    for (int i = 0; i < colnames.Length; i++)
                    {
                        colnames[i] = colnames[i].Trim();
                        if (colnames[i].Length > 2 && colnames[i][0] == '"' && colnames[i][colnames[i].Length - 1] == '"')
                        {
                            colnames[i] = colnames[i].Substring(1, colnames[i].Length - 2);
                            sf.Quote = "\"";
                            quoteSet = true;
                        }
                        else if (colnames[i].Length > 1 && colnames[i][0] == '\'' && colnames[i][colnames[i].Length - 1] == '\'')
                        {
                            colnames[i] = colnames[i].Substring(1, colnames[i].Length - 2);
                            sf.Quote = "\'";
                            quoteSet = true;
                        }
                        string unsupportedChars = queryLanguageVersion == 0 ? Util.OLD_QUERY_LANGUAGE_UNSUPPORTED_CHARS : Util.NEW_QUERY_LANGUAGE_UNSUPPORTED_CHARS;
                        colnames[i] = Regex.Replace(colnames[i], unsupportedChars, "");
                        if (colnames[i].Length == 0)
                        {
                            colnames[i] = prefix + " Column " + i;
                        }
                        else
                        {
                            colnames[i] = Regex.Replace(colnames[i], @"[ ]{2,}", " "); // Remove multiple spaces
                        }
                        if (builderVersion < 28)
                        {
                            if (colnames[i].Length > MAX_COLUMN_NAME_WIDTH)
                                colnames[i] = colnames[i].Substring(0, MAX_COLUMN_NAME_WIDTH);
                        }
                        string pkolname = Util.generatePhysicalName(colnames[i], ma.builderVersion);
                        if (nameList.ContainsKey(pkolname))
                        {
                            int dupe = 0;
                            do
                            {
                                ++dupe;
                                pkolname = Util.generatePhysicalName(colnames[i], ma.builderVersion) + "_" + dupe;
                            } while (nameList.ContainsKey(pkolname));
                            colnames[i] += " " + dupe;
                        }
                        nameList.Add(pkolname, null);
                    }
                    dtypeStrings = new string[colnames.Length];
                    dtypes = new DType[colnames.Length];
                    widths = new int[colnames.Length];
                    parsedvalues = new bool[colnames.Length];
                    if (getStats)
                    {
                        uniqueValues = new Dictionary<string, object>[colnames.Length];
                        for (int i = 0; i < colnames.Length; i++)
                            uniqueValues[i] = new Dictionary<string, object>();
                    }
                }
                if (!firstLineHasNames && sf.Columns != null && sf.Columns.Length > 0 && colnames == null)
                {
                    colnames = new string[sf.Columns.Length];
                    for (int i=0; i<sf.Columns.Length;i++)
                    {
                        colnames[i] = sf.Columns[i].Name;
                    }
                    dtypeStrings = new string[colnames.Length];
                    dtypes = new DType[colnames.Length];
                    widths = new int[colnames.Length];
                    parsedvalues = new bool[colnames.Length];
                    if (getStats || findKeys)
                    {
                        uniqueValues = new Dictionary<string, object>[colnames.Length];
                        for (int i = 0; i < colnames.Length; i++)
                            uniqueValues[i] = new Dictionary<string, object>();
                    }
                }
                Dictionary<string, object>[] validKeys = null;
                List<int[]> keySet = null;
                if (findKeys)
                {
                    keySet = scanKeys(directory + "\\" + sf.FileName, sep, sf.IgnoredRows + (sf.HasHeaders ? 1 : 0), sf.IgnoredLastRows, sf.Separator, sf.OnlyQuoteAfterSeparator, getEncoding(sf), sf.IgnoreBackslash, sf.ForceNumColumns, sf.Columns != null ? sf.Columns.Length : 0, sf.Quote, sf.HasHeaders, sf.DoNotTreatCarriageReturnAsNewline);
                    validKeys = new Dictionary<string, object>[keySet.Count];
                    for (int i = 0; i < keySet.Count; i++)
                        validKeys[i] = new Dictionary<string, object>();
                }
                DateTime dt;
                Double d;
                long longVal;
                int numNullEscapes = 0;
                string[] cols = null;
                int linesOfData = 0;
                while ((line = readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns && sf.Columns != null && !(sf.HasHeaders && linesRead == 0) ? sf.Columns.Length - 1 : 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline)) != null)
                {
                    linesRead++;
                    bool skip = false;
                    if (sf.IgnoredLastRows > 0 &&
                        (lineCount - count - 1) < sf.IgnoredLastRows)
                        skip = true;
                    if ((numLines <= 0 || count < numLines) && !skip)
                    {
                        cols = Util.splitString(line, sep);
                        if (colnames != null && sf.ForceNumColumns && cols.Length != colnames.Length)
                            continue;
                        if (escapes != null)
                            for (int j = 0; j < escapes.Length; j++)
                            {
                                string[] esc = escapes[j];
                                for (int i = 0; i < cols.Length; i++)
                                {
                                    if (escapeTypes[j] == ESCAPE_SUBSTRING)
                                        cols[i] = cols[i].Replace(esc[0], esc[1]);
                                    else if (escapeTypes[j] == ESCAPE_ALL)
                                    {
                                        if (cols[i] == esc[0])
                                            cols[i] = esc[1];
                                    }
                                    else if (escapeTypes[j] == ESCAPE_START)
                                    {
                                        if (cols[i].StartsWith(esc[0]))
                                            cols[i] = esc[1] + cols[i].Substring(esc[0].Length);
                                    }
                                    else if (escapeTypes[j] == ESCAPE_END)
                                    {
                                        if (cols[i].EndsWith(esc[0]))
                                            cols[i] = cols[i].Substring(0, cols[i].Length - esc[0].Length) + esc[1];
                                    }
                                }
                            }
                        if (colnames == null)
                        {
                            colnames = new string[cols.Length];
                            for (int i = 0; i < colnames.Length; i++)
                            {
                                colnames[i] = prefix + " Column " + i;
                            }
                            dtypeStrings = new string[colnames.Length];
                            dtypes = new DType[colnames.Length];
                            widths = new int[colnames.Length];
                            parsedvalues = new bool[colnames.Length];
                            if (getStats || findKeys)
                            {
                                uniqueValues = new Dictionary<string, object>[colnames.Length];
                                for (int i = 0; i < colnames.Length; i++)
                                    uniqueValues[i] = new Dictionary<string, object>();
                            }
                        }
                        for (int i = 0; i < colnames.Length && i < cols.Length; i++)
                        {
                            if (firstPass && cols[i].Length == 2 && cols[i] == "\\N")
                                numNullEscapes++;
                            string s = cols[i];
                            if (quoteSet || (update && sf.Quote.Length > 0))
                            {
                                if (s.Length > sf.Quote.Length*2 && s.StartsWith(sf.Quote) && s.EndsWith(sf.Quote))
                                {
                                    s = s.Substring(sf.Quote.Length, s.Length - sf.Quote.Length * 2);
                                }
                            }
                            else if (!update)
                            {
                                // If not updating - detect quote character
                                if (s.Length > 1 && s[0] == '"' && s[s.Length - 1] == '"')
                                {
                                    s = s.Substring(1, s.Length - 2);
                                    sf.Quote = "\"";
                                    quoteSet = true;
                                }
                                else if (s.Length > 1 && s[0] == '\'' && s[s.Length - 1] == '\'')
                                {
                                    s = s.Substring(1, s.Length - 2);
                                    sf.Quote = "\'";
                                    quoteSet = true;
                                }
                            }
                            if (s.Length == 0)
                                continue;
                            if (i == cols.Length - 1)
                                s = s.TrimEnd(new char[] { '\r' });
                            if (dtypes[i] != DType.Varchar)
                            {
                                if (dtypes[i] != DType.DateTime && dtypes[i] != DType.Date)
                                {
                                    if (dtypes[i] == DType.Integer || dtypes[i] == DType.None)
                                    {
                                        if (!Util.tryParseInt(builderVersion, s, out longVal) && s.Trim().Length > 0)
                                        {
                                            if (!Double.TryParse(s, out d) && s.Trim().Length > 0)
                                            {
                                                if (!Util.tryDateTimeParse(s, out dt))
                                                    dtypes[i] = DType.Varchar;
                                                else
                                                {
                                                    if (dtypes[i] != DType.DateTime && !containsTimeComponent(dt, s))                                                    
                                                    {
                                                        dtypes[i] = DType.Date;
                                                    }
                                                    else
                                                    {
                                                        dtypes[i] = DType.DateTime;
                                                    }
                                                    parsedvalues[i] = true;
                                                }
                                            }
                                            else
                                            {
                                                dtypes[i] = DType.Float;
                                                parsedvalues[i] = true;
                                            }
                                        }
                                        else
                                        {
                                            dtypes[i] = DType.Integer;
                                            parsedvalues[i] = true;
                                        }
                                    }
                                    else if (dtypes[i] == DType.Float || dtypes[i] == DType.None)
                                    {
                                        if (!Double.TryParse(s, out d) && s.Trim().Length > 0)
                                        {
                                            if (!Util.tryDateTimeParse(s, out dt))
                                                dtypes[i] = DType.Varchar;
                                            else
                                            {
                                                if (dtypes[i] != DType.DateTime && !containsTimeComponent(dt, s))
                                                {
                                                    dtypes[i] = DType.Date;
                                                }
                                                else
                                                {
                                                    dtypes[i] = DType.DateTime;
                                                }
                                                parsedvalues[i] = true;
                                            }
                                        }
                                        else
                                        {
                                            dtypes[i] = DType.Float;
                                            parsedvalues[i] = true;
                                        }
                                    }
                                    else if (dtypes[i] == DType.None)
                                    {
                                        if (!Util.tryDateTimeParse(s, out dt))
                                            dtypes[i] = DType.Varchar;
                                        else
                                        {
                                            if (dtypes[i] != DType.DateTime && !containsTimeComponent(dt, s))
                                            {
                                                dtypes[i] = DType.Date;
                                            }
                                            else
                                            {
                                                dtypes[i] = DType.DateTime;
                                            }
                                            parsedvalues[i] = true;
                                        }
                                    }
                                    else
                                        dtypes[i] = DType.Varchar;
                                }
                                else if (Util.tryDateTimeParse(s, out dt))
                                {
                                    if (dtypes[i] != DType.DateTime && !containsTimeComponent(dt, s))
                                    {
                                        dtypes[i] = DType.Date;
                                    }
                                    else
                                    {
                                        dtypes[i] = DType.DateTime;
                                    }
                                    parsedvalues[i] = true;
                                }
                                else
                                    dtypes[i] = DType.Varchar;
                            }
                            widths[i] = Math.Max(widths[i], s.Length);
                            if (getStats)
                            {
                                if (!uniqueValues[i].ContainsKey(s))
                                    uniqueValues[i].Add(s, null);
                            }
                        }
                        if (line.Length > 0)
                        {
                            count++;
                        }
                        // Test key possibilities
                        if (findKeys)
                        {
                            for (int k = 0; k < validKeys.Length; k++)
                            {
                                if (validKeys[k] == null)
                                    continue;
                                StringBuilder sb = new StringBuilder();
                                for (int j = 0; j < keySet[k].Length; j++)
                                {
                                    int index = keySet[k][j];
                                    if (index < cols.Length)
                                        sb.Append("{" + cols[index] + "}");
                                    else
                                        sb.Append("{null}");
                                }
                                string keyValue = sb.ToString();
                                // Null out any keys with duplicates
                                if (validKeys[k].ContainsKey(keyValue))
                                    validKeys[k] = null;
                                else
                                    validKeys[k].Add(keyValue, null);
                            }
                        }
                        // If, as in the case of MYSQL dumps, nulls are coded as \N, turn on the escape
                        if (firstPass && count == 20 && numNullEscapes > MIN_NULL_ESCAPES)
                        {
                            sf.Escapes = new string[][] { new string[] { "\\N", "" } };
                            reader.Close();
                            if (encoding == null)
                                reader = new StreamReader(fname);
                            else
                                reader = new StreamReader(fname, encoding);
                            for (int i = 0; i < widths.Length; i++)
                            {
                                widths[i] = 0;
                                dtypes[i] = DType.None;
                            }
                            firstPass = false;
                            count = 0;
                            if (uniqueValues != null)
                                for (int i = 0; i < uniqueValues.Length; i++)
                                {
                                    if (uniqueValues[i] != null)
                                        uniqueValues[i].Clear();
                                    uniqueValues[i] = new Dictionary<string, object>();
                                }
                            if (findKeys)
                            {
                                for (int i = 0; i < keySet.Count; i++)
                                {
                                    if (validKeys[i] != null)
                                        validKeys[i].Clear();
                                    validKeys[i] = new Dictionary<string, object>();
                                }
                            }
                        }
                    }
                    if (!skip && line.Length > 0)
                    {
                        linesOfData++;
                    }
                }
                for (int i = 0; i < parsedvalues.Length; i++)
                    if (!parsedvalues[i] && (uniqueValues == null || uniqueValues[i].Count > 0)) dtypes[i] = DType.Varchar;
                sf.NumRows = linesOfData;
                sf.FileLength = (new FileInfo(directory + "\\" + sf.FileName)).Length;
                for (int i = 0; i < colnames.Length; i++)
                {
                    switch (dtypes[i])
                    {
                        case DType.Varchar:
                            dtypeStrings[i] = "Varchar";
                            break;
                        case DType.Integer:
                            dtypeStrings[i] = "Integer";
                            break;
                        case DType.Float:
                            dtypeStrings[i] = "Float";
                            break;
                        case DType.Date:
                            dtypeStrings[i] = "Date";
                            break;
                        case DType.DateTime:
                            dtypeStrings[i] = "DateTime";
                            break;
                        case DType.None:
                            dtypeStrings[i] = "None";
                            break;
                        default:
                            dtypeStrings[i] = "Varchar";
                            break;
                    }
                }
                reader.Close();
                if (!update || sf.ImportColumns == null || sf.ImportColumns.Length == 0)
                {
                    sf.Columns = new SourceFile.SourceColumn[colnames.Length];
                    sf.ImportColumns = new SourceFile.SourceColumn[colnames.Length];
                    for (int i = 0; i < colnames.Length; i++)
                    {
                        sf.Columns[i] = new SourceFile.SourceColumn();
                        sf.Columns[i].Name = colnames[i];
                        sf.Columns[i].DataType = dtypeStrings[i];
                        sf.Columns[i].Width = widths[i];
                        sf.ImportColumns[i] = new SourceFile.SourceColumn();
                        sf.ImportColumns[i].Name = colnames[i];
                        sf.ImportColumns[i].DataType = dtypeStrings[i];
                        sf.ImportColumns[i].Width = widths[i];
                        if (getStats)
                        {
                            sf.Columns[i].NumDistinctValues = uniqueValues[i].Keys.Count;
                        }
                    }
                    // Pick key
                    if (findKeys)
                        sf.KeyIndices = pickKey(keySet, validKeys, dtypes, widths);
                }
                else
                {
                    //This is a reimport operation and information on original import is available
                    sf.UploadStatus = SourceFile.UPLOAD_NO_ERRORS;
                    ArrayList newSfcList = new ArrayList();
                    ArrayList newImportSfcList = new ArrayList();
                    int curSize = sf.ImportColumns.Length;
                    for (int i = 0; i < colnames.Length; i++)
                    {
                        SourceFile.SourceColumn importSfc = new SourceFile.SourceColumn();
                        importSfc.Name = colnames[i];
                        importSfc.DataType = dtypeStrings[i];
                        importSfc.Width = widths[i];
                        newImportSfcList.Add(importSfc);
                        SourceFile.SourceColumn sfc = null;
                        int index = -1;
                        if (i >= curSize || sf.ImportColumns[i].Name != colnames[i])
                        {
                            //It can be either a reorder or a brand new column
                            //Check if it is a reorder
                            for (int j = 0; j < sf.ImportColumns.Length; j++)
                            {
                                if (sf.ImportColumns[j].Name == colnames[i])
                                {
                                    index = j;
                                    break;
                                }
                            }
                            if (index == -1)
                            {
                                if (preventChanges)
                                {
                                    sf.UploadStatus = SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED;
                                    errors.Add("Column [" + colnames[i] + "] added");
                                }
                                else if (schemaLock && !sf.AllowAddColumns)
                                {
                                    sf.UploadStatus = SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED;
                                    errors.Add("Column [" + colnames[i] + "] added");
                                }
                                else
                                {
                                    //Brand new column
                                    sfc = new SourceFile.SourceColumn();
                                    sfc.Name = colnames[i];
                                    sfc.DataType = dtypeStrings[i];
                                    sfc.Width = widths[i];
                                    changes.Add("Column [" + colnames[i] + "] added");
                                }
                            }
                            else
                            {
                                //Reorder
                                if (preventChanges)
                                {
                                    sf.UploadStatus = SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED;
                                    errors.Add("Column [" + colnames[i] + "] moved to position " + i);
                                }
                                else if (schemaLock && !sf.AllowAddColumns)
                                {
                                    sf.UploadStatus = SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED;
                                    errors.Add("Column [" + colnames[i] + "] moved to position " + i);
                                }
                                else
                                {
                                    sfc = sf.Columns[index].clone();
                                    changes.Add("Column [" + colnames[i] + "] moved to position " + i);
                                }
                            }
                        }
                        else
                        {
                            //Same order, same name
                            sfc = sf.Columns[i].clone();
                            index = i;
                        }
                        if (index >=0 && index < curSize && sf.Columns[index].DataType == sf.ImportColumns[index].DataType && sf.ImportColumns[index].DataType != dtypeStrings[i])
                        {
                            bool up = false;
                            // update if not null
                            up = uniqueValues == null || uniqueValues[i].Count > 0;
                            if (!up && uniqueValues[i].Count == 0)
                            {
                                uniqueValues[i].Keys.GetEnumerator().MoveNext();
                                up = uniqueValues[i].Keys.GetEnumerator().Current != null;
                            }
                            if (up)
                            {
                                if (sfc.DataType.Equals("None"))
                                {
                                    changes.Add("Column [" + sfc.Name + "] data type changed from " + sfc.DataType + " to " + dtypeStrings[i]);
                                    sfc.DataType = dtypeStrings[i];
                                    if (sfc.Width < widths[i])
                                    {
                                        changes.Add("Column [" + sfc.Name + "] width changed from " + sfc.Width + " to " + widths[i]);
                                        sfc.Width = widths[i];
                                    }
                                }
                                else if (!dtypeStrings[i].Equals("None") && getHigherDataType(sfc.DataType, dtypeStrings[i]) > 0)
                                {
                                    if (sfc.PreventUpdate)
                                    {
                                        warnings.Add("Column [" + sfc.Name + "] has datatype of " + sfc.DataType + " and the type is locked" +
                                            ": not all incoming data can fit into this column - such values will be inserted as null");
                                    }
                                    else
                                    {
                                        // upcast the datatype
                                        if (preventChanges)
                                        {
                                            sf.UploadStatus = SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED;
                                            errors.Add("Column [" + sfc.Name + "] datatype changed from " + sfc.DataType + " to " + dtypeStrings[i]);
                                        }
                                        else if (schemaLock && !sf.AllowUpcast)
                                        {
                                            sf.UploadStatus = SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED;
                                            errors.Add("Column [" + sfc.Name + "] datatype changed from " + sfc.DataType + " to " + dtypeStrings[i]);
                                        }
                                        else
                                        {
                                            changes.Add("Column [" + sfc.Name + "] datatype changed from " + sfc.DataType + " to " + dtypeStrings[i]);
                                            sfc.DataType = dtypeStrings[i];
                                            if (sfc.Width < widths[i])
                                            {
                                                changes.Add("Column [" + sfc.Name + "] width changed from " + sfc.Width + " to " + widths[i]);
                                                sfc.Width = widths[i];
                                            }                                            
                                        }
                                    }
                                }
                                else if (!dtypeStrings[i].Equals("None") && getHigherDataType(sfc.DataType, dtypeStrings[i]) < 0)
                                {
                                    //Ignore downcast if not doable - do not fail the upload for such cases
                                    if (!preventChanges && !schemaLock && !sfc.PreventUpdate && allowDowncast)
                                    {
                                        changes.Add("Column [" + sfc.Name + "] datatype changed from " + sfc.DataType + " to " + dtypeStrings[i]);
                                        sfc.DataType = dtypeStrings[i];
                                        if (sfc.Width < widths[i])
                                        {
                                            changes.Add("Column [" + sfc.Name + "] width changed from " + sfc.Width + " to " + widths[i]);
                                            sfc.Width = widths[i];
                                        }
                                    }
                                }
                            }
                        }

                        if (index >= 0 && index < curSize && sf.Columns[index].Width != widths[i]
                            && dtypeStrings[i].Equals(sfc.DataType))
                        {
                            if (dtypeStrings[i].Equals("Varchar") && sfc.DataType.Equals("Varchar"))
                            {
                                if (sfc.Width < widths[i])
                                {
                                    //Expand varchar width
                                    if (preventChanges)
                                    {
                                        sf.UploadStatus = SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED;
                                        errors.Add("Column [" + sfc.Name + "] has width of " + sfc.Width + ": not wide enough to hold incoming data - " + widths[i]);
                                    }
                                    else if (schemaLock && sf.FailUploadOnVarcharExpansion)
                                    {
                                        sf.UploadStatus = SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED;
                                        errors.Add("Column [" + sfc.Name + "] has width of " + sfc.Width + ": not wide enough to hold incoming data - " + widths[i]);
                                    }
                                    else if (schemaLock && sf.AllowValueTruncationOnLoad)
                                    {
                                        warnings.Add("Column [" + sfc.Name + "] has width of " + sfc.Width +
                                            ": not wide enough to hold incoming data - may result in truncation of data on next processing");
                                    }
                                    else
                                    {
                                        sfc.Width = widths[i];
                                        changes.Add("Column [" + sfc.Name + "] width changed to " + widths[i]);
                                    }
                                }
                                else if (sfc.Width > widths[i] && !preventChanges && !sf.SchemaLock && allowWidthReduction)
                                {
                                    changes.Add("Column [" + sfc.Name + "] width changed to " + widths[i]);
                                    sfc.Width = widths[i];
                                }
                            }
                            else if (sfc.Width < widths[i])
                            {
                                changes.Add("Column [" + sfc.Name + "] width changed from " + sfc.Width + " to " + widths[i]);
                                sfc.Width = widths[i];
                            }
                        }
                        if (sfc != null)
                        {
                            if (getStats)
                                sfc.NumDistinctValues = uniqueValues[i].Keys.Count;
                            newSfcList.Add(sfc);
                        }
                    }

                    // Figure out if columns were dropped
                    for (int j = 0; j < sf.ImportColumns.Length; j++)
                    {
                        bool found = false;
                        for (int k = 0; k < colnames.Length; k++)
                        {
                            if (sf.ImportColumns[j].Name == colnames[k])
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            if (preventChanges)
                            {
                                sf.UploadStatus = SourceFile.UPLOAD_ERROR_SOURCE_FORMAT_LOCKED;
                                errors.Add("Column [" + sf.ImportColumns[j].Name + "] removed");
                            }
                            else if (schemaLock && !sf.AllowNullBindingRemovedColumns)
                            {
                                sf.UploadStatus = SourceFile.UPLOAD_ERROR_SCHEMA_CHANGED;
                                errors.Add("Column [" + sf.ImportColumns[j].Name + "] removed");
                            }
                            else
                            {
                                changes.Add("Column [" + sf.ImportColumns[j].Name + "] removed");
                            }
                        }
                    }

                    if (sf.UploadStatus == SourceFile.UPLOAD_NO_ERRORS)
                    {
                        sf.Columns = new SourceFile.SourceColumn[newSfcList.Count];
                        sf.ImportColumns = new SourceFile.SourceColumn[newSfcList.Count];
                        for (int i = 0; i < sf.Columns.Length; i++)
                        {
                            sf.Columns[i] = (SourceFile.SourceColumn)newSfcList[i];
                            sf.ImportColumns[i] = ((SourceFile.SourceColumn)newSfcList[i]).clone();
                            sf.ImportColumns[i].Name = ((SourceFile.SourceColumn)newImportSfcList[i]).Name;
                        }
                    }
                }
            }
            else
            {
                reader.Close();
                sf.Columns = new SourceFile.SourceColumn[0];
                sf.ImportColumns = new SourceFile.SourceColumn[0];
            }
            DataTable table = sf.getTable();
            sourceFileGridView.DataSource = table;
            return;
        }

        private static int getHigherDataType(string firstDataType, string secondDataType)
        {

            switch (firstDataType)
            {
                case "Integer": if (secondDataType.Equals("Integer"))
                        return 0;
                    else if (secondDataType.Equals("Float") || secondDataType.Equals("Varchar"))
                        return 1;
                    else if (secondDataType.Equals("None"))
                        return -1;
                    else // secondDataType.Equals("Date") || secondDataType.Equals("DateTime")
                        return -2;
                case "Float": if (secondDataType.Equals("Float"))
                        return 0;
                    else if (secondDataType.Equals("Varchar"))
                        return 1;
                    else if (secondDataType.Equals("Integer") || secondDataType.Equals("None"))
                        return -1;
                    else // secondDataType.Equals("Date") || secondDataType.Equals("DateTime")
                        return -2;
                case "Date": if (secondDataType.Equals("Date"))
                        return 0;
                    else if (secondDataType.Equals("DateTime") || secondDataType.Equals("Varchar"))
                        return 1;
                    else if (secondDataType.Equals("None"))
                        return -1;
                    else // secondDataType.Equals("Integer") || secondDataType.Equals("Float")
                        return -2;
                case "DateTime": if (secondDataType.Equals("DateTime"))
                        return 0;
                    else if (secondDataType.Equals("Varchar"))
                        return 1;
                    else if (secondDataType.Equals("Date") || secondDataType.Equals("None"))
                        return -1;
                    else // secondDataType.Equals("Integer") || secondDataType.Equals("Float")
                        return -2;
                case "Varchar": if (secondDataType.Equals("Varchar"))
                        return 0;
                    else if (secondDataType.Equals("None") || secondDataType.Equals("Integer")
                                          || secondDataType.Equals("Float") || secondDataType.Equals("Date") || secondDataType.Equals("DateTime"))
                        return -1;
                    else
                        return -2;
                case "None": if (secondDataType.Equals("None"))
                        return 0;
                    else if (secondDataType.Equals("Varchar") || secondDataType.Equals("Integer")
                                                   || secondDataType.Equals("Float") || secondDataType.Equals("Date") || secondDataType.Equals("DateTime"))
                        return 1;
                    else
                        return -2;
            }
            return -2;
        }

        private bool containsTimeComponent(DateTime dt, String s)
        {
            bool hasTime = true;
            if(dt != null)
            {
                if ((!s.Contains(":")) && dt.Hour == 0 && dt.Minute == 0 && dt.Second == 0 && dt.Millisecond == 0)
                {
                    hasTime = false;
                }
            }
            return hasTime;
        }

        private int score(int[] key, DType[] dtypes, int[] widths)
        {
            int score = 0;
            // Everything relative to an int (4 bytes)
            foreach (int index in key)
            {
                // The longer the varchar, the worse it is (assume double byte)
                if (dtypes[index] == DType.Varchar)
                    score += widths[index] * 2;
                else if (dtypes[index] == DType.DateTime || dtypes[index] == DType.Date)
                    score += 8;
                else if (dtypes[index] == DType.Float)
                {
                    if (ALLOW_FLOATS_AS_KEYS)
                        score += 8;
                    else
                        return (int.MaxValue);
                }
                else
                    score += 4;
            }
            return (score);
        }

        private int[] pickKey(List<int[]> keySet, Dictionary<string, object>[] validKeys, DType[] dtypes, int[] widths)
        {
            int[] curKey = null;
            int curScore = 0;
            for (int i = 0; i < keySet.Count; i++)
            {
                if (validKeys[i] != null)
                {
                    if (curKey == null)
                    {
                        curKey = keySet[i];
                        curScore = score(keySet[i], dtypes, widths);
                    }
                    else
                    {
                        /* 
                         * Give keys preferences based on presence of datatypes.
                         * Assign a penalty for each data type. Take the key with the lowest score
                         */
                        int newScore = score(keySet[i], dtypes, widths);
                        if (newScore < curScore)
                        {
                            curKey = keySet[i];
                            curScore = newScore;
                        }
                    }
                }
            }
            return (curKey);
        }

        private static int NUM_LINES_TO_SCAN_FOR_KEYS = 1000;
        private static int MAX_KEY_SIZE = 4;
        public List<int[]> scanKeys(string fname, char sep, int topLinesToSkip, int bottomLinesToSkip, string separator, bool onlyQuoteAfterSeparator, Encoding encoding, bool ignoreBackslash, bool forceMinColumns, int numColumns, string quotestr, bool firstRowHeaders, bool ignoreCarriageReturn)
        {
            List<string[]> buff = getFieldBuffer(fname, sep, NUM_LINES_TO_SCAN_FOR_KEYS, topLinesToSkip, bottomLinesToSkip, separator, onlyQuoteAfterSeparator, encoding, ignoreBackslash, forceMinColumns, numColumns, quotestr, firstRowHeaders, ignoreCarriageReturn);
            int maxKeySize = MAX_KEY_SIZE;
            // Prevent out of memory by looking for too complex keys in wide tables
            if (buff[0].Length > 50)
                maxKeySize = Math.Min(3, maxKeySize);
            if (buff[0].Length > 100)
                maxKeySize = Math.Min(2, maxKeySize);
            if (buff[0].Length > 300)
                maxKeySize = Math.Min(1, maxKeySize);
            List<int[]> keySet = generateKeySets(buff[0].Length, maxKeySize);
            List<int[]> resultSet = new List<int[]>();
            int minSize = int.MaxValue;
            foreach (int[] key in keySet)
            {
                if (key.Length > minSize)
                    continue;
                Dictionary<string, object> values = new Dictionary<string, object>();
                bool nonUnique = false;
                bool nextKey = false;
                int missedRows = 0;
                int maxMissed = (int)(0.5f * ((float)buff.Count));
                for (int i = 0; i < buff.Count; i++)
                {
                    bool nextRow = false;
                    bool containsNull = false;
                    StringBuilder sb = new StringBuilder();
                    for (int j = 0; j < key.Length; j++)
                    {
                        /*
                         * If there aren't enough columns in the row, skip the row
                         */
                        if (buff[i].Length <= key[j])
                        {
                            nextRow = true;
                            break;
                        }
                        if (buff[i][key[j]] == null || buff[i][key[j]].Length == 0)
                        {
                            /* 
                             * Don't allow keys that contain null values - they cannot be
                             * used for joins
                             */
                            containsNull = true;
                            break;
                        }
                        sb.Append("{" + buff[i][key[j]] + "}");
                    }
                    if (nextRow)
                    {
                        /*
                         * If the row is skipped, count it. If too many rows are skipped,
                         * then skip the whole key (not enough data)
                         */
                        if (missedRows++ > maxMissed)
                        {
                            nextKey = true;
                            break;
                        }
                        else
                            continue;
                    }
                    string keyValue = sb.ToString();
                    if (containsNull || values.ContainsKey(keyValue))
                    {
                        nonUnique = true;
                        break;
                    }
                    else
                        values.Add(keyValue, null);
                }
                if (nextKey)
                    continue;
                if (!nonUnique)
                {
                    resultSet.Add(key);
                    if (key.Length < minSize)
                        minSize = key.Length;
                }
            }
            return (resultSet);
        }

        public List<string[]> getFieldBuffer(string filename, char sep, int maxLines, int topLinesToSkip, int bottomLinesToSkip, string separator, bool onlyQuoteAfterSeparator, Encoding encoding, bool ignoreBackslash, bool forceMinColumns, int numColumns, string quotestr, bool firstRowHeaders, bool ignoreCarriageReturn)
        {
            StreamReader reader = null;
            if (encoding == null)
                reader = new StreamReader(filename);
            else
                reader = new StreamReader(filename, encoding);
            List<string[]> buff = new List<string[]>(maxLines);
            string line = null;
            int linesRead = 0;
            while ((line = readLine(reader, separator, ignoreBackslash, quotestr, forceMinColumns && !(firstRowHeaders && linesRead == 0) ? numColumns - 1 : 0, onlyQuoteAfterSeparator, ignoreCarriageReturn)) != null && buff.Count < maxLines)
            {
                linesRead++;
                string[] split = Util.splitString(line, sep);
                // Ignore blank lines
                if (split.Length > 1 || (split.Length == 1 && split[0].Length > 0))
                    buff.Add(split);
            }
            reader.Close();
            return (buff);
        }

        public static List<int[]> generateKeySets(int numKeys, int maxSetSize)
        {
            int keySize = 1;
            List<int[]> keySet = new List<int[]>(maxSetSize * (maxSetSize - 1));
            // Generate key sets
            int[][] keys = null;
            while (keySize <= maxSetSize && keySize <= numKeys)
            {
                try
                {
                    if (keys == null)
                    {
                        keys = new int[numKeys][];
                        for (int col = 0; col < numKeys; col++)
                        {
                            keys[col] = new int[] { col };
                        }
                    }
                    else
                        keys = compoundKeys(keys, numKeys);
                    foreach (int[] key in keys)
                        keySet.Add(key);
                    keySize++;
                }
                catch (OutOfMemoryException)
                {
                    break;
                }
            }
            return keySet;
        }

        public static int[][] compoundKeys(int[][] keys, int numCols)
        {
            int keySize = keys[0].Length + 1;
            int resultSize = 1;
            for (int i = 0; i < keySize; i++)
            {
                resultSize *= numCols - i;
                resultSize /= i + 1;
            }
            int[][] result = new int[resultSize][];
            int count = 0;
            foreach (int[] key in keys)
            {
                int start = key[key.Length - 1] + 1;
                for (int i = start; i < numCols; i++)
                {
                    int[] val = new int[keySize];
                    for (int j = 0; j < key.Length; j++)
                        val[j] = key[j];
                    val[key.Length] = i;
                    result[count++] = val;
                }
            }
            return (result);
        }

        public static string readLine(StreamReader reader, string separator, bool ignoreBackslash, string quotestr, int minSeparators, bool onlyQuoteAfterSeparator, bool ignoreCarriageReturn)
        {
            if (reader.EndOfStream) return (null);
            StringBuilder sb = new StringBuilder();
            int ci;
            bool quote = false;
            bool lastEscape = false;
		    int sepchar = 0;
		    bool lastSep = true;
            int numSeparators = 0;
            bool recognizeQuotes = quotestr != null && quotestr.Length > 0;
            while ((ci = reader.Read()) != -1)
            {
                // Prevent runaway buffer overflow if quote not terminated
                if (sb.Length > MAX_LINE_LENGTH)
                    continue;
                char c = (char)ci;
                if (ignoreBackslash && c == '\\')
                    continue;
                if (recognizeQuotes && c == quotestr[0] && (!onlyQuoteAfterSeparator || (lastSep && !quote) || quote))
                {
                    if (!lastEscape)
                        quote = !quote;
                    sb.Append(c);
                }
                else if (!quote)
                {
//                    if (minSeparators > 0 && separator != null && c == separator[sepchar])
                    if (separator != null && c == separator[sepchar])
                    {
                        sepchar++;
                        if (sepchar == separator.Length)
                        {
                            sepchar = 0;
                            lastSep = true;
                            numSeparators++;
                        }
                    }
                    else
                    {
                        sepchar = 0;
                        lastSep = false;
                    }
                    if ((!ignoreCarriageReturn && c == '\r') || c == '\n')
                    {
                        if (sb.Length > 0)
                        {
                            if (minSeparators <= 0 || numSeparators >= minSeparators)
                                return sb.ToString();
                            else
                                sb.Append(c);
                        }
                    }
                    else
                        sb.Append(c);
                }
                else
                {
                    sb.Append(c);
                }
                if (c == '\\' && !lastEscape)
                    lastEscape = true;
                else
                    lastEscape = false;
            }
            return (sb.ToString());
        }

        public void removeSourceFile(string name)
        {
            SourceFile sfToBeRemoved = null;
            sourceFiles.updateRepositoryDirectory(ma.getRepositoryDirectory(), ma);
            sourceFiles.refreshIfNecessary(ma);
            foreach (SourceFile sf in sourceFiles.variableList)
            {
                if (sf.FileName == name)
                {
                    sfToBeRemoved = sf;
                    break;
                }
            }
            if (sfToBeRemoved != null)
   	        {
	    		sourceFiles.deleteItem(sfToBeRemoved, ma);
            }
            setupNodes(rootNode);
        }

        private void removeSourceFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode == null) return;
            removeSourceFile(curNode.Text);
        }

        private void newSourceFileStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode tn = new TreeNode("New Source");
            tn.ImageIndex = 26;
            tn.SelectedImageIndex = 26;
            rootNode.Nodes.Add(tn);
            SourceFile sf = new SourceFile();
            sf.FileName = "New Source";
            sf.FormatType = SourceFile.FileFormatType.FixedWidth;
            DataTable table = sf.getTable();
            sourceFileGridView.DataSource = table;
            sourceFiles.Add(sf);
            ma.mainTree.SelectedNode = tn;
            tn.Tag = sf;
        }

        public SourceFile sourceFileExists(string sfname)
        {
            if (sourceFiles != null)
            {
                foreach (SourceFile sourceFile in sourceFiles.variableList)
                {
                    if (sfname.ToLower() == sourceFile.FileName.ToLower())
                    {
                        return (sourceFile);
                    }
                }
            }
            return null;
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode && curNode != null)
            {
                if (sourceFileExists(e.Label) != null)
                {
                    MessageBox.Show("Source file definition already exists - " + e.Label, "Error");
                    e.CancelEdit = true;
                    return;
                }
                curNode.Text = e.Label;
                sourceFiles.sourceFiles[curNode.Index].FileName = e.Label;
            }
        }

        #endregion

        private void sourceFileGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex >= 0)
                return;
            sourceFileGridView.DoDragDrop(e.RowIndex, DragDropEffects.Move);
        }

        private void sourceFileGridView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Int32", true))
            {
                e.Effect = e.AllowedEffect;
            }
        }

        private void sourceFileGridView_DragDrop(object sender, DragEventArgs e)
        {
            int startIndex = (int)e.Data.GetData(typeof(Int32));
            for (int i = 0; i < sourceFileGridView.Rows.Count; i++)
            {
                Point p = sourceFileGridView.PointToClient(new Point(e.X, e.Y));
                if (sourceFileGridView.GetRowDisplayRectangle(i, false).Contains(p))
                {
                    if (i != startIndex)
                    {
                        DataTable dt = (DataTable)sourceFileGridView.DataSource;
                        DataRow dr = dt.Rows[startIndex];
                        DataRow newdr = dt.NewRow();
                        for (int j = 0; j < dr.ItemArray.Length; j++)
                            newdr[j] = dr[j];
                        dt.Rows.InsertAt(newdr, i);
                        dt.Rows.RemoveAt(i < startIndex ? startIndex + 1 : startIndex);
                    }
                }
            }
        }


        #region DropModule Members
        
        public void doDrop(TreeNode sourceNode, TreeNode targetNode)
        {
            if (ma.getMainNode(sourceNode).Text == "Data Sources" && sourceNode.Text != "Data Sources")
            {
                List<SourceFile> list = ma.sourceFileMod.getSourceFiles();
                int sourceNodeIndex = rootNode.Nodes.IndexOf(sourceNode);
                int targetNodeIndex = 0;
                if (targetNode.Text != "Data Sources")
                {
                    targetNodeIndex = rootNode.Nodes.IndexOf(targetNode);
                }
                if (sourceNodeIndex==targetNodeIndex)
                    return;
                list[sourceNodeIndex].amIBeingDragged = true;
                rootNode.Nodes.RemoveAt(sourceNodeIndex);
                SourceFile sourceSourceFile = list[sourceNodeIndex];                
                list.RemoveAt(sourceNodeIndex);
                rootNode.Nodes.Insert(targetNodeIndex, sourceNode);
                list.Insert(targetNodeIndex, sourceSourceFile);
                list[targetNodeIndex].amIBeingDragged = false;
                if (sourceNodeIndex >= targetNodeIndex)
                    selectedNode(rootNode.Nodes[sourceNodeIndex+1]);
                else if (sourceNodeIndex < targetNodeIndex)
                    selectedNode(rootNode.Nodes[sourceNodeIndex]);
            }
        }
        
        public DragDropEffects startDrag(TreeNode tn)
        {
            if (curNode == null || tn == null) return (DragDropEffects.None);
            if (curNode.Index != tn.Index) return (DragDropEffects.None);
            if ((tn.Parent != null && tn.Parent.Text == "Data Sources"))
                return (DragDropEffects.Move);                
            return (DragDropEffects.None);            
        }

        #endregion

        private void updateSourceFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode == null) return;
            implicitSave();
            SourceFile sfToBeUpdated = null;
            foreach (SourceFile sf in sourceFiles.variableList)
            {
                if (sf.FileName == curNode.Text)
                {
                    sfToBeUpdated = sf;
                    break;
                }
            }
            if (curNode == null || sfToBeUpdated == null)
            {
                return;
            }

            importSourceFileDialog.Title = "Update Data Source";
            DialogResult result = importSourceFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filename = importSourceFileDialog.FileName;
                FileInfo fi = new FileInfo(filename);
                string nodeName = fi.Name;
                if (sourceFileExists(nodeName) == null)
                {
                    MessageBox.Show("Source file definition does not exist - " + nodeName, "Error");
                    return;
                }
                List<string> errors;
                List<string> warnings;
                List<string> changes;
                importSourceFile(fi.DirectoryName, sfToBeUpdated, curNode, true, NUM_LINES_TO_SCAN, out errors, out warnings, out changes, false, false, 1, false);
                ma.mainTree.SelectedNode = curNode;
            }
        }

        private void sourceFileGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            sourceFileGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void pasteNames_Click(object sender, EventArgs e)
        {
            if (!Clipboard.ContainsText())
                return;
            string text = Clipboard.GetText();
            string[] cols = text.Split('\n');
            DataTable dt = (DataTable)sourceFileGridView.DataSource;
            for (int i = 0; i < cols.Length && i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = cols[i].Trim(new char[] {'\r'});
            }
        }

        public SourceFile getSourceFile(string filename)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            sourceFiles.refreshIfNecessary(ma);
            foreach (SourceFile sf in sourceFiles.variableList)
            {
                if (sf.FileName.ToLower() == filename.ToLower())
                    return (sf);
            }
            return (null);
        }

        private void sourceFileGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (curNode != null)
            {
                SourceFile sf = getSourceFile(((SourceFile)curNode.Tag).FileName);
                if (sf != null && importColumns != null && importColumns.Length > 0)
                {
                    SourceFile.SourceColumn[] newImportColumns = new SourceFile.SourceColumn[importColumns.Length - 1];
                    for (int i = 0, j = 0; i < importColumns.Length; i++)
                    {
                        if (i != e.Row.Index)
                            newImportColumns[j++] = importColumns[i];
                    }
                    importColumns = newImportColumns;
                    sf.ImportColumns = importColumns;
                }
            }
        }

        private void schemaLockBox_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                //enable all suboption
                allowAddColumnsBox.Enabled = true;
                allowNullBindingRemovedColumnsBox.Enabled = true;
                allowUpcastBox.Enabled = true;
                allowVarcharExpansionRadioButton.Enabled = true;
                failUploadOnVarcharExpansionRadioButton.Enabled = true;
                allowValueTruncationOnLoadRadioButton.Enabled = true;
                //mark default sub-options true
                allowAddColumnsBox.Checked = true;
                allowNullBindingRemovedColumnsBox.Checked = false;
                allowUpcastBox.Checked = false;
                allowVarcharExpansionRadioButton.Checked = false;
                failUploadOnVarcharExpansionRadioButton.Checked = false;
                allowValueTruncationOnLoadRadioButton.Checked = true;
                //uncheck lock format
                lockFormatBox.Checked = false;
            }
            else
            {
                //disable all suboption
                allowAddColumnsBox.Enabled = false;
                allowNullBindingRemovedColumnsBox.Enabled = false;
                allowUpcastBox.Enabled = false;
                allowVarcharExpansionRadioButton.Enabled = false;
                failUploadOnVarcharExpansionRadioButton.Enabled = false;
                allowValueTruncationOnLoadRadioButton.Enabled = false;
                //clear all suboptions
                allowAddColumnsBox.Checked = false;
                allowNullBindingRemovedColumnsBox.Checked = false;
                allowUpcastBox.Checked = false;
                allowVarcharExpansionRadioButton.Checked = false;
                failUploadOnVarcharExpansionRadioButton.Checked = false;
                allowValueTruncationOnLoadRadioButton.Checked = false; 
                
            }
        }

        private void lockFormatBox_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                schemaLockBox.Checked = false;
            }
        }

        public bool removeSourceFile(SourceFile sf)
        {
            removeSourceFile(sf.FileName);
            return true;
        }

        public void updateSourceFile(SourceFile sf, string username)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            sourceFiles.updateItem(sf, username, "SourceFile", ma);
        }

        public void updateSourceFileWithoutLock(SourceFile sf, string username)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            sourceFiles.updateItemWithoutLock(sf, username, "SourceFile", ma);
        }

        public void isNewOrUpToDate(SourceFile sf)
        {
            setRepositoryDirectory(ma.getRepositoryDirectory());
            sourceFiles.isNewOrUpToDate(sf, "SourceFile", ma);
        }
    }
}