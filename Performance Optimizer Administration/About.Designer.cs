namespace Performance_Optimizer_Administration
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.toolName = new System.Windows.Forms.Label();
            this.copyright = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.repositoryVersion = new System.Windows.Forms.Label();
            this.buildVersion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // toolName
            // 
            this.toolName.AutoSize = true;
            this.toolName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolName.Location = new System.Drawing.Point(17, 22);
            this.toolName.Name = "toolName";
            this.toolName.Size = new System.Drawing.Size(94, 20);
            this.toolName.TabIndex = 0;
            this.toolName.Text = "Tool Name";
            this.toolName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // copyright
            // 
            this.copyright.AutoSize = true;
            this.copyright.Location = new System.Drawing.Point(36, 125);
            this.copyright.Name = "copyright";
            this.copyright.Size = new System.Drawing.Size(81, 13);
            this.copyright.TabIndex = 1;
            this.copyright.Text = "Copyright String";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.okButton.Location = new System.Drawing.Point(247, 181);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(81, 30);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // repositoryVersion
            // 
            this.repositoryVersion.AutoSize = true;
            this.repositoryVersion.Location = new System.Drawing.Point(36, 96);
            this.repositoryVersion.Name = "repositoryVersion";
            this.repositoryVersion.Size = new System.Drawing.Size(125, 13);
            this.repositoryVersion.TabIndex = 3;
            this.repositoryVersion.Text = "Repository Version String";
            // 
            // buildVersion
            // 
            this.buildVersion.AutoSize = true;
            this.buildVersion.Location = new System.Drawing.Point(36, 67);
            this.buildVersion.Name = "buildVersion";
            this.buildVersion.Size = new System.Drawing.Size(98, 13);
            this.buildVersion.TabIndex = 4;
            this.buildVersion.Text = "Build Version String";
            // 
            // About
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 219);
            this.ControlBox = false;
            this.Controls.Add(this.buildVersion);
            this.Controls.Add(this.repositoryVersion);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.toolName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "About";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label toolName;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label repositoryVersion;
        private System.Windows.Forms.Label buildVersion;
    }
}