namespace Performance_Optimizer_Administration
{
    partial class Hierarchies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.hierarchyTabPage = new System.Windows.Forms.TabPage();
            this.hierarchyPanel = new System.Windows.Forms.Panel();
            this.sourceGroupBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.webLocked = new System.Windows.Forms.CheckBox();
            this.hiddenBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dimKeyLevelBox = new System.Windows.Forms.ComboBox();
            this.hierarchyNoMeasureFilter = new System.Windows.Forms.ComboBox();
            this.label176 = new System.Windows.Forms.Label();
            this.hierarchyExclusionFilter = new System.Windows.Forms.ComboBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.hierarchyDimension = new System.Windows.Forms.ComboBox();
            this.levelPanel = new System.Windows.Forms.Panel();
            this.generateCurrentCheckBox = new System.Windows.Forms.CheckBox();
            this.typeIISCD = new System.Windows.Forms.CheckBox();
            this.degenerateTable = new System.Windows.Forms.CheckBox();
            this.toggleHidden = new System.Windows.Forms.Button();
            this.generateDimTableCheckBox = new System.Windows.Forms.CheckBox();
            this.removeKeyButton = new System.Windows.Forms.Button();
            this.editKeyButton = new System.Windows.Forms.Button();
            this.newKeyButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.keyListBox = new System.Windows.Forms.ListBox();
            this.levelColumns = new System.Windows.Forms.ListView();
            this.lcolnameHeader = new System.Windows.Forms.ColumnHeader();
            this.hiddenHeader = new System.Windows.Forms.ColumnHeader();
            this.cardinalityBox = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.hiearchyMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newHierarchyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharedLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharedLevel = new System.Windows.Forms.ToolStripComboBox();
            this.addSharedLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertLevelAboveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.hierarchyTabPage.SuspendLayout();
            this.hierarchyPanel.SuspendLayout();
            this.levelPanel.SuspendLayout();
            this.hiearchyMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.hierarchyTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // hierarchyTabPage
            // 
            this.hierarchyTabPage.AutoScroll = true;
            this.hierarchyTabPage.Controls.Add(this.hierarchyPanel);
            this.hierarchyTabPage.Controls.Add(this.levelPanel);
            this.hierarchyTabPage.Location = new System.Drawing.Point(4, 22);
            this.hierarchyTabPage.Name = "hierarchyTabPage";
            this.hierarchyTabPage.Size = new System.Drawing.Size(844, 707);
            this.hierarchyTabPage.TabIndex = 10;
            this.hierarchyTabPage.Text = "Hierarchies";
            this.hierarchyTabPage.UseVisualStyleBackColor = true;
            this.hierarchyTabPage.Leave += new System.EventHandler(this.hierarchyTabPage_Leave);
            // 
            // hierarchyPanel
            // 
            this.hierarchyPanel.AutoSize = true;
            this.hierarchyPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hierarchyPanel.Controls.Add(this.sourceGroupBox);
            this.hierarchyPanel.Controls.Add(this.label3);
            this.hierarchyPanel.Controls.Add(this.webLocked);
            this.hierarchyPanel.Controls.Add(this.hiddenBox);
            this.hierarchyPanel.Controls.Add(this.label2);
            this.hierarchyPanel.Controls.Add(this.dimKeyLevelBox);
            this.hierarchyPanel.Controls.Add(this.hierarchyNoMeasureFilter);
            this.hierarchyPanel.Controls.Add(this.label176);
            this.hierarchyPanel.Controls.Add(this.hierarchyExclusionFilter);
            this.hierarchyPanel.Controls.Add(this.label134);
            this.hierarchyPanel.Controls.Add(this.label67);
            this.hierarchyPanel.Controls.Add(this.hierarchyDimension);
            this.hierarchyPanel.Location = new System.Drawing.Point(8, 9);
            this.hierarchyPanel.Name = "hierarchyPanel";
            this.hierarchyPanel.Size = new System.Drawing.Size(796, 118);
            this.hierarchyPanel.TabIndex = 1;
            this.hierarchyPanel.Visible = false;
            // 
            // sourceGroupBox
            // 
            this.sourceGroupBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceGroupBox.Location = new System.Drawing.Point(280, 94);
            this.sourceGroupBox.Name = "sourceGroupBox";
            this.sourceGroupBox.Size = new System.Drawing.Size(192, 20);
            this.sourceGroupBox.TabIndex = 90;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(256, 24);
            this.label3.TabIndex = 89;
            this.label3.Text = "Source Groups";
            // 
            // webLocked
            // 
            this.webLocked.AutoSize = true;
            this.webLocked.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.webLocked.Location = new System.Drawing.Point(500, 69);
            this.webLocked.Name = "webLocked";
            this.webLocked.Size = new System.Drawing.Size(110, 17);
            this.webLocked.TabIndex = 88;
            this.webLocked.Text = "Locked in Web UI";
            this.webLocked.UseVisualStyleBackColor = true;
            // 
            // hiddenBox
            // 
            this.hiddenBox.AutoSize = true;
            this.hiddenBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hiddenBox.Location = new System.Drawing.Point(500, 46);
            this.hiddenBox.Name = "hiddenBox";
            this.hiddenBox.Size = new System.Drawing.Size(205, 17);
            this.hiddenBox.TabIndex = 87;
            this.hiddenBox.Text = "Hidden in Autogenerated Subject Area";
            this.hiddenBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(497, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 23);
            this.label2.TabIndex = 86;
            this.label2.Text = "Dimension Key Level";
            // 
            // dimKeyLevelBox
            // 
            this.dimKeyLevelBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dimKeyLevelBox.Location = new System.Drawing.Point(644, 12);
            this.dimKeyLevelBox.Name = "dimKeyLevelBox";
            this.dimKeyLevelBox.Size = new System.Drawing.Size(149, 21);
            this.dimKeyLevelBox.TabIndex = 85;
            // 
            // hierarchyNoMeasureFilter
            // 
            this.hierarchyNoMeasureFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hierarchyNoMeasureFilter.Location = new System.Drawing.Point(280, 70);
            this.hierarchyNoMeasureFilter.Name = "hierarchyNoMeasureFilter";
            this.hierarchyNoMeasureFilter.Size = new System.Drawing.Size(192, 21);
            this.hierarchyNoMeasureFilter.TabIndex = 84;
            // 
            // label176
            // 
            this.label176.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.Location = new System.Drawing.Point(16, 70);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(248, 24);
            this.label176.TabIndex = 83;
            this.label176.Text = "Filter to Apply When No Measures Used";
            // 
            // hierarchyExclusionFilter
            // 
            this.hierarchyExclusionFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hierarchyExclusionFilter.Location = new System.Drawing.Point(280, 46);
            this.hierarchyExclusionFilter.Name = "hierarchyExclusionFilter";
            this.hierarchyExclusionFilter.Size = new System.Drawing.Size(192, 21);
            this.hierarchyExclusionFilter.TabIndex = 81;
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(16, 46);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(256, 24);
            this.label134.TabIndex = 82;
            this.label134.Text = "Filter to Apply When Dimension Not Used";
            // 
            // label67
            // 
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(16, 14);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(72, 23);
            this.label67.TabIndex = 70;
            this.label67.Text = "Dimension";
            // 
            // hierarchyDimension
            // 
            this.hierarchyDimension.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hierarchyDimension.Location = new System.Drawing.Point(88, 14);
            this.hierarchyDimension.Name = "hierarchyDimension";
            this.hierarchyDimension.Size = new System.Drawing.Size(224, 21);
            this.hierarchyDimension.TabIndex = 69;
            // 
            // levelPanel
            // 
            this.levelPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.levelPanel.AutoScroll = true;
            this.levelPanel.Controls.Add(this.generateCurrentCheckBox);
            this.levelPanel.Controls.Add(this.typeIISCD);
            this.levelPanel.Controls.Add(this.degenerateTable);
            this.levelPanel.Controls.Add(this.toggleHidden);
            this.levelPanel.Controls.Add(this.generateDimTableCheckBox);
            this.levelPanel.Controls.Add(this.removeKeyButton);
            this.levelPanel.Controls.Add(this.editKeyButton);
            this.levelPanel.Controls.Add(this.newKeyButton);
            this.levelPanel.Controls.Add(this.label1);
            this.levelPanel.Controls.Add(this.keyListBox);
            this.levelPanel.Controls.Add(this.levelColumns);
            this.levelPanel.Controls.Add(this.cardinalityBox);
            this.levelPanel.Controls.Add(this.label77);
            this.levelPanel.Controls.Add(this.label68);
            this.levelPanel.Location = new System.Drawing.Point(8, 9);
            this.levelPanel.Name = "levelPanel";
            this.levelPanel.Size = new System.Drawing.Size(828, 497);
            this.levelPanel.TabIndex = 2;
            this.levelPanel.Visible = false;
            // 
            // generateCurrentCheckBox
            // 
            this.generateCurrentCheckBox.AutoSize = true;
            this.generateCurrentCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateCurrentCheckBox.Location = new System.Drawing.Point(386, 33);
            this.generateCurrentCheckBox.Name = "generateCurrentCheckBox";
            this.generateCurrentCheckBox.Size = new System.Drawing.Size(280, 17);
            this.generateCurrentCheckBox.TabIndex = 97;
            this.generateCurrentCheckBox.Text = "Generate current values version of the dimension table";
            this.generateCurrentCheckBox.UseVisualStyleBackColor = true;
            // 
            // typeIISCD
            // 
            this.typeIISCD.AutoSize = true;
            this.typeIISCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.typeIISCD.Location = new System.Drawing.Point(192, 33);
            this.typeIISCD.Name = "typeIISCD";
            this.typeIISCD.Size = new System.Drawing.Size(189, 17);
            this.typeIISCD.TabIndex = 96;
            this.typeIISCD.Text = "Type II Slowly Changing Dimension";
            this.typeIISCD.UseVisualStyleBackColor = true;
            this.typeIISCD.CheckedChanged += new System.EventHandler(this.typeIISCD_CheckedChanged);
            // 
            // degenerateTable
            // 
            this.degenerateTable.AutoSize = true;
            this.degenerateTable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.degenerateTable.Location = new System.Drawing.Point(386, 7);
            this.degenerateTable.Name = "degenerateTable";
            this.degenerateTable.Size = new System.Drawing.Size(161, 17);
            this.degenerateTable.TabIndex = 95;
            this.degenerateTable.Text = "Degenerate Dimension Table";
            this.degenerateTable.UseVisualStyleBackColor = true;
            this.degenerateTable.CheckedChanged += new System.EventHandler(this.degenerateTable_CheckedChanged);
            // 
            // toggleHidden
            // 
            this.toggleHidden.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toggleHidden.Location = new System.Drawing.Point(397, 67);
            this.toggleHidden.Name = "toggleHidden";
            this.toggleHidden.Size = new System.Drawing.Size(88, 23);
            this.toggleHidden.TabIndex = 94;
            this.toggleHidden.Text = "Toggle Hidden";
            this.toggleHidden.UseVisualStyleBackColor = true;
            this.toggleHidden.Click += new System.EventHandler(this.toggleHidden_Click);
            // 
            // generateDimTableCheckBox
            // 
            this.generateDimTableCheckBox.AutoSize = true;
            this.generateDimTableCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateDimTableCheckBox.Location = new System.Drawing.Point(192, 7);
            this.generateDimTableCheckBox.Name = "generateDimTableCheckBox";
            this.generateDimTableCheckBox.Size = new System.Drawing.Size(149, 17);
            this.generateDimTableCheckBox.TabIndex = 93;
            this.generateDimTableCheckBox.Text = "Generate Dimension Table";
            this.generateDimTableCheckBox.UseVisualStyleBackColor = true;
            this.generateDimTableCheckBox.CheckedChanged += new System.EventHandler(this.generateDimTableCheckBox_CheckedChanged);
            // 
            // removeKeyButton
            // 
            this.removeKeyButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeKeyButton.Location = new System.Drawing.Point(644, 337);
            this.removeKeyButton.Name = "removeKeyButton";
            this.removeKeyButton.Size = new System.Drawing.Size(81, 20);
            this.removeKeyButton.TabIndex = 91;
            this.removeKeyButton.Text = "Remove Key";
            this.removeKeyButton.Click += new System.EventHandler(this.removeKeyButton_Click);
            // 
            // editKeyButton
            // 
            this.editKeyButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.editKeyButton.Location = new System.Drawing.Point(644, 311);
            this.editKeyButton.Name = "editKeyButton";
            this.editKeyButton.Size = new System.Drawing.Size(81, 20);
            this.editKeyButton.TabIndex = 90;
            this.editKeyButton.Text = "Edit Key";
            this.editKeyButton.Click += new System.EventHandler(this.editKeyButton_Click);
            // 
            // newKeyButton
            // 
            this.newKeyButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.newKeyButton.Location = new System.Drawing.Point(644, 285);
            this.newKeyButton.Name = "newKeyButton";
            this.newKeyButton.Size = new System.Drawing.Size(81, 20);
            this.newKeyButton.TabIndex = 89;
            this.newKeyButton.Text = "New Key";
            this.newKeyButton.Click += new System.EventHandler(this.newKeyButton_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 88;
            this.label1.Text = "Keys";
            // 
            // keyListBox
            // 
            this.keyListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.keyListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.keyListBox.FormattingEnabled = true;
            this.keyListBox.Location = new System.Drawing.Point(88, 285);
            this.keyListBox.Name = "keyListBox";
            this.keyListBox.Size = new System.Drawing.Size(550, 132);
            this.keyListBox.TabIndex = 87;
            this.keyListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.keyListBox_DrawItem);
            // 
            // levelColumns
            // 
            this.levelColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.levelColumns.CheckBoxes = true;
            this.levelColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lcolnameHeader,
            this.hiddenHeader});
            this.levelColumns.FullRowSelect = true;
            this.levelColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.levelColumns.HideSelection = false;
            this.levelColumns.Location = new System.Drawing.Point(88, 67);
            this.levelColumns.Name = "levelColumns";
            this.levelColumns.ShowItemToolTips = true;
            this.levelColumns.Size = new System.Drawing.Size(303, 212);
            this.levelColumns.TabIndex = 83;
            this.levelColumns.UseCompatibleStateImageBehavior = false;
            this.levelColumns.View = System.Windows.Forms.View.Details;
            this.levelColumns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.levelColumns_ItemChecked);
            // 
            // lcolnameHeader
            // 
            this.lcolnameHeader.Text = "Column";
            this.lcolnameHeader.Width = 222;
            // 
            // hiddenHeader
            // 
            this.hiddenHeader.Text = "Hidden";
            this.hiddenHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cardinalityBox
            // 
            this.cardinalityBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cardinalityBox.Location = new System.Drawing.Point(88, 6);
            this.cardinalityBox.Name = "cardinalityBox";
            this.cardinalityBox.Size = new System.Drawing.Size(72, 20);
            this.cardinalityBox.TabIndex = 81;
            // 
            // label77
            // 
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(16, 6);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(72, 23);
            this.label77.TabIndex = 80;
            this.label77.Text = "Cardinality";
            // 
            // label68
            // 
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(16, 67);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(64, 23);
            this.label68.TabIndex = 74;
            this.label68.Text = "Columns";
            // 
            // hiearchyMenu
            // 
            this.hiearchyMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newHierarchyToolStripMenuItem,
            this.newLevelToolStripMenuItem,
            this.sharedLevelToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.insertLevelAboveToolStripMenuItem});
            this.hiearchyMenu.Name = "hiearchyMenu";
            this.hiearchyMenu.Size = new System.Drawing.Size(171, 114);
            // 
            // newHierarchyToolStripMenuItem
            // 
            this.newHierarchyToolStripMenuItem.Name = "newHierarchyToolStripMenuItem";
            this.newHierarchyToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.newHierarchyToolStripMenuItem.Text = "New Hierarchy";
            this.newHierarchyToolStripMenuItem.Click += new System.EventHandler(this.newHierarchyMenuItem_Click);
            // 
            // newLevelToolStripMenuItem
            // 
            this.newLevelToolStripMenuItem.Name = "newLevelToolStripMenuItem";
            this.newLevelToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.newLevelToolStripMenuItem.Text = "New Level";
            this.newLevelToolStripMenuItem.Click += new System.EventHandler(this.newLevelMenuItem_Click);
            // 
            // sharedLevelToolStripMenuItem
            // 
            this.sharedLevelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sharedLevel,
            this.addSharedLevel});
            this.sharedLevelToolStripMenuItem.Name = "sharedLevelToolStripMenuItem";
            this.sharedLevelToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.sharedLevelToolStripMenuItem.Text = "Shared Level";
            this.sharedLevelToolStripMenuItem.Visible = false;
            // 
            // sharedLevel
            // 
            this.sharedLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sharedLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sharedLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.sharedLevel.Name = "sharedLevel";
            this.sharedLevel.Size = new System.Drawing.Size(121, 21);
            this.sharedLevel.SelectedIndexChanged += new System.EventHandler(this.sharedLevel_SelectedIndexChanged);
            // 
            // addSharedLevel
            // 
            this.addSharedLevel.Name = "addSharedLevel";
            this.addSharedLevel.Size = new System.Drawing.Size(181, 22);
            this.addSharedLevel.Text = "Add Level";
            this.addSharedLevel.Click += new System.EventHandler(this.addSharedLevel_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeHierarchyItem_Click);
            // 
            // insertLevelAboveToolStripMenuItem
            // 
            this.insertLevelAboveToolStripMenuItem.Name = "insertLevelAboveToolStripMenuItem";
            this.insertLevelAboveToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.insertLevelAboveToolStripMenuItem.Text = "&Insert Level Above";
            this.insertLevelAboveToolStripMenuItem.Click += new System.EventHandler(this.insertLevelAboveToolStripMenuItem_Click);
            // 
            // Hierarchies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "Hierarchies";
            this.Text = "Hierarchies";
            this.tabControl.ResumeLayout(false);
            this.hierarchyTabPage.ResumeLayout(false);
            this.hierarchyTabPage.PerformLayout();
            this.hierarchyPanel.ResumeLayout(false);
            this.hierarchyPanel.PerformLayout();
            this.levelPanel.ResumeLayout(false);
            this.levelPanel.PerformLayout();
            this.hiearchyMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage hierarchyTabPage;
        private System.Windows.Forms.Panel levelPanel;
        private System.Windows.Forms.ListView levelColumns;
        private System.Windows.Forms.ColumnHeader lcolnameHeader;
        private System.Windows.Forms.TextBox cardinalityBox;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Panel hierarchyPanel;
        private System.Windows.Forms.ComboBox hierarchyNoMeasureFilter;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.ComboBox hierarchyExclusionFilter;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox hierarchyDimension;
        private System.Windows.Forms.ContextMenuStrip hiearchyMenu;
        private System.Windows.Forms.ToolStripMenuItem newHierarchyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader hiddenHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox keyListBox;
        private System.Windows.Forms.Button removeKeyButton;
        private System.Windows.Forms.Button editKeyButton;
        private System.Windows.Forms.Button newKeyButton;
        private System.Windows.Forms.ToolStripMenuItem sharedLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox sharedLevel;
        private System.Windows.Forms.ToolStripMenuItem addSharedLevel;
        private System.Windows.Forms.CheckBox generateDimTableCheckBox;
        private System.Windows.Forms.Button toggleHidden;
        private System.Windows.Forms.ToolStripMenuItem insertLevelAboveToolStripMenuItem;
        private System.Windows.Forms.CheckBox degenerateTable;
        private System.Windows.Forms.CheckBox typeIISCD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox dimKeyLevelBox;
        private System.Windows.Forms.CheckBox hiddenBox;
        private System.Windows.Forms.CheckBox webLocked;
        private System.Windows.Forms.TextBox sourceGroupBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox generateCurrentCheckBox;
    }
}