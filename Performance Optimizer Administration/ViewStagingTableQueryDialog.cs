﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class ViewStagingTableQueryDialog : Form
    {
        public ViewStagingTableQueryDialog(string query)
        {
            InitializeComponent();
            txtQuery.Text = query;
            txtQuery.SelectionStart = 0;
            txtQuery.SelectionLength = 0;
        }

        public string getQuery()
        {
            return txtQuery.Text;
        }

        //handling ctrl+A event
        private void txtQuery_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\x1')
            {
                ((TextBox)sender).SelectAll();
                e.Handled = true;
            }

        }
    }
}
