using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class DefineTableSource : Form, ResizeMemoryForm
    {
        public TableSource ts;
        private AppConfig appconfig;
        private string catalog;
        private string username;
        private string password;
        private string windowName = "DefineTableSource";
        private MainAdminForm ma;

        public DefineTableSource(MainAdminForm ma, AppConfig appconfig, string catalog, string username, string password, TableSource ts)
        {
            InitializeComponent();
            this.ma = ma;
            resetWindowFromSettings();
            this.ts = ts;
            if (this.ts == null)
            {
                this.ts = new TableSource();
            }
            for (int i = 0; i < ts.Tables.Length; i++)
            {
                string jointypestr = null;
                if (this.ts.Tables[i].JoinClause == null) this.ts.Tables[i].JoinClause = "";
                if (i == 0) jointypestr = "";
                else if (this.ts.Tables[i].JoinType == 0) jointypestr = "Inner";
                else if (this.ts.Tables[i].JoinType == 1) jointypestr = "Left Outer";
                else if (this.ts.Tables[i].JoinType == 2) jointypestr = "Right Outer";
                else if (this.ts.Tables[i].JoinType == 3) jointypestr = "Full Outer";
                ListViewItem lvi = new ListViewItem(new string[] { this.ts.Tables[i].PhysicalName, jointypestr, this.ts.Tables[i].JoinClause });
                tableList.Items.Add(lvi);
            }
            this.ts = ts;
            this.appconfig = appconfig;
            this.catalog = catalog;
            this.username = username;
            this.password = password;
        }

        private ListViewItem getLVI()
        {
            string jointypestr = null;
            if (tableList.Items.Count == 0) jointypestr = "";
            else if (innerJoin.Checked) jointypestr = "Inner";
            else if (leftOuterJoin.Checked) jointypestr = "Left Outer";
            else if (rightOuterJoin.Checked) jointypestr = "Right Outer";
            else if (fullOuterJoin.Checked) jointypestr = "Full Outer";
            ListViewItem lvi = new ListViewItem(new string[] { tableNameBox.Text, jointypestr, joinClause.Text });
            return (lvi);
        }

        private void addTable_Click(object sender, EventArgs e)
        {
            tableList.Items.Add(getLVI());
        }

        private void removeTable_Click(object sender, EventArgs e)
        {
            if (tableList.SelectedIndices.Count == 1)
            {
                tableList.Items.RemoveAt(tableList.SelectedIndices[0]);
                if (tableList.Items.Count > 0)
                    tableList.Items[0].SubItems[2].Text = "";
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            ts = new TableSource();
            ts.Tables = new TableSource.TableDefinition[tableList.Items.Count];
            for (int i = 0; i < tableList.Items.Count; i++)
            {
                ts.Tables[i] = new TableSource.TableDefinition();
                ListViewItem lvi = tableList.Items[i];
                ts.Tables[i].PhysicalName = lvi.SubItems[0].Text;
                ts.Tables[i].JoinClause = lvi.SubItems[2].Text;
                String jointypestr = lvi.SubItems[1].Text;
                if (jointypestr == "") ts.Tables[i].JoinType = 0;
                else if (jointypestr == "Inner") ts.Tables[i].JoinType = 0;
                else if (jointypestr == "Left Outer") ts.Tables[i].JoinType = 1;
                else if (jointypestr == "Right Outer") ts.Tables[i].JoinType = 2;
                else if (jointypestr == "Full Outer") ts.Tables[i].JoinType = 3;
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {

        }

        private void tableList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tableList.SelectedItems.Count > 0)
            {
                ListViewItem lvi = tableList.SelectedItems[0];
                tableNameBox.Text = lvi.SubItems[0].Text;
                String jointypestr = lvi.SubItems[1].Text;
                if (jointypestr == "") innerJoin.Checked=true;
                else if (jointypestr == "Inner") innerJoin.Checked=true;
                else if (jointypestr == "Left Outer") leftOuterJoin.Checked=true;
                else if (jointypestr == "Right Outer") rightOuterJoin.Checked=true;
                else if (jointypestr == "Full Outer") fullOuterJoin.Checked=true;
                joinClause.Text = lvi.SubItems[2].Text;
            }
        }

        private void updateTable_Click(object sender, EventArgs e)
        {
            if (tableList.SelectedItems.Count != 1) return;
            tableList.Items[tableList.SelectedIndices[0]] = getLVI();
        }

        private void tableNameBox_DropDown(object sender, EventArgs e)
        {
            List<string> tables = Server.getTables(null,appconfig.ServerConnectionURL, catalog, username, password);
            tableNameBox.Items.Clear();
            foreach (string s in tables)
                tableNameBox.Items.Add(s);
        }

        private void DefineTableSource_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members
    }
}