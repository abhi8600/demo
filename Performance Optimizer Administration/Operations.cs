using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class Operations : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Operations";
        string categoryName = "Business Modeling";
        List<Operation> opList;
        List<Panel> actionPanels = new List<Panel>();
        List<Type> panelClasses = new List<Type>();

        public Operations(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            actionPanels.Add(updatePanel);
            panelClasses.Add(typeof(UpdateStatement));
            actionPanels.Add(invalidateCachePanel);
            panelClasses.Add(typeof(InvalidateCache));
            actionPanels.Add(emailActionPanel);
            panelClasses.Add(typeof(Email));
            opList = new List<Operation>(0);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.Operations = (Operation[])opList.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Operations != null)
            {
                opList = new List<Operation>(r.Operations);
            }
            else
            {
                opList = new List<Operation>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {
            invalidateTable.Items.Clear();
            foreach (MeasureTable mt in ma.measureTablesList)
                if (mt.TableName != null) invalidateTable.Items.Add(mt.TableName);
            foreach (DimensionTable dt in ma.dimensionTablesList)
                if (dt.TableName != null) invalidateTable.Items.Add(dt.TableName);
        }

        TreeNode rootNode;
        TreeNode selNode;
        int selIndex;
        int selAction;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (Operation op in opList)
            {
                TreeNode tn = new TreeNode(op.Name);
                tn.ImageIndex = 21;
                tn.SelectedImageIndex = 21;
                rootNode.Nodes.Add(tn);
                if (op.Actions != null)
                    foreach (Action act in op.Actions)
                    {
                        TreeNode atn = new TreeNode(act.Name);
                        tn.Nodes.Add(atn);
                        atn.ImageIndex = act.ImageIndex;
                        atn.SelectedImageIndex = act.ImageIndex;
                    }
            }
        }

        public bool selectedNode(TreeNode node)
        {
            implicitSave();
            if (opList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = opMenuStrip;
                addActionMenuItem.Visible = false;
                upActionMenuItem.Visible = false;
                downActionMenuItem.Visible = false;
                removeActionMenuItem.Visible = false;
                addOpMenuItem.Visible = true;
                removeOpMenuItem.Visible = false;
                return false;
            }
            if (node.Parent == rootNode)
            {
                selIndex = (rootNode.Nodes.IndexOf(node));
                if (selIndex >= 0)
                {
                    selNode = node;
                    ma.mainTree.ContextMenuStrip = opMenuStrip;
                }
                indexChanged();
                addActionMenuItem.Visible = true;
                removeActionMenuItem.Visible = false;
                upActionMenuItem.Visible = false;
                downActionMenuItem.Visible = false;
                addOpMenuItem.Visible = true;
                removeOpMenuItem.Visible = true;
                operationPanel.Visible = true;
                foreach (Panel p in actionPanels)
                    p.Visible = false;
                return true;
            }
            else
            {
                addActionMenuItem.Visible = true;
                upActionMenuItem.Visible = true;
                downActionMenuItem.Visible = true;
                removeActionMenuItem.Visible = true;
                addOpMenuItem.Visible = false;
                removeOpMenuItem.Visible = false;
                selIndex = (rootNode.Nodes.IndexOf(node.Parent));
                if (selIndex >= 0)
                {
                    selNode = node;
                    selAction = node.Parent.Nodes.IndexOf(node);
                    ma.mainTree.ContextMenuStrip = opMenuStrip;
                    actionIndexChanged();
                    operationPanel.Visible = false;
                    for (int i = 0; i < actionPanels.Count; i++)
                    {
                        if (opList[selIndex].Actions[selAction].GetType() == panelClasses[i])
                            actionPanels[i].Visible = true;
                        else
                            actionPanels[i].Visible = false;
                    }
                }
                return true;
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (turnOffimplicitSave) return;
            updateOperationOrUpdateOrInvalidate();
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return opTabPage;
            }
            set
            {
            }
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        public void indexChanged()
        {
            if (selIndex < 0) return;
            Operation op = opList[selIndex];
            //opNameBox.Text = op.Name;
            parameterGrid.Rows.Clear();
            if (op.Parameters != null)
                foreach (Parameter p in op.Parameters)
                {
                    int index = parameterGrid.Rows.Add();
                    DataGridViewRow dr = parameterGrid.Rows[index];
                    dr.Cells[0].Value = p.Name;
                    if (p.Type == Parameter.ParameterType.SingleValue)
                        dr.Cells[1].Value = "Single-value";
                    else if (p.Type == Parameter.ParameterType.MultiValue)
                        dr.Cells[1].Value = "Multi-value";
                    if (p.Datatype == Parameter.DataType.Varchar)
                        dr.Cells[2].Value = "Varchar";
                    else if (p.Datatype == Parameter.DataType.Number)
                        dr.Cells[2].Value = "Number";
                    else if (p.Datatype == Parameter.DataType.Integer)
                        dr.Cells[2].Value = "Integer";
                    else if (p.Datatype == Parameter.DataType.DateTime)
                        dr.Cells[2].Value = "DateTime";
                }
        }

        public void actionIndexChanged()
        {
            if (selIndex < 0 || selAction < 0) return;
            Operation op = opList[selIndex];
            Action ac = op.Actions[selAction];
            if (ac.GetType() == typeof(UpdateStatement))
            {
                UpdateStatement upd = (UpdateStatement)ac;
                //updateNameBox.Text = upd.Name;
                updateStatement.Text = upd.Statement;
                sqlStoredProcedure.Checked = upd.StoredProcedure;
                sqlUpdate.Checked = !upd.StoredProcedure;
            }
            else if (ac.GetType() == typeof(InvalidateCache))
            {
                InvalidateCache inv = (InvalidateCache)ac;
                //invalidateNameBox.Text = inv.Name;
                invalidateQuery.Text = inv.Query;
                invalidateTable.Text = inv.TableName;
                logicalQuery.Checked = inv.Type == InvalidateCache.InvalidateType.LogicalQuery;
                tableDefinition.Checked = inv.Type == InvalidateCache.InvalidateType.LogicalTable;
            }
            else if (ac.GetType() == typeof(Email))
            {
                Email inv = (Email)ac;
                //emailActionNameText.Text = inv.Name;
                emailActionFromText.Text = inv.From;
                emailActionToText.Text = inv.To;
                emailActionSubjectText.Text = inv.Subject;
                emailActionBodyText.Text = inv.Body;
                emailActionReportText.Text = inv.Report;
            }
        }

        private void addOpButton_Click(object sender, System.EventArgs e)
        {
            Operation op = new Operation();
            op.Name = "New Operation";
            opList.Add(op);
            TreeNode tn = new TreeNode(op.Name);
            tn.ImageIndex = 21;
            tn.SelectedImageIndex = 21;
            rootNode.Nodes.Add(tn);
        }

        private void removeOpButton_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0) return;
            opList.RemoveAt(selIndex);
            selNode.Remove();
        }

        private void upActionMenuItem_Click(object sender, EventArgs e)
        {
            if (selAction > 0)
            {
                turnOffimplicitSave = true;
                Operation op = opList[selIndex];
                List<Action> actList;
                Action act = op.Actions[selAction];
                if (op.Actions != null)
                    actList = new List<Action>(op.Actions);
                else
                    actList = new List<Action>();
                actList.RemoveAt(selAction);
                actList.Insert(selAction - 1, act);
                op.Actions = actList.ToArray();
                TreeNode ptn = selNode.Parent;
                TreeNode tn = ptn.Nodes[selAction];
                ptn.Nodes.RemoveAt(selAction);
                ptn.Nodes.Insert(selAction - 1, tn);
                ma.mainTree.SelectedNode = tn;
                turnOffimplicitSave = false;
            }
        }

        private void downActionMenuItem_Click(object sender, EventArgs e)
        {
            Operation op = opList[selIndex];
            if (selAction >= 0 && selAction < op.Actions.Length - 1)
            {
                turnOffimplicitSave = true;
                List<Action> actList;
                Action act = op.Actions[selAction];
                if (op.Actions != null)
                    actList = new List<Action>(op.Actions);
                else
                    actList = new List<Action>();
                actList.RemoveAt(selAction);
                actList.Insert(selAction + 1, act);
                op.Actions = actList.ToArray();
                TreeNode ptn = selNode.Parent;
                TreeNode tn = ptn.Nodes[selAction];
                ptn.Nodes.RemoveAt(selAction);
                ptn.Nodes.Insert(selAction + 1, tn);
                ma.mainTree.SelectedNode = tn;
                turnOffimplicitSave = false;
            }
        }

        private void addAction(Action act)
        {
            Operation op = opList[selIndex];
            TreeNode tn = new TreeNode(act.Name);
            tn.ImageIndex = act.ImageIndex;
            tn.SelectedImageIndex = act.ImageIndex;
            List<Action> actList;
            if (op.Actions != null)
                actList = new List<Action>(op.Actions);
            else
                actList = new List<Action>();
            actList.Add(act);
            op.Actions = actList.ToArray();

            if (selNode.Parent == rootNode)
                selNode.Nodes.Add(tn);
            else
                selNode.Parent.Nodes.Add(tn);
        }

        private void removeActionMenuItem_Click(object sender, EventArgs e)
        {
            if (selIndex < 0 || selAction < 0) return;
            Operation op = opList[selIndex];
            List<Action> actList = new List<Action>(op.Actions);
            actList.RemoveAt(selAction);
            op.Actions = actList.ToArray();
            selNode.Remove();
        }

        private void addUpdateStatementMenuItem_Click(object sender, EventArgs e)
        {
            UpdateStatement us = new UpdateStatement();
            us.Name = "New " + us.ActionTypeName;
            addAction(us);
        }

        private void invalidateCacheMenuItem_Click(object sender, EventArgs e)
        {
            InvalidateCache ic = new InvalidateCache();
            ic.Name = "New " + ic.ActionTypeName;
            addAction(ic);
        }

        private void invalidateDashboardCache_Click(object sender, EventArgs e)
        {
            InvalidateDashboardCache idc = new InvalidateDashboardCache();
            idc.Name = idc.ActionTypeName;
            addAction(idc);

        }

        private void invalidateCache_RadioButtonsCheckedChanged(object sender, EventArgs e)
        {
            if (this.logicalQuery.Checked)
            {
                invalidateQuery.Enabled = true;
                invalidateTable.Enabled = false;
            }
            else
            {
                invalidateQuery.Enabled = false;
                invalidateTable.Enabled = true;
            }
        }
        private void opTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        private void updateOperation()
        {
            if (selNode == null || selNode == rootNode || selNode.Parent != rootNode) return;
            if (selIndex < 0 || selIndex >= opList.Count) return;

            Operation op = opList[selIndex];
            //op.Name = opNameBox.Text;
            op.Parameters = new Parameter[parameterGrid.Rows.Count - 1];
            int count = 0;
            parameterGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
            foreach (DataGridViewRow row in parameterGrid.Rows)
            {
                if (!row.IsNewRow)
                {
                    Parameter p = new Parameter();
                    p.Name = (string)row.Cells[0].Value;
                    if ((string)row.Cells[1].Value == "Single-value")
                        p.Type = Parameter.ParameterType.SingleValue;
                    else if ((string)row.Cells[1].Value == "Multi-value")
                        p.Type = Parameter.ParameterType.MultiValue;
                    if ((string)row.Cells[2].Value == "Varchar")
                        p.Datatype = Parameter.DataType.Varchar;
                    else if ((string)row.Cells[2].Value == "Number")
                        p.Datatype = Parameter.DataType.Number;
                    else if ((string)row.Cells[2].Value == "Integer")
                        p.Datatype = Parameter.DataType.Integer;
                    else if ((string)row.Cells[2].Value == "DateTime")
                        p.Datatype = Parameter.DataType.DateTime;
                    op.Parameters[count++] = p;
                }
            }
            selNode.Text = op.Name;
        }
        private void updateUpdate()
        {
            if (selNode == null || selNode == rootNode || selNode.Parent == rootNode) return;
            if (selIndex < 0 || selIndex >= opList.Count) return;
            Operation op = opList[selIndex];
            if (selAction < 0 || selAction >= op.Actions.Length) return;
            UpdateStatement upd = (UpdateStatement)op.Actions[selAction];
            //upd.Name = updateNameBox.Text;
            upd.Statement = updateStatement.Text;
            upd.StoredProcedure = sqlStoredProcedure.Checked;
            selNode.Text = upd.Name;
        }
        private void updateInvalidate()
        {
            if (selNode == null || selNode == rootNode || selIndex < 0 || selIndex >= opList.Count) return;
            Operation op = opList[selIndex];
            if (selAction < 0 || selAction >= op.Actions.Length) return;
            InvalidateCache inv = (InvalidateCache)op.Actions[selAction];
            //inv.Name = invalidateNameBox.Text;
            inv.TableName = invalidateTable.Text;
            inv.Query = invalidateQuery.Text;
            inv.Type = logicalQuery.Checked ? InvalidateCache.InvalidateType.LogicalQuery : InvalidateCache.InvalidateType.LogicalTable;
            selNode.Text = inv.Name;
        }
        private void updateOperationOrUpdateOrInvalidate()
        {
            if (selNode == null || selNode == rootNode) return;
            if (selNode.Parent == rootNode)
                updateOperation();
            else
            {
                if (selIndex < 0 || selIndex >= opList.Count) return;
                Operation op = opList[selIndex];
                if (op.Actions == null || op.Actions.Length == 0 || selAction < 0 || selAction >= op.Actions.Length) return;
                Type t = op.Actions[selAction].GetType();
                if (t.Equals(typeof(InvalidateCache)))
                {
                    InvalidateCache inv = (InvalidateCache)op.Actions[selAction];
                    //inv.Name = invalidateNameBox.Text;
                    inv.TableName = invalidateTable.Text;
                    inv.Query = invalidateQuery.Text;
                    inv.Type = logicalQuery.Checked ? InvalidateCache.InvalidateType.LogicalQuery : InvalidateCache.InvalidateType.LogicalTable;
                    selNode.Text = inv.Name;
                }
                else if (t.Equals(typeof(InvalidateDashboardCache)))
                {
                }
                else if (t.Equals(typeof(Email)))
                {
                    Email email = (Email)op.Actions[selAction];
                    //email.Name = emailActionNameText.Text;
                    email.From = emailActionFromText.Text;
                    email.To = emailActionToText.Text;
                    email.Subject = emailActionSubjectText.Text;
                    email.Body = emailActionBodyText.Text;
                    email.Report = emailActionReportText.Text;
                    selNode.Text = email.Name;
                }
                else if (t.Equals(typeof(UpdateStatement)))
                {
                    UpdateStatement upd = (UpdateStatement)op.Actions[selAction];
                    //upd.Name = updateNameBox.Text;
                    upd.Statement = updateStatement.Text;
                    upd.StoredProcedure = sqlStoredProcedure.Checked;
                    selNode.Text = upd.Name;
                }
            }
        }

        private void emailMenuItem_Click(object sender, EventArgs e)
        {
            Email ic = new Email();
            ic.Name = "New " + ic.ActionTypeName;
            addAction(ic);
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                opList[selIndex].Name = e.Label;
            }
            else if (e.Node.Parent.Parent == rootNode)
            {
                selNode.Text = e.Label;
                opList[selIndex].Actions[selAction].Name = e.Label;
            }
        }

        #endregion

        private void parameterGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            parameterGrid.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

    }
}