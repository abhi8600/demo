namespace Performance_Optimizer_Administration
{
    partial class StagingFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StagingFilters));
            this.stagingFiltersPage = new System.Windows.Forms.TabPage();
            this.stagingFilterPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.conditionTextArea = new System.Windows.Forms.TextBox();
            this.conditionLbl = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.stagingFilterMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newStagingFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeStagingFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stagingFiltersPage.SuspendLayout();
            this.stagingFilterPanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.stagingFilterMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // stagingFiltersPage
            // 
            this.stagingFiltersPage.AutoScroll = true;
            this.stagingFiltersPage.Controls.Add(this.stagingFilterPanel);
            this.stagingFiltersPage.Location = new System.Drawing.Point(4, 22);
            this.stagingFiltersPage.Name = "stagingFiltersPage";
            this.stagingFiltersPage.Padding = new System.Windows.Forms.Padding(3);
            this.stagingFiltersPage.Size = new System.Drawing.Size(844, 707);
            this.stagingFiltersPage.TabIndex = 0;
            this.stagingFiltersPage.Text = "Staging Filters";
            this.stagingFiltersPage.UseVisualStyleBackColor = true;
            // 
            // stagingFilterPanel
            // 
            this.stagingFilterPanel.Controls.Add(this.label1);
            this.stagingFilterPanel.Controls.Add(this.conditionTextArea);
            this.stagingFilterPanel.Controls.Add(this.conditionLbl);
            this.stagingFilterPanel.Location = new System.Drawing.Point(3, 6);
            this.stagingFilterPanel.Name = "stagingFilterPanel";
            this.stagingFilterPanel.Size = new System.Drawing.Size(838, 309);
            this.stagingFilterPanel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(827, 31);
            this.label1.TabIndex = 112;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // conditionTextArea
            // 
            this.conditionTextArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.conditionTextArea.Location = new System.Drawing.Point(9, 62);
            this.conditionTextArea.Multiline = true;
            this.conditionTextArea.Name = "conditionTextArea";
            this.conditionTextArea.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.conditionTextArea.Size = new System.Drawing.Size(824, 244);
            this.conditionTextArea.TabIndex = 111;
            // 
            // conditionLbl
            // 
            this.conditionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conditionLbl.Location = new System.Drawing.Point(3, 5);
            this.conditionLbl.Name = "conditionLbl";
            this.conditionLbl.Size = new System.Drawing.Size(80, 23);
            this.conditionLbl.TabIndex = 110;
            this.conditionLbl.Text = "Condition";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.stagingFiltersPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(852, 733);
            this.tabControl1.TabIndex = 0;
            // 
            // stagingFilterMenuStrip
            // 
            this.stagingFilterMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStagingFilterToolStripMenuItem,
            this.removeStagingFilterToolStripMenuItem});
            this.stagingFilterMenuStrip.Name = "sourceFileMenuStrip";
            this.stagingFilterMenuStrip.Size = new System.Drawing.Size(191, 48);
            // 
            // newStagingFilterToolStripMenuItem
            // 
            this.newStagingFilterToolStripMenuItem.Name = "newStagingFilterToolStripMenuItem";
            this.newStagingFilterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.newStagingFilterToolStripMenuItem.Text = "&New Staging Filter";
            this.newStagingFilterToolStripMenuItem.Click += new System.EventHandler(this.newStagingFilterToolStripMenuItem_Click);
            // 
            // removeStagingFilterToolStripMenuItem
            // 
            this.removeStagingFilterToolStripMenuItem.Name = "removeStagingFilterToolStripMenuItem";
            this.removeStagingFilterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.removeStagingFilterToolStripMenuItem.Text = "&Remove Staging Filter";
            this.removeStagingFilterToolStripMenuItem.Click += new System.EventHandler(this.removeStagingFilterToolStripMenuItem_Click);
            // 
            // StagingFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "StagingFilters";
            this.Text = "Staging Filters";
            this.stagingFiltersPage.ResumeLayout(false);
            this.stagingFilterPanel.ResumeLayout(false);
            this.stagingFilterPanel.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.stagingFilterMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage stagingFiltersPage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ContextMenuStrip stagingFilterMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newStagingFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeStagingFilterToolStripMenuItem;
        private System.Windows.Forms.Panel stagingFilterPanel;
        private System.Windows.Forms.Label conditionLbl;
        private System.Windows.Forms.TextBox conditionTextArea;
        private System.Windows.Forms.Label label1;
    }
}