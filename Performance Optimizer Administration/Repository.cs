using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text;

namespace Performance_Optimizer_Administration
{
    /// <summary>
    /// Summary description for Repository.
    /// </summary>
    [XmlRootAttribute("Repository", Namespace = "http://www.successmetricsinc.com",
         IsNullable = false)]

    [Serializable]
    public class Repository : BaseObject
    {
        public static int RepositoryWriteVersion = 9;
        public static int RepositoryReadVersion = 3;

        public static string FILENAME = "\\Repository";

        public static string CACHE_NO_BOUNDARY = "NONE";
        public static string CACHE_DAY_BOUNDARY = "DAY";
        public static string CACHE_WEEK_BOUNDARY = "WEEK";
        public static string CACHE_MONTH_BOUNDARY = "MONTH";
        public static string CACHE_QUARTER_BOUNDARY = "QUARTER";
        public static string CACHE_YEAR_BOUNDARY = "YEAR";

        // 9  introduces the change to the SUBSTRING/POSITION semantics
        // 10 moves unknown value an checksums into bulkload
        // 11 includes columnheaders to calculate checksum of row - bug #10550
        // 12 physical table and column names to start with 'X' (instead of 'x') if they are too long and hashed (for oracle)
        // 12 Compound Key Name should have maximum allowed width = MaxPhysicalColumnNameLength - 4
        // 13 Always make surrogate key data type as integer
        // 13 Do not hash surrogate key name for non infobright spaces
        // 14 Handle delete Last for SCD Type 2 
        // 15 Changed surrogate key datatype from integer to BigInt
        // 15 Prefix dimension table physical name with DW_DM_ if it is hashed
        // 15 New Snowflake Changes
        // -------- 5.3.X ---------
        // 16 Allows New ETL scheme for IB - select into outfile and load data infile into table (like PADB) - Requires processing engine 5.3 or higher
        // 17 Allows for a snowflake to combine two tables inherited from the same source and to independently use columns with the same name from each
        // 18 Introduces a flag restricting the creation of fact tables where the grain is not a unique key
        // 19 To support  int(4) to int(8)
        // 20 Change handling of SPARSE to middle tier
        // 21 Use CRC32 for checksum rather than Object.hashCode() - this is not guaranteed to be stable across Java sessions
        // 21 BPD-9927 - made Quarter, Month and Day floating levels in Time hierarchy
        // 22 BPD-18353 - added logicalName for StagingTable, Hierarchy and DimensionNames
        // 23 5.6.1 BPD-19129 - Push Filtered Measures to SQL database
        // 24 5.6.2 BPD-20069 - Add $$ to identify a hashed foreign key
        //    5.6.2 BPD-19401 - Allow Hashing of Long Source File Names
        // 25 5.6.3 BPD-19355 - Need to be able to process just changed data sources
        // 26 5.7 BPD-19355 - Need to be able to process just changed data sources (Enabling this feature as beta)
        //    5.7 BPD-20688  Added hashing to control aggregate name length 
        // 27 5.7.1 BPD-21024 Unsupported character replacement in salesforce/connector file upload
		//			BPD-20756 - Copy data from older fact table if grain is lowered
        // 28 5.7.3 BPD-21647 - [IB] - Processing fail with duplicate column found in create staging table
		//			BPD-21056 - Handle Staging Column Names exceeding database supported length
        // 29 5.8   BPD-21946 - Changing staging table name after hashing for corner cases
        // 30 5.10.1 BPD-23361 - Handle Row Size too large for Infobright
        // 31 5.12 BPD-25483 - New fact table created instead of renaming the existing table on changing grains. This is leading to loss of historical data reporting for old grains as well
        // 32 5.10.2 BPD-23209 - On IB platform, Birst chooses to use aggregates when required fiels are not part of aggregate
        //    5.12.1 BPD-25593 - DnB - SFDC Prod - Issue in Main Aggregate 1.2      
        public static int CurrentBuilderVersion = 32;
        public static int IntToLongBuilderVersion = 20; // XXX isn't this 19?
        public static int FloatedTimeLevels = 21;
        public static int I18NLogicalNames = 22;
        public int Version = 8;
        public int BuilderVersion = CurrentBuilderVersion;
        public string SourceCodeControlVersion;
        public int CurrentReferenceID = 1;
        public string LastEdit;
        public string ApplicationName;
        public string TargetDimension;
        public string OptimizationGoal;
        public string DefaultConnection;
        public bool GenerateSubjectArea;
        public bool IsDynamic;
        public int QueryLanguageVersion = 1; // 0 is classic, 1 is new (0 is default through 4.2, 4.3 changes to 1)
        public int MaxPhysicalTableNameLength = 120;
        public int MaxPhysicalColumnNameLength = -1;
        public bool UseNewETLSchemeForIB = true;
        public int MinYear = -1;
        public int MaxYear = -1;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowNonUniqueGrains = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool LogCacheKey = false;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int DimensionalStructure;

        public ReferencePopulation[] ReferencePopulations;
        public PeerItem[] PerformancePeerColumns;
        public PeerItem[] SuccessPatternPeerColumns;
        public int NumPerformancePeers;
        public int NumSuccessPatternPeers;
        public string SuccessMeasure;
        public string[] PeeringFilters;

        public PerformanceModel Performance;

        public PerformanceModel[] PerformanceModels;
        public SuccessModel[] SuccessModels;
        public PatternSearchDefinition[] SearchDefinitions;
        public QueryFilter[] Filters;
        public Outcome[] Outcomes;
        public MeasureTable[] MeasureTables;
        public DimensionTable[] DimensionTables;
        public Dimension[] Dimensions;
        public Join[] Joins;
        public VirtualColumn[] VirtualColumns;
        public Hierarchy[] Hierarchies;
        public Aggregate[] Aggregates;
        public SubjectArea[] SubjectAreas;
        public ServerParameters ServerParameters;
        public DatabaseConnection[] Connections;
        public ModelParameters ModelParameters;
        public OptimizerParameters OptimizerParameters;
        public Group[] Groups;
        public User[] Users;
        public Variable[] Variables;
        public Resource[] Resources;
        public Broadcast[] Broadcasts;
        public ACLItem[] ACL;
        public Operation[] Operations;
        public SourceFile[] SourceFiles;
        public StagingTable[] StagingTables;
        public StagingFilter[] StagingFilters;
        public MeasureGrain[] UnmappedMeasureGrains;
        public TimeDefinition TimeDefinition;
        public LoadGroup[] LoadGroups;
        public ScriptGroup[] ScriptGroups;
        public HierarchyLevel[][] Dependencies;
        public QuickDashboard[] QuickDashboards;
        public CustomDrill[] CustomDrills;
        public LogicalExpression[] Expressions;
        public PackageImport[] Imports;
        public bool RequiresPublish;
        public bool RequiresRebuild;
        public BuildProperties BuildProperties;
        public string[][] DynamicGroups;
        public bool Hybrid;     // to let us know that the repository has potentially been modified outside of Birst
        public bool NoBirstApplicationRebuild;  // don't automatically rebuild this in Birst (mainly for Enterprise control freaks :))
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DisablePublishLock;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool EnableLiveAccessToDefaultConnection;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowExternalIntegrations;
        [System.ComponentModel.DefaultValueAttribute(-1)]
        public int ExternalIntegrationTimeout;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int NumThresholdFailedRecords;
        [XmlIgnore]
        public DateTime? fileModifiedTime;
        public bool SkipDBAConnectionMerge; // rarely used and that to only changed manually - triggered by Host Analytics use case
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DisableImpliedGrainsProcessing;
        public WarehousePartition[] WarehousePartitions;
        public PartitionConnectionLoadInfo[] PartitionConnectionLoadInfoList;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IsPartitioned = false;
        public bool LiveAccessSecurityFilter;
        // R Server Connection
        public RServer RServer;

        public Repository()
        {
        }

        public static bool EqualsArray(Object[] one, Object[] two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                Object o1 = one[i];
                Object o2 = two[i];
                if (!Repository.EqualsWithNull(o1, o2)) return false;
            }
            return true;
        }

        public static bool EqualsDoubleArray(Object[][] one, Object[][] two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                Object[] o1 = one[i];
                Object[] o2 = two[i];
                if (!Repository.EqualsArray(o1, o2)) return false;
            }
            return true;
        }

        public static bool EqualsDoubleValueArray(double[][] one, double[][] two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                double[] o1 = one[i];
                double[] o2 = two[i];
                if (o1 == null && o2 == null) return true;
                if ((o1 == null && o2 != null) || (o1 != null && o2 == null)) return false;
                if (o1.Length != o2.Length) return false;
                for (int j = 0; j < o1.Length; j++)
                {
                    if (o1[j] != o2[j]) return false;
                }
            }
            return true;
        }

        public static bool FullEqualsArray(Object[] one, Object[] two)
        {
            bool nochanges = true;
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                Object o1 = one[i];
                Object o2 = two[i];
                if (!Repository.EqualsWithNull(o1, o2)) nochanges = false;
            }
            return nochanges;
        }

        public static bool EqualsBoolArray(bool[] one, bool[] two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                bool o1 = one[i];
                bool o2 = two[i];
                if (o1 != o2) return false;
            }
            return true;
        }

        public static bool EqualsIntArray(int[] one, int[] two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (one.Length != two.Length) return false;
            for (int i = 0; i < one.Length; i++)
            {
                int o1 = one[i];
                int o2 = two[i];
                if (o1 != o2) return false;
            }
            return true;
        }

        public static bool EqualsWithNull(Object one, Object two)
        {
            if (one == null && two == null) return true;
            if ((one == null && two != null) || (one != null && two == null)) return false;
            if (!one.Equals(two)) return false;
            return true;
        }

        public static bool EqualsStringEmptyIsNull(string one, string two)
        {
            if (one == null && (two == null || two.Trim().Length == 0))
                return true;
            if (two == null && (one == null || one.Trim().Length == 0))
                return true;
            if (one.Equals(two))
                return true;
            return false;
        }

        public bool CompareTo(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Repository).Equals(obj.GetType())) return false;
            Repository other = (Repository)obj;

            if (Version != other.Version) return false;
            if (!Repository.EqualsWithNull(ApplicationName, other.ApplicationName)) return false;
            if (!Repository.EqualsWithNull(TargetDimension, other.TargetDimension)) return false;
            if (!Repository.EqualsWithNull(OptimizationGoal, other.OptimizationGoal)) return false;
            if (!Repository.EqualsArray(ReferencePopulations, other.ReferencePopulations)) return false;
            if (!Repository.EqualsArray(PerformanceModels, other.PerformanceModels)) return false;
            if (!Repository.EqualsArray(SuccessModels, other.SuccessModels)) return false;
            if (!Repository.EqualsArray(SearchDefinitions, other.SearchDefinitions)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (!Repository.EqualsArray(Outcomes, other.Outcomes)) return false;
            if (!Repository.EqualsArray(MeasureTables, other.MeasureTables))
            {
                bool result = false;
                bool otherNoDerived = true;
                foreach (MeasureTable mt in other.MeasureTables)
                {
                    if (mt.TableName == "Derived Measure Table")
                    {
                        otherNoDerived = false;
                        break;
                    }
                }
                if (otherNoDerived)
                    foreach (MeasureTable mt in MeasureTables)
                    {
                        if (mt.TableName == "Derived Measure Table")
                            result = mt.MeasureColumns.Length == 0;

                    }
                if (!result)
                    return false;
            }
            if (!Repository.EqualsArray(DimensionTables, other.DimensionTables)) return false;
            if (!Repository.EqualsArray(Joins, other.Joins)) return false;
            if (!Repository.EqualsArray(VirtualColumns, other.VirtualColumns)) return false;
            if (!Repository.EqualsArray(Hierarchies, other.Hierarchies)) return false;
            if (!Repository.EqualsArray(Aggregates, other.Aggregates)) return false;
            if (!Repository.EqualsArray(SubjectAreas, other.SubjectAreas)) return false;
            if (!Repository.EqualsWithNull(ServerParameters, other.ServerParameters)) return false;
            if (!Repository.EqualsArray(Connections, other.Connections)) return false;
            if (!Repository.EqualsWithNull(ModelParameters, other.ModelParameters)) return false;
            if (!Repository.EqualsWithNull(OptimizerParameters, other.OptimizerParameters)) return false;
            if (!Repository.EqualsWithNull(BuildProperties, other.BuildProperties)) return false;
            if (!Repository.EqualsArray(Groups, other.Groups)) return false;
            if (!Repository.EqualsArray(Users, other.Users)) return false;
            if (!Repository.EqualsArray(Variables, other.Variables)) return false;
            if (!Repository.EqualsArray(Resources, other.Resources)) return false;
            if (!Repository.EqualsArray(Broadcasts, other.Broadcasts)) return false;
            if (!Repository.EqualsArray(ACL, other.ACL)) return false;
            if (!Repository.EqualsArray(Operations, other.Operations)) return false;

            // ETL Metadata related modules -- Warehouse category
            if (!Repository.EqualsArray(SourceFiles, other.SourceFiles)) return false;
            if (!Repository.EqualsArray(StagingTables, other.StagingTables)) return false;
            if (!Repository.EqualsArray(LoadGroups, other.LoadGroups)) return false;
            if (!Repository.EqualsArray(UnmappedMeasureGrains, other.UnmappedMeasureGrains)) return false;
            if (!Repository.EqualsWithNull(TimeDefinition, other.TimeDefinition)) return false;
            if (!Repository.EqualsArray(ScriptGroups, other.ScriptGroups)) return false;

            return true;
        }

    }

    [Serializable]
    public class ServerParameters : BaseObject
    {
        public string ApplicationPath;
        public string CachePath;
        public int MaxCacheEntries;
        public int MaxRecords;
        public string CustomerMailHost;
        public bool MapNullToZero;
        public int MaxQueryTime;
        public string StagingEncryptionKey;
        public bool GenerateUniqueKeys;
        public int MaxSnowflakeSize = -1;
        public bool AllowDimensionalCrossjoins;
        public string ProcessingTimeZone;
        public string DisplayTimeZone;
        public long MaxScriptStatements;
        public int MaxInputRows;
        public int MaxOutputRows;
        public int ScriptQueryTimeout; // Seconds
        public string ProxyDBConnectionString;
        public string ProxyDBUserName;
        public string ProxyDBPassword;
        public string Collation;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int MaxThreadsInETL;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DisableCachingForGeneratedMeasureTables;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DisableCachingForGeneratedDimensionTables;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ForceDimensionCrossJoinsThroughFacts = true;
        [System.ComponentModel.DefaultValueAttribute(1000000)]
        public long BulkDeleteBatchSize;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DisableNewIncrementalDeleteOnIB;
        [System.ComponentModel.DefaultValueAttribute(1000000)]
        public long IBIncrementalDeleteThresholdValue;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ServerParameters).Equals(obj.GetType())) return false;
            ServerParameters other = (ServerParameters)obj;
            if (ApplicationPath != other.ApplicationPath) return false;
            if (CachePath != other.CachePath) return false;
            if (MaxCacheEntries != other.MaxCacheEntries) return false;
            if (MaxRecords != other.MaxRecords) return false;
            if (MaxQueryTime != other.MaxQueryTime) return false;
            if (MapNullToZero != other.MapNullToZero) return false;
            if (CustomerMailHost != other.CustomerMailHost) return false;
            if (StagingEncryptionKey != other.StagingEncryptionKey) return false;
            if (GenerateUniqueKeys != other.GenerateUniqueKeys) return false;
            if (MaxSnowflakeSize != other.MaxSnowflakeSize) return false;
            if (AllowDimensionalCrossjoins != other.AllowDimensionalCrossjoins) return false;
            if (ProcessingTimeZone != other.ProcessingTimeZone) return false;
            if (DisplayTimeZone != other.DisplayTimeZone) return false;
            if (MaxScriptStatements != other.MaxScriptStatements) return false;
            if (MaxInputRows != other.MaxInputRows) return false;
            if (MaxOutputRows != other.MaxOutputRows) return false;
            if (ScriptQueryTimeout != other.ScriptQueryTimeout) return false;
            if (ProxyDBConnectionString != other.ProxyDBConnectionString) return false;
            if (ProxyDBUserName != other.ProxyDBUserName) return false;
            if (ProxyDBPassword != other.ProxyDBPassword) return false;
            if (DisableCachingForGeneratedMeasureTables != other.DisableCachingForGeneratedMeasureTables) return false;
            if (DisableCachingForGeneratedDimensionTables != other.DisableCachingForGeneratedDimensionTables) return false;
            if (Collation != other.Collation) return false;
            if (MaxThreadsInETL != other.MaxThreadsInETL) return false;
            if (BulkDeleteBatchSize != other.BulkDeleteBatchSize) return false;
            if (DisableNewIncrementalDeleteOnIB != other.DisableNewIncrementalDeleteOnIB) return false;
            if (IBIncrementalDeleteThresholdValue != other.IBIncrementalDeleteThresholdValue) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PeerItem : BaseObject
    {
        public string Column;
        public int Type; // 0 - Attribute, 1 - Measure
        public double Weight;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(PeerItem).Equals(obj.GetType())) return false;
            PeerItem other = (PeerItem)obj;
            if (Column != other.Column) return false;
            if (Type != other.Type) return false;
            if (Weight != other.Weight) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class TableSource : BaseObject
    {

        [Serializable]
        public class TableDefinition : BaseObject
        {
            public string PhysicalName;
            [System.ComponentModel.DefaultValueAttribute(0)]
            public int JoinType;
            public string JoinClause;
            public string Schema;

            public TableDefinition clone()
            {
                TableDefinition td = new TableDefinition();
                td.PhysicalName = PhysicalName;
                td.JoinClause = JoinClause;
                td.JoinType = JoinType;
                td.Schema = Schema;
                return (td);
            }

            public override bool Equals(Object obj)
            {
                if (obj == null) return false;
                if (!typeof(TableDefinition).Equals(obj.GetType())) return false;
                TableDefinition other = (TableDefinition)obj;
                if (PhysicalName != other.PhysicalName) return false;
                if (JoinClause != other.JoinClause) return false;
                if (JoinType != other.JoinType) return false;
                if (Schema != other.Schema) return false;
                return true;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public string toSerializedString()
            {
                return (Schema == null ? "" : Schema) + "\\" + PhysicalName + "\\" + JoinType + "\\" + (JoinClause == null ? "" : JoinClause);
            }

            public static TableDefinition fromSerializedString(string s)
            {
                string[] parts = s.Split('\\');
                if (!(parts.Length == 3 || parts.Length == 4))
                    return null;
                if (parts.Length == 3)
                {
                    //add missing schema
                    parts = ("\\" + s).Split('\\');
                }
                TableDefinition td = new TableDefinition();
                td.Schema = parts[0].Length == 0 ? null : parts[0];
                td.PhysicalName = parts[1];
                int.TryParse(parts[2], out td.JoinType);
                td.JoinClause = parts[3].Length == 0 ? null : parts[3];
                return td;
            }
        }

        [Serializable]
        public class TableSourceFilter : BaseObject
        {
            public string Filter;
            [System.ComponentModel.DefaultValueAttribute(false)]
            public bool CurrentLoadFilter = false;
            [System.ComponentModel.DefaultValueAttribute(false)]
            public bool SecurityFilter = false;
            public string[] FilterGroups;
            public string LogicalColumnName;

            public TableSourceFilter clone()
            {
                TableSourceFilter tsf = new TableSourceFilter();
                tsf.Filter = Filter;
                tsf.CurrentLoadFilter = CurrentLoadFilter;
                tsf.SecurityFilter = SecurityFilter;
                if (FilterGroups != null)
                {
                    tsf.FilterGroups = new string[FilterGroups.Length];
                    for (int i = 0; i < FilterGroups.Length; i++)
                    {
                        tsf.FilterGroups[i] = FilterGroups[i];
                    }
                }
                tsf.LogicalColumnName = LogicalColumnName;
                return (tsf);
            }

            public override bool Equals(Object obj)
            {
                if (obj == null) return false;
                if (!typeof(TableSourceFilter).Equals(obj.GetType())) return false;
                TableSourceFilter other = (TableSourceFilter)obj;
                if (Filter != other.Filter) return false;
                if (CurrentLoadFilter != other.CurrentLoadFilter) return false;
                if (SecurityFilter != other.SecurityFilter) return false;
                if (!Repository.EqualsArray(FilterGroups, other.FilterGroups)) return false;
                return true;
            }

            public string toSerializedString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(CurrentLoadFilter ? "1" : "0");
                sb.Append(SecurityFilter ? "1" : "0");
                sb.Append(Filter);
                if (FilterGroups != null)
                    for (int i = 0; i < FilterGroups.Length; i++)
                    {
                        sb.Append(',' + FilterGroups[i]);
                    }
                return sb.ToString();
            }

            public static TableSourceFilter fromSerializedString(string s)
            {
                TableSourceFilter tsf = new TableSourceFilter();
                tsf.CurrentLoadFilter = (s[0] == '1');
                tsf.SecurityFilter = (s[1] == '1');
                string[] parts = s.Substring(2).Split(',');
                tsf.Filter = parts[0];
                if (parts.Length > 1)
                {
                    tsf.FilterGroups = new string[parts.Length - 1];
                    for (int i = 1; i < parts.Length; i++)
                    {
                        tsf.FilterGroups[i - 1] = parts[i];
                    }
                }
                return tsf;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public TableDefinition[] Tables;
        public string Connection;
        public string Schema;
        public string[] ContentFilters;
        public TableSourceFilter[] Filters;

        public static TableSource getBlank(string name)
        {
            TableSource ts = new TableSource();
            ts.Tables = new TableSource.TableDefinition[1];
            ts.Tables[0] = new TableSource.TableDefinition();
            ts.Tables[0].PhysicalName = name;
            return (ts);
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(TableSource).Equals(obj.GetType())) return false;
            TableSource other = (TableSource)obj;
            if (Schema != other.Schema) return false;
            if (!Repository.EqualsArray(Tables, other.Tables)) return false;
            if (Connection != other.Connection) return false;
            if (!Repository.EqualsArray(ContentFilters, other.ContentFilters)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            string result = "";
            foreach (TableDefinition td in Tables)
            {
                if (result.Length > 0) result += ",";
                result += td.PhysicalName;
            }
            return (result);
        }

        public string toSerializedString()
        {
            string result = (Schema == null ? "" : Schema);
            if (Connection != null)
                result += "/" + Connection;
            foreach (TableDefinition td in Tables)
                result += "|D" + td.toSerializedString();
            if (Filters != null)
                foreach (TableSourceFilter tsf in Filters)
                    result += "|F" + tsf.toSerializedString();
            return result;
        }

        public static TableSource fromSerializedString(string s)
        {
            string[] parts = s.Split('|');
            if (parts.Length == 0)
                return null;
            TableSource ts = new TableSource();
            if (parts[0].Length > 0)
            {
                ts.Schema = parts[0];
                int conindex = ts.Schema.IndexOf('/');
                if (conindex >= 0)
                {
                    string c = ts.Schema;
                    ts.Schema = c.Substring(0, conindex);
                    ts.Connection = c.Substring(conindex + 1);
                }
            }
            List<TableDefinition> tdlist = new List<TableDefinition>();
            List<TableSourceFilter> tsflist = new List<TableSourceFilter>();
            for (int i = 1; i < parts.Length; i++)
            {
                s = parts[i];
                if (s[0] == 'D')
                    tdlist.Add(TableDefinition.fromSerializedString(s.Substring(1)));
                else if (s[0] == 'F')
                    tsflist.Add(TableSourceFilter.fromSerializedString(s.Substring(1)));
            }
            ts.Tables = tdlist.ToArray();
            if (tsflist.Count > 0)
                ts.Filters = tsflist.ToArray();
            return ts;
        }

        public TableSource clone()
        {
            TableSource ts = new TableSource();
            ts.Tables = new TableDefinition[Tables.Length];
            for (int i = 0; i < Tables.Length; i++)
                ts.Tables[i] = Tables[i].clone();
            ts.Schema = Schema;
            ts.Connection = Connection;
            if (Filters != null)
            {
                ts.Filters = new TableSourceFilter[Filters.Length];
                for (int i = 0; i < Filters.Length; i++)
                    ts.Filters[i] = Filters[i];
            }
            if (ContentFilters != null)
            {
                ts.ContentFilters = new string[ContentFilters.Length];
                for (int i = 0; i < ContentFilters.Length; i++)
                    ts.ContentFilters[i] = ContentFilters[i];
            }
            return (ts);
        }
    }

    [Serializable]
    public class ColumnSubstitution : BaseObject
    {
        public string Original;
        public string New;

        public ColumnSubstitution clone()
        {
            ColumnSubstitution cs = new ColumnSubstitution();
            cs.New = New;
            cs.Original = Original;
            return (cs);
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ColumnSubstitution).Equals(obj.GetType())) return false;
            ColumnSubstitution other = (ColumnSubstitution)obj;
            if (Original != other.Original) return false;
            if (New != other.New) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class MeasureTable : BaseObject
    {
        public static int NORMAL = 0;
        public static int OPAQUE_VIEW = 1;
        public static int DERIVED = 2;
        public static int UNMAPPED = 3;
        public string TableName;
        public string PhysicalName;
        public TableSource TableSource;
        public ColumnSubstitution[] ColumnSubstitutions;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int Type;
        public double Cardinality;
        public bool RecalculateCardinality;
        public string OpaqueView;
        public MeasureColumn[] MeasureColumns;
        public string InheritTable;
        public string InheritPrefix;
        public string Folder;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DontInheritJoins = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DontInheritColumns = false;
        public bool Cacheable = true;
        [System.ComponentModel.DefaultValueAttribute(-1)]
        public int TTL = -1;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutoGenerated = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Calculated = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Transactional = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool CurrentSnapshotOnly = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool TimeAlias = false;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int ModelingStep = 0;
        public MeasureGrain Grain;
        public string[] LoadGroups;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(MeasureTable).Equals(obj.GetType())) return false;
            MeasureTable other = (MeasureTable)obj;
            if (TableName != other.TableName) return false;
            if (PhysicalName != other.PhysicalName) return false;
            if (!Repository.EqualsArray(ColumnSubstitutions, other.ColumnSubstitutions)) return false;
            if (Type != other.Type) return false;
            if (Cardinality != other.Cardinality) return false;
            if (OpaqueView != other.OpaqueView) return false;
            if (!Repository.EqualsArray(MeasureColumns, other.MeasureColumns)) return false;
            if (InheritTable != other.InheritTable) return false;
            if (InheritPrefix != other.InheritPrefix) return false;
            if (ModelingStep != other.ModelingStep) return false;
            if (Cacheable != other.Cacheable) return false;
            if (TTL != other.TTL) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (Transactional != other.Transactional) return false;
            if (CurrentSnapshotOnly != other.CurrentSnapshotOnly) return false;
            if (TimeAlias != other.TimeAlias) return false;
            if (ModelingStep != other.ModelingStep) return false;
            if (!Repository.EqualsWithNull(Grain, other.Grain)) return false;
            if (!Repository.EqualsArray(LoadGroups, other.LoadGroups)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class MeasureColumn : BaseObject
    {
        public string TableName;
        public string ColumnName;
        public string Description;
        public string PhysicalName;
        public string AggregationRule;
        public string DimensionRules;
        public string DisplayFunction;
        [System.ComponentModel.DefaultValueAttribute("Integer")]
        public string DataType;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int Width;
        public string Format;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Qualify;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Key;
        public string PerformanceCategory;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int Improve;
        public string Minimum;
        public string Maximum;
        public string MinTarget;
        public string MaxTarget;
        public string MinImprovement;
        public string MaxImprovement;
        public string ValidMeasure;
        public string Opportunity;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool EnforceLimits;
        public string AttributeWeightMeasure;
        public string AnalysisMeasure;
        public string SegmentMeasure;
        public FilteredMeasureDefinition FilteredMeasures;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutoGenerated;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool LogicalAutoGenerated;
        public string RootMeasure;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool NotCacheable = false;
        public string PhysicalFilter;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ManuallyEdited;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(MeasureColumn).Equals(obj.GetType())) return false;
            MeasureColumn other = (MeasureColumn)obj;
            if (!Repository.EqualsWithNull(TableName, other.TableName)) return false;
            if (!Repository.EqualsWithNull(ColumnName, other.ColumnName)) return false;
            if (!Repository.EqualsWithNull(Description, other.Description)) return false;
            if (!Repository.EqualsWithNull(PhysicalName, other.PhysicalName)) return false;
            if (!Repository.EqualsWithNull(AggregationRule, other.AggregationRule)) return false;
            if (!Repository.EqualsWithNull(DimensionRules, other.DimensionRules)) return false;
            if (!Repository.EqualsWithNull(DisplayFunction, other.DisplayFunction)) return false;
            if (!Repository.EqualsWithNull(DataType, other.DataType)) return false;
            if (!Repository.EqualsWithNull(Format, other.Format)) return false;
            if (Qualify != other.Qualify) return false;
            if (Key != other.Key) return false;
            if (!Repository.EqualsWithNull(PerformanceCategory, other.PerformanceCategory)) return false;
            if (Improve != other.Improve) return false;
            if (!Repository.EqualsWithNull(Minimum, other.Minimum)) return false;
            if (!Repository.EqualsWithNull(Maximum, other.Maximum)) return false;
            if (!Repository.EqualsWithNull(MinTarget, other.MinTarget)) return false;
            if (!Repository.EqualsWithNull(MaxTarget, other.MaxTarget)) return false;
            if (!Repository.EqualsWithNull(MinImprovement, other.MinImprovement)) return false;
            if (!Repository.EqualsWithNull(MaxImprovement, other.MaxImprovement)) return false;
            if (!Repository.EqualsWithNull(ValidMeasure, other.ValidMeasure)) return false;
            if (!Repository.EqualsWithNull(Opportunity, other.Opportunity)) return false;
            if (EnforceLimits != other.EnforceLimits) return false;
            if (!Repository.EqualsWithNull(AttributeWeightMeasure, other.AttributeWeightMeasure)) return false;
            if (!Repository.EqualsWithNull(AnalysisMeasure, other.AnalysisMeasure)) return false;
            if (!Repository.EqualsWithNull(SegmentMeasure, other.SegmentMeasure)) return false;
            if (!Repository.EqualsWithNull(FilteredMeasures, other.FilteredMeasures)) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (LogicalAutoGenerated != other.LogicalAutoGenerated) return false;
            if (Width != other.Width) return false;
            if (!Repository.EqualsWithNull(RootMeasure, other.RootMeasure)) return false;
            if (!Repository.EqualsWithNull(NotCacheable, other.NotCacheable)) return false;
            if (!Repository.EqualsWithNull(PhysicalFilter, other.PhysicalFilter)) return false;
            if (ManuallyEdited != other.ManuallyEdited) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public String outputDocString(char d)
        {
            String output = ColumnName + d + AggregationRule + d + DataType + d +
                Format + d + PerformanceCategory + d + Improve + d + Minimum + d + Maximum + d +
                MinTarget + d + MaxTarget + d +
                MinImprovement + d + MaxImprovement + d + Opportunity + d + EnforceLimits + d +
                AttributeWeightMeasure;
            return (output);
        }

        public static String outputDocTitleString(char d)
        {
            String output = "Column Name" + d + "Rule" + d + "DataType" + d +
                "Format" + d + "Performance Category" + d + "Improve" + d + "Minimum" + d + "Maximum" + d +
                "MinTarget" + d + "MaxTarget" + d +
                "MinImprovement" + d + "MaxImprovement" + d + "Opportunity" + d + "EnforceLimits" + d +
                "AttributeWeightMeasure";
            return (output);
        }
    }

    [Serializable]
    public class DimensionTable : BaseObject
    {
        public static int NORMAL = 0;
        public static int OPAQUE_VIEW = 1;
        public string DimensionName;
        public string TableName;
        public string Level;
        public int Type;
        public string PhysicalName;
        public TableSource TableSource;
        public ColumnSubstitution[] ColumnSubstitutions;
        public string OpaqueView;
        public DimensionColumn[] DimensionColumns;
        public string InheritTable;
        public string InheritPrefix;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DontInheritJoins = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DontInheritColumns = false;
        public string VirtualMeasuresInheritTable;
        public string VirtualMeasuresPrefix;
        public string[] IgnoreJoinsForTables;
        public bool Cacheable = true;
        [System.ComponentModel.DefaultValueAttribute(-1)]
        public int TTL = -1;
        public int ModelingStep = 0;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutoGenerated = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Calculated = false;
        [XmlIgnore]
        [NonSerialized]
        public bool amIBeingDragged = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool SnowflakeAllDimensions;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(DimensionTable).Equals(obj.GetType())) return false;
            DimensionTable other = (DimensionTable)obj;
            if (!Repository.EqualsWithNull(DimensionName, other.DimensionName)) return false;
            if (!Repository.EqualsWithNull(TableName, other.TableName)) return false;
            if (!Repository.EqualsWithNull(Level, other.Level)) return false;
            if (Type != other.Type) return false;
            if (!Repository.EqualsWithNull(PhysicalName, other.PhysicalName)) return false;
            if (!Repository.EqualsWithNull(TableSource, other.TableSource)) return false;
            if (!Repository.EqualsArray(ColumnSubstitutions, other.ColumnSubstitutions)) return false;
            if (!Repository.EqualsWithNull(OpaqueView, other.OpaqueView)) return false;
            if (!Repository.EqualsArray(DimensionColumns, other.DimensionColumns)) return false;
            if (!Repository.EqualsWithNull(InheritTable, other.InheritTable)) return false;
            if (!Repository.EqualsWithNull(InheritPrefix, other.InheritPrefix)) return false;
            if (!Repository.EqualsWithNull(VirtualMeasuresInheritTable, other.VirtualMeasuresInheritTable)) return false;
            if (!Repository.EqualsWithNull(VirtualMeasuresPrefix, other.VirtualMeasuresPrefix)) return false;
            if (Cacheable != other.Cacheable) return false;
            if (TTL != other.TTL) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (Calculated != other.Calculated) return false;
            if (ModelingStep != other.ModelingStep) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class DimensionColumn : BaseObject
    {
        public string TableName;
        public string ColumnName;
        public string Description;
        [System.ComponentModel.DefaultValueAttribute("Integer")]
        public string DataType;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Key;
        public string Format;
        [System.ComponentModel.DefaultValueAttribute("0")]
        public string Width;
        public string PhysicalName;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Qualify;
        public DisplayMap DisplayMap;
        public string UnknownValue;
        public string VolatileKey;
        public string MeasureExpression;
        public string GenericName;
        public string DesiredValues;
        public string IgnoredValues;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public double Score;
        public string BlockingGroup;
        [System.ComponentModel.DefaultValueAttribute("No")]
        public string Maskable = "No";
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutoGenerated;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool LogicalAutoGenerated;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Index;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Constant;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool NotCacheable = false;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ManuallyEdited;
        [System.ComponentModel.DefaultValueAttribute("NONE")]
        public string CacheBoundary = Repository.CACHE_NO_BOUNDARY;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(DimensionColumn).Equals(obj.GetType())) return false;
            DimensionColumn other = (DimensionColumn)obj;
            if (!Repository.EqualsWithNull(TableName, other.TableName)) return false;
            if (!Repository.EqualsWithNull(ColumnName, other.ColumnName)) return false;
            if (!Repository.EqualsWithNull(Description, other.Description)) return false;
            if (!Repository.EqualsWithNull(DataType, other.DataType)) return false;
            if (Key != other.Key) return false;
            if (!Repository.EqualsWithNull(Width, other.Width)) return false;
            if (!Repository.EqualsWithNull(Format, other.Format)) return false;
            if (!Repository.EqualsWithNull(PhysicalName, other.PhysicalName)) return false;
            if (Qualify != other.Qualify) return false;
            if (UnknownValue != other.UnknownValue) return false;
            if (!Repository.EqualsWithNull(VolatileKey, other.VolatileKey)) return false;
            if (!Repository.EqualsWithNull(DisplayMap, other.DisplayMap)) return false;
            if (!Repository.EqualsWithNull(MeasureExpression, other.MeasureExpression)) return false;
            if (!Repository.EqualsWithNull(GenericName, other.GenericName)) return false;
            if (!Repository.EqualsWithNull(DesiredValues, other.DesiredValues)) return false;
            if (!Repository.EqualsWithNull(IgnoredValues, other.IgnoredValues)) return false;
            if (Score != other.Score) return false;
            if (!Repository.EqualsWithNull(BlockingGroup, other.BlockingGroup)) return false;
            if (!Repository.EqualsWithNull(Maskable, other.Maskable)) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (LogicalAutoGenerated != other.LogicalAutoGenerated) return false;
            if (Index != other.Index) return false;
            if (Constant != other.Constant) return false;
            if (NotCacheable != other.NotCacheable) return false;
            if (ManuallyEdited != other.ManuallyEdited) return false;
            if (CacheBoundary != other.CacheBoundary) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public String outputDocString(DimensionTable dt, char d)
        {
            String output = dt.DimensionName + d + ColumnName + d + DataType + d +
                Format + d + DesiredValues + d + IgnoredValues + d + Score + d + BlockingGroup;
            return (output);
        }

        public static String outputDocTitleString(char d)
        {
            String output = "Dimension Name" + d + "Column Name" + d + "DataType" + d +
                "Format" + d + "Desired Values" + d + "Ignored Values" + d + "Score" + d + "Blocking Group";
            return (output);
        }
    }

    [Serializable]
    public class Join : BaseObject
    {
        public string Table1;
        public string Table2;
        public string JoinCondition;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Redundant;
        public string JoinType;
        public string Level;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutoGenerated;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Invalid;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Federated;
        [System.ComponentModel.DefaultValueAttribute(null)]
        public string[] FederatedJoinKeys;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Join).Equals(obj.GetType())) return false;
            Join other = (Join)obj;
            if (!Repository.EqualsWithNull(Table1, other.Table1)) return false;
            if (!Repository.EqualsWithNull(Table2, other.Table2)) return false;
            if (!Repository.EqualsWithNull(JoinCondition, other.JoinCondition)) return false;
            if (Redundant != other.Redundant) return false;
            if (!Repository.EqualsWithNull(JoinType, other.JoinType)) return false;
            if (!Repository.EqualsWithNull(Level, other.Level)) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (Invalid != other.Invalid) return false;
            if (Federated != other.Federated) return false;
            if (!Repository.EqualsArray(FederatedJoinKeys, other.FederatedJoinKeys)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

    [Serializable]
    public class SuccessModel : BaseObject
    {
        public string Name;
        public string Measure;
        public string TargetDimension;
        public string TargetLevel;
        public string IterateDimension;
        public string IterateLevel;
        public int BreakModel; //0 - No Break, 1 - Break on Cluster, 2 - Break on Attribute
        public string BreakAttribute;
        public string PartitionAttribute;
        public string[] Attributes;
        public Pattern[] Patterns;
        public string[] Filters;
        public bool FilterNullCategories;
        public string InheritFromModel;
        public bool GroupMeasureOnly;
        public bool Runnable;
        public bool CreateAggregates;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(SuccessModel).Equals(obj.GetType())) return false;
            SuccessModel other = (SuccessModel)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(Measure, other.Measure)) return false;
            if (!Repository.EqualsWithNull(TargetDimension, other.TargetDimension)) return false;
            if (!Repository.EqualsWithNull(TargetLevel, other.TargetLevel)) return false;
            if (!Repository.EqualsWithNull(IterateDimension, other.IterateDimension)) return false;
            if (!Repository.EqualsWithNull(IterateLevel, other.IterateLevel)) return false;
            if (BreakModel != other.BreakModel) return false;
            if (!Repository.EqualsWithNull(BreakAttribute, other.BreakAttribute)) return false;
            if (!Repository.EqualsWithNull(PartitionAttribute, other.PartitionAttribute)) return false;
            if (!Repository.EqualsArray(Attributes, other.Attributes)) return false;
            if (!Repository.EqualsArray(Patterns, other.Patterns)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (FilterNullCategories != other.FilterNullCategories) return false;
            if (!Repository.EqualsWithNull(InheritFromModel, other.InheritFromModel)) return false;
            if (GroupMeasureOnly != other.GroupMeasureOnly) return false;
            if (Runnable != other.Runnable) return false;
            if (CreateAggregates != other.CreateAggregates) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public List<Pattern> getPatterns(List<SuccessModel> smlist)
        {
            List<Pattern> plist = new List<Pattern>();
            if (InheritFromModel != null && smlist != null)
            {
                foreach (SuccessModel sm in smlist)
                {
                    if (sm.Name == InheritFromModel)
                    {
                        plist.AddRange(sm.getPatterns(smlist));
                        break;
                    }
                }
            }
            plist.AddRange(Patterns);
            return (plist);
        }
    }

    [Serializable]
    public class Pattern : BaseObject
    {
        public static int MEASUREONLY = 0;
        public static int ONEATTRIBUTE = 1;
        public static int TWOATTRIBUTE = 2;
        public int Type;
        public string Measure;
        public string Dimension1;
        public string Attribute1;
        public string Dimension2;
        public string Attribute2;
        public bool Proportion;
        public bool Actionable;
        public bool Model;
        public int MaxPatterns;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Pattern).Equals(obj.GetType())) return false;
            Pattern other = (Pattern)obj;
            if (Type != other.Type) return false;
            if (!Repository.EqualsWithNull(Measure, other.Measure)) return false;
            if (!Repository.EqualsWithNull(Dimension1, other.Dimension1)) return false;
            if (!Repository.EqualsWithNull(Attribute1, other.Attribute1)) return false;
            if (!Repository.EqualsWithNull(Dimension2, other.Dimension2)) return false;
            if (!Repository.EqualsWithNull(Attribute2, other.Attribute2)) return false;
            if (Proportion != other.Proportion) return false;
            if (Actionable != other.Actionable) return false;
            if (Model != other.Model) return false;
            if (MaxPatterns != other.MaxPatterns) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }

    [Serializable]
    public class QueryFilter : BaseObject
    {
        public static int TYPE_PREDICATE = 0;
        public static int TYPE_LOGICAL_OP = 1;
        public static int TYPE_DIMENSION_COLUMN = 0;
        public static int TYPE_MEASURE = 1;
        public string Name;
        public int Type;
        public int ColumnType;
        public string Dimension;
        public string Column;
        public string Operator;
        public string Operand;
        public QueryFilter QF1;
        public QueryFilter QF2;

        public static int sortCompareTo(QueryFilter one, QueryFilter two)
        {
            if (one == null && two == null) return 0;
            if (one != null && two == null) return -1;
            if (one == null && two != null) return 1;
            if (one.Name == null && two.Name == null) return 0;
            if (one.Name != null && two.Name == null) return -1;
            if (one.Name == null && two.Name != null) return 1;
            return one.Name.CompareTo(two.Name);

        }

        public override bool Equals(Object obj)
        {
            if (obj == null || obj == System.DBNull.Value) return false;
            if (!typeof(QueryFilter).Equals(obj.GetType())) return false;
            QueryFilter other = (QueryFilter)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (Type != other.Type) return false;
            if (ColumnType != other.ColumnType) return false;
            if (!Repository.EqualsWithNull(Dimension, other.Dimension)) return false;
            if (!Repository.EqualsWithNull(Column, other.Column)) return false;
            if (!Repository.EqualsWithNull(Operator, other.Operator)) return false;
            if (!Repository.EqualsWithNull(Operand, other.Operand)) return false;
            if (!Repository.EqualsWithNull(QF1, other.QF1)) return false;
            if (!Repository.EqualsWithNull(QF2, other.QF2)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            bool formatWithSpaces = false;
            if (Operator == "LIKE" || Operator == "NOT LIKE" || Operator == "IN" || Operator == "NOT IN")
            {
                formatWithSpaces = true;
            }
            if (Type == TYPE_PREDICATE)
            {
                if (ColumnType == TYPE_DIMENSION_COLUMN)
                {
                    return (Dimension + "." + Column + (formatWithSpaces ? " " : "") + Operator + (formatWithSpaces ? " " : "") + Operand);
                }
                else if (ColumnType == TYPE_MEASURE)
                {
                    return (Column + (formatWithSpaces ? " " : "") + Operator + (formatWithSpaces ? " " : "") + Operand);
                }
            }
            else if (Type == TYPE_LOGICAL_OP)
            {
                if (QF1 != null && QF2 != null)
                {
                    return ("(" + QF1.ToString() + ") " + (formatWithSpaces ? " " : "") + Operator + (formatWithSpaces ? " " : "") + " (" + QF2.ToString() + ")");
                }
                else if (QF1 != null)
                {
                    return (QF1.ToString());
                }
                else if (QF2 != null)
                {
                    return (QF2.ToString());
                }
                else
                {
                    return "";
                }
            }
            return (null);
        }

        [Serializable]
        public class UseFilter
        {
            public QueryFilter QF;
            public bool used;

            public UseFilter(QueryFilter QF, bool used)
            {
                this.QF = QF;
                this.used = used;
            }
            public static int compareFilterAsUsed(UseFilter One, UseFilter Two)
            {
                if (One == null && Two == null) return 0;
                if (One != null && Two == null) return -1;
                if (One == null && Two != null) return 1;
                if (One.used && !Two.used)
                {
                    return -1;
                }
                else if (!One.used && Two.used)
                {
                    return 1;
                }
                else
                {
                    return (One.QF.Name.CompareTo(Two.QF.Name));
                }

            }
        }

        public static List<QueryFilter> sortListAsUsed(List<QueryFilter> list, string[] listOfUsed)
        {
            if (list == null) return null;
            if (listOfUsed == null || listOfUsed.Length == 0)
            {
                list.Sort(QueryFilter.sortCompareTo);
                return list;
            }
            List<string> lListOfUsed = new List<string>(listOfUsed);
            List<UseFilter> uList = new List<UseFilter>();
            for (int i = 0; i < list.Count; i++)
            {
                UseFilter UF = new UseFilter(list[i], lListOfUsed.Contains(list[i].Name) ? true : false);
                uList.Add(UF);
            }
            uList.Sort(UseFilter.compareFilterAsUsed);
            List<QueryFilter> sList = new List<QueryFilter>();
            for (int i = 0; i < uList.Count; i++)
            {
                sList.Add(uList[i].QF);
            }
            return sList;

        }
        public static void sortedListBox(CheckedListBox c, List<QueryFilter> list, string[] listOfUsed)
        {
            List<QueryFilter> sortedList = QueryFilter.sortListAsUsed(list, listOfUsed);
            List<string> sListOfUsed = listOfUsed != null ? new List<string>(listOfUsed) : new List<string>();
            c.Items.Clear();
            foreach (QueryFilter qf in sortedList)
            {
                c.Items.Add(qf.Name);
                if (sListOfUsed.Contains(qf.Name))
                {
                    c.SetItemChecked(c.Items.Count - 1, true);
                }
            }
            return;
        }
    }

    [Serializable]
    public class ClassificationRule : BaseObject
    {
        public string Priority; // really an 'int', but easier to treat as a string in the data grid view
        public string Filter;
        public string Explanation;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ClassificationRule).Equals(obj.GetType())) return false;
            ClassificationRule other = (ClassificationRule)obj;
            if (!Repository.EqualsWithNull(Priority, other.Priority)) return false;
            if (!Repository.EqualsWithNull(Filter, other.Filter)) return false;
            if (!Repository.EqualsWithNull(Explanation, other.Explanation)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Outcome : BaseObject
    {
        public string Name;
        public string SuccessModelName;
        public string OutputTableName;
        public string OutcomeGroupName;
        public string OutcomeGroupTableName;
        public string IterationAnalysisValue;
        public string IterationModelValue;
        public bool EstimateClass;
        public bool EstimateValue;
        public bool EstimateValueInClass;
        public bool IncludeAllNonzero;
        public bool EstimateDifferenceFromActual;
        public string TargetMeasure;
        public string ClassMeasure;
        public bool DropExistingTable;
        public bool OutputOnlyClass;
        public bool BiasToUniformClass;
        public bool UseMinEstimatedValue;
        public double MinEstimatedValue;
        public bool ExcludeIfBelowMinValue;
        public bool RelativeMinValue;
        public bool MinRelativeToMeasure;
        public string MinMeasure;
        public bool UseMaxEstimatedValue;
        public double MaxEstimatedValue;
        public bool ExcludeIfAboveMaxValue;
        public bool RelativeMaxValue;
        public bool MaxRelativeToMeasure;
        public string MaxMeasure;
        public bool OutputBuckets;
        public int OutputBucketType; // 0 - Value, 1 - Difference from Actual, 2 - %Difference from Actual
        public MeasureBucket[] Buckets;
        public bool RankFactors;
        public int NumFactorsToRank; // 0 - All
        public OutcomeFactor[] Factors;
        public ClassificationRule[] RuleSet;
        public ClassificationRule[] ExclusionRuleSet;
        public ClassifierSettings Classifiers;
        public string TrainingFilter;
        public string ScoringFilter;
        public string NameFilter;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Outcome).Equals(obj.GetType())) return false;
            Outcome other = (Outcome)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(SuccessModelName, other.SuccessModelName)) return false;
            if (!Repository.EqualsWithNull(OutputTableName, other.OutputTableName)) return false;
            if (!Repository.EqualsWithNull(OutcomeGroupName, other.OutcomeGroupName)) return false;
            if (!Repository.EqualsWithNull(OutcomeGroupTableName, other.OutcomeGroupTableName)) return false;
            if (!Repository.EqualsWithNull(IterationAnalysisValue, other.IterationAnalysisValue)) return false;
            if (!Repository.EqualsWithNull(IterationModelValue, other.IterationModelValue)) return false;
            if (EstimateClass != other.EstimateClass) return false;
            if (EstimateValue != other.EstimateValue) return false;
            if (EstimateValueInClass != other.EstimateValueInClass) return false;
            if (IncludeAllNonzero != other.IncludeAllNonzero) return false;
            if (EstimateDifferenceFromActual != other.EstimateDifferenceFromActual) return false;
            if (!Repository.EqualsWithNull(TargetMeasure, other.TargetMeasure)) return false;
            if (!Repository.EqualsWithNull(ClassMeasure, other.ClassMeasure)) return false;
            if (DropExistingTable != other.DropExistingTable) return false;
            if (OutputOnlyClass != other.OutputOnlyClass) return false;
            if (BiasToUniformClass != other.BiasToUniformClass) return false;
            if (UseMaxEstimatedValue != other.UseMaxEstimatedValue) return false;
            if (MinEstimatedValue != other.MinEstimatedValue) return false;
            if (ExcludeIfBelowMinValue != other.ExcludeIfBelowMinValue) return false;
            if (RelativeMinValue != other.RelativeMinValue) return false;
            if (MinRelativeToMeasure != other.MinRelativeToMeasure) return false;
            if (!Repository.EqualsWithNull(MinMeasure, other.MinMeasure)) return false;
            if (UseMaxEstimatedValue != other.UseMaxEstimatedValue) return false;
            if (MaxEstimatedValue != other.MaxEstimatedValue) return false;
            if (ExcludeIfAboveMaxValue != other.ExcludeIfAboveMaxValue) return false;
            if (RelativeMaxValue != other.RelativeMaxValue) return false;
            if (MaxRelativeToMeasure != other.MaxRelativeToMeasure) return false;
            if (!Repository.EqualsWithNull(MaxMeasure, other.MaxMeasure)) return false;
            if (OutputBuckets != other.OutputBuckets) return false;
            if (OutputBucketType != other.OutputBucketType) return false;
            if (!Repository.EqualsArray(Buckets, other.Buckets)) return false;
            if (RankFactors != other.RankFactors) return false;
            if (NumFactorsToRank != other.NumFactorsToRank) return false;
            if (!Repository.EqualsArray(Factors, other.Factors)) return false;
            if (!Repository.EqualsArray(RuleSet, other.RuleSet)) return false;
            if (!Repository.EqualsArray(ExclusionRuleSet, other.ExclusionRuleSet)) return false;
            if (!Repository.EqualsWithNull(Classifiers, other.Classifiers)) return false;
            if (!Repository.EqualsWithNull(TrainingFilter, other.TrainingFilter)) return false;
            if (!Repository.EqualsWithNull(ScoringFilter, other.ScoringFilter)) return false;
            if (!Repository.EqualsWithNull(NameFilter, other.NameFilter)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class VirtualColumn : BaseAuditableObject
    {
        public static int TYPE_PIVOT = 2;
        public static int TYPE_BUCKETED = 3;
        public string TableName;
        public string MeasurePrefix;
        public string Measure;
        [XmlArray("Filters"), XmlArrayItem("Filter", typeof(string))]
        public string[] Filters;
        public int Type;
        public string PivotDimension;
        public string PivotLevel;
        public bool PivotPercent;
        [XmlArray("Buckets"), XmlArrayItem("MeasureBucket", typeof(MeasureBucket))]
        public MeasureBucket[] Buckets;
        public string measureBucketDefaultName;
        public string measureBucketNullName;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(VirtualColumn).Equals(obj.GetType())) return false;
            VirtualColumn other = (VirtualColumn)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(TableName, other.TableName)) return false;
            if (!Repository.EqualsWithNull(MeasurePrefix, other.MeasurePrefix)) return false;
            if (!Repository.EqualsWithNull(Measure, other.Measure)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (Type != other.Type) return false;
            if (!Repository.EqualsWithNull(PivotDimension, other.PivotDimension)) return false;
            if (!Repository.EqualsWithNull(PivotLevel, other.PivotLevel)) return false;
            if (PivotPercent != other.PivotPercent) return false;
            if (!Repository.EqualsArray(Buckets, other.Buckets)) return false;
            if (!Repository.EqualsWithNull(measureBucketDefaultName, other.measureBucketDefaultName)) return false;
            if (!Repository.EqualsWithNull(measureBucketNullName, other.measureBucketNullName)) return false;
            return true;
        }

        public List<DiffObject> diffDescription(VirtualColumn other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(VirtualColumn).Equals(other.GetType())) return null;

            if (!Repository.EqualsWithNull(Name, other.Name))
                differences.Add(new DiffObject("Name", Name, other.Name));
            if (!Repository.EqualsWithNull(TableName, other.TableName))
                differences.Add(new DiffObject("Table Name", TableName, other.TableName));
            if (!Repository.EqualsWithNull(MeasurePrefix, other.MeasurePrefix))
                differences.Add(new DiffObject("Measure Prefix", MeasurePrefix, other.MeasurePrefix));
            if (!Repository.EqualsWithNull(Measure, other.Measure))
                differences.Add(new DiffObject("Measure", Measure, other.Measure));
            if (!Repository.EqualsArray(Filters, other.Filters))
                differences.Add(new DiffObject("Filters", Util.getDisplayString(Filters), Util.getDisplayString(other.Filters)));
            if (Type != other.Type)
                differences.Add(new DiffObject("Type", Type == TYPE_BUCKETED ? "Bucketed Measure" : "Pivot", other.Type == TYPE_BUCKETED ? "Bucket" : "Pivot"));
            if (!Repository.EqualsWithNull(PivotDimension, other.PivotDimension))
                differences.Add(new DiffObject("Pivot Dimension", PivotDimension, other.PivotDimension));
            if (!Repository.EqualsWithNull(PivotLevel, other.PivotLevel))
                differences.Add(new DiffObject("Pivot Level", PivotLevel, other.PivotLevel));
            if (PivotPercent != other.PivotPercent)
                differences.Add(new DiffObject("Pivot Percent", PivotPercent, other.PivotPercent));
            if (!Repository.EqualsArray(Buckets, other.Buckets))
                differences.Add(new DiffObject("Buckets", "", ""));
            if (!Repository.EqualsWithNull(measureBucketDefaultName, other.measureBucketDefaultName))
                differences.Add(new DiffObject("Bucket Default Name", measureBucketDefaultName, other.measureBucketDefaultName));
            if (!Repository.EqualsWithNull(measureBucketNullName, other.measureBucketNullName))
                differences.Add(new DiffObject("Bucket Null Name", measureBucketNullName, other.measureBucketNullName));

            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Bucketed Measure", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public void getDifferences(VirtualColumn currentvc, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            // Object level differences
            List<DiffObject> diffs = diffDescription(currentvc);
            if (diffs.Count > 0)
            {
                itemnames.Add(Name);
                itempaths.Add((Type == TYPE_BUCKETED ? "Bucketed Measure" : "Pivot") + "|" + Name);
                itemtypes.Add(Type == TYPE_BUCKETED ? "Bucketed Measure" : "Pivot");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string toAuditString()
        {
            string ret = "VirtualColumn ";
            ret += Name;
            ret += " ";
            ret += Guid;
            ret += " TableName=";
            ret += TableName;
            ret += " MeasurePrefix=";
            ret += MeasurePrefix;
            ret += " Measure=";
            ret += Measure;
            if (Filters != null && Filters.Length > 0)
            {
                ret += " Filters=[";
                foreach (string f in Filters)
                {
                    ret += "[";
                    ret += f;
                    ret += "]";
                }
                ret += "]";
            }
            ret += " Type=";
            if (Type == TYPE_BUCKETED)
                ret += "Bucketed";
            else
                ret += "Pivoted";
            if (PivotDimension != null && PivotDimension.Length > 0)
            {
                ret += " PivotDimension=";
                ret += PivotDimension;
            }
            if (PivotLevel != null && PivotLevel.Length > 0)
            {
                ret += " PivotLevel=";
                ret += PivotLevel;
            }
            if (PivotPercent)
            {
                ret += " Pivot Percent";
            }
            if (Buckets != null && Buckets.Length > 0)
            {
                ret += " Buckets=[";
                foreach (MeasureBucket mb in Buckets)
                {
                    ret += "[";
                    ret += mb.toAuditString();
                    ret += "]";
                }
                ret += "]";
            }
            if (measureBucketDefaultName != null && measureBucketDefaultName.Length > 0)
            {
                ret += " MeasureBucketDefaultName=";
                ret += measureBucketDefaultName;
            }
            if (measureBucketNullName != null && measureBucketNullName.Length > 0)
            {
                ret += " MeasureBucketNullName=";
                ret += measureBucketNullName;
            }
            return ret;

        }
    }

    [Serializable]
    public class MeasureBucket : BaseObject
    {
        public string Name;
        public string Min;
        public string Max;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(MeasureBucket).Equals(obj.GetType())) return false;
            MeasureBucket other = (MeasureBucket)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(Min, other.Min)) return false;
            if (!Repository.EqualsWithNull(Max, other.Max)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string toAuditString()
        {
            string ret = "Name=";
            ret += Name;
            ret += " Min=";
            ret += Min;
            ret += " Max=";
            ret += Max;
            return ret;
        }
    }

    [Serializable]
    public class Hierarchy : BaseAuditableObject
    {
        public string DimensionName;
        public string DimensionKeyLevel;
        public string OlapDimensionName;
        public string ExclusionFilter;
        public string NoMeasureFilter;
        public Level[] Children;
        public string[] SharedChildren;
        public bool AutoGenerated;
        public bool GeneratedTime;
        public bool Hidden;
        public bool Locked;
        public bool ProcessedDependencies;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool FilterForAllLoads;
        public string SourceGroups;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ModelWizard;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Imported;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Hierarchy).Equals(obj.GetType())) return false;
            Hierarchy other = (Hierarchy)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(DimensionName, other.DimensionName)) return false;
            if (!Repository.EqualsWithNull(DimensionKeyLevel, other.DimensionKeyLevel)) return false;
            if (!Repository.EqualsWithNull(OlapDimensionName, other.OlapDimensionName)) return false;
            if (!Repository.EqualsWithNull(ExclusionFilter, other.ExclusionFilter)) return false;
            if (!Repository.EqualsWithNull(NoMeasureFilter, other.NoMeasureFilter)) return false;
            if (!Repository.EqualsArray(Children, other.Children)) return false;
            if (!Repository.EqualsArray(SharedChildren, other.SharedChildren)) return false;
            if (AutoGenerated != other.AutoGenerated) return false;
            if (GeneratedTime != other.GeneratedTime) return false;
            if (Hidden != other.Hidden) return false;
            if (Locked != other.Locked) return false;
            if (ProcessedDependencies != other.ProcessedDependencies) return false;
            if (FilterForAllLoads != other.FilterForAllLoads) return false;
            if (!Repository.EqualsWithNull(SourceGroups, other.SourceGroups)) return false;
            if (ModelWizard != other.ModelWizard) return false;
            if (Imported != other.Imported) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public List<DiffObject> diffDescription(Hierarchy other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(Hierarchy).Equals(other.GetType())) return null;
            if (!Repository.EqualsWithNull(Name, other.Name))
            {
                differences.Add(new DiffObject("Hierarchy Name", Name, other.Name));
            }
            if (!Repository.EqualsArray(Children, other.Children))
            {
                if (Children.Length != other.Children.Length)
                {
                    differences.Add(new DiffObject("Level [" + Name + "] Different number of child levels", "", ""));
                }
                else
                {
                    for (int i = 0; i < Children.Length; i++)
                    {
                        differences.AddRange(Children[i].diffDescription(other.Children[i]));
                    }
                }
            }
            if (!Repository.EqualsWithNull(DimensionName, other.DimensionName))
                differences.Add(new DiffObject("Dimension", DimensionName, other.DimensionName));
            if (!Repository.EqualsWithNull(DimensionKeyLevel, other.DimensionKeyLevel))
                differences.Add(new DiffObject("Dimension Key Level", DimensionKeyLevel, other.DimensionKeyLevel));
            if (!Repository.EqualsWithNull(OlapDimensionName, other.OlapDimensionName))
                differences.Add(new DiffObject("Olap Dimension Name", OlapDimensionName, other.OlapDimensionName));
            if (!Repository.EqualsWithNull(ExclusionFilter, other.ExclusionFilter))
                differences.Add(new DiffObject("Exclusion Filter", ExclusionFilter, other.ExclusionFilter));
            if (!Repository.EqualsWithNull(NoMeasureFilter, other.NoMeasureFilter))
                differences.Add(new DiffObject("No Measure Filter", NoMeasureFilter, other.NoMeasureFilter));
            if (!Repository.EqualsArray(SharedChildren, other.SharedChildren))
                differences.Add(new DiffObject("Shared Children", SharedChildren, other.SharedChildren));
            if (AutoGenerated != other.AutoGenerated)
                differences.Add(new DiffObject("Auto Generated", AutoGenerated, other.AutoGenerated));
            if (GeneratedTime != other.GeneratedTime)
                differences.Add(new DiffObject("Generated Time", GeneratedTime, other.GeneratedTime));
            if (Hidden != other.Hidden)
                differences.Add(new DiffObject("Hidden", Hidden, other.Hidden));
            if (Locked != other.Locked)
                differences.Add(new DiffObject("Locked", Locked, other.Locked));
            if (ProcessedDependencies != other.ProcessedDependencies)
                differences.Add(new DiffObject("Processed Dependencies", ProcessedDependencies, other.ProcessedDependencies));
            if (FilterForAllLoads != other.FilterForAllLoads)
                differences.Add(new DiffObject("Filter For All Loads", FilterForAllLoads, other.FilterForAllLoads));
            if (!Repository.EqualsWithNull(SourceGroups, other.SourceGroups))
                differences.Add(new DiffObject("Source Groups", SourceGroups, other.SourceGroups));
            if (ModelWizard != other.ModelWizard)
                differences.Add(new DiffObject("Model Wizard", ModelWizard, other.ModelWizard));
            if (Imported != other.Imported)
                differences.Add(new DiffObject("Imported", Imported, other.Imported));
            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Hierarchy [" + Name + "] Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public void getDifferences(Hierarchy currenth, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            // Object level differences
            List<DiffObject> diffs = diffDescription(currenth);
            if (diffs.Count > 0)
            {
                itemnames.Add(Name);
                itempaths.Add(Name);
                itemtypes.Add("Hierarchy");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }

        public void getChildLevelNames(List<string> list)
        {
            foreach (Level l in Children)
            {
                if (!list.Contains(l.Name))
                    list.Add(l.Name);
                l.getChildLevelNames(list);
            }
        }

        public void getChildLevels(List<Level> list)
        {
            foreach (Level l in Children)
            {
                if (!list.Contains(l))
                    list.Add(l);
                l.getChildLevels(this, list);
            }
        }

        public Level findLevel(string levelName)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                if (l.Name.Equals(levelName))
                {
                    return l;
                }
                else
                {
                    // Call the findLevel on the level, which will recursively try to find the 
                    // level under the subtree below the level node.
                    retLevel = l.findLevel(this, levelName, true);
                    if (retLevel != null)
                    {
                        return retLevel;
                    }
                }
            }

            return retLevel;
        }

        public Level findLevelWithColumn(string colName)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                retLevel = l.findLevelWithColumn(colName);
                if (retLevel != null)
                {
                    return retLevel;
                }
            }
            return retLevel;
        }

        public Level findLevelWithKey(LevelKey lk)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                foreach (LevelKey ck in l.Keys)
                {
                    bool found = true;
                    foreach (string s in lk.ColumnNames)
                    {
                        if (Array.IndexOf<string>(ck.ColumnNames, s) < 0)
                        {
                            found = false;
                            break;
                        }
                    }
                    if (found)
                    {
                        return (l);
                    }
                }
                retLevel = l.findLevelWithKey(lk);
                if (retLevel != null)
                {
                    return retLevel;
                }
            }
            return retLevel;
        }

        public Level findLevelWithKeyColumn(string s)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                foreach (LevelKey ck in l.Keys)
                {
                    if (Array.IndexOf<string>(ck.ColumnNames, s) >= 0)
                        return (l);
                }
                retLevel = l.findLevelWithKeyColumn(s);
                if (retLevel != null)
                    return retLevel;
            }
            return retLevel;
        }

        public List<Level> getAncestors(String levelName)
        {
            List<Level> ancestors = new List<Level>();
            findAncestors(levelName, ancestors);
            return ancestors;
        }

        public void findAncestors(String levelName, List<Level> ancestors)
        {
            List<Level> parents = getParents(levelName);
            foreach (Level parent in parents)
            {
                if (ancestors.Contains(parent))
                {
                    continue; // nothing to do
                }
                ancestors.Add(parent);
                // recursively add ancestors of the parent
                findAncestors(parent.Name, ancestors);
            }
        }

        public void findParents(string levelName, List<Level> parentList)
        {
            List<Level> plist = getParents(levelName);
            parentList.AddRange(plist);
        }

        public List<Level> getParents(string levelName)
        {
            List<Level> descendents = new List<Level>();
            List<Level> parentList = new List<Level>();
            getChildLevels(descendents);
            foreach (Level l in descendents)
            {
                if (l.findChildLevel(this, levelName, false) != null)
                {
                    parentList.Add(l);
                }
            }
            return parentList;
        }

        public Level findLevelContainingDimensionKey()
        {
            return (findLevel(DimensionKeyLevel));
        }

        public Level getLowestGeneratedLevel()
        {
            foreach (Level l in Children)
            {
                Level cl = l.getLowestGeneratedLevel();
                if (cl != null)
                    return (cl);
            }
            return (null);
        }

        public override string toAuditString()
        {
            string ret = "Hierarchy ";
            ret += Name;
            ret += " ";
            ret += Guid;
            return ret;
        }
    }

    [Serializable]
    public class Level : BaseObject
    {
        public string Name;
        public int Cardinality;
        public string[] ColumnNames;
        public bool[] HiddenColumns;
        public Level[] Children;
        public LevelKey[] Keys;
        public LevelAlias[] Aliases;
        public string[] SharedChildren;
        public bool GenerateDimensionTable;
        public bool Degenerate;
        public int SCDType = 1;
        public bool GenerateInheritedCurrentDimTable;
        public bool Locked;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AutomaticallyCreatedAsDegenerate;
        public ForeignKey[] ForeignKeys;
        // Depricated column that will be converted automatically into ForeignKeys upon load
        public string[] ForeignKeySources;
        [XmlIgnore]
        [NonSerialized]
        public string[] FloatingColumnsToMap;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Level).Equals(obj.GetType())) return false;
            Level other = (Level)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (Cardinality != other.Cardinality) return false;
            if (!Repository.EqualsArray(ColumnNames, other.ColumnNames)) return false;
            if (!Repository.EqualsBoolArray(HiddenColumns, other.HiddenColumns)) return false;
            if (!Repository.EqualsArray(Children, other.Children)) return false;
            if (!Repository.EqualsArray(Keys, other.Keys)) return false;
            if (!Repository.EqualsArray(Aliases, other.Aliases)) return false;
            if (!Repository.EqualsArray(SharedChildren, other.SharedChildren)) return false;
            if (!Repository.EqualsArray(ForeignKeys, other.ForeignKeys)) return false;
            if (GenerateDimensionTable != other.GenerateDimensionTable) return false;
            if (SCDType != other.SCDType) return false;
            if (Degenerate != other.Degenerate) return false;
            if (GenerateInheritedCurrentDimTable != other.GenerateInheritedCurrentDimTable) return false;
            if (Locked != other.Locked) return false;
            if (AutomaticallyCreatedAsDegenerate != other.AutomaticallyCreatedAsDegenerate) return false;
            if (!Repository.EqualsArray(FloatingColumnsToMap, other.FloatingColumnsToMap)) return false;
            return true;
        }

        public List<DiffObject> diffDescription(Level other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(Level).Equals(other.GetType())) return null;
            if (!Repository.EqualsWithNull(Name, other.Name))
            {
                differences.Add(new DiffObject("Level Name", Name, other.Name));
            }
            if (Cardinality != other.Cardinality)
                differences.Add(new DiffObject("Level [" + Name + "] Cardinality", Cardinality, other.Cardinality));
            if (!Repository.EqualsArray(ColumnNames, other.ColumnNames))
                differences.Add(new DiffObject("Level [" + Name + "] Column Names", ColumnNames, other.ColumnNames));
            if (!Repository.EqualsBoolArray(HiddenColumns, other.HiddenColumns))
                differences.Add(new DiffObject("Level [" + Name + "] Hidden Columns", HiddenColumns, other.HiddenColumns));
            if (!Repository.EqualsArray(Children, other.Children))
            {
                if (Children.Length != other.Children.Length)
                {
                    differences.Add(new DiffObject("Level [" + Name + "] Different number of child levels", "", ""));
                }
                else
                {
                    for (int i = 0; i < Children.Length; i++)
                    {
                        differences.AddRange(Children[i].diffDescription(other.Children[i]));
                    }
                }
            }
            if (!Repository.EqualsArray(Keys, other.Keys))
            {
                StringBuilder sb1 = new StringBuilder();
                foreach (LevelKey lk in Keys)
                    sb1.Append((sb1.Length > 0 ? "," : "") + lk.getDisplayString());
                StringBuilder sb2 = new StringBuilder();
                foreach (LevelKey lk in other.Keys)
                    sb2.Append((sb2.Length > 0 ? "," : "") + lk.getDisplayString());
                differences.Add(new DiffObject("Level [" + Name + "] Keys", sb1.ToString(), sb2.ToString()));
            }
            if (!Repository.EqualsArray(Aliases, other.Aliases))
            {
                StringBuilder sb1 = new StringBuilder();
                foreach (LevelAlias la in Aliases)
                    sb1.Append((sb1.Length > 0 ? "," : "") + la.getDisplayString());
                StringBuilder sb2 = new StringBuilder();
                foreach (LevelAlias la in other.Aliases)
                    sb2.Append((sb2.Length > 0 ? "," : "") + la.getDisplayString());
                differences.Add(new DiffObject("Level [" + Name + "] Aliases", sb1.ToString(), sb2.ToString()));
            }
            if (!Repository.EqualsArray(SharedChildren, other.SharedChildren))
                differences.Add(new DiffObject("Level [" + Name + "] Shared Children", SharedChildren, other.SharedChildren));
            if (!Repository.EqualsArray(ForeignKeys, other.ForeignKeys))
            {
                StringBuilder sb1 = new StringBuilder();
                foreach (ForeignKey fk in ForeignKeys)
                    sb1.Append((sb1.Length > 0 ? "," : "") + fk.getDisplayString());
                StringBuilder sb2 = new StringBuilder();
                foreach (ForeignKey fk in other.ForeignKeys)
                    sb2.Append((sb2.Length > 0 ? "," : "") + fk.getDisplayString());
                differences.Add(new DiffObject("Level [" + Name + "] Foreign Keys", sb1.ToString(), sb2.ToString()));
            }
            if (GenerateDimensionTable != other.GenerateDimensionTable)
                differences.Add(new DiffObject("Level [" + Name + "] Generate Dimension Table", GenerateDimensionTable, other.GenerateDimensionTable));
            if (SCDType != other.SCDType)
                differences.Add(new DiffObject("Level [" + Name + "] SCDType", SCDType, other.SCDType));
            if (Degenerate != other.Degenerate)
                differences.Add(new DiffObject("Level [" + Name + "] Degenerate", Degenerate, other.Degenerate));
            if (GenerateInheritedCurrentDimTable != other.GenerateInheritedCurrentDimTable)
                differences.Add(new DiffObject("Level [" + Name + "] Generate Inherited Current Dim Table", GenerateInheritedCurrentDimTable, other.GenerateInheritedCurrentDimTable));
            if (Locked != other.Locked)
                differences.Add(new DiffObject("Level [" + Name + "] Locked", Locked, other.Locked));
            if (AutomaticallyCreatedAsDegenerate != other.AutomaticallyCreatedAsDegenerate)
                differences.Add(new DiffObject("Level [" + Name + "] AutomaticallyCreatedAsDegenerate", AutomaticallyCreatedAsDegenerate, other.AutomaticallyCreatedAsDegenerate));
            if (!Repository.EqualsArray(FloatingColumnsToMap, other.FloatingColumnsToMap))
                differences.Add(new DiffObject("Level [" + Name + "] FloatingColumnsToMap", FloatingColumnsToMap, other.FloatingColumnsToMap));
            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Level [" + Name + "] Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void getChildLevelNames(List<string> list)
        {
            foreach (Level l in Children)
            {
                list.Add(l.Name);
                l.getChildLevelNames(list);
            }
        }

        public int getDepth()
        {
            if (Children.Length == 0)
                return (1);
            int max = 0;
            foreach (Level l in Children)
            {
                int gd = l.getDepth();
                if (gd > max)
                    max = gd;
            }
            return (max + 1);
        }

        public List<Level> getChildLevels(Hierarchy h, bool ignoreSharedChildren)
        {
            List<Level> retList = new List<Level>();
            getChildLevels(h, retList, ignoreSharedChildren);
            return retList;
        }

        public void getChildLevels(Hierarchy h, List<Level> list)
        {
            getChildLevels(h, list, false);
        }


        public void getChildLevels(Hierarchy h, List<Level> list, bool ignoreSharedChildren)
        {
            if (Children != null && Children.Length > 0)
            {
                foreach (Level l in Children)
                {
                    list.Add(l);
                    l.getChildLevels(h, list);
                }
            }
            if (!ignoreSharedChildren &&
                SharedChildren != null && SharedChildren.Length > 0)
            {
                foreach (String s in SharedChildren)
                {
                    Level l = h.findLevel(s);
                    if (l != null)
                    {
                        if (!list.Contains(l))
                            list.Add(l);
                        l.getChildLevels(h, list);
                    }
                }
            }
        }

        public Level findLevel(Hierarchy h, string levelName)
        {
            return findLevel(h, levelName, false);
        }

        public Level findChildLevel(Hierarchy h, string levelName, bool ignoreSharedLevels)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                if (l.Name.Equals(levelName))
                {
                    return l;
                }
            }
            if (!ignoreSharedLevels &&
                SharedChildren != null && SharedChildren.Length > 0)
            {
                //Iterate over shared levels as well
                foreach (string slname in SharedChildren)
                {
                    Level sl = h.findLevel(slname);
                    if (slname == levelName)
                    {
                        return sl;
                    }
                }
            }
            return retLevel;
        }

        public Level findLevel(Hierarchy h, string levelName, bool ignoreSharedLevels)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                if (l.Name.Equals(levelName))
                {
                    return l;
                }
                else
                {
                    retLevel = l.findLevel(h, levelName, ignoreSharedLevels);
                    if (retLevel != null)
                        return (retLevel);
                }
            }
            if (!ignoreSharedLevels &&
                SharedChildren != null && SharedChildren.Length > 0)
            {
                //Iterate over shared levels as well
                foreach (string slname in SharedChildren)
                {
                    Level sl = h.findLevel(slname);
                    if (sl != null)
                    {
                        if (slname == levelName)
                        {
                            return sl;
                        }
                        else
                        {
                            retLevel = sl.findLevel(h, levelName, ignoreSharedLevels);
                            if (retLevel != null)
                            {
                                return (retLevel);
                            }
                        }
                    }
                }
            }
            return retLevel;
        }

        public int findIndexOf(string[] input, string str)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].Equals(str))
                {
                    return i;
                }
            }
            return -1;
        }

        // Search the children of this level for the given name. 
        // returns true if and only if the given level name is the direct child of this level.
        public bool isChildLevel(string levelName)
        {
            if (SharedChildren != null && SharedChildren.Length > 0)
            {
                if (findIndexOf(SharedChildren, levelName) >= 0)
                {
                    return true;
                }
            }
            foreach (Level l in Children)
            {
                if (l.Name.Equals(levelName))
                {
                    return true;
                }
            }
            return false;
        }


        public Level findLevelWithColumn(string colName)
        {
            Level retLevel = null;
            foreach (string s in ColumnNames)
            {
                if (s == colName)
                    return (this);
            }
            foreach (Level l in Children)
            {
                retLevel = l.findLevelWithColumn(colName);
                if (retLevel != null)
                    return (retLevel);
            }
            return retLevel;
        }

        public Level findLevelWithKey(LevelKey lk)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                foreach (LevelKey ck in l.Keys)
                {
                    bool found = true;
                    foreach (string s in lk.ColumnNames)
                    {
                        if (Array.IndexOf<string>(ck.ColumnNames, s) < 0)
                        {
                            found = false;
                            break;
                        }
                    }
                    if (found)
                    {
                        return (l);
                    }
                }
                retLevel = l.findLevelWithKey(lk);
                if (retLevel != null)
                {
                    return retLevel;
                }
            }
            return retLevel;
        }

        public Level findLevelWithKeyColumn(string s)
        {
            Level retLevel = null;
            foreach (Level l in Children)
            {
                foreach (LevelKey ck in l.Keys)
                {
                    if (Array.IndexOf<string>(ck.ColumnNames, s) >= 0)
                        return (l);
                }
                retLevel = l.findLevelWithKeyColumn(s);
                if (retLevel != null)
                    return retLevel;
            }
            return retLevel;
        }

        public LevelKey getSurrogateKey()
        {
            foreach (LevelKey lk in Keys)
            {
                if (lk.SurrogateKey)
                    return (lk);
            }
            return (null);
        }

        public LevelKey getNaturalKey()
        {
            foreach (LevelKey lk in Keys)
            {
                if (!lk.SurrogateKey)
                    return (lk);
            }
            return (null);
        }

        public void addSurrogateKey(Hierarchy h, string surrogateKeyColumnName)
        {
            List<LevelKey> keyList = new List<LevelKey>(Keys);
            int index = 0;
            LevelKey lk = null;
            for (; index < keyList.Count; index++)
            {
                lk = keyList[index];
                if (lk.ColumnNames.Length == 1 && lk.ColumnNames[0] == surrogateKeyColumnName)
                {
                    break;
                }
            }
            // If it exists, but isn't primary, make it so
            if (index < keyList.Count && index >= 0)
            {
                lk = keyList[index];
                keyList.RemoveAt(index);
                keyList.Insert(0, lk);
                lk.SurrogateKey = true;
            }
            else
            {
                // Otherwise add it as primary
                lk = new LevelKey();
                lk.ColumnNames = new string[1];
                lk.ColumnNames[0] = surrogateKeyColumnName;
                keyList.Insert(0, lk);
                lk.SurrogateKey = true;
            }
            // Remove any other surrogate keys that may exist
            LevelKey elk = null;
            foreach (LevelKey slk in keyList)
            {
                if (slk.SurrogateKey && slk != lk)
                {
                    elk = slk;
                    break;
                }
            }
            if (elk != null)
                keyList.Remove(elk);
            Keys = keyList.ToArray();
            // Make sure it's in the columns too
            bool found = false;
            foreach (string s in ColumnNames)
            {
                if (surrogateKeyColumnName == s)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                List<string> newlist = new List<string>(ColumnNames);
                newlist.Add(surrogateKeyColumnName);
                ColumnNames = newlist.ToArray();
                List<bool> newHidden = null;
                if (HiddenColumns != null)
                    newHidden = new List<bool>(HiddenColumns);
                else
                    newHidden = new List<bool>();
                newHidden.Add(true);
                HiddenColumns = newHidden.ToArray();
            }
            // Make sure it isn't a column in any other level
            List<Level> childList = new List<Level>();
            h.getChildLevels(childList);
            foreach (Level cl in childList)
            {
                if (cl == this)
                    continue;
                found = false;
                int count = 0;
                foreach (string s in cl.ColumnNames)
                {
                    if (surrogateKeyColumnName == s)
                    {
                        found = true;
                        break;
                    }
                    count++;
                }
                if (found)
                {
                    List<string> newlist = new List<string>(cl.ColumnNames);
                    newlist.Remove(surrogateKeyColumnName);
                    cl.ColumnNames = newlist.ToArray();
                    List<bool> newHidden = new List<bool>(cl.HiddenColumns);
                    newHidden.RemoveAt(count);
                    cl.HiddenColumns = newHidden.ToArray();
                    elk = null;
                    foreach (LevelKey slk in cl.Keys)
                    {
                        if (slk.SurrogateKey && slk.ColumnNames[0] == surrogateKeyColumnName)
                        {
                            elk = slk;
                            break;
                        }
                    }
                    if (elk != null)
                    {
                        keyList = new List<LevelKey>(cl.Keys);
                        keyList.Remove(elk);
                        cl.Keys = keyList.ToArray();
                    }
                }
            }
        }

        public override string ToString()
        {
            return (Name);
        }

        public bool containsDimensionColumn(string colName, bool isCheckFloatingColumnsToMap)
        {
            if (ColumnNames != null)
            {
                foreach (string columnName in ColumnNames)
                {
                    if (columnName == colName)
                    {
                        return true;
                    }
                }
            }
            if (isCheckFloatingColumnsToMap && FloatingColumnsToMap != null)
            {
                foreach (string columnName in FloatingColumnsToMap)
                {
                    if (columnName == colName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Dictionary<string, bool> getLevelKeyColumns()
        {
            Dictionary<string, bool> levelKeyColumns = new Dictionary<string, bool>();
            if (Keys != null)
            {
                foreach (LevelKey lk in Keys)
                {
                    if (lk.ColumnNames != null)
                    {
                        foreach (string colName in lk.ColumnNames)
                        {
                            if (!levelKeyColumns.ContainsKey(colName))
                            {
                                levelKeyColumns.Add(colName, lk.SurrogateKey);
                            }
                        }
                    }
                }
            }
            return levelKeyColumns;
        }

        public Level getLowestGeneratedLevel()
        {
            foreach (Level l in Children)
            {
                Level cl = l.getLowestGeneratedLevel();
                if (cl != null)
                    return (cl);
            }
            if (GenerateDimensionTable)
                return (this);
            return (null);
        }
    }

    [Serializable]
    public class LevelAlias : BaseObject
    {
        public static int ATTRIBUTE_ONLY = 0;
        public static int MEASURE_ONLY = 1;
        public string AliasName;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int AliasType; // 0 - AttributeOnly, 1 - MeasureOnly
        public AliasLevelKey[] AliasKeys;

        [Serializable]
        public class AliasLevelKey : BaseObject
        {
            public string LevelKeyColumn;
            public string AliasKeyColumn;

            public AliasLevelKey()
            {
            }

            public override bool Equals(Object obj)
            {
                if (obj == null || obj is System.DBNull) return false;
                if (!typeof(AliasLevelKey).Equals(obj.GetType())) return false;
                AliasLevelKey other = (AliasLevelKey)obj;
                if (LevelKeyColumn != other.LevelKeyColumn) return false;
                if (AliasKeyColumn != other.AliasKeyColumn) return false;
                return true;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public string getDisplayString()
            {
                return "[" + LevelKeyColumn + ":" + AliasKeyColumn + "]";
            }
        }

        public LevelAlias()
        {
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || obj is System.DBNull) return false;
            if (!typeof(LevelAlias).Equals(obj.GetType())) return false;
            LevelAlias other = (LevelAlias)obj;
            if (AliasName != other.AliasName) return false;
            if (AliasType != other.AliasType) return false;
            if (!Repository.EqualsArray(AliasKeys, other.AliasKeys)) return false;
            return true;
        }

        public string getDisplayString()
        {
            StringBuilder sb = new StringBuilder(AliasName + (AliasType == 0 ? "Attribute" : "Measure") + " - ");
            for (int i = 0; i < AliasKeys.Length; i++)
                sb.Append((i > 0 ? "," : "") + AliasKeys[i].getDisplayString());
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class LevelKey : BaseObject
    {
        public bool SurrogateKey;
        public string[] ColumnNames;

        public LevelKey()
        {
        }

        public LevelKey(string columnName)
        {
            ColumnNames = new string[] { columnName };
        }

        public LevelKey(string[] columnNames)
        {
            ColumnNames = columnNames;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || obj is System.DBNull) return false;
            if (!typeof(LevelKey).Equals(obj.GetType())) return false;
            LevelKey other = (LevelKey)obj;
            if (SurrogateKey != other.SurrogateKey) return false;
            if (!Repository.EqualsArray(ColumnNames, other.ColumnNames)) return false;
            return true;
        }

        public string getDisplayString()
        {
            StringBuilder result = new StringBuilder((SurrogateKey ? "Surrogate" : "Natural") + " Key: [");
            for (int i = 0; i < ColumnNames.Length; i++)
            {
                if (i > 0)
                    result.Append(',');
                result.Append(ColumnNames[i]);
            }
            result.Append(']');
            return result.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            string name = "";
            foreach (string s in ColumnNames)
            {
                if (name.Length > 0) name += ',';
                name += s;
            }
            return (name);
        }
    }

    [Serializable]
    public class Aggregate : BaseAuditableObject
    {
        public bool Query;
        public string[] Measures;
        public AggColumn[] Columns;
        public string[] Filters;
        public string LogicalQuery;
        public string IncrementalFilter;
        public string BuildFilter;
        public AggTransformation[] Transformations;
        public string[] LoadGroups;
        public bool OuterJoin = false;
        public bool Disabled = false;
        public bool InMemory = false;
        public string Connection;
        public string[] SubGroups;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Aggregate).Equals(obj.GetType())) return false;
            Aggregate other = (Aggregate)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (Query != other.Query) return false;
            if (!Repository.EqualsArray(Measures, other.Measures)) return false;
            if (!Repository.EqualsArray(Columns, other.Columns)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (!Repository.EqualsWithNull(LogicalQuery, other.LogicalQuery)) return false;
            if (OuterJoin != other.OuterJoin) return false;
            if (Disabled != other.Disabled) return false;
            if (InMemory != other.InMemory) return false;
            if (!Repository.EqualsWithNull(Connection, other.Connection)) return false;
            if (!Repository.EqualsArray(SubGroups, other.SubGroups)) return false;
            if (!Repository.EqualsWithNull(IncrementalFilter, other.IncrementalFilter)) return false;
            if (!Repository.EqualsWithNull(BuildFilter, other.BuildFilter)) return false;
            return true;
        }

        private string getAggColumnString(AggColumn[] columns)
        {
            if (columns == null)
                return null;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < columns.Length; i++)
            {
                if (i > 0)
                    sb.Append(',');
                sb.Append(columns[i].getDisplayString());
            }
            return sb.ToString();
        }

        public List<DiffObject> diffDescription(Aggregate other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(Aggregate).Equals(other.GetType())) return null;

            if (!Repository.EqualsWithNull(Name, other.Name))
                differences.Add(new DiffObject("Name", Name, other.Name));
            if (Query != other.Query)
                differences.Add(new DiffObject("Query", Query, other.Query));
            if (!Repository.EqualsArray(Measures, other.Measures))
                differences.Add(new DiffObject("Measures", Util.getDisplayString(Measures), Util.getDisplayString(other.Measures)));
            if (!Repository.EqualsArray(Columns, other.Columns))
                differences.Add(new DiffObject("Columns", getAggColumnString(Columns), getAggColumnString(other.Columns)));
            if (!Repository.EqualsArray(Filters, other.Filters))
                differences.Add(new DiffObject("Filters", Util.getDisplayString(Filters), Util.getDisplayString(other.Filters)));
            if (!Repository.EqualsWithNull(LogicalQuery, other.LogicalQuery))
                differences.Add(new DiffObject("Logical Query", LogicalQuery, other.LogicalQuery));
            if (OuterJoin != other.OuterJoin)
                differences.Add(new DiffObject("OuterJoin", OuterJoin, other.OuterJoin));
            if (Disabled != other.Disabled)
                differences.Add(new DiffObject("Disabled", Disabled, other.Disabled));
            if (InMemory != other.InMemory)
                differences.Add(new DiffObject("InMemory", InMemory, other.InMemory));
            if (!Repository.EqualsWithNull(Connection, other.Connection))
                differences.Add(new DiffObject("Connection", Connection, other.Connection));
            if (!Repository.EqualsArray(SubGroups, other.SubGroups))
                differences.Add(new DiffObject("SubGroups", Util.getDisplayString(SubGroups), Util.getDisplayString(other.SubGroups)));
            if (!Repository.EqualsWithNull(IncrementalFilter, other.IncrementalFilter))
                differences.Add(new DiffObject("IncrementalFilter", Name, other.IncrementalFilter));
            if (!Repository.EqualsWithNull(BuildFilter, other.BuildFilter))
                differences.Add(new DiffObject("BuildFilter", Name, other.BuildFilter));
            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Aggregate" + " [" + Name + "] Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public void getDifferences(Aggregate currentagg, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            // Object level differences
            List<DiffObject> diffs = diffDescription(currentagg);
            if (diffs.Count > 0)
            {
                itemnames.Add(Name);
                itempaths.Add("Aggregate|" + Name);
                itemtypes.Add("Aggregate");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string toAuditString()
        {
            string ret = "Aggregate ";
            ret += Name;
            ret += " ";
            ret += Guid;
            ret += " Query=";
            ret += Query ? Boolean.TrueString : Boolean.FalseString;
            if (Measures != null && Measures.Length > 0)
            {
                ret += " Measures{";
                foreach (string m in Measures)
                {
                    ret += "[";
                    ret += m;
                    ret += "]";
                }
                ret += "}";
            }
            if (Columns != null && Columns.Length > 0)
            {
                ret += " Columns{";
                foreach (AggColumn ac in Columns)
                {
                    ret += "[";
                    ret += ac.toAuditString();
                    ret += "]";
                }
                ret += "}";
            }
            if (Filters != null && Filters.Length > 0)
            {
                ret += " Filters{";
                foreach (string s in Filters)
                {
                    ret += s;
                }
                ret += "}";
            }
            if (LogicalQuery != null && LogicalQuery.Length > 0)
            {
                ret += " LogicalQuery=[";
                ret += LogicalQuery;
                ret += "]";
            }
            if (IncrementalFilter != null && IncrementalFilter.Length > 0)
            {
                ret += " IncrementalFilter=[";
                ret += IncrementalFilter;
                ret += "]";
            }
            if (BuildFilter != null && BuildFilter.Length > 0)
            {
                ret += " BuildFilter=[";
                ret += BuildFilter;
                ret += "]";
            }
            if (Transformations != null && Transformations.Length > 0)
            {
                ret += " Transformations{";
                foreach (AggTransformation at in Transformations)
                {
                    ret += "[";
                    ret += at.toAuditString();
                    ret += "]";
                }
                ret += "}";
            }
            if (LoadGroups != null && LoadGroups.Length > 0)
            {
                ret += " LoadGroups{";
                foreach (string s in LoadGroups)
                {
                    ret += "[";
                    ret += s;
                    ret += "]";
                }
                ret += "}";
            }
            ret += " OuterJoin=";
            ret += OuterJoin ? Boolean.TrueString : Boolean.FalseString;

            ret += " Disabled=";
            ret += Disabled ? Boolean.TrueString : Boolean.FalseString;

            ret += " InMemory=";
            ret += InMemory ? Boolean.TrueString : Boolean.FalseString;

            ret += " Connection=";
            ret += Connection;

            if (SubGroups != null && SubGroups.Length > 0)
            {
                ret += " SubGroups{";
                foreach (string s in SubGroups)
                {
                    ret += "[";
                    ret += s;
                    ret += "]";
                }
                ret += "}";
            }
            return ret;
        }
    }

    [Serializable]
    public class AggTransformation
    {
        public string Dimension;
        public string ColumnName;
        [XmlArrayItem(Type = typeof(Procedure)), XmlArrayItem(Type = typeof(Expression)), XmlArrayItem(Type = typeof(LogicalColumnDefinition)), XmlArrayItem(Type = typeof(TableLookup)), XmlArrayItem(Type = typeof(Rank))]
        public Transformation[] Transformations;

        internal string toAuditString()
        {
            string ret = "Dimension=";
            ret += Dimension;
            ret += " ColumnName=";
            ret += ColumnName;
            if (Transformations != null && Transformations.Length > 0)
            {
                ret += " Transformations={";
                foreach (Transformation t in Transformations)
                {
                    ret += "[";
                    ret += t.toAuditString();
                    ret += "]";
                }
                ret += "}";
            }
            return ret;
        }
    }

    [Serializable]
    public class AggColumn : BaseObject
    {
        public string Dimension;
        public string Column;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(AggColumn).Equals(obj.GetType())) return false;
            AggColumn other = (AggColumn)obj;
            if (!Repository.EqualsWithNull(Dimension, other.Dimension)) return false;
            if (!Repository.EqualsWithNull(Column, other.Column)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string getDisplayString()
        {
            return ("[" + Dimension + "." + Column + "]");
        }

        internal string toAuditString()
        {
            return "Dimension=" + Dimension + " Column=" + Column;
        }
    }

    [Serializable]
    public class DatabaseConnection : BaseObject
    {
        public string Name = "New Connection";
        public string Type;
        public string Driver;
        public string ConnectString;
        public string UserName;
        public string Password;
        public string Schema;
        public string Database;
        public string DBTimeZone;
        public int ConnectionPoolSize = 2;
        public int MaxConnectionThreads = 1;
        public bool UnicodeDatabase = false;
        public string ID;
        public bool Realtime = false;
        public string VisibleName;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool useDirectConnection;
        public bool useCompression = false;
        public string[] PartitionConnectionNames;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IsPartitioned = false;
        public bool EqualsSpecial(Object obj, bool ignoreIDAndSchemaCheck)
        {
            if (obj == null) return false;
            if (!typeof(DatabaseConnection).Equals(obj.GetType())) return false;
            DatabaseConnection other = (DatabaseConnection)obj;
            if (Name != other.Name) return false;
            if (Type != other.Type) return false;
            if (Driver != other.Driver) return false;
            if (ConnectString != other.ConnectString) return false;
            if (UserName != other.UserName) return false;
            if (Password != other.Password) return false;
            if (ConnectionPoolSize != other.ConnectionPoolSize) return false;
            if (MaxConnectionThreads != other.MaxConnectionThreads) return false;
            if (UnicodeDatabase != other.UnicodeDatabase) return false;
            if (Realtime != other.Realtime) return false;
            if (useDirectConnection != other.useDirectConnection) return false;
            if (useCompression != other.useCompression) return false;
            if (VisibleName != other.VisibleName) return false;
            if (!ignoreIDAndSchemaCheck)
            {
                if (Schema != other.Schema) return false;
                if (ID != other.ID) return false;
            }
            if (!Repository.EqualsArray(PartitionConnectionNames, other.PartitionConnectionNames)) return false;
            if (IsPartitioned != other.IsPartitioned) return false;
            if (DBTimeZone != other.DBTimeZone) return false;
            return true;
        }

        public override bool Equals(Object obj)
        {
            return EqualsSpecial(obj, false);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class WarehousePartition : BaseObject
    {
        public string Name;
        public string DatabaseConnectionName;
        public string SourceName;
        public string[] PartitionColumnNames;
        public string[] ChildPartitionColumnNames;
        public string PrimarySourceName;

        public bool EqualsSpecial(Object obj, bool ignoreIDAndSchemaCheck)
        {
            if (obj == null) return false;
            if (!typeof(WarehousePartition).Equals(obj.GetType())) return false;
            WarehousePartition other = (WarehousePartition)obj;
            if (Name != other.Name) return false;
            if (DatabaseConnectionName != other.DatabaseConnectionName) return false;
            if (SourceName != other.SourceName) return false;
            if (!Repository.EqualsArray(PartitionColumnNames, other.PartitionColumnNames)) return false;
            if (!Repository.EqualsArray(ChildPartitionColumnNames, other.ChildPartitionColumnNames)) return false;
            if (PrimarySourceName != other.PrimarySourceName) return false;
            return true;
        }

        public override bool Equals(Object obj)
        {
            return EqualsSpecial(obj, false);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PartitionConnectionLoadInfo : BaseObject
    {
        public string PartitionConnectionName;
        public string StagingLoadDir;
        public string DatabaseLoadDir;
        public string LoadMachine;

        public bool EqualsSpecial(Object obj, bool ignoreIDAndSchemaCheck)
        {
            if (obj == null) return false;
            if (!typeof(PartitionConnectionLoadInfo).Equals(obj.GetType())) return false;
            PartitionConnectionLoadInfo other = (PartitionConnectionLoadInfo)obj;
            if (PartitionConnectionName != other.PartitionConnectionName) return false;
            if (StagingLoadDir != other.StagingLoadDir) return false;
            if (DatabaseLoadDir != other.DatabaseLoadDir) return false;
            if (LoadMachine != other.LoadMachine) return false;
            return true;
        }

        public override bool Equals(Object obj)
        {
            return EqualsSpecial(obj, false);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class BuildProperties : BaseObject
    {
        public string postBuildReminder;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(BuildProperties).Equals(obj.GetType())) return false;
            BuildProperties other = (BuildProperties)obj;
            if (!Repository.EqualsWithNull(this.postBuildReminder, other.postBuildReminder)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ModelParameters : BaseObject
    {
        public int MaxComputationThreads;
        public string BulkLoadDirectory;
        public int MaxClustersPerPattern;
        public int MaxOneAttributeCategories;
        public bool ClusterDensity;
        public int SampleSize;
        public double MinSamplesPerAttribute;
        public int MaxAttributesPerModel;
        public double TargetReductionRate;
        public int MaxAttributesPerFold;
        public ClassifierSettings Classifiers;
        public bool ModelBasedFiltering;
        public bool MapNullsToZero;
        public int MaxInstances;
        public bool UseOnlyExplanableClassificationModels;
        public double TargetCorrelationThreshold;
        public double CrossCorrelationThreshold;
        public bool UseCrossCorrelation = true;
        public bool UseWekaRemoveUseless = true;
        public bool EnableAttributeSelection = true;
        public bool UseRemoveRelated = true;
        public int MaxARFFInstances;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ModelParameters).Equals(obj.GetType())) return false;
            ModelParameters other = (ModelParameters)obj;
            if (MaxComputationThreads != other.MaxComputationThreads) return false;
            if (!Repository.EqualsWithNull(BulkLoadDirectory, other.BulkLoadDirectory)) return false;
            if (MaxClustersPerPattern != other.MaxClustersPerPattern) return false;
            if (MaxOneAttributeCategories != other.MaxOneAttributeCategories) return false;
            if (ClusterDensity != other.ClusterDensity) return false;
            if (SampleSize != other.SampleSize) return false;
            if (MinSamplesPerAttribute != other.MinSamplesPerAttribute) return false;
            if (MaxAttributesPerModel != other.MaxAttributesPerModel) return false;
            if (TargetReductionRate != other.TargetReductionRate) return false;
            if (!Repository.EqualsWithNull(Classifiers, other.Classifiers)) return false;
            if (ModelBasedFiltering != other.ModelBasedFiltering) return false;
            if (MapNullsToZero != other.MapNullsToZero) return false;
            if (TargetCorrelationThreshold != other.TargetCorrelationThreshold) return false;
            if (CrossCorrelationThreshold != other.CrossCorrelationThreshold) return false;
            if (MaxInstances != other.MaxInstances) return false;
            if (UseOnlyExplanableClassificationModels != other.UseOnlyExplanableClassificationModels) return false;
            if (UseWekaRemoveUseless != other.UseWekaRemoveUseless) return false;
            if (UseCrossCorrelation != other.UseCrossCorrelation) return false;
            if (EnableAttributeSelection != other.EnableAttributeSelection) return false;
            if (UseRemoveRelated != other.UseRemoveRelated) return false;
            if (MaxARFFInstances != other.MaxARFFInstances) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ClassifierSettings : BaseObject
    {
        public int[] RegressionModels;
        public int[] ClassificationModels;
        public long MaxSampleInstanceValues;
        public long MaxInstanceValues;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ClassifierSettings).Equals(obj.GetType())) return false;
            ClassifierSettings other = (ClassifierSettings)obj;
            if (!Repository.EqualsIntArray(RegressionModels, other.RegressionModels)) return false;
            if (!Repository.EqualsIntArray(ClassificationModels, other.ClassificationModels)) return false;
            if (MaxSampleInstanceValues != other.MaxSampleInstanceValues) return false;
            if (MaxInstanceValues != other.MaxInstanceValues) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class OptimizerParameters : BaseObject
    {
        public int Method; // 0 - Simplex, 1 - QuasiNewton
        public int PopulationSize;
        public int MaxGenerations;
        public int MaxNoChangeGenerations;
        public double MutationRate;
        public double ReproductionRate;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(OptimizerParameters).Equals(obj.GetType())) return false;
            OptimizerParameters other = (OptimizerParameters)obj;
            if (Method != other.Method) return false;
            if (PopulationSize != other.PopulationSize) return false;
            if (MaxGenerations != other.MaxGenerations) return false;
            if (MaxNoChangeGenerations != other.MaxNoChangeGenerations) return false;
            if (MutationRate != other.MutationRate) return false;
            if (ReproductionRate != other.ReproductionRate) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PerformanceMeasure : BaseObject
    {
        public string Name;
        public int Bucket;
        public string BlockGroup;
        public string CoverGroup;
        public int NumInCoverGroup;
        public bool ModelFlag;
        public bool BaseCaseMeasure;
        public bool Optimize;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(PerformanceMeasure).Equals(obj.GetType())) return false;
            PerformanceMeasure other = (PerformanceMeasure)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (Bucket != other.Bucket) return false;
            if (!Repository.EqualsWithNull(BlockGroup, other.BlockGroup)) return false;
            if (!Repository.EqualsWithNull(CoverGroup, other.CoverGroup)) return false;
            if (NumInCoverGroup != other.NumInCoverGroup) return false;
            if (ModelFlag != other.ModelFlag) return false;
            if (BaseCaseMeasure != other.BaseCaseMeasure) return false;
            if (Optimize != other.Optimize) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PerformanceModel : BaseObject
    {
        public string Name;
        public string ModelFile;
        public string ReferencePopulation;
        public string OptimizationGoal;
        public string[] PerformanceFilters;
        public string PerformanceOutputTable;
        public bool DropExistingTable;
        public double PerformanceImpactPercent = 5;
        public bool PerformanceLogRelationship = false;
        public PerformanceMeasure[] Measures;
        public string PerformanceIterationDimension;
        public string PerformanceIterationDefaultAttribute;
        public string PerformanceIterationModelValue;
        public string PerformanceIterationAnalysisValue;
        public int NumMeasuresToOptimize;
        public int TargetType; // 0 - Improvement, 1 - Probability
        public double TargetImprovement;
        public double TargetProbability;
        public double TargetMinimum;
        public double MinTargetImprovement;
        public double NumIterations;
        public double TargetAccuracy;
        public double DampeningFactor;
        public bool Optimize;
        public bool SearchPatterns;
        public string SearchPatternDefinition;
        public ClassifierSettings Classifiers;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(PerformanceModel).Equals(obj.GetType())) return false;
            PerformanceModel other = (PerformanceModel)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(ModelFile, other.ModelFile)) return false;
            if (!Repository.EqualsWithNull(OptimizationGoal, other.OptimizationGoal)) return false;
            if (!Repository.EqualsWithNull(ReferencePopulation, other.ReferencePopulation)) return false;
            if (!Repository.EqualsArray(PerformanceFilters, other.PerformanceFilters)) return false;
            if (!Repository.EqualsWithNull(PerformanceOutputTable, other.PerformanceOutputTable)) return false;
            if (DropExistingTable != other.DropExistingTable) return false;
            if (PerformanceImpactPercent != other.PerformanceImpactPercent) return false;
            if (PerformanceLogRelationship != other.PerformanceLogRelationship) return false;
            if (!Repository.EqualsArray(Measures, other.Measures)) return false;
            if (!Repository.EqualsWithNull(PerformanceIterationDimension, other.PerformanceIterationDimension)) return false;
            if (!Repository.EqualsWithNull(PerformanceIterationDefaultAttribute, other.PerformanceIterationDefaultAttribute)) return false;
            if (!Repository.EqualsWithNull(PerformanceIterationAnalysisValue, other.PerformanceIterationAnalysisValue)) return false;
            if (NumMeasuresToOptimize != other.NumMeasuresToOptimize) return false;
            if (TargetType != other.TargetType) return false;
            if (TargetImprovement != other.TargetImprovement) return false;
            if (TargetProbability != other.TargetProbability) return false;
            if (TargetMinimum != other.TargetMinimum) return false;
            if (MinTargetImprovement != other.MinTargetImprovement) return false;
            if (NumIterations != other.NumIterations) return false;
            if (TargetAccuracy != other.TargetAccuracy) return false;
            if (DampeningFactor != other.DampeningFactor) return false;
            if (Optimize != other.Optimize) return false;
            if (SearchPatterns != other.SearchPatterns) return false;
            if (!Repository.EqualsWithNull(SearchPatternDefinition, other.SearchPatternDefinition)) return false;
            if (!Repository.EqualsWithNull(Classifiers, other.Classifiers)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class SubjectArea : BaseObject
    {
        public string Name;
        public Folder[] Folders;
        public string[] Groups;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(SubjectArea).Equals(obj.GetType())) return false;
            SubjectArea other = (SubjectArea)obj;
            if (Name != other.Name) return false;
            if (!Repository.EqualsArray(Folders, other.Folders)) return false;
            if (!Repository.EqualsArray(Groups, other.Groups)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Folder : BaseObject
    {
        public string Name;
        public Folder[] Subfolders;
        public Column[] Columns;
        public string[] Groups;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Folder).Equals(obj.GetType())) return false;
            Folder other = (Folder)obj;
            if (Name != other.Name) return false;
            if (!Repository.EqualsArray(Subfolders, other.Subfolders)) return false;
            if (!Repository.EqualsArray(Columns, other.Columns)) return false;
            if (!Repository.EqualsArray(Groups, other.Groups)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Column : BaseObject
    {
        public string Dimension;
        public string Name;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Column).Equals(obj.GetType())) return false;
            Column other = (Column)obj;
            if (Dimension != other.Dimension) return false;
            if (Name != other.Name) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class PatternSearchDefinition : BaseObject
    {
        public string Name;
        public string SuccessModelName;
        public string OutputTableName;
        public string ReferencePopulation;
        public string IterationAnalysisValue;
        public int PatternSearchType; // 0 - Peer, 1 - Population, 2 - Split
        public int NumPatternsToSave;
        public int NumPatternsPerMeasure;
        public int NumPeersToCompare;
        public bool DropExistingTable;
        public bool IngoreZeroWeights;  // sigh :)
        public bool GenerateSegments;
        public string[] SegmentMeasures;
        public string TargetDimension;
        public string TargetLevel;
        public string JoinMeasure;
        public int NumTargetSegmentsToSave;
        public bool ScreenDuplicateMeasures;
        public bool ProcessTargetsSeparately;
        public string SegmentOutputTableName;
        public bool DropExistingSegmentOutputTable;
        public string[] ReferenceFilters;
        public string[] ComparisonFilters;
        public double SplitPercentage;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(PatternSearchDefinition).Equals(obj.GetType())) return false;
            PatternSearchDefinition other = (PatternSearchDefinition)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(SuccessModelName, other.SuccessModelName)) return false;
            if (!Repository.EqualsWithNull(OutputTableName, other.OutputTableName)) return false;
            if (!Repository.EqualsWithNull(ReferencePopulation, other.ReferencePopulation)) return false;
            if (!Repository.EqualsWithNull(IterationAnalysisValue, other.IterationAnalysisValue)) return false;
            if (PatternSearchType != other.PatternSearchType) return false;
            if (NumPatternsToSave != other.NumPatternsToSave) return false;
            if (NumPatternsPerMeasure != other.NumPatternsPerMeasure) return false;
            if (NumPeersToCompare != other.NumPeersToCompare) return false;
            if (DropExistingTable != other.DropExistingTable) return false;
            if (IngoreZeroWeights != other.IngoreZeroWeights) return false;
            if (GenerateSegments != other.GenerateSegments) return false;
            if (!Repository.EqualsArray(SegmentMeasures, other.SegmentMeasures)) return false;
            if (!Repository.EqualsWithNull(TargetDimension, other.TargetDimension)) return false;
            if (!Repository.EqualsWithNull(TargetLevel, other.TargetLevel)) return false;
            if (!Repository.EqualsWithNull(JoinMeasure, other.JoinMeasure)) return false;
            if (NumTargetSegmentsToSave != other.NumTargetSegmentsToSave) return false;
            if (ScreenDuplicateMeasures != other.ScreenDuplicateMeasures) return false;
            if (ProcessTargetsSeparately != other.ProcessTargetsSeparately) return false;
            if (!Repository.EqualsWithNull(SegmentOutputTableName, other.SegmentOutputTableName)) return false;
            if (DropExistingSegmentOutputTable != other.DropExistingSegmentOutputTable) return false;
            if (!Repository.EqualsArray(ReferenceFilters, other.ReferenceFilters)) return false;
            if (!Repository.EqualsArray(ComparisonFilters, other.ComparisonFilters)) return false;
            if (SplitPercentage != other.SplitPercentage) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Group : BaseObject
    {
        public string Id;
        public string Name;
        public string[] Usernames;
        public string[] Filters;
        public bool InternalGroup = false;

        public bool hasUser(string username)
        {
            if (Usernames == null)
                return false;
            foreach (string s in Usernames)
                if (s.ToLower() == username.ToLower())
                    return true;
            return false;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Group).Equals(obj.GetType())) return false;
            Group other = (Group)obj;
            if (Name != other.Name) return false;
            if (Id != other.Id) return false;
            if (!Repository.EqualsArray(Usernames, other.Usernames)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (InternalGroup != other.InternalGroup) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class User : BaseObject
    {
        public string Username;
        public string Password;
        public string Fullname;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(User).Equals(obj.GetType())) return false;
            User other = (User)obj;
            if (!Repository.EqualsWithNull(Username, other.Username)) return false;
            if (!Repository.EqualsWithNull(Password, other.Password)) return false;
            if (!Repository.EqualsWithNull(Fullname, other.Fullname)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public User clone()
        {
            User u = new User();
            u.Username = Username;
            u.Password = Password;
            u.Fullname = Fullname;
            return (u);
        }
    }

    [Serializable]
    public class Variable : BaseAuditableObject
    {
        public enum VariableType { Session, Repository, Warehouse };
        public VariableType Type;
        public bool Session;
        public bool MultiValue;
        public bool Constant;
        public bool MultipleColumns;
        public bool LogicalQuery;
        public bool PhysicalQuery;
        public string Query;
        public string Connection;
        public string DefaultIfInvalid;
        public bool Cacheable;
        public bool EvaluateOnDemand;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool BirstCanModify;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Variable).Equals(obj.GetType())) return false;
            Variable other = (Variable)obj;
            if (Name != other.Name) return false;
            if (Type != other.Type) return false;
            if (Session != other.Session) return false;
            if (MultiValue != other.MultiValue) return false;
            if (Constant != other.Constant) return false;
            if (MultipleColumns != other.MultipleColumns) return false;
            if (LogicalQuery != other.LogicalQuery) return false;
            if (PhysicalQuery != other.PhysicalQuery) return false;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(Query, other.Query)) return false;
            if (!Repository.EqualsWithNull(Connection, other.Connection)) return false;
            if (!Repository.EqualsWithNull(DefaultIfInvalid, other.DefaultIfInvalid)) return false;
            if (Cacheable != other.Cacheable) return false;
            if (EvaluateOnDemand != other.EvaluateOnDemand) return false;
            if (BirstCanModify != other.BirstCanModify) return false;
            return true;
        }

        public string getVariableTypeString(VariableType type)
        {
            switch (type)
            {
                case VariableType.Repository:
                    return "Repository";
                case VariableType.Session:
                    return "Session";
                case VariableType.Warehouse:
                    return "Warehouse";
            }
            return null;
        }

        public List<DiffObject> diffDescription(Variable other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(Variable).Equals(other.GetType())) return null;

            if (!Repository.EqualsWithNull(Name, other.Name))
                differences.Add(new DiffObject("Name", Name, other.Name));
            if (Type != other.Type)
                differences.Add(new DiffObject("Type", getVariableTypeString(Type), getVariableTypeString(other.Type)));
            if (Session != other.Session)
                differences.Add(new DiffObject("Session", Session, other.Session));
            if (MultiValue != other.MultiValue)
                differences.Add(new DiffObject("MultiValue", MultiValue, other.MultiValue));
            if (Constant != other.Constant)
                differences.Add(new DiffObject("Constant", Constant, other.Constant));
            if (MultipleColumns != other.MultipleColumns)
                differences.Add(new DiffObject("Multiple Columns", MultipleColumns, other.MultipleColumns));
            if (LogicalQuery != other.LogicalQuery)
                differences.Add(new DiffObject("Logical Query", LogicalQuery, other.LogicalQuery));
            if (PhysicalQuery != other.PhysicalQuery)
                differences.Add(new DiffObject("Physical Query", PhysicalQuery, other.PhysicalQuery));
            if (!Repository.EqualsWithNull(Query, other.Query))
                differences.Add(new DiffObject("Query", Query, other.Query));
            if (!Repository.EqualsWithNull(Connection, other.Connection))
                differences.Add(new DiffObject("Connection", Connection, other.Connection));
            if (!Repository.EqualsWithNull(DefaultIfInvalid, other.DefaultIfInvalid))
                differences.Add(new DiffObject("Default If Invalid", DefaultIfInvalid, other.DefaultIfInvalid));
            if (Cacheable != other.Cacheable)
                differences.Add(new DiffObject("Cacheable", Cacheable, other.Cacheable));
            if (EvaluateOnDemand != other.EvaluateOnDemand)
                differences.Add(new DiffObject("EvaluateOnDemand", EvaluateOnDemand, other.EvaluateOnDemand));
            if (BirstCanModify != other.BirstCanModify)
                differences.Add(new DiffObject("Birst Can Modify", BirstCanModify, other.BirstCanModify));

            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Variable" + " [" + Name + "] Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public void getDifferences(Variable currentv, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            // Object level differences
            List<DiffObject> diffs = diffDescription(currentv);
            if (diffs.Count > 0)
            {
                itemnames.Add(Name);
                itempaths.Add("Variable|" + Name);
                itemtypes.Add("Variable");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string toAuditString()
        {
            string ret = "Variable ";
            ret += Name;
            ret += " ";
            ret += Guid;
            ret += " VariableType=";
            if (Type == VariableType.Repository)
                ret += "Repository";
            else if (Type == VariableType.Session)
                ret += "Session";
            else
                ret += "Warehouse";
            ret += " Session=";
            ret += Session ? Boolean.TrueString : Boolean.FalseString;
            ret += " MultiValue=";
            ret += MultiValue ? Boolean.TrueString : Boolean.FalseString;
            ret += " Constant=";
            ret += Constant ? Boolean.TrueString : Boolean.FalseString;
            ret += " MultipleColumns=";
            ret += MultipleColumns ? Boolean.TrueString : Boolean.FalseString;
            ret += " LogicalQuery=";
            ret += LogicalQuery ? Boolean.TrueString : Boolean.FalseString;
            ret += " PhysicalQuery=";
            ret += PhysicalQuery ? Boolean.TrueString : Boolean.FalseString;
            if (Query != null && Query.Length > 0)
            {
                ret += " Query=";
                ret += Query;
            }
            if (Connection != null && Connection.Length > 0)
            {
                ret += " Connection=";
                ret += Connection;
            }
            if (DefaultIfInvalid != null && DefaultIfInvalid.Length > 0)
            {
                ret += " DefaultIfInvalid=";
                ret += DefaultIfInvalid;
            }
            ret += " Cacheable=";
            ret += Cacheable ? Boolean.TrueString : Boolean.FalseString;
            ret += " EvaluateOnDemand=";
            ret += EvaluateOnDemand ? Boolean.TrueString : Boolean.FalseString;

            return ret;
        }
    }

    [Serializable]
    public class Resource : BaseObject
    {
        public string Name;
        public string Type;
        public string Area;
        public string Description;
        public string Value;

        public Resource()
        {
        }

        public Resource(string Name, string Type, string Area, string Description, string Value)
        {
            this.Name = Name;
            this.Type = Type;
            this.Area = Area;
            this.Description = Description;
            this.Value = Value;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Resource).Equals(obj.GetType())) return false;
            Resource other = (Resource)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(Type, other.Type)) return false;
            if (!Repository.EqualsWithNull(Area, other.Area)) return false;
            if (!Repository.EqualsWithNull(Description, other.Description)) return false;
            if (!Repository.EqualsWithNull(Value, other.Value)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ACLItem : BaseObject
    {
        public string Group;
        public string Tag;
        public bool Access;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(ACLItem).Equals(obj.GetType())) return false;
            ACLItem other = (ACLItem)obj;
            if (!Repository.EqualsWithNull(Group, other.Group)) return false;
            if (!Repository.EqualsWithNull(Tag, other.Tag)) return false;
            if (Access != other.Access) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class OutcomeFactor : BaseObject
    {
        public enum SettingType
        {
            Zero, Average, Current, Value, RelativeValue
        }
        public string Measure;
        public string Dimension1;
        public string Attribute1;
        public string Attribute1Value;
        public string Dimension2;
        public string Attribute2;
        public string Attribute2Value;
        public SettingType Base;
        public double BaseValue;
        public SettingType Test;
        public double TestValue;

        public OutcomeFactor()
        {
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(OutcomeFactor).Equals(obj.GetType())) return false;
            OutcomeFactor other = (OutcomeFactor)obj;
            if (!Repository.EqualsWithNull(Measure, other.Measure)) return false;
            if (!Repository.EqualsWithNull(Dimension1, other.Dimension1)) return false;
            if (!Repository.EqualsWithNull(Attribute1, other.Attribute1)) return false;
            if (!Repository.EqualsWithNull(Attribute1Value, other.Attribute1Value)) return false;
            if (!Repository.EqualsWithNull(Dimension2, other.Dimension2)) return false;
            if (!Repository.EqualsWithNull(Attribute2, other.Attribute2)) return false;
            if (!Repository.EqualsWithNull(Attribute2Value, other.Attribute2Value)) return false;
            if (Base != other.Base) return false;
            if (BaseValue != other.BaseValue) return false;
            if (Test != other.Test) return false;
            if (TestValue != other.TestValue) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string ToString(bool test)
        {
            if (test)
                return ((Attribute1Value != null ? Attribute1Value : "") + (Attribute2Value != null ? "," + Attribute2Value + "=" : "=") + TestValue);
            else
                return ((Attribute1Value != null ? Attribute1Value : "") + (Attribute2Value != null ? "," + Attribute2Value + "=" : "=") + BaseValue);
        }

        private void setPattern(Pattern p)
        {
            this.Measure = p.Measure;
            this.Dimension1 = p.Dimension1;
            this.Attribute1 = p.Attribute1;
            this.Dimension2 = p.Dimension2;
            this.Attribute2 = p.Attribute2;
        }

        public OutcomeFactor(Pattern p)
        {
            setPattern(p);
        }

        public OutcomeFactor(Pattern p, double baseMeasure, double testMeasure)
        {
            setPattern(p);
            this.BaseValue = baseMeasure;
            this.TestValue = testMeasure;
        }

        public OutcomeFactor(Pattern p, double baseMeasure, double testMeasure, Object attribute1Value)
        {
            setPattern(p);
            this.Attribute1Value = attribute1Value == null ? "null" : attribute1Value.ToString();
            this.BaseValue = baseMeasure;
            this.TestValue = testMeasure;
        }

        public OutcomeFactor(Pattern p, double baseMeasure, double testMeasure, Object attribute1Value, Object attribute2Value)
        {
            setPattern(p);
            this.Attribute1Value = attribute1Value == null ? "null" : attribute1Value.ToString();
            this.Attribute2Value = attribute2Value == null ? "null" : attribute2Value.ToString();
            this.BaseValue = baseMeasure;
            this.TestValue = testMeasure;
        }

        public Object[] getRow()
        {
            Object[] row = new Object[5];
            row[0] = getName();
            switch (Base)
            {
                case SettingType.Current:
                    row[1] = "Current";
                    break;
                case SettingType.Average:
                    row[1] = "Average";
                    break;
                case SettingType.Zero:
                    row[1] = "Zero";
                    break;
                case SettingType.Value:
                    row[1] = "Specific Value";
                    row[2] = ToString(false);
                    break;
            }
            switch (Test)
            {
                case SettingType.Current:
                    row[3] = "Current";
                    break;
                case SettingType.Average:
                    row[3] = "Average";
                    break;
                case SettingType.Zero:
                    row[3] = "Zero";
                    break;
                case SettingType.Value:
                    row[3] = "Specific Value";
                    row[4] = ToString(true);
                    break;
            }
            return (row);
        }

        public static SettingType getSetting(string name)
        {
            if (name == "Current") return (SettingType.Current);
            else if (name == "Average") return (SettingType.Average);
            else if (name == "Zero") return (SettingType.Zero);
            else if (name == "Specific Value") return (SettingType.Value);
            return (SettingType.Current);
        }

        public string getName()
        {
            if (Dimension2 != null)
                return (Measure + " by " + Dimension1 + "." + Attribute1 + " by " + Dimension2 + "." + Attribute2);
            else if (Dimension1 != null)
                return (Measure + " by " + Dimension1 + "." + Attribute1);
            return (Measure);
        }

        public static string getName(Pattern p)
        {
            if (p.Type == Pattern.MEASUREONLY)
                return (p.Measure);
            if (p.Type == Pattern.ONEATTRIBUTE)
                return (p.Measure + " by " + p.Dimension1 + "." + p.Attribute1);
            if (p.Type == Pattern.TWOATTRIBUTE)
                return (p.Measure + " by " + p.Dimension1 + "." + p.Attribute1 + " by " + p.Dimension2 + "." + p.Attribute2);
            return (null);
        }

        public bool matchesPattern(Pattern p)
        {
            if (p.Type == Pattern.MEASUREONLY)
                return (p.Measure == Measure && Dimension1 == null && Dimension2 == null);
            if (p.Type == Pattern.ONEATTRIBUTE)
                return (p.Measure == Measure && p.Dimension1 == Dimension1 && p.Attribute1 == Attribute1 && Dimension2 == null);
            if (p.Type == Pattern.TWOATTRIBUTE)
                return (p.Measure == Measure && p.Dimension1 == Dimension1 && p.Attribute1 == Attribute1 && p.Dimension2 == Dimension2 && p.Attribute2 == Attribute2);
            return (false);
        }
    }

    [Serializable]
    public class Operation : BaseObject
    {
        public string Name;
        public Parameter[] Parameters;
        [XmlArrayItem(Type = typeof(UpdateStatement)), XmlArrayItem(Type = typeof(InvalidateCache)), XmlArrayItem(Type = typeof(InvalidateDashboardCache)), XmlArrayItem(Type = typeof(Email))]
        public Action[] Actions;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Operation).Equals(obj.GetType())) return false;
            Operation other = (Operation)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsArray(Parameters, other.Parameters)) return false;
            if (!Repository.EqualsArray(Actions, other.Actions)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Parameter : BaseObject
    {
        public string Name;
        public enum DataType { Varchar, Number, Integer, DateTime };
        public enum ParameterType { SingleValue, MultiValue };
        public DataType Datatype;
        public ParameterType Type;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Parameter).Equals(obj.GetType())) return false;
            Parameter other = (Parameter)obj;
            if (Name != other.Name) return false;
            if (Datatype != other.Datatype) return false;
            if (Type != other.Type) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Action : BaseObject
    {
        public virtual string ActionTypeName { get { return ""; } set { } }
        public virtual int ImageIndex { get { return 0; } set { } }
        public string Name;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(Action).Equals(obj.GetType())) return false;
            Action other = (Action)obj;
            if (Name != other.Name) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class UpdateStatement : Action
    {
        public string Statement;
        public bool StoredProcedure;

        #region Action Members
        public override string ActionTypeName
        {
            get
            {
                return ("Update");
            }
            set
            {
            }
        }
        public override int ImageIndex
        {
            get
            {
                return 22;
            }
            set
            {
            }
        }
        #endregion

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            try
            {
                if (!typeof(UpdateStatement).Equals(obj.GetType())) return false;
                UpdateStatement other = (UpdateStatement)obj;
                if (!base.Equals(other)) return false;
                if (Statement != other.Statement) return false;
                if (StoredProcedure != other.StoredProcedure) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class InvalidateCache : Action
    {
        public enum InvalidateType { LogicalQuery, LogicalTable };

        public InvalidateType Type;
        public string Query;
        public string TableName;

        #region Action Members
        public override string ActionTypeName
        {
            get
            {
                return ("Invalidate Query Cache");
            }
            set
            {
            }
        }
        public override int ImageIndex
        {
            get
            {
                return 24;
            }
            set
            {
            }
        }
        #endregion

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            try
            {
                if (!typeof(InvalidateCache).Equals(obj.GetType())) return false;
                InvalidateCache other = (InvalidateCache)obj;
                if (!base.Equals(other)) return false;
                if (Type != other.Type) return false;
                if (Query != other.Query) return false;
                if (TableName != other.TableName) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class InvalidateDashboardCache : Action
    {
        #region Action Members
        public override string ActionTypeName
        {
            get
            {
                return ("Invalidate Dashboard Cache");
            }
            set
            {
            }
        }
        public override int ImageIndex
        {
            get
            {
                return 23;
            }
            set
            {
            }
        }
        #endregion

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(InvalidateDashboardCache).Equals(obj.GetType())) return false;
            InvalidateDashboardCache other = (InvalidateDashboardCache)obj;
            if (!base.Equals(other)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Email : Action
    {
        public string From;
        public string To;
        public string Subject;
        public string Body;
        public string Report;
        public string MailHost;

        #region Action Members
        public override string ActionTypeName
        {
            get
            {
                return ("Email");
            }
            set
            {
            }
        }
        public override int ImageIndex
        {
            get
            {
                return 24;
            }
            set
            {
            }
        }
        #endregion

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            try
            {
                if (!typeof(Email).Equals(obj.GetType())) return false;
                Email other = (Email)obj;
                if (!base.Equals(other)) return false;
                if (!Repository.EqualsWithNull(From, other.From)) return false;
                if (!Repository.EqualsWithNull(To, other.To)) return false;
                if (!Repository.EqualsWithNull(Subject, other.Subject)) return false;
                if (!Repository.EqualsWithNull(Body, other.Body)) return false;
                if (!Repository.EqualsWithNull(Report, other.Report)) return false;
                if (!Repository.EqualsWithNull(MailHost, other.MailHost)) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public abstract class xBaseObject : ICloneable
    {
        private object Clone(object vObj)
        {
            if (vObj == null || vObj.GetType().IsValueType || vObj.GetType() == Type.GetType("System.String"))
                return vObj;
            else
            {
                object newObject = Activator.CreateInstance(vObj.GetType());
                foreach (PropertyInfo Item in newObject.GetType().GetProperties())
                {
                    if (Item.GetType().GetInterface("ICloneable") != null)
                    {
                        ICloneable IClone = (ICloneable)Item.GetValue(vObj, null);
                        Item.SetValue(newObject, IClone.Clone(), null);
                    }
                    else
                        Item.SetValue(newObject, Clone(Item.GetValue(vObj, null)), null);
                }
                foreach (FieldInfo Item in newObject.GetType().GetFields())
                {
                    if (Item.GetType().GetInterface("ICloneable") != null)
                    {
                        ICloneable IClone = (ICloneable)Item.GetValue(vObj);
                        Item.SetValue(newObject, IClone.Clone());
                    }
                    else
                        Item.SetValue(newObject, Clone(Item.GetValue(vObj)));
                }
                return newObject;
            }
        }

        public object Clone()
        {
            return Clone(this);
        }
    }

    /// Base class for cloning an object in C#
    /// By Amir Harel. 
    /// <summary>
    /// BaseObject class is an abstract class for you to derive from.
    /// Every class that will be derived from this class will support the 
    /// Clone method automaticly.

    /// The class implements the interface ICloneable and there 
    /// for every object that will be derived 

    /// from this object will support the ICloneable interface as well.
    /// </summary>

    [Serializable]
    public abstract class BaseObject : ICloneable
    {
        /// <summary>
        /// Clone the object, and returning a reference to a cloned object.
        /// </summary>
        /// <returns>Reference to the new cloned 
        /// object.</returns>
        public object Clone()
        {
            //First we create an instance of this specific type.
            object newObject = Activator.CreateInstance(this.GetType());

            //We get the array of fields for the new type instance.
            FieldInfo[] fields = newObject.GetType().GetFields();

            int i = 0;

            foreach (FieldInfo fi in this.GetType().GetFields())
            {
                //We query if the fiels support the ICloneable interface.
                Type ICloneType = fi.FieldType.
                            GetInterface("ICloneable", true);

                if (ICloneType != null)
                {
                    //Getting the ICloneable interface from the object.
                    ICloneable IClone = (ICloneable)fi.GetValue(this);

                    if (IClone == null)
                    {
                        i++;
                        continue; // Unitialized in source, so skip the copying
                    }

                    //We use the clone method to set the new value to the field.
                    fields[i].SetValue(newObject, IClone.Clone());
                }
                else
                {
                    // If the field doesn't support the ICloneable 
                    // interface then just set it.
                    fields[i].SetValue(newObject, fi.GetValue(this));
                }

                //Now we check if the object support the 
                //IEnumerable interface, so if it does
                //we need to enumerate all its items and check if 
                //they support the ICloneable interface.
                Type IEnumerableType = fi.FieldType.GetInterface
                                ("IEnumerable", true);
                if (IEnumerableType != null)
                {
                    //Get the IEnumerable interface from the field.
                    IEnumerable IEnum = (IEnumerable)fi.GetValue(this);

                    //This version support the IList and the 
                    //IDictionary interfaces to iterate on collections.
                    Type IListType = fields[i].FieldType.GetInterface
                                        ("IList", true);
                    Type IDicType = fields[i].FieldType.GetInterface
                                        ("IDictionary", true);

                    int j = 0;
                    if (IListType != null)
                    {
                        //Getting the IList interface.
                        IList list = (IList)fields[i].GetValue(newObject);
                        cloneList((IList)fi.GetValue(this), list);
                        j++;
                    }
                    else if (IDicType != null)
                    {
                        //Getting the dictionary interface.
                        IDictionary dic = (IDictionary)fields[i].
                                            GetValue(newObject);
                        j = 0;

                        if (dic != null)
                            foreach (DictionaryEntry de in dic)
                            {
                                //Checking to see if the item 
                                //support the ICloneable interface.
                                ICloneType = de.Value.GetType().
                                    GetInterface("ICloneable", true);

                                if (ICloneType != null)
                                {
                                    ICloneable clone = (ICloneable)de.Value;
                                    dic[de.Key] = clone.Clone();
                                }
                                else
                                {
                                    Type IEnumerableType2 = de.Value.GetType().GetInterface("IEnumerable", true);
                                    if (IEnumerableType2 != null)
                                    {
                                        //Get the IEnumerable interface from the field.
                                        IEnumerable IEnum2 = (IEnumerable)de.Value;

                                        //This version support the IList and the 
                                        //IDictionary interfaces to iterate on collections.
                                        Type IListType2 = de.Value.GetType().GetInterface
                                                            ("IList", true);
                                        if (IListType2 != null)
                                        {
                                            IList list = (IList)Activator.CreateInstance(de.Value.GetType());
                                            cloneList((IList)de.Value, list);
                                        }
                                    }
                                }
                                j++;
                            }
                    }
                }
                i++;
            }
            return newObject;
        }

        public void cloneList(IList list, IList newList)
        {
            if (list == null || list.Count == 0)
            {
                return;
            }

            for (int i = 0; i < list.Count; i++)
            {
                object obj = list[i];
                if (obj != null)
                {
                    //Checking to see if the current item 
                    //support the ICloneable interface.
                    Type ICloneType = obj.GetType().
                        GetInterface("ICloneable", true);

                    if (ICloneType != null)
                    {
                        //If it does support the ICloneable interface, 
                        //we use it to set the clone of
                        //the object in the list.
                        ICloneable clone = (ICloneable)obj;
                        if (newList.IsFixedSize)
                            newList[i] = clone.Clone();
                        else
                            newList.Add(clone.Clone());
                    }
                    else
                    {
                        if (newList.IsFixedSize)
                            newList[i] = obj;
                        else
                            newList.Add(obj);
                    }
                }
            }
        }
    }

    [Serializable]
    public abstract class BaseAuditableObject : BaseObject
    {
        private static int MAX_NUMBER_LOCK_TRIES = 20;
        private static int SLEEP_TIME = 1000;
        // the client will not set these values, so they need to be Nullable types
        public Guid? Guid;
        public DateTime? CreatedDate;
        public DateTime? LastModifiedDate;
        public string CreatedUsername;
        public string LastModifiedUsername;

        public string Name;//physical
        public string LogicalName;

        [XmlIgnore]
        private string lockFileName = null;

        public bool lockThis(string lockFilename)
        {
            if (lockFileName != null && File.Exists(lockFileName))
                return true;

            return internalLockThis(lockFilename, 0);
        }

        public void useLock(BaseAuditableObject o)
        {
            o.lockFileName = lockFileName;
        }

        private bool internalLockThis(string l, int count)
        {
            FileStream lockFileStream = null;
            if (count > MAX_NUMBER_LOCK_TRIES)
            {
                throw new RepositoryLockedException("Can not complete task.  " + l + " already exists.");
            }
            try
            {
                lockFileStream = new FileStream(l, FileMode.CreateNew);
                this.lockFileName = l;
            }
            catch (IOException)
            {
                Thread.Sleep(SLEEP_TIME);
                return internalLockThis(l, count + 1);
            }
            finally
            {
                if (lockFileStream != null)
                    lockFileStream.Close();
            }

            return lockFileStream != null;
        }

        public void unlockThis()
        {
            if (lockFileName != null)
            {
                File.Delete(lockFileName);
                lockFileName = null;
            }
        }


        public static Guid generateGuid()
        {
            return System.Guid.NewGuid();
        }

        public abstract string toAuditString();

        virtual public bool IsEqualTo(object obj)
        {
            if (obj is BaseAuditableObject)
            {
                return (Name.Equals(((BaseAuditableObject)obj).Name));
            }
            return false;
        }

    }


    [Serializable]
    public class SourceFile : BaseAuditableObject
    {
        public static int UPLOAD_NO_ERRORS = 0;
        public static int UPLOAD_ERROR_SOURCE_FORMAT_LOCKED = 1;
        public static int UPLOAD_ERROR_SOURCE_COLUMN_LOCKED = 2;
        public static int UPLOAD_ERROR_SCHEMA_CHANGED = 3;
        public static string REPLACE_WITH_NULL = "ReplaceWithNull";
        public static string SKIP_RECORD = "SkipRecord";

        [Serializable]
        public class SourceColumn : BaseObject
        {
            public string Name;
            public string DataType;
            public int Width;
            public string Format;
            [System.ComponentModel.DefaultValueAttribute(0)]
            public double Multiplier;
            [System.ComponentModel.DefaultValueAttribute(0)]
            public int NumDistinctValues;
            [System.ComponentModel.DefaultValueAttribute(false)]
            public bool Encrypted = false;
            [System.ComponentModel.DefaultValueAttribute(false)]
            public bool PreventUpdate;

            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                if (!typeof(SourceColumn).Equals(obj.GetType())) return false;
                SourceColumn other = (SourceColumn)obj;
                if (!Repository.EqualsWithNull(Name, other.Name)) return false;
                if (!Repository.EqualsWithNull(DataType, other.DataType)) return false;
                if (!Repository.EqualsWithNull(Width, other.Width)) return false;
                if (!Repository.EqualsWithNull(Format, other.Format)) return false;
                if (!Repository.Equals(Multiplier, other.Multiplier)) return false;
                if (Encrypted != other.Encrypted) return false;
                if (PreventUpdate != other.PreventUpdate) return false;
                return true;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public SourceColumn clone()
            {
                SourceColumn sc = new SourceColumn();
                sc.Name = Name;
                sc.DataType = DataType;
                sc.Width = Width;
                sc.Format = Format;
                sc.Multiplier = Multiplier;
                sc.NumDistinctValues = NumDistinctValues;
                sc.Encrypted = Encrypted;
                sc.PreventUpdate = PreventUpdate;
                return sc;
            }
        }
        public enum FileFormatType
        {
            FixedWidth, Delimited
        }

        public string FileName;
        public string Encoding;
        public string Separator;
        public int IgnoredRows;
        public bool HasHeaders;
        public int IgnoredLastRows;
        public string InputTimeZone;
        public SourceColumn[] Columns;
        public SourceColumn[] ImportColumns;
        public FileFormatType FormatType = FileFormatType.Delimited;
        public string SearchPattern;
        public string S3Bucket;
        public string S3Folder;
        public string Quote = "\"";
        public int NumRows;
        public long FileLength;
        public DateTime LastUploadDate;
        public string FilterColumn;
        public string FilterOperator;
        public string FilterValue;
        public string[][] Escapes;
        public int[] KeyIndices;
        public bool ForceNumColumns;
        public bool OnlyQuoteAfterSeparator;
        public bool DoNotTreatCarriageReturnAsNewline;
        public bool IgnoreBackslash;
        //This is radio button "Locked - Fail Processing and source updates if format changes are detected" in Birst UI
        public bool LockFormat;
        //This is radio button "Custom - Manually assign source update rules" in Birst UI
        public bool SchemaLock;
        //If both LockFormat and SchemaLock are false, it will be fully automatic mode i.e. "Automatic - Adds/Removes Columns, 
        //Update Types, and expand widths as needed" in Birst UI
        //The following 5 options/flags will be considered only if LockFormat is false and SchemaLock is true
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowAddColumns;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowNullBindingRemovedColumns;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowUpcast;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowVarcharExpansion;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AllowValueTruncationOnLoad;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool FailUploadOnVarcharExpansion;
        public bool ContinueIfMissing;
        public int MinRows;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool CustomUpload;
        [System.ComponentModel.DefaultValueAttribute("ReplaceWithNull")]
        public string BadColumnValueAction;

        [XmlIgnore]
        [NonSerialized]
        public bool amIBeingDragged = false;
        [XmlIgnore]
        [NonSerialized]
        public int UploadStatus = UPLOAD_NO_ERRORS;

        public SourceFile()
        {
        }

        public DataTable getTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ColumnName");
            dt.Columns.Add("DataType");
            int i = 0;
            dt.Columns.Add("Width", i.GetType());
            dt.Columns.Add("Format");
            dt.Columns.Add("Multiplier", typeof(double));
            dt.Columns.Add("Encrypted", typeof(bool));
            dt.Columns.Add("PreventUpdate", typeof(bool));
            if (Columns != null && Columns.Length > 0)
            {
                foreach (SourceColumn sc in Columns)
                {
                    DataRow dr = dt.NewRow();
                    dr["ColumnName"] = sc.Name;
                    dr["DataType"] = sc.DataType;
                    dr["Width"] = sc.Width;
                    dr["Format"] = sc.Format;
                    if (sc.Multiplier != 0)
                        dr["Multiplier"] = sc.Multiplier;
                    dr["Encrypted"] = sc.Encrypted;
                    dr["PreventUpdate"] = sc.PreventUpdate;
                    dt.Rows.Add(dr);
                }
            }
            return (dt);
        }

        private static char[] trimChars = new char[] { ' ', '\n', '\r' };

        public void updateFromDataTable(DataTable dt)
        {
            List<SourceColumn> sclist = new List<SourceColumn>();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["ColumnName"] != System.DBNull.Value)
                {
                    SourceColumn sc = new SourceColumn();
                    sc.Name = ((string)dr["ColumnName"]).Trim(trimChars);
                    sc.DataType = dr["DataType"] == System.DBNull.Value ? "Varchar" : (string)dr["DataType"];
                    sc.Width = dr["Width"] == System.DBNull.Value ? 0 : (int)dr["Width"];
                    sc.Format = dr["Format"] == System.DBNull.Value ? null : (string)dr["Format"];
                    if (sc.Format != null && sc.Format.Trim().Length == 0) // make sure we don't output <Format />
                        sc.Format = null;
                    sc.Multiplier = dr["Multiplier"] == System.DBNull.Value ? 0 : (double)dr["Multiplier"];
                    sc.Encrypted = dr["Encrypted"] == System.DBNull.Value ? false : (bool)dr["Encrypted"];
                    sc.PreventUpdate = dr["PreventUpdate"] == System.DBNull.Value ? false : (bool)dr["PreventUpdate"];
                    sclist.Add(sc);
                }
            }
            Columns = sclist.ToArray();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(SourceFile).Equals(obj.GetType())) return false;
            SourceFile other = (SourceFile)obj;
            if (!Repository.EqualsWithNull(FileName, other.FileName)) return false;
            if (!Repository.EqualsWithNull(Encoding, other.Encoding)) return false;
            if (!Repository.EqualsWithNull(Separator, other.Separator)) return false;
            if (!Repository.EqualsWithNull(IgnoredRows, other.IgnoredRows)) return false;
            if (!Repository.Equals(HasHeaders, other.HasHeaders)) return false;
            if (!Repository.EqualsWithNull(InputTimeZone, other.InputTimeZone)) return false;
            if (!Repository.EqualsWithNull(IgnoredLastRows, other.IgnoredLastRows)) return false;
            if (!Repository.EqualsArray(Columns, other.Columns)) return false;
            if (!Repository.Equals(FormatType, other.FormatType)) return false;
            if (!Repository.EqualsWithNull(SearchPattern, other.SearchPattern)) return false;
            if (!Repository.EqualsWithNull(S3Bucket, other.S3Bucket)) return false;
            if (!Repository.EqualsWithNull(S3Folder, other.S3Folder)) return false;
            if (!Repository.EqualsArray(Escapes, other.Escapes)) return false;
            if (ForceNumColumns != other.ForceNumColumns) return false;
            if (OnlyQuoteAfterSeparator != other.OnlyQuoteAfterSeparator) return false;
            if (IgnoreBackslash != other.IgnoreBackslash) return false;
            if (LockFormat != other.LockFormat) return false;
            if (SchemaLock != other.SchemaLock) return false;
            if (AllowAddColumns != other.AllowAddColumns) return false;
            if (AllowNullBindingRemovedColumns != other.AllowNullBindingRemovedColumns) return false;
            if (AllowUpcast != other.AllowUpcast) return false;
            if (AllowVarcharExpansion != other.AllowVarcharExpansion) return false;
            if (AllowValueTruncationOnLoad != other.AllowValueTruncationOnLoad) return false;
            if (FailUploadOnVarcharExpansion != other.FailUploadOnVarcharExpansion) return false;
            if (ContinueIfMissing != other.ContinueIfMissing) return false;
            if (!Repository.EqualsWithNull(MinRows, other.MinRows)) return false;
            if (CustomUpload != other.CustomUpload) return false;
            if (BadColumnValueAction != other.BadColumnValueAction) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string toAuditString()
        {
            string ret = "SourceFile ";
            ret += FileName;
            ret += " ";
            ret += Guid;
            return ret;
        }

        public override bool IsEqualTo(object obj)
        {
            if (obj is SourceFile)
            {
                return (FileName.Equals(((SourceFile)obj).FileName));
            }
            return false;
        }

        public int getColumnIndex(string name)
        {
            for (int i = 0; i < Columns.Length; i++)
            {
                if (Columns[i].Name == name)
                    return i;
            }
            return -1;
        }

    }

    [Serializable]
    public class SecurityFilter : BaseObject
    {
        public static int TYPE_VARIABLE = 0;
        public static int TYPE_SET_BASED = 1;
        public int Type;
        // Name of the Session Variable OR Direct Logical Query String for set based filter report
        public string SessionVariable;
        public bool Enabled;
        public string[] FilterGroups;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool addIsNullJoinCondition;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(SecurityFilter).Equals(obj.GetType())) return false;
            SecurityFilter other = (SecurityFilter)obj;
            if (!Repository.EqualsWithNull(SessionVariable, other.SessionVariable)) return false;
            if (Enabled != other.Enabled) return false;
            if (addIsNullJoinCondition != other.addIsNullJoinCondition) return false;
            if (Type != other.Type) return false;
            if (!Repository.EqualsArray(FilterGroups, other.FilterGroups)) return false;
            return true;
        }

        public string getSerializedString()
        {
            return Type + "|4" + SessionVariable + "|4" + Enabled + Util.getSerializedString(FilterGroups);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class StagingColumn : BaseObject
    {
        public string Name;
 		public string PhysicalName;
        public string DataType;
        public int Width;
        public string SourceFileColumn;
        [XmlArrayItem(Type = typeof(Expression)), XmlArrayItem(Type = typeof(DateID)), XmlArrayItem(Type = typeof(TableLookup)), XmlArrayItem(Type = typeof(Rank))]
        public Transformation[] Transformations;
        public string[] TargetTypes;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool NaturalKey;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool GenerateTimeDimension;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Index;
        public StagingColumnGrainInfo[] GrainInfo;
        public string UnknownValue;
        public string[] TargetAggregations;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ExcludeFromChecksum;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool TableKey;
        public SecurityFilter SecFilter;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool AnalyzeMeasure;
        public string getLogicalColumnName()
        {
            return (this.Name);
        }

        public string getPhysicalColumnName()
        {
            return ((this.PhysicalName != null && this.PhysicalName.Length > 0) ? this.PhysicalName : this.Name);
        }

        public void setNameAndPhysicalName(MainAdminForm ma,string columName)
        {
            ApplicationBuilder appBuilderInstance = new ApplicationBuilder(ma);
            if (columName!=null && ma.builderVersion >= 28 && columName.Length > appBuilderInstance.getMaxPhysicalColumnNameLength())
            {
                this.Name = columName;
                this.PhysicalName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(ma), columName + ApplicationBuilder.PHYSICAL_SUFFIX, appBuilderInstance.getMaxPhysicalColumnNameLength(), ma.builderVersion);
            }
            else
            {
                this.Name = columName;
            }
        }

        public string getSerializedString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("n=" + Name);
            sb.Append("|3d=" + DataType);
            sb.Append("|3w=" + Width);
            sb.Append("|3s=" + SourceFileColumn);
            if (TargetTypes != null)
                sb.Append("|3t=" + Util.getSerializedString(TargetTypes));
            if (NaturalKey)
                sb.Append("|3k");
            if (GenerateTimeDimension)
                sb.Append("|3g");
            if (Index)
                sb.Append("|3i");
            if (UnknownValue != null)
                sb.Append("|3u=" + UnknownValue);
            if (TargetAggregations != null)
                sb.Append("|3a=" + Util.getSerializedString(TargetAggregations));
            if (ExcludeFromChecksum)
                sb.Append("|3e");
            if (TableKey)
                sb.Append("|3y");
            if (SecFilter != null)
                sb.Append("|3f=" + SecFilter.getSerializedString());
            if (AnalyzeMeasure)
                sb.Append("|3m");
            return sb.ToString();
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(StagingColumn).Equals(obj.GetType())) return false;
            StagingColumn other = (StagingColumn)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(PhysicalName, other.PhysicalName)) return false;
            if (!Repository.EqualsWithNull(DataType, other.DataType)) return false;
            if (!Repository.EqualsWithNull(Width, other.Width)) return false;
            if (!Repository.EqualsWithNull(SourceFileColumn, other.SourceFileColumn)) return false;
            if (!Repository.EqualsArray(Transformations, other.Transformations)) return false;
            if (!Repository.EqualsArray(TargetTypes, other.TargetTypes)) return false;
            if (NaturalKey != other.NaturalKey) return false;
            if (GenerateTimeDimension != other.GenerateTimeDimension) return false;
            if (Index != other.Index) return false;
            if (!Repository.EqualsArray(GrainInfo, other.GrainInfo)) return false;
            if (UnknownValue != other.UnknownValue) return false;
            if (!Repository.EqualsArray(TargetAggregations, other.TargetAggregations)) return false;
            if (ExcludeFromChecksum != other.ExcludeFromChecksum) return false;
            if (TableKey != other.TableKey) return false;
            if (AnalyzeMeasure != other.AnalyzeMeasure) return false;
            if (SecFilter != null && other.SecFilter == null) return false;
            if (SecFilter == null && other.SecFilter != null) return false;
            if (SecFilter != null && !SecFilter.Equals(other.SecFilter)) return false;
            return true;
        }

        public List<DiffObject> diffDescription(StagingColumn other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(StagingColumn).Equals(other.GetType())) return null;
            if (!Repository.EqualsWithNull(Name, other.Name))
            {
                differences.Add(new DiffObject("Name", other.Name, Name));
            }
            if (!Repository.EqualsWithNull(PhysicalName, other.PhysicalName))
            {
                differences.Add(new DiffObject("PhysicalName", other.PhysicalName, PhysicalName));
            }
            if (!Repository.EqualsWithNull(DataType, other.DataType))
            {
                differences.Add(new DiffObject("Data Type", other.DataType, DataType));
            }
            if (!Repository.EqualsWithNull(Width, other.Width))
            {
                differences.Add(new DiffObject("Width", other.Width, Width));
            }
            if (UnknownValue != other.UnknownValue)
            {
                differences.Add(new DiffObject("Unknown Value", other.UnknownValue, UnknownValue));
            }
            if (AnalyzeMeasure != other.AnalyzeMeasure)
            {
                differences.Add(new DiffObject("Analyze Measure", other.AnalyzeMeasure, AnalyzeMeasure));
            }
            if (NaturalKey != other.NaturalKey)
                differences.Add(new DiffObject("Natural Key", other.NaturalKey, NaturalKey));
            if (!Repository.EqualsArray(TargetTypes, other.TargetTypes))
            {
                bool measure = TargetTypes != null && Array.IndexOf<string>(TargetTypes, "Measure") >= 0;
                bool othermeasure = other.TargetTypes != null && Array.IndexOf<string>(other.TargetTypes, "Measure") >= 0;
                if (measure != othermeasure)
                {
                    differences.Add(new DiffObject("Measure", othermeasure, measure));
                }
                List<string> targetedDims = new List<string>();
                List<string> othertargetedDims = new List<string>();
                if (TargetTypes != null)
                    foreach (string s in TargetTypes)
                        if (s != "Measure" && s != "Time") targetedDims.Add(s);
                if (other.TargetTypes != null)
                    foreach (string s in other.TargetTypes)
                        if (s != "Measure" && s != "Time") othertargetedDims.Add(s);
                bool matchDims = targetedDims.Count == othertargetedDims.Count;
                if (matchDims)
                {
                    foreach (string s in targetedDims)
                    {
                        bool found = false;
                        foreach (string s2 in othertargetedDims)
                        {
                            if (s == s2)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            matchDims = false;
                            break;
                        }
                    }
                }
                if (!matchDims)
                {
                    DiffObject dobj = new DiffObject("Dimensions");
                    if (targetedDims.Count == 0)
                        dobj.baseObjectVersion = "No dimension target";
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        int count = 0;
                        sb.Append('[');
                        foreach (string s in targetedDims)
                        {
                            if (count++ > 0) sb.Append(',');
                            sb.Append(s);
                        }
                        sb.Append("]");
                        dobj.baseObjectVersion = sb.ToString();
                    }
                    if (othertargetedDims.Count == 0)
                        dobj.compareObjectVersion = "No dimension target";
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        int count = 0;
                        sb.Append('[');
                        foreach (string s in othertargetedDims)
                        {
                            if (count++ > 0) sb.Append(',');
                            sb.Append(s);
                        }
                        sb.Append("]");
                        dobj.compareObjectVersion = sb.ToString();
                    }
                    differences.Add(dobj);
                }
            }
            if ((SecFilter != null && other.SecFilter == null) ||
                (SecFilter == null && other.SecFilter != null) ||
                (SecFilter != null && !SecFilter.Equals(other.SecFilter)))
            {
                DiffObject dobj = new DiffObject("Security Filter", "", "");
                differences.Add(dobj);
            }
            if (GenerateTimeDimension != other.GenerateTimeDimension)
            {
                DiffObject dobj = new DiffObject("Analyze by Date", other.GenerateTimeDimension, GenerateTimeDimension);
                differences.Add(dobj);
            }
            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject("Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class LoadGroup : BaseObject
    {
        public string Name;
        public LoadGroupCondition[] Conditions;

        public LoadGroup()
        {
        }

        public LoadGroup(string name)
        {
            this.Name = name;
        }

        public DataTable getTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DimensionName");
            dt.Columns.Add("DimensionColumn");
            dt.Columns.Add("Operator");
            dt.Columns.Add("Value");

            if (Conditions != null)
                foreach (LoadGroupCondition lgc in Conditions)
                {
                    DataRow dr = dt.NewRow();
                    dr["DimensionName"] = lgc.DimensionName;
                    dr["DimensionColumn"] = lgc.DimensionColumn;
                    dr["Operator"] = lgc.Operator;
                    dr["Value"] = lgc.Value;
                    dt.Rows.Add(dr);
                }
            return (dt);
        }

        public void updateFromDataTable(DataTable dt)
        {
            List<LoadGroupCondition> lgclist = new List<LoadGroupCondition>();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["DimensionName"] != System.DBNull.Value)
                {
                    LoadGroupCondition lgc = new LoadGroupCondition();
                    lgc.DimensionName = (string)dr["DimensionName"];
                    lgc.DimensionColumn = (string)dr["DimensionColumn"];
                    lgc.Operator = (string)dr["Operator"];
                    lgc.Value = (string)dr["Value"];
                    lgclist.Add(lgc);
                }
            }
            Conditions = lgclist.ToArray();
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(LoadGroup).Equals(obj.GetType())) return false;
            LoadGroup other = (LoadGroup)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsArray(Conditions, other.Conditions)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class LoadGroupCondition : BaseObject
    {
        public string DimensionName;
        public string DimensionColumn;
        public string Operator;
        public string Value;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(LoadGroupCondition).Equals(obj.GetType())) return false;
            LoadGroupCondition other = (LoadGroupCondition)obj;
            if (!Repository.EqualsWithNull(DimensionName, other.DimensionName)) return false;
            if (!Repository.EqualsWithNull(DimensionColumn, other.DimensionColumn)) return false;
            if (!Repository.EqualsWithNull(Operator, other.Operator)) return false;
            if (!Repository.EqualsWithNull(Value, other.Value)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class CustomTimeShift
    {
        public string BaseKey;
        public string RelativeKey;
        public string Prefix;
        public string[] Columns;

        public string getSerializedString()
        {
            return BaseKey + "|2" + RelativeKey + "|2" + Prefix + "|2" + Util.getSerializedString(Columns);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !typeof(CustomTimeShift).Equals(obj.GetType())) return false;
            CustomTimeShift other = (CustomTimeShift)obj;
            if (BaseKey != other.BaseKey) return false;
            if (RelativeKey != other.RelativeKey) return false;
            if (Prefix != other.Prefix) return false;
            if (!Repository.EqualsArray(Columns, other.Columns)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class SnapshotPolicy : BaseObject
    {
        public static int INTERVAL_WEEK = 1;
        public static int INTERVAL_MONTH = 2;
        public static int DAY_OF_WEEK_SUNDAY = 1;
        public static int DAY_OF_WEEK_MONDAY = 2;
        public static int DAY_OF_WEEK_TUESDAY = 4;
        public static int DAY_OF_WEEK_WEDNESDAY = 8;
        public static int DAY_OF_WEEK_THURSDAY = 16;
        public static int DAY_OF_WEEK_FRIDAY = 32;
        public static int DAY_OF_WEEK_SATURDAY = 64;

        public bool CurrentDay;
        public int Type; // 1 - Weekly, 2 - Monthly
        public int DaysOfWeek;
        public int DayOfMonth;
        public bool firstDayOfMonth;
        public bool lastDayOfMonth;

        public string getSerializedString()
        {
            return CurrentDay.ToString() + "|2" + Type + "|2" + DaysOfWeek + "|2" + DayOfMonth + "|2" + firstDayOfMonth + "|2" + lastDayOfMonth;
        }
    }

    [Serializable]
    public class OverrideLevelKey
    {
        public string Dimension;
        public string Level;
        public string[] KeyColumns;

        public string getSerializedString()
        {
            return Dimension + "|2" + Level + "|2" + Util.getSerializedString(KeyColumns);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !typeof(OverrideLevelKey).Equals(obj.GetType()))
                return false;
            OverrideLevelKey other = (OverrideLevelKey)obj;
            if (Dimension != other.Dimension) return false;
            if (Level != other.Level) return false;
            if (!Repository.EqualsArray(KeyColumns, other.KeyColumns)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class InheritTable : BaseObject
    {
        public static int STAGING_TYPE = 0;
        public static int DIMENSION_TYPE = 1;
        public static int MEASURE_TYPE = 2;
        public int Type;
        public string Name;
        public string Prefix;

        public string toSerializedString()
        {
            return Type + "|" + Name + "|" + Prefix;
        }

        public void getFromSerializedString(string s)
        {
            string[] parts = s.Split(new char[] { '|' });
            if (parts.Length != 3)
                return;
            int.TryParse(parts[0], out Type);
            Name = parts[1];
            Prefix = parts[2];
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !typeof(InheritTable).Equals(obj.GetType())) return false;
            InheritTable other = (InheritTable)obj;
            if (Type != other.Type) return false;
            if (Name != other.Name) return false;
            if (Prefix != other.Prefix) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class StagingTable : BaseAuditableObject
    {
        public static int SOURCE_FILE = 0;
        public static int SOURCE_QUERY = 1;
        public static string[] SOURCE_FILE_TYPE_NAMES = new string[] { "Source File", "Query" };
        public int ID;
        public int SourceType;
        public string SourceFile;
        public string Query;
        public bool PersistQuery;
        public string[][] Levels;
        public string[][] LevelFilters;
        public StagingColumn[] Columns;
        public bool Disabled;
        [XmlArrayItem(Type = typeof(Procedure)), XmlArrayItem(Type = typeof(Household)), XmlArrayItem(Type = typeof(JavaTransform))]
        public Transformation[] Transformations;
        public string[] LoadGroups;
        public string[] SubGroups;
        public bool WebOptional;
        public bool WebReadOnly;
        public bool Consolidation;
        public bool Transactional;
        public bool TruncateOnLoad;
        public string SourceGroups;
        public string ChecksumGroup;
        public bool IncrementalSnapshotFact;
        public string[] SnapshotDeleteKeys;
        public OverrideLevelKey[] OverrideLevelKeys;
        [XmlIgnore]
        [NonSerialized]
        public bool amIBeingDragged = false;
        public string SCDDateName;
        public ScriptDefinition Script;
        public string CustomTimeColumn;
        public string CustomTimePrefix;
        public CustomTimeShift[] CustomTimeShifts;
        public SnapshotPolicy Snapshots;
        public static string NO_TARGET = "NO_TARGET";
        public static string TARGET_COMMON = "TARGET_COMMON";
        public string NewColumnTarget;
        public string[] SourceKey;
        public ForeignKey[] ForeignKeys;
        public ForeignKey[] FactForeignKeys;
        // Depricated columns that will get converted to proper foreign keys
        public string[] ForeignKeySources;
        public string[] FactForeignKeySources;
        //
        public string ParentForeignKeySource;
        public string HierarchyName;
        public string LevelName;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ExcludeFromModel;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool DiscoveryTable;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool UseBirstLocalForSource;
        public string PreviousLoadGroup;
        public Location[] Locations;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool Autoplace;
        [System.ComponentModel.DefaultValueAttribute("")]
        public string MeasureNamePrefix;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool LiveAccess;
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool UnCached;
        public TableSource TableSource;
        public InheritTable[] InheritTables;
        [System.ComponentModel.DefaultValueAttribute(0)]
        public int Cardinality;
        // R Server Source Parameters
        public string RServerPath;
        public string RExpression;
        public string RServerSettings;

        public StagingTable()
        {
        }

        public StagingTable(string name, int id)
        {
            this.Name = name;
            this.ID = id;
        }

        public DataTable getTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ColumnName");
            dt.Columns.Add("DataType");
            int fcount = 0;
            dt.Columns.Add("Width", fcount.GetType());
            dt.Columns.Add("SourceFileColumn");
            dt.Columns.Add("Transformation", typeof(Transformation));
            dt.Columns.Add("TargetTypes", typeof(List<string>));
            dt.Columns.Add("NaturalKey", typeof(bool));
            dt.Columns.Add("GenerateTimeDimension", typeof(bool));
            dt.Columns.Add("Index", typeof(bool));
            dt.Columns.Add("ExcludeFromChecksum", typeof(bool));
            dt.Columns.Add("GrainInfo", typeof(StagingColumnGrainInfoDisplay));
            dt.Columns.Add("UnknownValue");
            dt.Columns.Add("TargetAggregations", typeof(string[]));
            dt.Columns.Add("TableKey", typeof(bool));
            dt.Columns.Add("SecFilter", typeof(SecurityFilter));

            if (Columns != null)
            {
                foreach (StagingColumn sc in Columns)
                {
                    DataRow dr = dt.NewRow();
                    dr["ColumnName"] = sc.Name;
                    dr["DataType"] = sc.DataType;
                    dr["Width"] = sc.Width;
                    dr["SourceFileColumn"] = sc.SourceFileColumn;
                    dr["Transformation"] = sc.Transformations != null ? sc.Transformations[0] : null;
                    dr["TargetTypes"] = sc.TargetTypes == null ? new List<string>(new string[] { }) : new List<string>(sc.TargetTypes);
                    ((List<string>)dr["TargetTypes"]).Remove("None");
                    dr["NaturalKey"] = sc.NaturalKey;
                    dr["GenerateTimeDimension"] = sc.GenerateTimeDimension;
                    dr["Index"] = sc.Index;
                    dr["ExcludeFromChecksum"] = sc.ExcludeFromChecksum;
                    StagingColumnGrainInfoDisplay obj = new StagingColumnGrainInfoDisplay();
                    obj.scGrainInfo = sc.GrainInfo;
                    dr["GrainInfo"] = obj;
                    dr["UnknownValue"] = sc.UnknownValue == null ? "" : sc.UnknownValue;
                    if (sc.TargetAggregations != null)
                        dr["TargetAggregations"] = sc.TargetAggregations;
                    dr["TableKey"] = sc.TableKey;
                    if (sc.SecFilter != null)
                        dr["SecFilter"] = sc.SecFilter;
                    dt.Rows.Add(dr);
                }
            }
            return (dt);
        }

        private static char[] trimChars = new char[] { ' ', '\n', '\r' };

        public void updateFromDataTable(DataTable dt)
        {
            List<StagingColumn> sclist = new List<StagingColumn>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (dr["ColumnName"] != System.DBNull.Value)
                {
                    StagingColumn sc = new StagingColumn();
                    sc.Name = ((string)dr["ColumnName"]).Trim(trimChars);
                    sc.DataType = dr["DataType"] == System.DBNull.Value ? "Varchar" : (string)dr["DataType"];
                    sc.Width = dr["Width"] == System.DBNull.Value ? 0 : (int)dr["Width"];
                    sc.SourceFileColumn = dr["SourceFileColumn"] == System.DBNull.Value ? null : ((string)dr["SourceFileColumn"]).Trim(trimChars);
                    if (dr["Transformation"] != System.DBNull.Value)
                    {
                        sc.Transformations = new Transformation[1];
                        sc.Transformations[0] = (Transformation)dr["Transformation"];
                    }
                    sc.TargetTypes = dr["TargetTypes"] == System.DBNull.Value ? new string[] { } : ((List<string>)dr["TargetTypes"]).ToArray();
                    sc.NaturalKey = dr["NaturalKey"] == System.DBNull.Value ? false : (bool)dr["NaturalKey"];
                    sc.GenerateTimeDimension = dr["GenerateTimeDimension"] == System.DBNull.Value ? false : (bool)dr["GenerateTimeDimension"];
                    sc.Index = dr["Index"] == System.DBNull.Value ? false : (bool)dr["Index"];
                    sc.ExcludeFromChecksum = dr["ExcludeFromChecksum"] == System.DBNull.Value ? false : (bool)dr["ExcludeFromChecksum"];
                    sc.TableKey = dr["TableKey"] == System.DBNull.Value ? false : (bool)dr["TableKey"];
                    StagingColumnGrainInfoDisplay obj
                                                = dr["GrainInfo"] == System.DBNull.Value ? null : (StagingColumnGrainInfoDisplay)dr["GrainInfo"];
                    if (obj == null || obj.scGrainInfo == null)
                    {
                        sc.GrainInfo = null;
                    }
                    else
                    {
                        sc.GrainInfo = obj.scGrainInfo;
                    }
                    if (dr["UnknownValue"] != System.DBNull.Value)
                        sc.UnknownValue = (string)dr["UnknownValue"];
                    if (dr["TargetAggregations"] != System.DBNull.Value)
                        sc.TargetAggregations = (string[])dr["TargetAggregations"];
                    if (dr["SecFilter"] != System.DBNull.Value)
                        sc.SecFilter = (SecurityFilter)dr["SecFilter"];
                    sclist.Add(sc);
                }
            }
            Columns = sclist.ToArray();
        }

        public static string SerializeGrain(string[][] grain)
        {
            if (grain == null)
                return "";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < grain.Length; i++)
            {
                if (i > 0)
                    sb.Append(",");
                sb.Append("[" + grain[i][0] + "." + grain[i][1] + "]");
            }
            return sb.ToString();
        }

        public static string SerializeLevelFilters(string[][] filters)
        {
            if (filters == null)
                return "";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < filters.Length; i++)
            {
                if (i > 0)
                    sb.Append(",");
                sb.Append("[" + filters[i][0] + " " + filters[i][1] + "]");
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(StagingTable).Equals(obj.GetType())) return false;
            StagingTable other = (StagingTable)obj;
            if (SourceType != other.SourceType) return false;
            if (!Repository.EqualsStringEmptyIsNull(Name, other.Name)) return false;
            if (!Repository.EqualsStringEmptyIsNull(LogicalName, other.LogicalName)) return false;
            if (!Repository.EqualsStringEmptyIsNull(SourceFile, other.SourceFile)) return false;
            if (!Repository.EqualsWithNull(Query, other.Query)) return false;
            if (PersistQuery != other.PersistQuery) return false;
            if (!Repository.EqualsDoubleArray(Levels, other.Levels)) return false;
            if (!Repository.EqualsDoubleArray(LevelFilters, other.LevelFilters)) return false;
            if (!Repository.EqualsArray(Columns, other.Columns)) return false;
            if (Disabled != other.Disabled) return false;
            if (!Repository.EqualsArray(Transformations, other.Transformations)) return false;
            if (!Repository.EqualsArray(LoadGroups, other.LoadGroups)) return false;
            if (!Repository.EqualsArray(SubGroups, other.SubGroups)) return false;
            if (WebOptional != other.WebOptional) return false;
            if (WebReadOnly != other.WebReadOnly) return false;
            if (Consolidation != other.Consolidation) return false;
            if (Transactional != other.Transactional) return false;
            if (TruncateOnLoad != other.TruncateOnLoad) return false;
            if (!Repository.EqualsWithNull(SourceGroups, other.SourceGroups)) return false;
            if (!Repository.EqualsWithNull(ChecksumGroup, other.ChecksumGroup)) return false;
            if (IncrementalSnapshotFact != other.IncrementalSnapshotFact) return false;
            if (!Repository.EqualsArray(SnapshotDeleteKeys, other.SnapshotDeleteKeys)) return false;
            if (!Repository.EqualsArray(OverrideLevelKeys, other.OverrideLevelKeys)) return false;
            if (!Repository.EqualsStringEmptyIsNull(SCDDateName, other.SCDDateName)) return false;
            if (!ScriptDefinition.EqualScripts(Script, other.Script)) return false;
            if (!Repository.EqualsWithNull(Snapshots, other.Snapshots)) return false;
            if (!Repository.EqualsStringEmptyIsNull(CustomTimeColumn, other.CustomTimeColumn)) return false;
            if (!Repository.EqualsStringEmptyIsNull(CustomTimePrefix, other.CustomTimePrefix)) return false;
            if (!Repository.EqualsStringEmptyIsNull(NewColumnTarget, other.NewColumnTarget)) return false;
            if (!Repository.EqualsArray(CustomTimeShifts, other.CustomTimeShifts)) return false;
            if (!Repository.EqualsArray(SourceKey, other.SourceKey)) return false;
            if (!Repository.EqualsArray(ForeignKeys, other.ForeignKeys)) return false;
            if (!Repository.EqualsArray(FactForeignKeys, other.FactForeignKeys)) return false;
            if (!Repository.EqualsArray(ForeignKeySources, other.ForeignKeySources)) return false;
            if (!Repository.EqualsArray(FactForeignKeySources, other.FactForeignKeySources)) return false;
            if (!Repository.EqualsStringEmptyIsNull(ParentForeignKeySource, other.ParentForeignKeySource)) return false;
            if (!Repository.EqualsStringEmptyIsNull(HierarchyName, other.HierarchyName)) return false;
            if (!Repository.EqualsStringEmptyIsNull(LevelName, other.LevelName)) return false;
            if (ExcludeFromModel != other.ExcludeFromModel) return false;
            if (DiscoveryTable != other.DiscoveryTable) return false;
            if (UseBirstLocalForSource != other.UseBirstLocalForSource) return false;
            if (!Repository.EqualsArray(Locations, other.Locations)) return false;
            if (Autoplace != other.Autoplace) return false;
            if (!Repository.EqualsStringEmptyIsNull(MeasureNamePrefix, other.MeasureNamePrefix)) return false;
            if (LiveAccess != other.LiveAccess) return false;
            if (UnCached != other.UnCached) return false;
            if (!Repository.EqualsWithNull(TableSource, other.TableSource)) return false;
            if (!Repository.EqualsWithNull(InheritTables, other.InheritTables)) return false;
            if (Cardinality != other.Cardinality) return false;
            if (!Repository.EqualsWithNull(RServerPath, other.RServerPath)) return false;
            if (!Repository.EqualsWithNull(RExpression, other.RExpression)) return false;
            if (!Repository.EqualsWithNull(RServerSettings, other.RServerSettings)) return false;
            return true;
        }

        public List<DiffObject> diffDescription(StagingTable currentst)
        {
            List<DiffObject> diffs = new List<DiffObject>();
            if (SourceType != currentst.SourceType)
                diffs.Add(new DiffObject("SourceType", SOURCE_FILE_TYPE_NAMES[SourceType], SOURCE_FILE_TYPE_NAMES[currentst.SourceType]));
            if (!Repository.EqualsStringEmptyIsNull(Name, currentst.Name))
                diffs.Add(new DiffObject("Name", Name, currentst.Name));
            if (!Repository.EqualsStringEmptyIsNull(LogicalName, currentst.LogicalName))
                diffs.Add(new DiffObject("Logical Name", LogicalName, currentst.LogicalName));
            if (!Repository.EqualsStringEmptyIsNull(SourceFile, currentst.SourceFile))
                diffs.Add(new DiffObject("Source File", SourceFile, currentst.SourceFile));
            if (!Repository.EqualsWithNull(Query, currentst.Query))
                diffs.Add(new DiffObject("Query", Query, currentst.Query));
            if (PersistQuery != currentst.PersistQuery)
                diffs.Add(new DiffObject("Persist Query", PersistQuery, currentst.PersistQuery));
            if (!Repository.EqualsDoubleArray(Levels, currentst.Levels))
                diffs.Add(new DiffObject("Grain", SerializeGrain(Levels), SerializeGrain(currentst.Levels)));
            if (!Repository.EqualsDoubleArray(LevelFilters, currentst.LevelFilters))
                diffs.Add(new DiffObject("Level Filters", SerializeLevelFilters(LevelFilters), SerializeLevelFilters(currentst.LevelFilters)));
            if (Disabled != currentst.Disabled)
                diffs.Add(new DiffObject("Disabled", Disabled, currentst.Disabled));
            if (!Repository.EqualsArray(Transformations, currentst.Transformations))
                diffs.Add(new DiffObject("Transformations", "", "")); // Need more sophisticated logic
            if (!Repository.EqualsArray(LoadGroups, currentst.LoadGroups))
                diffs.Add(new DiffObject("Load Groups", LoadGroups, currentst.LoadGroups));
            if (!Repository.EqualsArray(SubGroups, currentst.SubGroups))
                diffs.Add(new DiffObject("Sub Groups", SubGroups, currentst.SubGroups));
            if (WebOptional != currentst.WebOptional)
                diffs.Add(new DiffObject("Web Optional", WebOptional, currentst.WebOptional));
            if (WebReadOnly != currentst.WebReadOnly)
                diffs.Add(new DiffObject("Web Read Only", WebReadOnly, currentst.WebReadOnly));
            if (Consolidation != currentst.Consolidation)
                diffs.Add(new DiffObject("Consolidation", Consolidation, currentst.Consolidation));
            if (Transactional != currentst.Transactional)
                diffs.Add(new DiffObject("Transactional", Transactional, currentst.Transactional));
            if (TruncateOnLoad != currentst.TruncateOnLoad)
                diffs.Add(new DiffObject("Data From Last Load Only", TruncateOnLoad, currentst.TruncateOnLoad));
            if (!Repository.EqualsWithNull(SourceGroups, currentst.SourceGroups))
                diffs.Add(new DiffObject("Processing Groups", SourceGroups, currentst.SourceGroups));
            if (!Repository.EqualsWithNull(ChecksumGroup, currentst.ChecksumGroup))
                diffs.Add(new DiffObject("Checksum Group", ChecksumGroup, currentst.ChecksumGroup));
            if (IncrementalSnapshotFact != currentst.IncrementalSnapshotFact)
                diffs.Add(new DiffObject("Incremental Snapshot Fact", IncrementalSnapshotFact, currentst.IncrementalSnapshotFact));
            if (!Repository.EqualsArray(SnapshotDeleteKeys, currentst.SnapshotDeleteKeys))
                diffs.Add(new DiffObject("Snapshot Delete Keys", "", "")); // Need more sophisticated logic
            if (!Repository.EqualsArray(OverrideLevelKeys, currentst.OverrideLevelKeys))
                diffs.Add(new DiffObject("Override Level Keys", "", "")); // Need more sophisticated logic
            if (!Repository.EqualsStringEmptyIsNull(SCDDateName, currentst.SCDDateName))
                diffs.Add(new DiffObject("SCD Date Name", SCDDateName, currentst.SCDDateName));
            if (!ScriptDefinition.EqualScripts(Script, currentst.Script))
                diffs.Add(new DiffObject("Script", "", ""));
            if (!Repository.EqualsWithNull(Snapshots, currentst.Snapshots))
                diffs.Add(new DiffObject("Snapshot Policy", "", ""));
            if (!Repository.EqualsStringEmptyIsNull(CustomTimeColumn, currentst.CustomTimeColumn))
                diffs.Add(new DiffObject("Custom Time Column", CustomTimeColumn, currentst.CustomTimeColumn));
            if (!Repository.EqualsStringEmptyIsNull(CustomTimePrefix, currentst.CustomTimePrefix))
                diffs.Add(new DiffObject("Custom Time Prefix", CustomTimePrefix, currentst.CustomTimePrefix));
            if (!Repository.EqualsStringEmptyIsNull(NewColumnTarget, currentst.NewColumnTarget))
                diffs.Add(new DiffObject("New Column Target", NewColumnTarget, currentst.NewColumnTarget));
            if (!Repository.EqualsWithNull(TableSource, currentst.TableSource))
                diffs.Add(new DiffObject("Connection", "", ""));
            if (!Repository.EqualsArray(CustomTimeShifts, currentst.CustomTimeShifts))
                diffs.Add(new DiffObject("Custom Time Shifts", "", ""));
            if (!Repository.EqualsArray(SourceKey, currentst.SourceKey))
                diffs.Add(new DiffObject("Primary Key", SourceKey, currentst.SourceKey));
            if (!Repository.EqualsArray(ForeignKeys, currentst.ForeignKeys) ||
                !Repository.EqualsArray(FactForeignKeys, currentst.FactForeignKeys) ||
                !Repository.EqualsArray(ForeignKeySources, currentst.ForeignKeySources) ||
                !Repository.EqualsArray(FactForeignKeySources, currentst.FactForeignKeySources) ||
                !Repository.EqualsWithNull(ParentForeignKeySource, currentst.ParentForeignKeySource))
                diffs.Add(new DiffObject("Foreign Keys", "", ""));
            if (!Repository.EqualsStringEmptyIsNull(HierarchyName, currentst.HierarchyName))
                diffs.Add(new DiffObject("Hierachy Name", HierarchyName, currentst.HierarchyName));
            if (!Repository.EqualsStringEmptyIsNull(LevelName, currentst.LevelName))
                diffs.Add(new DiffObject("Level Name", LevelName, currentst.LevelName));
            if (ExcludeFromModel != currentst.ExcludeFromModel)
                diffs.Add(new DiffObject("Exclude From Model", ExcludeFromModel, currentst.ExcludeFromModel));
            if (DiscoveryTable != currentst.DiscoveryTable)
                diffs.Add(new DiffObject("Discovery", DiscoveryTable, currentst.DiscoveryTable));
            if (UseBirstLocalForSource != currentst.UseBirstLocalForSource)
                diffs.Add(new DiffObject("Use Birst Local For Source", UseBirstLocalForSource, currentst.UseBirstLocalForSource));
            if (LiveAccess != currentst.LiveAccess)
                diffs.Add(new DiffObject("Live Access", LiveAccess, currentst.LiveAccess));
            if (UnCached != currentst.UnCached)
                diffs.Add(new DiffObject("UnCached", UnCached, currentst.UnCached));
            if (!Repository.EqualsArray(Locations, currentst.Locations) ||
                Autoplace != currentst.Autoplace)
                diffs.Add(new DiffObject("Dataflow Placement", "", ""));
            if (!Repository.EqualsStringEmptyIsNull(MeasureNamePrefix, currentst.MeasureNamePrefix))
                diffs.Add(new DiffObject("Measure Name Prefix", LevelName, currentst.LevelName));
            if (!Repository.EqualsWithNull(InheritTables, currentst.InheritTables))
                diffs.Add(new DiffObject("Inheritance", "", ""));
            if (Cardinality != currentst.Cardinality)
                diffs.Add(new DiffObject("Cardinality", Cardinality, currentst.Cardinality));
            if (!Repository.EqualsWithNull(RServerPath, currentst.RServerPath))
                diffs.Add(new DiffObject("RServerPath", RServerPath, currentst.RServerPath));
            if (!Repository.EqualsWithNull(RExpression, currentst.RExpression))
                diffs.Add(new DiffObject("RExpression", RExpression, currentst.RExpression));
            if (!Repository.EqualsWithNull(RServerSettings, currentst.RServerSettings))
                diffs.Add(new DiffObject("RServerSettings", RServerSettings, currentst.RServerSettings));
            return diffs;
        }

        public void getDifferences(StagingTable currentst, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            string displayName = getDisplayName();
            // Object level differences
            List<DiffObject> diffs = diffDescription(currentst);
            if (diffs.Count > 0)
            {
                itemnames.Add(displayName);
                itempaths.Add(Name);
                itemtypes.Add("Data Source");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }

            // Column level differences
            foreach (StagingColumn csc in currentst.Columns)
            {
                bool found = false;
                foreach (StagingColumn ssc in Columns)
                {
                    if (ssc.Name == csc.Name)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    // Columns that only exist in the current source 
                    itemnames.Add(displayName + ": " + csc.Name);
                    itempaths.Add(Name + "|" + csc.Name);
                    itemtypes.Add("Source Column");
                    diffarr = new string[] { DiffObject.getCompareOnlyExists() };
                    differences.Add(diffarr);
                }
            }
            foreach (StagingColumn sc in Columns)
            {
                StagingColumn currentsc = null;
                foreach (StagingColumn ssc in currentst.Columns)
                {
                    if (ssc.Name == sc.Name)
                    {
                        currentsc = ssc;
                        break;
                    }
                }
                if (currentsc == null)
                {
                    // Columns that only exist in the compare source
                    itemnames.Add(displayName + ": " + sc.Name);
                    itempaths.Add(Name + "|" + sc.Name);
                    itemtypes.Add("Source Column");
                    diffarr = new string[] { DiffObject.getBaseOnlyExists() };
                    differences.Add(diffarr);
                    continue;
                }
                if (currentsc.Equals(sc))
                    continue;
                itemnames.Add(displayName + ": " + sc.Name);
                itempaths.Add(Name + "|" + sc.Name);
                itemtypes.Add("Source Column");
                diffs = currentsc.diffDescription(sc);
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool matchesLevels(string[][] clevels)
        {
            if (Levels.Length != clevels.Length)
                return false;
            foreach (string[] level in Levels)
            {
                bool found = false;
                foreach (string[] clevel in clevels)
                {
                    if (clevel[0] == level[0] && clevel[1] == level[1])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }

        public StagingColumn getStagingColumn(string sfColumnName)
        {
            foreach (StagingColumn sc in Columns)
            {
                if (sc.SourceFileColumn == sfColumnName)
                {
                    return sc;
                }
            }
            return null;
        }

        public int getStagingColumnIndex(string sfColumnName)
        {
            for (int i = 0; i < Columns.Length; i++)
            {
                StagingColumn sc = Columns[i];
                if (sc.SourceFileColumn == sfColumnName)
                {
                    return i;
                }
            }
            return -1;
        }

        public string getDiscoveryColumnPhysicalName(string logicalName, int builderVersion)
        {
            foreach (StagingColumn sc in Columns)
            {
                if (sc.Name == logicalName)
                {
                    if (LiveAccess)
                    {
                        if (TableSource != null && TableSource.Tables.Length == 1)
                        {
                            // For single table sources, strip off the qualifier
                            int index = sc.SourceFileColumn.IndexOf('.');
                            if (index < 0)
                                return sc.SourceFileColumn;
                            return sc.SourceFileColumn.Substring(index + 1);
                        }
                        else
                        {
                            return sc.SourceFileColumn;
                        }
                    }
                    else
                    {
                        return Util.generatePhysicalName(sc.getPhysicalColumnName(), builderVersion) + ApplicationBuilder.PHYSICAL_SUFFIX;
                    }
                }
            }
            return null;
        }

        public string getDiscoveryColumnDateID(string logicalName, int builderVersion)
        {
            return Util.generatePhysicalName(logicalName + " Day ID", builderVersion) + ApplicationBuilder.PHYSICAL_SUFFIX;
        }

        public void renameColumn(string oldName, string newName)
        {
            if (SourceKey != null)
            {
                for (int i = 0; i < SourceKey.Length; i++)
                {
                    if (SourceKey[i] == oldName)
                        SourceKey[i] = newName;
                }
            }
            if (Columns != null)
                foreach (StagingColumn sc in Columns)
                {
                    if (sc.Name == oldName)
                        sc.Name = newName;
                }
            if (CustomTimeColumn == oldName)
                CustomTimeColumn = newName;
        }

        public override string toAuditString()
        {
            string ret = "StagingTable ";
            ret += Name;
            ret += " ";
            ret += Guid;
            return ret;
        }

        public string getDisplayName()
        {
            string fname = null;
            int extindex = SourceFile.LastIndexOf('.');
            if (extindex >= 0)
                fname = SourceFile.Substring(0, extindex);
            else
                fname = SourceFile;
            return fname;
        }
    }

    [Serializable]
    public class StagingFilter : BaseObject
    {
        public string Name;
        public string Condition;

        public StagingFilter()
        {
        }

        public StagingFilter(string name)
        {
            this.Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(StagingFilter).Equals(obj.GetType())) return false;
            StagingFilter other = (StagingFilter)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(Condition, other.Condition)) return false;

            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class StagingAggregation : BaseObject
    {
        public string Type;
        public string Default;
        public string[] ValueList;
        public string[][] SetList;

        public StagingAggregation()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(StagingAggregation).Equals(obj.GetType())) return false;
            StagingAggregation other = (StagingAggregation)obj;
            if (!Repository.EqualsWithNull(Type, other.Type)) return false;
            if (!Repository.EqualsWithNull(Default, other.Default)) return false;
            if (!Repository.EqualsArray(ValueList, other.ValueList)) return false;
            if (!Repository.EqualsDoubleArray(SetList, other.SetList)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Type;
        }
    }

    [Serializable]
    public class StagingColumnGrainInfo : BaseObject
    {
        public static int TYPE_PREFIX = 0;
        public static int TYPE_RENAME = 1;
        public string Dimension;
        public string Level;
        public string StagingFilterName;
        public string NewName;
        public int NewNameType;
        public StagingAggregation Aggregation;

        public StagingColumnGrainInfo()
        {
        }

        public StagingColumnGrainInfo(string stagingFilterName, string dimension, string level, string name, int nameType,
            StagingAggregation aggregation)
        {
            this.StagingFilterName = stagingFilterName;
            this.Dimension = dimension;
            this.Level = level;
            this.NewName = name;
            this.NewNameType = nameType;
            this.Aggregation = aggregation;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(StagingColumnGrainInfo).Equals(obj.GetType())) return false;
            StagingColumnGrainInfo other = (StagingColumnGrainInfo)obj;
            if (!Repository.EqualsWithNull(StagingFilterName, other.StagingFilterName)) return false;
            if (!Repository.EqualsWithNull(Dimension, other.Dimension)) return false;
            if (!Repository.EqualsWithNull(Level, other.Level)) return false;
            if (!Repository.EqualsWithNull(NewName, other.NewName)) return false;
            if (NewNameType != other.NewNameType) return false;
            if (!Repository.EqualsWithNull(Aggregation, other.Aggregation)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public ListViewItem getItem()
        {
            List<string> itemList = new List<string>();
            itemList.Add(Dimension);
            itemList.Add(Level);
            itemList.Add(StagingFilterName);
            itemList.Add(Aggregation == null ? "" : Aggregation.Type);
            if (NewName != null && NewName.Trim().Length > 0)
                itemList.Add((NewNameType == TYPE_PREFIX ? "Prefix:" : "Name:") + NewName);
            ListViewItem lvi = new ListViewItem(itemList.ToArray());
            lvi.Tag = this;
            return (lvi);
        }

        public override string ToString()
        {
            return Dimension + "/" + Level;
        }
    }

    [Serializable]
    public class DisplayMap : BaseObject
    {
        public static int TYPE_MAP = 0;
        public static int TYPE_BUCKET = 1;
        public static int TYPE_QUERY = 2;
        public static int TYPE_LOOKUP = 3;
        public int Type;
        public string[] Keys;
        public string[] MappedValues;
        public double[][] Buckets;
        public string Query;
        public string LookupTable;
        public string LookupExpression;
        public string LookupJoinColumn;
        public string LookupFilterExpression;
        public bool LookupOuterJoin;
        public string Default;
        public string DataType;

        public DisplayMap()
        {
            Type = TYPE_MAP;
            DataType = "Varchar";
            Keys = new string[0];
            MappedValues = new string[0];
        }

        public override bool Equals(Object obj)
        {
            if (obj == null || obj == System.DBNull.Value) return false;
            if (!typeof(DisplayMap).Equals(obj.GetType())) return false;
            DisplayMap other = (DisplayMap)obj;
            if (Type != other.Type) return false;
            if (!Repository.EqualsArray(Keys, other.Keys)) return false;
            if (!Repository.EqualsArray(MappedValues, other.MappedValues)) return false;
            if (!Repository.EqualsWithNull(Query, other.Query)) return false;
            if (!Repository.EqualsWithNull(Default, other.Default)) return false;
            if (!Repository.EqualsDoubleValueArray(Buckets, other.Buckets)) return false;
            if (!Repository.EqualsWithNull(LookupTable, other.LookupTable)) return false;
            if (!Repository.EqualsWithNull(LookupExpression, other.LookupExpression)) return false;
            if (!Repository.EqualsWithNull(LookupJoinColumn, other.LookupJoinColumn)) return false;
            if (!Repository.EqualsWithNull(LookupFilterExpression, other.LookupFilterExpression)) return false;
            if (!Repository.EqualsWithNull(DataType, other.DataType)) return false;
            if (LookupOuterJoin != other.LookupOuterJoin) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return ("Map(" + (Type == TYPE_MAP || Type == TYPE_BUCKET ? MappedValues.Length.ToString() : "Q") + ")");
        }
    }

    [Serializable]
    public class Dimension : BaseObject
    {
        public string Name;

        public Dimension()
        {
        }

        public Dimension(string name)
        {
            this.Name = name;
        }
    }

    [Serializable]
    public class MeasureTableGrain : BaseObject
    {
        public MeasureTableGrain()
        {
        }
        public string DimensionName;
        public string DimensionLevel;

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(MeasureTableGrain).Equals(obj.GetType())) return false;
            MeasureTableGrain other = (MeasureTableGrain)obj;
            if (!DimensionName.Equals(other.DimensionName)) return false;
            if (!DimensionLevel.Equals(other.DimensionLevel)) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class MeasureGrain : BaseObject
    {
        public MeasureTableGrain[] measureTableGrains;
        public string measureTableName;
        public string measureColumnName;
        public bool transactional;
        public bool currentSnapshotOnly;
        public string[] LoadGroups;
        public SecurityFilter filter;
        [XmlIgnore]
        [NonSerialized]
        public string connection;
        [XmlIgnore]
        [NonSerialized]
        public Dictionary<string, List<string>> overrideKeyColumns = new Dictionary<string, List<string>>();

        public MeasureGrain()
        {
        }

        public bool atSameGrain(Object obj)
        {
            if (obj == null) return false;
            MeasureGrain other = (MeasureGrain)obj;
            if (connection != other.connection)
                return false;
            int matchCount = 0;
            foreach (MeasureTableGrain mt in measureTableGrains)
            {
                foreach (MeasureTableGrain othermt in other.measureTableGrains)
                {
                    if (mt.Equals(othermt))
                    {
                        matchCount++;
                        break;
                    }
                }
            }
            if (matchCount != measureTableGrains.Length || measureTableGrains.Length != other.measureTableGrains.Length) return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(MeasureGrain).Equals(obj.GetType())) return false;
            MeasureGrain other = (MeasureGrain)obj;
            if (!Repository.EqualsArray(measureTableGrains, other.measureTableGrains)) return false;
            if (measureTableName != other.measureTableName) return false;
            if (measureColumnName != other.measureColumnName) return false;
            if (transactional != other.transactional) return false;
            if (currentSnapshotOnly != other.currentSnapshotOnly) return false;
            if (!Repository.EqualsWithNull(filter, other.filter)) return false;
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ReferencePopulation : BaseObject
    {
        public string TargetDimension;
        public string Level;
        public PeerItem[] PerformancePeerColumns;
        public PeerItem[] SuccessPatternPeerColumns;
        public int NumPerformancePeers = 100;
        public int NumSuccessPatternPeers = 100;
        public string SuccessMeasure;
        public string[] PeeringFilters;
        public string PopulationName;
        public string PerformanceTable;
        public string SuccessPatternTable;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(ReferencePopulation).Equals(obj.GetType())) return false;
            ReferencePopulation other = (ReferencePopulation)obj;
            if (!Repository.EqualsWithNull(this.TargetDimension, other.TargetDimension)) return false;
            if (!Repository.EqualsWithNull(this.Level, other.Level)) return false;
            if (!Repository.EqualsArray(this.PerformancePeerColumns, other.PerformancePeerColumns)) return false;
            if (!Repository.EqualsArray(this.SuccessPatternPeerColumns, other.SuccessPatternPeerColumns)) return false;
            if (this.NumPerformancePeers != other.NumPerformancePeers) return false;
            if (this.NumSuccessPatternPeers != other.NumSuccessPatternPeers) return false;
            if (!Repository.EqualsWithNull(this.SuccessMeasure, other.SuccessMeasure)) return false;
            if (!Repository.EqualsArray(this.PeeringFilters, other.PeeringFilters)) return false;
            if (!Repository.EqualsWithNull(this.PopulationName, other.PopulationName)) return false;
            if (!Repository.EqualsWithNull(this.PerformanceTable, other.PerformanceTable)) return false;
            if (!Repository.EqualsWithNull(this.SuccessPatternTable, other.SuccessPatternTable)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class Transformation : BaseObject
    {
        public int ExecuteAfter;

        public const int AFTER_FILES_STAGED = 1;
        public const int AFTER_QUERY_BASED_TABLES_GENERATED = 2;
        public const int AFTER_UNKNOWN_KEYS_REPLACED = 3;
        public const int AFTER_RANKS_SET = 4;
        public const int AFTER_CHECKSUMS_UPDATED = 5;
        public const int DO_NOT_EXECUTE = 6;

        public override bool Equals(Object obj)
        {
            if (obj == null || obj == System.DBNull.Value) return false;
            Transformation other = (Transformation)obj;
            if (this.ExecuteAfter != other.ExecuteAfter) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        internal string toAuditString()
        {
            string ret = "Execute after ";
            switch (ExecuteAfter)
            {
                case AFTER_FILES_STAGED:
                    ret += "files stages";
                    break;
                case AFTER_QUERY_BASED_TABLES_GENERATED:
                    ret += "query based tables generated";
                    break;
                case AFTER_UNKNOWN_KEYS_REPLACED:
                    ret += "unknown keys replaced";
                    break;
                case AFTER_RANKS_SET:
                    ret += "ranks set";
                    break;
                case AFTER_CHECKSUMS_UPDATED:
                    ret += "checksums updated";
                    break;
                case DO_NOT_EXECUTE:
                    ret = "Do not execute";
                    break;
            }
            return ret;
        }
    }

    [Serializable]
    public class Expression : Transformation
    {
        public string Formula;

        public override string ToString()
        {
            return "Expression";
        }
    }

    [Serializable]
    public class DateID : Transformation
    {
        public string DateExpression;
        public int DateType;

        public override string ToString()
        {
            return "Date ID";
        }
    }

    [Serializable]
    public class LogicalColumnDefinition : Transformation
    {
        public string Formula;

        public override string ToString()
        {
            return "Logical Column Definition";
        }
    }

    [Serializable]
    public class TableLookup : Transformation
    {
        public string Formula;
        public string LookupTable;
        public string JoinCondition;

        public override string ToString()
        {
            return "Lookup";
        }
    }

    [Serializable]
    public class Procedure : Transformation
    {
        public string Name;
        public string Body;

        public override string ToString()
        {
            return "Procedure: " + Name;
        }
    }

    [Serializable]
    public class NameValuePair
    {
        public string Name;
        public string Value;
        public NameValuePair()
        {
        }
        public NameValuePair(String nm, String val)
        {
            Name = nm;
            Value = val;
        }
        public override string ToString()
        {
            return Name + "=" + Value;
        }
    }

    [Serializable]
    public class JavaTransform : Transformation
    {
        public string Name;
        public string ClassName;
        public NameValuePair[] Parameters;
        public override string ToString()
        {
            return "Java: " + Name;
        }
    }

    [Serializable]
    public class Rank : Transformation
    {
        public static int RankType = 0;
        public static int NTileType = 1;
        public int Type;
        public int NTileNum;
        public string[] PartitionByColumns;
        public string[] OrderByColumns;
        public bool[] OrderByAscending;

        public override string ToString()
        {
            return "Rank: " + (Type == RankType ? "Rank" : "NTile(" + NTileNum + ")");
        }
    }

    [Serializable]
    public class Household : Transformation
    {
        public string KeyColumn;
        public string HHIDColumn;
        public string Address;
        public string City;
        public string State;
        public string Zip;
        public string TargetTable;
        public string TargetKeyColumn;
        public string TargetHHIDColumn;
        public string TargetAddress;
        public string TargetCity;
        public string TargetState;
        public string TargetZip;

        public override string ToString()
        {
            return "Household";
        }
    }

    [Serializable]
    public class TimePeriod
    {
        public const int AGG_TYPE_NONE = 0;
        public const int AGG_TYPE_TRAILING = 1;
        public const int AGG_TYPE_TO_DATE = 2;
        public const int PERIOD_DAY = 0;
        public const int PERIOD_WEEK = 1;
        public const int PERIOD_MONTH = 2;
        public const int PERIOD_QUARTER = 3;
        public const int PERIOD_HALF_YEAR = 4;
        public const int PERIOD_YEAR = 5;

        public int AggregationType;
        public int AggregationPeriod;
        public int AggregationNumPeriods;

        public int ShiftAmount;
        public int ShiftPeriod;
        public bool ShiftAgo;

        public string Prefix;

        public TimePeriod()
        {
        }

        public TimePeriod(string prefix, int aggregationType, int aggregationPeriod, int aggregationNumPeriods, int shiftAmount, int shiftPeriod, bool shiftAgo)
        {
            this.Prefix = prefix;
            this.AggregationType = aggregationType;
            this.AggregationPeriod = aggregationPeriod;
            this.AggregationNumPeriods = aggregationNumPeriods;
            this.ShiftAmount = shiftAmount;
            this.ShiftPeriod = shiftPeriod;
            this.ShiftAgo = shiftAgo;
        }

        public static string getPeriodString(int period)
        {
            switch (period)
            {
                case PERIOD_DAY:
                    return ("Day");
                case PERIOD_WEEK:
                    return ("Week");
                case PERIOD_MONTH:
                    return ("Month");
                case PERIOD_QUARTER:
                    return ("Quarter");
                case PERIOD_HALF_YEAR:
                    return ("Half-year");
                case PERIOD_YEAR:
                    return ("Year");
            }
            return (null);
        }

        public int getPeriod(string periodString)
        {
            if (periodString.StartsWith("Day"))
                return PERIOD_DAY;
            else if (periodString.StartsWith("Week"))
                return PERIOD_WEEK;
            else if (periodString.StartsWith("Year"))
                return PERIOD_YEAR;
            else if (periodString.StartsWith("Quarter"))
                return PERIOD_QUARTER;
            else if (periodString.StartsWith("Half-year"))
                return PERIOD_HALF_YEAR;
            return (PERIOD_MONTH);
        }

        public ListViewItem getItem()
        {
            List<string> itemList = new List<string>();
            switch (AggregationType)
            {
                case AGG_TYPE_NONE:
                    itemList.Add("");
                    break;
                case AGG_TYPE_TRAILING:
                    itemList.Add("Trailing " + AggregationNumPeriods + " " + getPeriodString(AggregationPeriod));
                    break;
                case AGG_TYPE_TO_DATE:
                    itemList.Add(getPeriodString(AggregationPeriod) + " to date");
                    break;
            }
            if (ShiftAmount != 0)
            {
                itemList.Add(ShiftAmount + " " + getPeriodString(ShiftPeriod) + (ShiftAgo ? " ago" : " ahead"));
            }
            else itemList.Add("");
            itemList.Add(Prefix);
            ListViewItem lvi = new ListViewItem(itemList.ToArray());
            lvi.Tag = this;
            return (lvi);
        }
    }

    [Serializable]
    public class TimeDefinition : BaseObject
    {
        // Whether to generate a time dimension automatically
        public bool GenerateTimeDimension;

        // Name of dimension
        public string Name = "Time";

        // Levels to instantiate
        public bool Day;
        public bool Week;
        public bool Month;
        public bool Quarter;
        public bool Halfyear;
        public bool Year;

        // Array of selected time periods to analyze
        public TimePeriod[] Periods;

        //Week properties
        public string WeekDeterminantDay;
        public string FirstDayOfWeek;

        // Other time parameters
        public DateTime StartDate; // Beginning of time
        public DateTime EndDate;   // End of time

        // Partition parameters
        public bool PartitionFacts;
        public string PartitionPeriod;  // String representing periodicity (e.g. Month)
        public DateTime PartitionStart;      // Date to start partitioning
        public DateTime PartitionEnd;        // Date to end partitioning

        public TimeDefinition()
        {
            StartDate = new DateTime(1900, 1, 1);
            EndDate = new DateTime(2050, 12, 31);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(TimeDefinition).Equals(obj.GetType())) return false;
            TimeDefinition other = (TimeDefinition)obj;
            if (GenerateTimeDimension != other.GenerateTimeDimension) return false;
            if (Name != other.Name) return false;
            if (Day != other.Day) return false;
            if (Week != other.Week) return false;
            if (Month != other.Month) return false;
            if (Quarter != other.Quarter) return false;
            if (Halfyear != other.Halfyear) return false;
            if (Year != other.Year) return false;
            if (!Repository.EqualsArray(Periods, other.Periods)) return false;
            if (WeekDeterminantDay != other.WeekDeterminantDay) return false;
            if (FirstDayOfWeek != other.FirstDayOfWeek) return false;
            if (!Repository.EqualsWithNull(StartDate, other.StartDate)) return false;
            if (!Repository.EqualsWithNull(EndDate, other.EndDate)) return false;
            if (PartitionFacts != other.PartitionFacts) return false;
            if (!Repository.EqualsWithNull(PartitionPeriod, other.PartitionPeriod)) return false;
            if (!Repository.EqualsWithNull(PartitionStart, other.PartitionStart)) return false;
            if (!Repository.EqualsWithNull(PartitionEnd, other.PartitionEnd)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool isDefault()
        {
            return this.Equals(new TimeDefinition());
        }

        public string getLowestSelectedLevel()
        {
            string lowestSelectedLevel = null;
            if (Day)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_DAY);
            }
            else if (Week)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_WEEK);
            }
            else if (Month)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_MONTH);
            }
            else if (Quarter)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_QUARTER);
            }
            else if (Halfyear)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_HALF_YEAR);
            }
            else if (Year)
            {
                lowestSelectedLevel = TimePeriod.getPeriodString(TimePeriod.PERIOD_YEAR);
            }
            return lowestSelectedLevel;
        }
    }

    [Serializable]
    public class Broadcast : BaseObject
    {
        public string Name;
        public string From;
        public string Subject;
        public string Body;
        public string Report;
        public string Query;

        public Broadcast clone()
        {
            Broadcast bc = new Broadcast();
            bc.Name = Name;
            bc.From = From;
            bc.Subject = Subject;
            bc.Body = Body;
            bc.Report = Report;
            bc.Query = Query;
            return bc;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(Broadcast).Equals(obj.GetType())) return false;
            Broadcast other = (Broadcast)obj;
            if (Name != other.Name) return false;
            if (From != other.From) return false;
            if (Subject != other.Subject) return false;
            if (Body != other.Body) return false;
            if (Report != other.Report) return false;
            if (Query != other.Query) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public Broadcast()
        {
            Name = "New Broadcast";
            From = "Sender";
            Subject = "Subject";
            Report = "V{CatalogDir}/...";
            Body = "Add a optional message here";
            Query = "Logical Query that returns Username,Email";
        }
    }

    [Serializable]
    public class Script : BaseObject
    {
        public string Name;
        public string Text;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(Script).Equals(obj.GetType())) return false;
            Script other = (Script)obj;
            if (Name != other.Name) return false;
            if (Text != other.Text) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    [Serializable]
    public class ScriptGroup : BaseObject
    {
        public string Name;
        public Script[] Scripts;

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!typeof(ScriptGroup).Equals(obj.GetType())) return false;
            ScriptGroup other = (ScriptGroup)obj;
            if (Name != other.Name) return false;
            if (!Repository.EqualsArray(Scripts, other.Scripts)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    // Wrapper object to display staging column grain info string on the grid view column button
    [Serializable]
    public class StagingColumnGrainInfoDisplay : BaseObject
    {
        public StagingColumnGrainInfo[] scGrainInfo;

        public override bool Equals(object obj)
        {
            if (obj == null || obj == System.DBNull.Value) return false;
            if (!typeof(StagingColumnGrainInfoDisplay).Equals(obj.GetType())) return false;
            StagingColumnGrainInfoDisplay other = (StagingColumnGrainInfoDisplay)obj;
            if (!Repository.EqualsArray(scGrainInfo, other.scGrainInfo)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            if (scGrainInfo == null || scGrainInfo.Length == 0)
            {
                return "";
            }
            else if (scGrainInfo.Length > 1)
            {
                return "<Multiple>";
            }
            else
            {
                return scGrainInfo[0].ToString();
            }
        }
    }

    [Serializable]
    public class FilteredMeasureDefinition
    {
        public string[] Names;
        public QueryFilter[] Filters;
        public string[] Defaults;

        public override bool Equals(object obj)
        {
            if (obj == null || obj == System.DBNull.Value) return false;
            if (!typeof(FilteredMeasureDefinition).Equals(obj.GetType())) return false;
            FilteredMeasureDefinition other = (FilteredMeasureDefinition)obj;
            if (!Repository.EqualsArray(Names, other.Names)) return false;
            if (!Repository.EqualsArray(Filters, other.Filters)) return false;
            if (!Repository.EqualsArray(Defaults, other.Defaults)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Filtered";
        }
    }

    public enum ChartType
    {
        None, Bar, Column, Line, BarLine, Pie, StackedBar, StackedColumn, Area, StackedArea, Scatterplot, BubbleChart, ContourPlot, SpiderWebPlot, MeterPlot,
        HeatMap, StackedColumnLine
    }

    [Serializable]
    public class QuickReportDimension
    {
        public string DimensionName;
        public string ColumnName;
        public int OrderBy;
    }

    [Serializable]
    public class QuickReportMeasure
    {
        public string MeasureName;
        public string MeasureLabel;
        public int OrderBy;
        public string BaseMeasure;
        public string AggregationRule;
        public string TimePrefix;
    }

    [Serializable]
    public class QuickReportExpression
    {
        public string Formula;
        public string Label;
        public int OrderBy;
    }

    [Serializable]
    public class DisplayFilter
    {
        public static string TYPE_LOGICAL_OP = "LogicalOperator";
        public static string TYPE_PREDICATE = "Predicate";
        public static string OPERATOR_AND = "And";
        public static string OPERATOR_OR = "Or";
        public static string OPERATOR_EQ = "Equal";
        public static string OPERATOR_GT = "Greater than";
        public static string OPERATOR_LT = "Less than";
        public static string OPERATOR_GTE = "Greater than or equal";
        public static string OPERATOR_LTE = "Less than or equal";

        public string TableName;
        public string ColumnName;
        public string Type;
        public string Operator;
        public string Operand;
        public string DataType; // Integer, Float, Date, DateTime
        public DisplayFilter[] FilterList;
    }

    [Serializable]
    public class QuickReport
    {
        public static int TYPE_MEASURE_BY_DIMENSION = 0;
        public static int SORT_NONE = 0;
        public static int SORT_ASC = 1;
        public static int SORT_DESC = 2;
        public ChartType ChartType = ChartType.None;
        public string Title = null;
        public int Type;
        public QuickReportDimension[] DimensionColumns;
        public QuickReportMeasure[] Measures;
        public QuickReportExpression[] Expressions;
        public bool ShowTable;
        public QueryFilter Filter;
        public DisplayFilter DisplayFilter;
    }

    [Serializable]
    public class QuickPrompt
    {
        public static int PROMPT_TYPE_DIMENSION_COLUMN = 0;
        public int Type;
        public QuickReportDimension DimensionColumn;
        public string VisibleName;
    }

    [Serializable]
    public class QuickDashboard
    {
        public static int TYPE_FOUR_REPORTS = 0;
        public int Type;
        public string DashboardName;
        public string PageName;
        public QuickPrompt[] Prompts;
        public QuickReport[] Reports;
        public QueryFilter Filter;
    }

    [Serializable]
    public class LogicalExpression : BaseAuditableObject
    {
        public static int TYPE_MEASURE = 0;
        public static int TYPE_DIMENSION = 1;

        public int Type;
        public string AggregationRule;
        public string[][] Levels;
        public string Expression;

        public override string toAuditString()
        {
            string ret = "LogicalExpression ";
            ret += Name;
            ret += " ";
            ret += Guid;
            ret += " Type=";
            if (Type == TYPE_DIMENSION)
                ret += "Dimension";
            else
                ret += "Measure";
            ret += " AggregationRule=";
            ret += AggregationRule;
            if (Levels != null && Levels.Length > 0)
            {
                ret += " Levels=[";
                foreach (string[] l in Levels)
                {
                    if (l.Length == 2)
                    {
                        ret += "[";
                        ret += l[0];
                        ret += ", ";
                        ret += l[1];
                        ret += "]";
                    }
                    else
                    {
                        ret += "[";
                        ret += l[0];
                        ret += "]";
                    }
                }
                ret += "]";
            }
            ret += " Expression=";
            ret += Expression;
            return ret;
        }

        public override bool IsEqualTo(object obj)
        {
            if (base.Equals(obj) && obj is LogicalExpression)
            {
                LogicalExpression other = (LogicalExpression)obj;
                if (Levels == null && other.Levels == null)
                    return true;

                if (Levels != null && other.Levels != null)
                {
                    if (Levels.Length == other.Levels.Length)
                    {
                        for (int i = 0; i < Levels.Length; i++)
                        {
                            String[] currentLevel = Levels[i];
                            String[] otherLevel = other.Levels[i];
                            if (currentLevel == null && otherLevel == null)
                                continue;

                            if (currentLevel != null && otherLevel != null && currentLevel.Length == otherLevel.Length)
                            {
                                for (int j = 0; j < currentLevel.Length; j++)
                                {
                                    if (currentLevel[j].Equals(otherLevel[j]))
                                        continue;

                                    return false;
                                }
                            }
                            else
                                return false;
                        }
                        return true;
                    }
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            if (!typeof(LogicalExpression).Equals(obj.GetType())) return false;
            LogicalExpression other = (LogicalExpression)obj;
            if (!Repository.EqualsWithNull(Name, other.Name)) return false;
            if (!Repository.EqualsWithNull(AggregationRule, other.AggregationRule)) return false;
            if (!Repository.EqualsWithNull(Expression, other.Expression)) return false;
            string ls1 = getLevelString();
            string ls2 = other.getLevelString();
            if (ls1 != ls2)
                return false;
            return true;
        }

        public string getLevelString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string[] level in Levels)
            {
                if (level.Length == 2)
                {
                    if (sb.Length > 0)
                        sb.Append(',');
                    sb.Append("[" + level[0] + "." + level[1] + "]");
                }
            }
            return sb.ToString();
        }

        public List<DiffObject> diffDescription(LogicalExpression other)
        {
            List<DiffObject> differences = new List<DiffObject>();
            if (other == null) return null;
            if (!typeof(LogicalExpression).Equals(other.GetType())) return null;

            if (!Repository.EqualsWithNull(Name, other.Name))
                differences.Add(new DiffObject("Name", Name, other.Name));
            if (!Repository.EqualsWithNull(AggregationRule, other.AggregationRule))
                differences.Add(new DiffObject("Aggregation Rule", AggregationRule, other.AggregationRule));
            if (!Repository.EqualsWithNull(Expression, other.Expression))
                differences.Add(new DiffObject("Expression", Expression, other.Expression));
            string ls1 = getLevelString();
            string ls2 = other.getLevelString();

            if (ls1 != ls2)
                differences.Add(new DiffObject("Assigned Levels", ls1, ls2));

            if (differences.Count == 0)
            {
                DiffObject dobj = new DiffObject((Type == TYPE_DIMENSION ? "Custom Attribute" : "Custom Measure") + " [" + Name + "] Other", "", "");
                differences.Add(dobj);
            }
            return differences;
        }

        public void getDifferences(LogicalExpression currentle, List<string> itemnames, List<string> itempaths, List<string> itemtypes, List<string[]> differences)
        {
            string[] diffarr = null;

            // Object level differences
            List<DiffObject> diffs = diffDescription(currentle);
            if (diffs.Count > 0)
            {
                itemnames.Add(Name);
                itempaths.Add((Type == TYPE_DIMENSION ? "Custom Attribute" : "Custom Measure") + "|" + Name);
                itemtypes.Add(Type == TYPE_DIMENSION ? "Custom Attribute" : "Custom Measure");
                diffarr = new string[diffs.Count];
                for (int i = 0; i < diffs.Count; i++)
                    diffarr[i] = diffs[i].getSerializedString();
                differences.Add(diffarr);
            }
        }
    }

    [Serializable]
    public class CustomDrill
    {
        public string Name;
        public string[] URLs;
        public Function[] Functions;
    }

    [Serializable]
    public class Function
    {
        public string Name;
        public string Description;
        public string Label;
    }

    [Serializable]
    public class ScriptDefinition
    {
        public string InputQuery;
        public string Output;
        public string Script;

        public static bool EqualScripts(ScriptDefinition a, ScriptDefinition b)
        {
            if (a == null && b == null)
                return true;
            if (a == null && b != null && b.isNullScript())
                return true;
            if (b == null && a != null && a.isNullScript())
                return true;
            if (a.Equals(b))
                return true;
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj == System.DBNull.Value)
                return false;
            if (!typeof(ScriptDefinition).Equals(obj.GetType())) return false;
            ScriptDefinition other = (ScriptDefinition)obj;
            if (InputQuery != other.InputQuery) return false;
            if (Output != other.Output) return false;
            if (Script != other.Script) return false;
            return true;
        }

        public bool isNullScript()
        {
            if ((InputQuery == null || InputQuery.Length == 0) &&
                (Output == null || Output.Length == 0) &&
                (Script == null || Script.Length == 0))
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public string getSerializedString()
        {
            return InputQuery + "|1" + Output + "|1" + Script;
        }
    }

    [Serializable]
    public class RepositoryElementOutOfDateException : System.Exception
    {
        public RepositoryElementOutOfDateException(string message)
            : base(message)
        {
        }
    }

    [Serializable]
    public class RepositoryLockedException : System.Exception
    {
        public RepositoryLockedException(string message)
            : base(message)
        {
        }
    }

    [Serializable]
    public class RServer
    {
        public string URL;
        public int Port;
        public string Username;
        public string Password;
    }
}