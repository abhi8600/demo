﻿namespace Performance_Optimizer_Administration
{
    partial class PostBuildReminderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPostBuildReminder = new System.Windows.Forms.Label();
            this.postBuildReminderTextBox = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblPostBuildReminder
            // 
            this.lblPostBuildReminder.AutoSize = true;
            this.lblPostBuildReminder.Location = new System.Drawing.Point(12, 10);
            this.lblPostBuildReminder.Name = "lblPostBuildReminder";
            this.lblPostBuildReminder.Size = new System.Drawing.Size(102, 13);
            this.lblPostBuildReminder.TabIndex = 8;
            this.lblPostBuildReminder.Text = "Post Build Reminder";
            // 
            // postBuildReminderTextBox
            // 
            this.postBuildReminderTextBox.Location = new System.Drawing.Point(12, 26);
            this.postBuildReminderTextBox.Multiline = true;
            this.postBuildReminderTextBox.Name = "postBuildReminderTextBox";
            this.postBuildReminderTextBox.ReadOnly = true;
            this.postBuildReminderTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.postBuildReminderTextBox.Size = new System.Drawing.Size(608, 85);
            this.postBuildReminderTextBox.TabIndex = 7;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(253, 127);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // PostBuildReminderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 162);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblPostBuildReminder);
            this.Controls.Add(this.postBuildReminderTextBox);
            this.Name = "PostBuildReminderDialog";
            this.Text = "PostBuildReminderDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPostBuildReminder;
        private System.Windows.Forms.TextBox postBuildReminderTextBox;
        private System.Windows.Forms.Button btnOK;

    }
}