using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class ReferencePopulations : Form, RepositoryModule, RenameNode
    {
        string moduleName = "Reference Populations";
        string categoryName = "Business Modeling";
        MainAdminForm ma;
        IEnumerable filterList;
        //Repository r;
        public List<ReferencePopulation> refPopList;
        int selIndex;

        public ReferencePopulations(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.ReferencePopulations = (ReferencePopulation[])refPopList.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r == null)
            {
                refPopList = new List<ReferencePopulation>();
            }
            else
            {
                if (r.ReferencePopulations == null)
                {
                    r.ReferencePopulations = new ReferencePopulation[1];
                    r.ReferencePopulations[0] = makeReferencePopulation(r);
                }
                refPopList = new List<ReferencePopulation>(r.ReferencePopulations);
            }
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public TreeNode rootNode;
        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (refPopList != null)
                foreach (ReferencePopulation rp in refPopList)
                {
                    TreeNode n = new TreeNode(rp.PopulationName);
                    //n.Tag = rp.Clone();
                    n.ImageIndex = 6;
                    n.SelectedImageIndex = 6;
                    rootNode.Nodes.Add(n);
                }
        }

        TreeNode selNode;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();

            this.selNode = node;
            if (refPopList.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = refPopulationsMenu;
                removeToolStripMenuItem.Visible = false;
                return false;
            }

            selIndex = (rootNode.Nodes.IndexOf(node));
            selNode = node;
            if (selIndex >= 0)
                indexChanged();
            ma.mainTree.ContextMenuStrip = refPopulationsMenu;
            removeToolStripMenuItem.Visible = true;
            return true;
        }


        public void setup()
        {
            setupForRepository();
        }


        public TabPage TabPage
        {
            get
            {
                return referenceTabPage;
            }
            set
            {
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
                moduleName = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
                categoryName = value;
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            for (int count = 0; count < perfPeerAttributes.Items.Count; count++)
            {
                if (match)
                {
                    if (perfPeerAttributes.Items[count].SubItems[0].Text == find) perfPeerAttributes.Items[count].SubItems[0].Text = replace;
                }
                else
                {
                    perfPeerAttributes.Items[count].SubItems[0].Text = perfPeerAttributes.Items[count].SubItems[0].Text.Replace(find, replace);
                }
            }
            for (int count = 0; count < patternPeerAttributes.Items.Count; count++)
            {
                if (match)
                {
                    if (patternPeerAttributes.Items[count].SubItems[0].Text == find) patternPeerAttributes.Items[count].SubItems[0].Text = replace;
                }
                else
                {
                    patternPeerAttributes.Items[count].SubItems[0].Text = patternPeerAttributes.Items[count].SubItems[0].Text.Replace(find, replace);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            for (int count = 0; count < perfPeerAttributes.Items.Count; count++)
            {
                if (match)
                {
                    if (perfPeerAttributes.Items[count].SubItems[0].Text == find) results.Add("Performance Peer - Attribute");
                }
                else
                {
                    if (perfPeerAttributes.Items[count].SubItems[0].Text.IndexOf(find) >= 0) results.Add("Performance Peer - Attribute");
                }
            }
            for (int count = 0; count < patternPeerAttributes.Items.Count; count++)
            {
                if (match)
                {
                    if (patternPeerAttributes.Items[count].SubItems[0].Text == find) results.Add("Pattern Peer - Attribute");
                }
                else
                {
                    if (patternPeerAttributes.Items[count].SubItems[0].Text.IndexOf(find) >= 0) results.Add("Pattern Peer - Attribute");
                }
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateReferencePopulation();
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                e.Node.Text = e.Label;
            }
        }

        #endregion

        private void setupForRepository()
        {
            targetDimension.Items.Clear();
            List<DimensionTable> dtlist = ma.dimensionTablesList;
            foreach (DimensionTable dt in dtlist)
            {
                if (dt.DimensionName != null && !targetDimension.Items.Contains(dt.DimensionName))
                    targetDimension.Items.Add(dt.DimensionName);
            }
            peerAvailableColumns.BeginUpdate();
            referenceFilterListBox.BeginUpdate();
            peerAvailableColumns.Items.Clear();
            List<string> dcolList = ma.getDimensionColumnList(ma.targetDimension.Text);
            if (peerAvailableColumns.Items.Count == 0 || ma.curMeasureList == null)
            {
                //peerAvailableColumns.Items.Clear();
                foreach (string s in dcolList)
                {
                    ListViewItem lvi = new ListViewItem(
                        new String[] { s, "Attribute" });
                    peerAvailableColumns.Items.Add(lvi);
                }
                List<string> mlist = ma.getMeasureList(null)[0];
                ListViewItem[] lvitems = new ListViewItem[mlist.Count];
                successMeasure.Items.Clear();
                int i = 0;
                foreach (string s in mlist)
                {
                    ListViewItem lvi = new ListViewItem(
                        new String[] { s, "Measure" });
                    lvitems[i++] = lvi;
                }
                peerAvailableColumns.Items.AddRange(lvitems);
                successMeasure.Items.AddRange(mlist.ToArray());
            }
            ArrayList checkedFilters = new ArrayList();
            foreach (string s in referenceFilterListBox.CheckedItems)
            {
                checkedFilters.Add(s);
            }
            referenceFilterListBox.Items.Clear();
            if (filterList != null)
                foreach (QueryFilter f in filterList)
                {
                    referenceFilterListBox.Items.Add(f.Name);
                    if (checkedFilters.Contains(f.Name))
                        referenceFilterListBox.SetItemChecked(referenceFilterListBox.Items.Count - 1, true);
                }
            peerAvailableColumns.ListViewItemSorter = new MainAdminForm.Listcomparer(1, 0);
            peerAvailableColumns.EndUpdate();
            referenceFilterListBox.EndUpdate();
        }

        private ReferencePopulation getCurrentReferencePopulation()
        {
            ReferencePopulation rp = new ReferencePopulation();
            if (selIndex < 0 || rootNode.Nodes.Count == 0)
                rp.PopulationName = "New Reference Population";
            else
                rp.PopulationName = rootNode.Nodes[selIndex].Text;
            rp.PerformancePeerColumns = new PeerItem[perfPeerAttributes.Items.Count];
            for (int count = 0; count < perfPeerAttributes.Items.Count; count++)
            {
                PeerItem pi = new PeerItem();
                int siCount = perfPeerAttributes.Items[count].SubItems.Count;
                pi.Column = (string)perfPeerAttributes.Items[count].SubItems[0].Text;
                pi.Type = perfPeerAttributes.Items[count].SubItems[1].Text == "Attribute" ? 0 : 1;
                if (siCount >= 3)
                {
                    ListViewItem.ListViewSubItem o = perfPeerAttributes.Items[count].SubItems[2];
                    pi.Weight = (o != null && o.Text != "") ? double.Parse(o.Text) : 0.0;
                }
                else
                    pi.Weight = 0.0;
                rp.PerformancePeerColumns[count] = pi;
            }
            rp.SuccessPatternPeerColumns = new PeerItem[patternPeerAttributes.Items.Count];
            for (int count = 0; count < patternPeerAttributes.Items.Count; count++)
            {
                PeerItem pi = new PeerItem();
                int siCount = patternPeerAttributes.Items[count].SubItems.Count;
                pi.Column = (string)patternPeerAttributes.Items[count].SubItems[0].Text;
                pi.Type = patternPeerAttributes.Items[count].SubItems[1].Text == "Attribute" ? 0 : 1;
                if (siCount >= 3)
                {
                    ListViewItem.ListViewSubItem o = patternPeerAttributes.Items[count].SubItems[2];
                    pi.Weight = (o != null && o.Text != "") ? double.Parse(o.Text) : 0.0;
                }
                else
                    pi.Weight = 0.0;
                rp.SuccessPatternPeerColumns[count] = pi;
            }
            rp.NumPerformancePeers = Int32.Parse(numPerfPeers.Text);
            rp.NumSuccessPatternPeers = Int32.Parse(numSuccessPeers.Text);
            rp.SuccessMeasure = successMeasure.Text;
            ArrayList pfList = new ArrayList();
            foreach (string s in referenceFilterListBox.CheckedItems) if (!pfList.Contains(s)) pfList.Add(s);
            rp.PeeringFilters = (string[])pfList.ToArray(typeof(string));
            rp.PerformanceTable = this.performanceTableTextBox.Text;
            rp.SuccessPatternTable = this.successPatternTableTextBox.Text;
            rp.TargetDimension = this.targetDimension.Text;
            rp.Level = this.levelTextBox.Text;
            return rp;
        }

        private ReferencePopulation makeReferencePopulation(Repository r)
        {
            ReferencePopulation rp = new ReferencePopulation();
            rp.PerformancePeerColumns = (PeerItem[])r.PerformancePeerColumns.Clone();
            rp.SuccessPatternPeerColumns =(PeerItem[])r.SuccessPatternPeerColumns.Clone();
            rp.NumPerformancePeers = r.NumPerformancePeers;
            rp.NumSuccessPatternPeers = r.NumSuccessPatternPeers;
            rp.SuccessMeasure = r.SuccessMeasure;
            rp.PeeringFilters = (string[])r.PeeringFilters.Clone();
            rp.PopulationName = "Default Reference Population";
            //rp.PerformanceTable = "";
            //rp.SuccessPatternTable = "";
            return rp;
        }

        private void setCurrentReferencePopulation(ReferencePopulation rp)
        {
            perfPeerAttributes.Items.Clear();
            patternPeerAttributes.Items.Clear();
            if (rp != null)
            {
                if (rp.PerformancePeerColumns != null && rp.PerformancePeerColumns.Length > 0)
                {
                    foreach (PeerItem pi in rp.PerformancePeerColumns)
                    {
                        ListViewItem lvi = new ListViewItem(
                            new string[] {pi.Column, 
									 pi.Type==0?"Attribute":"Measure",
									 pi.Weight.ToString()});
                        perfPeerAttributes.Items.Add(lvi);
                    }
                }
                if (rp.SuccessPatternPeerColumns != null && rp.SuccessPatternPeerColumns.Length > 0)
                {
                    foreach (PeerItem pi in rp.SuccessPatternPeerColumns)
                    {
                        ListViewItem lvi = new ListViewItem(
                            new string[] {pi.Column, 
									 pi.Type==0?"Attribute":"Measure",
									 pi.Weight.ToString()});
                        patternPeerAttributes.Items.Add(lvi);
                    }
                }
                numPerfPeers.Text = rp.NumPerformancePeers.ToString();
                numSuccessPeers.Text = rp.NumSuccessPatternPeers.ToString();
                successMeasure.Text = rp.SuccessMeasure == null ? "" : rp.SuccessMeasure.ToString();
                performanceTableTextBox.Text = rp.PerformanceTable == null ? "" : rp.PerformanceTable.ToString();
                successPatternTableTextBox.Text = rp.SuccessPatternTable == null ? "" : rp.SuccessPatternTable.ToString();
                targetDimension.Text = rp.TargetDimension == null ? "" : rp.TargetDimension.ToString();
                levelTextBox.Text = rp.Level == null ? "" : rp.Level.ToString();
            }
            else
            {
            }
        }

        private void perfPeerAdd_Click(object sender, System.EventArgs e)
        {
            if (peerAvailableColumns.SelectedIndices.Count == 0) return;
            ListViewItem it = new ListViewItem(new string[] 
				{
						peerAvailableColumns.SelectedItems[0].SubItems[0].Text, 
					peerAvailableColumns.SelectedItems[0].SubItems[1].Text,
					peerWeight.Text});
            //Add measures at end and attributes at beginning. That's the way they get 
            //brought back from queries and the order that the list will be traversed
            if (peerAvailableColumns.SelectedItems[0].SubItems[1].Text == "Attribute")
            {
                perfPeerAttributes.Items.Insert(0, it);
            }
            else
            {
                perfPeerAttributes.Items.Add(it);
            }
        }

        private void perfPeerRemove_Click(object sender, System.EventArgs e)
        {
            if (perfPeerAttributes.SelectedIndices.Count == 0) return;
            perfPeerAttributes.Items.Remove(perfPeerAttributes.SelectedItems[0]);
        }

        private void patternPeerAdd_Click(object sender, System.EventArgs e)
        {
            if (peerAvailableColumns.SelectedIndices.Count == 0) return;
            ListViewItem it = new ListViewItem(new string[] 
				{
					peerAvailableColumns.SelectedItems[0].SubItems[0].Text, 
					peerAvailableColumns.SelectedItems[0].SubItems[1].Text,
					peerWeight.Text});
            if (peerAvailableColumns.SelectedItems[0].SubItems[1].Text == "Attribute")
            {
                patternPeerAttributes.Items.Insert(0, it);
            }
            else
            {
                patternPeerAttributes.Items.Add(it);
            }
        }

        private void patternPeerRemove_Click(object sender, System.EventArgs e)
        {
            if (patternPeerAttributes.SelectedIndices.Count == 0) return;
            patternPeerAttributes.Items.Remove(patternPeerAttributes.SelectedItems[0]);
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReferencePopulation rp = new ReferencePopulation();
            rp.PopulationName = "New Reference Population";
            if (refPopList == null)
            {
                refPopList = new List<ReferencePopulation>();
            }
            refPopList.Add(rp);
            
            TreeNode tn = new TreeNode(rp.PopulationName);
            //tn.Tag = rp.Clone();
            tn.ImageIndex = 6;
            tn.SelectedImageIndex = 6;
            rootNode.Nodes.Add(tn);
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Cannot remove last reference population
            if (refPopList.Count <= 1) return;
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
           
            turnOffimplicitSave = true;
            refPopList.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;

        }

        private void updateReferencePopulation()
        {
            if (selNode == null || selIndex < 0 || selIndex >= refPopList.Count || selNode.Parent != rootNode) return;
            ReferencePopulation rp = getCurrentReferencePopulation();
            selNode.Text = rp.PopulationName;
            refPopList[selIndex] = rp;
        }

        private void indexChanged()
        {
            // if (selIndex < 0) return;
            ReferencePopulation rp = refPopList[selIndex];
            setCurrentReferencePopulation(rp);
            setRPFilters(rp, ma.filterMod.filterList);
        
        }


        private void setRPFilters(ReferencePopulation rp, List<QueryFilter> list)
        {
            List<QueryFilter> sortedList = QueryFilter.sortListAsUsed(list, rp.PeeringFilters);
            QueryFilter.sortedListBox(referenceFilterListBox, list, rp.PeeringFilters);
            this.filterList = sortedList;
            
        }

        private void ReferencePopulations_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

    }
}