using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class StagingColumnPrefixes : Form, ResizeMemoryForm
    {
        private MainAdminForm ma;
        private string windowName = "StagingColumnPrefixes";
        private bool fullWindow = false;
        public string columnPrefixesVal;

        public StagingColumnPrefixes()
        {
            InitializeComponent();
        }

        public void formatStagingColumnPrefixes(bool fullwindow, MainAdminForm ma)
        {
            this.fullWindow = fullwindow;
            this.ma = ma;
            
            okButton.Visible = fullWindow;
            cancelButton.Visible = fullWindow;
            if (fullwindow)
            {
                this.Text = "Staging Column Prefixes";
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.StartPosition = FormStartPosition.Manual;
                resetWindowFromSettings();
            }
            else
            {
                this.Text = null;
                this.StartPosition = FormStartPosition.Manual;
                this.FormBorderStyle = FormBorderStyle.None;
            }
        }

        private void StagingColumnPrefixes_Leave(object sender, EventArgs e)
        {
            if (!fullWindow) closeWin();
        }

        private void closeWin()
        {
            this.Close();
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            if (!fullWindow) return;
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty)
            {
                this.Size = new Size(sz.Width, sz.Height);
            }
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
            else
            {
                this.StartPosition = FormStartPosition.CenterParent;
            }
        }

        public void saveWindowSettings()
        {
            if (!fullWindow) return;
            ma.addPopupWindowStatus(windowName, this.Size, this.Location, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void StagingColumnPrefixes_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (fullWindow) saveWindowSettings();
        }

        private void columnPrefixesBox_TextChanged(object sender, EventArgs e)
        {
            columnPrefixesVal = columnPrefixesBox.Text;
        }
    }
}