using System;
using System.Collections.Generic;
using System.Text;

namespace Performance_Optimizer_Administration
{
    public enum MessageType
    {
        BASIC = 0, VERBOSE = 1
    }
    public class BuildAction : BaseObject
    {
        public string Message;
        public MessageType Type;

        public BuildAction(string message, MessageType type)
        {
            this.Message = message;
            this.Type = type;
        }
    }
}
