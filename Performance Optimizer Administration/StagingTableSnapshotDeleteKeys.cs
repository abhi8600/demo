﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class StagingTableSnapshotDeleteKeyForm : Form
    {
        public StagingTableSnapshotDeleteKeyForm(string[] snapshotDeleteKeys)
        {
            InitializeComponent();
            InitializeDataTable(snapshotDeleteKeys);
        }

        private void InitializeDataTable(string[] snapshotDeleteKeys)
        {
            stagingTableSnapshotDeleteKeyDataGridView.Rows.Clear();
            if (snapshotDeleteKeys != null && snapshotDeleteKeys.Length > 0)
            {
                foreach (string deleteKey in snapshotDeleteKeys)
                {
                    stagingTableSnapshotDeleteKeyDataGridView.Rows.Add(deleteKey);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
