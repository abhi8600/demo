using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class BestFitClassifierDialog : Form, ResizeMemoryForm
    {
        public bool isDefault = true;
        private string windowName = "BestFitClassifier";
        private MainAdminForm ma;

        public BestFitClassifierDialog(MainAdminForm ma, ClassifierSettings s, bool allowDefaultButton)
        {
            InitializeComponent();
            this.ma = ma;
            if (allowDefaultButton)
                defaultButton.Visible = true;
            else
                defaultButton.Visible = false;

            if (s != null)
            {
                isDefault = false;
                maxValues.Text = s.MaxInstanceValues == 0 ? "200000" : s.MaxInstanceValues.ToString();
                maxSampleValues.Text = s.MaxSampleInstanceValues == 0 ? "90000" : s.MaxSampleInstanceValues.ToString();
                smo1r.Checked = false;
                smo2rl.Checked = false;
                smo3rl.Checked = false;
                smo4rl.Checked = false;
                smo2r.Checked = false;
                smo3r.Checked = false;
                smo4r.Checked = false;
                smocl.Checked = false;
                smoch.Checked = false;
                lr.Checked = false;
                mlp.Checked = false;
                m5r.Checked = false;
                rbf.Checked = false;
                reptree.Checked = false;

                libsvm1rc.Checked = false;

                svm1r.Checked = false;
                svm2rl.Checked = false;
                svm3rl.Checked = false;
                svm4rl.Checked = false;
                svm2r.Checked = false;
                svm3r.Checked = false;
                svm4r.Checked = false;
                svmcl.Checked = false;
                svmch.Checked = false;

                if (s.RegressionModels != null)
                {
                    foreach (int i in s.RegressionModels)
                    {
                        if (i == 0) smo1r.Checked = true;
                        if (i == 1) smo2rl.Checked = true;
                        if (i == 2) smo3rl.Checked = true;
                        if (i == 5) smo4rl.Checked = true;
                        if (i == 3) smo2r.Checked = true;
                        if (i == 4) smo3r.Checked = true;
                        if (i == 6) smo4r.Checked = true;
                        if (i == 7) smocl.Checked = true;
                        if (i == 8) smoch.Checked = true;
                        if (i == 9) lr.Checked = true;
                        if (i == 10) mlp.Checked = true;
                        if (i == 11) m5r.Checked = true;
                        if (i == 12) reptree.Checked = true;
                        if (i == 13) rbf.Checked = true;

                        if (i == 15) libsvm1rc.Checked = true;

                        if (i == 20) svm1r.Checked = true;
                        if (i == 21) svm2rl.Checked = true;
                        if (i == 22) svm3rl.Checked = true;
                        if (i == 25) svm4rl.Checked = true;
                        if (i == 23) svm2r.Checked = true;
                        if (i == 24) svm3r.Checked = true;
                        if (i == 26) svm4r.Checked = true;
                        if (i == 27) svmcl.Checked = true;
                        if (i == 28) svmch.Checked = true;
                    }
                }

                smo1c.Checked = false;
                smo2cl.Checked = false;
                smo3cl.Checked = false;
                logr.Checked = false;
                j48.Checked = false;
                libsvm1r.Checked = false;

                if (s.ClassificationModels != null)
                {
                    foreach (int i in s.ClassificationModels)
                    {
                        if (i == 100) smo1c.Checked = true;
                        if (i == 101) smo2cl.Checked = true;
                        if (i == 102) smo3cl.Checked = true;
                        if (i == 103) j48.Checked = true;
                        if (i == 104) logr.Checked = true;
                        if (i == 105) ridor.Checked = true;
                        if (i == 150) libsvm1r.Checked = true;
                    }
                }
            }
            else
                isDefault = true;

            resetWindowFromSettings();
        }

        public ClassifierSettings getClassifierSettings()
        {
            ClassifierSettings s = new ClassifierSettings();
            try
            {
                s.MaxSampleInstanceValues = long.Parse(maxSampleValues.Text);
                s.MaxInstanceValues = long.Parse(maxValues.Text);
            }
            catch (FormatException)
            {
                s.MaxSampleInstanceValues = 85000;
                s.MaxInstanceValues = 175000;
            }

            ArrayList regressionList = new ArrayList();
            if (smo1r.Checked) regressionList.Add(0);
            if (smo2rl.Checked) regressionList.Add(1);
            if (smo3rl.Checked) regressionList.Add(2);
            if (smo4rl.Checked) regressionList.Add(5);
            if (smo2r.Checked) regressionList.Add(3);
            if (smo3r.Checked) regressionList.Add(4);
            if (smo4r.Checked) regressionList.Add(6);
            if (smocl.Checked) regressionList.Add(7);
            if (smoch.Checked) regressionList.Add(8);
            if (lr.Checked) regressionList.Add(9);
            if (mlp.Checked) regressionList.Add(10);
            if (m5r.Checked) regressionList.Add(11);
            if (reptree.Checked) regressionList.Add(12);
            if (rbf.Checked) regressionList.Add(13);
            if (libsvm1rc.Checked) regressionList.Add(15);

            if (svm1r.Checked) regressionList.Add(20);
            if (svm2rl.Checked) regressionList.Add(21);
            if (svm3rl.Checked) regressionList.Add(22);
            if (svm4rl.Checked) regressionList.Add(25);
            if (svm2r.Checked) regressionList.Add(23);
            if (svm3r.Checked) regressionList.Add(24);
            if (svm4r.Checked) regressionList.Add(26);
            if (svmcl.Checked) regressionList.Add(27);
            if (svmch.Checked) regressionList.Add(28);

            ArrayList classificationList = new ArrayList();
            if (smo1c.Checked) classificationList.Add(100);
            if (smo2cl.Checked) classificationList.Add(101);
            if (smo3cl.Checked) classificationList.Add(102);
            if (logr.Checked) classificationList.Add(104);
            if (j48.Checked) classificationList.Add(103);
            if (ridor.Checked) classificationList.Add(105);
            if (libsvm1r.Checked) classificationList.Add(150);

            s.RegressionModels = new int[regressionList.Count];
            for (int i = 0; i < regressionList.Count; i++)
                s.RegressionModels[i] = (int)regressionList[i];
            s.ClassificationModels = new int[classificationList.Count];
            for (int i = 0; i < classificationList.Count; i++)
                s.ClassificationModels[i] = (int)classificationList[i];

            return (s);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void defaultButton_Click(object sender, EventArgs e)
        {
            isDefault = true;
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void libsvm1r_CheckedChanged(object sender, EventArgs e)
        {

        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members

        private void BestFitClassifierDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }

    }
}