using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class FindResults : Form
    {
        public FindResults(string findColumn, List<string> results)
        {
            InitializeComponent();
            this.Text = "Find Column Results: " + findColumn;
            foreach (string s in results)
                this.resultsList.Items.Add(s);
        }
    }
}