using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Security.Cryptography;

namespace Performance_Optimizer_Administration
{
    public class ApplicationBuilder
    {
        public static bool GENERATE_INHERITED_TABLES = false;
        public static string TIME_PREFIX_SEPARATOR = ": ";
        private static bool CARRY_KEYS_DOWN_LEVELS = true;
        private int MAX_PHYSICAL_TABLE_NAME_LENGTH = 120;
        private int MAX_PHYSICAL_COLUMN_NAME_LENGTH = -1;
        private static int MAX_COMBINATION_DEPTH = 4;
        private static int MAX_COMBINATIONS = 40;
        private static string TYPEII_CURRENT_VERSION_INHERIT_PREFIX = "Current: ";
        private MainAdminForm ma;
        private Level dayLevel;
        private Level weekLevel;
        private Level monthLevel;
        private Level quarterLevel;
        private Level halfyearLevel;
        private Level yearLevel;
        private bool useDegenerateTables = false;
        private bool generateInheritedTables = GENERATE_INHERITED_TABLES;
        private Dictionary<MeasureTable, MeasureGrain> mtlist = new Dictionary<MeasureTable, MeasureGrain>();
        private Dictionary<MeasureTable, List<MeasureTablePair>> pairs = new Dictionary<MeasureTable, List<MeasureTablePair>>();
        private Dictionary<MeasureTable, List<int[]>> setMap = new Dictionary<MeasureTable, List<int[]>>();
        private bool requiresFloatCast = true;
        public static string PHYSICAL_SUFFIX = "$";
        public enum SqlGenType { Default, MySQL, MonetDB, Oracle, MemDB, ParAccel, Hana };
        private SqlGenType dbType;
        private bool automaticModeDependencies;
        private Dictionary<StagingTable, SourceFile> importList;
        private Dictionary<StagingTable, MainAdminForm> importRepositories;
        Dictionary<DimensionTable, MainAdminForm> importDimRepositories;
        Dictionary<MeasureTable, MainAdminForm> importMRepositories;
        Dictionary<string, int> ttlValueMap = new Dictionary<string, int>();

        public ApplicationBuilder(MainAdminForm ma)
        {
            this.ma = ma;
            dbType = getSQLGenType(ma);
            if (ma.MaxPhysicalColumnNameLength != 0)
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = ma.MaxPhysicalColumnNameLength;
            if (ma.MaxPhysicalTableNameLength != 0)
                MAX_PHYSICAL_TABLE_NAME_LENGTH = ma.MaxPhysicalTableNameLength;
            if (dbType == SqlGenType.MySQL)
            {
                requiresFloatCast = false;
                MAX_PHYSICAL_TABLE_NAME_LENGTH = 64;
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 60;
            }
            else if (dbType == SqlGenType.Oracle)
            {
                MAX_PHYSICAL_TABLE_NAME_LENGTH = 23;
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 26;
            }
            else if (dbType == SqlGenType.Hana)
            {
                requiresFloatCast = false;
                MAX_PHYSICAL_TABLE_NAME_LENGTH = 127;
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 127;
            }
            else if (dbType == SqlGenType.MonetDB)
            {
                PHYSICAL_SUFFIX = "_SMI";
            }
            else if (dbType == SqlGenType.MemDB)
            {
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 120;
            }
            else if ((dbType == SqlGenType.Default || dbType == SqlGenType.ParAccel) && ma.builderVersion >= 24)
            {
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 120;
            }
            else
            {
                MAX_PHYSICAL_COLUMN_NAME_LENGTH = 120;
            }
        }

        public SqlGenType getDbType()
        {
            return dbType;
        }

        public static SqlGenType getSQLGenType(MainAdminForm ma)
        {
            SqlGenType dbType = SqlGenType.Default;
            if (!(ma == null || ma.defaultConnection == null || ma.defaultConnection != "IB Connection") || ma.connection.connectionList[0].Type == "Infobright")
                dbType = SqlGenType.MySQL;
            else if (ma.connection.connectionList.Count > 0 && ma.connection.connectionList[0].Type == "MonetDB")
                dbType = SqlGenType.MonetDB;
            else if (ma.connection.connectionList.Count > 0 && ma.connection.connectionList[0].Type == "Oracle")
                dbType = SqlGenType.Oracle;
            else if (ma.connection.connectionList.Count > 0 && ma.connection.connectionList[0].Type == "MemDB")
                dbType = SqlGenType.MemDB;
            else if (ma.connection.connectionList.Count > 0 && (ma.connection.connectionList[0].Type == "ParAccel" || ma.connection.connectionList[0].Type == "Redshift"))
                dbType = SqlGenType.ParAccel;
            else if (ma.connection.connectionList.Count > 0 && ma.connection.connectionList[0].Type == "SAP Hana")
                dbType = SqlGenType.Hana;
            return dbType;
        }

        public static int getSqlGenIntType(SqlGenType dbType)
        {
            if (dbType == SqlGenType.MySQL)
                return ExpressionParser.LogicalExpression.SQL_GEN_MYSQL;
            else if (dbType == SqlGenType.Oracle)
                return ExpressionParser.LogicalExpression.SQL_GEN_ORACLE;
            else if (dbType == SqlGenType.MemDB)
                return ExpressionParser.LogicalExpression.SQL_GEN_BIRST;
            else if (dbType == SqlGenType.ParAccel)
                return ExpressionParser.LogicalExpression.SQL_GEN_PARACCEL;
            else if (dbType == SqlGenType.Hana)
                return ExpressionParser.LogicalExpression.SQL_GEN_HANA;
            return ExpressionParser.LogicalExpression.SQL_GEN_DEFAULT;
        }

        public void setCarryKeysDownLevels(bool val)
        {
            CARRY_KEYS_DOWN_LEVELS = val;
        }

        public void setGenerateInheritedTables(bool val)
        {
            generateInheritedTables = val;
        }

        public bool getGenerateInheritedTables()
        {
            return generateInheritedTables;
        }

        public void setUseDegenerateTables(bool val)
        {
            useDegenerateTables = val;
        }

        public void setAutomaticModeDependencies(bool automaticModeDependencies)
        {
            this.automaticModeDependencies = automaticModeDependencies;
        }

        public static void saveAndSuspendHooks(MainAdminForm ma)
        {
            ma.removeMeasureEventHandler();
        }

        public static void restoreHooks(MainAdminForm ma)
        {
            ma.addMeasureEventHandler();
        }

        public void setImportList(Dictionary<StagingTable, SourceFile> importList, Dictionary<StagingTable, MainAdminForm> importRepositories, 
            Dictionary<DimensionTable, MainAdminForm> importDimRepositories, Dictionary<MeasureTable, MainAdminForm> importMRepositories)
        {
            this.importList = importList;
            this.importRepositories = importRepositories;
            this.importDimRepositories = importDimRepositories;
            this.importMRepositories = importMRepositories;
        }

        public List<BuildAction> cleanApplication(Dictionary<string, List<MeasureGrain>> unmappedMeasureGrainsByColumnName, bool partOfRebuild)
        {
            saveAndSuspendHooks(ma);
            //Make sure that the non auto generated fact table's grains are valid.
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                bool isAutoGenerated = false;
                if (mt.AutoGenerated)
                {
                    DataRow[] drlist = ma.measureColumnMappings.Select("TableName='" + mt.TableName + "' AND AutoGenerated=false");
                    if (drlist == null || drlist.Length == 0)
                    {
                        isAutoGenerated = true;
                    }
                }
                if (!isAutoGenerated && mt.Grain != null && mt.Grain.measureTableGrains != null)
                {
                    if (mt.TableSource != null)
                    {
                        mt.Grain.connection = mt.TableSource.Connection;
                    }
                    ma.validateAndFixGrains(mt);
                }
                if (mt.TTL != -1)
                {
                    ttlValueMap.Add(mt.TableName, mt.TTL);
                }
            }


            List<BuildAction> buildActions = new List<BuildAction>();
            /*
             * Remove any previously generated tables and physical mappings
             */
            List<DimensionTable> dtlist = new List<DimensionTable>();
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if (dt.AutoGenerated)
                {
                    int count = 0;
                    DataRow[] drlist = ma.dimensionColumnMappings.Select("TableName='" + dt.TableName + "'");
                    foreach (DataRow dr in drlist)
                    {
                        if (!((bool)dr["AutoGenerated"]))
                            count++;
                        else
                        {
                            buildActions.Add(new BuildAction("Removed previously generated dimension column: " + dr["ColumnName"], MessageType.VERBOSE));
                            ma.dimensionColumnMappings.Rows.Remove(dr);
                        }
                    }
                    if (count == 0)
                        dtlist.Add(dt);
                    if (dt.TTL != -1)
                    {
                        ttlValueMap.Add(dt.DimensionName+"."+dt.TableName, dt.TTL);
                    }
                }
            }
            removeSurrogateKeys(buildActions);
            foreach (DimensionTable dt in dtlist)
            {
                if (!dt.AutoGenerated)
                    continue;
                ma.removeDimensionTable(dt.TableName, false);
                buildActions.Add(new BuildAction("Removed previously generated dimension table: " + dt.TableName, MessageType.BASIC));
            }
            List<MeasureTable> mtlist = new List<MeasureTable>();
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                if (mt.AutoGenerated)
                {
                    int count = 0;
                    DataRow[] drlist = ma.measureColumnMappings.Select("TableName='" + mt.TableName + "'");
                    foreach (DataRow dr in drlist)
                    {
                        if (!((bool)dr["AutoGenerated"]))
                            count++;
                        else
                        {
                            buildActions.Add(new BuildAction("Removed previously generated measure column: " + (string)dr["ColumnName"], MessageType.VERBOSE));
                            ma.measureColumnMappings.Rows.Remove(dr);
                        }
                    }
                    if (count == 0)
                        mtlist.Add(mt);
                }
            }
            // Remove any auto-generated logical mappings
            DataRow[] remList = ma.measureColumnsTable.Select("AutoGenerated=true");
            foreach (DataRow dr in remList)
            {
                if (dr["FilteredMeasures"] == System.DBNull.Value)
                {
                    ma.measureColumnsTable.Rows.Remove(dr);
                }
            }
            remList = ma.dimensionColumns.Select("AutoGenerated=true");
            foreach (DataRow dr in remList)
                ma.dimensionColumns.Rows.Remove(dr);
            foreach (MeasureTable mt in mtlist)
            {
                if (!mt.AutoGenerated)
                    continue;
                ma.removeMeasureTableDefinition(mt.TableName, false);
                buildActions.Add(new BuildAction("Removed previously generated measure table: " + mt.TableName, MessageType.BASIC));
            }
            ma.removeAutoGeneratedJoins();
            // Remove any previously created staging columns
            if (ma.stagingTableMod.curNode != null)
                ma.stagingTableMod.implicitSave();
            ma.stagingTableMod.curNode = null;
            ma.mainTree.SelectedNode = null;
            ma.mainTabControl.SelectedTab = null;
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                List<StagingColumn> removeList = new List<StagingColumn>();
                foreach (StagingColumn sc in st.Columns)
                {
                    //Set analyze by date to false for columns with data types other than DateTime & Date.
                    if (sc.GenerateTimeDimension && (!(sc.DataType == "DateTime" || sc.DataType == "Date" || sc.DataType.StartsWith("Date ID"))))
                    {
                        sc.GenerateTimeDimension = false;
                    }
                    string customTimeDimension = getCustomTimeDimension(sc, st);
                    if (isAddedDateIDColumn(sc, customTimeDimension))
                    {
                        bool found = false;
                        // Make sure there's another column that generated it
                        foreach (StagingColumn sc2 in st.Columns)
                        {
                            if (sc == sc2)
                                continue;
                            if ((sc2.DataType == "DateTime" || sc2.DataType == "Date") && sc2.GenerateTimeDimension && sc2.SourceFileColumn != null && sc2.SourceFileColumn == sc.SourceFileColumn)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found)
                        {
                            removeList.Add(sc);
                        }
                    }
                }
                if (removeList.Count > 0)
                {
                    List<StagingColumn> sclist = new List<StagingColumn>(st.Columns);
                    foreach (StagingColumn ssc in removeList)
                        sclist.Remove(ssc);
                    st.Columns = sclist.ToArray();
                }
            }
            foreach (Hierarchy ph in ma.hmodule.getHierarchies())
            {
                //Remove auto-generated hierarchies. But do not remove time hierarchy since it is needed as part of validateAndFixGrains
                //method later, also, it gets removed and re-added when we build the application.
                if (ph.AutoGenerated && (!ph.GeneratedTime))
                {
                    ma.hmodule.removeHierarchyWithoutRefresh(ph);
                }
            }

            restoreHooks(ma);
            return buildActions;
        }

        //Remove all surrogate keys that are no longer referenced as level keys for levels that will not have corresponding dimension tables
        private void removeSurrogateKeys(List<BuildAction> buildActions)
        {
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            foreach (Hierarchy h in hlist)
            {
                string dimension = h.DimensionName;
                List<Level> levelList = new List<Level>();
                h.getChildLevels(levelList);
                foreach (Level l in levelList)
                {
                    if (!l.GenerateDimensionTable)
                    {
                        string surrogateKeyColumnName = ma.getLevelSurrogateKeyName(l.Name, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        bool existsAsLevelKey = false;
                        //Check to see if the surrogate key column exists as a level key for the level.
                        if (l.Keys != null && l.Keys.Length > 0)
                        {
                            foreach (LevelKey lk in l.Keys)
                            {
                                if (lk.ColumnNames != null && lk.ColumnNames.Length > 0)
                                {
                                    foreach (string colName in lk.ColumnNames)
                                    {
                                        if (colName == surrogateKeyColumnName)
                                        {
                                            existsAsLevelKey = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (!existsAsLevelKey)
                        {
                            DataRow dr = ma.removeDimensionColumnDataRowIfExisting(dimension, surrogateKeyColumnName);
                            if (dr != null)
                            {
                                buildActions.Add(new BuildAction("Remove surrogate key column " + surrogateKeyColumnName + " from dimension " + dimension, MessageType.BASIC));
                            }
                        }
                    }
                }
            }

        }
        public List<BuildAction> buildApplication(List<DimensionTable> unmappedDimensionTablesList, Dictionary<string,
            List<MeasureGrain>> unmappedMeasureGrainsByColumnName, string timeSchema, List<HierarchyLevel[]> dependencies)
        {
            List<BuildAction> buildActions = cleanApplication(unmappedMeasureGrainsByColumnName, true);
            // Optimizations
            saveAndSuspendHooks(ma);
            ma.setMeasureColumnCache();
            colNameMap = null;

            // if unmappedDimensionTablesList exist, make sure that all security filters are removed also.
            // When needed, it will be inserted again during generation of dimesion tables
            foreach (DimensionTable dt in unmappedDimensionTablesList)
            {
                if (dt.TableSource != null && dt.TableSource.Filters != null)
                {
                    List<TableSource.TableSourceFilter> tsfs = new List<TableSource.TableSourceFilter>(dt.TableSource.Filters);
                    foreach (TableSource.TableSourceFilter tsf in dt.TableSource.Filters)
                    {
                        if (tsf.SecurityFilter)
                        {
                            tsfs.Remove(tsf);
                        }
                    }
                    dt.TableSource.Filters = tsfs.ToArray();
                }
            }

            List<string> calculatedDimensionNames = new List<string>();
            /*
             * Set any implied grains. An implied grain is a grain that exists through a dependency.
             * For example, Order Details is at a lower grain than the Order table. The Order table
             * could have foreign keys to things like Customer that likely are not on the Order Detail
             * table. If Customer is dependent on Order, and the Order table includes the Customer
             * dimension in its grain, then you can assume that the Order Detail table also includes
             * Customer in its grain. This will allow foreign keys to be carried down to lower level
             * tables
             */
            processImpliedGrains(ma);
            /*
             * Mark tables as degenerate if specified
             */
            if (useDegenerateTables)
                processDegenerates(ma);
            /*
             * Add any generated staging columns first
             */
            addGeneratedStagingColumns();
            /*
             * Generate the time dimension first
             */
            string dname = null;
            if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension)
            {
                dname = generateTime(ma.timeDefinition, buildActions);
                calculatedDimensionNames.Add(dname);
            }
            // Add discovery tables
            addDiscoveryTables(ma, ma.timeDefinition, buildActions);
            /*
             * Generate all dimension table mappings. 
             * Identify all dimensions that will need to be mapped and then map them. Pull as appropriate from staging tables
             */
            mapDimensions(unmappedDimensionTablesList, buildActions, calculatedDimensionNames, timeSchema);
            /* 
             * Need to map the additional time dimension tables after generating dimensions so that the base time
             * tables get mapped first
             */
            if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension)
                generateTimeDimensionTables(dname, ma.timeDefinition, buildActions, timeSchema, ma);

            /*
             * Next, add any measures necessary from staging tables and then create the appropriate measure table definitions
             */
            mapMeasures(unmappedMeasureGrainsByColumnName, dependencies, buildActions);
            /*
             * Add degenerate dimension tables
             */
            addDegenerateDimensionTables(buildActions);
            /*
             * Because columns must be mapped in order, move any unmapped staging columns to the 
             * end of their staging tables
             */
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.SourceType != StagingTable.SOURCE_QUERY)//cannot change order of columns for querybased staging table
                {
                    List<StagingColumn> sclist = new List<StagingColumn>();
                    List<StagingColumn> endlist = new List<StagingColumn>();
                    foreach (StagingColumn sc in st.Columns)
                        if ((sc.SourceFileColumn != null && sc.SourceFileColumn.Length > 0) || (sc.NaturalKey && sc.Transformations == null))
                            sclist.Add(sc);
                        else
                            endlist.Add(sc);
                    foreach (StagingColumn sc in endlist)
                        sclist.Add(sc);
                    st.Columns = sclist.ToArray();
                }
            }
            /*
             * Override any joins necessary for type II SCD dimension tables with facts that join with time. 
             *
             * First, find all the time dimension tables
             */
            if (ma.timeDefinition != null)
            {
                List<DimensionTable> timedts = new List<DimensionTable>();
                Hierarchy h = ma.hmodule.getDimensionHierarchy(ma.timeDefinition.Name);
                if (h != null)
                {
                    List<string> lnames = new List<string>();
                    h.getChildLevelNames(lnames);
                    foreach (DimensionTable dt in ma.dimensionTablesList)
                    {
                        foreach (string s in lnames)
                        {
                            if (dt.TableName == ma.timeDefinition.Name + " " + s)
                                timedts.Add(dt);
                        }
                    }
                    // Now, find all the measure tables that join to time
                    List<MeasureTable> mtlist = new List<MeasureTable>();
                    foreach (MeasureTable mt in ma.measureTablesList)
                    {
                        foreach (DimensionTable dt in timedts)
                        {
                            if (ma.joinExists(mt, dt))
                                if (!mtlist.Contains(mt))
                                    mtlist.Add(mt);
                        }
                    }
                    // Now, find inherited current version of alls type II SCD dimension tables
                    List<DimensionTable> typeIIdtlist = new List<DimensionTable>();
                    List<DimensionTable> inheritedDTForLevelAliaslist = new List<DimensionTable>();
                    foreach (DimensionTable dt in ma.dimensionTablesList)
                    {
                        if (dt.AutoGenerated && dt.Level != null && dt.Level.Length > 0
                            && dt.InheritTable != null && dt.InheritPrefix != null
                            && dt.InheritTable.Length > 0 && dt.InheritPrefix.Length > 0)
                        {
                            Hierarchy dh = ma.hmodule.getDimensionHierarchy(dt.DimensionName);
                            if (dh != null)
                            {
                                Level l = dh.findLevel(dt.Level);
                                if (l != null)
                                {
                                    if (l.SCDType == 2)
                                    {
                                        typeIIdtlist.Add(dt);
                                    }
                                    else if (l.Aliases != null && l.Aliases.Length > 0)
                                    {
                                        foreach (LevelAlias la in l.Aliases)
                                        {
                                            if (la.AliasType == LevelAlias.ATTRIBUTE_ONLY && dt.InheritPrefix.Equals(la.AliasName + ": "))
                                            {
                                                inheritedDTForLevelAliaslist.Add(dt);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    foreach (MeasureTable mt in mtlist)
                    {
                        List<DimensionTable> typeIIdtlistForMeasureTable = getJoiningDimensionTablesList(mt, typeIIdtlist, true);
                        if (typeIIdtlistForMeasureTable != null && typeIIdtlistForMeasureTable.Count > 0)
                        {
                            addJoinsForMeasureTable(mt, typeIIdtlistForMeasureTable, true, true);
                        }
                        List<DimensionTable> inheritedDTForLevelAliaslistForMeasureTable = getJoiningDimensionTablesList(mt, inheritedDTForLevelAliaslist, false);
                        if (inheritedDTForLevelAliaslistForMeasureTable != null && inheritedDTForLevelAliaslistForMeasureTable.Count > 0)
                        {
                            addJoinsForMeasureTable(mt, inheritedDTForLevelAliaslistForMeasureTable, true, false);
                            if (ma.timeDefinition.Periods != null)
                            {
                                foreach (TimePeriod tp in ma.timeDefinition.Periods)
                                {
                                    MeasureTable newmt = (MeasureTable)mt.Clone();
                                    newmt.TableName = tp.Prefix + " " + mt.TableName;
                                    addJoinsForMeasureTable(newmt, inheritedDTForLevelAliaslistForMeasureTable, true, false);
                                }
                            }
                        }
                    }
                }
            }
            // Add any necessary snowflakes
            addHigherLevelSnowflakes(ma);
            // Add discovery table joins to warehouse tables
            addDiscoveryToWarehouseJoins(ma);
          
            // Update inherited tables
            updateInheritedTables();

            if (useDegenerateTables)
                validateAndFixupDegenerates(ma);

            // Restore hooks
            restoreHooks(ma);
            return (buildActions);
        }

        /**
         * From the given list of dimension tables, identify and return all dimension tables that join with the given measure table.
         */
        private List<DimensionTable> getJoiningDimensionTablesList(MeasureTable mt, List<DimensionTable> dtlist, bool includeParentLevels)
        {
            List<DimensionTable> joiningDtList = new List<DimensionTable>();
            if (dtlist == null || dtlist.Count == 0)
            {
                return joiningDtList;
            }
            if ((mt.Grain == null) || (mt.Grain.measureTableGrains == null) || (mt.Grain.measureTableGrains.Length == 0))
            {
                return joiningDtList;
            }
            foreach (DimensionTable dt in dtlist)
            {
                bool found = false;
                foreach (MeasureTableGrain mtg in mt.Grain.measureTableGrains)
                {
                    if (dt.DimensionName == mtg.DimensionName)
                    {
                        if (dt.Level == mtg.DimensionLevel)
                        {
                            found = true;
                        }
                        else if (includeParentLevels)
                        {
                            Level dtl = findLevel(dt.DimensionName, dt.Level);
                            Hierarchy h = ma.hmodule.getDimensionHierarchy(dt.DimensionName);
                            if ((h != null) && (dtl != null))
                            {
                                if (dtl.findLevel(h, mtg.DimensionLevel) != null)
                                {
                                    found = true;
                                }
                            }
                        }
                    }
                    if (found && (!joiningDtList.Contains(dt)))
                    {
                        joiningDtList.Add(dt);
                        break;
                    }
                }
            }
            return joiningDtList;
        }

        private void addDegenerateDimensionTables(List<BuildAction> buildActions)
        {
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                List<StagingColumn> sclist = new List<StagingColumn>();
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.AnalyzeMeasure)
                        sclist.Add(sc);
                }
                if (sclist.Count == 0)
                    continue;
                MeasureGrain mg = getStagingTableGrain(ma, st);
                string baseName = getMeasureTableBaseName(mg);
                string pname = getMeasureTablePhysicalName(baseName);
                SourceFile sf = ma.sourceFileMod.getSourceFile(st.SourceFile);
                string dimName = st.Name.Substring(3) + " Measures";
                if (sf != null && ma.builderVersion >= 10)
                    dimName = getDisplayName(st);
                int copy = 0;
                bool dupe = false;
                do
                {
                    dupe = false;
                    foreach (Hierarchy sh in ma.hmodule.getHierarchies())
                    {
                        if (sh.Name == (copy > 0 ? dimName + " " + copy : dimName) && !sh.AutoGenerated)
                        {
                            dupe = true;
                            copy++;
                            break;
                        }
                    }
                } while (dupe);
                dimName = (copy > 0 ? dimName + " " + copy : dimName);
                string dimTableName = dimName + " Table";
                DimensionTable dt = null;
                foreach (DimensionTable dimTable in ma.dimensionTablesList)
                {
                    if (dimTable.TableName == dimTableName)
                    {
                        dt = dimTable;
                        break;
                    }
                }
                //Always create new hierarchy and level - even if there is an already existing dimension table
                Hierarchy h = new Hierarchy();
                h.Name = dimName;
                h.AutoGenerated = true;
                h.Children = new Level[1];
                h.DimensionName = dimName;
                Level l = new Level();
                h.Children[0] = l;
                l.Name = dimName;
                l.Cardinality = 1;
                l.Children = new Level[0];
                l.ColumnNames = new string[0];
                l.SharedChildren = new string[0];
                l.Keys = new LevelKey[0];
                h.DimensionKeyLevel = l.Name;
                ma.hmodule.addHierarchy(h);
                l.AutomaticallyCreatedAsDegenerate = true;
                if (dt == null)
                {
                    dt = new DimensionTable();
                    dt.TableSource = new TableSource();
                    dt.TableSource.Connection = mg.connection == null ? ma.connection.connectionList[0].Name : mg.connection;
                    dt.TableSource.Tables = new TableSource.TableDefinition[1];
                    dt.TableSource.Tables[0] = new TableSource.TableDefinition();
                    buildActions.Add(new BuildAction("Generated dimension table: " + dimTableName, MessageType.BASIC));
                    dt.TableName = dimTableName;
                    dt.DimensionName = dimName;
                    dt.TableSource.Connection = findConnection(dt.TableSource.Connection);
                    dt.TableSource.Tables[0].PhysicalName = pname;
                    dt.PhysicalName = dt.TableSource.Tables[0].PhysicalName;
                    dt.AutoGenerated = true;
                    dt.Calculated = true;
                    if (ma.serverPropertiesModule.disableCachingForDimensionsBox.Checked)
                        dt.Cacheable = false;
                    if (!ma.getDimensionList().Contains(dimName))
                        ma.addDimension(dimName);
                    l = ma.addDimensionTable(dt, l, false, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                }
                foreach (StagingColumn sc in sclist)
                {
                    ma.addDimensionColumnDataRowIfNecessary(dimName, sc.Name, sc.DataType, sc.Width, null);
                    DataRow dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dimTableName;
                    dcmr["ColumnName"] = sc.Name;
                    dcmr["Qualify"] = true;
                    dcmr["PhysicalName"] = Util.generatePhysicalName(sc.getPhysicalColumnName(), ma.builderVersion) + PHYSICAL_SUFFIX;
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                    buildActions.Add(new BuildAction("Added column " + sc.Name + " to dimension table " + dimTableName, MessageType.VERBOSE));
                    checkAndAddSecurityFilter(dt, sc, (string)dcmr["PhysicalName"], (string)dcmr["ColumnName"]);
                }
                foreach (MeasureTable mt in ma.measureTablesList)
                {
                    if (mt.AutoGenerated && mt.TableSource.Tables.Length == 1 && mt.TableSource.Tables[0].PhysicalName == pname)
                    {
                        if (!equalsConnectionWithNull(dt, mt))
                            continue;
                        addJoins(null, null, dt, mt, true, false, dimName, null);
                    }
                }
                //Add dimension column expressions for degenerate dimension table created above
                if (ma.logicalExpressionList != null)
                {
                    foreach (LogicalExpression le in ma.logicalExpressionList.logicalExpressions)
                    {
                        if (le.Type == LogicalExpression.TYPE_DIMENSION && le.Levels[0][0] == dimName && le.Levels[0][1] == dimName)
                        {
                            addDimensionColumnExpression(le, buildActions);
                        }
                    }
                }
            }
        }

        private void checkAndAddSecurityFilter(MeasureTable measureTable, StagingColumn sc, string columnPhysicalName, string logicalColumnName)
        {
            if (sc != null && sc.SecFilter != null && sc.SecFilter.Enabled && sc.SecFilter.SessionVariable != null)
            {
                SecurityFilter securityFilter = sc.SecFilter;                
                if (measureTable.TableSource == null)
                    measureTable.TableSource = new TableSource();
                List<TableSource.TableSourceFilter> flist = null;
                if (measureTable.TableSource.Filters == null)
                    flist = new List<TableSource.TableSourceFilter>();
                else
                    flist = new List<TableSource.TableSourceFilter>(measureTable.TableSource.Filters);

                string filterPart = null;
                if (securityFilter.Type == SecurityFilter.TYPE_VARIABLE)
                {
                    filterPart = " IN (V{" + securityFilter.SessionVariable + "})";
                }
                else
                {
                    filterPart = " IN (Q{" + securityFilter.SessionVariable + "})";
                }

                string fTableWithColumnName = measureTable.TableSource.Tables[0].PhysicalName + "." + columnPhysicalName;
                int index = columnPhysicalName.IndexOf('.');
                if (index >= 0)
                {
                    fTableWithColumnName = columnPhysicalName;
                }
                string fstring = fTableWithColumnName + filterPart;

                if (securityFilter.addIsNullJoinCondition)
                {
                    fstring = "(" + fstring + " OR " + fTableWithColumnName + " IS NULL)";
                }

                bool found = false;
                foreach (TableSource.TableSourceFilter tsf in flist)
                {
                    if (tsf.SecurityFilter && tsf.Filter == fstring)
                    {
                        found = true;
                        break;
                    }
                }
                // Add if not a dupe
                if (!found)
                {
                    TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                    tsf.Filter = fstring;
                    tsf.SecurityFilter = true;
                    if (securityFilter.FilterGroups != null)
                    {
                        List<string> tsfFilterGroups = new List<string>(securityFilter.FilterGroups);
                        tsf.FilterGroups = tsfFilterGroups.ToArray();
                    }
                    tsf.LogicalColumnName = logicalColumnName;
                    flist.Add(tsf);
                    measureTable.TableSource.Filters = flist.ToArray();
                }
            }
        }

        private void checkAndAddSecurityFilter(DimensionTable dimTable, StagingColumn sc, string columnPhysicalName, string logicalColumnName)
        {
            if (sc != null && sc.SecFilter != null && sc.SecFilter.Enabled && sc.SecFilter.SessionVariable != null)
            {
                SecurityFilter securityFilter = sc.SecFilter;
                List<TableSource.TableSourceFilter> flist = null;
                if (dimTable.TableSource.Filters == null)
                    flist = new List<TableSource.TableSourceFilter>();
                else
                    flist = new List<TableSource.TableSourceFilter>(dimTable.TableSource.Filters);

                string filterPart = null;
                if (securityFilter.Type == SecurityFilter.TYPE_VARIABLE)
                {
                    filterPart = " IN (V{" + securityFilter.SessionVariable + "})";
                }
                else
                {
                    filterPart = " IN (Q{" + securityFilter.SessionVariable + "})";
                }

                string fTableWithColumnName = dimTable.TableSource.Tables[0].PhysicalName + "." + columnPhysicalName;
                int index = columnPhysicalName.IndexOf('.');
                if (index >= 0)
                {
                    fTableWithColumnName = columnPhysicalName;
                }
                string fstring = fTableWithColumnName + filterPart;

                if (securityFilter.addIsNullJoinCondition)
                {
                    fstring = "(" + fstring + " OR " + fTableWithColumnName + " IS NULL)";
                }

                bool found = false;
                foreach (TableSource.TableSourceFilter tsf in flist)
                {
                    if (tsf.SecurityFilter && tsf.Filter == fstring)
                    {
                        found = true;
                        break;
                    }
                }
                // Add if not a dupe
                if (!found)
                {
                    TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                    tsf.Filter = fstring;
                    tsf.SecurityFilter = true;
                    if (securityFilter.FilterGroups != null)
                    {
                        List<string> tsfFilterGroups = new List<string>(securityFilter.FilterGroups);
                        tsf.FilterGroups = tsfFilterGroups.ToArray();
                    }
                    tsf.LogicalColumnName = logicalColumnName;
                    flist.Add(tsf);
                    dimTable.TableSource.Filters = flist.ToArray();
                }
            }
        }

        private void addLogicalTime(MainAdminForm maf, List<BuildAction> buildActions)
        {
            if (maf.timeDefinition == null)
                return;
            bool addedDay = false;
            bool addedMonth = false;
            string dayName = "Days Ago";
            string weekName = "Weeks Ago";
            string monthName = "Months Ago";
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (maf.timeDefinition.GenerateTimeDimension && dt.DimensionName == maf.timeDefinition.Name && (dt.VirtualMeasuresInheritTable == null || dt.VirtualMeasuresInheritTable.Length == 0))
                {
                    if (!dt.AutoGenerated)
                        continue;
                    if (dt.Level == dayLevel.Name)
                    {
                        ma.addDimensionColumnDataRowIfNecessary(maf.timeDefinition.Name, dayName, "Integer", 0, null);
                        string pname = Util.generatePhysicalName(DateName, ma.builderVersion) + PHYSICAL_SUFFIX;
                        string formula = null;
                        if (dbType == SqlGenType.Default || dbType == SqlGenType.MemDB || dbType == SqlGenType.ParAccel)
                            formula = "DATEDIFF(DAY," + dt.TableSource.Tables[0].PhysicalName + "." + pname + ",GETDATE())";
                        else if (dbType == SqlGenType.MySQL)
                            formula = ExpressionParser.LogicalExpression.getMySQLDateDiff(LogicalExpressionParser.DAY, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE()");
                        else if (dbType == SqlGenType.Oracle)
                            formula = ExpressionParser.LogicalExpression.getOracleDateDiff(LogicalExpressionParser.DAY, dt.TableSource.Tables[0].PhysicalName + "." + pname, "SYSDATE");
                        else if (dbType == SqlGenType.Hana)
                            formula = ExpressionParser.LogicalExpression.getHanaDateDiff(LogicalExpressionParser.DAY, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE");
                        DataRow dr = addDimensionColumnMapping(ma, dt.TableName, dayName, formula, false,
                            false, false, null, null, buildActions, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        dr["CacheBoundary"] = Repository.CACHE_DAY_BOUNDARY;
                        addedDay = true;
                    }
                    if (dt.Level == dayLevel.Name || dt.Level == weekLevel.Name || dt.Level == monthLevel.Name)
                    {
                        ma.addDimensionColumnDataRowIfNecessary(maf.timeDefinition.Name, weekName, "Integer", 0, null);
                        string pname = Util.generatePhysicalName(DateName, ma.builderVersion) + PHYSICAL_SUFFIX;
                        string formula = null;
                        if (dbType == SqlGenType.Default || dbType == SqlGenType.MemDB || dbType == SqlGenType.ParAccel)
                            formula = "DATEDIFF(WEEK," + dt.TableSource.Tables[0].PhysicalName + "." + pname + ",GETDATE())";
                        else if (dbType == SqlGenType.MySQL)
                            formula = ExpressionParser.LogicalExpression.getMySQLDateDiff(LogicalExpressionParser.WEEK, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE()");
                        else if (dbType == SqlGenType.Oracle)
                            formula = ExpressionParser.LogicalExpression.getOracleDateDiff(LogicalExpressionParser.WEEK, dt.TableSource.Tables[0].PhysicalName + "." + pname, "SYSDATE");
                        else if (dbType == SqlGenType.Hana)
                            formula = ExpressionParser.LogicalExpression.getHanaDateDiff(LogicalExpressionParser.WEEK, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE");
                        DataRow dr = addDimensionColumnMapping(ma, dt.TableName, weekName, formula, false,
                            false, false, null, null, buildActions, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        dr["CacheBoundary"] = Repository.CACHE_WEEK_BOUNDARY;
                        addedMonth = true;
                    }
                    if (dt.Level == dayLevel.Name || dt.Level == weekLevel.Name || dt.Level == monthLevel.Name)
                    {
                        ma.addDimensionColumnDataRowIfNecessary(maf.timeDefinition.Name, monthName, "Integer", 0, null);
                        string pname = Util.generatePhysicalName(DateName, ma.builderVersion) + PHYSICAL_SUFFIX;
                        string formula = null;
                        if (dbType == SqlGenType.Default || dbType == SqlGenType.MemDB || dbType == SqlGenType.ParAccel)
                            formula = "DATEDIFF(MONTH," + dt.TableSource.Tables[0].PhysicalName + "." + pname + ",GETDATE())";
                        else if (dbType == SqlGenType.MySQL)
                            formula = ExpressionParser.LogicalExpression.getMySQLDateDiff(LogicalExpressionParser.MONTH, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE()");
                        else if (dbType == SqlGenType.Oracle)
                            formula = ExpressionParser.LogicalExpression.getOracleDateDiff(LogicalExpressionParser.MONTH, dt.TableSource.Tables[0].PhysicalName + "." + pname, "SYSDATE");
                        else if (dbType == SqlGenType.Hana)
                            formula = ExpressionParser.LogicalExpression.getHanaDateDiff(LogicalExpressionParser.MONTH, dt.TableSource.Tables[0].PhysicalName + "." + pname, "CURRENT_DATE");
                        DataRow dr = addDimensionColumnMapping(ma, dt.TableName, monthName, formula, false,
                            false, false, null, null, buildActions, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        dr["CacheBoundary"] = Repository.CACHE_MONTH_BOUNDARY;
                        addedMonth = true;
                    }
                }
            }
            if (addedDay)
            {
                List<string> clist = new List<string>(dayLevel.ColumnNames);
                clist.Add(dayName);
                dayLevel.ColumnNames = clist.ToArray();
                List<bool> hlist = new List<bool>(dayLevel.HiddenColumns);
                hlist.Add(true);
                dayLevel.HiddenColumns = hlist.ToArray();
            }
            if (addedMonth)
            {
                List<string> clist = new List<string>(monthLevel.ColumnNames);
                clist.Add(monthName);
                monthLevel.ColumnNames = clist.ToArray();
                List<bool> hlist = new List<bool>(monthLevel.HiddenColumns);
                hlist.Add(true);
                monthLevel.HiddenColumns = hlist.ToArray();
            }
        }

        private void addLogicalExpressions(List<BuildAction> buildActions)
        {
            List<LogicalExpression> expressions = new List<LogicalExpression>();
            if (ma.logicalExpressionList != null)
                expressions.AddRange(ma.logicalExpressionList.logicalExpressions);

            if (expressions == null || expressions.Count == 0)
                return;

            foreach (LogicalExpression le in expressions)
            {
                if (le.Type == LogicalExpression.TYPE_DIMENSION)
                    addDimensionColumnExpression(le, buildActions);
                else if (le.Type == LogicalExpression.TYPE_MEASURE)
                    addMeasureColumnExpression(le, buildActions);
            }
        }

        private void addDimensionColumnExpression(LogicalExpression le, List<BuildAction> buildActions)
        {
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if (dt.DimensionName == le.Levels[0][0] && dt.Level == le.Levels[0][1])
                {
                    bool found = false;
                    DataRow mcr = ma.addDimensionColumnDataRowIfNecessary(dt.DimensionName, le.Name, "Varchar", 100, null);
                    DataRow dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dt.TableName;
                    dcmr["ColumnName"] = le.Name;
                    dcmr["Qualify"] = false;
                    Dictionary<string, string> columnMap = new Dictionary<string, string>();
                    Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                        new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
                    DataRowView[] drv = ma.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                    found = false;
                    string ptname = dt.Type == MeasureTable.NORMAL ? dt.TableSource.Tables[0].PhysicalName : dt.PhysicalName;
                    foreach (DataRowView dr in drv)
                    {
                        string cname = (string)dr.Row["ColumnName"];
                        if (le.Name == cname)
                        {
                            found = true;
                            break;
                        }
                        //Do not qualify any logical expressions
                        bool qualify =(bool) dr.Row["Qualify"];
                        columnMap.Add(cname, (!qualify ? "" : ptname + ".") + ((string)dr.Row["PhysicalName"]));
                        DataRowView[] dcrArr = ma.dimensionColumnNamesView.FindRows(new string[] { dt.DimensionName, cname });
                        if (dcrArr == null || dcrArr.Length == 0)
                        {
                            continue;
                        }
                        DataRow dcr = dcrArr[0].Row;
                        string s = (string)dcr["ColWidth"];
                        int width = 20;
                        try
                        {
                            width = int.Parse(s);
                        }
                        catch (Exception)
                        {
                        }
                        ExpressionParser.LogicalExpression.DataType ftype =
                            new ExpressionParser.LogicalExpression.DataType((string)dcr["DataType"], width);
                        typeMap.Add(cname, ftype);
                    }
                    if (found)
                        continue;
                    ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(le.Expression, columnMap, typeMap, getSqlGenIntType(dbType), (ma.builderVersion >= 9));
                    if (lep.HasError)
                        continue;
                    ExpressionParser.LogicalExpression.DataType dtype = lep.Type;
                    mcr["DataType"] = dtype.Name;
                    mcr["ColWidth"] = dtype.Width;
                    mcr["Constant"] = lep.isConstant;
                    dcmr["PhysicalName"] = lep.GeneratedCode;
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                    buildActions.Add(new BuildAction("Added column " + le.Name + " to dimension table " + dt.TableName, MessageType.VERBOSE));
                    break;
                }
            }
        }

        private void addMeasureColumnExpression(LogicalExpression le, List<BuildAction> buildActions)
        {
            bool added = false;
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                /*
                 * The only type of inherited table it can map to is an autogenerated one that is created when 
                 * combining two measure tables at two or more different grains
                 */
                bool isInhertedGeneratedMeasureTable = mt.InheritTable != null && mt.InheritTable.Length > 0 && mt.AutoGenerated && mt.InheritTable.Split(new char[] { ',' }).Length >= 2;
                string[] splitTables = null;
                if (isInhertedGeneratedMeasureTable)
                    splitTables = mt.InheritTable.Split(new char[] { ',' });
                if (mt.InheritTable != null && mt.InheritTable.Length > 0 && ((mt.InheritPrefix != null && mt.InheritPrefix.Length > 0) || !isInhertedGeneratedMeasureTable))
                {
                    if (le.Levels.Length == 1 && le.Levels[0].Length == 1 && (le.Levels[0][0] + " Fact" == mt.TableName))
                    {
                        // Inherited fact
                        added = addMeasureColumnExpressionFormula(le, mt, buildActions);
                        if (added)
                            break;
                    }
                    continue;
                }
                if (mt.Type == MeasureTable.DERIVED || mt.Type == MeasureTable.UNMAPPED || !mt.AutoGenerated)
                    continue;
                if (!mtlist.ContainsKey(mt) && isInhertedGeneratedMeasureTable)
                {
                    MeasureGrain mg2 = null;
                    foreach (string tname in splitTables)
                    {
                        MeasureTable foundmt = null;
                        foreach (MeasureTable mt2 in ma.measureTablesList)
                        {
                            if (mt2.TableName == tname)
                            {
                                foundmt = mt2;
                            }
                        }
                        if (foundmt != null && mtlist.ContainsKey(foundmt))
                        {
                            MeasureGrain newmg = mtlist[foundmt];
                            if (mg2 == null)
                                mg2 = newmg;
                            else
                            {
                                // If other table is at a lower grain, use it's grain
                                if (isLowerGrain(ma, mg2.measureTableGrains, newmg.measureTableGrains, ma.dependencies, automaticModeDependencies) != null)
                                    mg2 = newmg;
                            }
                        }
                    }
                    if (mg2 != null)
                        mtlist.Add(mt, mg2);
                }
                if (!checkGrain(mt, le))
                    continue;
                added = addMeasureColumnExpressionFormula(le, mt, buildActions);
                if (added)
                    break;
            }
            if (!added && le.Levels.Length == 1 && le.Levels[0].Length == 1)
            {
                string sname = le.Levels[0][0];
                // Discovery source
                foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
                {
                    if (sname == ApplicationBuilder.getDisplayName(st))
                    {
                        // Find all applicable measure tables
                        foreach (MeasureTable mt in ma.measureTablesList)
                        {
                            foreach (TableSource.TableDefinition td in mt.TableSource.Tables)
                            {
                                if (td.PhysicalName == st.Name)
                                {
                                    added = addMeasureColumnExpressionFormula(le, mt, buildActions) || added;
                                }
                            }
                        }
                        if (added)
                            break;
                    }
                }
            }
            else if (!added && setMap != null && pairs != null)
            {
                /* 
                 * If a matching set of tables can't be found, need to create a new measure table as the formula must contain
                 * columns from multiple measure tables
                 */
                ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(le.Expression, null, null, getSqlGenIntType(dbType), (ma.builderVersion >= 9));
                foreach (MeasureTable mt in pairs.Keys)
                {
                    if (!checkGrain(mt, le))
                        continue;
                    List<MeasureTablePair> pairList = pairs[mt];
                    if (pairList == null)
                        continue;
                    // Check all combinations to see if we can find one with the right measure table combination
                    bool match = true;
                    List<int> set = new List<int>();
                    foreach (string s in lep.LogicalColumns)
                    {
                        bool found = false;
                        DataRowView[] drvlist = ma.measureColumnMappingsTablesView.FindRows(mt.TableName);
                        foreach (DataRowView drv in drvlist)
                        {
                            string name = (string)drv.Row["PhysicalName"];
                            string pname = Util.generatePhysicalName(s, ma.builderVersion) + PHYSICAL_SUFFIX;
                            if (name == pname)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            int count = 0;
                            foreach (MeasureTablePair mtp in pairList)
                            {
                                drvlist = ma.measureColumnMappingsTablesView.FindRows(mtp.mt2.TableName);
                                foreach (DataRowView drv in drvlist)
                                {
                                    string name = (string)drv.Row["PhysicalName"];
                                    string pname = Util.generatePhysicalName(s, ma.builderVersion) + PHYSICAL_SUFFIX;
                                    if (name == pname)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (found)
                                {
                                    if (!set.Contains(count))
                                        set.Add(count);
                                    break;
                                }
                                count++;
                            }
                        }
                        if (!found)
                        {
                            match = false;
                            break;
                        }
                    }
                    if (!match)
                        continue;
                    if (set.Count == 0)
                        continue;
                    MeasureTable newmt = createCombinedMeasureTable(mt, pairList, set.ToArray());
                    if (newmt != null)
                        added = addMeasureColumnExpressionFormula(le, newmt, buildActions);
                }
            }
        }

        public static string getDisplayName(StagingTable st)
        {
            string fname = null;
            int extindex = st.SourceFile.LastIndexOf('.');
            if (extindex >= 0)
                fname = st.SourceFile.Substring(0, extindex);
            else
                fname = st.SourceFile;
            return fname;
        }

        private bool checkGrain(MeasureTable mt, LogicalExpression le)
        {
            if (!mtlist.ContainsKey(mt) && (importMRepositories == null || !importMRepositories.ContainsKey(mt)))
                return false;
            MeasureGrain mg = null;
            if (importMRepositories != null && importMRepositories.ContainsKey(mt))
            {
                if (mt.Grain == null)
                    return false;
                mg = mt.Grain;
            }
            else
                mg = mtlist[mt];
            bool found = false;
            if (le.Levels.Length != mg.measureTableGrains.Length)
                return false;
            foreach (string[] level in le.Levels)
            {
                found = false;
                foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                {
                    if (mtg.DimensionName == level[0] && mtg.DimensionLevel == level[1])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    return false;
                }
            }
            return true;
        }

        private bool addMeasureColumnExpressionFormula(LogicalExpression le, MeasureTable mt, List<BuildAction> buildActions)
        {
            Dictionary<MeasureTable, DataRowView[]> drv = new Dictionary<MeasureTable, DataRowView[]>();
            if (mt.InheritTable == null || mt.InheritTable.Length == 0)
                drv.Add(mt, ma.measureColumnMappingsTablesView.FindRows(new object[] { mt.TableName }));
            else if (mt.InheritPrefix == null || mt.InheritPrefix.Length == 0)
            {
                string[] splitTables = mt.InheritTable.Split(new char[] { ',' });
                // If an inherited table, get list of columns
                foreach (string tname in splitTables)
                {
                    foreach (MeasureTable mt2 in ma.measureTablesList)
                    {
                        if (mt2.TableName == tname)
                            drv.Add(mt2, ma.measureColumnMappingsTablesView.FindRows(new object[] { mt2.TableName }));
                    }
                }
            }
            else
                return false;

            Dictionary<string, string> columnMap = new Dictionary<string, string>();
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
            bool found = false;
            foreach (MeasureTable mtkey in drv.Keys)
            {
                foreach (DataRowView dr in drv[mtkey])
                {
                    string cname = (string)dr.Row["ColumnName"];
                    DataRowView[] ldrv = ma.measureColumnNamesView.FindRows(cname);
                    string rname = null;
                    if (ldrv.Length > 0)
                    {
                        object o = ldrv[0].Row["RootMeasure"];
                        rname = o == System.DBNull.Value ? null : (string)o;
                    }
                    if (le.Name == cname || le.Name == rname)
                        found = true;
                    string ptname = mtkey.Type == MeasureTable.NORMAL ? mtkey.TableSource.Tables[0].PhysicalName : mtkey.PhysicalName;
                    if (ldrv.Length > 0 && !columnMap.ContainsKey(cname))
                    {
                        columnMap.Add(cname, ptname + "." + ((string)dr.Row["PhysicalName"]));
                        DataRow fmcr = ldrv[0].Row;
                        ExpressionParser.LogicalExpression.DataType ftype =
                            new ExpressionParser.LogicalExpression.DataType((string)fmcr["DataType"], (int)fmcr["Width"]);
                        typeMap.Add(cname, ftype);
                        if (rname != null && !columnMap.ContainsKey(rname))
                        {
                            columnMap.Add(rname, mtkey.TableSource.Tables[0].PhysicalName + "." + ((string)dr.Row["PhysicalName"]));
                            typeMap.Add(rname, ftype);
                        }
                    }
                }
            }
            if (found)
                return false;
            ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(le.Expression, columnMap, typeMap, getSqlGenIntType(dbType), (ma.builderVersion >= 9));
            if (lep.HasError)
                return false;
            ExpressionParser.LogicalExpression.DataType dtype = lep.Type;
            DataRow mcr = ma.addMeasureColumnDataRowIfNecessary(le.Name, "Varchar", 100, le.AggregationRule, false);
            mcr["DataType"] = dtype.Name;
            mcr["RootMeasure"] = mcr["ColumnName"];
            mcr["Width"] = dtype.Width;
            string formula = le.AggregationRule == "AVG" && requiresFloatCast ? "CAST(" + lep.GeneratedCode + " AS FLOAT)" : lep.GeneratedCode;
            DataRow dcmr = ma.addMeasureColumnMapping(le.Name, formula, mt, true, -1, true);
            dcmr["Qualify"] = false;
            if (lep.physicalFilter != null && lep.physicalFilter.Length > 0)
                dcmr["PhysicalFilter"] = lep.physicalFilter;
            else
                dcmr["PhysicalFilter"] = null;

            buildActions.Add(new BuildAction("Added column " + le.Name + " to measure table " + mt.TableName, MessageType.VERBOSE));
            return true;
        }

        /*
         * Add snowflakes for dimension tables at higher levels
         */
        private void addHigherLevelSnowflakes(MainAdminForm maf)
        {
            List<DimensionTable> curList = new List<DimensionTable>(maf.dimensionTablesList);
            foreach (DimensionTable dt in curList)
            {
                // See if it can be snowflaked off a lower level
                Hierarchy h = maf.hmodule.getDimensionHierarchy(dt.DimensionName);
                if (h != null && maf.timeDefinition != null && maf.timeDefinition.Name == h.Name)
                    continue;
                if (h != null)
                {
                    Level l = h.findLevel(dt.Level);
                    {
                        if (l != null)
                        {
                            foreach (DimensionTable dt2 in curList)
                            {
                                if (dt2 == dt || dt.DimensionName != dt2.DimensionName || dt.Level == dt2.Level ||
                                    dt.TableSource == null || dt2.TableSource == null || dt.TableSource.Connection != dt2.TableSource.Connection)
                                    continue;
                                Level l2 = l.findLevel(h, dt2.Level);
                                if (l2 != null)
                                {
                                    List<DimensionTable> dtlist = new List<DimensionTable>();
                                    dtlist.Add(dt2);
                                    snowflakeTable(dt, dtlist, true, true, false);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static MeasureGrain getMeasureGrain(StagingTable st, MainAdminForm maf)
        {
            MeasureGrain mg = new MeasureGrain();
            mg.LoadGroups = st.LoadGroups != null && st.LoadGroups.Length == 0 ? null : st.LoadGroups;
            string liveAccessConnection = null;
            if (st.LoadGroups != null && st.LoadGroups.Length > 0)
            {
                foreach (string lg in st.LoadGroups)
                {
                    if (lg.StartsWith("LIVEACCESS_"))
                    {
                        if (maf.localETLLoadGroupToLocalETLConfig != null && maf.localETLLoadGroupToLocalETLConfig.ContainsKey(lg))
                        {
                            liveAccessConnection = maf.localETLLoadGroupToLocalETLConfig[lg].getLocalETLConnection();
                            break;
                        }
                    }
                }
            }
            addOverrideKeyColumnsToGrain(mg, st, maf);
            mg.connection = liveAccessConnection != null ? liveAccessConnection : "Default Connection";
            mg.measureTableGrains = new MeasureTableGrain[st.Levels.Length];
            for (int i = 0; i < st.Levels.Length; i++)
            {
                mg.measureTableGrains[i] = new MeasureTableGrain();
                mg.measureTableGrains[i].DimensionName = st.Levels[i][0];
                mg.measureTableGrains[i].DimensionLevel = st.Levels[i][1];
            }
            return (mg);
        }

        private void processImpliedGrainsForTables(MainAdminForm maf, List<StagingTable> tablesToProcess, HashSet<StagingTable> tablesModified)
        {
            foreach (StagingTable st in tablesToProcess)
            {
                MeasureGrain mg = getStagingTableGrain(maf, st);
                foreach (StagingTable st2 in maf.stagingTableMod.getStagingTables())
                {
                    if (st == st2)
                        continue;
                    if (st2.SourceGroups != null)
                    {
                        if (st.SourceGroups == null)
                            continue;
                        string[] sg = st.SourceGroups.Split(new char[] { ',' });
                        string[] sg2 = st2.SourceGroups.Split(new char[] { ',' });
                        bool found = false;
                        foreach (string s in sg2)
                        {
                            foreach (string s2 in sg)
                            {
                                if (s == s2)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                break;
                        }
                        if (!found)
                            continue;
                    }
                    MeasureGrain mg2 = getStagingTableGrain(maf, st2);
                    List<MeasureTableGrain[]> matches = isLowerGrain(ma, mg.measureTableGrains, mg2.measureTableGrains, maf.dependencies, automaticModeDependencies);
                    if (matches != null)
                    {
                        // If mg2 is at a lower level than mg, carry down any missing grains
                        foreach (string[] level in st.Levels)
                        {
                            bool found = false;
                            foreach (string[] level2 in st2.Levels)
                            {
                                if (level[0] == level2[0])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            /* 
                             * If that dimension is already marked as a grain, ignore. Otherwise
                             * add the level
                             */
                            if (!found)
                            {
                                /*
                                 * Make sure that we can carry this down. If the lower-level staging
                                 * table has a column that has the same name as the level key, but is 
                                 * not compatible (i.e. has a different data type), then we cannot
                                 * carry this down because the keys would get lost
                                 */
                                Hierarchy h = maf.hmodule.getDimensionHierarchy(level[0]);
                                if (h == null)
                                    continue;
                                Level l = h.findLevel(level[1]);
                                bool badKey = false;
                                List<StagingTable> keystlist = maf.stagingTableMod.getStagingTablesAtLevel(level[0], level[1]);

                                foreach (LevelKey lk in l.Keys)
                                {
                                    if (lk.SurrogateKey)
                                        continue;
                                    foreach (string s in lk.ColumnNames)
                                    {
                                        string datatype = null;
                                        foreach (StagingTable keyst in keystlist)
                                        {
                                            foreach (StagingColumn sc in keyst.Columns)
                                            {
                                                if (sc.NaturalKey && sc.Name == s)
                                                {
                                                    datatype = sc.DataType;
                                                    break;
                                                }
                                            }
                                            if (datatype != null)
                                                break;
                                        }
                                        if (datatype != null)
                                            foreach (StagingColumn sc in st2.Columns)
                                            {
                                                if (sc.Name == s && sc.DataType != datatype)
                                                {
                                                    badKey = true;
                                                    break;
                                                }
                                            }
                                        if (badKey)
                                            break;
                                    }
                                    if (badKey)
                                        break;
                                }
                                if (!badKey)
                                {
                                    List<string[]> levelList = new List<string[]>(st2.Levels);
                                    levelList.Add(level);
                                    st2.Levels = levelList.ToArray();
                                    if (!tablesModified.Contains(st2))
                                        tablesModified.Add(st2);
                                }
                            }
                        }
                    }
                }
            }
        }

        /*
         * If we are carrying keys down to lower levels, then it makes sense to update
         * the grains associated with staging tables. Since we are determining whether a staging
         * table is at a lower level using not just grain, but dependencies (which may not be 
         * visible in a UI) then we should turn the dependencies into the grains that they imply
         */
        private void processImpliedGrains(MainAdminForm maf)
        {
            if (!CARRY_KEYS_DOWN_LEVELS)
                return;
            List<StagingTable> tablesToProcess = maf.stagingTableMod.getStagingTables();
            HashSet<StagingTable> tablesModified = null;
            // Iterate until no more tables can carry down levels
            do
            {
                tablesModified = new HashSet<StagingTable>(new IdentityComparer<StagingTable>());
                processImpliedGrainsForTables(maf, tablesToProcess, tablesModified);
                tablesToProcess = new List<StagingTable>(tablesModified);
            } while (tablesModified.Count > 0);
        }

        /*
         * Using birst UI, data sources can be marked as "Rows Are Transactions". In order to optimize the underlying
         * dimensional design, we attempt to figure out if we can mark some of the hierarchy levels as degenerate. As 
         * part of scanning the source files earlier, the we set KeyIndices for each source file that represents the
         * uniqueness of rows for a given combination of key columns in source files. We reuse this property to figure
         * our for each staging table (that have been marked as "Rows are transactions") whether there are any hierarchy
         * levels we can set as degenerates.
        */
        private void processDegenerates(MainAdminForm maf)
        {
            Dictionary<Level, object> degenerateLevels = new Dictionary<Level, object>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                SourceFile sfile = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sfile == null || sfile.KeyIndices == null || sfile.KeyIndices.Length == 0)
                {
                    continue;
                }

                foreach (string[] level in st.Levels)
                {
                    if (maf.timeDefinition != null && level[0] == maf.timeDefinition.Name)
                        continue;
                    Hierarchy h = maf.hmodule.getDimensionHierarchy(level[0]);
                    if (h == null)
                        continue;
                    Level l = h.findLevel(level[1]);
                    if (l == null)
                        continue;
                    //Mark all levels as degenerate so that changing value of Transactional flag from true to false
                    //clears the degenerate flag from the level.
                    l.Degenerate = false;
                    if (h.DimensionKeyLevel != level[1] || !st.Transactional)
                    {
                        continue;
                    }

                    if (ma.builderVersion <= 18)
                    {
                        //Check to see if this level is being referred to in grainset of any other staging table. If so,
                        //it should not be marked as degenerate.
                        bool isLevelReferredByOtherStagingTable = false;
                        foreach (StagingTable st2 in maf.stagingTableMod.getStagingTables())
                        {
                            if (st == st2)
                            {
                                continue;
                            }
                            foreach (string[] level2 in st2.Levels)
                            {
                                if (level[0] == level2[0] && level[1] == level2[1])
                                {
                                    isLevelReferredByOtherStagingTable = true;
                                    break;
                                }
                            }
                            if (isLevelReferredByOtherStagingTable)
                            {
                                break;
                            }
                        }
                        if (isLevelReferredByOtherStagingTable)
                        {
                            continue;
                        }
                    } 
                    foreach (LevelKey lk in l.Keys)
                    {
                        bool matchFound = false;
                        if (lk.ColumnNames.Length != sfile.KeyIndices.Length)
                        {
                            continue;
                        }
                        matchFound = true;
                        //Check to see if the all key indices columns are part of one single level's level key.
                        foreach (string levelKeyColName in lk.ColumnNames)
                        {
                            bool matchesKeyIndex = false;
                            foreach (int keyIndex in sfile.KeyIndices)
                            {
                                string keyIndexColName = sfile.Columns[keyIndex].Name;
                                StagingColumn sc = st.getStagingColumn(keyIndexColName);
                                if (sc == null)
                                {
                                    break;
                                }
                                string stagingColName = sc.Name;
                                //Make sure that the corresponding staging column's dimension target is exactly 
                                //the same as dimensional hierarchy level's dimension we are considering for marking
                                //as degenerate.
                                if (levelKeyColName == stagingColName && Array.IndexOf(sc.TargetTypes, h.DimensionName) >= 0)
                                {
                                    matchesKeyIndex = true;
                                    break;
                                }
                            }
                            if (!matchesKeyIndex)
                            {
                                matchFound = false;
                                break;
                            }
                        }
                        if (matchFound && !degenerateLevels.ContainsKey(l))
                        {
                            degenerateLevels.Add(l, null);
                        }
                    }
                }
            }
            //Now set the degenerate flag for all levels in the map
            foreach (Level l in degenerateLevels.Keys)
            {
                l.Degenerate = true;
            }
        }

        /**
         * We mark levels as degenerate based on transactional falg on staging tables as part of processDegenerates() method. Later, as
         * part of method updateDegenerateDimension(), we set the corresponding dimension table physical name to point to the measure
         * table physical name. However, under some conditions, the dimension table physical name will not be updated. This should be 
         * interpreted as that level being non-degenerate. As part of this method, we look for such levels and mark them as non-degenerate.
         **/
        private void validateAndFixupDegenerates(MainAdminForm ma)
        {
            foreach (Hierarchy h in ma.hmodule.getHierarchies())
            {
                if (!h.GeneratedTime)
                {
                    List<Level> llist = new List<Level>();
                    h.getChildLevels(llist);
                    if (llist.Count > 0)
                    {
                        foreach (Level l in llist)
                        {
                            if (!l.Degenerate)
                            {
                                continue;
                            }
                            DimensionTable dt = findDimensionTableAtLevel(h.DimensionName, l.Name, null);
                            if (dt == null || (dt.InheritPrefix != null && dt.InheritTable != null))
                            {
                                continue;
                            }
                            if (!dt.PhysicalName.StartsWith("DW_SF_"))
                            {
                                l.Degenerate = false;
                            }
                        }
                    }
                }
            }
        }

        Dictionary<StagingTable, MeasureGrain> mgcache = new Dictionary<StagingTable, MeasureGrain>(new IdentityComparer<StagingTable>());
        public MeasureGrain getStagingTableGrain(MainAdminForm maf, StagingTable st)
        {
            if (mgcache.ContainsKey(st))
                return (mgcache[st]);
            MeasureGrain mg = new MeasureGrain();
            mg.LoadGroups = st.LoadGroups != null && st.LoadGroups.Length == 0 ? null : st.LoadGroups;
            string connection = null;
            mg.connection = hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : "Default Connection";
            addOverrideKeyColumnsToGrain(mg, st, maf);
            List<MeasureTableGrain> mtglist = new List<MeasureTableGrain>();
            foreach (string[] level in st.Levels)
            {
                MeasureTableGrain mtg = new MeasureTableGrain();
                mtg.DimensionName = level[0];
                mtg.DimensionLevel = level[1];
                mtglist.Add(mtg);
            }
            mg.measureTableGrains = mtglist.ToArray();
            mgcache.Add(st, mg);
            return (mg);
        }

        private bool isAddedDateIDColumn(StagingColumn sc, string customTimeDimension)
        {
            if (sc.DataType.StartsWith("Date ID:") && sc.GenerateTimeDimension &&
                sc.TargetTypes.Length == 1 && sc.NaturalKey)
            {
                return customTimeDimension == null ? sc.TargetTypes[0] == ma.timeDefinition.Name :
                    sc.TargetTypes[0] == customTimeDimension;
            }
            return false;
        }

        private string getCustomTimeDimension(StagingColumn sc, StagingTable st)
        {
            if (st.CustomTimeColumn != null && sc.TargetTypes != null)
            {
                if (st.CustomTimeColumn == sc.Name)
                {
                    foreach (string s in sc.TargetTypes)
                        if (s != "Measure")
                        {
                            return s;
                        }
                }
            }
            return null;
        }

        private void addGeneratedStagingColumns()
        {
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.LiveAccess)
                    continue;
                if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension)
                {
                    // Add generated time ID columns
                    List<StagingColumn> sclist = new List<StagingColumn>();
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.GenerateTimeDimension && (sc.DataType == "DateTime" || sc.DataType == "Date"))
                            sclist.Add(sc);
                    }
                    foreach (StagingColumn sc in sclist)
                    {
                        string lowestSelectedLevel = ma.timeDefinition.getLowestSelectedLevel();
                        string name = sc.getLogicalColumnName() + " " + lowestSelectedLevel + " ID";
                        // Target time generically, but if this is a custom time key column, target the dimension of the source
                        string customTimeDimension = getCustomTimeDimension(sc, st);
                        /* 
                         * If any other staging tables have the same date column for analysis, then add the table name
                         * to the column to distinguish it from other generated dates (only do this for builder versions less 
                         * than 4. With ETL services, it may be more common to add columns that you want to be the same date
                         * to cloned tables. We need to allow that. Now the workaround for users who have date columns with the 
                         * same name in different source staging tables that are not, in fact, the same logical date, is to 
                         * rename the columns
                         */
                        bool found = false;
                        if (ma.builderVersion < 4)
                            foreach (StagingTable st2 in ma.stagingTableMod.getStagingTables())
                            {
                                if (st == st2)
                                    continue;
                                foreach (StagingColumn sc2 in st2.Columns)
                                {
                                    if (sc2.GenerateTimeDimension && (sc2.DataType == "DateTime" || sc2.DataType == "Date") && sc.Name == sc2.Name)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        if (found && ma.builderVersion < 4)
                        {
                            /*
                             * If the application was built before this conflict occurred (e.g. when
                             * sources are added one at a time), delete the old version
                             */
                            StagingColumn foundsc = null;
                            foreach (StagingColumn csc in st.Columns)
                            {
                                if (csc.Name == name && isAddedDateIDColumn(csc, customTimeDimension))
                                {
                                    foundsc = csc;
                                    break;
                                }
                            }
                            if (foundsc != null)
                            {
                                List<StagingColumn> csclist = new List<StagingColumn>(st.Columns);
                                csclist.Remove(foundsc);
                                st.Columns = csclist.ToArray();
                            }
                            // Use the targeted dimension if there is one
                            if (sc.TargetTypes != null && sc.TargetTypes.Length <= 2)
                            {
                                bool sfound = false;
                                foreach (string s in sc.TargetTypes)
                                {
                                    if (s == "Measure")
                                        continue;
                                    name = s + " " + name;
                                    sfound = true;
                                    break;
                                }
                                if (!sfound)
                                    name = st.Name.Substring(3) + " " + name;
                            }
                            else
                                name = st.Name.Substring(3) + " " + name;
                        }
                        StagingColumn newsc = null;
                        foreach (StagingColumn findsc in st.Columns)
                        {
                            if (findsc.getLogicalColumnName() == name)
                            {
                                if (Array.IndexOf<string>(findsc.TargetTypes, customTimeDimension == null ? ma.timeDefinition.Name : customTimeDimension) >= 0)
                                {
                                    newsc = findsc;
                                    break;
                                }
                            }
                        }
                        // Add column if necessary
                        if (newsc == null)
                        {
                            newsc = new StagingColumn();
                            List<StagingColumn> newsclist = new List<StagingColumn>(st.Columns);
                            newsclist.Add(newsc);
                            st.Columns = newsclist.ToArray();
                            newsc.setNameAndPhysicalName(ma, name);
                        }
                        newsc.NaturalKey = true;
                        if (customTimeDimension == null)
                            newsc.TargetTypes = new string[] { ma.timeDefinition.Name };
                        else
                            newsc.TargetTypes = new string[] { customTimeDimension };
                        newsc.DataType = "Date ID: " + lowestSelectedLevel;
                        newsc.GenerateTimeDimension = true;
                        newsc.SourceFileColumn = sc.SourceFileColumn;
                    }
                }
                for (int i = 0; i < st.Levels.Length; i++)
                {
                    Hierarchy h = ma.hmodule.getDimensionHierarchy(st.Levels[i][0]);
                    if (h != null && h.Name != ma.timeDefinition.Name)
                    {
                        Level l = h.findLevel(st.Levels[i][1]);
                        if (l != null && l.Aliases != null)
                        {
                            foreach (LevelAlias la in l.Aliases)
                            {
                                if (la.AliasType == LevelAlias.MEASURE_ONLY)
                                {
                                    foreach (LevelAlias.AliasLevelKey alk in la.AliasKeys)
                                    {
                                        foreach (StagingColumn sc in st.Columns)
                                        {
                                            if (alk.AliasKeyColumn.Equals(sc.Name) && Array.IndexOf<string>(sc.TargetTypes, "Measure") < 0)
                                            {
                                                string[] tTypes = new string[sc.TargetTypes.Length + 1];
                                                for (int j = 0; j < sc.TargetTypes.Length; j++)
                                                    tTypes[j] = sc.TargetTypes[j];
                                                tTypes[sc.TargetTypes.Length] = "Measure";
                                                sc.TargetTypes = tTypes;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void addToDimensionConnections(Dictionary<string, HashSet<string>> dimensionConnections, string dimensionName, string connectionName)
        {
            if (!dimensionConnections.ContainsKey(dimensionName))
                dimensionConnections.Add(dimensionName, new HashSet<string>());
            dimensionConnections[dimensionName].Add(connectionName);
        }

        private void mapDimensions(List<DimensionTable> unmappedDimensionTablesList, List<BuildAction> buildActions, List<string> calculatedDimensionNames, string timeSchema)
        {
            Dictionary<string, object> dimensionNames = new Dictionary<string, object>();
            Dictionary<string, HashSet<string>> dimensionConnections = new Dictionary<string, HashSet<string>>();

            // Identify dimensions that are to be built - start with unmapped dimension tables
            foreach (DimensionTable dt in unmappedDimensionTablesList)
            {
                if (!dimensionNames.ContainsKey(dt.DimensionName))
                {
                    dimensionNames.Add(dt.DimensionName, null);
                }
                List<StagingTable> stables = ma.stagingTableMod.getStagingTables();
                HashSet<string> stConnection = new HashSet<string>();
                if (stables != null)
                {
                    foreach (StagingTable st in stables)
                    {
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] == dt.DimensionName && level[1] == dt.Level)
                            {
                                string localETLConn = findLocalETLConnectionForStagingTable(st);
                                stConnection.Add(localETLConn == null ? "Default Connection" : localETLConn);
                            }
                        }
                    }
                }
                string connectionName = (dt.TableSource != null && dt.TableSource.Connection != null) ? dt.TableSource.Connection : "Default Connection";
                if (!stConnection.Contains(connectionName) && stConnection.Count > 0)
                {
                    foreach (string conn in stConnection)
                    {
                        if (dt.TableSource != null && dt.TableSource.Connection != null)
                            dt.TableSource.Connection = conn;
                        addToDimensionConnections(dimensionConnections, dt.DimensionName, conn);
                    }
                }
                else
                {
                    addToDimensionConnections(dimensionConnections, dt.DimensionName, connectionName);
                }
            }

            List<string> allConnections = new List<string>();
            foreach (LocalETLConfig config in ma.localETLLoadGroupToLocalETLConfig.Values)
            {
                allConnections.Add(config.getLocalETLConnection());
            }
            allConnections.Add("Default Connection");

            // Add calculated dimensions
            // assuming only Time Dimension to be calculated and hence always adding to dimensionConnections for all the connections
            foreach (string s in calculatedDimensionNames)
            {
                if (!dimensionNames.ContainsKey(s))
                    dimensionNames.Add(s, null);
                foreach (string conn in allConnections)
                {
                    addToDimensionConnections(dimensionConnections, s, conn);
                }
            }

            // Add any dimensions from staging tables
            List<StagingTable> stlist = ma.stagingTableMod.getStagingTables();
            if (stlist != null)
                foreach (StagingTable st in stlist)
                {
                    foreach (string[] level in st.Levels)
                    {
                        //check if the staging table is part of local ETL loadgroup and if so, 
                        //find corresponding local ETL connection for it
                        string localETLConn = findLocalETLConnectionForStagingTable(st);
                        if (!dimensionNames.ContainsKey(level[0]))
                        {
                            dimensionNames.Add(level[0], null);
                        }
                        string connectionName = localETLConn == null ? "Default Connection" : localETLConn;
                        addToDimensionConnections(dimensionConnections, level[0], connectionName);
                    }
                }

            // Keep track of any manually defined dimension columns
            Dictionary<string, List<string>> existingDimColMappings = new Dictionary<string, List<string>>();

            foreach (DataRow dr in ma.dimensionColumnMappings.Rows)
            {
                string dimTableName = (string)dr["TableName"];
                bool autogen = (bool)dr["AutoGenerated"];
                bool manedit = (bool)dr["ManuallyEdited"];
                if (autogen && !manedit)
                    continue;
                int index = ma.findDimensionTableIndex(dimTableName);
                if (index != -1)
                {
                    DimensionTable dt = ma.dimensionTablesList[index];
                    string dtConnection = null;
                    if (dt.TableSource == null || dt.TableSource.Connection == null)
                        dtConnection = "Default Connection";
                    else
                        dtConnection = dt.TableSource.Connection;
                    string keySuffix = dtConnection == "Default Connection" ? "" : "_" + dtConnection;
                    string key = dt.DimensionName + keySuffix;
                    List<string> dimCols = null;
                    if (existingDimColMappings.ContainsKey(key))
                    {
                        dimCols = existingDimColMappings[key];
                    }
                    if (dimCols == null)
                    {
                        dimCols = new List<string>();
                    }
                    dimCols.Add((string)dr["ColumnName"]);
                    existingDimColMappings[key] = dimCols;
                }
            }

            foreach (string dim in dimensionConnections.Keys)
            {
                if (dimensionConnections[dim].Contains("Default Connection")
                    && dimensionConnections[dim].Contains(null))
                    dimensionConnections[dim].Remove(null);
            }

            // Generate dimension tables for each dimension identified
            foreach (string dimName in dimensionNames.Keys)
            {
                foreach (string connection in dimensionConnections[dimName])
                {
                    string colKey = connection == null || connection == "Default Connection" ? dimName : dimName + "_" + connection;

                    List<string> columnsAdded = null;
                    if (existingDimColMappings.ContainsKey(colKey))
                    {
                        columnsAdded = existingDimColMappings[colKey];
                    }

                    if (columnsAdded == null)
                    {
                        columnsAdded = new List<string>();
                    }

                    // For each dimension, find each level that is to be generated
                    List<Level> levelList = findLevelsForDimTableGeneration(dimName);
                    if (levelList != null)
                    {
                        bool firstT = true;
                        foreach (Level l in levelList)
                        {
                            Dictionary<string, SecurityFilter> columnsFromStagingTable = getDimensionColumnsFromStaging(dimName, l, buildActions);

                            string dimTableName = generateDimensionTable(dbType, dimName, l, firstT, unmappedDimensionTablesList, columnsAdded,
                                columnsFromStagingTable, buildActions, calculatedDimensionNames.Contains(dimName), timeSchema,
                                null, null, (connection == null || connection == "Default Connection" ? null : connection));

                            firstT = false;

                            if (l.SCDType == 2 && l.GenerateInheritedCurrentDimTable)
                            {
                                generateDimensionTable(dbType, dimName, l, firstT, unmappedDimensionTablesList, columnsAdded,
                                    columnsFromStagingTable, buildActions, calculatedDimensionNames.Contains(dimName), timeSchema,
                                    dimTableName, TYPEII_CURRENT_VERSION_INHERIT_PREFIX, (connection == null || connection == "Default Connection" ? null : connection));
                            }
                            if (l.Aliases != null && l.Aliases.Length > 0)
                            {
                                //generate inherited dimensiontable for each level alias with alias name as prefix
                                foreach (LevelAlias la in l.Aliases)
                                {
                                    if (la.AliasType == LevelAlias.ATTRIBUTE_ONLY)
                                    {
                                        generateDimensionTable(dbType, dimName, l, firstT, unmappedDimensionTablesList, columnsAdded,
                                            columnsFromStagingTable, buildActions, calculatedDimensionNames.Contains(dimName), timeSchema,
                                            dimTableName, la.AliasName + ": ", (connection == null || connection == "Default Connection" ? null : connection));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private string findLocalETLConnectionForStagingTable(StagingTable st)
        {
            //check if the staging table is part of local ETL loadgroup and find corresponding local ETL connection for it
            bool isLocalETLLoadGroup = false;
            string localETLLoadGroup = null;
            string localETLConnection = null;
            foreach (string lg in st.LoadGroups)
            {
                if (lg.StartsWith("LIVEACCESS_"))
                {
                    isLocalETLLoadGroup = true;
                    localETLLoadGroup = lg;
                    break;
                }
            }
            if (isLocalETLLoadGroup && localETLLoadGroup != null &&
                ma.localETLLoadGroupToLocalETLConfig != null && ma.localETLLoadGroupToLocalETLConfig.ContainsKey(localETLLoadGroup))
            {
                localETLConnection = ma.localETLLoadGroupToLocalETLConfig[localETLLoadGroup].getLocalETLConnection();
            }
            bool foundConnection = false;
            if (localETLConnection != null)
            {

                foreach (DatabaseConnection dc in ma.connection.connectionList)
                {
                    if (dc.Name.Equals(localETLConnection))
                    {
                        foundConnection = true;
                        break;
                    }
                }
            }
            if (foundConnection)
            {
                return localETLConnection;
            }
            return null;
        }

        private MeasureGrain findGrain(StagingTable st, string measureColumnName, SecurityFilter secfilter)
        {
            List<MeasureGrain> mglist = ma.getMappedGrains(measureColumnName);
            bool foundGrain = false;
            MeasureGrain grain = null;
            foreach (MeasureGrain mg in mglist)
            {
                if (mg.measureTableGrains.Length != st.Levels.Length)
                {
                    continue;
                }
                int count = 0;
                // See if this grain matches the staging table one
                foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                {
                    bool found = false;
                    foreach (string[] level in st.Levels)
                    {
                        if (level[0] == mtg.DimensionName && level[1] == mtg.DimensionLevel)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        break;
                    else
                        count++;
                }
                if (count == mg.measureTableGrains.Length)
                {
                    foundGrain = true;
                    grain = mg;
                    if (!grain.transactional && st.Transactional)
                        grain.transactional = true;
                    if (!grain.currentSnapshotOnly && st.TruncateOnLoad)
                        grain.currentSnapshotOnly = true;
                    if (grain.filter == null && secfilter != null)
                        grain.filter = secfilter;
                    if (!Util.equalsArray(grain.LoadGroups, st.LoadGroups))
                        grain.LoadGroups = st.LoadGroups != null && st.LoadGroups.Length == 0 ? null : st.LoadGroups;
                    string connection = null;
                    grain.connection = hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : "Default Connection";
                    break;
                }
            }
            if (!foundGrain)
            {
                grain = new MeasureGrain();
                grain.transactional = st.Transactional;
                grain.currentSnapshotOnly = st.TruncateOnLoad;
                grain.LoadGroups = st.LoadGroups != null && st.LoadGroups.Length == 0 ? null : st.LoadGroups;
                string connection = null;
                grain.connection = hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : "Default Connection";
                grain.measureColumnName = measureColumnName;
                grain.filter = secfilter;
                grain.measureTableGrains = new MeasureTableGrain[st.Levels.Length];
                for (int i = 0; i < st.Levels.Length; i++)
                {
                    grain.measureTableGrains[i] = new MeasureTableGrain();
                    grain.measureTableGrains[i].DimensionName = st.Levels[i][0];
                    grain.measureTableGrains[i].DimensionLevel = st.Levels[i][1];
                }
            }

            addOverrideKeyColumnsToGrain(grain, st, ma);

            return (grain);
        }

        public static void addOverrideKeyColumnsToGrain(MeasureGrain grain, StagingTable st, MainAdminForm maf)
        {
            // Check if the stagingtable overrideslevelkeys for this level
            if (st.OverrideLevelKeys != null && st.OverrideLevelKeys.Length > 0)
            {
                foreach (string[] level in st.Levels)
                {
                    foreach (OverrideLevelKey olk in st.OverrideLevelKeys)
                    {
                        if (level[0] == olk.Dimension && level[1] == olk.Level)
                        {
                            if (maf.isHeadless() && grain.overrideKeyColumns.ContainsKey(olk.Dimension + "_" + olk.Level))
                            {
                                maf.getLogger().Warn("Overriding levelkeys for dimension " + olk.Dimension + " level " + olk.Level + " are already defined by another source.");
                            }
                            grain.overrideKeyColumns.Add(olk.Dimension + "_" + olk.Level, new List<string>(olk.KeyColumns));
                            break;
                        }
                    }
                }
            }
        }

        public bool hasLiveAccessLoadGroup(string[] loadGroups, out string liveAccessConnection)
        {
            bool isLiveAccessLoadGroup = false;
            liveAccessConnection = "Default Connection";
            if (loadGroups != null && loadGroups.Length > 0)
            {
                foreach (string lg in loadGroups)
                {
                    if (lg.StartsWith("LIVEACCESS_"))
                    {
                        if (ma.localETLLoadGroupToLocalETLConfig != null && ma.localETLLoadGroupToLocalETLConfig.ContainsKey(lg))
                        {
                            liveAccessConnection = ma.localETLLoadGroupToLocalETLConfig[lg].getLocalETLConnection();
                            isLiveAccessLoadGroup = true;
                            break;
                        }
                    }
                }
            }
            return isLiveAccessLoadGroup;
        }

        public static void writeDependencies(string filename, List<HierarchyLevel[]> dependencies)
        {
            StreamWriter writer = new StreamWriter(filename);
            foreach (HierarchyLevel[] dep in dependencies)
            {
                writer.WriteLine(dep[1].HierarchyName + "." + dep[1].LevelName + "\t->\t" + dep[0].HierarchyName + "." + dep[0].LevelName);
            }
            writer.Close();
        }

        public static string getAggregatedMeasureName(string name, string agg)
        {
            if (agg == "SUM")
                name = "Sum: " + name;
            else if (agg == "AVG")
                name = "Avg: " + name;
            else if (agg == "COUNT")
                name = "# " + name;
            else if (agg == "COUNT DISTINCT")
                name = "# Distinct " + name;
            else if (agg == "MIN")
                name = "Min: " + name;
            else if (agg == "MAX")
                name = "Max: " + name;
            return (name);
        }

        /*
         * Returns aggregation rule and measure name
         */
        public static string[] getDisaggregatedMeasure(string name)
        {
            if (name.StartsWith("Sum: "))
                return (new string[] { "SUM", name.Substring(5) });
            else if (name.StartsWith("Avg: "))
                return (new string[] { "AVG", name.Substring(5) });
            else if (name.StartsWith("# "))
                return (new string[] { "COUNT", name.Substring(2) });
            else if (name.StartsWith("# Distinct "))
                return (new string[] { "COUNT DISTINCT", name.Substring(11) });
            else if (name.StartsWith("Min: "))
                return (new string[] { "MIN", name.Substring(5) });
            else if (name.StartsWith("Max: "))
                return (new string[] { "MAX", name.Substring(5) });
            return (null);
        }

        public static string getStagingColumnMeasureName(StagingColumn sc, string agg, string measureNamePrefix)
        {
            string measureName = ((measureNamePrefix != null && measureNamePrefix.Length > 0) ? measureNamePrefix : "") + sc.getLogicalColumnName();
            return (getAggregatedMeasureName(measureName, agg));
        }

        private List<string> mapMeasure(MainAdminForm ma, StagingColumn sc, string type, int width,
            Dictionary<string, List<MeasureGrain>> unmappedMeasureGrainsByColumnName,
            MeasureGrain grain, Dictionary<string, string> aggregatedMetrics, HashSet<string> averageIntegers,
            string measureNamePrefix)
        {
            List<string> names = new List<string>();
            string measureName = ((measureNamePrefix != null && measureNamePrefix.Length > 0) ? measureNamePrefix : "") + sc.getLogicalColumnName();
            if (sc.TargetAggregations == null)
            {
                ma.addMeasureColumnDataRowIfNecessary(measureName, type, width, null, sc.NaturalKey);
                names.Add(measureName);
                if (unmappedMeasureGrainsByColumnName != null)
                {
                    if (unmappedMeasureGrainsByColumnName.ContainsKey(measureName))
                        unmappedMeasureGrainsByColumnName[measureName].Add(grain);
                    else
                    {
                        List<MeasureGrain> newmglist = new List<MeasureGrain>();
                        newmglist.Add(grain);
                        unmappedMeasureGrainsByColumnName.Add(measureName, newmglist);
                    }
                }
            }
            else
            {
                foreach (string agg in sc.TargetAggregations)
                {
                    string name = getStagingColumnMeasureName(sc, agg, measureNamePrefix);
                    DataRow dr = ma.addMeasureColumnDataRowIfNecessary(name, type, width, agg, sc.NaturalKey);
                    names.Add(name);
                    // Record original metric name
                    if (name != measureName && !aggregatedMetrics.ContainsKey(name))
                    {
                        aggregatedMetrics.Add(name, measureName);
                        if (agg == "AVG" && sc.DataType == "Integer")
                        {
                            dr["DataType"] = "Float";
                            averageIntegers.Add(name);
                        }
                        dr["RootMeasure"] = measureName;
                    }
                    if (unmappedMeasureGrainsByColumnName != null)
                    {
                        if (unmappedMeasureGrainsByColumnName.ContainsKey(name))
                            unmappedMeasureGrainsByColumnName[name].Add(grain);
                        else
                        {
                            List<MeasureGrain> newmglist = new List<MeasureGrain>();
                            newmglist.Add(grain);
                            unmappedMeasureGrainsByColumnName.Add(name, newmglist);
                        }
                    }
                }
            }
            return names;
        }

        private void mapMeasures(Dictionary<string, List<MeasureGrain>> unmappedMeasureGrainsByColumnName,
            List<HierarchyLevel[]> dependencies, List<BuildAction> buildActions)
        {
            // Add unmapped measures from staging tables
            List<StagingTable> stlist = ma.stagingTableMod.getStagingTables();
            Dictionary<string, string> aggregatedMetrics = new Dictionary<string, string>();
            HashSet<string> averageIntegers = new HashSet<string>();
            Dictionary<string, string> addedForeignKeys = new Dictionary<string, string>();
            Dictionary<string, string> addedDateForeignKeys = new Dictionary<string, string>();
            Dictionary<string, string> addedDateForeignKeyDimensions = new Dictionary<string, string>();
            Dictionary<MeasureGrain, object> measureGrains = new Dictionary<MeasureGrain, object>();
            DataView mdv = new DataView(ma.measureColumnMappings, null, "TableName,PhysicalName",
                DataViewRowState.CurrentRows);
            foreach (StagingTable st in stlist)
            {
                int grainCount = 0;
                foreach (string[] level in st.Levels)
                {
                    if (ma.timeDefinition != null && ma.timeDefinition.Name == level[0])
                        continue;
                    grainCount++;
                }
                // Don't include metrics in tables that don't have grain other than time targeted
                if (ma.builderVersion >= 5 && grainCount == 0)
                    continue;
                string localETLConnectionForST = findLocalETLConnectionForStagingTable(st);
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.TargetTypes != null && sc.TargetTypes.Length > 0 && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                    {
                        string type = sc.DataType;
                        int width = sc.Width;
                        if (type.StartsWith("Date ID"))
                            type = "Integer";

                        MeasureGrain grain = findGrain(st, sc.getLogicalColumnName(), sc.SecFilter);
                        measureGrains.Add(grain, null);

                        mapMeasure(ma, sc, type, width, unmappedMeasureGrainsByColumnName, grain, aggregatedMetrics, averageIntegers, null);
                    }
                    else
                    {
                        /*
                         * Include any key columns that are in the staging table that are targeted for a 
                         * dimension in the grain and are marked as natural key and are time ids
                         */
                        if (sc.NaturalKey && sc.GenerateTimeDimension && !(sc.DataType == "DateTime" || sc.DataType == "Date"))
                        {
                            MeasureGrain grain = findGrain(st, sc.getLogicalColumnName(), null);
                            measureGrains.Add(grain, null);
                            foreach (string s in sc.TargetTypes)
                            {
                                bool include = false;
                                foreach (string[] sl in st.Levels)
                                {
                                    if (sl[0] == s)
                                    {
                                        string type = sc.DataType;
                                        if (type.StartsWith("Date ID"))
                                            type = "Integer";
                                        string name = getForeignKeyLogicalName(s, sc.getLogicalColumnName());
                                        if (!addedForeignKeys.ContainsKey(name))
                                            addedForeignKeys.Add(name, getForeignKeyPhysicalName(dbType, s, sc.getLogicalColumnName(), PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion));
                                        if (!addedDateForeignKeys.ContainsKey(name))
                                            addedDateForeignKeys.Add(name, sc.DataType);
                                        if (!addedDateForeignKeyDimensions.ContainsKey(name))
                                        {
                                            string tt = null;
                                            foreach (string ss in sc.TargetTypes)
                                            {
                                                if (ss == "Measure")
                                                    continue;
                                                tt = ss;
                                            }
                                            addedDateForeignKeyDimensions.Add(name, tt);
                                        }
                                        ma.addMeasureColumnDataRowIfNecessary(name,
                                            type, sc.Width, null, true);
                                        if (unmappedMeasureGrainsByColumnName.ContainsKey(name))
                                            unmappedMeasureGrainsByColumnName[name].Add(grain);
                                        else
                                        {
                                            List<MeasureGrain> newmglist = new List<MeasureGrain>();
                                            newmglist.Add(grain);
                                            unmappedMeasureGrainsByColumnName.Add(name, newmglist);
                                        }
                                        include = true;
                                        break;
                                    }
                                }
                                if (include)
                                    break;
                            }
                        }
                    }
                }
            }

            /*
             * For added foreign keys, carry them down to lower grains
             */
            if (CARRY_KEYS_DOWN_LEVELS)
                foreach (string measureColumnName in unmappedMeasureGrainsByColumnName.Keys)
                {
                    if (addedForeignKeys.ContainsKey(measureColumnName))
                    {
                        List<MeasureGrain> mglist = unmappedMeasureGrainsByColumnName[measureColumnName];
                        List<MeasureGrain> addList = new List<MeasureGrain>();
                        foreach (MeasureGrain mg in mglist)
                        {
                            foreach (MeasureGrain mg2 in measureGrains.Keys)
                            {
                                bool containsGrain = false;
                                foreach (MeasureGrain mglistgrain in mglist)
                                {
                                    if (mglistgrain.atSameGrain(mg2))
                                    {
                                        containsGrain = true;
                                        break;
                                    }
                                }
                                if (!containsGrain)
                                    foreach (MeasureGrain smg in addList)
                                    {
                                        if (mg2.atSameGrain(smg))
                                        {
                                            containsGrain = true;
                                            break;
                                        }
                                    }
                                if (!containsGrain)
                                {
                                    /*
                                     * For each measure table grain in the overall list, see if it already is mapped for this
                                     * measure. If not, then see if this grain is lower than the currently mapped one. If so,
                                     * add it to the list of to-be-mapped grains
                                     */
                                    List<MeasureTableGrain[]> matches = isLowerGrain(ma, mg.measureTableGrains, mg2.measureTableGrains, dependencies, automaticModeDependencies);
                                    if (matches != null)
                                    {
                                        /*
                                         * Don't allow carrying of keys from a non-transactional grain to a transactional one
                                         */
                                        if (ma.builderVersion < 7 || !(!mg.transactional && mg2.transactional))
                                        {
                                            MeasureGrain newmg = new MeasureGrain();
                                            newmg.LoadGroups = mg2.LoadGroups;
                                            newmg.connection = mg2.connection;
                                            newmg.overrideKeyColumns = mg2.overrideKeyColumns;
                                            newmg.measureTableName = mg2.measureTableName;
                                            newmg.measureColumnName = mg.measureColumnName;
                                            newmg.measureTableGrains = mg2.measureTableGrains;
                                            newmg.transactional = mg2.transactional;
                                            newmg.currentSnapshotOnly = mg2.currentSnapshotOnly;
                                            addList.Add(newmg);
                                        }
                                    }
                                }
                            }
                        }
                        mglist.AddRange(addList);
                    }
                }

            bool updateNames = false;
            List<MeasureTable> addedKeys = new List<MeasureTable>();
            if (unmappedMeasureGrainsByColumnName != null)
            {
                foreach (string measureColumnName in unmappedMeasureGrainsByColumnName.Keys)
                {
                    List<MeasureGrain> mgList = unmappedMeasureGrainsByColumnName[measureColumnName];
                    foreach (MeasureGrain mg in mgList)
                    {
                        MeasureTable mt = findMeasureTableAtGrain(ma, mg);
                        if (mt == null)
                        {
                            List<DimensionTable> dimTablesList = getDimTablesForMeasureGrain(mg, false);
                            mt = ma.findMeasureTableWithJoins(dimTablesList);
                        }
                        if (mt == null)
                        {
                            //Create a new measure table and add the column to it
                            mt = createNewMeasureTable(mg, null, null, false);
                            if (mt != null)
                            {
                                addedKeys.Add(mt);
                                buildActions.Add(new BuildAction("Generated measure table: " + mt.TableName, MessageType.BASIC));
                                List<DimensionTable> dimTablesList = getDimTablesForMeasureGrain(mg, true);
                                if ((dimTablesList != null) && (dimTablesList.Count > 0))
                                {
                                    addJoinsForMeasureTable(mt, dimTablesList, false, false);
                                }
                                mtlist.Add(mt, mg);                                
                            }
                        }
                        if (mt != null)
                        {
                            if (!addedKeys.Contains(mt))
                            {
                                if (mt.RecalculateCardinality)
                                    mt.Cardinality = 1;
                                addKeyColumns(mt, mg, false);
                                addedKeys.Add(mt);
                                mtlist.Add(mt, mg);
                            }
                            bool checkDupe = true;
                            string mapName = null;
                            bool castFloat = false;
                            if (aggregatedMetrics.ContainsKey(measureColumnName))
                            {
                                checkDupe = false;
                                mapName = aggregatedMetrics[measureColumnName];
                                if (averageIntegers.Contains(measureColumnName))
                                    castFloat = true;
                            }
                            else
                                mapName = measureColumnName;
                            string physicalName = null;
                            if (addedForeignKeys.ContainsKey(mapName))
                            {
                                checkDupe = false;
                                physicalName = addedForeignKeys[mapName];
                            }
                            else
                                physicalName = Util.generatePhysicalName(mapName, ma.builderVersion) + PHYSICAL_SUFFIX;
                            DataRowView[] drvlist = null;
                            List<DataRowView> autoGenDrvList = null;
                            if (checkDupe)
                                do
                                {
                                    // Prevent duplicate physical names
                                    drvlist = mdv.FindRows(new object[] { mt.TableName, physicalName });
                                    autoGenDrvList = new List<DataRowView>();
                                    if (drvlist != null && drvlist.Length > 0)
                                    {
                                        foreach (DataRowView drv in drvlist)
                                        {
                                            if ((bool)drv["AutoGenerated"])
                                            {
                                                autoGenDrvList.Add(drv);
                                            }
                                        }
                                    }
                                    if (autoGenDrvList.Count > 0)
                                    {
                                        physicalName += "_";
                                    }
                                } while (autoGenDrvList.Count > 0);
                            DataRow dr = ma.addMeasureColumnMapping(measureColumnName, physicalName, mt, true, MAX_PHYSICAL_COLUMN_NAME_LENGTH, true);
                            if (castFloat && (bool)dr["Qualify"] && requiresFloatCast)
                            {
                                dr["PhysicalName"] = "CAST(" + mt.TableSource.Tables[0].PhysicalName + "." + dr["PhysicalName"] + " AS FLOAT)";
                                dr["Qualify"] = false;
                            }
                            updateNames = true;
                            updateDegenerateDimension(mt, mg);
                            List<DimensionTable> dimTablesList = getDimTablesForMeasureGrain(mg, true);
                            if ((dimTablesList != null) && (dimTablesList.Count > 0))
                            {
                                addJoinsForMeasureTable(mt, dimTablesList, false, false);
                            }
                            //Check if any level alias is defined for any of the levels of grain of mt
                            //if so, generate an inherited version of fact table overriding joins to level
                            foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                            {
                                foreach (Level l in ma.hmodule.getDimensionLevels(mtg.DimensionName))
                                {
                                    if (mtg.DimensionLevel.Equals(l.Name))
                                    {
                                        if (l.Aliases != null)
                                        {
                                            foreach (LevelAlias la in l.Aliases)
                                            {
                                                if (la.AliasType == LevelAlias.MEASURE_ONLY)
                                                {
                                                    bool foundAllKeyColumns = true;
                                                    foreach (LevelAlias.AliasLevelKey alk in la.AliasKeys)
                                                    {
                                                        bool found = false;
                                                        DataRow[] rows = ma.measureColumnMappings.Select("TableName='" + mt.TableName + "'");
                                                        foreach (DataRow row in rows)
                                                        {
                                                            string colName = (string)row["ColumnName"];
                                                            if (colName.Contains("."))
                                                                colName = colName.Substring(colName.IndexOf(".") + 1);
                                                            if (colName.Contains(": "))
                                                                colName = colName.Substring(colName.IndexOf(": ") + 2);
                                                            if (colName.Equals(alk.AliasKeyColumn))
                                                            {
                                                                found = true;
                                                                break;
                                                            }
                                                        }                                                        
                                                        if (!found)
                                                        {
                                                            foundAllKeyColumns = false;
                                                            break;
                                                        }
                                                    }
                                                    if (foundAllKeyColumns)
                                                    {
                                                        //generate inherited fact table
                                                        MeasureTable inheritMT = createNewMeasureTable(mg, mt, la.AliasName + ": ", false);
                                                        if (inheritMT != null && dimTablesList != null && dimTablesList.Count > 0)
                                                        {
                                                            List<DimensionTable> aliasDimTablesList = new List<DimensionTable>();
                                                            List<DimensionTable> nonAliasDimTablesList = new List<DimensionTable>();
                                                            foreach (DimensionTable dt in dimTablesList)
                                                            {
                                                                if (mtg.DimensionName == dt.DimensionName)
                                                                {
                                                                    aliasDimTablesList.Add(dt);
                                                                }
                                                                else
                                                                {
                                                                    nonAliasDimTablesList.Add(dt);
                                                                }
                                                            }
                                                            addJoinsForMeasureTable(inheritMT, aliasDimTablesList, true, false);
                                                            addJoinsForMeasureTable(inheritMT, nonAliasDimTablesList, false, false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Add any security filters associated with the column if its enabled
                            if (mg.filter != null && mg.filter.Enabled)
                            {
                                List<TableSource.TableSourceFilter> flist = null;
                                if (mt.TableSource.Filters == null)
                                    flist = new List<TableSource.TableSourceFilter>();
                                else
                                    flist = new List<TableSource.TableSourceFilter>(mt.TableSource.Filters);

                                string filterPart = null;
                                if (mg.filter.Type == SecurityFilter.TYPE_VARIABLE)
                                {
                                    filterPart = " IN (V{" + mg.filter.SessionVariable + "})";
                                }
                                else
                                {
                                    // set based filter. Put logical query directly
                                    filterPart = " IN (Q{" + mg.filter.SessionVariable + "})";
                                }
                                string fstring = (((bool)dr["Qualify"]) ? (mt.TableSource.Tables[0].PhysicalName + ".") : "") + dr["PhysicalName"] + filterPart;

                                if (mg.filter.addIsNullJoinCondition)
                                {
                                    fstring = "(" + fstring + " OR " + mt.TableSource.Tables[0].PhysicalName + "." + dr["PhysicalName"] + " IS NULL)";
                                }

                                bool found = false;
                                foreach (TableSource.TableSourceFilter tsf in flist)
                                {
                                    if (tsf.SecurityFilter && tsf.Filter == fstring)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                // Add if not a dupe
                                if (!found)
                                {
                                    TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                                    tsf.Filter = fstring;
                                    tsf.SecurityFilter = true;
                                    if (mg.filter.FilterGroups != null)
                                    {
                                        List<string> tsfFilterGroups = new List<string>(mg.filter.FilterGroups);
                                        tsf.FilterGroups = tsfFilterGroups.ToArray();
                                    }
                                    tsf.LogicalColumnName = (string)dr["ColumnName"];
                                    flist.Add(tsf);
                                    mt.TableSource.Filters = flist.ToArray();
                                }
                            }
                            buildActions.Add(new BuildAction("Added measure column " + measureColumnName + " to measure table " + mt.TableName, MessageType.VERBOSE));
                        }
                    }
                }
            }
            // Create any inherited measure tables needed
            if (generateInheritedTables)
                generateInheritedMeasureTables();
            // Add any logical expressions
            addLogicalExpressions(buildActions);
            // Second pass to add date alias tables
            List<MeasureTable> curList = new List<MeasureTable>(ma.measureTablesList);
            if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension)
                foreach (MeasureTable mt in mtlist.Keys)
                {
                    if (mt.Type != MeasureTable.NORMAL)
                        continue;
                    DataRow[] rows = ma.measureColumnMappings.Select("TableName='" + mt.TableName + "'");
                    foreach (DataRow dr in rows)
                    {
                        string colName = (string)dr["ColumnName"];
                        if (addedDateForeignKeys.ContainsKey(colName))
                        {
                            int index = colName.IndexOf('.');
                            string prefix = colName.Substring(index + 1);
                            prefix = prefix.Replace(" " + ApplicationBuilder.DayIDName, "");
                            prefix = prefix.Replace(" " + ApplicationBuilder.WeekIDName, "");
                            prefix = prefix.Replace(" " + ApplicationBuilder.MonthIDName, "");
                            List<MeasureTable> inheritList = new List<MeasureTable>();
                            inheritList.Add(mt);
                            foreach (MeasureTable imt in curList)
                                if (imt.InheritTable != null)
                                {
                                    if (imt.Type != MeasureTable.NORMAL)
                                        continue;
                                    if (imt.InheritTable == mt.TableName)
                                    {
                                        if (!inheritList.Contains(imt))
                                            inheritList.Add(imt);
                                    }
                                    else
                                    {
                                        string[] tlist = imt.InheritTable.Split(new char[] { ',' });
                                        foreach (string s in tlist)
                                        {
                                            if (s == mt.TableName && !inheritList.Contains(imt))
                                                inheritList.Add(imt);
                                        }
                                    }
                                }
                            foreach (MeasureTable curmt in inheritList)
                            {
                                MeasureGrain mg = mtlist[mt];
                                if (curmt != mt)
                                {
                                    mg = (MeasureGrain)mg.Clone();
                                    mg.measureTableName = prefix + TIME_PREFIX_SEPARATOR + curmt.TableName;
                                }
                                bool found = false;
                                foreach (MeasureTable smt in ma.measureTablesList)
                                    if (smt.TableName == mg.measureTableName)
                                    {
                                        found = true;
                                        break;
                                    }
                                if (found)
                                    continue;
                                MeasureTable newmt = createNewMeasureTable(mg, curmt, prefix + TIME_PREFIX_SEPARATOR, curmt != mt);
                                if (newmt == null)
                                    continue;
                                newmt.TimeAlias = true;
                                if (curmt != mt)
                                    newmt.TableSource = curmt.TableSource.clone();
                                // Get target level - remove "Date ID" from the type
                                string level = addedDateForeignKeys[colName].Substring(9);
                                string targetDim = addedDateForeignKeyDimensions[colName];
                                foreach (DimensionTable dt in ma.dimensionTablesList)
                                {
                                    if (!dt.AutoGenerated)
                                        continue;
                                    if (dt.DimensionName == ma.timeDefinition.Name)
                                    {
                                        TimePeriod tp = null;
                                        if (ma.timeDefinition.Periods != null)
                                            foreach (TimePeriod stp in ma.timeDefinition.Periods)
                                            {
                                                if (stp.Prefix + " " == dt.VirtualMeasuresPrefix)
                                                {
                                                    tp = stp;
                                                    break;
                                                }
                                            }
                                        if (dt.Level == level || tp != null)
                                        {
                                            List<string> keyList = new List<string>();
                                            int dimSepIndex = colName.IndexOf('.');
                                            keyList.Add(colName.Substring(dimSepIndex + 1));
                                            List<string> keyList2 = new List<string>();
                                            Hierarchy h = ma.hmodule.getDimensionHierarchy(dt.DimensionName);
                                            Level l = h.findLevel(level);
                                            if (tp != null)
                                                keyList2.Add(timePeriodKey(tp, l));
                                            else
                                                keyList2.Add(l.getSurrogateKey().ColumnNames[0]);
                                            if (!equalsConnectionWithNull(dt, newmt))
                                                continue;
                                            DataRow jr = addJoins(keyList, keyList2, dt, newmt, false, false, targetDim, null);
                                            if (tp != null && jr != null)
                                                jr[0] = tp.Prefix + " " + jr[0];
                                            if (tp != null && dt.Level != level && jr != null)
                                            {
                                                /* 
                                                 * If not aggregating at the day level, but this is a 
                                                 * time period, join through the day table as a bridge
                                                 */
                                                DimensionTable joinDt = null;
                                                foreach (DimensionTable sdt in ma.dimensionTablesList)
                                                {
                                                    if (!sdt.AutoGenerated)
                                                        continue;
                                                    if (dt.DimensionName == sdt.DimensionName && sdt.Level == level)
                                                    {
                                                        joinDt = sdt;
                                                        break;
                                                    }
                                                }
                                                if (joinDt != null)
                                                {
                                                    // Build bridge join
                                                    List<string> keyList3 = new List<string>();
                                                    keyList3.Add(l.getSurrogateKey().ColumnNames[0]);
                                                    string condition1 = ma.getJoinCondition(joinDt, keyList, keyList3, newmt, targetDim, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                                                    Hierarchy h2 = ma.hmodule.getDimensionHierarchy(joinDt.DimensionName);
                                                    Level l2 = h2.findLevel(dt.Level);
                                                    List<string> keyList4 = new List<string>();
                                                    List<string> keyList5 = new List<string>();
                                                    keyList4.Add(l2.getSurrogateKey().ColumnNames[0]);
                                                    keyList5.Add(timePeriodKey(tp, l2));
                                                    string condition2 = ma.getJoinCondition(dt, keyList4, keyList5, joinDt);
                                                    string joinCondition = condition1 + " AND " + condition2;

                                                    joinCondition = joinCondition.Replace("\"" + joinDt.TableName + "\"",
                                                        (joinDt.TableSource.Schema != null ? joinDt.TableSource.Schema + "." : "") + joinDt.TableSource.Tables[0].PhysicalName);
                                                    jr[2] = joinCondition;
                                                }
                                            }
                                            if (tp != null && jr != null)
                                            {
                                                jr[2] = ((string)jr[2]).Replace(newmt.TableName, tp.Prefix + " " + newmt.TableName);
                                            }
                                            // If this is a compound table source, make sure the join is properly qualified for the right table
                                            // Since it may be inherited, need to search through  all tables to find where key exists
                                            if (newmt.TableSource.Tables.Length > 1)
                                            {
                                                MeasureTable foundmt = null;
                                                DataRowView[] drv = ma.measureColumnMappingsNamesView.FindRows(dt.DimensionName + "." + keyList[0]);
                                                foreach (TableSource.TableDefinition td in newmt.TableSource.Tables)
                                                {
                                                    foreach (DataRowView jdr in drv)
                                                    {
                                                        string foundName = (string)jdr["TableName"];
                                                        foreach (MeasureTable pmt in ma.measureTablesList)
                                                        {
                                                            if (pmt.TableSource.Tables.Length != 1)
                                                                continue;
                                                            if (pmt.TableName == foundName && pmt.TableSource.Tables[0].PhysicalName == td.PhysicalName)
                                                            {
                                                                foundmt = pmt;
                                                                break;
                                                            }
                                                        }
                                                        if (foundmt != null)
                                                            break;
                                                    }
                                                    if (foundmt != null)
                                                        break;
                                                }
                                                if (foundmt != null && jr != null)
                                                {
                                                    if (tp == null)
                                                        jr[2] = ((string)jr[2]).Replace("\"" + newmt.TableName + "\"" + ".", "\"" + newmt.TableName + "\"" + "." + foundmt.TableSource.Tables[0].PhysicalName + ".");
                                                    else
                                                        jr[2] = ((string)jr[2]).Replace("\"" + tp.Prefix + " " + newmt.TableName + "\"" + ".", "\"" + tp.Prefix + " " + newmt.TableName + "\"" + "." + foundmt.TableSource.Tables[0].PhysicalName + ".");
                                                }
                                            }
                                            if (dt.Level != level)
                                            {
                                                // Either add joins bridging through the day table
                                            }
                                            else
                                            {
                                                // Or invalidate non-day-table joins
                                                string dtConn = null;
                                                if (dt.TableSource != null)
                                                    dtConn = dt.TableSource.Connection;
                                                invalidateInheritedDimensionJoins(dt.DimensionName, dtConn, newmt, mt);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            if (updateNames)
                ma.updateMeasureColumnNames();
            unmappedMeasureGrainsByColumnName.Clear();
        }

        /*
         * Returns whether one grain (mtglist2) is at a lower level than another (mtglist)
         */
        public static List<MeasureTableGrain[]> isLowerGrain(MainAdminForm ma, IList<MeasureTableGrain> mtglist, IList<MeasureTableGrain> mtglist2, List<HierarchyLevel[]> dependencies, bool automaticModeDependencies)
        {
            List<MeasureTableGrain[]> matches = new List<MeasureTableGrain[]>();
            Dictionary<MeasureTableGrain, MeasureTableGrain> grainDependencies = new Dictionary<MeasureTableGrain, MeasureTableGrain>();
            foreach (MeasureTableGrain mtg in mtglist)
            {
                bool found = false;
                foreach (MeasureTableGrain mtg2 in mtglist2)
                {
                    if (mtg2.DimensionName == mtg.DimensionName)
                    {
                        if (mtg2.DimensionLevel != mtg.DimensionLevel)
                        {
                            Hierarchy h = ma.hmodule.getDimensionHierarchy(mtg.DimensionName);
                            if (h == null)
                                continue;
                            Level l = h.findLevel(mtg.DimensionLevel);
                            /*
                             * Look one level down only. In the future, could consider looking 
                             * down multiple levels which would create more complex ETL - would have
                             * to join through staging tables to ultimately get to the key of interest.
                             * Restricting to one level ensures that a single join to a single staging
                             * table can be used to lookup a foreign key
                             */
                            if (l != null && l.findLevel(h, mtg2.DimensionLevel, false) != null)
                            {
                                matches.Add(new MeasureTableGrain[] { mtg, mtg2 });
                                grainDependencies.Add(mtg, mtg2);
                                found = true;
                                break;
                            }
                        }
                        else
                        {
                            found = true;
                            matches.Add(new MeasureTableGrain[] { mtg, mtg2 });
                            grainDependencies.Add(mtg, mtg2);
                            break;
                        }
                    }
                }
                if (!found)
                {
                    if (dependencies != null)
                    {
                        /*
                         * If a level isn't found, then see if it's dependent on one of the other levels
                         * in the grain - if so, it is OK
                         */
                        foreach (HierarchyLevel[] dep in dependencies)
                        {
                            if (dep[1].HierarchyName == mtg.DimensionName && dep[1].LevelName == mtg.DimensionLevel)
                            {
                                foreach (MeasureTableGrain mtg2 in mtglist)
                                {
                                    if (mtg2 != mtg && dep[0].HierarchyName == mtg2.DimensionName &&
                                        dep[0].LevelName == mtg2.DimensionLevel)
                                    {
                                        if (!isGrainDependency(mtg2, mtg, grainDependencies))
                                        {
                                            found = true;
                                            matches.Add(new MeasureTableGrain[] { mtg, mtg2 });
                                            grainDependencies.Add(mtg, mtg2);
                                            break;
                                        }
                                    }
                                }
                            }
                            if (found)
                                break;
                        }
                        /*
                         * If a level isn't found, then see if it's dependent on one of the other levels
                         * in the other grain - if so, it is OK. Don't do this in automatic mode because 
                         * grain dependencies can be more than just foreign key relationshis and it's possible
                         * to have a dependency where the keys cannot be populated downhill.
                         */
                        if (!found && ma.builderVersion >= 8 && !automaticModeDependencies)
                        {
                            foreach (HierarchyLevel[] dep in dependencies)
                            {
                                if (dep[1].HierarchyName == mtg.DimensionName && dep[1].LevelName == mtg.DimensionLevel)
                                {
                                    foreach (MeasureTableGrain mtg2 in mtglist2)
                                    {
                                        if (mtg2 != mtg && dep[0].HierarchyName == mtg2.DimensionName &&
                                            dep[0].LevelName == mtg2.DimensionLevel)
                                        {
                                            if (!isGrainDependency(mtg2, mtg, grainDependencies))
                                            {
                                                found = true;
                                                matches.Add(new MeasureTableGrain[] { mtg, mtg2 });
                                                grainDependencies.Add(mtg, mtg2);
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (found)
                                    break;
                            }
                        }
                    }
                }
                if (!found)
                    return (null);
            }
            return (matches);
        }

        /*
         * See if a dependencey (either direct or indirect) has been mapped from mtg1 to mtg2 
         */
        private static bool isGrainDependency(MeasureTableGrain mtg1, MeasureTableGrain mtg2, Dictionary<MeasureTableGrain,
            MeasureTableGrain> grainDependencies)
        {
            if (grainDependencies.ContainsKey(mtg1))
            {
                if (grainDependencies[mtg1] == mtg2)
                    return (true);
                else if (isGrainDependency(grainDependencies[mtg1], mtg2, grainDependencies))
                    return (true);
            }
            return (false);
        }

        public static MeasureTable findMeasureTableAtGrain(MainAdminForm ma, MeasureGrain mg)
        {
            MeasureTable existingMeasureTable = null;
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                if (mt.Grain != null && mt.Grain.atSameGrain(mg))
                {
                    existingMeasureTable = mt;
                    break;
                }
            }
            return existingMeasureTable;
        }

        private List<DimensionTable> getDimTablesForMeasureGrain(MeasureGrain mg, bool includeAncestorLevels)
        {
            List<DimensionTable> dimTableList = new List<DimensionTable>();
            if ((mg != null) && (mg.measureTableGrains != null) && (mg.measureTableGrains.Length > 0))
            {
                string localETLConnection = mg.connection == null || mg.connection == "Default Connection" ? null : mg.connection;
                foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                {
                    DimensionTable dt = findDimensionTableAtLevel(mtg.DimensionName, mtg.DimensionLevel, localETLConnection);
                    if (dt != null)
                    {
                        if (!dimTableList.Contains(dt))
                        {
                            dimTableList.Add(dt);
                        }
                    }
                    if (includeAncestorLevels)
                    {
                        List<Level> ancestorLevels = new List<Level>();
                        List<Hierarchy> hlist = ma.hmodule.getHierarchies();
                        foreach (Hierarchy h in hlist)
                        {
                            if (h.DimensionName.Equals(mtg.DimensionName))
                            {
                                h.findAncestors(mtg.DimensionLevel, ancestorLevels);
                                break;
                            }
                        }
                        if (ancestorLevels != null && ancestorLevels.Count > 0)
                        {
                            foreach (Level al in ancestorLevels)
                            {
                                DimensionTable ancestordt = findDimensionTableAtLevel(mtg.DimensionName, al.Name, localETLConnection);
                                if ((ancestordt != null) && (!dimTableList.Contains(ancestordt)))
                                {
                                    dimTableList.Add(ancestordt);
                                }
                            }
                        }
                    }
                }
            }
            return dimTableList;
        }

        private DimensionTable findDimensionTableAtLevel(string dimensionName, string levelName, string localETLConnection)
        {
            DimensionTable dimensionTable = null;
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if ((dimensionName.Equals(dt.DimensionName))
                    && (levelName == dt.Level)
                    && (dt.VirtualMeasuresInheritTable == null || dt.VirtualMeasuresInheritTable.Trim() == "")
                    && dt.AutoGenerated
                    && (dt.InheritTable == null || dt.InheritTable.Trim() == ""))
                {
                    if (localETLConnection != null)
                    {
                        if (dt.TableSource != null && !localETLConnection.Equals(dt.TableSource.Connection))
                            continue;
                    }
                    else if (!(dt.TableSource == null || dt.TableSource.Connection == null || dt.TableSource.Connection == "Default Connection"))
                    {
                        continue;
                    }
                    dimensionTable = dt;
                    break;
                }
            }
            return dimensionTable;
        }

        private List<string> getLevelKeyColumnsList(string dimensionName, string levelName)
        {
            List<string> levelColumnsList = new List<string>();
            Level l = findLevel(dimensionName, levelName);
            foreach (LevelKey lk in l.Keys)
            {
                if ((lk.ColumnNames != null) && (lk.ColumnNames.Length > 0))
                {
                    foreach (string keyColName in lk.ColumnNames)
                    {
                        if (!levelColumnsList.Contains(keyColName))
                        {
                            levelColumnsList.Add(keyColName);
                        }
                    }
                }
            }
            return levelColumnsList;
        }

        private List<string> getDimColNames(DimensionTable dt)
        {
            List<string> dimColNames = new List<string>();
            foreach (DataRow dcmr in ma.dimensionColumnMappings.Rows)
            {
                string tableName = (string)dcmr["TableName"];
                string colName = (string)dcmr["ColumnName"];
                if (dt.InheritTable != null)
                {
                    if ((!tableName.Equals(dt.TableName)) && (!tableName.Equals(dt.InheritTable)))
                    {
                        continue;
                    }
                }
                else
                {
                    if (!tableName.Equals(dt.TableName))
                    {
                        continue;
                    }
                }
                dimColNames.Add(colName);
            }
            return dimColNames;
        }

        private List<Level> findLevelsForDimTableGeneration(string dimensionName)
        {
            Hierarchy h = ma.hmodule.getDimensionHierarchy(dimensionName);
            List<Level> levelList = ma.hmodule.getDimensionLevels(dimensionName);
            List<Level> result = new List<Level>();
            foreach (Level l in levelList)
            {
                if (!l.GenerateDimensionTable)
                {
                    continue;
                }
                // Position in the ordered list that this particular level
                // table needs to be inserted at.
                int insertPos = -1;
                List<Level> decendents = new List<Level>();
                l.getChildLevels(h, decendents);
                if (decendents.Count > 0)
                {
                    foreach (Level decendentLevel in decendents)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            String genLevelName = result[i].Name;
                            if (genLevelName != null && (decendentLevel.Name == genLevelName) && ((insertPos == -1) || (i < insertPos)))
                            {
                                insertPos = i;
                            }
                        }
                    }
                }
                if (insertPos != -1)
                {
                    if (!result.Contains(l))
                    {
                        result.Insert(insertPos, l);
                    }
                }
                else if (!result.Contains(l))
                {
                    result.Add(l);
                }
            }
            if (result.Count == 0 && h != null)
            {
                Level l = h.findLevelContainingDimensionKey();
                if (l != null && !result.Contains(l))
                    result.Add(l);
            }
            return (result);
        }

        public static string getPhysicalTableName(SqlGenType dbType, string dimName, Level l, int maxLength, int builderVersion)
        {
            String physicalName = dimName.ToUpper();

            if (l != null)
            {
                physicalName += "_" + l.Name.ToUpper();
            }
            string tname = builderVersion >= 26 ? ("DW_DM_" + Util.generatePhysicalName(physicalName, builderVersion)) : (Util.generatePhysicalName("DW_DM_" + physicalName, builderVersion));
            if (tname.Length > maxLength)
            {
                if (builderVersion >= 15)
                    tname = "DW_DM_" + getMD5Hash(dbType, tname, maxLength, builderVersion);
                else
                    tname = getMD5Hash(dbType, tname, maxLength, builderVersion);
            }
            return (tname);
        }

        public static string getLogicalTableName(string dimName, Level l, string localETLConnection)
        {
            String dimTableName = dimName;

            if (l != null)
            {
                dimTableName += " " + l.Name;
            }
            if (localETLConnection != null && localETLConnection != "Default Connection")
            {
                dimTableName += " " + localETLConnection;
            }
            return (dimTableName);
        }

        private string generateDimensionTable(SqlGenType dbType, string dimName, Level l, bool useUnmappedTable, List<DimensionTable> unmappedDimensionTablesList, List<string> columnsAdded, Dictionary<string, SecurityFilter> columnsFromStagingTable, List<BuildAction> buildActions, bool calculated, string timeSchema, string inheritTable, string inheritPrefix, string dimensionLocalETLConnection)
        {
            bool isInheritedTable = (inheritTable != null && inheritPrefix != null && inheritTable.Length > 0 && inheritPrefix.Length > 0);
            String dimTableName = (isInheritedTable ? inheritPrefix : "") + getLogicalTableName(dimName, l, dimensionLocalETLConnection);
            String physicalName = getPhysicalTableName(dbType, dimName, l, MAX_PHYSICAL_TABLE_NAME_LENGTH, ma.builderVersion);

            DimensionTable selectedDimensionTable = null;
            bool generateTable = true;
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if (dt.TableName == dimTableName)
                {
                    selectedDimensionTable = dt;
                    generateTable = false;
                    break;
                }
            }

            if (useUnmappedTable)
            {
                foreach (DimensionTable dimTable in unmappedDimensionTablesList)
                {
                    if (dimTable.DimensionName == dimName && dimTable.Level == l.Name)
                    {
                        selectedDimensionTable = dimTable;
                        break;
                    }
                }
            }

            if (selectedDimensionTable == null)
            {
                selectedDimensionTable = new DimensionTable();
                selectedDimensionTable.TableSource = new TableSource();
                selectedDimensionTable.TableSource.Connection = dimensionLocalETLConnection == null ? ma.connection.connectionList[0].Name : dimensionLocalETLConnection;
                selectedDimensionTable.TableSource.Tables = new TableSource.TableDefinition[1];
                selectedDimensionTable.TableSource.Tables[0] = new TableSource.TableDefinition();
            }

            if (selectedDimensionTable != null)
            {
                buildActions.Add(new BuildAction("Generated dimension table: " + dimTableName, MessageType.BASIC));
                selectedDimensionTable.TableName = dimTableName;
                selectedDimensionTable.DimensionName = dimName;
                selectedDimensionTable.TableSource.Connection = findConnection(selectedDimensionTable.TableSource.Connection);
                selectedDimensionTable.TableSource.Tables[0].PhysicalName = physicalName;
                selectedDimensionTable.PhysicalName = selectedDimensionTable.TableSource.Tables[0].PhysicalName;
                selectedDimensionTable.AutoGenerated = true;
                selectedDimensionTable.Calculated = calculated;
                if (ma.serverPropertiesModule.disableCachingForDimensionsBox.Checked)
                    selectedDimensionTable.Cacheable = false;
                if (isInheritedTable)
                {
                    selectedDimensionTable.InheritPrefix = inheritPrefix;
                    selectedDimensionTable.InheritTable = inheritTable;
                }
                if (ma.timeDefinition != null && ma.timeDefinition.Name == dimName && timeSchema != null)
                    selectedDimensionTable.TableSource.Schema = timeSchema;
                if (generateTable)
                    l = ma.addDimensionTable(selectedDimensionTable, l, !isInheritedTable, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                else if (!isInheritedTable)
                {
                    Hierarchy h = ma.hmodule.getDimensionHierarchy(selectedDimensionTable.DimensionName);
                    ma.addDimensionTableSurrogateKey(selectedDimensionTable, h, l, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                }

                if (!isInheritedTable)
                {
                    bool levelContainsDimensionKey = false;
                    if (l != null)
                    {
                        Hierarchy h = ma.hmodule.getDimensionHierarchy(dimName);
                        if (h != null && h.DimensionKeyLevel == l.Name)
                            levelContainsDimensionKey = true;
                    }
                    foreach (DataRow dr in ma.dimensionColumns.Select("DimensionName='" + dimName + "'"))
                    {
                        if (!((bool)dr["AutoGenerated"]))
                            continue;
                        string colName = (string)dr["ColumnName"];
                        bool isLevelKey = false;
                        bool levelContainsDimensionColumn = false;
                        bool anotherLevelContainsDimensionColumn = false;
                        if (l != null)
                        {
                            foreach (LevelKey lk in l.Keys)
                            {
                                if (Array.IndexOf<string>(lk.ColumnNames, colName) >= 0)
                                {
                                    isLevelKey = true;
                                    break;
                                }
                            }
                            levelContainsDimensionColumn = l.containsDimensionColumn(colName, ((ma.timeDefinition != null) && (dimName == ma.timeDefinition.Name)));
                            if (!levelContainsDimensionColumn)
                            {
                                bool foundpl = false;
                                foreach (Hierarchy h in ma.hmodule.getHierarchies())
                                {
                                    if (h.DimensionName == dimName)
                                    {
                                        Level pl = h.findLevelWithColumn(colName);
                                        if (pl != null)
                                        {
                                            if (pl.findLevel(h, l.Name) != null)
                                            {
                                                foundpl = true;
                                                break;
                                            }
                                            else if (pl.GenerateDimensionTable)
                                            {
                                                anotherLevelContainsDimensionColumn = true;
                                            }
                                        }
                                    }
                                }
                                if (foundpl)
                                    levelContainsDimensionColumn = true;
                            }
                            if (!isLevelKey)
                            {
                                Hierarchy h = ma.hmodule.getDimensionHierarchy(dimName);
                                List<Level> ancestorLevels = new List<Level>();
                                h.findAncestors(l.Name, ancestorLevels);
                                foreach (Level al in ancestorLevels)
                                {
                                    foreach (LevelKey lk in al.Keys)
                                    {
                                        if (Array.IndexOf<string>(lk.ColumnNames, colName) >= 0)
                                        {
                                            isLevelKey = true;
                                            break;
                                        }
                                    }
                                    if (isLevelKey)
                                        break;
                                }
                            }
                        }
                        bool isGeneratedTime = ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension && dimName == ma.timeDefinition.Name;
                        if (columnsAdded.Contains(colName) && !isGeneratedTime && !isLevelKey)
                        {
                            continue;
                        }
                        if (((!anotherLevelContainsDimensionColumn) && (levelContainsDimensionKey))
                            || levelContainsDimensionColumn
                            || ((!anotherLevelContainsDimensionColumn) && (columnsFromStagingTable.ContainsKey(colName)))
                            || isLevelKey)
                        {
                            bool exists = dimensionColumnRowExists(dimTableName, colName);
                            bool existsInAggregate = false;
                            if (!exists)
                            {
                                existsInAggregate = dimensionColumnExistsInAggregateAsLogicalColumnDefinition(dimName, colName);
                            }
                            if (!exists && !existsInAggregate)
                            {
                                string pname = Util.generatePhysicalName(colName, ma.builderVersion) + PHYSICAL_SUFFIX;
                                List<DataRow> drlist = null;
                                List<DataRow> autoGenDrList = null;
                                do
                                {
                                    // Prevent duplicate physical names
                                    autoGenDrList = new List<DataRow>();
                                    drlist = Util.selectRows(ma.dimensionColumnMappings,
                                     new string[][] { new string[] { "TableName", dimTableName },
                                     new string[] { "PhysicalName", pname } }, false);
                                    if (drlist != null && drlist.Count > 0)
                                    {
                                        foreach (DataRow drow in drlist)
                                        {
                                            if ((bool)drow["AutoGenerated"])
                                            {
                                                autoGenDrList.Add(drow);
                                            }
                                        }
                                    }

                                    if (autoGenDrList.Count > 0)
                                    {
                                        pname += "_";
                                    }
                                } while (autoGenDrList.Count > 0);
                                addDimensionColumnMapping(ma, dimTableName, colName, pname, true,
                                    levelContainsDimensionKey, levelContainsDimensionColumn,
                                    columnsFromStagingTable, columnsAdded, buildActions, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                                // Add any security filters associated with the column
                                if (columnsFromStagingTable.ContainsKey(colName))
                                {
                                    SecurityFilter sf = columnsFromStagingTable[colName];
                                    if (sf != null && sf.Enabled)
                                    {
                                        List<TableSource.TableSourceFilter> flist = null;
                                        if (selectedDimensionTable.TableSource.Filters == null)
                                            flist = new List<TableSource.TableSourceFilter>();
                                        else
                                            flist = new List<TableSource.TableSourceFilter>(selectedDimensionTable.TableSource.Filters);

                                        string filterPart = null;
                                        if (sf.Type == SecurityFilter.TYPE_VARIABLE)
                                        {
                                            filterPart = " IN (V{" + sf.SessionVariable + "})";
                                        }
                                        else
                                        {
                                            // set based filter. Put logical query directly and wrap it with Q construct
                                            // Q construct will be expanded while building where clause for table source filters
                                            filterPart = " IN (Q{" + sf.SessionVariable + "})";
                                        }

                                        string fstring = selectedDimensionTable.TableSource.Tables[0].PhysicalName + "." + pname + filterPart;

                                        if (sf.addIsNullJoinCondition)
                                        {
                                            fstring = "(" + fstring + " OR " + selectedDimensionTable.TableSource.Tables[0].PhysicalName + "." + pname + " IS NULL)";
                                        }

                                        bool found = false;
                                        foreach (TableSource.TableSourceFilter tsf in flist)
                                        {
                                            if (tsf.SecurityFilter && tsf.Filter == fstring)
                                            {
                                                found = true;
                                                break;
                                            }
                                        }
                                        // Add if not a dupe
                                        if (!found)
                                        {
                                            TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                                            tsf.Filter = fstring;
                                            tsf.SecurityFilter = true;
                                            if (sf.FilterGroups != null)
                                            {
                                                List<string> tsfFilterGroups = new List<string>(sf.FilterGroups);
                                                tsf.FilterGroups = tsfFilterGroups.ToArray();
                                            }
                                            tsf.LogicalColumnName = colName;
                                            flist.Add(tsf);
                                            selectedDimensionTable.TableSource.Filters = flist.ToArray();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                /*
                 * Add type II SCD columns if necessary
                 */
                if (l.SCDType == 2 && !isInheritedTable)
                {
                    ma.addDimensionColumnDataRowIfNecessary(dimName, "Start Date", "DateTime", 0, null);
                    DataRow dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dimTableName;
                    dcmr["ColumnName"] = "Start Date";
                    dcmr["Qualify"] = true;
                    dcmr["PhysicalName"] = "SCD_START_DATE";
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                    columnsAdded.Add("Start Date");
                    buildActions.Add(new BuildAction("Added column Start Date to dimension table " + dimTableName, MessageType.VERBOSE));
                    ma.addDimensionColumnDataRowIfNecessary(dimName, "End Date", "DateTime", 0, null);
                    dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dimTableName;
                    dcmr["ColumnName"] = "End Date";
                    dcmr["Qualify"] = true;
                    dcmr["PhysicalName"] = "SCD_END_DATE";
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                    columnsAdded.Add("End Date");
                    buildActions.Add(new BuildAction("Added column End Date to dimension table " + dimTableName, MessageType.VERBOSE));

                    //Add the Is Current column based on end date being null
                    string colName = l.Name + " Is Current";
                    ma.addDimensionColumnDataRowIfNecessary(dimName, colName, "Varchar", 1, null);
                    dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dimTableName;
                    dcmr["ColumnName"] = colName;
                    dcmr["Qualify"] = false;
                    dcmr["PhysicalName"] = "CASE WHEN " + physicalName + ".SCD_END_DATE IS NULL THEN 'Y' ELSE 'N' END";
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                    columnsAdded.Add(colName);
                    buildActions.Add(new BuildAction("Added column End Date to dimension table " + dimTableName, MessageType.VERBOSE));
                }

                //Add table source filter for current version of SCD type II dimension tables.
                if ((ma.builderVersion >=16) && (l.SCDType == 2) && (isInheritedTable) && (inheritPrefix == TYPEII_CURRENT_VERSION_INHERIT_PREFIX))
                {
                    string tableSourceFilterString = physicalName + ".SCD_END_DATE IS NULL";
                    bool currentFilterExists = false;
                    List<TableSource.TableSourceFilter> existingFilters = new List<TableSource.TableSourceFilter>();
                    if ((selectedDimensionTable.TableSource.Filters != null) && (selectedDimensionTable.TableSource.Filters.Length > 0))
                    {
                        foreach (TableSource.TableSourceFilter tsf in selectedDimensionTable.TableSource.Filters)
                        {
                            existingFilters.Add(tsf);
                            if ((tsf.Filter != null) && (tsf.Filter == tableSourceFilterString))
                            {
                                currentFilterExists = true;
                            }
                        }
                    }
                    //The < 100 check is to make sure repository doesn't keep growing if there is a bug in the code.
                    if (!currentFilterExists && (existingFilters.Count <= 100))
                    {
                        TableSource.TableSourceFilter tsf = new TableSource.TableSourceFilter();
                        tsf.Filter = tableSourceFilterString;
                        int newCount = existingFilters.Count + 1;
                        selectedDimensionTable.TableSource.Filters = new TableSource.TableSourceFilter[newCount];
                        for (int i = 0; i < existingFilters.Count; i++)
                        {
                            selectedDimensionTable.TableSource.Filters[i] = existingFilters[i];
                        }
                        selectedDimensionTable.TableSource.Filters[newCount - 1] = tsf;
                    }
                }

            }
            return dimTableName;
        }

        private DataRow addDimensionColumnMapping(MainAdminForm ma, string dimTableName, string colName, string physicalName, bool qualify, bool levelContainsDimensionKey,
            bool levelContainsDimensionColumn, Dictionary<string, SecurityFilter> columnsFromStagingTable, List<string> columnsAdded, List<BuildAction> buildActions,
            int maxColLength)
        {
            DataRow dcmr = ma.dimensionColumnMappings.NewRow();
            dcmr["TableName"] = dimTableName;
            dcmr["ColumnName"] = colName;
            dcmr["Qualify"] = qualify;
            if ((qualify && maxColLength > 0 && physicalName.Length > maxColLength))
            {
                physicalName = ApplicationBuilder.getMD5Hash(dbType, physicalName, maxColLength, ma.builderVersion) + (ma.builderVersion>=28 ? ApplicationBuilder.PHYSICAL_SUFFIX : "");
            }
            dcmr["PhysicalName"] = physicalName;
            dcmr["AutoGenerated"] = true;
            ma.dimensionColumnMappings.Rows.Add(dcmr);
            if (levelContainsDimensionKey || levelContainsDimensionColumn || (columnsFromStagingTable != null && columnsFromStagingTable.ContainsKey(colName)))
                if (columnsAdded != null)
                    columnsAdded.Add(colName);
            buildActions.Add(new BuildAction("Added column " + colName + " to dimension table " + dimTableName + " as " + physicalName, MessageType.VERBOSE));
            return (dcmr);
        }


        public static bool ContainsNonASCIICharacters(string s)
        {
            bool result = false;
            if (s == null)
                return false;
            char c;
            for (int i = 0; i < s.Length; i++)
            {
                c = s[i];
                if (c > 0x007F)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /**
         * if a connection named 'connectionName' exists, otherwise return the default connection
         */
        private string findConnection(string connectionName)
        {
            foreach (DatabaseConnection dc in ma.connection.connectionList)
            {
                if (dc.Name == connectionName)
                    return connectionName;
            }
            return "Default Connection";
        }

        private bool dimensionColumnRowExists(string dimTableName, string colName)
        {
            bool exists = false;
            DataRowView[] drv = ma.dimensionColumnMappingsView.FindRows(new object[] { colName, dimTableName });
            if (drv.Length > 0)
            {
                exists = true;
            }
            else
            {
                foreach (DimensionTable dt in ma.dimensionTablesList)
                {
                    if (dt.InheritTable == dimTableName)
                    {
                        drv = ma.dimensionColumnMappingsView.FindRows(new object[] { colName, dt.TableName });
                        if (drv.Length > 0)
                        {
                            exists = true;
                        }
                        if (exists)
                        {
                            break;
                        }
                    }
                    if (dt.VirtualMeasuresInheritTable == dimTableName)
                    {
                        drv = ma.dimensionColumnMappingsView.FindRows(new object[] { colName, dt.TableName });
                        if (drv.Length > 0)
                        {
                            exists = true;
                        }
                        if (exists)
                        {
                            break;
                        }
                    }
                }
            }
            return exists;
        }

        private bool dimensionColumnExistsInAggregateAsLogicalColumnDefinition(string dimName, string colName)
        {
            bool exists = false;
            Aggregate[] aggList = ma.aggModule.getAggregateList();
            if (aggList != null && aggList.Length > 0)
            {
                foreach (Aggregate agg in aggList)
                {
                    if (agg.Transformations != null && agg.Transformations.Length > 0)
                    {
                        foreach (AggTransformation at in agg.Transformations)
                        {
                            if (at.ColumnName == colName && at.Dimension == dimName)
                            {
                                if (at.Transformations != null && at.Transformations.Length > 0)
                                {
                                    foreach (Transformation tr in at.Transformations)
                                    {
                                        if (tr.GetType() == typeof(LogicalColumnDefinition))
                                        {
                                            exists = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (exists)
                            {
                                break;
                            }
                        }
                        if (exists)
                        {
                            break;
                        }
                    }
                }
            }
            return exists;
        }

        private Dictionary<string, SecurityFilter> getDimensionColumnsFromStaging(string dimName, Level l, List<BuildAction> buildActions)
        {
            Dictionary<string, SecurityFilter> columnsFromStagingTable = new Dictionary<string, SecurityFilter>();
            // Pull in any other columns from staging tables
            List<StagingTable> stlist = ma.stagingTableMod.getStagingTables();
            foreach (StagingTable st in stlist)
            {
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == dimName)
                    {
                        Dictionary<string, int> widthMap = new Dictionary<string, int>();
                        foreach (StagingColumn sc in st.Columns)
                        {
                            string scLogicalName = sc.getLogicalColumnName();
                            bool levelFound = false;
                            List<string> colList = new List<string>();
                            if ((sc.GrainInfo == null) || (sc.GrainInfo.Length == 0))
                            {
                                colList.Add(scLogicalName);
                            }
                            else
                            {
                                bool prefixFound = false;
                                foreach (StagingColumnGrainInfo scgf in sc.GrainInfo)
                                {
                                    if ((scgf.Level == l.Name) && (Array.IndexOf<string>(sc.TargetTypes, scgf.Dimension) >= 0))
                                    {
                                        string name = scLogicalName;
                                        if (scgf.NewName != null && scgf.NewName.Trim().Length > 0)
                                        {
                                            if (scgf.NewNameType == StagingColumnGrainInfo.TYPE_PREFIX)
                                                name = scgf.NewName + scLogicalName;
                                            else if (scgf.NewNameType == StagingColumnGrainInfo.TYPE_RENAME)
                                                name = scgf.NewName;
                                            prefixFound = true;
                                        }
                                        if (!colList.Contains(name))
                                            colList.Add(name);
                                        if (scgf.Aggregation != null && scgf.Aggregation.Type == "Map")
                                        {
                                            int length = 0;
                                            foreach (string s in scgf.Aggregation.ValueList)
                                            {
                                                if (s.Length > length) length = s.Length;
                                            }
                                            if (scgf.Aggregation.Default != null && scgf.Aggregation.Default.Length > length)
                                                length = scgf.Aggregation.Default.Length;
                                            widthMap.Add(name, length);
                                        }
                                        levelFound = true;
                                    }
                                }
                                if (!prefixFound && levelFound)
                                {
                                    if (!colList.Contains(scLogicalName))
                                        colList.Add(scLogicalName);
                                }
                            }
                            /*
                             * Make sure all the natural level keys are added for the level of the table.
                             * Pick the level key that is the higher of that of the staging table level
                             * or the current dimension table level as that will be the join key
                             */
                            Hierarchy sh = ma.hmodule.getDimensionHierarchy(level[0]);
                            Level keyLevel = null;
                            if (sh != null)
                            {
                                Level sl = sh.findLevel(level[1]);
                                if (sl != null)
                                {
                                    if (sl != l && sl.findLevel(sh, l.Name) != null)
                                    {
                                        keyLevel = sl;
                                    }
                                    else
                                        keyLevel = l;
                                }
                                else
                                    keyLevel = l;
                            }
                            else
                                keyLevel = l;
                            bool isLevelKey = false;
                            foreach (LevelKey lk in keyLevel.Keys)
                            {
                                if (!lk.SurrogateKey)
                                {
                                    foreach (string s in lk.ColumnNames)
                                    {
                                        if (scLogicalName == s)
                                        {
                                            isLevelKey = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            /*
                             * If the levels of the staging table is the same as the level to be generated, then add the column
                             */
                            if (isLevelKey || (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, dimName) >= 0))
                            {
                                if (level[1] == l.Name)
                                {
                                    levelFound = true;
                                }
                                else
                                {
                                    /*
                                     * Make sure that any level key columns are also included 
                                     */
                                    for (int i = 0; i < l.Keys.Length; i++)
                                    {
                                        for (int j = 0; j < l.Keys[i].ColumnNames.Length; j++)
                                        {
                                            if (colList.Contains(l.Keys[i].ColumnNames[j]))
                                            {
                                                levelFound = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                foreach (string colName in colList)
                                {
                                    /*
                                     * Make sure every column is in the appropriate dimension for later mapping
                                     */
                                    int width = sc.Width;
                                    if (widthMap.ContainsKey(colName) && widthMap[colName] != sc.Width)
                                        width = widthMap[colName];
                                    string dtype = sc.DataType;
                                    bool isDateID = sc.DataType.StartsWith("Date ID: ");
                                    if (isDateID)
                                        dtype = "Integer";
                                    if (isDateID && ma.timeDefinition != null && dimName == ma.timeDefinition.Name)
                                        continue;
                                    DataRow dr = ma.addDimensionColumnDataRowIfNecessary(dimName, colName, dtype, width, sc.UnknownValue);
                                    if (isDateID)
                                        dr["Key"] = true;
                                    dr["Index"] = sc.Index;
                                    // Label generics
                                    if (sc.GrainInfo != null)
                                        foreach (StagingColumnGrainInfo scgf in sc.GrainInfo)
                                        {
                                            if (scgf.NewName != null && scgf.NewName.Trim().Length > 0 &&
                                                scgf.NewNameType == StagingColumnGrainInfo.TYPE_PREFIX)
                                            {
                                                if (((string)dr["ColumnName"]) == scgf.NewName + scLogicalName)
                                                    dr["GenericName"] = scLogicalName;
                                            }
                                        }
                                    if (dr != null)
                                    {
                                        buildActions.Add(new BuildAction("Added attribute " + colName + " to dimension " + dimName + " from staging table " + st.Name, MessageType.VERBOSE));
                                    }
                                    if (levelFound)
                                    {
                                        if (!columnsFromStagingTable.ContainsKey(colName))
                                        {
                                            columnsFromStagingTable.Add(colName, sc.SecFilter);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return (columnsFromStagingTable);
        }

        public string getMeasureTableBaseName(MeasureGrain mg)
        {
            StringBuilder sb = new StringBuilder();
            if (mg.measureTableGrains != null && mg.measureTableGrains.Length > 0)
            {
                List<string> names = new List<string>();
                foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                {
                    names.Add(mtg.DimensionLevel);
                }
                if (ma.builderVersion >= 3)
                    names.Sort();
                foreach (string s in names)
                {
                    if (sb.Length > 0) sb.Append(' ');
                    sb.Append(s);
                }
            }
            return sb.ToString();
        }

        public string getLogicalMeasureTableName(string prefix, MeasureGrain mg)
        {
            string n = getMeasureTableBaseName(mg);
            string localETLConn = mg.connection == null || mg.connection == "Default Connection" ? null : mg.connection;
            return getMeasureTableName(prefix, n, localETLConn);
        }

        private static string getMeasureTableName(string prefix, string baseName, string localETLConn)
        {
            return (prefix != null ? prefix + " " : "") + baseName + " Fact" + (localETLConn != null && localETLConn != "Default Connection" ? " " + localETLConn : "");
        }

        public string getMeasureTablePhysicalName(string baseName)
        {
            string result = ma.builderVersion >= 26 ? "DW_SF_" + Util.generatePhysicalName(baseName.ToUpper(), ma.builderVersion) : Util.generatePhysicalName("DW_SF_" + baseName.ToUpper(), ma.builderVersion); 
            if (result.Length > MAX_PHYSICAL_TABLE_NAME_LENGTH)
            {
                // Return the hexadecimal string.
                result = "DW_SF_" + getMD5Hash(dbType, baseName, MAX_PHYSICAL_TABLE_NAME_LENGTH, ma.builderVersion);
            }
            return result;
        }

        private MeasureTable createNewMeasureTable(MeasureGrain mg, MeasureTable inheritTable, string prefix, bool userMeasureGrainName)
        {
            string localETLConn = mg.connection == null || mg.connection == "Default Connection" ? null : mg.connection;
            string baseName = getMeasureTableBaseName(mg);
            // Check to see if the same measure table definition already exists.
            string measureTableName = getMeasureTableName(prefix, baseName, localETLConn);
            // Add measure table definition
            MeasureTable mt = new MeasureTable();
            mt.TableName = userMeasureGrainName ? mg.measureTableName : measureTableName;
            mt.Transactional = mg.transactional;
            mt.CurrentSnapshotOnly = mg.currentSnapshotOnly;
            mt.LoadGroups = mg.LoadGroups;
            mt.TableSource = new TableSource();
            mt.TableSource.Connection = localETLConn == null ? ma.connection.connectionList[0].Name : localETLConn;
            if (ma.serverPropertiesModule.disableMeasureTableCacheBox.Checked)
                mt.Cacheable = false;
            if (inheritTable == null || inheritTable.OpaqueView == null || inheritTable.OpaqueView.Length == 0)
            {
                mt.TableSource.Tables = new TableSource.TableDefinition[1];
                mt.TableSource.Tables[0] = new TableSource.TableDefinition();
                if (!userMeasureGrainName)
                    mt.TableSource.Tables[0].PhysicalName = getMeasureTablePhysicalName(baseName);
                else
                    mt.TableSource.Tables[0].PhysicalName = inheritTable.TableSource.Tables[0].PhysicalName;
                mt.PhysicalName = mt.TableSource.Tables[0].PhysicalName;
            }
            else
            {
                mt.PhysicalName = inheritTable.PhysicalName;
            }
            mt.Type = 0;
            mt.Cardinality = inheritTable != null ? inheritTable.Cardinality : 1;
            mt.AutoGenerated = true;
            // Make sure its not a dupe
            foreach (MeasureTable smt in ma.measureTablesList)
            {
                if (smt.TableName == mt.TableName)
                {
                    return null;
                }
            }
            ma.addNewMeasureTable(mt);
            if (inheritTable == null)
            {
                addKeyColumns(mt, mg, true);
                // Look for any degenerate dimension tables and reset name
                updateDegenerateDimension(mt, mg);
            }
            else
            {
                mt.InheritTable = inheritTable.TableName;
                mt.InheritPrefix = prefix;
            }
            return mt;
        }

        public static string getMD5Hash(SqlGenType dbType, string input, int maxLength, int builderVersion)
        {
            // Ensure names don't get too long. If explanatory name is too long, substitute Hashcode
            MD5 md5Hasher = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            Encoding encoding = Encoding.Default;
            if ((builderVersion >= Repository.I18NLogicalNames) && (builderVersion != int.MaxValue))
            {
                encoding = Encoding.UTF8;
            }
            byte[] data = md5Hasher.ComputeHash(encoding.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                if (builderVersion >= 12)
                    sBuilder.Append(data[i].ToString("x2").ToUpper());//use upper case as oracle is not case sensitive
                else
                    sBuilder.Append(data[i].ToString("x2"));
            }
            if (builderVersion >= 9 || dbType == SqlGenType.Oracle) // this change was added for 4.3 (builderVersion 9)
            {
                // names generally must start with characters
                if (sBuilder[0] >= '0' && sBuilder[0] <= '9')
                {
                    if (builderVersion >= 12)
                        sBuilder.Insert(0, 'X');//use upper case as oracle is not case sensitive
                    else
                        sBuilder.Insert(0, 'x');
                }
                if (maxLength > 0 && sBuilder.Length > maxLength)
                {
                    if (dbType == SqlGenType.Oracle)
                        return sBuilder.ToString().Substring(0, maxLength).ToUpper();//always uppercase for oracle
                    return sBuilder.ToString().Substring(0, maxLength);
                }
            }
            if (dbType == SqlGenType.Oracle)
                return (sBuilder.ToString().ToUpper()); //always uppercase for oracle
            return (sBuilder.ToString());
        }

        /*
         * For a given measure table, if there are degenerate dimensions as part of it's grain, update them to point to the fact table instead
         * of a physical dimension table. But only do it if this fact table has the HIGHEST grain with that dimension in it.
         */ 
        private void updateDegenerateDimension(MeasureTable mt, MeasureGrain mg)
        {
            List<DimensionTable> dimTablesList = getDimTablesForMeasureGrain(mg, false);
            foreach (DimensionTable dt in dimTablesList)
            {
                if (dt.AutoGenerated && dt.Level != null)
                {
                    Level l = findLevel(dt.DimensionName, dt.Level);
                    if ((l != null) && (l.Degenerate))
                    {
                        /*
                         * Now, make sure this is the highest level grain for this degenerate level
                         */
					    bool isHighestLevelGrain = true;
                        foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
                        {
                            if (st.Disabled || st.DiscoveryTable || st.Levels == null || st.Levels.Length <= 1)
                                continue;
                            MeasureGrain stgrain = getStagingTableGrain(ma, st);
                            if (stgrain.atSameGrain(mt.Grain))
                                continue;
                            if (isLowerGrain(ma, stgrain.measureTableGrains, mt.Grain.measureTableGrains, null, false) != null)
                            {
                                foreach (MeasureTableGrain mtg2 in stgrain.measureTableGrains)
                                {
                                    Level l2 = findLevel(mtg2.DimensionName, mtg2.DimensionLevel);
                                    if (l2 == l)
                                    {
                                        isHighestLevelGrain = false;
                                        break;
                                    }
                                }
                                if (!isHighestLevelGrain)
                                    break;
                            }
                        }
					    if (!isHighestLevelGrain)
						    continue;
                        /* 
                         * If it's a degenerate dimension, only override if this fact table has a smaller grain
                         */
                        bool ok = true;
                        if (dt.PhysicalName.StartsWith("DW_SF_"))
                        {
                            MeasureTable curmt = null;
                            foreach (MeasureTable smt in ma.measureTablesList)
                            {
                                if (smt.AutoGenerated && smt.TableSource.Tables[0].PhysicalName == dt.TableSource.Tables[0].PhysicalName)
                                {
                                    curmt = smt;
                                    break;
                                }
                            }
                            if (curmt != null)
                            {
                                List<MeasureTableGrain> curgrain = ma.getGrainInfo(curmt);
                                if (curgrain.Count < ma.getGrainInfo(mt).Count)
                                    ok = false;
                            }
                        }
                        if (ok)
                        {
                            // update the table physical name in security filter string
                            updateTableNamesinSecurityFilters(dt, mt.PhysicalName);
                            dt.PhysicalName = mt.PhysicalName;
                            dt.TableSource.Tables[0].PhysicalName = mt.PhysicalName;
                            // Now make sure that higher level keys use the fact table format
                            DataRowView[] drv = ma.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                            List<Level> llist = ma.hmodule.getDimensionLevels(dt.DimensionName);
                            foreach (DataRowView dr in drv)
                            {
                                string cname = (string) dr["ColumnName"];
                                foreach (Level cl in llist)
                                {
                                    if (cl.Name == dt.Level)
                                        continue;
                                    foreach (LevelKey lk in cl.Keys)
                                    {
                                        foreach (string kname in lk.ColumnNames)
                                        {
                                            if (cname == kname)
                                            {
                                                dr["PhysicalName"] = getForeignKeyPhysicalName(dbType, dt.DimensionName, cname, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void updateTableNamesinSecurityFilters(DimensionTable dimTable, String updatedTablePhysicalName)
        {
            if (dimTable != null && dimTable.TableSource != null && dimTable.TableSource.Filters != null && dimTable.TableSource.Filters.Length > 0)
            {
                foreach (TableSource.TableSourceFilter tsf in dimTable.TableSource.Filters)
                {
                    String tableSourcePhysicalName = dimTable.TableSource.Tables[0].PhysicalName;
                    if (tsf.SecurityFilter && tsf.Filter != null && tableSourcePhysicalName != updatedTablePhysicalName)
                    {
                        tsf.Filter = tsf.Filter.Replace(tableSourcePhysicalName, updatedTablePhysicalName);
                    }
                }
            }
        }

        private string getForeignKeyLogicalName(string dimensionName, string colName)
        {
            return (dimensionName + "." + colName);
        }

        public static string getForeignKeyPhysicalName(SqlGenType dbType, string dimensionName, string colName, string suffix, int maxColLength, int builderVersion)
        {
            string cname = (dimensionName == null ? "" : Util.generatePhysicalName(dimensionName, builderVersion) + suffix) + Util.generatePhysicalName(colName, builderVersion) + suffix;
            if (maxColLength > 0 && cname.Length > maxColLength)
                cname = getMD5Hash(dbType, cname, maxColLength, builderVersion) + ((builderVersion >= 24 && dbType != SqlGenType.Oracle) ? (suffix + suffix) : "");
            return cname;
        }

        /*
         * Add the required key columns to a measure table
         */
        private void addKeyColumns(MeasureTable mt, MeasureGrain mg, bool recalcCardinality)
        {
            if (mg.measureTableGrains != null)
            {
                List<StagingTable> stlist = ma.stagingTableMod.getStagingTables();
                Dictionary<string, Hierarchy> keyColumnsAdded = new Dictionary<string, Hierarchy>();
                foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                {
                    Hierarchy mtgh = ma.hmodule.getDimensionHierarchy(mtg.DimensionName);
                    if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension && mtgh != null && mtgh.GeneratedTime)
                        continue;
                    if (mtgh == null)
                        continue;
                    Level l = mtgh.findLevel(mtg.DimensionLevel);
                    if (l == null)
                        continue;
                    // Add keys for current level
                    foreach (string s in addKeysForLevel(mtgh, l, mt, recalcCardinality))
                        if (!keyColumnsAdded.ContainsKey(s))
                            keyColumnsAdded.Add(s, mtgh);
                }
                /*
                 * See if another staging table exists at a higher grain. If so, then so will a fact table. Get its
                 * natural keys as well because they can be carried down. Otherwise, if a staging table
                 * matches the level, add any level keys in that staging table from higher levels
                 */
                foreach (StagingTable st in stlist)
                {
                    Dictionary<Level, Hierarchy> levelList = new Dictionary<Level, Hierarchy>();
                    bool lower = false;
                    bool matchGrain = true;
                    foreach (string[] level in st.Levels)
                    {
                        bool found = false;
                        foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                        {
                            if (level[0] == mtg.DimensionName)
                            {
                                found = true;
                                if (level[1] != mtg.DimensionLevel)
                                {
                                    Hierarchy h = ma.hmodule.getDimensionHierarchy(mtg.DimensionName);
                                    if (h == null)
                                        continue;
                                    if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension && h != null && h.GeneratedTime)
                                        continue;
                                    Level l = h.findLevel(level[1]);
                                    if (l == null)
                                        continue;
                                    if (l.findChildLevel(h, mtg.DimensionLevel, false) != null)
                                    {
                                        levelList.Add(l, h);
                                        lower = true;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        if (!found)
                        {
                            matchGrain = false;
                            levelList.Clear();
                            break;
                        }
                    }
                    if (matchGrain && !lower)
                    {
                        foreach (MeasureTableGrain mtg in mg.measureTableGrains)
                        {
                            Hierarchy mtgh = ma.hmodule.getDimensionHierarchy(mtg.DimensionName);
                            if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension && mtgh != null && mtgh.GeneratedTime)
                                continue;
                            if (mtgh == null)
                                continue;
                            Level l = mtgh.findLevel(mtg.DimensionLevel);
                            if (l == null)
                                continue;
                            List<Level> llist = mtgh.getAncestors(l.Name);
                            foreach (Level nl in llist)
                            {
                                foreach (LevelKey lk in nl.Keys)
                                {
                                    if (lk.SurrogateKey)
                                        continue;
                                    bool match = true;
                                    foreach (string s in lk.ColumnNames)
                                    {
                                        bool found = false;
                                        foreach (StagingColumn sc in st.Columns)
                                        {
                                            if (sc.NaturalKey && sc.Name == s && sc.TargetTypes != null &&
                                                Array.IndexOf<string>(sc.TargetTypes, mtg.DimensionName) >= 0)
                                            {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (!found)
                                        {
                                            match = false;
                                            break;
                                        }
                                    }
                                    if (match)
                                    {
                                        // Add keys for current level
                                        foreach (string s in addKeysForLevel(mtgh, nl, mt, recalcCardinality))
                                            if (!keyColumnsAdded.ContainsKey(s))
                                                keyColumnsAdded.Add(s, mtgh);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (Level l in levelList.Keys)
                        {
                            foreach (string s in addKeysForLevel(levelList[l], l, mt, recalcCardinality))
                                if (!keyColumnsAdded.ContainsKey(s))
                                    keyColumnsAdded.Add(s, levelList[l]);
                        }
                    }
                }

                if (keyColumnsAdded.Count > 0)
                    addCarriedDownKeySecurityFilters(mt, stlist, mg, keyColumnsAdded);

                /*
                 * Add any natural keys for levels that could be used to join
                 */
                List<DimensionTable> dimTablesList = getDimTablesForMeasureGrain(mg, true);
                Dictionary<DimensionTable, List<string>> keyMap = getJoinKeyMap(mt, dimTablesList, true);
                //Add key columns needed to join to dimension tables
                foreach (DimensionTable dt in keyMap.Keys)
                {
                    if (ma.timeDefinition != null && ma.timeDefinition.GenerateTimeDimension && dt.DimensionName == ma.timeDefinition.Name)
                        continue;
                    List<string> joinKeyColList = keyMap[dt];
                    if (joinKeyColList != null)
                    {
                        foreach (string s in joinKeyColList)
                        {
                            // Ensure the key is added with the correct datatype - so look it up
                            object[] dtype = getDimensionColumnType(dt.DimensionName, s, stlist);
                            string dataType = "Varchar";
                            int width = 1000;
                            if (dtype != null)
                            {
                                if ((dtype[0] != null) && (dtype[1] != null))
                                {
                                    dataType = (string)dtype[0];
                                    width = (int)dtype[1];
                                }
                            }
                            ma.addMeasureColumnDataRowIfNecessary(getForeignKeyLogicalName(dt.DimensionName, s),
                                dataType, width, null, true);
                            ma.addMeasureColumnMapping(getForeignKeyLogicalName(dt.DimensionName, s),
                                getForeignKeyPhysicalName(dbType, dt.DimensionName, s, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion), mt, true,
                                MAX_PHYSICAL_COLUMN_NAME_LENGTH, true);
                        }
                    }
                }
            }
            mg.measureColumnName = null;
            mt.Grain = mg;
        }

        private void addCarriedDownKeySecurityFilters(MeasureTable mt, List<StagingTable> stlist, MeasureGrain mg, Dictionary<string, Hierarchy> keyColumnsAdded)
        {
            // See if we are pulling a key from a higher level grain that has a security filter
            foreach (StagingTable st in stlist)
            {
                MeasureGrain mg2 = getStagingTableGrain(ma, st);
                if (mg2 == null)
                    continue;
                if (mg2.atSameGrain(mg))
                {
                    List<string> columnsNotInStaging = new List<string>();
                    foreach (string s in keyColumnsAdded.Keys)
                    {
                        bool found = false;
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.Name == s)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            columnsNotInStaging.Add(s);
                    }
                    if (columnsNotInStaging.Count > 0)
                    {
                        /* 
                         * Find higher grained staging tables and add security filters to level keys that were carried down from a higher grain
                         */
                        foreach (StagingTable st2 in stlist)
                        {
                            if (st == st2)
                                continue;
                            List<MeasureTableGrain[]> lowerGrains =
                                isLowerGrain(ma, getStagingTableGrain(ma, st2).measureTableGrains, getStagingTableGrain(ma, st).measureTableGrains, null, false);
                            if (lowerGrains != null && lowerGrains.Count > 0)
                            {
                                foreach (string s in columnsNotInStaging)
                                {
                                    StagingColumn foundsc = null;
                                    foreach (StagingColumn sc in st2.Columns)
                                    {
                                        if (sc.Name == s)
                                        {
                                            foundsc = sc;
                                            break;
                                        }
                                    }
                                    if (foundsc != null && foundsc.SecFilter != null)
                                    {
                                        checkAndAddSecurityFilter(mt, foundsc,
                                            Util.generatePhysicalName(keyColumnsAdded[s].DimensionName, ma.builderVersion) + PHYSICAL_SUFFIX +
                                            Util.generatePhysicalName(foundsc.getPhysicalColumnName(), ma.builderVersion) + PHYSICAL_SUFFIX,
                                            keyColumnsAdded[s].DimensionName + "." + foundsc.Name);
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        private List<string> addKeysForLevel(Hierarchy h, Level l, MeasureTable mt, bool recalcCardinality)
        {
            List<string> keyColumnsToAdd = new List<string>();
            List<StagingTable> stlist = ma.stagingTableMod.getStagingTables();
            if (l != null)
            {
                //Make sure we don't set the cardinality of fact table to 0 if one of the levels has cardinality as 0.
                if ((recalcCardinality || mt.RecalculateCardinality) && (l.Cardinality > 0))
                {
                    // Estimate cardinality from grain
                    mt.Cardinality *= l.Cardinality;
                }
                // Add keys
                Dictionary<string, bool> levelKeyColumns = l.getLevelKeyColumns();
                if (levelKeyColumns != null)
                {
                    foreach (string lkCol in levelKeyColumns.Keys)
                    {
                        if (!keyColumnsToAdd.Contains(lkCol))
                            keyColumnsToAdd.Add(lkCol);
                    }
                }
                foreach (string lkCol in keyColumnsToAdd)
                {
                    object[] dtype = new object[2];
                    bool isSurrogateKey = false;
                    if (levelKeyColumns.ContainsKey(lkCol))
                    {
                        isSurrogateKey = levelKeyColumns[lkCol];
                    }
                    if (isSurrogateKey && ma.builderVersion > 12)
                    {
                        dtype[0] = "Integer";
                        dtype[1] = 0;
                    }
                    else
                    {
                        // Ensure the key is added with the correct datatype - so look it up
                        dtype = getDimensionColumnType(h.DimensionName, lkCol, stlist);
                        if (dtype[0] == null)
                        {
                            dtype[0] = "Varchar";
                            dtype[1] = 1000;
                        }
                    }
                    ma.addMeasureColumnDataRowIfNecessary(getForeignKeyLogicalName(h.DimensionName, lkCol),
                        (string)dtype[0], (int)dtype[1], null, true);
                    ma.addMeasureColumnMapping(getForeignKeyLogicalName(h.DimensionName, lkCol),
                        getForeignKeyPhysicalName(dbType, h.DimensionName, lkCol, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion), mt, true,
                        MAX_PHYSICAL_COLUMN_NAME_LENGTH, true);
                }
            }
            return keyColumnsToAdd;
        }

        private bool isMapped(string dimName, string lName, List<StagingTable> stlist)
        {
            bool mapped = false;
            foreach (StagingTable st in stlist)
            {
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == dimName && level[1] == lName)
                    {
                        mapped = true;
                        break;
                    }
                }
                if (mapped)
                    break;
            }
            return (mapped);
        }

        private object[] getDimensionColumnType(string dimensionName, string colName, List<StagingTable> stlist)
        {
            DataRow[] drs = ma.dimensionColumns.Select("DimensionName='" + dimensionName + "' AND ColumnName='" + colName + "'");
            // If not already existing - look in staging tables
            object[] dtype = new object[2];
            if (drs.Length == 0)
            {
                foreach (StagingTable st in stlist)
                {
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, dimensionName) >= 0)
                        {
                            dtype[0] = sc.DataType;
                            dtype[1] = sc.Width;
                            break;
                        }
                    }
                    if (dtype[0] != null)
                        break;
                }
            }
            else
            {
                dtype[0] = (string)drs[0]["DataType"].ToString();
                string s = (string)drs[0]["ColWidth"];
                dtype[1] = (int)((s.Length == 0) ? 0 : int.Parse(s));
            }
            return (dtype);
        }

        private Dictionary<DimensionTable, List<string>> colNameMap;

        private Dictionary<DimensionTable, List<string>> getJoinKeyMap(MeasureTable mt, List<DimensionTable> dimTablesList, bool useNaturalKeys)
        {
            if (colNameMap == null)
                colNameMap = new Dictionary<DimensionTable, List<string>>();
            Dictionary<DimensionTable, List<string>> joinKeyMap = new Dictionary<DimensionTable, List<string>>();
            //Add key columns needed to join to dimension tables
            foreach (DimensionTable dt in dimTablesList)
            {
                List<string> dimCols = null;
                if (colNameMap.ContainsKey(dt))
                    dimCols = colNameMap[dt];
                else
                {
                    dimCols = getDimColNames(dt);
                    colNameMap.Add(dt, dimCols);
                }
                string levelName = dt.Level;
                if (levelName == null || levelName.Equals(""))
                {
                    continue;
                }
                List<string> joinKeyColList = new List<string>();
                Level l = findLevel(dt.DimensionName, levelName);
                bool redundant = false;
                if (l != null)
                {
                    if (l.Degenerate && dt.PhysicalName == mt.PhysicalName)
                        redundant = true;

                    bool useOverrideKeyColumns = false;
                    if (mt.Grain != null && mt.Grain.overrideKeyColumns != null
                        && mt.Grain.overrideKeyColumns.Count > 0 && mt.Grain.overrideKeyColumns.ContainsKey(dt.DimensionName + "_" + levelName))
                    {
                        useOverrideKeyColumns = true;
                    }

                    LevelKey[] keyList = l.Keys;
                    if ((keyList != null) && (keyList.Length > (useNaturalKeys ? 1 : 0)))
                    {
                        LevelKey key = keyList[useNaturalKeys ? 1 : 0];
                        string[] keyColumns = useOverrideKeyColumns ? mt.Grain.overrideKeyColumns[dt.DimensionName + "_" + levelName].ToArray() : key.ColumnNames;
                        if ((keyColumns != null) && (keyColumns.Length > 0))
                        {
                            foreach (string keyColName in keyColumns)
                            {
                                foreach (string colName in dimCols)
                                {if (keyColName == colName)
                                    {
                                        joinKeyColList.Add(colName);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                joinKeyMap.Add(dt, redundant ? null : joinKeyColList);
            }
            return (joinKeyMap);
        }

        private void addJoinsForMeasureTable(MeasureTable mt, List<DimensionTable> dimTablesList, bool useNaturalKeys, bool typeIISCD)
        {
            Dictionary<DimensionTable, List<string>> keyMap = getJoinKeyMap(mt, dimTablesList, useNaturalKeys);
            Dictionary<string, string> joinConditionReplacementMap = null;
            //Add key columns needed to join to dimension tables
            foreach (DimensionTable dt in keyMap.Keys)
            {
                if (!equalsConnectionWithNull(dt, mt))
                    continue;
                List<string> joinKeyColList = keyMap[dt];
                List<string> joinKeyColList2 = null;
                LevelAlias levelAttributeAlias = null;
                LevelAlias levelMeasureAlias = null;
                Level l = null;
                if (useNaturalKeys)
                {
                    l = findLevel(dt.DimensionName, dt.Level);
                    if (dt.InheritTable != null && dt.InheritPrefix != null)
                    {
                        if (l != null && l.Aliases != null && l.Aliases.Length > 0)
                        {
                            foreach (LevelAlias la in l.Aliases)
                            {
                                if (la.AliasType == LevelAlias.ATTRIBUTE_ONLY && dt.InheritPrefix.Equals(la.AliasName + ": "))
                                {
                                    levelAttributeAlias = la;
                                    break;
                                }                                
                            }
                        }
                    }
                    else if (mt.InheritTable != null && mt.InheritPrefix != null)
                    {
                        if (l != null && l.Aliases != null && l.Aliases.Length > 0)
                        {
                            foreach (LevelAlias la in l.Aliases)
                            {
                                if (la.AliasType == LevelAlias.MEASURE_ONLY && mt.InheritPrefix.Equals(la.AliasName + ": "))
                                {
                                    levelMeasureAlias = la;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (levelAttributeAlias != null && l.Keys != null && l.Keys.Length > 1)
                {
                    LevelKey[] keyList = l.Keys;
                    LevelKey key = keyList[1];
                    List<string> dimCols = getDimColNames(dt);
                    List<string> aliasKeyColumns = new List<string>();
                    foreach (string keyCol in key.ColumnNames)
                    {
                        bool found = false;
                        foreach (LevelAlias.AliasLevelKey alk in levelAttributeAlias.AliasKeys)
                        {
                            if (alk.LevelKeyColumn.Equals(keyCol))
                            {
                                found = true;
                                aliasKeyColumns.Add(alk.AliasKeyColumn);
                                break;
                            }
                        }
                        if (!found)
                            aliasKeyColumns.Add(keyCol);
                    }
                    string[] keyColumns = aliasKeyColumns.ToArray();
                    joinKeyColList2 = new List<string>();
                    if ((keyColumns != null) && (keyColumns.Length > 0))
                    {
                        foreach (string keyColName in keyColumns)
                        {
                            foreach (string colName in dimCols)
                            {
                                if (keyColName == colName)
                                {
                                    joinKeyColList2.Add(colName);
                                    break;
                                }
                            }
                        }
                    }
                    if (joinKeyColList2.Count == 0)
                    {
                        //disable alias
                        if (ma.isHeadless())
                        {
                            ma.getLogger().Debug("All AliasKeyColumns not found for LevelAlias '" + levelAttributeAlias.AliasName + "' in dimensiontable '" + dt.TableName + "', disabling level alias.");
                        }
                        joinKeyColList2 = null;
                        keyMap = getJoinKeyMap(mt, dimTablesList, false);
                        joinKeyColList = keyMap[dt];
                    }
                }
                else if (levelMeasureAlias != null && l.Keys != null && l.Keys.Length > 1)
                {
                    LevelKey[] keyList = l.Keys;
                    LevelKey key = keyList[1];
                    List<string> aliasKeyColumns = new List<string>();
                    foreach (string keyCol in key.ColumnNames)
                    {
                        bool found = false;
                        foreach (LevelAlias.AliasLevelKey alk in levelMeasureAlias.AliasKeys)
                        {
                            if (alk.LevelKeyColumn.Equals(keyCol))
                            {
                                found = true;
                                aliasKeyColumns.Add(alk.AliasKeyColumn);
                                break;
                            }
                        }
                        if (!found)
                            aliasKeyColumns.Add(keyCol);
                    }
                    string[] keyColumns = aliasKeyColumns.ToArray();
                    joinKeyColList2 = keyMap[dt];
                    joinKeyColList = new List<string>();
                    if ((keyColumns != null) && (keyColumns.Length > 0))
                    {
                        foreach (string keyColName in keyColumns)
                        {
                            DataRow[] rows = ma.measureColumnMappings.Select("TableName='" + mt.InheritTable + "'");
                            foreach (DataRow row in rows)
                            {
                                string colName = (string)row["ColumnName"];
                                if (colName.Contains("."))
                                    colName = colName.Substring(colName.IndexOf(".") + 1);
                                if (colName.Contains(": "))
                                    colName = colName.Substring(colName.IndexOf(": ") + 2);
                                if (colName.Equals(keyColName))
                                {
                                    joinKeyColList.Add(colName);
                                    if (!joinKeyColList[joinKeyColList.Count - 1].Equals(joinKeyColList2[joinKeyColList.Count - 1]))
                                    {
                                        if (joinConditionReplacementMap == null)
                                            joinConditionReplacementMap = new Dictionary<string, string>();
                                        joinConditionReplacementMap.Add(ApplicationBuilder.getForeignKeyPhysicalName(getSQLGenType(ma), dt.DimensionName, colName, ApplicationBuilder.PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion), (string)row["PhysicalName"]);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (joinKeyColList.Count == 0)
                    {
                        //disable alias
                        if (ma.isHeadless())
                        {
                            ma.getLogger().Debug("All AliasKeyColumns not found for LevelAlias '" + levelMeasureAlias.AliasName + "' in measuretable '" + mt.TableName + "', disabling level alias.");
                        }
                        keyMap = getJoinKeyMap(mt, dimTablesList, false);
                        joinKeyColList = keyMap[dt];
                        joinKeyColList2 = null;                        
                    }
                }
                addJoins(joinKeyColList, joinKeyColList2, dt, mt, joinKeyColList == null, typeIISCD, null, joinConditionReplacementMap);
            }
        }

        private bool equalsConnectionWithNull(DimensionTable dt, MeasureTable mt)
        {
            if (dt.TableSource != null && dt.TableSource.Connection != null
                && mt.TableSource != null && mt.TableSource.Connection != null)
            {
                if (dt.TableSource.Connection.Equals(mt.TableSource.Connection))
                    return true;
                else
                    return false;
            }
            else//true if any one is null
                return true;
        }

        private bool equalsConnectionWithNull(MeasureTable mt, MeasureTable mt2)
        {
            if (mt.TableSource != null && mt.TableSource.Connection != null
                    && mt2.TableSource != null && mt2.TableSource.Connection != null)
            {
                if (mt.TableSource.Connection.Equals(mt2.TableSource.Connection))
                    return true;
                else
                    return false;
            }
            else//true if any one is null
                return true;
        }

        private DataRow addJoins(List<string> joinKeyColList, List<string> joinKeyColList2, DimensionTable dt, MeasureTable mt, bool redundant, bool typeIISCD, string dimension, Dictionary<string, string> joinConditionReplacementMap)
        {
            if (((joinKeyColList != null) && (joinKeyColList.Count > 0)) || redundant)
            {
                string dimName = dimension == null ? dt.DimensionName : dimension;
                if (manualOverrideJoinExists(dt, mt))
                {
                    return null;
                }
                DataRowView[] joinRows = ma.joinsView.FindRows(new object[] { mt.TableName, dt.TableName });
                DataRow joinRow = null;
                if (joinRows.Length == 0)
                {
                    joinRow = ma.Joins.NewRow();
                    ma.Joins.Rows.Add(joinRow);
                }
                else if ((bool)joinRows[0].Row["AutoGenerated"])
                    joinRow = joinRows[0].Row;
                if (joinRow != null)
                {
                    if (!redundant)
                        foreach (string joinKeyCol in joinKeyColList)
                        {
                            DataRowView[] drv = ma.dimensionColumnNamesView.FindRows(new object[] { dimName, joinKeyCol });
                            string s = (string)(drv.Length > 0 ? (string)drv[0].Row["ColWidth"] : null);
                            if (drv.Length > 0)
                            {
                                ma.addMeasureColumnDataRowIfNecessary(getForeignKeyLogicalName(dimName, joinKeyCol),
                                    drv[0].Row["DataType"].ToString(), ((s.Length == 0) ? 0 : int.Parse(s)), null, true);
                                ma.addMeasureColumnMapping(getForeignKeyLogicalName(dimName, joinKeyCol),
                                    getForeignKeyPhysicalName(dbType, dimName, joinKeyCol, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion), mt, true,
                                    MAX_PHYSICAL_COLUMN_NAME_LENGTH, true);
                            }
                            else
                            {
                                /*
                                 * If it didn't find the dimension column, see if it's an analyze-by-time colume (which
                                 * shouldn't be in the time dimension). If so, it's an integer. Otherwise, assume a varchar.
                                 */
                                string datatype = "Varchar";
                                int width = 1000;
                                if (ma.timeDefinition != null && ma.timeDefinition.Name == dimName)
                                {
                                    datatype = "Integer";
                                    width = 0;
                                }
                                ma.addMeasureColumnDataRowIfNecessary(getForeignKeyLogicalName(dimName, joinKeyCol),
                                    datatype, width, null, true);
                                ma.addMeasureColumnMapping(getForeignKeyLogicalName(dimName, joinKeyCol),
                                    getForeignKeyPhysicalName(dbType, dimName, joinKeyCol, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion), mt, true,
                                    MAX_PHYSICAL_COLUMN_NAME_LENGTH, true);
                            }
                        }
                    //Now add the necessary joins
                    joinRow[0] = mt.TableName; // table1
                    joinRow[1] = dt.TableName; // table2

                    if (redundant)
                        joinRow[3] = true; // redundant
                    else
                    {
                        string joinCondition = ma.getJoinCondition(dt, joinKeyColList, joinKeyColList2, mt, dimension, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        if (typeIISCD)
                            joinCondition += " AND \"" + dt.TableName + "\".SCD_END_DATE IS NULL";
                        if (joinConditionReplacementMap != null)
                        {
                            foreach (string key in joinConditionReplacementMap.Keys)
                            {
                                joinCondition = joinCondition.Replace(key, joinConditionReplacementMap[key]);
                            }
                        }
                        joinRow[2] = joinCondition; // joincondition
                    }
                    joinRow[6] = true; // autogenerated
                    return (joinRow);
                }
            }
            return (null);
        }

        /*
         * Invalidate any joins between a measure table and a dimension that 
         * have been inherited but not explicitly overridden (assumes dimension
         * table is the second table in the join)
         */
        private void invalidateInheritedDimensionJoins(string dimension, string dimensionTableConnection, MeasureTable mt, MeasureTable imt)
        {
            DataRowView[] inheritJoinRows = ma.joinsFirstView.FindRows(imt.TableName);
            DataRowView[] curJoinRows = ma.joinsFirstView.FindRows(mt.TableName);
            foreach (DataRowView drv in inheritJoinRows)
            {
                bool found = false;
                // If overridden, ignore
                foreach (DataRowView drv2 in curJoinRows)
                {
                    if (drv2["table2"] == drv.Row["table2"])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    DimensionTable dt = null;
                    foreach (DimensionTable cdt in ma.dimensionTablesList)
                    {
                        if (cdt.DimensionName == dimension && cdt.TableName == (string)drv.Row["table2"])
                        {
                            if (dimensionTableConnection != null && cdt.TableSource != null && !dimensionTableConnection.Equals(cdt.TableSource.Connection))
                                continue;
                            dt = cdt;
                            break;
                        }
                    }
                    if (dt != null)
                    {
                        DataRow joinRow = ma.Joins.NewRow();
                        ma.Joins.Rows.Add(joinRow);
                        joinRow["AutoGenerated"] = true;
                        joinRow["table1"] = mt.TableName;
                        joinRow["table2"] = dt.TableName;
                        joinRow["Invalid"] = true;
                    }
                }
            }
        }

        private Level findLevel(string dimensionName, string levelName)
        {
            List<Hierarchy> hlist = ma.hmodule.getHierarchies();
            foreach (Hierarchy h in hlist)
            {
                if (h.DimensionName.Equals(dimensionName))
                {
                    Level l = h.findLevel(levelName);
                    if (l != null)
                    {
                        return l;
                    }
                }
            }
            return null;
        }

        public static string DayIDName = "Day ID";
        public static string WeekIDName = "Week ID";
        public static string MonthIDName = "Month ID";
        public static string QuarterIDName = "Quarter ID";
        public static string HalfYearIDName = "Half Year ID";
        public static string YearIDName = "Year ID";

        public static string DayLevelName = "Day";
        public static string WeekLevelName = "Week";
        public static string MonthLevelName = "Month";
        public static string QuarterLevelName = "Quarter";
        public static string HalfYearLevelName = "Half";
        public static string YearLevelName = "Year";

        public static string Year = "Year";
        public static string YearQuarter = "Year/Quarter";
        public static string YearMonth = "Year/Month";
        public static string DateName = "Date";

        public string generateTime(TimeDefinition td, List<BuildAction> balist)
        {
            string dimensionName = td.Name;
            if (!ma.getDimensionList().Contains(dimensionName))
            {
                balist.Add(new BuildAction("Added dimension:" + dimensionName, MessageType.BASIC));
                ma.addDimension(dimensionName);
            }
            Hierarchy h = new Hierarchy();
            // Remove previously generated hierarchy and copy over filter information so the filters are not lost on rebuild
            foreach (Hierarchy ph in ma.hmodule.getHierarchies())
            {
                if (ph.Name == dimensionName)
                {
                    h.NoMeasureFilter = ph.NoMeasureFilter;
                    h.ExclusionFilter = ph.ExclusionFilter;
                    ma.hmodule.removeHierarchyWithoutRefresh(ph);
                    break;
                }
            }
            h.Name = dimensionName;
            h.AutoGenerated = true;
            h.GeneratedTime = true;
            h.Children = new Level[0];
            h.DimensionName = dimensionName;
            TreeNode hnode = ma.hmodule.newHierarchy(h);
            Level l = null;
            string dkeylevel = null;
            TreeNode pnode = hnode;
            string keyname = null;
            /*
             * Create all time level objects irrespective of current selection in the time definitions. The time levels are used for
             * comparison in build application code and if we don't create all time level objects, there will be null reference 
             * exceptions while building application.
             */

            //Year Level
            yearLevel = new Level();
            yearLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days / 365;
            yearLevel.Name = YearLevelName;
            keyname = YearIDName;
            yearLevel.ColumnNames = new string[] { Year, "Year Seq Number", keyname };
            yearLevel.HiddenColumns = new bool[] { false, true, true };
            yearLevel.Children = new Level[0];
            yearLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey(Year), new LevelKey("Year Seq Number") };
            yearLevel.Keys[0].SurrogateKey = true;

            //Half year level
            halfyearLevel = new Level();
            halfyearLevel.GenerateDimensionTable = true;
            halfyearLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days * 2 / 365;
            halfyearLevel.Name = HalfYearLevelName;
            keyname = HalfYearIDName;
            halfyearLevel.ColumnNames = new string[] { "Half Year Number", "Half Year Seq Number", "Half", "Year/Half", keyname };
            halfyearLevel.HiddenColumns = new bool[] { true, true, true, false, true };
            halfyearLevel.Children = new Level[0];
            halfyearLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey("Half Year Number"), new LevelKey("Half Year Seq Number") };
            halfyearLevel.Keys[0].SurrogateKey = true;

            //Quarter level
            quarterLevel = new Level();
            quarterLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days * 4 / 365;
            quarterLevel.Name = QuarterLevelName;
            quarterLevel.GenerateDimensionTable = true;
            keyname = QuarterIDName;
            if (ma.builderVersion >= Repository.FloatedTimeLevels)
            {
                quarterLevel.ColumnNames = new string[] { "Quarter Number", "Quarter Seq Number", "Year/Quarter", keyname };
                quarterLevel.HiddenColumns = new bool[] { true, true, false, true };
                quarterLevel.FloatingColumnsToMap = new string[] { "Quarter" };
            }
            else
            {
                quarterLevel.ColumnNames = new string[] { "Quarter Number", "Quarter Seq Number", "Quarter", "Year/Quarter", keyname };
                quarterLevel.HiddenColumns = new bool[] { true, true, true, false, true };
            }
            quarterLevel.Children = new Level[0];
            quarterLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey("Quarter Number"), new LevelKey("Quarter Seq Number") };
            quarterLevel.Keys[0].SurrogateKey = true;

            //Month level
            monthLevel = new Level();
            monthLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days / 30;
            monthLevel.GenerateDimensionTable = true;
            monthLevel.Name = MonthLevelName;
            keyname = MonthIDName;
            if (ma.builderVersion >= Repository.FloatedTimeLevels)
            {
                monthLevel.ColumnNames = (td.Day || td.Week) ? new string[] { "Month Number", "Month Seq Number", "Year/Month", keyname } :
                    new string[] { "Month Number", "Month Seq Number", "Year/Month", "Date", keyname };
                monthLevel.HiddenColumns = (td.Day || td.Week) ? new bool[] { true, true, false, true } :
                    new bool[] { false, false, false, true, true };
                monthLevel.FloatingColumnsToMap = new string[] { "Quarter", "Month", "Month of Year", "Trailing 3 Month Name", "Num Weeks In Month" };
            }
            else
            {
                monthLevel.ColumnNames = (td.Day || td.Week) ? new string[] { "Month Number", "Month Seq Number", "Month", "Year/Month", "Month of Year", "Trailing 3 Month Name", "Num Weeks In Month", keyname } :
                    new string[] { "Month Number", "Month Seq Number", "Month", "Year/Month", "Month of Year", "Trailing 3 Month Name", "Num Weeks In Month", "Date", keyname };
                monthLevel.HiddenColumns = (td.Day || td.Week) ? new bool[] { true, true, true, false, true, true, true, true } :
                    new bool[] { false, false, false, false, false, false, false, true, true };
            }
            monthLevel.Children = new Level[0];
            monthLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey("Month Number"), new LevelKey("Month Seq Number") };
            monthLevel.Keys[0].SurrogateKey = true;

            //Week level
            weekLevel = new Level();
            weekLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days / 7;
            weekLevel.GenerateDimensionTable = true;
            weekLevel.Name = WeekLevelName;
            keyname = WeekIDName;
            if (ma.builderVersion >= Repository.FloatedTimeLevels)
            {
                weekLevel.ColumnNames = td.Day ? new string[] { "Week Number", "Week Seq Number", "Week Start Date", "Week End Date", keyname } :
                    new string[] { "Week Number", "Week Seq Number", "Week Start Date", "Week End Date", "Date", keyname };
                weekLevel.HiddenColumns = td.Day ? new bool[] { true, true, false, true, true } :
                    new bool[] { true, true, false, true, true, true };
                weekLevel.FloatingColumnsToMap = new string[] { "Quarter", "Month", "Month of Year", "Trailing 3 Month Name", "Num Weeks In Month", "Week of Month", "Week of Year" };
            }
            else
            {
                weekLevel.ColumnNames = td.Day ? new string[] { "Week Number", "Week Seq Number", "Week of Month", "Week of Year", "Week Start Date", "Week End Date", keyname } :
                    new string[] { "Week Number", "Week Seq Number", "Week of Month", "Week of Year", "Week Start Date", "Week End Date", "Date", keyname };
                weekLevel.HiddenColumns = td.Day ? new bool[] { true, true, true, true, false, true, true } :
                    new bool[] { true, true, true, true, false, true, true, true };
            }
            weekLevel.Children = new Level[0];
            weekLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey("Week Number"), new LevelKey("Week Seq Number") };
            weekLevel.Keys[0].SurrogateKey = true;

            //Day level
            dayLevel = new Level();
            dayLevel.GenerateDimensionTable = true;
            dayLevel.Name = DayLevelName;
            dayLevel.Cardinality = ((TimeSpan)td.EndDate.Subtract(td.StartDate)).Days;
            keyname = DayIDName;
            if (ma.builderVersion >= Repository.FloatedTimeLevels)
            {
                dayLevel.ColumnNames = new string[] { "Day Number", "Day Seq Number", "Date", keyname };
                dayLevel.HiddenColumns = new bool[] { true, true, false, true };
            }
            else
            {
                dayLevel.ColumnNames = new string[] { "Day Number", "Day Seq Number", "Day", "Day of Week", "Day of Week in Month", "Day of Month", 
                    "Day of Year", "Date", keyname };
                dayLevel.HiddenColumns = new bool[] { true, true, true, true, true, true, true, false, true };
            }
            dayLevel.Children = new Level[0];
            dayLevel.Keys = new LevelKey[] { new LevelKey(keyname), new LevelKey("Day Number"), new LevelKey("Day Seq Number") };
            dayLevel.Keys[0].SurrogateKey = true;

            if (td.Year)
            {
                l = yearLevel;
                dkeylevel = l.Name;
                keyname = YearIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, Year, "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Year Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                balist.Add(new BuildAction("Added Year Level", MessageType.BASIC));
            }
            if (td.Halfyear)
            {
                l = halfyearLevel;
                dkeylevel = l.Name;
                keyname = HalfYearIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Half Year Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Half Year Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Half", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Year/Half", "Varchar", 7, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                balist.Add(new BuildAction("Added Half Year Level", MessageType.BASIC));
            }
            if (td.Quarter)
            {
                l = quarterLevel;
                dkeylevel = l.Name;
                keyname = QuarterIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Quarter Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Quarter Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Quarter", "Varchar", 2, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, YearQuarter, "Varchar", 7, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                if (!td.Day && !td.Week && !td.Month)
                    ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Date", "Integer", 0, null);
                balist.Add(new BuildAction("Added Quarter Level", MessageType.BASIC));
            }
            if (td.Month)
            {
                l = monthLevel;
                dkeylevel = l.Name;
                keyname = MonthIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Month Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Month Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Month", "Varchar", 10, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, YearMonth, "Varchar", 20, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Month of Year", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Trailing 3 Month Name", "Varchar", 20, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Num Weeks In Month", "Integer", 0, null);
                if (!td.Day && !td.Week)
                    ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Date", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                balist.Add(new BuildAction("Added Month Level", MessageType.BASIC));
            }
            if (td.Week)
            {
                l = weekLevel;
                dkeylevel = l.Name;
                keyname = WeekIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week of Month", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week of Year", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week Start Date", "Date", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Week End Date", "Date", 0, null);
                if (!td.Day)
                    ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Date", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                balist.Add(new BuildAction("Added Week Level", MessageType.BASIC));
            }
            if (td.Day)
            {
                l = dayLevel;
                dkeylevel = l.Name;
                keyname = DayIDName;
                pnode = ma.hmodule.newLevel(pnode, l, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day Seq Number", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day", "Varchar", 15, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day of Week", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day of Week in Month", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day of Month", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, "Day of Year", "Integer", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, DateName, "Date", 0, null);
                ma.addDimensionColumnDataRowIfNecessary(dimensionName, keyname, "Integer", 0, null)["Key"] = true;
                balist.Add(new BuildAction("Added Day Level", MessageType.BASIC));
            }
            h.DimensionKeyLevel = dkeylevel;
            return (dimensionName);
        }

        public static string getCustomTimeDimension(StagingTable st)
        {
            StagingColumn sc = null;
            foreach (StagingColumn ssc in st.Columns)
            {
                if (ssc.Name == st.CustomTimeColumn)
                {
                    sc = ssc;
                    break;
                }
            }
            if (sc == null || sc.TargetTypes == null)
                return null;
            // Assume that columns are only targeted to one dimension and take that one
            string targetDim = null;
            foreach (string s in sc.TargetTypes)
                if (s != "Measure")
                {
                    targetDim = s;
                    break;
                }
            return targetDim;
        }

        public static string getCustomTimeLevel(MainAdminForm ma, StagingTable st, string customTimeDimension)
        {
            if (st.DiscoveryTable)
            {
                Hierarchy h = ma.hmodule.getDimensionHierarchy(customTimeDimension);
                Level l = h.findLevelContainingDimensionKey();
                return l.Name;
            }
            // Get the target level for that dimension
            string targetLevel = null;
            foreach (string[] level in st.Levels)
            {
                if (level[0] == customTimeDimension)
                {
                    targetLevel = level[1];
                    break;
                }
            }
            return targetLevel;
        }

        private void addCustomTime(string dimName, TimeDefinition td, List<BuildAction> balist, string schema, MainAdminForm ma)
        {
            // See if there are any custom time columns
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.CustomTimeColumn == null)
                    continue;
                string targetDim = getCustomTimeDimension(st);
                if (targetDim == null)
                    continue;
                string targetLevel = getCustomTimeLevel(ma, st, targetDim);
                if (targetLevel == null)
                    continue;
                DimensionTable dt = null;
                foreach (DimensionTable sdt in ma.dimensionTablesList)
                {
                    if (sdt.DimensionName != targetDim || sdt.Level != targetLevel)
                        continue;
                    dt = sdt;
                    break;
                }
                if (dt == null)
                    continue;

                Hierarchy dth = ma.hmodule.getDimensionHierarchy(dt.DimensionName);
                //Add dimension column expressions for the dimension table level (if any exists) before we copy columns over to create inherited table
                if (ma.logicalExpressionList != null && dth != null && dt.Level != null)
                {
                    foreach (LogicalExpression le in ma.logicalExpressionList.logicalExpressions)
                    {
                        if (le.Type == LogicalExpression.TYPE_DIMENSION && le.Levels[0][0] == dth.Name && le.Levels[0][1] == dt.Level)
                        {
                            addDimensionColumnExpression(le, balist);
                        }
                    }
                }
                DimensionTable newdt = new DimensionTable();
                newdt.TableName = st.CustomTimeColumn + dt.TableName;
                if (st.CustomTimePrefix != null && st.CustomTimePrefix.Length > 0)
                {
                    newdt.VirtualMeasuresPrefix = st.CustomTimePrefix;
                    newdt.VirtualMeasuresInheritTable = td.Name + " " + DayLevelName;
                }
                else
                {
                    newdt.InheritTable = td.Name + " " + DayLevelName;
                }
                newdt.DimensionName = dt.DimensionName;
                newdt.Calculated = true;
                newdt.Cacheable = true;
                newdt.AutoGenerated = true;
                // Copy columns                
                DataRowView[] drv = ma.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                foreach (DataRowView dr in drv)
                {
                    DataRow newdr = ma.dimensionColumnMappings.NewRow();
                    newdr.ItemArray = dr.Row.ItemArray;
                    newdr["TableName"] = newdt.TableName;
                    ma.dimensionColumnMappings.Rows.Add(newdr);
                }
                newdt.TableSource = dt.TableSource.clone();
                newdt.PhysicalName = dt.PhysicalName;
                newdt.ColumnSubstitutions = new ColumnSubstitution[1];
                newdt.ColumnSubstitutions[0] = new ColumnSubstitution();
                newdt.ColumnSubstitutions[0].Original = newdt.TableSource.Tables[0].PhysicalName + "." + Util.generatePhysicalName(DayIDName, ma.builderVersion) + PHYSICAL_SUFFIX;
                newdt.ColumnSubstitutions[0].New = newdt.TableSource.Tables[0].PhysicalName + "." + Util.generatePhysicalName(st.CustomTimeColumn + "_" + dayLevel.Name, ma.builderVersion) + "_ID" + PHYSICAL_SUFFIX;
                newdt.DontInheritColumns = true;

                Hierarchy h = ma.hmodule.getDimensionHierarchy(newdt.DimensionName);
                Level newl = h.findLevel(dt.Level);
                ma.addDimensionTable(newdt, newl, false, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                balist.Add(new BuildAction("Added Time Dimension Table: " + newdt.TableName, MessageType.BASIC));

                DimensionTable basedt = newdt;
                /* Add any time shifted versions */
                if (st.CustomTimeShifts != null)
                {
                    foreach (CustomTimeShift cts in st.CustomTimeShifts)
                    {
                        newdt = new DimensionTable();
                        newdt.TableName = st.CustomTimeColumn + " " + cts.BaseKey + " " + cts.RelativeKey + " " + dt.TableName;
                        newdt.DimensionName = dt.DimensionName;
                        newdt.Calculated = true;
                        newdt.Cacheable = true;
                        newdt.AutoGenerated = true;
                        // Copy columns
                        drv = ma.dimensionColumnMappingsTablesView.FindRows(new object[] { basedt.TableName });
                        Dictionary<string, string> colmap = new Dictionary<string, string>();
                        DataRow basedr = null;
                        DataRow relativedr = null;
                        foreach (DataRowView dr in drv)
                        {
                            // Only add columns at this level of aggregation
                            if (cts.Columns != null && cts.Columns.Length > 0 && Array.IndexOf<string>(cts.Columns, (string)dr.Row["ColumnName"]) >= 0)
                            {
                                DataRow newdr = ma.dimensionColumnMappings.NewRow();
                                newdr.ItemArray = dr.Row.ItemArray;
                                newdr["TableName"] = newdt.TableName;
                                if (!colmap.ContainsKey((string)newdr["ColumnName"]))
                                {
                                    colmap.Add((string)newdr["ColumnName"], (string)newdr["PhysicalName"]);
                                }
                                ma.dimensionColumnMappings.Rows.Add(newdr);
                            }
                            if (cts.BaseKey == (string)dr.Row["ColumnName"])
                            {
                                basedr = dr.Row;
                            }
                            if (cts.RelativeKey == (string)dr.Row["ColumnName"])
                            {
                                relativedr = dr.Row;
                            }
                        }
                        if (basedr == null || relativedr == null)
                            continue;
                        newdt.TableSource = dt.TableSource.clone();
                        newdt.PhysicalName = dt.PhysicalName;
                        newdt.Type = DimensionTable.OPAQUE_VIEW;
                        // Create opaque view
                        StringBuilder sb = new StringBuilder();
                        string key = Util.generatePhysicalName(st.CustomTimeColumn + "_" + dayLevel.Name, ma.builderVersion) + "_ID" + PHYSICAL_SUFFIX;
                        sb.Append("SELECT A." + key);
                        foreach (KeyValuePair<string, string> item in colmap)
                        {
                            DimensionColumn dc = Util.getDimensionColumn(dt, item.Key);
                            if (dc != null && !dc.Qualify)
                            {
                                string phyName = Util.generatePhysicalName(item.Key, ma.builderVersion);
                                String val = item.Value.Replace(dt.PhysicalName + ".", "B.");
                                if (dbType == SqlGenType.MemDB)
                                    sb.Append(",MIN(" + val + ") '" + phyName + "'");
                                else
                                    sb.Append(",MIN(" + val + ") " + phyName);
                                continue;
                            }
                            if (dbType == SqlGenType.MemDB)
                                sb.Append(",MIN(B." + item.Value + ") '" + item.Value + "'");
                            else
                                sb.Append(",MIN(B." + item.Value + ") " + item.Value);
                        } 
                 
                        sb.Append(" FROM V{SCHEMA}." + newdt.TableSource.Tables[0].PhysicalName + " A INNER JOIN ");
                        sb.Append("V{SCHEMA}." + newdt.TableSource.Tables[0].PhysicalName + " B ON A." + basedr["PhysicalName"] + "=B." + relativedr["PhysicalName"]);
                        sb.Append(" GROUP BY A." + key);
                        newdt.OpaqueView = sb.ToString();
                        /*
                         * Since this is a level of aggregation above 
                         */
                        newdt.ColumnSubstitutions = new ColumnSubstitution[1];
                        newdt.ColumnSubstitutions[0] = new ColumnSubstitution();
                        newdt.ColumnSubstitutions[0].Original = newdt.TableSource.Tables[0].PhysicalName + "." + Util.generatePhysicalName(DayIDName, ma.builderVersion) + PHYSICAL_SUFFIX;
                        newdt.ColumnSubstitutions[0].New = newdt.TableSource.Tables[0].PhysicalName + "." + Util.generatePhysicalName(st.CustomTimeColumn + "_" + dayLevel.Name, ma.builderVersion) + "_ID" + PHYSICAL_SUFFIX;
                        newdt.DontInheritColumns = true;

                        // Use virtual inheritence if there's a prefix, normal if not
                        if (cts.Prefix != null && cts.Prefix.Length > 0)
                        {
                            newdt.VirtualMeasuresPrefix = cts.Prefix;
                            newdt.VirtualMeasuresInheritTable = td.Name + " " + DayLevelName;
                        }
                        else
                        {
                            newdt.InheritTable = td.Name + " " + DayLevelName;
                        }

                        ma.addDimensionTable(newdt, newl, false, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                        balist.Add(new BuildAction("Added Time Dimension Table: " + newdt.TableName, MessageType.BASIC));
                    }
                }
            }
        }

        string[] dwm = null;
        private void generateTimeDimensionTables(string dimName, TimeDefinition td, List<BuildAction> balist, string schema, MainAdminForm ma)
        {
            dwm = new string[5] { td.Day ? "DY" : null, td.Week ? "WK" : null, td.Month ? "MN" : null, td.Quarter ? "QT" : null, td.Halfyear ? "HY" : null };
            if (td.Periods != null)
            {
                foreach (TimePeriod tp in td.Periods)
                {
                    bool[] grains = grains = new bool[] { td.Day, td.Week, td.Month, td.Quarter, td.Halfyear };
                    generateTimeDimensionTable(grains, td.Name, tp, td, balist, schema, null);
                    if (ma.localETLLoadGroupToLocalETLConfig != null && ma.localETLLoadGroupToLocalETLConfig.Count > 0)
                    {
                        foreach (LocalETLConfig leConfig in ma.localETLLoadGroupToLocalETLConfig.Values)
                        {
                            generateTimeDimensionTable(new bool[] { td.Day, td.Week, td.Month, td.Quarter, td.Halfyear }, td.Name, tp, td, balist, schema, leConfig.getLocalETLConnection());
                        }
                    }
                }
            }
            // Add logical time columns before adding custom time so that we can support Logical Time Columns in Expressions
            addLogicalTime(ma, balist);
            addCustomTime(dimName, td, balist, schema, ma);
        }

        private void generateTimeDimensionTable(bool[] grains, string dimName, TimePeriod tp, TimeDefinition td, List<BuildAction> balist, string schema, string localETLConnection)
        {
            for (int i = 0; i < grains.Length; i++)
            {
                if (dwm[i] != null)
                {
                    Level l = monthLevel;
                    if (i == 0)
                        l = dayLevel;
                    else if (i == 1)
                        l = weekLevel;
                    else if (i == 2)
                        l = monthLevel;
                    else if (i == 3)
                        l = quarterLevel;
                    else if (i == 4)
                        l = halfyearLevel;
                    DimensionTable dt = new DimensionTable();
                    dt.TableName = tp.Prefix + " " + l.Name + (localETLConnection == null ? "" : " " + localETLConnection);
                    dt.VirtualMeasuresPrefix = tp.Prefix + " ";
                    dt.VirtualMeasuresInheritTable = dimName + " " + l.Name + (localETLConnection == null ? "" : " " + localETLConnection);
                    dt.DimensionName = dimName;
                    dt.Calculated = true;
                    dt.Cacheable = true;
                    dt.AutoGenerated = true;
                    if (l.Keys == null || l.Keys.Length == 0)
                    {
                        dt.DimensionColumns = new DimensionColumn[0];
                    }
                    else
                    {
                        Level[] levels = null;
                        List<Level> levelList = new List<Level>();
                        if (l == dayLevel)
                        {
                            if (td.Day)
                                levelList.Add(dayLevel);
                            if (td.Week)
                                levelList.Add(weekLevel);
                            if (td.Month)
                                levelList.Add(monthLevel);
                            if (td.Quarter)
                                levelList.Add(quarterLevel);
                            if (td.Halfyear)
                                levelList.Add(halfyearLevel);
                        }
                        else if (l == weekLevel)
                        {
                            if (td.Week)
                                levelList.Add(weekLevel);
                            if (td.Month)
                                levelList.Add(monthLevel);
                            if (td.Quarter)
                                levelList.Add(quarterLevel);
                            if (td.Halfyear)
                                levelList.Add(halfyearLevel);
                        }
                        else if (l == monthLevel)
                        {
                            if (td.Month)
                                levelList.Add(monthLevel);
                            if (td.Quarter)
                                levelList.Add(quarterLevel);
                            if (td.Halfyear)
                                levelList.Add(halfyearLevel);
                        }
                        else if (l == quarterLevel)
                        {
                            if (td.Quarter)
                                levelList.Add(quarterLevel);
                            if (td.Halfyear)
                                levelList.Add(halfyearLevel);
                        }
                        else if (l == halfyearLevel)
                        {
                            if (td.Halfyear)
                                levelList.Add(halfyearLevel);
                        }
                        levels = levelList.ToArray();
                        setupColumnSubstitutions(dt, tp, levels);
                    }
                    dt.Level = l.Name;
                    if (dt.PhysicalName == null)
                        dt.PhysicalName = Util.generatePhysicalName("DW_DM_" + tp.Prefix + "_" + dwm[i], ma.builderVersion);
                    dt.TableSource = new TableSource();
                    dt.TableSource.Connection = localETLConnection == null ? ma.connection.connectionList[0].Name : localETLConnection;
                    dt.TableSource.Tables = new TableSource.TableDefinition[1];
                    dt.TableSource.Tables[0] = new TableSource.TableDefinition();
                    dt.TableSource.Tables[0].PhysicalName = dt.PhysicalName;
                    if (schema != null)
                        dt.TableSource.Schema = schema;
                    ma.addDimensionTable(dt, l, false, MAX_PHYSICAL_COLUMN_NAME_LENGTH);
                    balist.Add(new BuildAction("Added Time Dimension Table: " + dt.TableName, MessageType.BASIC));
                }
            }
        }

        private string timePeriodKey(TimePeriod tp, Level l)
        {
            return (tp.Prefix + " " + l.Keys[0].ColumnNames[0]);
        }

        private void setupColumnSubstitutions(DimensionTable dt, TimePeriod tp, Level[] levels)
        {
            dt.ColumnSubstitutions = new ColumnSubstitution[levels.Length];
            for (int i = 0; i < levels.Length; i++)
            {
                Level l = levels[i];
                if (l.Keys[0] == null)
                    continue;
                string colName = timePeriodKey(tp, l);
                ma.addDimensionColumnDataRowIfNecessary(dt.DimensionName, colName, "Integer", 0, null)["Key"] = true;
                DataRow[] existingRows = ma.dimensionColumnMappings.Select("ColumnName='" + colName + "' AND TableName='" + dt.TableName + "'");
                if (existingRows == null || existingRows.Length == 0)
                {
                    DataRow dcmr = ma.dimensionColumnMappings.NewRow();
                    dcmr["TableName"] = dt.TableName;
                    dcmr["ColumnName"] = colName;
                    dcmr["Qualify"] = false;
                    dcmr["PhysicalName"] = Util.generatePhysicalName(colName, ma.builderVersion) + PHYSICAL_SUFFIX;
                    dcmr["AutoGenerated"] = true;
                    ma.dimensionColumnMappings.Rows.Add(dcmr);
                }
                dt.ColumnSubstitutions[i] = new ColumnSubstitution();
                dt.ColumnSubstitutions[i].Original = Util.generatePhysicalName(l.Keys[0].ColumnNames[0], ma.builderVersion);
                dt.ColumnSubstitutions[i].New = Util.generatePhysicalName(colName, ma.builderVersion);
            }
        }

        public void generateSnowflakeJoinsForDependencies()
        {
            //Hidden hierarchy flag is now deprecated - so clear it from previously generated hierarchies
            foreach (Hierarchy h in ma.hmodule.getHierarchies())
            {
                h.Hidden = false;
            }

            if (ma.dependencies == null)
            {
                return;
            }
            // Build up dependency layers
            for (int dependencyLevelNum = 2; dependencyLevelNum <= 5; dependencyLevelNum++)
            {
                foreach (HierarchyLevel[] dependency in ma.dependencies)
                {
                    string depLevelName = null;
                    HierarchyLevel baseLevel = dependency[0];
                    Hierarchy h = ma.hmodule.getHierarchy(baseLevel.HierarchyName);
                    if (h == null)
                        continue;
                    Level l = h.findLevel(baseLevel.LevelName);
                    if (l == null)
                        continue;
                    Level gl = ma.hmodule.getNextGeneratedTableLevel(h, l);
                    if (gl == null)
                        continue;
                    depLevelName = gl.Name;
                    List<DimensionTable> baseTables = new List<DimensionTable>();
                    DimensionTable basedt = null;
                    // Find the table that we are going to snowflake from
                    foreach (DimensionTable sdt in ma.dimensionTablesList)
                    {
                        if (sdt.DimensionName == baseLevel.HierarchyName && sdt.Level == depLevelName &&
                            (sdt.InheritTable == null || sdt.InheritTable.Length == 0))
                        {
                            basedt = sdt;
                            break;
                        }
                    }
                    if(basedt == null)
                    {
                        continue;
                    }
                    basedt.SnowflakeAllDimensions = true;
                    baseTables.Add(basedt);
                    /*
                     * Make sure the dependency level matches
                     */
                    int depLevel = dependencyLevel(dependency, ma.dependencies, null);
                    if (depLevel != dependencyLevelNum)
                    {
                        continue;
                    }
                    DimensionTable dt = null;
                    // Find a dimension table at this level
                    foreach (DimensionTable sdt in ma.dimensionTablesList)
                    {
                        if (sdt.DimensionName == dependency[1].HierarchyName && sdt.Level == dependency[1].LevelName &&
                            !baseTables.Contains(sdt) &&
                            (sdt.InheritTable == null || sdt.InheritTable.Length == 0))
                        {
                            dt = sdt;
                            break;
                        }
                    }
                    if (dt != null)
                    {
                        dt.SnowflakeAllDimensions = true;
                        //Just create the joins, no table creation necessary - note the argument useExistingTable to the method is set to true
                        snowflakeTable(dt, baseTables, true, true, false);
                    }
                }
            }
        }

        public void generateSnowflakeHierarchies()
        {
            // Clear previous hidden flags
            foreach (Hierarchy h in ma.hmodule.getHierarchies())
            {
                h.Hidden = false;
            }
            List<HierarchyLevel> snowflaked = new List<HierarchyLevel>();
            HashSet<string> movedSnowflakes = new HashSet<string>();
            // Build up dependency layers
            for (int dependencyLevelNum = 2; dependencyLevelNum <= 5; dependencyLevelNum++)
            {
                foreach (HierarchyLevel[] dependency in ma.dependencies)
                {
                    /*
                     * See if we have already snowflaked this one
                     */
                    bool secondary = false;
                    foreach (HierarchyLevel hl in snowflaked)
                    {
                        if (hl.HierarchyName == dependency[1].HierarchyName && hl.LevelName == dependency[1].LevelName)
                        {
                            secondary = true;
                            break;
                        }
                    }
                    bool found = false;
                    string depLevelName = null;
                    HierarchyLevel baseLevel = dependency[0];
                    Hierarchy h = ma.hmodule.getHierarchy(baseLevel.HierarchyName);
                    if (h == null)
                        continue;
                    Level l = h.findLevel(baseLevel.LevelName);
                    if (l == null)
                        continue;
                    Level gl = ma.hmodule.getNextGeneratedTableLevel(h, l);
                    if (gl == null)
                        continue;
                    found = true;
                    depLevelName = gl.Name;
                    List<DimensionTable> baseTables = new List<DimensionTable>();
                    DimensionTable basedt = null;
                    // Find the table that we are going to snowflake from
                    foreach (DimensionTable sdt in ma.dimensionTablesList)
                    {
                        if (sdt.DimensionName == baseLevel.HierarchyName && sdt.Level == depLevelName &&
                            (sdt.InheritTable == null || sdt.InheritTable.Length == 0))
                        {
                            basedt = sdt;
                            break;
                        }
                    }
                    if (!h.Hidden)
                    {
                        baseTables.Add(basedt);
                    }
                    else
                    {
                        /* 
                         * Get the tables we should snowflake from instead.
                         * Find all tables that inherit from the table we are interested in
                         */
                        foreach (DimensionTable sdt in ma.dimensionTablesList)
                        {
                            if (sdt.InheritTable == basedt.TableName &&
                                !(sdt.DimensionName == dependency[1].HierarchyName && sdt.Level == dependency[1].LevelName))
                            {
                                baseTables.Add(sdt);
                            }
                        }
                    }
                    if (baseTables.Count == 0)
                        continue;
                    /*
                     * Make sure the dependency level matches
                     */
                    int depLevel = dependencyLevel(dependency, ma.dependencies, null);
                    if (depLevel != dependencyLevelNum)
                    {
                        found = false;
                    }
                    if (found)
                    {
                        DimensionTable dt = null;
                        // Find a dimension table at this level
                        foreach (DimensionTable sdt in ma.dimensionTablesList)
                        {
                            if (sdt.DimensionName == dependency[1].HierarchyName && sdt.Level == dependency[1].LevelName &&
                                !baseTables.Contains(sdt) &&
                                (sdt.InheritTable == null || sdt.InheritTable.Length == 0))
                            {
                                dt = sdt;
                                break;
                            }
                        }
                        if (dt != null)
                        {
                            List<DimensionTable> result = snowflakeTable(dt, baseTables, secondary, false, true);
                            if (result != null)
                            {
                                /*
                                 * See if the table we just snowflaked has any tables in its current
                                 * dimension that are snowflaked off of it. If so, we need to carry those
                                 * along with us. For example, if we just moved store.store from the store
                                 * dimension to the employee dimension and there is another table in the
                                 * store dimension store.region that snowflakes off of store.store,
                                 * we need to copy that table into the employee dimension and snowflake
                                 * it off the newly added table. Just create a new table that inherits 
                                 * from the old one, then copy the join.
                                 */
                                Dictionary<DimensionTable, DimensionTable> dtlist = new Dictionary<DimensionTable, DimensionTable>();
                                foreach (DimensionTable pdt in result)
                                {
                                    if (!dtlist.ContainsKey(dt))
                                    {
                                        dtlist.Add(dt, pdt);
                                    }
                                }
                                while (dtlist.Count > 0)
                                {
                                    Dictionary<DimensionTable, DimensionTable> newlist = new Dictionary<DimensionTable, DimensionTable>();
                                    foreach (DimensionTable rootdt in dtlist.Keys)
                                    {
                                        DimensionTable sdt = dtlist[rootdt];
                                        List<DimensionTable> pslist = getSnowflakedTables(ma, rootdt);
                                        foreach (DimensionTable pdt in pslist)
                                        {
                                            if (movedSnowflakes.Contains(pdt.TableName))
                                                continue;
                                            // Only do this for the main connection
                                            if (pdt.TableSource.Connection != ma.connection.connectionList[0].Name)
                                                continue;
                                            DimensionTable newdt = (DimensionTable)pdt.Clone();
                                            newdt.TableName = sdt.DimensionName + " " + pdt.TableName;
                                            if (movedSnowflakes.Contains(newdt.TableName))
                                                continue;
                                            movedSnowflakes.Add(newdt.TableName);
                                            newdt.DimensionName = sdt.DimensionName;
                                            newdt.Level = sdt.Level;
                                            newdt.InheritTable = pdt.TableName;
                                            // If duplicates, add a prefix
                                            Hierarchy nh = ma.hmodule.getDimensionHierarchy(pdt.DimensionName);
                                            if (nh == null)
                                                continue;
                                            Level nl = nh.findLevel(pdt.Level);
                                            if (nl == null)
                                                continue;
                                            LevelKey lk = null;
                                            foreach (LevelKey slk in nl.Keys)
                                            {
                                                if (slk.SurrogateKey)
                                                    continue;
                                                lk = slk;
                                                break;
                                            }
                                            if (lk == null)
                                                continue;
                                            if (hasDuplicateNames(pdt, lk, newdt.DimensionName))
                                                newdt.InheritPrefix = pdt.DimensionName + "-";
                                            newdt.Calculated = true;
                                            ma.addDimensionTable(newdt, null, false, MAX_PHYSICAL_TABLE_NAME_LENGTH);
                                            addSnowflakeJoin(ma, sdt, newdt, lk);
                                            newlist.Add(pdt, newdt);
                                        }
                                    }
                                    dtlist.Clear();
                                    if (newlist.Count > 0)
                                        dtlist = newlist;
                                }
                                snowflaked.Add(dependency[1]);
                            }
                        }
                    }
                }
            }
        }

        private List<DimensionTable> getSnowflakedTables(MainAdminForm ma, DimensionTable basedt)
        {
            List<DimensionTable> plist = new List<DimensionTable>();
            DataRow joinRow = null;
            foreach (DataRow dr in ma.Joins.Rows)
            {
                if ((string)dr["Table1"] == basedt.TableName)
                {
                    joinRow = dr;
                    break;
                }
            }
            if (joinRow != null)
            {
                foreach (DimensionTable dt in ma.dimensionTablesList)
                {
                    if (dt.TableName == ((string)joinRow["Table2"]))
                    {
                        plist.Add(dt);
                        break;
                    }
                }
            }
            return (plist);
        }

        /*
         * See if a dependencey (either direct or indirect) has been mapped from key to dep
         */
        private bool isDuplicate(HierarchyLevel key, HierarchyLevel dep, HashSet<HierarchyLevel[]> traversed)
        {
            foreach (HierarchyLevel[] hl in traversed)
            {
                if (hl[0].HierarchyName == key.HierarchyName && hl[0].LevelName == key.LevelName)
                {
                    if (hl[1].HierarchyName == dep.HierarchyName && hl[1].LevelName == dep.LevelName)
                        return (true);
                    else
                        if (isDuplicate(hl[1], dep, traversed))
                            return (true);
                }
            }
            return (false);
        }

        private List<DimensionTable> snowflakeTable(DimensionTable dt, List<DimensionTable> baseTables, bool secondary, bool useExistingTable, bool hideHierarchy)
        {
            Hierarchy h = ma.hmodule.getDimensionHierarchy(dt.DimensionName);
            if (h == null)
                return null;
            Level l = h.findLevel(dt.Level);
            if (l == null)
                return null;
            HashSet<string> leConnections = new HashSet<string>();
            if (ma.localETLLoadGroupToLocalETLConfig != null)
            {
                foreach (LocalETLConfig leConfig in ma.localETLLoadGroupToLocalETLConfig.Values)
                {
                    leConnections.Add(leConfig.getLocalETLConnection());
                }
            }
            if (dt.TableSource.Connection != ma.connection.connectionList[0].Name && !leConnections.Contains(dt.TableSource.Connection))
                return null;
            LevelKey lk = null;
            foreach (LevelKey slk in l.Keys)
            {
                if (!slk.SurrogateKey)
                {
                    lk = slk;
                    break;
                }
            }
            if (lk == null)
                return (null);
            List<DimensionTable> result = new List<DimensionTable>();
            foreach (DimensionTable basedt in baseTables)
            {
                // Make sure the base table has the key
                DimensionTable keydt = basedt;
                if (basedt.InheritTable != null && basedt.InheritTable.Length > 0)
                {
                    foreach (DimensionTable sdt in ma.dimensionTablesList)
                    {
                        if (sdt.TableName == basedt.InheritTable)
                        {
                            keydt = sdt;
                            break;
                        }
                    }
                }
                bool hasKey = true;
                DataRow[] dcmRows = ma.dimensionColumnMappings.Select("TableName='" + keydt.TableName + "'");
                foreach (string s in lk.ColumnNames)
                {
                    bool found = false;
                    foreach (DataRow dr in dcmRows)
                    {
                        if ((string)dr["ColumnName"] == lk.ColumnNames[0])
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        hasKey = false;
                        break;
                    }
                }
                if (!hasKey)
                    return null;
                // Create the snowflake table (only on primary connection)
                DimensionTable newdt = null;
                if (!secondary)
                {
                    newdt = (DimensionTable)dt.Clone();
                    newdt.TableName = basedt.DimensionName + " " + dt.TableName;
                    newdt.DimensionName = basedt.DimensionName;
                    newdt.Level = basedt.Level;
                    newdt.InheritTable = dt.TableName;
                    // If duplicates, add a prefix
                    if (hasDuplicateNames(dt, lk, newdt.DimensionName))
                        newdt.InheritPrefix = dt.DimensionName + "-";
                    newdt.Calculated = true;
                    ma.addDimensionTable(newdt, null, false, MAX_PHYSICAL_TABLE_NAME_LENGTH);
                    secondary = true;
                }
                else
                {
                    /* 
                     * If this level has already been snowflaked, just add a join
                     */
                    if (useExistingTable)
                    {
                        newdt = dt;
                    }
                    else
                    {
                        /*
                         * First, find the table that has already been snowflaked
                         */
                        foreach (DimensionTable sdt in ma.dimensionTablesList)
                        {
                            if (sdt.InheritTable == dt.TableName)
                            {
                                newdt = sdt;
                                break;
                            }
                        }
                    }
                }
                // Generate snowflake join
                addSnowflakeJoin(ma, basedt, newdt, lk);
                // Move any snowflaked tables into this new dimension
                foreach (DimensionTable sdt in ma.dimensionTablesList)
                {
                    if (sdt.AutoGenerated && sdt.InheritTable != null && sdt.InheritTable.Length > 0
                        && sdt.DimensionName == dt.DimensionName && sdt.InheritPrefix != TYPEII_CURRENT_VERSION_INHERIT_PREFIX)
                    {
                        sdt.DimensionName = newdt.DimensionName;
                        sdt.Level = newdt.Level;
                    }
                }
                if (hideHierarchy)
                    h.Hidden = true;
                result.Add(newdt);
            }
            if (result.Count == 0)
                return (null);
            return (result);
        }

        private void addSnowflakeJoin(MainAdminForm ma, DimensionTable basedt, DimensionTable newdt, LevelKey lk)
        {
            if (manualOverrideSnowflakeJoinExists(basedt, newdt))
            {
                return;
            }
            DataRow[] joinRows = ma.Joins.Select("table1='" + basedt.TableName + "' AND table2='" + newdt.TableName + "'");
            DataRow joinRow = null;
            if (joinRows.Length > 0)
            {
                foreach (DataRow jr in joinRows)
                {
                    if ((bool)jr["AutoGenerated"])
                    {
                        joinRow = jr;
                        break;
                    }
                }
            }
            if (joinRow == null)
            {
                joinRow = ma.Joins.NewRow();
                ma.Joins.Rows.Add(joinRow);
            }
            joinRow["AutoGenerated"] = true;
            List<string> keyColumns = new List<string>(1);
            // Need to use natural key (surrogate key won't be created on basedt dimension table)
            foreach (string colname in lk.ColumnNames)
            {
                keyColumns.Add(colname);
            }
            // See if the base table is a degnerate dimension
            Hierarchy h = ma.hmodule.getDimensionHierarchy(basedt.DimensionName);
            if (h == null)
                return;
            Level l = h.findLevel(basedt.Level);
            if (l == null)
                return;
            string condition = ma.getJoinCondition(newdt, keyColumns, basedt, l.Degenerate);
            joinRow["table1"] = basedt.TableName;
            joinRow["table2"] = newdt.TableName;
            joinRow["JoinCondition"] = condition;
        }

        private bool hasDuplicateNames(DimensionTable dt, LevelKey lk, string dimName)
        {
            foreach (DataRow dr in ma.dimensionColumnMappings.Rows)
            {
                if (Array.IndexOf<string>(lk.ColumnNames, (string)dr["ColumnName"]) >= 0)
                    continue;
                if ((string)dr["TableName"] == dt.TableName)
                {
                    DataRowView[] drv = ma.dimensionColumnNamesView.FindRows(new object[] { dimName, (string)dr["ColumnName"] });
                    if (drv.Length > 0)
                        return (true);
                }
            }
            return (false);
        }

        /*
         * Determine the level of a dependency. I.e. trace how deep a dependency goes (e.g. a level is dependent
         * on another, which is dependent on another, etc.)
         */
        private int dependencyLevel(HierarchyLevel[] dependency, List<HierarchyLevel[]> dependencies, List<HierarchyLevel[]> traversedLevels)
        {
            if (traversedLevels == null)
                traversedLevels = new List<HierarchyLevel[]>();
            else
                if (traversedLevels.Contains(dependency))
                    return (getDepth(dependency[0]));
            traversedLevels.Add(dependency);
            foreach (HierarchyLevel[] dependency2 in dependencies)
            {
                if (dependency[0].HierarchyName == dependency2[1].HierarchyName &&
                    dependency[0].LevelName == dependency2[1].LevelName)
                {
                    return (dependencyLevel(dependency2, dependencies, traversedLevels) + 1);
                }
            }
            return (getDepth(dependency[0]));
        }

        private int getDepth(HierarchyLevel hl)
        {
            Hierarchy h = ma.hmodule.getHierarchy(hl.HierarchyName);
            if (h == null)
                return (1);
            Level l = h.findLevel(hl.LevelName);
            if (l == null)
                return (1);
            return (l.getDepth());
        }

        private class MeasureTablePair
        {
            public MeasureTable mt2;
            public List<MeasureTableGrain[]> matches;
        }
        Dictionary<MeasureTable, List<MeasureTableGrain>> grainCache = new Dictionary<MeasureTable, List<MeasureTableGrain>>();

        /*
         * Was originally an experimental routine - not fully functional. Was meant to be used as a replacement for 
         * carrying keys down hierarchies in the ETL by using metadata to make the complex relationships.
         * The goal was to simplify the ETL and move the logic up. In this case, each measure table
         * would effectively be a clone of a staging table, with no additional keys. To join to other
         * dimensions, metadata would create composite measure table definitions that joined different 
         * fact tables together, combining their keys. This has the disadvantage of losing much of the
         * star-schema design (fact tables basically become snapshots of staging tables) and requiring
         * many more joins. Now it's used in conjunction with carrying keys down to provide the ability
         * to create calculations at the measure grain across measure tables.
         * 
         * The approach is to find all measure tables at a higher grain than a given table. Then, create
         * new combined measure tables with those higher level ones. Start out just by combining the base
         * table with each of the higher level ones in pairs. Then, if there are few enough tables, combine
         * in sets of threes, then fours, etc. Combinatorics prohibit all combinations (e.g. if a table had
         * 25 higher level tables, then to produce all combinations - ie. 25 choose 1 plus 25 choose 2 plus
         * 25 choose 3 and so on - would create millions of logical tables. This would take forever to create
         * and process so the system has some limits on combinations and complexity. In the future, if we
         * discover a situation where someone wants to combine facts from more tables than the limiters permit
         * then we could add an enhancement. We could rule it out in Adhoc, but allow it in custom formulas. 
         * Then, during custom formula processing if we find that a given combination of metrics does not exist
         * we could create just that one combination. However, this is an unlikely corner case and one I am
         * deferring for now.
         */
        public void generateInheritedMeasureTables()
        {
            /*
             * For each measure table, see if it is at a lower grain than another measure table.
             * If so, create a new meaure table definition that combines the two
             */
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                if (mt.Type != MeasureTable.NORMAL)
                    continue;
                if (mt.InheritTable != null && mt.InheritTable.Length > 0)
                    continue;
                if (!mt.AutoGenerated)
                    continue;
                if (mt.TableSource == null || mt.TableSource.Tables == null)
                    continue;
                if (mt.Grain == null && mt.TableSource.Tables.Length == 1 && ma.stagingTableMod.findTable(mt.TableSource.Tables[0].PhysicalName) != null)
                    continue;
                grainCache.Add(mt, ma.getGrainInfo(mt));
            }
            // Find all pairs where one measure table is at a higher grain than the other
            foreach (MeasureTable mt in grainCache.Keys)
            {
                foreach (MeasureTable mt2 in grainCache.Keys)
                {
                    if (mt2 == mt)
                        continue;
                    if (!equalsConnectionWithNull(mt, mt2))
                        continue;
                    if (mt.Grain != null && mt2.Grain != null)
                    {
                        if (mt.Grain.connection != mt2.Grain.connection)
                            continue;
                    }
                    List<MeasureTableGrain> mtgrain = grainCache[mt];
                    List<MeasureTableGrain> mtgrain2 = grainCache[mt2];
                    // See if mtgrain is at a lower level than mtgrain2
                    List<MeasureTableGrain[]> matches = isLowerGrain(ma, mtgrain2, mtgrain, ma.dependencies, automaticModeDependencies);
                    if (matches != null)
                    {
                        List<MeasureTablePair> pairList = null;
                        if (pairs.ContainsKey(mt))
                            pairList = pairs[mt];
                        else
                        {
                            pairList = new List<MeasureTablePair>();
                            pairs[mt] = pairList;
                        }
                        MeasureTablePair mtp = new MeasureTablePair();
                        // Don't need commented pieces
                        mtp.mt2 = mt2;
                        mtp.matches = matches;
                        pairList.Add(mtp);
                    }
                }
            }
            /*
             * Calculate the depth of combinations. Need to prevent combinatoric explosion. Maximum of new logical tables.
             */
            // Create combinations and then create measure tables for each
            int[] sizes = new int[MAX_COMBINATION_DEPTH];
            foreach (MeasureTable mt in pairs.Keys)
            {
                List<MeasureTablePair> pairList = pairs[mt];
                // Generate sets
                List<int[]> sets = SourceFiles.generateKeySets(pairList.Count, MAX_COMBINATION_DEPTH);
                foreach (int[] set in sets)
                {
                    sizes[set.Length - 1]++;
                }
                setMap.Add(mt, sets);
            }
            int maxSize = 0;
            for (int i = 1; i <= MAX_COMBINATION_DEPTH; i++)
            {
                double sum = ma.stagingTableMod.getStagingTables().Count;
                // Scale by average expansion due to date tables
                double scaleFactor = ((double)numBaseMeasureTables(ma)) / ((double)ma.stagingTableMod.getStagingTables().Count);
                for (int j = 0; j < i; j++)
                    sum += sizes[j] * scaleFactor;
                if (sum < MAX_COMBINATIONS)
                    maxSize = i;
                else
                    break;
            }
            if (maxSize > 0)
                foreach (MeasureTable mt in pairs.Keys)
                {
                    List<MeasureTablePair> pairList = pairs[mt];
                    List<int[]> sets = setMap[mt];
                    // Process each set
                    foreach (int[] set in sets)
                    {
                        if (set.Length > maxSize)
                            continue;
                        createCombinedMeasureTable(mt, pairList, set);
                    }
                }
        }

        public static int numBaseMeasureTables(MainAdminForm maf)
        {
            int count = 0;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                count++;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.GenerateTimeDimension && (sc.DataType == "DateTime" || sc.DataType == "Date"))
                        count++;
                }
            }
            return count;
        }

        private MeasureTable createCombinedMeasureTable(MeasureTable mt, List<MeasureTablePair> pairList, int[] set)
        {
            MeasureGrain mg = new MeasureGrain();
            mg.measureTableGrains = grainCache[mt].ToArray();
            mg.measureTableName = mt.TableName;
            mg.LoadGroups = mt.Grain.LoadGroups;
            string connection = null;
            mg.connection = hasLiveAccessLoadGroup(mt.Grain.LoadGroups, out connection) ? connection : "Default Connection";
            mg.overrideKeyColumns = mt.Grain.overrideKeyColumns;
            foreach (int i in set)
                mg.measureTableName += '_' + pairList[i].mt2.TableName;
            MeasureTable newmt = createNewMeasureTable(mg, pairList[set[0]].mt2, "", true);
            if (newmt == null)
                return null;
            newmt.Cardinality = mt.Cardinality;
            newmt.InheritTable = mt.TableName;
            newmt.Transactional = mt.Transactional;
            newmt.CurrentSnapshotOnly = mt.CurrentSnapshotOnly;
            newmt.TableSource.Tables = new TableSource.TableDefinition[set.Length + 1];
            newmt.TableSource.Tables[0] = new TableSource.TableDefinition();
            newmt.TableSource.Tables[0].PhysicalName = mt.TableSource.Tables[0].PhysicalName;
            newmt.TimeAlias = mt.TimeAlias;
            bool excludeLoadDateAsKey = mt.CurrentSnapshotOnly;
            for (int i = 0; i < set.Length; i++)
            {
                MeasureTable mt2 = pairList[set[i]].mt2;
                excludeLoadDateAsKey = excludeLoadDateAsKey || mt2.CurrentSnapshotOnly || ( mt.Transactional && mt2.Transactional);
            }
            List<TableSource.TableSourceFilter> filterList = new List<TableSource.TableSourceFilter>();
            for (int i = 0; i < set.Length; i++)
            {
                MeasureTable mt2 = pairList[set[i]].mt2;
                // Make sure security filters apply
                if (mt2.TableSource.Filters != null)
                    filterList.AddRange(mt2.TableSource.Filters);
                newmt.Cardinality = Math.Max(newmt.Cardinality, mt2.Cardinality);
                newmt.InheritTable += ',' + mt2.TableName;
                newmt.TableSource.Tables[i + 1] = new TableSource.TableDefinition();
                newmt.TableSource.Tables[i + 1].PhysicalName = mt2.TableSource.Tables[0].PhysicalName;
                // If any of the tables are transactional, make it transactional
                newmt.Transactional = newmt.Transactional || mt2.Transactional;
                newmt.CurrentSnapshotOnly = newmt.CurrentSnapshotOnly && mt2.CurrentSnapshotOnly;
                newmt.TimeAlias = newmt.TimeAlias || mt2.TimeAlias;
                // Build join clause
                StringBuilder sb = new StringBuilder();
                foreach (MeasureTableGrain[] match in pairList[set[i]].matches)
                {
                    Hierarchy h = ma.hmodule.getDimensionHierarchy(match[0].DimensionName);
                    if (ma.builderVersion >= 5 && excludeLoadDateAsKey && ma.timeDefinition != null && h != null && ma.timeDefinition.Name == h.DimensionName)
                        continue;
                    if (h != null)
                    {
                        Level l = h.findLevel(match[0].DimensionLevel);
                        if (l != null)
                        {
                            LevelKey lk = l.getSurrogateKey();
                            if (lk == null)
                            {
                                lk = l.Keys[0];
                            }
                            // Make sure it exists in both tables
                            bool foundmt = false;
                            bool foundmt2 = false;
                            foreach (string s in lk.ColumnNames)
                            {
                                string lname = h.DimensionName + "." + s;
                                DataRowView[] drv = ma.measureColumnMappingsNamesView.FindRows(lname);
                                foreach (DataRowView dr in drv)
                                {
                                    if ((string)dr.Row["TableName"] == mt.TableName)
                                    {
                                        foundmt = true;
                                    }
                                    else if ((string)dr.Row["TableName"] == mt2.TableName)
                                    {
                                        foundmt2 = true;
                                    }
                                }
                            }
                            if (!foundmt || !foundmt2)
                                continue;
                            foreach (string s in lk.ColumnNames)
                            {
                                string pname = getForeignKeyPhysicalName(dbType, h.DimensionName, s, PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion);
                                if (sb.Length > 0)
                                    sb.Append(" AND ");
                                sb.Append(newmt.TableSource.Tables[0].PhysicalName + "." + pname + "=" +
                                    newmt.TableSource.Tables[i + 1].PhysicalName + "." + pname);
                            }
                        }
                    }
                }
                newmt.TableSource.Tables[i + 1].JoinClause = sb.ToString();
            }
            if (filterList.Count > 0)
                newmt.TableSource.Filters = filterList.ToArray();
            // Add a cardinality penalty based on set size
            newmt.Cardinality *= (1 + ((double)set.Length / 10.0));
            return newmt;
        }

        /*
         * Return true if manually defined join exists for a given combination of dimension table and measure table, false otherwise
         */
        private bool manualOverrideJoinExists(DimensionTable dt, MeasureTable mt)
        {
            bool manualJoinExists = false;
            DataRowView[] joinRows = ma.joinsView.FindRows(new object[] { mt.TableName, dt.TableName });
            if (joinRows.Length > 0)
            {
                foreach (DataRowView jr in joinRows)
                {
                    if (!(bool)jr["AutoGenerated"])
                    {
                        manualJoinExists = true;
                        break;
                    }
                }
            }
            return manualJoinExists;
        }

        /*
         * Return true if manually defined join exists for a given combination two dimension tables, false otherwise
         */
        private bool manualOverrideSnowflakeJoinExists(DimensionTable dt1, DimensionTable dt2)
        {
            bool manualJoinExists = false;
            DataRowView[] joinRows = ma.joinsView.FindRows(new object[] { dt1.TableName, dt2.TableName });
            if (joinRows.Length > 0)
            {
                foreach (DataRowView jr in joinRows)
                {
                    if (!(bool)jr["AutoGenerated"])
                    {
                        manualJoinExists = true;
                        break;
                    }
                }
            }
            return manualJoinExists;
        }

        public int getMaxPhysicalTableNameLength()
        {
            return this.MAX_PHYSICAL_TABLE_NAME_LENGTH;
        }

        public int getMaxPhysicalColumnNameLength()
        {
            return this.MAX_PHYSICAL_COLUMN_NAME_LENGTH;
        }

        public void addDiscoveryTables(MainAdminForm ma, TimeDefinition td, List<BuildAction> balist)
        {
            Dictionary<string, DimensionTable> dimTableNames = new Dictionary<string, DimensionTable>();
            Dictionary<string, MeasureTable> baseMeasureTableNames = new Dictionary<string, MeasureTable>();
            Dictionary<MeasureTable, List<StagingTable>> stagingTableMap = new Dictionary<MeasureTable, List<StagingTable>>();
            Dictionary<MeasureTable, List<StagingColumn>> timeTablesToCreate = new Dictionary<MeasureTable, List<StagingColumn>>();

            // Add base tables
            addBaseTables(ma, td, balist, dimTableNames, baseMeasureTableNames, stagingTableMap, timeTablesToCreate);

            // Add joins and combination facts
            addJoinsAndCombinationFacts(ma, td, balist, dimTableNames, baseMeasureTableNames, stagingTableMap, timeTablesToCreate);

            // Add time transformed measure tables
            addTimeTransformedTables(ma, td, balist, stagingTableMap, timeTablesToCreate);
        }

        private void addTimeTransformedTables(MainAdminForm ma, TimeDefinition td, List<BuildAction> balist, Dictionary<MeasureTable, List<StagingTable>> stagingTableMap,
            Dictionary<MeasureTable, List<StagingColumn>> timeTablesToCreate)
        {
            foreach(MeasureTable mt in timeTablesToCreate.Keys)
            {
                if (!stagingTableMap.ContainsKey(mt))
                    continue;
                List<StagingTable> stlist = stagingTableMap[mt];
                if (stlist.Count == 0)
                    continue;
                string localETLConnection = findLocalETLConnectionForStagingTable(stlist[0]);

                List<StagingColumn> dateColumns = timeTablesToCreate[mt];

                // Add measure tables and columns
                foreach (StagingColumn dateCol in dateColumns)
                {
                    string prefix = dateCol.Name + ": ";
                    // Create the base measure table
                    MeasureTable imt = new MeasureTable();
                    imt.TableName = prefix + mt.TableName;
                    imt.Transactional = true;
                    imt.CurrentSnapshotOnly = false;
                    imt.LoadGroups = null;
                    imt.TableSource = mt.TableSource;
                    imt.PhysicalName = mt.PhysicalName;
                    imt.Type = 0;
                    imt.Cardinality = mt.Cardinality;
                    imt.AutoGenerated = true;
                    imt.Calculated = false;
                    imt.InheritTable = mt.TableName;
                    imt.InheritPrefix = prefix;
                    imt.TimeAlias = true;
                    ma.addNewMeasureTable(imt);

                    // Add time joins
                    DataRow joinRow = ma.Joins.NewRow();
                    ma.Joins.Rows.Add(joinRow);
                    joinRow["AutoGenerated"] = true;
                    joinRow["table1"] = imt.TableName;
                    joinRow["table2"] = "Time Day";
                    joinRow["Redundant"] = false;
                    StringBuilder sb = new StringBuilder();
                    StagingTable foundst = null;
                    foreach (StagingTable st in stlist)
                    {
                        if (dateCol.Name == null || dateCol.Name.Length == 0)
                            continue;
                        bool foundcol = false;
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc == dateCol)
                            {
                                foundcol = true;
                                break;
                            }
                        }
                        if (!foundcol)
                            continue;
                        sb.Append("\"Time Day\".Day_ID" + PHYSICAL_SUFFIX + "=\"" + imt.TableName + "\"." + st.Name + "." + st.getDiscoveryColumnDateID(dateCol.Name, ma.builderVersion));
                        foundst = st;
                        break;
                    }
                    joinRow["JoinCondition"] = sb;

                    // Add other time joins
                    if (td != null && foundst != null)
                    {
                        if (td.Periods != null)
                        {
                            if (td.Day)
                                addDiscoveryTimePeriods(dayLevel, td, imt, localETLConnection, foundst, dateCol);
                            if (td.Week)
                                addDiscoveryTimePeriods(weekLevel, td, imt, localETLConnection, foundst, dateCol);
                            if (td.Month)
                                addDiscoveryTimePeriods(monthLevel, td, imt, localETLConnection, foundst, dateCol);
                            if (td.Quarter)
                                addDiscoveryTimePeriods(quarterLevel, td, imt, localETLConnection, foundst, dateCol);
                            if (td.Halfyear)
                                addDiscoveryTimePeriods(halfyearLevel, td, imt, localETLConnection, foundst, dateCol);
                        }
                    }
                }
            }
        }

        private void addJoinsAndCombinationFacts(MainAdminForm ma, TimeDefinition td, List<BuildAction> balist, Dictionary<string, DimensionTable> dimTableNames,
            Dictionary<string, MeasureTable> baseMeasureTableNames, Dictionary<MeasureTable, List<StagingTable>> stagingTableMap,
            Dictionary<MeasureTable, List<StagingColumn>> timeTablesToCreate)
        {
            List<StagingTable> stables = new List<StagingTable>();
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.Disabled || !st.DiscoveryTable)
                    continue;
                stables.Add(st);
            }
            if (ma.Imports != null && ma.Imports.Length > 0 && importList != null)
            {
                foreach (StagingTable st in importList.Keys)
                    stables.Add(st);
            }
            foreach (StagingTable st in stables)
            {
                if (!baseMeasureTableNames.ContainsKey(st.Name) && !dimTableNames.ContainsKey(st.Name))
                    continue;
                MeasureTable mt = null;
                if (baseMeasureTableNames.ContainsKey(st.Name))
                    mt = baseMeasureTableNames[st.Name];
                DimensionTable dt = null;
                if (dimTableNames.ContainsKey(st.Name))
                    dt = dimTableNames[st.Name];
                if (mt == null && dt == null)
                    continue;
                List<ForeignKey> fksources = new List<ForeignKey>();
                if (st.ForeignKeys != null)
                {
                    foreach (ForeignKey fk in st.ForeignKeys)
                        fksources.Add(fk);
                }
                if (importList != null && importList.ContainsKey(st))
                {
                    foreach (PackageImport pi in ma.Imports)
                    {
                        if (pi.Items != null && pi.Items.Length > 0)
                        {
                            foreach (ImportedItem ii in pi.Items)
                            {
                                if (ii.ItemName == st.Name && ii.ForeignKeys != null)
                                {
                                    foreach (ForeignKey fk in ii.ForeignKeys)
                                        fksources.Add(fk);
                                }
                            }
                        }
                    }
                }
                foreach (ForeignKey fk in fksources)
                {
                    StagingTable primaryst = ma.stagingTableMod.findTable(fk.Source);
                    if (primaryst == null || primaryst.Disabled)
                        continue;
                    /* 
                     * Make sure that all sourceKey columns of primaryst actually exist on st - this should have 
                     * been validated already on the UI but just in case by a combination of tricks on UI, if we
                     * up with a source key that doesnt exist on joining tables, the join will fail on execution                         
                     */
                    bool foundSourceKey = false;
                    if (fk.Condition != null && fk.Condition.Length > 0)
                    {
                        foundSourceKey = true;
                    }
                    else if (primaryst.SourceKey != null)
                    {
                        foreach (string kname in primaryst.SourceKey)
                        {
                            foundSourceKey = false;
                            foreach (StagingColumn sc in st.Columns)
                            {
                                if (kname == sc.Name)
                                {
                                    foundSourceKey = true;
                                    break;
                                }
                            }
                            if (!foundSourceKey)
                            {
                                break;
                            }
                        }
                    }
                    if (!foundSourceKey)
                    {
                        continue;
                    }
                    MeasureTable primarymt = null;
                    if (baseMeasureTableNames.ContainsKey(primaryst.Name))
                    {
                        primarymt = baseMeasureTableNames[primaryst.Name];
                    }
                    DimensionTable primarydt = null;
                    if (dimTableNames.ContainsKey(primaryst.Name))
                    {
                        primarydt = dimTableNames[primaryst.Name];
                    }

                    if (mt != null && primarydt != null)
                    {
                        bool addJoin = true;
                        if (mt.TableSource != null && primarydt.TableSource != null && mt.TableSource.Connection != null && primarydt.TableSource.Connection != null && !mt.TableSource.Connection.Equals(primarydt.TableSource.Connection))
                        {
                            addJoin = false;
                        }
                        if (addJoin)
                        {
                            // Fact dimension join
                            DataRow joinRow = ma.Joins.NewRow();
                            ma.Joins.Rows.Add(joinRow);
                            joinRow["AutoGenerated"] = true;
                            joinRow["table1"] = mt.TableName;
                            joinRow["table2"] = primarydt.TableName;
                            joinRow["Redundant"] = false;
                            StringBuilder sb = new StringBuilder();
                            if (fk.Condition != null && fk.Condition.Length > 0)
                            {
                                sb.Append(fk.Condition);
                                string dname = st.getDisplayName();
                                sb = sb.Replace("\"" + dname + "\"", "\"" + dname + " Fact\"");
                            }
                            else
                            {
                                bool first = true;
                                foreach (string kname in primaryst.SourceKey)
                                {
                                    if (first)
                                        first = false;
                                    else
                                        sb.Append(" AND ");
                                    string primarypname = primaryst.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (primarypname == null && primaryst.InheritTables != null && primaryst.InheritTables.Length > 0)
                                    {
                                        // Check for inheritance
                                        foreach (InheritTable it in primaryst.InheritTables)
                                        {
                                            if (it.Type == 0)
                                            {
                                                StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                                                if (ist != null)
                                                    primarypname = ist.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                                if (primarypname != null)
                                                    break;
                                            }
                                        }
                                    }
                                    primarypname = "\"" + primarydt.TableName + "\"." + primarypname;
                                    string pname = st.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (pname == null && st.InheritTables != null && st.InheritTables.Length > 0)
                                    {
                                        // Check for inheritance
                                        foreach (InheritTable it in st.InheritTables)
                                        {
                                            if (it.Type == 0)
                                            {
                                                StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                                                if (ist != null)
                                                    pname = ist.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                                if (pname != null)
                                                    break;
                                            }
                                        }
                                    }
                                    pname = "\"" + mt.TableName + "\"." + pname;
                                    sb.Append(primarypname + "=" + pname);
                                }
                            }
                            // Reverse the direction since the type is maintained on the foreign key side (and direction is meant to be referenced from primary)
                            if (fk.Type == 2)
                                joinRow["JoinType"] = "Left Outer";
                            else if (fk.Type == 1)
                                joinRow["JoinType"] = "Right Outer";
                            else if (fk.Type == 3)
                                joinRow["JoinType"] = "Full Outer";
                            else
                                joinRow["JoinType"] = "Inner";
                            joinRow["Invalid"] = fk.Invalid;
                            joinRow["Redundant"] = fk.Redundant;
                            joinRow["JoinCondition"] = sb;
                        }
                        /*
                         * Combination fact
                         */
                        if (baseMeasureTableNames.ContainsKey(primaryst.Name) && mt.TableSource.Schema == primarymt.TableSource.Schema)
                        {
                            SourceFile sf = ma.getSourceFile(st);
                            if (sf == null && importList != null)
                                sf = importList[st];
                            SourceFile primarysf = ma.getSourceFile(primaryst);
                            if (primarysf == null && importList != null)
                                primarysf = importList[primaryst];
                            MeasureTable cmt = new MeasureTable();
                            cmt.TableName = st.Name + " " + primaryst.Name + " Fact";
                            cmt.Transactional = true;
                            cmt.CurrentSnapshotOnly = false;
                            cmt.LoadGroups = null;
                            cmt.InheritTable = primarymt.TableName + "," + mt.TableName;
                            cmt.InheritPrefix = "";
                            cmt.TableSource = new TableSource();
                            if (mt == null)
                            {
                                cmt.TableSource.Connection = findConnection(dt.TableSource.Connection);
                                cmt.TableSource.Schema = dt.TableSource.Schema;
                            }
                            else
                            {
                                cmt.TableSource.Connection = findConnection(mt.TableSource.Connection);
                                cmt.TableSource.Schema = mt.TableSource.Schema;
                            }
                            cmt.Cacheable = true;
                            cmt.TableSource.Tables = new TableSource.TableDefinition[2];
                            cmt.TableSource.Tables[0] = new TableSource.TableDefinition();
                            cmt.TableSource.Tables[0].PhysicalName = Util.generatePhysicalName(primaryst.Name, ma.builderVersion);
                            cmt.TableSource.Tables[1] = new TableSource.TableDefinition();
                            cmt.TableSource.Tables[1].PhysicalName = Util.generatePhysicalName(st.Name, ma.builderVersion);
                            cmt.TableSource.Tables[1].JoinType = 0;

                            StringBuilder sb = new StringBuilder();
                            if (fk.Condition != null && fk.Condition.Length > 0)
                            {
                                sb.Append(fk.Condition);
                                if (st.LiveAccess)
                                {
                                    if (st.TableSource != null && st.TableSource.Tables != null && st.TableSource.Tables.Length > 0)
                                        sb = sb.Replace("\"" + st.Name + "\"", st.TableSource.Tables[0].PhysicalName);
                                    if (primaryst.TableSource != null && primaryst.TableSource.Tables != null && primaryst.TableSource.Tables.Length > 0)
                                        sb = sb.Replace("\"" + primaryst.Name + "\"", primaryst.TableSource.Tables[0].PhysicalName);
                                }
                                else
                                {
                                    sb = sb.Replace("\"" + st.Name + "\"", st.Name);
                                    sb = sb.Replace("\"" + primaryst.Name + "\"", primaryst.Name);
                                }
                            }
                            else
                            {
                                bool first = true;
                                foreach (string kname in primaryst.SourceKey)
                                {
                                    if (first)
                                        first = false;
                                    else
                                        sb.Append(" AND ");
                                    string primarypname = primaryst.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (primarypname == null)
                                        continue;
                                    if (primarypname.IndexOf('.') < 0)
                                        primarypname = primaryst.Name + "." + primarypname;
                                    string pname = st.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (pname.IndexOf('.') < 0)
                                        pname = st.Name + "." + pname;
                                    sb.Append(pname + "=" + primarypname);
                                }
                            }
                            cmt.TableSource.Tables[1].JoinClause = sb.ToString();
                            cmt.PhysicalName = mt.TableSource.Tables[0].PhysicalName;
                            cmt.Type = 0;
                            if (sf != null && primarysf != null)
                                cmt.Cardinality = Math.Max(sf.NumRows, primarysf.NumRows);
                            else
                                cmt.Cardinality = Math.Max(mt.Cardinality, primarymt.Cardinality);
                            cmt.AutoGenerated = true;
                            cmt.Calculated = true;
                            List<StagingTable> stlist = null;
                            if (stagingTableMap.ContainsKey(cmt))
                                stlist = stagingTableMap[cmt];
                            else
                            {
                                stlist = new List<StagingTable>();
                                stagingTableMap.Add(cmt, stlist);
                            }
                            stlist.Add(st);
                            stlist.Add(primaryst);
                            ma.addNewMeasureTable(cmt);
                            // Add time transformations
                            List<StagingColumn> dateColumns = new List<StagingColumn>();
                            if (mt != null && timeTablesToCreate.ContainsKey(mt))
                                dateColumns.AddRange(timeTablesToCreate[mt]);
                            if (primarymt != null && timeTablesToCreate.ContainsKey(primarymt))
                                dateColumns.AddRange(timeTablesToCreate[primarymt]);
                            if (dateColumns.Count > 0)
                                timeTablesToCreate.Add(cmt, dateColumns);
                            cmt.Folder = "NO_TIME";
                        }
                    }
                    if (primarydt != null && dt != null)
                    {
                        bool addJoin = true;
                        if (primarydt.TableSource != null && dt.TableSource != null && primarydt.TableSource.Connection != null && dt.TableSource.Connection != null && !primarydt.TableSource.Connection.Equals(dt.TableSource.Connection))
                        {
                            addJoin = false;
                        }
                        if (addJoin)
                        {
                            // Snowflake join
                            DataRow joinRow = ma.Joins.NewRow();
                            ma.Joins.Rows.Add(joinRow);
                            joinRow["AutoGenerated"] = true;
                            joinRow["table1"] = dt.TableName;
                            joinRow["table2"] = primarydt.TableName;
                            joinRow["Redundant"] = false;
                            StringBuilder sb = new StringBuilder();
                            if (fk.Condition != null && fk.Condition.Length > 0)
                            {
                                sb.Append(fk.Condition);
                            }
                            else
                            {
                                bool first = true;
                                foreach (string kname in primaryst.SourceKey)
                                {
                                    if (first)
                                        first = false;
                                    else
                                        sb.Append(" AND ");
                                    string primarypname = primaryst.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (primarypname == null && primaryst.InheritTables != null && primaryst.InheritTables.Length > 0)
                                    {
                                        // Check for inheritance
                                        foreach (InheritTable it in primaryst.InheritTables)
                                        {
                                            if (it.Type == 0)
                                            {
                                                StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                                                if (ist != null)
                                                    primarypname = ist.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                                if (primarypname != null)
                                                    break;
                                            }
                                        }
                                    }
                                    string pname = st.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                    if (pname == null && st.InheritTables != null && st.InheritTables.Length > 0)
                                    {
                                        // Check for inheritance
                                        foreach (InheritTable it in st.InheritTables)
                                        {
                                            if (it.Type == 0)
                                            {
                                                StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                                                if (ist != null)
                                                    pname = ist.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                                if (pname != null)
                                                    break;
                                            }
                                        }
                                    }
                                    sb.Append("\"" + primarydt.TableName + "\"." + primarypname + "=" +
                                        "\"" + dt.TableName + "\"." + pname);
                                }
                            }
                            // Need to reverse the join direction
                            if (fk.Type == 2)
                                joinRow["JoinType"] = "Left Outer";
                            else if (fk.Type == 1)
                                joinRow["JoinType"] = "Right Outer";
                            else if (fk.Type == 3)
                                joinRow["JoinType"] = "Full Outer";
                            else
                                joinRow["JoinType"] = "Inner";
                            joinRow["Invalid"] = fk.Invalid;
                            joinRow["Redundant"] = fk.Redundant;
                            joinRow["JoinCondition"] = sb;
                        }
                    }
                }
            }
        }

        private void addBaseTables(MainAdminForm ma, TimeDefinition td, List<BuildAction> balist, Dictionary<string, DimensionTable> dimTableNames,
            Dictionary<string, MeasureTable> baseMeasureTableNames, Dictionary<MeasureTable, List<StagingTable>> stagingTableMap,
            Dictionary<MeasureTable, List<StagingColumn>> timeTablesToCreate)
        {
            Dictionary<string, string> aggregatedMetrics = new Dictionary<string, string>();
            HashSet<string> averageIntegers = new HashSet<string>();
            List<StagingTable> stables = new List<StagingTable>();
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.Disabled || !st.DiscoveryTable)
                    continue;
                stables.Add(st);
            }
            foreach (StagingTable st in stables)
            {
                SourceFile sf = null;
                if (!st.LiveAccess)
                {
                    sf = ma.getSourceFile(st);
                    if (sf == null && importList != null)
                        sf = importList[st];
                }
                /*
                 * For the purposes of discovery tables, the physical name of the table is the staging table name. The logical name of the
                 * table is the source file name (absent any extension). Also the physical name is set in the table source.
                 * 
                 * The opposite is true for columns. The logical name for a column is the staging column name
                 * The physical formula for a column is:
                 *      Live Access: the source file column name
                 *      Discovery Tables: the physical name version of the staging column name (ie. source file name is ignored)
                 */ 
                string basename = getDisplayName(st);
                string dimTableName = basename;
                string dimName = st.HierarchyName != null && st.HierarchyName.Length > 0 ? st.HierarchyName : dimTableName;

                int numDimColumns = 0;
                int numMeasureColumns = 0;
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.TargetTypes != null)
                    {
                        if (new List<string>(sc.TargetTypes).Contains(dimName))
                        {
                            numDimColumns++;
                        }
                        if (new List<string>(sc.TargetTypes).Contains("Measure"))
                        {
                            numMeasureColumns++;
                        }
                    }
                }

                DimensionTable dt = null;
                Hierarchy h = null;
                Level l = null;
                string connection = null;
                List<string> inheritDimTables = new List<string>();
                List<string> inheritMeasureTables = new List<string>();
                StringBuilder inheritPrefix = new StringBuilder();
                if (st.InheritTables != null && st.InheritTables.Length > 0)
                {
                    foreach (InheritTable it in st.InheritTables)
                    {
                        if (it.Prefix != null && it.Prefix.Length > 0)
                        {
                            inheritPrefix.Append(it.Prefix);
                        }
                        if (it.Type == InheritTable.STAGING_TYPE)
                        {
                            StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                            if (ist == null)
                                continue;
                            string iname = getDisplayName(ist);
                            inheritDimTables.Add(iname);
                            inheritMeasureTables.Add(iname + " Fact");
                        }
                        else if (it.Type == InheritTable.MEASURE_TYPE)
                        {
                            inheritMeasureTables.Add(it.Name);
                        }
                        else if (it.Type == InheritTable.DIMENSION_TYPE)
                        {
                            int index = it.Name.IndexOf('.');
                            if (index > 0)
                            {
                                string dim = it.Name.Substring(0, index);
                                string level = it.Name.Substring(index + 1);
                                Hierarchy ih = ma.hmodule.getDimensionHierarchy(dim);
                                if (ih != null)
                                {
                                    Level il = ih.findLevel(level);
                                    if (il != null)
                                        inheritDimTables.Add(getLogicalTableName(dim, il, null));
                                }
                            }
                        }
                    }
                }
                if (numDimColumns > 0 || inheritDimTables.Count > 0)
                {
                    // Map a dimension table
                    dt = new DimensionTable();
                    dt.TableSource = new TableSource();
                    dt.TableSource.Connection = hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : "Default Connection";
                    balist.Add(new BuildAction("Added dimension table: " + dimTableName, MessageType.BASIC));
                    dt.TableName = dimTableName;
                    dt.DimensionName = dimName;
                    dt.TableSource.Connection = findConnection(dt.TableSource.Connection);
                    if (ttlValueMap.ContainsKey(dt.DimensionName + "." + dt.TableName))
                    {
                        dt.TTL = ttlValueMap[dt.DimensionName + "." + dt.TableName];                      
                    }
                    if (inheritDimTables.Count > 0)
                    {
                        string s = "";
                        foreach (string tname in inheritDimTables)
                        {
                            if (tname == "Time Day")
                                dt.TableSource.Schema = "dbo";
                            if (s.Length > 0)
                                s += ",";
                            s += tname;
                        }
                        dt.InheritTable = s;
                        if (inheritPrefix.Length > 0)
                            dt.InheritPrefix = inheritPrefix.ToString();
                    }
                    if (st.LiveAccess)
                    {
                        if (st.TableSource != null && st.TableSource.Tables.Length > 0)
                        {
                            if (st.TableSource.Tables.Length == 1 && st.TableSource.Tables[0].JoinType < 0)
                            {
                                // Opaque view
                                dt.Type = DimensionTable.OPAQUE_VIEW;
                                dt.PhysicalName = Util.generatePhysicalName(st.Name, ma.builderVersion);
                                dt.OpaqueView = st.TableSource.Tables[0].PhysicalName;
                            }
                            else
                            {
                                dt.TableSource.Tables = new TableSource.TableDefinition[st.TableSource.Tables.Length];
                                for (int i = 0; i < st.TableSource.Tables.Length; i++)
                                {
                                    dt.TableSource.Tables[i] = new TableSource.TableDefinition();
                                    dt.TableSource.Tables[i].PhysicalName = st.TableSource.Tables[i].PhysicalName;
                                    dt.TableSource.Tables[i].JoinType = st.TableSource.Tables[i].JoinType;
                                    dt.TableSource.Tables[i].JoinClause = st.TableSource.Tables[i].JoinClause;
                                    dt.TableSource.Tables[i].Schema = st.TableSource.Tables[i].Schema;
                                }
                            }
                            if (inheritDimTables.Count == 0)
                                dt.TableSource.Connection = st.TableSource.Connection;
                        }
                        else
                        {
                            dt.TableSource.Tables = new TableSource.TableDefinition[1];
                            dt.TableSource.Tables[0] = new TableSource.TableDefinition();
                            dt.TableSource.Tables[0].PhysicalName = st.Name;
                        }
                        if (st.UnCached)
                            dt.Cacheable = false;
                    }
                    else
                    {
                        dt.TableSource.Tables = new TableSource.TableDefinition[1];
                        dt.TableSource.Tables[0] = new TableSource.TableDefinition();
                        dt.TableSource.Tables[0].PhysicalName = Util.generatePhysicalName(st.Name, ma.builderVersion);                     
                    }
                    if (st.TableSource != null && st.TableSource.Filters != null)
                        dt.TableSource.Filters = st.TableSource.Filters;
                    if (st.TableSource != null && st.TableSource.Schema != null && st.TableSource.Schema.Length > 0)
                        dt.TableSource.Schema = st.TableSource.Schema;
                    if (importList != null && importList.ContainsKey(st))
                    {
                        dt.TableSource.Schema = importRepositories[st].connection.connectionList[0].Schema;
                    }
                                        
                    if (dt.PhysicalName == null)
                        dt.PhysicalName = dt.TableSource.Tables[0].PhysicalName;
                    
                    dt.AutoGenerated = true;
                    dt.Calculated = true;
                    dt.SnowflakeAllDimensions = true;
                    dimTableNames.Add(st.Name, dt);

                    if (ma.serverPropertiesModule.disableCachingForDimensionsBox.Checked)
                        dt.Cacheable = false;
                    if (!ma.getDimensionList().Contains(dimName))
                        ma.addDimension(dimName);
                    h = ma.hmodule.getHierarchy(dimName);
                    if (h == null)
                    {
                        h = new Hierarchy();
                        h.Name = dimName;
                        h.AutoGenerated = true;
                        h.Children = new Level[1];
                        h.DimensionName = dimName;
                        l = new Level();
                        h.Children[0] = l;
                        l.Name = dimName;
                        l.Cardinality = 1;
                        l.Children = new Level[0];
                        l.ColumnNames = new string[0];
                        l.SharedChildren = new string[0];
                        l.Keys = new LevelKey[1];
                        l.Keys[0] = new LevelKey();
                        if (st.SourceKey != null && st.SourceKey.Length > 0)
                        {
                            l.Keys[0].ColumnNames = new string[st.SourceKey.Length];
                            for (int skIdx = 0; skIdx < st.SourceKey.Length; skIdx++)
                            {
                                l.Keys[0].ColumnNames[skIdx] = st.SourceKey[skIdx];
                            }
                        }
                        else
                        {
                            l.Keys[0].ColumnNames = new string[0];
                        }
                        h.DimensionKeyLevel = l.Name;
                        ma.hmodule.addHierarchy(h);
                        l.AutomaticallyCreatedAsDegenerate = true;
                    }
                    else
                    {
                        l = h.findLevel(dimName);
                    }
                    l = ma.addDimensionTable(dt, l, false, MAX_PHYSICAL_COLUMN_NAME_LENGTH);

                    // Add dimension columns
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null && (new List<string>(sc.TargetTypes).Contains(dimName)))
                        {
                            ma.addDimensionColumnDataRowIfNecessary(dimName, sc.Name, sc.DataType, sc.Width, null);

                            DataRow dcmr = ma.dimensionColumnMappings.NewRow();
                            dcmr["TableName"] = dimTableName;
                            dcmr["ColumnName"] = sc.Name;
                            dcmr["Qualify"] = !st.LiveAccess;
                            if (st.LiveAccess)
                                dcmr["PhysicalName"] = sc.SourceFileColumn;
                            else
                                dcmr["PhysicalName"] = Util.generatePhysicalName(sc.getPhysicalColumnName(), ma.builderVersion) + PHYSICAL_SUFFIX;
                            dcmr["AutoGenerated"] = true;

                            ma.dimensionColumnMappings.Rows.Add(dcmr);
                            balist.Add(new BuildAction("Added column " + sc.Name + " to dimension table " + dimTableName, MessageType.VERBOSE));
                            checkAndAddSecurityFilter(dt, sc, (string)dcmr["PhysicalName"], (string)dcmr["ColumnName"]);
                        }
                    }
                }

                MeasureTable mt = null;
                if (numMeasureColumns > 0 || inheritMeasureTables.Count > 0)
                {
                    // Create the base measure table
                    mt = new MeasureTable();
                    mt.TableName = basename + " Fact";
                    mt.Transactional = true;
                    mt.CurrentSnapshotOnly = false;
                    mt.LoadGroups = null;
                    mt.TableSource = new TableSource();
                    mt.TableSource.Connection = hasLiveAccessLoadGroup(st.LoadGroups, out connection) ? connection : "Default Connection";
                    mt.Cacheable = true;
                    mt.Type = 0;
                    if (ttlValueMap.ContainsKey(mt.TableName))
                    {
                        mt.TTL = ttlValueMap[mt.TableName];
                    }
                    if (inheritMeasureTables.Count > 0)
                    {
                        string s = "";
                        foreach (string tname in inheritMeasureTables)
                        {
                            if (tname == "Time Day")
                                mt.TableSource.Schema = "dbo";
                            if (s.Length > 0)
                                s += ",";
                            s += tname;
                        }
                        mt.InheritTable = s;
                        if (inheritPrefix.Length > 0)
                            mt.InheritPrefix = inheritPrefix.ToString();
                    }
                    if (st.LiveAccess)
                    {
                        if (st.TableSource != null && st.TableSource.Tables.Length > 0)
                        {
                            if (st.TableSource.Tables.Length == 1 && st.TableSource.Tables[0].JoinType < 0)
                            {
                                // Opaque view
                                mt.Type = MeasureTable.OPAQUE_VIEW;
                                mt.PhysicalName = Util.generatePhysicalName(st.Name, ma.builderVersion);
                                mt.OpaqueView = st.TableSource.Tables[0].PhysicalName;
                            }
                            else
                            {
                                mt.TableSource.Tables = new TableSource.TableDefinition[st.TableSource.Tables.Length];
                                for (int i = 0; i < st.TableSource.Tables.Length; i++)
                                {
                                    mt.TableSource.Tables[i] = new TableSource.TableDefinition();
                                    mt.TableSource.Tables[i].PhysicalName = st.TableSource.Tables[i].PhysicalName;
                                    mt.TableSource.Tables[i].JoinType = st.TableSource.Tables[i].JoinType;
                                    mt.TableSource.Tables[i].JoinClause = st.TableSource.Tables[i].JoinClause;
                                    mt.TableSource.Tables[i].Schema = st.TableSource.Tables[i].Schema;
                                }
                            }
                            if (inheritMeasureTables.Count == 0)
                                mt.TableSource.Connection = st.TableSource.Connection;
                        }
                        else
                        {
                            mt.TableSource.Tables = new TableSource.TableDefinition[1];
                            mt.TableSource.Tables[0] = new TableSource.TableDefinition();
                            mt.TableSource.Tables[0].PhysicalName = st.Name;
                        }
                        if (st.UnCached)
                            mt.Cacheable = false;
                    }
                    else
                    {
                    	mt.TableSource.Tables = new TableSource.TableDefinition[1];
                        mt.TableSource.Tables[0] = new TableSource.TableDefinition();
                        mt.TableSource.Tables[0].PhysicalName = Util.generatePhysicalName(st.Name, ma.builderVersion);
                    }
                    if (st.TableSource != null && st.TableSource.Filters != null)
                        mt.TableSource.Filters = st.TableSource.Filters;

                    if (st.TableSource != null && st.TableSource.Schema != null && st.TableSource.Schema.Length > 0)
                        mt.TableSource.Schema = st.TableSource.Schema;
                    if (importList != null && importList.ContainsKey(st))
                    {
                        mt.TableSource.Schema = importRepositories[st].connection.connectionList[0].Schema;
                    }
                    if (mt.PhysicalName == null)
                        mt.PhysicalName = mt.TableSource.Tables[0].PhysicalName;
                    if (sf != null)
                        mt.Cardinality = sf.NumRows;
                    else if (st.Cardinality > 0)
                        mt.Cardinality = st.Cardinality;
                    mt.AutoGenerated = true;
                    mt.Calculated = true;
                    ma.addNewMeasureTable(mt);
                    baseMeasureTableNames.Add(st.Name, mt);
                    List<StagingTable> stlist = null;
                    if (stagingTableMap.ContainsKey(mt))
                        stlist = stagingTableMap[mt];
                    else
                    {
                        stlist = new List<StagingTable>();
                        stagingTableMap.Add(mt, stlist);
                    }
                    stlist.Add(st);
                    Dictionary<string, StagingColumn> columnsForSecurityFilter = new Dictionary<string, StagingColumn>();
                    Dictionary<string, string> logicalColumnsForSecurityFilter = new Dictionary<string, string>();
                    foreach (StagingColumn sc in st.Columns)
                    {

                        if (sc.TargetTypes != null && (new List<string>(sc.TargetTypes).Contains("Measure")))
                        {
                            if (sc.DataType == "Varchar" || sc.DataType == "DateTime" || sc.DataType == "Date")
                            {
                                sc.TargetAggregations = new string[] { "COUNT", "COUNT DISTINCT", "MIN", "MAX" };
                            }
                            else
                            {
                                sc.TargetAggregations = new string[] { "SUM", "AVG", "COUNT", "COUNT DISTINCT", "MIN", "MAX" };
                            }

                            List<string> names = mapMeasure(ma, sc, sc.DataType, sc.Width, null, null, aggregatedMetrics, averageIntegers, st.MeasureNamePrefix);
                            foreach (string name in names)
                            {
                                DataRow dr = null;
                                if (st.LiveAccess)
                                {
                                    dr = ma.addMeasureColumnMapping(name, sc.SourceFileColumn, mt, true, -1, false);
                                }
                                else
                                    dr = ma.addMeasureColumnMapping(name, Util.generatePhysicalName(sc.getPhysicalColumnName(), ma.builderVersion) + PHYSICAL_SUFFIX, mt, true, -1, true);
                                if (dr != null && dr["PhysicalName"] != null)
                                {
                                    string physicalColName = (string)dr["PhysicalName"];
                                    if (!columnsForSecurityFilter.ContainsKey(physicalColName))
                                    {
                                        columnsForSecurityFilter.Add(physicalColName, sc);
                                    }
                                    if (!logicalColumnsForSecurityFilter.ContainsKey(physicalColName))
                                    {
                                        logicalColumnsForSecurityFilter.Add(physicalColName, (string)dr["ColumnName"]);
                                    }

                                }
                            }
                        }
                    }

                    foreach (string key in columnsForSecurityFilter.Keys)
                    {
                        checkAndAddSecurityFilter(mt, columnsForSecurityFilter[key], key, logicalColumnsForSecurityFilter[key]);
                    }

                    // Get a list of all of the analyze by date columns
                    List<StagingColumn> dateColumns = new List<StagingColumn>();
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.GenerateTimeDimension && !sc.DataType.StartsWith("Date ID:"))
                            dateColumns.Add(sc);
                    }
                    mt.Folder = "NO_TIME";

                    // Record time transformed versions to create
                    timeTablesToCreate.Add(mt, dateColumns);
                }

                if (mt != null && dt != null)
                {
                    // Add a redundant join
                    DataRow joinRow = ma.Joins.NewRow();
                    ma.Joins.Rows.Add(joinRow);
                    joinRow["AutoGenerated"] = true;
                    joinRow["table1"] = mt.TableName;
                    joinRow["table2"] = dt.TableName;
                    joinRow["Redundant"] = true;
                    //Also add join condition so that virtual columns can make use of it
                    if ((l != null) && (l.Keys!=null ) && (l.Keys.Length>0) && (l.Keys[0].ColumnNames.Length > 0))
                    {
                        joinRow["JoinCondition"] = "";
                        bool first = true;
                        foreach (string keyCol in l.Keys[0].ColumnNames)
                        {
                            if (!first)
                                joinRow["JoinCondition"] += " AND ";
                            else
                                first = false;
                            string colName = null;
                            int index = keyCol.IndexOf(ma.getPhysicalSuffix());
                            // if it already contains $ as a delimiter
                            if (index >= 0 & index < keyCol.Length - ma.getPhysicalSuffix().Length)
                                colName = keyCol;
                            else
                                colName = ApplicationBuilder.getForeignKeyPhysicalName(ApplicationBuilder.getSQLGenType(ma), null, keyCol, ApplicationBuilder.PHYSICAL_SUFFIX, MAX_PHYSICAL_COLUMN_NAME_LENGTH, ma.builderVersion);
                            joinRow["JoinCondition"] += "\"" + dt.TableName + "\"." + colName + "=\"" + mt.TableName + "\"." + colName;
                        }
                    }
                }
            }
        }

        private void addDiscoveryTimePeriods(Level l, TimeDefinition td, MeasureTable mt, string localETLConnection, StagingTable st, StagingColumn sc)
        {
            foreach (TimePeriod tp in td.Periods)
            {
                DataRow joinRow = ma.Joins.NewRow();
                ma.Joins.Rows.Add(joinRow);
                /*
                 * Since date table math is done in the tables using date id's change join to use date instead and then 
                 * calc the differences. Aggregations still need to refer to aggregation date tables
                 */
                StringBuilder sb = new StringBuilder();
                string mtname = tp.Prefix + " " + mt.TableName + (localETLConnection == null ? "" : " " + localETLConnection);
                string mtcol = "\"" + mtname + "\"." + st.Name + "." + st.getDiscoveryColumnDateID(sc.Name, ma.builderVersion);
                string ttname = null;
                ttname = tp.Prefix + " " + l.Name + (localETLConnection == null ? "" : " " + localETLConnection);
                if (l == dayLevel)
                {
                    sb.Append("\"" + ttname + "\"." + tp.Prefix + "_Date" + PHYSICAL_SUFFIX + "=" + mtcol);
                }
                else
                {
                    string ttcol = Util.generatePhysicalName(l.Keys[0].ColumnNames[0], ma.builderVersion) + PHYSICAL_SUFFIX;
                    sb.Append("dbo.DW_DM_TIME_DAY.Day_ID" + PHYSICAL_SUFFIX + "=" + mtcol +
                        " AND dbo.DW_DM_TIME_DAY." + ttcol + "=\"" + ttname + "\"." + tp.Prefix + "_" + ttcol);
                }
                joinRow["AutoGenerated"] = true;
                joinRow["table1"] = mtname;
                joinRow["table2"] = ttname;
                joinRow["Redundant"] = false;
                joinRow["JoinCondition"] = sb;
            }
        }

        private void addDiscoveryToWarehouseJoins(MainAdminForm ma)
        {
            List<DimensionTable> dtables = new List<DimensionTable>(ma.dimensionTablesList);
            if (importDimRepositories != null)
            {
                foreach (DimensionTable dt in importDimRepositories.Keys)
                    dtables.Add(dt);
            }
            /*
             * Add joins that snowflake from a level to a discovery source
             */
            List<Hierarchy> hlist = new List<Hierarchy>(ma.hmodule.getHierarchies());
            Dictionary<Level, ForeignKey[]> fkeymap = new Dictionary<Level, ForeignKey[]>();
            if (ma.Imports != null && ma.Imports.Length > 0 && importDimRepositories != null && importDimRepositories.Count > 0)
            {
                Hierarchy h = null;
                Level l = null;
                foreach (DimensionTable dt in importDimRepositories.Keys)
                {
                    getDimensionHierarchyAndLevel(dt.DimensionName, dt.Level, out h, out l);
                    if (h == null || l == null)
                        continue;
                    if (h != null && !hlist.Contains(h))
                        hlist.Add(h);
                    foreach (PackageImport pi in ma.Imports)
                    {
                        if (pi.Items == null)
                            continue;
                        foreach (ImportedItem ii in pi.Items)
                        {
                            if (ii.ItemName == dt.TableName && ii.ForeignKeys != null && ii.ForeignKeys.Length > 0)
                            {
                                fkeymap.Add(l, ii.ForeignKeys);
                            }
                        }
                    }
                }
            }
            foreach (Hierarchy h in hlist)
            {
                List<Level> childLevels = new List<Level>();
                h.getChildLevels(childLevels);
                foreach (Level l in childLevels)
                {
                    List<ForeignKey> fklist = new List<ForeignKey>();
                    if (l.ForeignKeys != null && l.ForeignKeys.Length > 0)
                        fklist.AddRange(l.ForeignKeys);
                    if (fkeymap.ContainsKey(l))
                        fklist.AddRange(fkeymap[l]);
                    if (fklist.Count == 0)
                        continue;
                    foreach (DimensionTable dt in dtables)
                    {
                        if (dt.DimensionName == h.DimensionName && dt.Level == l.Name)
                        {
                            foreach(ForeignKey fk in fklist)
                            {
                                string sname = fk.Source;
                                StagingTable st = ma.stagingTableMod.findTable(sname);
                                if (st == null)
                                    continue;
                                string basename = getDisplayName(st);
                                string dimTableName = basename;
                                int index = ma.findDimensionTableIndex(dimTableName);
                                if (index < 0)
                                    continue;
                                DimensionTable snowflakeTable = ma.dimensionTablesList[index];
                                // Snowflake join
                                DataRow joinRow = ma.Joins.NewRow();
                                ma.Joins.Rows.Add(joinRow);
                                joinRow["AutoGenerated"] = true;
                                joinRow["table1"] = dt.TableName;
                                joinRow["table2"] = snowflakeTable.TableName;
                                joinRow["Redundant"] = false;
                                DataRowView[] drv = null;
                                StringBuilder sb = new StringBuilder();
                                if (fk.Condition != null && fk.Condition.Length > 0)
                                {
                                    sb.Append(fk.Condition);
                                }
                                else
                                {
                                    bool first = true;
                                    foreach (string kname in st.SourceKey)
                                    {
                                        if (first)
                                            first = false;
                                        else
                                            sb.Append(" AND ");
                                        string pname = st.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                        if (pname == null && st.InheritTables != null && st.InheritTables.Length > 0)
                                        {
                                            // Look for inherited columns
                                            foreach (InheritTable it in st.InheritTables)
                                            {
                                                if (it.Type == InheritTable.STAGING_TYPE)
                                                {
                                                    StagingTable ist = ma.stagingTableMod.findTable(it.Name);
                                                    if (ist != null)
                                                    {
                                                        pname = ist.getDiscoveryColumnPhysicalName(kname, ma.builderVersion);
                                                        if (pname != null)
                                                            break;
                                                    }
                                                }
                                                else if (it.Type == InheritTable.DIMENSION_TYPE)
                                                {
                                                    int ix = it.Name.IndexOf('.');
                                                    if (ix < 0)
                                                        continue;
                                                    string dim = it.Name.Substring(0, ix);
                                                    string level = it.Name.Substring(ix + 1);
                                                    DimensionTable idt = null;
                                                    foreach (DimensionTable sdt in ma.dimensionTablesList)
                                                    {
                                                        if (sdt.DimensionName == dim && sdt.Level == level)
                                                        {
                                                            idt = sdt;
                                                            break;
                                                        }
                                                    }
                                                    if (idt != null)
                                                    {
                                                        drv = ma.dimensionColumnMappingsView.FindRows(new object[] { kname, idt.TableName });
                                                        if (drv.Length > 0)
                                                        {
                                                            pname = (string)drv[0].Row["PhysicalName"];
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        string dtpname = null;
                                        drv = ma.dimensionColumnMappingsView.FindRows(new object[] { kname, dt.TableName });
                                        if (drv.Length == 0)
                                        {
                                            // check the imported repositories
                                            if (importDimRepositories != null && importDimRepositories.Count > 0 && importDimRepositories.ContainsKey(dt))
                                            {
                                                drv = importDimRepositories[dt].dimensionColumnMappingsView.FindRows(new object[] { kname, dt.TableName });
                                            }

                                            // if its still not found continue;
                                            if(drv.Length == 0)
                                                continue;
                                        }
                                        dtpname = (string) drv[0].Row["PhysicalName"];
                                        sb.Append("\"" + snowflakeTable.TableName + "\"." + pname + "=" +
                                            "\"" + dt.TableName + "\"." + dtpname);
                                    }
                                }
                                // Reverse the join direction
                                if (fk.Type == 2)
                                    joinRow["JoinType"] = "Left Outer";
                                else if (fk.Type == 1)
                                    joinRow["JoinType"] = "Right Outer";
                                else if (fk.Type == 3)
                                    joinRow["JoinType"] = "Full Outer";
                                else
                                    joinRow["JoinType"] = "Inner";
                                joinRow["Invalid"] = fk.Invalid;
                                joinRow["Redundant"] = fk.Redundant;
                                joinRow["JoinCondition"] = sb;
                            }
                        }
                    }
                }
            }
            /*
             * Add joins that allow a level to be used as a dimension against a discovery source as a fact
             */
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                if (st.ForeignKeys == null || st.Disabled)
                    continue;
                foreach (ForeignKey fk in st.ForeignKeys)
                {
                    string s = fk.Source;
                    string[] level = s.Split('.');
                    if (level.Length != 2)
                        continue;
                    string dimension = level[0];
                    string lname = level[1];
                    Hierarchy h = null;
                    Level l = null;
                    getDimensionHierarchyAndLevel(dimension, lname, out h, out l);
                    if (h == null || l == null)
                        continue;

                    foreach (DimensionTable dt in dtables)
                    {
                        if (dt.DimensionName != dimension)
                            continue;
                        if (dt.Level != lname)
                            continue;
                        if (st != null && dt != null && st.TableSource != null && dt.TableSource != null && st.TableSource.Connection != null && dt.TableSource.Connection != null && !st.TableSource.Connection.Equals(dt.TableSource.Connection))
                        {
                            continue;
                        }
                        string basename = getDisplayName(st);
                        DataRow joinRow = ma.Joins.NewRow();
                        ma.Joins.Rows.Add(joinRow);
                        joinRow["AutoGenerated"] = true;
                        joinRow["table1"] = dt.TableName;
                        joinRow["table2"] = basename + " Fact";
                        joinRow["Redundant"] = false;
                        StringBuilder sb = new StringBuilder();
                        bool first = true;
                        foreach (string kname in l.getNaturalKey().ColumnNames)
                        {
                            if (first)
                                first = false;
                            else
                                sb.Append(" AND ");
                            sb.Append("\"" + basename + " Fact" + "\"." + Util.generatePhysicalName(kname, ma.builderVersion) + PHYSICAL_SUFFIX + "=" +
                                "\"" + dt.TableName + "\"." + Util.generatePhysicalName(kname, ma.builderVersion) + PHYSICAL_SUFFIX);
                        }
                        // Reverse the direction since the type is maintained on the foreign key side (and direction is meant to be referenced from primary)
                        if (fk.Type == 2)
                            joinRow["JoinType"] = "Left Outer";
                        else if (fk.Type == 1)
                            joinRow["JoinType"] = "Right Outer";
                        else if (fk.Type == 3)
                            joinRow["JoinType"] = "Full Outer";
                        else
                            joinRow["JoinType"] = "Inner";
                        joinRow["Invalid"] = fk.Invalid;
                        joinRow["Redundant"] = fk.Redundant;
                        joinRow["JoinCondition"] = sb;
                    }
                }
            }
            /*
             * Add joins that allow a discovery source to be used as a dimension against a current fact
             */
            foreach (StagingTable st in ma.stagingTableMod.getStagingTables())
            {
                MeasureTable mt = null;
                ForeignKey[] fkeys = st.FactForeignKeys;
                if (fkeys == null)
                {
                    if (ma.Imports != null && ma.Imports.Length > 0 && importMRepositories != null && importMRepositories.Count > 0)
                    {
                        foreach (PackageImport pi in ma.Imports)
                        {
                            if (pi.Items != null && pi.Items.Length > 0)
                            {
                                foreach (ImportedItem ii in pi.Items)
                                {
                                    if (ii.ForeignKeys != null && containsKey(ii.ForeignKeys, st.Name))
                                    {
                                        foreach (MeasureTable imt in importMRepositories.Keys)
                                        {
                                            if (imt.TableName == ii.ItemName)
                                            {
                                                fkeys = ii.ForeignKeys;
                                                mt = imt;
                                                break;
                                            }
                                        }
                                    }
                                    if (mt != null)
                                        break;
                                }
                                if (mt != null)
                                    break;
                            }
                        }
                    }
                    else
                        continue;
                }
                else
                {
                    MeasureGrain mg = getStagingTableGrain(ma, st);
                    if (mg == null)
                        continue;
                    mt = findMeasureTableAtGrain(ma, mg);
                }
                if (mt == null)
                    continue;
                foreach (ForeignKey fk in fkeys)
                {
                    string s = fk.Source;
                    StagingTable fkst = ma.stagingTableMod.findTable(s);
                    if (fkst == null)
                        continue;
                    string basename = getDisplayName(fkst);
                    DataRow joinRow = ma.Joins.NewRow();
                    ma.Joins.Rows.Add(joinRow);
                    joinRow["AutoGenerated"] = true;
                    joinRow["table1"] = mt.TableName;
                    joinRow["table2"] = basename;
                    joinRow["Redundant"] = false;
                    // Reverse the join direction
                    if (fk.Type == 2)
                        joinRow["JoinType"] = "Left Outer";
                    else if (fk.Type == 1)
                        joinRow["JoinType"] = "Right Outer";
                    else if (fk.Type == 3)
                        joinRow["JoinType"] = "Full Outer";
                    else
                        joinRow["JoinType"] = "Inner";
                    joinRow["Invalid"] = fk.Invalid;
                    joinRow["Redundant"] = fk.Redundant;
                    bool first = true;
                    if (fk.Condition != null && fk.Condition.Length > 0)
                    {
                        joinRow["JoinCondition"] = fk.Condition;
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (string kname in fkst.SourceKey)
                        {
                            
                            string found = null;
                            string pname = Util.generatePhysicalName(kname, ma.builderVersion) + PHYSICAL_SUFFIX;
                            DataRowView[] drvlist = ma.measureColumnMappingsTablesView.FindRows(mt.TableName);
                            if (drvlist == null || drvlist.Length == 0)
                            {
                                // check the imported repositories
                                if (importMRepositories != null && importMRepositories.Count > 0 && importMRepositories.ContainsKey(mt))
                                {
                                    drvlist = importMRepositories[mt].measureColumnMappingsTablesView.FindRows(mt.TableName);
                                }
                            }

                            if (drvlist == null || drvlist.Length == 0)
                            {
                                logDebug(ma, "addDiscoveryToWarehouseJoins: Not able to find the measure columns for measure table " + mt.TableName);
                                continue;
                            }
                            
                            foreach (DataRowView drv in drvlist)
                            {
                                string name = (string)drv.Row["PhysicalName"];
                                if (name == pname || name.EndsWith("$" + pname))
                                {
                                    found = name;
                                    break;
                                }
                            }                            
                            if (found != null)
                            {
                                if (first)
                                    first = false;
                                else
                                    sb.Append(" AND ");

                                sb.Append("\"" + basename + "\"." + pname + "=" +
                                    "\"" + mt.TableName + "\"." + found);
                            }
                        }
                        joinRow["JoinCondition"] = sb;
                    }
                }
            }
        }

        public static void logDebug(MainAdminForm ma, string msg)
        {
            log(ma, "debug", msg, null);
        }

        public static void logError(MainAdminForm ma, string msg, Exception ex)
        {
            log(ma, "error", msg, ex);
        }

        public static void logInfo(MainAdminForm ma, string msg)
        {
            log(ma, "info", msg, null);
        }

        public static void log(MainAdminForm ma, string level, string msg, Exception ex)
        {
            log4net.ILog logger = ma.getLogger();
            if (logger != null)
            {
                if (level == "error")
                {
                    if (ex != null)
                    {
                        logger.Error(msg, ex);
                    }
                    else
                    {
                        logger.Error(msg);
                    }                    
                }
                else if (level == "debug")
                {
                    logger.Debug(msg);
                }
                else
                {
                    logger.Info(msg);
                }
            }
        }

        private bool containsKey(ForeignKey[] keys, string itemName)
        {
            foreach (ForeignKey fk in keys)
            {
                if (fk.Source == itemName)
                    return true;
            }
            return false;
        }

        private void getDimensionHierarchyAndLevel(string dimension, string lname, out Hierarchy h, out Level l)
        {
            h = ma.hmodule.getDimensionHierarchy(dimension);
            l = null;
            if (h == null)
            {
                if (importDimRepositories != null)
                {
                    Dictionary<MainAdminForm, Object> seen = new Dictionary<MainAdminForm, Object>();
                    foreach (MainAdminForm maf in importDimRepositories.Values)
                    {
                        if (seen.ContainsKey(maf))
                            continue;
                        seen.Add(maf, null);
                        h = maf.hmodule.getDimensionHierarchy(dimension);
                        if (h != null)
                        {
                            l = h.findLevel(lname);
                            break;
                        }
                    }
                }

            }
            else
            {
                l = h.findLevel(lname);
            }
        }

        /*
         * Make sure inherited tables inherit the appropriate properties from their ancestors. Need to do this now because inheritance
         * relationships are sometimes setup before the actual base measure or dimension table is built.
         */
        private void updateInheritedTables()
        {
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if (!dt.AutoGenerated || dt.InheritTable == null || dt.InheritTable.Trim().Length == 0)
                    continue;
                string[] inheritedTables = dt.InheritTable.Split(new char[] { ',' });
                foreach (string inheritTableName in inheritedTables)
                {
                    int index = ma.findDimensionTableIndex(inheritTableName);
                    if (index < 0)
                        continue;
                    DimensionTable idt = ma.dimensionTablesList[index];
                    string inheritConn = findConnection(idt.TableSource.Connection);
                    if (inheritConn != dt.TableSource.Connection)
                        dt.TableSource.Connection = inheritConn;
                }
            }
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                if (!mt.AutoGenerated || mt.InheritTable == null || mt.InheritTable.Trim().Length == 0)
                    continue;
                string[] inheritedTables = mt.InheritTable.Split(new char[] { ',' });
                foreach (string inheritTableName in inheritedTables)
                {
                    int index = ma.findMeasureTableIndex(inheritTableName);
                    if (index < 0)
                        continue;
                    MeasureTable imt = ma.measureTablesList[index];
                    string inheritConn = findConnection(imt.TableSource.Connection);
                    if (inheritConn != mt.TableSource.Connection)
                        mt.TableSource.Connection = inheritConn;
                    if (imt.Cardinality > mt.Cardinality)
                        mt.Cardinality = imt.Cardinality + 1;
                }
            }
        }

        
 }

}

  
