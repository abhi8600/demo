using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class VirtualColumns : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Virtual Columns";
        string categoryName = "Business Modeling";
        DataTable bucketTable = new DataTable();
        public VirtualColumnList vclist;

        public VirtualColumns(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
            vclist = new VirtualColumnList(ma.getLogger());
            bucketTable.Columns.Add("Category");
            bucketTable.Columns.Add("Min");
            bucketTable.Columns.Add("Max");
            bucketGridView.DataSource = bucketTable;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (vclist != null)
            {
                r.VirtualColumns = vclist.virtualColumns;
            }
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.VirtualColumns != null)
            {
                vclist.virtualColumns = r.VirtualColumns;
            }
            else
            {
                vclist = new VirtualColumnList(ma.getLogger());
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public Boolean updateGuids()
        {
            return vclist.updateGuids();
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (VirtualColumn vc in vclist.virtualColumns)
            {
                TreeNode tn = new TreeNode(vc.Name);
                tn.ImageIndex = 20;
                tn.SelectedImageIndex = 20;
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            if (vclist.virtualColumns.Length == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = vcMenuStrip;
                this.removeVirtualColumnToolStripMenuItem.Visible = false;
                return false;
            }

            implicitSave();

            selIndex = rootNode.Nodes.IndexOf(node);
            if (selIndex < 0) return false;
            selNode = node;
            ma.mainTree.ContextMenuStrip = vcMenuStrip;
            this.removeVirtualColumnToolStripMenuItem.Visible = true;
            indexChanged();
            return true;
        }

        public void setup()
        {
            columnTableName.Items.Clear();
            pivotDimension.Items.Clear();
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                if (dt.TableName != null && !columnTableName.Items.Contains(dt.TableName))
                    columnTableName.Items.Add(dt.TableName);
            }
            foreach (string s in ma.getDimensionList())
            {
                if (s != null && !pivotDimension.Items.Contains(s))
                    pivotDimension.Items.Add(s);
            }
            foreach (MeasureTable mt in ma.measureTablesList)
            {
                if (mt.TableName != null && !columnTableName.Items.Contains(mt.TableName))
                    columnTableName.Items.Add(mt.TableName);
            }
            columnMeasure.Items.Clear();
            List<string> mlist = ma.getMeasureList(null)[0];
            foreach (string s in mlist)
            {
                if (s != null && !columnMeasure.Items.Contains(s))
                    columnMeasure.Items.Add(s);
            }
            if (virtualColumnFilterBox.Items.Count == 0)
            {
                if (virtualColumnFilterBox.Tag == null) virtualColumnFilterBox.Tag = new ArrayList();
                QueryFilter.sortedListBox(virtualColumnFilterBox, ma.filterMod.filterList, (string[])((ArrayList)virtualColumnFilterBox.Tag).ToArray(typeof(string)));

            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return virtualColumnsTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (VirtualColumn vc in vclist.virtualColumns)
            {
                if (match)
                {
                    if (vc.PivotLevel == find) vc.PivotLevel = replace;
                    if (vc.Measure == find) vc.Measure = replace;
                    if (vc.Name == find) vc.Name = replace;
                }
                else
                {
                    vc.PivotLevel = vc.PivotLevel.Replace(find, replace);
                    vc.Measure = vc.Measure.Replace(find, replace);
                    vc.Name = vc.Name.Replace(find, replace);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (VirtualColumn vc in vclist.virtualColumns)
            {
                if (match)
                {
                    if (vc.PivotLevel == find) results.Add("Virtual Column: " + vc.Name + " - Pivot level");
                    if (vc.Measure == find) results.Add("Virtual Column: " + vc.Name + " - Measure");
                    if (vc.Name == find) results.Add("Virtual Column: " + vc.Name + " - Name");
                }
                else
                {
                    if (vc.PivotLevel.IndexOf(find) >= 0) results.Add("Virtual Column: " + vc.Name + " - Pivot level");
                    if (vc.Measure.IndexOf(find) >= 0) results.Add("Virtual Column: " + vc.Name + " - Measure");
                    if (vc.Name.IndexOf(find) >= 0) results.Add("Virtual Column: " + vc.Name + " - Name");
                }
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updateColumn();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        private void pivotDimension_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (pivotDimension.Text != "")
            {
                pivotLevel.Items.Clear();
                List<string> dcolList = ma.getDimensionColumnList(pivotDimension.Text);
                foreach (string s in dcolList)
                {
                    pivotLevel.Items.Add(s);
                }
                if (!bucketMeasureAttriAvail() && bucketMeasure.Checked==false)
                {
                    if(pivotMeasureAttriAvail())
                        pivotMeasure.Checked = true;
                }
            }
        }

        private VirtualColumn getVirtualColumn()
        {
            VirtualColumn d = new VirtualColumn();
            if (selIndex < 0 || rootNode.Nodes.Count == 0)
                d.Name = "New Virtual Column";
            else
                d.Name = selNode.Text;
            d.Measure = columnMeasure.Text;
            ArrayList vcflist = new ArrayList();
            foreach (string s in virtualColumnFilterBox.CheckedItems) vcflist.Add(s);
            d.Filters = (string[])(vcflist).ToArray(typeof(string));
            d.TableName = columnTableName.Text;
            d.Type = pivotMeasure.Checked ? VirtualColumn.TYPE_PIVOT : bucketMeasure.Checked ? VirtualColumn.TYPE_BUCKETED : 0;
            d.PivotDimension = pivotDimension.Text;
            d.PivotLevel = pivotLevel.Text;
            d.PivotPercent = pivotPercentBox.Checked;
            d.Buckets = new MeasureBucket[bucketTable.Rows.Count];
            int index = 0;
            foreach (DataRow dr in bucketTable.Rows)
            {
                MeasureBucket mb = new MeasureBucket();
                mb.Name = (string)dr["Category"];
                mb.Min = dr["Min"].ToString();
                mb.Max = dr["Max"].ToString();
                d.Buckets[index++] = mb;
            }
            d.measureBucketDefaultName = DefaultCategoryName.Text.ToString();
            d.measureBucketNullName = NullCategoryName.Text.ToString();
            return (d);
        }

        private void addColumn_Click(object sender, System.EventArgs e)
        {
            VirtualColumn d = new VirtualColumn();
            d.Name = "New Virtual Column";
            d.Buckets = new MeasureBucket[0];
            d.Filters = new string[0];
            vclist.Add(d);
            TreeNode tn = new TreeNode(d.Name);
            tn.ImageIndex = 20;
            tn.SelectedImageIndex = 20;
            rootNode.Nodes.Add(tn);
        }

        private void removeColumn_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            vclist.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void updateColumn()
        {
            if (selIndex < 0 || selIndex >= vclist.getCount() || selNode == null) return;
            VirtualColumn d = getVirtualColumn();
            vclist.replaceItemAt(d, selIndex);
            selNode.Text = d.Name;
        }

        private void indexChanged()
        {
            if (selIndex < 0) return;
            VirtualColumn d = vclist.virtualColumns[selIndex];
            columnMeasure.Text = d.Measure;
            columnTableName.Text = d.TableName;
            pivotMeasure.Checked = (d.Type == VirtualColumn.TYPE_PIVOT);
            bucketMeasure.Checked = (d.Type == VirtualColumn.TYPE_BUCKETED);
            pivotDimension.Text = d.PivotDimension;
            pivotLevel.Text = d.PivotLevel;
            pivotPercentBox.Checked = d.PivotPercent;
            DefaultCategoryName.Text = d.measureBucketDefaultName;
            NullCategoryName.Text = d.measureBucketNullName;

            QueryFilter.sortedListBox(virtualColumnFilterBox, ma.filterMod.filterList, d.Filters);

            if (d.Buckets != null)
            {
                bucketTable.Clear();
                foreach (MeasureBucket mb in d.Buckets)
                {
                    DataRow dr = bucketTable.NewRow();
                    dr["Category"] = mb.Name;
                    dr["Min"] = mb.Min;
                    dr["Max"] = mb.Max;
                    bucketTable.Rows.Add(dr);
                }
                //applybucketMeasureChange();
            }
        }

        private void virtualColumnsTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                vclist.virtualColumns[selIndex].Name = e.Label;
            }
        }

        #endregion

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void VirtualColumns_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

        private Boolean bucketMeasureAttriAvail()
        {
            if (DefaultCategoryName.Text.Length == 0 && NullCategoryName.Text.Length == 0 && bucketTable.Rows.Count==0)
                return false;
            return true;
        }

        private Boolean pivotMeasureAttriAvail()
        {
            if (pivotDimension.Text.Length == 0 && pivotLevel.Text.Length == 0 && pivotPercentBox.Checked == false)
                return false;
            return true;
        }

        private void applybucketMeasureChange()
        {
            if(bucketMeasureAttriAvail() && bucketMeasure.Checked==true)
            {
                return;
            }
            else if (!bucketMeasureAttriAvail() && bucketMeasure.Checked == true)
            {
                bucketMeasure.Checked = false;
                if (pivotMeasureAttriAvail())
                    pivotMeasure.Checked = true;
            }
            else if (!pivotMeasureAttriAvail() && pivotMeasure.Checked==false)
            {
                if(bucketMeasureAttriAvail())
                    bucketMeasure.Checked = true;
            }
        }

        private void applyPivotMeasureChange()
        {
            if (pivotMeasureAttriAvail() && pivotMeasure.Checked == true)
            {
                return;
            }
            else if (!pivotMeasureAttriAvail() && pivotMeasure.Checked == true)
            {
                pivotMeasure.Checked = false;
                if (bucketMeasureAttriAvail())
                    bucketMeasure.Checked = true;               
            }   
            else if (!bucketMeasureAttriAvail() && bucketMeasure.Checked==false)
            {
                if (pivotMeasureAttriAvail())
                    pivotMeasure.Checked = true;
            }   
            
        }

        private void pivotLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            applyPivotMeasureChange();
        }

        private void pivotPercentBox_CheckedChanged(object sender, EventArgs e)
        {
            applyPivotMeasureChange();
        }

        private void bucketGridView_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            applybucketMeasureChange();
        }

        private void pivotDimension_TextChanged(object sender, EventArgs e)
        {
            applyPivotMeasureChange();
        }

        private void pivotLevel_TextChanged(object sender, EventArgs e)
        {
            applyPivotMeasureChange();
        }

        private void DefaultCategoryName_TextChanged(object sender, EventArgs e)
        {
            applybucketMeasureChange();
        }

        private void NullCategoryName_TextChanged(object sender, EventArgs e)
        {
            applybucketMeasureChange();
        }

        private void bucketGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            applybucketMeasureChange();
        }
    }
}