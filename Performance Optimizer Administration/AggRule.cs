using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class AggRule : Form
    {
        public AggRule(MainAdminForm ma, string baseRule, string aggRuleString)
        {
            InitializeComponent();
            baseAggRuleBox.Text = baseRule;
            foreach (DimensionTable dt in ma.dimensionTablesList)
            {
                dimViewColumn.Items.Add(dt.DimensionName);
            }
            string[] rules = aggRuleString.Split(',');
            foreach (string s in rules)
            {
                if (s.Length > 0)
                {
                    DataRow dr = aggRuleTable.NewRow();
                    int index = s.IndexOf('=');
                    dr["Dimension"] = s.Substring(0, index);
                    dr["Rule"] = s.Substring(index + 1);
                    aggRuleTable.Rows.Add(dr);
                }
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public string getBaseAggRule()
        {
            return(baseAggRuleBox.Text);
        }

        public string getAggRuleString()
        {
            string result = "";
            foreach(DataRow dr in aggRuleTable.Rows)
            {
                if (result.Length > 0)
                    result += ",";
                result += dr["Dimension"] + "=" + dr["Rule"];
            }
            return (result);
        }
    }
}