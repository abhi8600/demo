using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class DefineKeyForm : Form, ResizeMemoryForm
    {
        MainAdminForm ma;
        string dimension;
        LevelKey key;
        private string windowName = "DefineKeyForm";

        public DefineKeyForm(LevelKey key, MainAdminForm ma, string dimension)
        {
            InitializeComponent();
            this.key = key;
            this.ma = ma;
            this.dimension = dimension;
            resetWindowFromSettings();

            List<string> columns = ma.getDimensionColumnList(dimension);
            columns.Sort();
            keyColumns.BeginUpdate();
            keyColumns.Items.Clear();
            foreach (string s in columns)
            {
                keyColumns.Items.Add(s);
                if (key.ColumnNames != null)
                    if ((new List<string>(key.ColumnNames)).IndexOf(s) >= 0)
                    {
                        keyColumns.SetItemChecked(keyColumns.Items.Count - 1, true);
                    }
            }
            keyColumns.EndUpdate();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            key.ColumnNames = new string[keyColumns.CheckedItems.Count];
            for(int i = 0; i < keyColumns.CheckedItems.Count; i++)
            {
                key.ColumnNames[i] = keyColumns.CheckedItems[i].ToString();                
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public LevelKey LevelKey
        {
            get
            {
                return (key);
            }
            set
            {
            }
        }

        private void DefineKeyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveWindowSettings();
        }

        #region ResizeMemoryForm members

        public void resetWindowFromSettings()
        {
            Size sz = ma.getPopupWindowSize(windowName);
            Point location = ma.getPopupWindowLocation(windowName);
            if (sz != Size.Empty) this.Size = new Size(sz.Width, sz.Height);
            if (location != Point.Empty)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(location.X, location.Y);
            }
        }

        public void saveWindowSettings()
        {
            ma.addPopupWindowStatus(windowName, this.Size, this.StartPosition == FormStartPosition.Manual ? this.Location : Point.Empty, FormWindowState.Normal);
        }
        #endregion ResizeMemoryForm members
    }
}