using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            AssemblyInfo ainfo = new AssemblyInfo();
       
            toolName.Text = ainfo.Product;
            buildVersion.Text = "Release: " + Version.releaseNumber + ", Build: " + Version.buildNumber;
            copyright.Text = ainfo.Copyright;
            repositoryVersion.Text = "Repository Read Version: " + Repository.RepositoryReadVersion + ", Write Version: " + Repository.RepositoryWriteVersion;       
        }
    }
}