using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class StagingFilters : Form, RepositoryModule, RenameNode
    {
        string moduleName = "Staging Filters";
        string categoryName = "Warehouse";
        MainAdminForm ma;
        TreeNode rootNode;
        List<StagingFilter> filters = new List<StagingFilter>();

        public StagingFilters(MainAdminForm ma)
        {
            this.ma = ma;
            InitializeComponent();
        }

        public List<StagingFilter> getStagingFilters()
        {
            return (filters);
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            r.StagingFilters = filters.ToArray();
        }

        public void updateFromRepository(Repository r)
        {
            if (r != null && r.StagingFilters != null)
                filters = new List<StagingFilter>(r.StagingFilters);
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        public void setup()
        {
        }

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            foreach (StagingFilter sf in filters)
            {
                TreeNode tn = new TreeNode(sf.Name);
                tn.ImageIndex = 10;
                tn.SelectedImageIndex = 10;
                tn.Tag = sf;
                rootNode.Nodes.Add(tn);
            }
        }

        TreeNode curNode = null;

        public bool selectedNode(TreeNode node)
        {
            if (curNode != null) implicitSave();
            ma.mainTree.ContextMenuStrip = stagingFilterMenuStrip;
            if (node.Parent.Parent == null)
            {
                stagingFilterPanel.Visible = false;
                curNode = null;
                newStagingFilterToolStripMenuItem.Visible = true;
                removeStagingFilterToolStripMenuItem.Visible = false;
            }
            else
            {
                if (node.Index < filters.Count)
                {
                    curNode = node;
                    StagingFilter sf = (StagingFilter)curNode.Tag;
                    stagingFilterPanel.Visible = true;
                    newStagingFilterToolStripMenuItem.Visible = false;
                    removeStagingFilterToolStripMenuItem.Visible = true;
                    conditionTextArea.Text = sf.Condition;
                }
                else
                    curNode = null;
            }
            return true;
        }

        public void replaceColumn(string find, string replace, bool match)
        {
        }

        public void findColumn(List<string> results, string find, bool match)
        {
        }

        public void implicitSave()
        {
            if (curNode != null)
            {
                StagingFilter sf = (StagingFilter)curNode.Tag;
                sf.Name = curNode.Text;
                sf.Condition = conditionTextArea.Text;
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return stagingFiltersPage;
            }
            set
            {
            }
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion

        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode && curNode != null)
            {
                curNode.Text = e.Label;
                filters[curNode.Index].Name = e.Label;
            }
        }

        #endregion

        private void newStagingFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode tn = new TreeNode("New Filter");
            tn.ImageIndex = 10;
            tn.SelectedImageIndex = 10;
            rootNode.Nodes.Add(tn);
            StagingFilter sf = new StagingFilter("New Filter");
            tn.Tag = sf;
            filters.Add(sf);
        }

        private void dataErrorHandler(object sender, DataGridViewDataErrorEventArgs args)
        {
        }

        private void removeStagingFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curNode != null && curNode.Parent == rootNode)
            {
                int curNodeIndex = rootNode.Nodes.IndexOf(curNode);
                curNode = null;
                if (curNodeIndex < rootNode.Nodes.Count)
                {
                    rootNode.Nodes.RemoveAt(curNodeIndex);
                }
                if (curNodeIndex < filters.Count)
                {
                    filters.RemoveAt(curNodeIndex);
                }
            }
        }
   }
}