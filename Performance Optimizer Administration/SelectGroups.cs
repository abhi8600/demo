using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class SelectGroups : Form
    {
        public List<String> selectedGroups;

        public SelectGroups(List<Group> groupList, List<String> selectedGroups)
        {
            InitializeComponent();
            for(int i = 0; i < groupList.Count; i++)
            {
                Group g = groupList[i];
                listBox.Items.Add(g.Name);
                if (selectedGroups.Contains(g.Name))
                    listBox.SetItemChecked(i,true);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            selectedGroups = new List<string>();
            foreach (String s in listBox.CheckedItems)
            {
                selectedGroups.Add(s);
            }
            this.Close();
        }
    }
}