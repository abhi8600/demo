using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class GetAuthentication : Form
    {
        public GetAuthentication(string username, string password)
        {
            InitializeComponent();
        }

        public string username
        {
            get { return (this.usernameBox.Text); }
            set { this.usernameBox.Text = value; }
        }
        public string password
        {
            get { return (this.passwordBox.Text); }
            set { this.passwordBox.Text = value; }
        }
    }
}