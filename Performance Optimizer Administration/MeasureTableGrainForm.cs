using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class MeasureTableGrainForm : System.Windows.Forms.Form
    {
        public MeasureTableGrainForm(List<MeasureTableGrain> mtgList)
        {
            InitializeComponent();
            InitializeDataTable(mtgList);
        }

        private void InitializeDataTable(List<MeasureTableGrain> mtgList)
        {
            measureTableGrainDataGridView.Rows.Clear();
            foreach (MeasureTableGrain mtg in mtgList)
            {
                measureTableGrainDataGridView.Rows.Add(new String[] { mtg.DimensionName, mtg.DimensionLevel});
            }
        }
    }
}