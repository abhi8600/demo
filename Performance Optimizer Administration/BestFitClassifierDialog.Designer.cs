namespace Performance_Optimizer_Administration
{
    partial class BestFitClassifierDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BestFitClassifierDialog));
            this.modelParamGroupBox = new System.Windows.Forms.GroupBox();
            this.ridor = new System.Windows.Forms.CheckBox();
            this.libsvm1rc = new System.Windows.Forms.CheckBox();
            this.libsvm1r = new System.Windows.Forms.CheckBox();
            this.svmch = new System.Windows.Forms.CheckBox();
            this.svmcl = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.svm4r = new System.Windows.Forms.CheckBox();
            this.svm3r = new System.Windows.Forms.CheckBox();
            this.svm2r = new System.Windows.Forms.CheckBox();
            this.svm4rl = new System.Windows.Forms.CheckBox();
            this.svm3rl = new System.Windows.Forms.CheckBox();
            this.svm2rl = new System.Windows.Forms.CheckBox();
            this.svm1r = new System.Windows.Forms.CheckBox();
            this.rbf = new System.Windows.Forms.CheckBox();
            this.reptree = new System.Windows.Forms.CheckBox();
            this.m5r = new System.Windows.Forms.CheckBox();
            this.maxValues = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.maxSampleValues = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.j48 = new System.Windows.Forms.CheckBox();
            this.logr = new System.Windows.Forms.CheckBox();
            this.label161 = new System.Windows.Forms.Label();
            this.smo3cl = new System.Windows.Forms.CheckBox();
            this.smo2cl = new System.Windows.Forms.CheckBox();
            this.smo1c = new System.Windows.Forms.CheckBox();
            this.label160 = new System.Windows.Forms.Label();
            this.smoch = new System.Windows.Forms.CheckBox();
            this.smocl = new System.Windows.Forms.CheckBox();
            this.mlp = new System.Windows.Forms.CheckBox();
            this.lr = new System.Windows.Forms.CheckBox();
            this.label159 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.smo4r = new System.Windows.Forms.CheckBox();
            this.smo3r = new System.Windows.Forms.CheckBox();
            this.smo2r = new System.Windows.Forms.CheckBox();
            this.smo4rl = new System.Windows.Forms.CheckBox();
            this.smo3rl = new System.Windows.Forms.CheckBox();
            this.smo2rl = new System.Windows.Forms.CheckBox();
            this.smo1r = new System.Windows.Forms.CheckBox();
            this.okButton = new System.Windows.Forms.Button();
            this.defaultButton = new System.Windows.Forms.Button();
            this.modelParamGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // modelParamGroupBox
            // 
            this.modelParamGroupBox.Controls.Add(this.ridor);
            this.modelParamGroupBox.Controls.Add(this.libsvm1rc);
            this.modelParamGroupBox.Controls.Add(this.libsvm1r);
            this.modelParamGroupBox.Controls.Add(this.svmch);
            this.modelParamGroupBox.Controls.Add(this.svmcl);
            this.modelParamGroupBox.Controls.Add(this.label1);
            this.modelParamGroupBox.Controls.Add(this.svm4r);
            this.modelParamGroupBox.Controls.Add(this.svm3r);
            this.modelParamGroupBox.Controls.Add(this.svm2r);
            this.modelParamGroupBox.Controls.Add(this.svm4rl);
            this.modelParamGroupBox.Controls.Add(this.svm3rl);
            this.modelParamGroupBox.Controls.Add(this.svm2rl);
            this.modelParamGroupBox.Controls.Add(this.svm1r);
            this.modelParamGroupBox.Controls.Add(this.rbf);
            this.modelParamGroupBox.Controls.Add(this.reptree);
            this.modelParamGroupBox.Controls.Add(this.m5r);
            this.modelParamGroupBox.Controls.Add(this.maxValues);
            this.modelParamGroupBox.Controls.Add(this.label163);
            this.modelParamGroupBox.Controls.Add(this.maxSampleValues);
            this.modelParamGroupBox.Controls.Add(this.label162);
            this.modelParamGroupBox.Controls.Add(this.j48);
            this.modelParamGroupBox.Controls.Add(this.logr);
            this.modelParamGroupBox.Controls.Add(this.label161);
            this.modelParamGroupBox.Controls.Add(this.smo3cl);
            this.modelParamGroupBox.Controls.Add(this.smo2cl);
            this.modelParamGroupBox.Controls.Add(this.smo1c);
            this.modelParamGroupBox.Controls.Add(this.label160);
            this.modelParamGroupBox.Controls.Add(this.smoch);
            this.modelParamGroupBox.Controls.Add(this.smocl);
            this.modelParamGroupBox.Controls.Add(this.mlp);
            this.modelParamGroupBox.Controls.Add(this.lr);
            this.modelParamGroupBox.Controls.Add(this.label159);
            this.modelParamGroupBox.Controls.Add(this.label158);
            this.modelParamGroupBox.Controls.Add(this.smo4r);
            this.modelParamGroupBox.Controls.Add(this.smo3r);
            this.modelParamGroupBox.Controls.Add(this.smo2r);
            this.modelParamGroupBox.Controls.Add(this.smo4rl);
            this.modelParamGroupBox.Controls.Add(this.smo3rl);
            this.modelParamGroupBox.Controls.Add(this.smo2rl);
            this.modelParamGroupBox.Controls.Add(this.smo1r);
            this.modelParamGroupBox.Location = new System.Drawing.Point(12, 12);
            this.modelParamGroupBox.Name = "modelParamGroupBox";
            this.modelParamGroupBox.Size = new System.Drawing.Size(456, 458);
            this.modelParamGroupBox.TabIndex = 20;
            this.modelParamGroupBox.TabStop = false;
            this.modelParamGroupBox.Text = "Best Fit Classifier Settings";
            // 
            // ridor
            // 
            this.ridor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ridor.Location = new System.Drawing.Point(264, 138);
            this.ridor.Name = "ridor";
            this.ridor.Size = new System.Drawing.Size(190, 16);
            this.ridor.TabIndex = 39;
            this.ridor.Text = "RIpple DOwn Rules (Explanable)";
            // 
            // libsvm1rc
            // 
            this.libsvm1rc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.libsvm1rc.Location = new System.Drawing.Point(16, 436);
            this.libsvm1rc.Name = "libsvm1rc";
            this.libsvm1rc.Size = new System.Drawing.Size(250, 16);
            this.libsvm1rc.TabIndex = 38;
            this.libsvm1rc.Text = "LibSVM Regression (nu-SVR with defaults)";
            // 
            // libsvm1r
            // 
            this.libsvm1r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.libsvm1r.Location = new System.Drawing.Point(264, 160);
            this.libsvm1r.Name = "libsvm1r";
            this.libsvm1r.Size = new System.Drawing.Size(180, 16);
            this.libsvm1r.TabIndex = 37;
            this.libsvm1r.Text = "LibSVM (C-SVC with defaults)";
            this.libsvm1r.CheckedChanged += new System.EventHandler(this.libsvm1r_CheckedChanged);
            // 
            // svmch
            // 
            this.svmch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svmch.Location = new System.Drawing.Point(24, 422);
            this.svmch.Name = "svmch";
            this.svmch.Size = new System.Drawing.Size(184, 16);
            this.svmch.TabIndex = 36;
            this.svmch.Text = "High Complexity Boosting";
            this.svmch.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // svmcl
            // 
            this.svmcl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svmcl.Location = new System.Drawing.Point(24, 405);
            this.svmcl.Name = "svmcl";
            this.svmcl.Size = new System.Drawing.Size(176, 16);
            this.svmcl.TabIndex = 35;
            this.svmcl.Text = "Low Complexity Boosting";
            this.svmcl.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 34;
            this.label1.Text = "SVM Regression";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // svm4r
            // 
            this.svm4r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm4r.Location = new System.Drawing.Point(24, 388);
            this.svm4r.Name = "svm4r";
            this.svm4r.Size = new System.Drawing.Size(184, 16);
            this.svm4r.TabIndex = 33;
            this.svm4r.Text = "Fourth Order (No Lower Terms)";
            this.svm4r.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // svm3r
            // 
            this.svm3r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm3r.Location = new System.Drawing.Point(24, 372);
            this.svm3r.Name = "svm3r";
            this.svm3r.Size = new System.Drawing.Size(176, 16);
            this.svm3r.TabIndex = 32;
            this.svm3r.Text = "Third Order (No Lower Terms)";
            this.svm3r.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // svm2r
            // 
            this.svm2r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm2r.Location = new System.Drawing.Point(24, 356);
            this.svm2r.Name = "svm2r";
            this.svm2r.Size = new System.Drawing.Size(192, 16);
            this.svm2r.TabIndex = 31;
            this.svm2r.Text = "Second Order (No Lower Terms)";
            this.svm2r.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // svm4rl
            // 
            this.svm4rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm4rl.Location = new System.Drawing.Point(24, 340);
            this.svm4rl.Name = "svm4rl";
            this.svm4rl.Size = new System.Drawing.Size(176, 16);
            this.svm4rl.TabIndex = 30;
            this.svm4rl.Text = "Fourth Order (Lower Terms)";
            this.svm4rl.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // svm3rl
            // 
            this.svm3rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm3rl.Location = new System.Drawing.Point(24, 324);
            this.svm3rl.Name = "svm3rl";
            this.svm3rl.Size = new System.Drawing.Size(176, 16);
            this.svm3rl.TabIndex = 29;
            this.svm3rl.Text = "Third Order (Lower Terms)";
            this.svm3rl.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // svm2rl
            // 
            this.svm2rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm2rl.Location = new System.Drawing.Point(24, 308);
            this.svm2rl.Name = "svm2rl";
            this.svm2rl.Size = new System.Drawing.Size(176, 16);
            this.svm2rl.TabIndex = 28;
            this.svm2rl.Text = "Second Order (Lower Terms)";
            this.svm2rl.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // svm1r
            // 
            this.svm1r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.svm1r.Location = new System.Drawing.Point(24, 292);
            this.svm1r.Name = "svm1r";
            this.svm1r.Size = new System.Drawing.Size(80, 16);
            this.svm1r.TabIndex = 27;
            this.svm1r.Text = "First Order";
            this.svm1r.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // rbf
            // 
            this.rbf.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbf.Location = new System.Drawing.Point(24, 160);
            this.rbf.Name = "rbf";
            this.rbf.Size = new System.Drawing.Size(184, 16);
            this.rbf.TabIndex = 26;
            this.rbf.Text = "RBF Kernel";
            // 
            // reptree
            // 
            this.reptree.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.reptree.Location = new System.Drawing.Point(16, 257);
            this.reptree.Name = "reptree";
            this.reptree.Size = new System.Drawing.Size(173, 16);
            this.reptree.TabIndex = 25;
            this.reptree.Text = "Reduced Error Pruning Tree";
            // 
            // m5r
            // 
            this.m5r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.m5r.Location = new System.Drawing.Point(16, 240);
            this.m5r.Name = "m5r";
            this.m5r.Size = new System.Drawing.Size(173, 16);
            this.m5r.TabIndex = 24;
            this.m5r.Text = "M5 Rules (Regression Tree)";
            // 
            // maxValues
            // 
            this.maxValues.Location = new System.Drawing.Point(376, 253);
            this.maxValues.Name = "maxValues";
            this.maxValues.Size = new System.Drawing.Size(56, 20);
            this.maxValues.TabIndex = 23;
            this.maxValues.Text = "175000";
            // 
            // label163
            // 
            this.label163.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.Location = new System.Drawing.Point(224, 253);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(152, 24);
            this.label163.TabIndex = 22;
            this.label163.Text = "Max Instance Values";
            this.label163.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // maxSampleValues
            // 
            this.maxSampleValues.Location = new System.Drawing.Point(376, 229);
            this.maxSampleValues.Name = "maxSampleValues";
            this.maxSampleValues.Size = new System.Drawing.Size(56, 20);
            this.maxSampleValues.TabIndex = 21;
            this.maxSampleValues.Text = "85000";
            // 
            // label162
            // 
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.Location = new System.Drawing.Point(224, 229);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(152, 24);
            this.label162.TabIndex = 20;
            this.label162.Text = "Max Sample Instance Values";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // j48
            // 
            this.j48.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.j48.Location = new System.Drawing.Point(264, 116);
            this.j48.Name = "j48";
            this.j48.Size = new System.Drawing.Size(180, 16);
            this.j48.TabIndex = 19;
            this.j48.Text = "J48 Decision Tree (Explanable)";
            // 
            // logr
            // 
            this.logr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.logr.Location = new System.Drawing.Point(264, 98);
            this.logr.Name = "logr";
            this.logr.Size = new System.Drawing.Size(180, 16);
            this.logr.TabIndex = 18;
            this.logr.Text = "Logistic Regression (Explanable)";
            // 
            // label161
            // 
            this.label161.Location = new System.Drawing.Point(264, 32);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(100, 16);
            this.label161.TabIndex = 17;
            this.label161.Text = "SMO Classification";
            // 
            // smo3cl
            // 
            this.smo3cl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo3cl.Location = new System.Drawing.Point(272, 80);
            this.smo3cl.Name = "smo3cl";
            this.smo3cl.Size = new System.Drawing.Size(160, 16);
            this.smo3cl.TabIndex = 16;
            this.smo3cl.Text = "Third Order (Lower Terms)";
            // 
            // smo2cl
            // 
            this.smo2cl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo2cl.Location = new System.Drawing.Point(272, 64);
            this.smo2cl.Name = "smo2cl";
            this.smo2cl.Size = new System.Drawing.Size(176, 16);
            this.smo2cl.TabIndex = 15;
            this.smo2cl.Text = "Second Order (Lower Terms)";
            // 
            // smo1c
            // 
            this.smo1c.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo1c.Location = new System.Drawing.Point(272, 48);
            this.smo1c.Name = "smo1c";
            this.smo1c.Size = new System.Drawing.Size(80, 16);
            this.smo1c.TabIndex = 14;
            this.smo1c.Text = "First Order";
            // 
            // label160
            // 
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.Location = new System.Drawing.Point(256, 16);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(100, 16);
            this.label160.TabIndex = 13;
            this.label160.Text = "Classification";
            // 
            // smoch
            // 
            this.smoch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smoch.Location = new System.Drawing.Point(24, 191);
            this.smoch.Name = "smoch";
            this.smoch.Size = new System.Drawing.Size(184, 16);
            this.smoch.TabIndex = 12;
            this.smoch.Text = "High Complexity Boosting";
            // 
            // smocl
            // 
            this.smocl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smocl.Location = new System.Drawing.Point(24, 175);
            this.smocl.Name = "smocl";
            this.smocl.Size = new System.Drawing.Size(176, 16);
            this.smocl.TabIndex = 11;
            this.smocl.Text = "Low Complexity Boosting";
            // 
            // mlp
            // 
            this.mlp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mlp.Location = new System.Drawing.Point(16, 223);
            this.mlp.Name = "mlp";
            this.mlp.Size = new System.Drawing.Size(136, 16);
            this.mlp.TabIndex = 10;
            this.mlp.Text = "Multilayer Perceptron";
            // 
            // lr
            // 
            this.lr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lr.Location = new System.Drawing.Point(16, 207);
            this.lr.Name = "lr";
            this.lr.Size = new System.Drawing.Size(120, 16);
            this.lr.TabIndex = 9;
            this.lr.Text = "Linear Regression";
            // 
            // label159
            // 
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(8, 16);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(100, 16);
            this.label159.TabIndex = 8;
            this.label159.Text = "Regression";
            // 
            // label158
            // 
            this.label158.Location = new System.Drawing.Point(16, 32);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(100, 16);
            this.label158.TabIndex = 7;
            this.label158.Text = "SMO Regression";
            // 
            // smo4r
            // 
            this.smo4r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo4r.Location = new System.Drawing.Point(24, 144);
            this.smo4r.Name = "smo4r";
            this.smo4r.Size = new System.Drawing.Size(184, 16);
            this.smo4r.TabIndex = 6;
            this.smo4r.Text = "Fourth Order (No Lower Terms)";
            // 
            // smo3r
            // 
            this.smo3r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo3r.Location = new System.Drawing.Point(24, 128);
            this.smo3r.Name = "smo3r";
            this.smo3r.Size = new System.Drawing.Size(176, 16);
            this.smo3r.TabIndex = 5;
            this.smo3r.Text = "Third Order (No Lower Terms)";
            // 
            // smo2r
            // 
            this.smo2r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo2r.Location = new System.Drawing.Point(24, 112);
            this.smo2r.Name = "smo2r";
            this.smo2r.Size = new System.Drawing.Size(192, 16);
            this.smo2r.TabIndex = 4;
            this.smo2r.Text = "Second Order (No Lower Terms)";
            // 
            // smo4rl
            // 
            this.smo4rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo4rl.Location = new System.Drawing.Point(24, 96);
            this.smo4rl.Name = "smo4rl";
            this.smo4rl.Size = new System.Drawing.Size(176, 16);
            this.smo4rl.TabIndex = 3;
            this.smo4rl.Text = "Fourth Order (Lower Terms)";
            // 
            // smo3rl
            // 
            this.smo3rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo3rl.Location = new System.Drawing.Point(24, 80);
            this.smo3rl.Name = "smo3rl";
            this.smo3rl.Size = new System.Drawing.Size(176, 16);
            this.smo3rl.TabIndex = 2;
            this.smo3rl.Text = "Third Order (Lower Terms)";
            // 
            // smo2rl
            // 
            this.smo2rl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo2rl.Location = new System.Drawing.Point(24, 64);
            this.smo2rl.Name = "smo2rl";
            this.smo2rl.Size = new System.Drawing.Size(176, 16);
            this.smo2rl.TabIndex = 1;
            this.smo2rl.Text = "Second Order (Lower Terms)";
            // 
            // smo1r
            // 
            this.smo1r.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.smo1r.Location = new System.Drawing.Point(24, 48);
            this.smo1r.Name = "smo1r";
            this.smo1r.Size = new System.Drawing.Size(80, 16);
            this.smo1r.TabIndex = 0;
            this.smo1r.Text = "First Order";
            // 
            // okButton
            // 
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Location = new System.Drawing.Point(483, 12);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 21;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // defaultButton
            // 
            this.defaultButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.defaultButton.Location = new System.Drawing.Point(483, 41);
            this.defaultButton.Name = "defaultButton";
            this.defaultButton.Size = new System.Drawing.Size(75, 23);
            this.defaultButton.TabIndex = 22;
            this.defaultButton.Text = "Default";
            this.defaultButton.UseVisualStyleBackColor = true;
            this.defaultButton.Click += new System.EventHandler(this.defaultButton_Click);
            // 
            // BestFitClassifierDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 485);
            this.ControlBox = false;
            this.Controls.Add(this.defaultButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.modelParamGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BestFitClassifierDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Classifier Model Parameters";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BestFitClassifierDialog_FormClosing);
            this.modelParamGroupBox.ResumeLayout(false);
            this.modelParamGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox modelParamGroupBox;
        private System.Windows.Forms.TextBox maxValues;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox maxSampleValues;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.CheckBox j48;
        private System.Windows.Forms.CheckBox logr;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.CheckBox smo3cl;
        private System.Windows.Forms.CheckBox smo2cl;
        private System.Windows.Forms.CheckBox smo1c;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.CheckBox smoch;
        private System.Windows.Forms.CheckBox smocl;
        private System.Windows.Forms.CheckBox mlp;
        private System.Windows.Forms.CheckBox lr;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.CheckBox smo4r;
        private System.Windows.Forms.CheckBox smo3r;
        private System.Windows.Forms.CheckBox smo2r;
        private System.Windows.Forms.CheckBox smo4rl;
        private System.Windows.Forms.CheckBox smo3rl;
        private System.Windows.Forms.CheckBox smo2rl;
        private System.Windows.Forms.CheckBox smo1r;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button defaultButton;
        private System.Windows.Forms.CheckBox reptree;
        private System.Windows.Forms.CheckBox m5r;
        private System.Windows.Forms.CheckBox rbf;
        private System.Windows.Forms.CheckBox svmch;
        private System.Windows.Forms.CheckBox svmcl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox svm4r;
        private System.Windows.Forms.CheckBox svm3r;
        private System.Windows.Forms.CheckBox svm2r;
        private System.Windows.Forms.CheckBox svm4rl;
        private System.Windows.Forms.CheckBox svm3rl;
        private System.Windows.Forms.CheckBox svm2rl;
        private System.Windows.Forms.CheckBox svm1r;
        private System.Windows.Forms.CheckBox libsvm1r;
        private System.Windows.Forms.CheckBox libsvm1rc;
        private System.Windows.Forms.CheckBox ridor;
    }
}