using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class Filters : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Filters";
        string categoryName = "Business Modeling";
        public List<QueryFilter> filterList;

        public Filters(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (filterList != null)
            {
                r.Filters = (QueryFilter[])filterList.ToArray();
            }
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            if (r != null && r.Filters != null)
            {
                filterList = new List<QueryFilter>(r.Filters);
            }
            else
            {
                filterList = new List<QueryFilter>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (filterList != null)
                foreach (QueryFilter qf in filterList)
                {
                    TreeNode tn = new TreeNode(qf.Name);
                    tn.ImageIndex = 10;
                    tn.SelectedImageIndex = 10;
                    rootNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;
        int selIndex;

        public void setupLists()
        {
            updateFilterListView(this.leftFilterListView);
            updateFilterListView(this.rightFilterListView);
        }

        public void setDialog()
        {
            okButton.Visible = true;
            cancelButton.Visible = true;
        }

        public bool selectedNode(TreeNode node)
        {
            implicitSave();
            if (filterList.Count == 0)
            {
                ma.mainTree.ContextMenuStrip = filterMenuStrip;
                removeFilterToolStripMenuItem.Visible = false;
                return false;
            }
            selIndex = rootNode.Nodes.IndexOf(node);
            setupLists();
            ma.mainTree.ContextMenuStrip = filterMenuStrip;
            if (selIndex >= 0)
            {
                selNode = node;
                selectFilterForEdit();
                removeFilterToolStripMenuItem.Visible = true;
            }
            else
            {
                removeFilterToolStripMenuItem.Visible = false;
            }
            return true;
        }

        public void setup()
        {
            TreeNode lastNode = null;
            if (ma.lastNode != null)
            {
                lastNode = ma.lastNode;
                if (lastNode == rootNode)
                {
                    lastNode = null;
                }
            }
            if (lastNode == null || ma.lastPNode.Text != this.moduleName)
            {
                setupDimensionFilters();
                List<string>[] measureLists = ma.getMeasureList(null);
                measureLists[0].Sort();
                foreach (String s in measureLists[0])
                {
                    ListViewItem lvi = new ListViewItem(
                        new string[] { s, "Measure" });
                    availFilterColumns.Items.Add(lvi);
                }
            }
        }

        public void setupDimensionFilters()
        {
            List<string> dlist = ma.getDimensionList();
            dlist.Sort();
            availFilterColumns.Items.Clear();
            foreach (string d in dlist)
            {
                List<string> clist = ma.getDimensionColumnList(d);
                clist.Sort();
                foreach (string c in clist)
                {
                    ListViewItem lvi = new ListViewItem(
                        new string[] { d + ":" + c, "Attribute" });
                    availFilterColumns.Items.Add(lvi);
                }
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return filterTabPage;
            }
            set
            {
            }
        }

        private void replaceFilterColumn(QueryFilter qf, string find, string replace, bool match)
        {
            if (qf.Type == QueryFilter.TYPE_LOGICAL_OP)
            {
                replaceFilterColumn(qf.QF1, find, replace, match);
                replaceFilterColumn(qf.QF2, find, replace, match);
            }
            else
                if (qf.Type == QueryFilter.TYPE_PREDICATE)
                {
                    if (match)
                    {
                        if (qf.Column == find) qf.Column = replace;
                    }
                    else
                    {
                        qf.Column = qf.Column.Replace(find,replace);
                    }
                }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (QueryFilter qf in filterList)
            {
                replaceFilterColumn(qf, find, replace, match);
            }
        }

        private void findFilterColumn(string name, QueryFilter qf, List<string> results, string find, bool match)
        {
            if (qf.Type == QueryFilter.TYPE_LOGICAL_OP)
            {
                findFilterColumn(name, qf.QF1, results, find, match);
                findFilterColumn(name, qf.QF2, results, find, match);
            }
            else
            {
                if (match)
                {
                    if (qf.Type == QueryFilter.TYPE_PREDICATE && qf.Column == find) results.Add("Filter: " + name);
                }
                else
                {
                    if (qf.Type == QueryFilter.TYPE_PREDICATE && qf.Column.IndexOf(find) >= 0) results.Add("Filter: " + name);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (QueryFilter qf in filterList)
            {
                findFilterColumn(qf.Name,qf, results,find, match);
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
           if (!turnOffimplicitSave) updateFilter();
        }

        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        public QueryFilter getQueryFilter()
        {
            QueryFilter f = new QueryFilter();
            f.Operator = operatorBox.Text;
            if (columnButton.Checked)
            {
                f.Type = QueryFilter.TYPE_PREDICATE;
                if (filterColumn.Text.IndexOf(':') > 0)
                {
                    f.ColumnType = QueryFilter.TYPE_DIMENSION_COLUMN;
                    string s = filterColumn.Text;
                    int p = s.IndexOf(':');
                    f.Dimension = s.Substring(0, p);
                    f.Column = s.Substring(p + 1);
                }
                else
                {
                    f.ColumnType = QueryFilter.TYPE_MEASURE;
                    f.Column = filterColumn.Text;
                }
                f.Operand = operandBox.Text;
            }
            else
            {
                f.Type = QueryFilter.TYPE_LOGICAL_OP;
                f.QF1 = (QueryFilter)leftFilterPredicate.Tag;
                f.QF2 = (QueryFilter)rightFilterPredicate.Tag;
            }
            return (f);
        }

        private QueryFilter getFilter()
        {
            QueryFilter f = getQueryFilter();
            f.Name = selNode.Text;
            return (f);
        }

        private void addFilter_Click(object sender, System.EventArgs e)
        {
            QueryFilter f = new QueryFilter();
            f.Name = "New Filter";
            if (filterList == null)
            {
                filterList = new List<QueryFilter>();
            }
            filterList.Add(f);
            TreeNode tn = new TreeNode(f.Name);
            tn.ImageIndex = 10;
            tn.SelectedImageIndex = 10;
            rootNode.Nodes.Add(tn);
        }

        private void updateFilter_Click(object sender, System.EventArgs e)
        {
            updateFilter();
        }
        private void updateFilter()
        {
            if (selIndex < 0 || selIndex >= filterList.Count || selNode == null || selNode == rootNode) return;
            QueryFilter f = getFilter();
            filterList[selIndex] = f;
            selNode.Text = f.Name;
        }

        private void removeFilter_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0) return;
            TreeNode n = selNode;
            if (n == null || n.Parent != rootNode) return;
            turnOffimplicitSave = true;
            filterList.RemoveAt(selIndex);
            n.Remove();
            turnOffimplicitSave = false;
        }

        private void editFilter_Click(object sender, System.EventArgs e)
        {
            selectFilterForEdit();
        }

        public void showFilter(QueryFilter f)
        {
            if (f.Type == QueryFilter.TYPE_PREDICATE)
            {
                operandBox.Text = f.Operand;
                if (f.ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN)
                {
                    if (f.Column != null)
                        filterColumn.Text = f.Dimension + ":" + f.Column;
                    else
                        filterColumn.Text = "";
                }
                else if (f.ColumnType == QueryFilter.TYPE_MEASURE)
                {
                    filterColumn.Text = f.Column;
                }
                leftFilterPredicate.Text = "";
                rightFilterPredicate.Text = "";
                leftFilterListView.Enabled = false;
                rightFilterListView.Enabled = false;
                columnButton.Checked = true;
                operatorBox.Text = f.Operator;
            }
            else if (f.Type == QueryFilter.TYPE_LOGICAL_OP)
            {
                leftFilterPredicate.Text = f.QF1 == null ? "" : f.QF1.ToString();
                leftFilterPredicate.Tag = f.QF1;
                rightFilterPredicate.Text = f.QF2 == null ? "" : f.QF2.ToString();
                rightFilterPredicate.Tag = f.QF2;
                filterColumn.Text = "";
                leftFilterButton.Checked = true;
                operandBox.Text = "";
                operatorBox.Text = f.Operator;
            }
        }

        private void selectFilterForEdit()
        {
            if (selIndex < 0 || selIndex >= filterList.Count) return;
            QueryFilter f = (QueryFilter)filterList[selIndex];
            showFilter(f);
        }

        class specialListViewItem : ListViewItem
        {
            public QueryFilter itemFilter;
            public specialListViewItem(string txt, QueryFilter f)
            {
                this.Text = txt;
                this.itemFilter = f;
            }
        }
        private void updateFilterListView(ListView lv)
        {
            QueryFilter currentFilter = (selIndex < 0 || selIndex >= filterList.Count) ? null : filterList[selIndex];
            lv.Items.Clear();
            foreach (QueryFilter f in filterList)
            {
                if (selIndex < 0 || selNode == rootNode || (selIndex >= 0 && currentFilter.Name != f.Name))
                    lv.Items.Add(new specialListViewItem(f.Name, f));
            }
        }

        private void columnButton_CheckedChanged(object sender, System.EventArgs e)
        {
            operandBox.Enabled = true;
            selectFilterColumn.Enabled = true;
            selectRightPredicate.Enabled = false;
            selectLeftPredicate.Enabled = false;
            operatorBox.Items.Clear();
            operatorBox.Items.AddRange(new String[] { "=", ">", "<", "<>", ">=", "<=", "LIKE", "NOT LIKE", "IN", "NOT IN" });
            availFilterColumns.Enabled = true;
            leftFilterListView.Enabled = false;
            rightFilterListView.Enabled = false;
        }

        private void leftfilterButton_CheckedChanged(object sender, System.EventArgs e)
        {
            operandBox.Enabled = false;
            selectFilterColumn.Enabled = false;
            selectRightPredicate.Enabled = true;
            selectLeftPredicate.Enabled = true;
            operatorBox.Items.Clear();
            operatorBox.Items.AddRange(new String[] { "AND", "OR" });
            availFilterColumns.Enabled = false;
            leftFilterListView.Enabled = true;
            rightFilterListView.Enabled = true;
        }

        private void selectFilterColumn_Click(object sender, System.EventArgs e)
        {
            if (availFilterColumns.SelectedItems.Count == 0) return;
            ListViewItem lvi = availFilterColumns.SelectedItems[0];
            filterColumn.Text = lvi.SubItems[0].Text;
        }

        private void selectLeftPredicate_Click(object sender, System.EventArgs e)
        {
            if (leftFilterListView.SelectedItems.Count == 0) return;
            specialListViewItem lvi = (specialListViewItem)leftFilterListView.SelectedItems[0];
            leftFilterPredicate.Text = lvi.itemFilter.ToString();
            leftFilterPredicate.Tag = lvi.itemFilter;
        }

        private void selectRightPredicate_Click(object sender, System.EventArgs e)
        {
            if (rightFilterListView.SelectedItems.Count == 0) return;
            specialListViewItem lvi = (specialListViewItem)rightFilterListView.SelectedItems[0];
            rightFilterPredicate.Text = lvi.itemFilter.ToString();
            rightFilterPredicate.Tag = lvi.itemFilter;
        }

        private void filterTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }


        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                selNode.Text = e.Label;
                filterList[selIndex].Name = e.Label;
            }
        }

        #endregion

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}