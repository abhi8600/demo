namespace Performance_Optimizer_Administration
{
    partial class UsersAndGroups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.usersAndGroupsTabPage = new System.Windows.Forms.TabPage();
            this.usersGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBoxUsers = new System.Windows.Forms.GroupBox();
            this.userPasswordBox = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.fullNameBox = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.groupsGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBoxGroups = new System.Windows.Forms.GroupBox();
            this.internalGroup = new System.Windows.Forms.CheckBox();
            this.setPriviliges = new System.Windows.Forms.Button();
            this.label143 = new System.Windows.Forms.Label();
            this.groupUserList = new System.Windows.Forms.ListView();
            this.columnHeader46 = new System.Windows.Forms.ColumnHeader();
            this.groupFilterList = new System.Windows.Forms.Label();
            this.groupFilterListBox = new System.Windows.Forms.CheckedListBox();
            this.groupsMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl.SuspendLayout();
            this.usersAndGroupsTabPage.SuspendLayout();
            this.usersGroupBox.SuspendLayout();
            this.groupBoxUsers.SuspendLayout();
            this.groupsGroupBox.SuspendLayout();
            this.groupBoxGroups.SuspendLayout();
            this.groupsMenuStrip.SuspendLayout();
            this.usersMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.usersAndGroupsTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // usersAndGroupsTabPage
            // 
            this.usersAndGroupsTabPage.AutoScroll = true;
            this.usersAndGroupsTabPage.Controls.Add(this.usersGroupBox);
            this.usersAndGroupsTabPage.Controls.Add(this.groupsGroupBox);
            this.usersAndGroupsTabPage.Location = new System.Drawing.Point(4, 22);
            this.usersAndGroupsTabPage.Name = "usersAndGroupsTabPage";
            this.usersAndGroupsTabPage.Size = new System.Drawing.Size(844, 707);
            this.usersAndGroupsTabPage.TabIndex = 19;
            this.usersAndGroupsTabPage.Text = "Users and Groups";
            this.usersAndGroupsTabPage.UseVisualStyleBackColor = true;
            // 
            // usersGroupBox
            // 
            this.usersGroupBox.Controls.Add(this.groupBoxUsers);
            this.usersGroupBox.Location = new System.Drawing.Point(0, 0);
            this.usersGroupBox.Name = "usersGroupBox";
            this.usersGroupBox.Size = new System.Drawing.Size(841, 312);
            this.usersGroupBox.TabIndex = 6;
            this.usersGroupBox.TabStop = false;
            this.usersGroupBox.Text = "Manage Users (deprecated)";
            // 
            // groupBoxUsers
            // 
            this.groupBoxUsers.Controls.Add(this.userPasswordBox);
            this.groupBoxUsers.Controls.Add(this.label147);
            this.groupBoxUsers.Controls.Add(this.fullNameBox);
            this.groupBoxUsers.Controls.Add(this.label144);
            this.groupBoxUsers.Location = new System.Drawing.Point(8, 16);
            this.groupBoxUsers.Name = "groupBoxUsers";
            this.groupBoxUsers.Size = new System.Drawing.Size(827, 288);
            this.groupBoxUsers.TabIndex = 14;
            this.groupBoxUsers.TabStop = false;
            this.groupBoxUsers.Text = "User Properties (deprecated)";
            this.groupBoxUsers.Leave += new System.EventHandler(this.groupBoxUsers_Leave);
            // 
            // userPasswordBox
            // 
            this.userPasswordBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userPasswordBox.Location = new System.Drawing.Point(97, 51);
            this.userPasswordBox.Name = "userPasswordBox";
            this.userPasswordBox.PasswordChar = '*';
            this.userPasswordBox.Size = new System.Drawing.Size(176, 20);
            this.userPasswordBox.TabIndex = 6;
            // 
            // label147
            // 
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(9, 51);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(88, 16);
            this.label147.TabIndex = 9;
            this.label147.Text = "Password";
            // 
            // fullNameBox
            // 
            this.fullNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fullNameBox.Location = new System.Drawing.Point(97, 20);
            this.fullNameBox.Name = "fullNameBox";
            this.fullNameBox.Size = new System.Drawing.Size(176, 20);
            this.fullNameBox.TabIndex = 7;
            // 
            // label144
            // 
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(9, 20);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(88, 16);
            this.label144.TabIndex = 7;
            this.label144.Text = "Full Name";
            // 
            // groupsGroupBox
            // 
            this.groupsGroupBox.Controls.Add(this.groupBoxGroups);
            this.groupsGroupBox.Location = new System.Drawing.Point(0, 0);
            this.groupsGroupBox.Name = "groupsGroupBox";
            this.groupsGroupBox.Size = new System.Drawing.Size(792, 312);
            this.groupsGroupBox.TabIndex = 5;
            this.groupsGroupBox.TabStop = false;
            this.groupsGroupBox.Text = "Manage Groups (deprecated)";
            // 
            // groupBoxGroups
            // 
            this.groupBoxGroups.Controls.Add(this.internalGroup);
            this.groupBoxGroups.Controls.Add(this.setPriviliges);
            this.groupBoxGroups.Controls.Add(this.label143);
            this.groupBoxGroups.Controls.Add(this.groupUserList);
            this.groupBoxGroups.Controls.Add(this.groupFilterList);
            this.groupBoxGroups.Controls.Add(this.groupFilterListBox);
            this.groupBoxGroups.Location = new System.Drawing.Point(8, 16);
            this.groupBoxGroups.Name = "groupBoxGroups";
            this.groupBoxGroups.Size = new System.Drawing.Size(776, 288);
            this.groupBoxGroups.TabIndex = 13;
            this.groupBoxGroups.TabStop = false;
            this.groupBoxGroups.Text = "Group Properties (deprecated)";
            this.groupBoxGroups.Leave += new System.EventHandler(this.groupBoxGroups_Leave);
            // 
            // internalGroup
            // 
            this.internalGroup.AutoSize = true;
            this.internalGroup.Location = new System.Drawing.Point(434, 261);
            this.internalGroup.Name = "internalGroup";
            this.internalGroup.Size = new System.Drawing.Size(93, 17);
            this.internalGroup.TabIndex = 47;
            this.internalGroup.Text = "Internal Group";
            this.internalGroup.UseVisualStyleBackColor = true;
            // 
            // setPriviliges
            // 
            this.setPriviliges.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.setPriviliges.Location = new System.Drawing.Point(16, 246);
            this.setPriviliges.Name = "setPriviliges";
            this.setPriviliges.Size = new System.Drawing.Size(96, 36);
            this.setPriviliges.TabIndex = 46;
            this.setPriviliges.Text = "Set Privileges (deprecated)";
            this.setPriviliges.Click += new System.EventHandler(this.setPriviliges_Click);
            // 
            // label143
            // 
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(9, 16);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(64, 16);
            this.label143.TabIndex = 7;
            this.label143.Text = "Users";
            // 
            // groupUserList
            // 
            this.groupUserList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupUserList.CheckBoxes = true;
            this.groupUserList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader46});
            this.groupUserList.FullRowSelect = true;
            this.groupUserList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.groupUserList.HideSelection = false;
            this.groupUserList.Location = new System.Drawing.Point(12, 35);
            this.groupUserList.Name = "groupUserList";
            this.groupUserList.ShowItemToolTips = true;
            this.groupUserList.Size = new System.Drawing.Size(274, 199);
            this.groupUserList.TabIndex = 8;
            this.groupUserList.UseCompatibleStateImageBehavior = false;
            this.groupUserList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "Username";
            this.columnHeader46.Width = 150;
            // 
            // groupFilterList
            // 
            this.groupFilterList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupFilterList.Location = new System.Drawing.Point(431, 16);
            this.groupFilterList.Name = "groupFilterList";
            this.groupFilterList.Size = new System.Drawing.Size(64, 16);
            this.groupFilterList.TabIndex = 45;
            this.groupFilterList.Text = "Filters";
            // 
            // groupFilterListBox
            // 
            this.groupFilterListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupFilterListBox.CheckOnClick = true;
            this.groupFilterListBox.Location = new System.Drawing.Point(434, 42);
            this.groupFilterListBox.Name = "groupFilterListBox";
            this.groupFilterListBox.Size = new System.Drawing.Size(231, 197);
            this.groupFilterListBox.TabIndex = 44;
            this.groupFilterListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UserAndGroups_MouseMove);
            // 
            // groupsMenuStrip
            // 
            this.groupsMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGroupToolStripMenuItem,
            this.removeGroupToolStripMenuItem});
            this.groupsMenuStrip.Name = "groupsMenuStrip";
            this.groupsMenuStrip.ShowImageMargin = false;
            this.groupsMenuStrip.Size = new System.Drawing.Size(129, 48);
            // 
            // newGroupToolStripMenuItem
            // 
            this.newGroupToolStripMenuItem.Name = "newGroupToolStripMenuItem";
            this.newGroupToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.newGroupToolStripMenuItem.Text = "New Group";
            this.newGroupToolStripMenuItem.Click += new System.EventHandler(this.addGroupButton_Click);
            // 
            // removeGroupToolStripMenuItem
            // 
            this.removeGroupToolStripMenuItem.Name = "removeGroupToolStripMenuItem";
            this.removeGroupToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.removeGroupToolStripMenuItem.Text = "Remove Group";
            this.removeGroupToolStripMenuItem.Click += new System.EventHandler(this.removeGroupButton_Click);
            // 
            // usersMenuStrip
            // 
            this.usersMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserToolStripMenuItem,
            this.removeUserToolStripMenuItem});
            this.usersMenuStrip.Name = "usersMenuStrip";
            this.usersMenuStrip.ShowImageMargin = false;
            this.usersMenuStrip.Size = new System.Drawing.Size(119, 48);
            // 
            // newUserToolStripMenuItem
            // 
            this.newUserToolStripMenuItem.Name = "newUserToolStripMenuItem";
            this.newUserToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.newUserToolStripMenuItem.Text = "New User";
            this.newUserToolStripMenuItem.Click += new System.EventHandler(this.addUserButton_Click);
            // 
            // removeUserToolStripMenuItem
            // 
            this.removeUserToolStripMenuItem.Name = "removeUserToolStripMenuItem";
            this.removeUserToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.removeUserToolStripMenuItem.Text = "Remove User";
            this.removeUserToolStripMenuItem.Click += new System.EventHandler(this.removeUserButton_Click);
            // 
            // UsersAndGroups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "UsersAndGroups";
            this.Text = "UsersAndGroups";
            this.tabControl.ResumeLayout(false);
            this.usersAndGroupsTabPage.ResumeLayout(false);
            this.usersGroupBox.ResumeLayout(false);
            this.groupBoxUsers.ResumeLayout(false);
            this.groupBoxUsers.PerformLayout();
            this.groupsGroupBox.ResumeLayout(false);
            this.groupBoxGroups.ResumeLayout(false);
            this.groupBoxGroups.PerformLayout();
            this.groupsMenuStrip.ResumeLayout(false);
            this.usersMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage usersAndGroupsTabPage;
        private System.Windows.Forms.GroupBox usersGroupBox;
        private System.Windows.Forms.TextBox userPasswordBox;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox fullNameBox;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.GroupBox groupBoxUsers;
        private System.Windows.Forms.GroupBox groupsGroupBox;
        private System.Windows.Forms.ListView groupUserList;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.GroupBox groupBoxGroups;
        private System.Windows.Forms.Label groupFilterList;
        private System.Windows.Forms.CheckedListBox groupFilterListBox;
        private System.Windows.Forms.Button setPriviliges;
        private System.Windows.Forms.ContextMenuStrip groupsMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeGroupToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip usersMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem newUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeUserToolStripMenuItem;
        private System.Windows.Forms.CheckBox internalGroup;
        private System.Windows.Forms.ToolTip toolTip;
    }
}