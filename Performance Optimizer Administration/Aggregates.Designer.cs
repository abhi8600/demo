namespace Performance_Optimizer_Administration
{
    partial class Aggregates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.aggTabPage = new System.Windows.Forms.TabPage();
            this.groupBoxAggregates = new System.Windows.Forms.GroupBox();
            this.groupBoxLogicalQuery = new System.Windows.Forms.GroupBox();
            this.buildFilterTextBox = new System.Windows.Forms.TextBox();
            this.lblBuildFilter = new System.Windows.Forms.Label();
            this.incrementalFilterTextBox = new System.Windows.Forms.TextBox();
            this.lblIncFilter = new System.Windows.Forms.Label();
            this.processingGroupTextBox = new System.Windows.Forms.TextBox();
            this.lblProcessingGroup = new System.Windows.Forms.Label();
            this.logicalQueryTextBox = new System.Windows.Forms.TextBox();
            this.dbConnection = new System.Windows.Forms.ComboBox();
            this.lblDBConnetion = new System.Windows.Forms.Label();
            this.aggDisabledCheckBox = new System.Windows.Forms.CheckBox();
            this.logicalQueryAggButton = new System.Windows.Forms.RadioButton();
            this.columnAggButton = new System.Windows.Forms.RadioButton();
            this.groupBoxColumnsFilters = new System.Windows.Forms.GroupBox();
            this.availAggMeasures = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.prefixesComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxOuterJoin = new System.Windows.Forms.CheckBox();
            this.moveDownBtn = new System.Windows.Forms.Button();
            this.moveUpBtn = new System.Windows.Forms.Button();
            this.loadGroupsListBox = new System.Windows.Forms.ListView();
            this.LoadGroupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.modifyTransformation = new System.Windows.Forms.Button();
            this.deleteTransformation = new System.Windows.Forms.Button();
            this.addTransformation = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.transformList = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.transFormHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filterListBox = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.aggColListBox = new System.Windows.Forms.ListBox();
            this.label93 = new System.Windows.Forms.Label();
            this.aggDimensionBox = new System.Windows.Forms.ComboBox();
            this.removeLevel = new System.Windows.Forms.Button();
            this.addLevel = new System.Windows.Forms.Button();
            this.label92 = new System.Windows.Forms.Label();
            this.aggSelectedColumns = new System.Windows.Forms.ListView();
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label89 = new System.Windows.Forms.Label();
            this.selectedAggMeasures = new System.Windows.Forms.ListView();
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.removeAggMeasure = new System.Windows.Forms.Button();
            this.addAggMeasure = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.aggMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addAggregateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAggregateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl.SuspendLayout();
            this.aggTabPage.SuspendLayout();
            this.groupBoxAggregates.SuspendLayout();
            this.groupBoxLogicalQuery.SuspendLayout();
            this.groupBoxColumnsFilters.SuspendLayout();
            this.aggMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.aggTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // aggTabPage
            // 
            this.aggTabPage.AutoScroll = true;
            this.aggTabPage.Controls.Add(this.groupBoxAggregates);
            this.aggTabPage.Location = new System.Drawing.Point(4, 22);
            this.aggTabPage.Name = "aggTabPage";
            this.aggTabPage.Size = new System.Drawing.Size(844, 707);
            this.aggTabPage.TabIndex = 13;
            this.aggTabPage.Text = "Aggregates";
            this.aggTabPage.UseVisualStyleBackColor = true;
            this.aggTabPage.Leave += new System.EventHandler(this.aggTabPage_Leave);
            // 
            // groupBoxAggregates
            // 
            this.groupBoxAggregates.AutoSize = true;
            this.groupBoxAggregates.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxAggregates.Controls.Add(this.groupBoxLogicalQuery);
            this.groupBoxAggregates.Controls.Add(this.dbConnection);
            this.groupBoxAggregates.Controls.Add(this.lblDBConnetion);
            this.groupBoxAggregates.Controls.Add(this.aggDisabledCheckBox);
            this.groupBoxAggregates.Controls.Add(this.logicalQueryAggButton);
            this.groupBoxAggregates.Controls.Add(this.columnAggButton);
            this.groupBoxAggregates.Controls.Add(this.groupBoxColumnsFilters);
            this.groupBoxAggregates.Location = new System.Drawing.Point(8, 3);
            this.groupBoxAggregates.Name = "groupBoxAggregates";
            this.groupBoxAggregates.Size = new System.Drawing.Size(769, 690);
            this.groupBoxAggregates.TabIndex = 93;
            this.groupBoxAggregates.TabStop = false;
            this.groupBoxAggregates.Text = "Aggregate Definition";
            this.groupBoxAggregates.Leave += new System.EventHandler(this.groupBoxAggregates_Leave);
            // 
            // groupBoxLogicalQuery
            // 
            this.groupBoxLogicalQuery.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxLogicalQuery.Controls.Add(this.buildFilterTextBox);
            this.groupBoxLogicalQuery.Controls.Add(this.lblBuildFilter);
            this.groupBoxLogicalQuery.Controls.Add(this.incrementalFilterTextBox);
            this.groupBoxLogicalQuery.Controls.Add(this.lblIncFilter);
            this.groupBoxLogicalQuery.Controls.Add(this.processingGroupTextBox);
            this.groupBoxLogicalQuery.Controls.Add(this.lblProcessingGroup);
            this.groupBoxLogicalQuery.Controls.Add(this.logicalQueryTextBox);
            this.groupBoxLogicalQuery.Location = new System.Drawing.Point(12, 46);
            this.groupBoxLogicalQuery.Name = "groupBoxLogicalQuery";
            this.groupBoxLogicalQuery.Size = new System.Drawing.Size(751, 625);
            this.groupBoxLogicalQuery.TabIndex = 112;
            this.groupBoxLogicalQuery.TabStop = false;
            this.groupBoxLogicalQuery.Text = "Logical Query";
            // 
            // buildFilterTextBox
            // 
            this.buildFilterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buildFilterTextBox.Location = new System.Drawing.Point(10, 463);
            this.buildFilterTextBox.Multiline = true;
            this.buildFilterTextBox.Name = "buildFilterTextBox";
            this.buildFilterTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.buildFilterTextBox.Size = new System.Drawing.Size(729, 110);
            this.buildFilterTextBox.TabIndex = 6;
            // 
            // lblBuildFilter
            // 
            this.lblBuildFilter.AutoSize = true;
            this.lblBuildFilter.Location = new System.Drawing.Point(7, 437);
            this.lblBuildFilter.Name = "lblBuildFilter";
            this.lblBuildFilter.Size = new System.Drawing.Size(55, 13);
            this.lblBuildFilter.TabIndex = 5;
            this.lblBuildFilter.Text = "Build Filter";
            // 
            // incrementalFilterTextBox
            // 
            this.incrementalFilterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.incrementalFilterTextBox.Location = new System.Drawing.Point(10, 316);
            this.incrementalFilterTextBox.Multiline = true;
            this.incrementalFilterTextBox.Name = "incrementalFilterTextBox";
            this.incrementalFilterTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.incrementalFilterTextBox.Size = new System.Drawing.Size(729, 109);
            this.incrementalFilterTextBox.TabIndex = 4;
            // 
            // lblIncFilter
            // 
            this.lblIncFilter.AutoSize = true;
            this.lblIncFilter.Location = new System.Drawing.Point(7, 292);
            this.lblIncFilter.Name = "lblIncFilter";
            this.lblIncFilter.Size = new System.Drawing.Size(87, 13);
            this.lblIncFilter.TabIndex = 3;
            this.lblIncFilter.Text = "Incremental Filter";
            // 
            // processingGroupTextBox
            // 
            this.processingGroupTextBox.Location = new System.Drawing.Point(118, 245);
            this.processingGroupTextBox.Name = "processingGroupTextBox";
            this.processingGroupTextBox.Size = new System.Drawing.Size(207, 20);
            this.processingGroupTextBox.TabIndex = 2;
            // 
            // lblProcessingGroup
            // 
            this.lblProcessingGroup.AutoSize = true;
            this.lblProcessingGroup.Location = new System.Drawing.Point(7, 248);
            this.lblProcessingGroup.Name = "lblProcessingGroup";
            this.lblProcessingGroup.Size = new System.Drawing.Size(91, 13);
            this.lblProcessingGroup.TabIndex = 1;
            this.lblProcessingGroup.Text = "Processing Group";
            // 
            // logicalQueryTextBox
            // 
            this.logicalQueryTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logicalQueryTextBox.Location = new System.Drawing.Point(8, 17);
            this.logicalQueryTextBox.Multiline = true;
            this.logicalQueryTextBox.Name = "logicalQueryTextBox";
            this.logicalQueryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logicalQueryTextBox.Size = new System.Drawing.Size(731, 200);
            this.logicalQueryTextBox.TabIndex = 0;
            this.logicalQueryTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.logicalQueryTextBox_KeyDown);
            this.logicalQueryTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.logicalQueryTextBox_KeyPress);
            // 
            // dbConnection
            // 
            this.dbConnection.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dbConnection.Location = new System.Drawing.Point(594, 19);
            this.dbConnection.Name = "dbConnection";
            this.dbConnection.Size = new System.Drawing.Size(169, 21);
            this.dbConnection.Sorted = true;
            this.dbConnection.TabIndex = 117;
            // 
            // lblDBConnetion
            // 
            this.lblDBConnetion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDBConnetion.Location = new System.Drawing.Point(510, 18);
            this.lblDBConnetion.Name = "lblDBConnetion";
            this.lblDBConnetion.Size = new System.Drawing.Size(78, 23);
            this.lblDBConnetion.TabIndex = 116;
            this.lblDBConnetion.Text = "Connection";
            // 
            // aggDisabledCheckBox
            // 
            this.aggDisabledCheckBox.AutoSize = true;
            this.aggDisabledCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.aggDisabledCheckBox.Location = new System.Drawing.Point(422, 17);
            this.aggDisabledCheckBox.Name = "aggDisabledCheckBox";
            this.aggDisabledCheckBox.Size = new System.Drawing.Size(82, 20);
            this.aggDisabledCheckBox.TabIndex = 115;
            this.aggDisabledCheckBox.Text = "Disabled";
            this.aggDisabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // logicalQueryAggButton
            // 
            this.logicalQueryAggButton.AutoSize = true;
            this.logicalQueryAggButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logicalQueryAggButton.Location = new System.Drawing.Point(253, 16);
            this.logicalQueryAggButton.Name = "logicalQueryAggButton";
            this.logicalQueryAggButton.Size = new System.Drawing.Size(157, 20);
            this.logicalQueryAggButton.TabIndex = 114;
            this.logicalQueryAggButton.Text = "Specify Logical Query";
            this.logicalQueryAggButton.UseVisualStyleBackColor = true;
            // 
            // columnAggButton
            // 
            this.columnAggButton.AutoSize = true;
            this.columnAggButton.Checked = true;
            this.columnAggButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnAggButton.Location = new System.Drawing.Point(12, 16);
            this.columnAggButton.Name = "columnAggButton";
            this.columnAggButton.Size = new System.Drawing.Size(235, 20);
            this.columnAggButton.TabIndex = 113;
            this.columnAggButton.TabStop = true;
            this.columnAggButton.Text = "Select Specific Columns and Filters";
            this.columnAggButton.UseVisualStyleBackColor = true;
            this.columnAggButton.CheckedChanged += new System.EventHandler(this.columnAggButton_CheckedChanged);
            // 
            // groupBoxColumnsFilters
            // 
            this.groupBoxColumnsFilters.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxColumnsFilters.Controls.Add(this.availAggMeasures);
            this.groupBoxColumnsFilters.Controls.Add(this.prefixesComboBox);
            this.groupBoxColumnsFilters.Controls.Add(this.label3);
            this.groupBoxColumnsFilters.Controls.Add(this.checkBoxOuterJoin);
            this.groupBoxColumnsFilters.Controls.Add(this.moveDownBtn);
            this.groupBoxColumnsFilters.Controls.Add(this.moveUpBtn);
            this.groupBoxColumnsFilters.Controls.Add(this.loadGroupsListBox);
            this.groupBoxColumnsFilters.Controls.Add(this.label4);
            this.groupBoxColumnsFilters.Controls.Add(this.modifyTransformation);
            this.groupBoxColumnsFilters.Controls.Add(this.deleteTransformation);
            this.groupBoxColumnsFilters.Controls.Add(this.addTransformation);
            this.groupBoxColumnsFilters.Controls.Add(this.label2);
            this.groupBoxColumnsFilters.Controls.Add(this.transformList);
            this.groupBoxColumnsFilters.Controls.Add(this.filterListBox);
            this.groupBoxColumnsFilters.Controls.Add(this.label1);
            this.groupBoxColumnsFilters.Controls.Add(this.aggColListBox);
            this.groupBoxColumnsFilters.Controls.Add(this.label93);
            this.groupBoxColumnsFilters.Controls.Add(this.aggDimensionBox);
            this.groupBoxColumnsFilters.Controls.Add(this.removeLevel);
            this.groupBoxColumnsFilters.Controls.Add(this.addLevel);
            this.groupBoxColumnsFilters.Controls.Add(this.label92);
            this.groupBoxColumnsFilters.Controls.Add(this.aggSelectedColumns);
            this.groupBoxColumnsFilters.Controls.Add(this.label89);
            this.groupBoxColumnsFilters.Controls.Add(this.selectedAggMeasures);
            this.groupBoxColumnsFilters.Controls.Add(this.removeAggMeasure);
            this.groupBoxColumnsFilters.Controls.Add(this.addAggMeasure);
            this.groupBoxColumnsFilters.Controls.Add(this.label88);
            this.groupBoxColumnsFilters.Controls.Add(this.label90);
            this.groupBoxColumnsFilters.Location = new System.Drawing.Point(12, 38);
            this.groupBoxColumnsFilters.Name = "groupBoxColumnsFilters";
            this.groupBoxColumnsFilters.Size = new System.Drawing.Size(751, 632);
            this.groupBoxColumnsFilters.TabIndex = 111;
            this.groupBoxColumnsFilters.TabStop = false;
            this.groupBoxColumnsFilters.Text = "Select Columns and Filters";
            // 
            // availAggMeasures
            // 
            this.availAggMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availAggMeasures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.availAggMeasures.FullRowSelect = true;
            this.availAggMeasures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.availAggMeasures.HideSelection = false;
            this.availAggMeasures.Location = new System.Drawing.Point(7, 74);
            this.availAggMeasures.Name = "availAggMeasures";
            this.availAggMeasures.ShowItemToolTips = true;
            this.availAggMeasures.Size = new System.Drawing.Size(318, 119);
            this.availAggMeasures.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.availAggMeasures.TabIndex = 127;
            this.availAggMeasures.UseCompatibleStateImageBehavior = false;
            this.availAggMeasures.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Measure";
            this.columnHeader1.Width = 200;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Category";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 120;
            // 
            // prefixesComboBox
            // 
            this.prefixesComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.prefixesComboBox.Location = new System.Drawing.Point(90, 41);
            this.prefixesComboBox.Name = "prefixesComboBox";
            this.prefixesComboBox.Size = new System.Drawing.Size(184, 21);
            this.prefixesComboBox.Sorted = true;
            this.prefixesComboBox.TabIndex = 125;
            this.prefixesComboBox.TextChanged += new System.EventHandler(this.prefixesComboBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 20);
            this.label3.TabIndex = 124;
            this.label3.Text = "Prefix";
            // 
            // checkBoxOuterJoin
            // 
            this.checkBoxOuterJoin.AutoSize = true;
            this.checkBoxOuterJoin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxOuterJoin.Location = new System.Drawing.Point(245, 204);
            this.checkBoxOuterJoin.Name = "checkBoxOuterJoin";
            this.checkBoxOuterJoin.Size = new System.Drawing.Size(87, 20);
            this.checkBoxOuterJoin.TabIndex = 120;
            this.checkBoxOuterJoin.Text = "Outer Join";
            this.checkBoxOuterJoin.UseVisualStyleBackColor = true;
            // 
            // moveDownBtn
            // 
            this.moveDownBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveDownBtn.Location = new System.Drawing.Point(640, 603);
            this.moveDownBtn.Name = "moveDownBtn";
            this.moveDownBtn.Size = new System.Drawing.Size(99, 23);
            this.moveDownBtn.TabIndex = 119;
            this.moveDownBtn.Text = "Move Down";
            this.moveDownBtn.Click += new System.EventHandler(this.moveDownBtn_Click);
            // 
            // moveUpBtn
            // 
            this.moveUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveUpBtn.Location = new System.Drawing.Point(640, 574);
            this.moveUpBtn.Name = "moveUpBtn";
            this.moveUpBtn.Size = new System.Drawing.Size(99, 23);
            this.moveUpBtn.TabIndex = 118;
            this.moveUpBtn.Text = "Move Up";
            this.moveUpBtn.Click += new System.EventHandler(this.moveUpBtn_Click);
            // 
            // loadGroupsListBox
            // 
            this.loadGroupsListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.loadGroupsListBox.CheckBoxes = true;
            this.loadGroupsListBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.LoadGroupName});
            this.loadGroupsListBox.FullRowSelect = true;
            this.loadGroupsListBox.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.loadGroupsListBox.Location = new System.Drawing.Point(407, 385);
            this.loadGroupsListBox.Name = "loadGroupsListBox";
            this.loadGroupsListBox.ShowItemToolTips = true;
            this.loadGroupsListBox.Size = new System.Drawing.Size(338, 80);
            this.loadGroupsListBox.TabIndex = 117;
            this.loadGroupsListBox.UseCompatibleStateImageBehavior = false;
            this.loadGroupsListBox.View = System.Windows.Forms.View.Details;
            // 
            // LoadGroupName
            // 
            this.LoadGroupName.Text = "Name";
            this.LoadGroupName.Width = 300;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.Location = new System.Drawing.Point(404, 365);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 16);
            this.label4.TabIndex = 116;
            this.label4.Text = "Load Groups";
            // 
            // modifyTransformation
            // 
            this.modifyTransformation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.modifyTransformation.Location = new System.Drawing.Point(640, 516);
            this.modifyTransformation.Name = "modifyTransformation";
            this.modifyTransformation.Size = new System.Drawing.Size(99, 23);
            this.modifyTransformation.TabIndex = 115;
            this.modifyTransformation.Text = "Modify";
            this.modifyTransformation.Click += new System.EventHandler(this.modifyTransformation_Click);
            // 
            // deleteTransformation
            // 
            this.deleteTransformation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.deleteTransformation.Location = new System.Drawing.Point(640, 545);
            this.deleteTransformation.Name = "deleteTransformation";
            this.deleteTransformation.Size = new System.Drawing.Size(99, 23);
            this.deleteTransformation.TabIndex = 114;
            this.deleteTransformation.Text = "Delete";
            this.deleteTransformation.Click += new System.EventHandler(this.deleteTransformation_Click);
            // 
            // addTransformation
            // 
            this.addTransformation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addTransformation.Location = new System.Drawing.Point(640, 471);
            this.addTransformation.Name = "addTransformation";
            this.addTransformation.Size = new System.Drawing.Size(99, 39);
            this.addTransformation.TabIndex = 113;
            this.addTransformation.Text = "Add Selected Column";
            this.addTransformation.Click += new System.EventHandler(this.addTransformation_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 471);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 23);
            this.label2.TabIndex = 112;
            this.label2.Text = "Transformations";
            // 
            // transformList
            // 
            this.transformList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.transformList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.transFormHeader});
            this.transformList.FullRowSelect = true;
            this.transformList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.transformList.Location = new System.Drawing.Point(118, 471);
            this.transformList.MultiSelect = false;
            this.transformList.Name = "transformList";
            this.transformList.ShowItemToolTips = true;
            this.transformList.Size = new System.Drawing.Size(516, 156);
            this.transformList.TabIndex = 111;
            this.transformList.UseCompatibleStateImageBehavior = false;
            this.transformList.View = System.Windows.Forms.View.Details;
            // 
            // colName
            // 
            this.colName.Text = "Column Name";
            this.colName.Width = 117;
            // 
            // transFormHeader
            // 
            this.transFormHeader.Text = "Transformation";
            this.transFormHeader.Width = 183;
            // 
            // filterListBox
            // 
            this.filterListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filterListBox.CheckOnClick = true;
            this.filterListBox.Location = new System.Drawing.Point(8, 385);
            this.filterListBox.Name = "filterListBox";
            this.filterListBox.Size = new System.Drawing.Size(317, 77);
            this.filterListBox.TabIndex = 110;
            this.filterListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Aggregates_MouseMove);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 363);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 23);
            this.label1.TabIndex = 109;
            this.label1.Text = "Filters";
            // 
            // aggColListBox
            // 
            this.aggColListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.aggColListBox.FormattingEnabled = true;
            this.aggColListBox.Location = new System.Drawing.Point(7, 268);
            this.aggColListBox.Name = "aggColListBox";
            this.aggColListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.aggColListBox.Size = new System.Drawing.Size(318, 93);
            this.aggColListBox.Sorted = true;
            this.aggColListBox.TabIndex = 108;
            this.aggColListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Aggregates_MouseMove);
            // 
            // label93
            // 
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(4, 204);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(160, 23);
            this.label93.TabIndex = 107;
            this.label93.Text = "Dimensions and Columns";
            // 
            // aggDimensionBox
            // 
            this.aggDimensionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.aggDimensionBox.Location = new System.Drawing.Point(90, 232);
            this.aggDimensionBox.Name = "aggDimensionBox";
            this.aggDimensionBox.Size = new System.Drawing.Size(184, 21);
            this.aggDimensionBox.Sorted = true;
            this.aggDimensionBox.TabIndex = 105;
            this.aggDimensionBox.SelectedIndexChanged += new System.EventHandler(this.aggDimensionBox_SelectedIndexChanged);
            // 
            // removeLevel
            // 
            this.removeLevel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeLevel.Location = new System.Drawing.Point(331, 295);
            this.removeLevel.Name = "removeLevel";
            this.removeLevel.Size = new System.Drawing.Size(73, 23);
            this.removeLevel.TabIndex = 104;
            this.removeLevel.Text = "Remove";
            this.removeLevel.Click += new System.EventHandler(this.removeColumn_Click);
            // 
            // addLevel
            // 
            this.addLevel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addLevel.Location = new System.Drawing.Point(331, 266);
            this.addLevel.Name = "addLevel";
            this.addLevel.Size = new System.Drawing.Size(72, 23);
            this.addLevel.TabIndex = 103;
            this.addLevel.Text = "Add";
            this.addLevel.Click += new System.EventHandler(this.addColumn_Click);
            // 
            // label92
            // 
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(409, 204);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(238, 23);
            this.label92.TabIndex = 102;
            this.label92.Text = "Selected Dimensions and Columns";
            // 
            // aggSelectedColumns
            // 
            this.aggSelectedColumns.AutoArrange = false;
            this.aggSelectedColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.aggSelectedColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader18});
            this.aggSelectedColumns.FullRowSelect = true;
            this.aggSelectedColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.aggSelectedColumns.HideSelection = false;
            this.aggSelectedColumns.Location = new System.Drawing.Point(407, 228);
            this.aggSelectedColumns.Margin = new System.Windows.Forms.Padding(0);
            this.aggSelectedColumns.Name = "aggSelectedColumns";
            this.aggSelectedColumns.ShowGroups = false;
            this.aggSelectedColumns.ShowItemToolTips = true;
            this.aggSelectedColumns.Size = new System.Drawing.Size(338, 133);
            this.aggSelectedColumns.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.aggSelectedColumns.TabIndex = 101;
            this.aggSelectedColumns.UseCompatibleStateImageBehavior = false;
            this.aggSelectedColumns.View = System.Windows.Forms.View.Details;
            this.aggSelectedColumns.SelectedIndexChanged += new System.EventHandler(this.aggSelectedColumns_SelectedIndexChanged);
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Dimension";
            this.columnHeader17.Width = 146;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Column";
            this.columnHeader18.Width = 184;
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(4, 233);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(80, 23);
            this.label89.TabIndex = 99;
            this.label89.Text = "Dimension";
            // 
            // selectedAggMeasures
            // 
            this.selectedAggMeasures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectedAggMeasures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14});
            this.selectedAggMeasures.FullRowSelect = true;
            this.selectedAggMeasures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.selectedAggMeasures.HideSelection = false;
            this.selectedAggMeasures.Location = new System.Drawing.Point(406, 39);
            this.selectedAggMeasures.Name = "selectedAggMeasures";
            this.selectedAggMeasures.ShowItemToolTips = true;
            this.selectedAggMeasures.Size = new System.Drawing.Size(339, 154);
            this.selectedAggMeasures.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.selectedAggMeasures.TabIndex = 98;
            this.selectedAggMeasures.UseCompatibleStateImageBehavior = false;
            this.selectedAggMeasures.View = System.Windows.Forms.View.Details;
            this.selectedAggMeasures.SelectedIndexChanged += new System.EventHandler(this.selectedAggMeasures_SelectedIndexChanged);
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Measure";
            this.columnHeader13.Width = 200;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Category";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader14.Width = 120;
            // 
            // removeAggMeasure
            // 
            this.removeAggMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeAggMeasure.Location = new System.Drawing.Point(330, 106);
            this.removeAggMeasure.Name = "removeAggMeasure";
            this.removeAggMeasure.Size = new System.Drawing.Size(70, 23);
            this.removeAggMeasure.TabIndex = 96;
            this.removeAggMeasure.Text = "Remove";
            this.removeAggMeasure.Click += new System.EventHandler(this.removeAggMeasure_Click);
            // 
            // addAggMeasure
            // 
            this.addAggMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addAggMeasure.Location = new System.Drawing.Point(331, 74);
            this.addAggMeasure.Name = "addAggMeasure";
            this.addAggMeasure.Size = new System.Drawing.Size(69, 23);
            this.addAggMeasure.TabIndex = 95;
            this.addAggMeasure.Text = "Add";
            this.addAggMeasure.Click += new System.EventHandler(this.addAggMeasure_Click);
            // 
            // label88
            // 
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(427, 15);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(192, 23);
            this.label88.TabIndex = 94;
            this.label88.Text = "Selected Business Measures";
            // 
            // label90
            // 
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(6, 15);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(192, 23);
            this.label90.TabIndex = 93;
            this.label90.Text = "Available Business Measures";
            // 
            // aggMenuStrip
            // 
            this.aggMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAggregateToolStripMenuItem,
            this.removeAggregateToolStripMenuItem});
            this.aggMenuStrip.Name = "aggMenuStrip";
            this.aggMenuStrip.ShowImageMargin = false;
            this.aggMenuStrip.Size = new System.Drawing.Size(151, 48);
            // 
            // addAggregateToolStripMenuItem
            // 
            this.addAggregateToolStripMenuItem.Name = "addAggregateToolStripMenuItem";
            this.addAggregateToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.addAggregateToolStripMenuItem.Text = "New Aggregate";
            this.addAggregateToolStripMenuItem.Click += new System.EventHandler(this.addAggButton_Click);
            // 
            // removeAggregateToolStripMenuItem
            // 
            this.removeAggregateToolStripMenuItem.Name = "removeAggregateToolStripMenuItem";
            this.removeAggregateToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.removeAggregateToolStripMenuItem.Text = "Remove Aggregate";
            this.removeAggregateToolStripMenuItem.Click += new System.EventHandler(this.removeAggButton_Click);
            // 
            // Aggregates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "Aggregates";
            this.Text = "Aggregates";
            this.tabControl.ResumeLayout(false);
            this.aggTabPage.ResumeLayout(false);
            this.aggTabPage.PerformLayout();
            this.groupBoxAggregates.ResumeLayout(false);
            this.groupBoxAggregates.PerformLayout();
            this.groupBoxLogicalQuery.ResumeLayout(false);
            this.groupBoxLogicalQuery.PerformLayout();
            this.groupBoxColumnsFilters.ResumeLayout(false);
            this.groupBoxColumnsFilters.PerformLayout();
            this.aggMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage aggTabPage;
        private System.Windows.Forms.GroupBox groupBoxAggregates;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.ComboBox aggDimensionBox;
        private System.Windows.Forms.Button removeLevel;
        private System.Windows.Forms.Button addLevel;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.ListView aggSelectedColumns;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.ListView selectedAggMeasures;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.Button removeAggMeasure;
        private System.Windows.Forms.Button addAggMeasure;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ContextMenuStrip aggMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addAggregateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeAggregateToolStripMenuItem;
        private System.Windows.Forms.ListBox aggColListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox filterListBox;
        private System.Windows.Forms.RadioButton logicalQueryAggButton;
        private System.Windows.Forms.RadioButton columnAggButton;
        private System.Windows.Forms.GroupBox groupBoxLogicalQuery;
        private System.Windows.Forms.GroupBox groupBoxColumnsFilters;
        private System.Windows.Forms.TextBox logicalQueryTextBox;
        private System.Windows.Forms.ListView transformList;
        private System.Windows.Forms.Button deleteTransformation;
        private System.Windows.Forms.Button addTransformation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader transFormHeader;
        private System.Windows.Forms.Button modifyTransformation;
        private System.Windows.Forms.ListView loadGroupsListBox;
        private System.Windows.Forms.ColumnHeader LoadGroupName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button moveDownBtn;
        private System.Windows.Forms.Button moveUpBtn;
        private System.Windows.Forms.CheckBox checkBoxOuterJoin;
        private System.Windows.Forms.ComboBox prefixesComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView availAggMeasures;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.CheckBox aggDisabledCheckBox;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ComboBox dbConnection;
        private System.Windows.Forms.Label lblDBConnetion;
        private System.Windows.Forms.TextBox buildFilterTextBox;
        private System.Windows.Forms.Label lblBuildFilter;
        private System.Windows.Forms.TextBox incrementalFilterTextBox;
        private System.Windows.Forms.Label lblIncFilter;
        private System.Windows.Forms.TextBox processingGroupTextBox;
        private System.Windows.Forms.Label lblProcessingGroup;
    }
}