﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    public partial class ViewStagingTableScriptDialog : Form
    {
        public ViewStagingTableScriptDialog(ScriptDefinition Script)
        {
            InitializeComponent();
            if (Script != null)
            {
                if (Script.InputQuery != null)
                    txtInputQuery.Text = Script.InputQuery;
                if (Script.Output != null)
                    txtOutput.Text = Script.Output;
                if (Script.Script != null)
                    txtScript.Text = Script.Script;
                txtInputQuery.SelectionStart = txtInputQuery.SelectionLength = 0;
                txtOutput.SelectionStart = txtOutput.SelectionLength = 0;
                txtScript.SelectionStart = txtScript.SelectionLength = 0;
            }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
