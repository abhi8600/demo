using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using log4net;
using System.Web.Services.Protocols;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Threading;

namespace Performance_Optimizer_Administration
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainAdminForm : System.Windows.Forms.Form, IMRUClient, DropModule
    {
        private static int FLOAT_WIDTH = 10; // Width to use for floats when converting to varchar
        public static readonly int RELATIONAL_DIMENSIONAL_STRUCTURE = 0;
        public static readonly int OLAP_DIMENSIONAL_STRUCTURE = 1;
        private Repository savedRepository = null; // for comparing against the version when exiting
        private Repository emptyRepository = null;
        private Repository latestSavedRepository = null;
        private Object repositoryUpdateLock = new Object();
        private string currentFilename = null;
        public bool fileOpened = false;
        private string defaultWindowTitle = "Birst Administration Console";
        private string adminConfigFileDirectory = null;
        private string adminConfigFile = null;
        private AppConfig appconfig = new AppConfig();
        internal LogicalExpressionList logicalExpressionList;
        private bool requiresPublish = false;
        private bool requiresRebuild = false;
        public string[][] DynamicGroups;
        public bool hybrid = false;         // true if this repository has been edited via ths Birst Admin Tool (i.e., cracked the box)
        public bool disablePublishLock = false;
        public bool disableImpliedGrainsProcessing = false;
        public int MaxPhysicalTableNameLength = 120;
        public int MaxPhysicalColumnNameLength = -1;
        public bool UseNewETLSchemeForIB = true;
        private bool inUberTransaction = false;
        public bool EnableLiveAccessToDefaultConnection = false;
        public bool AllowExternalIntegrations = false;
        public int ExternalIntegrationTimeout = -1;
        public PackageImport[] Imports;
        public bool SkipDBAConnectionMerge;
        public int DimensionalStructure = RELATIONAL_DIMENSIONAL_STRUCTURE;
        public bool IsPartitioned = false;
        public WarehousePartition[] WarehousePartitions;
        public PartitionConnectionLoadInfo[] PartitionConnectionLoadInfoList;
        private Dictionary<string, PartitionConnectionLoadInfo> PartitionConnectionLoadInfoMap;
        public bool AllowNonUniqueGrains = false;
        public bool LogCacheKey = false;

        private string repositoryDirectory;
        private ParamsList paramsList;

        public Performance perf;
        public SuccessModels smodels;
        public SuccessPatterns patterns;
        public Hierarchies hmodule;
        public VirtualColumns vcolumns;
        public UsersAndGroups usersAndGroups;
        public Filters filterMod;
        public ConnectionForm connection;
        public ModelParameterModule modelMod;
        public SourceFiles sourceFileMod;
        public StagingTables stagingTableMod;
        public StagingFilters stagingFilterMod;
        public LoadGroups loadGroupMod;
        public Scripts scriptGroupMod;
        public BuildPropertiesModule buildPropertiesModule;
        public ReferencePopulations refPopModule;
        public Aggregates aggModule;
        public Operations operationsModule;
        public SubjectAreas subjectAreaMod;
        public ServerProperties serverPropertiesModule;
        public Variables variableMod;
        private RepositoryModule[] modules;
        public List<QuickDashboard> quickDashboards;
        public List<CustomDrill> customDrills;
        public string defaultConnection;
        public TimeDefinition timeDefinition = new TimeDefinition();
        public List<MeasureTable> measureTablesList;
        public List<DimensionTable> dimensionTablesList;
        public List<DimensionTable> unmappedDimensionTablesList;
        public List<Dimension> dimensionsList;
        public DataTable measureColumnsTable;
        public DataTable dimensionColumns;
        public Dictionary<string, List<MeasureGrain>> unmappedMeasureGrainsByColumnName;
        public List<HierarchyLevel[]> dependencies;
        public RServer RServer;
        public DataView measureColumnNamesView;
        public DataView measureColumnMappingsNamesView;
        public DataView measureColumnMappingsTablesView;
        public DataView dimensionColumnNamesView;
        public DataView dimensionColumnMappingsView;
        public DataView dimensionColumnMappingsTablesView;
        public DataView joinsView;
        public DataView joinsFirstView;
        private System.Windows.Forms.MainMenu adminMainMenu;
        private System.Windows.Forms.TabPage measuresTabPage;
        private System.Windows.Forms.TabPage dimensionsTabPage;
        public TabControl mainTabControl;
        private System.Windows.Forms.TabPage mainTabPage;
        public DataSet schemaDataSet;
        private System.Windows.Forms.MenuItem fileMenuItem;
        private System.Windows.Forms.MenuItem aboutMenuItem;
        private System.Windows.Forms.MenuItem loadMenuItem;
        private System.Windows.Forms.Label label2;
        public TextBox appName;
        private System.Windows.Forms.Label label3;
        public ComboBox targetDimension;
        private System.Windows.Forms.Label label5;
        public ComboBox goalMeasure;
        public TreeView mainTree;
        public DataTable measureTables;
        public int builderVersion;
        public int currentReferenceID;
        private System.Data.DataColumn measureColumnName;
        private System.Data.DataColumn measureDataType;
        private System.Data.DataColumn measurePerformanceCategory;
        private System.Data.DataColumn dimensionColumnDimensionName;
        private System.Data.DataColumn dimensionColumnName;
        private System.Data.DataColumn dimensionColumnDataType;
        private System.Data.DataColumn dimensionColumnWidth;
        private System.Data.DataColumn dimensionColumnKey;
        private System.Data.DataColumn measureTableName;
        private System.Data.DataColumn measureAggregationRule;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox measureTablePName;
        private System.Windows.Forms.MenuItem addMeasureTable;
        private System.Windows.Forms.MenuItem removeMeasureTable;
        private System.Windows.Forms.ContextMenu measureTableContextMenu;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ContextMenu dimensionTableContextMenu;
        private System.Windows.Forms.MenuItem newDimensionTableMenuItem;
        private System.Windows.Forms.MenuItem removeDimensionTableMenuItem;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox measureTableCardinality;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox dimTableLevel;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox dimensionViewSQL;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.RadioButton dimNormal;
        private System.Windows.Forms.RadioButton dimView;
        private System.Windows.Forms.TextBox measureViewSQL;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.RadioButton measureView;
        private System.Windows.Forms.RadioButton measureNormal;
        private System.Windows.Forms.RadioButton measureDerived;
        private System.Windows.Forms.TabPage joinsTabPage;
        public DataTable Joins;
        private System.Data.DataColumn table1;
        private System.Data.DataColumn table2;
        private System.Data.DataColumn joinCondition;
        private System.Data.DataColumn keyColumn;
        private System.Data.DataColumn dimFormatColumn;
        private System.Data.DataColumn measureFormatColumn;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn7;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn1;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn13;
        private System.Windows.Forms.DataGridBoolColumn dataGridBoolColumn3;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn15;
        private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn17;
        public static string SecretPassword = "kashdfoiwuehrqweinauvnaweu";
        private System.Windows.Forms.MenuItem optionsMenuItem;
        private System.Windows.Forms.MenuItem serverConnMenuItem;
        private System.Windows.Forms.MenuItem importMenuItem;
        private System.Windows.Forms.MenuItem toolsMenuItem;
        private System.Windows.Forms.MenuItem copyTableMenuItem;
        private System.Windows.Forms.ComboBox measureInheritBox;
        private System.Windows.Forms.TextBox measureInheritPrefix;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.CheckBox inheritMeasures;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox dimensionInheritPrefix;
        private System.Windows.Forms.ComboBox dimensionInheritBox;
        private System.Windows.Forms.MenuItem replaceMenuItem;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.RadioButton inheritDimensionColumns;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.ComboBox virtualMeasuresInheritBox;
        private System.Windows.Forms.RadioButton inheritWithVirtualMeasures;
        private System.Windows.Forms.TextBox virtualMeasuresPrefix;
        private System.Windows.Forms.MenuItem exportMenuItem;
        private System.Windows.Forms.MenuItem exportMeasureListMenuItem;
        public SaveFileDialog exportSaveFileDialog;
        private System.Windows.Forms.MenuItem dimensionColumnListMenuItem;
        private System.Data.DataColumn measureImprove;
        private System.Data.DataColumn measureMinimum;
        private System.Data.DataColumn measureMaximum;
        private System.Data.DataColumn measureOpportunity;
        private System.Data.DataColumn measureMinImprovement;
        private System.Data.DataColumn measureMaxImprovement;
        private System.Data.DataColumn measureEnforceLimits;
        private System.Data.DataColumn measureAttributeWeight;
        private System.Data.DataColumn measureMinTarget;
        private System.Data.DataColumn measureMaxTarget;
        private System.Data.DataColumn measureValid;
        private System.Data.DataColumn dimMeasureExpression;
        private System.Data.DataColumn dimDesiredValues;
        private System.Data.DataColumn dimIgnoredValues;
        private System.Data.DataColumn dimScore;
        private System.Data.DataColumn dimBlockingGroup;
        private System.Data.DataColumn dimMaskable;
        private System.Windows.Forms.MenuItem validateTargetsMenuItem;
        private System.Windows.Forms.RadioButton noDimtableInheritance;
        private System.Data.DataColumn measureAnalysis;
        private System.Data.DataColumn segmentMeasure;
        private DataColumn redundantJoin;
        private DataColumn autoGeneratedJoin;
        private DataColumn federatedJoin;
        private DataColumn federatedJoinKeys;
        private TextBox dimensionTablePName;
        private Button defineTableSource;
        private Button defineMeasureTableSource;
        private Button defineDSubstitutions;
        private Button defineMSubstitutions;
        private Label label1;
        private Label label4;
        private DataGridView measureDataGridView;
        private BindingSource measureColumnSource;
        private DataGridView dimDataGridView;
        private BindingSource dimensionColumnSource;
        private DataGridView joinDataGridView;
        private BindingSource joinSource;
        private DataColumn displayFunctionColumn;
        private DataColumn joinType;
        private DataColumn dimRuleColumn;
        private ContextMenuStrip measureTableMenu;
        private ToolStripMenuItem newMeasureTableDefinitionToolStripMenuItem;
        private ToolStripMenuItem removeMeasureTableDefinitionToolStripMenuItem;
        private ContextMenuStrip dimensionTableMenu;
        private ToolStripMenuItem newDimensionTableMenuStripItem;
        private ToolStripMenuItem removeDimensionTableMenuStripItem;
        private ImageList mainTreeImageList;
        private OpenFileDialog openRepositoryDialog;
        private SaveFileDialog saveRepositoryDialog;
        private MenuItem exitMenuItem;
        private ComboBox dimConnectionBox;
        private Label label6;
        private ComboBox mtConnectionBox;
        private Label label7;
        private CheckBox dimCacheable;
        private CheckBox mtCacheable;
        private MenuItem mainLoadMenuItem;
        private MenuItem mainSaveMenuItem;
        private MenuItem findMenuItem;
        private DataColumn measureDescription;
        private DataColumn dimDescription;
        private DataColumn dimVolatileKey;
        private Label label8;
        private CheckedListBox dimensionContentFilters;
        private Label label9;
        private CheckedListBox measureContentFilters;
        private DataColumn levelColumn;
        private MenuItem mainSaveAsMenuItem;
        private MenuItem menuItem1;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        public DataTable dimensionColumnMappings;
        private DataColumn dimensionColumnTableName;
        private DataColumn dimMappingColumnName;
        private DataColumn dimensionColumnPhysicalName;
        private DataColumn dimQualify;
        private DataGridView dimColMappingGridView;
        private BindingSource dimensionColumnMappingSource;
        private ToolStripMenuItem newDimensionToolStripMenuItem;
        private ToolStripMenuItem removeDimensionToolStripMenuItem;
        public DataTable measureColumnMappings;
        private DataColumn dataColumn1;
        private DataGridView measureMapGridView;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private BindingSource measureColumnMappingSource;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataGridViewComboBoxColumn measureComboBoxColumn;
        private DataGridViewTextBoxColumn TableName;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private DataGridViewComboBoxColumn AggRuleOverride;
        private ToolStripMenuItem viewUnmappedToolStripMenuItem;
        private ToolStripMenuItem viewMeasureTableGrainToolStripMenuItem;
        private ToolStripMenuItem viewUnmappedMeasuresToolStripMenuItem;
        private ContextMenuStrip measureContextMenu;
        private ToolStripMenuItem viewMeasureGrainToolStripMenuItem;
        private MenuItem buildApplicationMenuItem;
        private GroupBox dimModelBox;
        private RadioButton successButton;
        private RadioButton performanceButton;
        private RadioButton peerButton;
        private RadioButton noneButton;
        private GroupBox measureModelBox;
        private RadioButton measureSuccess;
        private RadioButton measurePerformance;
        private RadioButton measurePeers;
        private RadioButton measureNone;
        private IContainer components;
        private MenuItem menuItem5;
        private MenuItem menuItemMRU;
        private DataColumn dataColumn8;
        private MenuItem timePropertiesItem;
        private DataColumn dataColumn9;
        private DataColumn dataColumn10;
        private MenuItem cleanApplication;
        private MenuItem menuItem6;
        private MenuItem menuItem7;
        private DataColumn dataColumn11;
        private DataColumn dataColumn12;
        private DataGridViewButtonColumn dataGridViewButtonColumn1;
        private DataGridViewComboBoxColumn columnNameComboBox;
        private DataGridViewTextBoxColumn PhysicalName;
        private DataGridViewCheckBoxColumn Qualify;
        private DataGridViewCheckBoxColumn autoGenColumn;
        private DataGridViewButtonColumn dataGridViewButtonColumn2;
        private DataGridViewButtonColumn dataGridViewButtonColumn3;
        private DataColumn dataColumn13;
        private DataGridViewButtonColumn dataGridViewButtonColumn4;
        private DataGridViewButtonColumn dataGridViewButtonColumn5;
        private DataColumn dataColumn14;
        private DataGridViewButtonColumn dataGridViewButtonColumn6;
        private DataGridViewButtonColumn dataGridViewButtonColumn7;
        private Label sourceCodeVersionLabel;
        private Label label10;
        private Label lastEditLabel;
        private Label label12;
        private DataGridViewButtonColumn dataGridViewButtonColumn8;
        private DataGridViewButtonColumn dataGridViewButtonColumn9;
        private DataGridViewButtonColumn dataGridViewButtonColumn10;
        private DataGridViewButtonColumn dataGridViewButtonColumn11;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataGridViewButtonColumn dataGridViewButtonColumn12;
        private DataGridViewButtonColumn dataGridViewButtonColumn13;
        private DataGridViewButtonColumn dataGridViewButtonColumn14;
        private DataGridViewButtonColumn dataGridViewButtonColumn15;
        private DataGridViewButtonColumn dataGridViewButtonColumn16;
        private DataGridViewButtonColumn dataGridViewButtonColumn17;
        private DataGridViewTextBoxColumn columnNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn PhysicalNameCol;
        private DataGridViewTextBoxColumn Description;
        private DataGridViewComboBoxColumn dataTypeDataGridViewTextBoxColumn;
        private DataGridViewButtonColumn aggregationRuleColumn;
        private DataGridViewTextBoxColumn DimensionRules;
        private DataGridViewComboBoxColumn DisplayFunction;
        private DataGridViewCheckBoxColumn keyDataGridViewCheckBoxColumn;
        private DataGridViewTextBoxColumn formatDataGridViewTextBoxColumn;
        private DataGridViewButtonColumn filtersColumn;
        private DataGridViewTextBoxColumn performanceCategoryDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn improveDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn enforceLimitsDataGridViewCheckBoxColumn;
        private DataGridViewTextBoxColumn minimumDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maximumDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn minImprovementDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maxImprovementDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn attributeWeightMeasureDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn minTargetDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn maxTargetDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn validMeasureDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn analysisMeasureDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn segmentMeasureDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn opportunityDataGridViewTextBoxColumn;
        private DataGridViewButtonColumn dataGridViewButtonColumn18;
        private DataGridViewButtonColumn dataGridViewButtonColumn19;
        private DataGridViewButtonColumn dataGridViewButtonColumn20;
        private DataGridViewButtonColumn dataGridViewButtonColumn21;
        private DataColumn dataColumn17;
        private DataGridViewComboBoxColumn table1Column;
        private DataGridViewComboBoxColumn table2Column;
        private DataGridViewTextBoxColumn joinConditionDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn joinTypeDataGridViewComboBoxColumn;
        private DataGridViewCheckBoxColumn redundantDataGridViewCheckBoxColumn;
        private DataGridViewCheckBoxColumn federatedDataGridViewCheckBoxColumn;
        private DataGridViewTextBoxColumn federatedJoinKeysDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn Level;
        private DataGridViewCheckBoxColumn AutoGenerated;
        private DataGridViewCheckBoxColumn Invalid;
        private DataColumn dataColumn18;
        private DataColumn dataColumn19;
        private DataGridViewTextBoxColumn dimColumnName;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewComboBoxColumn dataTypeDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn formatDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn ColWidth;
        private DataGridViewCheckBoxColumn keyDataGridViewCheckBoxColumn1;
        private DataGridViewCheckBoxColumn Index;
        private DataGridViewTextBoxColumn GenericName;
        private DataGridViewTextBoxColumn UnknownValue;
        private DataGridViewTextBoxColumn VolatileKey;
        private DataGridViewTextBoxColumn measureExpressionDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn desiredValuesDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn ignoredValuesDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn scoreDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn blockingGroupDataGridViewTextBoxColumn;
        private DataGridViewComboBoxColumn maskableTypeDataGridViewTextBoxColumn1;
        private DataGridViewButtonColumn DisplayMap;
        private DataGridViewButtonColumn dataGridViewButtonColumn22;
        private DataGridViewButtonColumn dataGridViewButtonColumn23;
        private DataColumn dataColumn20;
        private DataColumn dataColumn21;
        private MRUManager mruManager;
        private static bool Headless = false;
        private ToolTip toolTip;
        private CheckBox recalculateCardinality;
        private DataColumn dataColumn22;
        private DataColumn dataColumn23;
        public CheckBox DoNotAllowBirstToRebuildApplicationCheckBox;
        private Label RepositoryVersion;
        private Label label13;
        private Label AppBuilderVersion;
        private Label label14;
        private CheckBox RequiresPublishBox;
        public TextBox QueryLanguageVersionText;
        private Label label11;
        private DataColumn dataColumn24;        
        private static ILog Logger = null;
        public TextBox NumThresholdFailedRecordsBox;
        private DataColumn dataColumn26;
        private DataColumn dataColumn25;
        private DataColumn dataColumn27;
        private DataColumn dataColumn28;
        public TextBox maxYearText;
        public TextBox minYearText;
        private Label label16;
        private DataColumn dataColumn29;
        private Label label15;

        private bool LiveAccessSecurityFilter;
        
        public MainAdminForm()
        {
            //
            // Required for Windows Form Designer support
            //
            Control.CheckForIllegalCrossThreadCalls = false;
            this.Visible = false;
            InitializeComponent();
            this.Visible = false;
            this.Text = defaultWindowTitle;
            measureColumnsTable.CaseSensitive = true;
            measureColumnNamesView = new DataView(measureColumnsTable, null, "ColumnName",
                                    DataViewRowState.CurrentRows);
            measureColumnMappings.CaseSensitive = true;
            measureColumnMappingsNamesView = new DataView(measureColumnMappings, null, "ColumnName",
                                    DataViewRowState.CurrentRows);
            measureColumnMappingsTablesView = new DataView(measureColumnMappings, null, "TableName",
                                    DataViewRowState.CurrentRows);
            dimensionColumns.CaseSensitive = true;
            dimensionColumnNamesView = new DataView(dimensionColumns, null, "DimensionName,ColumnName",
                                    DataViewRowState.CurrentRows);
            dimensionColumnMappings.CaseSensitive = true;
            dimensionColumnMappingsView = new DataView(dimensionColumnMappings, null, "ColumnName,TableName",
                                    DataViewRowState.CurrentRows);
            dimensionColumnMappingsTablesView = new DataView(dimensionColumnMappings, null, "TableName",
                                    DataViewRowState.CurrentRows);
            joinsView = new DataView(Joins, null, "table1,table2",
                                    DataViewRowState.CurrentRows);
            joinsFirstView = new DataView(Joins, null, "table1",
                                    DataViewRowState.CurrentRows);
            refPopModule = new ReferencePopulations(this);
            aggModule = new Aggregates(this);
            operationsModule = new Operations(this);
            perf = new Performance(this);
            smodels = new SuccessModels(this);
            patterns = new SuccessPatterns(this);
            hmodule = new Hierarchies(this);
            vcolumns = new VirtualColumns(this);
            usersAndGroups = new UsersAndGroups(this);
            filterMod = new Filters(this);
            modelMod = new ModelParameterModule(this);
            connection = new ConnectionForm(this, appconfig, usersAndGroups);
            sourceFileMod = new SourceFiles(this);
            stagingTableMod = new StagingTables(this);
            stagingFilterMod = new StagingFilters(this);
            loadGroupMod = new LoadGroups(this);
            scriptGroupMod = new Scripts(this);
            subjectAreaMod = new SubjectAreas(this);
            serverPropertiesModule = new ServerProperties();
            variableMod = new Variables(this);
            buildPropertiesModule = new BuildPropertiesModule(this);
            paramsList = new ParamsList(this);
            modules = new RepositoryModule[] { refPopModule , perf, smodels,
                patterns, new Outcomes(this),variableMod, subjectAreaMod, hmodule,
                vcolumns, usersAndGroups, modelMod, filterMod,
                aggModule, connection, serverPropertiesModule, new Resources(), new Broadcasts(this),
                operationsModule, sourceFileMod, stagingTableMod, stagingFilterMod, loadGroupMod, scriptGroupMod, buildPropertiesModule};
            foreach (RepositoryModule rm in modules)
            {
                mainTabControl.TabPages.Add(rm.TabPage);
            }

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            foreach (TreeNode n in mainTree.Nodes) n.Expand();
            mainTabControl.Top = -21;
            loadConfig();
            this.Menu = adminMainMenu;
        }

        public void setHeadless(bool headless)
        {
            Headless = headless;
        }

        public bool isHeadless()
        {
            return Headless;
        }

        public void setLogger(ILog logger)
        {
            Logger = logger;                        
        }

        public ILog getLogger()
        {
            return Logger;
        }

        /* 
         * if popping up messagebox, use frm=null 
         * when showing form as dialog box use default values for caption="", buttons=MessageBoxButtons.OK, icon=MessageBoxIcon.None
         * and supply form object as frm.
         * Headless mode will log as "Using default value '<defaultSel>.ToString()' for Admin Tool Message '<message>.'
         * Hence, use message in such a manner that makes sense when appended to log and supply defaultSel that represents the 
         * default behaviour of application.
         */
        public DialogResult reportMessage(System.Windows.Forms.Form frm, IWin32Window owner, string message, DialogResult defaultSel, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            if (Headless)
            {
            	// causing too many log messages
                // Logger.Info("Using default value \"" + defaultSel.ToString() + "\" for Admin Tool Message \"" + message + ".\"");
                return defaultSel;                
            }
            else
            {
                if (frm == null)//Show MessageBox
                    return MessageBox.Show(owner, message, caption, buttons, icon);
                else//Show Form as DialogBox
                    return frm.ShowDialog(owner);                 
            }            
        }

        public TreeNode getMainNode(string name)
        {
            foreach (TreeNode tn in mainTree.Nodes)
            {
                foreach (TreeNode tn2 in tn.Nodes)
                {
                    if (tn2.Text == name) return (tn2);
                }
            }
            return (null);
        }

        public TreeNode dimensionTablesNode = null;
        TreeNode measureTablesNode = null;
        TreeNode baseNode = null;
        TreeNode derivedNode = null;

        public void setupTree()
        {
            dimensionTablesNode = getMainNode("Dimensions");
            dimensionTablesNode.Nodes.Clear();
            if (dimensionsList != null)
                foreach (Dimension d in dimensionsList)
                {
                    TreeNode dtn = new TreeNode(d.Name);
                    dtn.ImageIndex = 7;
                    dtn.SelectedImageIndex = 7;
                    dimensionTablesNode.Nodes.Add(dtn);
                }
            if (dimensionTablesList != null)
                foreach (DimensionTable dt in (dimensionTablesList))
                {
                    TreeNode dtn = null;
                    Dimension dim = null;
                    foreach (Dimension d in dimensionsList)
                    {
                        if (d.Name == dt.DimensionName)
                        {
                            dim = d;
                            break;
                        }
                    }
                    if (dim == null)
                    {
                        dtn = new TreeNode(dt.DimensionName);
                        dtn.ImageIndex = 7;
                        dtn.SelectedImageIndex = 7;
                        dimensionTablesNode.Nodes.Add(dtn);
                        dim = new Dimension(dt.DimensionName);
                        dimensionsList.Add(dim);
                    }
                    else
                    {
                        int i = 0;
                        for (; i < dimensionTablesNode.Nodes.Count; i++)
                        {
                            if (dimensionTablesNode.Nodes[i].Text == dt.DimensionName)
                            {
                                dtn = dimensionTablesNode.Nodes[i];
                                break;
                            }
                        }
                    }
                    TreeNode tn = new TreeNode(dt.TableName);
                    tn.ImageIndex = 1;
                    tn.SelectedImageIndex = 1;
                    dtn.Nodes.Add(tn);
                    if (dt.AutoGenerated)
                    {
                        tn.ForeColor = Color.Blue;
                    }
                }
            measureTablesNode = getMainNode("Measures");
            baseNode = measureTablesNode.Nodes["BaseNode"];
            derivedNode = measureTablesNode.Nodes["DerivedNode"];
            baseNode.Nodes.Clear();
            derivedNode.Nodes.Clear();
            if (measureTablesList != null)
                foreach (MeasureTable mt in (measureTablesList))
                {
                    if (mt.TableName != null)
                    {
                        TreeNode tn = new TreeNode(mt.TableName);
                        tn.ImageIndex = 1;
                        tn.SelectedImageIndex = 1;
                        if (mt.Type != 2)
                            baseNode.Nodes.Add(tn);
                        if (mt.AutoGenerated)
                        {
                            tn.ForeColor = Color.Blue;
                        }
                    }
                }
            bool previousInUberTransaction = inUberTransaction;
            setInUberTransaction(true);
            foreach (RepositoryModule rm in modules)
            {
                rm.setupNodes(getMainNode(rm.ModuleName));
            }
            inUberTransaction = previousInUberTransaction;
        }

        private void loadConfig()
        {
            try
            {
                string userAppDataPathOld = Application.UserAppDataPath;
                string version = Application.ProductVersion;
                string newVersion = Version.majorMinorNumber;
                string userAppDataPath = userAppDataPathOld.Replace(version, newVersion);

                adminConfigFileDirectory = userAppDataPath;

                adminConfigFile = adminConfigFileDirectory + "\\admin.cfg";
                loadConfigFile(adminConfigFile);
            }
            catch (Exception)
            {
            }
        }

        private void saveConfig()
        {
            appconfig.mWindowLocation = new Point(this.Location.X, this.Location.Y);
            appconfig.mWindowSize = new Size(this.Size.Width, this.Size.Height);
            appconfig.mWindowState = this.WindowState;

            if (!Directory.Exists(adminConfigFileDirectory)) Directory.CreateDirectory(adminConfigFileDirectory);
            saveConfigFile(adminConfigFile);
        }

        private void loadConfigFile(string fName)
        {
            if (fName != null && File.Exists(fName))
            {
                StreamReader sreader = null;
                XmlReader xreader = null;
                try
                {
                    sreader = new StreamReader(fName, Encoding.UTF8);
                    xreader = Util.getSecureXMLReader(sreader);
                    XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
                    AppConfig getappconfig = (AppConfig)serializer.Deserialize(xreader);
                    appconfig.ServerConnectionURL = getappconfig.ServerConnectionURL;
                    appconfig.MRUItems = (string[])getappconfig.MRUItems.Clone();
                    appconfig.mWindowLocation = getappconfig.mWindowLocation;
                    appconfig.mWindowSize = getappconfig.mWindowSize;
                    appconfig.mWindowState = getappconfig.mWindowState;
                    appconfig.mPopupWindowStatus = (AppConfig.PopupWindowStatus[])getappconfig.mPopupWindowStatus.Clone();
                }
                finally
                {
                    if (xreader != null)
                        xreader.Close();
                    if (sreader != null)
                        sreader.Close();
                }
            }
            else
            {
                appconfig.ServerConnectionURL = "http://";
                appconfig.MRUItems = new string[0];
                appconfig.mWindowLocation = new Point(this.Location.X, this.Location.Y);
                appconfig.mWindowSize = new Size(this.Size.Width, this.Size.Height);
                appconfig.mWindowState = this.WindowState;
            }
        }

        private void saveConfigFile(string fName)
        {
            TextWriter writer = new StreamWriter(fName);
            XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
            serializer.Serialize(writer, appconfig);
            writer.Close();
        }

        private void onload(object sender, System.EventArgs e)
        {
            mainTreeRight = mainTree.Right;
            setupBlankRepository();
            setupTree();
            emptyRepository = getRepository();

            mruManager = new MRUManager();
            mruManager.Initialize(this, menuItemMRU, appconfig.MRUItems);


            if (appconfig.mWindowLocation != Point.Empty) this.Location = new Point(appconfig.mWindowLocation.X, appconfig.mWindowLocation.Y);
            if (appconfig.mWindowSize != Size.Empty) this.Size = new System.Drawing.Size(appconfig.mWindowSize.Width, appconfig.mWindowSize.Height);
            this.WindowState = appconfig.mWindowState;

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAdminForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("General", 6, 6);
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Reference Populations", 6, 6);
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Performance", 6, 6);
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Model Datasets", 17, 17);
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Success Patterns", 18, 18);
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Outcomes", 19, 19);
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Filters", 10, 10);
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Operations", 21, 21);
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Business Modeling", 3, 3, new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Base", 0, 0);
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Derived", 0, 0);
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Measures", 1, 1, new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Dimensions", 1, 1);
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Joins");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Virtual Columns", 20, 20);
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Hierarchies", 7, 7);
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Aggregates", 8, 8);
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Subject Areas", 6, 6);
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Dimensional Mapping", 2, 2, new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17,
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Data Sources", 26, 26);
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Staging Tables", 27, 27);
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Staging Filters", 10, 10);
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Load Groups", 28, 28);
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Script Groups", 30, 30);
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Build Properties", 31, 31);
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Warehouse", 25, 25, new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25});
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Database Connection", 11, 11);
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Model Parameters", 12, 12);
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("Server Properties", 13, 13);
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("Users and Groups", 14, 14);
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Variables", 15, 15);
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Resources", 16, 16);
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("Broadcasts", 29, 29);
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("System", 4, 4, new System.Windows.Forms.TreeNode[] {
            treeNode27,
            treeNode28,
            treeNode29,
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33});
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Runtime", 5, 5);
            this.adminMainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.mainLoadMenuItem = new System.Windows.Forms.MenuItem();
            this.loadMenuItem = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mainSaveMenuItem = new System.Windows.Forms.MenuItem();
            this.mainSaveAsMenuItem = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItemMRU = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.importMenuItem = new System.Windows.Forms.MenuItem();
            this.exportMenuItem = new System.Windows.Forms.MenuItem();
            this.exportMeasureListMenuItem = new System.Windows.Forms.MenuItem();
            this.dimensionColumnListMenuItem = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.exitMenuItem = new System.Windows.Forms.MenuItem();
            this.toolsMenuItem = new System.Windows.Forms.MenuItem();
            this.copyTableMenuItem = new System.Windows.Forms.MenuItem();
            this.findMenuItem = new System.Windows.Forms.MenuItem();
            this.replaceMenuItem = new System.Windows.Forms.MenuItem();
            this.validateTargetsMenuItem = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.timePropertiesItem = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.buildApplicationMenuItem = new System.Windows.Forms.MenuItem();
            this.cleanApplication = new System.Windows.Forms.MenuItem();
            this.optionsMenuItem = new System.Windows.Forms.MenuItem();
            this.serverConnMenuItem = new System.Windows.Forms.MenuItem();
            this.aboutMenuItem = new System.Windows.Forms.MenuItem();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.mainTabPage = new System.Windows.Forms.TabPage();
            this.maxYearText = new System.Windows.Forms.TextBox();
            this.minYearText = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.NumThresholdFailedRecordsBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.QueryLanguageVersionText = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.RequiresPublishBox = new System.Windows.Forms.CheckBox();
            this.AppBuilderVersion = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.RepositoryVersion = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.DoNotAllowBirstToRebuildApplicationCheckBox = new System.Windows.Forms.CheckBox();
            this.lastEditLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sourceCodeVersionLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.goalMeasure = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.targetDimension = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.appName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dimensionsTabPage = new System.Windows.Forms.TabPage();
            this.dimDataGridView = new System.Windows.Forms.DataGridView();
            this.dimColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.formatDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keyDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Index = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.GenericName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnknownValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VolatileKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.measureExpressionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.desiredValuesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ignoredValuesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scoreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.blockingGroupDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maskableTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.DisplayMap = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dimensionColumnSource = new System.Windows.Forms.BindingSource(this.components);
            this.schemaDataSet = new System.Data.DataSet();
            this.measureColumnsTable = new System.Data.DataTable();
            this.measureColumnName = new System.Data.DataColumn();
            this.measureDescription = new System.Data.DataColumn();
            this.measureDataType = new System.Data.DataColumn();
            this.measurePerformanceCategory = new System.Data.DataColumn();
            this.measureAggregationRule = new System.Data.DataColumn();
            this.keyColumn = new System.Data.DataColumn();
            this.measureFormatColumn = new System.Data.DataColumn();
            this.measureImprove = new System.Data.DataColumn();
            this.measureMinimum = new System.Data.DataColumn();
            this.measureMaximum = new System.Data.DataColumn();
            this.measureOpportunity = new System.Data.DataColumn();
            this.measureMinImprovement = new System.Data.DataColumn();
            this.measureMaxImprovement = new System.Data.DataColumn();
            this.measureEnforceLimits = new System.Data.DataColumn();
            this.measureAttributeWeight = new System.Data.DataColumn();
            this.measureMinTarget = new System.Data.DataColumn();
            this.measureMaxTarget = new System.Data.DataColumn();
            this.measureValid = new System.Data.DataColumn();
            this.measureAnalysis = new System.Data.DataColumn();
            this.segmentMeasure = new System.Data.DataColumn();
            this.displayFunctionColumn = new System.Data.DataColumn();
            this.dimRuleColumn = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dimensionColumns = new System.Data.DataTable();
            this.dimensionColumnDimensionName = new System.Data.DataColumn();
            this.dimensionColumnName = new System.Data.DataColumn();
            this.dimDescription = new System.Data.DataColumn();
            this.dimVolatileKey = new System.Data.DataColumn();
            this.dimensionColumnDataType = new System.Data.DataColumn();
            this.dimensionColumnWidth = new System.Data.DataColumn();
            this.dimensionColumnKey = new System.Data.DataColumn();
            this.dimFormatColumn = new System.Data.DataColumn();
            this.dimMeasureExpression = new System.Data.DataColumn();
            this.dimDesiredValues = new System.Data.DataColumn();
            this.dimIgnoredValues = new System.Data.DataColumn();
            this.dimScore = new System.Data.DataColumn();
            this.dimBlockingGroup = new System.Data.DataColumn();
            this.dimMaskable = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.measureTables = new System.Data.DataTable();
            this.measureTableName = new System.Data.DataColumn();
            this.Joins = new System.Data.DataTable();
            this.table1 = new System.Data.DataColumn();
            this.table2 = new System.Data.DataColumn();
            this.joinCondition = new System.Data.DataColumn();
            this.redundantJoin = new System.Data.DataColumn();
            this.federatedJoin = new System.Data.DataColumn();
            this.federatedJoinKeys = new System.Data.DataColumn();
            this.joinType = new System.Data.DataColumn();
            this.levelColumn = new System.Data.DataColumn();
            this.autoGeneratedJoin = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dimensionColumnMappings = new System.Data.DataTable();
            this.dimensionColumnTableName = new System.Data.DataColumn();
            this.dimMappingColumnName = new System.Data.DataColumn();
            this.dimensionColumnPhysicalName = new System.Data.DataColumn();
            this.dimQualify = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.measureColumnMappings = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.dimensionContentFilters = new System.Windows.Forms.CheckedListBox();
            this.dimCacheable = new System.Windows.Forms.CheckBox();
            this.dimConnectionBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dimTableLevel = new System.Windows.Forms.ComboBox();
            this.defineTableSource = new System.Windows.Forms.Button();
            this.dimView = new System.Windows.Forms.RadioButton();
            this.dimNormal = new System.Windows.Forms.RadioButton();
            this.label81 = new System.Windows.Forms.Label();
            this.dimensionViewSQL = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.dimensionTablePName = new System.Windows.Forms.TextBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.virtualMeasuresPrefix = new System.Windows.Forms.TextBox();
            this.defineDSubstitutions = new System.Windows.Forms.Button();
            this.dimensionInheritPrefix = new System.Windows.Forms.TextBox();
            this.label149 = new System.Windows.Forms.Label();
            this.inheritWithVirtualMeasures = new System.Windows.Forms.RadioButton();
            this.virtualMeasuresInheritBox = new System.Windows.Forms.ComboBox();
            this.dimensionInheritBox = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this.noDimtableInheritance = new System.Windows.Forms.RadioButton();
            this.inheritDimensionColumns = new System.Windows.Forms.RadioButton();
            this.dimColMappingGridView = new System.Windows.Forms.DataGridView();
            this.columnNameComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PhysicalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qualify = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.autoGenColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dimensionColumnMappingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dimModelBox = new System.Windows.Forms.GroupBox();
            this.successButton = new System.Windows.Forms.RadioButton();
            this.performanceButton = new System.Windows.Forms.RadioButton();
            this.peerButton = new System.Windows.Forms.RadioButton();
            this.noneButton = new System.Windows.Forms.RadioButton();
            this.measuresTabPage = new System.Windows.Forms.TabPage();
            this.measureDataGridView = new System.Windows.Forms.DataGridView();
            this.columnNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhysicalNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aggregationRuleColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DimensionRules = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayFunction = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.keyDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.formatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.performanceCategoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.improveDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enforceLimitsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.minimumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maximumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minImprovementDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxImprovementDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.attributeWeightMeasureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minTargetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxTargetDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.validMeasureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.analysisMeasureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.segmentMeasureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opportunityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filtersColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.measureContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewMeasureGrainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measureColumnSource = new System.Windows.Forms.BindingSource(this.components);
            this.recalculateCardinality = new System.Windows.Forms.CheckBox();
            this.measureMapGridView = new System.Windows.Forms.DataGridView();
            this.measureComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AggRuleOverride = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.measureColumnMappingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.measureContentFilters = new System.Windows.Forms.CheckedListBox();
            this.mtCacheable = new System.Windows.Forms.CheckBox();
            this.mtConnectionBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.defineMSubstitutions = new System.Windows.Forms.Button();
            this.defineMeasureTableSource = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.measureInheritPrefix = new System.Windows.Forms.TextBox();
            this.measureInheritBox = new System.Windows.Forms.ComboBox();
            this.inheritMeasures = new System.Windows.Forms.CheckBox();
            this.measureDerived = new System.Windows.Forms.RadioButton();
            this.measureView = new System.Windows.Forms.RadioButton();
            this.measureNormal = new System.Windows.Forms.RadioButton();
            this.measureViewSQL = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.measureTableCardinality = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.measureTablePName = new System.Windows.Forms.TextBox();
            this.measureModelBox = new System.Windows.Forms.GroupBox();
            this.measureSuccess = new System.Windows.Forms.RadioButton();
            this.measurePerformance = new System.Windows.Forms.RadioButton();
            this.measurePeers = new System.Windows.Forms.RadioButton();
            this.measureNone = new System.Windows.Forms.RadioButton();
            this.joinsTabPage = new System.Windows.Forms.TabPage();
            this.joinDataGridView = new System.Windows.Forms.DataGridView();
            this.table1Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.table2Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.joinConditionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.joinTypeDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.redundantDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.federatedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.federatedJoinKeysDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Level = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AutoGenerated = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Invalid = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.joinSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dimensionTableContextMenu = new System.Windows.Forms.ContextMenu();
            this.newDimensionTableMenuItem = new System.Windows.Forms.MenuItem();
            this.removeDimensionTableMenuItem = new System.Windows.Forms.MenuItem();
            this.measureTableContextMenu = new System.Windows.Forms.ContextMenu();
            this.addMeasureTable = new System.Windows.Forms.MenuItem();
            this.removeMeasureTable = new System.Windows.Forms.MenuItem();
            this.mainTree = new System.Windows.Forms.TreeView();
            this.mainTreeImageList = new System.Windows.Forms.ImageList(this.components);
            this.dataGridTextBoxColumn7 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridBoolColumn1 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridTextBoxColumn13 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridBoolColumn3 = new System.Windows.Forms.DataGridBoolColumn();
            this.dataGridTextBoxColumn15 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn17 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.exportSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.measureTableMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newMeasureTableDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMeasureTableDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMeasureTableGrainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewUnmappedMeasuresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dimensionTableMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newDimensionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDimensionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newDimensionTableMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDimensionTableMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewUnmappedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openRepositoryDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveRepositoryDialog = new System.Windows.Forms.SaveFileDialog();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn4 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn5 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn6 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn7 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn8 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn9 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn10 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn11 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn12 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn13 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn14 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn15 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn16 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn17 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn18 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn19 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn20 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn21 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn22 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn23 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainTabControl.SuspendLayout();
            this.mainTabPage.SuspendLayout();
            this.dimensionsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dimDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schemaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumns)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureTables)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Joins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnMappings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnMappings)).BeginInit();
            this.groupBox31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dimColMappingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnMappingSource)).BeginInit();
            this.dimModelBox.SuspendLayout();
            this.measuresTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measureDataGridView)).BeginInit();
            this.measureContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureMapGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnMappingSource)).BeginInit();
            this.measureModelBox.SuspendLayout();
            this.joinsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.joinDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinSource)).BeginInit();
            this.measureTableMenu.SuspendLayout();
            this.dimensionTableMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // adminMainMenu
            // 
            this.adminMainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenuItem,
            this.toolsMenuItem,
            this.optionsMenuItem,
            this.aboutMenuItem});
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mainLoadMenuItem,
            this.menuItem1,
            this.mainSaveMenuItem,
            this.mainSaveAsMenuItem,
            this.menuItem5,
            this.menuItemMRU,
            this.menuItem2,
            this.importMenuItem,
            this.exportMenuItem,
            this.menuItem3,
            this.exitMenuItem});
            this.fileMenuItem.Text = "&File";
            // 
            // mainLoadMenuItem
            // 
            this.mainLoadMenuItem.Index = 0;
            this.mainLoadMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.loadMenuItem});
            this.mainLoadMenuItem.Text = "&Load Repository";
            // 
            // loadMenuItem
            // 
            this.loadMenuItem.Index = 0;
            this.loadMenuItem.Text = "From &File";
            this.loadMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "-";
            // 
            // mainSaveMenuItem
            // 
            this.mainSaveMenuItem.Index = 2;
            this.mainSaveMenuItem.Text = "&Save Repository";
            this.mainSaveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // mainSaveAsMenuItem
            // 
            this.mainSaveAsMenuItem.Index = 3;
            this.mainSaveAsMenuItem.Text = "Save Repository &As...";
            this.mainSaveAsMenuItem.Click += new System.EventHandler(this.saveAsMenuItem_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 4;
            this.menuItem5.Text = "-";
            // 
            // menuItemMRU
            // 
            this.menuItemMRU.Index = 5;
            this.menuItemMRU.Text = "Recent Files";
            this.menuItemMRU.Click += new System.EventHandler(this.menuItemMRU_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 6;
            this.menuItem2.Text = "-";
            // 
            // importMenuItem
            // 
            this.importMenuItem.Index = 7;
            this.importMenuItem.Text = "&Import Table";
            this.importMenuItem.Click += new System.EventHandler(this.importMenuItem_Click);
            // 
            // exportMenuItem
            // 
            this.exportMenuItem.Index = 8;
            this.exportMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.exportMeasureListMenuItem,
            this.dimensionColumnListMenuItem});
            this.exportMenuItem.Text = "&Export";
            // 
            // exportMeasureListMenuItem
            // 
            this.exportMeasureListMenuItem.Index = 0;
            this.exportMeasureListMenuItem.Text = "&Measure List";
            this.exportMeasureListMenuItem.Click += new System.EventHandler(this.exportMeasureListMenuItem_Click);
            // 
            // dimensionColumnListMenuItem
            // 
            this.dimensionColumnListMenuItem.Index = 1;
            this.dimensionColumnListMenuItem.Text = "&Dimension Column List";
            this.dimensionColumnListMenuItem.Click += new System.EventHandler(this.dimensionColumnListMenuItem_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 9;
            this.menuItem3.Text = "-";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Index = 10;
            this.exitMenuItem.Text = "E&xit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // toolsMenuItem
            // 
            this.toolsMenuItem.Index = 1;
            this.toolsMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.copyTableMenuItem,
            this.findMenuItem,
            this.replaceMenuItem,
            this.validateTargetsMenuItem,
            this.menuItem6,
            this.timePropertiesItem,
            this.menuItem7,
            this.buildApplicationMenuItem,
            this.cleanApplication});
            this.toolsMenuItem.Text = "&Tools";
            // 
            // copyTableMenuItem
            // 
            this.copyTableMenuItem.Index = 0;
            this.copyTableMenuItem.Text = "&Copy Table";
            this.copyTableMenuItem.Click += new System.EventHandler(this.copyTableMenuItem_Click);
            // 
            // findMenuItem
            // 
            this.findMenuItem.Index = 1;
            this.findMenuItem.Text = "&Find Column Name";
            this.findMenuItem.Click += new System.EventHandler(this.findMenuItem_Click);
            // 
            // replaceMenuItem
            // 
            this.replaceMenuItem.Index = 2;
            this.replaceMenuItem.Text = "&Replace Name";
            this.replaceMenuItem.Click += new System.EventHandler(this.searchMenuItem_Click);
            // 
            // validateTargetsMenuItem
            // 
            this.validateTargetsMenuItem.Index = 3;
            this.validateTargetsMenuItem.Text = "&Validate Targets";
            this.validateTargetsMenuItem.Click += new System.EventHandler(this.validateTargetsMenuItem_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 4;
            this.menuItem6.Text = "-";
            // 
            // timePropertiesItem
            // 
            this.timePropertiesItem.Index = 5;
            this.timePropertiesItem.Text = "&Time Properties";
            this.timePropertiesItem.Click += new System.EventHandler(this.timePropertiesItem_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 6;
            this.menuItem7.Text = "-";
            // 
            // buildApplicationMenuItem
            // 
            this.buildApplicationMenuItem.Index = 7;
            this.buildApplicationMenuItem.Text = "&Build Application";
            this.buildApplicationMenuItem.Click += new System.EventHandler(this.buildApplication_Click);
            // 
            // cleanApplication
            // 
            this.cleanApplication.Index = 8;
            this.cleanApplication.Text = "C&lean Application";
            this.cleanApplication.Click += new System.EventHandler(this.cleanApplication_Click);
            // 
            // optionsMenuItem
            // 
            this.optionsMenuItem.Index = 2;
            this.optionsMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.serverConnMenuItem});
            this.optionsMenuItem.Text = "&Options";
            // 
            // serverConnMenuItem
            // 
            this.serverConnMenuItem.Index = 0;
            this.serverConnMenuItem.Text = "&Server Connection";
            this.serverConnMenuItem.Click += new System.EventHandler(this.serverConnMenuItem_Click);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Index = 3;
            this.aboutMenuItem.Text = "&About";
            this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // mainTabControl
            // 
            this.mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControl.Controls.Add(this.mainTabPage);
            this.mainTabControl.Controls.Add(this.dimensionsTabPage);
            this.mainTabControl.Controls.Add(this.measuresTabPage);
            this.mainTabControl.Controls.Add(this.joinsTabPage);
            this.mainTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainTabControl.Location = new System.Drawing.Point(225, 0);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(860, 753);
            this.mainTabControl.TabIndex = 0;
            this.mainTabControl.SelectedIndexChanged += new System.EventHandler(this.mainTabControl_SelectedIndexChanged);
            // 
            // mainTabPage
            // 
            this.mainTabPage.AutoScroll = true;
            this.mainTabPage.Controls.Add(this.maxYearText);
            this.mainTabPage.Controls.Add(this.minYearText);
            this.mainTabPage.Controls.Add(this.label16);
            this.mainTabPage.Controls.Add(this.NumThresholdFailedRecordsBox);
            this.mainTabPage.Controls.Add(this.label15);
            this.mainTabPage.Controls.Add(this.QueryLanguageVersionText);
            this.mainTabPage.Controls.Add(this.label11);
            this.mainTabPage.Controls.Add(this.RequiresPublishBox);
            this.mainTabPage.Controls.Add(this.AppBuilderVersion);
            this.mainTabPage.Controls.Add(this.label14);
            this.mainTabPage.Controls.Add(this.RepositoryVersion);
            this.mainTabPage.Controls.Add(this.label13);
            this.mainTabPage.Controls.Add(this.DoNotAllowBirstToRebuildApplicationCheckBox);
            this.mainTabPage.Controls.Add(this.lastEditLabel);
            this.mainTabPage.Controls.Add(this.label12);
            this.mainTabPage.Controls.Add(this.sourceCodeVersionLabel);
            this.mainTabPage.Controls.Add(this.label10);
            this.mainTabPage.Controls.Add(this.goalMeasure);
            this.mainTabPage.Controls.Add(this.label5);
            this.mainTabPage.Controls.Add(this.targetDimension);
            this.mainTabPage.Controls.Add(this.label3);
            this.mainTabPage.Controls.Add(this.appName);
            this.mainTabPage.Controls.Add(this.label2);
            this.mainTabPage.Location = new System.Drawing.Point(4, 22);
            this.mainTabPage.Name = "mainTabPage";
            this.mainTabPage.Size = new System.Drawing.Size(852, 727);
            this.mainTabPage.TabIndex = 0;
            this.mainTabPage.Text = "General";
            this.mainTabPage.UseVisualStyleBackColor = true;
            // 
            // maxYearText
            // 
            this.maxYearText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maxYearText.Location = new System.Drawing.Point(456, 434);
            this.maxYearText.Name = "maxYearText";
            this.maxYearText.Size = new System.Drawing.Size(121, 20);
            this.maxYearText.TabIndex = 25;
            // 
            // minYearText
            // 
            this.minYearText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.minYearText.Location = new System.Drawing.Point(281, 434);
            this.minYearText.Name = "minYearText";
            this.minYearText.Size = new System.Drawing.Size(121, 20);
            this.minYearText.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 431);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(190, 23);
            this.label16.TabIndex = 23;
            this.label16.Text = "Min/Max Year";
            // 
            // NumThresholdFailedRecordsBox
            // 
            this.NumThresholdFailedRecordsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NumThresholdFailedRecordsBox.Location = new System.Drawing.Point(281, 394);
            this.NumThresholdFailedRecordsBox.Name = "NumThresholdFailedRecordsBox";
            this.NumThresholdFailedRecordsBox.Size = new System.Drawing.Size(296, 20);
            this.NumThresholdFailedRecordsBox.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 394);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(190, 23);
            this.label15.TabIndex = 21;
            this.label15.Text = "Max Failed Records Allowed";
            // 
            // QueryLanguageVersionText
            // 
            this.QueryLanguageVersionText.Location = new System.Drawing.Point(280, 195);
            this.QueryLanguageVersionText.Name = "QueryLanguageVersionText";
            this.QueryLanguageVersionText.Size = new System.Drawing.Size(100, 20);
            this.QueryLanguageVersionText.TabIndex = 20;
            this.toolTip.SetToolTip(this.QueryLanguageVersionText, "0 for OLD, 1 for NEW. Do not change this if you have existing logical queries.");
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 195);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(236, 23);
            this.label11.TabIndex = 19;
            this.label11.Text = "Query Language Version";
            this.toolTip.SetToolTip(this.label11, "0 for OLD, 1 for NEW.  Do not change this if you have existing logical queries.");
            // 
            // RequiresPublishBox
            // 
            this.RequiresPublishBox.AutoSize = true;
            this.RequiresPublishBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RequiresPublishBox.Location = new System.Drawing.Point(11, 273);
            this.RequiresPublishBox.Name = "RequiresPublishBox";
            this.RequiresPublishBox.Size = new System.Drawing.Size(400, 20);
            this.RequiresPublishBox.TabIndex = 18;
            this.RequiresPublishBox.Text = "Requires Publish (only change if you know what you\'re doing... )";
            this.RequiresPublishBox.UseVisualStyleBackColor = true;
            // 
            // AppBuilderVersion
            // 
            this.AppBuilderVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AppBuilderVersion.Location = new System.Drawing.Point(278, 159);
            this.AppBuilderVersion.Name = "AppBuilderVersion";
            this.AppBuilderVersion.Size = new System.Drawing.Size(457, 23);
            this.AppBuilderVersion.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(236, 23);
            this.label14.TabIndex = 16;
            this.label14.Text = "Application Builder Version";
            // 
            // RepositoryVersion
            // 
            this.RepositoryVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RepositoryVersion.Location = new System.Drawing.Point(278, 121);
            this.RepositoryVersion.Name = "RepositoryVersion";
            this.RepositoryVersion.Size = new System.Drawing.Size(457, 23);
            this.RepositoryVersion.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(236, 23);
            this.label13.TabIndex = 14;
            this.label13.Text = "Repository Version";
            // 
            // DoNotAllowBirstToRebuildApplicationCheckBox
            // 
            this.DoNotAllowBirstToRebuildApplicationCheckBox.AutoSize = true;
            this.DoNotAllowBirstToRebuildApplicationCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DoNotAllowBirstToRebuildApplicationCheckBox.Location = new System.Drawing.Point(11, 232);
            this.DoNotAllowBirstToRebuildApplicationCheckBox.Name = "DoNotAllowBirstToRebuildApplicationCheckBox";
            this.DoNotAllowBirstToRebuildApplicationCheckBox.Size = new System.Drawing.Size(510, 20);
            this.DoNotAllowBirstToRebuildApplicationCheckBox.TabIndex = 13;
            this.DoNotAllowBirstToRebuildApplicationCheckBox.Text = "Do not allow Birst to rebuild the application (mainly for Enterprise implementati" +
    "ons)";
            this.DoNotAllowBirstToRebuildApplicationCheckBox.UseVisualStyleBackColor = true;
            // 
            // lastEditLabel
            // 
            this.lastEditLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastEditLabel.Location = new System.Drawing.Point(278, 84);
            this.lastEditLabel.Name = "lastEditLabel";
            this.lastEditLabel.Size = new System.Drawing.Size(457, 23);
            this.lastEditLabel.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(236, 23);
            this.label12.TabIndex = 10;
            this.label12.Text = "Last Edit with the Administration Tool";
            // 
            // sourceCodeVersionLabel
            // 
            this.sourceCodeVersionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceCodeVersionLabel.Location = new System.Drawing.Point(277, 48);
            this.sourceCodeVersionLabel.Name = "sourceCodeVersionLabel";
            this.sourceCodeVersionLabel.Size = new System.Drawing.Size(211, 23);
            this.sourceCodeVersionLabel.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(190, 23);
            this.label10.TabIndex = 8;
            this.label10.Text = "Source Code Control Version";
            // 
            // goalMeasure
            // 
            this.goalMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.goalMeasure.Location = new System.Drawing.Point(280, 356);
            this.goalMeasure.Name = "goalMeasure";
            this.goalMeasure.Size = new System.Drawing.Size(208, 21);
            this.goalMeasure.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 356);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(255, 23);
            this.label5.TabIndex = 6;
            this.label5.Text = "Default Optimization Goal";
            // 
            // targetDimension
            // 
            this.targetDimension.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.targetDimension.Location = new System.Drawing.Point(280, 324);
            this.targetDimension.Name = "targetDimension";
            this.targetDimension.Size = new System.Drawing.Size(208, 21);
            this.targetDimension.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(255, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Default Performance Target Dimension";
            // 
            // appName
            // 
            this.appName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.appName.Location = new System.Drawing.Point(281, 12);
            this.appName.Name = "appName";
            this.appName.Size = new System.Drawing.Size(296, 20);
            this.appName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Application Name";
            // 
            // dimensionsTabPage
            // 
            this.dimensionsTabPage.AutoScroll = true;
            this.dimensionsTabPage.Controls.Add(this.dimDataGridView);
            this.dimensionsTabPage.Controls.Add(this.label8);
            this.dimensionsTabPage.Controls.Add(this.dimensionContentFilters);
            this.dimensionsTabPage.Controls.Add(this.dimCacheable);
            this.dimensionsTabPage.Controls.Add(this.dimConnectionBox);
            this.dimensionsTabPage.Controls.Add(this.label6);
            this.dimensionsTabPage.Controls.Add(this.dimTableLevel);
            this.dimensionsTabPage.Controls.Add(this.defineTableSource);
            this.dimensionsTabPage.Controls.Add(this.dimView);
            this.dimensionsTabPage.Controls.Add(this.dimNormal);
            this.dimensionsTabPage.Controls.Add(this.label81);
            this.dimensionsTabPage.Controls.Add(this.dimensionViewSQL);
            this.dimensionsTabPage.Controls.Add(this.label80);
            this.dimensionsTabPage.Controls.Add(this.label79);
            this.dimensionsTabPage.Controls.Add(this.label75);
            this.dimensionsTabPage.Controls.Add(this.dimensionTablePName);
            this.dimensionsTabPage.Controls.Add(this.groupBox31);
            this.dimensionsTabPage.Controls.Add(this.dimColMappingGridView);
            this.dimensionsTabPage.Controls.Add(this.dimModelBox);
            this.dimensionsTabPage.Location = new System.Drawing.Point(4, 22);
            this.dimensionsTabPage.Name = "dimensionsTabPage";
            this.dimensionsTabPage.Size = new System.Drawing.Size(852, 727);
            this.dimensionsTabPage.TabIndex = 6;
            this.dimensionsTabPage.Text = "Dimensions";
            this.dimensionsTabPage.UseVisualStyleBackColor = true;
            this.dimensionsTabPage.Leave += new System.EventHandler(this.dimensionsTabPage_Leave);
            // 
            // dimDataGridView
            // 
            this.dimDataGridView.AllowUserToOrderColumns = true;
            this.dimDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dimDataGridView.AutoGenerateColumns = false;
            this.dimDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dimDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dimDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dimDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dimColumnName,
            this.dataGridViewTextBoxColumn1,
            this.dataTypeDataGridViewTextBoxColumn1,
            this.formatDataGridViewTextBoxColumn1,
            this.ColWidth,
            this.keyDataGridViewCheckBoxColumn1,
            this.Index,
            this.GenericName,
            this.UnknownValue,
            this.VolatileKey,
            this.measureExpressionDataGridViewTextBoxColumn,
            this.desiredValuesDataGridViewTextBoxColumn,
            this.ignoredValuesDataGridViewTextBoxColumn,
            this.scoreDataGridViewTextBoxColumn,
            this.blockingGroupDataGridViewTextBoxColumn,
            this.maskableTypeDataGridViewTextBoxColumn1,
            this.DisplayMap});
            this.dimDataGridView.DataSource = this.dimensionColumnSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dimDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.dimDataGridView.Location = new System.Drawing.Point(0, 0);
            this.dimDataGridView.Name = "dimDataGridView";
            this.dimDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dimDataGridView.RowTemplate.Height = 18;
            this.dimDataGridView.Size = new System.Drawing.Size(852, 727);
            this.dimDataGridView.TabIndex = 116;
            this.dimDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dimDataGridView_CellBeginEdit);
            this.dimDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimDataGridView_CellContentClick);
            this.dimDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimDataGridView_CellEndEdit);
            this.dimDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dimDataGridView_CellValidating);
            this.dimDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimDataGridView_CellValueChanged);
            this.dimDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dimDataGridView_DataError);
            // 
            // dimColumnName
            // 
            this.dimColumnName.DataPropertyName = "ColumnName";
            this.dimColumnName.Frozen = true;
            this.dimColumnName.HeaderText = "ColumnName";
            this.dimColumnName.Name = "dimColumnName";
            this.dimColumnName.Width = 175;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn1.HeaderText = "Description";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataTypeDataGridViewTextBoxColumn1
            // 
            this.dataTypeDataGridViewTextBoxColumn1.DataPropertyName = "DataType";
            this.dataTypeDataGridViewTextBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataTypeDataGridViewTextBoxColumn1.HeaderText = "DataType";
            this.dataTypeDataGridViewTextBoxColumn1.Items.AddRange(new object[] {
            "Number",
            "Integer",
            "Varchar",
            "Date",
            "DateTime",
            "Float"});
            this.dataTypeDataGridViewTextBoxColumn1.Name = "dataTypeDataGridViewTextBoxColumn1";
            this.dataTypeDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataTypeDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataTypeDataGridViewTextBoxColumn1.Width = 80;
            // 
            // formatDataGridViewTextBoxColumn1
            // 
            this.formatDataGridViewTextBoxColumn1.DataPropertyName = "Format";
            this.formatDataGridViewTextBoxColumn1.HeaderText = "Format";
            this.formatDataGridViewTextBoxColumn1.Name = "formatDataGridViewTextBoxColumn1";
            // 
            // ColWidth
            // 
            this.ColWidth.DataPropertyName = "ColWidth";
            this.ColWidth.HeaderText = "Width";
            this.ColWidth.Name = "ColWidth";
            this.ColWidth.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColWidth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColWidth.Width = 40;
            // 
            // keyDataGridViewCheckBoxColumn1
            // 
            this.keyDataGridViewCheckBoxColumn1.DataPropertyName = "Key";
            this.keyDataGridViewCheckBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.keyDataGridViewCheckBoxColumn1.HeaderText = "Key";
            this.keyDataGridViewCheckBoxColumn1.Name = "keyDataGridViewCheckBoxColumn1";
            this.keyDataGridViewCheckBoxColumn1.Width = 40;
            // 
            // Index
            // 
            this.Index.DataPropertyName = "Index";
            this.Index.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.Width = 40;
            // 
            // GenericName
            // 
            this.GenericName.DataPropertyName = "GenericName";
            this.GenericName.HeaderText = "GenericColumn";
            this.GenericName.Name = "GenericName";
            // 
            // UnknownValue
            // 
            this.UnknownValue.DataPropertyName = "UnknownValue";
            this.UnknownValue.HeaderText = "Unknown Value";
            this.UnknownValue.Name = "UnknownValue";
            // 
            // VolatileKey
            // 
            this.VolatileKey.DataPropertyName = "VolatileKey";
            this.VolatileKey.HeaderText = "Volatile Key";
            this.VolatileKey.Name = "VolatileKey";
            // 
            // measureExpressionDataGridViewTextBoxColumn
            // 
            this.measureExpressionDataGridViewTextBoxColumn.DataPropertyName = "MeasureExpression";
            this.measureExpressionDataGridViewTextBoxColumn.HeaderText = "MeasureExpression";
            this.measureExpressionDataGridViewTextBoxColumn.Name = "measureExpressionDataGridViewTextBoxColumn";
            // 
            // desiredValuesDataGridViewTextBoxColumn
            // 
            this.desiredValuesDataGridViewTextBoxColumn.DataPropertyName = "DesiredValues";
            this.desiredValuesDataGridViewTextBoxColumn.HeaderText = "DesiredValues";
            this.desiredValuesDataGridViewTextBoxColumn.Name = "desiredValuesDataGridViewTextBoxColumn";
            // 
            // ignoredValuesDataGridViewTextBoxColumn
            // 
            this.ignoredValuesDataGridViewTextBoxColumn.DataPropertyName = "IgnoredValues";
            this.ignoredValuesDataGridViewTextBoxColumn.HeaderText = "IgnoredValues";
            this.ignoredValuesDataGridViewTextBoxColumn.Name = "ignoredValuesDataGridViewTextBoxColumn";
            // 
            // scoreDataGridViewTextBoxColumn
            // 
            this.scoreDataGridViewTextBoxColumn.DataPropertyName = "Score";
            this.scoreDataGridViewTextBoxColumn.HeaderText = "Score";
            this.scoreDataGridViewTextBoxColumn.Name = "scoreDataGridViewTextBoxColumn";
            // 
            // blockingGroupDataGridViewTextBoxColumn
            // 
            this.blockingGroupDataGridViewTextBoxColumn.DataPropertyName = "BlockingGroup";
            this.blockingGroupDataGridViewTextBoxColumn.HeaderText = "BlockingGroup";
            this.blockingGroupDataGridViewTextBoxColumn.Name = "blockingGroupDataGridViewTextBoxColumn";
            // 
            // maskableTypeDataGridViewTextBoxColumn1
            // 
            this.maskableTypeDataGridViewTextBoxColumn1.DataPropertyName = "Maskable";
            this.maskableTypeDataGridViewTextBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.maskableTypeDataGridViewTextBoxColumn1.HeaderText = "Maskable";
            this.maskableTypeDataGridViewTextBoxColumn1.Items.AddRange(new object[] {
            "No",
            "Axxx",
            "AxxA",
            "1000",
            "1001"});
            this.maskableTypeDataGridViewTextBoxColumn1.Name = "maskableTypeDataGridViewTextBoxColumn1";
            this.maskableTypeDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.maskableTypeDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.maskableTypeDataGridViewTextBoxColumn1.Width = 60;
            // 
            // DisplayMap
            // 
            this.DisplayMap.DataPropertyName = "DisplayMap";
            this.DisplayMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DisplayMap.HeaderText = "Map";
            this.DisplayMap.Name = "DisplayMap";
            this.DisplayMap.Width = 65;
            // 
            // dimensionColumnSource
            // 
            this.dimensionColumnSource.DataMember = "DimensionColumns";
            this.dimensionColumnSource.DataSource = this.schemaDataSet;
            // 
            // schemaDataSet
            // 
            this.schemaDataSet.DataSetName = "SchemaDataSet";
            this.schemaDataSet.Locale = new System.Globalization.CultureInfo("en-US");
            this.schemaDataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.measureColumnsTable,
            this.dimensionColumns,
            this.measureTables,
            this.Joins,
            this.dimensionColumnMappings,
            this.measureColumnMappings});
            // 
            // measureColumnsTable
            // 
            this.measureColumnsTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.measureColumnName,
            this.measureDescription,
            this.measureDataType,
            this.measurePerformanceCategory,
            this.measureAggregationRule,
            this.keyColumn,
            this.measureFormatColumn,
            this.measureImprove,
            this.measureMinimum,
            this.measureMaximum,
            this.measureOpportunity,
            this.measureMinImprovement,
            this.measureMaxImprovement,
            this.measureEnforceLimits,
            this.measureAttributeWeight,
            this.measureMinTarget,
            this.measureMaxTarget,
            this.measureValid,
            this.measureAnalysis,
            this.segmentMeasure,
            this.displayFunctionColumn,
            this.dimRuleColumn,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn13,
            this.dataColumn15,
            this.dataColumn18,
            this.dataColumn20,
            this.dataColumn28});
            this.measureColumnsTable.TableName = "MeasureColumns";
            // 
            // measureColumnName
            // 
            this.measureColumnName.AllowDBNull = false;
            this.measureColumnName.Caption = "Column Name";
            this.measureColumnName.ColumnName = "ColumnName";
            this.measureColumnName.DefaultValue = "New Column";
            // 
            // measureDescription
            // 
            this.measureDescription.ColumnName = "Description";
            // 
            // measureDataType
            // 
            this.measureDataType.AllowDBNull = false;
            this.measureDataType.Caption = "Data Type";
            this.measureDataType.ColumnName = "DataType";
            this.measureDataType.DefaultValue = "Number";
            // 
            // measurePerformanceCategory
            // 
            this.measurePerformanceCategory.AllowDBNull = false;
            this.measurePerformanceCategory.Caption = "Performance Category";
            this.measurePerformanceCategory.ColumnName = "PerformanceCategory";
            this.measurePerformanceCategory.DefaultValue = "";
            // 
            // measureAggregationRule
            // 
            this.measureAggregationRule.AllowDBNull = false;
            this.measureAggregationRule.ColumnName = "AggregationRule";
            this.measureAggregationRule.DefaultValue = "SUM";
            // 
            // keyColumn
            // 
            this.keyColumn.AllowDBNull = false;
            this.keyColumn.ColumnName = "Key";
            this.keyColumn.DataType = typeof(bool);
            this.keyColumn.DefaultValue = false;
            // 
            // measureFormatColumn
            // 
            this.measureFormatColumn.AllowDBNull = false;
            this.measureFormatColumn.ColumnName = "Format";
            this.measureFormatColumn.DefaultValue = "#,###,###.##";
            // 
            // measureImprove
            // 
            this.measureImprove.ColumnName = "Improve";
            this.measureImprove.DataType = typeof(int);
            this.measureImprove.DefaultValue = 1;
            // 
            // measureMinimum
            // 
            this.measureMinimum.ColumnName = "Minimum";
            this.measureMinimum.DefaultValue = "-Infinity";
            // 
            // measureMaximum
            // 
            this.measureMaximum.ColumnName = "Maximum";
            this.measureMaximum.DefaultValue = "Infinity";
            // 
            // measureOpportunity
            // 
            this.measureOpportunity.ColumnName = "Opportunity";
            // 
            // measureMinImprovement
            // 
            this.measureMinImprovement.ColumnName = "MinImprovement";
            this.measureMinImprovement.DefaultValue = "-Infinity";
            // 
            // measureMaxImprovement
            // 
            this.measureMaxImprovement.ColumnName = "MaxImprovement";
            this.measureMaxImprovement.DefaultValue = "Infinity";
            // 
            // measureEnforceLimits
            // 
            this.measureEnforceLimits.AllowDBNull = false;
            this.measureEnforceLimits.ColumnName = "EnforceLimits";
            this.measureEnforceLimits.DataType = typeof(bool);
            this.measureEnforceLimits.DefaultValue = false;
            // 
            // measureAttributeWeight
            // 
            this.measureAttributeWeight.ColumnName = "AttributeWeightMeasure";
            // 
            // measureMinTarget
            // 
            this.measureMinTarget.ColumnName = "MinTarget";
            this.measureMinTarget.DefaultValue = "-Infinity";
            // 
            // measureMaxTarget
            // 
            this.measureMaxTarget.ColumnName = "MaxTarget";
            this.measureMaxTarget.DefaultValue = "Infinity";
            // 
            // measureValid
            // 
            this.measureValid.ColumnName = "ValidMeasure";
            // 
            // measureAnalysis
            // 
            this.measureAnalysis.ColumnName = "AnalysisMeasure";
            // 
            // segmentMeasure
            // 
            this.segmentMeasure.ColumnName = "SegmentMeasure";
            // 
            // displayFunctionColumn
            // 
            this.displayFunctionColumn.ColumnName = "DisplayFunction";
            // 
            // dimRuleColumn
            // 
            this.dimRuleColumn.ColumnName = "DimensionRules";
            // 
            // dataColumn5
            // 
            this.dataColumn5.AllowDBNull = false;
            this.dataColumn5.ColumnName = "Derived";
            this.dataColumn5.DataType = typeof(bool);
            this.dataColumn5.DefaultValue = false;
            // 
            // dataColumn6
            // 
            this.dataColumn6.AllowDBNull = false;
            this.dataColumn6.ColumnName = "PhysicalName";
            this.dataColumn6.DefaultValue = "";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "Filters";
            this.dataColumn13.ColumnName = "FilteredMeasures";
            this.dataColumn13.DataType = typeof(object);
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "AutoGenerated";
            this.dataColumn15.DataType = typeof(bool);
            this.dataColumn15.DefaultValue = false;
            // 
            // dataColumn18
            // 
            this.dataColumn18.AllowDBNull = false;
            this.dataColumn18.ColumnName = "Width";
            this.dataColumn18.DataType = typeof(int);
            this.dataColumn18.DefaultValue = 0;
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "RootMeasure";
            // 
            // dataColumn28
            // 
            this.dataColumn28.AllowDBNull = false;
            this.dataColumn28.ColumnName = "ManuallyEdited";
            this.dataColumn28.DataType = typeof(bool);
            this.dataColumn28.DefaultValue = false;
            // 
            // dimensionColumns
            // 
            this.dimensionColumns.Columns.AddRange(new System.Data.DataColumn[] {
            this.dimensionColumnDimensionName,
            this.dimensionColumnName,
            this.dimDescription,
            this.dimVolatileKey,
            this.dimensionColumnDataType,
            this.dimensionColumnWidth,
            this.dimensionColumnKey,
            this.dimFormatColumn,
            this.dimMeasureExpression,
            this.dimDesiredValues,
            this.dimIgnoredValues,
            this.dimScore,
            this.dimBlockingGroup,
            this.dimMaskable,
            this.dataColumn8,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn14,
            this.dataColumn16,
            this.dataColumn19,
            this.dataColumn21,
            this.dataColumn27});
            this.dimensionColumns.TableName = "DimensionColumns";
            // 
            // dimensionColumnDimensionName
            // 
            this.dimensionColumnDimensionName.AllowDBNull = false;
            this.dimensionColumnDimensionName.Caption = "Dimension Name";
            this.dimensionColumnDimensionName.ColumnName = "DimensionName";
            this.dimensionColumnDimensionName.DefaultValue = "";
            // 
            // dimensionColumnName
            // 
            this.dimensionColumnName.AllowDBNull = false;
            this.dimensionColumnName.Caption = "Column Name";
            this.dimensionColumnName.ColumnName = "ColumnName";
            this.dimensionColumnName.DefaultValue = "New Column";
            // 
            // dimDescription
            // 
            this.dimDescription.ColumnName = "Description";
            // 
            // dimVolatileKey
            // 
            this.dimVolatileKey.ColumnName = "VolatileKey";
            // 
            // dimensionColumnDataType
            // 
            this.dimensionColumnDataType.AllowDBNull = false;
            this.dimensionColumnDataType.Caption = "Data Type";
            this.dimensionColumnDataType.ColumnName = "DataType";
            this.dimensionColumnDataType.DefaultValue = "Varchar";
            // 
            // dimensionColumnWidth
            // 
            this.dimensionColumnWidth.ColumnName = "ColWidth";
            this.dimensionColumnWidth.DefaultValue = "";
            // 
            // dimensionColumnKey
            // 
            this.dimensionColumnKey.AllowDBNull = false;
            this.dimensionColumnKey.ColumnName = "Key";
            this.dimensionColumnKey.DataType = typeof(bool);
            this.dimensionColumnKey.DefaultValue = false;
            // 
            // dimFormatColumn
            // 
            this.dimFormatColumn.AllowDBNull = false;
            this.dimFormatColumn.ColumnName = "Format";
            this.dimFormatColumn.DefaultValue = "";
            // 
            // dimMeasureExpression
            // 
            this.dimMeasureExpression.AllowDBNull = false;
            this.dimMeasureExpression.ColumnName = "MeasureExpression";
            this.dimMeasureExpression.DefaultValue = "";
            // 
            // dimDesiredValues
            // 
            this.dimDesiredValues.AllowDBNull = false;
            this.dimDesiredValues.ColumnName = "DesiredValues";
            this.dimDesiredValues.DefaultValue = "";
            // 
            // dimIgnoredValues
            // 
            this.dimIgnoredValues.AllowDBNull = false;
            this.dimIgnoredValues.ColumnName = "IgnoredValues";
            this.dimIgnoredValues.DefaultValue = "";
            // 
            // dimScore
            // 
            this.dimScore.AllowDBNull = false;
            this.dimScore.ColumnName = "Score";
            this.dimScore.DataType = typeof(double);
            this.dimScore.DefaultValue = 0D;
            // 
            // dimBlockingGroup
            // 
            this.dimBlockingGroup.ColumnName = "BlockingGroup";
            // 
            // dimMaskable
            // 
            this.dimMaskable.AllowDBNull = false;
            this.dimMaskable.ColumnName = "Maskable";
            this.dimMaskable.DefaultValue = "No";
            // 
            // dataColumn8
            // 
            this.dataColumn8.AllowDBNull = false;
            this.dataColumn8.ColumnName = "SurrogateKey";
            this.dataColumn8.DataType = typeof(bool);
            this.dataColumn8.DefaultValue = false;
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "GenericName";
            this.dataColumn11.DefaultValue = "";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "DisplayMap";
            this.dataColumn12.DataType = typeof(object);
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "UnknownValue";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "AutoGenerated";
            this.dataColumn16.DataType = typeof(bool);
            this.dataColumn16.DefaultValue = false;
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "Index";
            this.dataColumn19.DataType = typeof(bool);
            this.dataColumn19.DefaultValue = false;
            // 
            // dataColumn21
            // 
            this.dataColumn21.AllowDBNull = false;
            this.dataColumn21.ColumnName = "Constant";
            this.dataColumn21.DataType = typeof(bool);
            this.dataColumn21.DefaultValue = false;
            // 
            // dataColumn27
            // 
            this.dataColumn27.AllowDBNull = false;
            this.dataColumn27.ColumnName = "ManuallyEdited";
            this.dataColumn27.DataType = typeof(bool);
            this.dataColumn27.DefaultValue = false;
            // 
            // measureTables
            // 
            this.measureTables.Columns.AddRange(new System.Data.DataColumn[] {
            this.measureTableName});
            this.measureTables.TableName = "MeasureTables";
            // 
            // measureTableName
            // 
            this.measureTableName.AllowDBNull = false;
            this.measureTableName.ColumnName = "TableName";
            // 
            // Joins
            // 
            this.Joins.Columns.AddRange(new System.Data.DataColumn[] {
            this.table1,
            this.table2,
            this.joinCondition,
            this.redundantJoin,
            this.joinType,
            this.levelColumn,
            this.autoGeneratedJoin,
            this.dataColumn17,
            this.federatedJoin,
            this.federatedJoinKeys});
            this.Joins.TableName = "Joins";
            // 
            // table1
            // 
            this.table1.Caption = "Table1";
            this.table1.ColumnName = "Table1";
            this.table1.DefaultValue = "";
            // 
            // table2
            // 
            this.table2.ColumnName = "Table2";
            this.table2.DefaultValue = "";
            // 
            // joinCondition
            // 
            this.joinCondition.ColumnName = "JoinCondition";
            this.joinCondition.DefaultValue = "";
            // 
            // redundantJoin
            // 
            this.redundantJoin.ColumnName = "Redundant";
            this.redundantJoin.DataType = typeof(bool);
            this.redundantJoin.DefaultValue = false;
            // 
            // joinType
            // 
            this.joinType.ColumnName = "JoinType";
            this.joinType.DefaultValue = "Inner";
            // 
            // levelColumn
            // 
            this.levelColumn.ColumnName = "Level";
            this.levelColumn.DefaultValue = "";
            // 
            // autoGeneratedJoin
            // 
            this.autoGeneratedJoin.ColumnName = "AutoGenerated";
            this.autoGeneratedJoin.DataType = typeof(bool);
            this.autoGeneratedJoin.DefaultValue = false;
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "Invalid";
            this.dataColumn17.DataType = typeof(bool);
            this.dataColumn17.DefaultValue = false;
            //
            //Federated
            //
            this.federatedJoin.ColumnName = "Federated";
            this.federatedJoin.DataType = typeof(bool);
            this.federatedJoin.DefaultValue = false;
            //
            //Federated Join Keys
            //
            this.federatedJoinKeys.ColumnName = "FederatedJoinKeys";
            this.federatedJoinKeys.DefaultValue = "";
            // 
            // dimensionColumnMappings
            // 
            this.dimensionColumnMappings.Columns.AddRange(new System.Data.DataColumn[] {
            this.dimensionColumnTableName,
            this.dimMappingColumnName,
            this.dimensionColumnPhysicalName,
            this.dimQualify,
            this.dataColumn9,
            this.dataColumn22,
            this.dataColumn26,
            this.dataColumn29});
            this.dimensionColumnMappings.TableName = "DimensionColumnMappings";
            // 
            // dimensionColumnTableName
            // 
            this.dimensionColumnTableName.AllowDBNull = false;
            this.dimensionColumnTableName.ColumnName = "TableName";
            this.dimensionColumnTableName.DefaultValue = "";
            // 
            // dimMappingColumnName
            // 
            this.dimMappingColumnName.AllowDBNull = false;
            this.dimMappingColumnName.ColumnName = "ColumnName";
            this.dimMappingColumnName.DefaultValue = "";
            // 
            // dimensionColumnPhysicalName
            // 
            this.dimensionColumnPhysicalName.AllowDBNull = false;
            this.dimensionColumnPhysicalName.ColumnName = "PhysicalName";
            this.dimensionColumnPhysicalName.DefaultValue = "";
            // 
            // dimQualify
            // 
            this.dimQualify.AllowDBNull = false;
            this.dimQualify.ColumnName = "Qualify";
            this.dimQualify.DataType = typeof(bool);
            this.dimQualify.DefaultValue = false;
            // 
            // dataColumn9
            // 
            this.dataColumn9.AllowDBNull = false;
            this.dataColumn9.ColumnName = "AutoGenerated";
            this.dataColumn9.DataType = typeof(bool);
            this.dataColumn9.DefaultValue = false;
            // 
            // dataColumn22
            // 
            this.dataColumn22.AllowDBNull = false;
            this.dataColumn22.Caption = "NotCacheable";
            this.dataColumn22.ColumnName = "NotCacheable";
            this.dataColumn22.DataType = typeof(bool);
            this.dataColumn22.DefaultValue = false;
            // 
            // dataColumn26
            // 
            this.dataColumn26.AllowDBNull = false;
            this.dataColumn26.ColumnName = "ManuallyEdited";
            this.dataColumn26.DataType = typeof(bool);
            this.dataColumn26.DefaultValue = false;
            // 
            // dataColumn29
            // 
            this.dataColumn29.AllowDBNull = false;
            this.dataColumn29.ColumnName = "CacheBoundary";
            this.dataColumn29.DefaultValue = "NONE";
            // 
            // measureColumnMappings
            // 
            this.measureColumnMappings.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn7,
            this.dataColumn10,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25});
            this.measureColumnMappings.TableName = "MeasureColumnMappings";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ColumnName";
            this.dataColumn1.DefaultValue = "";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "PhysicalName";
            this.dataColumn2.DefaultValue = "";
            // 
            // dataColumn3
            // 
            this.dataColumn3.AllowDBNull = false;
            this.dataColumn3.ColumnName = "Qualify";
            this.dataColumn3.DataType = typeof(bool);
            this.dataColumn3.DefaultValue = false;
            // 
            // dataColumn4
            // 
            this.dataColumn4.AllowDBNull = false;
            this.dataColumn4.ColumnName = "TableName";
            this.dataColumn4.DefaultValue = "";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "AggRuleOverride";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "AutoGenerated";
            this.dataColumn10.DataType = typeof(bool);
            this.dataColumn10.DefaultValue = false;
            // 
            // dataColumn23
            // 
            this.dataColumn23.AllowDBNull = false;
            this.dataColumn23.Caption = "NotCacheable";
            this.dataColumn23.ColumnName = "NotCacheable";
            this.dataColumn23.DataType = typeof(bool);
            this.dataColumn23.DefaultValue = false;
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "PhysicalFilter";
            // 
            // dataColumn25
            // 
            this.dataColumn25.AllowDBNull = false;
            this.dataColumn25.ColumnName = "ManuallyEdited";
            this.dataColumn25.DataType = typeof(bool);
            this.dataColumn25.DefaultValue = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(403, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 121;
            this.label8.Text = "Content Filters";
            // 
            // dimensionContentFilters
            // 
            this.dimensionContentFilters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionContentFilters.CheckOnClick = true;
            this.dimensionContentFilters.FormattingEnabled = true;
            this.dimensionContentFilters.Location = new System.Drawing.Point(483, 124);
            this.dimensionContentFilters.Name = "dimensionContentFilters";
            this.dimensionContentFilters.Size = new System.Drawing.Size(294, 77);
            this.dimensionContentFilters.TabIndex = 120;
            this.dimensionContentFilters.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainAdminForm_MouseMove);
            // 
            // dimCacheable
            // 
            this.dimCacheable.AutoSize = true;
            this.dimCacheable.Location = new System.Drawing.Point(258, 32);
            this.dimCacheable.Name = "dimCacheable";
            this.dimCacheable.Size = new System.Drawing.Size(77, 17);
            this.dimCacheable.TabIndex = 119;
            this.dimCacheable.Text = "Cacheable";
            this.dimCacheable.UseVisualStyleBackColor = true;
            // 
            // dimConnectionBox
            // 
            this.dimConnectionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimConnectionBox.Location = new System.Drawing.Point(99, 30);
            this.dimConnectionBox.Name = "dimConnectionBox";
            this.dimConnectionBox.Size = new System.Drawing.Size(147, 21);
            this.dimConnectionBox.TabIndex = 118;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 20);
            this.label6.TabIndex = 117;
            this.label6.Text = "Connection";
            // 
            // dimTableLevel
            // 
            this.dimTableLevel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimTableLevel.Location = new System.Drawing.Point(99, 7);
            this.dimTableLevel.Name = "dimTableLevel";
            this.dimTableLevel.Size = new System.Drawing.Size(176, 21);
            this.dimTableLevel.TabIndex = 2;
            // 
            // defineTableSource
            // 
            this.defineTableSource.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.defineTableSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defineTableSource.Location = new System.Drawing.Point(216, 77);
            this.defineTableSource.Margin = new System.Windows.Forms.Padding(0);
            this.defineTableSource.Name = "defineTableSource";
            this.defineTableSource.Size = new System.Drawing.Size(119, 18);
            this.defineTableSource.TabIndex = 115;
            this.defineTableSource.Text = "Define Table Source";
            this.defineTableSource.Click += new System.EventHandler(this.defineTableSource_Click);
            // 
            // dimView
            // 
            this.dimView.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimView.Location = new System.Drawing.Point(216, 50);
            this.dimView.Name = "dimView";
            this.dimView.Size = new System.Drawing.Size(104, 24);
            this.dimView.TabIndex = 4;
            this.dimView.Text = "Opaque View";
            this.dimView.CheckedChanged += new System.EventHandler(this.dimView_CheckedChanged);
            // 
            // dimNormal
            // 
            this.dimNormal.Checked = true;
            this.dimNormal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimNormal.Location = new System.Drawing.Point(146, 50);
            this.dimNormal.Name = "dimNormal";
            this.dimNormal.Size = new System.Drawing.Size(64, 24);
            this.dimNormal.TabIndex = 3;
            this.dimNormal.TabStop = true;
            this.dimNormal.Text = "Normal";
            this.dimNormal.CheckedChanged += new System.EventHandler(this.dimNormal_CheckedChanged);
            // 
            // label81
            // 
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(7, 54);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(146, 20);
            this.label81.TabIndex = 93;
            this.label81.Text = "Table Source Type";
            // 
            // dimensionViewSQL
            // 
            this.dimensionViewSQL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dimensionViewSQL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionViewSQL.Location = new System.Drawing.Point(341, 31);
            this.dimensionViewSQL.Multiline = true;
            this.dimensionViewSQL.Name = "dimensionViewSQL";
            this.dimensionViewSQL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dimensionViewSQL.Size = new System.Drawing.Size(506, 87);
            this.dimensionViewSQL.TabIndex = 6;
            // 
            // label80
            // 
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(338, 8);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(128, 23);
            this.label80.TabIndex = 91;
            this.label80.Text = "Opaque View Query";
            // 
            // label79
            // 
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(7, 8);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(86, 20);
            this.label79.TabIndex = 89;
            this.label79.Text = "Table Level";
            // 
            // label75
            // 
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(7, 77);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(146, 20);
            this.label75.TabIndex = 86;
            this.label75.Text = "Physical Table Source";
            // 
            // dimensionTablePName
            // 
            this.dimensionTablePName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionTablePName.Location = new System.Drawing.Point(11, 98);
            this.dimensionTablePName.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dimensionTablePName.Name = "dimensionTablePName";
            this.dimensionTablePName.ReadOnly = true;
            this.dimensionTablePName.Size = new System.Drawing.Size(324, 20);
            this.dimensionTablePName.TabIndex = 5;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.virtualMeasuresPrefix);
            this.groupBox31.Controls.Add(this.defineDSubstitutions);
            this.groupBox31.Controls.Add(this.dimensionInheritPrefix);
            this.groupBox31.Controls.Add(this.label149);
            this.groupBox31.Controls.Add(this.inheritWithVirtualMeasures);
            this.groupBox31.Controls.Add(this.virtualMeasuresInheritBox);
            this.groupBox31.Controls.Add(this.dimensionInheritBox);
            this.groupBox31.Controls.Add(this.label173);
            this.groupBox31.Controls.Add(this.noDimtableInheritance);
            this.groupBox31.Controls.Add(this.inheritDimensionColumns);
            this.groupBox31.Location = new System.Drawing.Point(2, 119);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(395, 88);
            this.groupBox31.TabIndex = 114;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Column Inheritance";
            // 
            // virtualMeasuresPrefix
            // 
            this.virtualMeasuresPrefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.virtualMeasuresPrefix.Location = new System.Drawing.Point(304, 64);
            this.virtualMeasuresPrefix.Name = "virtualMeasuresPrefix";
            this.virtualMeasuresPrefix.Size = new System.Drawing.Size(80, 20);
            this.virtualMeasuresPrefix.TabIndex = 3;
            // 
            // defineDSubstitutions
            // 
            this.defineDSubstitutions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.defineDSubstitutions.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defineDSubstitutions.Location = new System.Drawing.Point(265, 14);
            this.defineDSubstitutions.Margin = new System.Windows.Forms.Padding(0);
            this.defineDSubstitutions.Name = "defineDSubstitutions";
            this.defineDSubstitutions.Size = new System.Drawing.Size(119, 18);
            this.defineDSubstitutions.TabIndex = 116;
            this.defineDSubstitutions.Text = "Column Substitutions";
            this.defineDSubstitutions.Click += new System.EventHandler(this.defineDSubstitutions_Click);
            // 
            // dimensionInheritPrefix
            // 
            this.dimensionInheritPrefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dimensionInheritPrefix.Location = new System.Drawing.Point(304, 40);
            this.dimensionInheritPrefix.Name = "dimensionInheritPrefix";
            this.dimensionInheritPrefix.Size = new System.Drawing.Size(80, 20);
            this.dimensionInheritPrefix.TabIndex = 1;
            // 
            // label149
            // 
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.Location = new System.Drawing.Point(258, 40);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(48, 16);
            this.label149.TabIndex = 106;
            this.label149.Text = "Prefix";
            // 
            // inheritWithVirtualMeasures
            // 
            this.inheritWithVirtualMeasures.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inheritWithVirtualMeasures.Location = new System.Drawing.Point(8, 64);
            this.inheritWithVirtualMeasures.Name = "inheritWithVirtualMeasures";
            this.inheritWithVirtualMeasures.Size = new System.Drawing.Size(104, 16);
            this.inheritWithVirtualMeasures.TabIndex = 112;
            this.inheritWithVirtualMeasures.Text = "Virtual Measure";
            // 
            // virtualMeasuresInheritBox
            // 
            this.virtualMeasuresInheritBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.virtualMeasuresInheritBox.Location = new System.Drawing.Point(120, 64);
            this.virtualMeasuresInheritBox.Name = "virtualMeasuresInheritBox";
            this.virtualMeasuresInheritBox.Size = new System.Drawing.Size(136, 21);
            this.virtualMeasuresInheritBox.TabIndex = 2;
            // 
            // dimensionInheritBox
            // 
            this.dimensionInheritBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dimensionInheritBox.Location = new System.Drawing.Point(120, 40);
            this.dimensionInheritBox.Name = "dimensionInheritBox";
            this.dimensionInheritBox.Size = new System.Drawing.Size(136, 21);
            this.dimensionInheritBox.TabIndex = 0;
            // 
            // label173
            // 
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.Location = new System.Drawing.Point(258, 64);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(48, 16);
            this.label173.TabIndex = 110;
            this.label173.Text = "Prefix";
            // 
            // noDimtableInheritance
            // 
            this.noDimtableInheritance.Checked = true;
            this.noDimtableInheritance.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.noDimtableInheritance.Location = new System.Drawing.Point(8, 16);
            this.noDimtableInheritance.Name = "noDimtableInheritance";
            this.noDimtableInheritance.Size = new System.Drawing.Size(88, 16);
            this.noDimtableInheritance.TabIndex = 113;
            this.noDimtableInheritance.TabStop = true;
            this.noDimtableInheritance.Text = "None";
            // 
            // inheritDimensionColumns
            // 
            this.inheritDimensionColumns.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.inheritDimensionColumns.Location = new System.Drawing.Point(8, 40);
            this.inheritDimensionColumns.Name = "inheritDimensionColumns";
            this.inheritDimensionColumns.Size = new System.Drawing.Size(106, 16);
            this.inheritDimensionColumns.TabIndex = 111;
            this.inheritDimensionColumns.Text = "Dimension Table";
            // 
            // dimColMappingGridView
            // 
            this.dimColMappingGridView.AllowUserToOrderColumns = true;
            this.dimColMappingGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dimColMappingGridView.AutoGenerateColumns = false;
            this.dimColMappingGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dimColMappingGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimColMappingGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dimColMappingGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dimColMappingGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnNameComboBox,
            this.PhysicalName,
            this.Qualify,
            this.autoGenColumn});
            this.dimColMappingGridView.DataSource = this.dimensionColumnMappingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dimColMappingGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.dimColMappingGridView.Location = new System.Drawing.Point(0, 209);
            this.dimColMappingGridView.Name = "dimColMappingGridView";
            this.dimColMappingGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dimColMappingGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dimColMappingGridView.RowTemplate.Height = 19;
            this.dimColMappingGridView.Size = new System.Drawing.Size(852, 498);
            this.dimColMappingGridView.TabIndex = 122;
            this.dimColMappingGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dimColMappingGridView_CellBeginEdit);
            this.dimColMappingGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dimColMappingGridView_CellEndEdit);
            this.dimColMappingGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dimColMappingGridView_DataError);
            this.dimColMappingGridView.SelectionChanged += new System.EventHandler(this.dimColMappingGridView_SelectionChanged);
            // 
            // columnNameComboBox
            // 
            this.columnNameComboBox.DataPropertyName = "ColumnName";
            this.columnNameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.columnNameComboBox.HeaderText = "ColumnName";
            this.columnNameComboBox.Name = "columnNameComboBox";
            this.columnNameComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.columnNameComboBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.columnNameComboBox.Width = 150;
            // 
            // PhysicalName
            // 
            this.PhysicalName.DataPropertyName = "PhysicalName";
            this.PhysicalName.HeaderText = "PhysicalName";
            this.PhysicalName.Name = "PhysicalName";
            this.PhysicalName.Width = 600;
            // 
            // Qualify
            // 
            this.Qualify.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Qualify.DataPropertyName = "Qualify";
            this.Qualify.HeaderText = "Qualify";
            this.Qualify.Name = "Qualify";
            this.Qualify.Width = 45;
            // 
            // autoGenColumn
            // 
            this.autoGenColumn.DataPropertyName = "AutoGenerated";
            this.autoGenColumn.HeaderText = "AutoGenerated";
            this.autoGenColumn.Name = "autoGenColumn";
            this.autoGenColumn.ReadOnly = true;
            this.autoGenColumn.Visible = false;
            // 
            // dimensionColumnMappingSource
            // 
            this.dimensionColumnMappingSource.DataMember = "DimensionColumnMappings";
            this.dimensionColumnMappingSource.DataSource = this.schemaDataSet;
            // 
            // dimModelBox
            // 
            this.dimModelBox.Controls.Add(this.successButton);
            this.dimModelBox.Controls.Add(this.performanceButton);
            this.dimModelBox.Controls.Add(this.peerButton);
            this.dimModelBox.Controls.Add(this.noneButton);
            this.dimModelBox.Location = new System.Drawing.Point(3, 209);
            this.dimModelBox.Name = "dimModelBox";
            this.dimModelBox.Size = new System.Drawing.Size(395, 50);
            this.dimModelBox.TabIndex = 123;
            this.dimModelBox.TabStop = false;
            this.dimModelBox.Text = "Modeling";
            // 
            // successButton
            // 
            this.successButton.AutoSize = true;
            this.successButton.Location = new System.Drawing.Point(264, 19);
            this.successButton.Name = "successButton";
            this.successButton.Size = new System.Drawing.Size(66, 17);
            this.successButton.TabIndex = 3;
            this.successButton.Text = "Success";
            this.successButton.UseVisualStyleBackColor = true;
            // 
            // performanceButton
            // 
            this.performanceButton.AutoSize = true;
            this.performanceButton.Location = new System.Drawing.Point(158, 19);
            this.performanceButton.Name = "performanceButton";
            this.performanceButton.Size = new System.Drawing.Size(85, 17);
            this.performanceButton.TabIndex = 2;
            this.performanceButton.Text = "Performance";
            this.performanceButton.UseVisualStyleBackColor = true;
            // 
            // peerButton
            // 
            this.peerButton.AutoSize = true;
            this.peerButton.Location = new System.Drawing.Point(78, 19);
            this.peerButton.Name = "peerButton";
            this.peerButton.Size = new System.Drawing.Size(61, 17);
            this.peerButton.TabIndex = 1;
            this.peerButton.Text = "Peering";
            this.peerButton.UseVisualStyleBackColor = true;
            // 
            // noneButton
            // 
            this.noneButton.AutoSize = true;
            this.noneButton.Checked = true;
            this.noneButton.Location = new System.Drawing.Point(7, 19);
            this.noneButton.Name = "noneButton";
            this.noneButton.Size = new System.Drawing.Size(51, 17);
            this.noneButton.TabIndex = 0;
            this.noneButton.TabStop = true;
            this.noneButton.Text = "None";
            this.noneButton.UseVisualStyleBackColor = true;
            // 
            // measuresTabPage
            // 
            this.measuresTabPage.AutoScroll = true;
            this.measuresTabPage.Controls.Add(this.measureDataGridView);
            this.measuresTabPage.Controls.Add(this.recalculateCardinality);
            this.measuresTabPage.Controls.Add(this.measureMapGridView);
            this.measuresTabPage.Controls.Add(this.label9);
            this.measuresTabPage.Controls.Add(this.measureContentFilters);
            this.measuresTabPage.Controls.Add(this.mtCacheable);
            this.measuresTabPage.Controls.Add(this.mtConnectionBox);
            this.measuresTabPage.Controls.Add(this.label7);
            this.measuresTabPage.Controls.Add(this.defineMSubstitutions);
            this.measuresTabPage.Controls.Add(this.defineMeasureTableSource);
            this.measuresTabPage.Controls.Add(this.label51);
            this.measuresTabPage.Controls.Add(this.measureInheritPrefix);
            this.measuresTabPage.Controls.Add(this.measureInheritBox);
            this.measuresTabPage.Controls.Add(this.inheritMeasures);
            this.measuresTabPage.Controls.Add(this.measureDerived);
            this.measuresTabPage.Controls.Add(this.measureView);
            this.measuresTabPage.Controls.Add(this.measureNormal);
            this.measuresTabPage.Controls.Add(this.measureViewSQL);
            this.measuresTabPage.Controls.Add(this.label82);
            this.measuresTabPage.Controls.Add(this.measureTableCardinality);
            this.measuresTabPage.Controls.Add(this.label78);
            this.measuresTabPage.Controls.Add(this.label73);
            this.measuresTabPage.Controls.Add(this.label72);
            this.measuresTabPage.Controls.Add(this.measureTablePName);
            this.measuresTabPage.Controls.Add(this.measureModelBox);
            this.measuresTabPage.Location = new System.Drawing.Point(4, 22);
            this.measuresTabPage.Name = "measuresTabPage";
            this.measuresTabPage.Size = new System.Drawing.Size(852, 727);
            this.measuresTabPage.TabIndex = 1;
            this.measuresTabPage.Text = "Measures";
            this.measuresTabPage.UseVisualStyleBackColor = true;
            this.measuresTabPage.Leave += new System.EventHandler(this.measuresTabPage_Leave);
            // 
            // measureDataGridView
            // 
            this.measureDataGridView.AllowUserToOrderColumns = true;
            this.measureDataGridView.AutoGenerateColumns = false;
            this.measureDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.measureDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.measureDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.measureDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnNameDataGridViewTextBoxColumn,
            this.PhysicalNameCol,
            this.Description,
            this.dataTypeDataGridViewTextBoxColumn,
            this.aggregationRuleColumn,
            this.DimensionRules,
            this.DisplayFunction,
            this.keyDataGridViewCheckBoxColumn,
            this.formatDataGridViewTextBoxColumn,
            this.performanceCategoryDataGridViewTextBoxColumn,
            this.improveDataGridViewTextBoxColumn,
            this.enforceLimitsDataGridViewCheckBoxColumn,
            this.minimumDataGridViewTextBoxColumn,
            this.maximumDataGridViewTextBoxColumn,
            this.minImprovementDataGridViewTextBoxColumn,
            this.maxImprovementDataGridViewTextBoxColumn,
            this.attributeWeightMeasureDataGridViewTextBoxColumn,
            this.minTargetDataGridViewTextBoxColumn,
            this.maxTargetDataGridViewTextBoxColumn,
            this.validMeasureDataGridViewTextBoxColumn,
            this.analysisMeasureDataGridViewTextBoxColumn,
            this.segmentMeasureDataGridViewTextBoxColumn,
            this.opportunityDataGridViewTextBoxColumn,
            this.filtersColumn});
            this.measureDataGridView.ContextMenuStrip = this.measureContextMenu;
            this.measureDataGridView.DataSource = this.measureColumnSource;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.measureDataGridView.DefaultCellStyle = dataGridViewCellStyle11;
            this.measureDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.measureDataGridView.Location = new System.Drawing.Point(0, 0);
            this.measureDataGridView.Name = "measureDataGridView";
            this.measureDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.measureDataGridView.RowTemplate.Height = 18;
            this.measureDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.measureDataGridView.Size = new System.Drawing.Size(852, 727);
            this.measureDataGridView.TabIndex = 118;
            this.measureDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.measureDataGridView_CellBeginEdit);
            this.measureDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureDataGridView_CellContentClick);
            this.measureDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureDataGridView_CellEndEdit);
            this.measureDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.measureDataGridView_CellValidating);
            this.measureDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureDataGridView_CellValueChanged);
            this.measureDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.measureDataGridView_DataError);
            this.measureDataGridView.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureDataGridView_RowEnter);
            this.measureDataGridView.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.measureDataGridView_UserAddedRow);
            this.measureDataGridView.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.measureDataGridView_UserDeletedRow);
            this.measureDataGridView.Leave += new System.EventHandler(this.measureDataGridView_Leave);
            // 
            // columnNameDataGridViewTextBoxColumn
            // 
            this.columnNameDataGridViewTextBoxColumn.DataPropertyName = "ColumnName";
            this.columnNameDataGridViewTextBoxColumn.Frozen = true;
            this.columnNameDataGridViewTextBoxColumn.HeaderText = "ColumnName";
            this.columnNameDataGridViewTextBoxColumn.Name = "columnNameDataGridViewTextBoxColumn";
            this.columnNameDataGridViewTextBoxColumn.Width = 125;
            // 
            // PhysicalNameCol
            // 
            this.PhysicalNameCol.DataPropertyName = "PhysicalName";
            this.PhysicalNameCol.HeaderText = "PhysicalName";
            this.PhysicalNameCol.Name = "PhysicalNameCol";
            this.PhysicalNameCol.Visible = false;
            this.PhysicalNameCol.Width = 450;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.Width = 200;
            // 
            // dataTypeDataGridViewTextBoxColumn
            // 
            this.dataTypeDataGridViewTextBoxColumn.DataPropertyName = "DataType";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataTypeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataTypeDataGridViewTextBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataTypeDataGridViewTextBoxColumn.HeaderText = "DataType";
            this.dataTypeDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Number",
            "Integer",
            "Varchar",
            "Date",
            "DateTime",
            "Float"});
            this.dataTypeDataGridViewTextBoxColumn.Name = "dataTypeDataGridViewTextBoxColumn";
            this.dataTypeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataTypeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataTypeDataGridViewTextBoxColumn.Width = 75;
            // 
            // aggregationRuleColumn
            // 
            this.aggregationRuleColumn.DataPropertyName = "AggregationRule";
            this.aggregationRuleColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aggregationRuleColumn.HeaderText = "AggregationRule";
            this.aggregationRuleColumn.Name = "aggregationRuleColumn";
            this.aggregationRuleColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.aggregationRuleColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DimensionRules
            // 
            this.DimensionRules.DataPropertyName = "DimensionRules";
            this.DimensionRules.HeaderText = "DimensionRules";
            this.DimensionRules.Name = "DimensionRules";
            this.DimensionRules.Visible = false;
            // 
            // DisplayFunction
            // 
            this.DisplayFunction.DataPropertyName = "DisplayFunction";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.NullValue = "None";
            this.DisplayFunction.DefaultCellStyle = dataGridViewCellStyle9;
            this.DisplayFunction.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.DisplayFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DisplayFunction.HeaderText = "Display";
            this.DisplayFunction.Items.AddRange(new object[] {
            "None",
            "RANK",
            "PTILE"});
            this.DisplayFunction.Name = "DisplayFunction";
            this.DisplayFunction.Width = 60;
            // 
            // keyDataGridViewCheckBoxColumn
            // 
            this.keyDataGridViewCheckBoxColumn.DataPropertyName = "Key";
            this.keyDataGridViewCheckBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.keyDataGridViewCheckBoxColumn.HeaderText = "Key";
            this.keyDataGridViewCheckBoxColumn.Name = "keyDataGridViewCheckBoxColumn";
            this.keyDataGridViewCheckBoxColumn.Width = 50;
            // 
            // formatDataGridViewTextBoxColumn
            // 
            this.formatDataGridViewTextBoxColumn.DataPropertyName = "Format";
            this.formatDataGridViewTextBoxColumn.HeaderText = "Format";
            this.formatDataGridViewTextBoxColumn.Name = "formatDataGridViewTextBoxColumn";
            // 
            // performanceCategoryDataGridViewTextBoxColumn
            // 
            this.performanceCategoryDataGridViewTextBoxColumn.DataPropertyName = "PerformanceCategory";
            this.performanceCategoryDataGridViewTextBoxColumn.HeaderText = "PerformanceCategory";
            this.performanceCategoryDataGridViewTextBoxColumn.Name = "performanceCategoryDataGridViewTextBoxColumn";
            // 
            // improveDataGridViewTextBoxColumn
            // 
            this.improveDataGridViewTextBoxColumn.DataPropertyName = "Improve";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.improveDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this.improveDataGridViewTextBoxColumn.HeaderText = "Improve";
            this.improveDataGridViewTextBoxColumn.Name = "improveDataGridViewTextBoxColumn";
            this.improveDataGridViewTextBoxColumn.Width = 50;
            // 
            // enforceLimitsDataGridViewCheckBoxColumn
            // 
            this.enforceLimitsDataGridViewCheckBoxColumn.DataPropertyName = "EnforceLimits";
            this.enforceLimitsDataGridViewCheckBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.enforceLimitsDataGridViewCheckBoxColumn.HeaderText = "EnforceLimits";
            this.enforceLimitsDataGridViewCheckBoxColumn.Name = "enforceLimitsDataGridViewCheckBoxColumn";
            this.enforceLimitsDataGridViewCheckBoxColumn.Width = 70;
            // 
            // minimumDataGridViewTextBoxColumn
            // 
            this.minimumDataGridViewTextBoxColumn.DataPropertyName = "Minimum";
            this.minimumDataGridViewTextBoxColumn.HeaderText = "Minimum";
            this.minimumDataGridViewTextBoxColumn.Name = "minimumDataGridViewTextBoxColumn";
            // 
            // maximumDataGridViewTextBoxColumn
            // 
            this.maximumDataGridViewTextBoxColumn.DataPropertyName = "Maximum";
            this.maximumDataGridViewTextBoxColumn.HeaderText = "Maximum";
            this.maximumDataGridViewTextBoxColumn.Name = "maximumDataGridViewTextBoxColumn";
            // 
            // minImprovementDataGridViewTextBoxColumn
            // 
            this.minImprovementDataGridViewTextBoxColumn.DataPropertyName = "MinImprovement";
            this.minImprovementDataGridViewTextBoxColumn.HeaderText = "MinImprovement";
            this.minImprovementDataGridViewTextBoxColumn.Name = "minImprovementDataGridViewTextBoxColumn";
            // 
            // maxImprovementDataGridViewTextBoxColumn
            // 
            this.maxImprovementDataGridViewTextBoxColumn.DataPropertyName = "MaxImprovement";
            this.maxImprovementDataGridViewTextBoxColumn.HeaderText = "MaxImprovement";
            this.maxImprovementDataGridViewTextBoxColumn.Name = "maxImprovementDataGridViewTextBoxColumn";
            // 
            // attributeWeightMeasureDataGridViewTextBoxColumn
            // 
            this.attributeWeightMeasureDataGridViewTextBoxColumn.DataPropertyName = "AttributeWeightMeasure";
            this.attributeWeightMeasureDataGridViewTextBoxColumn.HeaderText = "AttributeWeightMeasure";
            this.attributeWeightMeasureDataGridViewTextBoxColumn.Name = "attributeWeightMeasureDataGridViewTextBoxColumn";
            // 
            // minTargetDataGridViewTextBoxColumn
            // 
            this.minTargetDataGridViewTextBoxColumn.DataPropertyName = "MinTarget";
            this.minTargetDataGridViewTextBoxColumn.HeaderText = "MinTarget";
            this.minTargetDataGridViewTextBoxColumn.Name = "minTargetDataGridViewTextBoxColumn";
            // 
            // maxTargetDataGridViewTextBoxColumn
            // 
            this.maxTargetDataGridViewTextBoxColumn.DataPropertyName = "MaxTarget";
            this.maxTargetDataGridViewTextBoxColumn.HeaderText = "MaxTarget";
            this.maxTargetDataGridViewTextBoxColumn.Name = "maxTargetDataGridViewTextBoxColumn";
            // 
            // validMeasureDataGridViewTextBoxColumn
            // 
            this.validMeasureDataGridViewTextBoxColumn.DataPropertyName = "ValidMeasure";
            this.validMeasureDataGridViewTextBoxColumn.HeaderText = "ValidMeasure";
            this.validMeasureDataGridViewTextBoxColumn.Name = "validMeasureDataGridViewTextBoxColumn";
            // 
            // analysisMeasureDataGridViewTextBoxColumn
            // 
            this.analysisMeasureDataGridViewTextBoxColumn.DataPropertyName = "AnalysisMeasure";
            this.analysisMeasureDataGridViewTextBoxColumn.HeaderText = "AnalysisMeasure";
            this.analysisMeasureDataGridViewTextBoxColumn.Name = "analysisMeasureDataGridViewTextBoxColumn";
            // 
            // segmentMeasureDataGridViewTextBoxColumn
            // 
            this.segmentMeasureDataGridViewTextBoxColumn.DataPropertyName = "SegmentMeasure";
            this.segmentMeasureDataGridViewTextBoxColumn.HeaderText = "SegmentMeasure";
            this.segmentMeasureDataGridViewTextBoxColumn.Name = "segmentMeasureDataGridViewTextBoxColumn";
            // 
            // opportunityDataGridViewTextBoxColumn
            // 
            this.opportunityDataGridViewTextBoxColumn.DataPropertyName = "Opportunity";
            this.opportunityDataGridViewTextBoxColumn.HeaderText = "Opportunity";
            this.opportunityDataGridViewTextBoxColumn.Name = "opportunityDataGridViewTextBoxColumn";
            this.opportunityDataGridViewTextBoxColumn.Width = 250;
            // 
            // filtersColumn
            // 
            this.filtersColumn.DataPropertyName = "FilteredMeasures";
            this.filtersColumn.FillWeight = 60F;
            this.filtersColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filtersColumn.HeaderText = "Filters";
            this.filtersColumn.Name = "filtersColumn";
            this.filtersColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.filtersColumn.Width = 60;
            // 
            // measureContextMenu
            // 
            this.measureContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMeasureGrainToolStripMenuItem});
            this.measureContextMenu.Name = "measureContextMenu";
            this.measureContextMenu.Size = new System.Drawing.Size(131, 26);
            // 
            // viewMeasureGrainToolStripMenuItem
            // 
            this.viewMeasureGrainToolStripMenuItem.Name = "viewMeasureGrainToolStripMenuItem";
            this.viewMeasureGrainToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.viewMeasureGrainToolStripMenuItem.Text = "View Grain";
            this.viewMeasureGrainToolStripMenuItem.Click += new System.EventHandler(this.viewMeasureGrain_Click);
            // 
            // measureColumnSource
            // 
            this.measureColumnSource.DataMember = "MeasureColumns";
            this.measureColumnSource.DataSource = this.schemaDataSet;
            // 
            // recalculateCardinality
            // 
            this.recalculateCardinality.AutoSize = true;
            this.recalculateCardinality.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recalculateCardinality.Location = new System.Drawing.Point(185, 97);
            this.recalculateCardinality.Name = "recalculateCardinality";
            this.recalculateCardinality.Size = new System.Drawing.Size(134, 17);
            this.recalculateCardinality.TabIndex = 126;
            this.recalculateCardinality.Text = "Recalculate Cardinality";
            this.recalculateCardinality.UseVisualStyleBackColor = true;
            // 
            // measureMapGridView
            // 
            this.measureMapGridView.AllowUserToOrderColumns = true;
            this.measureMapGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measureMapGridView.AutoGenerateColumns = false;
            this.measureMapGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.measureMapGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.measureMapGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureMapGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.measureMapGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measureMapGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.measureComboBoxColumn,
            this.TableName,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn2,
            this.AggRuleOverride});
            this.measureMapGridView.DataSource = this.measureColumnMappingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.measureMapGridView.DefaultCellStyle = dataGridViewCellStyle14;
            this.measureMapGridView.Location = new System.Drawing.Point(2, 244);
            this.measureMapGridView.Name = "measureMapGridView";
            this.measureMapGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.measureMapGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.measureMapGridView.RowTemplate.Height = 19;
            this.measureMapGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.measureMapGridView.Size = new System.Drawing.Size(846, 474);
            this.measureMapGridView.TabIndex = 124;
            this.measureMapGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.measureMapGridView_CellBeginEdit);
            this.measureMapGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.measureMapGridView_CellEndEdit);
            this.measureMapGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.measureMapGridView_DataError);
            this.measureMapGridView.SelectionChanged += new System.EventHandler(this.measureMapGridView_SelectionChanged);
            // 
            // measureComboBoxColumn
            // 
            this.measureComboBoxColumn.DataPropertyName = "ColumnName";
            this.measureComboBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.measureComboBoxColumn.HeaderText = "ColumnName";
            this.measureComboBoxColumn.Name = "measureComboBoxColumn";
            this.measureComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.measureComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.measureComboBoxColumn.Width = 175;
            // 
            // TableName
            // 
            this.TableName.DataPropertyName = "ColumnName";
            this.TableName.HeaderText = "TableName";
            this.TableName.Name = "TableName";
            this.TableName.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PhysicalName";
            this.dataGridViewTextBoxColumn4.HeaderText = "PhysicalName";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 425;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Qualify";
            this.dataGridViewCheckBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dataGridViewCheckBoxColumn2.HeaderText = "Qualify";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 50;
            // 
            // AggRuleOverride
            // 
            this.AggRuleOverride.DataPropertyName = "AggRuleOverride";
            this.AggRuleOverride.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AggRuleOverride.HeaderText = "Agg Rule Override";
            this.AggRuleOverride.Items.AddRange(new object[] {
            "",
            "SUM",
            "AVG",
            "COUNT",
            "COUNT DISTINCT",
            "MIN",
            "MAX",
            "STDEV"});
            this.AggRuleOverride.Name = "AggRuleOverride";
            this.AggRuleOverride.Width = 125;
            // 
            // measureColumnMappingSource
            // 
            this.measureColumnMappingSource.DataMember = "MeasureColumnMappings";
            this.measureColumnMappingSource.DataSource = this.schemaDataSet;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(401, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 123;
            this.label9.Text = "Content Filters";
            // 
            // measureContentFilters
            // 
            this.measureContentFilters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.measureContentFilters.CheckOnClick = true;
            this.measureContentFilters.FormattingEnabled = true;
            this.measureContentFilters.Location = new System.Drawing.Point(481, 123);
            this.measureContentFilters.Name = "measureContentFilters";
            this.measureContentFilters.Size = new System.Drawing.Size(301, 62);
            this.measureContentFilters.TabIndex = 122;
            this.measureContentFilters.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainAdminForm_MouseMove);
            // 
            // mtCacheable
            // 
            this.mtCacheable.AutoSize = true;
            this.mtCacheable.Location = new System.Drawing.Point(245, 34);
            this.mtCacheable.Name = "mtCacheable";
            this.mtCacheable.Size = new System.Drawing.Size(77, 17);
            this.mtCacheable.TabIndex = 121;
            this.mtCacheable.Text = "Cacheable";
            this.mtCacheable.UseVisualStyleBackColor = true;
            // 
            // mtConnectionBox
            // 
            this.mtConnectionBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mtConnectionBox.Location = new System.Drawing.Point(93, 32);
            this.mtConnectionBox.Name = "mtConnectionBox";
            this.mtConnectionBox.Size = new System.Drawing.Size(146, 21);
            this.mtConnectionBox.TabIndex = 120;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 23);
            this.label7.TabIndex = 119;
            this.label7.Text = "Connection";
            // 
            // defineMSubstitutions
            // 
            this.defineMSubstitutions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.defineMSubstitutions.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defineMSubstitutions.Location = new System.Drawing.Point(276, 123);
            this.defineMSubstitutions.Margin = new System.Windows.Forms.Padding(0);
            this.defineMSubstitutions.Name = "defineMSubstitutions";
            this.defineMSubstitutions.Size = new System.Drawing.Size(119, 18);
            this.defineMSubstitutions.TabIndex = 117;
            this.defineMSubstitutions.Text = "Column Substitutions";
            this.defineMSubstitutions.Click += new System.EventHandler(this.defineMSubstitutions_Click);
            // 
            // defineMeasureTableSource
            // 
            this.defineMeasureTableSource.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.defineMeasureTableSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defineMeasureTableSource.Location = new System.Drawing.Point(204, 56);
            this.defineMeasureTableSource.Margin = new System.Windows.Forms.Padding(0);
            this.defineMeasureTableSource.Name = "defineMeasureTableSource";
            this.defineMeasureTableSource.Size = new System.Drawing.Size(119, 18);
            this.defineMeasureTableSource.TabIndex = 116;
            this.defineMeasureTableSource.Text = "Define Table Source";
            this.defineMeasureTableSource.Click += new System.EventHandler(this.defineMeasureTableSource_Click);
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(235, 158);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(48, 23);
            this.label51.TabIndex = 102;
            this.label51.Text = "Prefix";
            // 
            // measureInheritPrefix
            // 
            this.measureInheritPrefix.BackColor = System.Drawing.SystemColors.Window;
            this.measureInheritPrefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.measureInheritPrefix.Location = new System.Drawing.Point(283, 158);
            this.measureInheritPrefix.Name = "measureInheritPrefix";
            this.measureInheritPrefix.Size = new System.Drawing.Size(112, 20);
            this.measureInheritPrefix.TabIndex = 7;
            // 
            // measureInheritBox
            // 
            this.measureInheritBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.measureInheritBox.Location = new System.Drawing.Point(91, 158);
            this.measureInheritBox.Name = "measureInheritBox";
            this.measureInheritBox.Size = new System.Drawing.Size(136, 21);
            this.measureInheritBox.TabIndex = 6;
            // 
            // inheritMeasures
            // 
            this.inheritMeasures.Location = new System.Drawing.Point(3, 158);
            this.inheritMeasures.Name = "inheritMeasures";
            this.inheritMeasures.Size = new System.Drawing.Size(88, 24);
            this.inheritMeasures.TabIndex = 99;
            this.inheritMeasures.Text = "Inherit From";
            // 
            // measureDerived
            // 
            this.measureDerived.Location = new System.Drawing.Point(181, 117);
            this.measureDerived.Name = "measureDerived";
            this.measureDerived.Size = new System.Drawing.Size(104, 24);
            this.measureDerived.TabIndex = 4;
            this.measureDerived.Text = "Derived Table";
            this.measureDerived.CheckedChanged += new System.EventHandler(this.measureDerived_CheckedChanged);
            // 
            // measureView
            // 
            this.measureView.Location = new System.Drawing.Point(102, 133);
            this.measureView.Name = "measureView";
            this.measureView.Size = new System.Drawing.Size(104, 24);
            this.measureView.TabIndex = 5;
            this.measureView.Text = "Opaque View";
            this.measureView.CheckedChanged += new System.EventHandler(this.measureView_CheckedChanged);
            // 
            // measureNormal
            // 
            this.measureNormal.Checked = true;
            this.measureNormal.Location = new System.Drawing.Point(102, 117);
            this.measureNormal.Name = "measureNormal";
            this.measureNormal.Size = new System.Drawing.Size(64, 24);
            this.measureNormal.TabIndex = 3;
            this.measureNormal.TabStop = true;
            this.measureNormal.Text = "Normal";
            this.measureNormal.CheckedChanged += new System.EventHandler(this.measureNormal_CheckedChanged);
            // 
            // measureViewSQL
            // 
            this.measureViewSQL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measureViewSQL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.measureViewSQL.Location = new System.Drawing.Point(330, 33);
            this.measureViewSQL.Multiline = true;
            this.measureViewSQL.Name = "measureViewSQL";
            this.measureViewSQL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.measureViewSQL.Size = new System.Drawing.Size(519, 87);
            this.measureViewSQL.TabIndex = 8;
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(327, 8);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(136, 20);
            this.label82.TabIndex = 93;
            this.label82.Text = "Opaque View Query";
            // 
            // measureTableCardinality
            // 
            this.measureTableCardinality.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.measureTableCardinality.Location = new System.Drawing.Point(70, 97);
            this.measureTableCardinality.Name = "measureTableCardinality";
            this.measureTableCardinality.Size = new System.Drawing.Size(109, 20);
            this.measureTableCardinality.TabIndex = 2;
            // 
            // label78
            // 
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(3, 97);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(112, 23);
            this.label78.TabIndex = 79;
            this.label78.Text = "Cardinality";
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(3, 121);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(112, 23);
            this.label73.TabIndex = 74;
            this.label73.Text = "Table Type";
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(3, 56);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(154, 15);
            this.label72.TabIndex = 72;
            this.label72.Text = "Physical Table Source";
            // 
            // measureTablePName
            // 
            this.measureTablePName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.measureTablePName.Location = new System.Drawing.Point(6, 74);
            this.measureTablePName.Name = "measureTablePName";
            this.measureTablePName.ReadOnly = true;
            this.measureTablePName.Size = new System.Drawing.Size(317, 20);
            this.measureTablePName.TabIndex = 1;
            // 
            // measureModelBox
            // 
            this.measureModelBox.Controls.Add(this.measureSuccess);
            this.measureModelBox.Controls.Add(this.measurePerformance);
            this.measureModelBox.Controls.Add(this.measurePeers);
            this.measureModelBox.Controls.Add(this.measureNone);
            this.measureModelBox.Location = new System.Drawing.Point(2, 188);
            this.measureModelBox.Name = "measureModelBox";
            this.measureModelBox.Size = new System.Drawing.Size(395, 50);
            this.measureModelBox.TabIndex = 125;
            this.measureModelBox.TabStop = false;
            this.measureModelBox.Text = "Modeling";
            // 
            // measureSuccess
            // 
            this.measureSuccess.AutoSize = true;
            this.measureSuccess.Location = new System.Drawing.Point(264, 19);
            this.measureSuccess.Name = "measureSuccess";
            this.measureSuccess.Size = new System.Drawing.Size(66, 17);
            this.measureSuccess.TabIndex = 3;
            this.measureSuccess.Text = "Success";
            this.measureSuccess.UseVisualStyleBackColor = true;
            // 
            // measurePerformance
            // 
            this.measurePerformance.AutoSize = true;
            this.measurePerformance.Location = new System.Drawing.Point(158, 19);
            this.measurePerformance.Name = "measurePerformance";
            this.measurePerformance.Size = new System.Drawing.Size(85, 17);
            this.measurePerformance.TabIndex = 2;
            this.measurePerformance.Text = "Performance";
            this.measurePerformance.UseVisualStyleBackColor = true;
            this.measurePerformance.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // measurePeers
            // 
            this.measurePeers.AutoSize = true;
            this.measurePeers.Location = new System.Drawing.Point(78, 19);
            this.measurePeers.Name = "measurePeers";
            this.measurePeers.Size = new System.Drawing.Size(61, 17);
            this.measurePeers.TabIndex = 1;
            this.measurePeers.Text = "Peering";
            this.measurePeers.UseVisualStyleBackColor = true;
            // 
            // measureNone
            // 
            this.measureNone.AutoSize = true;
            this.measureNone.Checked = true;
            this.measureNone.Location = new System.Drawing.Point(7, 19);
            this.measureNone.Name = "measureNone";
            this.measureNone.Size = new System.Drawing.Size(51, 17);
            this.measureNone.TabIndex = 0;
            this.measureNone.TabStop = true;
            this.measureNone.Text = "None";
            this.measureNone.UseVisualStyleBackColor = true;
            // 
            // joinsTabPage
            // 
            this.joinsTabPage.Controls.Add(this.joinDataGridView);
            this.joinsTabPage.Controls.Add(this.label4);
            this.joinsTabPage.Controls.Add(this.label1);
            this.joinsTabPage.Location = new System.Drawing.Point(4, 22);
            this.joinsTabPage.Name = "joinsTabPage";
            this.joinsTabPage.Size = new System.Drawing.Size(852, 727);
            this.joinsTabPage.TabIndex = 10;
            this.joinsTabPage.Text = "Joins";
            this.joinsTabPage.UseVisualStyleBackColor = true;
            // 
            // joinDataGridView
            // 
            this.joinDataGridView.AllowUserToOrderColumns = true;
            this.joinDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.joinDataGridView.AutoGenerateColumns = false;
            this.joinDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.joinDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.joinDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.joinDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.joinDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.table1Column,
            this.table2Column,
            this.joinConditionDataGridViewTextBoxColumn,
            this.joinTypeDataGridViewComboBoxColumn,
            this.redundantDataGridViewCheckBoxColumn,
            this.Level,
            this.AutoGenerated,
            this.Invalid,
            this.federatedDataGridViewCheckBoxColumn,
            this.federatedJoinKeysDataGridViewTextBoxColumn});
            this.joinDataGridView.DataSource = this.joinSource;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.joinDataGridView.DefaultCellStyle = dataGridViewCellStyle19;
            this.joinDataGridView.Location = new System.Drawing.Point(2, 89);
            this.joinDataGridView.Name = "joinDataGridView";
            this.joinDataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.joinDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.joinDataGridView.RowTemplate.Height = 18;
            this.joinDataGridView.Size = new System.Drawing.Size(850, 635);
            this.joinDataGridView.TabIndex = 3;
            this.joinDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.joinDataGridView_CellBeginEdit);
            this.joinDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.joinDataGridView_CellEndEdit);
            this.joinDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.joinDataGridView_CellValueChanged);
            this.joinDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.joinDataGridView_DataError);
            // 
            // table1Column
            // 
            this.table1Column.DataPropertyName = "Table1";
            this.table1Column.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.table1Column.HeaderText = "Table 1";
            this.table1Column.Name = "table1Column";
            this.table1Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.table1Column.Sorted = true;
            this.table1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.table1Column.Width = 150;
            // 
            // table2Column
            // 
            this.table2Column.DataPropertyName = "Table2";
            this.table2Column.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.table2Column.HeaderText = "Table 2";
            this.table2Column.Name = "table2Column";
            this.table2Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.table2Column.Sorted = true;
            this.table2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.table2Column.Width = 150;
            // 
            // joinConditionDataGridViewTextBoxColumn
            // 
            this.joinConditionDataGridViewTextBoxColumn.DataPropertyName = "JoinCondition";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.joinConditionDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.joinConditionDataGridViewTextBoxColumn.HeaderText = "Join Condition";
            this.joinConditionDataGridViewTextBoxColumn.Name = "joinConditionDataGridViewTextBoxColumn";
            this.joinConditionDataGridViewTextBoxColumn.Width = 400;
            // 
            // joinTypeDataGridViewComboBoxColumn
            // 
            this.joinTypeDataGridViewComboBoxColumn.DataPropertyName = "JoinType";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.joinTypeDataGridViewComboBoxColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this.joinTypeDataGridViewComboBoxColumn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.joinTypeDataGridViewComboBoxColumn.HeaderText = "Type";
            this.joinTypeDataGridViewComboBoxColumn.Items.AddRange(new object[] {
            "Inner",
            "Left Outer",
            "Right Outer",
            "Full Outer"});
            this.joinTypeDataGridViewComboBoxColumn.Name = "joinTypeDataGridViewComboBoxColumn";
            this.joinTypeDataGridViewComboBoxColumn.Width = 90;
            // 
            // redundantDataGridViewCheckBoxColumn
            // 
            this.redundantDataGridViewCheckBoxColumn.DataPropertyName = "Redundant";
            this.redundantDataGridViewCheckBoxColumn.HeaderText = "Redundant";
            this.redundantDataGridViewCheckBoxColumn.Name = "redundantDataGridViewCheckBoxColumn";
            // 
            // Level
            // 
            this.Level.DataPropertyName = "Level";
            this.Level.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Level.HeaderText = "Level";
            this.Level.Name = "Level";
            // 
            // AutoGenerated
            // 
            this.AutoGenerated.DataPropertyName = "AutoGenerated";
            this.AutoGenerated.HeaderText = "Auto Generated";
            this.AutoGenerated.Name = "AutoGenerated";
            this.AutoGenerated.ReadOnly = true;
            // 
            // Invalid
            // 
            this.Invalid.DataPropertyName = "Invalid";
            this.Invalid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Invalid.HeaderText = "Invalid";
            this.Invalid.Name = "Invalid";
            //
            //Federated
            //
            this.federatedDataGridViewCheckBoxColumn.DataPropertyName = "Federated";
            this.federatedDataGridViewCheckBoxColumn.HeaderText = "Federated";
            this.federatedDataGridViewCheckBoxColumn.Name = "federatedDataGridViewCheckBoxColumn";
            //
            //Federated Join Keys
            //
            this.federatedJoinKeysDataGridViewTextBoxColumn.DataPropertyName = "FederatedJoinKeys";
            this.federatedJoinKeysDataGridViewTextBoxColumn.HeaderText = "FederatedJoinKeys";
            this.federatedJoinKeysDataGridViewTextBoxColumn.Name = "federatedJoinKeysDataGridViewTextBoxColumn";
            // 
            // joinSource
            // 
            this.joinSource.DataMember = "Joins";
            this.joinSource.DataSource = this.schemaDataSet;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(710, 65);
            this.label4.TabIndex = 2;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Join Logical Measure Tables to Logical Dimension Tables:";
            // 
            // dimensionTableContextMenu
            // 
            this.dimensionTableContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.newDimensionTableMenuItem,
            this.removeDimensionTableMenuItem});
            this.dimensionTableContextMenu.Popup += new System.EventHandler(this.dimensionTableContextMenu_Popup);
            // 
            // newDimensionTableMenuItem
            // 
            this.newDimensionTableMenuItem.Index = 0;
            this.newDimensionTableMenuItem.Text = "New Table";
            this.newDimensionTableMenuItem.Click += new System.EventHandler(this.newDimensionTableMenuItem_Click);
            // 
            // removeDimensionTableMenuItem
            // 
            this.removeDimensionTableMenuItem.Index = 1;
            this.removeDimensionTableMenuItem.Text = "Remove Table";
            this.removeDimensionTableMenuItem.Click += new System.EventHandler(this.removeDimensionTableMenuItem_Click);
            // 
            // measureTableContextMenu
            // 
            this.measureTableContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.addMeasureTable,
            this.removeMeasureTable});
            // 
            // addMeasureTable
            // 
            this.addMeasureTable.Index = 0;
            this.addMeasureTable.Text = "New Table";
            this.addMeasureTable.Click += new System.EventHandler(this.addMeasureTable_Click);
            // 
            // removeMeasureTable
            // 
            this.removeMeasureTable.Index = 1;
            this.removeMeasureTable.Text = "Remove Table";
            this.removeMeasureTable.Click += new System.EventHandler(this.removeMeasureTable_Click);
            // 
            // mainTree
            // 
            this.mainTree.AllowDrop = true;
            this.mainTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mainTree.HideSelection = false;
            this.mainTree.ImageIndex = 0;
            this.mainTree.ImageList = this.mainTreeImageList;
            this.mainTree.Location = new System.Drawing.Point(0, 0);
            this.mainTree.Name = "mainTree";
            treeNode1.ImageIndex = 6;
            treeNode1.Name = "";
            treeNode1.SelectedImageIndex = 6;
            treeNode1.Text = "General";
            treeNode2.ImageIndex = 6;
            treeNode2.Name = "";
            treeNode2.SelectedImageIndex = 6;
            treeNode2.Text = "Reference Populations";
            treeNode3.ImageIndex = 6;
            treeNode3.Name = "";
            treeNode3.SelectedImageIndex = 6;
            treeNode3.Text = "Performance";
            treeNode4.ImageIndex = 17;
            treeNode4.Name = "";
            treeNode4.SelectedImageIndex = 17;
            treeNode4.Text = "Model Datasets";
            treeNode5.ImageIndex = 18;
            treeNode5.Name = "";
            treeNode5.SelectedImageIndex = 18;
            treeNode5.Text = "Success Patterns";
            treeNode6.ImageIndex = 19;
            treeNode6.Name = "";
            treeNode6.SelectedImageIndex = 19;
            treeNode6.Text = "Outcomes";
            treeNode7.ImageIndex = 10;
            treeNode7.Name = "";
            treeNode7.SelectedImageIndex = 10;
            treeNode7.Text = "Filters";
            treeNode8.ImageIndex = 21;
            treeNode8.Name = "";
            treeNode8.SelectedImageIndex = 21;
            treeNode8.Text = "Operations";
            treeNode9.ImageIndex = 3;
            treeNode9.Name = "";
            treeNode9.SelectedImageIndex = 3;
            treeNode9.Text = "Business Modeling";
            treeNode10.ImageIndex = 0;
            treeNode10.Name = "BaseNode";
            treeNode10.SelectedImageIndex = 0;
            treeNode10.Text = "Base";
            treeNode11.ImageIndex = 0;
            treeNode11.Name = "DerivedNode";
            treeNode11.SelectedImageIndex = 0;
            treeNode11.Text = "Derived";
            treeNode12.ImageIndex = 1;
            treeNode12.Name = "";
            treeNode12.SelectedImageIndex = 1;
            treeNode12.Text = "Measures";
            treeNode13.ImageIndex = 1;
            treeNode13.Name = "";
            treeNode13.SelectedImageIndex = 1;
            treeNode13.Text = "Dimensions";
            treeNode14.ImageIndex = 9;
            treeNode14.Name = "";
            treeNode14.SelectedImageKey = "er.bmp";
            treeNode14.Text = "Joins";
            treeNode15.ImageIndex = 20;
            treeNode15.Name = "";
            treeNode15.SelectedImageIndex = 20;
            treeNode15.Text = "Virtual Columns";
            treeNode16.ImageIndex = 7;
            treeNode16.Name = "";
            treeNode16.SelectedImageIndex = 7;
            treeNode16.Text = "Hierarchies";
            treeNode17.ImageIndex = 8;
            treeNode17.Name = "";
            treeNode17.SelectedImageIndex = 8;
            treeNode17.Text = "Aggregates";
            treeNode18.ImageIndex = 6;
            treeNode18.Name = "";
            treeNode18.SelectedImageIndex = 6;
            treeNode18.Text = "Subject Areas";
            treeNode19.ImageIndex = 2;
            treeNode19.Name = "";
            treeNode19.SelectedImageIndex = 2;
            treeNode19.Text = "Dimensional Mapping";
            treeNode20.ImageIndex = 26;
            treeNode20.Name = "SourceFileNode";
            treeNode20.SelectedImageIndex = 26;
            treeNode20.Text = "Data Sources";
            treeNode21.ImageIndex = 27;
            treeNode21.Name = "StagingNode";
            treeNode21.SelectedImageIndex = 27;
            treeNode21.Text = "Staging Tables";
            treeNode22.ImageIndex = 10;
            treeNode22.Name = "StagingFilterNode";
            treeNode22.SelectedImageIndex = 10;
            treeNode22.Text = "Staging Filters";
            treeNode23.ImageIndex = 28;
            treeNode23.Name = "loadGroupsNode";
            treeNode23.SelectedImageIndex = 28;
            treeNode23.Text = "Load Groups";
            treeNode24.ImageIndex = 30;
            treeNode24.Name = "Script Groups";
            treeNode24.SelectedImageIndex = 30;
            treeNode24.Text = "Script Groups";
            treeNode25.ImageIndex = 31;
            treeNode25.Name = "BuildProperties";
            treeNode25.SelectedImageIndex = 31;
            treeNode25.Text = "Build Properties";
            treeNode26.ImageIndex = 25;
            treeNode26.Name = "WarehouseNode";
            treeNode26.SelectedImageIndex = 25;
            treeNode26.Text = "Warehouse";
            treeNode27.ImageIndex = 11;
            treeNode27.Name = "";
            treeNode27.SelectedImageIndex = 11;
            treeNode27.Text = "Database Connection";
            treeNode28.ImageIndex = 12;
            treeNode28.Name = "";
            treeNode28.SelectedImageIndex = 12;
            treeNode28.Text = "Model Parameters";
            treeNode29.ImageIndex = 13;
            treeNode29.Name = "";
            treeNode29.SelectedImageIndex = 13;
            treeNode29.Text = "Server Properties";
            treeNode30.ImageIndex = 14;
            treeNode30.Name = "";
            treeNode30.SelectedImageIndex = 14;
            treeNode30.Text = "Users and Groups";
            treeNode31.ImageIndex = 15;
            treeNode31.Name = "";
            treeNode31.SelectedImageIndex = 15;
            treeNode31.Text = "Variables";
            treeNode32.ImageIndex = 16;
            treeNode32.Name = "Resources";
            treeNode32.SelectedImageIndex = 16;
            treeNode32.Text = "Resources";
            treeNode33.ImageIndex = 29;
            treeNode33.Name = "Broadcasts";
            treeNode33.SelectedImageIndex = 29;
            treeNode33.Text = "Broadcasts";
            treeNode34.ImageIndex = 4;
            treeNode34.Name = "";
            treeNode34.SelectedImageIndex = 4;
            treeNode34.Text = "System";
            treeNode35.ImageIndex = 5;
            treeNode35.Name = "";
            treeNode35.SelectedImageIndex = 5;
            treeNode35.Text = "Runtime";
            this.mainTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode19,
            treeNode26,
            treeNode34,
            treeNode35});
            this.mainTree.SelectedImageIndex = 0;
            this.mainTree.Size = new System.Drawing.Size(225, 731);
            this.mainTree.TabIndex = 1;
            this.mainTree.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.mainTree_AfterLabelEdit);
            this.mainTree.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.mainTree_AfterExpandCollapse);
            this.mainTree.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.mainTree_AfterExpandCollapse);
            this.mainTree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.mainTree_ItemDrag);
            this.mainTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.mainTree_AfterSelect);
            this.mainTree.DragDrop += new System.Windows.Forms.DragEventHandler(this.mainTree_DragDrop);
            this.mainTree.DragEnter += new System.Windows.Forms.DragEventHandler(this.mainTree_DragEnter);
            // 
            // mainTreeImageList
            // 
            this.mainTreeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("mainTreeImageList.ImageStream")));
            this.mainTreeImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.mainTreeImageList.Images.SetKeyName(0, "");
            this.mainTreeImageList.Images.SetKeyName(1, "");
            this.mainTreeImageList.Images.SetKeyName(2, "");
            this.mainTreeImageList.Images.SetKeyName(3, "");
            this.mainTreeImageList.Images.SetKeyName(4, "");
            this.mainTreeImageList.Images.SetKeyName(5, "");
            this.mainTreeImageList.Images.SetKeyName(6, "");
            this.mainTreeImageList.Images.SetKeyName(7, "");
            this.mainTreeImageList.Images.SetKeyName(8, "");
            this.mainTreeImageList.Images.SetKeyName(9, "");
            this.mainTreeImageList.Images.SetKeyName(10, "");
            this.mainTreeImageList.Images.SetKeyName(11, "");
            this.mainTreeImageList.Images.SetKeyName(12, "");
            this.mainTreeImageList.Images.SetKeyName(13, "");
            this.mainTreeImageList.Images.SetKeyName(14, "");
            this.mainTreeImageList.Images.SetKeyName(15, "");
            this.mainTreeImageList.Images.SetKeyName(16, "");
            this.mainTreeImageList.Images.SetKeyName(17, "");
            this.mainTreeImageList.Images.SetKeyName(18, "");
            this.mainTreeImageList.Images.SetKeyName(19, "");
            this.mainTreeImageList.Images.SetKeyName(20, "");
            this.mainTreeImageList.Images.SetKeyName(21, "");
            this.mainTreeImageList.Images.SetKeyName(22, "");
            this.mainTreeImageList.Images.SetKeyName(23, "");
            this.mainTreeImageList.Images.SetKeyName(24, "");
            this.mainTreeImageList.Images.SetKeyName(25, "");
            this.mainTreeImageList.Images.SetKeyName(26, "");
            this.mainTreeImageList.Images.SetKeyName(27, "");
            this.mainTreeImageList.Images.SetKeyName(28, "");
            this.mainTreeImageList.Images.SetKeyName(29, "System.Drawing.Bitmap");
            this.mainTreeImageList.Images.SetKeyName(30, "System.Drawing.Bitmap");
            this.mainTreeImageList.Images.SetKeyName(31, "build.bmp");
            // 
            // dataGridTextBoxColumn7
            // 
            this.dataGridTextBoxColumn7.Format = "";
            this.dataGridTextBoxColumn7.FormatInfo = null;
            this.dataGridTextBoxColumn7.HeaderText = "Format";
            this.dataGridTextBoxColumn7.MappingName = "Format";
            this.dataGridTextBoxColumn7.Width = 75;
            // 
            // dataGridBoolColumn1
            // 
            this.dataGridBoolColumn1.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridBoolColumn1.AllowNull = false;
            this.dataGridBoolColumn1.HeaderText = "Key";
            this.dataGridBoolColumn1.MappingName = "Key";
            this.dataGridBoolColumn1.Width = 50;
            // 
            // dataGridTextBoxColumn13
            // 
            this.dataGridTextBoxColumn13.Format = "";
            this.dataGridTextBoxColumn13.FormatInfo = null;
            this.dataGridTextBoxColumn13.HeaderText = "Performance Category";
            this.dataGridTextBoxColumn13.MappingName = "PerformanceCategory";
            this.dataGridTextBoxColumn13.Width = 75;
            // 
            // dataGridBoolColumn3
            // 
            this.dataGridBoolColumn3.HeaderText = "Qualify";
            this.dataGridBoolColumn3.MappingName = "Qualify";
            this.dataGridBoolColumn3.Width = 50;
            // 
            // dataGridTextBoxColumn15
            // 
            this.dataGridTextBoxColumn15.Format = "";
            this.dataGridTextBoxColumn15.FormatInfo = null;
            this.dataGridTextBoxColumn15.HeaderText = "Physical Name/Formula";
            this.dataGridTextBoxColumn15.MappingName = "PhysicalName";
            this.dataGridTextBoxColumn15.Width = 450;
            // 
            // dataGridTextBoxColumn17
            // 
            this.dataGridTextBoxColumn17.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            this.dataGridTextBoxColumn17.Format = "";
            this.dataGridTextBoxColumn17.FormatInfo = null;
            this.dataGridTextBoxColumn17.HeaderText = "Agg Rule";
            this.dataGridTextBoxColumn17.MappingName = "AggregationRule";
            this.dataGridTextBoxColumn17.Width = 75;
            // 
            // exportSaveFileDialog
            // 
            this.exportSaveFileDialog.DefaultExt = "txt";
            this.exportSaveFileDialog.Filter = "Text Files (*.txt)|*.txt";
            // 
            // measureTableMenu
            // 
            this.measureTableMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMeasureTableDefinitionToolStripMenuItem,
            this.removeMeasureTableDefinitionToolStripMenuItem,
            this.viewMeasureTableGrainToolStripMenuItem,
            this.viewUnmappedMeasuresToolStripMenuItem});
            this.measureTableMenu.Name = "measureTableMenu";
            this.measureTableMenu.ShowImageMargin = false;
            this.measureTableMenu.Size = new System.Drawing.Size(228, 92);
            // 
            // newMeasureTableDefinitionToolStripMenuItem
            // 
            this.newMeasureTableDefinitionToolStripMenuItem.Name = "newMeasureTableDefinitionToolStripMenuItem";
            this.newMeasureTableDefinitionToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.newMeasureTableDefinitionToolStripMenuItem.Text = "New Measure Table Definition";
            this.newMeasureTableDefinitionToolStripMenuItem.Click += new System.EventHandler(this.newMeasureTable_Click);
            // 
            // removeMeasureTableDefinitionToolStripMenuItem
            // 
            this.removeMeasureTableDefinitionToolStripMenuItem.Name = "removeMeasureTableDefinitionToolStripMenuItem";
            this.removeMeasureTableDefinitionToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.removeMeasureTableDefinitionToolStripMenuItem.Text = "Remove Measure Table Definition";
            this.removeMeasureTableDefinitionToolStripMenuItem.Click += new System.EventHandler(this.removeMeasureTable_Click);
            // 
            // viewMeasureTableGrainToolStripMenuItem
            // 
            this.viewMeasureTableGrainToolStripMenuItem.Name = "viewMeasureTableGrainToolStripMenuItem";
            this.viewMeasureTableGrainToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.viewMeasureTableGrainToolStripMenuItem.Text = "View Grain";
            this.viewMeasureTableGrainToolStripMenuItem.Click += new System.EventHandler(this.viewMeasureTableGrainToolStripMenuItem_Click);
            // 
            // viewUnmappedMeasuresToolStripMenuItem
            // 
            this.viewUnmappedMeasuresToolStripMenuItem.Name = "viewUnmappedMeasuresToolStripMenuItem";
            this.viewUnmappedMeasuresToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.viewUnmappedMeasuresToolStripMenuItem.Text = "View Unmapped Measures";
            this.viewUnmappedMeasuresToolStripMenuItem.Click += new System.EventHandler(this.viewUnmappedMeasuresMenuItem_Click);
            // 
            // dimensionTableMenu
            // 
            this.dimensionTableMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newDimensionToolStripMenuItem,
            this.removeDimensionToolStripMenuItem,
            this.newDimensionTableMenuStripItem,
            this.removeDimensionTableMenuStripItem,
            this.viewUnmappedToolStripMenuItem});
            this.dimensionTableMenu.Name = "measureTableMenu";
            this.dimensionTableMenu.Size = new System.Drawing.Size(265, 114);
            this.dimensionTableMenu.Opening += new System.ComponentModel.CancelEventHandler(this.dimensionTableMenu_Opening);
            // 
            // newDimensionToolStripMenuItem
            // 
            this.newDimensionToolStripMenuItem.Name = "newDimensionToolStripMenuItem";
            this.newDimensionToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.newDimensionToolStripMenuItem.Text = "New &Dimension";
            this.newDimensionToolStripMenuItem.Click += new System.EventHandler(this.addDimension_Click);
            // 
            // removeDimensionToolStripMenuItem
            // 
            this.removeDimensionToolStripMenuItem.Name = "removeDimensionToolStripMenuItem";
            this.removeDimensionToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.removeDimensionToolStripMenuItem.Text = "Remo&ve Dimension";
            this.removeDimensionToolStripMenuItem.Click += new System.EventHandler(this.removeDimension_Click);
            // 
            // newDimensionTableMenuStripItem
            // 
            this.newDimensionTableMenuStripItem.Name = "newDimensionTableMenuStripItem";
            this.newDimensionTableMenuStripItem.Size = new System.Drawing.Size(264, 22);
            this.newDimensionTableMenuStripItem.Text = "&New Dimension Table Definition";
            this.newDimensionTableMenuStripItem.Click += new System.EventHandler(this.newDimensionTableMenuItem_Click);
            // 
            // removeDimensionTableMenuStripItem
            // 
            this.removeDimensionTableMenuStripItem.Name = "removeDimensionTableMenuStripItem";
            this.removeDimensionTableMenuStripItem.Size = new System.Drawing.Size(264, 22);
            this.removeDimensionTableMenuStripItem.Text = "&Remove Dimension Table Definition";
            this.removeDimensionTableMenuStripItem.Click += new System.EventHandler(this.removeDimensionTableMenuItem_Click);
            // 
            // viewUnmappedToolStripMenuItem
            // 
            this.viewUnmappedToolStripMenuItem.Name = "viewUnmappedToolStripMenuItem";
            this.viewUnmappedToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.viewUnmappedToolStripMenuItem.Text = "&View Unmapped Attributes";
            this.viewUnmappedToolStripMenuItem.Click += new System.EventHandler(this.viewUnmappedAttributesMenuItem_Click);
            // 
            // openRepositoryDialog
            // 
            this.openRepositoryDialog.DefaultExt = "xml";
            this.openRepositoryDialog.FileName = "repository.xml";
            this.openRepositoryDialog.Filter = "Repository Files|*.xml";
            this.openRepositoryDialog.Title = "Load Repository File";
            // 
            // saveRepositoryDialog
            // 
            this.saveRepositoryDialog.DefaultExt = "xml";
            this.saveRepositoryDialog.Filter = "Repository Files|*.xml";
            this.saveRepositoryDialog.Title = "Save Repository";
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn1.HeaderText = "Map";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Width = 65;
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn2.HeaderText = "Map";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            this.dataGridViewButtonColumn2.Width = 65;
            // 
            // dataGridViewButtonColumn3
            // 
            this.dataGridViewButtonColumn3.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn3.HeaderText = "Map";
            this.dataGridViewButtonColumn3.Name = "dataGridViewButtonColumn3";
            this.dataGridViewButtonColumn3.Width = 65;
            // 
            // dataGridViewButtonColumn4
            // 
            this.dataGridViewButtonColumn4.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn4.HeaderText = "Map";
            this.dataGridViewButtonColumn4.Name = "dataGridViewButtonColumn4";
            this.dataGridViewButtonColumn4.Width = 65;
            // 
            // dataGridViewButtonColumn5
            // 
            this.dataGridViewButtonColumn5.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn5.HeaderText = "Map";
            this.dataGridViewButtonColumn5.Name = "dataGridViewButtonColumn5";
            this.dataGridViewButtonColumn5.Width = 65;
            // 
            // dataGridViewButtonColumn6
            // 
            this.dataGridViewButtonColumn6.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn6.HeaderText = "Map";
            this.dataGridViewButtonColumn6.Name = "dataGridViewButtonColumn6";
            this.dataGridViewButtonColumn6.Width = 65;
            // 
            // dataGridViewButtonColumn7
            // 
            this.dataGridViewButtonColumn7.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn7.FillWeight = 60F;
            this.dataGridViewButtonColumn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn7.HeaderText = "Filters";
            this.dataGridViewButtonColumn7.Name = "dataGridViewButtonColumn7";
            this.dataGridViewButtonColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn7.Width = 60;
            // 
            // dataGridViewButtonColumn8
            // 
            this.dataGridViewButtonColumn8.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn8.HeaderText = "Map";
            this.dataGridViewButtonColumn8.Name = "dataGridViewButtonColumn8";
            this.dataGridViewButtonColumn8.Width = 65;
            // 
            // dataGridViewButtonColumn9
            // 
            this.dataGridViewButtonColumn9.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn9.FillWeight = 60F;
            this.dataGridViewButtonColumn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn9.HeaderText = "Filters";
            this.dataGridViewButtonColumn9.Name = "dataGridViewButtonColumn9";
            this.dataGridViewButtonColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn9.Width = 60;
            // 
            // dataGridViewButtonColumn10
            // 
            this.dataGridViewButtonColumn10.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn10.HeaderText = "Map";
            this.dataGridViewButtonColumn10.Name = "dataGridViewButtonColumn10";
            this.dataGridViewButtonColumn10.Width = 65;
            // 
            // dataGridViewButtonColumn11
            // 
            this.dataGridViewButtonColumn11.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn11.FillWeight = 60F;
            this.dataGridViewButtonColumn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn11.HeaderText = "Filters";
            this.dataGridViewButtonColumn11.Name = "dataGridViewButtonColumn11";
            this.dataGridViewButtonColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn11.Width = 60;
            // 
            // dataGridViewButtonColumn12
            // 
            this.dataGridViewButtonColumn12.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn12.HeaderText = "Map";
            this.dataGridViewButtonColumn12.Name = "dataGridViewButtonColumn12";
            this.dataGridViewButtonColumn12.Width = 65;
            // 
            // dataGridViewButtonColumn13
            // 
            this.dataGridViewButtonColumn13.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn13.FillWeight = 60F;
            this.dataGridViewButtonColumn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn13.HeaderText = "Filters";
            this.dataGridViewButtonColumn13.Name = "dataGridViewButtonColumn13";
            this.dataGridViewButtonColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn13.Width = 60;
            // 
            // dataGridViewButtonColumn14
            // 
            this.dataGridViewButtonColumn14.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn14.HeaderText = "Map";
            this.dataGridViewButtonColumn14.Name = "dataGridViewButtonColumn14";
            this.dataGridViewButtonColumn14.Width = 65;
            // 
            // dataGridViewButtonColumn15
            // 
            this.dataGridViewButtonColumn15.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn15.FillWeight = 60F;
            this.dataGridViewButtonColumn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn15.HeaderText = "Filters";
            this.dataGridViewButtonColumn15.Name = "dataGridViewButtonColumn15";
            this.dataGridViewButtonColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn15.Width = 60;
            // 
            // dataGridViewButtonColumn16
            // 
            this.dataGridViewButtonColumn16.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn16.HeaderText = "Map";
            this.dataGridViewButtonColumn16.Name = "dataGridViewButtonColumn16";
            this.dataGridViewButtonColumn16.Width = 65;
            // 
            // dataGridViewButtonColumn17
            // 
            this.dataGridViewButtonColumn17.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn17.FillWeight = 60F;
            this.dataGridViewButtonColumn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn17.HeaderText = "Filters";
            this.dataGridViewButtonColumn17.Name = "dataGridViewButtonColumn17";
            this.dataGridViewButtonColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn17.Width = 60;
            // 
            // dataGridViewButtonColumn18
            // 
            this.dataGridViewButtonColumn18.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn18.HeaderText = "Map";
            this.dataGridViewButtonColumn18.Name = "dataGridViewButtonColumn18";
            this.dataGridViewButtonColumn18.Width = 65;
            // 
            // dataGridViewButtonColumn19
            // 
            this.dataGridViewButtonColumn19.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn19.FillWeight = 60F;
            this.dataGridViewButtonColumn19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn19.HeaderText = "Filters";
            this.dataGridViewButtonColumn19.Name = "dataGridViewButtonColumn19";
            this.dataGridViewButtonColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn19.Width = 60;
            // 
            // dataGridViewButtonColumn20
            // 
            this.dataGridViewButtonColumn20.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn20.HeaderText = "Map";
            this.dataGridViewButtonColumn20.Name = "dataGridViewButtonColumn20";
            this.dataGridViewButtonColumn20.Width = 65;
            // 
            // dataGridViewButtonColumn21
            // 
            this.dataGridViewButtonColumn21.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn21.FillWeight = 60F;
            this.dataGridViewButtonColumn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn21.HeaderText = "Filters";
            this.dataGridViewButtonColumn21.Name = "dataGridViewButtonColumn21";
            this.dataGridViewButtonColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn21.Width = 60;
            // 
            // dataGridViewButtonColumn22
            // 
            this.dataGridViewButtonColumn22.DataPropertyName = "DisplayMap";
            this.dataGridViewButtonColumn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn22.HeaderText = "Map";
            this.dataGridViewButtonColumn22.Name = "dataGridViewButtonColumn22";
            this.dataGridViewButtonColumn22.Width = 65;
            // 
            // dataGridViewButtonColumn23
            // 
            this.dataGridViewButtonColumn23.DataPropertyName = "FilteredMeasures";
            this.dataGridViewButtonColumn23.FillWeight = 60F;
            this.dataGridViewButtonColumn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dataGridViewButtonColumn23.HeaderText = "Filters";
            this.dataGridViewButtonColumn23.Name = "dataGridViewButtonColumn23";
            this.dataGridViewButtonColumn23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewButtonColumn23.Width = 60;
            // 
            // MainAdminForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1081, 733);
            this.Controls.Add(this.mainTree);
            this.Controls.Add(this.mainTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainAdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Birst Administration Console";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainAdminForm_FormClosing);
            this.Load += new System.EventHandler(this.onload);
            this.mainTabControl.ResumeLayout(false);
            this.mainTabPage.ResumeLayout(false);
            this.mainTabPage.PerformLayout();
            this.dimensionsTabPage.ResumeLayout(false);
            this.dimensionsTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dimDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schemaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumns)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureTables)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Joins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnMappings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnMappings)).EndInit();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dimColMappingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dimensionColumnMappingSource)).EndInit();
            this.dimModelBox.ResumeLayout(false);
            this.dimModelBox.PerformLayout();
            this.measuresTabPage.ResumeLayout(false);
            this.measuresTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measureDataGridView)).EndInit();
            this.measureContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureMapGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measureColumnMappingSource)).EndInit();
            this.measureModelBox.ResumeLayout(false);
            this.measureModelBox.PerformLayout();
            this.joinsTabPage.ResumeLayout(false);
            this.joinsTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.joinDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinSource)).EndInit();
            this.measureTableMenu.ResumeLayout(false);
            this.dimensionTableMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        #region IMRUClient implementation
        public void OpenMRUFile(string fName)
        {
            openFile(fName, false);
        }
        #endregion IMRUClient implementation

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new MainAdminForm());
        }

        private void setLogicalMeasureFields(MeasureColumn mc, DataRow dr)
        {
            if (dr["Description"] != System.DBNull.Value && ((string)dr["Description"]).Length > 0)
                mc.Description = (string)dr["Description"];
            if (dr["DataType"] != System.DBNull.Value)
                mc.DataType = (string)dr["DataType"];
            else
                mc.DataType = "Number";
            if (dr["PerformanceCategory"] != System.DBNull.Value && ((string)dr["PerformanceCategory"]).Length > 0)
                mc.PerformanceCategory = (string)dr["PerformanceCategory"];
            mc.Key = (bool)dr["Key"];
            mc.EnforceLimits = (bool)dr["EnforceLimits"];
            mc.Format = (string)dr["Format"];
            mc.AggregationRule = (string)dr["AggregationRule"];
            if (mc.AggregationRule == "SUM")
                mc.AggregationRule = null;
            if (dr["DimensionRules"] != System.DBNull.Value && ((string)dr["DimensionRules"]).Length > 0)
                mc.DimensionRules = (string)dr["DimensionRules"];
            if (dr["DisplayFunction"] != System.DBNull.Value && ((string)dr["DisplayFunction"]) != "None")
                mc.DisplayFunction = (string)dr["DisplayFunction"];
            mc.LogicalAutoGenerated = (bool)dr["AutoGenerated"];
            mc.Improve = (int)dr["Improve"];
            if (dr["Minimum"] != System.DBNull.Value && (string)dr["Minimum"] != "-Infinity")
                mc.Minimum = (string)dr["Minimum"];
            if (dr["Maximum"] != System.DBNull.Value && (string)dr["Maximum"] != "Infinity")
                mc.Maximum = (string)dr["Maximum"];
            if (dr["MinImprovement"] != System.DBNull.Value && (string)dr["MinImprovement"] != "-Infinity")
                mc.MinImprovement = (string)dr["MinImprovement"];
            if (dr["MaxImprovement"] != System.DBNull.Value && (string)dr["MaxImprovement"] != "Infinity")
                mc.MaxImprovement = (string)dr["MaxImprovement"];
            if (dr["MinTarget"] != System.DBNull.Value && (string) dr["MinTarget"] != "-Infinity")
                mc.MinTarget = (string)dr["MinTarget"];
            if (dr["MaxTarget"] != System.DBNull.Value && (string)dr["MaxTarget"] != "Infinity")
                mc.MaxTarget = (string)dr["MaxTarget"];
            if (dr["Width"] != System.DBNull.Value)
                mc.Width = (int)dr["Width"];
            if (dr["ValidMeasure"] != System.DBNull.Value)
                mc.ValidMeasure = (string)dr["ValidMeasure"];
            if (dr["AttributeWeightMeasure"] != System.DBNull.Value)
                mc.AttributeWeightMeasure = (string)dr["AttributeWeightMeasure"];
            if (dr["AnalysisMeasure"] != System.DBNull.Value)
                mc.AnalysisMeasure = (string)dr["AnalysisMeasure"];
            if (dr["SegmentMeasure"] != System.DBNull.Value)
                mc.SegmentMeasure = (string)dr["SegmentMeasure"];
            if (dr["Opportunity"] != System.DBNull.Value && ((string)dr["Opportunity"]).Length > 0)
                mc.Opportunity = (string)dr["Opportunity"];
            if (dr["FilteredMeasures"] != System.DBNull.Value)
                mc.FilteredMeasures = (FilteredMeasureDefinition)dr["FilteredMeasures"];
            if (dr["RootMeasure"] != System.DBNull.Value)
                mc.RootMeasure = (string)dr["RootMeasure"];
            if (!mc.ManuallyEdited)
                mc.ManuallyEdited = (bool)dr["ManuallyEdited"];
        }

        internal Repository getOpenedRepository()
        {
            if (latestSavedRepository == null)
                latestSavedRepository = savedRepository;
            return latestSavedRepository;
        }

        internal void setOpenedRepository(Repository r)
        {
            if (inUberTransaction == false)
                latestSavedRepository = r;
        }

        private Repository getRepository()
        {
            Repository r = new Repository();

            //General Information
            r.SourceCodeControlVersion = sourceCodeVersionLabel.Text;
            r.ApplicationName = appName.Text;
            r.TargetDimension = targetDimension.Text;
            r.OptimizationGoal = goalMeasure.Text;
            r.TimeDefinition = timeDefinition;
            r.BuilderVersion = builderVersion;
            r.CurrentReferenceID = currentReferenceID;
            if (DynamicGroups != null)
                r.DynamicGroups = DynamicGroups;
            if (quickDashboards != null)
                r.QuickDashboards = quickDashboards.ToArray();
            if (customDrills != null)
                r.CustomDrills = customDrills.ToArray();
            if (dependencies != null)
                r.Dependencies = dependencies.ToArray();
            if (logicalExpressionList != null && logicalExpressionList.logicalExpressions != null)
                r.Expressions = logicalExpressionList.logicalExpressions;
            r.RequiresPublish = requiresPublish;
            r.RequiresRebuild = requiresRebuild;
            r.DefaultConnection = defaultConnection;
            int temp = 1; // new query language
            if (Int32.TryParse(QueryLanguageVersionText.Text, out temp))
                r.QueryLanguageVersion = temp;
            else
                r.QueryLanguageVersion = 1; // new query language
            int numFailedAllow = 0; 
            if (Int32.TryParse(NumThresholdFailedRecordsBox.Text, out numFailedAllow))
                r.NumThresholdFailedRecords = numFailedAllow;
            else
                r.NumThresholdFailedRecords = 0;
            r.Hybrid = hybrid;
            r.SkipDBAConnectionMerge = SkipDBAConnectionMerge;
            r.NoBirstApplicationRebuild = DoNotAllowBirstToRebuildApplicationCheckBox.Checked;
            r.RequiresPublish = RequiresPublishBox.Checked;
            r.DisablePublishLock = disablePublishLock;
            r.DisableImpliedGrainsProcessing = disableImpliedGrainsProcessing;
            r.MaxPhysicalTableNameLength = MaxPhysicalTableNameLength;
            r.MaxPhysicalColumnNameLength = MaxPhysicalColumnNameLength;
            r.UseNewETLSchemeForIB = UseNewETLSchemeForIB;
            r.EnableLiveAccessToDefaultConnection = EnableLiveAccessToDefaultConnection;
            r.AllowExternalIntegrations = AllowExternalIntegrations;
            r.ExternalIntegrationTimeout = ExternalIntegrationTimeout;
            r.Imports = Imports;
            r.DimensionalStructure = DimensionalStructure;
            r.IsPartitioned = IsPartitioned;
            r.AllowNonUniqueGrains = AllowNonUniqueGrains;
            r.LiveAccessSecurityFilter = LiveAccessSecurityFilter;
            r.LogCacheKey = LogCacheKey;
            if (WarehousePartitions != null)
                r.WarehousePartitions = WarehousePartitions;
            if (PartitionConnectionLoadInfoList != null)
                r.PartitionConnectionLoadInfoList = PartitionConnectionLoadInfoList;
            int minYear = -1;
            if (Int32.TryParse(minYearText.Text, out minYear))
                r.MinYear = minYear;
            else
                r.MinYear = -1;
            int maxYear = -1;
            if (Int32.TryParse(maxYearText.Text, out maxYear))
                r.MaxYear = maxYear;
            else
                r.MaxYear = -1;
            if (RServer != null)
            {
                r.RServer = new RServer();
                r.RServer.URL = RServer.URL;
                r.RServer.Port = RServer.Port;
                r.RServer.Username = MainAdminForm.encrypt(RServer.Username, MainAdminForm.SecretPassword);
                r.RServer.Password = MainAdminForm.encrypt(RServer.Password, MainAdminForm.SecretPassword);
            }

            foreach (RepositoryModule rm in modules)
            {
                rm.setRepository(r);
            }

            // Measures
            // Add derived measure table
            MeasureTable dmt = null;
            MeasureTable unmappedMeasures = null;
            List<MeasureColumn> mColList = new List<MeasureColumn>();
            if (measureTablesList != null && measureTablesList.Count > 0)
            {
                foreach (MeasureTable mt in measureTablesList)
                {
                    if ((mt.Type == MeasureTable.DERIVED) && (mt.TableName == "Derived Measure Table"))
                    {
                        dmt = mt;
                        break;
                    }
                }
                if (dmt == null)
                {
                    dmt = new MeasureTable();
                    dmt.Type = MeasureTable.DERIVED;
                    dmt.TableName = "Derived Measure Table";
                    dmt.PhysicalName = "DMTABLE";
                    dmt.TableSource = TableSource.getBlank("DMTABLE");
                    measureTablesList.Add(dmt);
                }
                else
                {
                    // to self-correct for the bug
                    dmt.PhysicalName = "DMTABLE";
                    dmt.TableSource = TableSource.getBlank("DMTABLE");
                }
            }
            // Unmapped Measure Columns
            foreach (DataRow dr in measureColumnsTable.Rows)
            {
                DataRowView[] drv = measureColumnMappingsNamesView.FindRows(dr["ColumnName"].ToString());
                if (!((bool)dr["Derived"]) && drv.Length == 0)
                {
                    MeasureColumn mc = new MeasureColumn();
                    mc.ColumnName = (string)dr["ColumnName"];
                    setLogicalMeasureFields(mc, dr);
                    mColList.Add(mc);
                }
            }
            foreach (MeasureTable mt in measureTablesList)
            {
                if (mt.Type == MeasureTable.UNMAPPED)
                {
                    unmappedMeasures = mt;
                    break;
                }
            }
            if (unmappedMeasures == null)
            {
                unmappedMeasures = new MeasureTable();
                unmappedMeasures.TableName = "UNMAPPED";
                unmappedMeasures.Type = MeasureTable.UNMAPPED;
                unmappedMeasures.TableSource = TableSource.getBlank("UNMAPPED");
                unmappedMeasures.MeasureColumns = mColList.ToArray();
                measureTablesList.Add(unmappedMeasures);
            }
            else
            {
                unmappedMeasures.MeasureColumns = mColList.ToArray();
            }
            r.MeasureTables = measureTablesList.ToArray();
            DataRow[] srows = null;
            Dictionary<string, string> mtables = new Dictionary<string, string>();
            List<MeasureTable> mtlist = new List<MeasureTable>();
            for (int count = 0; count < r.MeasureTables.Length; count++)
            {
                string tname = r.MeasureTables[count].TableName;
                if (mtables.ContainsKey(tname))
                    continue;
                mtables.Add(tname, null);
                mtlist.Add(r.MeasureTables[count]);
            }
            r.MeasureTables = mtlist.ToArray();
            for (int count = 0; count < r.MeasureTables.Length; count++)
            {
                string tname = r.MeasureTables[count].TableName;
                if (tname != "UNMAPPED" && tname != "DMTABLE")
                {
                    srows = measureColumnMappings.Select("TableName='" + tname + "'");
                    Dictionary<string, string> mcols = new Dictionary<string, string>();
                    List<DataRow> rows = new List<DataRow>();
                    foreach (DataRow dr in srows)
                    {
                        string cname = (string)dr["ColumnName"];
                        if (mcols.ContainsKey(cname))
                            continue;
                        mcols.Add(cname, null);
                        rows.Add(dr);
                    }
                    r.MeasureTables[count].MeasureColumns = new MeasureColumn[rows.Count];
                    for (int ccount = 0; ccount < rows.Count; ccount++)
                    {
                        r.MeasureTables[count].MeasureColumns[ccount] = new MeasureColumn();
                        r.MeasureTables[count].MeasureColumns[ccount].ColumnName = (string)rows[ccount]["ColumnName"];
                        r.MeasureTables[count].MeasureColumns[ccount].TableName = (string)rows[ccount]["TableName"];
                        if (rows[ccount]["PhysicalName"] != System.DBNull.Value)
                            r.MeasureTables[count].MeasureColumns[ccount].PhysicalName = (string)rows[ccount]["PhysicalName"];
                        r.MeasureTables[count].MeasureColumns[ccount].Qualify = (bool)rows[ccount]["Qualify"];
                        r.MeasureTables[count].MeasureColumns[ccount].AutoGenerated = (bool)rows[ccount]["AutoGenerated"];
                        r.MeasureTables[count].MeasureColumns[ccount].ManuallyEdited = (bool)rows[ccount]["ManuallyEdited"];
                        r.MeasureTables[count].MeasureColumns[ccount].NotCacheable = (bool)rows[ccount]["NotCacheable"];
                        if (rows[ccount]["PhysicalFilter"] != System.DBNull.Value)
                        {
                            string s = (string)rows[ccount]["PhysicalFilter"];
                            if (s != null && s.Length > 0)
                                r.MeasureTables[count].MeasureColumns[ccount].PhysicalFilter = s;
                            else
                                r.MeasureTables[count].MeasureColumns[ccount].PhysicalFilter = null;
                        }
                        else
                            r.MeasureTables[count].MeasureColumns[ccount].PhysicalFilter = null;
                        // Get logical info
                        DataRowView[] lrv = measureColumnNamesView.FindRows(r.MeasureTables[count].MeasureColumns[ccount].ColumnName);
                        foreach (DataRowView drv in lrv)
                        {
                            if (!((bool)drv["Derived"]))
                            {
                                setLogicalMeasureFields(r.MeasureTables[count].MeasureColumns[ccount], drv.Row);
                                break;
                            }
                        }
                        if (rows[ccount]["AggRuleOverride"] != System.DBNull.Value)
                        {
                            r.MeasureTables[count].MeasureColumns[ccount].AggregationRule = (string)rows[ccount]["AggRuleOverride"];
                        }
                    }
                }
            }

            // Derived Measures
            if (dmt != null)
            {
                srows = measureColumnsTable.Select("Derived=True");
                dmt.MeasureColumns = new MeasureColumn[srows.Length];
                for (int ccount = 0; ccount < srows.Length; ccount++)
                {
                    dmt.MeasureColumns[ccount] = new MeasureColumn();
                    dmt.MeasureColumns[ccount].ColumnName = (string)srows[ccount]["ColumnName"];
                    dmt.MeasureColumns[ccount].TableName = dmt.TableName;
                    if (srows[ccount]["PhysicalName"] != System.DBNull.Value)
                        dmt.MeasureColumns[ccount].PhysicalName = (string)srows[ccount]["PhysicalName"];
                    setLogicalMeasureFields(dmt.MeasureColumns[ccount], srows[ccount]);
                }
            }

            //Dimensions
            // Unmapped Dimension Columns
            Dictionary<string, List<DimensionColumn>> unmappedColumns = new Dictionary<string, List<DimensionColumn>>();
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                List<DataRow> result = Util.selectRows(dimensionColumnMappings, new string[][] { new string[] { "ColumnName", dr["ColumnName"].ToString() } });
                if (result.Count == 0)
                {
                    List<DimensionColumn> colList = null;
                    if (!unmappedColumns.ContainsKey((string)dr["DimensionName"]))
                    {
                        colList = new List<DimensionColumn>();
                        unmappedColumns.Add((string)dr["DimensionName"], colList);
                    }
                    colList = unmappedColumns[(string)dr["DimensionName"]];
                    DimensionColumn dc = new DimensionColumn();
                    dc.ColumnName = (string)dr["ColumnName"];
                    if (dr["Description"] != System.DBNull.Value)
                        dc.Description = (string)dr["Description"];
                    if (dr["VolatileKey"] != System.DBNull.Value)
                        dc.VolatileKey = (string)dr["VolatileKey"];
                    dc.DataType = (string)dr["DataType"];
                    dc.Width = (string)dr["ColWidth"];
                    dc.Key = (bool)dr["Key"];
                    dc.Format = (string)dr["Format"];
                    if (dr["MeasureExpression"] != System.DBNull.Value && ((string)dr["MeasureExpression"]).Length > 0)
                        dc.MeasureExpression = (string)dr["MeasureExpression"];
                    if (dr["GenericName"] != System.DBNull.Value && ((string)dr["GenericName"]).Length > 0)
                        dc.GenericName = (string)dr["GenericName"];
                    if (dr["DesiredValues"] != System.DBNull.Value && ((string)dr["DesiredValues"]).Length > 0)
                        dc.DesiredValues = (string)dr["DesiredValues"];
                    if (dr["IgnoredValues"] != System.DBNull.Value && ((string)dr["IgnoredValues"]).Length > 0)
                        dc.IgnoredValues = (string)dr["IgnoredValues"];
                    dc.Score = (double)dr["Score"];
                    dc.LogicalAutoGenerated = (bool)dr["AutoGenerated"];
                    dc.ManuallyEdited = (bool)dr["ManuallyEdited"];
                    if (dr["BlockingGroup"] != System.DBNull.Value)
                        dc.BlockingGroup = (string)dr["BlockingGroup"];
                    dc.Maskable = (string)dr["Maskable"];
                    if (dr["DisplayMap"] != System.DBNull.Value)
                        dc.DisplayMap = (DisplayMap)dr["DisplayMap"];
                    dc.Index = (bool)dr["Index"];
                    dc.Constant = (bool)dr["Constant"];
                    colList.Add(dc);
                }
            }
            r.Dimensions = dimensionsList.ToArray();
            r.DimensionTables = new DimensionTable[dimensionTablesList.Count + unmappedColumns.Count];
            for (int i = 0; i < dimensionTablesList.Count; i++)
                r.DimensionTables[i] = dimensionTablesList[i];
            for (int count = 0; count < dimensionTablesList.Count; count++)
            {
                DataRow[] childRows = dimensionColumnMappings.Select("TableName='" + r.DimensionTables[count].TableName + "'");
                r.DimensionTables[count].DimensionColumns = new DimensionColumn[childRows.Length];
                DataRow[] logicalRows = dimensionColumns.Select("DimensionName='" + r.DimensionTables[count].DimensionName + "'");
                for (int ccount = 0; ccount < childRows.Length; ccount++)
                {
                    r.DimensionTables[count].DimensionColumns[ccount] = new DimensionColumn();
                    r.DimensionTables[count].DimensionColumns[ccount].TableName = (string)childRows[ccount]["TableName"];
                    r.DimensionTables[count].DimensionColumns[ccount].ColumnName = (string)childRows[ccount]["ColumnName"];
                    r.DimensionTables[count].DimensionColumns[ccount].PhysicalName = (string)childRows[ccount]["PhysicalName"];
                    r.DimensionTables[count].DimensionColumns[ccount].Qualify = (bool)childRows[ccount]["Qualify"];
                    r.DimensionTables[count].DimensionColumns[ccount].AutoGenerated = (bool)childRows[ccount]["AutoGenerated"];
                    r.DimensionTables[count].DimensionColumns[ccount].NotCacheable = (bool)childRows[ccount]["NotCacheable"];
                    r.DimensionTables[count].DimensionColumns[ccount].ManuallyEdited = (bool)childRows[ccount]["ManuallyEdited"];
                    r.DimensionTables[count].DimensionColumns[ccount].CacheBoundary = (string)childRows[ccount]["CacheBoundary"];
                    // Get logical column information
                    foreach (DataRow dr in logicalRows)
                    {
                        if ((string)dr["ColumnName"] == r.DimensionTables[count].DimensionColumns[ccount].ColumnName)
                        {
                            if (dr["Description"] != System.DBNull.Value)
                                r.DimensionTables[count].DimensionColumns[ccount].Description = (string)dr["Description"];
                            if (dr["VolatileKey"] != System.DBNull.Value)
                                r.DimensionTables[count].DimensionColumns[ccount].VolatileKey = (string)dr["VolatileKey"];
                            r.DimensionTables[count].DimensionColumns[ccount].DataType = (string)dr["DataType"];
                            if (dr["ColWidth"] != System.DBNull.Value)
                            {
                                r.DimensionTables[count].DimensionColumns[ccount].Width = (string)dr["ColWidth"];
                            }
                            r.DimensionTables[count].DimensionColumns[ccount].Key = (bool)dr["Key"];
                            if (dr["Format"] != System.DBNull.Value && ((string)dr["Format"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].Format = (string)dr["Format"];
                            if (dr["MeasureExpression"] != System.DBNull.Value && ((string)dr["MeasureExpression"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].MeasureExpression = (string)dr["MeasureExpression"];
                            if (dr["GenericName"] != System.DBNull.Value && ((string)dr["GenericName"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].GenericName = (string)dr["GenericName"];
                            if (dr["DesiredValues"] != System.DBNull.Value && ((string)dr["DesiredValues"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].DesiredValues = (string)dr["DesiredValues"];
                            if (dr["IgnoredValues"] != System.DBNull.Value && ((string)dr["IgnoredValues"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].IgnoredValues = (string)dr["IgnoredValues"];
                            r.DimensionTables[count].DimensionColumns[ccount].Score = (double)dr["Score"];
                            if (dr["BlockingGroup"] != System.DBNull.Value)
                                r.DimensionTables[count].DimensionColumns[ccount].BlockingGroup = (string)dr["BlockingGroup"];
                            if (dr["Maskable"] != System.DBNull.Value && ((string)dr["Maskable"]).Length > 0)
                                r.DimensionTables[count].DimensionColumns[ccount].Maskable = (string)dr["Maskable"];
                            if (dr["DisplayMap"] != System.DBNull.Value)
                                r.DimensionTables[count].DimensionColumns[ccount].DisplayMap = (DisplayMap)dr["DisplayMap"];
                            if (dr["UnknownValue"] != System.DBNull.Value)
                                r.DimensionTables[count].DimensionColumns[ccount].UnknownValue = (string)dr["UnknownValue"];
                            r.DimensionTables[count].DimensionColumns[ccount].LogicalAutoGenerated = (bool)dr["AutoGenerated"];
                            r.DimensionTables[count].DimensionColumns[ccount].Index = (bool)dr["Index"];
                            r.DimensionTables[count].DimensionColumns[ccount].Constant = (bool)dr["Constant"];
                            if (!r.DimensionTables[count].DimensionColumns[ccount].ManuallyEdited)
                            {
                                r.DimensionTables[count].DimensionColumns[ccount].ManuallyEdited = (bool)dr["ManuallyEdited"];
                            }
                            break;
                        }
                    }
                }
            }
            // Create unmapped table definitons
            int dcount = 0;
            foreach (string dimname in unmappedColumns.Keys)
            {
                DimensionTable dt = new DimensionTable();
                dt.DimensionName = dimname;
                dt.DimensionColumns = unmappedColumns[dimname].ToArray();
                dt.TableSource = TableSource.getBlank("UNMAPPED");
                r.DimensionTables[dimensionTablesList.Count + (dcount++)] = dt;
            }

            //Joins
            r.Joins = new Join[Joins.Rows.Count];
            for (int i = 0; i < Joins.Rows.Count; i++)
            {
                Join j = new Join();
                DataRow dr = Joins.Rows[i];
                if (dr["Table1"] != System.DBNull.Value)
                    j.Table1 = (string)dr["Table1"];
                if (dr["Table2"] != System.DBNull.Value)
                    j.Table2 = (string)dr["Table2"];
                if (dr["JoinCondition"] != System.DBNull.Value)
                    j.JoinCondition = (string)dr["JoinCondition"];
                j.Redundant = (bool)dr["Redundant"];
                if (dr["JoinType"] != System.DBNull.Value && (string)dr["JoinType"] != "Inner")
                    j.JoinType = (string)dr["JoinType"];
                if (dr["Level"] != System.DBNull.Value && ((string)dr["Level"]).Length > 0)
                    j.Level = (string)dr["Level"];
                j.AutoGenerated = (bool)dr["AutoGenerated"];
                j.Invalid = (bool)dr["Invalid"];
                j.Federated = (bool)dr["Federated"];
                if (dr["FederatedJoinKeys"] != System.DBNull.Value && ((string)dr["FederatedJoinKeys"]).Length > 0)
                    j.FederatedJoinKeys = ((string)dr["FederatedJoinKeys"]).Split(',');
                r.Joins[i] = j;
            }
            List<MeasureGrain> unmappedMeasureGrainList = getUnmappedMeasureGrainList();
            r.UnmappedMeasureGrains = new MeasureGrain[unmappedMeasureGrainList.Count];
            for (int i = 0; i < unmappedMeasureGrainList.Count; i++)
            {
                r.UnmappedMeasureGrains[i] = unmappedMeasureGrainList[i];
            }
            return (r);
        }

        private List<MeasureGrain> getUnmappedMeasureGrainList()
        {
            List<MeasureGrain> retList = new List<MeasureGrain>();

            Dictionary<string, List<MeasureGrain>>.ValueCollection masterlist = unmappedMeasureGrainsByColumnName.Values;
            foreach (List<MeasureGrain> list in masterlist)
            {
                retList.AddRange(list);
            }
            return retList;
        }

        // used in Acorn and save on the admin tool menu
        public void saveRepository(string currentFilename)
        {
            implicitSaveModules();
            Repository r = getRepository();
            saveRepositoryFile(currentFilename, r, typeof(Repository));
            savedRepository = (Repository) r.Clone();
            fileHasBeenOpened();
        }

        static ulong repositoryCounter = 0;  // add some uniqueness to the temp file names, for safety

        private void saveRepositoryFile(string currentFilename, Object o, Type type)
        {
            ulong value = ++repositoryCounter; // should be atomic
            TextWriter writer = null;
            string newVersion = currentFilename + ".new" + value;
            try
            {
                writer = new StreamWriter(newVersion, true, Encoding.UTF8);
                XmlSerializer serializer = new XmlSerializer(type);
                serializer.Serialize(writer, o);
            }
            catch (Exception ex)
            {
                Logger.Warn("Exception trying to serialize the repository file: " + newVersion + ex.Message);
                throw new Exception("Error trying to save the repository, the previous version still exists");
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                    writer.Dispose();
                }
            }

            // save the old version, move the new version to the proper location, delete the old version
            FileInfo currentFileI = new FileInfo(currentFilename);
            string oldVersion = currentFilename + ".old" + value;
            if (currentFileI.Exists)
            {
                Util.SafeMove(currentFilename, oldVersion); // save away the old version
            }
            Util.SafeMove(newVersion, currentFilename);     // move new to current
            File.Delete(oldVersion);                        // delete old

            // write out a binary version, performance optimization
            Stream stream = null;
            string bname = currentFilename + Util.getBinaryExtension();
            try
            {
                IFormatter formatter = new BinaryFormatter();
                stream = new FileStream(bname, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, o);
            }
            catch (Exception ex)
            {
                Logger.Debug("Exception trying to serialize the binary repository file (not fatal): " + bname +", " + ex.Message);
                try
                {
                    File.Delete(currentFilename + Util.getBinaryExtension()); // remove the old one in case it is corrupt
                }
                catch (Exception ex2)
                {
                    Logger.Debug("Exception trying to remove an old binary repository file (not fatal): " + bname + ", " + ex2.Message);
                }
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public LogicalExpression[] getLogicalExpressions()
        {
            if (logicalExpressionList == null)
                return null;

            logicalExpressionList.updateRepositoryDirectory(repositoryDirectory, this);
            logicalExpressionList.refreshIfNecessary(this);
            return logicalExpressionList.logicalExpressions;
        }

        public void updateLogicalExpression(LogicalExpression le, string username, string label)
        {
            if (logicalExpressionList == null)
                logicalExpressionList = new LogicalExpressionList(getLogger());

            logicalExpressionList.updateRepositoryDirectory(repositoryDirectory, this);
            logicalExpressionList.updateItem(le, username, label, this);
        }

        public void deleteLogicalExpression(LogicalExpression le)
        {
            logicalExpressionList.updateRepositoryDirectory(repositoryDirectory, this);
            logicalExpressionList.deleteItem(le, this);
        }

        private void saveMenuItem_Click(object sender, System.EventArgs e)
        {
            if (currentFilename == null || currentFilename.Length < 1)
            {
                hybrid = true; // the box has been cracked
                saveAsMenuItem_Click(sender, e);
            }
            else
            {
                hybrid = true; // the box has been cracked
                saveRepository(currentFilename);
            }
        }

        public void setupBlankRepository()
        {
            initializeBlank();
            curMeasureList = null;
            foreach (RepositoryModule rm in modules)
            {
                rm.updateFromRepository(null);
            }
            foreach (RepositoryModule rm in modules)
            {
                rm.updateFromRepositoryFinal(null);
            }
            // Load filters into form
            foreach (RepositoryModule rm in modules)
            {
                rm.setFilters(null, filterMod.filterList);
            }
        }

        public void initializeBlank()
        {
            appName.Text = "";
            targetDimension.Text = "";
            goalMeasure.Text = "";
            timeDefinition = null;
            dimensionColumns.Rows.Clear();
            dimensionColumnMappings.Rows.Clear();
            dimensionsList = new List<Dimension>(0);
            dimensionTablesList = new List<DimensionTable>(0);
            unmappedDimensionTablesList = new List<DimensionTable>(0);
            measureColumnsTable.Rows.Clear();
            measureColumnMappings.Rows.Clear();
            measureTablesList = new List<MeasureTable>(0);
            unmappedMeasureGrainsByColumnName = new Dictionary<string, List<MeasureGrain>>();
            curMeasureList = null;
            builderVersion = Repository.CurrentBuilderVersion;
        }

        public void setAllowDimensionalCrossJoins(bool allowDimensionalCrossJoins)
        {
            Repository r = getRepository();
            if (allowDimensionalCrossJoins)
                r.ServerParameters.AllowDimensionalCrossjoins = true;
            else
                r.ServerParameters.AllowDimensionalCrossjoins = false;
            foreach (RepositoryModule rm in modules)
                if (rm.ModuleName.Equals("Server Properties"))
                    rm.updateFromRepository(r);
        }

        private bool nowarn;

        public bool NoWarn
        {
            get
            {
                return nowarn;
            }
            set
            {
                nowarn = value;
            }
        }

        public void updateVariable(Variable var, string username)
        {
            variableMod.updateRepositoryDirectory(repositoryDirectory);
            variableMod.updateVariable(var, username);
        }
		public void updateCurrentVariable(Variable var)
		{
			List<Variable> varList = variableMod.getCurrentVariables();
			for (int i = 0; i < varList.Count; i++) 
			{
				Variable v = varList[i];
				if (v.Name == var.Name)
				{
					varList[i] = var;
					return;
				}
			}
			varList.Add(var);
		}

        public void deleteVariable(Variable var)
        {
            variableMod.updateRepositoryDirectory(repositoryDirectory);
            variableMod.deleteVariable(var);
        }
        public List<Variable> getVariables()
        {
            variableMod.updateRepositoryDirectory(repositoryDirectory);
            return variableMod.getVariables();
        }

		public List<Variable> getCurrentVariables()
		{
			return variableMod.getCurrentVariables();
		}

        public void setRepositoryDirectory(string s)
        {
            if (repositoryDirectory != s)
            {
                repositoryDirectory = s;
                foreach (RepositoryModule rm in modules)
                {
                    rm.setRepositoryDirectory(s);
                }
            }
        }

        public string getRepositoryDirectory()
        {
            return repositoryDirectory;
        }

        public void loadRepositoryFromFile(string fileName)
        {
            Boolean loaded = false;
            FileInfo fi = new FileInfo(fileName);
            FileInfo fibin = new FileInfo(fileName + Util.getBinaryExtension());
            if (fibin.Exists && fibin.LastWriteTime > fi.LastWriteTime)
            {
                Stream stream = null;
                try
                {
                    IFormatter formatter = new BinaryFormatter();
                    stream = new FileStream(fibin.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    Repository r = (Repository)formatter.Deserialize(stream);
                    ApplicationBuilder.saveAndSuspendHooks(this);
                    initializeMAFFromRepository(r);
                    if (savedRepository != null)
                    	savedRepository.fileModifiedTime = fi.LastWriteTime;
                    loaded = true;
                }
                catch (Exception ex)
                {
                    if (Logger != null)
                        Logger.Debug("Exception trying to deserialize the binary repository file (not fatal): " + currentFilename + Util.getBinaryExtension() + " - " + ex.Message);
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }
            }

            // if could not load the binary version, load the XML version
            if (!loaded)
            {
                Repository r = loadRepositoryFromXMLFile(fileName);
                ApplicationBuilder.saveAndSuspendHooks(this);
                initializeMAFFromRepository(r);
            }

            Boolean needToSave = false;

            // check VirtualColumn, Hierarchy, Aggregate, Variable, SourceFile, StagingTable, LogicalExpression for GUIDs
            // older repositories don't have GUIDs which causes updates to sometimes fail
            // this fix will update all the GUIDs so that the update won't fail.
            needToSave = this.vcolumns.updateGuids() || needToSave;
            needToSave = this.aggModule.updateGuids() || needToSave;
            needToSave = this.variableMod.updateGuids() || needToSave;
            needToSave = this.sourceFileMod.updateGuids() || needToSave;
            needToSave = this.stagingTableMod.updateGuids() || needToSave;
            if (this.logicalExpressionList != null)
                needToSave = this.logicalExpressionList.updateGuids() || needToSave;

            needToSave = this.hmodule.updateGuids() || needToSave;

            if (needToSave)
            {
                saveRepository(fileName);
                fi = new FileInfo(fileName);
            }
            if (savedRepository != null)
            	savedRepository.fileModifiedTime = fi.LastWriteTime;
            ApplicationBuilder.restoreHooks(this);

        }

        /**
         * load a repository from an XML file
         */
        private Repository loadRepositoryFromXMLFile(string fileName)
        {
            StreamReader sreader = null;
            XmlReader xreader = null;
            XmlSerializer serializer = new XmlSerializer(typeof(Repository));
            Repository r = null;
            try
            {
                sreader = new StreamReader(fileName, Encoding.UTF8);
                xreader = Util.getSecureXMLReader(sreader);
                r = (Repository) serializer.Deserialize(xreader);
            }
            catch (Exception ex)
            {
                reportMessage(null, null, ex.ToString(), DialogResult.OK, "Error trying to read the repository", MessageBoxButtons.OK, MessageBoxIcon.None);
                throw new Exception("Error trying to read the repository");
            }
            finally
            {
                if (xreader != null)
                {
                    xreader.Close();
                }
                if (sreader != null)
                {
                    sreader.Close();
                    sreader.Dispose();
                }
            }

            if (r != null)
            {
                savedRepository = (Repository) r.Clone(); // save away the original (for determining if there are changes)
            }
            return r;
        }

        private void updateDeprecatedForeignKeys(Repository r)
        {
            foreach (StagingTable st in r.StagingTables)
            {
                if (st.ForeignKeySources != null)
                {
                    st.ForeignKeys = new ForeignKey[st.ForeignKeySources.Length];
                    for (int i = 0; i < st.ForeignKeySources.Length; i++)
                    {
                        st.ForeignKeys[i] = new ForeignKey();
                        st.ForeignKeys[i].Source = st.ForeignKeySources[i];
                    }
                    st.ForeignKeySources = null;
                }
                if (st.FactForeignKeySources != null)
                {
                    st.FactForeignKeys = new ForeignKey[st.FactForeignKeySources.Length];
                    for (int i = 0; i < st.FactForeignKeySources.Length; i++)
                    {
                        st.FactForeignKeys[i] = new ForeignKey();
                        st.FactForeignKeys[i].Source = st.FactForeignKeySources[i];
                    }
                    st.FactForeignKeySources = null;
                }
            }
            foreach (Hierarchy h in r.Hierarchies)
            {
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    if (l.ForeignKeySources != null)
                    {
                        l.ForeignKeys = new ForeignKey[l.ForeignKeySources.Length];
                        for (int i = 0; i < l.ForeignKeySources.Length; i++)
                        {
                            l.ForeignKeys[i] = new ForeignKey();
                            l.ForeignKeys[i].Source = l.ForeignKeySources[i];
                        }
                        l.ForeignKeySources = null;
                    }
                }
            }
        }

        private void initializeMAFFromRepository(Repository r)
        {
            if (r.IsDynamic) // read only dynamic repository, disable save/save as repository menu items.
            {
                if (!nowarn)
                {
                    reportMessage(null, null, "Warning: This is read only dynamic repository. Birst will open this repository in read only mode.", DialogResult.OK,
                            "", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
                mainSaveMenuItem.Enabled = false;
                mainSaveAsMenuItem.Enabled = false;
            }
            if (r.Version < Repository.RepositoryReadVersion)
            {
                reportMessage(null, null, "Can not read this repository; the repository it is too old for this version of Birst.\n" +
                    "Repository Version = " + r.Version + ", Birst Read Version = " + Repository.RepositoryReadVersion, DialogResult.OK,
                    "", MessageBoxButtons.OK, MessageBoxIcon.None);
                throw new Exception("Can not read this repository; the repository it is too old for this version of Birst.\n" +
                    "Repository Version = " + r.Version + ", Birst Read Version = " + Repository.RepositoryReadVersion);
            }
            if (r.Version > Repository.RepositoryWriteVersion)
            {
                reportMessage(null, null, "Can not read this repository; the Birst version is too old.\n" +
                    "Repository Version = " + r.Version + ", Birst Write Version = " + Repository.RepositoryWriteVersion, DialogResult.OK,
                    "", MessageBoxButtons.OK, MessageBoxIcon.None);
                throw new Exception("Can not read this repository; the Birst version is too old.\n" +
                    "Repository Version = " + r.Version + ", Birst Tool Write Version = " + Repository.RepositoryWriteVersion);
            }
            if (r.Version < Repository.RepositoryWriteVersion)
            {
                if (!nowarn)
                {
                    reportMessage(null, null, "Warning: this repository is at version " + r.Version + ", Birst will save this as version " + Repository.RepositoryWriteVersion + ".",
                        DialogResult.OK, "", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
            }

            appName.Text = r.ApplicationName;
            targetDimension.Text = r.TargetDimension;
            builderVersion = r.BuilderVersion;
            NumThresholdFailedRecordsBox.Text = r.NumThresholdFailedRecords.ToString();
            currentReferenceID = r.CurrentReferenceID;
            defaultConnection = r.DefaultConnection;
            if (r.SourceCodeControlVersion == null)
            {
                r.SourceCodeControlVersion = "$" + "Id" + "$";  // so this does not expand to the CVS tag for this file
            }
            sourceCodeVersionLabel.Text = r.SourceCodeControlVersion;
            RepositoryVersion.Text = r.Version.ToString() + " (read: " + Repository.RepositoryReadVersion + ", write: " + Repository.RepositoryWriteVersion + ")";
            AppBuilderVersion.Text = r.BuilderVersion.ToString() + " (current: " + Repository.CurrentBuilderVersion + ")";
            NumThresholdFailedRecordsBox.Text = r.NumThresholdFailedRecords.ToString();
            lastEditLabel.Text = r.LastEdit + " (now: " + DateTime.Now.ToUniversalTime().ToString("u") + ")";
            goalMeasure.Text = r.OptimizationGoal;
            if (r.Dependencies != null)
                dependencies = new List<HierarchyLevel[]>(r.Dependencies);
            if (r.TimeDefinition != null)
                timeDefinition = r.TimeDefinition;
            if (r.QuickDashboards != null)
                quickDashboards = new List<QuickDashboard>(r.QuickDashboards);
            if (r.CustomDrills != null)
                customDrills = new List<CustomDrill>(r.CustomDrills);
            if (r.Expressions != null)
            {
                logicalExpressionList = new LogicalExpressionList(getLogger());
                logicalExpressionList.logicalExpressions = r.Expressions;
            }
            if (r.DynamicGroups != null)
                DynamicGroups = r.DynamicGroups;
            requiresPublish = r.RequiresPublish;
            requiresRebuild = r.RequiresRebuild;
            QueryLanguageVersionText.Text = r.QueryLanguageVersion.ToString();
            SkipDBAConnectionMerge = r.SkipDBAConnectionMerge;
            hybrid = r.Hybrid;
            DoNotAllowBirstToRebuildApplicationCheckBox.Checked = r.NoBirstApplicationRebuild;
            RequiresPublishBox.Checked = r.RequiresPublish;
            disablePublishLock = r.DisablePublishLock;
            disableImpliedGrainsProcessing = r.DisableImpliedGrainsProcessing;
            if (r.MaxPhysicalColumnNameLength != 0)
                MaxPhysicalColumnNameLength = r.MaxPhysicalColumnNameLength;
            if (r.MaxPhysicalTableNameLength != 0)
                MaxPhysicalTableNameLength = r.MaxPhysicalTableNameLength;
            UseNewETLSchemeForIB = r.UseNewETLSchemeForIB;
            minYearText.Text = r.MinYear.ToString();
            maxYearText.Text = r.MaxYear.ToString();
            EnableLiveAccessToDefaultConnection = r.EnableLiveAccessToDefaultConnection;
            AllowExternalIntegrations = r.AllowExternalIntegrations;
            ExternalIntegrationTimeout = r.ExternalIntegrationTimeout;
            Imports = r.Imports;
            DimensionalStructure = r.DimensionalStructure;
            IsPartitioned = r.IsPartitioned;
            AllowNonUniqueGrains = r.AllowNonUniqueGrains;
            LogCacheKey = r.LogCacheKey;
            WarehousePartitions = r.WarehousePartitions;
            PartitionConnectionLoadInfoList = r.PartitionConnectionLoadInfoList;
            if (PartitionConnectionLoadInfoList != null && PartitionConnectionLoadInfoList.Length > 0)
            {
                PartitionConnectionLoadInfoMap = new Dictionary<string, PartitionConnectionLoadInfo>();
                foreach (PartitionConnectionLoadInfo pcli in PartitionConnectionLoadInfoList)
                {
                    if (pcli.PartitionConnectionName != null && PartitionConnectionLoadInfoMap.ContainsKey(pcli.PartitionConnectionName))
                    {
                        PartitionConnectionLoadInfoMap.Add(pcli.PartitionConnectionName, pcli);
                    }
                }
            }
            LiveAccessSecurityFilter = r.LiveAccessSecurityFilter;
            if (r.RServer != null)
            {
                RServer = new RServer();
                RServer.URL = r.RServer.URL;
                RServer.Port = r.RServer.Port;
                RServer.Username = MainAdminForm.decrypt(r.RServer.Username, MainAdminForm.SecretPassword);
                RServer.Password = MainAdminForm.decrypt(r.RServer.Password, MainAdminForm.SecretPassword);
            }
            // Update to proper foreign keys
            updateDeprecatedForeignKeys(r);

            curMeasureList = null;
            foreach (RepositoryModule rm in modules)
            {
                rm.updateFromRepository(r);
            }

            mainTreeNode = null;
            lastNode = null;
            lastPNode = null;

            // measures
            measureColumnsTable.Rows.Clear();
            measureColumnMappings.Rows.Clear();
            measureTablesList = new List<MeasureTable>();
            unmappedMeasureGrainsByColumnName = new Dictionary<string, List<MeasureGrain>>();
            foreach (MeasureTable mt in r.MeasureTables)
            {
                if (mt.Type == MeasureTable.OPAQUE_VIEW && mt.OpaqueView != null)
                    measureTablesList.Add(mt);
                else if (mt.TableSource.Tables.Length > 0)
                {
                    if (mt.TableSource.Tables[0].PhysicalName == "UNMAPPED") mt.Type = MeasureTable.UNMAPPED;
                    if (mt.Type != MeasureTable.DERIVED && mt.Type != MeasureTable.UNMAPPED)
                        if (mt.TableName != null) measureTablesList.Add(mt);
                }
                else if (mt.Type == MeasureTable.NORMAL)
                {
                    measureTablesList.Add(mt);
                }
            }

            foreach (MeasureTable mt in r.MeasureTables)
            {
                if (mt.Type == MeasureTable.DERIVED || mt.Type == MeasureTable.UNMAPPED)
                    measureTablesList.Remove(mt);
                if (mt.MeasureColumns != null)
                {
                    foreach (MeasureColumn mc in mt.MeasureColumns)
                    {
                        DataRowView[] drv = measureColumnNamesView.FindRows(mc.ColumnName);
                        if (drv.Length == 0)
                        {
                            DataRow dr = measureColumnsTable.NewRow();
                            dr["ColumnName"] = mc.ColumnName;
                            dr["Description"] = mc.Description == null ? "" : mc.Description;
                            dr["DataType"] = (mc.DataType == null || mc.DataType.Length == 0) ? "Integer" : mc.DataType;
                            if (mc.PerformanceCategory == null)
                                dr["PerformanceCategory"] = "";
                            else
                                dr["PerformanceCategory"] = mc.PerformanceCategory;
                            dr["Width"] = mc.Width;
                            dr["Key"] = mc.Key;
                            dr["EnforceLimits"] = mc.EnforceLimits;
                            dr["Format"] = mc.Format == null ? (mc.Key ? "" : "#,###,###.##") : mc.Format;
                            dr["AggregationRule"] = (mc.AggregationRule == null) ? "SUM" : mc.AggregationRule;
                            if (mc.DimensionRules == null)
                                dr["DimensionRules"] = "";
                            else
                                dr["DimensionRules"] = mc.DimensionRules;
                            dr["DisplayFunction"] = (mc.DisplayFunction == null) ? "None" : mc.DisplayFunction;
                            dr["Improve"] = mc.Improve;
                            dr["Maximum"] = mc.Maximum == null ? "Infinity" : mc.Maximum;
                            dr["Minimum"] = mc.Minimum == null ? "-Infinity" : mc.Minimum;
                            dr["MaxImprovement"] = mc.MaxImprovement == null ? "Infinity" : mc.MaxImprovement;
                            dr["MinImprovement"] = mc.MinImprovement == null ? "-Infinity" : mc.MinImprovement;
                            dr["MaxTarget"] = mc.MaxTarget == null ? "Infinity" : mc.MaxTarget;
                            dr["MinTarget"] = mc.MinTarget == null ? "-Infinity" : mc.MinTarget;
                            dr["ValidMeasure"] = mc.ValidMeasure;
                            dr["Opportunity"] = mc.Opportunity == null ? "" : mc.Opportunity;
                            dr["AttributeWeightMeasure"] = mc.AttributeWeightMeasure;
                            dr["AnalysisMeasure"] = mc.AnalysisMeasure;
                            dr["SegmentMeasure"] = mc.SegmentMeasure;
                            if (mc.FilteredMeasures != null)
                                dr["FilteredMeasures"] = mc.FilteredMeasures;
                            dr["AutoGenerated"] = mc.LogicalAutoGenerated;
                            dr["ManuallyEdited"] = mc.ManuallyEdited;
                            if (mc.RootMeasure != null)
                                dr["RootMeasure"] = mc.RootMeasure;
                            //setLogicalMeasureFields(mc, dr);
                            dr["Derived"] = mt.Type == MeasureTable.DERIVED;
                            if (mt.Type == MeasureTable.DERIVED)
                                dr["PhysicalName"] = mc.PhysicalName;
                            measureColumnsTable.Rows.Add(dr);
                        }

                        if (mt.Type != 2 && mc.TableName != null)
                        {
                            DataRow mcmr = measureColumnMappings.NewRow();
                            mcmr["ColumnName"] = mc.ColumnName;
                            mcmr["TableName"] = mc.TableName;
                            mcmr["PhysicalName"] = mc.PhysicalName == null ? "" : mc.PhysicalName;
                            mcmr["Qualify"] = mc.Qualify;
                            mcmr["AutoGenerated"] = mc.AutoGenerated;
                            mcmr["ManuallyEdited"] = mc.ManuallyEdited;
                            mcmr["NotCacheable"] = mc.NotCacheable;
                            if (drv.Length > 0 && (string)drv[0].Row["AggregationRule"] != mc.AggregationRule)
                            {
                                mcmr["AggRuleOverride"] = mc.AggregationRule;
                            }
                            if (mc.PhysicalFilter != null && mc.PhysicalFilter.Length > 0)
                            {
                                mcmr["PhysicalFilter"] = mc.PhysicalFilter;
                            }
                            measureColumnMappings.Rows.Add(mcmr);
                        }
                    }
                }
            }
                
            addMeasureEventHandler();
            resetMeasureColumns = true;

            // dimensions
            if (r.Dimensions != null)
                dimensionsList = new List<Dimension>(r.Dimensions);
            else
                dimensionsList = new List<Dimension>();
            dimensionColumns.Rows.Clear();
            dimensionColumnMappings.Rows.Clear();
            dimensionTablesList = new List<DimensionTable>();
            unmappedDimensionTablesList = new List<DimensionTable>();
            foreach (DimensionTable dt in r.DimensionTables)
            {
                if (dt.TableName != null)
                {
                    if (dt.VirtualMeasuresInheritTable != null && dt.TableName == dt.VirtualMeasuresInheritTable)
                    {
                        reportMessage(null, null, "Dimension '" + dt.DimensionName + "': Dimension Table '" + dt.TableName + "' inherits from itself, which it cannot do!", DialogResult.OK, "Reading Repository", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        throw new Exception("Dimension '" + dt.DimensionName + "': Dimension Table '" + dt.TableName + "' inherits from itself, which it cannot do!");
                    }
                    dimensionTablesList.Add(dt);
                }
                else
                {
                    unmappedDimensionTablesList.Add(dt);
                }
            }

            foreach (DimensionTable dim in r.DimensionTables)
            {
                foreach (DimensionColumn dimCol in dim.DimensionColumns)
                {
                    DataRowView[] drv = dimensionColumnNamesView.FindRows(new object[] { dim.DimensionName, dimCol.ColumnName });
                    if (drv.Length == 0)
                    {
                            DataRow cdr = dimensionColumns.NewRow();
                            cdr["DimensionName"] = dim.DimensionName;
                            cdr["ColumnName"] = dimCol.ColumnName;
                            if (dimCol.Description != null) cdr["Description"] = dimCol.Description;
                            cdr["DataType"] = dimCol.DataType == null || dimCol.DataType.Length == 0 ? "Integer" : dimCol.DataType;
                            cdr["ColWidth"] = dimCol.Width == null || dimCol.Width.Length == 0 ? "" : dimCol.Width;
                            cdr["Key"] = dimCol.Key;
                            cdr["Format"] = dimCol.Format == null ? "" : dimCol.Format;
                            if (dimCol.VolatileKey != null) cdr["VolatileKey"] = dimCol.VolatileKey;
                            cdr["MeasureExpression"] = dimCol.MeasureExpression == null ? "" : dimCol.MeasureExpression;
                            cdr["GenericName"] = dimCol.GenericName == null ? "" : dimCol.GenericName;
                            cdr["DesiredValues"] = dimCol.DesiredValues == null ? "" : dimCol.DesiredValues;
                            cdr["IgnoredValues"] = dimCol.IgnoredValues == null ? "" : dimCol.IgnoredValues;
                            cdr["Score"] = Double.IsNaN(dimCol.Score) ? 0 : dimCol.Score;
                            cdr["BlockingGroup"] = dimCol.BlockingGroup;
                            cdr["Maskable"] = dimCol.Maskable == "false" || dimCol.Maskable.Length == 0 ? "No" : dimCol.Maskable;
                            if (dimCol.DisplayMap != null)
                                cdr["DisplayMap"] = dimCol.DisplayMap;
                            if (dimCol.UnknownValue != null)
                                cdr["UnknownValue"] = dimCol.UnknownValue;
                            cdr["AutoGenerated"] = dimCol.LogicalAutoGenerated;
                            cdr["ManuallyEdited"] = dimCol.ManuallyEdited;
                            cdr["Index"] = dimCol.Index;
                            cdr["Constant"] = dimCol.Constant;
                            dimensionColumns.Rows.Add(cdr);
                    }
                }
            }

            foreach (DimensionTable dim in r.DimensionTables)
            {
                foreach (DimensionColumn dimCol in dim.DimensionColumns)
                {
                    if (dimCol.TableName != null)
                    {
                        DataRowView[] drv = dimensionColumnNamesView.FindRows(new object[] { dim.DimensionName, dimCol.ColumnName });
                        if (drv.Length == 0 || (string)drv[0].Row["ColumnName"] != dimCol.ColumnName)
                        {
                            reportMessage(null,null, "Invalid Column in Dimension Table:\r\n" +
                                            "Dimension: " + dim.DimensionName + "\r\n" +
                                            "Dimension Table: " + dim.TableName + "\r\n" +
                                            "Column Name: " + dimCol.ColumnName,
                                            DialogResult.OK,
                                            "LoadRepository Error",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                        }
                        DataRow dcmr = dimensionColumnMappings.NewRow();
                        dcmr["TableName"] = dimCol.TableName;
                        dcmr["ColumnName"] = dimCol.ColumnName;
                        dcmr["Qualify"] = dimCol.Qualify;
                        dcmr["AutoGenerated"] = dimCol.AutoGenerated;
                        dcmr["ManuallyEdited"] = dimCol.ManuallyEdited;
                        dcmr["PhysicalName"] = dimCol.PhysicalName;
                        dcmr["NotCacheable"] = dimCol.NotCacheable;
                        dcmr["CacheBoundary"] = dimCol.CacheBoundary;
                        dimensionColumnMappings.Rows.Add(dcmr);
                    }
                }
            }

            // joins
            Joins.Rows.Clear();
            if (r.Joins != null)
            {
                foreach (Join j in r.Joins)
                {
                    DataRow dr = Joins.NewRow();
                    dr["Table1"] = j.Table1;
                    dr["Table2"] = j.Table2;
                    dr["JoinCondition"] = j.JoinCondition;
                    dr["Redundant"] = j.Redundant;
                    dr["JoinType"] = j.JoinType == null ? "Inner" : j.JoinType;
                    dr["Level"] = j.Level == null ? "" : j.Level;
                    dr["AutoGenerated"] = j.AutoGenerated;
                    dr["Invalid"] = j.Invalid;
                    dr["Federated"] = j.Federated;
                    if (j.FederatedJoinKeys != null)
                    {
                         dr["FederatedJoinKeys"]  = String.Join(",", j.FederatedJoinKeys);
                    }
                   Joins.Rows.Add(dr);
                }
            }

            mainTabControl.SelectedIndex = 0;

            unmappedMeasureGrainsByColumnName.Clear();
            if ((r.UnmappedMeasureGrains != null) && (r.UnmappedMeasureGrains.Length > 0))
            {
                for (int i = 0; i < r.UnmappedMeasureGrains.Length; i++)
                {
                    string columnName = r.UnmappedMeasureGrains[i].measureColumnName;
                    if (columnName == null)
                        continue;
                    List<MeasureGrain> unmappedMeasureGrainList = null;
                    if (unmappedMeasureGrainsByColumnName.ContainsKey(columnName))
                    {
                        unmappedMeasureGrainList = unmappedMeasureGrainsByColumnName[columnName];
                    }
                    else
                    {
                        unmappedMeasureGrainList = new List<MeasureGrain>();
                        unmappedMeasureGrainsByColumnName.Add(columnName, unmappedMeasureGrainList);
                    }
                    if (!unmappedMeasureGrainList.Contains(r.UnmappedMeasureGrains[i]))
                    {
                        unmappedMeasureGrainList.Add(r.UnmappedMeasureGrains[i]);
                    }
                }
            }

            foreach (RepositoryModule rm in modules)
            {
                rm.updateFromRepositoryFinal(r);
            }

            // load filters into form
            foreach (RepositoryModule rm in modules)
            {
                rm.setFilters(r, filterMod.filterList);
            }
        }

        DataColumnChangeEventHandler measureHandler;

        public void addMeasureEventHandler()
        {
            if (Headless)
                return;
            measureHandler = new DataColumnChangeEventHandler(measureColumnsTable_ColumnChanged);
            measureColumnsTable.ColumnChanged += measureHandler;
        }

        public void removeMeasureEventHandler()
        {
            measureColumnsTable.ColumnChanged -= measureHandler;
        }

        private void loadMenuItem_Click(object sender, System.EventArgs e)
        {
            openFile(null, true);
        }

        private void openFile(string fName, bool promptForFile)
        {
            string fileToUse;

            // First, check to see if what you've been working on has changed
            if (savedRepository != null)
            {
                Repository currentRepository = getRepository();
                bool changes = repositoryHasBeenChanged(currentRepository);
                if (changes)
                {
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    string dialogQuestion = "You have unsaved changes to your repository. Do you want to save those changes first?";
                    DialogResult resultSave = MessageBox.Show(dialogQuestion, "Unsaved Repository Changes", buttons);
                    if (resultSave == DialogResult.No)
                    {
                        changes = false;
                    }
                }
                if (changes) return;
            }

            //Prompt for file if requested 
            if (promptForFile)
            {
                DialogResult result = openRepositoryDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    fileToUse = openRepositoryDialog.FileName;
                }
                else
                {
                    return;
                }
            }
            else
            {
                fileToUse = fName;
            }

            if (!File.Exists(fileToUse))
            {
                loadRepositoryFromXMLFile(null);
                this.Text = defaultWindowTitle;
            }
            else
            {
                FileInfo fi = new FileInfo(fileToUse);
                loadRepositoryFromFile(fileToUse);
                repositoryDirectory = fi.DirectoryName;
                currentFilename = fileToUse;
                this.Text = defaultWindowTitle + ": " + mruManager.GetDisplayName(currentFilename, 80);
                mruManager.Add(currentFilename);
                appconfig.MRUItems = (string[])(mruManager.MRUList.ToArray(typeof(string)));
                fileHasBeenOpened();
            }
            setupTree();
        }

        private void fileHasBeenOpened()
        {
            fileOpened = true;
            this.mainLoadMenuItem.Enabled = false;
            this.menuItemMRU.Enabled = false;
        }

        private void mainTabControl_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (mainTabControl.SelectedTab == mainTabPage)
            {
                targetDimension.Items.Clear();
                List<DimensionTable> dtlist = dimensionTablesList;
                foreach (DimensionTable dt in dtlist)
                {
                    if (dt.DimensionName != null && !targetDimension.Items.Contains(dt.DimensionName))
                        targetDimension.Items.Add(dt.DimensionName);
                }
                goalMeasure.Items.Clear();
                List<string> mlist = getMeasureList(null)[0];
                foreach (string s in mlist)
                {
                    if (s != null && !goalMeasure.Items.Contains(s))
                        goalMeasure.Items.Add(s);
                }
            }
            else if (mainTabControl.SelectedTab == measuresTabPage)
            {
                mtConnectionBox.Items.Clear();
                foreach (DatabaseConnection dc in connection.connectionList)
                {
                    if (dc.Name != null && !mtConnectionBox.Items.Contains(dc.Name))
                        mtConnectionBox.Items.Add(dc.Name);
                }
            }
            else if (mainTabControl.SelectedTab == dimensionsTabPage)
            {
                dimTableLevel.Items.Clear();
                if (mainTree.SelectedNode != null &&
                    (mainTree.SelectedNode.Parent != null || mainTree.SelectedNode.Parent != dimensionTablesNode))
                {
                    string dimName = mainTree.SelectedNode.Parent.Text;
                    string dimTableName = mainTree.SelectedNode.Text;
                    List<string> list = hmodule.getDimensionLevelNames(dimName);
                    foreach (String s in list)
                        if (s != null) dimTableLevel.Items.Add(s);
                    list = getDimensionTableList(dimName);
                    setDimensionInheritOptions(dimName, dimTableName);
                    dimConnectionBox.Items.Clear();
                    foreach (DatabaseConnection dc in connection.connectionList)
                    {
                        if (dc.Name != null && !dimConnectionBox.Items.Contains(dc.Name))
                            dimConnectionBox.Items.Add(dc.Name);
                    }
                }
            }
        }

        public TreeNode getMainNode(TreeNode node)
        {
            if (node.Parent == null) return (null);
            TreeNode tn = node;
            while (tn.Parent.Parent != null)
            {
                tn = tn.Parent;
            }
            return (tn);
        }

        private int getMainNodeIndex(TreeNode node)
        {
            TreeNode pnode = getMainNode(node);
            int index = 0;
            for (; index < pnode.Nodes.Count; index++)
            {
                if (pnode.Nodes[index] == node) break;
            }
            if (index >= pnode.Nodes.Count)
                return (-1);
            return (index);
        }

        TreeNode mainTreeNode = null;
        public TreeNode lastNode;
        public TreeNode lastPNode;

        private void mainTree_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            mainTree.LabelEdit = false;
            implicitSaveDimensionTable();
            implicitSaveMeasureTable();
            TreeNode pnode = getMainNode(e.Node);
            mainTree.ContextMenuStrip = null;
            if (pnode == null) return;
            if (pnode.Text == "General") mainTabControl.SelectedTab = mainTabPage;
            else if (pnode.Text == "Measures")
            {
                if (columnNameComboBox.Items.Count == 0 || resetMeasureColumns)
                    updateMeasureColumnNames();
                if (e.Node.Parent == baseNode)
                {
                    measureDataGridView.Visible = false;
                    measureMapGridView.Visible = true;
                    int index = findMeasureTableIndex(e.Node.Text);
                    mainTree.ContextMenuStrip = measureTableMenu;
                    if (index >= 0)
                    {
                        MeasureTable mt = measureTablesList[index];
                        selectMeasureTable(mt);
                        mainTabControl.SelectedTab = measuresTabPage;
                        mainTreeNode = e.Node;
                        mainTree.LabelEdit = true;
                        removeMeasureTableDefinitionToolStripMenuItem.Visible = true;
                        viewUnmappedMeasuresToolStripMenuItem.Visible = false;
                        viewMeasureTableGrainToolStripMenuItem.Visible = true;
                        measureColumnMappingSource.Filter = "TableName='" + mt.TableName + "'";
                    }
                    else
                    {
                        removeMeasureTableDefinitionToolStripMenuItem.Visible = false;
                        viewUnmappedMeasuresToolStripMenuItem.Visible = false;
                        viewMeasureTableGrainToolStripMenuItem.Visible = false;
                    }
                }
                else if (e.Node == baseNode)
                {
                    mainTabControl.SelectedTab = measuresTabPage;
                    measureDataGridView.Visible = true;
                    measureMapGridView.Visible = false;
                    measureColumnSource.Filter = "Derived=False";
                    aggregationRuleColumn.Visible = true;
                    DimensionRules.Visible = true;
                    DisplayFunction.Visible = true;
                    keyDataGridViewCheckBoxColumn.Visible = true;
                    PhysicalNameCol.Visible = false;
                    measureColumnsTable.Columns["Derived"].DefaultValue = false;
                    mainTree.ContextMenuStrip = measureTableMenu;
                    removeMeasureTableDefinitionToolStripMenuItem.Visible = false;
                    viewUnmappedMeasuresToolStripMenuItem.Visible = true;
                    viewMeasureTableGrainToolStripMenuItem.Visible = false;
                    mainTreeNode = e.Node;
                }
                else if (e.Node == derivedNode)
                {
                    mainTabControl.SelectedTab = measuresTabPage;
                    measureDataGridView.Visible = true;
                    measureMapGridView.Visible = false;
                    measureColumnSource.Filter = "Derived=True";
                    aggregationRuleColumn.Visible = true;
                    DimensionRules.Visible = false;
                    DisplayFunction.Visible = false;
                    keyDataGridViewCheckBoxColumn.Visible = false;
                    PhysicalNameCol.Visible = true;
                    measureColumnsTable.Columns["Derived"].DefaultValue = true;
                    viewMeasureTableGrainToolStripMenuItem.Visible = false;
                }
            }
            else if (pnode.Text == "Dimensions")
            {
                mainTree.ContextMenuStrip = dimensionTableMenu;
                if (e.Node.Parent != dimensionTablesNode && e.Node != dimensionTablesNode)
                {
                    dimDataGridView.Visible = false;
                    dimDataGridView.Enabled = false;
                    dimColMappingGridView.Visible = true;
                    dimColMappingGridView.Enabled = true;
                    for (int i = 0; i < dimensionTablesList.Count; i++)
                    {
                        if (e.Node.Text == dimensionTablesList[i].TableName)
                        {
                            DimensionTable dim = dimensionTablesList[i];
                            selectDimensionTable(dim);
                            mainTabControl.SelectedTab = dimensionsTabPage;
                            mainTreeNode = e.Node;
                            removeDimensionTableMenuStripItem.Visible = true;
                            removeDimensionToolStripMenuItem.Visible = false;
                            viewUnmappedToolStripMenuItem.Visible = false;
                            mainTree.LabelEdit = true;
                        }
                    }
                }
                else if (e.Node != dimensionTablesNode)
                {
                    dimDataGridView.Visible = true;
                    dimDataGridView.Enabled = true;
                    dimColMappingGridView.Visible = false;
                    dimColMappingGridView.Enabled = false;
                    dimensionColumnMappingSource.Filter = "TableName=null";
                    dimensionColumnSource.Filter = "DimensionName='" + e.Node.Text + "'";
                    dimensionColumns.Columns["DimensionName"].DefaultValue = e.Node.Text;
                    removeDimensionTableMenuStripItem.Visible = false;
                    newDimensionTableMenuStripItem.Visible = true;
                    removeDimensionToolStripMenuItem.Visible = true;
                    viewUnmappedToolStripMenuItem.Visible = false;
                    mainTabControl.SelectedTab = dimensionsTabPage;
                    mainTreeNode = e.Node;
                    mainTree.LabelEdit = true;
                    dimensionColumns.ColumnChanged += new DataColumnChangeEventHandler(dimColChanged);
                }
                else
                {
                    newDimensionTableMenuStripItem.Visible = false;
                    removeDimensionTableMenuStripItem.Visible = false;
                    removeDimensionToolStripMenuItem.Visible = false;
                    viewUnmappedToolStripMenuItem.Visible = true;
                    mainTreeNode = e.Node;
                }
            }
            else if (pnode.Text == "Joins")
            {
                table1Column.Items.Clear();
                table2Column.Items.Clear();
                /*
                 * Add all measure table to choices for table 1
                 */
                HashSet<string> t1list = new HashSet<string>();
                HashSet<string> t2list = new HashSet<string>();
                foreach (MeasureTable mt in measureTablesList)
                {
                    if (mt.TableName != null)
                        t1list.Add(mt.TableName);
                }
                table1Column.Sorted = false;
                table2Column.Sorted = false;

                /*
                 * Add all virtual measure tables and dimension tables
                 */
                foreach (DimensionTable dt in dimensionTablesList)
                {
                    t1list.Add(dt.TableName);
                    t2list.Add(dt.TableName);
                    foreach (Hierarchy h in hmodule.getHierarchies())
                    {
                        if (h.DimensionName == dt.DimensionName)
                        {
                            foreach (Level l in h.Children)
                                addItems(Level, l);
                        }
                    }
                    if (dt.VirtualMeasuresInheritTable != null && dt.VirtualMeasuresInheritTable.Length > 0)
                    {
                        foreach (MeasureTable mt in measureTablesList)
                        {
                            int ix = findDimensionTableIndex(dt.VirtualMeasuresInheritTable);
                            if (ix >= 0)
                            {
                                DimensionTable dtNew = dimensionTablesList[ix];
                                string prefix = prefixDimensionTableJoin(mt.TableName, dtNew);
                                //if (checkDimensionTableJoin(mt.TableName, dt.VirtualMeasuresInheritTable))
                                if (prefix != null)
                                {
                                    t1list.Add(dt.VirtualMeasuresPrefix + prefix + mt.TableName);
                                }
                            }
                        }
                    }
                }
                //Make sure we dont have any dangling references
                foreach (DataRow dr in Joins.Rows)
                {
                    string table1 = (string)dr["Table1"];
                    string table2 = (string)dr["Table2"];
                    t1list.Add(table1);
                    t2list.Add(table2);
                }

                string[] arr = new string[t1list.Count];
                int count = 0;
                foreach (string s in t1list)
                    arr[count++] = s;
                table1Column.Items.AddRange(arr);
                arr = new string[t2list.Count];
                count = 0;
                foreach (string s in t2list)
                    arr[count++] = s;
                table2Column.Items.AddRange(arr);

                mainTabControl.SelectedTab = joinsTabPage;
                table1Column.Sorted = true;
                table2Column.Sorted = true;
            }
            foreach (RepositoryModule rm in modules)
            {
                if (pnode.Text == rm.ModuleName)
                {
                    bool result;
                    if (e.Node.PrevNode != null && (
                        (e.Node.Parent != null && getMainNode(e.Node).Text == "Data Sources" && e.Node.Text != "Data Sources" 
                            && (sourceFileMod.getSourceFiles())[e.Node.Index].amIBeingDragged) ||
                        (e.Node.Parent != null && getMainNode(e.Node).Text == "Staging Tables" && e.Node.Text != "Staging Tables"
                            && (stagingTableMod.getAllStagingTables())[e.Node.Index].amIBeingDragged)))
                        result= rm.selectedNode(e.Node.PrevNode);                        
                    else
                        result = rm.selectedNode(e.Node);
                    if (result)
                    {
                        mainTabControl.SelectedTab = rm.TabPage;
                        rm.setup();
                    }
                    if (!(pnode == e.Node) && typeof(RenameNode).IsInstanceOfType(rm))
                        mainTree.LabelEdit = true;
                }
            }
            if (lastPNode != null)
            {
                foreach (RepositoryModule rm in modules)
                {
                    if (lastPNode.Text == rm.ModuleName)
                    {
                        rm.implicitSave();
                    }
                }
            }
            lastNode = e.Node;
            lastPNode = pnode;
        }

        private int mainTreeRight;//original width of the TreeView pane
        private static int MARGIN_WIDTH = 5;
        private void mainTree_AfterExpandCollapse(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            int maxRight = mainTreeRight;
            foreach (TreeNode node in mainTree.Nodes)
            {
                maxRight = getMaxRight(node, maxRight);
            }
            if (maxRight > mainTreeRight)
                mainTree.Width = maxRight + MARGIN_WIDTH;
            else
                mainTree.Width = mainTreeRight;
            if (getBottomOfNode(mainTree.Nodes[mainTree.Nodes.Count - 1]) + (2 * MARGIN_WIDTH) > mainTree.Bottom)//if vertical scrollbar exists
                mainTree.Width += SystemInformation.VerticalScrollBarWidth;
            if (mainTree.Width > Screen.PrimaryScreen.Bounds.Width / 2)
                mainTree.Width = Screen.PrimaryScreen.Bounds.Width / 2;
            mainTabControl.Left = mainTree.Right;
            mainTabControl.Width = mainTabControl.Parent.Width - mainTree.Width - SystemInformation.VerticalScrollBarWidth;
        }

        private int getMaxRight(TreeNode node, int maxRight)
        {
            if (node.Nodes != null && node.Nodes.Count > 0)
            {
                foreach (TreeNode cNode in node.Nodes)
                {
                    if (cNode.IsExpanded || cNode.IsVisible || (cNode.Parent != null && cNode.Parent.IsExpanded))
                        maxRight = getMaxRight(cNode, maxRight);
                }
            }
            return maxRight > node.Bounds.Right ? maxRight : node.Bounds.Right;
        }

        private int getBottomOfNode(TreeNode node)
        {
            if (node.Nodes != null && node.Nodes.Count > 0 && node.Nodes[0].IsVisible)
            {
                return getBottomOfNode(node.LastNode);
            }
            return node.Bounds.Bottom;
        }

        private void addItems(DataGridViewComboBoxColumn column, Level l)
        {
            if (column.Items.Count == 0) column.Items.Add("");
            if (!column.Items.Contains(l.Name))
                column.Items.Add(l.Name);
            foreach (Level nl in l.Children)
            {
                addItems(column, nl);
            }
        }

        private void selectMeasureTable(MeasureTable mt)
        {
            setMeasureTable(mt);
            if (mt.Type != 1)
                measureTablePName.Text = mt.TableSource.ToString();
            else
                measureTablePName.Text = mt.PhysicalName;
            mtConnectionBox.Text = mt.TableSource.Connection;
            measureViewSQL.Text = mt.OpaqueView;
            mtCacheable.Checked = mt.Cacheable;
            if (mt.InheritTable != null)
            {
                inheritMeasures.Checked = true;
                measureInheritBox.Text = mt.InheritTable;
                measureInheritPrefix.Text = mt.InheritPrefix;
            }
            else
            {
                inheritMeasures.Checked = false;
                measureInheritBox.Text = "";
                measureInheritPrefix.Text = "";
            }
            switch (mt.Type)
            {
                case 0: //Normal
                    measureNormal.Checked = true;
                    measureModelBox.Enabled = false;
                    break;
                case 1: //Opaque View
                    measureView.Checked = true;
                    measureModelBox.Enabled = true;
                    break;
                case 2: //Derived Measure Table
                    measureDerived.Checked = true;
                    measureModelBox.Enabled = false;
                    break;
            }
            switch (mt.ModelingStep)
            {
                case Util.MODEL_NONE:
                    measureNone.Checked = true;
                    break;
                case Util.MODEL_PERFORMANCE:
                    measurePerformance.Checked = true;
                    break;
                case Util.MODEL_REFERENCE_POPULATIONS:
                    measurePeers.Checked = true;
                    break;
                case Util.MODEL_SUCCESS_MODELS:
                    measureSuccess.Checked = true;
                    break;
            }
            measureTableCardinality.Text = mt.Cardinality.ToString();
            recalculateCardinality.Checked = mt.RecalculateCardinality;
            measureColumnMappings.Columns["TableName"].DefaultValue = mt.TableName;
            QueryFilter.sortedListBox(measureContentFilters, this.filterMod.filterList, mt.TableSource.ContentFilters);
            setMeasureInheritOptions(mt.TableName);

        }

        private void measureTableList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
        }

        private void setMeasureTable(MeasureTable mt)
        {
            measureColumnMappingSource.SuspendBinding();
            measureColumnMappingSource.Filter = "[TableName]='" + mt.TableName + "'";
            measureColumnMappingSource.ResumeBinding();
        }

        private void measureTableOK_Click(object sender, System.EventArgs e)
        {
            updateMeasureTable();
        }

        private void updateMeasureTable()
        {
            if (mainTreeNode == null || mainTreeNode.Parent == null || mainTreeNode.Parent.Text != "Base") return;
            int selIndex = findMeasureTableIndex(mainTreeNode.Text);
            if (selIndex < 0 || selIndex >= measureTablesList.Count) return;
            MeasureTable mt = (measureTablesList)[selIndex];
            string oldname = mt.TableName;
            if (measureNormal.Checked) mt.Type = 0;
            else
                if (measureView.Checked) mt.Type = 1;
                else
                    if (measureDerived.Checked) mt.Type = 2;

            if (measureNone.Checked) mt.ModelingStep = Util.MODEL_NONE;
            else
                if (measurePeers.Checked) mt.ModelingStep = Util.MODEL_REFERENCE_POPULATIONS;
                else
                    if (measurePerformance.Checked) mt.ModelingStep = Util.MODEL_PERFORMANCE;
                    else
                        if (measureSuccess.Checked) mt.ModelingStep = Util.MODEL_SUCCESS_MODELS;

            mt.TableSource.Connection = mtConnectionBox.Text;
            mt.Cacheable = mtCacheable.Checked;
            mt.OpaqueView = measureViewSQL.Text;
            mt.Cardinality = Double.Parse(measureTableCardinality.Text);
            mt.RecalculateCardinality = recalculateCardinality.Checked;            
            if (inheritMeasures.Checked)
            {
                mt.InheritTable = measureInheritBox.Text;
                mt.InheritPrefix = measureInheritPrefix.Text;
            }
            else
            {
                mt.InheritTable = null;
                mt.InheritPrefix = null;
            }
            mt.PhysicalName = measureTablePName.Text;
            setMeasureTable(mt);
            measureTablesList[selIndex] = mt;

            for (int i = 0; i < measureColumnMappings.Rows.Count; i++)
            {
                DataRow dr = measureColumnMappings.Rows[i];
                string tname = (string)dr["TableName"];
                if (tname == oldname)
                {
                    dr["TableName"] = mt.TableName;
                    if (dr["ColumnName"] == null || (string)dr["ColumnName"] == "")
                    {
                        reportMessage(null, null, "A column in Measure Table '" + mt.TableName + "' has no name! Column will not be saved.",
                                                                DialogResult.OK, "Update Measure Table Error", 
                                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        measureColumnMappings.Rows.Remove(dr);
                        i--;
                    }
                }
            }

            mt.TableSource.ContentFilters = new string[measureContentFilters.CheckedItems.Count];
            for (int i = 0; i < measureContentFilters.CheckedItems.Count; i++)
            {
                mt.TableSource.ContentFilters[i] = measureContentFilters.CheckedItems[i].ToString();
            }
            mainTreeNode.Text = mt.TableName;
        }

        public void removeMeasureTableDefinition(string mtName, bool removeAssociatedJoins)
        {
            int index = findMeasureTableIndex(mtName);
            for (int i = 0; i < measureColumnMappings.Rows.Count; i++)
            {
                DataRow dr = measureColumnMappings.Rows[i];
                string tname = (string)dr["TableName"];
                if (tname == mtName)
                {
                    measureColumnMappings.Rows.Remove(dr);
                    i--;
                }
            }
            measureTablesList.RemoveAt(index);
            foreach (TreeNode tn in baseNode.Nodes)
            {
                if (tn.Text == mtName)
                {
                    baseNode.Nodes.Remove(tn);
                    break;
                }
            }
            if (removeAssociatedJoins)
                for (int i = 0; i < Joins.Rows.Count; i++)
                {
                    DataRow dr = Joins.Rows[i];
                    string table1 = (string)dr["table1"];
                    string table2 = (string)dr["table2"];
                    if (table1.Equals(mtName) || table2.Equals(mtName))
                    {
                        Joins.Rows.Remove(dr);
                        i--;
                    }
                }
        }

        private void removeMeasureTable_Click(object sender, System.EventArgs e)
        {
            if (mainTreeNode == null)
                return;
            removeMeasureTableDefinition(mainTreeNode.Text, true);
        }

        private void measureTableRemove_Click(object sender, System.EventArgs e)
        {
            removeMeasureTable_Click(sender, e);
        }

        private void viewMeasureTableGrainToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            int index = findMeasureTableIndex(mainTreeNode.Text);
            MeasureTable mt = measureTablesList[index];
            List<MeasureTableGrain> mtgList = getGrainInfo(mt);
            MeasureTableGrainForm mtgForm = new MeasureTableGrainForm(mtgList);
            mtgForm.Text = "Grains for measure table " + mt.TableName;
            mtgForm.ShowDialog();
        }

        //Validates that the dimension/level mentioned in each grain exists in the hierarchy. If it doesnt exist, set the level
        //to lowest generated level in hierarchy.
        public void validateAndFixGrains(MeasureTable mt)
        {
            if (mt.Grain == null || mt.Grain.measureTableGrains == null || mt.Grain.measureTableGrains.Length == 0)
            {
                return;
            }

            for (int i = 0; i < mt.Grain.measureTableGrains.Length; i++)
            {
                Hierarchy h = hmodule.getDimensionHierarchy(mt.Grain.measureTableGrains[i].DimensionName);
                Level dimensionTableLevel = h.findLevel(mt.Grain.measureTableGrains[i].DimensionLevel);
                if (dimensionTableLevel == null)
                {
                    Level l = h.getLowestGeneratedLevel();
                    if (l != null)
                    {
                        mt.Grain.measureTableGrains[i].DimensionLevel = l.Name;
                    }
                    else
                    {
                        mt.Grain.measureTableGrains[i].DimensionLevel = h.DimensionKeyLevel;
                    }
                }
            }
        }

        /*
         * Get the individual grains (dimension/level combinations) for a given measure table
         * Use existing joins to determine
         */
        public List<MeasureTableGrain> getGrainInfo(MeasureTable mt)
        {
            List<MeasureTableGrain> mtgList = new List<MeasureTableGrain>();
            // If already there, use existing
            if (mt.Grain != null && mt.Grain.measureTableGrains != null)
            {
                mtgList.AddRange(mt.Grain.measureTableGrains);
                return mtgList;
            }
            /*
             * If this measure table is inheriting from another table, 
             * then get its grain first (so can be overridden if needed)
             */
            if (mt.InheritTable != null)
            {
                int inheritTableIndex = findMeasureTableIndex(mt.InheritTable);
                if (inheritTableIndex != -1)
                {
                    List<MeasureTableGrain> inheritTableGrainInfo = getGrainInfo(measureTablesList[inheritTableIndex]);
                    if (inheritTableGrainInfo != null && inheritTableGrainInfo.Count > 0)
                    {
                        foreach(MeasureTableGrain mtg in inheritTableGrainInfo)
                        {
                            mtgList.Add((MeasureTableGrain)mtg.Clone());
                        }
                    }
                }
            }
            // Now look at explicit grain of this table
            List<DimensionTable> joiningDimensionTables = getJoiningDimensionTables(mt);
            List<string> dimensionsAdded = new List<string>();
            foreach (DimensionTable dt in joiningDimensionTables)
            {
                if (!dimensionsAdded.Contains(dt.DimensionName))
                {
                    dimensionsAdded.Add(dt.DimensionName);
                }
                // Override existing grain if new one is at a lower level
                MeasureTableGrain mtg = null;
                foreach (MeasureTableGrain lmtg in mtgList)
                {
                    if (lmtg.DimensionName == dt.DimensionName)
                    {
                        mtg = lmtg;
                        if (lmtg.DimensionLevel == dt.Level)
                        {
                            break;
                        }
                        Hierarchy h = hmodule.getDimensionHierarchy(dt.DimensionName);
                        Level dimensionTableLevel = h.findLevel(dt.Level);
                        if (dimensionTableLevel == null)
                        {
                            mtg = null;
                            continue;
                        }

                        if (dimensionTableLevel.findLevel(h, mtg.DimensionLevel) != null)
                        {
                            break;
                        }
                        // If the dimension table is at a lower level, override
                        mtg.DimensionLevel = dt.Level;
                        break;
                    }
                }
                if (mtg != null)
                    continue;
                mtg = new MeasureTableGrain();
                mtg.DimensionName = dt.DimensionName;
                mtg.DimensionLevel = (dt.Level == null || dt.Level.Equals("")) ? "<Implicit>" : dt.Level;
                mtgList.Add(mtg);
            }
            return mtgList;
        }

        // Determine dimension tables that join to a given measure table
        private List<DimensionTable> getJoiningDimensionTables(MeasureTable mt)
        {
            List<DimensionTable> joiningDimensionTables = new List<DimensionTable>();
            for (int i = 0; i < Joins.Rows.Count; i++)
            {
                String table1 = (String)Joins.Rows[i]["Table1"];
                String table2 = (String)Joins.Rows[i]["Table2"]; ;
                if (table1 == mt.TableName)
                {
                    int dimIndex = findDimensionTableIndex(table2);
                    if (dimIndex >= 0)
                    {
                        DimensionTable dt = dimensionTablesList[dimIndex];
                        joiningDimensionTables.Add(dt);
                    }
                }
                else if (table2 == mt.TableName)
                {
                    int dimIndex = findDimensionTableIndex(table1);
                    if (dimIndex >= 0)
                    {
                        DimensionTable dt = dimensionTablesList[dimIndex];
                        joiningDimensionTables.Add(dt);
                    }
                }
            }
            return joiningDimensionTables;
        }

        public bool joinExists(MeasureTable mt, DimensionTable dt)
        {
            return checkDimensionTableJoin(mt.TableName, dt.TableName);
        }

        public void removeAutoGeneratedJoins()
        {
            List<object[]> joinList = new List<object[]>();
            for (int i = 0; i < Joins.Rows.Count; i++)
            {
                DataRow dr = Joins.Rows[i];
                bool autoGenerated = (bool)dr["AutoGenerated"];
                if (!autoGenerated)
                    joinList.Add(dr.ItemArray);
            }
            Joins.Rows.Clear();
            foreach (object[] row in joinList)
                Joins.Rows.Add(row);
        }

        public void addNewMeasureTable(MeasureTable mt)
        {
            measureTablesList.Add(mt);
            TreeNode tn = new TreeNode(mt.TableName);
            tn.ImageIndex = 1;
            tn.SelectedImageIndex = 1;
            baseNode.Nodes.Add(tn);
            if (mt.AutoGenerated)
            {
                tn.ForeColor = Color.Blue;
            }
        }

        private void addMeasureTable_Click(object sender, System.EventArgs e)
        {
            MeasureTable mt = new MeasureTable();
            mt.TableName = "New Table";
            mt.TableSource = new TableSource();
            mt.TableSource.Connection = connection.connectionList[0].Name;
            mt.TableSource.Tables = new TableSource.TableDefinition[0];
            mt.Type = 0;
            mt.Cardinality = 1;
            addNewMeasureTable(mt);
        }

        private void newMeasureTable_Click(object sender, System.EventArgs e)
        {
            addMeasureTable_Click(sender, e);
        }

        private void setDimensionInheritOptions(String dname, string dtname)
        {
            dimensionInheritBox.Items.Clear();
            virtualMeasuresInheritBox.Items.Clear();
            List<string> list = getDimensionTableList(dname);
            foreach (String s in list)
            {
                if (s != null && s != dtname)
                {
                    dimensionInheritBox.Items.Add(s);
                    virtualMeasuresInheritBox.Items.Add(s);
                }
            }
        }

        private void setMeasureInheritOptions(string mname)
        {
            List<string> list = getMeasureTableList();
            measureInheritBox.Items.Clear();
            foreach (String s in list)
            {
                if (s != null && !measureInheritBox.Items.Contains(s) && s != mname)
                    measureInheritBox.Items.Add(s);
            }
            Aggregate[] aggList = aggModule.getAggregateList();
            if ((aggList != null) && (aggList.Length > 0))
            {
                foreach (Aggregate agg in aggList)
                {
                    if (agg != null && agg.Name != null && !measureInheritBox.Items.Contains(agg.Name) && agg.Name != mname)
                        measureInheritBox.Items.Add(agg.Name);
                }
            }
        }
        private void selectDimensionTable(DimensionTable dim)
        {
            setDimensionTable(dim);
            if (dim.Type != 1)
                dimensionTablePName.Text = dim.TableSource.ToString();
            else
                dimensionTablePName.Text = dim.PhysicalName;
            dimConnectionBox.Text = dim.TableSource.Connection;
            dimCacheable.Checked = dim.Cacheable;
            dimTableLevel.Text = dim.Level;
            if (dim.InheritTable != null)
            {
                inheritDimensionColumns.Checked = true;
                dimensionInheritBox.Text = dim.InheritTable;
                dimensionInheritPrefix.Text = dim.InheritPrefix;
                virtualMeasuresInheritBox.Text = "";
                virtualMeasuresPrefix.Text = "";
            }
            else if (dim.VirtualMeasuresInheritTable != null)
            {
                inheritWithVirtualMeasures.Checked = true;
                virtualMeasuresInheritBox.Text = dim.VirtualMeasuresInheritTable;
                virtualMeasuresPrefix.Text = dim.VirtualMeasuresPrefix;
                dimensionInheritBox.Text = "";
                dimensionInheritPrefix.Text = "";
            }
            else
            {
                noDimtableInheritance.Checked = true;
                dimensionInheritBox.Text = "";
                dimensionInheritPrefix.Text = "";
                virtualMeasuresInheritBox.Text = "";
                virtualMeasuresPrefix.Text = "";
            }
            if (mainTreeNode != null && mainTreeNode.Parent != null)
            {
                string dimName = mainTreeNode.Parent.Text;
                setDimensionInheritOptions(dimName, dim.TableName);
            }
            switch (dim.Type)
            {
                case 0: //Normal
                    dimNormal.Checked = true;
                    dimModelBox.Enabled = false;
                    break;
                case 1: //Opaque View
                    dimView.Checked = true;
                    dimModelBox.Enabled = true;
                    break;
            }
            switch (dim.ModelingStep)
            {
                case Util.MODEL_NONE:
                    noneButton.Checked = true;
                    break;
                case Util.MODEL_PERFORMANCE:
                    performanceButton.Checked = true;
                    break;
                case Util.MODEL_REFERENCE_POPULATIONS:
                    peerButton.Checked = true;
                    break;
                case Util.MODEL_SUCCESS_MODELS:
                    successButton.Checked = true;
                    break;
            }
            dimensionViewSQL.Text = dim.OpaqueView;
            dimensionColumnMappings.Columns["TableName"].DefaultValue = dim.TableName;
            QueryFilter.sortedListBox(dimensionContentFilters, this.filterMod.filterList, dim.TableSource.ContentFilters);
            int col = dimColMappingGridView.Columns["autoGenColumn"].Index;
        }

        private String lastDimension;

        private void setDimensionTable(DimensionTable dim)
        {
            dimensionColumnMappingSource.SuspendBinding();
            if (lastDimension != dim.DimensionName)
            {

                columnNameComboBox.Items.Clear();
                DataRow[] drS = dimensionColumns.Select("[DimensionName]='" + dim.DimensionName + "'");
                List<string> colList = new List<string>(drS.Length);
                foreach (DataRow dr in drS)
                {
                    colList.Add((string)dr["ColumnName"]);
                }
                colList.Sort();
                columnNameComboBox.Items.AddRange(colList.ToArray());
            }
            dimensionColumnSource.Filter = "DimensionName='" + dim.DimensionName + "'";
            dimensionColumns.Columns["DimensionName"].DefaultValue = dim.DimensionName;
            dimensionColumnMappingSource.Filter = "[TableName]='" + dim.TableName + "'";
            dimensionColumnMappingSource.ResumeBinding();
            lastDimension = dim.DimensionName;
        }

        private void dimensionTableOK_Click(object sender, System.EventArgs e)
        {
            updateDimensionTable();
        }
        private void updateDimensionTable()
        {
            if (mainTreeNode == null || mainTreeNode.Parent == null || mainTreeNode.Parent.Parent == null || mainTreeNode.Parent.Parent.Text != "Dimensions") return;
            int selIndex = findDimensionTableIndex(mainTreeNode.Text);
            if (selIndex < 0) return;
            DimensionTable dim = dimensionTablesList[selIndex];
            string oldname = dim.TableName;
            dim.Level = dimTableLevel.Text;
            dim.TableSource.Connection = dimConnectionBox.Text;
            dim.Cacheable = dimCacheable.Checked;
            dim.OpaqueView = dimensionViewSQL.Text;
            if (inheritDimensionColumns.Checked)
            {
                dim.InheritTable = dimensionInheritBox.Text;
                dim.InheritPrefix = dimensionInheritPrefix.Text;
            }
            else
            {
                dim.InheritTable = null;
                dim.InheritPrefix = null;
            }
            if (inheritWithVirtualMeasures.Checked)
            {
                dim.VirtualMeasuresInheritTable = virtualMeasuresInheritBox.Text;
                dim.VirtualMeasuresPrefix = virtualMeasuresPrefix.Text;
            }
            else
            {
                dim.VirtualMeasuresInheritTable = null;
                dim.VirtualMeasuresPrefix = null;
            }
            if (dimNormal.Checked) dim.Type = 0;
            else
                if (dimView.Checked) dim.Type = 1;

            if (noneButton.Checked) dim.ModelingStep = Util.MODEL_NONE;
            else
                if (peerButton.Checked) dim.ModelingStep = Util.MODEL_REFERENCE_POPULATIONS;
                else
                    if (performanceButton.Checked) dim.ModelingStep = Util.MODEL_PERFORMANCE;
                    else
                        if (successButton.Checked) dim.ModelingStep = Util.MODEL_SUCCESS_MODELS;

            dim.PhysicalName = dimensionTablePName.Text;
            setDimensionTable(dim);
            dimensionTablesList[selIndex] = dim;
            for (int i = 0; i < dimensionColumnMappings.Rows.Count; i++)
            {
                DataRow dr = dimensionColumnMappings.Rows[i];
                string tname = (string)dr["TableName"];
                if (tname == oldname)
                {
                    dr["TableName"] = dim.TableName;
                    if (dr["ColumnName"] == null || (string)dr["ColumnName"] == "")
                    {
                        reportMessage(null, null, "A column in Dimension Table " + dim.TableName + " in Dimension " + dim.DimensionName + " has no name! Column will not be saved.",
                                                                DialogResult.OK, "Update Dimension Table Error",
                                                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dimensionColumnMappings.Rows.Remove(dr);
                        i--;
                    }
                }
            }
            dim.TableSource.ContentFilters = new string[dimensionContentFilters.CheckedItems.Count];
            for (int i = 0; i < dimensionContentFilters.CheckedItems.Count; i++)
            {
                dim.TableSource.ContentFilters[i] = dimensionContentFilters.CheckedItems[i].ToString();
            }
            mainTreeNode.Text = dim.TableName;
        }

        public string getLevelSurrogateKeyName(string levelName, int maxLength)
        {
            string cname = null;
            if (serverPropertiesModule.generateUniqueKeys.Checked)
            {
                cname = levelName + Unsafe32BitHashCodeGenerator.GetHashCode32(levelName);
            }
            else
            {
                cname = levelName + " ID";
            }
            if ((builderVersion < 13 || ApplicationBuilder.getSQLGenType(this) == ApplicationBuilder.SqlGenType.MySQL || (builderVersion >= 24)) && maxLength > 0 && cname.Length > maxLength)
                cname = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(this), cname, maxLength, this.builderVersion);
            return cname;
        }

        public void addDimensionTableSurrogateKey(DimensionTable dim, Hierarchy h, Level l, int maxLength)
        {
            string surrogateKeyColumnName = getLevelSurrogateKeyName(dim.Level, maxLength);
            // Make sure that it is a level key for the right level and that level exists
            if (l == null)
            {
                l = addSurrogateKeyLevel(dim.DimensionName, dim.Level, surrogateKeyColumnName);
            }
            else
            {
                /* If adding a surrogate key, make sure it exists 
                 * or add it to the dimension */
                addSurrogateKeyColumn(dim, surrogateKeyColumnName);
                // See if existing surrogate key exists
                LevelKey lk = l.getSurrogateKey();
                if (lk == null)
                {
                    // Make sure the surrogate key is in the level and is primary
                    l.addSurrogateKey(h, surrogateKeyColumnName);
                }
                if (Array.IndexOf<string>(l.ColumnNames, surrogateKeyColumnName) < 0)
                {
                    List<string> newList = new List<string>(l.ColumnNames);
                    newList.Add(surrogateKeyColumnName);
                    l.ColumnNames = newList.ToArray();
                    List<bool> newblist = new List<bool>(l.HiddenColumns);
                    newblist.Add(true);
                    l.HiddenColumns = newblist.ToArray();
                }
            }
            //Clear the last selected node from hierarchies module so that the UI state does not overwrite the changes made to level
            //while adding the surrogate key.
            hmodule.clearSelectedNode();
        }

        public Dictionary<string, LocalETLConfig> localETLLoadGroupToLocalETLConfig = new Dictionary<string,LocalETLConfig>();
        
        public Level addDimensionTable(DimensionTable dim, Level l, bool addSurrogateKeyFlag, int maxLength)
        {
            if (l != null)
            {
                dim.Level = l.Name;
            }
            if (addSurrogateKeyFlag)
            {
                Hierarchy h = hmodule.getDimensionHierarchy(dim.DimensionName);
                addDimensionTableSurrogateKey(dim, h, l, maxLength);
                hmodule.updateHierarchy(h);
            }
            dimensionTablesList.Add(dim);
            TreeNode ptn = null;
            foreach (TreeNode dtn in dimensionTablesNode.Nodes)
            {
                if (dtn.Text == dim.DimensionName)
                {
                    ptn = dtn;
                    break;
                }
            }
            if (ptn == null)
            {
                ptn = addDimension(dim.DimensionName);
            }
            TreeNode tn = new TreeNode(dim.TableName);
            tn.ImageIndex = 1;
            tn.SelectedImageIndex = 1;
            ptn.Nodes.Add(tn);
            if (dim.AutoGenerated)
            {
                tn.ForeColor = Color.Blue;
            }
            return l;
        }

        public Level findLevelContainingDimensionKey(string dimensionName)
        {
            Hierarchy h = hmodule.getDimensionHierarchy(dimensionName);
            if (h != null)
            {
                return (h.findLevelContainingDimensionKey());
            }
            return (null);
        }

        private void addSurrogateKeyColumn(DimensionTable dim, string surrogateKeyColumnName)
        {
            // Don't add surrogate keys for time
            if (timeDefinition != null && timeDefinition.Name == dim.DimensionName)
                return;
            bool keyExists = false;
            List<DimensionColumn> dcList = new List<DimensionColumn>();
            Hierarchy h = hmodule.getDimensionHierarchy(dim.DimensionName);
            if (h == null)
                return;
            Level l = h.findLevel(dim.Level);
            if (l == null)
                return;
            DataRowView[] rows = null;
            LevelKey lk = null;
            foreach (LevelKey slk in l.Keys)
            {
                if (slk.SurrogateKey)
                {
                    lk = slk;
                    break;
                }
            }
            if (lk != null)
            {
                DataRowView[] drv = dimensionColumnNamesView.FindRows(
                    new object[] { dim.DimensionName, surrogateKeyColumnName });
                if (drv.Length > 0)
                {
                    drv[0].Row["AutoGenerated"] = true;
                    keyExists = true;
                }
                // Remove old surrogate key name from level
                if (lk.ColumnNames[0] != surrogateKeyColumnName)
                {
                    int skindex = Array.IndexOf<string>(l.ColumnNames, lk.ColumnNames[0]);
                    if (skindex >= 0)
                        l.ColumnNames[skindex] = surrogateKeyColumnName;
                    lk.ColumnNames[0] = surrogateKeyColumnName;
                }
            }
            if (!keyExists)
            {
                DimensionColumn newdc = new DimensionColumn();
                newdc.TableName = dim.TableName;
                newdc.ColumnName = surrogateKeyColumnName;
                newdc.DataType = "Integer";
                newdc.Key = true;
                newdc.Score = 0;
                newdc.Maskable = "No";
                if (dim.DimensionColumns != null)
                {
                    foreach (DimensionColumn dc in dim.DimensionColumns)
                        dcList.Add(dc);
                    dcList.Add(newdc);
                    dim.DimensionColumns = dcList.ToArray();
                }
                if (rows == null)
                    rows = dimensionColumnNamesView.FindRows(
                        new object[] { dim.DimensionName, newdc.ColumnName });
                DataRow dcr = null;
                if (rows.Length == 0)
                {
                    dcr = dimensionColumns.NewRow();
                    dimensionColumns.Rows.Add(dcr);
                }
                else dcr = rows[0].Row;
                dcr["DimensionName"] = dim.DimensionName;
                dcr["ColumnName"] = newdc.ColumnName;
                dcr["DataType"] = newdc.DataType;
                dcr["Key"] = newdc.Key;
                dcr["Score"] = newdc.Score;
                dcr["Maskable"] = newdc.Maskable;
                dcr["AutoGenerated"] = true;
            }
        }

        /*
         * Make sure a hierarchy and level exist where the level key is the specified surrogate key
         */
        private Level addSurrogateKeyLevel(string dimensionName, string levelName, string surrogateKeyColumnName)
        {
            Hierarchy h = hmodule.getDimensionHierarchy(dimensionName);
            Level l = null;
            if (h == null)
            {
                h = new Hierarchy();
                h.Name = dimensionName;
                h.DimensionName = dimensionName;
                l = new Level();
                h.DimensionKeyLevel = l.Name;
                l.Name = surrogateKeyColumnName;
                l.Cardinality = 1;
                l.ColumnNames = new String[] { surrogateKeyColumnName };
                l.Children = new Level[0];
                l.Keys = new LevelKey[0];
                l.addSurrogateKey(h, surrogateKeyColumnName);
                l.GenerateDimensionTable = true;
                h.Children = new Level[] { l };
                hmodule.addHierarchy(h);
            }
            else
            {
                LevelKey lk = new LevelKey();
                lk.ColumnNames = new String[] { surrogateKeyColumnName };
                l = h.findLevelWithKey(lk);
                if (l == null)
                {
                    Level dkeylevel = h.findLevelContainingDimensionKey();
                    l = new Level();
                    l.Name = surrogateKeyColumnName;
                    l.Cardinality = 1;
                    l.ColumnNames = new String[0];
                    l.Children = new Level[0];
                    l.addSurrogateKey(h, surrogateKeyColumnName);
                    l.GenerateDimensionTable = true;
                    Level[] oldChildren = h.Children;
                    h.Children = new Level[h.Children.Length + 1];
                    for (int i = 0; i < oldChildren.Length; i++)
                        h.Children[i] = oldChildren[i];
                    h.Children[oldChildren.Length] = l;
                }
            }
            return l;
        }

        private void dimensionTableNew_Click(object sender, System.EventArgs e)
        {
            if (mainTreeNode.Parent == dimensionTablesNode || mainTreeNode.Parent.Parent == dimensionTablesNode)
            {
                DimensionTable dim = new DimensionTable();
                dim.TableName = "New Table";
                dim.DimensionName = mainTreeNode.Parent == dimensionTablesNode ? mainTreeNode.Text : mainTreeNode.Parent.Text;
                dim.TableSource = new TableSource();
                dim.TableSource.Connection = connection.connectionList[0].Name;
                dim.TableSource.Tables = new TableSource.TableDefinition[0];
                dim.Level = "";
                addDimensionTable(dim, null, false, -1);
            }
        }

        private void viewUnmappedMeasures_Click(object sender, System.EventArgs e)
        {
            ViewUnmappedForm vuform = new ViewUnmappedForm(this, false);
            vuform.ShowDialog();
        }

        private void viewUnmappedAttributes_Click(object sender, System.EventArgs e)
        {
            ViewUnmappedForm vuform = new ViewUnmappedForm(this, true);
            vuform.ShowDialog();
        }

        public TreeNode addDimension(string name)
        {
            TreeNode tn = new TreeNode(name);
            tn.ImageIndex = 7;
            tn.SelectedImageIndex = 7;
            dimensionTablesNode.Nodes.Add(tn);
            Dimension d = new Dimension(name);
            tn.Tag = d;
            dimensionsList.Add(d);
            return (tn);
        }

        private void addDimension_Click(object sender, System.EventArgs e)
        {
            addDimension("New Dimension");
        }

        private void removeDimension_Click(object sender, System.EventArgs e)
        {
            if (mainTree.SelectedNode == null || mainTree.SelectedNode.Parent != dimensionTablesNode) return;
            deleteDimension(mainTree.SelectedNode.Text);
        }

        private void dimensionTableRemove_Click(object sender, System.EventArgs e)
        {
            if (mainTree.SelectedNode == null) return;
            removeDimensionTable(mainTree.SelectedNode.Text, true);
        }

        public void removeDimensionTable(string dimTableName, bool removeAssociatedJoins)
        {
            int selIndex = 0;
            for (; selIndex < dimensionTablesList.Count; selIndex++)
            {
                if (dimensionTablesList[selIndex].TableName == dimTableName)
                    break;
            }
            if (selIndex < dimensionTablesList.Count)
            {
                DimensionTable dim = dimensionTablesList[selIndex];
                for (int i = 0; i < dimensionColumnMappings.Rows.Count; i++)
                {
                    DataRow dr = dimensionColumnMappings.Rows[i];
                    string tname = (string)dr["TableName"];
                    if (tname == dim.TableName)
                    {
                        dimensionColumnMappings.Rows.Remove(dr);
                        i--;
                    }
                }
                bool foundUnmapped = false;
                foreach (DimensionTable unmappeddt in unmappedDimensionTablesList)
                {
                    if (unmappeddt.TableName == dim.TableName)
                    {
                        foundUnmapped = true;
                        break;
                    }
                }
                if (!foundUnmapped && (!dim.Calculated))
                {
                    unmappedDimensionTablesList.Add(dim);
                }
                dimensionTablesList.RemoveAt(selIndex);
                TreeNode dtnToBeRemoved = null;
                foreach (TreeNode dn in dimensionTablesNode.Nodes)
                {
                    if (dn.Text == dim.DimensionName)
                    {
                        foreach (TreeNode dtn in dn.Nodes)
                        {
                            if (dtn.Text == dim.TableName)
                            {
                                dtnToBeRemoved = dtn;
                                break;
                            }
                        }
                        if (dtnToBeRemoved != null)
                        {
                            break;
                        }
                    }
                }
                if (dtnToBeRemoved != null)
                    dtnToBeRemoved.Remove();
                if (removeAssociatedJoins)
                {
                    for (int i = 0; i < Joins.Rows.Count; i++)
                    {
                        DataRow dr = Joins.Rows[i];
                        Object o1 = dr["table1"];
                        Object o2 = dr["table2"];
                        string table1 = o1 == System.DBNull.Value ? null : (string)o1;
                        string table2 = o2 == System.DBNull.Value ? null : (string)o2;
                        if (table1.Equals(dim.TableName) || table2.Equals(dim.TableName))
                        {
                            Joins.Rows.Remove(dr);
                            i--;
                        }
                    }
                }
            }
        }

        private void newDimensionTableMenuItem_Click(object sender, System.EventArgs e)
        {
            dimensionTableNew_Click(sender, e);
        }

        private void viewUnmappedAttributesMenuItem_Click(object sender, System.EventArgs e)
        {
            viewUnmappedAttributes_Click(sender, e);
        }

        private void viewUnmappedMeasuresMenuItem_Click(object sender, System.EventArgs e)
        {
            viewUnmappedMeasures_Click(sender, e);
        }

        private void removeDimensionTableMenuItem_Click(object sender, System.EventArgs e)
        {
            dimensionTableRemove_Click(sender, e);
        }

        public List<string> getMeasureTableList()
        {
            List<string> list = new List<string>();
            foreach (MeasureTable mt in measureTablesList)
            {
                list.Add(mt.TableName);
            }
            return (list);
        }

        public List<string>[] curMeasureList = null;
        public Hashtable dimMeasureLists = new Hashtable();
        public Dictionary<string, List<string>> aggAvailableMeasures = null;

        public List<string>[] getMeasureList(string dname)
        {
            return (getMeasureList(dname, false));
        }

        public List<string>[] refreshMeasureList(string dname, bool getPrefix)
        {
            string dimKeyLevel = null;
            if (dname != null) dimKeyLevel = getDimensionKeyLevel(dname);
            HashSet<string> mset = new HashSet<string>();
            List<string> mlist = new List<string>();
            List<string> clist = new List<string>();
            aggAvailableMeasures = new Dictionary<string, List<string>>();

            // Derived Measure Columns
            List<string> aggAvailNoPrefixMeasures = new List<string>();
            DataRow[] rows = measureColumnsTable.Select("Derived=true");
            foreach (DataRow dr in rows)
                if (dr != null)
                {
                    if (!((bool)dr["Key"]))
                    {
                        string colName = (string)dr["ColumnName"];
                        if (!mset.Contains(colName))
                        {
                            mset.Add(colName);
                            mlist.Add(colName);
                            clist.Add((string)dr["PerformanceCategory"]);
                            aggAvailNoPrefixMeasures.Add(colName);
                        }
                    }
                }

            // Unmapped Measure Columns
            foreach (DataRow dr in measureColumnsTable.Rows)
            {
                DataRowView[] drv = measureColumnMappingsNamesView.FindRows(dr["ColumnName"].ToString());
                if (!((bool)dr["Derived"]) && drv.Length == 0)
                {
                    if (!((bool)dr["Key"]))
                    {
                        string colName = (string)dr["ColumnName"];
                        if (!mset.Contains(colName))
                        {
                            mset.Add(colName); ;
                            mlist.Add(colName);
                            aggAvailNoPrefixMeasures.Add(colName);
                            if (getPrefix)
                                clist.Add(null);
                            else
                                clist.Add((string)dr["PerformanceCategory"]);
                        }
                    }
                }
            }

            foreach (MeasureTable mt in measureTablesList)
            {
                bool dimJoin = true;
                if (dname != null) dimJoin = checkDimensionJoin(mt.TableName, dname);
                if (dimJoin || mt.Type == MeasureTable.DERIVED)
                {
                    List<string> prefixes = new List<string>();
                    // Add virtual measure columns generated from inherited dimensions
                    foreach (DimensionTable dt in dimensionTablesList)
                    {
                        if (dt.VirtualMeasuresInheritTable != null)
                        {
                            if (mt.InheritTable != null)
                            {
                                string prefix = prefixDimensionTableJoin(mt.InheritTable, dt);
                                if (prefix != null && !prefixes.Contains(prefix))
                                //if (checkDimensionTableJoin(mt.InheritTable, dt.VirtualMeasuresInheritTable))
                                {
                                    //prefixes.Add(dt.VirtualMeasuresPrefix);
                                    prefixes.Add(prefix);
                                    if (mt.InheritPrefix != null)
                                    {
                                        if (!prefixes.Contains(mt.InheritPrefix))
                                        {
                                            prefixes.Add(mt.InheritPrefix);
                                        }
                                        //Address the case when a measure table inherits with a prefix and also joins to dimension table with virtual measure prefix
                                        string combinedPrefix = prefix + mt.InheritPrefix;
                                        if (!prefixes.Contains(combinedPrefix))
                                        {
                                            prefixes.Add(combinedPrefix);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string prefix = prefixDimensionTableJoin(mt.TableName, dt);
                                if (prefix != null && !prefixes.Contains(prefix))
                                //if (checkDimensionTableJoin(mt.TableName, dt.VirtualMeasuresInheritTable))
                                {
                                    //prefixes.Add(dt.VirtualMeasuresPrefix);
                                    prefixes.Add(prefix);
                                }
                            }
                        }
                    }

                    if (prefixes.Count > 0)
                    {
                        for (int i = 0; i < prefixes.Count; i++)
                        {
                            if (!aggAvailableMeasures.ContainsKey(prefixes[i].Trim()))
                                aggAvailableMeasures.Add(prefixes[i].Trim(), new List<string>());
                        }
                    }
                    rows = measureColumnMappings.Select("TableName='" + mt.TableName + "'");
                    if (rows.Length > 0)
                        foreach (DataRow mdr in rows)
                        {
                            DataRowView[] drv = measureColumnNamesView.FindRows(mdr["ColumnName"].ToString());
                            if (drv.Length > 0)
                            {
                                DataRow dr = drv[0].Row;
                                if (!((bool)dr["Key"]))
                                {
                                    FilteredMeasureDefinition fmd = null;
                                    if (dr["FilteredMeasures"] != System.DBNull.Value)
                                        fmd = (FilteredMeasureDefinition)dr["FilteredMeasures"];
                                    string colName = (string)dr["ColumnName"];
                                    if (!mset.Contains(colName))
                                    {
                                        mset.Add(colName);
                                        mlist.Add(colName);
                                        aggAvailNoPrefixMeasures.Add(colName);
                                        if (getPrefix)
                                            clist.Add(mt.InheritPrefix);
                                        else
                                            clist.Add((string)dr["PerformanceCategory"]);
                                    }
                                    if (fmd != null)
                                    {
                                        foreach (string n in fmd.Names)
                                            if (!mset.Contains(n))
                                            {
                                                mset.Add(n);
                                                mlist.Add(n);
                                                aggAvailNoPrefixMeasures.Add(n);
                                                if (getPrefix)
                                                    clist.Add(mt.InheritPrefix);
                                                else
                                                    clist.Add((string)dr["PerformanceCategory"]);
                                            }
                                    }
                                    if (prefixes.Count > 0)
                                    {
                                        foreach (string s in prefixes)
                                        {
                                            string pcolName = s + dr["ColumnName"];
                                            if (!mset.Contains(pcolName))
                                            {
                                                mset.Add(pcolName);
                                                mlist.Add(pcolName);
                                                aggAvailableMeasures[s.Trim()].Add(pcolName);
                                                if (getPrefix)
                                                    clist.Add(mt.InheritPrefix);
                                                else
                                                    clist.Add((string)dr["PerformanceCategory"]);
                                                if (fmd != null)
                                                {
                                                    foreach (string n in fmd.Names)
                                                    {
                                                        pcolName = s + n;
                                                        if (!mset.Contains(pcolName))
                                                        {
                                                            mset.Add(pcolName);
                                                            mlist.Add(pcolName);
                                                            aggAvailableMeasures[s.Trim()].Add(pcolName);
                                                            if (getPrefix)
                                                                clist.Add(mt.InheritPrefix);
                                                            else
                                                                clist.Add((string)dr["PerformanceCategory"]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    if (mt.InheritTable != null)
                        foreach (MeasureTable mt2 in measureTablesList)
                        {
                            if (mt2.TableName == mt.InheritTable)
                            {
                                rows = measureColumnMappings.Select("TableName='" + mt2.TableName + "'");
                                if (rows.Length > 0)
                                    foreach (DataRow mdr in rows)
                                    {
                                        DataRowView[] drv = measureColumnNamesView.FindRows(mdr["ColumnName"].ToString());
                                        DataRow dr = drv[0].Row;
                                        if (dr != null)
                                        {
                                            string colName = mt.InheritPrefix + dr["ColumnName"];
                                            string inheritPrefixTrim = mt.InheritPrefix == null ? "" : mt.InheritPrefix.Trim();
                                            if (!((bool)dr["Key"]))
                                            {
                                                FilteredMeasureDefinition fmd = null;
                                                if (dr["FilteredMeasures"] != System.DBNull.Value)
                                                    fmd = (FilteredMeasureDefinition)dr["FilteredMeasures"];
                                                if (!mset.Contains(colName))
                                                {
                                                    mset.Add(colName);
                                                    mlist.Add(colName);
                                                    if (!aggAvailableMeasures.ContainsKey(inheritPrefixTrim))
                                                    {
                                                        aggAvailableMeasures.Add(inheritPrefixTrim, new List<String>());
                                                    }
                                                    aggAvailableMeasures[inheritPrefixTrim].Add(colName);
                                                    if (getPrefix)
                                                        clist.Add(mt.InheritPrefix);
                                                    else
                                                        clist.Add((string)dr["PerformanceCategory"]);
                                                }
                                                if (fmd != null)
                                                {
                                                    foreach (string n in fmd.Names)
                                                    {
                                                        string inheritName = mt.InheritPrefix + n;
                                                        if (!mset.Contains(inheritName))
                                                        {
                                                            mset.Add(inheritName);
                                                            mlist.Add(inheritName);
                                                            aggAvailableMeasures[inheritPrefixTrim].Add(inheritName);
                                                            if (getPrefix)
                                                                clist.Add(mt.InheritPrefix);
                                                            else
                                                                clist.Add((string)dr["PerformanceCategory"]);
                                                        }
                                                    }
                                                }
                                                if (prefixes.Count > 0)
                                                {
                                                    foreach (string s in prefixes)
                                                    {
                                                        string pcolName = s + dr["ColumnName"];
                                                        if (!mset.Contains(pcolName))
                                                        {
                                                            mset.Add(pcolName);
                                                            mlist.Add(pcolName);
                                                            aggAvailableMeasures[s.Trim()].Add(pcolName);
                                                            clist.Add((string)dr["PerformanceCategory"]);
                                                            if (fmd != null)
                                                            {
                                                                foreach (string n in fmd.Names)
                                                                {
                                                                    pcolName = s + n;
                                                                    if (!mset.Contains(pcolName))
                                                                    {
                                                                        mset.Add(pcolName);
                                                                        mlist.Add(pcolName);
                                                                        aggAvailableMeasures[s.Trim()].Add(pcolName);
                                                                        if (getPrefix)
                                                                            clist.Add(mt.InheritPrefix);
                                                                        else
                                                                            clist.Add((string)dr["PerformanceCategory"]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                            }
                        }
                }
            }
            // Add virtual measure columns
            if (vcolumns.vclist != null)
            {
                foreach (VirtualColumn vc in vcolumns.vclist.virtualColumns)
                {
                    if (vc.Type == VirtualColumn.TYPE_PIVOT) //Pivot Measures
                    {
                        mlist.Add(vc.Name);
                        aggAvailNoPrefixMeasures.Add(vc.Name);
                    }
                }
            }
            List<string>[] lists = new List<string>[2];
            lists[0] = mlist;
            lists[1] = clist;
            if (dname == null)
            {
                curMeasureList = lists;
                genNewMeasureList = false;
            }
            else
            {
                dimMeasureLists.Add(dname, lists);
                genNewMeasureList = false;
            }
            aggAvailableMeasures.Add("<blank>", aggAvailNoPrefixMeasures);

            return (lists);
        }

        public List<string> getDimensionList()
        {
            List<string> list = new List<string>();
            foreach (TreeNode tn in dimensionTablesNode.Nodes)
                list.Add(tn.Text);
            return (list);
        }

        public List<string> getDimensionTableList(string dimName)
        {
            List<string> list = new List<string>();
            if (dimensionTablesList == null) return (list);
            foreach (DimensionTable dt in (dimensionTablesList))
            {
                if (dt.DimensionName == dimName)
                {
                    list.Add(dt.TableName);
                }
            }
            return (list);
        }

        public List<string>[] getMeasureList(string dname, bool getPrefix)
        {
            if (genNewMeasureList)
            {
                dimMeasureLists = new Hashtable();
                curMeasureList = null;
            }
            if (dname == null)
            {
                if (curMeasureList != null)
                    return curMeasureList;                
            }
            else
            {
                List<string>[] dlists = (List<string>[])dimMeasureLists[dname];
                if (dlists != null)
                    return dlists;
            }

            return refreshMeasureList(dname, getPrefix);            
        }

        public List<string> getDimensionColumnList(string dimName)
        {
            List<string> list = new List<string>();
            if (dimensionTablesList == null) return (list);
            DataRow[] drs = dimensionColumns.Select("DimensionName='" + dimName + "'");
            foreach (DataRow dr in drs)
            {
                list.Add((string)dr["ColumnName"]);
            }
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if ((dt.DimensionName == dimName) || (dimName == null))
                {
                    if (vcolumns.vclist != null)
                        foreach (VirtualColumn vc in vcolumns.vclist.virtualColumns)
                        {
                            if ((vc.Type != VirtualColumn.TYPE_PIVOT) && (vc.TableName == dt.TableName)) //Not a Pivot Measure - i.e. an attribute
                            {
                                if (!list.Contains(vc.Name))
                                {
                                    list.Add(vc.Name);
                                }
                            }
                        }
                    if (dt.InheritTable != null)
                    {
                        int inhIndex = findDimensionTableIndex(dt.InheritTable);
                        if (inhIndex >= 0)
                        {
                            DimensionTable idt = dimensionTablesList[inhIndex];
                            if (idt.DimensionColumns != null)
                                foreach (DimensionColumn dimCol in idt.DimensionColumns)
                                {
                                    string colName = dt.InheritPrefix + dimCol.ColumnName;
                                    if (!(dimCol.Key) && !list.Contains(colName))
                                    {
                                        list.Add(colName);
                                    }
                                }
                        }

                    }
                }
            }
            return (list);
        }

        public VirtualColumn[] getVirtualColumnList()
        {
            vcolumns.vclist.updateRepositoryDirectory(repositoryDirectory, this);
            vcolumns.vclist.refreshIfNecessary(this);
            return vcolumns.vclist.virtualColumns;
        }

        public void updateVirtualColumn(VirtualColumn vc, string userName)
        {
            vcolumns.vclist.updateRepositoryDirectory(repositoryDirectory, this);
            vcolumns.vclist.updateItem(vc, userName, "Bucketed measure", this);
        }

        public void deleteVirtualColumn(VirtualColumn vc)
        {
            vcolumns.vclist.updateRepositoryDirectory(repositoryDirectory, this);
            vcolumns.vclist.deleteItem(vc, this);
        }

        public static string hashstring(string password)
        {
            byte[] passwordData = UnicodeEncoding.ASCII.GetBytes(password);

            // Create a 2-byte salt using a cryptographically secure random number generator
            byte[] saltData = new byte[2];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(saltData);

            // Append the salt to the end of the password
            byte[] saltedPasswordData = new byte[passwordData.Length + saltData.Length];
            Array.Copy(passwordData, 0, saltedPasswordData, 0, passwordData.Length);
            Array.Copy(saltData, 0, saltedPasswordData, passwordData.Length, saltData.Length);

            // Create a new SHA-1 instance and compute the hash 
            SHA1Managed sha = new SHA1Managed();
            byte[] hashData = sha.ComputeHash(saltedPasswordData);

            // add the salt back in
            byte[] finalData = new byte[hashData.Length + saltData.Length];
            Array.Copy(hashData, 0, finalData, 0, hashData.Length);
            Array.Copy(saltData, 0, finalData, hashData.Length, saltData.Length);
            return (Convert.ToBase64String(finalData));
        }

        public static string digest(string password)
        {
            if (password == null) return null;

            // Base64 encode the hash and salt, and then add a prefix to identify the digest type
            return "$sha-1$" + hashstring(password);
        }

        public static string encrypt(string text, string password)
        {
            if (text == null) return null;

            AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
            provider.Mode = CipherMode.CBC;
            provider.Padding = PaddingMode.PKCS7;
            provider.BlockSize = 128;
            provider.KeySize = 128;
            byte[] pwdBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] keyBytes = new byte[16];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length) len = keyBytes.Length;
            System.Array.Copy(pwdBytes, keyBytes, len);

            provider.Key = keyBytes;
            provider.IV = keyBytes;
            ICryptoTransform transform = provider.CreateEncryptor();

            byte[] plainText = Encoding.UTF8.GetBytes(text);
            byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);

            string newVal = Convert.ToBase64String(cipherBytes);
            return newVal;
        }

        public static string decrypt(string text, string password)
        {
            if (text == null) return null;
            try
            {
                AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
                provider.Mode = CipherMode.CBC;
                provider.Padding = PaddingMode.PKCS7;
                provider.BlockSize = 128;
                provider.KeySize = 128;

                byte[] encryptedData = Convert.FromBase64String(text);
                byte[] pwdBytes = System.Text.Encoding.UTF8.GetBytes(password);
                byte[] keyBytes = new byte[16];
                int len = pwdBytes.Length;
                if (len > keyBytes.Length) len = keyBytes.Length;
                System.Array.Copy(pwdBytes, keyBytes, len);

                provider.Key = keyBytes;
                provider.IV = keyBytes;
                ICryptoTransform transform = provider.CreateDecryptor();

                byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);

                string newVal = Encoding.UTF8.GetString(plainText);
                return newVal;
            }
            catch (Exception)
            {
                return text;
            }
        }

        private void serverConnMenuItem_Click(object sender, System.EventArgs e)
        {
            ServerConnection sc = new ServerConnection();
            sc.serverConnectionBox.Text = appconfig.ServerConnectionURL;
            if (sc.ShowDialog() == DialogResult.OK)
            {
                appconfig.ServerConnectionURL = sc.serverConnectionBox.Text;
                saveConfig();
            }
        }

        private void processDataRowType(int type, DataRow dr, Boolean addAggregationRule)
        {
            switch (type)
            {
                case 12:
                case 1:
                case -8:
                    // Varchar
                    dr["DataType"] = "Varchar";
                    dr["Format"] = "";
                    break;
                case 3:
                    // Currency
                    dr["DataType"] = "Number";
                    dr["Format"] = "$#,###,###";
                    if (addAggregationRule)
                        dr["AggregationRule"] = "SUM";
                    break;
                case 4:
                case 5:
                case -6:
                case -7:
                    // Integer, Smallint, Tinyint
                    dr["DataType"] = "Integer";
                    dr["Format"] = "#,###";
                    if (addAggregationRule)
                        dr["AggregationRule"] = "SUM";
                    break;
                case 8:
                case 7:
                    // Double, Float
                    dr["DataType"] = "Number";
                    dr["Format"] = "#,###";
                    if (addAggregationRule)
                        dr["AggregationRule"] = "SUM";
                    break;
                case 93:
                    dr["DataType"] = "DateTime";
                    dr["Format"] = "MM/dd/yyyy";
                    break;
            }
        }

        private string importDimensionTable(ImportTableResult result, bool onlyNonNumbers)
        {
            DimensionTable dim = null;
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.TableName == result.ImportAsName)
                {
                    dim = dt;
                    break;
                }
            }
            if (dim == null)
            {
                dim = new DimensionTable();
                if (result.ImportAsName.Length > 0)
                    dim.TableName = result.ImportAsName;
                else
                    dim.TableName = result.TableName;
                if (result.TableName.EndsWith(" Dim"))
                    dim.DimensionName = result.TableName.Substring(0, result.TableName.Length - 4);
                else
                    dim.DimensionName = result.TableName;
                dim.TableSource = new TableSource();
                dim.TableSource.Connection = result.ConnectionName;
                dim.TableSource.Tables = new TableSource.TableDefinition[1];
                dim.TableSource.Tables[0] = new TableSource.TableDefinition();
                dim.TableSource.Tables[0].PhysicalName = result.TableName;
                dim.TableSource.Tables[0].JoinClause = "";
                dim.Level = "";
                addDimensionTable(dim, null, false, -1);
            }
            for (int i = 0; i < result.ColumnNames.Count; i++)
            {
                if (dimensionColumnMappings.Select("TableName='" + result.ImportAsName + "' AND PhysicalName='" + result.ColumnNames[i] + "'").Length == 0)
                {
                    DataRow dr = dimensionColumnMappings.NewRow();
                    dr["TableName"] = dim.TableName;
                    dr["ColumnName"] = result.ColumnNames[i];
                    dr["PhysicalName"] = result.ColumnNames[i];
                    dr["Qualify"] = true;
                    DataRow ddr = null;
                    bool found = false;
                    DataRowView[] drv = dimensionColumnNamesView.FindRows(
                                        new object[] { dim.DimensionName, (string)result.ColumnNames[i] });
                    if (drv.Length > 0)
                    {
                        ddr = drv[0].Row;
                        found = true;
                    }
                    else
                        ddr = dimensionColumns.NewRow();
                    ddr["Maskable"] = "No";
                    ddr["DimensionName"] = dim.DimensionName;
                    ddr["ColumnName"] = result.ColumnNames[i];
                    ddr["Key"] = false;
                    ddr["ColWidth"] = "";
                    processDataRowType((int)result.ColumnTypes[i], ddr, false);
                    if (!onlyNonNumbers || (((string)ddr["DataType"]) == "Varchar" || ((string)ddr["DataType"]) == "DateTime") || ((string)ddr["DataType"]) == "Date")
                    {
                        if (!found) dimensionColumns.Rows.Add(ddr);
                        dimensionColumnMappings.Rows.Add(dr);
                    }
                }
            }
            return (dim.TableName);
        }

        private string importMeasureTable(ImportTableResult result, bool onlyNumbers)
        {
            MeasureTable mt = null;
            foreach (MeasureTable nmt in measureTablesList)
            {
                if (nmt.TableName == result.ImportAsName)
                {
                    mt = nmt;
                    break;
                }
            }
            if (mt == null)
            {
                mt = new MeasureTable();
                if (result.ImportAsName.Length > 0)
                    mt.TableName = result.ImportAsName;
                else
                    mt.TableName = result.TableName;
                mt.TableSource = new TableSource();
                mt.TableSource.Connection = result.ConnectionName;
                mt.TableSource.Tables = new TableSource.TableDefinition[1];
                mt.TableSource.Tables[0] = new TableSource.TableDefinition();
                mt.TableSource.Tables[0].PhysicalName = result.TableName;
                mt.TableSource.Tables[0].JoinClause = "";
                mt.Type = 0;
                mt.Cardinality = 1;
                addNewMeasureTable(mt);
            }
            for (int i = 0; i < result.ColumnNames.Count; i++)
            {
                if (measureColumnMappings.Select("TableName='" + result.ImportAsName + "' AND PhysicalName='" + result.ColumnNames[i] + "'").Length == 0)
                {
                    DataRow dr = measureColumnMappings.NewRow();
                    dr["TableName"] = mt.TableName;
                    dr["ColumnName"] = result.ColumnNames[i];
                    dr["PhysicalName"] = result.ColumnNames[i];
                    dr["Qualify"] = true;
                    DataRow mdr = measureColumnsTable.NewRow();
                    processDataRowType((int)result.ColumnTypes[i], mdr, true);
                    if (!onlyNumbers || !(((string)mdr["DataType"]) == "Varchar" || ((string)mdr["DataType"]) == "DateTime") || ((string)mdr["DataType"]) == "Date")
                    {
                        DataRowView[] drv = measureColumnNamesView.FindRows((string)result.ColumnNames[i]);
                        bool found = false;
                        if (drv.Length > 0)
                        {
                            drv[0].Row["DataType"] = mdr["DataType"];
                            mdr = drv[0].Row;
                            found = true;
                        }
                        mdr["ColumnName"] = result.ColumnNames[i];
                        mdr["PerformanceCategory"] = "";
                        mdr["Key"] = false;
                        mdr["Improve"] = 0;
                        if (!found)
                            measureColumnsTable.Rows.Add(mdr);
                        measureColumnMappings.Rows.Add(dr);
                    }
                }
            }
            return (mt.TableName);
        }

        private void importMenuItem_Click(object sender, System.EventArgs e)
        {
            mainTreeNode = null;
            mainTree.SelectedNode = null;
            List<string> measureTableDefinitions = new List<string>();
            List<string> dimensionTableDefinitions = new List<string>();
            foreach (DimensionTable dt in dimensionTablesList)
            {
                dimensionTableDefinitions.Add(dt.TableName);
            }
            foreach (MeasureTable mt in measureTablesList)
            {
                measureTableDefinitions.Add(mt.TableName);
            }
            ImportTable it = new ImportTable(appconfig.ServerConnectionURL, usersAndGroups.getAdminUsername(), usersAndGroups.getAdminPassword(), measureTableDefinitions, dimensionTableDefinitions);
            if ((it.ShowDialog() == DialogResult.OK) && (it.result.TableName != null))
            {
                it.result.TableName = it.result.TableName.Replace("'", "");
                if (it.dimensionButton.Checked)
                {
                    importDimensionTable(it.result, false);
                }
                else if (it.factButton.Checked)
                {
                    importMeasureTable(it.result, false);
                }
                else
                {
                    string name = null;
                    if (it.result.ImportAsName.Length > 0)
                        name = it.result.ImportAsName;
                    else
                        name = it.result.TableName;
                    name = name.Replace("'", "");
                    it.result.ImportAsName = name + " Dim";
                    importDimensionTable(it.result, false);
                    it.result.ImportAsName = name + " Fact";
                    importMeasureTable(it.result, true);
                    // Add Join
                    DataRow dr = Joins.NewRow();
                    dr["Table1"] = name + " Fact";
                    dr["Table2"] = name + " Dim";
                    dr["Redundant"] = true;
                    dr["AutoGenerated"] = false;
                    dr["JoinType"] = "Inner";
                    Joins.Rows.Add(dr);
                }
            }
        }

        public string getDimensionKeyLevel(string dname)
        {
            // Get hierarchy for dimension
            for (int i = 0; i < hmodule.rootNode.Nodes.Count; i++)
            {
                Hierarchy h = (Hierarchy)hmodule.rootNode.Nodes[i].Tag;
                if (h.DimensionName == dname)
                {
                    return (h.DimensionKeyLevel);
                }
            }
            return (null);
        }

        private bool findMeasureTableColumn(string measure, string tablename, string lookuptable, string prefix, List<String> mtables, List<String> mptables)
        {
            MeasureTable mt = getMeasureTableFromList(lookuptable);
            //Inheriting a measure table from an aggregate might cause mt to be null.
            if (mt == null)
            {
                return (false);
            }
            foreach (MeasureColumn mc in mt.MeasureColumns)
            //foreach (DataRow dr in measureColumnsTable.Select("TableName='" + lookuptable + "'"))
            {
                //string colname = prefix + ((string)dr["ColumnName"]);
                string colname = prefix + mc.ColumnName;
                int ind1 = mtables.IndexOf(tablename);
                int ind2 = mptables.IndexOf(lookuptable);
                if ((colname == measure) && ((ind1 < 0 && ind2 < 0) || ind1 != ind2))
                {
                    mtables.Add(tablename);
                    mptables.Add(lookuptable);
                    return (true);
                }
            }
            return (false);
        }

        private MeasureTable getMeasureTableFromList(string name)
        {
            foreach (MeasureTable mt in measureTablesList)
                if (mt.TableName != null && mt.TableName == name) return mt;
            return null;
        }

        /*
         * Returns two arrays for a given measure: 1) array of all the measure 
         * tables that contain it, 2) the measure table names of the tables that are the parent
         * tables (i.e. the ones that the owners are derived from
         */
        private List<String>[] getMeasureTables(string measure)
        {
            // Find all measure tables containing the measure
            List<String> mtables = new List<String>();
            List<String> mptables = new List<String>();
            foreach (MeasureTable mt in measureTablesList)
            {
                bool found = false;
                if (mt.TableName != null)
                {
                    found = findMeasureTableColumn(measure, mt.TableName, mt.TableName, "", mtables, mptables);
                }
                // If not found, see if column is directly inheritetd
                if (!found && (mt.InheritTable != null) && (mt.InheritTable.Length > 0))
                {
                    found = findMeasureTableColumn(measure, mt.TableName, mt.InheritTable, mt.InheritPrefix, mtables, mptables);
                }
                // If still not found, look for virtual measure table
                if (!found)
                {
                    foreach (DimensionTable dt in dimensionTablesList)
                    {
                        if (dt.VirtualMeasuresInheritTable != null && mt.TableName != null)
                        {

                            if (checkDimensionTableJoin(mt.TableName, dt.VirtualMeasuresInheritTable))
                            {
                                found = findMeasureTableColumn(measure, mt.TableName, mt.TableName, dt.VirtualMeasuresPrefix, mtables, mptables);
                                // If not found, see if column is directly inheritetd
                                if (!found && (mt.InheritTable != null) && (mt.InheritTable.Length > 0))
                                {
                                    found = findMeasureTableColumn(measure, mt.TableName, mt.InheritTable, dt.VirtualMeasuresPrefix + mt.InheritPrefix, mtables, mptables);
                                }
                            }

                        }
                        if (found) break;
                    }
                }
            }
            return (new List<String>[] { mtables, mptables });
        }

        public bool checkDerived(string measure)
        {
            foreach (MeasureTable mt in measureTablesList)
            {
                // Look for derived tables
                if (mt.Type == MeasureTable.DERIVED)
                {
                    foreach (MeasureColumn mc in mt.MeasureColumns)
                    {
                        if (mc.ColumnName == measure) return (true);
                    }
                }
            }
            return (false);
        }

        public List<String> checkJoin(string measure, string dimension, string attribute)
        {
            List<String> results = new List<String>();
            List<String>[] measureTables = getMeasureTables(measure);
            List<String> mtables = measureTables[0];
            List<String> mptables = measureTables[1];
            // Find all dimension tables that contain the attribute
            List<String> dimtables = new List<String>();
            List<String> dimptables = new List<String>();
            for (int count = 0; count < dimensionTablesList.Count; count++)
            {
                if (dimensionTablesList[count].DimensionName == dimension)
                {
                    bool found = false;
                    foreach (DimensionColumn dimCol in dimensionTablesList[count].DimensionColumns)
                    {
                        string colname = dimCol.ColumnName;
                        if ((colname == attribute) && !dimtables.Contains(dimensionTablesList[count].TableName))
                        {
                            dimtables.Add(dimensionTablesList[count].TableName);
                            dimptables.Add(dimensionTablesList[count].TableName);
                            found = true;
                        }
                    }
                    if (!found && (dimensionTablesList[count].InheritTable != null) && (dimensionTablesList[count].InheritTable.Length > 0))
                    {
                        int inhIndex = findDimensionTableIndex(dimensionTablesList[count].InheritTable);
                        if (inhIndex >= 0)
                        {
                            foreach (DimensionColumn dimCol in dimensionTablesList[inhIndex].DimensionColumns)
                            {
                                string colname = dimensionTablesList[count].InheritPrefix + dimCol.ColumnName;
                                if ((colname == attribute) && !dimtables.Contains(dimensionTablesList[count].TableName))
                                {
                                    dimtables.Add(dimensionTablesList[count].TableName);
                                    dimptables.Add(dimensionTablesList[count].TableName);
                                }
                            }
                        }
                    }
                }
            }
            // Add inherited tables for purposes of joins
            List<string> newtables = new List<string>();
            foreach (string mtable in mtables)
            {
                foreach (MeasureTable mt in measureTablesList)
                {
                    if (mt.TableName == mtable && mt.InheritTable != null && mt.InheritTable.Length > 0)
                    {
                        if (!mtables.Contains(mt.InheritTable) && !newtables.Contains(mt.InheritTable))
                            newtables.Add(mt.InheritTable);
                    }
                }
            }
            mtables.AddRange(newtables);
            newtables.Clear();
            foreach (string dtable in dimtables)
            {
                foreach (DimensionTable dt in dimensionTablesList)
                {
                    if (dt.TableName == dtable && dt.InheritTable != null && dt.InheritTable.Length > 0)
                    {
                        if (!dimtables.Contains(dt.InheritTable) && !newtables.Contains(dt.InheritTable))
                            newtables.Add(dt.InheritTable);
                    }
                }
            }
            dimtables.AddRange(newtables);
            // Now check joins
            for (int i = 0; i < Joins.Rows.Count; i++)
            {
                DataRow dr = Joins.Rows[i];
                string t1 = (string)dr["Table1"];
                string t2 = (string)dr["Table2"];
                if (((mptables.Contains(t1) && dimptables.Contains(t2)) || (mptables.Contains(t2) && dimptables.Contains(t1))) ||
                    (mtables.Contains(t1) && dimptables.Contains(t2)) || (mtables.Contains(t2) && dimptables.Contains(t1)))
                {
                    if (mptables.Contains(t1))
                        results.Add(mtables[mptables.IndexOf(t1)]);
                    else if (mptables.Contains(t2))
                        results.Add(mtables[mptables.IndexOf(t2)]);
                    else if (mtables.Contains(t1)) results.Add(t1);
                    else if (mtables.Contains(t2)) results.Add(t2);
                }
            }
            // Return measure tables that join
            return (results);
        }

        // Check that a measure table joins with a given dimension
        public bool checkDimensionJoin(string measureTable, string dimension)
        {
            // Get the measure table in question
            MeasureTable mt = null;
            foreach (MeasureTable tmt in measureTablesList)
            {
                if (tmt.TableName == measureTable)
                {
                    mt = tmt;
                    break;
                }
            }
            if (mt == null) return (false);
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.DimensionName == dimension)
                {
                    int result = joinsView.Find(new object[] {measureTable, dt.TableName});
                    if (result >= 0)
                        return true;
                    result = joinsView.Find(new object[] {dt.TableName, measureTable});
                    if (result >= 0)
                        return true;
                    result = joinsView.Find(new object[] {mt.InheritTable, dt.TableName});
                    if (result >= 0)
                        return true;
                    result = joinsView.Find(new object[] {dt.TableName, mt.InheritTable});
                    if (result >= 0)
                        return true;
                }
            }
            return (false);
        }

        // Check that a measure table joins with a given dimension table
        public bool checkDimensionTableJoin(string measureTable, string dimensionTable)
        {
            int result = joinsView.Find(new object[] { measureTable, dimensionTable });
            if (result >= 0)
                return true;
            result = joinsView.Find(new object[] { dimensionTable, measureTable });
            if (result >= 0)
                return true;
            return (false);
        }

        // If a measure table joins with a given dimension table (or a table from which the dimension table inherits), 
        // return the right prefix for the columns
        // Otherwise return null
        public string prefixDimensionTableJoin(string measureTable, DimensionTable dt)
        {
            if (dt.VirtualMeasuresInheritTable == null)
            {
                if (checkDimensionTableJoin(measureTable, dt.TableName)) return "";
                else return null;
            }
            else
            {
                int ix = findDimensionTableIndex(dt.VirtualMeasuresInheritTable);
                if (ix >= 0)
                {
                    DimensionTable dtNew = dimensionTablesList[ix];
                    string p = prefixDimensionTableJoin(measureTable, dtNew);
                    return (p == null ? null : dt.VirtualMeasuresPrefix + p);
                }
            }
            return null;
        }

        // Get a list of all the dimensions that join to this measure
        public List<String> getJoinedDimensions(string measure)
        {
            List<String> results = new List<String>();
            List<String>[] measureTables = getMeasureTables(measure);
            if (measureTables == null)
            {
                Console.Out.WriteLine("test");
                measureTables = getMeasureTables(measure);
            }
            List<String> mtables = measureTables[1];
            List<String> dtables = new List<String>();
            for (int i = 0; i < dimensionTablesList.Count; i++)
            {
                dtables.Add(dimensionTablesList[i].TableName);
            }
            // Now check joins
            for (int i = 0; i < Joins.Rows.Count; i++)
            {
                DataRow dr = Joins.Rows[i];
                string t1 = (string)dr["Table1"];
                string t2 = (string)dr["Table2"];
                if ((mtables.Contains(t1) && dtables.Contains(t2)) || (mtables.Contains(t2) && dtables.Contains(t1)))
                {
                    String dname = null;
                    if (mtables.Contains(t1))
                        dname = dimensionTablesList[dtables.IndexOf(t2)].DimensionName;
                    else
                        dname = dimensionTablesList[dtables.IndexOf(t1)].DimensionName;
                    if (!results.Contains(dname)) results.Add(dname);
                }
            }
            // Return measure tables that join
            return (results);
        }

        public class Listcomparer : IComparer
        {
            private int column;
            private int column2;
            public Listcomparer(int column)
            {
                this.column = column;
                this.column2 = -1;
            }
            public Listcomparer(int column, int column2)
            {
                this.column = column;
                this.column2 = column2;
            }
            public int Compare(Object a, Object b)
            {
                ListViewItem lvia = (ListViewItem)a;
                ListViewItem lvib = (ListViewItem)b;
                if (column2 < 0)
                {
                    return (String.Compare(lvia.SubItems[column].Text, lvib.SubItems[column].Text));
                }
                else
                {
                    int result = String.Compare(lvia.SubItems[column].Text, lvib.SubItems[column].Text);
                    if (result == 0)
                    {
                        result = String.Compare(lvia.SubItems[column2].Text, lvib.SubItems[column2].Text);
                    }
                    return (result);
                }
            }
        }

        private void copyTableMenuItem_Click(object sender, System.EventArgs e)
        {
            if (dimensionTablesList == null && measureTablesList == null)
            {
                MessageBox.Show("No Tables Created", "Error");
                return;
            }
            CopyTable ct = new CopyTable(measureTablesList, dimensionTablesList);
            if (ct.ShowDialog() == DialogResult.OK)
            {
                MeasureTable foundmt = null;
                foreach (MeasureTable mt in measureTablesList)
                {
                    if (mt.TableName == ct.copyTableBox.Text)
                    {
                        foundmt = mt;
                        break;
                    }
                }
                DimensionTable founddt = null;
                foreach (DimensionTable dt in dimensionTablesList)
                {
                    if (dt.TableName == ct.copyTableBox.Text)
                    {
                        founddt = dt;
                        break;
                    }
                }
                if (foundmt != null)
                {
                    MeasureTable newmt = new MeasureTable();
                    newmt.TableName = ct.newTableName.Text;
                    newmt.TableSource = foundmt.TableSource.clone();
                    newmt.Type = foundmt.Type;
                    newmt.OpaqueView = foundmt.OpaqueView;
                    newmt.Cardinality = foundmt.Cardinality;
                    newmt.TableSource.Connection = foundmt.TableSource.Connection;
                    addNewMeasureTable(newmt);

                    bool addMeasureColumns = (ct.prefixBox.Text != null && ct.prefixBox.Text != "");
                    ArrayList rowList = new ArrayList();
                    ArrayList mcrowList = new ArrayList();
                    DataRow[] mcmRows = measureColumnMappings.Select("TableName='" + foundmt.TableName + "'");
                    if (mcmRows != null && mcmRows.Length > 0)
                    {
                        for (int mcmCount = 0; mcmCount < mcmRows.Length; mcmCount++)
                        {
                            DataRow dr = mcmRows[mcmCount];
                            if (addMeasureColumns)
                            {
                                //Add new measure column since the column name is different
                                List<DataRow> result = Util.selectRows(measureColumnsTable, new string[][] {
                                    new string [] {"ColumnName", dr["ColumnName"].ToString()}});
                                DataRow mcdr = result[0];
                                DataRow newmcdr = measureColumnsTable.NewRow();
                                newmcdr["ColumnName"] = ct.prefixBox.Text + dr["ColumnName"];
                                newmcdr["Description"] = mcdr["Description"];
                                newmcdr["DataType"] = mcdr["DataType"];
                                newmcdr["PerformanceCategory"] = mcdr["PerformanceCategory"];
                                newmcdr["Key"] = mcdr["Key"];
                                newmcdr["Format"] = mcdr["Format"];
                                newmcdr["AggregationRule"] = mcdr["AggregationRule"];
                                mcrowList.Add(newmcdr);
                            }
                            DataRow newdr = measureColumnMappings.NewRow();
                            newdr["ColumnName"] = ct.prefixBox.Text + dr["ColumnName"];
                            newdr["TableName"] = ct.newTableName.Text;
                            newdr["PhysicalName"] = dr["PhysicalName"];
                            newdr["Qualify"] = dr["Qualify"];
                            rowList.Add(newdr);
                        }
                    }
                    foreach (DataRow dr in mcrowList) measureColumnsTable.Rows.Add(dr);
                    updateMeasureColumnNames();
                    foreach (DataRow dr in rowList) measureColumnMappings.Rows.Add(dr);
                }
                else if (founddt != null)
                {
                    DimensionTable newdt = new DimensionTable();
                    newdt.TableName = ct.newTableName.Text;
                    newdt.TableSource = founddt.TableSource.clone();
                    newdt.Type = founddt.Type;
                    newdt.Level = founddt.Level;
                    newdt.DimensionName = founddt.DimensionName;
                    newdt.OpaqueView = founddt.OpaqueView;
                    newdt.TableSource.Connection = founddt.TableSource.Connection;
                    addDimensionTable(newdt, null, false, -1);

                    bool addDimColumns = (ct.prefixBox.Text != null && ct.prefixBox.Text != "");
                    ArrayList rowList = new ArrayList();
                    ArrayList dcrowList = new ArrayList();
                    DataRow[] dcmRows = dimensionColumnMappings.Select("TableName='" + founddt.TableName + "'");
                    if (dcmRows != null && dcmRows.Length > 0)
                    {
                        for (int dcmCount = 0; dcmCount < dcmRows.Length; dcmCount++)
                        {
                            DataRow dr = dcmRows[dcmCount];
                            if (addDimColumns)
                            {
                                //Add new measure column since the column name is different
                                List<DataRow> result = Util.selectRows(dimensionColumns, new string[][] {
                                    new string[] {"ColumnName", dr["ColumnName"] .ToString()}});
                                DataRow dcdr = result[0];
                                DataRow newdcdr = dimensionColumns.NewRow();
                                newdcdr["DimensionName"] = dcdr["DimensionName"];
                                newdcdr["ColumnName"] = ct.prefixBox.Text + dr["ColumnName"];
                                newdcdr["DataType"] = dcdr["DataType"];
                                newdcdr["Key"] = dcdr["Key"];
                                newdcdr["Format"] = dcdr["Format"];
                                newdcdr["Maskable"] = dcdr["Maskable"];
                                dcrowList.Add(newdcdr);
                            }
                            DataRow newdr = dimensionColumnMappings.NewRow();
                            newdr["ColumnName"] = ct.prefixBox.Text + dr["ColumnName"];
                            newdr["TableName"] = ct.newTableName.Text;
                            newdr["PhysicalName"] = dr["PhysicalName"];
                            newdr["Qualify"] = dr["Qualify"];
                            rowList.Add(newdr);
                        }
                    }
                    foreach (DataRow dr in dcrowList) dimensionColumns.Rows.Add(dr);
                    updateDimensionColumnNames();
                    foreach (DataRow dr in rowList) dimensionColumnMappings.Rows.Add(dr);
                }
            }
        }

        public void replaceColumns(string findColumn, string replaceColumn, bool match)
        {
            foreach (MeasureTable mt in measureTablesList)
            {
                foreach (MeasureColumn mc in mt.MeasureColumns)
                {
                    string s = mc.ColumnName;
                    if (match)
                    {
                        if (s != null && s == findColumn) 
                            mc.ColumnName = replaceColumn;
                    }
                    else
                    {
                        if (s != null && s.IndexOf(findColumn) >= 0)
                            mc.ColumnName = mc.ColumnName.Replace(findColumn, replaceColumn);
                    }
                }
            }
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                string s = (string)dr["ColumnName"];
                string dimName = (string)dr["DimensionName"];
                if (s != null && ((match && s == findColumn) || (!match && s.IndexOf(findColumn) >= 0)))
                {
                    if (match)
                    {
                        if ((string)dr["ColumnName"] == s)
                            dr["ColumnName"] = s;
                    }
                    else
                    {
                        if (((string)dr["ColumnName"]).IndexOf(s) >= 0)
                            dr["ColumnName"] = ((string)dr["ColumnName"]).Replace(s, replaceColumn);
                    }
                    foreach (DataRow dr2 in dimensionColumnMappings.Rows)
                    {
                        if (s != null && ((match && s == findColumn) || (!match && s.IndexOf(findColumn) >= 0)))
                        {
                            if (match)
                            {
                                if ((string)dr2["ColumnName"] == s)
                                    dr2["ColumnName"] = s;
                            }
                            else
                            {
                                if (((string)dr2["ColumnName"]).IndexOf(s) >= 0)
                                    dr2["ColumnName"] = ((string)dr2["ColumnName"]).Replace(s, replaceColumn);
                            }
                        }
                    }
                }
            }
            foreach (RepositoryModule rm in modules)
            {
                rm.replaceColumn(findColumn, replaceColumn, match);
            }
        }

        private void searchMenuItem_Click(object sender, System.EventArgs e)
        {
            SearchForm sf = new SearchForm(measureColumnsTable, dimensionColumns, true);
            sf.ShowDialog();
            if (sf.DialogResult == DialogResult.OK)
            {
                replaceColumns(sf.findColumn, sf.replaceColumn, sf.match);
            }
        }

        private void findMenuItem_Click(object sender, System.EventArgs e)
        {
            SearchForm sf = new SearchForm(measureColumnsTable, dimensionColumns, false);
            sf.ShowDialog();
            List<string> results = new List<string>();
            if (sf.DialogResult == DialogResult.OK)
            {
                foreach (MeasureTable mt in measureTablesList)
                {
                    foreach (MeasureColumn mc in mt.MeasureColumns)
                    {
                        string s = mc.ColumnName;
                        if (sf.match)
                        {
                            if (s != null && s == sf.findColumn) results.Add("Measure Column: " + mt.TableName);
                        }
                        else
                        {
                            if (s != null && s.IndexOf(sf.findColumn) >= 0) results.Add("Measure Column: " + mt.TableName + ", Full Measure Column Name: " + s);
                        }
                    }
                }
                foreach (DataRow dr in dimensionColumns.Rows)
                {
                    string s = (string)dr["ColumnName"];
                    string dimName = (string)dr["DimensionName"];
                    if (s != null && ((sf.match && s == sf.findColumn) || (!sf.match && s.IndexOf(sf.findColumn) >= 0)))
                    {
                        results.Add("Dimension Column: " + dimName + (sf.match ? "" : ", Full Dimension Column Name: " + s));
                        foreach (DimensionTable dt in dimensionTablesList)
                        {
                            if (dt.DimensionName == dimName)
                            {
                                foreach (DimensionColumn dc in dt.DimensionColumns)
                                {
                                    if (dc.ColumnName == s)
                                    {
                                        results.Add("            Dimension Table: " + dt.TableName);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (RepositoryModule rm in modules)
                {
                    rm.findColumn(results, sf.findColumn, sf.match);
                }
            }
            if (results.Count > 0)
            {
                FindResults fr = new FindResults(sf.findColumn, results);
                fr.ShowDialog();
            }
            else
            {
                MessageBox.Show("No Results Found!", "Find Column Name");
            }
        }
        private bool processingMCChange = false;
        public bool genNewMeasureList = true;

        private void measureColumnsTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            genNewMeasureList = true;
            if ((e.Column.ColumnName == "PhysicalName") || (e.Column.ColumnName == "TableName") ||
                (e.Column.ColumnName == "AggregationRule") || (e.Column.ColumnName == "Key") ||
                (e.Column.ColumnName == "Qualify")) return;
            if (!processingMCChange)
            {
                processingMCChange = true;
                for (int i = 0; i < measureColumnsTable.Rows.Count; i++)
                {
                    if ((measureColumnsTable.Rows[i] != e.Row)
                        && ((string)measureColumnsTable.Rows[i]["ColumnName"] == (string)e.Row["ColumnName"]))
                    {
                        if (e.ProposedValue == null)
                            measureColumnsTable.Rows[i][e.Column] = e.Column.DefaultValue;
                        else
                            measureColumnsTable.Rows[i][e.Column] = e.ProposedValue;
                    }
                }
                processingMCChange = false;
            }
        }

        private void exportMeasureListMenuItem_Click(object sender, System.EventArgs e)
        {
            if (exportSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(exportSaveFileDialog.FileName, false);
                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to Open File");
                    return;
                }
                writer.Write(MeasureColumn.outputDocTitleString('\t'));
                Hashtable ht = new Hashtable();
                // Whether this is a performance measure or not
                writer.Write("\tPerf");
                // Get list of dimensions
                ArrayList dims = new ArrayList();
                foreach (DimensionTable dt in dimensionTablesList)
                {
                    if (!dims.Contains(dt.DimensionName))
                    {
                        dims.Add(dt.DimensionName);
                        writer.Write("\t" + dt.DimensionName);
                    }
                }
                writer.WriteLine();
                foreach (MeasureTable mt in measureTablesList)
                {
                    foreach (MeasureColumn mc in mt.MeasureColumns)
                    {
                        if (!ht.ContainsKey(mc.ColumnName))
                        {
                            writer.Write(mc.outputDocString('\t'));
                            bool found = false;
                            for (int i = 0; i < perf.selectedMeasures.Items.Count; i++)
                            {
                                if (((ListViewItem)perf.selectedMeasures.Items[i]).SubItems[0].Text == mc.ColumnName)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            writer.Write(found ? "\tX" : "\t");
                            List<String> list = getJoinedDimensions(mc.ColumnName);
                            for (int i = 0; i < dims.Count; i++)
                            {
                                if (list.Contains((string)dims[i]))
                                    writer.Write("\tX");
                                else
                                    writer.Write("\t");
                            }
                            writer.WriteLine();
                            ht.Add(mc.ColumnName, null);
                        }
                    }
                }
                writer.WriteLine();
                writer.WriteLine("All Measures - Including Derived and Virtual");
                List<string>[] mlist = getMeasureList(null);
                foreach (string mname in mlist[0])
                {
                    writer.WriteLine(mname);
                }
                writer.Close();
            }
        }

        private void dimensionColumnListMenuItem_Click(object sender, System.EventArgs e)
        {
            if (exportSaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(exportSaveFileDialog.FileName, false);
                }
                catch (Exception)
                {
                    MessageBox.Show("Unable to Open File");
                    return;
                }
                writer.WriteLine(DimensionColumn.outputDocTitleString('\t'));
                Hashtable ht = new Hashtable();
                foreach (DimensionTable dt in dimensionTablesList)
                {
                    foreach (DimensionColumn dc in dt.DimensionColumns)
                    {
                        if (!ht.ContainsKey(dt.DimensionName + "." + dc.ColumnName))
                        {
                            writer.WriteLine(dc.outputDocString(dt, '\t'));
                            ht.Add(dt.DimensionName + "." + dc.ColumnName, null);
                        }
                    }
                }
                writer.Close();
            }
        }


        private void validateTargetsMenuItem_Click(object sender, System.EventArgs e)
        {
            foreach (DataRow dr in measureColumnsTable.Rows)
            {
                if (((string)dr["Minimum"] != "-Infinity") && ((string)dr["MinTarget"] == "-Infinity"))
                {
                    dr["MinTarget"] = (string)dr["Minimum"];
                }
                if (((string)dr["Maximum"] != "Infinity") && ((string)dr["MaxTarget"] == "Infinity"))
                {
                    dr["MaxTarget"] = (string)dr["Maximum"];
                }
            }
        }

        private void defineTableSource_Click(object sender, EventArgs e)
        {
            int index = findDimensionTableIndex(mainTreeNode.Text);
            if (index < 0) return;
            DimensionTable dt = dimensionTablesList[index];
            TableSource ts = dt.TableSource;
            DefineTableSource dts = new DefineTableSource(this, appconfig, connection.dbDatabase.Text, usersAndGroups.getAdminUsername(), usersAndGroups.getAdminPassword(), ts);
            DialogResult dr = dts.ShowDialog();
            if (dr == DialogResult.OK)
            {
                dt.TableSource = dts.ts;
                dimensionTablePName.Text = dt.TableSource.ToString();
            }
        }

        private void defineMeasureTableSource_Click(object sender, EventArgs e)
        {
            int index = findMeasureTableIndex(mainTreeNode.Text);
            if (index < 0) return;
            MeasureTable mt = measureTablesList[index];
            TableSource ts = mt.TableSource;
            DefineTableSource dts = new DefineTableSource(this, appconfig, connection.dbDatabase.Text, usersAndGroups.getAdminUsername(), usersAndGroups.getAdminPassword(), ts);
            DialogResult dr = dts.ShowDialog();
            if (dr == DialogResult.OK)
            {
                mt.TableSource = dts.ts;
                measureTablePName.Text = mt.TableSource.ToString();
            }
        }

        private void defineDSubstitutions_Click(object sender, EventArgs e)
        {
            int index = findDimensionTableIndex(mainTreeNode.Text);
            if (index < 0) return;
            DimensionTable dt = dimensionTablesList[index];
            DefineSubstitutions ds = null;
            if (dt.ColumnSubstitutions != null)
                ds = new DefineSubstitutions(this, new System.Collections.Generic.List<ColumnSubstitution>(dt.ColumnSubstitutions));
            else
                ds = new DefineSubstitutions(this, null);
            DialogResult dr = ds.ShowDialog();
            if (dr == DialogResult.OK)
            {
                dt.ColumnSubstitutions = ds.subList.ToArray();
            }
        }

        private void defineMSubstitutions_Click(object sender, EventArgs e)
        {
            int index = findMeasureTableIndex(mainTreeNode.Text);
            if (index < 0) return;
            MeasureTable mt = measureTablesList[index];
            DefineSubstitutions ds = null;
            if (mt.ColumnSubstitutions != null)
                ds = new DefineSubstitutions(this, new System.Collections.Generic.List<ColumnSubstitution>(mt.ColumnSubstitutions));
            else
                ds = new DefineSubstitutions(this, null);
            DialogResult dr = ds.ShowDialog();
            if (dr == DialogResult.OK)
            {
                mt.ColumnSubstitutions = ds.subList.ToArray();
            }
        }

        private void setMeasurePhysicalTableOptions()
        {
            if (measureNormal.Checked || measureDerived.Checked)
            {
                defineMeasureTableSource.Enabled = true;
                measureTablePName.ReadOnly = true;
                measureModelBox.Enabled = false;
            }
            else
            {
                defineMeasureTableSource.Enabled = false;
                measureTablePName.ReadOnly = false;
                measureModelBox.Enabled = measureView.Checked;
            }
        }

        private void setDimPhysicalTableOptions()
        {
            if (dimNormal.Checked)
            {
                defineMeasureTableSource.Enabled = true;
                dimensionTablePName.ReadOnly = true;
                dimModelBox.Enabled = false;
            }
            else
            {
                defineMeasureTableSource.Enabled = false;
                dimensionTablePName.ReadOnly = false;
                dimModelBox.Enabled = true;
            }
        }
        private void measureView_CheckedChanged(object sender, EventArgs e)
        {
            setMeasurePhysicalTableOptions();
        }

        private void measureNormal_CheckedChanged(object sender, EventArgs e)
        {
            setMeasurePhysicalTableOptions();
        }

        private void measureDerived_CheckedChanged(object sender, EventArgs e)
        {
            if (measureDerived.Checked)
            {
                aggregationRuleColumn.Visible = false;
                DimensionRules.Visible = false;
                DisplayFunction.Visible = false;
                keyDataGridViewCheckBoxColumn.Visible = false;
            }
            else
            {
                aggregationRuleColumn.Visible = true;
                DimensionRules.Visible = true;
                DisplayFunction.Visible = true;
                keyDataGridViewCheckBoxColumn.Visible = true;
            }
            setMeasurePhysicalTableOptions();
        }

        private void dimNormal_CheckedChanged(object sender, EventArgs e)
        {
            setDimPhysicalTableOptions();
        }

        private void dimView_CheckedChanged(object sender, EventArgs e)
        {
            setDimPhysicalTableOptions();
        }

        private void measureDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == measureDataGridView.Columns["aggregationRuleColumn"].Index)
            {
                DataGridViewRow dr = measureDataGridView.CurrentRow;
                if (dr.Cells["DimensionRules"].Value == System.DBNull.Value)
                    dr.Cells["DimensionRules"].Value = "";
                AggRule ar = new AggRule(this, (string)dr.Cells["aggregationRuleColumn"].Value, (string)dr.Cells["DimensionRules"].Value);
                DialogResult result = ar.ShowDialog();
                if (result == DialogResult.OK)
                {
                    dr.Cells["aggregationRuleColumn"].Value = ar.getBaseAggRule();
                    dr.Cells["DimensionRules"].Value = ar.getAggRuleString();
                }
            }
            else if (e.ColumnIndex == measureDataGridView.Columns["filtersColumn"].Index)
            {
                DataGridViewRow dr = measureDataGridView.CurrentRow;
                Object o = dr.Cells["filtersColumn"].Value;
                FilteredMeasures fm = new FilteredMeasures(this,
                    (o == System.DBNull.Value ? null : (FilteredMeasureDefinition)o));
                DialogResult result = fm.ShowDialog();
                if (result == DialogResult.OK)
                {
                    FilteredMeasureDefinition fmd = fm.getFilteredMeasureDefinition();
                    if (fmd == null)
                        dr.Cells["filtersColumn"].Value = System.DBNull.Value;
                    dr.Cells["filtersColumn"].Value = fmd;
                }
            }
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private bool repositoryHasBeenChanged(Repository r)
        {
            bool changes = false;
            if (r != null && emptyRepository != null && r.CompareTo(emptyRepository))
            {
                changes = false;
            }
            else if (r != null)
            {
                if (savedRepository == null)
                {
                    changes = true;
                }
                else
                {
                    // compare them
                    if (!r.CompareTo(savedRepository))
                    {
                        changes = true;
                    }
                }
            }
            return changes;
        }

        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        private bool exitTasks()
        {
            // check that the repository has not changed... since we don't have dirty bits, we compare the saved and the current
            Repository r = getRepository();
            bool changes = repositoryHasBeenChanged(r);

            if (changes)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = reportMessage(null, null, "You have unsaved changes to your repository, do you really want to exit?\nNote that this change could be a version update.", DialogResult.Yes, "Unsaved Repository Changes", buttons, MessageBoxIcon.None);
                if (result == DialogResult.Yes)
                {
                    changes = false;
                }
            }
            if (!changes)
            {
                saveConfig(); // Save configuration file
                return true;
            }
            else
                return false;
        }

        private void joinDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridViewCell dgvc = joinDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
            dgvc.Value = null;
        }

        private void measureDataGridView_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            genNewMeasureList = true;
            dimMeasureLists.Clear();
        }

        private void measureDataGridView_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            genNewMeasureList = true;
            dimMeasureLists.Clear();
        }

        private void saveAsMenuItem_Click(object sender, EventArgs e)
        {
            implicitSaveModules();
            Repository r = getRepository();
            saveRepositoryDialog.FileName = this.currentFilename;
            DialogResult result = saveRepositoryDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                currentFilename = saveRepositoryDialog.FileName;
                TextWriter writer = new StreamWriter(currentFilename + ".temp");
                XmlSerializer serializer = new XmlSerializer(typeof(Repository));
                serializer.Serialize(writer, r);
                writer.Close();
                File.Copy(currentFilename + ".temp", currentFilename, true);
                File.Delete(currentFilename + ".temp");

                savedRepository = (Repository)r.Clone(); // new saved repository
                this.Text = defaultWindowTitle + ": " + mruManager.GetDisplayName(currentFilename, 80);
                mruManager.Add(currentFilename);
                appconfig.MRUItems = (string[])(mruManager.MRUList.ToArray(typeof(string)));
                fileHasBeenOpened();

            }
        }

        private void implicitSaveModules()
        {
            implicitSaveDimensionTable();
            implicitSaveMeasureTable();
            foreach (RepositoryModule rm in modules)
                rm.implicitSave();

        }
        private void implicitSaveDimensionTable()
        {
            updateDimensionTable();
        }
        private void implicitSaveMeasureTable()
        {
            updateMeasureTable();
        }

        private void dimensionsTabPage_Leave(object sender, EventArgs e)
        {
            implicitSaveDimensionTable();
        }

        private void measuresTabPage_Leave(object sender, EventArgs e)
        {
            measureDataGridView_Leave(sender, e);
            implicitSaveMeasureTable();
        }

        private bool resetMeasureColumns = true;

        public void updateMeasureColumnNames()
        {
            measureDataGridView.Enabled = false;
            measureColumnMappingSource.SuspendBinding();
            measureComboBoxColumn.Items.Clear();
            List<string> colList = new List<string>(measureColumnsTable.Rows.Count);
            foreach (DataRow dr in measureColumnsTable.Rows)
            {
                if (!((bool)dr["Derived"]))
                    colList.Add((string)dr["ColumnName"]);
            }
            colList.Sort();
            measureComboBoxColumn.Items.AddRange(colList.ToArray());
            measureColumnMappingSource.ResumeBinding();
            resetMeasureColumns = false;
            measureDataGridView.Enabled = true;
        }

        public void updateDimensionColumnNames()
        {
            dimDataGridView.Enabled = false;
            dimensionColumnMappingSource.SuspendBinding();
            columnNameComboBox.Items.Clear();
            List<string> colList = new List<string>(dimensionColumns.Rows.Count);
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                colList.Add((string)dr["ColumnName"]);
            }
            colList.Sort();
            columnNameComboBox.Items.AddRange(colList.ToArray());
            dimensionColumnMappingSource.ResumeBinding();
            dimDataGridView.Enabled = true;
        }

        private void measureDataGridView_Leave(object sender, EventArgs e)
        {
            if (resetMeasureColumns)
            {
                updateMeasureColumnNames();
            }
        }

        public void renameDimension(string currentName, string newName, bool updateStagingTables)
        {
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.DimensionName == currentName)
                    dt.DimensionName = newName;
            }
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                if ((string)dr["DimensionName"] == currentName)
                    dr["DimensionName"] = newName;
            }
            foreach (Dimension d in dimensionsList)
            {
                if (d.Name == currentName)
                    d.Name = newName;
            }
            foreach (TreeNode tn in dimensionTablesNode.Nodes)
            {
                if (tn.Text == currentName)
                {
                    tn.Text = newName;
                }
            }
            if (updateStagingTables)
            {
                foreach (StagingTable st in stagingTableMod.getAllStagingTables())
                {
                    for (int i = 0; i < st.Levels.Length; i++)
                    {
                        if (st.Levels == null)
                            continue;
                        if (st.Levels[i][0] == currentName)
                            st.Levels[i][0] = newName;
                    }
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes == null)
                            continue;
                        for (int j = 0; j < sc.TargetTypes.Length; j++)
                        {
                            if (sc.TargetTypes[j] == currentName)
                                sc.TargetTypes[j] = newName;
                        }
                    }
                }
            }
        }

        private void renameDimensionTable(string currentName, string newName)
        {
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.TableName == currentName)
                    dt.TableName = newName;
            }
            foreach (DataRow dr in dimensionColumnMappings.Rows)
            {
                if ((string)dr["TableName"] == currentName)
                    dr["TableName"] = newName;
            }
            if (Joins != null && Joins.Rows != null)
            {
                for (int i = 0; i < Joins.Rows.Count; i++)
                {
                    DataRow dr = Joins.Rows[i];
                    string table1 = (string)dr["table1"];
                    string table2 = (string)dr["table2"];
                    string condition = "";
                    if (dr["JoinCondition"] != System.DBNull.Value)
                    {
                        condition = (string)dr["JoinCondition"];
                    }
                    if (table1.Equals(currentName)) dr["table1"] = newName;
                    if (table2.Equals(currentName)) dr["table2"] = newName;
                    string currentPref = '"' + currentName + '"' + '.';
                    string newPref = '"' + newName + '"' + '.';
                    if (condition.Contains(currentPref)) dr["JoinCondition"] = condition.Replace(currentPref, newPref);
                }
            }
        }

        private void renameMeasureTable(string currentName, string newName)
        {
            foreach (MeasureTable mt in measureTablesList)
            {
                if (mt.TableName == currentName)
                    mt.TableName = newName;
            }
            foreach (DataRow dr in measureColumnMappings.Rows)
            {
                if ((string)dr["TableName"] == currentName)
                    dr["TableName"] = newName;
            }
            if (Joins != null && Joins.Rows != null)
            {
                for (int i = 0; i < Joins.Rows.Count; i++)
                {
                    DataRow dr = Joins.Rows[i];
                    string table1 = (string)dr["table1"];
                    if (table1.Equals(currentName)) dr["table1"] = newName;
                    if (dr["JoinCondition"] != System.DBNull.Value)
                    {
                        string condition = (string)dr["JoinCondition"];
                        string currentPref = '"' + currentName + '"' + '.';
                        if (condition.Contains(currentPref))
                        {
                            string newPref = '"' + newName + '"' + '.';
                            dr["JoinCondition"] = condition.Replace(currentPref, newPref);
                        }
                    }
                }
            }
        }

        public void deleteDimension(string dimName)
        {
            foreach (Dimension d in dimensionsList)
            {
                if (d.Name == dimName)
                {
                    dimensionsList.Remove(d);
                    break;
                }
            }
            List<DimensionTable> toRemove = new List<DimensionTable>(dimensionTablesList.Count);
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.DimensionName == dimName)
                    toRemove.Add(dt);
            }
            foreach (DimensionTable dt in toRemove)
                dimensionTablesList.Remove(dt);
            List<DataRow> drlist = new List<DataRow>();
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                if ((string)dr["DimensionName"] == dimName)
                    drlist.Add(dr);
            }
            foreach (DataRow dr in drlist)
                dimensionColumns.Rows.Remove(dr);
            foreach (TreeNode tn in dimensionTablesNode.Nodes)
                if (tn != null && tn.Text == dimName) tn.Remove();
        }

        private void mainTree_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Label == null) return;
            if (e.Node.Parent == dimensionTablesNode)
            {
                dimensionColumnSource.SuspendBinding();
                renameDimension(e.Node.Text, e.Label, true);
                e.Node.Text = e.Label;
                dimensionColumnDimensionName.DefaultValue = e.Label;
                dimensionColumnSource.Filter = "DimensionName='" + e.Label + "'";
                dimensionColumnSource.ResumeBinding();
            }
            else if (e.Node.Parent != null && e.Node.Parent.Parent == dimensionTablesNode)
            {
                dimensionColumnMappingSource.SuspendBinding();
                renameDimensionTable(e.Node.Text, e.Label);
                e.Node.Text = e.Label;
                dimensionColumnTableName.DefaultValue = e.Label;
                dimensionColumnMappingSource.Filter = "TableName='" + e.Label + "'";
                dimensionColumnMappingSource.ResumeBinding();
            }
            else if (e.Node.Parent.Parent != null && e.Node.Parent.Parent == measureTablesNode)
            {
                measureColumnMappingSource.SuspendBinding();
                renameMeasureTable(e.Node.Text, e.Label);
                e.Node.Text = e.Label;
                measureTableName.DefaultValue = e.Label;
                measureColumnMappingSource.Filter = "TableName='" + e.Label + "'";
                measureColumnMappingSource.ResumeBinding();
            }
            foreach (RepositoryModule rm in modules)
            {
                if (typeof(RenameNode).IsInstanceOfType(rm))
                    ((RenameNode)rm).rename(e);
            }
        }

        public int findDimensionTableIndex(string name)
        {
            int index = 0;
            for (; index < dimensionTablesList.Count; index++)
            {
                if (dimensionTablesList[index].TableName == name)
                    return (index);
            }
            return (-1);
        }

        public int findMeasureTableIndex(string name)
        {
            int index = 0;
            for (; index < measureTablesList.Count; index++)
            {
                if (measureTablesList[index].TableName == name)
                    return (index);
            }
            return (-1);
        }

        public void dimColChanged(object sender, DataColumnChangeEventArgs args)
        {
            if (args.Column.ColumnName == "ColumnName")
            {
                lastDimension = null;
            }
        }

        private void mainTree_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode sourceNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
            if (sourceNode != null)
            {
                Point pt = mainTree.PointToClient(new Point(e.X, e.Y));
                TreeNode targetNode = mainTree.GetNodeAt(pt);
                foreach (RepositoryModule rm in modules)
                {
                    if (typeof(DropModule).IsInstanceOfType(rm) && rm.ModuleName == getMainNode(targetNode).Text)
                    {
                        ((DropModule)rm).doDrop(sourceNode, targetNode);
                    }
                }
                if (sourceNode != null && sourceNode.Parent != null && sourceNode.Parent.Parent != null && sourceNode.Parent.Parent.Text == "Dimensions")
                    this.doDrop(sourceNode, targetNode);
            }
        }

        private void mainTree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode tn = (TreeNode)e.Item;
            foreach (RepositoryModule rm in modules)
                if (typeof(DropModule).IsInstanceOfType(rm))
                {
                    DragDropEffects effects = ((DropModule)rm).startDrag(tn);
                    if (effects != DragDropEffects.None)
                        mainTree.DoDragDrop(e.Item, effects);                    
                }
            if (tn != null && tn.Parent != null && tn.Parent.Parent != null && tn.Parent.Parent.Text == "Dimensions")
            {
                DragDropEffects effects = this.startDrag(tn);
                if (effects != DragDropEffects.None)
                    mainTree.DoDragDrop(e.Item, effects);
            }
        }

        private void mainTree_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", true))
            {
                e.Effect = e.AllowedEffect;
            }
        }

        #region DropModule Members

        public void doDrop(TreeNode sourceNode, TreeNode targetNode)
        {
            if (getMainNode(sourceNode).Text == "Dimensions" && sourceNode.Parent.Text != "Dimensions")
            {
                List<DimensionTable> list = dimensionTablesList;
                int sourceNodeIndex = sourceNode.Parent.Nodes.IndexOf(sourceNode);
                int targetNodeIndex = 0;
                if (targetNode.Parent.Text != "Dimensions")
                {
                    targetNodeIndex = sourceNode.Parent.Nodes.IndexOf(targetNode);
                }
                if (sourceNodeIndex == targetNodeIndex)
                    return;
                if (sourceNode.Parent != targetNode.Parent)
                    return;
                list[sourceNodeIndex].amIBeingDragged = true;
                targetNode.Parent.Nodes.RemoveAt(sourceNodeIndex);
                DimensionTable sourceDimensionTable = list[sourceNodeIndex];
                list.RemoveAt(sourceNodeIndex);
                targetNode.Parent.Nodes.Insert(targetNodeIndex, sourceNode);
                list.Insert(targetNodeIndex, sourceDimensionTable);
                list[targetNodeIndex].amIBeingDragged = false;
                if (sourceNodeIndex >= targetNodeIndex)
                    mainTree.SelectedNode = targetNode.Parent.Nodes[sourceNodeIndex + 1];
                else if (sourceNodeIndex < targetNodeIndex)
                    mainTree.SelectedNode = targetNode.Parent.Nodes[sourceNodeIndex];
            }
        }

        public DragDropEffects startDrag(TreeNode tn)
        {
            if (tn != null && tn.Parent != null && tn.Parent.Parent != null && tn.Parent.Parent.Text == "Dimensions")
                return (DragDropEffects.Move);
            return (DragDropEffects.None);
        }

        #endregion

        private void measureDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            measureDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
        }

        private void measureMapGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            measureMapGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
        }

        private void dimDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dimDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
        }

        private void dimColMappingGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dimColMappingGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
        }

        private void measureDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                string newValue = (string)e.FormattedValue;
                for (int i = 0; i < measureDataGridView.Rows.Count; i++)
                {
                    string existingValue = (string)measureDataGridView.Rows[i].Cells[0].Value;
                    if (i != e.RowIndex && newValue == existingValue)
                    {
                        MessageBox.Show(this, "This measure already exists!", "Measures", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                        break;
                    }
                }
                if(!e.Cancel)
                {
                    resetMeasureColumns = true;
                    string oldValue = (string)measureDataGridView.Rows[e.RowIndex].Cells[0].Value;
                    foreach (DataRow dr in measureColumnMappings.Rows)
                    {
                        if ((string)dr["ColumnName"] == oldValue && (newValue != oldValue))
                        {
                            dr["ColumnName"] = newValue;
                            dr["AutoGenerated"] = false;
                        }
                    }
                }
            }
            else
                e.Cancel = false;
        }

        private void dimDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                string newValue = (string)e.FormattedValue;
                for (int i = 0; i < dimDataGridView.Rows.Count; i++)
                {
                    DataGridViewRow dr = dimDataGridView.Rows[i];
                    string tableName = dimensionColumnMappings.Columns["TableName"].DefaultValue.ToString();
                    string dimensionName = dimensionColumns.Columns["DimensionName"].DefaultValue.ToString();
                    string existingValue = (string)dimDataGridView.Rows[i].Cells[0].Value;
                    if (i != e.RowIndex && newValue == existingValue)
                    {
                        MessageBox.Show(this, "Dimension attribute (" + existingValue + ") in row " + e.RowIndex.ToString() + " also exists in row # " + i.ToString() + "!", "Dimension " + dimensionName + ", Dimension Table " + tableName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                        break;
                    }
                }
                if (!e.Cancel)
                {
                    Predicate<DimensionTable> pr = new Predicate<DimensionTable>(matchDimensionName);
                    List<DimensionTable> ldt = dimensionTablesList.FindAll(pr);
                    foreach (DimensionTable dt in ldt)
                    {
                        foreach (DataRow dr in Util.selectRows(dimensionColumnMappings, new string[][] { new string[] { "TableName", dt.TableName }, new string[] { "ColumnName", (string)dimDataGridView.Rows[e.RowIndex].Cells[0].Value } }))
                        {
                            if (dr["ColumnName"].ToString() != (string)e.FormattedValue)
                            {
                                    if (((bool)dr["AutoGenerated"]))
                                        dr["AutoGenerated"] = false;
                            }
                            dr["ColumnName"] = e.FormattedValue;
                        }
                    }
                }
            }
        }

        private bool matchDimensionName(DimensionTable dt)
        {
            return dt.DimensionName.Equals(dimensionColumns.Columns["DimensionName"].DefaultValue.ToString());
        }

        private void dimensionTableContextMenu_Popup(object sender, EventArgs e)
        {

        }

        private void dimensionTableMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void measureDataGridView_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (mainTreeNode != null && mainTreeNode == baseNode)
            {
                if (measureDataGridView.SelectedRows.Count == 1)
                {
                    viewMeasureGrainToolStripMenuItem.Visible = true;
                }
                else if (measureDataGridView.SelectedRows.Count > 1)
                {
                    viewMeasureGrainToolStripMenuItem.Visible = false;
                }
                else
                {
                    viewMeasureGrainToolStripMenuItem.Visible = false;
                }
            }
            else
            {
                viewMeasureGrainToolStripMenuItem.Visible = false;
            }
        }

        public MeasureTable findMeasureTableWithJoins(List<DimensionTable> dimTableList)
        {
            MeasureTable existingMeasureTable = null;
            foreach (MeasureTable mt in measureTablesList)
            {
                List<DimensionTable> joiningDimTables = getJoiningDimensionTables(mt);
                if (mt.InheritTable != null)
                {
                    int inheritTableIndex = findMeasureTableIndex(mt.InheritTable);
                    if (inheritTableIndex != -1)
                    {
                        MeasureTable parentTable = measureTablesList[inheritTableIndex];
                        joiningDimTables.AddRange(getJoiningDimensionTables(parentTable));
                    }
                }
                if (dimTableList.Count == joiningDimTables.Count)
                {
                    int matchingCount = 0;
                    foreach (DimensionTable dt in dimTableList)
                    {
                        foreach (DimensionTable jdt in joiningDimTables)
                        {
                            if (dt.Equals(jdt))
                            {
                                matchingCount++;
                                break;
                            }
                        }
                    }
                    if (matchingCount == dimTableList.Count)
                    {
                        existingMeasureTable = mt;
                        break;
                    }
                }
            }
            return existingMeasureTable;
        }

        public string getPhysicalSuffix()
        {
            if (connection.connectionList.Count > 0 && connection.connectionList[0].Type == "MonetDB")
                return "_SMI";
            return "$";
        }

        public bool containsRealTimeConnection(String name)
        {
            foreach (Performance_Optimizer_Administration.DatabaseConnection sdc in connection.connectionList)
            {
                if (sdc.Realtime == true && sdc.Name.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }

        public string getJoinCondition(DimensionTable dt, List<string> joinKeyColList, List<string> joinKeyColList2, MeasureTable mt, string dimName, int maxColLength)
        {
            StringBuilder joinConditionBuf = new StringBuilder();
            for (int i = 0; i < joinKeyColList.Count; i++)
            {
                string joinKeyCol = joinKeyColList[i];
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(mt.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                int index = joinKeyCol.IndexOf(getPhysicalSuffix());
                // if it already contains $ as a delimiter
                if (index >= 0 & index < joinKeyCol.Length - getPhysicalSuffix().Length)
                    joinConditionBuf.Append(joinKeyCol);
                else
                    joinConditionBuf.Append(ApplicationBuilder.getForeignKeyPhysicalName(ApplicationBuilder.getSQLGenType(this), dimName == null ? dt.DimensionName : dimName, joinKeyCol, ApplicationBuilder.PHYSICAL_SUFFIX, maxColLength, builderVersion));

                joinConditionBuf.Append("=\"");
                joinConditionBuf.Append(dt.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                string joinKeyColLogicalName = null;
                string joinKeyColPhysicalName = null;
                if (joinKeyColList2 != null && joinKeyColList2.Count > i)
                    joinKeyColLogicalName = joinKeyColList2[i];
                else
                    joinKeyColLogicalName = joinKeyCol;
                joinKeyColPhysicalName = findDimensionColumnPhysicalName(dt, joinKeyColLogicalName);
                if (joinKeyColPhysicalName == null)
                {
                    joinKeyColPhysicalName = Util.generatePhysicalName(joinKeyColLogicalName, this.builderVersion) + getPhysicalSuffix();
                }
                joinConditionBuf.Append(joinKeyColPhysicalName);
                if (i < (joinKeyColList.Count - 1))
                {
                    joinConditionBuf.Append(" AND ");
                }
            }
            return joinConditionBuf.ToString();
        }

        public string getJoinCondition(DimensionTable dt, List<string> joinKeyColList, DimensionTable dt2, bool degenerate)
        {
            StringBuilder joinConditionBuf = new StringBuilder();
            for (int i = 0; i < joinKeyColList.Count; i++)
            {
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(dt2.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                string joinKeyColLogicalName = joinKeyColList[i];
                string joinKeyColPhysicalName = findDimensionColumnPhysicalName(dt2, joinKeyColLogicalName);
                if (joinKeyColPhysicalName == null)
                {
                    joinKeyColPhysicalName = (degenerate ? Util.generatePhysicalName(dt2.DimensionName, this.builderVersion) + getPhysicalSuffix() : "") + Util.generatePhysicalName(joinKeyColLogicalName, this.builderVersion) + getPhysicalSuffix();
                }
                joinConditionBuf.Append(joinKeyColPhysicalName);
                joinConditionBuf.Append("=\"");
                joinConditionBuf.Append(dt.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                joinKeyColPhysicalName = findDimensionColumnPhysicalName(dt, joinKeyColLogicalName);
                if (joinKeyColPhysicalName == null)
                {
                    joinKeyColPhysicalName = Util.generatePhysicalName(joinKeyColLogicalName, this.builderVersion) + getPhysicalSuffix();
                }
                joinConditionBuf.Append(joinKeyColPhysicalName);
                if (i < (joinKeyColList.Count - 1))
                {
                    joinConditionBuf.Append(" AND ");
                }
            }
            return joinConditionBuf.ToString();
        }

        public string getJoinCondition(DimensionTable dt, List<string> joinKeyColList, List<string> joinKeyColList2, DimensionTable dt2)
        {
            StringBuilder joinConditionBuf = new StringBuilder();
            for (int i = 0; i < joinKeyColList.Count; i++)
            {
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(dt2.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                string joinKeyColLogicalName = joinKeyColList[i];
                string joinKeyColPhysicalName = findDimensionColumnPhysicalName(dt2, joinKeyColLogicalName);
                if (joinKeyColPhysicalName == null)
                {
                    joinKeyColPhysicalName = Util.generatePhysicalName(joinKeyColLogicalName, this.builderVersion) + getPhysicalSuffix();
                }
                joinConditionBuf.Append(joinKeyColPhysicalName);
                joinConditionBuf.Append("=\"");
                joinConditionBuf.Append(dt.TableName);
                joinConditionBuf.Append("\"");
                joinConditionBuf.Append(".");
                if (joinKeyColList2 != null)
                {
                    joinKeyColLogicalName = joinKeyColList2[i];
                }
                joinKeyColPhysicalName = findDimensionColumnPhysicalName(dt, joinKeyColLogicalName);
                if (joinKeyColPhysicalName == null)
                {
                    joinKeyColPhysicalName = Util.generatePhysicalName(joinKeyColLogicalName, this.builderVersion) + getPhysicalSuffix();
                }
                joinConditionBuf.Append(joinKeyColPhysicalName);
                if (i < (joinKeyColList.Count - 1))
                {
                    joinConditionBuf.Append(" AND ");
                }
            }
            return joinConditionBuf.ToString();
        }

        /**
         * Find the physical name from dimension table mapping, if available
         **/
        private string findDimensionColumnPhysicalName(DimensionTable dt, string dcLogicalName)
        {
            string dcPhysicalName = null;
            DataRowView[] drv = dimensionColumnMappingsView.FindRows(new object[] { dcLogicalName, dt.TableName });
            if (drv.Length > 0)
            {
                dcPhysicalName = (string) drv[0].Row["PhysicalName"];
            }
            //Dont look further if we found the data row in the view.
            if ((dcPhysicalName == null) && (dt.DimensionColumns != null))
            {
                foreach (DimensionColumn dc in dt.DimensionColumns)
                {
                    if (dcLogicalName.Equals(dc.ColumnName))
                    {
                        dcPhysicalName = dc.PhysicalName;
                        break;
                    }
                }
            }
            return dcPhysicalName;
        }

        Dictionary<string, DataRow> measureColumnCache = null;

        public void setMeasureColumnCache()
        {
            measureColumnCache = new Dictionary<string, DataRow>();
            foreach (DataRow dr in measureColumnsTable.Rows)
            {
                measureColumnCache.Add((string)dr["ColumnName"], dr);
            }
        }

        /*
         * Add an auto-generated measure column 
         */
        public DataRow addMeasureColumnDataRowIfNecessary(string columnName, string dataType, int width, string aggregationRule, bool key)
        {
            return addMeasureColumnDataRowIfNecessary(columnName, dataType, null, width, aggregationRule, key, true);
        }
        public DataRow addMeasureColumnDataRowIfNecessary(string columnName, string dataType, string columnformat, int width, string aggregationRule, bool key, bool resolveDataTypes)
        {
            return addMeasureColumnDataRowIfNecessary(columnName, dataType, columnformat, width, aggregationRule, key, true, true);
        }
        public DataRow addMeasureColumnDataRowIfNecessary(string columnName, string dataType, string columnformat, int width, string aggregationRule, bool key, bool resolveDataTypes, bool setkey)
        {
            DataRow retDataRow = measureColumnCache.ContainsKey(columnName) ? measureColumnCache[columnName] : null;
            if (retDataRow == null)
            {
                retDataRow = measureColumnsTable.NewRow();
                retDataRow["ColumnName"] = columnName;
                retDataRow["Key"] = key;
                retDataRow["Derived"] = false;
                if (dataType != null)
                    retDataRow["DataType"] = dataType;
                if (dataType != null && dataType == "Float")
                    width = FLOAT_WIDTH;
                retDataRow["Width"] = width;
                if (aggregationRule != null)
                    retDataRow["AggregationRule"] = aggregationRule;
                else if (dataType != null && (dataType == "DateTime" || dataType == "Date"))
                    retDataRow["AggregationRule"] = "MAX";
                if (columnformat != null)
                    retDataRow["Format"] = columnformat;
                measureColumnsTable.Rows.Add(retDataRow);
                retDataRow["AutoGenerated"] = true;
                measureColumnCache.Add(columnName, retDataRow);
            }
            else if (dataType != null)
            {
                if (setkey)
                    retDataRow["Key"] = key;
                if (resolveDataTypes)
                {
                    retDataRow["DataType"] = resolveTypes(dataType, (string)retDataRow["DataType"]);
                }
                else
                {
                    retDataRow["DataType"] = dataType;
                }
                if (width > 0 && width > (int)retDataRow["Width"])
                    retDataRow["Width"] = width;
                if (aggregationRule != null)
                    retDataRow["AggregationRule"] = aggregationRule;
                if (columnformat != null)
                    retDataRow["Format"] = columnformat;
            }

            return retDataRow;
        }

        public void removeMeasureColumnDataRows(List<string> mcToRemove)
        {
            foreach (string columnName in mcToRemove)
            {
                DataRow row = measureColumnCache.ContainsKey(columnName) ? measureColumnCache[columnName] : null;
                if (row != null)
                {
                    measureColumnsTable.Rows.Remove(row);
                    measureColumnCache.Remove(columnName);
                }
            }
        }
        /*
         * Add an auto-generated dimension column
         */
        public DataRow addDimensionColumnDataRowIfNecessary(string dimension, string colName, string dataType, string columnformat, int width, string unknownValue)
        {
            return addDimensionColumnDataRowIfNecessary(dimension, colName, dataType, columnformat, width, unknownValue, true);
        }

        public DataRow addDimensionColumnDataRowIfNecessary(string dimension, string colName, string dataType, int width, string unknownValue)
        {
            return addDimensionColumnDataRowIfNecessary(dimension, colName, dataType, null, width, unknownValue, true);
        }

        public DataRow addDimensionColumnDataRowIfNecessary(string dimension, string colName, string dataType, string columnformat, int width, string unknownValue, bool resolveDataTypes)
        {
            DataRowView[] drv = dimensionColumnNamesView.FindRows(new object[] { dimension, colName });
            DataRow retDataRow = drv.Length > 0 ? drv[0].Row : null;
            string format = timeDefinition != null && dimension == timeDefinition.Name && (dataType == "DateTime" || dataType == "Date") ? "MM/dd/yyyy" : columnformat;
            if (retDataRow == null)
            {
                retDataRow = dimensionColumns.NewRow();
                retDataRow["DimensionName"] = dimension;
                retDataRow["ColumnName"] = colName;
                retDataRow["DataType"] = dataType == "None" ? "Varchar" : dataType;
                if (dataType != null && dataType == "Float")
                    width = FLOAT_WIDTH;
                retDataRow["ColWidth"] = width;
                if (unknownValue != null && unknownValue.Length > 0)
                    retDataRow["UnknownValue"] = unknownValue;
                dimensionColumns.Rows.Add(retDataRow);
                retDataRow["AutoGenerated"] = true;
                if (format != null)
                    retDataRow["Format"] = format;
            }
            else
            {
                if (resolveDataTypes)
                {
                    retDataRow["DataType"] = resolveTypes(dataType, (string)retDataRow["DataType"]);
                }
                else
                {
                    retDataRow["DataType"] = dataType;
                }
                retDataRow["AutoGenerated"] = true;
                string widthstr = retDataRow["ColWidth"] != System.DBNull.Value ? (string)retDataRow["ColWidth"] : null;
                if (widthstr != null)
                {
                    if (widthstr.Length > 0)
                        try
                        {
                            int curWidth = int.Parse(widthstr);
                            if (curWidth > width)
                                width = curWidth;
                        }
                        catch (Exception)
                        {
                        }
                }
                retDataRow["ColWidth"] = width;
                if (unknownValue != null && unknownValue.Length > 0)
                    retDataRow["UnknownValue"] = unknownValue;
                if (format != null)
                    retDataRow["Format"] = format;
            }
            return retDataRow;
        }

        public DataRow removeDimensionColumnDataRowIfExisting(string dimension, string colName)
        {
            DataRow retDataRow = null;
            foreach (DataRow dr in dimensionColumns.Rows)
            {
                if (dr["DimensionName"].Equals(dimension) && dr["ColumnName"].Equals(colName))
                {
                    retDataRow = dr;
                    break;
                }
            }
            if (retDataRow != null)
            {
                dimensionColumns.Rows.Remove(retDataRow);
            }

            return retDataRow;
        }

        /*
         * Resolve two types to a type that can support both (occurs if two column definitions are made for the same column of different types)
         */
        private string resolveTypes(string type1, string type2)
        {
            if (type1.StartsWith("Date ID:") || type2.StartsWith("Date ID:"))
                return "Integer";
            if (type1 == "Varchar" || type2 == "Varchar")
                return ("Varchar");
            if (type1 == "DateTime" || type2 == "DateTime")
            {
                if (type1 == "DateTime" && type2 == "DateTime")
                    return ("DateTime");
                else if (type1 == "Date" || type2 == "Date")
                    return ("DateTime");
                else
                    return ("Varchar");                
            }
            if (type1 == "Date" || type2 == "Date")
            {
                if (type1 != "Date" || type2 != "Date")
                    return ("Varchar");
                else
                    return ("Date");
            }
            if (type1 == "Integer" && type2 == "Integer")
                return ("Integer");
            if (type1 == "Number" && type2 == "Number")
                return ("Number");
            return ("Float");
        }

        private List<DataRow> getSelectedMeasureColumnDataRows()
        {
            List<DataRow> selectedDataRows = new List<DataRow>();
            foreach (DataGridViewRow sr in measureDataGridView.SelectedRows)
            {
                DataRow dr = measureColumnsTable.Rows[sr.Index];
                selectedDataRows.Add(dr);
            }
            return selectedDataRows;
        }

        public List<BuildAction> buildApplication(string timeSchema, ApplicationBuilder ab)
        {
            bool previousUberTransactionState = inUberTransaction;
            inUberTransaction = true;
            try
            {
                List<BuildAction> buildActions = ab.buildApplication(unmappedDimensionTablesList, unmappedMeasureGrainsByColumnName, timeSchema, dependencies);
                return (buildActions);
            }
            finally
            {
                inUberTransaction = previousUberTransactionState;
            }
        }

        public List<BuildAction> cleanApp()
        {
            ApplicationBuilder ab = new ApplicationBuilder(this);
            List<BuildAction> buildActions = ab.cleanApplication(unmappedMeasureGrainsByColumnName, false);
            return (buildActions);
        }

        private void buildApplication_Click(object sender, EventArgs e)
        {
            TreeNode selNode = mainTree.SelectedNode;
            // Clear any selected node (and therefore any autosaves
            mainTree.SelectedNode = null;
            ApplicationBuilder ab = new ApplicationBuilder(this);
            //ab.setCarryKeysDownLevels(false);
            //ab.setGenerateInheritedTables(true);
            string timeSchema = getTimeSchema();
            List<BuildAction> buildActions = buildApplication(timeSchema, ab);
            BuildApplicationReportForm bar;
            bar = new BuildApplicationReportForm(buildActions);
            bar.Show(this);
            lastDimension = null;
            mainTree.SelectedNode = selNode;            
        }

        /**
         * This method helps distinguish the Birst vs. Enterprise implementation. For Birst, the schema is always present and for such
         * repositories, we need to pass the time schema name as "dbo" explicitly to build application process. For enterprise case, 
         * the schema is usually absent so we dont need to pass any time schema name to build application process.
         */
        private string getTimeSchema()
        {
            //Find schema associated with Default Connection
            string defaultConnectionSchema = null;
            if (connection.connectionList != null && connection.connectionList.Count > 0)
            {
                foreach(DatabaseConnection dc in connection.connectionList)
                {
                    if(dc.Name == "Default Connection")
                    {
                        defaultConnectionSchema = dc.Schema;
                        break;
                    }
                }
            }
            if (defaultConnectionSchema == null || defaultConnectionSchema.Trim().Length == 0)
            {
                return null;
            }
            return "dbo";
        }

        public string[] getPartitionConnectionNames(string connectionName)
        {
            //Find partition connections associated with given Connection
            string[] pcNames = null;
            if (connection.connectionList != null && connection.connectionList.Count > 0)
            {
                foreach (DatabaseConnection dc in connection.connectionList)
                {
                    if (dc.Name == connectionName && dc.IsPartitioned)
                    {
                        pcNames = dc.PartitionConnectionNames;
                        break;
                    }
                }
            }
            return pcNames;
        }

        public PartitionConnectionLoadInfo getPartitionConnectionLoadInfo(string partitionConnectionName)
        {
            if (PartitionConnectionLoadInfoMap == null)
            {
                return null;
            }
            PartitionConnectionLoadInfo pcli = null;
            if(partitionConnectionName != null && PartitionConnectionLoadInfoMap.ContainsKey(partitionConnectionName))
            {
                pcli = PartitionConnectionLoadInfoMap[partitionConnectionName];
            }
            return pcli;
        }

        private DataView mcView = null;

        public DataRow addMeasureColumnMapping(string columnName, string physicalName, MeasureTable mt, bool autoGenerated, int maxColLength, bool qualify)
        {
            if (mcView == null)
                mcView = new DataView(measureColumnMappings, null, "TableName,ColumnName",
                DataViewRowState.CurrentRows);

            DataRowView[] drvlist = mcView.FindRows(new object[] { mt.TableName, columnName });
            if (drvlist.Length == 0)
            {
                // Make sure case insensitive match doesn't happen for physical column
                DataRowView[] drvlist2 = measureColumnMappingsTablesView.FindRows(mt.TableName);
                bool found = true;
                while (found)
                {
                    found = false;
                    foreach (DataRowView drv in drvlist2)
                    {
                        string pname = (string)drv.Row["PhysicalName"];
                        if (pname.Equals(physicalName, StringComparison.OrdinalIgnoreCase))
                        {
                            if (!pname.Equals(physicalName) && (((bool)drv["AutoGenerated"])))
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found)
                        physicalName += "_";
                }

                DataRow mcmr = measureColumnMappings.NewRow();
                mcmr["ColumnName"] = columnName;
                mcmr["TableName"] = mt.TableName;
                if ((maxColLength > 0 && physicalName.Length > maxColLength))
                {
                    physicalName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(this), physicalName, maxColLength, builderVersion) + (this.builderVersion>=28 ? ApplicationBuilder.PHYSICAL_SUFFIX : "");
                }
                mcmr["PhysicalName"] = physicalName;
                mcmr["Qualify"] = qualify;
                mcmr["AutoGenerated"] = autoGenerated;
                measureColumnMappings.Rows.Add(mcmr);
                return (mcmr);
            }
            return (drvlist[0].Row);
        }

        private bool columnExistsInMeasureTable(string columnName, MeasureTable mt)
        {
            if (mt.MeasureColumns != null)
            {
                foreach (MeasureColumn mc in mt.MeasureColumns)
                {
                    if (columnName.Equals(mc.ColumnName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /*
         * Determine the existing grains for a given measure column (based on the measure table 
         * definitions that it is currently mapped to and what dimensions they join to)
         */
        public List<MeasureGrain> getMappedGrains(string measureColumnName)
        {
            List<MeasureTable> containerMeasureTables = new List<MeasureTable>();
            List<MeasureTableGrain> mtgList = new List<MeasureTableGrain>();
            /*
             * First, find all measure tables that contain the measure
             */
            foreach (MeasureTable mt in measureTablesList)
            {
                bool contains = false;
                if (mt.MeasureColumns != null)
                {
                    foreach (MeasureColumn mc in mt.MeasureColumns)
                    {
                        if (measureColumnName == mc.ColumnName)
                        {
                            contains = true;
                            break;
                        }
                    }
                }
                if (contains)
                {
                    containerMeasureTables.Add(mt);
                }
            }
            List<MeasureGrain> mgList = new List<MeasureGrain>();
            foreach (MeasureTable mt in containerMeasureTables)
            {
                List<MeasureTableGrain> mtgListForMeasureTable = getGrainInfo(mt);
                if (mtgListForMeasureTable.Count > 0)
                {
                    MeasureGrain mg = new MeasureGrain();
                    mg.measureTableName = mt.TableName;
                    mg.measureColumnName = measureColumnName;
                    mg.LoadGroups = (mt.Grain == null) ? null : mt.Grain.LoadGroups;
                    string connection = null;
                    ApplicationBuilder ab = new ApplicationBuilder(this);
                    mg.connection = (mt.Grain == null) ? "Default Connection" : (ab.hasLiveAccessLoadGroup(mt.Grain.LoadGroups, out connection) ? connection : "Default Connection");
                    mg.overrideKeyColumns = (mt.Grain == null) ? null : mt.Grain.overrideKeyColumns;
                    mg.measureTableGrains = new MeasureTableGrain[mtgListForMeasureTable.Count];
                    mg.measureTableGrains = mtgListForMeasureTable.ToArray();
                    mgList.Add(mg);
                }
            }
            return (mgList);
        }

        private void viewMeasureGrain_Click(object sender, EventArgs e)
        {
            int index = measureDataGridView.SelectedRows[0].Index;
            DataRow dr = measureColumnsTable.Rows[index];
            String measureColumnName = (String)dr["columnName"];

            List<MeasureGrain> mgList = getMappedGrains(measureColumnName);

            //Add the unmapped grains
            bool keyExists = unmappedMeasureGrainsByColumnName.ContainsKey(measureColumnName);
            List<MeasureGrain> unmappedMeasureGrainList = null;
            if (keyExists)
            {
                unmappedMeasureGrainList = unmappedMeasureGrainsByColumnName[measureColumnName];
            }
            else
            {
                unmappedMeasureGrainList = new List<MeasureGrain>();
            }

            MeasureGrainForm mgForm = new MeasureGrainForm(this, measureColumnName, mgList, unmappedMeasureGrainList);
            mgForm.Text = "Grains for measure " + measureColumnName;
            DialogResult mgFormDialogResult = mgForm.ShowDialog();
            if (mgFormDialogResult == DialogResult.OK)
            {
                List<MeasureGrain> unmappedMeasureGrainsList = mgForm.getUnmappedMeasureGrains();
                unmappedMeasureGrainsByColumnName.Remove(measureColumnName);
                unmappedMeasureGrainsByColumnName.Add(measureColumnName, unmappedMeasureGrainsList);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void menuItemMRU_Click(object sender, EventArgs e)
        {

        }

        private void MainAdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.UserClosing)
            {
                if (!exitTasks()) e.Cancel = true;
            }
        }

        private void timePropertiesItem_Click(object sender, EventArgs e)
        {
            TimeProperties tp = new TimeProperties(timeDefinition);
            DialogResult dr = tp.ShowDialog();
            if (dr == DialogResult.OK)
            {
                timeDefinition = tp.getTimeDefinition();
            }
        }

        private void cleanApplication_Click(object sender, EventArgs e)
        {
            TreeNode selNode = mainTree.SelectedNode;
            // Clear any selected node (and therefore any autosaves
            mainTree.SelectedNode = null;
            ApplicationBuilder ab = new ApplicationBuilder(this);
            List<BuildAction> buildActions = ab.cleanApplication(unmappedMeasureGrainsByColumnName, false);
            BuildApplicationReportForm bar = new BuildApplicationReportForm(buildActions);
            bar.Show(this);
            mainTree.SelectedNode = selNode;            
        }

        public void addPopupWindowStatus(string name, Size sz, Point location, FormWindowState fws)
        {
            appconfig.addPopupWindowStatus(name, sz, location, fws);
        }
        public Size getPopupWindowSize(string name)
        {
            return appconfig.getPopupWindowSize(name);
        }
        public Point getPopupWindowLocation(string name)
        {
            return appconfig.getPopupWindowLocation(name);
        }
        public FormWindowState getPopupWindowState(string name)
        {
            return appconfig.getPopupWindowState(name);
        }

        private void measureDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            measureDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void measureMapGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            measureMapGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void dimDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dimDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void dimColMappingGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dimColMappingGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void joinDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            joinDataGridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }

        private void dimDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dimDataGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void joinDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            joinDataGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void measureDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            measureDataGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void dimColMappingGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dimColMappingGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void measureMapGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            measureMapGridView.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        }

        private void dimDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dimDataGridView.Columns[e.ColumnIndex].Name == "DisplayMap")
            {
                object val = dimDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                DisplayMap dm = val == System.DBNull.Value ? null : (DisplayMap)val;
                DisplayMapForm dmf = new DisplayMapForm(dm);
                DialogResult dr = dmf.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    dm = dmf.getMap();
                    dimDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = dm;
                }
            }
        }

        private void dimColMappingGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dimColMappingGridView.SelectedCells.Count == 1)
            {
                Object o = dimColMappingGridView.SelectedCells[0].OwningRow.Cells[2].Value;
                if (o != null && (bool)o)
                {
                    dimColMappingGridView.SelectedCells[0].OwningRow.Cells[1].ReadOnly = true;
                }
                else
                {
                    dimColMappingGridView.SelectedCells[0].OwningRow.Cells[1].ReadOnly = false;
                }

                if (dimColMappingGridView.SelectedCells[0].OwningRow.IsNewRow)
                {
                    dimColMappingGridView.SelectedCells[0].OwningRow.Cells[0].ReadOnly = false;
                }
                else
                {
                    dimColMappingGridView.SelectedCells[0].OwningRow.Cells[0].ReadOnly = true;
                }
            }
        }

        private void measureMapGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (measureMapGridView.SelectedCells.Count == 1)
            {
                Object o = measureMapGridView.SelectedCells[0].OwningRow.Cells[3].Value;
                if (o != null && (bool)o)
                {
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[1].ReadOnly = true;
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[2].ReadOnly = true;
                }
                else
                {
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[1].ReadOnly = false;
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[2].ReadOnly = false;
                }

                if (measureMapGridView.SelectedCells[0].OwningRow.IsNewRow)
                {
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[0].ReadOnly = false;
                }
                else
                {
                    measureMapGridView.SelectedCells[0].OwningRow.Cells[0].ReadOnly = true;
                }
            }
        }

        public void clearLogicalColumns()
        {
            dimensionColumns.Clear();
            dimensionColumnMappings.Clear();
            measureColumnsTable.Clear();
            measureColumnMappings.Clear();
            unmappedMeasureGrainsByColumnName.Clear();
            unmappedDimensionTablesList.Clear();
        }

        private void measureDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= measureColumnsTable.Rows.Count) return;

            DataRow[] drlist = measureColumnMappings.Select("ColumnName='" + measureDataGridView.Rows[e.RowIndex].Cells[0].Value + "'");
            foreach (DataRow dr in drlist)
            {
                if (((bool)dr["AutoGenerated"]))
                    dr["AutoGenerated"] = false;
            }
        }

        private void dimDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            if (mainTree.SelectedNode != null &&
               (mainTree.SelectedNode.Parent != null || mainTree.SelectedNode.Parent != dimensionTablesNode))
            {
                string dimName = mainTree.SelectedNode.Text;
                string colName = (string)dimDataGridView.Rows[e.RowIndex].Cells["dimColumnName"].Value;
                Predicate<DimensionTable> pr = new Predicate<DimensionTable>(matchDimensionName);
                List<DimensionTable> ldt = dimensionTablesList.FindAll(pr);
                foreach (DimensionTable dt in ldt)
                {
                    foreach (DataRow dr in Util.selectRows(dimensionColumnMappings, new string[][] { new string[] { "TableName", dt.TableName }, new string[] { "ColumnName", (string)dimDataGridView.Rows[e.RowIndex].Cells[0].Value } }))
                    {
                        dr["AutoGenerated"] = false;
                    }
                }
            }
        }

        public SourceFile getSourceFile(StagingTable st)
        {
            SourceFile sf = null;
            foreach (SourceFile ssf in sourceFileMod.getSourceFiles())
            {
                if (ssf.FileName.ToLower() == st.SourceFile.ToLower())
                {
                    sf = ssf;
                    break;
                }
            }
            return (sf);
        }

        public bool RequiresPublish
        {
            get { return (requiresPublish); }
            set { requiresPublish = RequiresPublishBox.Checked = value; }
        }

        public bool RequiresRebuild
        {
            get { return (requiresRebuild); }
            set { requiresRebuild = value; }
        }

        private void joinDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int colIdx = joinDataGridView.Columns.IndexOf(AutoGenerated);
                joinDataGridView[colIdx, e.RowIndex].Value = false;
            }
        }

        private void MainAdminForm_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }

        public bool removeConnections(ArrayList toRemoveList)
        {
            int count = 0;
            if (toRemoveList != null && connection.connectionList != null)
            {
                count = toRemoveList.Count;
                foreach (DatabaseConnection sdc in toRemoveList)
                {
                    connection.connectionList.Remove(sdc);
                }
            }
            return count > 0;
        }

        public bool isConnectionInUse(String name)
        {
            return (aggModule.isConnectionInUse(name) || variableMod.isConnectionInUse(name)
                 || isConnectionInUseByDimension(name) || isConnectionInUseByeMeasure(name));
        }

        private bool isConnectionInUseByDimension(string name)
        {
            string lname = name.ToLower();
            bool connectionInUse = false;
            foreach (DimensionTable dt in dimensionTablesList)
            {
                if (dt.TableSource != null && dt.TableSource.Connection != null && dt.TableSource.Connection.ToLower() == lname)
                {
                    connectionInUse = true;
                    break;
                }
            }
            return connectionInUse;
        }

        private bool isConnectionInUseByeMeasure(string name)
        {
            string lname = name.ToLower();
            bool connectionInUse = false;
            foreach (MeasureTable mt in measureTablesList)
            {
                if (mt.TableSource != null && mt.TableSource.Connection != null && mt.TableSource.Connection.ToLower() == lname)
                {
                    connectionInUse = true;
                    break;
                }
            }
            return connectionInUse;
        }

        public string getConnectionName(String visiblename)
        {
            List<DatabaseConnection> dcList = connection.connectionList;
            string name = null;
            if (dcList != null && dcList.Count > 0)
            {
                foreach (DatabaseConnection dc in dcList)
                {
                    if (dc.VisibleName != null && dc.VisibleName.ToLower() == visiblename.ToLower())
                    {
                        name = dc.Name;
                        break;
                    }
                }
            }
            return name;
        }

        public bool isConnectionExist(String name)
        {
            List <DatabaseConnection> dcList = connection.connectionList;
            bool isExist = false;
            if (dcList != null && dcList.Count > 0)
            {
                foreach (DatabaseConnection dc in dcList)
                {
                    if (dc.Name.ToLower() == name.ToLower())
                    {
                        isExist = true;
                        break;
                    }
                }
            }
            return isExist;
        }

        public bool hasConnectionWithVisibleName(String visiblename)
        {
            List<DatabaseConnection> dcList = connection.connectionList;
            bool isExist = false;
            if (dcList != null)
            {
                foreach (DatabaseConnection dc in dcList)
                {
                    string vname = dc.VisibleName != null ? dc.VisibleName : dc.Name;
                    if (vname.ToLower() == visiblename.ToLower())
                    {
                        isExist = true;
                        break;
                    }
                }
            }
            return isExist;
        }

        public bool isInUberTransaction()
        {
            return inUberTransaction;
        }
        public void setInUberTransaction(bool b)
        {
            inUberTransaction = b;
        }

        public void updatedParams(string paramName)
        {
            paramsList.updatedParams(paramName);
        }
        public void refreshParams()
        {   
            paramsList.refreshParams();
        }
    }

    public class AppConfig
    {
        public string ServerConnectionURL;
        public string[] MRUItems;
        public Size mWindowSize = Size.Empty;
        public Point mWindowLocation = Point.Empty;
        public FormWindowState mWindowState;
        public PopupWindowStatus[] mPopupWindowStatus;

        public class PopupWindowStatus
        {
            public string mName;
            public Size mWindowSize = Size.Empty;
            public Point mWindowLocation = Point.Empty;
            public FormWindowState mWindowState;
        }


        public AppConfig()
        {
            ServerConnectionURL = "http://";
        }

        public void addPopupWindowStatus(string name, Size sz, Point location, FormWindowState fws)
        {
            PopupWindowStatus pws = new PopupWindowStatus();
            pws.mName = name;
            pws.mWindowSize = new Size(sz.Width, sz.Height);
            pws.mWindowLocation = new Point(location.X, location.Y);
            pws.mWindowState = fws;

            List<PopupWindowStatus> pwl = (mPopupWindowStatus == null) ? new List<PopupWindowStatus>(0) : new List<PopupWindowStatus>(mPopupWindowStatus);

            if (pwl.Count == 0)
            {

                pwl.Add(pws);
            }
            else
            {
                bool found = false;
                for (int i = 0; i < pwl.Count; i++)
                {
                    if (pwl[i].mName == name)
                    {
                        found = true;
                        pwl.RemoveAt(i);
                        pwl.Insert(i, pws);
                        break;
                    }
                }
                if (!found)
                {
                    pwl.Add(pws);
                }
            }
            mPopupWindowStatus = pwl.ToArray();

        }

        private PopupWindowStatus getPopupWindowStatus(string name)
        {
            PopupWindowStatus pws = null;
            if (this.mPopupWindowStatus == null || this.mPopupWindowStatus.Length == 0) return pws;
            for (int i = 0; i < this.mPopupWindowStatus.Length; i++)
            {
                if (mPopupWindowStatus[i].mName == name)
                {
                    return mPopupWindowStatus[i];
                }
            }
            return pws;
        }

        public Size getPopupWindowSize(string name)
        {
            PopupWindowStatus pws = getPopupWindowStatus(name);
            if (pws == null) return Size.Empty;
            return pws.mWindowSize;
        }
        public Point getPopupWindowLocation(string name)
        {
            PopupWindowStatus pws = getPopupWindowStatus(name);
            if (pws == null) return Point.Empty;
            return pws.mWindowLocation;
        }
        public FormWindowState getPopupWindowState(string name)
        {
            PopupWindowStatus pws = getPopupWindowStatus(name);
            if (pws == null) return FormWindowState.Normal;
            return pws.mWindowState;
        }

    }
}