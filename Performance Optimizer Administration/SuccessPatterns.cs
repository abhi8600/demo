using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Performance_Optimizer_Administration
{
    public partial class SuccessPatterns : Form, RepositoryModule, RenameNode
    {
        MainAdminForm ma;
        string moduleName = "Success Patterns";
        string categoryName = "Business Modeling";
        public List<PatternSearchDefinition> definitions;

        public SuccessPatterns(MainAdminForm ma)
        {
            InitializeComponent();
            this.ma = ma;
        }

        #region RepositoryModule Members

        public void setRepository(Repository r)
        {
            if (definitions != null)
            {
                r.SearchDefinitions = (PatternSearchDefinition[])definitions.ToArray();
            }
        }

        public void updateFromRepository(Repository r)
        {
            selIndex = -1;
            selNode = null;
            if (r != null && r.SearchDefinitions != null)
            {
                definitions = new List<PatternSearchDefinition>(r.SearchDefinitions);
            }
            else
            {
                definitions = new List<PatternSearchDefinition>();
            }
        }

        public void updateFromRepositoryFinal(Repository r)
        {
        }

        public void setFilters(Repository r, List<QueryFilter> list)
        {
        }

        TreeNode rootNode;

        public void setupNodes(TreeNode rootNode)
        {
            this.rootNode = rootNode;
            rootNode.Nodes.Clear();
            if (definitions != null)
                foreach (PatternSearchDefinition psd in definitions)
                {
                    TreeNode tn = new TreeNode(psd.Name);
                    tn.ImageIndex = 18;
                    tn.SelectedImageIndex = 18;
                    rootNode.Nodes.Add(tn);
                }
        }

        TreeNode selNode;
        int selIndex;

        public bool selectedNode(TreeNode node)
        {
            implicitSave();
            if (definitions.Count == 0 || node.Parent.Parent == null)
            {
                ma.mainTree.ContextMenuStrip = patternMenuStrip;
                removePatternSearchDefinitionToolStripMenuItem.Visible = false;
                return false;
            }
            selIndex = rootNode.Nodes.IndexOf(node);
            if (selIndex >= 0)
            {
                selNode = node;
                indexChanged(selIndex);
                ma.mainTree.ContextMenuStrip = patternMenuStrip;
                removePatternSearchDefinitionToolStripMenuItem.Visible = true;
            }
            return (true);
        }

        public void setup()
        {
            List<SuccessModel> smlist = ma.smodels.modelList;
            if (smlist != null)
            {
                patternDefinitionModel.Items.Clear();
                foreach (SuccessModel sm in smlist)
                {
                    patternDefinitionModel.Items.Add(sm.Name);
                    if (sm.Name == patternDefinitionModel.Name)
                    {
                        segmentListMeasures.Items.Clear();
                        Hashtable ht = new Hashtable();
                        foreach (Pattern p in sm.Patterns)
                        {
                            ht.Add(p.Measure,null);
                        }
                        foreach (String s in ht.Keys)
                        {
                            segmentListMeasures.Items.Add(s);

                        }
                    }
                }
            }
        }

        public string ModuleName
        {
            get
            {
                return moduleName;
            }
            set
            {
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }
            set
            {
            }
        }

        public TabPage TabPage
        {
            get
            {
                return patternsTabPage;
            }
            set
            {
            }
        }

        public void replaceColumn(string find, string replace, bool match)
        {
            foreach (PatternSearchDefinition psd in definitions)
            {
                if (match)
                {
                    if (psd.JoinMeasure == find) psd.JoinMeasure = replace;
                    for (int i = 0; i < psd.SegmentMeasures.Length; i++)
                        if (psd.SegmentMeasures[i] == find) psd.SegmentMeasures[i] = replace;
                    if (psd.TargetLevel == find) psd.TargetLevel = replace;
                }
                else
                {
                    psd.JoinMeasure = psd.JoinMeasure.Replace(find, replace); ;
                    for (int i = 0; i < psd.SegmentMeasures.Length; i++)
                        psd.SegmentMeasures[i] = psd.SegmentMeasures[i].Replace(find, replace);
                    psd.TargetLevel = psd.TargetLevel.Replace(find, replace);
                }
            }
        }

        public void findColumn(List<string> results, string find, bool match)
        {
            foreach (PatternSearchDefinition psd in definitions)
            {
                if (match)
                {
                    if (psd.JoinMeasure == find) results.Add("Success Pattern: " + psd.Name + " - Join measure");
                    for (int i = 0; i < psd.SegmentMeasures.Length; i++)
                        if (psd.SegmentMeasures[i] == find) results.Add("Success Pattern: " + psd.Name + " - Segment measure");
                    if (psd.TargetLevel == find) results.Add("Success Pattern: " + psd.Name + " - Target level");
                }
                else
                {
                    if (psd.JoinMeasure.IndexOf(find) >= 0) results.Add("Success Pattern: " + psd.Name + " - Join measure");
                    for (int i = 0; i < psd.SegmentMeasures.Length; i++)
                        if (psd.SegmentMeasures[i].IndexOf(find) >= 0) results.Add("Success Pattern: " + psd.Name + " - Segment measure");
                    if (psd.TargetLevel.IndexOf(find) >= 0) results.Add("Success Pattern: " + psd.Name + " - Target level");
                }
            }
        }

        private bool turnOffimplicitSave = false;
        public void implicitSave()
        {
            if (!turnOffimplicitSave) updatePatternDefinition();
        }
        public void setRepositoryDirectory(string d)
        {
        }
        #endregion
        private PatternSearchDefinition getPatternDefinition()
        {
            PatternSearchDefinition psd = new PatternSearchDefinition();
            if (selNode != null)
                psd.Name = selNode.Text;
            else
                psd.Name = "New Success Pattern";
            psd.SuccessModelName = patternDefinitionModel.Text;
            psd.IterationAnalysisValue = patternIterationValue.Text;
            psd.OutputTableName = patternOutputTable.Text;
            psd.DropExistingTable = dropPSTable.Checked;
            psd.IngoreZeroWeights = ignoreZeroWeights.Checked;
            psd.GenerateSegments = genSegments.Checked;
            try
            {
                psd.NumPatternsToSave = Int32.Parse(patternNumToSave.Text);
            }
            catch (Exception)
            {
                psd.NumPatternsToSave = 20;
            }
            try
            {
                psd.NumPeersToCompare = Int32.Parse(numTopPeers.Text);
            }
            catch (Exception)
            {
                psd.NumPeersToCompare = 10;
            }
            try
            {
                psd.NumTargetSegmentsToSave = Int32.Parse(segmentNumToSave.Text);
            }
            catch (Exception)
            {
                psd.NumTargetSegmentsToSave = 3;
            }
            try
            {
                psd.NumPatternsPerMeasure = Int32.Parse(patternsPerMeasure.Text);
            }
            catch (Exception)
            {
                psd.NumPatternsPerMeasure = 3;
            }
            psd.TargetDimension = targetDimBox.Text;
            psd.TargetLevel = targetLevelBox.Text;
            psd.JoinMeasure = joinMeasureBox.Text;
            psd.SegmentOutputTableName = segmentOutputTableName.Text;
            psd.DropExistingSegmentOutputTable = dropSegmentTable.Checked;
            psd.ScreenDuplicateMeasures = screenDupMeasures.Checked;
            psd.ProcessTargetsSeparately = processSeparately.Checked;
            psd.SegmentMeasures = new string[segmentListMeasures.CheckedIndices.Count];
            for (int i = 0; i < psd.SegmentMeasures.Length; i++)
            {
                psd.SegmentMeasures[i] = segmentListMeasures.CheckedItems[i].ToString();
            }
            psd.ReferenceFilters = new string[referenceFilters.CheckedItems.Count];
            for (int i = 0; i < referenceFilters.CheckedItems.Count; i++)
                psd.ReferenceFilters[i] = referenceFilters.CheckedItems[i].ToString();
            psd.ComparisonFilters = new string[comparisonFilters.CheckedItems.Count];
            for (int i = 0; i < comparisonFilters.CheckedItems.Count; i++)
                psd.ComparisonFilters[i] = comparisonFilters.CheckedItems[i].ToString();
            psd.PatternSearchType = peerSearch.Checked ? 0 : populationSearch.Checked ? 1 : 2;
            try
            {
                psd.SplitPercentage = Double.Parse(refPercent.Text) / 100;
            }
            catch (Exception)
            {
                psd.SplitPercentage = 0.5;
            }
            psd.ReferencePopulation = SPreferencePopulationBox.Text;
            return (psd);
        }

        private void addPatternDefinition_Click(object sender, System.EventArgs e)
        {
            PatternSearchDefinition psd = getPatternDefinition();
            psd.Name = "New Success Pattern";
            if (definitions == null)
            {
                definitions = new List<PatternSearchDefinition>();
            }
            definitions.Add(psd);
            TreeNode tn = new TreeNode(psd.Name);
            tn.ImageIndex = 18;
            tn.SelectedImageIndex = 18;
            rootNode.Nodes.Add(tn);
        }

        private void removePatternDefinition_Click(object sender, System.EventArgs e)
        {
            if (selIndex < 0 || selNode == null || selNode.Parent != rootNode) return;
            turnOffimplicitSave = true;
            definitions.RemoveAt(selIndex);
            selNode.Remove();
            turnOffimplicitSave = false;
        }

        private void updatePatternDefinition()
        {
            if (selNode == null || selNode == rootNode || selIndex < 0 || selIndex >= definitions.Count) return;
            PatternSearchDefinition psd = getPatternDefinition();
            definitions[selIndex] = psd;
            selNode.Text = psd.Name;
        }

        private void indexChanged(int index)
        {
            PatternSearchDefinition psd = definitions[index];
            patternDefinitionModel.Text = psd.SuccessModelName;
            patternIterationValue.Text = psd.IterationAnalysisValue;
            patternOutputTable.Text = psd.OutputTableName;
            patternNumToSave.Text = psd.NumPatternsToSave.ToString();
            patternsPerMeasure.Text = psd.NumPatternsPerMeasure.ToString();
            numTopPeers.Text = psd.NumPeersToCompare.ToString();
            targetDimBox.Text = psd.TargetDimension;
            targetLevelBox.Text = psd.TargetLevel;
            joinMeasureBox.Text = psd.JoinMeasure;
            segmentNumToSave.Text = psd.NumTargetSegmentsToSave.ToString();
            segmentOutputTableName.Text = psd.SegmentOutputTableName;
            dropSegmentTable.Checked = psd.DropExistingSegmentOutputTable;
            processSeparately.Checked = psd.ProcessTargetsSeparately;
            screenDupMeasures.Checked = psd.ScreenDuplicateMeasures;
            dropPSTable.Checked = psd.DropExistingTable;
            ignoreZeroWeights.Checked = psd.IngoreZeroWeights;
            genSegments.Checked = psd.GenerateSegments;
            segmentListMeasures.Items.Clear();
            if (ma.smodels.modelList != null)
            {
                foreach (SuccessModel sm in ma.smodels.modelList)
                {
                    if (sm.Name == patternDefinitionModel.Text)
                    {
                        ArrayList list = new ArrayList();
                        foreach (Pattern p in sm.Patterns)
                        {
                            if (!list.Contains(p.Measure))
                                list.Add(p.Measure);
                        }
                        for (int i = 0; i < list.Count; i++)
                        {
                            string s = (string)list[i];
                            segmentListMeasures.Items.Add(s);
                            bool found = false;
                            if (psd.SegmentMeasures != null)
                                for (int j = 0; j < psd.SegmentMeasures.Length; j++)
                                {
                                    if (psd.SegmentMeasures[j] == s)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            segmentListMeasures.SetItemChecked(i, found);
                        }
                        if (sm.TargetDimension != null)
                        {
                            List<string>[] lists = ma.getMeasureList(sm.TargetDimension);
                            List<string> mlist = lists[0];
                            foreach (string name in mlist)
                            {
                                joinMeasureBox.Items.Add(name);
                            }
                        }
                        break;
                    }
                }
            }
            List<string> dlist = ma.getDimensionList();
            targetDimBox.Items.Clear();
            foreach (string d in dlist)
            {
                if ((d != null) && (d.Length > 0))
                    targetDimBox.Items.Add(d);
            }
            peerSearch.Checked = psd.PatternSearchType == 0;
            populationSearch.Checked = psd.PatternSearchType == 1;
            splitSearch.Checked = psd.PatternSearchType == 2;

            QueryFilter.sortedListBox(referenceFilters, ma.filterMod.filterList, psd.ReferenceFilters);
            QueryFilter.sortedListBox(comparisonFilters, ma.filterMod.filterList, psd.ComparisonFilters);
           
            List<string>[] measures = ma.getMeasureList(null);
            refPercent.Text = psd.SplitPercentage > 0 ? (psd.SplitPercentage * 100).ToString() : "50";

            // Populate reference populations
            SPreferencePopulationBox.Items.Clear();
            foreach (ReferencePopulation refPop in ma.refPopModule.refPopList)
            {
                SPreferencePopulationBox.Items.Add(refPop.PopulationName);
            }
            SPreferencePopulationBox.Text = psd.ReferencePopulation;
        }

        private void patternListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
        }

        private void targetDimBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> clist = ma.getDimensionColumnList(targetDimBox.Text);
            targetLevelBox.Items.Clear();
            foreach (string c in clist)
            {
                targetLevelBox.Items.Add(c);
            }

        }

        private void patternsTabPage_Leave(object sender, EventArgs e)
        {
            implicitSave();
        }


        #region RenameNode Members

        public void rename(NodeLabelEditEventArgs e)
        {
            if (e.Node.Parent == rootNode)
            {
                e.Node.Text = e.Label;
            }
        }

        #endregion

        private void SuccessPatterns_MouseMove(object sender, MouseEventArgs e)
        {
            Util.MouseMove(sender, e, toolTip);
        }
    }
}