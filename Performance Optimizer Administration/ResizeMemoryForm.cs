using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Performance_Optimizer_Administration
{
    interface ResizeMemoryForm
    {
        void resetWindowFromSettings();
        void saveWindowSettings();
    }
}
