using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Performance_Optimizer_Administration
{
	/// <summary>
	/// Summary description for SearchForm.
	/// </summary>
    public class SearchForm : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.TextBox replaceBox;
        private System.Windows.Forms.Button OKbutton;
        private System.Windows.Forms.Button cancelButton;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private DataTable measureColumns;
        private System.Windows.Forms.CheckBox matchCheckBox;
        private DataTable dimensionColumns;
        private bool replace;

        public SearchForm(DataTable measureColumns, DataTable dimensionColumns, bool replace)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.measureColumns = measureColumns;
            this.dimensionColumns = dimensionColumns;
            replaceBox.Enabled = replace;
            this.replace = replace;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.replaceBox = new System.Windows.Forms.TextBox();
            this.matchCheckBox = new System.Windows.Forms.CheckBox();
            this.OKbutton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search For:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Replace With:";
            // 
            // searchBox
            // 
            this.searchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchBox.Location = new System.Drawing.Point(96, 16);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(184, 20);
            this.searchBox.TabIndex = 2;
            // 
            // replaceBox
            // 
            this.replaceBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.replaceBox.Location = new System.Drawing.Point(96, 40);
            this.replaceBox.Name = "replaceBox";
            this.replaceBox.Size = new System.Drawing.Size(184, 20);
            this.replaceBox.TabIndex = 3;
            // 
            // matchCheckBox
            // 
            this.matchCheckBox.Checked = true;
            this.matchCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.matchCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchCheckBox.Location = new System.Drawing.Point(8, 64);
            this.matchCheckBox.Name = "matchCheckBox";
            this.matchCheckBox.Size = new System.Drawing.Size(136, 16);
            this.matchCheckBox.TabIndex = 4;
            this.matchCheckBox.Text = "Match Whole Name";
            // 
            // OKbutton
            // 
            this.OKbutton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKbutton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKbutton.Location = new System.Drawing.Point(112, 88);
            this.OKbutton.Name = "OKbutton";
            this.OKbutton.Size = new System.Drawing.Size(75, 23);
            this.OKbutton.TabIndex = 5;
            this.OKbutton.Text = "OK";
            this.OKbutton.Click += new System.EventHandler(this.OKbutton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cancelButton.Location = new System.Drawing.Point(200, 88);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // SearchForm
            // 
            this.AcceptButton = this.OKbutton;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(290, 117);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.OKbutton);
            this.Controls.Add(this.matchCheckBox);
            this.Controls.Add(this.replaceBox);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Search and Replace Column Names";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void OKbutton_Click(object sender, System.EventArgs e)
        {
            if (this.replaceBox.Enabled)
            {
                foreach (DataRow dr in measureColumns.Rows)
                {
                    string s = (string)dr["ColumnName"];
                    if (this.matchCheckBox.Checked)
                    {
                        if (s == searchBox.Text) dr["ColumnName"] = replaceBox.Text;
                    }
                    else
                    {
                        dr["ColumnName"] = s.Replace(searchBox.Text, replaceBox.Text);
                    }
                }
                foreach (DataRow dr in dimensionColumns.Rows)
                {
                    string s = (string)dr["ColumnName"];
                    if (this.matchCheckBox.Checked)
                    {
                        if (s == searchBox.Text) dr["ColumnName"] = replaceBox.Text;
                    }
                    else
                    {
                        dr["ColumnName"] = s.Replace(searchBox.Text, replaceBox.Text);
                    }
                }
            }
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {

        }

        public string findColumn
        {
            get
            {
                return (searchBox.Text);
            }
            set
            {
            }
        }

        public string replaceColumn
        {
            get
            {
                return (replaceBox.Text);
            }
            set
            {
            }
        }

        public bool match
        {
            get
            {
                return (this.matchCheckBox.Checked);
            }
            set
            {
            }
        }

    }
}
