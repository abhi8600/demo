namespace Performance_Optimizer_Administration
{
    partial class SubjectAreas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.subjectAreaTabPage = new System.Windows.Forms.TabPage();
            this.saHighlight = new System.Windows.Forms.Button();
            this.addSAColumns = new System.Windows.Forms.Button();
            this.availableSubjectAreaColumns = new System.Windows.Forms.ListView();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.saContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.upToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saAccessMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruneBadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addSubjectAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saToolStripBox = new System.Windows.Forms.ToolStripTextBox();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderStripBox = new System.Windows.Forms.ToolStripTextBox();
            this.addFolderStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.subjectAreaTabPage.SuspendLayout();
            this.saContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.subjectAreaTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(852, 733);
            this.tabControl.TabIndex = 0;
            // 
            // subjectAreaTabPage
            // 
            this.subjectAreaTabPage.Controls.Add(this.saHighlight);
            this.subjectAreaTabPage.Controls.Add(this.addSAColumns);
            this.subjectAreaTabPage.Controls.Add(this.availableSubjectAreaColumns);
            this.subjectAreaTabPage.Location = new System.Drawing.Point(4, 22);
            this.subjectAreaTabPage.Name = "subjectAreaTabPage";
            this.subjectAreaTabPage.Size = new System.Drawing.Size(844, 707);
            this.subjectAreaTabPage.TabIndex = 17;
            this.subjectAreaTabPage.Text = "Subject Areas";
            this.subjectAreaTabPage.UseVisualStyleBackColor = true;
            // 
            // saHighlight
            // 
            this.saHighlight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.saHighlight.Location = new System.Drawing.Point(94, 3);
            this.saHighlight.Name = "saHighlight";
            this.saHighlight.Size = new System.Drawing.Size(80, 35);
            this.saHighlight.TabIndex = 16;
            this.saHighlight.Text = "Highlight Unused";
            this.saHighlight.Click += new System.EventHandler(this.saHighlight_Click);
            // 
            // addSAColumns
            // 
            this.addSAColumns.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addSAColumns.Location = new System.Drawing.Point(8, 3);
            this.addSAColumns.Name = "addSAColumns";
            this.addSAColumns.Size = new System.Drawing.Size(80, 34);
            this.addSAColumns.TabIndex = 8;
            this.addSAColumns.Text = "Add Columns";
            this.addSAColumns.Click += new System.EventHandler(this.addSAColumns_Click);
            // 
            // availableSubjectAreaColumns
            // 
            this.availableSubjectAreaColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.availableSubjectAreaColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.availableSubjectAreaColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader29,
            this.columnHeader30});
            this.availableSubjectAreaColumns.FullRowSelect = true;
            this.availableSubjectAreaColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.availableSubjectAreaColumns.HideSelection = false;
            this.availableSubjectAreaColumns.Location = new System.Drawing.Point(8, 44);
            this.availableSubjectAreaColumns.Name = "availableSubjectAreaColumns";
            this.availableSubjectAreaColumns.ShowItemToolTips = true;
            this.availableSubjectAreaColumns.Size = new System.Drawing.Size(528, 660);
            this.availableSubjectAreaColumns.TabIndex = 4;
            this.availableSubjectAreaColumns.UseCompatibleStateImageBehavior = false;
            this.availableSubjectAreaColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Dimension";
            this.columnHeader29.Width = 200;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Column";
            this.columnHeader30.Width = 300;
            // 
            // saContextMenu
            // 
            this.saContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upToolStripMenuItem,
            this.downToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.saAccessMenuItem,
            this.pruneBadToolStripMenuItem,
            this.addSubjectAreaToolStripMenuItem,
            this.addFolderToolStripMenuItem});
            this.saContextMenu.Name = "saContextMenu";
            this.saContextMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.saContextMenu.ShowImageMargin = false;
            this.saContextMenu.Size = new System.Drawing.Size(143, 158);
            // 
            // upToolStripMenuItem
            // 
            this.upToolStripMenuItem.Name = "upToolStripMenuItem";
            this.upToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.upToolStripMenuItem.Text = "Up";
            this.upToolStripMenuItem.Click += new System.EventHandler(this.upSASelected_Click);
            // 
            // downToolStripMenuItem
            // 
            this.downToolStripMenuItem.Name = "downToolStripMenuItem";
            this.downToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.downToolStripMenuItem.Text = "Down";
            this.downToolStripMenuItem.Click += new System.EventHandler(this.downSASelected_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeSASelected_Click);
            // 
            // saAccessMenuItem
            // 
            this.saAccessMenuItem.Name = "saAccessMenuItem";
            this.saAccessMenuItem.Size = new System.Drawing.Size(142, 22);
            this.saAccessMenuItem.Text = "Groups with access";
            this.saAccessMenuItem.Click += new System.EventHandler(this.saAccessMenuItem_Click);
            // 
            // pruneBadToolStripMenuItem
            // 
            this.pruneBadToolStripMenuItem.Name = "pruneBadToolStripMenuItem";
            this.pruneBadToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.pruneBadToolStripMenuItem.Text = "Prune Bad";
            this.pruneBadToolStripMenuItem.Click += new System.EventHandler(this.pruneSAbadColumns_Click);
            // 
            // addSubjectAreaToolStripMenuItem
            // 
            this.addSubjectAreaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saToolStripBox,
            this.addToolStripMenuItem});
            this.addSubjectAreaToolStripMenuItem.Name = "addSubjectAreaToolStripMenuItem";
            this.addSubjectAreaToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.addSubjectAreaToolStripMenuItem.Text = "Add Subject Area";
            // 
            // saToolStripBox
            // 
            this.saToolStripBox.AcceptsReturn = true;
            this.saToolStripBox.Name = "saToolStripBox";
            this.saToolStripBox.Size = new System.Drawing.Size(100, 20);
            this.saToolStripBox.Validated += new System.EventHandler(this.addSAButton_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addSAButton_Click);
            // 
            // addFolderToolStripMenuItem
            // 
            this.addFolderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.folderStripBox,
            this.addFolderStripItem});
            this.addFolderToolStripMenuItem.Name = "addFolderToolStripMenuItem";
            this.addFolderToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.addFolderToolStripMenuItem.Text = "Add Folder";
            // 
            // folderStripBox
            // 
            this.folderStripBox.Name = "folderStripBox";
            this.folderStripBox.Size = new System.Drawing.Size(100, 20);
            // 
            // addFolderStripItem
            // 
            this.addFolderStripItem.Name = "addFolderStripItem";
            this.addFolderStripItem.Size = new System.Drawing.Size(160, 22);
            this.addFolderStripItem.Text = "Add";
            this.addFolderStripItem.Click += new System.EventHandler(this.addSAFolder_Click);
            // 
            // SubjectAreas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl);
            this.Name = "SubjectAreas";
            this.Text = "SubjectAreas";
            this.tabControl.ResumeLayout(false);
            this.subjectAreaTabPage.ResumeLayout(false);
            this.saContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage subjectAreaTabPage;
        private System.Windows.Forms.Button saHighlight;
        private System.Windows.Forms.Button addSAColumns;
        private System.Windows.Forms.ListView availableSubjectAreaColumns;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ContextMenuStrip saContextMenu;
        private System.Windows.Forms.ToolStripMenuItem saAccessMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruneBadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addSubjectAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox saToolStripBox;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox folderStripBox;
        private System.Windows.Forms.ToolStripMenuItem addFolderStripItem;
    }
}