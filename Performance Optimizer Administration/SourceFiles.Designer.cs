namespace Performance_Optimizer_Administration
{
    partial class SourceFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SourceFiles));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sourceFilePage = new System.Windows.Forms.TabPage();
            this.sourceFilePanel = new System.Windows.Forms.Panel();
            this.allowNullBindingRemovedColumnsBox = new System.Windows.Forms.CheckBox();
            this.filterOperatorLabel = new System.Windows.Forms.Label();
            this.filterOperatorComboBox = new System.Windows.Forms.ComboBox();
            this.ignoreCarriageReturnCheckBox = new System.Windows.Forms.CheckBox();
            this.failUploadOnVarcharExpansionRadioButton = new System.Windows.Forms.RadioButton();
            this.allowValueTruncationOnLoadRadioButton = new System.Windows.Forms.RadioButton();
            this.allowVarcharExpansionRadioButton = new System.Windows.Forms.RadioButton();
            this.allowUpcastBox = new System.Windows.Forms.CheckBox();
            this.allowAddColumnsBox = new System.Windows.Forms.CheckBox();
            this.customUploadBox = new System.Windows.Forms.CheckBox();
            this.lblMinRows2 = new System.Windows.Forms.Label();
            this.minimumRowsBox = new System.Windows.Forms.TextBox();
            this.lblMinRows = new System.Windows.Forms.Label();
            this.schemaLockBox = new System.Windows.Forms.CheckBox();
            this.lockFormatBox = new System.Windows.Forms.CheckBox();
            this.continueIfMissingCheckBox = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.InputTimeZoneComboBox = new System.Windows.Forms.ComboBox();
            this.ignoreBackslashCheckBox = new System.Windows.Forms.CheckBox();
            this.quoteAfterSeparator = new System.Windows.Forms.CheckBox();
            this.forceNumColumns = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.fileEncodingTextBox = new System.Windows.Forms.TextBox();
            this.pasteNames = new System.Windows.Forms.Button();
            this.fileFormatTypeLbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.searchPatternLbl = new System.Windows.Forms.Label();
            this.escapeGrid = new System.Windows.Forms.DataGridView();
            this.escapeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.replacementDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataSet = new System.Data.DataSet();
            this.escapesTable = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.fixedWidthRadioButton = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ignoreLastRowsBox = new System.Windows.Forms.TextBox();
            this.rowFilterValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.searchPatternTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.containsHeaders = new System.Windows.Forms.CheckBox();
            this.delimitedByRadioButton = new System.Windows.Forms.RadioButton();
            this.filterColumnBox = new System.Windows.Forms.ComboBox();
            this.quoteTextBox = new System.Windows.Forms.TextBox();
            this.delimiterTxt = new System.Windows.Forms.TextBox();
            this.ignoreRowsBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.sourceFileGridView = new System.Windows.Forms.DataGridView();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Format = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Multiplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Encrypted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PreventUpdate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.sourceFileMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newSourceFileStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSourceFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateSourceFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeSourceFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSourceFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.replaceWithNullRadioButton = new System.Windows.Forms.RadioButton();
            this.skipRecordRadioButton = new System.Windows.Forms.RadioButton();
            this.sourceFilePage.SuspendLayout();
            this.sourceFilePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.escapeGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.escapesTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceFileGridView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.sourceFileMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceFilePage
            // 
            this.sourceFilePage.AutoScroll = true;
            this.sourceFilePage.Controls.Add(this.sourceFilePanel);
            this.sourceFilePage.Controls.Add(this.sourceFileGridView);
            this.sourceFilePage.Location = new System.Drawing.Point(4, 22);
            this.sourceFilePage.Name = "sourceFilePage";
            this.sourceFilePage.Padding = new System.Windows.Forms.Padding(3);
            this.sourceFilePage.Size = new System.Drawing.Size(844, 707);
            this.sourceFilePage.TabIndex = 0;
            this.sourceFilePage.Text = "Data Sources";
            this.sourceFilePage.UseVisualStyleBackColor = true;
            // 
            // sourceFilePanel
            // 
            this.sourceFilePanel.Controls.Add(this.skipRecordRadioButton);
            this.sourceFilePanel.Controls.Add(this.replaceWithNullRadioButton);
            this.sourceFilePanel.Controls.Add(this.allowNullBindingRemovedColumnsBox);
            this.sourceFilePanel.Controls.Add(this.filterOperatorLabel);
            this.sourceFilePanel.Controls.Add(this.filterOperatorComboBox);
            this.sourceFilePanel.Controls.Add(this.ignoreCarriageReturnCheckBox);
            this.sourceFilePanel.Controls.Add(this.failUploadOnVarcharExpansionRadioButton);
            this.sourceFilePanel.Controls.Add(this.allowValueTruncationOnLoadRadioButton);
            this.sourceFilePanel.Controls.Add(this.allowVarcharExpansionRadioButton);
            this.sourceFilePanel.Controls.Add(this.allowUpcastBox);
            this.sourceFilePanel.Controls.Add(this.allowAddColumnsBox);
            this.sourceFilePanel.Controls.Add(this.customUploadBox);
            this.sourceFilePanel.Controls.Add(this.lblMinRows2);
            this.sourceFilePanel.Controls.Add(this.minimumRowsBox);
            this.sourceFilePanel.Controls.Add(this.lblMinRows);
            this.sourceFilePanel.Controls.Add(this.schemaLockBox);
            this.sourceFilePanel.Controls.Add(this.lockFormatBox);
            this.sourceFilePanel.Controls.Add(this.continueIfMissingCheckBox);
            this.sourceFilePanel.Controls.Add(this.label11);
            this.sourceFilePanel.Controls.Add(this.InputTimeZoneComboBox);
            this.sourceFilePanel.Controls.Add(this.ignoreBackslashCheckBox);
            this.sourceFilePanel.Controls.Add(this.quoteAfterSeparator);
            this.sourceFilePanel.Controls.Add(this.forceNumColumns);
            this.sourceFilePanel.Controls.Add(this.label10);
            this.sourceFilePanel.Controls.Add(this.label9);
            this.sourceFilePanel.Controls.Add(this.fileEncodingTextBox);
            this.sourceFilePanel.Controls.Add(this.pasteNames);
            this.sourceFilePanel.Controls.Add(this.fileFormatTypeLbl);
            this.sourceFilePanel.Controls.Add(this.label8);
            this.sourceFilePanel.Controls.Add(this.searchPatternLbl);
            this.sourceFilePanel.Controls.Add(this.escapeGrid);
            this.sourceFilePanel.Controls.Add(this.fixedWidthRadioButton);
            this.sourceFilePanel.Controls.Add(this.label7);
            this.sourceFilePanel.Controls.Add(this.label2);
            this.sourceFilePanel.Controls.Add(this.ignoreLastRowsBox);
            this.sourceFilePanel.Controls.Add(this.rowFilterValue);
            this.sourceFilePanel.Controls.Add(this.label4);
            this.sourceFilePanel.Controls.Add(this.searchPatternTextBox);
            this.sourceFilePanel.Controls.Add(this.label1);
            this.sourceFilePanel.Controls.Add(this.label6);
            this.sourceFilePanel.Controls.Add(this.label3);
            this.sourceFilePanel.Controls.Add(this.containsHeaders);
            this.sourceFilePanel.Controls.Add(this.delimitedByRadioButton);
            this.sourceFilePanel.Controls.Add(this.filterColumnBox);
            this.sourceFilePanel.Controls.Add(this.quoteTextBox);
            this.sourceFilePanel.Controls.Add(this.delimiterTxt);
            this.sourceFilePanel.Controls.Add(this.ignoreRowsBox);
            this.sourceFilePanel.Controls.Add(this.label5);
            this.sourceFilePanel.Location = new System.Drawing.Point(0, 0);
            this.sourceFilePanel.Name = "sourceFilePanel";
            this.sourceFilePanel.Size = new System.Drawing.Size(844, 307);
            this.sourceFilePanel.TabIndex = 24;
            // 
            // allowNullBindingRemovedColumnsBox
            // 
            this.allowNullBindingRemovedColumnsBox.AutoSize = true;
            this.allowNullBindingRemovedColumnsBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowNullBindingRemovedColumnsBox.Location = new System.Drawing.Point(173, 201);
            this.allowNullBindingRemovedColumnsBox.Name = "allowNullBindingRemovedColumnsBox";
            this.allowNullBindingRemovedColumnsBox.Size = new System.Drawing.Size(199, 17);
            this.allowNullBindingRemovedColumnsBox.TabIndex = 77;
            this.allowNullBindingRemovedColumnsBox.Text = "Allow Null Binding Removed Columns";
            this.toolTip.SetToolTip(this.allowNullBindingRemovedColumnsBox, "Prevent any changes during scanning that would result in a changed target schema");
            this.allowNullBindingRemovedColumnsBox.UseVisualStyleBackColor = true;
            // 
            // filterOperatorLabel
            // 
            this.filterOperatorLabel.AutoSize = true;
            this.filterOperatorLabel.Location = new System.Drawing.Point(642, 49);
            this.filterOperatorLabel.Name = "filterOperatorLabel";
            this.filterOperatorLabel.Size = new System.Drawing.Size(73, 13);
            this.filterOperatorLabel.TabIndex = 76;
            this.filterOperatorLabel.Text = "Filter Operator";
            // 
            // filterOperatorComboBox
            // 
            this.filterOperatorComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterOperatorComboBox.FormattingEnabled = true;
            this.filterOperatorComboBox.Items.AddRange(new object[] {
            "=",
            "<",
            ">",
            "<>"});
            this.filterOperatorComboBox.Location = new System.Drawing.Point(645, 70);
            this.filterOperatorComboBox.Name = "filterOperatorComboBox";
            this.filterOperatorComboBox.Size = new System.Drawing.Size(146, 21);
            this.filterOperatorComboBox.TabIndex = 75;
            // 
            // ignoreCarriageReturnCheckBox
            // 
            this.ignoreCarriageReturnCheckBox.AutoSize = true;
            this.ignoreCarriageReturnCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ignoreCarriageReturnCheckBox.Location = new System.Drawing.Point(461, 243);
            this.ignoreCarriageReturnCheckBox.Name = "ignoreCarriageReturnCheckBox";
            this.ignoreCarriageReturnCheckBox.Size = new System.Drawing.Size(130, 17);
            this.ignoreCarriageReturnCheckBox.TabIndex = 74;
            this.ignoreCarriageReturnCheckBox.Text = "Ignore Carriage Return";
            this.toolTip.SetToolTip(this.ignoreCarriageReturnCheckBox, "Ignore Backslash - primarily for unclean data files.");
            this.ignoreCarriageReturnCheckBox.UseVisualStyleBackColor = true;
            // 
            // failUploadOnVarcharExpansionRadioButton
            // 
            this.failUploadOnVarcharExpansionRadioButton.AutoSize = true;
            this.failUploadOnVarcharExpansionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.failUploadOnVarcharExpansionRadioButton.Location = new System.Drawing.Point(171, 247);
            this.failUploadOnVarcharExpansionRadioButton.Name = "failUploadOnVarcharExpansionRadioButton";
            this.failUploadOnVarcharExpansionRadioButton.Size = new System.Drawing.Size(171, 17);
            this.failUploadOnVarcharExpansionRadioButton.TabIndex = 73;
            this.failUploadOnVarcharExpansionRadioButton.TabStop = true;
            this.failUploadOnVarcharExpansionRadioButton.Text = "Do not allow Width Expansions";
            this.failUploadOnVarcharExpansionRadioButton.UseVisualStyleBackColor = true;
            // 
            // allowValueTruncationOnLoadRadioButton
            // 
            this.allowValueTruncationOnLoadRadioButton.AutoSize = true;
            this.allowValueTruncationOnLoadRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowValueTruncationOnLoadRadioButton.Location = new System.Drawing.Point(28, 270);
            this.allowValueTruncationOnLoadRadioButton.Name = "allowValueTruncationOnLoadRadioButton";
            this.allowValueTruncationOnLoadRadioButton.Size = new System.Drawing.Size(242, 17);
            this.allowValueTruncationOnLoadRadioButton.TabIndex = 72;
            this.allowValueTruncationOnLoadRadioButton.TabStop = true;
            this.allowValueTruncationOnLoadRadioButton.Text = "Lock Width and Truncate values  if necessary";
            this.allowValueTruncationOnLoadRadioButton.UseVisualStyleBackColor = true;
            // 
            // allowVarcharExpansionRadioButton
            // 
            this.allowVarcharExpansionRadioButton.AutoSize = true;
            this.allowVarcharExpansionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowVarcharExpansionRadioButton.Location = new System.Drawing.Point(28, 247);
            this.allowVarcharExpansionRadioButton.Name = "allowVarcharExpansionRadioButton";
            this.allowVarcharExpansionRadioButton.Size = new System.Drawing.Size(137, 17);
            this.allowVarcharExpansionRadioButton.TabIndex = 71;
            this.allowVarcharExpansionRadioButton.TabStop = true;
            this.allowVarcharExpansionRadioButton.Text = "Allow Width Expansions";
            this.allowVarcharExpansionRadioButton.UseVisualStyleBackColor = true;
            // 
            // allowUpcastBox
            // 
            this.allowUpcastBox.AutoSize = true;
            this.allowUpcastBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowUpcastBox.Location = new System.Drawing.Point(28, 224);
            this.allowUpcastBox.Name = "allowUpcastBox";
            this.allowUpcastBox.Size = new System.Drawing.Size(126, 17);
            this.allowUpcastBox.TabIndex = 70;
            this.allowUpcastBox.Text = "Allow Type Upcasting";
            this.toolTip.SetToolTip(this.allowUpcastBox, "Prevent any changes during scanning that would result in a changed target schema");
            this.allowUpcastBox.UseVisualStyleBackColor = true;
            // 
            // allowAddColumnsBox
            // 
            this.allowAddColumnsBox.AutoSize = true;
            this.allowAddColumnsBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.allowAddColumnsBox.Location = new System.Drawing.Point(28, 201);
            this.allowAddColumnsBox.Name = "allowAddColumnsBox";
            this.allowAddColumnsBox.Size = new System.Drawing.Size(132, 17);
            this.allowAddColumnsBox.TabIndex = 69;
            this.allowAddColumnsBox.Text = "Allow Column Additions";
            this.toolTip.SetToolTip(this.allowAddColumnsBox, "Prevent any changes during scanning that would result in a changed target schema");
            this.allowAddColumnsBox.UseVisualStyleBackColor = true;
            // 
            // customUploadBox
            // 
            this.customUploadBox.AutoSize = true;
            this.customUploadBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customUploadBox.Location = new System.Drawing.Point(275, 179);
            this.customUploadBox.Name = "customUploadBox";
            this.customUploadBox.Size = new System.Drawing.Size(95, 17);
            this.customUploadBox.TabIndex = 68;
            this.customUploadBox.Text = "Custom Upload";
            this.toolTip.SetToolTip(this.customUploadBox, "Prevent any changes during scanning that would result in a changed target schema");
            this.customUploadBox.UseVisualStyleBackColor = true;
            // 
            // lblMinRows2
            // 
            this.lblMinRows2.AutoSize = true;
            this.lblMinRows2.Location = new System.Drawing.Point(572, 225);
            this.lblMinRows2.Name = "lblMinRows2";
            this.lblMinRows2.Size = new System.Drawing.Size(29, 13);
            this.lblMinRows2.TabIndex = 67;
            this.lblMinRows2.Text = "rows";
            // 
            // minimumRowsBox
            // 
            this.minimumRowsBox.Location = new System.Drawing.Point(515, 221);
            this.minimumRowsBox.Name = "minimumRowsBox";
            this.minimumRowsBox.Size = new System.Drawing.Size(51, 20);
            this.minimumRowsBox.TabIndex = 65;
            this.minimumRowsBox.Text = "0";
            // 
            // lblMinRows
            // 
            this.lblMinRows.AutoSize = true;
            this.lblMinRows.Location = new System.Drawing.Point(462, 224);
            this.lblMinRows.Name = "lblMinRows";
            this.lblMinRows.Size = new System.Drawing.Size(48, 13);
            this.lblMinRows.TabIndex = 66;
            this.lblMinRows.Text = "Minimum";
            // 
            // schemaLockBox
            // 
            this.schemaLockBox.AutoSize = true;
            this.schemaLockBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.schemaLockBox.Location = new System.Drawing.Point(5, 179);
            this.schemaLockBox.Name = "schemaLockBox";
            this.schemaLockBox.Size = new System.Drawing.Size(89, 17);
            this.schemaLockBox.TabIndex = 64;
            this.schemaLockBox.Text = "Schema Lock";
            this.toolTip.SetToolTip(this.schemaLockBox, "Prevent any changes during scanning that would result in a changed target schema");
            this.schemaLockBox.UseVisualStyleBackColor = true;
            this.schemaLockBox.CheckedChanged += new System.EventHandler(this.schemaLockBox_CheckedChanged);
            // 
            // lockFormatBox
            // 
            this.lockFormatBox.AutoSize = true;
            this.lockFormatBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lockFormatBox.Location = new System.Drawing.Point(173, 179);
            this.lockFormatBox.Name = "lockFormatBox";
            this.lockFormatBox.Size = new System.Drawing.Size(82, 17);
            this.lockFormatBox.TabIndex = 63;
            this.lockFormatBox.Text = "Lock Format";
            this.toolTip.SetToolTip(this.lockFormatBox, "Lock the format during scanning");
            this.lockFormatBox.UseVisualStyleBackColor = true;
            this.lockFormatBox.CheckedChanged += new System.EventHandler(this.lockFormatBox_CheckedChanged);
            // 
            // continueIfMissingCheckBox
            // 
            this.continueIfMissingCheckBox.AutoSize = true;
            this.continueIfMissingCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.continueIfMissingCheckBox.Location = new System.Drawing.Point(461, 205);
            this.continueIfMissingCheckBox.Name = "continueIfMissingCheckBox";
            this.continueIfMissingCheckBox.Size = new System.Drawing.Size(147, 17);
            this.continueIfMissingCheckBox.TabIndex = 62;
            this.continueIfMissingCheckBox.Text = "Continue loading if missing";
            this.toolTip.SetToolTip(this.continueIfMissingCheckBox, "Ignore Backslash - primarily for unclean data files.");
            this.continueIfMissingCheckBox.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "Input Time Zone";
            // 
            // InputTimeZoneComboBox
            // 
            this.InputTimeZoneComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.InputTimeZoneComboBox.FormattingEnabled = true;
            this.InputTimeZoneComboBox.Location = new System.Drawing.Point(94, 150);
            this.InputTimeZoneComboBox.Name = "InputTimeZoneComboBox";
            this.InputTimeZoneComboBox.Size = new System.Drawing.Size(276, 21);
            this.InputTimeZoneComboBox.TabIndex = 60;
            this.InputTimeZoneComboBox.DropDown += new System.EventHandler(this.TimeZoneComboBox_DropDown);
            // 
            // ignoreBackslashCheckBox
            // 
            this.ignoreBackslashCheckBox.AutoSize = true;
            this.ignoreBackslashCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ignoreBackslashCheckBox.Location = new System.Drawing.Point(461, 186);
            this.ignoreBackslashCheckBox.Name = "ignoreBackslashCheckBox";
            this.ignoreBackslashCheckBox.Size = new System.Drawing.Size(105, 17);
            this.ignoreBackslashCheckBox.TabIndex = 30;
            this.ignoreBackslashCheckBox.Text = "Ignore Backslash";
            this.toolTip.SetToolTip(this.ignoreBackslashCheckBox, "Ignore Backslash - primarily for unclean data files.");
            this.ignoreBackslashCheckBox.UseVisualStyleBackColor = true;
            // 
            // quoteAfterSeparator
            // 
            this.quoteAfterSeparator.AutoSize = true;
            this.quoteAfterSeparator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quoteAfterSeparator.Location = new System.Drawing.Point(461, 168);
            this.quoteAfterSeparator.Name = "quoteAfterSeparator";
            this.quoteAfterSeparator.Size = new System.Drawing.Size(145, 17);
            this.quoteAfterSeparator.TabIndex = 29;
            this.quoteAfterSeparator.Text = "Quote only after separator";
            this.toolTip.SetToolTip(this.quoteAfterSeparator, "Only consider quotes if they start after a separator (or beginning of line). If c" +
                    "hecked, quote characters in the middle of fields are treated like any other char" +
                    "acter.");
            this.quoteAfterSeparator.UseVisualStyleBackColor = true;
            // 
            // forceNumColumns
            // 
            this.forceNumColumns.AutoSize = true;
            this.forceNumColumns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.forceNumColumns.Location = new System.Drawing.Point(461, 150);
            this.forceNumColumns.Name = "forceNumColumns";
            this.forceNumColumns.Size = new System.Drawing.Size(142, 17);
            this.forceNumColumns.TabIndex = 28;
            this.forceNumColumns.Text = "Force number of columns";
            this.toolTip.SetToolTip(this.forceNumColumns, "Ignores end-of-line for every field except the last one - ensuring the right numb" +
                    "er of columns are found if carriage returns are not quoted.");
            this.forceNumColumns.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(240, 117);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(244, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "iso-8859-1 (ASCII/Default), Unicode (International)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "File Encoding";
            // 
            // fileEncodingTextBox
            // 
            this.fileEncodingTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileEncodingTextBox.Location = new System.Drawing.Point(94, 111);
            this.fileEncodingTextBox.Name = "fileEncodingTextBox";
            this.fileEncodingTextBox.Size = new System.Drawing.Size(139, 20);
            this.fileEncodingTextBox.TabIndex = 26;
            this.fileEncodingTextBox.Text = "iso-8859-1";
            // 
            // pasteNames
            // 
            this.pasteNames.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pasteNames.Location = new System.Drawing.Point(260, 76);
            this.pasteNames.Name = "pasteNames";
            this.pasteNames.Size = new System.Drawing.Size(110, 23);
            this.pasteNames.TabIndex = 24;
            this.pasteNames.Text = "Paste Names";
            this.pasteNames.UseVisualStyleBackColor = true;
            this.pasteNames.Click += new System.EventHandler(this.pasteNames_Click);
            // 
            // fileFormatTypeLbl
            // 
            this.fileFormatTypeLbl.AutoSize = true;
            this.fileFormatTypeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileFormatTypeLbl.Location = new System.Drawing.Point(3, 4);
            this.fileFormatTypeLbl.Name = "fileFormatTypeLbl";
            this.fileFormatTypeLbl.Size = new System.Drawing.Size(66, 13);
            this.fileFormatTypeLbl.TabIndex = 2;
            this.fileFormatTypeLbl.Text = "Format Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(458, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Escapes";
            this.toolTip.SetToolTip(this.label8, resources.GetString("label8.ToolTip"));
            // 
            // searchPatternLbl
            // 
            this.searchPatternLbl.AutoSize = true;
            this.searchPatternLbl.Location = new System.Drawing.Point(3, 78);
            this.searchPatternLbl.Name = "searchPatternLbl";
            this.searchPatternLbl.Size = new System.Drawing.Size(78, 13);
            this.searchPatternLbl.TabIndex = 10;
            this.searchPatternLbl.Text = "Search Pattern";
            // 
            // escapeGrid
            // 
            this.escapeGrid.AllowUserToResizeRows = false;
            this.escapeGrid.AutoGenerateColumns = false;
            this.escapeGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.escapeGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.escapeGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.escapeGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.escapeGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.escapeGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.escapeDataGridViewTextBoxColumn,
            this.replacementDataGridViewTextBoxColumn});
            this.escapeGrid.DataMember = "Escapes";
            this.escapeGrid.DataSource = this.dataSet;
            this.escapeGrid.Location = new System.Drawing.Point(461, 46);
            this.escapeGrid.MultiSelect = false;
            this.escapeGrid.Name = "escapeGrid";
            this.escapeGrid.RowHeadersVisible = false;
            this.escapeGrid.RowHeadersWidth = 20;
            this.escapeGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.escapeGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.escapeGrid.RowTemplate.Height = 18;
            this.escapeGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.escapeGrid.ShowEditingIcon = false;
            this.escapeGrid.ShowRowErrors = false;
            this.escapeGrid.Size = new System.Drawing.Size(169, 70);
            this.escapeGrid.TabIndex = 22;
            // 
            // escapeDataGridViewTextBoxColumn
            // 
            this.escapeDataGridViewTextBoxColumn.DataPropertyName = "Escape";
            this.escapeDataGridViewTextBoxColumn.HeaderText = "Escape";
            this.escapeDataGridViewTextBoxColumn.Name = "escapeDataGridViewTextBoxColumn";
            // 
            // replacementDataGridViewTextBoxColumn
            // 
            this.replacementDataGridViewTextBoxColumn.DataPropertyName = "Replacement";
            this.replacementDataGridViewTextBoxColumn.HeaderText = "Replacement";
            this.replacementDataGridViewTextBoxColumn.Name = "replacementDataGridViewTextBoxColumn";
            // 
            // dataSet
            // 
            this.dataSet.DataSetName = "NewDataSet";
            this.dataSet.Tables.AddRange(new System.Data.DataTable[] {
            this.escapesTable});
            // 
            // escapesTable
            // 
            this.escapesTable.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.escapesTable.TableName = "Escapes";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Escape";
            this.dataColumn1.DefaultValue = "";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Replacement";
            this.dataColumn2.DefaultValue = "";
            // 
            // fixedWidthRadioButton
            // 
            this.fixedWidthRadioButton.AutoSize = true;
            this.fixedWidthRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fixedWidthRadioButton.Location = new System.Drawing.Point(6, 20);
            this.fixedWidthRadioButton.Name = "fixedWidthRadioButton";
            this.fixedWidthRadioButton.Size = new System.Drawing.Size(80, 17);
            this.fixedWidthRadioButton.TabIndex = 1;
            this.fixedWidthRadioButton.TabStop = true;
            this.fixedWidthRadioButton.Text = "Fixed Width";
            this.fixedWidthRadioButton.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(642, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Filter rows using column";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(376, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "rows";
            // 
            // ignoreLastRowsBox
            // 
            this.ignoreLastRowsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ignoreLastRowsBox.Location = new System.Drawing.Point(319, 51);
            this.ignoreLastRowsBox.Name = "ignoreLastRowsBox";
            this.ignoreLastRowsBox.Size = new System.Drawing.Size(51, 20);
            this.ignoreLastRowsBox.TabIndex = 12;
            this.ignoreLastRowsBox.Text = "0";
            // 
            // rowFilterValue
            // 
            this.rowFilterValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rowFilterValue.Location = new System.Drawing.Point(645, 113);
            this.rowFilterValue.Name = "rowFilterValue";
            this.rowFilterValue.Size = new System.Drawing.Size(146, 20);
            this.rowFilterValue.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(257, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Ignore last";
            // 
            // searchPatternTextBox
            // 
            this.searchPatternTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchPatternTextBox.Location = new System.Drawing.Point(94, 76);
            this.searchPatternTextBox.Name = "searchPatternTextBox";
            this.searchPatternTextBox.Size = new System.Drawing.Size(139, 20);
            this.searchPatternTextBox.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(257, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ignore first";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(642, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Value to filter for";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(376, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "rows";
            // 
            // containsHeaders
            // 
            this.containsHeaders.AutoSize = true;
            this.containsHeaders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.containsHeaders.Location = new System.Drawing.Point(260, 26);
            this.containsHeaders.Name = "containsHeaders";
            this.containsHeaders.Size = new System.Drawing.Size(190, 17);
            this.containsHeaders.TabIndex = 6;
            this.containsHeaders.Text = "First readable row contains headers";
            this.containsHeaders.UseVisualStyleBackColor = true;
            // 
            // delimitedByRadioButton
            // 
            this.delimitedByRadioButton.AutoSize = true;
            this.delimitedByRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delimitedByRadioButton.Location = new System.Drawing.Point(6, 43);
            this.delimitedByRadioButton.Name = "delimitedByRadioButton";
            this.delimitedByRadioButton.Size = new System.Drawing.Size(82, 17);
            this.delimitedByRadioButton.TabIndex = 3;
            this.delimitedByRadioButton.TabStop = true;
            this.delimitedByRadioButton.Text = "Delimited By";
            this.delimitedByRadioButton.UseVisualStyleBackColor = true;
            // 
            // filterColumnBox
            // 
            this.filterColumnBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filterColumnBox.FormattingEnabled = true;
            this.filterColumnBox.Location = new System.Drawing.Point(645, 25);
            this.filterColumnBox.Name = "filterColumnBox";
            this.filterColumnBox.Size = new System.Drawing.Size(146, 21);
            this.filterColumnBox.TabIndex = 18;
            // 
            // quoteTextBox
            // 
            this.quoteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.quoteTextBox.Location = new System.Drawing.Point(548, 2);
            this.quoteTextBox.MaxLength = 4;
            this.quoteTextBox.Name = "quoteTextBox";
            this.quoteTextBox.Size = new System.Drawing.Size(20, 20);
            this.quoteTextBox.TabIndex = 15;
            this.quoteTextBox.Text = "\"";
            // 
            // delimiterTxt
            // 
            this.delimiterTxt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.delimiterTxt.Location = new System.Drawing.Point(94, 43);
            this.delimiterTxt.MaxLength = 4;
            this.delimiterTxt.Name = "delimiterTxt";
            this.delimiterTxt.Size = new System.Drawing.Size(30, 20);
            this.delimiterTxt.TabIndex = 4;
            this.delimiterTxt.Text = ",";
            // 
            // ignoreRowsBox
            // 
            this.ignoreRowsBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ignoreRowsBox.Location = new System.Drawing.Point(319, 2);
            this.ignoreRowsBox.Name = "ignoreRowsBox";
            this.ignoreRowsBox.Size = new System.Drawing.Size(51, 20);
            this.ignoreRowsBox.TabIndex = 7;
            this.ignoreRowsBox.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(458, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Quote character";
            // 
            // sourceFileGridView
            // 
            this.sourceFileGridView.AllowDrop = true;
            this.sourceFileGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sourceFileGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sourceFileGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.DataType,
            this.ColWidth,
            this.Format,
            this.Multiplier,
            this.Encrypted,
            this.PreventUpdate});
            this.sourceFileGridView.Location = new System.Drawing.Point(3, 313);
            this.sourceFileGridView.Name = "sourceFileGridView";
            this.sourceFileGridView.Size = new System.Drawing.Size(838, 391);
            this.sourceFileGridView.TabIndex = 0;
            this.sourceFileGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.sourceFileGridView_CellBeginEdit);
            this.sourceFileGridView.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.sourceFileGridView_CellMouseDown);
            this.sourceFileGridView.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.sourceFileGridView_UserDeletingRow);
            this.sourceFileGridView.DragDrop += new System.Windows.Forms.DragEventHandler(this.sourceFileGridView_DragDrop);
            this.sourceFileGridView.DragEnter += new System.Windows.Forms.DragEventHandler(this.sourceFileGridView_DragEnter);
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "ColumnName";
            this.ColumnName.HeaderText = "Column Name";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.Width = 250;
            // 
            // DataType
            // 
            this.DataType.DataPropertyName = "DataType";
            this.DataType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DataType.HeaderText = "Data Type";
            this.DataType.Items.AddRange(new object[] {
            "None",
            "Number",
            "Integer",
            "Varchar",
            "Date",
            "DateTime",
            "Float"});
            this.DataType.Name = "DataType";
            this.DataType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DataType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DataType.Width = 150;
            // 
            // ColWidth
            // 
            this.ColWidth.DataPropertyName = "Width";
            this.ColWidth.HeaderText = "Width";
            this.ColWidth.Name = "ColWidth";
            this.ColWidth.ToolTipText = "Width of varchar";
            // 
            // Format
            // 
            this.Format.DataPropertyName = "Format";
            this.Format.HeaderText = "Format";
            this.Format.Name = "Format";
            this.Format.ToolTipText = resources.GetString("Format.ToolTipText");
            // 
            // Multiplier
            // 
            this.Multiplier.DataPropertyName = "Multiplier";
            this.Multiplier.HeaderText = "Multiply By";
            this.Multiplier.Name = "Multiplier";
            this.Multiplier.ToolTipText = "Amount to multiply an integer or float by when processing (float value - e.g. 0.0" +
                "1)";
            // 
            // Encrypted
            // 
            this.Encrypted.DataPropertyName = "Encrypted";
            this.Encrypted.HeaderText = "Encrypted";
            this.Encrypted.Name = "Encrypted";
            this.Encrypted.ToolTipText = "Column should be encrypted when loaded into the database";
            // 
            // PreventUpdate
            // 
            this.PreventUpdate.DataPropertyName = "PreventUpdate";
            this.PreventUpdate.HeaderText = "Prevent Update";
            this.PreventUpdate.Name = "PreventUpdate";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.sourceFilePage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(852, 733);
            this.tabControl1.TabIndex = 0;
            // 
            // sourceFileMenuStrip
            // 
            this.sourceFileMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSourceFileStripMenuItem,
            this.importSourceFileToolStripMenuItem,
            this.updateSourceFileToolStripMenuItem,
            this.removeSourceFileToolStripMenuItem});
            this.sourceFileMenuStrip.Name = "sourceFileMenuStrip";
            this.sourceFileMenuStrip.Size = new System.Drawing.Size(184, 92);
            // 
            // newSourceFileStripMenuItem
            // 
            this.newSourceFileStripMenuItem.Name = "newSourceFileStripMenuItem";
            this.newSourceFileStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.newSourceFileStripMenuItem.Text = "&New Data Source";
            this.newSourceFileStripMenuItem.Click += new System.EventHandler(this.newSourceFileStripMenuItem_Click);
            // 
            // importSourceFileToolStripMenuItem
            // 
            this.importSourceFileToolStripMenuItem.Name = "importSourceFileToolStripMenuItem";
            this.importSourceFileToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.importSourceFileToolStripMenuItem.Text = "&Import Data Source";
            this.importSourceFileToolStripMenuItem.Click += new System.EventHandler(this.importSourceFileToolStripMenuItem_Click);
            // 
            // updateSourceFileToolStripMenuItem
            // 
            this.updateSourceFileToolStripMenuItem.Name = "updateSourceFileToolStripMenuItem";
            this.updateSourceFileToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.updateSourceFileToolStripMenuItem.Text = "&Update Data Source";
            this.updateSourceFileToolStripMenuItem.Click += new System.EventHandler(this.updateSourceFileToolStripMenuItem_Click);
            // 
            // removeSourceFileToolStripMenuItem
            // 
            this.removeSourceFileToolStripMenuItem.Name = "removeSourceFileToolStripMenuItem";
            this.removeSourceFileToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.removeSourceFileToolStripMenuItem.Text = "&Remove Data Source";
            this.removeSourceFileToolStripMenuItem.Click += new System.EventHandler(this.removeSourceFileToolStripMenuItem_Click);
            // 
            // importSourceFileDialog
            // 
            this.importSourceFileDialog.DefaultExt = "txt";
            this.importSourceFileDialog.FileName = "file.txt";
            this.importSourceFileDialog.Title = "Import Data Source";
            // 
            // replaceWithNullRadioButton
            // 
            this.replaceWithNullRadioButton.AutoSize = true;
            this.replaceWithNullRadioButton.Location = new System.Drawing.Point(461, 270);
            this.replaceWithNullRadioButton.Name = "replaceWithNullRadioButton";
            this.replaceWithNullRadioButton.Size = new System.Drawing.Size(111, 17);
            this.replaceWithNullRadioButton.TabIndex = 78;
            this.replaceWithNullRadioButton.TabStop = true;
            this.replaceWithNullRadioButton.Text = "Replace With Null";
            this.replaceWithNullRadioButton.UseVisualStyleBackColor = true;
            // 
            // skipRecordRadioButton
            // 
            this.skipRecordRadioButton.AutoSize = true;
            this.skipRecordRadioButton.Location = new System.Drawing.Point(575, 270);
            this.skipRecordRadioButton.Name = "skipRecordRadioButton";
            this.skipRecordRadioButton.Size = new System.Drawing.Size(84, 17);
            this.skipRecordRadioButton.TabIndex = 79;
            this.skipRecordRadioButton.TabStop = true;
            this.skipRecordRadioButton.Text = "Skip Record";
            this.skipRecordRadioButton.UseVisualStyleBackColor = true;
            // 
            // SourceFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(852, 733);
            this.Controls.Add(this.tabControl1);
            this.Name = "SourceFiles";
            this.Text = "SourceFiles";
            this.sourceFilePage.ResumeLayout(false);
            this.sourceFilePanel.ResumeLayout(false);
            this.sourceFilePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.escapeGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.escapesTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceFileGridView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.sourceFileMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage sourceFilePage;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ContextMenuStrip sourceFileMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem importSourceFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateSourceFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSourceFileToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog importSourceFileDialog;
        private System.Windows.Forms.DataGridView sourceFileGridView;
        private System.Windows.Forms.RadioButton fixedWidthRadioButton;
        private System.Windows.Forms.Label fileFormatTypeLbl;
        private System.Windows.Forms.RadioButton delimitedByRadioButton;
        private System.Windows.Forms.TextBox delimiterTxt;
        private System.Windows.Forms.ToolStripMenuItem newSourceFileStripMenuItem;
        private System.Windows.Forms.TextBox ignoreRowsBox;
        private System.Windows.Forms.CheckBox containsHeaders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label searchPatternLbl;
        private System.Windows.Forms.TextBox searchPatternTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ignoreLastRowsBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox quoteTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox filterColumnBox;
        private System.Windows.Forms.TextBox rowFilterValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Data.DataSet dataSet;
        private System.Windows.Forms.Panel sourceFilePanel;
        private System.Data.DataTable escapesTable;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Windows.Forms.DataGridView escapeGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn escapeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn replacementDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button pasteNames;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox fileEncodingTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.CheckBox quoteAfterSeparator;
        private System.Windows.Forms.CheckBox forceNumColumns;
        private System.Windows.Forms.CheckBox ignoreBackslashCheckBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox InputTimeZoneComboBox;
        private System.Windows.Forms.CheckBox continueIfMissingCheckBox;
        private System.Windows.Forms.CheckBox schemaLockBox;
        private System.Windows.Forms.CheckBox lockFormatBox;
        private System.Windows.Forms.Label lblMinRows;
        private System.Windows.Forms.Label lblMinRows2;
        private System.Windows.Forms.TextBox minimumRowsBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewComboBoxColumn DataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn Format;
        private System.Windows.Forms.DataGridViewTextBoxColumn Multiplier;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Encrypted;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PreventUpdate;
        private System.Windows.Forms.CheckBox customUploadBox;
        private System.Windows.Forms.CheckBox allowAddColumnsBox;
        private System.Windows.Forms.CheckBox allowUpcastBox;
        private System.Windows.Forms.RadioButton allowVarcharExpansionRadioButton;
        private System.Windows.Forms.RadioButton allowValueTruncationOnLoadRadioButton;
        private System.Windows.Forms.RadioButton failUploadOnVarcharExpansionRadioButton;
        private System.Windows.Forms.CheckBox ignoreCarriageReturnCheckBox;
        private System.Windows.Forms.Label filterOperatorLabel;
        private System.Windows.Forms.ComboBox filterOperatorComboBox;
        private System.Windows.Forms.CheckBox allowNullBindingRemovedColumnsBox;
        private System.Windows.Forms.RadioButton skipRecordRadioButton;
        private System.Windows.Forms.RadioButton replaceWithNullRadioButton;
    }
}