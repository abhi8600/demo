package com.birst.webservices;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.birst.queueing.exceptions.InvalidQueueException;
import com.birst.queueing.exceptions.UserNotTrustedException;
import com.birst.queueing.queueconsumer.QueueConsumptionHandler;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;

public class Util {

	private static final Logger logger = Logger.getLogger(Util.class);
	
	public static void validateTrustedRequest(HttpServletRequest request) throws UserNotTrustedException{
		String error = validateTrustedInternalRequest(request.getRemoteAddr());
		if(error != null && error.trim().length() > 0)
		{
			throw new UserNotTrustedException();
		}
	}
	
	public static String validateTrustedInternalRequest(String requestIP)
	{	
		StringBuilder sb = new StringBuilder();
		// check for trusted sender
		List<String> loginAllowedIPs = QueueConsumptionHandler.getTrustedLoginAllowedIps();
		if (loginAllowedIPs != null)
		{
			if (!Net.containIp(loginAllowedIPs, requestIP))
			{
				logger.error("ConsumerApp : Request IP not allowed: bad request IP: " + requestIP);				
				sb.append("Request IP not allowed: bad request IP");
				return sb.toString();
			}
			if (logger.isTraceEnabled())
				logger.trace("SchedulerAccess: Valid request from host " + requestIP);
		} else
		{
			logger.error("No valid IPs list for Trusted Login Service.");			
			sb.append("No valid IPs list for Trusted Login Service.");
			return sb.toString();
		}
		
		return null;
	}
	
	public static Queue getQueue(String name) throws InvalidQueueException{
		try
		{
			return Queue.valueOf(name);
		}
		catch(IllegalArgumentException ex){
			throw new InvalidQueueException("Invalid queue name  : " + name);		
		}
	}

	public static void sleepThread(long timeInMilliseconds) {
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (InterruptedException ex) {
			logger.error(ex, ex);
		}
	}

}
