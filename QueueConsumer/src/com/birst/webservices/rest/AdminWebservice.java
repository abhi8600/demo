package com.birst.webservices.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.birst.queueing.QueueDetails;
import com.birst.queueing.queueconsumer.QueueConsumptionHandler;
import com.birst.webservices.Util;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;

@Path("/admin/")
public class AdminWebservice {

	private static final Logger logger = Logger.getLogger(AdminWebservice.class);
	@Context private HttpServletRequest request;
	@Context private ServletContext context;


	@POST
	@Path("consumers/start")
	public Response startAllConsumerThreads(){
		Util.validateTrustedRequest(request);
		try{		
			QueueConsumptionHandler.getInstance().startAllConsumers();
			QueueConsumptionHandler.getInstance().startAllTopicPublishers();
			QueueConsumptionHandler.getInstance().startQueueHealthCheck();
		}catch(Exception ex){
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}

	@POST
	@Path("consumers/stop")
	public Response stopAllConsumerThreads(){
		Util.validateTrustedRequest(request);
		try{
			QueueConsumptionHandler.getInstance().stopAllConsumers();
			QueueConsumptionHandler.getInstance().stopHealthCheck();
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}

	@POST
	@Path("consumer/start/{consumerName}")
	public Response startConsumer(@PathParam("consumerName") String consumerName){
		try{
			QueueConsumptionHandler.getInstance().restartConsumption(Util.getQueue(consumerName));
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}


	@POST
	@Path("consumer/stop/{consumerName}")
	public Response stopConsumer(@PathParam("consumerName") String consumerName){
		Util.validateTrustedRequest(request);
		try{
			QueueConsumptionHandler.getInstance().stopConsumer(Util.getQueue(consumerName));
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}


	@POST
	@Path("consumer/start/{consumerName}/threads/{numberThreads}")
	public Response setConsumerThreads(@PathParam("consumerName") String consumerName, @PathParam("numberThreads") int numThreads){
		Util.validateTrustedRequest(request);
		try{
			QueueConsumptionHandler.getInstance().resetConsumers(Util.getQueue(consumerName), numThreads);
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}


	@GET
	@Path("consumer/threads/{consumerName}")
	public Response getConsumerThreads(@PathParam("consumerName") String consumerName){
		Util.validateTrustedRequest(request);
		try{
			int numConsumers = QueueConsumptionHandler.getInstance().getNumberOfConsumersRunning(Util.getQueue(consumerName));
			return Response.ok(numConsumers).build();
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}		
	}

	@POST
	@Path("consumers/start/threads/{numberThreads}")
	public Response setAllConsumerThreads(@PathParam("numberThreads") int numThreads){
		Util.validateTrustedRequest(request);
		try{
			QueueConsumptionHandler.getInstance().resetAllConsumers(numThreads);
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return Response.ok().build();
	}


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("consumers/threads")
	public Response getAllConsumerThreads(){
		Util.validateTrustedRequest(request);
		try{
			List<QueueDetails> list = QueueConsumptionHandler.getInstance().getNumberOfConsumersRunning();			
			GenericEntity<List<QueueDetails>> entity = new GenericEntity<List<QueueDetails>>(list){};
			return Response.ok(entity).build();
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GET
	@Path("consumers/maintenancemode")
	public Response hasAllConsumersStopped(){
		Util.validateTrustedRequest(request);
		try{
			QueueConsumptionHandler.getInstance().reapDeadConsumerThreads();
			List<QueueDetails> list = QueueConsumptionHandler.getInstance().getNumberOfConsumersRunning();
			boolean activeConsumers = list != null && list.size() > 0; 
			if(!activeConsumers){
				list = QueueConsumptionHandler.getInstance().getNumberOfActiveConsumersInStopMode();
				activeConsumers = list != null && list.size() > 0;
			}
			return Response.ok(Boolean.toString(!activeConsumers)).build();
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GET
	@Path("consumer/{consumerName}/maintenancemode")
	public Response hasConsumersStopped(@PathParam("consumerName") String consumerName){
		Util.validateTrustedRequest(request);
		try{
			Queue queue = Util.getQueue(consumerName);
			QueueConsumptionHandler.getInstance().reapDeadConsumerThreads(queue);			
			int activeSize = QueueConsumptionHandler.getInstance().getNumberOfConsumersRunning(queue);
			boolean activeConsumers = activeSize > 0; 
			if(!activeConsumers){
				activeSize = QueueConsumptionHandler.getInstance().getNumberOfActiveConsumersInStopMode(queue);
				activeConsumers = activeSize > 0;
			}
			return Response.ok(Boolean.toString(!activeConsumers)).build();
		}catch(Exception ex){
			logger.error(ex, ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
