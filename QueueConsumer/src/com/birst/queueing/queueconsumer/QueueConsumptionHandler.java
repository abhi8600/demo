package com.birst.queueing.queueconsumer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.birst.queueing.QueueDetails;
import com.birst.queueing.database.Database;
import com.birst.queueing.exceptions.ProcessorException;
import com.birst.queueing.queueconsumer.processors.Processor;
import com.birst.queueing.queueconsumer.processors.ProcessorFactory;
import com.birst.queueing.utils.QueueHealthChecker;
import com.successmetricsinc.queueing.producer.QueuePublishManager;
import com.successmetricsinc.queueing.qClient.BrokerCredentials;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.QClientConnectionFactory;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class QueueConsumptionHandler implements ServletContextListener
{
	private static final String APP_LOGIN_ALLOWED_IPS = "CustomerApp.AllowedIPs";
	private static List<String> trustedInternalAllowedIPs;
	private Map<Queue, Integer> queueToNoOfThreadsMap = new HashMap<Queue, Integer>();
	private Map<Queue, GenericQueueConsumer[]> queueToConsumersMap = new HashMap<Queue, GenericQueueConsumer[]>();
	private Map<Queue, List<GenericQueueConsumer>> queueConsumersInStopModeMap = new HashMap<Queue, List<GenericQueueConsumer>>();
	private static QueueConsumptionHandler hndlr;
	private static final String QUEUE_CONSUMER = "QueueConsumer";
	private static final String TOPIC_PUBLISER = "TopicPublisher";
	private static final Logger logger = Logger.getLogger(QueueConsumptionHandler.class);
	private List<Topic> publisherTopics = new ArrayList<Topic>();
	private List<Queue> initializedQueues = new ArrayList<Queue>();
	private static String log4jFilePath = null;
	private static QueueHealthChecker healthChecker;
	private static QueuePublishManager queuePublishManager;
	
	public boolean configureConsumptionForType(Queue queue, int noThreads)
	{
		queueToNoOfThreadsMap.put(queue, noThreads);
		queueToConsumersMap.put(queue, null);
		return true;
	}
	
	public boolean startConsumption(Queue queue) throws ProcessorException
	{
		if (queueToNoOfThreadsMap.containsKey(queue))
		{
			int noThreads = queueToNoOfThreadsMap.get(queue);
			GenericQueueConsumer[] consumers = new GenericQueueConsumer[noThreads];
			for (int i=0; i<noThreads; i++)
			{
				Processor processor = ProcessorFactory.getProcessor(queue);
				processor.setId(String.valueOf(i));
				consumers[i] = new GenericQueueConsumer(queue, processor);
				consumers[i].setName(queue.name() + " - "+ i);
				consumers[i].start();
			}
			queueToConsumersMap.put(queue, consumers);
			return true;
		}
		else
		{
			return false;
		}
	}


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public static String getLog4jFilePath()
	{
		return log4jFilePath;
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try{
		String consumerAppHome = System.getProperty("consumerApp.home");
		logger.debug("Context Initializing - consumerApp.home=" + consumerAppHome);
		
		ServletContext context = sce.getServletContext();
		
		log4jFilePath = context.getRealPath("/WEB-INF/classes/log4j.xml");
		
		InputStream in = null;
		Properties buildProperties = new Properties();
		try
		{
			in = context.getResourceAsStream("build.number");
			if (in != null)
			{
				buildProperties.load(in);
				logger.debug("Using properties file: build.number (found via the classpath)");
			}
		} catch (IOException e)
		{
			logger.debug("classpath properties file (build.number) not found.");
		} finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				} catch (Exception e)
				{
				}
			}
		}
		// Report results of search
		if (buildProperties.isEmpty())
		{
			logger.debug("Properties file (build.number) not found or empty after full search was performed.");
		} else
		{
			if (logger.isDebugEnabled())
				printProperties(buildProperties);
		}
		
		File consumerProperties = new File(consumerAppHome + File.separator + "conf" + File.separator + "consumer.properties");
		Properties configProperties = new Properties();
		if (consumerProperties.exists())
		{
			logger.debug("Overriding consumer configuration properties from " + consumerProperties);
			try
			{
				configProperties.load(new FileReader(consumerProperties));
			}catch (IOException ioe)
			{
				logger.error("problem loading consumer.properties - " + ioe.getMessage(), ioe);
			}
		}
		
		Database.DBType = getParameter("DBType", configProperties, context);
		Database.DBDriver = getParameter("DBDriver", configProperties, context);
		Database.DBConnectString = getParameter("DBConnectString", configProperties, context);
		Database.DBAdminUser = getParameter("DBAdminUser", configProperties, context);
		Database.DBAdminPwd = getParameter("DBAdminPwd", configProperties, context);
		
		String BrokerHost = getParameter("BrokerHost", configProperties, context);
		int BrokerPort = Integer.parseInt(getParameter("BrokerPort", configProperties, context));
		BrokerCredentials brokerCredentials = new BrokerCredentials(BrokerHost, BrokerPort);
		QClientConnectionFactory.setUpDefaultCredentials(brokerCredentials);
		
		setLoginAllowedIps(context);
		
		hndlr = new QueueConsumptionHandler();
		queuePublishManager = new QueuePublishManager();
		String queueConsumer = getParameter(QUEUE_CONSUMER, configProperties, context);
		if (queueConsumer != null && !queueConsumer.isEmpty())
		{
			String[] consumptionConfig = queueConsumer.split(",");
			List<Queue> queues = Arrays.asList(Queue.values());
			for (String keyVal : consumptionConfig)
			{
				String[] cconfig = keyVal.split(":");
				if (cconfig.length == 2)
				{
					Queue queue = Queue.valueOf(cconfig[0]);
					int numThreads = Integer.parseInt(cconfig[1]);
					if (queues.contains(queue))
					{
						hndlr.configureConsumptionForType(queue, numThreads);
						hndlr.initializedQueues.add(queue);						
					}
				}
			}
		}
		
		String topicPublisher = getParameter(TOPIC_PUBLISER, configProperties, context);
		if (topicPublisher != null && !topicPublisher.isEmpty())
		{
			String[] topicPublishConfig = topicPublisher.split(",");
			List<Topic> topics = Arrays.asList(Topic.values());
			for (String topicStr : topicPublishConfig)
			{
				Topic topic = Topic.valueOf(topicStr);
				if (topics.contains(topic))
				{
					queuePublishManager.configurePublishersForTopic(topic, 1);						
					hndlr.publisherTopics.add(topic);					
				}				
			}
		}

		hndlr.startQueueHealthCheck();
		hndlr.startAllConsumption();
		hndlr.startAllTopicPublishers();		
		}catch(Exception ex){
			logger.fatal("Exception while initialization of Consumer App " , ex);
		}
	}

	private void startAllConsumption() throws ProcessorException{
		if(this.initializedQueues != null && this.initializedQueues.size() > 0){
			for(Queue queue : initializedQueues){
				startConsumption(queue);
			}
		}
	}
	
	public void startQueueHealthCheck(){
		if(healthChecker == null){
			healthChecker = new QueueHealthChecker();
		}
		healthChecker.init();
		healthChecker.run();
	}
	
	public void stopHealthCheck(){
		if(healthChecker != null){
			healthChecker.stop();
		}
	}	
	
	private static void setLoginAllowedIps(ServletContext context)
	{		
		String ips = getInitParameter(context, APP_LOGIN_ALLOWED_IPS);
		if (ips != null)
		{
			StringTokenizer st = new StringTokenizer(ips, ", ");
			trustedInternalAllowedIPs = new LinkedList<String>();
			while (st.hasMoreTokens())
			{
				String ip = st.nextToken();
				trustedInternalAllowedIPs.add(ip);
			}
		}
	}
	
	private static String getInitParameter(ServletContext context, String paramName){
		return context.getInitParameter(paramName);
	}
	
	public static List<String> getTrustedLoginAllowedIps() {		
		return trustedInternalAllowedIPs;
	}
	
	public static QueueConsumptionHandler getInstance(){
		return hndlr;
	}
	
	public synchronized void startAllConsumers() throws ProcessorException{
		for(Entry<Queue, Integer> entry : queueToNoOfThreadsMap.entrySet()){
			resetConsumers(entry.getKey(), entry.getValue());			
		}
	}
	
	public synchronized  void stopAllConsumers(){
		for(Entry<Queue,GenericQueueConsumer[]> entry : queueToConsumersMap.entrySet()){			
			Queue queue = entry.getKey();
			stopConsumer(queue);
		}
	}
	
	public synchronized  void stopConsumer(Queue queue){		
		GenericQueueConsumer[] consumers = this.getQueueConsumers(queue);
		if(consumers != null && consumers.length > 0){
			for(GenericQueueConsumer consumer : consumers){
				stopGenericConsumer(queue,consumer);				
			}
		}
		this.addRunningQueueConsumers(queue, null);
	}
	
	private void startGenericConsumer(GenericQueueConsumer consumer){
		if(consumer != null){
			consumer.start();
		}
	}
	
	private void stopGenericConsumer(Queue queue, GenericQueueConsumer consumer){
		if(consumer != null){
			consumer.stopConsumption();
			this.addToConsumersInStopModeMap(queue, consumer);
		}
	}
	
	private void initStopModeConsumersMap(Queue queue){
		if(!queueConsumersInStopModeMap.containsKey(queue)){
			queueConsumersInStopModeMap.put(queue, new ArrayList<GenericQueueConsumer>());			
		}
	}
	
	private void addToConsumersInStopModeMap(Queue queue, GenericQueueConsumer consumer){
		initStopModeConsumersMap(queue);
		queueConsumersInStopModeMap.get(queue).add(consumer);
	}
	
	public synchronized void restartConsumption(Queue queue){		
		GenericQueueConsumer[] consumers = getQueueConsumers(queue);
		if(consumers != null && consumers.length > 0){
			for(GenericQueueConsumer consumer : consumers){
				if(!consumer.isAlive()){
					startGenericConsumer(consumer);
				}
			}
		}
	}
	
	private GenericQueueConsumer[] getQueueConsumers(Queue queue){
		return queueToConsumersMap.get(queue);
	}
	
	public List<QueueDetails> getNumberOfConsumersRunning(){
		List<QueueDetails> response = new ArrayList<QueueDetails>();
		for(Queue queue : queueToConsumersMap.keySet()){
			int num = getNumberOfConsumersRunning(queue);
			if(num > 0){
				response.add(new QueueDetails(queue,num));
			}
		}
		return response;
	}
	
	public int getNumberOfConsumersRunning(Queue queue) {
		int active = 0;
		GenericQueueConsumer[] consumers = getQueueConsumers(queue);
		if(consumers != null){
			for(GenericQueueConsumer consumer : consumers){
				if(consumer != null && consumer.isAlive()){
					if(!consumer.isAvailable()){
						continue;
					}
					active++;
				}
			}
		}
		return active;
	}
	
	public synchronized void resetAllConsumers(Queue queue) throws ProcessorException{
		resetAllConsumers(this.queueToNoOfThreadsMap.get(queue));
	}
	
	public synchronized void resetAllConsumers(int num) throws ProcessorException {
		for(Queue queue : queueToConsumersMap.keySet()){
			resetConsumers(queue, num);
		}
	}
	
	public synchronized void resetConsumers(Queue queue, int num) throws ProcessorException {		
		GenericQueueConsumer[] consumers = getQueueConsumers(queue);
		if(consumers != null && consumers.length > 0){
			List<GenericQueueConsumer> existingConsumers = Arrays.asList(consumers);
			List<GenericQueueConsumer> running = new ArrayList<GenericQueueConsumer>();			
			for(GenericQueueConsumer consumer : consumers){				
				if(consumer.isAlive() && consumer.isAvailable()){
					running.add(consumer);
				}				
			}

			int runningSize = running.size();
			if(num == runningSize){
				return;
			}

			if(num > runningSize){
				for(int i=runningSize; i < num; i++){
					Processor processor = ProcessorFactory.getProcessor(queue);
					processor.setId(String.valueOf(i));
					GenericQueueConsumer consumer = new GenericQueueConsumer(queue, processor);
					consumer.setName(queue.name() + " - "+ i);
					running.add(consumer);
					consumer.start();
				}
			}
			else{
				for(GenericQueueConsumer existingConsumer : existingConsumers){
					if(runningSize > num){
						stopGenericConsumer(queue, existingConsumer);
						runningSize--;
					}else
					{
						break;
					}
				}
			}

			// reap all dead threads
			reapDeadConsumerThreads(queue);
			addRunningQueueConsumers(queue, (GenericQueueConsumer[])running.toArray(new GenericQueueConsumer[running.size()]));
		}
		else
		{
			configureConsumptionForType(queue, num);
			startConsumption(queue);
		}		
	}
	
	private void addRunningQueueConsumers(Queue queue, GenericQueueConsumer[] consumers){		
		queueToConsumersMap.put(queue, consumers);
	}
	
	public synchronized void reapDeadConsumerThreads(){
		for(Queue queue : this.queueConsumersInStopModeMap.keySet()){
			reapDeadConsumerThreads(queue);
		}
	}
	
	public synchronized void reapDeadConsumerThreads(Queue queue){
		List<GenericQueueConsumer> list = getConsumersInStopMode(queue);
		if(list != null && list.size() > 0){
			Iterator<GenericQueueConsumer> iter = list.iterator();
			while(iter.hasNext()){
				GenericQueueConsumer toReapConsumer = iter.next();
				if(!toReapConsumer.isAlive()){
					iter.remove();					
				}
			}
		}
	}
	
	public List<QueueDetails> getNumberOfActiveConsumersInStopMode(){
		List<QueueDetails> response = new ArrayList<QueueDetails>();
		for(Entry<Queue, List<GenericQueueConsumer>> entry : queueConsumersInStopModeMap.entrySet()){			
			List<GenericQueueConsumer> consumers = getConsumersInStopMode(entry.getKey());
			if(consumers != null && consumers.size() > 0){				
				response.add(new QueueDetails(entry.getKey(), consumers.size()));
			}
		}		
		return response;
	}
	
	public int getNumberOfActiveConsumersInStopMode(Queue queue){
		List<GenericQueueConsumer> consumers = getConsumersInStopMode(queue);
		if(consumers != null ){				
			return consumers.size();
		}	
		return -1;
	}

	public List<GenericQueueConsumer> getConsumersInStopMode(Queue queue){
		return this.queueConsumersInStopModeMap.get(queue);
	}

	public void startAllTopicPublishers() throws MessengerException{
		if(this.publisherTopics != null && publisherTopics.size() > 0){
			for(Topic topic : publisherTopics){
				queuePublishManager.closePublishers(topic);
				queuePublishManager.initializePublishers(topic);
			}
		}
	}
	
	/**
	 * dump the properties to the log file
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}
	
	private String getParameter(String parameterName, Properties configProperties, ServletContext context)
	{
		if (parameterName == null)
			return null;
		if (configProperties.containsKey(parameterName))
			return configProperties.getProperty(parameterName);
		return context.getInitParameter(parameterName);
	}
	
	public static QueuePublishManager getQueuePublishManager(){
		return queuePublishManager;
	}
}
