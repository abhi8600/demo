package com.birst.queueing.queueconsumer.processors;

import com.birst.queueing.exceptions.ProcessorException;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;

public class ProcessorFactory {

	public static Processor getProcessor(Queue queue) throws ProcessorException{
		if(queue == Queue.NOW_REPORT){
			return new AsynchReportProcessor();
		}
		
		throw new ProcessorException("No processor found for queue");
	}
}
