package com.birst.queueing.queueconsumer.processors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.log4j.Logger;

import com.birst.queueing.database.Database;
import com.birst.queueing.database.ProcessEngineInfo;
import com.birst.queueing.exceptions.ProcessorException;
import com.birst.queueing.queueconsumer.QueueConsumptionHandler;
import com.birst.queueing.utils.CommandUtil;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.producer.QueuePublishManager;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;

public class AsynchReportProcessor implements Processor {

	private static final Logger logger = Logger.getLogger(AsynchReportProcessor.class);
	
	private String consumerIndex;
	
	@Override
	public void consume(Message message) throws ProcessorException {
		Message resultMessage = new Message();		
		try
		{
			resultMessage.setId(message.getId());
			processMessage(message);
		}
		catch (ProcessorException ex)
		{
			logger.error(ex.toString(), ex);
		}
		finally
		{
			try {
				QueueConsumptionHandler.getQueuePublishManager().publishMessage(Topic.NOW_REPORTGEN_RESULT, resultMessage, 2);
			} catch (MessengerException e) {
				throw new ProcessorException("Unable to publish the result to the topic ", e);
			}
		}
	}
	
	private void processMessage(Message message) throws ProcessorException{		
		int exitValue = -1;
		File asyncCommandFile = null;		
		try
		{
			String releaseNumber = message.getReleaseNumber();
			ProcessEngineInfo peInfo = Database.getProcessEngineInfoForRelease(releaseNumber);
			if (peInfo == null)
			{
				String[] parts = releaseNumber.split("\\.");//strip off dot release
				if (parts.length > 2)
				{
					releaseNumber = parts[0] + "." + parts[1];
					peInfo = Database.getProcessEngineInfoForRelease(releaseNumber);
				}
			}
			if (peInfo == null)
			{
				throw new Exception("");
			}
			String engineCommand = peInfo.getEngineCommand();
			String engineHomeBin = engineCommand.substring(0, engineCommand.lastIndexOf(File.separator));
			String runAsyncCommand = CommandUtil.getRunAsyncTaskCommand(message, consumerIndex);
			asyncCommandFile = File.createTempFile("Async", ".cmd", new File(engineHomeBin));
			BufferedWriter bw = new BufferedWriter(new FileWriter(asyncCommandFile));
			bw.write(runAsyncCommand);
			bw.flush();
			bw.close();
			exitValue = CommandUtil.runCommand("cKey", "\"" + engineHomeBin + File.separator + CommandUtil.RUM_ASYNC_TASK_BATCH_FILE_NAME + "\" \"" + System.getProperty("consumerApp.home") + "\" \"" + QueueConsumptionHandler.getLog4jFilePath() + "\" -c " + asyncCommandFile.getName(), engineHomeBin, null, null);
		}
		catch(Exception ex)
		{	
			logger.error("Problem encountered in running async report generation operation: ", ex);					
		}
		finally
		{
			if (asyncCommandFile != null && asyncCommandFile.exists())
				asyncCommandFile.delete();
			if (CommandUtil.isSuccessfulExit(exitValue))
			{
				// For asynch reporting, the result data is the output file name
				File resultFile = new File(message.getResultData());
				if (!resultFile.exists())
				{
					throw new ProcessorException("Results file not found for capturing async report generation operation");
				}						
			}
			else
			{
				throw new ProcessorException("Problem encountered in running async report generation operation");
			}					
		}
	}
	

	@Override
	public void setId(String id) {
		this.consumerIndex = id;
	}

}
