package com.birst.queueing.queueconsumer.processors;

import com.birst.queueing.exceptions.ProcessorException;
import com.successmetricsinc.queueing.Message;

public interface Processor {

	public void setId(String id);
	public void consume(Message message) throws ProcessorException;
}
