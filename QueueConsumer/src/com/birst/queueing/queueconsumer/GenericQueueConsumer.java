package com.birst.queueing.queueconsumer;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import com.birst.queueing.queueconsumer.processors.Processor;
import com.birst.queueing.utils.LogUtils;
import com.birst.webservices.Util;
import com.netflix.servo.monitor.Counter;
import com.netflix.servo.monitor.Monitors;
import com.netflix.servo.monitor.Stopwatch;
import com.netflix.servo.monitor.Timer;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.Consumer;
import com.successmetricsinc.queueing.qClient.MessengerFactory;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;

public class GenericQueueConsumer extends Thread {
	
	private static final Logger logger = Logger.getLogger(GenericQueueConsumer.class);
	private boolean cancel = false;
	private Queue queue;
	private Consumer consumer;	
	private static final long SLEEP_RETRIEVING_MSG = 5000;
	private static final int RETRY_COUNT_LIMIT = 5;
	private final TimeUnit timeUnitToUse = TimeUnit.MICROSECONDS;
	private final Counter messagesReceived = Monitors.newCounter("messagesReceived");
	private final Timer   messageProcessingTime = Monitors.newTimer("messageProcessingTime", timeUnitToUse);
	private Processor processor;
	
	public GenericQueueConsumer(Queue queue, Processor processor)
	{
		this.queue = queue;
		this.processor = processor;		
		Monitors.registerObject(this.getName(), this);
	}
	
	@Override
	public void run()
	{ 
		try
		{
			boolean started = false;		
			try {
				consumer = MessengerFactory.getConsumer(queue);
				started = true;
			} catch (MessengerException ex) {
				logger.error("Exception in starting consumer " , ex);
			}

			logger.info("QueueConsumer for queue " + queue.toString() + " started.");

			if(started){

				int retryOnExceptionCount = 0;
				while (!cancel)
				{	
					Message message = null;
					try
					{
						message = consumer.retreiveNextMessage(SLEEP_RETRIEVING_MSG);
						retryOnExceptionCount = 0;
					}
					catch(Exception ex)
					{					
						retryOnExceptionCount++;
						if(retryOnExceptionCount > RETRY_COUNT_LIMIT)
						{
							logger.error("Retry limit reached for consumer errors. Disabling consumer ", ex);
							this.stopConsumption();
						}
						else
						{
							try 
							{
								Util.sleepThread(5000);
								consumer = MessengerFactory.getConsumer(queue);
							} catch (MessengerException ex2) 
							{							
								if(retryOnExceptionCount == RETRY_COUNT_LIMIT){
									// show the entire log message
									logger.error("Error on Last Retry : " + retryOnExceptionCount + " ", ex);
								}
								else{
									logger.error("Retry Count : " + retryOnExceptionCount + " : Error : " + ex2.getMessage());
								}
							} 
						}
					}

					Stopwatch stopwatch = null;
					try
					{	
						if(message != null){							
							stopwatch = messageProcessingTime.start();
							messagesReceived.increment();
							LogUtils.logMDC(message.getUserName(), message.getSpaceId(), message.getId());							
							processor.consume(message);
						}			
					}
					catch(Exception ex)
					{
						// This exception should not trigger resetting of consumer
						// since this is post retrieval i.e. consumer is fine
						logger.error(ex.toString(), ex);						
					}
					finally {						
						LogUtils.removeMDC();
						if (stopwatch != null) {
							stopwatch.stop();
						}
					}

				}
				logger.info("QueueConsumer for queue " + queue.toString() + " stopped.");
			}
			else
			{
				logger.error("Unable to start QueueConsumer for queue " + queue.toString());
			}	
		}
		finally
		{
			finish();
		}
	}

	public void finish(){
		try{
			if(consumer != null){
				consumer.close();
			}
		}
		catch(Exception ex){
			logger.error("Error while closing consumer resources ", ex);
		}
	}	
	
	public void stopConsumption()
	{
		cancel = true;		
	}
	
	public boolean isAvailable(){
		return !cancel;
	}
}
