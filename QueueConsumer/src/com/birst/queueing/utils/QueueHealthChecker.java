package com.birst.queueing.utils;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.birst.queueing.queueconsumer.QueueConsumptionHandler;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.monitor.HealthCheckThread;
import com.successmetricsinc.queueing.qClient.monitor.HealthResultListener;
import com.successmetricsinc.queueing.qClient.monitor.HeathCheckResult.HealthStatus;

public class QueueHealthChecker implements HealthResultListener{

	private static final Logger logger = Logger.getLogger(QueueHealthChecker.class);
	private HealthCheckThread healthCheckListener;
	private Thread healthCheckThread;
	private static AtomicInteger counter = new AtomicInteger();
	
	public void init(){
		healthCheckListener = new HealthCheckThread(-1);
		healthCheckListener.addHealthResultListener(this);
	}

	public void run(){
		if(healthCheckThread == null || !healthCheckThread.isAlive()){
			healthCheckThread = new Thread(healthCheckListener);
			healthCheckThread.setDaemon(true);
			healthCheckThread.setName("HealthCheck-" + counter.incrementAndGet());
			healthCheckThread.start();
		}
		healthCheckListener.changeRunState(true);
	}

	@Override
	public void onStateChange(HealthStatus newHealthStatus,
			HealthStatus oldHealthStatus) {
		try{
			if(newHealthStatus == HealthStatus.WORKING && oldHealthStatus == HealthStatus.DOWN){
				QueueConsumptionHandler.getInstance().startAllConsumers();
				QueueConsumptionHandler.getInstance().startAllTopicPublishers();
			}
		}
		catch(Exception ex){
			logger.warn("HealthChecker error on state change : " + ex.getMessage());
		}
	}
	
	public void stop(){
		healthCheckListener.changeRunState(false);
	}
	
}
