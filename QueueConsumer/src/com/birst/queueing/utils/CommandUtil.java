package com.birst.queueing.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.birst.queueing.exceptions.InvalidCommandException;
import com.successmetricsinc.queueing.Message;

public class CommandUtil
{
	public static final int COMMAND_INVALID = -1000;
	public static final int COMMAND_SUCCESS = 0;		
	
	public static final long TIMEOUT_PROC_OUTPUT_MILLIS = 1*60*60*1000; // 1 hour
	
	private static final Logger logger = Logger.getLogger(CommandUtil.class);
	
	public static final String KEY_COMMAND_UID = "uid";
	
	public static final String RUM_ASYNC_TASK_BATCH_FILE_NAME = "runAsyncTask.bat";
	public static final String CMD_PERFORM_ASYNC_TASK = "performasynctask";
	public static final String ASYNC_REPORT_GENERATOR_CLASS = "com.successmetricsinc.queuing.AsyncReportGenerator";
	
	public static String getRunAsyncTaskCommand(Message message, String consumerIndex) throws InvalidCommandException
	{
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(CMD_PERFORM_ASYNC_TASK);
		cmdBuilder.addAsyncClass(ASYNC_REPORT_GENERATOR_CLASS);
		cmdBuilder.addConsumerIndex(consumerIndex);
		cmdBuilder.addMessageId(message.getId());				
		cmdBuilder.addUserName(message.getUserName());
		cmdBuilder.addSpaceID(message.getSpaceId());
		cmdBuilder.addInputData(message.getInputData());
		cmdBuilder.addResultData(message.getResultData());
		return cmdBuilder.getCommand();
	}
	
	public static boolean isFileExists(String filePath)
	{
		try
		{
			File file = new File(filePath);
			if(file.exists())
			{
				return true;
			}
		}catch(Exception ex)
		{
			logger.warn("Exception while checking for existence of file :" , ex);
		}
		return false;	
	}

	public static Properties getProps(String filePath)
	{	
		FileReader reader = null;
		Properties props = null;
		try
		{
			reader = new FileReader(filePath);
			props = new Properties();
			props.load(reader);
		} catch (IOException e)
		{
			logger.error("Exception while loading properties from file " + filePath, e);
		}
		finally
		{
			if(reader != null)
			{
				try
				{
					reader.close();
				} catch (IOException e)
				{	
				}
			}
		}
		
		return props;
	}
	
	public static int runCommand(String cmdKey, String cmd, String workingDirectory, Map<String, Process> runningProcesses) throws IOException, InterruptedException
	{
		return runCommand(cmdKey, cmd, workingDirectory, runningProcesses, null);
	}
	
	public static int runCommand(String cmdKey, String cmd, String workingDirectory, Map<String, Process> runningProcesses,
			CommandProcessHandler handlr) throws IOException, InterruptedException
	{	
		if(cmd == null)
		{
			logger.error("No command to run " + cmdKey);
			return CommandUtil.COMMAND_INVALID;
		}
		
		Process proc = null;
		boolean exitedGracefully = false;
		try
		{
			logger.info("Running command " + cmd);
			Runtime rt = Runtime.getRuntime();
					
			File file = workingDirectory != null ? new File(workingDirectory) : null;
			proc = rt.exec(cmd, null, file);
			if(runningProcesses != null)
			{
				runningProcesses.put(cmdKey, proc);
			}
			
			StreamFetchThread errorStream = new StreamFetchThread(proc.getErrorStream(), cmdKey + " ERROR", null);
			StreamFetchThread outputStream = new StreamFetchThread(proc.getInputStream(), cmdKey + " OUTPUT", null);
			errorStream.start();
			outputStream.start();
			if(handlr != null){
				handlr.logInfo();
			}
			int exitValue = proc.waitFor();
			exitedGracefully = true;
			logger.info("Exit value for command " + cmd + " : " + exitValue);
			errorStream.join(TIMEOUT_PROC_OUTPUT_MILLIS);
			outputStream.join(TIMEOUT_PROC_OUTPUT_MILLIS);
			return exitValue;
		}		
		finally
		{
			if(!exitedGracefully && proc != null)
			{
				proc.destroy();
			}

			if(runningProcesses != null && runningProcesses.containsKey(cmdKey))
			{	
				runningProcesses.remove(cmdKey);
			}
		}
	}

	public static boolean isSuccessfulExit(int returnCode)
	{
		return returnCode == CommandUtil.COMMAND_SUCCESS;
	}
	
}
