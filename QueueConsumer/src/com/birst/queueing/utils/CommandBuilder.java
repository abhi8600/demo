package com.birst.queueing.utils;


public class CommandBuilder
{
	private StringBuilder builder;
	
	public CommandBuilder()
	{
		builder = new StringBuilder();
	}
	
	public static CommandBuilder getCommandBuilder()
	{
		return new CommandBuilder();
	}
	
	public String getCommand()
	{
		return builder.toString();
	}
	
	private String getQuotedString(String str)
	{
		String result = str;
		if (str != null && str.length() > 0)
		{
			result = str.trim();
			if (!result.startsWith("\"") && !result.endsWith("\""))
			{
				result = "\"" + result + "\"";
			}
		}
		return result;
	}
	
	public void add(String str)
	{
		addToBuilder(str);
	}
	
	private void addToBuilder(String str)
	{		
		builder.append(str);
		builder.append(" ");
	}

	public void addAsyncClass(String asyncClass)
	{
		addToBuilder("asyncclass=" + asyncClass);
	}
	
	public void addConsumerIndex(String consumerIndex)
	{
		addToBuilder("consumerindex=" + consumerIndex);
	}
	
	public void addMessageId(String msgID)
	{
		addToBuilder("msgid=" + msgID);
	}
	
	public void addUserName(String userName)
	{
		addToBuilder("username=" + userName);
	}
	
	public void addSpaceID(String spaceID)
	{
		addToBuilder("spaceid=" + spaceID);
	}
	
	public void addInputData(String inputData)
	{
		addToBuilder(getQuotedString("inputdata=" + inputData));
	}
	
	public void addResultData(String resultData)
	{
		addToBuilder(getQuotedString("resultdata=" + resultData));
	}
	
}
