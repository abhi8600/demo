package com.birst.queueing.utils;

import org.apache.log4j.MDC;

public class LogUtils {

	public static void logMDC(String userName, String spaceId, String messageId){
		MDC.put("username", userName);
		MDC.put("spaceID", spaceId);
		MDC.put("messageID", messageId);
	}

	public static void removeMDC() {
		MDC.remove("username");
		MDC.remove("spaceID");
		MDC.remove("messageID");
	}
}
