package com.birst.queueing.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class StreamFetchThread extends Thread
{
	public static final Logger logger = Logger.getLogger(StreamFetchThread.class);
	InputStream is;
	String type;
	String filterLine;
	List<String> linesBasedOnFilter;
	
	public StreamFetchThread(InputStream is , String type, String filterLine)
	{
		this.is = is;
		this.type = type;
		this.filterLine = filterLine;
		if(this.filterLine != null){
			linesBasedOnFilter = new ArrayList<String>();
		}
	}

	public void run()
	{	
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while((line = br.readLine()) != null)
			{
				logger.info(type + "> " + line);
				if(filterLine != null && line.indexOf(filterLine) >=0 ){
					linesBasedOnFilter.add(line);
				}
			}
		} catch (IOException ex)
		{
			logger.error("Exception in StreamFetchThread ", ex);
		}
	}
	
	public List<String> getLinesBasedOnFilter(){
		return linesBasedOnFilter;
	}
}
