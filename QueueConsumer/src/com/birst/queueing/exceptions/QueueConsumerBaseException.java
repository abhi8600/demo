package com.birst.queueing.exceptions;

public class QueueConsumerBaseException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int ERROR_OTHER = -1;
	public static final int ERROR_SCHEDULER_NOT_RUNNING = -2;
	public static final int ERROR_OBJECT_UNAVAILABLE = -3;
	public static final int ERROR_VALIDATION = -4;
	public static final int ERROR_JOB_RUNNING = -5;
	public static final int ERROR_NOT_LOGGED_IN = -6;
	public static final int ERROR_INVALID_CREDENTIALS = -7;
	public static final int ERROR_INVALID_COMMAND = -8;
	public static final int ERROR_INVALID_TRUSTED_IP = -9;
	public static final int ERROR_CONCURRENT_SPACE_OP = -10;
	public static final int ERROR_JOB_TYPE_NOT_FOUND = -11;
	public static final int ERROR_DUPLICATE_JOB_NAME = -12;
	public static final int ERROR_DUPLICATE_SCHEDULE = -13;
	public static final int ERROR_DUPLICATE_SCHEDULE_COPY = -14;
	public static final int ERROR_INVALID_QUEUE_NAME = -15;
	public static final int ERROR_PROCESSOR_ERROR = 16;
	
	public QueueConsumerBaseException() {
		super();
	}
	
	public QueueConsumerBaseException(String s) {
		super(s);
	}
	
	public QueueConsumerBaseException(String s, Throwable cause) {
		super(s, cause);
	}
	
	public int getErrorCode()
	{
		return ERROR_OTHER;
	}
	
	public String getErrorMessage()
	{
		return super.getMessage();
	}
}
