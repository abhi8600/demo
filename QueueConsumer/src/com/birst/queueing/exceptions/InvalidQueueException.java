package com.birst.queueing.exceptions;

public class InvalidQueueException extends QueueConsumerBaseException {

	
	private static final long serialVersionUID = 1L;
	
	public InvalidQueueException() {
		super();
	}
	
	public InvalidQueueException(String s) {
		super(s);
	}
	
	public InvalidQueueException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_INVALID_QUEUE_NAME;
	}
}
