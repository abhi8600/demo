package com.birst.queueing.exceptions;

public class InvalidCommandException extends QueueConsumerBaseException
{
	
	private static final long serialVersionUID = 1L;
	
	public InvalidCommandException() {
		super();
	}
	
	public InvalidCommandException(String s) {
		super(s);
	}
	
	public InvalidCommandException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_INVALID_COMMAND;
	}

}
