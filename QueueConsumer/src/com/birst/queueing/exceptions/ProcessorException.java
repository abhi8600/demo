package com.birst.queueing.exceptions;

public class ProcessorException extends QueueConsumerBaseException {
	
	private static final long serialVersionUID = 1L;
	
	public ProcessorException() {
		super();
	}
	
	public ProcessorException(String s) {
		super(s);
	}
	
	public ProcessorException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_PROCESSOR_ERROR;
	}
}
