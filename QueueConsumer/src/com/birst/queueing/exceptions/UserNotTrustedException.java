package com.birst.queueing.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class UserNotTrustedException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserNotTrustedException(){
		super(Status.UNAUTHORIZED);
	}
}
