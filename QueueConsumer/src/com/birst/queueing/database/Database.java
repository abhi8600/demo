package com.birst.queueing.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class Database {

	private static final Logger logger = Logger.getLogger(Database.class);
	
	public static String DBType;
	public static String DBDriver;
	public static String DBConnectString;
	public static String DBAdminUser;
	public static String DBAdminPwd;
	
	
	private static final String mainSchema = "dbo";
	private static final String PROCESS_VERSIONS_INFO_TABLE = "PROCESS_VERSIONS_INFO";
	private static final String SPACES_TABLE = "SPACES";
	
	public static void closePreparedStatement(PreparedStatement pstmt)
	{
		if (pstmt != null)
		{
			try
			{
				pstmt.close();
			} catch (SQLException e)
			{
				logger.warn("Exception during pstmt.close()", e);
			}
		}
	}
	
	public static ProcessEngineInfo getProcessEngineInfoForRelease(String releaseNumber) throws Exception
	{
		ConnectionPool cp = ConnectionPool.getInstance(DBDriver, DBConnectString, DBAdminUser, DBAdminPwd, 0, DBType);
		Connection conn = cp.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ProcessEngineInfo peInfo = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT ID, NAME, RELEASE, ENGINECOMMAND, DELETECOMMAND, VISIBLE, SUPPORTSFDCEXTRACT, SUPPORTSRETRYLOAD, ISPROCESSINGGROUPAWARE "
										+ "FROM " + mainSchema + "." + PROCESS_VERSIONS_INFO_TABLE + " WHERE RELEASE = ?");
			pstmt.setString(1, releaseNumber);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				peInfo = new ProcessEngineInfo();
				peInfo.setId(rs.getInt(1));
				peInfo.setName(rs.getString(2));
				peInfo.setRelease(rs.getString(3));
				peInfo.setEngineCommand(rs.getString(4));
				peInfo.setDeleteCommand(rs.getString(5));
				peInfo.setVisible(rs.getBoolean(6));
				peInfo.setSupportSFDCExtract(rs.getBoolean(7));
				peInfo.setSupportsRetryLoad(rs.getBoolean(8));
				peInfo.setProcessingGroupAware(rs.getBoolean(9));
			}
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing resultset - " + sqle.getMessage(), sqle);
				}
			}
			closePreparedStatement(pstmt);
		}
		return peInfo;
	}
	
	public static int getProcessVersionIDForSpace(String spaceID) throws Exception
	{
		ConnectionPool cp = ConnectionPool.getInstance(DBDriver, DBConnectString, DBAdminUser, DBAdminPwd, 0, DBType);
		Connection conn = cp.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int processVersionID = -1;
		try
		{
			pstmt = conn.prepareStatement("SELECT PROCESSVERSION FROM " + mainSchema + "." + SPACES_TABLE + " WHERE ID = ?");
			pstmt.setString(1, spaceID);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				processVersionID = rs.getInt(1);
			}
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing resultset - " + sqle.getMessage(), sqle);
				}
			}
			closePreparedStatement(pstmt);
		}
		return processVersionID;
	}
	
	public static ProcessEngineInfo getProcessEngineInfoForProcessVersion(int processVersionID) throws Exception
	{
		ConnectionPool cp = ConnectionPool.getInstance(DBDriver, DBConnectString, DBAdminUser, DBAdminPwd, 0, DBType);
		Connection conn = cp.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ProcessEngineInfo peInfo = new ProcessEngineInfo();
		try
		{
			pstmt = conn.prepareStatement("SELECT ID, NAME, RELEASE, ENGINECOMMAND, DELETECOMMAND, VISIBLE, SUPPORTSFDCEXTRACT, SUPPORTSRETRYLOAD, ISPROCESSINGGROUPAWARE "
										+ "FROM " + mainSchema + "." + PROCESS_VERSIONS_INFO_TABLE + " WHERE ID = ?");
			pstmt.setInt(1, processVersionID);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				peInfo.setId(rs.getInt(1));
				peInfo.setName(rs.getString(2));
				peInfo.setRelease(rs.getString(3));
				peInfo.setEngineCommand(rs.getString(4));
				peInfo.setDeleteCommand(rs.getString(5));
				peInfo.setVisible(rs.getBoolean(6));
				peInfo.setSupportSFDCExtract(rs.getBoolean(7));
				peInfo.setSupportsRetryLoad(rs.getBoolean(8));
				peInfo.setProcessingGroupAware(rs.getBoolean(9));
			}
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing resultset - " + sqle.getMessage(), sqle);
				}
			}
			closePreparedStatement(pstmt);
		}
		return peInfo;
	}
}
