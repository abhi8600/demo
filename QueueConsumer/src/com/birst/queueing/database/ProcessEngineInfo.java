package com.birst.queueing.database;

public class ProcessEngineInfo {
	private int id;
	private String name;
	private String release;
	private String engineCommand;
	private String deleteCommand;
	private boolean visible;
	private boolean supportSFDCExtract;
	private boolean supportsRetryLoad;
	private boolean processingGroupAware;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRelease() {
		return release;
	}
	public void setRelease(String release) {
		this.release = release;
	}
	public String getEngineCommand() {
		return engineCommand;
	}
	public void setEngineCommand(String engineCommand) {
		this.engineCommand = engineCommand;
	}
	public String getDeleteCommand() {
		return deleteCommand;
	}
	public void setDeleteCommand(String deleteCommand) {
		this.deleteCommand = deleteCommand;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public boolean isSupportSFDCExtract() {
		return supportSFDCExtract;
	}
	public void setSupportSFDCExtract(boolean supportSFDCExtract) {
		this.supportSFDCExtract = supportSFDCExtract;
	}
	public boolean isSupportsRetryLoad() {
		return supportsRetryLoad;
	}
	public void setSupportsRetryLoad(boolean supportsRetryLoad) {
		this.supportsRetryLoad = supportsRetryLoad;
	}
	public boolean isProcessingGroupAware() {
		return processingGroupAware;
	}
	public void setProcessingGroupAware(boolean processingGroupAware) {
		this.processingGroupAware = processingGroupAware;
	}
}
