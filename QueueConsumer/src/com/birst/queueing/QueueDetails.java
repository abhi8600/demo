package com.birst.queueing;

import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;

public class QueueDetails {
	
	private String queueName;
	private int numConsumers;
	
	public QueueDetails(Queue queue, int numConsumers){
		this.queueName = queue.toString();;
		this.numConsumers = numConsumers;
	}
	
	public String getQueueName(){
		return queueName;
	}
	
	public int getNumConsumers(){
		return numConsumers;
	}
}
