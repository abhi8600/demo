package com.birst.ati.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.birst.ati.MailSender;

/**
 * It loads ATI configuration
 * 
 * @author jshah
 */
public class ATIConfig {
	private static final Logger logger = Logger.getLogger(MailSender.class);

	public static ATIConfigBean loadConfiguration() {
		ATIConfigBean atiConfigBean = new ATIConfigBean();
		String filePathForConfig = ".\\config\\ATCTestNG.config";
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(filePathForConfig));
			atiConfigBean.setEmailToList(props.getProperty("EMAIL_TO_LIST"));
			atiConfigBean.setEmailCCList(props.getProperty("EMAIL_CC_LIST"));
			atiConfigBean.setEmailSubPrefix(props.getProperty("EMAIL_SUB_PREF"));
			atiConfigBean.setEmailFrom(props.getProperty("EMAIL_FROM"));
			atiConfigBean.setEmailServer(props.getProperty("EMAIL_SERVER"));
			atiConfigBean.setHostName(props.getProperty("HOST_NAME"));
			atiConfigBean.setNumOfDecPointDiffAllowed(props.getProperty("NUM_OF_DEC_POINT_DIFF_ALLOWED"));
			atiConfigBean.setAtiXMLFile(props.getProperty("ATI_XML_FILE"));
		} catch (IOException e) {
			System.out.println(e);
			logger.error("Unable to read configuration file");
			logger.error(e.getMessage());
		}
		return atiConfigBean;
	}
}
