package com.birst.ati.config;

/**
 * Setter/Getter for ATI configuration
 * 
 * @author jshah
 */
public class ATIConfigBean {
	private String email_to_list;
	private String email_cc_list;
	private String email_sub_prefix;
	private String email_from;
	private String email_server;
	private String host_name;
	private String num_of_dec_point_diff_allowed;
	private String atiXMLFile;

	public String getAtiXMLFile() {
		return atiXMLFile;
	}

	public void setAtiXMLFile(String atiXMLFile) {
		this.atiXMLFile = atiXMLFile;
	}

	public String getEmailToList() {
		return email_to_list;
	}
	
	public String getEmailCCList() {
		return email_cc_list;
	}

	public String getEmailSubPrefix() {
		return email_sub_prefix;
	}

	public String getEmailFrom() {
		return email_from;
	}

	public String getEmailServer() {
		return email_server;
	}

	public String getHostName() {
		return host_name;
	}

	public String getNumOfDecPointDiffAllowed() {
		return num_of_dec_point_diff_allowed;
	}

	public void setEmailToList(String email_to_list) {
		this.email_to_list = email_to_list;
	}
	
	public void setEmailCCList(String email_cc_list) {
		this.email_cc_list = email_cc_list;
	}

	public void setEmailSubPrefix(String email_sub_prefix) {
		this.email_sub_prefix = email_sub_prefix;
	}

	public void setEmailFrom(String email_from) {
		this.email_from = email_from;
	}

	public void setEmailServer(String email_server) {
		this.email_server = email_server;
	}

	public void setHostName(String host_name) {
		this.host_name = host_name;
	}

	public void setNumOfDecPointDiffAllowed(String num) {
		num_of_dec_point_diff_allowed = num;
	}
}
