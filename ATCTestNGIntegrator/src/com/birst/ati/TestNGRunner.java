package com.birst.ati;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.birst.ati.config.ATIConfig;
import com.birst.ati.config.ATIConfigBean;
import com.birst.ati.util.Util;

public class TestNGRunner {
	private static List<Object[]> testList = new ArrayList<Object[]>();

	private static final Logger logger = Logger.getLogger(TestNGRunner.class);
	protected static final String FAILED_QUERY_FILE = "FailedQueryNames.txt";
	
	/**
	 * 
	 * @param testSuiteName
	 * @param testName
	 * @return
	 */
	public static List<Object[]> getTestList(String testSuiteName, String testName) {
		List<Object[]> dataForTestSuite = new ArrayList<Object[]>();
		for (Object[] arr : testList) {
			if (arr[0] != null && arr[0].equals(testSuiteName)) {
				String tempTestName = arr[1] + "(" + arr[2] + ")";
				if (tempTestName.equals(testName)) {
					dataForTestSuite.add(arr);
				}
			}
		}
		return dataForTestSuite;
	}

	public static void main(String[] args) {
		ATIConfigBean atiConfigBean = ATIConfig.loadConfiguration();
		TestNGRunner testNGRunner = new TestNGRunner();
		try {
			File f = new File(FAILED_QUERY_FILE);
			if(f.exists())
			{
				f.delete();
			}
			f.createNewFile();
			List<TestMeta> atcTestsToRun = Util.getListofTestSuites(atiConfigBean.getAtiXMLFile());
			List<XmlSuite> suites = new ArrayList<XmlSuite>();

			List<String> uniquetestSuites = new ArrayList<String>();
			for (TestMeta testMeta : atcTestsToRun) {
				if (!uniquetestSuites.contains(testMeta.getTestSuiteName())) {
					uniquetestSuites.add(testMeta.getTestSuiteName());
				}
			}
			for (String testSuite : uniquetestSuites) {
				XmlSuite suite = new XmlSuite();
				suite.setName(testSuite);

				for (TestMeta testMeta : atcTestsToRun) {
					if (testMeta.getTestSuiteName().equals(testSuite)) {
						String suiteName = testMeta.getTestSuiteName();
						String fileName = testNGRunner.getFileName(testMeta.getExpectedFile());
						Map<String, List<String>> actualResultByQuery = testNGRunner.getQueryResult(testMeta.getActualFile());
						Map<String, List<String>> expectedResultByQuery = testNGRunner.getQueryResult(testMeta.getExpectedFile());

						for (Map.Entry<String, List<String>> entry : actualResultByQuery.entrySet()) {
							testList.add(new Object[] { suiteName, fileName, entry.getKey(), entry.getValue(), expectedResultByQuery.get(entry.getKey()) , testMeta.isIgnoreCase() });
							XmlTest test = new XmlTest(suite);
							test.setName(fileName + "(" + entry.getKey() + ")");
							List<XmlClass> classes = new ArrayList<XmlClass>();
							classes.add(new XmlClass(ATCTestIntegrator.class));
							test.setClasses(classes);
						}
					}
				}
				suites.add(suite);
			}

			TestNG testng = new TestNG();
			testng.setTestClasses(new Class[] { ATCTestIntegrator.class });
			testng.setXmlSuites(suites);
			testng.run();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			String logFilePath = Util.getLogFilePath();
			if (logFilePath == null) {
				logger.error("Could not find valid log file path. Will not be able to send email with attachments.");
			}
			ATCTestIntegrator.logSuccessfulTestSuites();
			MailSender mailSender = new MailSender(atiConfigBean);
			mailSender.sendEmail(true, "Please find attached ATC TestNG Integration Results.", logFilePath);
		}
	}

	/**
	 * returns fileName from absolute path
	 * 
	 * @param folderPath
	 * @return
	 */
	private String getFileName(String folderPath) {
		return (folderPath.substring(folderPath.lastIndexOf("\\") + 1));
	}

	/**
	 * It parses file based on query number and returns result query by query.
	 * 
	 * @param actualResultsFolder
	 * @return Map<String, List<String>>:- query number (Ex, q0001) would be key
	 *         and result would be in List
	 */
	private Map<String, List<String>> getQueryResult(String resultFile) {
		Map<String, List<String>> resultByQuery = new LinkedHashMap<String, List<String>>();
		Pattern pattern = Pattern.compile("q(\\d)*(_)*(\\d)*");
		BufferedReader br = null;
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(resultFile));

			String key = null;
			List<String> results = null;
			List<String> currentData = new ArrayList<String>();
			while ((sCurrentLine = br.readLine()) != null) {
				currentData.add(sCurrentLine);
				Matcher matcher = pattern.matcher(sCurrentLine);
				if (matcher.matches()) {
					key = matcher.group(); // query number
					results = new ArrayList<String>();
					resultByQuery.put(key, results);
				} else {
					if (key != null) {
						resultByQuery.get(key).add(sCurrentLine);
					}
				}
			}
			if(resultByQuery.isEmpty())
			{
				resultByQuery.put(getFileName(resultFile),currentData);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return resultByQuery;
	}
}
