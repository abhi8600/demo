package com.birst.ati.util;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.birst.ati.TestMeta;


public class Util extends DefaultHandler 
{
	private TestMeta tMeta;
	private List<TestMeta> testSuiteList = new ArrayList<TestMeta>();
	private StringBuilder acc; 
	private static final Logger logger = Logger.getLogger(Util.class);
	
	public Util()
	{
		acc = new StringBuilder();
	}

	public static List<TestMeta> getListofTestSuites(String filePath) throws FileNotFoundException 
	{
		List<TestMeta> tSuiteList = null;	
		try {
			Util handler = new Util();
			XMLFilter xmlFilter = new XMLFilterEntityImpl(XMLReaderFactory.createXMLReader());
		    xmlFilter.setContentHandler(handler);
		    xmlFilter.parse(filePath);	
			tSuiteList = handler.getList();
		} catch (SAXException e) {
			logger.error("Exception while parsing the file : " + e);			
		} catch (IOException e) {		
			logger.error("Exception while parsing the file : " + e);	
		}
		return tSuiteList;
	}

	public void characters(char[] buffer, int start, int length) 
	{		
		 acc.append(buffer, start, length);
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
		acc = new StringBuilder();	
		if (qName.equalsIgnoreCase("TestSuite")) {
			tMeta = new TestMeta();
			tMeta.setTestSuiteName(attributes.getValue("TestSuiteName"));
		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException 
	{		       
		if (qName.equalsIgnoreCase("TestSuite")) {
			testSuiteList.add(tMeta);
		} else if (qName.equalsIgnoreCase("ExpectedFile")) {
			tMeta.setExpectedFile(unescapeXML(acc.toString()));			
		} else if (qName.equalsIgnoreCase("ActualFile")) {
			tMeta.setActualFile(unescapeXML(acc.toString()));			
		} else if (qName.equalsIgnoreCase("TestSuiteName")) {
			tMeta.setTestSuiteName(unescapeXML(acc.toString()));
		}else if (qName.equalsIgnoreCase("IsIgnoreCase")) {
			tMeta.setIgnoreCase(Boolean.parseBoolean((acc.toString())));
		}
		acc.setLength(0);
	}

	private List<TestMeta> getList() {
		return this.testSuiteList;
	}

	public static String getLogFilePath() {
		Enumeration e = Logger.getRootLogger().getAllAppenders();
		while (e.hasMoreElements()) {
			Appender app = (Appender) e.nextElement();
			if (app instanceof FileAppender) {
				return ((FileAppender) app).getFile();
			}
		}
		return null;
	}
	
	/**
	 * replace escaped characters with special characters 
	 * @param str
	 * @return
	 */
	public static String unescapeXML(String str) 
	{
	    if (str == null || str.length() == 0)
	      return "";

	    StringBuffer buf = new StringBuffer();
	    int len = str.length();
	    for (int i = 0; i < len; ++i) 
	    {
	    	char c = str.charAt(i);
	    	if (c == '&') {
	    		int pos = str.indexOf(";", i);
	    		if (pos == -1) { // Really evil
	    			buf.append('&');
	    		} else if (str.charAt(i + 1) == '#') {
	    			int val = Integer.parseInt(str.substring(i + 2, pos), 16);
	    			buf.append((char) val);
	    			i = pos;
	    		} else {
	    			String substr = str.substring(i, pos + 1);
	    			if (substr.equals("&amp;"))
	    				buf.append('&');
	    			else if (substr.equals("&lt;"))
	    				buf.append('<');
	    			else if (substr.equals("&gt;"))
	    				buf.append('>');
	    			else if (substr.equals("&quot;"))
	    				buf.append('"');
	    			else if (substr.equals("&apos;"))
	    				buf.append('\'');
	    			else
	    				// ????
	    				buf.append(substr);
	    			i = pos;
	    		}
	      } else {
	    	  buf.append(c);
	      }
	    }
	    return buf.toString();
	}

	public static void AppendToFile(String fileName, String line)
	{
		 FileWriter fw  = null;
		try
		{
		    fw = new FileWriter(fileName,true); 
		    fw.write(line);
		    fw.close();
		}
		catch(IOException ioe)
		{
		    System.err.println("IOException: " + ioe.getMessage());
		}
		finally
		{
			if(fw!=null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
