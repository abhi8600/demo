package com.birst.ati;


import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.AssertJUnit;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.birst.ati.config.ATIConfig;
import com.birst.ati.config.ATIConfigBean;
import com.birst.ati.util.Util;


public class ATCTestIntegrator {
	private static final Logger logger = Logger.getLogger(ATCTestIntegrator.class);
	ATIConfigBean atiConfigBean = ATIConfig.loadConfiguration();

	static List<String> passedSuitesArrayList = new ArrayList<String>();
	static List<String> failedTestSuitesList = new ArrayList<String>();
	static Map<String, Integer> testSuitePassNumberMap = new LinkedHashMap<String, Integer>();
	static Map<String, Integer> testSuiteFailedNumberMap = new LinkedHashMap<String, Integer>();

	@Test(dataProvider = "DP1", enabled = true)
	public void executeTest(String testSuiteName, String fileName, String testName) {
		List<Object[]> testList = TestNGRunner.getTestList(testSuiteName, fileName+"("+testName+")");
		
		List<String> actualResults = ((List<String>) testList.get(0)[3]);
		
		List<String> expectedResults =((List<String>) testList.get(0)[4]);
		
		boolean isIgnoreCase = (boolean) testList.get(0)[5];
		
		boolean flag = new ATCTestIntegrator().compareResults(actualResults, expectedResults,isIgnoreCase);
		if (!flag) {
			Util.AppendToFile(TestNGRunner.FAILED_QUERY_FILE,"Suite: " + testSuiteName + " TestName: " + fileName+"("+testName +")" +"\n");
			if (testSuiteFailedNumberMap.containsKey(testSuiteName)) {
				int failedCount = testSuiteFailedNumberMap.get(testSuiteName);
				testSuiteFailedNumberMap.put(testSuiteName, failedCount + 1);
			} else {
				testSuiteFailedNumberMap.put(testSuiteName, 1);
			}
			if (!failedTestSuitesList.contains(testSuiteName)) {
				failedTestSuitesList.add(testSuiteName);
				if (passedSuitesArrayList.contains(testSuiteName)) {
					passedSuitesArrayList.remove(testSuiteName);
				}
			}
		} else {
			if (testSuitePassNumberMap.containsKey(testSuiteName)) {
				int successCount = testSuitePassNumberMap.get(testSuiteName);
				testSuitePassNumberMap.put(testSuiteName, successCount + 1);
			} else {
				testSuitePassNumberMap.put(testSuiteName, 1);
			}
			if (!passedSuitesArrayList.contains(testSuiteName) && !failedTestSuitesList.contains(testSuiteName)) {
				passedSuitesArrayList.add(testSuiteName);
			}
		}

		AssertJUnit.assertEquals(true, flag);
	}

	public static void logSuccessfulTestSuites() {
		logger.info("Global Status  : " + ((failedTestSuitesList.size() > 0) ? " Failed " : " Passed "));
		for (String failedSuite : failedTestSuitesList) {
			int totalPassed = testSuitePassNumberMap.get(failedSuite) == null ? 0 : testSuitePassNumberMap.get(failedSuite);
			int totalFailed = testSuiteFailedNumberMap.get(failedSuite) == null ? 0 : testSuiteFailedNumberMap.get(failedSuite);
			int totalTests = (totalPassed + totalFailed);
			logger.info(failedSuite + " (" + totalTests + ")  :  Passed (" + totalPassed + ") / Failed (" + totalFailed + ")");
		}
		for (String testSuite : passedSuitesArrayList) {
			int totalPassed = testSuitePassNumberMap.get(testSuite) == null ? 0 : testSuitePassNumberMap.get(testSuite);
			int totalFailed = testSuiteFailedNumberMap.get(testSuite) == null ? 0 : testSuiteFailedNumberMap.get(testSuite);
			int totalTests = (totalPassed + totalFailed);
			logger.info(testSuite + " (" + totalTests + ")  :  Passed ");
		}
	}

	@DataProvider(name = "DP1")
	public Object[][] parse(ITestContext ctx) throws FileNotFoundException {
		List<Object[]> testList = TestNGRunner.getTestList(ctx.getCurrentXmlTest().getSuite().getName(), ctx.getCurrentXmlTest().getName());
		Object[][] dataForExecution = new Object[testList.size()][3];
		int i = 0;
		for (Object[] objArray : testList) {
			dataForExecution[i] = Arrays.copyOf(objArray,3);
			i++;
		}
		return dataForExecution;
	}

	/**
	 * Compare actual and expected result line by line
	 * 
	 * @param suiteName
	 * @param fileName
	 * @param key
	 * @param actualResults
	 * @param expectedResults
	 * @return
	 */
	private boolean compareResults(List<String> actualResult, List<String> expectedResult, boolean isIgnoreCase) {
		if (actualResult.size() != expectedResult.size()) {
			return false;
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			String expected = expectedResult.get(i);
			String actual = actualResult.get(i);
			if (!(expected.equals(actual))) {
				String delimiter = getDelimiter(expected); // delimiter could be
															// '\t' or '|'
				String[] exp = expected.split(delimiter);
				String[] act = actual.split(delimiter);
				return compare(exp, act,isIgnoreCase); // if whole string doesn't match then
											// look for decimal difference
			}
		}
		return true;
	}

	/**
	 * Compare one particular line of result sets
	 * 
	 * @param expected
	 * @param actual
	 * @return
	 */
	private boolean compare(String[] expected, String[] actual, boolean isIgnoreCase) {
		for (int i = 0; i < expected.length; i++) {
			if (isIgnoreCase ? (!expected[i].equalsIgnoreCase(actual[i])) : (!expected[i].equals(actual[i])) ) {
				if (!expected[i].contains(".") && !actual[i].contains(".")) {
					return false;
				}
				try {
					// removing ',' to make it convertible to double. Ex:-
					// 29,827,410.93
					String e = expected[i].replace(",", "");
					String a = actual[i].replace(",", "");
					Double exp = Double.valueOf(e);
					Double act = Double.valueOf(a);
					if(compareDecimalDiff(e, a) == false)
					{
						return false;
					}
				} catch (NumberFormatException nfe) {
					return false; // Can't convert it to double, so there is a
									// diff. No need to process further
				}
			}
		}
		return true;
	}

	/**
	 * handle decimal difference
	 * 
	 * @param exp
	 * @param act
	 * @return
	 */
	private boolean compareDecimalDiff(String exp, String act) 
	{
		/** handle cases like 
		 * 1 -> 68.99 and 69
		 * 2 -> 50.49 and 50.5
		 * 
		 */
		int len = 0;
		if(!exp.contains(".") || !act.contains("."))
		{
			if(!exp.contains("."))
			{
				len = act.substring(act.lastIndexOf(".")+1).length();
				exp = exp.concat(".");
				for(int i=1;i<=len;i++)
				{
					exp = exp.concat("0");
				}
			}else if(!act.contains("."))
			{
				len = exp.substring(exp.lastIndexOf(".")+1).length();
				act = act.concat(".");
				for(int i=1;i<=len;i++)
				{
					act = act.concat("0");
				}
			}
		}else if(exp.contains(".") && act.contains("."))
		{
			int len1 = act.substring(act.lastIndexOf(".")+1).length();
			int len2 = exp.substring(exp.lastIndexOf(".")+1).length();
			if(len2 > len1)
			{
				for(int i=0; i<(len2-len1); i++)
				{
					act = act+'0';
				}
			}else if(len2 < len1)
			{
				for(int i=0; i<(len1-len2); i++)
				{
					exp = exp+'0';
				}
			}
		}
		
		// replacing "." from number else will get Numberformat exception
		Long e = new Long(exp.replace(".", ""));
		Long a = new Long(act.replace(".", ""));
		
		Long diff = (a > e)?a-e:e-a;
				
		if(diff <= new Integer(atiConfigBean.getNumOfDecPointDiffAllowed())){			
			return true;
		}
				
		return false;
	}

	/**
	 * returns a delimiter. It could be '\t' or '|'. default it returns space as
	 * a delimiter.
	 * 
	 * @param expected
	 * @return
	 */
	String getDelimiter(String expected) {
		if (expected.contains("|"))
			return "|";
		else if (expected.contains("\\t"))
			return "\\t";

		return "\\s";
	}

}
