package com.birst.ati;

public class TestMeta {

	private String testSuiteName;
	private String expectedFile;
	private String actualFile;
	private boolean isIgnoreCase;

	public String getTestSuiteName() {
		return testSuiteName;
	}

	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}

	public String getExpectedFile() {
		return expectedFile;
	}

	public void setExpectedFile(String expectedFile) {
		this.expectedFile = expectedFile;
	}

	public String getActualFile() {
		return actualFile;
	}

	public void setActualFile(String actualFile) {
		this.actualFile = actualFile;
	}

	public boolean isIgnoreCase() {
		return isIgnoreCase;
	}

	public void setIgnoreCase(boolean isIgnoreCase) {
		this.isIgnoreCase = isIgnoreCase;
	}

	@Override
	public String toString() {
		return "TestMeta [testSuiteName=" + testSuiteName + ", expectedFile=" + expectedFile + ", actualFile=" + actualFile + ", isIgnoreCase="+ isIgnoreCase +"]";
	}

}
