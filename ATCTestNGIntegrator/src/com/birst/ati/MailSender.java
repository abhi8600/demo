package com.birst.ati;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.birst.ati.config.ATIConfig;
import com.birst.ati.config.ATIConfigBean;

public class MailSender {

	private static final Logger logger = Logger.getLogger(MailSender.class);
	ATIConfigBean atiConfigBean;

	MailSender(ATIConfigBean atiConfigBean) {
		this.atiConfigBean = atiConfigBean;
	}

	public void sendEmail(boolean isATCTestNGSuccess, String emailBody, String filePath) 
	{
		try {
			String from = atiConfigBean.getEmailFrom();
			String to = atiConfigBean.getEmailToList();
			String cc = atiConfigBean.getEmailCCList();
			String subject = atiConfigBean.getEmailSubPrefix();

			if(from == null || to == null || from.isEmpty() || to.isEmpty())
			{
				logger.error("From/To list is empty. Can't send mail");
				return;
			}
			Session mailSession = null;
			Properties props = new Properties();
			props.put("mail.smtp.host", atiConfigBean.getEmailServer());
			if (mailSession == null) {
				mailSession = javax.mail.Session.getInstance(props, null);
			}

			Message msg = new MimeMessage(mailSession);
			Multipart mp = new MimeMultipart();
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			msg.setReplyTo(new InternetAddress[] { addressFrom });
			String[] recipients = to.split(";");
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			
			if(cc != null && !cc.isEmpty())
			{
				String[] ccRecipients = cc.split(";");
				InternetAddress[] addressCC = new InternetAddress[ccRecipients.length];
				for (int i = 0; i < ccRecipients.length; i++) {
					addressCC[i] = new InternetAddress(ccRecipients[i]);
				}			
				msg.setRecipients(Message.RecipientType.CC, addressCC);
			}
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeBodyPart messagePart = new MimeBodyPart();
			messagePart.setContent(emailBody, "text/html");
			mp.addBodyPart(messagePart);
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filePath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			String fileName = new File(filePath).getName();
			messageBodyPart.setFileName(fileName);
			mp.addBodyPart(messageBodyPart);
			msg.setContent(mp);
			Transport.send(msg);
		} catch (Exception ex) {
			System.out.println(ex);
			logger.error(ex.getMessage(), ex);
		}
	}

	public static void main(String args[]) {
		MailSender ms = new MailSender(ATIConfig.loadConfiguration());
		String emailBody = new String("Test Mail");
		ms.sendEmail(true, emailBody, "D:\\log.txt");
		System.out.println("Mail sent");
	}

}
