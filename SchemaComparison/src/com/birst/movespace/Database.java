package com.birst.movespace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class Database {

	private static Logger logger = Logger.getLogger(Database.class);

	public static Connection getConnection(String sqlDriver, String connectionString, String username, String password) throws SQLException, ClassNotFoundException {
		Connection con = null;
		Class.forName(sqlDriver);
		con = DriverManager.getConnection(connectionString, username, password);
		logger.info("SQLServerConnection Created " + con.toString());
		return con;
	}

	public static List<String> getSchemaTables(Connection conn, String schema, String driver) throws SQLException {
		logger.debug("getSchemaTables for " + driver);
		ResultSet set = null;
		List<String> results = new ArrayList<String>();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		if ("com.mysql.jdbc.Driver".equals(driver)) {
			set = meta.getTables(schema, null, null, null);
		} else {
			set = meta.getTables(null, schema, null, null);
		}
		while (set.next()) {
			String tname = set.getString(3);
			String tableType = set.getString(4);
			if (tableType != null) {
				results.add(tname);
			}
		}
		logger.info("Total Tables Found : " + results.size());
		return results;
	}

	public static List<String> getTableColumns(Connection conn, String schema, String driver, String tableName) throws SQLException {
		logger.debug("getSchemaTables for " + driver);
		ResultSet set = null;
		List<String> results = new ArrayList<String>();
		java.sql.DatabaseMetaData meta = conn.getMetaData();

		if ("com.mysql.jdbc.Driver".equals(driver)) {
			set = meta.getColumns(schema, null, tableName, null);
		} else {
			set = meta.getColumns(null, schema, tableName, null);
		}
		while (set.next()) {
			results.add(set.getString("COLUMN_NAME"));
		}
		logger.info("Total Columns Found for " + tableName + " : " + results.size());
		return results;

	}

	public static void closeDatabaseResource(Object obj) {
		if (obj instanceof PreparedStatement) {
			if (obj != null) {
				try {
					((PreparedStatement) obj).close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				obj = null;
			}
		}
		if (obj instanceof Connection) {
			if (obj != null) {
				try {
					((Connection) obj).close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				obj = null;
			}
		}
		if (obj instanceof ResultSet) {
			if (obj != null) {
				try {
					((ResultSet) obj).close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				obj = null;
			}
		}
	}
}
