package com.birst.movespace;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

public class CompareMain {

	private static Logger logger = Logger.getLogger(CompareMain.class);
	private static final int MAX_ROW_DIFF_ALLOWED = 25;

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		String sourceSchema, targetSchema, sourceDBDriver, targetDBDriver, sourceUserName, sourcePassword, targetUserName, targetPassword, sourceConnectionString, targetConnectionString;

		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("config.properties"));
			sourceSchema = prop.getProperty("sourceSchema");
			targetSchema = prop.getProperty("targetSchema");
			sourceDBDriver = prop.getProperty("sourceDBDriver");
			targetDBDriver = prop.getProperty("targetDBDriver");
			sourceUserName = prop.getProperty("sourceUserName");
			sourcePassword = prop.getProperty("sourcePassword");
			targetUserName = prop.getProperty("targetUserName");
			targetPassword = prop.getProperty("targetPassword");
			sourceConnectionString = prop.getProperty("sourceConnectionString");
			targetConnectionString = prop.getProperty("targetConnectionString");

		} catch (IOException ex) {
			logger.error("Could Not parse the config.properties File");
			return;
		}
		if (sourceSchema == null || targetSchema == null || sourceDBDriver == null || targetDBDriver == null || sourceUserName == null || sourcePassword == null || targetUserName == null
				|| targetPassword == null) {
			logger.error("Mandatory Parameters Missing.");
			return;
		}

		Connection sourceConn = null, targetConn = null;
		sourceConn = Database.getConnection(sourceDBDriver, sourceConnectionString, sourceUserName, sourcePassword);
		targetConn = Database.getConnection(targetDBDriver, targetConnectionString, targetUserName, targetPassword);

		List<String> sourceSchemaList = Database.getSchemaTables(sourceConn, sourceSchema, sourceDBDriver);
		List<String> targetSchemaList = Database.getSchemaTables(targetConn, targetSchema, targetDBDriver);
		logger.info("Starting Schema Comparison ....");
		logger.info("[Source] :  " + sourceConnectionString + " , " + sourceSchema);
		logger.info("[Target] :  " + targetConnectionString + " , " + targetSchema);
		compareTableRowCounts(sourceConn, targetConn, sourceSchema, targetSchema, sourceSchemaList, targetSchemaList);
		compareTableColumns(sourceConn, targetConn, sourceSchema, targetSchema, sourceSchemaList, targetSchemaList, sourceDBDriver, targetDBDriver);
	}

	private static void compareTableColumns(Connection sourceConnection, Connection targetConnection, String sourceSchema, String targetSchema, List<String> sourceSchemaList,
			List<String> targetSchemaList, String sourceDBDriver, String targetDBDriver) {
		PreparedStatement sourcePstmt = null, targetPstmt = null;
		boolean isCrossDBComparison = sourceDBDriver == targetDBDriver;
		try {
			for (String s : sourceSchemaList) {
				if (contains(targetSchemaList, s)) {

					String targetTable = s;
					for (String sTemp : targetSchemaList) {
						if (sTemp.equalsIgnoreCase(s)) {
							targetTable = sTemp;
						}
					}

					List<String> sourceTableColumnsList = Database.getTableColumns(sourceConnection, sourceSchema, sourceDBDriver, s);
					List<String> targetTableColumnsList = Database.getTableColumns(targetConnection, targetSchema, targetDBDriver, targetTable);

					List<String> commonColumns = validateColumns(sourceTableColumnsList, targetTableColumnsList);
					if (commonColumns.isEmpty()) {
						logger.error("Could not find any common columns in " + s + "  for Source(" + sourceSchema + ") and Target(" + targetSchema + ")");
						System.out.println("Could not find any common columns in " + s + "  for Source(" + sourceSchema + ") and Target(" + targetSchema + ")");
						continue;
					}
					String selectList = "";
					boolean isFirst = true;
					for (String s2 : commonColumns) {
						if (!isFirst) {
							selectList += ", ";
						}
						selectList += s2;
						isFirst = false;
					}
					String commonQuerySource = "SELECT " + selectList + " FROM " + sourceSchema + "." + s + " ORDER BY " + selectList;
					String commonQueryTarget = "SELECT " + selectList + " FROM " + targetSchema + "." + targetTable + " ORDER BY " + selectList;

					logger.info("[SourceQuery] :" + commonQuerySource);

					sourcePstmt = sourceConnection.prepareStatement(commonQuerySource, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
					if ("com.mysql.jdbc.Driver".equals(sourceDBDriver)) {
						((com.mysql.jdbc.Statement) sourcePstmt).enableStreamingResults();
					}

					targetPstmt = targetConnection.prepareStatement(commonQueryTarget, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
					if ("com.mysql.jdbc.Driver".equals(targetDBDriver)) {
						((com.mysql.jdbc.Statement) targetPstmt).enableStreamingResults();
					}

					ResultSet sourceRS = sourcePstmt.executeQuery();
					ResultSet targetRS = targetPstmt.executeQuery();
					logger.info("Starting Result Set Comparison");
					int columnCount = sourceRS.getMetaData().getColumnCount();
					int failCount = 0;
					int successCount = 0;
					boolean tableValidated = true;
					while (sourceRS.next()) {

						StringBuffer sourceSB = new StringBuffer();
						StringBuffer targetSB = new StringBuffer();
						for (int i = 1; i <= columnCount; i++) {
							String sColumn = sourceRS.getString(i);
							if(isCrossDBComparison)
							{
								sourceSB.append(cleanUpData(sColumn));
							}else{
								sourceSB.append(sColumn);
							}
							if (i < columnCount) {
								sourceSB.append("|");
							}
						}
						if (targetRS.next()) {
							for (int i = 1; i <= columnCount; i++) {
								String sColumn = targetRS.getString(i);
								if(isCrossDBComparison)
								{
									targetSB.append(cleanUpData(sColumn));
								}else{
									targetSB.append(sColumn);
								}
								if (i < columnCount) {
									targetSB.append("|");
								}
							}
						}

						if (!sourceSB.toString().equalsIgnoreCase(targetSB.toString())) {
							logger.warn("[DIFF] Source " + sourceSB.toString());
							logger.warn("[DIFF] Target " + targetSB.toString());
							failCount++;
						} else {
							successCount++;
						}

						sourceSB = null;
						targetSB = null;
						if (failCount > MAX_ROW_DIFF_ALLOWED) {
							logger.error("[MAX_DIFF] Table Columns " + s + " Does Not Match between Source(" + sourceSchema + ") and Target(" + targetSchema + ")");
							System.out.println("[MAX_DIFF] Table " + s + " Does Not Match between Source(" + sourceSchema + ") and Target(" + targetSchema + "). Sucess Count =" + successCount
									+ "  Failed Count =" + failCount);
							tableValidated = false;
							break;
						}

					}
					System.out.println("[DIFF_CHECK] Columns  for " + s + (tableValidated ? " Match " : " Do No Match ") + " between Source(" + sourceSchema + ") and Target(" + targetSchema
							+ "). Sucess Count =" + successCount + "  Failed Count =" + failCount);
					logger.info("[DIFF_CHECK] Columns for " + s + (tableValidated ? " Match " : "  Do No Match ") + " between Source(" + sourceSchema + ") and Target(" + targetSchema
							+ "). Sucess Count =" + successCount + "  Failed Count =" + failCount);
					logger.warn("******************************************");
					Database.closeDatabaseResource(sourceRS);
					Database.closeDatabaseResource(targetRS);
				} else {
					System.out.println("Table " + s + " Not Found in Target Schema. Please check it manually, it could be hashed");
					logger.info("Table " + s + " Not Found in Target Schema. Please check it manually, it could be hashed");
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
		} finally {
			Database.closeDatabaseResource(sourceConnection);
			Database.closeDatabaseResource(targetConnection);
			Database.closeDatabaseResource(sourcePstmt);
			Database.closeDatabaseResource(targetPstmt);
		}
	}

	private static List<String> validateColumns(List<String> sourceTableColumnsList, List<String> targetColumnsList) {
		List<String> commonColumns = new ArrayList<String>();
		for (String s : sourceTableColumnsList) {
			if (contains(targetColumnsList, s)) {
				commonColumns.add(s);
			} else {
				logger.error("[Validate Columns] : Source Column " + s + " Not found in Target Table. Column could possibly be hashed.");
				System.out.println("[Validate Columns] : Source Column " + s + " Not found in Target Table. Column could possibly be hashed.");
			}
		}
		for (String s : targetColumnsList) {
			if (!sourceTableColumnsList.contains(s)) {
				logger.info("[Validate Columns] : Column " + s + " only found in Target Table. Column could possibly be hashed / Composite Key OR Database Specific Implementation ");
				System.out.println("[Validate Columns] : Column " + s + " only found in Target Table. Column could possibly be hashed / Composite Key OR Database Specific Implementation ");
			}
		}
		logger.info("[Common Columns] : " + commonColumns.toString());
		return commonColumns;
	}

	private static void compareTableRowCounts(Connection sqlServerConn, Connection infoBrightConn, String sourceSchema, String targetSchema, List<String> sourceSchemaList,
			List<String> targetSchemaList) {
		PreparedStatement sqlPstmt = null, ibPstmt = null;
		try {

			for (String s : sourceSchemaList) {
				if (contains(targetSchemaList, s)) {
					int sourceCount = -1;
					int targetCount = -1;
					String targetTable = s;
					for (String sTemp : targetSchemaList) {
						if (sTemp.equalsIgnoreCase(s)) {
							targetTable = sTemp;
						}
					}
					sqlPstmt = sqlServerConn.prepareStatement("SELECT COUNT(*) CNT FROM " + sourceSchema + "." + s);
					ibPstmt = infoBrightConn.prepareStatement("SELECT COUNT(*) CNT FROM " + targetSchema + "." + targetTable);
					ResultSet sourceRS = sqlPstmt.executeQuery();
					if (sourceRS.next()) {
						sourceCount = sourceRS.getInt(1);
					}

					ResultSet targetRS = ibPstmt.executeQuery();
					if (targetRS.next()) {
						targetCount = targetRS.getInt(1);
					}
					logger.info("[ROW COUNT COMPARISON] : source=" + sourceSchema + "." + s + "(" + sourceCount + ")" + " target=" + targetSchema + "." + targetTable + "(" + targetCount + ")"
							+ " Diff: " + (targetCount - sourceCount));
					System.out.println("[ROW COUNT COMPARISON] : source=" + sourceSchema + "." + s + "(" + sourceCount + ")" + " target=" + targetSchema + "." + targetTable + "(" + targetCount + ")"
							+ " Diff: " + (targetCount - sourceCount));

					Database.closeDatabaseResource(sourceRS);
					Database.closeDatabaseResource(targetRS);

					Database.closeDatabaseResource(sqlPstmt);
					Database.closeDatabaseResource(ibPstmt);
				} else {
					System.out.println("Table " + s + " Not Found in Target Schema. Please check it manually, it could be hashed");
					logger.info("Table " + s + " Not Found in Target Schema. Please check it manually, it could be hashed");
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
			try {
				Database.closeDatabaseResource(sqlServerConn);
				Database.closeDatabaseResource(infoBrightConn);
			} catch (Exception ex2) {
			}
		} finally {
			Database.closeDatabaseResource(sqlPstmt);
			Database.closeDatabaseResource(ibPstmt);
		}
	}

	/*
	 * Ignore some minor differences between IB and SQL Server for dates and
	 * floats
	 */
	private static String cleanUpData(String sColumn) {
		if (sColumn != null && sColumn.indexOf(" 00:00:00") != -1) {
			sColumn = sColumn.substring(0, sColumn.indexOf(" 00:00:00"));
		}
		if (sColumn != null && sColumn.indexOf(".00000") != -1) {
			sColumn = sColumn.substring(0, sColumn.indexOf(".00000"));
		}
		if (sColumn != null && sColumn.endsWith(".0")) {
			sColumn = sColumn.substring(0, sColumn.indexOf(".0"));
		}

		return sColumn;
	}

	private static boolean contains(List<String> sourceList, String objectName) {
		for (String s : sourceList) {
			if (s.equalsIgnoreCase(objectName)) {
				return true;
			}
		}
		return false;
	}
}
