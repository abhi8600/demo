﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Exceptions;
using AcornExecute.Utils;
using AcornExecute.Commands;

namespace AcornExecute
{
    class CommandFactory
    {   
        public static CommandAction getCommandAction(string cmdArgument, string[] allArgs)
        {
            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_BUILDER)
            {
                return new AppBuilderCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_LOADER)
            {
                return new AppLoaderCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_POST_SFDCEXTRACT)
            {
                return new SFDCPostExtractCommand(allArgs);
            }

            if(cmdArgument == UtilConstants.CMD_ARG_VALUE_POST_PROCESS)
            {
                return new PostProcessCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_GET_SFDC_ENGINE)
            {
                return new GetSFDCEngineCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_DELETE_LOAD)
            {
                return new DeleteLoadCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_POST_DELETE)
            {
                return new PostDeleteLastCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_LOAD_EMAIL)
            {
                return new SendProcessEmailCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_EXTRACTION_EMAIL)
            {
                return new SendExtractionEmailCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_UPDATE_DB)
            {
                return new UpdateAdminDBCommand(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_GET_TRANSACTIONAL_OBJECT)
            {
                return new GetTransactionalObject(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_BIRST_PARAMETERS)
            {
                return new GetBirstParameters(allArgs);
            }

            if (cmdArgument == UtilConstants.CMD_ARG_VALUE_CONNECTOR_VARIABLES)
            {
                return new GetConnectorVariables(allArgs);
            }

            throw new InvalidArgumentException("Unrecognized command action : " + cmdArgument);
        }
    }
}
