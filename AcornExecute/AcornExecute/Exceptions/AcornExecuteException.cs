﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcornExecute.Exceptions
{
    public class AcornExecuteException : Exception
    {

        public const int EXIT_CODE_GENERAL                      = -100;
        public const int EXIT_CODE_INVALID_ARGUMENT             = -102;

        public const int EXIT_CODE_SPACE_PUBLISHABILITY         = -103;
        public const int EXIT_CODE_SPACE_PUBLISH_CONCURRENT     = -104;        
        public const int EXIT_CODE_SPACE_BUSY_OTHER             = -105;
        public const int EXIT_CODE_SPACE_LOAD_AUTH_DATA_LIMIT   = -106;
        public const int EXIT_CODE_SPACE_LOAD_AUTH_ROW_LIMIT    = -107;

        public const int EXIT_CODE_SFORCE_NOT_FOUND             = -108;
        public const int EXIT_CODE_SFORCE_NO_OBJECTS            = -109;
        public const int EXIT_CODE_SFORCE_EXTRACT_CONCURRENT    = -110;
        public const int EXIT_CODE_SFORCE_UPLOAD_ERRORS         = -111;

        private int exitCode = EXIT_CODE_GENERAL;
        public AcornExecuteException()
            : base()
        {
        }

        public AcornExecuteException(String message)
            : base(message)
        {
            
        }

        public AcornExecuteException(String message, int exitCode)
            : base(message)
        {
            this.exitCode = exitCode;
        }

        virtual public int getExitCode()
        {
            return exitCode;
        }
    }
}
