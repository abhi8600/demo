﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcornExecute.Exceptions
{
    public class InvalidArgumentException : AcornExecuteException
    {
        public InvalidArgumentException()
            : base()
        {
        }

        public InvalidArgumentException(string message):base(message) 
        {   
        }

        public InvalidArgumentException(string message, int exitCode)
            : base(message, exitCode)
        {
        }
    }
}
