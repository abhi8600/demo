﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using log4net;
using System.Collections.Specialized;
using AcornExecute.Exceptions;
using AcornExecute.Utils;
using Acorn.Utils;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Configuration;
using System.IO;

namespace AcornExecute
{
    class Program
    {
        
        public static readonly ILog systemLog = LogManager.GetLogger("SystemLog");
        static string dbConnectionString = "Driver={SQL Server};Server={REPLACESERVER};Database={REPLACEDB};Uid={REPLACEUSER};Pwd={REPLACEPWD}";
        private static Dictionary<String, String> dbConnectionParams = new Dictionary<string, string>();
        public static string logfile;
        public static string webConfigDirectory;
        public static string cmdActionArg;
        public static MailSettingsSectionGroup mailSettings;
        
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(onUnhandledException);
            string argString = "";
            CommandAction cmdAction = null;
            try
            {
                if (args == null)
                {
                    throw new InvalidArgumentException("No args present");
                }
                foreach (string arg in args)
                {
                    argString += arg + " ";
                }

                // System.Threading.Thread.Sleep(20 * 1000);
                configureDefaultLogger();
                parseAndValidateMandatoryArgs(args);
                bool configureNDC = argString.IndexOf(UtilConstants.ARG_UID) != -1 && argString.IndexOf(UtilConstants.ARG_SPACE_ID) != -1;
                configureFileLogger(configureNDC);
                if (configureNDC)
                {
                    List<String> lookUpArgs = new List<string>();
                    lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
                    lookUpArgs.Add(UtilConstants.ARG_UID);
                    Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
                    AcornExecuteUtil.setLogging(response[UtilConstants.ARG_SPACE_ID], response[UtilConstants.ARG_UID]);
                }
                configureAcornMode();
                configureSettings();
                cmdAction = CommandFactory.getCommandAction(cmdActionArg, args);
                cmdAction.run();

            }
            catch (Exception ex)
            {
                int exitCode = -1;
                if (ex is AcornExecuteException)
                {
                    AcornExecuteException aEx = (AcornExecuteException)ex;
                    exitCode = aEx.getExitCode();
                    systemLog.Error("Exception in AcornExecute. Exit Code : " + exitCode, ex);
                }
                systemLog.Error("Exception while running " + argString, ex);
                Environment.Exit(exitCode);
            }
            finally
            {
                if (cmdAction != null && cmdAction is SFDCPostExtractCommand)
                {
                    SFDCPostExtractCommand cmd = (SFDCPostExtractCommand)cmdAction;
                    cmd.writePostExtractLogs();
                }
            }
        }

        public static void configureDefaultLogger()
        {            
            log4net.Appender.ConsoleAppender appender = new log4net.Appender.ConsoleAppender();
            appender.Name = "AcornConsoleAppender";
            appender.Layout = new log4net.Layout.PatternLayout("%d %-5p - %m%n");
            log4net.Config.BasicConfigurator.Configure(appender);
        }

        public static void configureFileLogger(bool configureNDC)
        {
            if (logfile != null)
            {
                log4net.Appender.RollingFileAppender appender = new log4net.Appender.RollingFileAppender();
                appender.File = logfile;
                appender.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Date;
                appender.DatePattern = @".yyyy-MM-dd.lo\g";
                appender.StaticLogFileName = false;
                appender.AppendToFile = true;
                if (configureNDC)
                {
                    appender.Layout = new log4net.Layout.PatternLayout("%d [%property{NDC}] %-5p - %m%n");
                }
                else
                {
                    appender.Layout = new log4net.Layout.PatternLayout("%d %-5p - %m%n");
                }
                appender.ActivateOptions();
                log4net.Config.BasicConfigurator.Configure(appender);

                log4net.Appender.IAppender[] appenders = log4net.LogManager.GetRepository().GetAppenders();
                for (int i = 0; i < appenders.Length; i++)
                {
                    if(appenders[i] is log4net.Appender.ConsoleAppender)
                    {
                        log4net.Appender.ConsoleAppender cAppender = (log4net.Appender.ConsoleAppender)appenders[i];
                        cAppender.Threshold = log4net.Core.Level.Off;
                   }
                }
            }
        }

        private static void configureSettings()
        {
            ExeConfigurationFileMap fmap = new ExeConfigurationFileMap();
            fmap.ExeConfigFilename = webConfigDirectory + Path.DirectorySeparatorChar + "Web.config";
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fmap, ConfigurationUserLevel.None);
            configureDatabaseConnections(config);
            configureAppSettings(config);
            configureMailSettings(config);
        }

        private static void configureDatabaseConnections(Configuration config)
        {
            string dbConnectionString = getDBConnectionStringFromArgs();
            if (dbConnectionString == null)
            {
                // get it from Web.config file
                if (config.ConnectionStrings.ConnectionStrings.Count > 0)
                {
                    ConnectionStringSettings connString = config.ConnectionStrings.ConnectionStrings["DatabaseConnection"];
                    dbConnectionString = connString.ConnectionString;
                }
            }

            // if it is still null throw exception
            if (dbConnectionString == null)
            {
                throw new AcornExecuteException("Unable to retreive database connection string", AcornExecuteException.EXIT_CODE_GENERAL);
            }
            System.Web.Configuration.WebConfigurationManager.AppSettings[Acorn.Utils.ConfigUtils.KEY_ADMIN_DB_CONNECTION] = dbConnectionString;
        }

        private static void configureAppSettings(Configuration config)
        {
            KeyValueConfigurationCollection kvc = config.AppSettings.Settings;
            foreach (KeyValueConfigurationElement keyValue in kvc)
            {
                System.Web.Configuration.WebConfigurationManager.AppSettings[keyValue.Key] = keyValue.Value;
            }
        }

        private static void configureMailSettings(Configuration config)
        {
            mailSettings = config.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
        }

        private static void configureAdminDBConnection(Configuration config)
        {
            ConnectionStringSettingsCollection connections = config.ConnectionStrings.ConnectionStrings;
            foreach (ConnectionStringSettings connection in connections)
            {
                if (connection.Name == "DatabaseConnection")
                {
                    ConfigUtils.addConfigValue(ConfigUtils.KEY_ADMIN_DB_CONNECTION, connection.ConnectionString);
                    break;
                }
            }
        }

        private static void configureAcornMode()
        {
            ConfigUtils.setRunningMode(ConfigUtils.CONSOLE_MODE);
        }

        private static void parseAndValidateMandatoryArgs(string[] args)
        {
            parseMandatoryCmdArguments(args);
            validateArgs();
        }

        private static void parseMandatoryCmdArguments(string[] args)
        {
            List<String> lookupArgs = new List<string>();
            lookupArgs.Add(UtilConstants.ARGUMENT_CMD_ACTION);
            lookupArgs.Add(UtilConstants.ARGUMENT_WEB_CONFIG_DIR);
            lookupArgs.Add(UtilConstants.ARGUMENT_DBSERVER);
            lookupArgs.Add(UtilConstants.ARGUMENT_DBNAME);
            lookupArgs.Add(UtilConstants.ARGUMENT_DBUSER);
            lookupArgs.Add(UtilConstants.ARGUMENT_DBPASSWORD);
            lookupArgs.Add(UtilConstants.ARG_UID);
            lookupArgs.Add(UtilConstants.ARG_LOG_FILE);
            Dictionary<String, String> response = AcornExecuteUtil.parseCmdArguments(args, lookupArgs.ToArray());
            foreach (KeyValuePair<string, string> kvp in response)
            {
                string key = kvp.Key;
                string value = kvp.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOG_FILE:
                        logfile = value;
                        break;
                    case UtilConstants.ARGUMENT_CMD_ACTION:
                        cmdActionArg = response[UtilConstants.ARGUMENT_CMD_ACTION];
                        break;
                    case UtilConstants.ARGUMENT_DBSERVER:
                        dbConnectionParams.Add(UtilConstants.ARGUMENT_DBSERVER, response[UtilConstants.ARGUMENT_DBSERVER]);
                        break;
                    case UtilConstants.ARGUMENT_DBNAME:
                        dbConnectionParams.Add(UtilConstants.ARGUMENT_DBNAME, response[UtilConstants.ARGUMENT_DBNAME]);
                        break;
                    case UtilConstants.ARGUMENT_DBUSER:
                        dbConnectionParams.Add(UtilConstants.ARGUMENT_DBUSER, response[UtilConstants.ARGUMENT_DBUSER]);
                        break;
                    case UtilConstants.ARGUMENT_DBPASSWORD:
                        dbConnectionParams.Add(UtilConstants.ARGUMENT_DBPASSWORD, response[UtilConstants.ARGUMENT_DBPASSWORD]);
                        break;
                    case UtilConstants.ARGUMENT_WEB_CONFIG_DIR:
                        webConfigDirectory = value;
                        break;
                    default:
                        continue;
                }
            }
            
        }

        private static void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARGUMENT_CMD_ACTION, cmdActionArg);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARGUMENT_WEB_CONFIG_DIR, webConfigDirectory);

            /* Not required since the fall back option is the db settings in the web.config file
            if (!dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBSERVER))
            {
                AcornExecuteUtil.noArgFound("Database Server");
            }

            if (!dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBNAME))
            {
                AcornExecuteUtil.noArgFound("Database Name");
            }

            if (!dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBUSER))
            {
                AcornExecuteUtil.noArgFound("Database User");
            }

            if (!dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBPASSWORD))
            {
                AcornExecuteUtil.noArgFound("Database Password");
            }
             */
        }

        private static string getDBConnectionStringFromArgs()
        {
            if (dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBSERVER) &&
                dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBNAME) &&
                dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBUSER)
                && dbConnectionParams.ContainsKey(UtilConstants.ARGUMENT_DBPASSWORD))
            {

                dbConnectionString = dbConnectionString.Replace("{REPLACESERVER}", dbConnectionParams[UtilConstants.ARGUMENT_DBSERVER]);
                dbConnectionString = dbConnectionString.Replace("{REPLACEDB}", dbConnectionParams[UtilConstants.ARGUMENT_DBNAME]);
                dbConnectionString = dbConnectionString.Replace("{REPLACEUSER}", dbConnectionParams[UtilConstants.ARGUMENT_DBUSER]);
                dbConnectionString = dbConnectionString.Replace("{REPLACEPWD}", dbConnectionParams[UtilConstants.ARGUMENT_DBPASSWORD]);
                return dbConnectionString;
            }
            return null;
        }

        

        public static void onUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exception = null;
            if (e.ExceptionObject != null)
            {
                exception = (Exception)e.ExceptionObject;
            }
            systemLog.Error("Unhandled exception occured: ", exception);
            
            // We didn't expect this status code.
            Environment.Exit(-1);
        }
    }
}
