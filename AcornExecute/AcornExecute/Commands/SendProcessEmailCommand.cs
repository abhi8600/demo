﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using System.Net;
using Acorn;
using Performance_Optimizer_Administration;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace AcornExecute.Commands
{

    class SendProcessEmailCommand : CommandAction
    {
        string loadGroup;
        int loadNumber = -1;
        Guid spaceID;
        Guid userID;
        HashSet<string> liveAccessLoadGroups = new HashSet<string>();     
        bool useSSL;
        string emailType; // success or failure
        string parentSource;

        // for failure
        string failureType;
        string failureMessage;


        public SendProcessEmailCommand()
            : base()
        {
            failureType = "generalError";
        }

        public SendProcessEmailCommand(string[] allArgs)
            : base(allArgs)
        {
            failureType = "generalError";
        }

        override protected string getDisplayName()
        {
            return "Send ProcessEmail Command";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_LOAD_GROUP);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_NUMBER);
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_EMAIL_TEMPLATE_PATH);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_IMAGE_PATH);
            lookUpArgs.Add(UtilConstants.ARG_LIVE_ACCESS_LOADGROUPS);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_EMAIL_USE_SSL);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_FAILURE_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_FAILURE_DETAILS);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_EMAIL_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_PARENT_SOURCE);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOAD_GROUP:
                        loadGroup = value;
                        break;
                    case UtilConstants.ARG_LOAD_NUMBER:
                        loadNumber = int.Parse(value) ;
                        break;
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);            
                        break;
                    case UtilConstants.ARG_BIRST_EMAIL_USE_SSL:
                         bool result = false;
                        if (bool.TryParse(value, out result))
                        {
                            useSSL = result;
                        }
                        break;
                    case UtilConstants.ARG_LIVE_ACCESS_LOADGROUPS:
                        if (value != null && value.Trim().Length > 0)
                        {
                            string[] liveAccessGroups = value.Split(';');
                            if (liveAccessGroups != null && liveAccessGroups.Length > 0)
                            {
                                foreach (string liveAccessGroup in liveAccessGroups)
                                {
                                    liveAccessLoadGroups.Add(liveAccessGroup);
                                }
                            }
                        }
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_FAILURE_TYPE:
                        failureType = value;
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_FAILURE_DETAILS:
                        failureMessage = value;
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_EMAIL_TYPE:
                        emailType = value;
                        break;
                    case UtilConstants.ARG_PARENT_SOURCE:
                        parentSource = value;
                        break;
                    default:
                        continue;
                }
            }
            
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            Space sp = Acorn.Util.getSpaceById(spaceID);
            if (sp == null)
            {
                throw new Exception("Space not found for spaceID : " + spaceID);
            }
            Acorn.User u = Acorn.Util.getUserById(userID);
            if (u == null)
            {
                throw new Exception("User not found for spaceID : " + userID);
            }
            bool newRep = false;
            MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
            if (newRep)
            {
                throw new Exception("Unable to find repository file for space id " + spaceID);            }

            Status.StatusResult sr = Status.getLoadStatus(null, sp, loadNumber, loadGroup);
            if (sr == null)
            {
                Program.systemLog.Info("Status Result for spaceID=" + spaceID + " loadGroup=" + loadGroup + " loadNumber=" + loadNumber + "is null");    
                return ;
            }
            if (Status.isRunningCode(sr.code))
            {
                Program.systemLog.Info("Status Result for spaceID=" + spaceID + " loadGroup=" + loadGroup + " loadNumber=" + loadNumber + "is running");               
                return;
            }

            if (sr.code == Status.StatusCode.Failed)
            {                
                emailType = "failure";
                Program.systemLog.Info("Status Result for spaceID=" + spaceID + " loadGroup=" + loadGroup + " loadNumber=" + loadNumber + " EmailType=" + emailType);               
            }
            else
            {                
                emailType = "success";
                Program.systemLog.Info("Status Result for spaceID=" + spaceID + " loadGroup=" + loadGroup + " loadNumber=" + loadNumber + " EmailType=" + emailType); 
            }  
         
            if (emailType != null && (emailType.ToLower() == "success" || emailType.ToLower() == "failure"))
            {
                Stream imageStream = null;
                try
                {
                    MailMessage mm = null;
                    bool proceedToSendEmail = false;
                    if (emailType.ToLower() == "success")
                    {
                        validateParamsForSuccessEmail();
                        if (parentSource == "Connector")
                        {
                            mm = getMailMessage(sp, u, "AcornExecute.Templates.ProcessEmail.htm");
                        }
                        else
                        {
                            mm = getMailMessage(sp, u, "AcornExecute.Templates.ProcessOnlyEmail.htm");
                        }
                        mm = ProcessData.fixUpBodyOnSuccessfulProcessing(mm, sp, loadNumber, loadGroup, liveAccessLoadGroups, parentSource);
                        proceedToSendEmail = true;
                    }
                    else if (emailType.ToLower() == "failure")
                    {
                        bool cannotpublish = failureType.StartsWith("cannotPublish");
                        bool isBeingPublished = failureType.StartsWith("isBeingPublished");
                        bool isSpaceBusyInOtherOp = failureType.StartsWith("isSpaceBusyInOtherOp");
                        bool dataLimitExceeded = failureType.StartsWith("dataLimitExceeded");
                        bool rowLimitExceeded = failureType.StartsWith("rowLimitExceeded");
                        bool generalError = failureType.StartsWith("generalError");
                        if (cannotpublish || isBeingPublished || isSpaceBusyInOtherOp || dataLimitExceeded || rowLimitExceeded || generalError)
                        {
                            // note that we are flipping the flag.. the function takes canPublish flag
                            bool canSpaceBePublished = !cannotpublish;
                            if (parentSource == "Connector")
                            {
                                mm = getMailMessage(sp, u, "AcornExecute.Templates.ProcessErrorEmail.htm");
                            }
                            else
                            {
                                mm = getMailMessage(sp, u, "AcornExecute.Templates.ProcessOnlyErrorEmail.htm");
                            }
                            mm = ProcessData.fixUpMessageOnProcessFailure(mm, canSpaceBePublished, isBeingPublished, isSpaceBusyInOtherOp, dataLimitExceeded, rowLimitExceeded, generalError, sp);
                            proceedToSendEmail = true;
                        }
                    }

                    if (proceedToSendEmail && Program.mailSettings != null)
                    {
                        string host = Program.mailSettings.Smtp.Network.Host;
                        int port = Program.mailSettings.Smtp.Network.Port;
                        SmtpClient smtpClient = new SmtpClient(host, port);
                        if (useSSL)
                        {
                            smtpClient.EnableSsl = true;
                        }

                        smtpClient.Send(mm);
                    }
                }
                finally
                {
                    if(imageStream != null)
                        imageStream.Close();
                }
                
            }
            else
            {
                throw new Exception("Unrecognized email type " + emailType);
            }
        }

        private void validateParamsForSuccessEmail()
        {
            if (loadNumber == -1)
            {
                throw new Exception("Invalid Load Number");
            }
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_GROUP, loadGroup);
        }

        private MailMessage getMailMessage(Space sp, Acorn.User user, string template)
        {
            System.Reflection.Assembly assem = this.GetType().Assembly;
            Stream bodyStream = assem.GetManifestResourceStream(template);
            StreamReader sr = new StreamReader(bodyStream);
            string body = sr.ReadToEnd();
            sr.Close();
            return ProcessData.getMailMessage(sp, user.Email, body);
        }
    }
}
