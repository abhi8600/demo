﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using Acorn;
using System.IO;
using Performance_Optimizer_Administration;
using System.Xml;

namespace AcornExecute.Commands
{
    class GetTransactionalObject : CommandAction
    {
        private Guid spaceID;
        private string connectorType;
        private const string FILE_NAME = "transactional-objects.txt";
                
        public GetTransactionalObject()
            : base()
        {
        }

        public GetTransactionalObject(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "Get Transactional Object Command";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_TYPE);

            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_CONNECTOR_TYPE:
                        connectorType = value;
                        break;
                    default:
                        continue;
                }
            }
        }

        protected override void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_CONNECTOR_TYPE, connectorType);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                // Validate input.
                if (connectorType == null || connectorType.Trim().Length == 0)
                {
                    throw new Exception("ConnectorType not found : " + spaceID);
                }

                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for space : " + spaceID);
                }
                
                Acorn.User owner = Acorn.Util.getSpaceOwner(sp);
                if (owner == null)
                {
                    throw new Exception("Owner not found for space : " + spaceID);
                }

                // Load repository from disk.
                bool newRepository = false;
                MainAdminForm repository = Acorn.Util.loadRepository(sp, out newRepository);
                string connectorDirectory = Acorn.Utils.ConnectorUtils.getConnectorsDirectoryName(sp);

                // Create transactional object list.
                foreach (StagingTable stagingTable in repository.stagingTableMod.getStagingTables())
                {
                    if (stagingTable.Transactional)
                    {
                        sb.AppendLine(stagingTable.Name);
                    }
                }

                // Write transactional items to file.
                File.WriteAllText(connectorDirectory + "\\" + FILE_NAME, sb.ToString());
            }
            finally
            {
            }
        }
    }
}
