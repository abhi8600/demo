﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using Acorn;
using AcornExecute.Exceptions;

namespace AcornExecute.Commands
{
    class UpdateAdminDBCommand : CommandAction
    {
         Guid spaceID;
        Guid userID;
        String paramName;
        String paramValues;

        public UpdateAdminDBCommand()
            : base()
        {
        }

        public UpdateAdminDBCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "Update AdminDB Command";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_ADMIN_DB_PARAM_NAME);
            lookUpArgs.Add(UtilConstants.ARG_ADMIN_DB_PARAM_VALUES);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);            
                        break;
                    case UtilConstants.ARG_ADMIN_DB_PARAM_NAME:
                        paramName = value;
                        break;
                    case UtilConstants.ARG_ADMIN_DB_PARAM_VALUES:
                        paramValues = value;
                        break;
                    default:
                        continue;
                }
            }
            
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_ADMIN_DB_PARAM_NAME, paramName);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_ADMIN_DB_PARAM_VALUES, paramValues);
        }

        override protected void execute()
        {

            Space sp = Acorn.Util.getSpaceById(spaceID);
            if (sp == null)
            {
                throw new Exception("Space not found for spaceID : " + spaceID);
            }

            if (paramName == "runningProcessParam")
            {
                string[] values = paramValues.Split(';');
                if (values.Length < 2)
                {
                    throw new AcornExecuteException("Not enough parameter values for db operation " + paramName + " with values " + paramValues);
                }
                
                Acorn.Util.addRunningProcessInfoToDB(sp, "Scheduler", values[0], values[1]);
            }
        }
    }
}
