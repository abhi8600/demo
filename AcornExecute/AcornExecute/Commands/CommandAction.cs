﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using AcornExecute.Exceptions;

namespace AcornExecute
{
    public abstract class CommandAction
    {
        private string[] args;
        protected Guid uid;
        public CommandAction()
        {
        }

        public CommandAction(string[] allArgs)
        {
            args = allArgs;
        }

        public void run()
        {
            preExecute();
            parseArgsForUid();
            parseArgs(args);
            validateArgs();
            execute();
            postExecute();
        }

        abstract protected void parseArgs(string[] _args);
        abstract protected void validateArgs();
        abstract protected void execute();
        abstract protected string getDisplayName();

        public void parseArgsForUid()
        {
            List<String> lookupArgs = new List<string>();
            lookupArgs.Add(UtilConstants.ARG_UID);
            Dictionary<String, String> response = AcornExecuteUtil.parseCmdArguments(args, lookupArgs.ToArray());
            if (response.ContainsKey(UtilConstants.ARG_UID))
            {
                String value = response[UtilConstants.ARG_UID];
                AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, value);
                uid = new Guid(value);
            }
            
        }

        public void preExecute()
        {
            Program.systemLog.Info("Starting execution of Command : " + getDisplayName());
        }

        public void postExecute()
        {
            Program.systemLog.Info("Finished Command : " + getDisplayName());
        }
    }
}
