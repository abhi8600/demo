﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using Performance_Optimizer_Administration;
using AcornExecute.Utils;
using System.IO;
using AcornExecute.Exceptions;

namespace AcornExecute
{
    class AppLoaderCommand : CommandAction
    {
        string loadGroup;
        DateTime loadDate;
        int loadNumber = -1;
        Guid spaceID;
        Guid userID;
        string[] subGroups;
        bool retryFailed;

        public AppLoaderCommand():base()
        {
        }

        public AppLoaderCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "Application Loader";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_LOAD_GROUP);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_DATE);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_NUMBER);
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_SUB_GROUPS);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_RETRY_FAILED);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOAD_GROUP:
                        loadGroup = value;
                        break;
                    case UtilConstants.ARG_LOAD_DATE:
                        Performance_Optimizer_Administration.Util.tryDateTimeParse(value, out loadDate);
                        break;
                    case UtilConstants.ARG_LOAD_NUMBER:
                        loadNumber = int.Parse(value) ;
                        break;
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    case UtilConstants.ARG_SUB_GROUPS:
                        subGroups = value.Split(';');
                        break;
                    case UtilConstants.ARG_LOAD_RETRY_FAILED:
                        retryFailed = bool.Parse(value);
                        break;
                    default:
                        continue;
                }
            }
            
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_GROUP, loadGroup);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_DATE, loadDate);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {   
                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for spaceID : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for spaceID : " + userID);
                }
                bool newRep = false;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (newRep)
                {
                    throw new Exception("Unable to find repository file for space id " + spaceID);
                }

                if (loadNumber == -1)
                {
                    // get the updated load number from publish table based on the loadgroup
                    loadNumber = Acorn.Util.getLatestLoadNumber(sp, loadGroup) + 1;
                }

                if (loadNumber == -1)
                {
                    throw new Exception("Invalid Load Number");
                }

                sb.Append("loadNumber=" + loadNumber);
                string mainSchema = Acorn.Util.getMainSchema();
                string engineCmd = Acorn.Util.getEngineCommand(sp);
                string timeCmd = Acorn.Util.getTimeCommand();
                string contentDir = Acorn.Util.getContentDir();

                bool canpublish = SpaceAdminService.checkSpacePublishability(maf, sp);
                if (!canpublish)
                {
                    throw new AcornExecuteException("Check Space Publishability", AcornExecuteException.EXIT_CODE_SPACE_PUBLISHABILITY);
                }

                bool isBeingPublished = ApplicationLoader.isSpaceBeingPublished(sp);
                if (isBeingPublished)
                {
                    throw new AcornExecuteException("Space is being published", AcornExecuteException.EXIT_CODE_SPACE_PUBLISH_CONCURRENT);
                }

                int spaceOpID = Acorn.Utils.SpaceOpsUtils.getSpaceAvailability(sp);
                if (!Acorn.Utils.SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                {
                    Program.systemLog.Info("Cannot process : space id: " + sp.ID + " : name : " + sp.Name
                        + " in progress. " + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));

                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("availabilityMessage=" + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID));
                    throw new AcornExecuteException("Space is busy in other operation", AcornExecuteException.EXIT_CODE_SPACE_BUSY_OTHER);

                }                
               
                ApplicationLoader loader = new ApplicationLoader(maf, sp, loadDate, mainSchema, engineCmd, timeCmd, contentDir, null, u, loadGroup, loadNumber);
                if (subGroups != null && subGroups.Length > 0)
                {
                    loader.SubGroups = subGroups;
                }
                loader.retryFailedLoad = retryFailed;

                ApplicationLoader.LoadAuthorization loadAuthorization = loader.getLoadAuthorization(loadGroup);

                AcornExecuteUtil.addNewLine(sb);
                

                if (loadAuthorization == ApplicationLoader.LoadAuthorization.OK)
                {
                    if (Acorn.Utils.BirstConnectUtil.hasLocalETLConnections(sp))
                    {
                        sb.Append("birstLocalLoad=" + true);
                        AcornExecuteUtil.addNewLine(sb);
                    }
                    loader.load(false, null, false, false, true, false);
                    String engineCommandArgs = ApplicationLoader.getEngineCommandArgs(sp, loadNumber);
                    sb.Append("engineCommand=" + engineCmd.Replace("\\", "//"));
                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("args=" + engineCommandArgs.Replace("\\", "//"));
                }
                else
                {
                    Program.systemLog.Info("Cannot proceed to load for spaceID=" + spaceID + " loadGroup=" + loadGroup + " loadNumber=" + loadNumber);
                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("loadAuthorize=" + loadAuthorization.ToString());
                    int loadAuthExceptionCode = -1;
                    if(loadAuthorization == ApplicationLoader.LoadAuthorization.DataLimitExceeded || loadAuthorization == ApplicationLoader.LoadAuthorization.DataPublishLimitExceeded)
                    {
                        loadAuthExceptionCode = AcornExecuteException.EXIT_CODE_SPACE_LOAD_AUTH_DATA_LIMIT;
                    }

                    if (loadAuthorization == ApplicationLoader.LoadAuthorization.RowPublishLimitExceeded || loadAuthorization == ApplicationLoader.LoadAuthorization.RowLimitExceeded)
                    {
                        loadAuthExceptionCode = AcornExecuteException.EXIT_CODE_SPACE_LOAD_AUTH_ROW_LIMIT;
                    }

                    throw new AcornExecuteException("Load Authorization Error", loadAuthExceptionCode);
                }
            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }
        }
    }
}
