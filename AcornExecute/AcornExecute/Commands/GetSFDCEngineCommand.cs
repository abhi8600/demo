﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using Acorn;
using System.IO;

namespace AcornExecute.Commands
{
    class GetSFDCEngineCommand : CommandAction
    {
        private Guid spaceID;
        private Guid userID;
        private String clientId;
        private String sandBoxUrl;
        private int numThreads = 4;
        private Boolean writeEmailerInfo = false;
        private int extractionEngine = -1;
        private string connectorType;
        
        public GetSFDCEngineCommand()
            : base()
        {
        }

        public GetSFDCEngineCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "Get Connector Command";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_CLIENT_ID);
            lookUpArgs.Add(UtilConstants.ARG_SANDBOX_URL);
            lookUpArgs.Add(UtilConstants.ARG_WRITE_EMAILER_INFO);
            lookUpArgs.Add(UtilConstants.ARG_SFDC_NUM_THREADS);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_ENGINE);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_TYPE);            

            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    case UtilConstants.ARG_CLIENT_ID:
                        clientId = value;
                        break;
                    case UtilConstants.ARG_SANDBOX_URL:
                        sandBoxUrl = value;
                        break;
                    case UtilConstants.ARG_WRITE_EMAILER_INFO:
                        writeEmailerInfo = bool.Parse(value);
                        break;
                    case UtilConstants.ARG_SFDC_NUM_THREADS:
                        numThreads = int.Parse(value);
                        break;
                    case UtilConstants.ARG_CONNECTOR_ENGINE:
                        extractionEngine = int.Parse(value);
                        break;
                    case UtilConstants.ARG_CONNECTOR_TYPE:
                        connectorType = value;
                        break;
                    default:
                        continue;
                }
            }
        }

        protected override void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_CONNECTOR_TYPE, connectorType);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                if (connectorType == null || connectorType.Trim().Length == 0)
                {
                    throw new Exception("ConnectorType not found : " + spaceID);
                }

                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for space : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for user : " + userID);
                }

                Acorn.User owner = Acorn.Util.getSpaceOwner(sp);
                if (owner == null)
                {
                    throw new Exception("Owner not found for space : " + spaceID);
                }

                String mailHost = null;
                int port = -1;
                if (writeEmailerInfo && Program.mailSettings != null && Program.mailSettings.Smtp != null)
                {
                    mailHost = Program.mailSettings.Smtp.Network.Host;
                    port = Program.mailSettings.Smtp.Network.Port;
                }

                //override space level setting
                numThreads = sp.SFDCNumThreads > 0 ? sp.SFDCNumThreads : numThreads;
                extractionEngine = extractionEngine > 0 ? extractionEngine : Acorn.Util.getDefaultSFDCVersion();
                String engineCmd = Util.getExtractionEngineCommand(sp, extractionEngine);
                SalesforceLoader.writeSFDCCommandFile(uid, connectorType, sp, clientId, sandBoxUrl, numThreads, mailHost, port, owner.Email);                
                String engineCommandArgs = SalesforceLoader.getSFDCEngineCommandArgs(sp);
                sb.Append("engineCommand=" + engineCmd.Replace("\\", "//"));
                AcornExecuteUtil.addNewLine(sb);
                sb.Append("args=" + engineCommandArgs.Replace("\\", "//"));
                
            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }

        }
    }
}
