﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using System.Net;
using Acorn;
using Performance_Optimizer_Administration;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace AcornExecute.Commands
{

    class SendExtractionEmailCommand : CommandAction
    {
        Guid spaceID;
        Guid userID;
        string connectorType;
        bool useSSL;
        string emailType;
        string failureType;
        string failureMessage;
        bool extractOnly;
        string extractGroups;

        public SendExtractionEmailCommand()
            : base()
        {
            failureType = "generalError";
        }

        public SendExtractionEmailCommand(string[] allArgs)
            : base(allArgs)
        {
            failureType = "generalError";
        }

        override protected string getDisplayName()
        {
            return "Send ExtractionEmail Command";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_EMAIL_TEMPLATE_PATH);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_IMAGE_PATH);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_EMAIL_USE_SSL);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_FAILURE_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_FAILURE_DETAILS);
            lookUpArgs.Add(UtilConstants.ARG_BIRST_PROCESS_EMAIL_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_EXTRACT_ONLY);
            lookUpArgs.Add(UtilConstants.ARG_EXTRACT_GROUPS);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);            
                        break;
                    case UtilConstants.ARG_CONNECTOR_TYPE:
                        connectorType = value;
                        break;
                    case UtilConstants.ARG_BIRST_EMAIL_USE_SSL:
                        bool result = false;
                        if (bool.TryParse(value, out result))
                        {
                            useSSL = result;
                        }
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_FAILURE_TYPE:
                        failureType = value;
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_FAILURE_DETAILS:
                        failureMessage = value;
                        break;
                    case UtilConstants.ARG_BIRST_PROCESS_EMAIL_TYPE:
                        emailType = value;
                        break;
                    case UtilConstants.ARG_EXTRACT_ONLY:
                        bool result2 = false;
                        if (bool.TryParse(value, out result2))
                        {
                            extractOnly = result2;
                        }
                        break;
                    case UtilConstants.ARG_EXTRACT_GROUPS:
                        extractGroups = value;
                        break;
                    default:
                        continue;
                }
            }
            
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_CONNECTOR_TYPE, connectorType);
        }

        override protected void execute()
        {

            Space sp = Acorn.Util.getSpaceById(spaceID);
            if (sp == null)
            {
                throw new Exception("Space not found for spaceID : " + spaceID);
            }
            Acorn.User u = Acorn.Util.getUserById(userID);
            if (u == null)
            {
                throw new Exception("User not found for spaceID : " + userID);
            }
            if (connectorType == null)
            {
                throw new Exception("Connector type not found for spaceID : " + spaceID);
            }
            if (emailType != null && (emailType.ToLower() == "success" || emailType.ToLower() == "failure"))
            {
                Stream imageStream = null;
                try
                {
                    MailMessage mm = null;
                    bool proceedToSendEmail = false;
                    if (emailType.ToLower() == "success")
                    {
                        mm = getMailMessage(sp, u, "AcornExecute.Templates.ExtractionEmail.htm");
                        mm = ProcessData.fixUpBodyOnSuccessfulExtraction(mm, sp, connectorType, (extractGroups == null ? null : extractGroups.Split(',')));
                        proceedToSendEmail = true;
                    }
                    else if (emailType.ToLower() == "failure")
                    {
                        bool cannotpublish = failureType.StartsWith("cannotPublish");
                        bool isBeingPublished = failureType.StartsWith("isBeingPublished");
                        bool isSpaceBusyInOtherOp = failureType.StartsWith("isSpaceBusyInOtherOp");
                        bool dataLimitExceeded = failureType.StartsWith("dataLimitExceeded");
                        bool rowLimitExceeded = failureType.StartsWith("rowLimitExceeded");
                        bool generalError = failureType.StartsWith("generalError");
                        if (cannotpublish || isBeingPublished || isSpaceBusyInOtherOp || dataLimitExceeded || rowLimitExceeded || generalError)
                        {
                            // Note: we are flipping the flag..the function takes canPublish flag.
                            bool canSpaceBePublished = !cannotpublish;
                            string template = (extractOnly) ? "AcornExecute.Templates.ExtractionErrorEmail.htm" : "AcornExecute.Templates.ProcessExtractionErrorEmail.htm";
                            mm = getMailMessage(sp, u, template);
                            mm = ProcessData.fixUpMessageOnExtractionFailure(mm, canSpaceBePublished, isBeingPublished, isSpaceBusyInOtherOp, dataLimitExceeded, rowLimitExceeded, generalError, sp);
                            proceedToSendEmail = true;
                        }
                    }

                    if (proceedToSendEmail && Program.mailSettings != null)
                    {
                        string host = Program.mailSettings.Smtp.Network.Host;
                        int port = Program.mailSettings.Smtp.Network.Port;
                        SmtpClient smtpClient = new SmtpClient(host, port);
                        if (useSSL)
                        {
                            smtpClient.EnableSsl = true;
                        }

                        smtpClient.Send(mm);
                    }
                }
                finally
                {
                    if(imageStream != null)
                        imageStream.Close();
                }
                
            }
            else
            {
                throw new Exception("Unrecognized email type : " + emailType);
            }
        }

        private MailMessage getMailMessage(Space sp, Acorn.User user, string template)
        {
            System.Reflection.Assembly assem = this.GetType().Assembly;
            Stream bodyStream = assem.GetManifestResourceStream(template);
            StreamReader sr = new StreamReader(bodyStream);
            string body = sr.ReadToEnd();
            sr.Close();
            return ProcessData.getMailMessage(sp, user.Email, body);
        }
    }
}
