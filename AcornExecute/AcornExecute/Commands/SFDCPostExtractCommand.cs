﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using AcornExecute.Utils;
using AcornExecute.Exceptions;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.sforce;
using System.Data;

namespace AcornExecute
{
    class SFDCPostExtractCommand : CommandAction
    {
        private Guid spaceID;
        private Guid userID;
        private String fileListPath;
        private String connectorType;
        private string extractGroups;
        private DateTime startdate;
        private Space sp;


        public SFDCPostExtractCommand()
            : base()
        {
        }

        public SFDCPostExtractCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "SFDC Post Extraction";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_FILE_LIST_PATH);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_EXTRACT_GROUPS);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    case UtilConstants.ARG_FILE_LIST_PATH:
                        fileListPath = value;
                        break;
                    case UtilConstants.ARG_CONNECTOR_TYPE:
                        connectorType = value;
                        break;
                    case UtilConstants.ARG_EXTRACT_GROUPS:
                        extractGroups = value;
                        break;
                    default:
                        continue;
                }
            }
        }

        protected override void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                startdate = DateTime.Now.Date;
                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for space : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for user : " + userID);
                }

                Acorn.User owner = Acorn.Util.getSpaceOwner(sp);
                if (owner == null)
                {
                    throw new Exception("Owner not found for space : " + spaceID);
                }

                bool newRep = false;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (newRep)
                {
                    throw new Exception("Unable to find repository file for space id " + spaceID);
                }

                string mainSchema = Acorn.Util.getMainSchema();
                string engineCmd = Acorn.Util.getEngineCommand(sp);
                string timeCmd = Acorn.Util.getTimeCommand();
                string contentDir = Acorn.Util.getContentDir();

                maf.setInUberTransaction(true);
                SalesforceLoader.addLoadAndScriptGroups(maf);

                DataTable errorsTable = new DataTable();
                errorsTable.Columns.Add("Source");
                errorsTable.Columns.Add("Severity");
                errorsTable.Columns.Add("Error");
                string extractInfoId = Acorn.Utils.ConnectorUtils.getExtractInfoId(sp, connectorType);
                try
                {
                    Acorn.Utils.ConnectorUtils.postConnectorExtractionUploader(sp, maf, u, connectorType, errorsTable, (extractGroups == null ? null : extractGroups.Split(',')));
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while uploading the extracted files : " + sp.ID + " - " + ex.ToString(), ex);
                    string fileName = Acorn.Utils.ConnectorUtils.getPostExtractOutputFileName(sp, Status.FAILED, extractInfoId, connectorType);
                    File.WriteAllText(fileName, "Error while uploading the extracted files");
                    Acorn.Utils.ConnectorUtils.createEndStatusFile(sp, connectorType, Status.FAILED, extractInfoId);
                    throw new AcornExecuteException("Error encountered during uploading of data sources: " + sp.ID, AcornExecuteException.EXIT_CODE_SFORCE_UPLOAD_ERRORS);
                }
                bool ignoreErrors = Acorn.SalesforceLoader.ignoreUploaderErrors(sp, errorsTable);
                if (!ignoreErrors)
                {
                    Acorn.Utils.ConnectorUtils.writeFailedMessages(sp, connectorType, errorsTable);
                    Acorn.Utils.ConnectorUtils.createEndStatusFile(sp, connectorType, Status.FAILED, extractInfoId);
                    throw new AcornExecuteException("Error encountered during uploading of data sources: " + sp.ID, AcornExecuteException.EXIT_CODE_SFORCE_UPLOAD_ERRORS);
                }

                Acorn.Utils.ConnectorUtils.setConnectorExtractTimes(sp, maf, connectorType, true);
                Acorn.Util.saveApplication(maf, sp, null, u);
                maf.setInUberTransaction(false);
                string loadGroup = Acorn.DefaultUserPage.isIndependentMode(sp) ? SalesforceLoader.LOAD_GROUP_NAME : Acorn.Util.LOAD_GROUP_NAME;
                int loadNumber = ProcessLoad.getLoadNumber(sp, loadGroup);
                sb.Append("loadNumber=" + loadNumber);
                AcornExecuteUtil.addNewLine(sb);
                sb.Append("loadGroup=" + loadGroup);
            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }
        }

        public void writePostExtractLogs()
        {
            try
            {
                if (sp != null && startdate != null)
                {
                    string filename = Program.logfile + "." + startdate.ToString("yyyy-MM-dd") + ".log";
                    FileInfo fInfo = new FileInfo(filename);
                    FileInfo[] files = new FileInfo[] { fInfo };
                    Acorn.ConnectorLog.writePostExtractLogs(sp, files, connectorType, uid.ToString());
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in writing post extract log file " + connectorType + " : spaceID " + sp.ID, ex);
            }
        }
    }
}
