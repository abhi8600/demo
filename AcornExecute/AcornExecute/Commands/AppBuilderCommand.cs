﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;

namespace AcornExecute
{
    class AppBuilderCommand : CommandAction
    {

        
        public AppBuilderCommand():base()
        {
        }

        public AppBuilderCommand(string[] allArgs):base(allArgs)
        {   
        }

        override protected string getDisplayName()
        {
            return "Application Builder";
        }

        override protected void parseArgs(string[] args)
        {

        }

        override protected void validateArgs()
        {
        }

        override protected void execute()
        {
           
        }
    }
}
