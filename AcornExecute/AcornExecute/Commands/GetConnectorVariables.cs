﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using Acorn;
using System.IO;
using Performance_Optimizer_Administration;
using System.Xml;

namespace AcornExecute.Commands
{
    class GetConnectorVariables : CommandAction
    {
        private Guid spaceID;
        private string connectorType;
        private string connectionString;
        string extractGroups;
        private const string FILE_NAME = "variables.txt";
        private const string EQUALS = "=";
        private const string SLASH = "\\";
        private const char PIPE = '|';
        private const string DISPLAY_NAME = "Get Connector Variables Command";
        private const string CONNECTORS_DIR = "connectors";

        public GetConnectorVariables()
            : base()
        {
        }

        public GetConnectorVariables(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return DISPLAY_NAME;
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_TYPE);
            lookUpArgs.Add(UtilConstants.ARG_EXTRACT_GROUPS);
            lookUpArgs.Add(UtilConstants.ARG_CONNECTOR_CONNECTION_STRING);

            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_CONNECTOR_TYPE:
                        connectorType = value;
                        break;
                    case UtilConstants.ARG_EXTRACT_GROUPS:
                        extractGroups = value;
                        break;
                    case UtilConstants.ARG_CONNECTOR_CONNECTION_STRING:
                        connectionString = value;
                        break;
                    default:
                        continue;
                }
            }
        }

        protected override void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_CONNECTOR_TYPE, connectorType);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_CONNECTOR_CONNECTION_STRING, connectionString);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                // Validate input.
                if (connectorType == null || connectorType.Trim().Length == 0)
                {
                    throw new Exception("ConnectorType not found : " + spaceID);
                }

                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for space : " + spaceID);
                }
                
                Acorn.User owner = Acorn.Util.getSpaceOwner(sp);
                if (owner == null)
                {
                    throw new Exception("Owner not found for space : " + spaceID);
                }

                // Create variables file.
                string variablesStr = Acorn.Utils.ConnectorUtils.getConnectorVariables(null, sp.Directory, owner, sp, connectionString, extractGroups);
                string[] nameValues = variablesStr.Split(PIPE);
                foreach (string nameValue in nameValues)
                {
                    sb.AppendLine(nameValue);
                }

                // Write variables to file.
                File.WriteAllText(sp.Directory + SLASH + CONNECTORS_DIR + SLASH + FILE_NAME, sb.ToString());
            }
            finally
            {
            }
        }
    }
}
