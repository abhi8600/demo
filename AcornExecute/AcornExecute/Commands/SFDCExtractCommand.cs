﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using AcornExecute.Utils;
using AcornExecute.Exceptions;
using Performance_Optimizer_Administration;
using System.IO;
using Acorn.sforce;
using System.Data;

namespace AcornExecute
{
    class SFDCExtractCommand : CommandAction
    {
        private Guid spaceID;
        private Guid userID;
        private string loadGroup;
        private DateTime loadDate;
        int loadNumber;

        public SFDCExtractCommand():base()
        {
        }

        public SFDCExtractCommand(string[] allArgs):base(allArgs)
        {   
        }

        override protected string getDisplayName()
        {
            return "SFDC Extraction";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_LOAD_GROUP);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_DATE);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_NUMBER);
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOAD_GROUP:
                        loadGroup = value;
                        break;
                    case UtilConstants.ARG_LOAD_DATE:
                        Performance_Optimizer_Administration.Util.tryDateTimeParse(value, out loadDate);
                        break;
                    case UtilConstants.ARG_LOAD_NUMBER:
                        loadNumber = int.Parse(value);
                        break;
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    default:
                        continue;
                }
            }
        }

        protected override void validateArgs()
        {   
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_GROUP, loadGroup);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_DATE, loadDate);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for space : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for user : " + userID);
                }

                Acorn.User owner = Acorn.Util.getSpaceOwner(sp);
                if (owner == null)
                {
                    throw new Exception("Owner not found for space : " + spaceID);
                }

                bool newRep = false;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (newRep)
                {
                    throw new Exception("Unable to find repository file for space id " + spaceID);
                }

                if (loadNumber == -1)
                {
                    // get the updated load number from publish table based on the loadgroup
                    loadNumber = Acorn.Util.getLatestLoadNumber(sp, loadGroup) + 1;
                }

                if (loadNumber == -1)
                {
                    throw new Exception("Invalid Load Number");
                }


                sb.Append("loadNumber=" + loadNumber);
                string mainSchema = Acorn.Util.getMainSchema();
                string engineCmd = Acorn.Util.getEngineCommand(sp);
                string timeCmd = Acorn.Util.getTimeCommand();
                string contentDir = Acorn.Util.getContentDir();

                bool isBeingPublished = ApplicationLoader.isSpaceBeingPublished(sp);
                if (isBeingPublished)
                {
                    throw new AcornExecuteException("Space is being published", AcornExecuteException.EXIT_CODE_SPACE_PUBLISH_CONCURRENT);
                }

                int spaceOpID = Acorn.Utils.SpaceOpsUtils.getSpaceAvailability(sp);
                if (!Acorn.Utils.SpaceOpsUtils.isSpaceAvailable(spaceOpID))
                {
                    Program.systemLog.Info("Cannot extract : space id: " + sp.ID + " : name : " + sp.Name
                        + " in progress. " + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));

                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("availabilityMessage=" + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID));
                    throw new AcornExecuteException("Space is busy in other operation", AcornExecuteException.EXIT_CODE_SPACE_BUSY_OTHER);

                }

                if (Acorn.Util.isSFDCExtractStatusFilePresent(sp))
                {
                    Program.systemLog.Info("Cannot extract : space id: " + sp.ID + " : name : " + sp.Name
                        + " . Extraction already going on");

                    throw new AcornExecuteException("Space Extraction is already in progress", AcornExecuteException.EXIT_CODE_SFORCE_EXTRACT_CONCURRENT);

                }

                SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(sp.Directory + Path.DirectorySeparatorChar + "sforce.xml");
                if (settings == null)
                {
                    throw new AcornExecuteException("No sforce.xml found for " + sp.ID, AcornExecuteException.EXIT_CODE_SFORCE_NOT_FOUND);
                }

                if (settings.objects == null || (settings.objects != null && settings.objects.Length == 0))
                {
                    throw new AcornExecuteException("No objects found in sforce.xml for " + sp.ID, AcornExecuteException.EXIT_CODE_SFORCE_NO_OBJECTS);
                }

                List<string> objects = new List<string>();
                for (int i = 0; i < settings.Objects.Length; i++)
                {
                    objects.Add(settings.Objects[i].name);
                }


                SforceService binding = SalesforceLoader.createBinding(sp, owner, settings);
                SalesforceLoader sl = new SalesforceLoader(objects, maf, sp, owner, binding, settings, false, null, false);
                DataTable et = new DataTable();
                et.Columns.Add("Source");
                et.Columns.Add("Severity");
                et.Columns.Add("Error");
                sl.Errors = et;
                sl.getData();
                bool ignoreErrors = Acorn.SalesforceLoader.ignoreUploaderErrors(sp, et);
                if (!ignoreErrors)
                {
                    throw new AcornExecuteException("Error encountered during uploading of data sources: " + sp.ID, AcornExecuteException.EXIT_CODE_SFORCE_UPLOAD_ERRORS);
                }

            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }

        }
    }
}
