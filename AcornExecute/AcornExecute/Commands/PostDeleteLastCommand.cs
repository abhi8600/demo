﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn.Utils;
using Acorn;
using Performance_Optimizer_Administration;
using AcornExecute.Exceptions;
using AcornExecute.Utils;
using System.IO;

namespace AcornExecute.Commands
{
    class PostDeleteLastCommand : CommandAction
    {
        string loadGroup;
        int loadNumber = -1;
        Guid spaceID;
        Guid userID;

        public PostDeleteLastCommand()
            : base()
        {
        }

        public PostDeleteLastCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "PostDeleteLast";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_LOAD_GROUP);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_NUMBER);
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_DELETE_ALL);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOAD_GROUP:
                        loadGroup = value;
                        break;
                    case UtilConstants.ARG_LOAD_NUMBER:
                        loadNumber = int.Parse(value);
                        break;
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    default:
                        continue;
                }
            }
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_GROUP, loadGroup);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for spaceID : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for spaceID : " + userID);
                }
                bool newRep = false;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (newRep)
                {
                    throw new Exception("Unable to find repository file for space id " + spaceID);
                }

                if (loadNumber == -1)
                {
                    throw new Exception("Invalid Load Number");
                }

                DeleteDataService.postDeleteLastOperation(sp, u, maf, loadNumber);
            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }
        }
    }
}
