﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Utils;
using Acorn;
using Performance_Optimizer_Administration;
using AcornExecute.Exceptions;
using System.IO;

namespace AcornExecute.Commands
{
    class DeleteLoadCommand : CommandAction
    {
        string loadGroup;
        int loadNumber = -1;
        Guid spaceID;
        Guid userID;
        bool deleteAll;
        bool restoreRepository;
        
        public DeleteLoadCommand()
            : base()
        {
        }

        public DeleteLoadCommand(string[] allArgs)
            : base(allArgs)
        {
        }

        override protected string getDisplayName()
        {
            return "DeleteLoad";
        }

        override protected void parseArgs(string[] args)
        {
            List<String> lookUpArgs = new List<string>();
            lookUpArgs.Add(UtilConstants.ARG_LOAD_GROUP);
            lookUpArgs.Add(UtilConstants.ARG_LOAD_NUMBER);
            lookUpArgs.Add(UtilConstants.ARG_SPACE_ID);
            lookUpArgs.Add(UtilConstants.ARG_USER_ID);
            lookUpArgs.Add(UtilConstants.ARG_DELETE_ALL);
            Dictionary<string, string> response = AcornExecuteUtil.parseCmdArguments(args, lookUpArgs.ToArray());
            foreach (KeyValuePair<string, string> kvPair in response)
            {
                string key = kvPair.Key;
                string value = kvPair.Value;
                switch (key)
                {
                    case UtilConstants.ARG_LOAD_GROUP:
                        loadGroup = value;
                        break;
                    case UtilConstants.ARG_LOAD_NUMBER:
                        loadNumber = int.Parse(value);
                        break;
                    case UtilConstants.ARG_SPACE_ID:
                        spaceID = new Guid(value);
                        break;
                    case UtilConstants.ARG_USER_ID:
                        userID = new Guid(value);
                        break;
                    case UtilConstants.ARG_DELETE_ALL:
                        deleteAll = bool.Parse(value);
                        break;
                    case UtilConstants.ARG_DELETE_RESTORE:
                        restoreRepository = bool.Parse(value);
                        break;
                    default:
                        continue;
                }
            }
        }

        override protected void validateArgs()
        {
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_SPACE_ID, spaceID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_USER_ID, userID);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_LOAD_GROUP, loadGroup);
            AcornExecuteUtil.throwExceptionOnInvalidValue(UtilConstants.ARG_UID, uid);
        }

        override protected void execute()
        {

            StringBuilder sb = new StringBuilder();
            Space sp = null;
            try
            {
                sp = Acorn.Util.getSpaceById(spaceID);
                if (sp == null)
                {
                    throw new Exception("Space not found for spaceID : " + spaceID);
                }
                Acorn.User u = Acorn.Util.getUserById(userID);
                if (u == null)
                {
                    throw new Exception("User not found for spaceID : " + userID);
                }
                bool newRep = false;
                MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
                if (newRep)
                {
                    throw new Exception("Unable to find repository file for space id " + spaceID);
                }

                AcornExecuteUtil.validateSpaceConcurrentStatus(sp, sb);
                if (!deleteAll)
                {
                    if (loadNumber == -1)
                    {
                        // get the updated load number from publish table based on the loadgroup
                        loadNumber = Acorn.Util.getLatestLoadNumber(sp, loadGroup) + 1;
                    }

                    if (loadNumber == -1)
                    {
                        throw new Exception("Invalid Load Number");
                    }
                }

                bool spaceHasLocalETL = Acorn.Utils.BirstConnectUtil.hasLocalETLConnections(sp);
                if (spaceHasLocalETL)
                {
                    sb.Append("birstLocalLoad=" + true);
                    AcornExecuteUtil.addNewLine(sb);
                }

                bool doDeleteAll = deleteAll || loadNumber == 1;
                if (doDeleteAll)
                {
                    DeleteDataService.performDeleteAllOperation(sp, u, maf);
                }
                else
                {   
                    // if space has local etl, send local connections
                    if (spaceHasLocalETL)
                    {
                        DeleteDataService.deleteBirstLocal(sp, maf, loadNumber);
                    }
                    // for the birst cloud 
                    // spit out the cmd and the arguments
                    sb.Append("loadNumber=" + loadNumber);
                    String deleteCmd = Acorn.Util.getDeleteDataCommand(sp);
                    String deleteCmdArgs = Acorn.DeleteDataService.getDeleteCmdArguments(sp, loadNumber);
                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("engineCommand=" + deleteCmd.Replace("\\", "//"));
                    AcornExecuteUtil.addNewLine(sb);
                    sb.Append("args=" + deleteCmdArgs.Replace("\\", "//"));
                    DeleteDataService.preDeleteLastOperation(null, sp, u, maf, restoreRepository, loadNumber);
                }
            }
            finally
            {
                if (sp != null && sb != null && sb.Length > 0)
                {
                    File.WriteAllText(AcornExecuteUtil.getUniqueFileName(sp.Directory, uid), sb.ToString());
                }
            }
        }
    }
}
