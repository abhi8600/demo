﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcornExecute.Utils
{
    class UtilConstants
    {
        public const string ARG_LOG_FILE = "logfile=";
        public const string ARG_LOAD_GROUP = "loadGroup=";
        public const string ARG_SUB_GROUPS = "subGroups=";
        public const string ARG_LOAD_DATE = "loadDate=";
        public const string ARG_LOAD_NUMBER = "loadnumber=";
        public const string ARG_SPACE_ID = "spaceID=";
        public const string ARG_USER_ID = "userID=";
        public const string ARG_UID = "uid=";
        public const string ARG_FILE_LIST_PATH = "fileListPath=";
        public const string ARG_CLIENT_ID = "clientId=";
        public const string ARG_SANDBOX_URL = "sandBoxUrl=";
        public const string ARG_SFDC_NUM_THREADS = "numThreads";
        public const string ARG_CONNECTOR_ENGINE = "extractionEngine=";
        public const string ARG_CONNECTOR_TYPE = "connectorType=";
        public const string ARG_DELETE_ALL = "deleteAll=";
        public const string ARG_DELETE_RESTORE = "restore=";
        public const string ARG_WRITE_EMAILER_INFO = "passEmailInfo=";
        public const string ARG_LIVE_ACCESS_LOADGROUPS = "liveAccessLoadGroups=";
        public const string ARG_LOAD_RETRY_FAILED = "retryFailed=";
        public const string ARG_ADMIN_DB_PARAM_NAME = "dbparam=";
        public const string ARG_ADMIN_DB_PARAM_VALUES = "dbparamValues=";
        public const string ARG_EXTRACT_ONLY = "extractionOnly=";
        public const string ARG_PARENT_SOURCE = "parentSource=";
        public const string ARG_EXTRACT_GROUPS = "extractGroups=";
        public const string ARG_CONNECTOR_CONNECTION_STRING = "connectorConnectionString=";
        // Birst Email 
        public const string ARG_BIRST_PROCESS_EMAIL_TYPE = "emailType=";
        public const string ARG_BIRST_IMAGE_PATH = "birstImage=";
        public const string ARG_BIRST_EMAIL_TEMPLATE_PATH = "birstEmailTemplate=";
        public const string ARG_BIRST_EMAIL_USE_SSL = "useSSL=";
        // For failure emails on processing. Reasons
        public const string ARG_BIRST_PROCESS_FAILURE_TYPE = "failureType=";
        public const string ARG_BIRST_PROCESS_FAILURE_DETAILS = "failureDetail=";

        public const string CMD_ARG_VALUE_BUILDER = "appBuilder";
        public const string CMD_ARG_VALUE_LOADER = "appLoader";
        public const string CMD_ARG_VALUE_POST_SFDCEXTRACT = "postSFDCExtract";
        public const string CMD_ARG_VALUE_GET_SFDC_ENGINE = "getSFDCEngineCommand";
        public const string CMD_ARG_VALUE_POST_PROCESS = "postProcess";
        public const string CMD_ARG_VALUE_DELETE_LOAD = "deleteLoad";
        public const string CMD_ARG_VALUE_POST_DELETE = "postDeleteLoad";
        public const string CMD_ARG_VALUE_LOAD_EMAIL = "sendProcessEmail";
        public const string CMD_ARG_VALUE_EXTRACTION_EMAIL = "sendExtractionEmail";
        public const string CMD_ARG_VALUE_UPDATE_DB = "updateAdminDB";
        public const string CMD_ARG_VALUE_GET_TRANSACTIONAL_OBJECT = "getTransactionObject";
        public const string CMD_ARG_VALUE_BIRST_PARAMETERS = "getBirstParameters";
        public const string CMD_ARG_VALUE_CONNECTOR_VARIABLES = "getConnectorVariables";

        public const string ARGUMENT_SPACEID = "spaceid=";
        public const string ARGUMENT_CMD_ACTION = "cmdAction=";
        // two ways to pass db information
        // one way is to pass server,dbname, user, pwd
        public const string ARGUMENT_DBSERVER = "dbserver=";
        public const string ARGUMENT_DBNAME = "dbname=";
        public const string ARGUMENT_DBUSER = "dbuser=";
        public const string ARGUMENT_DBPASSWORD = "dbpwd=";

        public const string ARGUMENT_WEB_CONFIG_DIR = "webConfigDir=";
    }
}
