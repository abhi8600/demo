﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornExecute.Exceptions;
using System.IO;

namespace AcornExecute.Utils
{
    class AcornExecuteUtil
    {
        public static Dictionary<String,String> parseCmdArguments(string[] args, string[] lookUpArgs)
        {
            Dictionary<String, String> response = new Dictionary<string, string>();
            if (args != null && args.Length > 0 && lookUpArgs != null && lookUpArgs.Length > 0)
            {
                foreach (string arg in args)
                {
                    foreach(string lookUpArg in lookUpArgs)
                    {
                        if (arg.ToLower().StartsWith(lookUpArg.ToLower()))
                        {
                            response.Add(lookUpArg, extractArgValue(arg, lookUpArg));
                        }
                    }
                }
            }

            return response;
        }

        private static string extractArgValue(string arg, string argKey)
        {
            if (arg != null && argKey != null && argKey.StartsWith(argKey))
            {
                string result = arg.Substring(argKey.Length);
                if (arg == UtilConstants.ARG_EXTRACT_GROUPS && result.StartsWith("\"") && result.EndsWith("\""))
                {
                    result = result.Substring(1, result.Length - 2);
                }
                return result;
            }

            string errorMessage = "Invalid argument match. Looking for " + argKey + " in " + arg;
            throw new InvalidArgumentException(errorMessage);
        }

        public static void throwExceptionOnInvalidValue(string argKey, object argValue)
        {
            bool invalid = false;
            if (argValue == null)
            {
                invalid = true;
            }

            if (argValue is Guid && (Guid)argValue == Guid.Empty)
            {
                invalid = true;
            }

            if (argValue is DateTime && (DateTime)argValue == DateTime.MinValue)
            {
                invalid = true;
            }

            if(invalid)
            {
                noArgFound(argKey);
            }
        }

        public static void noArgFound(string requiredArg)
        {
            throw new InvalidArgumentException(requiredArg + " is not supplied. Please refer to usage");
        }

        public static StringBuilder getStringBuilder(List<String> stringList, string delimiter)
        {
            StringBuilder sb = new StringBuilder();
            foreach (String str in stringList)
            {
                if (sb.Length > 0 && delimiter != null)
                {
                    sb.Append(delimiter);
                }
                sb.Append(str);
                
            }
            return sb;
        }

        public static void addNewLine(StringBuilder sb)
        {
            sb.Append("\n");
        }

        public static string getUniqueFileName(string spaceDirectory, Guid uid)
        {
            return  getSLogsDirectory(spaceDirectory) + Path.DirectorySeparatorChar + "message." + uid.ToString() + ".txt";
        }

        // space specific slogs directory
        public static string getSLogsDirectory(string spaceDirectory)
        {
            return spaceDirectory + Path.DirectorySeparatorChar + "slogs";
        }

        public static void setLogging(string spaceID, string extractionUID)
        {
            log4net.NDC.Clear();
            if (spaceID != null)
            {
                string val = spaceID;
                if (extractionUID != null)
                {
                    val = val + "," + extractionUID;
                }
                log4net.NDC.Push(val);
            }
        }



        public static void validateSpaceConcurrentStatus(Acorn.Space sp, StringBuilder sb)
        {
            bool isBeingPublished = Acorn.ApplicationLoader.isSpaceBeingPublished(sp);
            if (isBeingPublished)
            {
                throw new AcornExecuteException("Space is being published", AcornExecuteException.EXIT_CODE_SPACE_PUBLISH_CONCURRENT);
            }

            int spaceOpID = Acorn.Utils.SpaceOpsUtils.getSpaceAvailability(sp);
            if (!Acorn.Utils.SpaceOpsUtils.isSpaceAvailable(spaceOpID))
            {
                Program.systemLog.Info("Cannot delete : space id: " + sp.ID + " : name : " + sp.Name
                    + " in progress. " + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));

                AcornExecuteUtil.addNewLine(sb);
                sb.Append("availabilityMessage=" + Acorn.Utils.SpaceOpsUtils.getAvailabilityMessage(sp.Name, spaceOpID));
                throw new AcornExecuteException("Space is busy in other operation", AcornExecuteException.EXIT_CODE_SPACE_BUSY_OTHER);
            }
        } 
    }
}
