package com.birst.BDS.mapreduce;


import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;

import com.successmetricsinc.transformation.ParseResult;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;


/**
 *
 * @author Nima Rad
 */
public class XMLBaseMapper extends Mapper<LongWritable, Text, Writable, MapWritable> {

	@Override
	public void map(LongWritable key, Text record,Context context) throws IOException, InterruptedException{

		String script = context.getConfiguration().get("mapper.script");
		TransformationScript tfScript = new TransformationScript(record.toString());
		//tfScript.mapperContext = context;
		try {
			ParseResult w = tfScript.parseScript(script);
			tfScript.executeMRScript();
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		context.write(key, tfScript.outputRow);	
	}
}
