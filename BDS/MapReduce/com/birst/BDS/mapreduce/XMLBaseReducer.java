package com.birst.BDS.mapreduce;

import java.io.IOException;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

public class XMLBaseReducer extends Reducer<Writable,MapWritable,Writable,MapWritable> {
	
	@Override
	public void reduce(Writable key, Iterable<MapWritable> values, Context context){
		for(MapWritable value:values){
			try {
				context.write(new Text(key.toString()), value);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	
}


