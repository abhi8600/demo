package com.birst.BDS.deployer;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.birst.BDS.deployer.XmlInputFormat;
import com.birst.BDS.mapreduce.XMLBaseMapper;
import com.birst.BDS.mapreduce.XMLBaseReducer;

public class Deploy {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// create a configuration
		Configuration conf = new Configuration();
		// this should be like defined in your mapred-site.xml
		conf.set("mapred.job.tracker", "192.168.90.145:9001"); 
		// like defined in hdfs-site.xml
		//conf.set("hadoop.job.ugi", "hadoop");
		conf.set("fs.default.name", "hdfs://192.168.90.145:9000");
		//conf.set("xmlinput.start", "<MSG");
		//conf.set("xmlinput.end", "</MSG>");
		//conf.set("mapred.map.tasks",  "20");
		//conf.set("mapred.compress.map.output", "true");
		//conf.set("mapred.output.compression.type", "BLOCK"); 
		//conf.set("mapred.map.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
		//conf.set("BDS.Columns" ,"ip|etime|latency|source|api|disp");
		conf.set("mapper.script", loadScript("./Mapper.script"));
		//conf.set("reducer.script", loadScript("./Reducer.script"));
		
		System.out.println(conf.get("mapper.script"));
		
	/*	Job job = new Job(conf);
		// here you have to put your mapper class
		job.setMapperClass(XMLBaseMapper.class);
		job.setMapSpeculativeExecution(true);
		// here you have to put your reducer class
		//job.setReducerClass(Reduce.class);
		job.setReducerClass(XMLBaseReducer.class);

		// here you have to set the jar which is containing your 
		// map/reduce class, so you can use the mapper class
		//job.setJarByClass(Deploy.class);
		job.setJarByClass(XMLBaseMapper.class);

		// key/value of your reducer output

		job.setMapOutputValueClass(MapWritable.class);

		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(XmlInputFormat.class);
		//XmlInputFormat.addInputPath(job, new Path("/hadoop/demoInput/input.xml"));
		XmlInputFormat.addInputPath(job, new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/raw/data.xml"));

		//Path outputDir = new Path("/hadoop/demoOutput/"+System.currentTimeMillis());
		Path outputDir = new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/results/");
		/*job.setOutputFormatClass(BDSOutputFormat.class);
		BDSOutputFormat.setOutputPath(job, outputDir);
		Vector<Text> cols = new Vector<Text>(); 
		cols.add(new Text("name"));
		cols.add(new Text("numbers"));
		BDSOutputFormat.setColumns(cols);
		
		
		job.setOutputFormatClass(BDSOutputFormat.class);
		TextOutputFormat.setOutputPath(job, outputDir);
*/
		// this waits until the job completes and prints debug out to STDOUT or whatever
		// has been configured in your log4j properties.
		
		
		
		
		
		
		
		Job job = new Job(conf);

		job.setMapperClass(XMLBaseMapper.class);
		job.setReducerClass(XMLBaseReducer.class);
		job.setJarByClass(XMLBaseMapper.class);
		
		job.setInputFormatClass(XmlInputFormat.class);
		job.setOutputFormatClass(BDSOutputFormat.class);
		
		job.setMapOutputValueClass(MapWritable.class);

		XmlInputFormat.addInputPath(job, new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/raw/data.xml"));
		BDSOutputFormat.setOutputPath(job,  new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/results/"));

		job.waitForCompletion(true);
		job.waitForCompletion(true);

	}


	private static String loadScript(String location) throws Exception{
		
		StringBuffer script = new StringBuffer();
		FileInputStream fstream = new FileInputStream(location);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		String curLine = "";
		while ((curLine = br.readLine()) != null)
		{
			script.append(curLine + "\n");
		}

		in.close();

		return script.toString();
	}

}
