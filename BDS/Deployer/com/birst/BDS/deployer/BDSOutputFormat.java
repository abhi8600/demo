/**
 * 
 */
package com.birst.BDS.deployer;

import java.io.DataOutputStream;
import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.OutputCommitter;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.security.TokenCache;


/**
 * @author Nima Rad

 *
 */
public class BDSOutputFormat extends FileOutputFormat<Writable, MapWritable> {
	private OutputCommitter committer = null;

	@Override
	public RecordWriter<Writable, MapWritable> getRecordWriter(TaskAttemptContext job) throws IOException, InterruptedException {
		
		String columnNames = job.getConfiguration().get("BDS.Columns");
		Path file = getDefaultWorkFile(job, "");
		FileSystem fs = file.getFileSystem(job.getConfiguration());
		FSDataOutputStream fileOut = fs.create(file);
		fileOut.writeBytes(columnNames + "\n" );
		
		return new BDSRecordWriter(fileOut,columnNames.split("\\|"));
	}

	@Override
	public void checkOutputSpecs(JobContext job) throws IOException {

		// Ensure that the output directory is set
		Path outDir = getOutputPath(job);
		if (outDir == null) {
			throw new IOException("Output directory not set in JobConf.");
		}
		// get delegation token for outDir's file system
		TokenCache.obtainTokensForNamenodes(job.getCredentials(),
				new Path[] { outDir }, job.getConfiguration());		
	}

	@Override
	public OutputCommitter getOutputCommitter(TaskAttemptContext context)
	throws IOException {
		if (committer == null) {
			Path output = getOutputPath(context);
			committer = new FileOutputCommitter(output, context);
		}
		return committer;
	}
	
		
	
	public class BDSRecordWriter extends RecordWriter<Writable,MapWritable> {
		protected DataOutputStream outStream;
		protected String[] columns;

		public BDSRecordWriter(DataOutputStream out, String[] columns) throws IOException, InterruptedException{
			this.outStream = out;
			this.columns = columns;
		}

		@Override
		public void close(TaskAttemptContext arg0) throws IOException,
		InterruptedException {
			outStream.close();
		}

		@Override
		public void write(Writable ignored, MapWritable row) throws IOException,InterruptedException {

			Writable value;
			StringBuffer output = new StringBuffer();
			for (int i = 0; i < columns.length  ; i++){

				value = row.get(new Text(columns[i]));
				output.append( (value == null ? "NULL" : value.toString()) +  "|");
			}

			output.replace(output.length() - 1,output.length() , "\n");		
			outStream.writeBytes(output.toString());
			
		}

	}

}
