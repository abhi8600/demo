package com.birst.BDS.deployer;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.mapreduce.Job;
import com.birst.BDS.deployer.XmlInputFormat;
import com.birst.BDS.mapreduce.XMLBaseMapper;
import com.birst.BDS.mapreduce.XMLBaseReducer;

public class Deployer extends Configured implements Tool {
    
    public int run(String[] args) throws Exception {
      // Configuration processed by ToolRunner
      Configuration conf = getConf();
      
      // Create a JobConf using the processed conf
    //  JobConf job = new JobConf(conf, Deployer.class);
      

      // Submit the job, then poll for progress until the job is complete
   //   JobClient.runJob(job);
          
		Job job = new Job(conf);

		job.setMapperClass(XMLBaseMapper.class);
		job.setReducerClass(XMLBaseReducer.class);
		job.setJarByClass(XMLBaseMapper.class);
		
		job.setInputFormatClass(XmlInputFormat.class);
		job.setOutputFormatClass(BDSOutputFormat.class);
		
		job.setMapOutputValueClass(MapWritable.class);

		XmlInputFormat.addInputPath(job, new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/raw/data.xml"));
		BDSOutputFormat.setOutputPath(job,  new Path("/hadoop/customer/"+ args[0] + "/" + args[1] + "/results/"));

		job.waitForCompletion(true);

      return 0;
    }
    
    public static void main(String[] args) throws Exception {
      // Let ToolRunner handle generic command-line options 
      int res = ToolRunner.run(new Configuration(), new Deployer(), args);
      
      System.exit(res);
    }
  }