﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSOTest.aspx.cs" Inherits="SSOTest.SSOTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms. 
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP.NET Birst SSO Test Page</title>
</head>
<body>
    <h1>ASP.NET Birst SSO Test Page</h1>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>
                Login Service Base URL:
            </td>
            <td>
                <asp:TextBox ID="baseUrl"  Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                For production this would be https://login.bws.birst.com
            </td>
        </tr>
        <tr>
            <td>
                Birst user name:
            </td>
            <td>
                <asp:TextBox ID="username" Width="300" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Space Id:
            </td>
            <td>
                <asp:TextBox ID="BirstSpaceId" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                This space must be enabled for SSO and the Birst user must have access to it
            </td>
        </tr>        
        <tr>
            <td>
                Space name:
            </td>
            <td>
                <asp:TextBox ID="spacename" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
            Deprecated (users can have access to multiple spaces with the same name), please use Space Id (BirstSpaceId)
            </td>
        </tr>
        <tr>
            <td>
                SSO password:
            </td>
            <td>
                <asp:TextBox ID="ssopassword" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                The SSO password is space specific (this is not your account password). You need
                to enable your space for SSO access and then request an SSO password (you will receive
                it via email).
            </td>
        </tr>
        <tr>
            <td>
                Module:
            </td>
            <td>
                <asp:DropDownList ID="module" runat="server">
                    <asp:ListItem Value="designer" />
                    <asp:ListItem Value="dashboard" />
                    <asp:ListItem Value="customupload" />
                </asp:DropDownList>           
            </td>
            <td>
                designer, dashboard, customupload
            </td>
        </tr>
        <tr>
            <td>
                Dashboard name:
            </td>
            <td>
                <asp:TextBox ID="dashboardName2" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Dashboard name.
            </td>            
        </tr>
        <tr>
            <td>
                Dashboard page:
            </td>
            <td>
                <asp:TextBox ID="pageName" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Page name.
            </td>            
        </tr>
        <tr>
            <td>
                Report name:
            </td>
            <td>
                <asp:TextBox ID="reportName" Width="300" runat="server"></asp:TextBox>
            </td>
            <td>
                Report name.
            </td>            
        </tr>     
        <asp:TextBox ID="dashboardname" Width="300" runat="server" Visible="false"></asp:TextBox>                           
        <tr>
            <td>
                Extra parameters (optional):
            </td>
            <td>
                <asp:TextBox ID="extraParameters" Width="300" runat="server" />
            </td>
            <td>
                This is for dashboard parameters, must start with &amp; and the names and values
                must be properly URL encoded (e.g., &amp;Time.Year=1998&amp;Account.Name=ACME%20Labs)
                .&nbsp; See <a href="http://en.wikipedia.org/wiki/Query_string" target="_blank">http://en.wikipedia.org/wiki/Query_string</a></td>
        </tr>
        <tr>
            <td>
                Security session variable (optional):
            </td>
            <td>
                <asp:TextBox ID="sessionVars" Width="300" runat="server"></asp:TextBox><br />
            </td>
        </tr>
        <tr>
            <td>
                Embedded:
            </td>
            <td>
                <asp:CheckBox ID="embedded" runat="server" />
            </td>
            <td>
                Set up the page for embedding within another page (via IFRAME).  This hides the top level navigation bar.
            </td>
        </tr>
        <tr>
            <td>
                Hide dashboard navigation:
            </td>
            <td>
                <asp:CheckBox ID="hideDashboardNavigation" runat="server" />
            </td>
            <td>
                This hides the dashboard navigation tabs
            </td>
        </tr>
        <tr>
            <td>
                Hide dashboard prompts:
            </td>
            <td>
                <asp:CheckBox ID="hideDashboardPrompts" runat="server" />
            </td>
            <td>
                This hides the dashboard prompts
            </td>
        </tr>    
        <tr>
            <td>
                View Mode:
            </td>
            <td>
                <asp:DropDownList ID="viewMode" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="borderless" />
                    <asp:ListItem Value="full" />
                    <asp:ListItem Value="headerless" />
                </asp:DropDownList>
            </td>
            <td>
                Set the dashboard view mode (borderless, full, headerless)
            </td>
        </tr>    
        <tr>
            <td>
                wmode:
            </td>
            <td>
                <asp:DropDownList ID="wmode" runat="server">
                    <asp:ListItem Value="" />
                    <asp:ListItem Value="window" />
                    <asp:ListItem Value="opaque" />
                    <asp:ListItem Value="transparent" />
                    <asp:ListItem Value="direct" />  
                    <asp:ListItem Value="gpu" />                                                          
                </asp:DropDownList>
            </td>
            <td>
                Set the Adobe Flash wmode parameter (if you don't know what this is, don't set it)
            </td>
        </tr>                             
    </table>
    <p />
    <asp:Button ID="Submit" runat="server" Text="Submit SSO request" OnClick="SSOTest_Submit" />
    </form>
</body>
</html>
