﻿//
// Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
// BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.IO;
using System.Net;
using System.Text;

namespace SSOTest
{
    public partial class SSOTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // process the request from the sample web form
        protected void SSOTest_Submit(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("baseUrl: " + baseUrl.Text);
            System.Diagnostics.Trace.WriteLine("username: " + username.Text + ", spaceid: " + BirstSpaceId.Text + ", spacename: " + spacename.Text + ", ssopassword: " + ssopassword.Text);
            System.Diagnostics.Trace.WriteLine("module: " + module.SelectedValue);
            // get the security token
            string birstURL = null;
            string token = getSecurityToken(baseUrl.Text + "/TokenGenerator.aspx", username.Text, BirstSpaceId.Text, spacename.Text, ssopassword.Text, sessionVars.Text, ref birstURL);
            if (token == null || token.Length == 0)
            {
                Response.Status = "400 Bad Request";
                return;
            }

            string redirectUrl = birstURL + "/SSO.aspx?BirstSSOToken=" + token;
            System.Diagnostics.Trace.WriteLine("Redirecting to " + redirectUrl);
            string moduleValue = module.SelectedValue.ToLower();
            if (moduleValue == "adhoc")
                moduleValue = "designer";
            if (moduleValue == "dashboard" || moduleValue == "designer" || moduleValue == "customupload" || moduleValue == "export")
            {
                redirectUrl = redirectUrl + "&birst.module=" + moduleValue;
            } 
            if (dashboardname.Text != null && dashboardname.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.name=" + Server.UrlEncode(dashboardname.Text);
            if (dashboardName2.Text != null && dashboardName2.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.dashboard=" + dashboardName2.Text;
            if (pageName.Text != null && pageName.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.page=" + pageName.Text;
            if (reportName.Text != null && reportName.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.reportName=" + reportName.Text;
            redirectUrl = redirectUrl + "&birst.embedded=" + embedded.Checked.ToString().ToLower();
            redirectUrl = redirectUrl + "&birst.hideDashboardNavigation=" + hideDashboardNavigation.Checked.ToString().ToLower();
            redirectUrl = redirectUrl + "&birst.hideDashboardPrompts=" + hideDashboardPrompts.Checked.ToString().ToLower();
            if (viewMode.SelectedValue != null && viewMode.SelectedValue.Length > 0)
                redirectUrl = redirectUrl + "&birst.viewMode=" + viewMode.SelectedValue.ToLower();
            if (wmode.Text != null && wmode.SelectedValue.Length > 0)
                redirectUrl = redirectUrl + "&birst.wmode=" + wmode.SelectedValue.ToLower();
            redirectUrl = redirectUrl + extraParameters.Text;
            System.Diagnostics.Trace.WriteLine(redirectUrl);
            Response.Redirect(redirectUrl);
        }
 
        //
        // POST a request to the security token service for a opaque token for the user and space
        //
        private static string getSecurityToken(string url, string username, string spaceid, string spacename, string ssopassword, string sessionVars, ref string redirectURL)
        {
            System.Diagnostics.Trace.WriteLine("tokenUrl: " + url);
            WebRequest wrq = WebRequest.Create(url);
            wrq.Method = "POST";    // for security reasons this request must be a POST
            wrq.ContentType = "application/x-www-form-urlencoded";  // make it look like a form submission

            // create the post data and write it to the stream
            string postData = "username=" + username + "&ssopassword=" + ssopassword;
            if (spaceid != null && spaceid.Length > 0)
            {
                postData += "&BirstSpaceId=" + spaceid;
            }
            else
            {
                postData += "&spacename=" + spacename;
            }
            if (sessionVars != null && sessionVars.Length > 0)
            {
                postData = postData + "&sessionVars=" + sessionVars;
            }
            System.Diagnostics.Trace.WriteLine("postData: " + postData);
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            wrq.ContentLength = byteArray.Length;
            Stream dataStream = wrq.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
 
            WebResponse wresp = null;
            try
            {
                wresp = wrq.GetResponse();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex);
                return null;
            }
            // get the redirect URL
            redirectURL = wresp.Headers["BirstURL"];
            // read the response - the opaque token
            StreamReader sr = new StreamReader(wresp.GetResponseStream(), System.Text.Encoding.ASCII);
            string result = sr.ReadToEnd();
            sr.Close();
            wresp.Close();
            return result;
        }
    }
}
