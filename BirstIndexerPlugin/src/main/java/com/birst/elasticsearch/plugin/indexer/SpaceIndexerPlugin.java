package com.birst.elasticsearch.plugin.indexer;

import org.elasticsearch.plugins.AbstractPlugin;
import org.elasticsearch.rest.RestModule;

public class SpaceIndexerPlugin extends AbstractPlugin {

	public String description() {
		return "Birst Space Indexer Plugin";
	}

	public String name() {
		return "SpaceIndexerPlugin";
	}
	
	public void onModule(RestModule module) {
		module.addRestAction(SpaceIndexerRestAction.class);
	}

}
