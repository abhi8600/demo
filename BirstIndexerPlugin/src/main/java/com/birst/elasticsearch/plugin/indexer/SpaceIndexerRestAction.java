package com.birst.elasticsearch.plugin.indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.xml.stream.XMLStreamException;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.rest.BaseRestHandler;
import org.elasticsearch.rest.RestChannel;
import org.elasticsearch.rest.RestController;
import org.elasticsearch.rest.RestRequest;
import org.elasticsearch.rest.RestRequest.Method;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.rest.StringRestResponse;

import com.birst.elasticsearch.report.Adhoc;
import com.birst.elasticsearch.report.BaseReport;
import com.birst.elasticsearch.report.Visualizer;
import com.fasterxml.jackson.core.JsonProcessingException;

public class SpaceIndexerRestAction extends BaseRestHandler{
	static ESLogger birstLogger = Loggers.getLogger("birst");
	static final String IDX_VERSION = "1.0";
	static final String ADHOC_REPORT = "adhocReport";
	static final String SPACES_DIR = "SPACES_DIR";
	
	@Inject
	protected SpaceIndexerRestAction(Settings settings, Client client, RestController controller) {
		super(settings, client);
		controller.registerHandler(Method.GET, "/_spaceIndexer/", this);
		controller.registerHandler(Method.POST, "/_spaceIndexer/{spaceId}", this);
		controller.registerHandler(Method.POST, "/_spaceIndexer/{spaceId}/report/{reportPath}", this);
		controller.registerHandler(Method.DELETE, "_spaceIndexer/{spaceId}/report/{reportPath}", this);
	}

	public void handleRequest(final RestRequest request, final RestChannel channel) {
		if(request.method() == Method.GET){
			sendInfoResponse(request, channel);
			return;
		}
	
		if(getSpaceDir() == null){
			sendBadRequest(channel, "$SPACES_DIR environment / property not set");
			return;
		}
		
		//Checks
		String spaceId = request.param("spaceId");
		if(spaceId == null){
			sendBadRequest(channel, "SpaceId parameter is required");
			return;
		}
				
		//Verify the space exists
		File catalogDir = getCatalogDirectory(spaceId);
		if(! (catalogDir.exists() && catalogDir.isDirectory()) ){
			sendBadRequest(channel, "Space doesn't exist");
			return;
		}
		
		String reportPath = request.param("reportPath");
		
		if(reportPath != null){
			if(request.method() == Method.POST){
				handlePostReport(channel, spaceId, reportPath);
			}else if(request.method() == Method.DELETE){
				handleDeleteReport(channel, spaceId, reportPath);
			}
		}else{
			//Index all the report files in the space
			handleIndexSpace(channel, spaceId);
		}
	}
	
	private void sendInfoResponse(final RestRequest request, final RestChannel channel){
		String spacesDir = getSpaceDir();
		if(spacesDir == null){
			spacesDir = "";
		}
		String responseStr = "";
		try{
			XContentBuilder xb = XContentFactory.jsonBuilder();
			xb.startObject().
				field("Version", IDX_VERSION).
				field("SPACE_DIR", spacesDir).
			endObject();
			responseStr = xb.prettyPrint().string();
		}catch(IOException e){
			responseStr = "{ \"Version\": \"" + IDX_VERSION + "\" \"SPACE_DIR\": \"" + spacesDir + "\"}";
		}
		StringRestResponse response = new StringRestResponse(RestStatus.OK, responseStr);
		channel.sendResponse(response);
	}
	
	private String getSpaceDir(){
		String spacesDir = System.getProperty(SPACES_DIR);
		if(spacesDir == null){
			spacesDir = System.getenv(SPACES_DIR);
		}
		return spacesDir;
	}
	
	private static void sendBadRequest(RestChannel channel, String msg){
		String responseStr = null;
		XContentBuilder xb = null;
		try {
			xb = XContentFactory.jsonBuilder();
			xb.startObject().
				field("msg", msg).
			endObject();
			responseStr = xb.string();
		} catch (IOException e) {
			responseStr = "{ \"msg\" : " + "\"" + msg + "\" }";
		}
		
		StringRestResponse response = new StringRestResponse(RestStatus.BAD_REQUEST, responseStr);
		channel.sendResponse(response);
	}
	
	/**
	 * Create or overwrite the index for the particular report
	 * @param channel
	 * @param spaceId
	 * @param reportPath
	 */
	private void handlePostReport(final RestChannel channel, String spaceId, String reportPath){
		if(doIndexSpaceExist(spaceId)){
			String type = "";
			
			BaseReport report = null;
			File catalogDir = getCatalogDirectory(spaceId);
			String reportFilePath = null;
			try {
				reportFilePath = catalogDir.getCanonicalPath() + File.separator + reportPath;
			} catch (IOException e1) {
				birstLogger.error("Catalog directory's canonical path failed", e1);
				sendBadRequest(channel, "Cannot get catalogDir's canonical path");
				return;
			}
			
			File reportFile = new File(reportFilePath);
			
			if(reportPath.endsWith(".AdhocReport")){
				type = BaseReport.TYPE_ADHOC;
				try {
					report = new Adhoc( reportFile);
				} catch (FileNotFoundException e) {
					birstLogger.error("File not found for report path " + reportPath, e);
					sendBadRequest(channel, "File not found");
					return;
				} catch (UnsupportedEncodingException e) {
					birstLogger.error("Bad encoding for report " + reportPath, e);
					sendBadRequest(channel, "Bad file encoding");
					return;
				} catch (XMLStreamException e) {
					birstLogger.error("XMLStreamException error", e);
					sendBadRequest(channel, "XMLStreamException");
					return;
				}
			}else if (reportPath.endsWith(".viz.dashlet")){
				type = BaseReport.TYPE_VIZ;
				try {
					report = new Visualizer(reportFile, catalogDir);
				} catch (JsonProcessingException e) {
					birstLogger.error("Unable to process data for file " + reportPath, e);
					sendBadRequest(channel, "Bad file data");
					return;
				} catch (IOException e) {
					birstLogger.error("IO error for file " + reportPath, e);
					sendBadRequest(channel, "IO Error");
					return;
				}
			}else{
				sendBadRequest(channel, "Invalid report type");
				return;
			}
			IndexResponse response = client.prepareIndex(getIndexName(spaceId), type, report.getPath()).setSource(report.toJson()).execute().actionGet();
			if(response.isCreated()){
				channel.sendResponse(new StringRestResponse(RestStatus.CREATED));
				return;
			}else{
				channel.sendResponse(new StringRestResponse(RestStatus.OK));
				return;
			}
		}else{
			handleIndexSpace(channel, spaceId);
		}
	}
	
	/**
	 * Delete the index for the particular report
	 * @param channel
	 * @param spaceId
	 * @param reportPath
	 * @throws IOException 
	 */
	private void handleDeleteReport(final RestChannel channel, String spaceId, String reportPath){
		if(doIndexSpaceExist(spaceId)){
			String type = "";
			
			if(reportPath.endsWith(".AdhocReport")){
				type = BaseReport.TYPE_ADHOC;
				reportPath = reportPath.replaceAll(".AdhocReport", "");
			}else if (reportPath.endsWith(".viz.dashlet")){
				type = BaseReport.TYPE_VIZ;
				reportPath = reportPath.replaceAll(".viz.dashlet", "");
			}else{
				sendBadRequest(channel, "Invalid report type");
				return;
			}
			
			DeleteResponse response = client.prepareDelete(getIndexName(spaceId), type, reportPath).execute().actionGet();
			if(!response.isFound()){
				channel.sendResponse(new StringRestResponse(RestStatus.NOT_FOUND));
				return;
			}else{
				channel.sendResponse(new StringRestResponse(RestStatus.OK));
				return;
			}
		}else{
			handleIndexSpace(channel, spaceId);
		}
	}
	
	/**
	 * Recurse through the space's catalog and index all applicable report file
	 * @param channel
	 * @param spaceId
	 */
	private void handleIndexSpace(final RestChannel channel, String spaceId){
		try {
			initIndex(spaceId);
			channel.sendResponse(indexSpace(spaceId));
		} catch (IOException e) {
			channel.sendResponse(new StringRestResponse(RestStatus.INTERNAL_SERVER_ERROR));
			birstLogger.error("Indexing error", e);
		}
	}
	
	private boolean doIndexSpaceExist(String spaceId){
		String indexName = getIndexName(spaceId);
		return client.admin().indices().prepareExists(indexName).execute().actionGet().isExists();
	}
	
	private File getCatalogDirectory(String spaceId){
		String spacesDir = System.getenv(SPACES_DIR);
		String catalogPath = spacesDir + File.separator + spaceId + File.separator + "catalog";
		return new File(catalogPath);
	}
	
	private String getIndexName(String spaceId){
		return "birst_" + spaceId; //"space_v_" + IDX_VERSION + "_" + spaceId;
	}

	private StringRestResponse indexSpace(String spaceId) throws IOException{
		String indexName = getIndexName(spaceId);
		File catalogDir = getCatalogDirectory(spaceId);
		
		if(catalogDir.exists() && catalogDir.isDirectory()){
			BulkProcessor bulkProc = 
				BulkProcessor.builder(client,
					new BulkProcessor.Listener() {
						
						public void beforeBulk(long executionId, BulkRequest request) {
							// TODO Auto-generated method stub
							
						}
						
						public void afterBulk(long executionId, BulkRequest request,
								Throwable failure) {
							birstLogger.error("_spaceIndexer bulk request failed", failure);
						}
						
						public void afterBulk(long executionId, BulkRequest request,
								BulkResponse response) {
							// TODO Auto-generated method stub
							
						}
					}
			).setBulkActions(100).setConcurrentRequests(10).build();
			
			File[] subDirs = catalogDir.listFiles(RootDirFilter.getInstance());
			for(int i = 0; i < subDirs.length; i++){
				parseIndexFiles(subDirs[i], catalogDir, indexName, bulkProc);
			}
			
			bulkProc.close();
		}
		
		//Refresh for immediate search results
		client.admin().indices().prepareRefresh(indexName).execute().actionGet();
		
		return new StringRestResponse(RestStatus.OK, "{ \"msg\" : \"Indexed space " + spaceId + "\", \"newIndex\" : " + "\"" + indexName + "\"" + "} \n");
	}
	
	/***
	 * Create the index with a marker document. If the index exists, update the marker
	 * @param spaceId
	 * @throws IOException 
	 */
	private void initIndex(String spaceId) throws IOException{
		String spaceIndexName = getIndexName(spaceId);
		
		/*
		XContentBuilder xb = XContentFactory.jsonBuilder();
		xb.startObject().
			field("created", new Date()).
			field("creatorVersion", IDX_VERSION).
		endObject();
		*/
		
		//Analyzers
		XContentBuilder settings = null;
		try{
			settings = XContentFactory.jsonBuilder();
			settings.startObject().
				startObject("analysis").
					startObject("analyzer").
						startObject("report_search_analyzer").
							field("type", "custom").
							field("tokenizer", "standard").
							field("filter", new String[]{"lowercase"}).
						endObject().
						startObject("report_index_analyzer").
							field("type", "custom").
							field("tokenizer", "standard").
							field("filter", new String[] {"lowercase", "ngramSubstring"}).
						endObject().
				    endObject().
					startObject("filter").
						startObject("ngramSubstring").
							field("type", "nGram").
							field("min_gram", 2).
							field("max_gram", 30).
						endObject().
					endObject().
				endObject().
			endObject();
			
		}catch(IOException e){
			birstLogger.error("Unable to create settings", e);
			return;
		}
		
		CreateIndexRequestBuilder cir = client.admin().indices().prepareCreate(spaceIndexName);
		cir.setSettings(settings);
		cir.addMapping(BaseReport.TYPE_ADHOC, BaseReport.toMapping());
		cir.addMapping(BaseReport.TYPE_VIZ, BaseReport.toMapping());
		
		CreateIndexResponse response = cir.execute().actionGet();
		if(!response.isAcknowledged()){
			birstLogger.error("Unable to create index " + spaceIndexName);
		}
	}
	
	/**
	 * Recursively parse each adhocReport files found in the catalog
	 * @param f
	 * @param indexName
	 * @throws IOException
	 */
	private void parseIndexFiles(File f, File catalogDir, String indexName, BulkProcessor bulkProc) throws IOException{
		BaseReport report = null;
		
		if(f.getName().startsWith(".")){
			return;
		}
		
		if(f.isDirectory()){
			File[] children = f.listFiles();
			for(int i = 0; i < children.length; i++){
				parseIndexFiles(children[i], catalogDir, indexName, bulkProc);
			}
		}else if (f.getName().endsWith(".AdhocReport")){
			try {
				report = new Adhoc(f);
			} catch (FileNotFoundException e) {
				birstLogger.error("Unable to parse file: " + f.getAbsolutePath(), e);
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				birstLogger.error("Unable to parse file: " + f.getAbsolutePath(), e);
				e.printStackTrace();
			} catch (XMLStreamException e) {
				birstLogger.error("Unable to parse file: " + f.getAbsolutePath(), e);
				e.printStackTrace();
			}
		}else if (f.getName().endsWith(".viz.dashlet")){
			report = new Visualizer(f, catalogDir);
		}
		
		if(report != null){
			bulkProc.add(Requests.indexRequest(indexName).type(report.indexType()).id(report.getPath()).source(report.toJson()));
		}
	}
	
	static class RootDirFilter implements FilenameFilter{
		private static RootDirFilter inst;
		
		public static RootDirFilter getInstance(){
			if(inst != null){
				inst = new RootDirFilter();
			}
			return inst;
		}
		
		public boolean accept(File dir, String name) {
			return (name != null && (name.equalsIgnoreCase("private") || name.equalsIgnoreCase("shared")));
		}
	}
}
