package com.birst.elasticsearch.report;

import java.io.File;
import java.io.IOException;

import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

public abstract class BaseReport {
	public final static String IDX_FIELD_CREATOR = "creator";
	public final static String IDX_FIELD_LAST_MOD = "lastModified";
	public final static String IDX_FIELD_FILENAME = "filename";
	public final static String IDX_FIELD_DIMENSIONS = "dimensions";
	public final static String IDX_FIELD_MEASURES = "measures";
	public final static String IDX_FIELD_EXPRESSIONS = "expressions";
	public final static String IDX_FIELD_CHARTS = "charts";
	public final static String IDX_FIELD_LABELS = "columnLabels";
	public final static String IDX_FIELD_ATTR_NAME = "columnNames";
	public final static String IDX_FIELD_SUBREPORTS = "subreports";
	public final static String IDX_FIELD_PROMPT_FILTERS = "promptFilters";
	public final static String IDX_FIELD_DIR = "dir";
	
	public final static String TYPE_ADHOC = "adhocReport";
	public final static String TYPE_VIZ = "viz_dashlet";
	
	static ESLogger birstLogger = Loggers.getLogger("birst");
	
	protected long lastModified;
	
	public BaseReport(File reportFile) { 
		if(reportFile != null){
			lastModified = reportFile.lastModified();
		}
	};
	
	public static XContentBuilder toMapping() throws IOException { 	
		XContentBuilder xb = XContentFactory.jsonBuilder();
		xb.startObject();
			xb.startObject("properties");
				stringField(xb, IDX_FIELD_CREATOR);
				stringField(xb, IDX_FIELD_DIMENSIONS);
				stringField(xb, IDX_FIELD_MEASURES);
				stringField(xb, IDX_FIELD_CHARTS);
				stringField(xb, IDX_FIELD_EXPRESSIONS);
				stringField(xb, IDX_FIELD_DIR);
				stringField(xb, IDX_FIELD_LABELS);
				stringField(xb, IDX_FIELD_ATTR_NAME);
				stringField(xb, IDX_FIELD_PROMPT_FILTERS);
				dateField(xb, IDX_FIELD_LAST_MOD);		
			xb.endObject();
		xb.endObject();
		return xb;
	}
	
	private static XContentBuilder stringField(XContentBuilder xb, String key) throws IOException{
		xb.startObject(key).
			field("type", "string").
			field("search_analyzer", "report_search_analyzer").
			field("index_analyzer", "report_index_analyzer").
			startObject("fields").
				startObject("raw").
					field("type", "string").
					field("index", "not_analyzed").
				endObject().
			endObject().
		endObject();
		
		return xb;
	}
	
	private static XContentBuilder dateField(XContentBuilder xb, String key) throws IOException{
		xb.startObject(key).
			field("type", "date").
			field("format", "date_hour_minute_second").
		endObject();
		
		return xb;
	}
	

	public abstract String indexType();
	
	public abstract String toJson();

	public abstract String getPath();
	
	public long getLastModified(){
		return lastModified;
	}
	
}
