package com.birst.elasticsearch.report;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Visualizer extends BaseReport {
	private String filename;
	
	private ObjectMapper mapper;
	
	private ObjectNode outputNode;
	private ArrayNode charts;
	private ArrayNode measures;
	private ArrayNode attributes;
	
	private String path;
	
	public Visualizer(File reportFile, File catalogDir) throws JsonProcessingException, IOException {
		super(reportFile);
		
		
		mapper = new ObjectMapper();
		outputNode = mapper.createObjectNode();
		
		
		//filename attribute
		filename = reportFile.getName().replaceAll(".viz.dashlet", "");
		outputNode.put(IDX_FIELD_FILENAME, filename);
		
		//dir attribute
		String reportPath = reportFile.getCanonicalPath();
		String catalogPath = catalogDir.getCanonicalPath();
		int idx = reportPath.indexOf(catalogPath);
		if (idx == 0){
			reportPath = reportPath.substring(catalogPath.length());
			reportPath = reportPath.replace('\\', '/');
			path = reportPath.replace(".viz.dashlet", "");
			reportPath = reportPath.replaceAll("/" + reportFile.getName(), "");
			outputNode.put(IDX_FIELD_DIR, reportPath);
		}
		
		//last modified
		outputNode.put(IDX_FIELD_LAST_MOD, lastModified);
		
		//Parse the json
		charts = addToOutputNode(IDX_FIELD_CHARTS);
		measures = addToOutputNode(IDX_FIELD_MEASURES);
		attributes = addToOutputNode(IDX_FIELD_DIMENSIONS);
	
		JsonNode root = mapper.readTree(reportFile);
		
		JsonNode measures = root.at("/stage/measures");
		for (JsonNode mes : measures){	
			JsonNode series = mes.path("series");
			for(JsonNode se : series){
				JsonNode chartName = se.path("type");
				if(!chartName.isMissingNode()){
					String chartType = chartName.asText();
					if(chartType != null && chartType.length() > 1){
						String firstUpperChartType = Character.toUpperCase(chartType.charAt(0)) + chartType.substring(1);
						charts.add(firstUpperChartType);
					}
				}
				
				addToAttrArrayNode(se.at("/column/expression/head"), "bql");
			}
		}
		
		addToAttrArrayNode(root.at("/stage/Category/expression/head/value"), "label");
		addToAttrArrayNode(root.at("/stage/Color/expression/head/value"), "label");
		addToAttrArrayNode(root.at("/stage/Shape/expression/head/value"), "label");
		addToAttrArrayNode(root.at("/stage/Size/expression/head/value"), "label");
		addToAttrArrayNode(root.at("/stage/HorizontalTrellis/expression/head/value"), "label");
		addToAttrArrayNode(root.at("/stage/VerticalTrellis/expression/head/value"), "label");
	}
	
	private ArrayNode addToOutputNode(String key){
		ArrayNode n = mapper.createArrayNode();
		outputNode.put(key, n);
		return n;
	}
	
	private void addToAttrArrayNode(JsonNode node, String valueKey){
		if(!node.isMissingNode()){
			String type = node.path("type").asText();
			ArrayNode arrayAttr = arrayNodeByAttrType(type);
			String val = node.path(valueKey).asText();
			if(val.length() > 0){
				arrayAttr.add(val);
			}
		}
	}
	
	private ArrayNode arrayNodeByAttrType(String type){
		if("measure".equals(type)){
			return measures;
		}else if ("attribute".equals(type)){
			return attributes;
		}else{
			return attributes;
		}
	}

	public String toJson() {
		String jsonStr = "";
		try{
			jsonStr = mapper.writeValueAsString(outputNode);
		}catch(JsonProcessingException ex){
			//TODO - logger
			ex.printStackTrace();
		}
		
		return jsonStr;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public String indexType() {
		return BaseReport.TYPE_VIZ;
	}
}
