package com.birst.elasticsearch.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;

@SuppressWarnings("restriction")
public class Adhoc extends BaseReport{
	static ESLogger birstLogger = Loggers.getLogger("birst");
	
	private final static String FILE_EXT = ".AdhocReport";
	private final static String ENTITIES = "Entities";
	private final static String ADHOC_REPORT_CLZ = "com.successmetricsinc.adhoc.AdhocReport";
	private final static String ENTITY_COLUMN = "com.successmetricsinc.adhoc.AdhocColumn";
	private final static String ENTITY_LABEL = "com.successmetricsinc.adhoc.AdhocLabel";
	private final static String ENTITY_CHART = "com.successmetricsinc.adhoc.AdhocChart";
	private final static String ENTITY_SUBREPORT = "com.successmetricsinc.adhoc.AdhocSubreport";
	private final static String COLUMN_TYPE = "ColumnType";
	private final static String DIMENSION = "Dimension";
	private final static String COLUMN = "Column";
	private final static String CHARTS = "Charts";
	private final static String CHART = "Chart";
	private final static String CHART_TYPE = "ChartType";
	private final static String DISPLAY_EXPR = "DisplayExpression";
	private final static String LABEL = "Label";
	private final static String SUBREPORT = "Subreport";
	private final static String FILTERS = "Filters";
	private final static String FILTER  = "Filter";
	private final static String PROMPT_NAME = "PromptName";
	private final static String NAME = "Name";
	
	public final static String IDX_FIELD_CREATOR = "creator";
	public final static String IDX_FIELD_LAST_MOD = "lastModified";
	public final static String IDX_FIELD_FILENAME = "filename";
	public final static String IDX_FIELD_DIMENSIONS = "dimensions";
	public final static String IDX_FIELD_MEASURES = "measures";
	public final static String IDX_FIELD_EXPRESSIONS = "expressions";
	public final static String IDX_FIELD_CHARTS = "charts";
	public final static String IDX_FIELD_LABELS = "columnLabels";
	public final static String IDX_FIELD_ATTR_NAME = "columnNames";
	public final static String IDX_FIELD_SUBREPORTS = "subreports";
	public final static String IDX_FIELD_PROMPT_FILTERS = "promptFilters";
	public final static String IDX_FIELD_DIR = "dir";
	
	private JsonObject jsonObj;
	private String name;
	private String path;
	
	private HashMap<String, JsonArrayBuilder> columnTypeLists = new HashMap<String, JsonArrayBuilder>();
	
	
	private JsonArrayBuilder dimensionList = Json.createArrayBuilder();
	private JsonArrayBuilder measureList = Json.createArrayBuilder();
	private JsonArrayBuilder expressionList = Json.createArrayBuilder();
	private JsonArrayBuilder ratioList = Json.createArrayBuilder();
	private JsonArrayBuilder chartList = Json.createArrayBuilder();
	
	private JsonArrayBuilder labelList = Json.createArrayBuilder();
	private JsonArrayBuilder attributeNameList = Json.createArrayBuilder();
	
	private JsonArrayBuilder subreportList = Json.createArrayBuilder();
	private JsonArrayBuilder promptFilterList = Json.createArrayBuilder();
	
	private static HashMap<String, String> flatKeysToIdx = new HashMap<String, String>();
	static{
		flatKeysToIdx.put("Title", "title");
		flatKeysToIdx.put("Path", IDX_FIELD_DIR);
		flatKeysToIdx.put("Name", "name");
	}
	
	public Adhoc(File inputFile) throws FileNotFoundException, XMLStreamException, UnsupportedEncodingException{
		super(inputFile);
		
		columnTypeLists.put("Dimension", dimensionList);
		columnTypeLists.put("Measure", measureList);
		columnTypeLists.put("Expression", expressionList);
		columnTypeLists.put("Ratio", ratioList);
		
		this.name = inputFile.getName().substring(0, inputFile.getName().lastIndexOf(FILE_EXT));
		
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		jsonBuilder.add(IDX_FIELD_FILENAME, this.name);
		
		FileInputStream fio = new FileInputStream(inputFile);
		InputStreamReader is = new InputStreamReader(fio, "UTF-8");
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = inputFactory.createXMLStreamReader(is);	
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(tag.equals(ADHOC_REPORT_CLZ)){
					String createdBy = reader.getAttributeValue("", "createdBy");
					if(createdBy != null){
						jsonBuilder.add(IDX_FIELD_CREATOR, createdBy);
					}
				}else if(tag.equals(ENTITIES)){
					parseEntity(reader, jsonBuilder);
				}else if (tag.equals(FILTERS)){
					parseFilters(reader);
				}else{
					String keyIdx = flatKeysToIdx.get(tag);
					if(keyIdx != null){
						String value = reader.getElementText();
						if (tag == "Path"){
							this.path = value;
						}
						jsonBuilder.add(keyIdx, value);
					}
				}
			}
		}
		reader.close();
		
		jsonBuilder.add(IDX_FIELD_LAST_MOD, inputFile.lastModified());
		jsonBuilder.add(IDX_FIELD_PROMPT_FILTERS, promptFilterList);
		
		addEntitesToJson(jsonBuilder);
		
		jsonObj = jsonBuilder.build();
	}

	private void parseEntity(XMLStreamReader reader, JsonObjectBuilder builder) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(tag.equals(ENTITY_COLUMN)){
					parseColumn(reader);	
				}else if (tag.equals(ENTITY_CHART)){
					parseChart(reader);
				}else if (tag.equals(ENTITY_LABEL)){
					parseLabel(reader);
				}else if (tag.equals(ENTITY_SUBREPORT)){
					parseSubreport(reader);
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				 String tag = reader.getLocalName();
				 if(ENTITIES.equals(tag)){
					 break;
				 }
			}
		}
	}
	
	private void parseColumn(XMLStreamReader reader) throws XMLStreamException{
		JsonArrayBuilder columnList = null;
		String dimension = null, column = null;
		String columnType = null;
		String displayExpr = null;
		String columnName = null;
		
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(tag.equals(COLUMN_TYPE)){
					columnType = reader.getElementText();
					columnList = columnTypeLists.get(columnType);
				}else if (tag.equals(DIMENSION)){
					dimension = reader.getElementText();
				}else if(tag.equals(COLUMN)){
					column = reader.getElementText();
				}else if (DISPLAY_EXPR.equals(tag)){
					displayExpr = reader.getElementText();
				}else if (NAME.equals(tag)){
					columnName = reader.getElementText();
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				String tag = reader.getLocalName();
				if(tag.equals(ENTITY_COLUMN)){
					break;
				}
			}
		}
		
		if(columnList != null){
			if(dimension != null && column != null){
				String dim = dimension + "." + column;
				if(columnType.equals("Measure")){
					dim = column;
				}else if (columnType.equals("Expression")){
					dim = displayExpr;
				}
				//birstLogger.debug("adding " + dim + " columnType: " + columnType);
				columnList.add(dim);
				if(columnName != null){
					attributeNameList.add(columnName);
				}
			}
		}
	}
	
	private void parseChart(XMLStreamReader reader) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(CHARTS.equals(tag)){
					parseChartElements(reader);
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				if(reader.getLocalName().equals(ENTITY_CHART)){
					break;
				}
			}
		}
	}
	
	private void parseChartElements(XMLStreamReader reader) throws XMLStreamException{
		String chartType = null;
		boolean isParsingChart = false;
		JsonArrayBuilder cB = Json.createArrayBuilder();
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(CHART.equals(tag)){
					isParsingChart = true;
				}else if(CHART_TYPE.equals(tag) && isParsingChart){
					chartType = reader.getElementText();
					if(chartType.equals("UMap")){
						chartType = "BingMap";
					}
					cB.add(chartType);
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				String tag = reader.getLocalName();
				if(CHARTS.equals(tag)){
					chartList.add(cB);
					break;
				}else if (CHART.equals(tag)){
					chartType = null;
					isParsingChart = false;
				}
			}
		}
	}
	
	private void parseFilters(XMLStreamReader reader) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(FILTER.equals(tag)){
					parseFilterElements(reader);
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				if(reader.getLocalName().equals(FILTERS)){
					break;
				}
			}
		}
	}
	
	private void parseFilterElements(XMLStreamReader reader) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(PROMPT_NAME.equals(tag)){
					String promptName = reader.getElementText();
					if(promptName != null && promptName.length() > 0){
						promptFilterList.add(promptName);
					}
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				String tag = reader.getLocalName();
				if(FILTERS.equals(tag)){
					break;
				}
			}
		}
	}
	
	private void parseLabel(XMLStreamReader reader) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(LABEL.equals(tag)){
					String label = reader.getElementText();
					if(label != null && label.length() > 0){
						labelList.add(label);
						break;
					}
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				if(reader.getLocalName().equals(ENTITY_LABEL)){
					break;
				}
			}
		}
	}
	
	private void parseSubreport(XMLStreamReader reader) throws XMLStreamException{
		while(reader.hasNext()){
			int eventType = reader.next();
			if(eventType == XMLStreamConstants.START_ELEMENT){
				String tag = reader.getLocalName();
				if(SUBREPORT.equals(tag)){
					String label = reader.getElementText();
					if(label != null && label.length() > 0){
						subreportList.add(label);
						break;
					}
				}
			}else if (eventType == XMLStreamConstants.END_ELEMENT){
				String tag = reader.getLocalName();
				if(tag.equals(ENTITY_SUBREPORT)){
					break;
				}
			}
		}
	}
	
	private void addEntitesToJson(JsonObjectBuilder builder){
		builder.add(IDX_FIELD_DIMENSIONS, dimensionList);
		builder.add(IDX_FIELD_MEASURES, measureList);
		builder.add(IDX_FIELD_EXPRESSIONS, expressionList);
		builder.add(IDX_FIELD_CHARTS, chartList);
		builder.add(IDX_FIELD_LABELS, labelList);
		builder.add(IDX_FIELD_ATTR_NAME, attributeNameList);
		builder.add(IDX_FIELD_SUBREPORTS, subreportList);
	}
	
	public String toJson(){
		if(jsonObj != null){
			return jsonObj.toString();
		}
		return null;
	}
	
	public String getPath(){
		return this.path + '/' + this.name;
	}
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, XMLStreamException{
		Adhoc x = new Adhoc(new File(args[0]));
		System.out.println("json - \n" + x.toJson());
	}

	@Override
	public String indexType() {
		return BaseReport.TYPE_ADHOC;
	}

}
