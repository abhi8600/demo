package com.birst.connectors.salesforce;

import java.util.ArrayList;
import java.util.List;

import com.birst.connectors.BaseConnector;
import com.birst.connectors.ConnectionProperty;
import com.birst.connectors.ConnectorMetaData;
import com.birst.connectors.ExtractionObject;

/**
 * 
 * @author mpandit
 *
 */
public class SalesforceConnectorMetaData extends ConnectorMetaData {
	private static final String name = "SFDC";
	private static final String interfaceVersion = "1.0";
	private static final String connectorVersion = "1.0";
	private static final String connectorCloudVersion = "26.0";
	private static final List<ConnectionProperty> connectionProperties = fillConnectionProperties();
	private static final List<ExtractionObject.Types> supportedTypes = fillSupportedTypes();
	
	private static List<ConnectionProperty> fillConnectionProperties() {
		List<ConnectionProperty> connectionProperties = getBaseConnectionProperties();
		List<ConnectionProperty> toRemoveList = new ArrayList<ConnectionProperty>();
		for (ConnectionProperty cp : connectionProperties)
		{
			if (cp.name.equalsIgnoreCase(BaseConnector.SOURCEFILE_NAME_PREFIX))
			{
				toRemoveList.add(cp);//No prefix to be used for SFDC
				break;
			}
		}
		connectionProperties.removeAll(toRemoveList);
		ConnectionProperty username = new ConnectionProperty();
		username.name = SalesforceConnector.USERNAME;
		username.isRequired = true;
		username.displayIndex = 1;
		username.displayLabel = "Username";
		connectionProperties.add(username);
		ConnectionProperty password = new ConnectionProperty();
		password.name = SalesforceConnector.PASSWORD;
		password.isRequired = true;
		password.isSecret = true;
		password.isEncrypted = true;
		password.displayIndex = 2;
		password.displayLabel = SalesforceConnector.PASSWORD;
		connectionProperties.add(password);
		ConnectionProperty sfdcClientID = new ConnectionProperty();
		sfdcClientID.name = SalesforceConnector.SFDC_CLIENT_ID;
		sfdcClientID.isRequired = false;
		sfdcClientID.saveToConfig = false;
		connectionProperties.add(sfdcClientID);
		ConnectionProperty sandBoxURL = new ConnectionProperty();
		sandBoxURL.name = BaseConnector.SANDBOX_URL;
		sandBoxURL.isRequired = false;
		sandBoxURL.saveToConfig = false;
		connectionProperties.add(sandBoxURL);
		return connectionProperties;
	}
	
	private static List<ExtractionObject.Types> fillSupportedTypes() {
		List<ExtractionObject.Types> types = new ArrayList<ExtractionObject.Types>();
		types.add(ExtractionObject.Types.OBJECT);
		types.add(ExtractionObject.Types.QUERY);
		return types;
	}
	
	
	@Override
	public String getConnectorName() {
		return name;
	}

	@Override
	public String getInterfaceVersion() {
		return interfaceVersion;
	}

	@Override
	public String getConnectorVersion() {
		return connectorVersion;
	}

	@Override
	public String getConnectorCloudVersion() {
		return connectorCloudVersion;
	}

	@Override
	public List<ConnectionProperty> getConnectionProperties() {
		
		return connectionProperties;
	}
	
	@Override
	public List<ExtractionObject.Types> getSupportedTypes() {
		return supportedTypes;
	}

	@Override
	public boolean supportsParallelExtraction(List<ExtractionObject> extractGroupObjectsList) {
		//salesforce parallel extraction does not depend on object type. 
		return true;
	}

	@Override
	public boolean allowsSourceFileNamePrefix() {
		return false;
	}
	
	@Override
	public boolean requiresDynamicParamForObjects()
	{
		return false;
	}
}
