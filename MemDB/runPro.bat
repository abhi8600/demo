rem echo off
set JAVA_HOME=C:\Program Files\Java\jdk1.7.0_01
set MEMDB_HOME=%CD%
rmdir /S/Q bin
mkdir bin
mkdir data
pushd "%MEMDB_HOME%\bin"
copy ..\dist\*.* . >log.txt
copy ..\log4j.properties . >>log.txt

set CLASSPATH=.\memdb.jar;.\memdbpro.jar;.\log4j-1.2.15.jar;.\antlrworks-1.4.2.jar;.\trove-3.0.0rc2.jar;.\commons-logging-1.1.1.jar;.\mysql-connector-java-5.1.7-bin.jar;.\snappy-java-1.0.3.1.jar;.\ojdbc6.jar;.\

"%JAVA_HOME%\jre\bin\java" -Djava.security.policy=="%MEMDB_HOME%\java.policy" -Dmemdb.home=%MEMDB_HOME% -Djava.rmi.server.codebase=file:/%MEMDB_HOME%/bin/memdb.jar -Xmx1000M -XX:-UseSplitVerifier memdb.Main "database=%MEMDB_HOME%\data"
popd
