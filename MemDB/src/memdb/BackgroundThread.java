/**
 * 
 */
package memdb;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bpeters
 * 
 */
public class BackgroundThread extends Thread
{
	private boolean running = true;
	private List<Runnable> runList = new ArrayList<Runnable>();
	private List<Long> delayList = new ArrayList<Long>();
	private List<Long> delayValues = new ArrayList<Long>();
	private static BackgroundThread runningThread = null;

	public void run()
	{
		while (running)
		{
			try
			{
				synchronized (runList)
				{
					for (int i = 0; i < runList.size(); i++)
					{
						Runnable r = runList.get(i);
						long delayVal = delayValues.get(i);
						long delay = delayList.get(i);
						if (delay < delayVal)
						{
							r.run();
							delayValues.set(i, 0L);
						} else
						{
							delayValues.set(i, delayVal + 1000L);
						}
					}
				}
				Thread.sleep(1000);
			} catch (InterruptedException e)
			{
				break;
			}
		}
	}

	public boolean isRunning()
	{
		return running;
	}

	public void setRunning(boolean running)
	{
		this.running = running;
	}

	public void addRunnable(Runnable r)
	{
		addRunnable(r, 0);
	}

	/**
	 * Add a runnable process that executes at a periodic interval (delay)
	 * 
	 * @param r
	 * @param delay
	 */
	public void addRunnable(Runnable r, long delay)
	{
		synchronized (runList)
		{
			runList.add(r);
			delayList.add(delay);
			delayValues.add(0L);
		}
	}

	public void removeRunnable(Runnable r)
	{
		synchronized (runList)
		{
			int i = runList.indexOf(r);
			if (i >= 0)
			{
				runList.remove(i);
				delayList.remove(i);
				delayValues.remove(i);
			}
		}
	}

	public static BackgroundThread getRunningThread()
	{
		return runningThread;
	}

	public static void setRunningThread(BackgroundThread runningThread)
	{
		BackgroundThread.runningThread = runningThread;
	}
}
