package memdb;

import memdb.storage.Table;

import org.apache.log4j.Logger;

public class TableLatch
{
	private static Logger logger = Logger.getLogger(TableLatch.class);
	private final Object synchObj = new Object();
	private int readCount = 0;
	private int writeCount = 0;
	private Table t;

	public TableLatch(Table t)
	{
		this.t = t;
	}

	public void awaitRead() throws InterruptedException
	{
		synchronized (synchObj)
		{
			while (writeCount > 0)
			{
				synchObj.wait();
			}
			readCount++;
			logger.trace("Await read returned [" + t.schema + "." + t.name + "] " + readCount);
		}
	}

	public void awaitWrite() throws InterruptedException
	{
		synchronized (synchObj)
		{
			while (readCount > 1 || writeCount > 0)
			{
				synchObj.wait();
			}
			writeCount++;
			logger.trace("Await write returned [" + t.schema + "." + t.name + "] " + writeCount);
		}
	}

	public void releaseRead()
	{
		synchronized (synchObj)
		{
			if (--readCount <= 1)
			{
				synchObj.notifyAll();
			}
			if (t.name.endsWith("DW_SF_CATEGORYNEW_DAY") && readCount == 1)
			{
				int a = 1;
			}
			logger.trace("Release read returned [" + t.schema + "." + t.name + "] " + readCount);
		}
	}

	public void releaseWrite()
	{
		synchronized (synchObj)
		{
			if (--writeCount <= 0)
			{
				synchObj.notifyAll();
			}
			logger.trace("Release write returned [" + t.schema + "." + t.name + "] " + writeCount);
		}
	}
}
