package memdb;

import java.io.PrintStream;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.sql.RowRange;
import memdb.storage.Table;

import org.apache.log4j.Logger;

public class ParallelAddRow
{
	private static Logger logger = Logger.getLogger(ParallelAddRow.class);
	private LinkedBlockingQueue<Object[]> rowQueue = new LinkedBlockingQueue<Object[]>(100);
	private Thread[] rowthreads;
	private AtomicBoolean addingRows = new AtomicBoolean(true);
	private TimeZone tz;

	public ParallelAddRow(Table table, PrintStream out, AtomicInteger addCount, TimeZone tz, RowRange rowRange)
	{
		this.tz = tz;
		rowthreads = new Thread[Runtime.getRuntime().availableProcessors()];
		for (int i = 0; i < rowthreads.length; i++)
		{
			rowthreads[i] = new AddRow(table, rowQueue, out, addCount, addingRows, rowRange);
			rowthreads[i].start();
		}
	}

	public void join()
	{
		addingRows.set(false);
		for (int i = 0; i < rowthreads.length; i++)
		{
			rowQueue.offer(new Object[0]);
		}
		for (int i = 0; i < rowthreads.length; i++)
		{
			try
			{
				rowthreads[i].join();
			} catch (InterruptedException e)
			{
				continue;
			}
		}
	}

	public LinkedBlockingQueue<Object[]> getRowQueue()
	{
		return rowQueue;
	}

	private class AddRow extends Thread
	{
		Table foundt;
		LinkedBlockingQueue<Object[]> rowQueue;
		PrintStream out;
		AtomicInteger addCount;
		AtomicBoolean addingRows;
		RowRange rowRange;

		public AddRow(Table foundt, LinkedBlockingQueue<Object[]> rowQueue, PrintStream out, AtomicInteger addCount, AtomicBoolean addingRows, RowRange rowRange)
		{
			this.foundt = foundt;
			this.rowQueue = rowQueue;
			this.out = out;
			this.addCount = addCount;
			this.addingRows = addingRows;
			this.rowRange = rowRange;
		}

		public void run()
		{
			while (addingRows.get() || rowQueue.size() > 0)
			{
				Object[] row = null;
				try
				{
					row = rowQueue.poll(2, TimeUnit.SECONDS);
				} catch (InterruptedException e)
				{
					logger.error(e);
					return;
				}
				if (row == null)
					continue;
				if (row.length == 0)
					return;
				foundt.addRow(row, tz, rowRange);
				int count = addCount.incrementAndGet();
				if (count % 100000 == 0)
				{
					if (out != null)
						out.println(count + " rows loaded");
					logger.trace(count + " rows loaded");
				}
			}
		}
	}
}
