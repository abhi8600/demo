/**
 * 
 */
package memdb.tcpserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.sql.Types;
import java.util.TimeZone;

import memdb.Database;
import memdb.TaskThread;
import memdb.jdbcserver.RemoteResultSetImpl;
import memdb.sql.QueryResultRow;

import org.apache.log4j.Logger;

/**
 * @author bpeters
 * 
 */
public class TcpServer
{
	private static Logger logger = Logger.getLogger(TcpServer.class);
	private int port;
	private Database db;
	private ServerSocket listener = null;
	private boolean running = false;

	public TcpServer(Database db, int port)
	{
		this.db = db;
		this.port = port;
	}

	public void start() throws IOException
	{
		listener = new ServerSocket(port);
		running = true;
		Thread t = new ListenerThread();
		t.start();
		logger.info("TCP listener started on port: " + port);
	}

	private class ListenerThread extends Thread
	{
		public void run()
		{
			while (running)
			{
				try
				{
					Socket s = listener.accept();
					Thread t = new ExecuteThread(s);
					t.start();
				} catch (IOException e)
				{
					logger.error(e);
				}
			}
		}
	}

	private class ExecuteThread extends Thread
	{
		private Socket s;

		public ExecuteThread(Socket s)
		{
			this.s = s;
		}

		public void run()
		{
			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
				String command = reader.readLine();
				TaskThread tt = TaskThread.launchNewCommand(command, db, TimeZone.getDefault(), null);
				RemoteResultSetImpl rs = null;
				try
				{
					tt.join();
					rs = tt.getResults();
				} catch (InterruptedException e)
				{
					return;
				}
				PrintStream outstream = new PrintStream(s.getOutputStream());
				if (rs != null)
				{
					outstream.print("Results:\r\n");
					int length = rs.getColumnNames().length;
					for (int i = 0; i < length; i++)
					{
						Object o = rs.getColumnNames()[i];
						outstream.print((i > 0 ? '|' : "") + (o == null ? "" : o.toString()));
					}
					outstream.println();
					for (int i = 0; i < length; i++)
					{
						int type = rs.getColumnTypes()[i];
						outstream.print((i > 0 ? '|' : "") + Integer.toString(type));
					}
					outstream.println();
					QueryResultRow[] results = rs.getResults(true);
					if (results == null)
						outstream.println(0);
					else
					{
						outstream.println(results.length);
						logger.info(command);
						for (QueryResultRow row : results)
						{
							length = row.dataRow.length;
							for (int i = 0; i < length; i++)
							{
								Object o = row.dataRow[i];
								String s = null;
								if (o != null)
								{
									if (o instanceof Integer && rs.getColumnTypes()[i] == Types.INTEGER && ((Integer) o) == Integer.MIN_VALUE)
										s = "";
									else if (o instanceof Long && rs.getColumnTypes()[i] == Types.BIGINT && ((Long) o) == Long.MAX_VALUE)
										s = "";
									else
										s = o.toString();
								}
								outstream.print((i > 0 ? "|0" : "") + (o == null ? "" : s));
							}
							outstream.print("|1");
						}
					}
				} else
				{
					outstream.println("OK");
				}
				outstream.flush();
			} catch (IOException e)
			{
				logger.error(e);
			} catch (SQLException e)
			{
				try
				{
					PrintStream outstream = new PrintStream(s.getOutputStream());
					outstream.println("SQLException: " + e.getMessage());
					outstream.flush();
				} catch (IOException e1)
				{
					logger.error(e1);
				}
			}
		}
	}
}
