package memdb;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class ThreadPool
{
	private static Logger logger = Logger.getLogger(ThreadPool.class);
	/*
	 * Global thread pool
	 */
	private static ExecutorService gtp = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 3);

	public static void execute(Runnable command)
	{
		gtp.execute(command);
	}
	/*
	 * Local thread pool
	 */
	private ExecutorService ltp = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 3);

	public void executeLocal(Runnable command)
	{
		ltp.execute(command);
	}

	public void delete()
	{
		ltp.shutdownNow();
	}

	public void terminateAndAwait()
	{
		ltp.shutdown();
		try
		{
			ltp.awaitTermination(12, TimeUnit.HOURS);
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
	}
}
