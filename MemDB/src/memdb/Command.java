package memdb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.jdbcserver.RemoteResultSetImpl;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;
import memdb.sql.Delete;
import memdb.sql.MemgrammarLexer;
import memdb.sql.MemgrammarParser;
import memdb.sql.RowRange;
import memdb.sql.Select;
import memdb.sql.Update;
import memdb.storage.Table;
import memdb.storage.TableMetadata;
import memdb.util.Util;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.sun.management.OperatingSystemMXBean;

public class Command
{
	private static Logger logger = Logger.getLogger(Command.class);
	private String outFileName = null;
	private int rowsProcessed = 0;
	private Tree t;
	private Object[] parameters;
	private int parameterIndex;
	private boolean killed = false;
	private String commandString;
	private int threadNum;
	private TimeZone clientTimeZone;

	public Command(int threadNum, TimeZone clientTimeZone)
	{
		this.threadNum = threadNum;
		this.clientTimeZone = clientTimeZone;
	}

	public void parseCommand(String command) throws SQLException
	{
		this.commandString = command;
		parameters = new Object[0];
		ANTLRStringStream stream = new ANTLRStringStream(command);
		MemgrammarLexer lex = new MemgrammarLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		MemgrammarParser parser = new MemgrammarParser(cts);
		MemgrammarParser.statement_return result = null;
		try
		{
			result = parser.statement();
		} catch (RecognitionException e)
		{
			throw new SQLException("Syntax error: Line " + e.line + ", Position " + e.charPositionInLine);
		}
		if (lex.failed() || lex.hasError())
		{
			throw new SQLException("Syntax error");
		}
		t = (Tree) result.getTree();
	}

	public boolean validPrepared()
	{
		return t != null;
	}

	public void setParameters(Object[] parameters)
	{
		this.parameters = new Object[parameters.length];
		for (int i = 0; i < parameters.length; i++)
			this.parameters[i] = parameters[i];
		parameterIndex = 0;
	}

	public void clear()
	{
		t = null;
	}

	public RemoteResultSetImpl process(Database db, String command, PrintStream out) throws SQLException
	{
		long beginTime = System.currentTimeMillis();
		boolean unExecuted = false;
		try
		{
			if (t == null)
			{
				if (command.toUpperCase().equals("SELECT 1"))
					try
					{
						unExecuted = true;
						return new RemoteResultSetImpl();
					} catch (RemoteException e2)
					{
						return null;
					}
				parseCommand(command);
				if (logger.isTraceEnabled())
				{
					logger.trace("CMD: " + command);
				}
			} else
			{
				if (logger.isTraceEnabled())
				{
					StringBuilder commandStr = new StringBuilder(command == null ? commandString : command);
					if (parameters != null)
					{
						for (int i = 0; i < parameters.length; i++)
						{
							int index = commandStr.indexOf("?");
							if (index >= 0)
							{
								commandStr = commandStr.replace(index, index + 1, parameters[i] == null ? "null" : "'" + parameters[i].toString() + "'");
							}
						}
					}
					logger.trace("CMD: " + commandStr);
				}
			}
			if (t.isNil())
				t = t.getChild(0);
			RemoteResultSetImpl results = null;
			switch (t.getType())
			{
			case MemgrammarParser.SELECT:
				try
				{
					Select s = new Select(this, db, t, null, parameters);
					results = s.process();
					rowsProcessed = s.rowsProcessed();
					outFileName = s.getOutfilename();
				} catch (InterruptedException e)
				{
					logger.error(e);
					throw new SQLException("Unable to select from table");
				}
				break;
			case MemgrammarParser.SHOW:
				if (t.getChild(0).getType() == MemgrammarParser.TABLES)
				{
					List<Object[]> rows = new ArrayList<Object[]>();
					TableMetadata tmd = new TableMetadata(4);
					tmd.columnNames = new String[]
					{ "Database", "Schema", "Table Name", "Key Store Name", "Transient", "Disk Based", "Compressed" };
					tmd.dataTypes = new int[]
					{ Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN };
					tmd.widths = new int[]
					{ 255, 255, 255, 255, 0, 0, 0 };
					tmd.tableName = "TABLES";
					for (Table tb : db.tables)
					{
						File f = new File(tb.database.path);
						rows.add(new String[]
						{ f.getName(), tb.schema, tb.name, tb.tmd.surrogateKeyStoreName, tb.tmd.transientTable ? "true" : "false",
								tb.tmd.defaultDisk ? "true" : "false", tb.tmd.defaultCompressed ? "true" : "false" });
					}
					try
					{
						results = new RemoteResultSetImpl(rows, tmd);
					} catch (RemoteException e)
					{
						logger.error(e);
					}
				} else if (t.getChild(0).getType() == MemgrammarParser.PROCESSLIST)
				{
					List<Object[]> rows = new ArrayList<Object[]>();
					TableMetadata tmd = new TableMetadata(3);
					tmd.columnNames = new String[]
					{ "Thread Number", "Age", "Command" };
					tmd.dataTypes = new int[]
					{ Types.INTEGER, Types.INTEGER, Types.VARCHAR };
					tmd.widths = new int[]
					{ 255, 255, 255 };
					tmd.tableName = "THREADS";
					for (Entry<Integer, TaskThread> en : TaskThread.getThreads())
					{
						rows.add(new Object[]
						{ en.getValue().getThreadNum(), System.currentTimeMillis() - en.getValue().getStarttime(), en.getValue().getCommandString() });
					}
					try
					{
						results = new RemoteResultSetImpl(rows, tmd);
					} catch (RemoteException e)
					{
						logger.error(e);
					}
				} else if (t.getChild(0).getType() == MemgrammarParser.SYSTEMSTATUS)
				{
					List<Object[]> rows = new ArrayList<Object[]>();
					TableMetadata tmd = new TableMetadata(3);
					tmd.columnNames = new String[]
					{ "Name", "Value" };
					tmd.dataTypes = new int[]
					{ Types.VARCHAR, Types.VARCHAR };
					tmd.widths = new int[]
					{ 255, 255 };
					tmd.tableName = "STATUS";
					rows.add(new Object[]
					{ "Total Memory", String.valueOf(Runtime.getRuntime().totalMemory()) });
					rows.add(new Object[]
					{ "Free Memory", String.valueOf(Runtime.getRuntime().freeMemory()) });
					rows.add(new Object[]
					{ "Max Memory", String.valueOf(Runtime.getRuntime().maxMemory()) });
					rows.add(new Object[]
					{ "Data Memory", String.valueOf(db.memUsed()) });
					rows.add(new Object[]
					{ "Num Processors", String.valueOf(Runtime.getRuntime().availableProcessors()) });
					rows.add(new Object[]
					{ "OS Architecture", String.valueOf(System.getProperty("os.arch")) });
					rows.add(new Object[]
					{ "OS Name", String.valueOf(System.getProperty("os.name")) });
					rows.add(new Object[]
					{ "OS Version", String.valueOf(System.getProperty("os.version")) });
					OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
					rows.add(new Object[]
					{ "Process CPU Load", String.valueOf(osBean.getProcessCpuLoad()) });
					rows.add(new Object[]
					{ "System CPU Load", String.valueOf(osBean.getSystemCpuLoad()) });
					try
					{
						results = new RemoteResultSetImpl(rows, tmd);
					} catch (RemoteException e)
					{
						logger.error(e);
					}
				} else if (t.getChild(0).getType() == MemgrammarParser.SURROGATEKEYS)
				{
					// int maxKey = db.common.getMaxSurrogateKey();
					// for (int i = 0; i < maxKey; i++)
					// {
					// System.out.println(i + "\t" + db.common.getSurrogateKeyValue(i, null, 0));
					// }
				} else
				{
					// Show table - must be last else
					Tree fc = t.getChild(0);
					boolean debug = false;
					if (t.getChildCount() > 1 && t.getChild(1).getType() == MemgrammarParser.DEBUG)
						debug = true;
					String tableName = fc.getText();
					String tableSchema = Select.getTableSchema(fc);
					Table foundt = db.findTable(tableSchema, tableName);
					if (foundt != null)
					{
						List<Object[]> rows = new ArrayList<Object[]>();
						TableMetadata tmd = new TableMetadata(7);
						tmd.columnNames = new String[]
						{ "Column", "DataType", "Width", "PrimaryKey", "ForeignKey", "NoIndex", "Transient", "Disk", "Compressed", "Size" };
						tmd.dataTypes = new int[]
						{ Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN, Types.BOOLEAN,
								Types.BOOLEAN, Types.BIGINT };
						tmd.widths = new int[]
						{ 255, 255, 0, 0, 0, 0, 0, 0, 0, 0 };
						tmd.tableName = "COLUMNS";
						for (int i = 0; i < foundt.tmd.columnNames.length; i++)
						{
							String typeName = "Unknown";
							switch (foundt.tmd.dataTypes[i])
							{
							case Types.VARCHAR:
								typeName = "VARCHAR";
								break;
							case Types.BIGINT:
								typeName = "BIGINT";
								break;
							case Types.INTEGER:
								typeName = "INTEGER";
								break;
							case Types.FLOAT:
								typeName = "FLOAT";
								break;
							case Types.DOUBLE:
								typeName = "DOUBLE";
								break;
							case Types.DATE:
								typeName = "DATE";
								break;
							case Types.TIMESTAMP:
								typeName = "TIMESTAMP";
								break;
							case Types.BOOLEAN:
								typeName = "BOOLEAN";
								break;
							case Types.DECIMAL:
								typeName = "Decimal";
								break;
							}
							rows.add(new Object[]
							{ foundt.tmd.columnNames[i], typeName, foundt.tmd.widths[i], foundt.tmd.primarykey[i], foundt.tmd.foreignkey[i],
									foundt.tmd.noindex[i], foundt.tmd.transientcolumns[i], foundt.tmd.diskBased[i], foundt.tmd.compressed[i],
									foundt.getColumnSize(i) });
						}
						try
						{
							results = new RemoteResultSetImpl(rows, tmd);
						} catch (RemoteException e)
						{
							logger.error(e);
						}
						if (debug)
						{
							try
							{
								foundt.reserve();
							} catch (Exception e)
							{
								logger.error(e);
							}
							System.out.println("Raw data:");
							for (int col = 0; col < foundt.getNumColumns(); col++)
							{
								System.out.print("\t");
								System.out.print(foundt.tmd.columnNames[col]);
							}
							System.out.println();
							for (int row = 0; row < foundt.getNumRows(); row++)
							{
								System.out.print(row);
								for (int col = 0; col < foundt.getNumColumns(); col++)
								{
									System.out.print("\t" + foundt.getTableValue(row, col));
								}
								System.out.println();
							}
							System.out.println("Column Indices:");
							for (int col = 0; col < foundt.getNumColumns(); col++)
							{
								System.out.println(foundt.tmd.columnNames[col] + ":");
								HashSet<Object> valSet = new HashSet<Object>();
								for (int row = 0; row < foundt.getNumRows(); row++)
								{
									Object val = foundt.getTableValue(row, col);
									if (valSet.contains(val))
										continue;
									valSet.add(val);
									RowSet rset = foundt.getRowList(col, val);
									System.out.print("[" + val + "]");
									if (rset == null)
									{
										System.out.print("\tNo index found");
									} else
									{
										RowSetIterator rsit = rset.getIterator();
										int rnum = 0;
										while ((rnum = rsit.getNextRow()) >= 0)
										{
											System.out.print("\t" + rnum);
										}
									}
									System.out.println();
								}
							}
							try
							{
								foundt.release();
							} catch (IOException e)
							{
								logger.error(e);
							}
						}
					} else
					{
						throw new SQLException("Table not found: " + tableName);
					}
				}
				break;
			case MemgrammarParser.KILL:
				int tnum = Integer.valueOf(t.getChild(0).getText());
				TaskThread.killTaskThread(tnum);
				break;
			case MemgrammarParser.DROP:
			case MemgrammarParser.TRUNCATE:
				Tree fc = t.getChild(0);
				String tableName = fc.getText();
				String tableSchema = Select.getTableSchema(fc);
				boolean ifexists = false;
				for (int i = 0; i < t.getChildCount(); i++)
				{
					Tree sfc = t.getChild(i);
					if (sfc.getType() == MemgrammarParser.IFEXISTS)
					{
						ifexists = true;
						break;
					}
				}
				Table foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
				{
					boolean succeeded = false;
					try
					{
						foundt.reserve();
						succeeded = true;
					} catch (ClassNotFoundException e)
					{
						logger.error(e);
					} catch (IOException e)
					{
						logger.error(e);
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
					boolean dropped = false;
					if (succeeded)
						try
						{
							rowsProcessed = foundt.getNumRows();
							if (t.getType() == MemgrammarParser.DROP)
							{
								db.deleteTable(foundt);
								dropped = true;
								if (out != null)
									out.println("Table [" + tableName + "] dropped");
							} else
							{
								foundt.initializeNew(db, foundt.tmd.surrogateKeyStoreName, foundt.tmd.diskBasedKeyStore, foundt.tmd.compressedKeyStore);
								if (out != null)
									out.println("Table [" + tableName + "] truncated");
							}
						} finally
						{
							try
							{
								if (!dropped)
									foundt.release();
							} catch (IOException e)
							{
								logger.error(e);
							}
						}
				} else if (!ifexists)
				{
					throw new SQLException("Table not found: " + tableName);
				} else if (ifexists)
				{
					if (out != null)
						out.println("Table [" + tableName + "] not dropped, does not exist");
				}
				break;
			case MemgrammarParser.QUIT:
			case MemgrammarParser.EXIT:
				throw new SQLException("exit");
			case MemgrammarParser.FLUSH:
				try
				{
					db.flush();
				} catch (IOException e1)
				{
					throw new SQLException(e1.getMessage());
				}
				break;
			case MemgrammarParser.ALTER:
				fc = t.getChild(0);
				tableName = fc.getText();
				tableSchema = Select.getTableSchema(fc);
				if (tableSchema == null)
				{
					throw new SQLException("Schema not specified: " + command);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt == null)
					throw new SQLException("Table " + tableName + " does not exist");
				try
				{
					foundt.latch.awaitWrite();
					for (int i = 1; i < t.getChildCount(); i++)
					{
						Tree alterdef = t.getChild(i);
						if (alterdef.getType() == MemgrammarParser.ADD)
						{
							TableMetadata tmd = foundt.tmd;
							int position = tmd.getColumnIndex(alterdef.getChild(0).getText());
							if (position >= 0)
								throw new SQLException("Unable to add, column exists: " + alterdef.getChild(0).getText());
							position = tmd.addColumn() - 1;
							setColumnMetadata(tmd, alterdef.getChild(0), position, tmd.defaultDisk, tmd.defaultCompressed, false, null);
							foundt.addColumn();
						} else if (alterdef.getType() == MemgrammarParser.DROP)
						{
							TableMetadata tmd = foundt.tmd;
							int position = tmd.getColumnIndex(alterdef.getChild(0).getText());
							if (position < 0)
								throw new SQLException("Unable to drop, column not found: " + alterdef.getChild(0).getText());
							foundt.dropColumn(position);
							tmd.dropColumn(position);
						} else if (alterdef.getType() == MemgrammarParser.ALTER)
						{
							TableMetadata tmd = foundt.tmd;
							int position = tmd.getColumnIndex(alterdef.getChild(0).getText());
							if (position < 0)
								throw new SQLException("Unable to alter, column not found: " + alterdef.getChild(0).getText());
							int fromType = tmd.dataTypes[position];
							boolean fromprimarykey = tmd.primarykey[position];
							boolean fromforeignkey = tmd.foreignkey[position];
							boolean fromnoindex = tmd.noindex[position];
							boolean fromdiskbased = tmd.diskBased[position];
							boolean fromcompression = tmd.compressed[position];
							setColumnMetadata(tmd, alterdef.getChild(0), position, tmd.diskBased[position], tmd.compressed[position], true, foundt);
							if (tmd.dataTypes[position] != fromType || tmd.primarykey[position] != fromprimarykey || tmd.foreignkey[position] != fromforeignkey
									|| tmd.noindex[position] != fromnoindex || tmd.diskBased[position] != fromdiskbased
									|| tmd.compressed[position] != fromcompression)
							{
								// Do basic check for enough memory
								if (tmd.diskBased[position] != fromdiskbased && fromdiskbased)
								{
									long size = foundt.getColumnSize(position);
									if ((size + db.memUsed() > (0.7 * Runtime.getRuntime().maxMemory())))
										throw new SQLException("Insufficient memory to convert disk based column to memory");
								}
								/*
								 * When changing column metadata, must flush the table when done
								 */
								foundt.updateColumnData(position, clientTimeZone);
								foundt.flush();
							}
						}
					}
				} catch (InterruptedException e)
				{
					logger.error(e.getMessage());
				} finally
				{
					foundt.latch.releaseWrite();
				}
				break;
			case MemgrammarParser.RENAME:
				Tree originalTable = t.getChild(0);
				Tree newTable = t.getChild(1);
				String originalTableSchema = Select.getTableSchema(originalTable);
				String newTableSchema = Select.getTableSchema(newTable);
				if (originalTableSchema == null || newTableSchema == null)
					throw new SQLException("Schema not specified: " + command);
				if (!originalTableSchema.equals(newTableSchema))
					throw new SQLException("Rename table must be within same schema: " + command);
				foundt = db.findTable(originalTableSchema, originalTable.getText());
				if (foundt == null)
					throw new SQLException("Table not found: " + command);
				foundt.latch.awaitWrite();
				foundt.rename(newTable.getText());
				foundt.latch.releaseWrite();
				break;
			case MemgrammarParser.CREATE:
				fc = t.getChild(0);
				tableName = fc.getText();
				tableSchema = Select.getTableSchema(fc);
				if (tableSchema == null)
				{
					throw new SQLException("Schema not specified: " + command);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
					throw new SQLException("Table " + tableName + " already exists");
				if (t.getChild(1).getType() == MemgrammarParser.REMOTEQUERY)
				{
					if (t.getChildCount() != 2)
						throw new SQLException("Syntax error: " + command);
					Tree rq = t.getChild(1);
					bulkLoad(db, Util.getString(rq.getChild(0).getText()), Util.getString(rq.getChild(1).getText()), Util.getString(rq.getChild(2).getText()),
							Util.getString(rq.getChild(3).getText()), tableSchema, tableName, Util.getString(rq.getChild(4).getText()), null, null, tableSchema);
				} else if (t.getChild(1).getType() == MemgrammarParser.SELECT)
				{
					if (t.getChildCount() != 2)
						throw new SQLException("Syntax error: " + command);
					try
					{
						Select s = new Select(this, db, t.getChild(1), tableSchema, parameters);
						s.setCreateTableAsName(tableName);
						s.process();
						rowsProcessed = s.rowsProcessed();
					} catch (InterruptedException e)
					{
						logger.error(e);
						throw new SQLException("Unable to select from table: " + tableName);
					}
				} else
				{
					int numColumns = 0;
					boolean transientTable = false;
					boolean diskkeystore = false;
					boolean compressedkeystore = false;
					boolean disk = false;
					boolean compressed = false;
					for (int i = 1; i < t.getChildCount(); i++)
					{
						if (t.getChild(i).getType() == MemgrammarParser.TRANSIENT)
						{
							transientTable = true;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.DISKKEYSTORE)
						{
							diskkeystore = true;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.COMPRESSEDKEYSTORE)
						{
							compressedkeystore = true;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.DISK)
						{
							disk = true;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.COMPRESSED)
						{
							compressed = true;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.MEMORY)
						{
							disk = false;
							continue;
						}
						if (t.getChild(i).getType() == MemgrammarParser.UNCOMPRESSED)
						{
							compressed = false;
							continue;
						}
						numColumns++;
					}
					TableMetadata newtmd = new TableMetadata(numColumns);
					newtmd.defaultDisk = disk;
					newtmd.defaultCompressed = compressed;
					for (int i = 0; i < numColumns; i++)
					{
						setColumnMetadata(newtmd, t.getChild(i + 1), i, newtmd.defaultDisk, newtmd.defaultCompressed, false, null);
					}
					Table createTable = null;
					try
					{
						// Intially create a key store for every schema (later, allow to specify in create statement)
						createTable = db.createTable(tableSchema, tableName, false, tableSchema, diskkeystore, compressedkeystore);
					} catch (ClassNotFoundException e)
					{
						logger.error(e);
						throw new SQLException(e.getMessage());
					} catch (InstantiationException e)
					{
						logger.error(e);
						throw new SQLException(e.getMessage());
					} catch (IllegalAccessException e)
					{
						logger.error(e);
						throw new SQLException(e.getMessage());
					}
					newtmd.transientTable = transientTable;
					createTable.tmd = newtmd;
				}
				break;
			case MemgrammarParser.INSERT:
				fc = t.getChild(0);
				tableName = fc.getText();
				tableSchema = Select.getTableSchema(fc);
				if (tableSchema == null)
				{
					throw new SQLException("Schema not specified: " + command);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt == null)
					throw new SQLException("Table " + tableName + " not found");
				boolean succeeded = false;
				try
				{
					foundt.reserve();
					succeeded = true;
				} catch (ClassNotFoundException e2)
				{
					logger.error(e2);
					throw new SQLException("Unable to reserve table for insert");
				} catch (IOException e2)
				{
					logger.error(e2);
					throw new SQLException("Unable to reserve table for insert: " + e2.getMessage());
				} catch (InterruptedException e2)
				{
					logger.error(e2);
					throw new SQLException("Unable to reserve table for insert: " + e2.getMessage());
				}
				Tree collist = null;
				Tree selectTree = null;
				Tree valuesTree = null;
				Tree remoteQuery = null;
				int[] colindices = null;
				if (!succeeded)
					break;
				try
				{
					for (int i = 0; i < t.getChildCount(); i++)
					{
						if (t.getChild(i).getType() == MemgrammarParser.COLLIST)
							collist = t.getChild(i);
						else if (t.getChild(i).getType() == MemgrammarParser.SELECT)
							selectTree = t.getChild(i);
						else if (t.getChild(i).getType() == MemgrammarParser.VALUES)
							valuesTree = t.getChild(i);
						else if (t.getChild(i).getType() == MemgrammarParser.REMOTEQUERY)
							remoteQuery = t.getChild(i);
					}
					colindices = getColumnList(collist, foundt);
					if (selectTree != null)
					{
						try
						{
							Select s = new Select(this, db, selectTree, tableSchema, parameters);
							s.setColindices(colindices);
							s.setInsertIntoTable(foundt);
							s.process();
							rowsProcessed = s.rowsProcessed();
						} catch (InterruptedException e)
						{
							logger.error(e);
							throw new SQLException("Unable to select from table: " + foundt.name);
						}
					} else if (valuesTree != null)
					{
						if (valuesTree.getChildCount() != colindices.length)
							throw new SQLException("Column count on insert does not match value count");
						Object[] row = new Object[foundt.getNumColumns()];
						for (int i = 0; i < colindices.length; i++)
						{
							if (colindices[i] < 0)
								continue;
							Tree child = valuesTree.getChild(i);
							Object val = null;
							if (child.getType() == MemgrammarParser.PARAMETER)
							{
								if (parameterIndex < parameters.length)
									val = parameters[parameterIndex++];
							} else
								val = child.getText();
							if (child.getType() == MemgrammarParser.STRING)
							{
								String s = val.toString();
								if (s.startsWith("'") && s.endsWith("'"))
								{
									val = s.substring(1, s.length() - 1);
								}
							} else if (child.getType() == MemgrammarParser.INTEGER)
							{
							}
							if (val != null && !(val instanceof String && ((String) val).toLowerCase().equals("null")))
							{
								try
								{
									if (foundt.tmd.dataTypes[colindices[i]] == Types.DOUBLE)
									{
										if (val instanceof String)
											row[colindices[i]] = Double.valueOf((String) val);
										else if (val instanceof Double)
											row[colindices[i]] = val;
										else if (val instanceof Float)
											row[colindices[i]] = ((Float) val).doubleValue();
										else if (val instanceof Integer)
											row[colindices[i]] = ((Integer) val).doubleValue();
										else if (val instanceof Long)
											row[colindices[i]] = ((Long) val).doubleValue();
										else if (val instanceof BigDecimal)
											row[colindices[i]] = ((BigDecimal) val).doubleValue();
										else
											row[colindices[i]] = Double.valueOf(val.toString());
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.FLOAT)
									{
										if (val instanceof String)
											row[colindices[i]] = Float.valueOf((String) val);
										else if (val instanceof Double)
											row[colindices[i]] = ((Double) val).floatValue();
										else if (val instanceof Float)
											row[colindices[i]] = val;
										else if (val instanceof Integer)
											row[colindices[i]] = ((Integer) val).floatValue();
										else if (val instanceof Long)
											row[colindices[i]] = ((Long) val).floatValue();
										else if (val instanceof BigDecimal)
											row[colindices[i]] = ((BigDecimal) val).floatValue();
										else
											row[colindices[i]] = Float.valueOf(val.toString());
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.DECIMAL)
									{
										if (val instanceof String)
											row[colindices[i]] = BigDecimal.valueOf(Double.valueOf((String) val));
										else if (val instanceof Double)
											row[colindices[i]] = BigDecimal.valueOf((Double) val);
										else if (val instanceof Float)
											row[colindices[i]] = BigDecimal.valueOf(((Float) val).doubleValue());
										else if (val instanceof Integer)
											row[colindices[i]] = BigDecimal.valueOf(((Integer) val).doubleValue());
										else if (val instanceof Long)
											row[colindices[i]] = BigDecimal.valueOf(((Long) val).doubleValue());
										else if (val instanceof BigDecimal)
											row[colindices[i]] = val;
										else
											row[colindices[i]] = BigDecimal.valueOf(Double.valueOf(val.toString()));
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.INTEGER)
									{
										if (val instanceof String)
											row[colindices[i]] = Integer.valueOf((String) val);
										else if (val instanceof Double)
											row[colindices[i]] = ((Double) val).intValue();
										else if (val instanceof Integer)
											row[colindices[i]] = val;
										else if (val instanceof Long)
											row[colindices[i]] = ((Long) val).intValue();
										else if (val instanceof Float)
											row[colindices[i]] = ((Float) val).intValue();
										else if (val instanceof BigDecimal)
											row[colindices[i]] = ((BigDecimal) val).intValue();
										else
											row[colindices[i]] = Integer.valueOf(val.toString());
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.BIGINT)
									{
										if (val instanceof String)
											row[colindices[i]] = Long.valueOf((String) val);
										else if (val instanceof Double)
											row[colindices[i]] = ((Double) val).longValue();
										else if (val instanceof Integer)
											row[colindices[i]] = ((Integer) val).longValue();
										else if (val instanceof Long)
											row[colindices[i]] = val;
										else if (val instanceof Float)
											row[colindices[i]] = ((Float) val).longValue();
										else if (val instanceof BigDecimal)
											row[colindices[i]] = ((BigDecimal) val).longValue();
										else
											row[colindices[i]] = Integer.valueOf(val.toString());
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.DATE || foundt.tmd.dataTypes[colindices[i]] == Types.TIMESTAMP)
									{
										if (val instanceof String)
											row[colindices[i]] = (new DateParser()).parseUsingPossibleFormats((String) val, clientTimeZone);
										else if (val instanceof Date)
											row[colindices[i]] = val;
										else if (val instanceof Timestamp)
											row[colindices[i]] = new Date(((Timestamp) val).getTime());
										else if (val instanceof Long)
											row[colindices[i]] = new Date((Long) val);
										else
											row[colindices[i]] = (new DateParser()).parseUsingPossibleFormats(val.toString(), clientTimeZone);
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.BOOLEAN)
									{
										if (val instanceof String)
											row[colindices[i]] = Boolean.valueOf((String) val);
										else if (val instanceof Double)
											row[colindices[i]] = ((Double) val) != 0;
										else if (val instanceof Integer)
											row[colindices[i]] = ((Integer) val) != 0;
										else if (val instanceof Long)
											row[colindices[i]] = ((Long) val) != 0;
										else if (val instanceof Float)
											row[colindices[i]] = ((Float) val) != 0;
										else
											row[colindices[i]] = Boolean.valueOf(val.toString());
									} else if (foundt.tmd.dataTypes[colindices[i]] == Types.VARCHAR)
									{
										if (val instanceof String)
											row[colindices[i]] = val;
										else
											row[colindices[i]] = val.toString();
									}
								} catch (NumberFormatException nfe)
								{
									logger.info("Unable to parse number: " + val.toString());
								}
							}
						}
						try
						{
							foundt.latch.awaitRead();
							foundt.latch.awaitWrite();
							foundt.addRow(row, clientTimeZone, null);
							rowsProcessed++;
						} catch (InterruptedException e)
						{
							throw new SQLException("Unable to insert into table: " + foundt.name);
						} finally
						{
							foundt.latch.releaseWrite();
							foundt.latch.releaseRead();
						}
					} else if (remoteQuery != null)
					{
						bulkLoad(db, Util.getString(remoteQuery.getChild(0).getText()), Util.getString(remoteQuery.getChild(1).getText()),
								Util.getString(remoteQuery.getChild(2).getText()), Util.getString(remoteQuery.getChild(3).getText()), tableSchema, tableName,
								Util.getString(remoteQuery.getChild(4).getText()), foundt, colindices, tableSchema);
					}
				} finally
				{
					try
					{
						foundt.release();
					} catch (IOException e)
					{
						logger.error(e);
					}
				}
				break;
			case MemgrammarParser.DELETE:
				Delete d;
				try
				{
					d = new Delete(this, db, t, parameters);
					d.delete();
					rowsProcessed = d.rowsProcessed();
				} catch (InterruptedException e3)
				{
					logger.error(e3);
				}
				break;
			case MemgrammarParser.UPDATE:
				Update u;
				try
				{
					u = new Update(this, db, t, parameters);
					u.update();
					rowsProcessed = u.rowsProcessed();
				} catch (InterruptedException e2)
				{
					logger.error(e2);
				}
				break;
			case MemgrammarParser.LOADDATAINFILE:
				File f = new File(Util.getString(t.getChild(0).getText()));
				if (!f.exists())
					throw new SQLException("File doesn't exist: " + f.getAbsoluteFile());
				String enclosedby = "\"";
				String delimitedby = ",";
				String terminatedby = "\n";
				boolean skipfirst = false;
				fc = t.getChild(1);
				tableName = fc.getText();
				tableSchema = Select.getTableSchema(fc);
				if (tableSchema == null)
				{
					throw new SQLException("Schema not specified");
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt == null)
					throw new SQLException("Table " + tableName + " not found");
				try
				{
					foundt.flush();
				} catch (IOException e1)
				{
					logger.error(e1);
					throw new SQLException("Unable to flush table " + tableName);
				}
				succeeded = false;
				try
				{
					foundt.reserve();
					succeeded = true;
				} catch (ClassNotFoundException e1)
				{
					logger.error(e1);
					throw new SQLException("Unable to reserve table for insert");
				} catch (IOException e1)
				{
					logger.error(e1);
					throw new SQLException("Unable to reserve table for insert");
				} catch (InterruptedException e1)
				{
					logger.error(e1);
					throw new SQLException("Unable to reserve table for insert");
				}
				try
				{
					foundt.latch.awaitRead();
				} catch (InterruptedException e1)
				{
					logger.error(e1);
					throw new SQLException("Unable to reserve table for insert");
				}
				try
				{
					foundt.latch.awaitWrite();
				} catch (InterruptedException e1)
				{
					logger.error(e1);
					foundt.latch.releaseRead();
					throw new SQLException("Unable to reserve table for insert");
				}
				collist = null;
				try
				{
					for (int i = 2; i < t.getChildCount(); i++)
					{
						Tree child = t.getChild(i);
						if (child.getType() == MemgrammarParser.DELIMITEDBY)
						{
							delimitedby = Util.getString(child.getChild(0).getText());
							delimitedby = Util.replaceStr(delimitedby, "\\t", "\t");
							if (delimitedby.length() != 1)
								delimitedby = "\n";
						}
						if (child.getType() == MemgrammarParser.ENCLOSEDBY)
						{
							enclosedby = Util.getString(child.getChild(0).getText());
							if (enclosedby.length() != 1)
								enclosedby = "\"";
						}
						if (child.getType() == MemgrammarParser.TERMINATEDBY)
						{
							terminatedby = Util.getString(child.getChild(0).getText());
							terminatedby = Util.replaceStr(terminatedby, "\\n", "\n");
							terminatedby = Util.replaceStr(terminatedby, "\\r", "\r");
						}
						if (child.getType() == MemgrammarParser.COLLIST)
							collist = child;
						if (child.getType() == MemgrammarParser.SKIPFIRST)
						{
							skipfirst = true;
						}
					}
					colindices = null;
					colindices = getColumnList(collist, foundt);
					try
					{
						long fileSize = f.length();
						// Do basic check for enough memory
						int countMemColumns = 0;
						for (int i = 0; i < foundt.tmd.diskBased.length; i++)
							if (!foundt.tmd.diskBased[i])
								countMemColumns++;
						if (((fileSize * countMemColumns / foundt.tmd.diskBased.length) + db.memUsed() > (0.7 * Runtime.getRuntime().maxMemory())))
							throw new SQLException("Insufficient memory to load file");
						BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"), 1024 * 32);
						StringBuilder sb = new StringBuilder();
						char sepc = '|';
						if (delimitedby.length() == 1)
							sepc = delimitedby.charAt(0);
						char eb = enclosedby.charAt(0);
						int numcols = colindices.length;
						LinkedBlockingQueue<String> lineQueue = new LinkedBlockingQueue<String>(100);
						ParseFile pf = new ParseFile();
						/*
						 * Capture the range of rows loaded so that we can do delayed indexing. This will allow better
						 * use of memory as smaller number of unpacked indices will need to be maintained in memory at
						 * any time.
						 */
						RowRange rowsProcessedRange = new RowRange();
						rowsProcessedRange.start = foundt.getNumRows();
						ParallelAddRow par = new ParallelAddRow(foundt, out, pf.rowcount, clientTimeZone, rowsProcessedRange);
						LinkedBlockingQueue<Object[]> rowQueue = par.getRowQueue();
						AddLine[] lineThreads = new AddLine[Runtime.getRuntime().availableProcessors()];
						for (int i = 0; i < lineThreads.length; i++)
						{
							lineThreads[i] = new AddLine(foundt, lineQueue, rowQueue, numcols, colindices, pf, enclosedby, sepc);
							lineThreads[i].start();
						}
						long charCount = 0;
						int lineCount = 0;
						int startRows = foundt.getNumRows();
						try
						{
							while ((sb = Util.readLine(sb, br, true, eb, false, sepc, terminatedby)) != null)
							{
								if (killed)
									break;
								if (lineCount == 0 && sb.charAt(0) == 65279)
								{
									// Ignore encoding marker
									sb.deleteCharAt(0);
								}
								if (skipfirst && pf.rowcount.get() == 0)
								{
									pf.rowcount.incrementAndGet();
									continue;
								}
								int len = sb.length();
								if (len > 0)
								{
									charCount += len + 1;
									lineCount++;
									if (lineCount % 50000 == 0)
									{
										foundt.setEstimatedRows(startRows + ((int) (((long) lineCount * fileSize) / charCount)));
									}
									try
									{
										lineQueue.offer(sb.toString(), 5, TimeUnit.MINUTES);
									} catch (InterruptedException e)
									{
										logger.error(e);
										pf.numparseerrors.incrementAndGet();
									}
								}
							}
							br.close();
							for (int i = 0; i < lineThreads.length; i++)
								try
								{
									lineQueue.offer("", 5, TimeUnit.MINUTES);
								} catch (InterruptedException e)
								{
									logger.error(e);
								}
						} catch (IOException e)
						{
							throw new SQLException("Error reading file: " + e.getMessage());
						}
						for (int i = 0; i < lineThreads.length; i++)
							try
							{
								lineThreads[i].join();
							} catch (InterruptedException e1)
							{
								logger.error(e1);
							}
						par.join();
						foundt.pack();
						// Now, build the indices in bulk
						foundt.indexRange(rowsProcessedRange);
						rowsProcessed = foundt.getNumRows();
						if (pf.numignored.get() > 0)
							logger.error(pf.numignored + " lines ignored due to number format exception");
						if (pf.numparseerrors.get() > 0)
							logger.error(pf.numparseerrors + " errors parsing numeric values");
						try
						{
							db.pack();
						} catch (IOException e)
						{
							logger.error(e);
							throw new SQLException("Error: " + e.getMessage());
						}
					} catch (FileNotFoundException e)
					{
						throw new SQLException("File doesn't exist: " + f.getAbsoluteFile());
					}
				} finally
				{
					try
					{
						foundt.latch.releaseRead();
						foundt.latch.releaseWrite();
						foundt.release();
					} catch (IOException e)
					{
						logger.error(e);
					}
				}
				break;
			case MemgrammarParser.DUMPDDL:
				tableSchema = Select.getTableSchema(t);
				tableName = t.getChild(2).getText();
				String qualifiedTableName = ((tableSchema != null && tableSchema.length() > 0) ? (tableSchema + ".") : "") + tableName;
				String outDir = Util.getString(t.getChild(3).getText());
				File foutDir = new File(outDir);
				if (!foutDir.exists() || !foutDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + outDir);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt == null)
				{
					throw new SQLException("Table not found: " + qualifiedTableName);
				}
				String ddl = foundt.getCreateDDL(tableSchema);
				String ddlFilename = qualifiedTableName + ".ddl";
				File ddlFile = new File(foutDir, ddlFilename);
				BufferedWriter bw = null;
				try
				{
					bw = new BufferedWriter(new FileWriter(ddlFile));
					bw.write(ddl);
					bw.flush();
				} finally
				{
					if (bw != null)
					{
						bw.close();
					}
				}
				break;
			case MemgrammarParser.LOADDDL:
				tableSchema = Select.getTableSchema(t);
				tableName = t.getChild(2).getText();
				qualifiedTableName = ((tableSchema != null && tableSchema.length() > 0) ? (tableSchema + ".") : "") + tableName;
				String inDir = Util.getString(t.getChild(3).getText());
				File finDir = new File(inDir);
				if (!finDir.exists() || !finDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + inDir);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
				{
					throw new SQLException("Table already exists: " + qualifiedTableName);
				}
				ddlFilename = qualifiedTableName + ".ddl";
				ddlFile = new File(finDir, ddlFilename);
				if (!ddlFile.exists() || !ddlFile.isFile())
				{
					throw new SQLException("File not found: " + ddlFile.getAbsolutePath());
				}
				ddl = Util.getFileContent(ddlFile);
				Command cmd = new Command(threadNum, clientTimeZone);
				cmd.process(db, ddl, out);
				foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
				{
					foundt.flush();
				}
				break;
			case MemgrammarParser.DUMPTABLE:
				tableSchema = Select.getTableSchema(t);
				tableName = t.getChild(2).getText();
				qualifiedTableName = ((tableSchema != null && tableSchema.length() > 0) ? (tableSchema + ".") : "") + tableName;
				outDir = Util.getString(t.getChild(3).getText());
				foutDir = new File(outDir);
				if (!foutDir.exists() || !foutDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + outDir);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt == null)
				{
					throw new SQLException("Table not found: " + qualifiedTableName);
				}
				// Invoke DUMPDDL command
				String dumpDdlCmdString = "DUMPDDL " + qualifiedTableName + " OUTDIR '" + outDir + "'";
				Command cmdDumpDdl = new Command(threadNum, clientTimeZone);
				cmdDumpDdl.process(db, dumpDdlCmdString, out);
				// Invoke select * into outfile command
				String dataFilename = outDir + ((outDir.endsWith(Util.FILE_SEPARATOR)) ? "" : ("\\" + Util.FILE_SEPARATOR)) + qualifiedTableName + ".txt";
				String selectIntoFileCmdString = "SELECT * INTO OUTFILE '" + dataFilename + "' FROM " + qualifiedTableName;
				Main.executeCommand(db, selectIntoFileCmdString);
				break;
			case MemgrammarParser.LOADTABLE:
				tableSchema = Select.getTableSchema(t);
				tableName = t.getChild(2).getText();
				qualifiedTableName = ((tableSchema != null && tableSchema.length() > 0) ? (tableSchema + ".") : "") + tableName;
				inDir = Util.getString(t.getChild(3).getText());
				finDir = new File(inDir);
				if (!finDir.exists() || !finDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + inDir);
				}
				foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
				{
					throw new SQLException("Table already exists: " + qualifiedTableName);
				}
				// Invoke LOADDDL command
				String loadDdlCmdString = "LOADDDL " + qualifiedTableName + " INDIR '" + inDir + "'";
				Command cmdLoadDdl = new Command(threadNum, clientTimeZone);
				cmdLoadDdl.process(db, loadDdlCmdString, out);
				// Invoke select * into outfile command
				dataFilename = inDir + ((inDir.endsWith(Util.FILE_SEPARATOR)) ? "" : ("\\" + Util.FILE_SEPARATOR)) + qualifiedTableName + ".txt";
				String loadDataInFileCmdString = "LOAD DATA INFILE '" + dataFilename + "' INTO TABLE " + qualifiedTableName
						+ " DELIMITED BY '\\t' TERMINATED BY '\\r\\n' ENCLOSED BY '\"' SKIPFIRST";
				Command cmdLoadDataInFile = new Command(threadNum, clientTimeZone);
				cmdLoadDataInFile.process(db, loadDataInFileCmdString, out);
				foundt = db.findTable(tableSchema, tableName);
				if (foundt != null)
				{
					foundt.flush();
				}
				break;
			case MemgrammarParser.DUMPDB:
				outDir = Util.getString(t.getChild(0).getText());
				foutDir = new File(outDir);
				if (!foutDir.exists() || !foutDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + outDir);
				}
				if (db.tables == null || db.tables.length == 0)
				{
					throw new SQLException("Empty database - no tables found");
				}
				for (Table t : db.tables)
				{
					if (t instanceof memdb.storage.CalculatedTime || (t.schema != null && t.schema.equals("INFORMATION_SCHEMA")))
					{
						continue;
					}
					qualifiedTableName = ((t.schema != null && t.schema.length() > 0) ? (t.schema + ".") : "") + t.name;
					String dumpTableCmdString = "DUMPTABLE " + qualifiedTableName + " OUTDIR '" + outDir + "'";
					Command cmdDumpTable = new Command(threadNum, clientTimeZone);
					cmdDumpTable.process(db, dumpTableCmdString, out);
				}
				break;
			case MemgrammarParser.LOADDB:
				inDir = Util.getString(t.getChild(0).getText());
				finDir = new File(inDir);
				if (!finDir.exists() || !finDir.isDirectory())
				{
					throw new SQLException("Directory doesn't exist: " + inDir);
				}
				File[] files = finDir.listFiles();
				if (files == null || files.length == 0)
				{
					throw new SQLException("No files found under directory: " + inDir);
				}
				for (File file : files)
				{
					String filename = file.getName();
					if (file.isFile() && filename.length() > 4 && (filename.endsWith(".ddl") || filename.endsWith(".DDL")))
					{
						String tname = filename.substring(0, filename.length() - 4);
						String loadTableCmdString = "LOADTABLE " + tname + " INDIR '" + inDir + "'";
						Command cmdLoadTable = new Command(threadNum, clientTimeZone);
						cmdLoadTable.process(db, loadTableCmdString, out);
					}
				}
				break;
			default:
				unExecuted = true;
			}
			return results;
		} catch (Exception ex)
		{
			unExecuted = true;
			if (!(ex instanceof SQLException))
			{
				System.out.println("Exception on command: " + command);
				logger.error(ex.toString(), ex);
			}
			throw new SQLException(ex.getMessage());
		} finally
		{
			if (!unExecuted)
				logger.debug("exec: " + (System.currentTimeMillis() - beginTime) + "ms [" + command + "]");
		}
	}

	private class ParseFile
	{
		AtomicInteger numparseerrors = new AtomicInteger();
		AtomicInteger rowcount = new AtomicInteger();
		AtomicInteger numignored = new AtomicInteger();
	}

	private class AddLine extends Thread
	{
		Table foundt;
		LinkedBlockingQueue<String> lineQueue;
		LinkedBlockingQueue<Object[]> rowQueue;
		int numcols;
		int[] colindices;
		ParseFile pf;
		String enclosedby;
		char sepc;

		public AddLine(Table foundt, LinkedBlockingQueue<String> lineQueue, LinkedBlockingQueue<Object[]> rowQueue, int numcols, int[] colindices,
				ParseFile pf, String enclosedby, char sepc)
		{
			this.foundt = foundt;
			this.lineQueue = lineQueue;
			this.rowQueue = rowQueue;
			this.numcols = numcols;
			this.colindices = colindices;
			this.pf = pf;
			this.enclosedby = enclosedby;
			this.sepc = sepc;
		}

		public void run()
		{
			DateParser[] dp = new DateParser[numcols];
			char eb = enclosedby.charAt(0);
			char[] buff = new char[1000];
			while (true)
			{
				String s;
				try
				{
					s = lineQueue.take();
				} catch (InterruptedException e)
				{
					logger.error(e);
					return;
				}
				if (s.length() == 0)
					return;
				// Get a list of strings that represent the columns
				List<String> cols = new ArrayList<String>();
				char[] arr = s.toCharArray();
				int pos = 0;
				char previousChar = '\0';
				char nextChar = '\0';
				int len = arr.length;
				for (int i = 0; i < len; i++)
				{
					char currentChar = arr[i];
					if ((i + 1) < len)
					{
						nextChar = arr[i + 1];
					}
					// Make sure that current character is a separator and it is not escaped
					if (currentChar == sepc && (previousChar != '\\'))
					{
						cols.add(new String(buff, 0, pos));
						pos = 0;
						previousChar = '\0';
					} else if ((nextChar != sepc) || (currentChar != '\\'))
					{
						buff[pos++] = currentChar;
						if (pos == buff.length)
						{
							char[] newbuff = new char[buff.length * 2];
							System.arraycopy(buff, 0, newbuff, 0, buff.length);
							buff = newbuff;
						}
					}
					previousChar = currentChar;
				}
				cols.add(new String(buff, 0, pos));
				// Now process the parsed columns
				if (cols.size() < numcols)
					continue;
				Object[] row = new Object[numcols];
				try
				{
					for (int i = 0; i < numcols; i++)
					{
						if (colindices[i] < 0 || colindices[i] > row.length)
							continue;
						String st = cols.get(i);
						int stlen = st.length();
						if (stlen > 1 && st.charAt(0) == eb && st.charAt(stlen - 1) == eb)
						{
							st = st.substring(1, stlen - 1);
						}
						if (st == null)
							continue;
						int dtype = foundt.tmd.dataTypes[colindices[i]];
						if (st.equals("\\N"))
							cols.set(i, null);
						else if (dtype == Types.INTEGER)
						{
							if (stlen == 0)
								row[colindices[i]] = Integer.MIN_VALUE;
							else
							{
								try
								{
									row[colindices[i]] = Integer.parseInt(st);
								} catch (NumberFormatException ex)
								{
									row[colindices[i]] = 0;
									pf.numparseerrors.incrementAndGet();
								}
							}
						} else if (dtype == Types.BIGINT)
						{
							if (stlen == 0)
								row[colindices[i]] = Long.MAX_VALUE;
							else
							{
								try
								{
									row[colindices[i]] = Long.parseLong(st);
								} catch (NumberFormatException ex)
								{
									row[colindices[i]] = 0L;
									pf.numparseerrors.incrementAndGet();
								}
							}
						} else if (dtype == Types.DOUBLE)
						{
							if (stlen == 0)
								row[colindices[i]] = Double.NaN;
							else
							{
								try
								{
									row[colindices[i]] = Double.parseDouble(st);
								} catch (NumberFormatException ex)
								{
									row[colindices[i]] = 0.0d;
									pf.numparseerrors.incrementAndGet();
								}
							}
						} else if (dtype == Types.FLOAT)
						{
							if (stlen == 0)
								row[colindices[i]] = Table.FLOATNULL;
							else
							{
								try
								{
									row[colindices[i]] = Float.parseFloat(st);
								} catch (NumberFormatException ex)
								{
									row[colindices[i]] = 0.0f;
									pf.numparseerrors.incrementAndGet();
								}
							}
						} else if (dtype == Types.VARCHAR)
							if (cols.get(i).length() == 0 && enclosedby.length() > 0)
								row[colindices[i]] = null;
							else
								row[colindices[i]] = Util.getString(st);
						else if (dtype == Types.DATE || dtype == Types.TIMESTAMP)
						{
							if (dp[i] == null)
								dp[i] = new DateParser();
							row[colindices[i]] = dp[i].parseUsingPossibleFormats(st, clientTimeZone);
						} else if (dtype == Types.BOOLEAN)
							row[colindices[i]] = Boolean.valueOf(st);
						else if (dtype == Types.DECIMAL)
							row[colindices[i]] = cols.get(i) == null ? null : BigDecimal.valueOf(Double.parseDouble(st));
						else if (stlen == 0)
							row[colindices[i]] = null;
					}
					try
					{
						if (row.length > 0)
							rowQueue.offer(row, 5, TimeUnit.MINUTES);
					} catch (InterruptedException e)
					{
						logger.error(e);
						continue;
					}
				} catch (NumberFormatException nfe)
				{
					pf.numignored.incrementAndGet();
				}
			}
		}
	}

	private void setColumnMetadata(TableMetadata newtmd, Tree columnDefinition, int columnPosition, boolean defaultDisk, boolean defaultCompressed,
			boolean alter, Table t) throws SQLException, ClassNotFoundException, IOException, InterruptedException
	{
		String name = columnDefinition.getText();
		newtmd.columnNames[columnPosition] = name;
		int dtype = columnDefinition.getChild(0).getType();
		switch (dtype)
		{
		case MemgrammarParser.VARCHARTYPE:
			newtmd.dataTypes[columnPosition] = Types.VARCHAR;
			break;
		case MemgrammarParser.INTEGERTYPE:
			newtmd.dataTypes[columnPosition] = Types.INTEGER;
			break;
		case MemgrammarParser.FLOATTYPE:
			newtmd.dataTypes[columnPosition] = Types.DOUBLE;
			break;
		case MemgrammarParser.DATETIMETYPE:
			newtmd.dataTypes[columnPosition] = Types.TIMESTAMP;
			break;
		case MemgrammarParser.DATETYPE:
			newtmd.dataTypes[columnPosition] = Types.DATE;
			break;
		case MemgrammarParser.BIGINTTYPE:
			newtmd.dataTypes[columnPosition] = Types.BIGINT;
			break;
		case MemgrammarParser.DECIMALTYPE:
			newtmd.dataTypes[columnPosition] = Types.DECIMAL;
			break;
		}
		newtmd.diskBased[columnPosition] = defaultDisk;
		newtmd.compressed[columnPosition] = defaultCompressed;
		if (columnDefinition.getChildCount() > 1)
		{
			for (int j = 1; j < columnDefinition.getChildCount(); j++)
			{
				Tree child = columnDefinition.getChild(j);
				if (child.getType() == MemgrammarParser.INTEGER)
				{
					newtmd.widths[columnPosition] = Integer.valueOf(columnDefinition.getChild(j).getText());
				} else if (child.getType() == MemgrammarParser.PRIMARYKEY)
				{
					newtmd.primarykey[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.NOINDEX)
				{
					newtmd.noindex[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.TRANSIENT)
				{
					newtmd.transientcolumns[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.FOREIGNKEY)
				{
					newtmd.foreignkey[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.DISK)
				{
					newtmd.diskBased[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.MEMORY)
				{
					newtmd.diskBased[columnPosition] = false;
				} else if (child.getType() == MemgrammarParser.COMPRESSED)
				{
					newtmd.compressed[columnPosition] = true;
				} else if (child.getType() == MemgrammarParser.UNCOMPRESSED)
				{
					newtmd.compressed[columnPosition] = false;
				}
			}
		}
		if (alter)
		{
			if (defaultDisk)
			{
				if (newtmd.diskBased[columnPosition])
				{
					/*
					 * Don't allow changes in compression on disk. Need to copy table first. This may be supported
					 * later, but it's VERY expensive
					 */
					if (defaultCompressed != newtmd.compressed[columnPosition])
						newtmd.compressed[columnPosition] = defaultCompressed;
				} else
				{
					/*
					 * Moving to in-memory so need to load
					 */
					t.updateColumnMetadata(columnPosition);
				}
			} else
			{
				if (!newtmd.diskBased[columnPosition])
					t.updateColumnMetadata(columnPosition);
			}
		}
	}

	private int[] getColumnList(Tree collist, Table foundt) throws SQLException
	{
		int[] colindices = null;
		if (collist == null)
		{
			colindices = new int[foundt.getNumColumns()];
			for (int i = 0; i < colindices.length; i++)
				colindices[i] = i;
		} else
		{
			colindices = new int[collist.getChildCount()];
			for (int i = 0; i < collist.getChildCount(); i++)
			{
				String s = collist.getChild(i).getText();
				// Allow an input column to be ignored
				if (s.toUpperCase().equals("NULL"))
				{
					colindices[i] = -1;
					continue;
				}
				colindices[i] = foundt.tmd.getColumnIndex(s);
				if (colindices[i] < 0)
					throw new SQLException("Column not found: " + collist.getChild(i).getText());
			}
		}
		return colindices;
	}

	public void bulkLoad(Database db, String driver, String connect, String username, String password, String schema, String name, String query, Table t,
			int[] colindices, String keyStoreName) throws SQLException
	{
		query = Util.replaceStr(query, "\\'", "'");
		try
		{
			Class.forName(driver);
		} catch (ClassNotFoundException e2)
		{
			logger.error(e2);
			throw new SQLException("Driver not found");
		}
		Connection conn = null;
		try
		{
			conn = DriverManager.getConnection(connect, username, password);
			conn.setReadOnly(true);
		} catch (SQLException e1)
		{
			logger.error(e1);
			throw new SQLException("Unable to connect to remote database");
		}
		boolean newTable = false;
		if (t == null)
		{
			try
			{
				t = db.createTable(schema, name, false, keyStoreName, false, false);
				newTable = true;
			} catch (ClassNotFoundException e1)
			{
				logger.error(e1);
				return;
			} catch (InstantiationException e1)
			{
				logger.error(e1);
				return;
			} catch (IllegalAccessException e1)
			{
				logger.error(e1);
				return;
			}
		}
		try
		{
			Statement stmt = conn.createStatement();
			stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
			if (driver.equals("com.mysql.jdbc.Driver"))
				stmt.setFetchSize(Integer.MIN_VALUE);
			int lineno = 0;
			ResultSet rs = stmt.executeQuery(query);
			if (newTable)
			{
				ResultSetMetaData rsmd = rs.getMetaData();
				TableMetadata tmd = new TableMetadata(rsmd.getColumnCount());
				t.tmd = tmd;
				for (int i = 0; i < tmd.dataTypes.length; i++)
				{
					tmd.columnNames[i] = rsmd.getColumnName(i + 1);
					tmd.dataTypes[i] = rsmd.getColumnType(i + 1);
					if (tmd.dataTypes[i] == Types.NCHAR)
						tmd.dataTypes[i] = Types.VARCHAR;
					else if (tmd.dataTypes[i] == Types.NVARCHAR)
						tmd.dataTypes[i] = Types.VARCHAR;
					else if (tmd.dataTypes[i] == Types.REAL)
						tmd.dataTypes[i] = Types.FLOAT;
					tmd.widths[i] = 0;
				}
			}
			if (colindices == null)
			{
				colindices = getColumnList(null, t);
			}
			DateParser[] dp = new DateParser[colindices.length];
			while (rs.next())
			{
				Object[] rowarr = new Object[colindices.length];
				for (int i = 0; i < rowarr.length; i++)
				{
					if (colindices[i] < 0)
						continue;
					rowarr[colindices[i]] = rs.getObject(i + 1);
					if (rowarr[colindices[i]] != null)
					{
						/*
						 * Do any necessary datatype conversion
						 */
						if (t.tmd.dataTypes[colindices[i]] == Types.VARCHAR)
						{
							if (!(rowarr[colindices[i]] instanceof String))
							{
								rowarr[colindices[i]] = rowarr[colindices[i]].toString();
							}
						} else if (t.tmd.dataTypes[colindices[i]] == Types.INTEGER)
						{
							if (!(rowarr[colindices[i]] instanceof Integer))
							{
								rowarr[colindices[i]] = Integer.valueOf(rowarr[colindices[i]].toString());
							}
						} else if (t.tmd.dataTypes[colindices[i]] == Types.BIGINT)
						{
							if (!(rowarr[colindices[i]] instanceof Long))
							{
								rowarr[colindices[i]] = Long.valueOf(rowarr[colindices[i]].toString());
							}
						} else if (t.tmd.dataTypes[colindices[i]] == Types.DATE || t.tmd.dataTypes[colindices[i]] == Types.TIMESTAMP)
						{
							if (!(rowarr[colindices[i]] instanceof Date))
							{
								if (dp[i] == null)
									dp[i] = new DateParser();
								rowarr[colindices[i]] = dp[i].parseUsingPossibleFormats(rowarr[colindices[i]].toString(), clientTimeZone);
							}
						} else if (t.tmd.dataTypes[colindices[i]] == Types.DOUBLE)
						{
							if (!(rowarr[colindices[i]] instanceof Double))
							{
								rowarr[colindices[i]] = Double.valueOf(rowarr[colindices[i]].toString());
							}
						} else if (t.tmd.dataTypes[colindices[i]] == Types.FLOAT)
						{
							if (!(rowarr[colindices[i]] instanceof Float))
							{
								rowarr[colindices[i]] = Float.valueOf(rowarr[colindices[i]].toString());
							}
						}
					}
				}
				t.addRow(rowarr, clientTimeZone, null);
				lineno++;
				if (lineno % 25000 == 0)
					logger.debug(lineno);
			}
			stmt.close();
			t.pack();
			t.writeExternal();
			db.writeExternal();
		} catch (FileNotFoundException e)
		{
			throw new SQLException(e.getMessage());
		} catch (IOException e)
		{
			throw new SQLException(e.getMessage());
		} catch (SQLException e)
		{
			throw new SQLException(e.getMessage());
		}
	}

	public String getOutFileName()
	{
		return outFileName;
	}

	public int getRowsProcessed()
	{
		return rowsProcessed;
	}

	public boolean isKilled()
	{
		return killed;
	}

	public void setKilled(boolean killed)
	{
		this.killed = killed;
	}

	public String getCommandString()
	{
		return commandString;
	}

	public int getThreadNum()
	{
		return threadNum;
	}

	public void setThreadNum(int threadNum)
	{
		this.threadNum = threadNum;
	}

	public TimeZone getClientTimeZone()
	{
		return clientTimeZone;
	}
}
