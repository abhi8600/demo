package memdb.sql;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.Command;
import memdb.Database;
import memdb.ThreadPool;
import memdb.expressions.Expression;
import memdb.expressions.UnsupportedException;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;
import memdb.storage.CalculatedTime;
import memdb.storage.Table;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

public abstract class WhereStatement
{
	static Logger logger = Logger.getLogger(WhereStatement.class);
	protected Database db = null;
	QueryPosition[] positions = new QueryPosition[0];
	protected boolean postJoin;
	protected List<Boolean> maxOne = new ArrayList<Boolean>();
	protected Tree whereClause = null;
	protected List<Where> whereList = null;
	protected Set<Where> preJoinWhereSet = new HashSet<Where>();
	protected List<Where> postJoinWhereList = new ArrayList<Where>();
	protected RowSet[] joinedrows = null;
	protected long starttime;
	protected long prejointime;
	protected long postjointime;
	private Object[] parameters;
	private int parameterIndex;
	protected int rowsProcessed;
	private List<Expression> distinctForKeyList = new ArrayList<Expression>();
	protected LinkedBlockingQueue<ProcessJoinSplit> joinRowQueue = new LinkedBlockingQueue<ProcessJoinSplit>(100);
	protected Command c;
	protected boolean containsAggregates = false;
	protected int topn = -1;
	protected boolean isSort;
	protected Map<Integer, List<Expression>> expressionCache = new HashMap<Integer, List<Expression>>();

	public WhereStatement(Command c, Database db, Tree t, Object[] parameters) throws SQLException, InterruptedException
	{
		boolean success = false;
		try
		{
			this.c = c;
			this.db = db;
			this.parameters = parameters;
			parameterIndex = 0;
			processFrom(t);
			for (int i = 0; i < t.getChildCount(); i++)
			{
				if (t.getChild(i).getType() == MemgrammarParser.WHERE)
					whereClause = t.getChild(i);
			}
			/*
			 * Get base rowsets pre-filtered if possible. Attempt to apply filters before joins to reduce join
			 * explosion.
			 */
			try
			{
				whereList = Where.parseWhere(this, whereClause);
			} catch (SQLException e)
			{
				throw e;
			}
			starttime = System.currentTimeMillis();
			/*
			 * Execute filters for each position in parallel
			 */
			ThreadPool tp = new ThreadPool();
			List<ProcessPositionFilters> ppflist = new ArrayList<ProcessPositionFilters>();
			try
			{
				CountDownLatch cdl = new CountDownLatch(positions.length);
				for (QueryPosition qp : positions)
				{
					ProcessPositionFilters ppf = new ProcessPositionFilters(cdl, qp);
					tp.executeLocal(ppf);
					ppflist.add(ppf);
				}
				cdl.await();
			} finally
			{
				tp.delete();
			}
			// Propagate exceptions
			for (ProcessPositionFilters ppf : ppflist)
				if (ppf.getException() != null)
				{
					logger.error(ppf.getException().getMessage());
					throw ppf.getException();
				}
			if (whereList != null)
				for (Where w : whereList)
				{
					if (!preJoinWhereSet.contains(w))
						postJoinWhereList.add(w);
				}
			if (distinctForKeyList.size() > 0)
			{
				/*
				 * If ensuring distinctness for a key, filter the rowset. This feature is used to ensure that only one
				 * row per unique key is used in a query. This ensures that queries that feed dimension tables that come
				 * from non-distinct sources do not create duplicate keys.
				 */
				positions[0].rowset = FilterForDistinctKeys.filterKeys(positions[0].rowset, positions, distinctForKeyList);
			}
			/*
			 * Calculate the amount each table is being filtered for join optimization
			 */
			for (int i = 0; i < positions.length; i++)
			{
				int numFilteredRows = positions[i].rowset != null ? positions[i].rowset.size() : 0;
				positions[i].filterAmount = ((double) numFilteredRows) / ((double) positions[i].table.getNumRows());
			}
			/*
			 * Calculate join order. Start with the most filtered table, and then continue to join the next most
			 * filtered table that has a join condition with the current table. Essentially this is
			 * cost-based-optimization.
			 */
			boolean hasOuterJoin = false;
			boolean hasSingleTableJoin = false;
			boolean hasDuplicateTables = false;
			/*
			 * Don't re-order join if there are outer joins or empty join conditions
			 */
			for (int i = 1; i < positions.length; i++)
			{
				if (positions[i].joinCondition.joinDirection == MemgrammarParser.LEFTOUTERJOIN
						|| positions[i].joinCondition.joinDirection == MemgrammarParser.RIGHTOUTERJOIN)
					hasOuterJoin = true;
				if (i > 0)
				{
					if (positions[i].joinCondition.conditionTree != null)
					{
						Set<String> tset = new HashSet<String>();
						getJoinTablesOrAliases(positions[i].joinCondition.conditionTree, tset);
						if (tset.size() < 2)
							hasSingleTableJoin = true;
					}
				}
				for (int j = i + 1; j < positions.length; j++)
				{
					if (positions[i].table == positions[j].table)
					{
						hasDuplicateTables = true;
					}
				}
			}
			if (!hasSingleTableJoin)
			{
				if (!hasOuterJoin)
				{
					if (!hasDuplicateTables)
						optimizeJoinPositionsAndConditions();
				} else
					changeRightToLeftOuterJoins();
			}
			success = true;
		} finally
		{
			if (!success)
				releaseTables();
		}
	}

	private class ProcessPositionFilters implements Runnable
	{
		private CountDownLatch cdl;
		private QueryPosition qp;
		private SQLException ex;

		public ProcessPositionFilters(CountDownLatch cdl, QueryPosition qp)
		{
			this.cdl = cdl;
			this.qp = qp;
		}

		public SQLException getException()
		{
			return ex;
		}

		@Override
		public void run()
		{
			RowSet newset = null;
			boolean filterApplied = false;
			AtomicBoolean nullset = new AtomicBoolean(false);
			try
			{
				if (whereList != null)
				{
					List<Where> parallelFilters = new ArrayList<Where>();
					List<Where> otherFilters = new ArrayList<Where>();
					for (Where w : whereList)
					{
						if (c.isKilled())
						{
							return;
						}
						if (w.containsAggregate(new AtomicInteger(0)))
						{
							ex = new SQLException("Invalid where clause, cannot contain aggregate functions");
							return;
						}
						Set<QueryPosition> tableSet = new HashSet<QueryPosition>();
						w.getTables(tableSet);
						// Only apply single table filters pre-join
						if (tableSet.size() != 1)
							continue;
						if (!tableSet.contains(qp))
							continue;
						if (w.isBaseRowsParallel())
							parallelFilters.add(w);
						else
							otherFilters.add(w);
					}
					if (parallelFilters.size() > 0)
					{
						CountDownLatch pcdl = new CountDownLatch(parallelFilters.size());
						ThreadPool tp = new ThreadPool();
						try
						{
							List<ExecuteFilter> eflist = new ArrayList<ExecuteFilter>();
							LinkedBlockingQueue<RowSet> resultList = new LinkedBlockingQueue<RowSet>();
							for (Where w : parallelFilters)
							{
								ExecuteFilter ef = new ExecuteFilter(qp, pcdl, w, resultList, nullset);
								eflist.add(ef);
								tp.executeLocal(ef);
							}
							CountDownLatch mcdl = new CountDownLatch(Runtime.getRuntime().availableProcessors());
							for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++)
							{
								MergeFilters mf = new MergeFilters(resultList, pcdl, mcdl, qp.table.getNumRows(), nullset);
								tp.executeLocal(mf);
							}
							try
							{
								mcdl.await();
							} catch (InterruptedException e)
							{
								ex = new SQLException("Thread interrupted during pre-filtering");
								return;
							}
							for (ExecuteFilter ef : eflist)
							{
								if (ef.getException() != null)
								{
									ex = ef.getException();
									return;
								}
							}
							try
							{
								if (nullset.get())
									newset = RowSet.getEmptySet();
								else
									newset = resultList.take();
							} catch (InterruptedException e)
							{
								ex = new SQLException("Thread interrupted during pre-filtering");
								return;
							}
						} finally
						{
							tp.delete();
						}
						filterApplied = true;
					}
					if (!nullset.get())
					{
						for (Where w : otherFilters)
						{
							try
							{
								long stime = System.currentTimeMillis();
								RowSet wset;
								try
								{
									wset = w.getBaseRows(newset);
								} catch (SQLException e)
								{
									ex = e;
									return;
								}
								long etime = System.currentTimeMillis();
								if (logger.isTraceEnabled())
									logger.trace("Pre-filtered (" + w.toString() + ") " + (etime - stime) + "ms (" + (wset == null ? "0" : wset.size()) + ")");
								if (wset == null)
								{
									newset = null;
									filterApplied = true;
									break;
								}
								newset = wset;
								filterApplied = true;
								synchronized (preJoinWhereSet)
								{
									preJoinWhereSet.add(w);
								}
							} catch (UnsupportedException ue)
							{
								continue;
							}
						}
						if (filterApplied)
						{
							if (newset == null)
								qp.rowset = RowSet.getEmptySet();
							else
								qp.rowset = newset;
						}
					} else
						qp.rowset = RowSet.getEmptySet();
				}
			} finally
			{
				if (!filterApplied)
				{
					RowSet rs = new RowSet(0, qp.table.getNumRows() - 1);
					qp.rowset = rs;
				}
				cdl.countDown();
			}
		}
	}

	private class MergeFilters implements Runnable
	{
		private LinkedBlockingQueue<RowSet> resultList;
		private CountDownLatch cdl;
		private CountDownLatch mcdl;
		private int numRows;
		private AtomicBoolean nullset;

		public MergeFilters(LinkedBlockingQueue<RowSet> resultList, CountDownLatch cdl, CountDownLatch mcdl, int numRows, AtomicBoolean nullset)
		{
			this.resultList = resultList;
			this.cdl = cdl;
			this.mcdl = mcdl;
			this.numRows = numRows;
			this.nullset = nullset;
		}

		@Override
		public void run()
		{
			try
			{
				while (true)
				{
					if (nullset.get())
						break;
					if (cdl.getCount() == 0 && resultList.size() == 1)
						break;
					RowSet rs1 = null;
					RowSet rs2 = null;
					synchronized (resultList)
					{
						if (resultList.size() < 2)
						{
							try
							{
								Thread.sleep(200);
							} catch (InterruptedException e)
							{
								logger.error(e);
								break;
							}
							continue;
						}
						try
						{
							rs1 = resultList.take();
							rs2 = resultList.take();
						} catch (InterruptedException e)
						{
							break;
						}
					}
					if (rs1 == null || rs2 == null)
					{
						nullset.set(true);
						break;
					}
					rs1 = rs1.getIntersection(rs2, numRows);
					resultList.add(rs1);
				}
			} finally
			{
				mcdl.countDown();
			}
		}
	}

	private class ExecuteFilter implements Runnable
	{
		private Where w;
		private CountDownLatch cdl;
		private RowSet wset;
		private SQLException ex = null;
		private LinkedBlockingQueue<RowSet> results;
		private AtomicBoolean nullset;
		QueryPosition qp;

		public ExecuteFilter(QueryPosition qp, CountDownLatch cdl, Where w, LinkedBlockingQueue<RowSet> results, AtomicBoolean nullset)
		{
			this.cdl = cdl;
			this.w = w;
			this.results = results;
			this.nullset = nullset;
			this.qp = qp;
		}

		@Override
		public void run()
		{
			long stime = System.currentTimeMillis();
			try
			{
				if (nullset.get())
				{
					results.add(RowSet.getEmptySet());
					return;
				}
				wset = w.getBaseRows(null);
				if (wset == null)
				{
					nullset.set(true);
					results.add(RowSet.getEmptySet());
				} else
					results.add(wset);
				long etime = System.currentTimeMillis();
				if (logger.isTraceEnabled())
					logger.trace("Pre-filtered (" + w.toString() + ") " + (etime - stime) + "ms (" + (wset == null ? "0" : wset.size()) + ")");
				synchronized (preJoinWhereSet)
				{
					preJoinWhereSet.add(w);
				}
			} catch (SQLException e)
			{
				nullset.set(true);
				ex = e;
				results.add(RowSet.getEmptySet());
				return;
			} catch (UnsupportedException e)
			{
				// This is an insufficiently selective filter, so ignore until later
				results.add(new RowSet(0, qp.table.getNumRows() - 1));
				return;
			} finally
			{
				cdl.countDown();
			}
		}

		public SQLException getException()
		{
			return ex;
		}
	}

	private void optimizeJoinPositionsAndConditions() throws SQLException
	{
		Set<Object> usedTables = new HashSet<Object>();
		QueryPosition[] orderedPositions = new QueryPosition[positions.length];
		QueryPosition[] oldPositions = new QueryPosition[positions.length];
		List<Tree> joinConditions = new ArrayList<Tree>();
		boolean hasThreeWayJoin = false;
		boolean hasNoConditionJoin = false;
		for (int i = 0; i < positions.length; i++)
		{
			try
			{
				oldPositions[i] = (QueryPosition) positions[i].clone();
			} catch (CloneNotSupportedException ex)
			{
				logger.error(ex);
				return;
			}
			if (positions[i].joinCondition != null && positions[i].joinCondition.conditionTree != null)
			{
				List<Tree> joinSet = getIndividualJoinConditions(positions[i].joinCondition.conditionTree);
				Set<String> tableSet = new HashSet<String>();
				getJoinTablesOrAliases(positions[i].joinCondition.conditionTree, tableSet);
				if (tableSet.size() > 2)
					hasThreeWayJoin = true;
				if (joinSet.size() == 0)
					hasNoConditionJoin = true;
				joinConditions.addAll(joinSet);
			} else if (i > 0)
				hasNoConditionJoin = true;
		}
		List<Tree> availableConditions = new ArrayList<Tree>(joinConditions);
		/*
		 * Re-order tables and joins based on optimized order. Only re-order if all inner join and no compound joins
		 * exist
		 */
		for (int i = 0; i < oldPositions.length; i++)
		{
			// Get the most filtered table that joins to the last one
			int firstIndex = -1;
			int curFilterRows = Integer.MAX_VALUE;
			if (!hasThreeWayJoin || hasNoConditionJoin)
			{
				for (int j = 0; j < oldPositions.length; j++)
				{
					Object tableObject = oldPositions[j].alias != null ? oldPositions[j].alias : oldPositions[j].table;
					if (!usedTables.contains(tableObject))
					{
						boolean valid = false;
						boolean hasPrimaryKeyJoin = false;
						if (i == 0)
							valid = true;
						else
						{
							// Otherwise, make sure this table joins to one of the previous ones
							for (int k = i - 1; k >= 0; k--)
							{
								List<Tree> conditions = hasJoin(orderedPositions[k].alias, oldPositions[j].alias, orderedPositions[k].table,
										oldPositions[j].table, joinConditions);
								// Get the join condition with this and the previous table
								if (conditions != null && conditions.size() > 0)
								{
									valid = true;
									// Check to see if this one of these is on our primary key
									for (Tree condition : conditions)
									{
										if (this.isEquiJoinOnPrimaryKey(condition, oldPositions[j].table))
										{
											hasPrimaryKeyJoin = true;
											break;
										}
									}
									break;
								}
							}
						}
						if (!valid)
							continue;
						int numFilteredRows = ((Double) (oldPositions[j].filterAmount * (double) oldPositions[j].table.getNumRows())).intValue();
						/*
						 * Need to give a major penalty to being joined to via a primary key. Because joins to primary
						 * keys are so much faster, would prefer that such a table is later. E.g. if table 1 in a query
						 * has a primary key that is joined to by table 2, it is MUCH better to have table 2 go first to
						 * lookup the primary key in table 1. To do this see if this table is being joined by it's
						 * primary key
						 */
						if (!hasPrimaryKeyJoin)
							for (int k = 0; k < oldPositions.length && !hasPrimaryKeyJoin; k++)
							{
								if (k == j)
									continue;
								List<Tree> conditions = hasJoin(oldPositions[k].alias, oldPositions[j].alias, oldPositions[k].table, oldPositions[j].table,
										joinConditions);
								// Get the join condition with this and the previous table
								if (conditions != null && conditions.size() > 0)
								{
									// Check to see if this one of these is on our primary key
									for (Tree condition : conditions)
									{
										if (this.isEquiJoinOnPrimaryKey(condition, oldPositions[j].table))
										{
											hasPrimaryKeyJoin = true;
											break;
										}
									}
								}
							}
						if (hasPrimaryKeyJoin)
							numFilteredRows *= 10;
						if (firstIndex == -1)
						{
							firstIndex = j;
							curFilterRows = numFilteredRows;
						} else if (numFilteredRows < curFilterRows)
						{
							curFilterRows = numFilteredRows;
							firstIndex = j;
						}
					}
				}
			} else
				firstIndex = i;
			if (firstIndex == -1)
				firstIndex = i;
			orderedPositions[i] = positions[firstIndex];
			if (i == 0)
			{
				orderedPositions[i].joinCondition = null;
			}
			Object tableObject = oldPositions[firstIndex].alias != null ? oldPositions[firstIndex].alias : oldPositions[firstIndex].table;
			usedTables.add(tableObject);
			if (i > 0)
			{
				List<Tree> conditions = new ArrayList<Tree>();
				for (int k = i - 1; k >= 0; k--)
				{
					List<Tree> tableConditions = hasJoin(orderedPositions[k].alias, orderedPositions[i].alias, orderedPositions[k].table,
							orderedPositions[i].table, availableConditions);
					// Get the join condition with this and one of the previous tables
					if (tableConditions != null)
					{
						conditions.addAll(tableConditions);
						availableConditions.removeAll(conditions);
						/*
						 * Add any conditions that are single table on this table
						 */
						List<Tree> newConditions = new ArrayList<Tree>();
						for (Tree conditionTree : availableConditions)
						{
							Set<String> tset = new HashSet<String>();
							if (conditionTree.getType() == MemgrammarParser.ON)
								conditionTree = conditionTree.getChild(0);
							getJoinTablesOrAliases(conditionTree, tset);
							if (tset.size() == 1 && (tset.contains(orderedPositions[i].table.name) || tset.contains(orderedPositions[i].alias)))
							{
								newConditions.add(conditionTree);
							}
						}
						conditions.addAll(newConditions);
						availableConditions.removeAll(newConditions);
					}
				}
				Tree condition = combineConditions(conditions);
				JoinCondition newjc = new JoinCondition(MemgrammarParser.INNERJOIN, condition, this, orderedPositions[i].table);
				orderedPositions[i].joinCondition = newjc;
			}
		}
		if (availableConditions.size() > 0)
		{
			if (whereList == null)
				whereList = new ArrayList<Where>(0);
		}
		positions = orderedPositions;
		if (whereList != null)
		{
			for (Where w : whereList)
			{
				// ReParse where to make sure it points to the proper table indexes
				w.reParse();
			}
			if (availableConditions.size() > 0)
			{
				/*
				 * Apply any unused conditions to the where list - example a single-table filter in a join condition for
				 * a table that is moved to the first position
				 */
				for (Tree t : availableConditions)
				{
					postJoinWhereList.add(new Where(this, t));
				}
			}
		}
		/*
		 * Make sure indexes are reset with new order
		 */
		for (int i = 0; i < positions.length; i++)
		{
			positions[i].index = i;
		}
	}

	private void changeRightToLeftOuterJoins()
	{
		/*
		 * Re-write join order to turn right outer joins into left outer joins
		 */
		boolean changed = true;
		while (changed)
		{
			changed = false;
			for (int i = 1; i < positions.length; i++)
			{
				if (positions[i].joinCondition == null)
					continue;
				if (positions[i].joinCondition.joinDirection == MemgrammarParser.RIGHTOUTERJOIN)
				{
					QueryPosition[] newPositions = new QueryPosition[positions.length];
					newPositions[0] = positions[i];
					int index = 1;
					for (int j = 0; j < positions.length; j++)
					{
						newPositions[j].index = j;
						if (j == i)
							continue;
						newPositions[index] = positions[j];
						if (j < i)
						{
							/*
							 * If the original join was to the left of the right outer join, turn it into a left outer
							 * join. Otherwise, any inner joins will negate the right outer join's original effect
							 */
							if (j == 0)
							{
								// In this case there is no join condition to copy, so use the original
								newPositions[index].joinCondition = positions[i].joinCondition;
							}
							newPositions[index].joinCondition.joinDirection = MemgrammarParser.LEFTOUTERJOIN;
						}
						index++;
					}
					positions = newPositions;
					changed = true;
					newPositions[0].joinCondition = null;
					break;
				}
			}
		}
	}

	private Tree combineConditions(List<Tree> conditions)
	{
		Tree current = null;
		for (Tree condition : conditions)
		{
			if (current == null)
				current = condition;
			else
			{
				CommonToken to = new CommonToken(MemgrammarParser.AND);
				CommonTree newtree = new CommonTree(to);
				newtree.addChild(current);
				newtree.addChild(condition);
				current = newtree;
			}
		}
		return current;
	}

	private List<Tree> getIndividualJoinConditions(Tree root)
	{
		if (root.getType() == MemgrammarParser.ON)
			root = root.getChild(0);
		List<Tree> clist = new ArrayList<Tree>();
		if (root.getType() == MemgrammarParser.AND)
		{
			clist.addAll(getIndividualJoinConditions(root.getChild(0)));
			clist.addAll(getIndividualJoinConditions(root.getChild(1)));
		} else
			clist.add(root);
		return clist;
	}

	private final List<Tree> hasJoin(String alias1, String alias2, Table table1, Table table2, List<Tree> conditions) throws SQLException
	{
		List<Tree> conditionList = new ArrayList<Tree>();
		for (Tree conditionTree : conditions)
		{
			Set<String> tset = new HashSet<String>();
			if (conditionTree.getType() == MemgrammarParser.ON)
				conditionTree = conditionTree.getChild(0);
			getJoinTablesOrAliases(conditionTree, tset);
			if ((tset.contains(table1.name) || tset.contains(alias1)) && (tset.contains(table2.name) || tset.contains(alias2)))
			{
				conditionList.add(conditionTree);
			}
		}
		return conditionList;
	}

	public static String getTableSchema(Tree tableNode)
	{
		for (int i = 0; i < tableNode.getChildCount(); i++)
		{
			Tree fc = tableNode.getChild(i);
			if (fc.getType() == MemgrammarParser.SCHEMA)
				return fc.getChild(0).getText();
		}
		return null;
	}

	public final String findIdentifierAlias(Tree identPredicate) throws SQLException
	{
		if (identPredicate.getType() != MemgrammarParser.IDENT)
			return null;
		String alias = null;
		if (identPredicate.getChildCount() > 0)
		{
			if (identPredicate.getChildCount() == 2)
			{
				return null;
			} else
			{
				// Just table or alias
				alias = identPredicate.getChild(0).getText();
			}
		}
		return alias;
	}

	private QueryPosition findTable(String schema, String name)
	{
		for (int i = 0; i < positions.length; i++)
		{
			QueryPosition qp = positions[i];
			if (schema != null && qp.table.schema != null && !schema.equals(qp.table.schema))
				continue;
			if (name == null)
				continue;
			if (name.equals(qp.table.name))
				return qp;
			if (qp.alias != null && name.equals(qp.alias))
				return qp;
		}
		return null;
	}

	public final QueryPosition findIdentifierTable(Tree identPredicate) throws SQLException
	{
		if (identPredicate.getType() != MemgrammarParser.IDENT)
			return null;
		String alias = null;
		if (identPredicate.getChildCount() > 0)
		{
			if (identPredicate.getChildCount() == 2)
			{
				// Schema and table
				String schema = identPredicate.getChild(0).getText();
				String tableName = identPredicate.getChild(1).getText();
				return findTable(schema, tableName);
			} else
			{
				// Just table or alias
				alias = identPredicate.getChild(0).getText();
				return findTable(null, alias);
			}
		}
		// Otherwise, find the column in the current table list
		String name = identPredicate.getText();
		List<Integer> tableList = new ArrayList<Integer>();
		for (int i = 0; i < positions.length; i++)
		{
			Table t = positions[i].table;
			int index = t.tmd.getColumnIndex(name);
			if (index >= 0)
				tableList.add(i);
		}
		if (tableList.size() == 0)
		{
			throw new SQLException("Column not found: " + name);
		}
		if (tableList.size() > 1)
			throw new SQLException("Ambiguous column name: " + name);
		return positions[tableList.get(0)];
	}

	public final TIntHashSet getUnion(List<TIntHashSet> rowSets)
	{
		if (rowSets.size() == 0)
			return new TIntHashSet(0);
		TIntHashSet result = rowSets.get(0);
		for (int i = 1; i < rowSets.size(); i++)
		{
			result.addAll(rowSets.get(i));
		}
		return result;
	}

	public Database getDb()
	{
		return db;
	}

	public Table[] getTables()
	{
		Table[] tables = new Table[positions.length];
		for (int i = 0; i < tables.length; i++)
			tables[i] = positions[i].table;
		return tables;
	}

	public boolean isPostJoin()
	{
		return postJoin;
	}

	private void addTable(Table t, String alias, Tree joinConditionTree) throws InterruptedException, SQLException
	{
		QueryPosition[] newPositions = null;
		QueryPosition pos = new QueryPosition();
		pos.index = positions.length;
		pos.originalindex = pos.index;
		if (positions.length == 0)
			newPositions = new QueryPosition[1];
		else
		{
			newPositions = new QueryPosition[positions.length + 1];
			for (int i = 0; i < positions.length; i++)
			{
				if (positions[i].table == t)
					pos.duplicate = true;
				newPositions[i] = positions[i];
			}
		}
		if (!pos.duplicate)
			t.latch.awaitRead();
		pos.table = t;
		pos.alias = alias;
		newPositions[positions.length] = pos;
		positions = newPositions;
		if (joinConditionTree != null)
		{
			JoinCondition jc = null;
			if (joinConditionTree.getChild(1) != null)
				jc = new JoinCondition(joinConditionTree.getType(), joinConditionTree.getChild(1).getChild(0), this, t);
			else
				// An empty join condition (missing the ON clause)
				jc = new JoinCondition(joinConditionTree.getType(), null, this, t);
			pos.joinCondition = jc;
		}
	}

	private String getTableAlias(Tree tableNode)
	{
		for (int i = 0; i < tableNode.getChildCount(); i++)
		{
			Tree fc = tableNode.getChild(i);
			if (fc.getType() == MemgrammarParser.TABLEALIAS)
				return fc.getChild(0).getText();
			if (fc.getType() != MemgrammarParser.SCHEMA)
				return fc.getText();
		}
		return null;
	}

	protected void processFrom(Tree t) throws SQLException, InterruptedException
	{
		Tree from = null;
		for (int i = 0; i < t.getChildCount(); i++)
		{
			if (t.getChild(i).getType() == MemgrammarParser.FROM)
				from = t.getChild(i);
		}
		if (from != null)
		{
			for (int i = 0; i < from.getChildCount(); i++)
			{
				Tree fc = from.getChild(i);
				if (fc.getType() == MemgrammarParser.INNERJOIN || fc.getType() == MemgrammarParser.LEFTOUTERJOIN
						|| fc.getType() == MemgrammarParser.RIGHTOUTERJOIN || fc.getType() == MemgrammarParser.FULLOUTERJOIN)
				{
					Table foundt = null;
					String tableAlias = null;
					if (fc.getChild(0).getType() == MemgrammarParser.SUBQUERY)
					{
						Select subselect;
						try
						{
							subselect = new Select(new Command(-1, c.getClientTimeZone()), db, fc.getChild(0).getChild(0), null, null);
						} catch (InterruptedException e)
						{
							throw new SQLException("Unable to select from table");
						}
						subselect.setCreateAsTempTable(true);
						subselect.setCreateTableAsName("T" + System.currentTimeMillis());
						subselect.process();
						foundt = subselect.getInsertIntoTable();
						tableAlias = fc.getChild(0).getChild(1).getText();
					} else
					{
						String tableName = fc.getChild(0).getText();
						String tableSchema = getTableSchema(fc.getChild(0));
						tableAlias = getTableAlias(fc.getChild(0));
						foundt = db.findTable(tableSchema, tableName);
						if (foundt == null)
						{
							throw new SQLException("Unknown table: " + (tableSchema == null ? "" : tableSchema + ".") + tableName);
						}
					}
					addTable(foundt, tableAlias, fc);
					Set<String> tset = new HashSet<String>();
					if (fc != null && fc.getChildCount() > 1)
					{
						/*
						 * Validate the join condition
						 */
						getJoinTablesOrAliases(fc.getChild(1).getChild(0), tset);
						for (String tname : tset)
						{
							boolean found = false;
							for (QueryPosition qp : positions)
							{
								if (qp.alias != null && tname.equals(qp.alias))
								{
									found = true;
									break;
								} else if (qp.alias == null && tname.equals(qp.table.name))
								{
									found = true;
									break;
								}
							}
							if (!found)
							{
								throw new SQLException("Unknown table in join condition: " + tname);
							}
						}
					}
					if (fc.getChildCount() > 2 && fc.getChild(2).getType() == MemgrammarParser.MAXONE)
						maxOne.add(true);
					else
						maxOne.add(false);
				} else if (fc.getType() == MemgrammarParser.DISTINCTFORKEY)
				{
					for (int j = 0; j < fc.getChildCount(); j++)
					{
						Tree distinctchild = fc.getChild(j);
						Expression exp = Expression.parseExpression(this, distinctchild);
						if (exp.containsAggregate(new AtomicInteger(0)))
							throw new SQLException("Cannot use DISTINCTFORKEY with aggregated columns");
						Set<QueryPosition> expset = new HashSet<QueryPosition>();
						exp.getTables(expset);
						if (expset.size() != 1)
							throw new SQLException("Cannot use DISTINCTFORKEY with any table other than first");
						if (expset.iterator().next() != positions[0])
							throw new SQLException("Cannot use DISTINCTFORKEY with any table other than first");
						distinctForKeyList.add(exp);
					}
				} else
				{
					String tableName = null;
					String tableSchema = null;
					Table foundt = null;
					String tableAlias = null;
					if (fc.getType() == MemgrammarParser.INFOTABLES)
					{
						tableName = "TABLES";
						tableSchema = "INFORMATION_SCHEMA";
						foundt = db.findTable(tableSchema, tableName);
						tableAlias = getTableAlias(fc);
					} else if (fc.getType() == MemgrammarParser.SUBQUERY)
					{
						Select subselect;
						try
						{
							subselect = new Select(new Command(-1, c.getClientTimeZone()), db, fc.getChild(0), null, null);
						} catch (InterruptedException e)
						{
							throw new SQLException("Unable to select from table");
						}
						subselect.setCreateAsTempTable(true);
						subselect.setCreateTableAsName("T" + System.currentTimeMillis());
						subselect.process();
						foundt = subselect.getInsertIntoTable();
						tableAlias = fc.getChild(1).getText();
					} else
					{
						tableName = fc.getText();
						tableSchema = getTableSchema(fc);
						foundt = db.findTable(tableSchema, tableName);
						tableAlias = getTableAlias(fc);
					}
					if (foundt == null)
					{
						throw new SQLException("Unknown table: " + (tableSchema == null ? "" : tableSchema + ".") + tableName);
					}
					addTable(foundt, tableAlias, null);
					maxOne.add(false);
				}
			}
		}
		try
		{
			for (QueryPosition qp : positions)
			{
				if (!qp.duplicate)
					qp.table.reserve();
			}
		} catch (ClassNotFoundException e)
		{
			logger.error(e);
		} catch (IOException e)
		{
			logger.error(e);
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
	}

	protected void releaseTables()
	{
		for (QueryPosition qp : positions)
			try
			{
				if (!qp.duplicate)
				{
					qp.table.latch.releaseRead();
					qp.table.release();
				}
			} catch (IOException e)
			{
				logger.error(e);
			}
	}

	protected void processRows() throws SQLException
	{
		prejointime = System.currentTimeMillis();
		logger.trace(c.getThreadNum() + ": prejoin-\t" + (prejointime - starttime));
		// Produce joined rowset
		joinedrows = new RowSet[positions.length];
		if (positions.length == 1)
		{
			if (positions[0].rowset instanceof RowSet)
				joinedrows[0] = positions[0].rowset;
			else
				joinedrows[0] = new RowSet(new TIntArrayList((TIntSet) positions[0].rowset));
			if (!containsAggregates && !isSort && topn >= 0)
				joinedrows[0].setTopN(topn);
		} else
		{
			int maxRows = 0;
			for (QueryPosition qp : positions)
				if (qp.rowset.size() > maxRows)
					maxRows = qp.rowset.size();
			if (maxRows > 1000000)
				maxRows = 1000000;
			for (int i = 0; i < positions.length; i++)
				joinedrows[i] = new RowSet(new TIntArrayList(maxRows));
			int[] rownums = new int[positions.length];
			RowSet[] rows = new RowSet[positions.length];
			for (int i = 0; i < positions.length; i++)
				rows[i] = positions[i].rowset;
			createJoinedRowset(joinedrows, 0, rownums, rows, null);
		}
		logger.trace(c.getThreadNum() + ": " + joinedrows[0].size() + " joined rows");
		/*
		 * Do post-join filtering
		 */
		postjointime = System.currentTimeMillis();
		logger.trace(c.getThreadNum() + ": postjoin-\t" + (postjointime - prejointime));
		postJoin = true;
		if (whereList != null)
		{
			boolean hasWhere = postJoinWhereList.size() > 0;
			if (hasWhere)
			{
				RowSet[] filteredJoinedRows = new RowSet[joinedrows.length];
				RowSetIterator[] rsit = new RowSetIterator[joinedrows.length];
				for (int j = 0; j < joinedrows.length; j++)
					rsit[j] = joinedrows[j].getIterator();
				for (int j = 0; j < joinedrows.length; j++)
					filteredJoinedRows[j] = new RowSet(new TIntArrayList(joinedrows[0].size()));
				ThreadPool tp = new ThreadPool();
				try
				{
					int numProcessors = Runtime.getRuntime().availableProcessors();
					CountDownLatch cdl = new CountDownLatch(numProcessors);
					for (int i = 0; i < numProcessors; i++)
					{
						ProcessPostJoinFilter ppjf = new ProcessPostJoinFilter(cdl, filteredJoinedRows, rsit);
						tp.executeLocal(ppjf);
					}
					try
					{
						cdl.await();
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
				} finally
				{
					tp.delete();
				}
				joinedrows = filteredJoinedRows;
			}
		}
		logger.trace(c.getThreadNum() + ": " + joinedrows[0].size() + " filtered rows");
	}

	private synchronized int[] getNextRowSet(RowSetIterator[] rsit)
	{
		int[] rowset = new int[joinedrows.length];
		int countValid = 0;
		for (int j = 0; j < rowset.length; j++)
		{
			rowset[j] = rsit[j].getNextRow();
			if (rowset[j] >= 0)
			{
				countValid++;
			}
		}
		if (countValid == 0)
			return null;
		return rowset;
	}

	private class ProcessPostJoinFilter implements Runnable
	{
		RowSet[] filteredJoinedRows;
		CountDownLatch cdl;
		RowSetIterator[] rsit;

		public ProcessPostJoinFilter(CountDownLatch cdl, RowSet[] filteredJoinedRows, RowSetIterator[] rsit)
		{
			this.cdl = cdl;
			this.filteredJoinedRows = filteredJoinedRows;
			this.rsit = rsit;
		}

		@Override
		public void run()
		{
			try
			{
				while (true)
				{
					int[] rowset = getNextRowSet(rsit);
					if (rowset == null)
					{
						return;
					}
					boolean valid = true;
					for (Where w : postJoinWhereList)
					{
						Object o;
						try
						{
							o = w.evaluate(rowset);
						} catch (SQLException e)
						{
							logger.error(e);
							valid = false;
							return;
						}
						if (o != null)
						{
							if (o instanceof Boolean)
								valid = valid && ((Boolean) o);
							else
								valid = false;
						}
					}
					if (valid)
					{
						synchronized (filteredJoinedRows)
						{
							for (int j = 0; j < rowset.length; j++)
								filteredJoinedRows[j].add(rowset[j]);
						}
					}
				}
			} finally
			{
				cdl.countDown();
			}
		}
	}

	private void splitRowSets(RowSet[][] splitRows, RowSet[] rows) throws SQLException
	{
		/*
		 * Split the row sets for threads. You cannot split a table that does not have a two-table join condition. Try
		 * to split the largest rowset. Try to split as early as possible to avoid having to work down the join chain.
		 * But, it's better to split later if there aren't enough rows to split early.
		 */
		int rowSetToSplit = -1;
		boolean okToSplit = false;
		int maxRows = 0;
		for (int i = 0; i < positions.length; i++)
		{
			int numRows = positions[i].rowset.size();
			if (numRows > maxRows)
				maxRows = numRows;
		}
		for (int i = 0; i < positions.length; i++)
		{
			if (i > 0 && positions[i].joinCondition.joinDirection != MemgrammarParser.INNERJOIN)
			{
				/*
				 * Note you can't split anything to the right of a left outer join because you'll end up with dupes
				 */
				break;
			}
			int numRows = positions[i].rowset.size();
			if (numRows >= splitRows.length)
			{
				Set<String> tset = new HashSet<String>();
				if (i > 0)
					getJoinTablesOrAliases(positions[i].joinCondition.conditionTree, tset);
				if (i == 0 || tset.size() > 1)
				{
					rowSetToSplit = i;
					okToSplit = true;
					break;
				}
			}
		}
		if (okToSplit)
		{
			RowSet[] splitRowSets = rows[rowSetToSplit].splitRowSet(splitRows.length);
			for (int i = 0; i < splitRows.length; i++)
			{
				splitRows[i] = new RowSet[rows.length];
				if (splitRowSets[i] == null)
					splitRowSets[i] = new RowSet();
				splitRows[i][rowSetToSplit] = splitRowSets[i];
				for (int j = 0; j < rows.length; j++)
				{
					if (j == rowSetToSplit)
						continue;
					splitRows[i][j] = rows[j];
				}
			}
		} else
		{
			/*
			 * Don't do any splitting
			 */
			splitRows[0] = rows;
		}
	}

	private void createJoinedRowset(RowSet[] joinedrows, int currentTable, int[] rownums, RowSet[] rows, JoinRowInfo[] jrilist) throws SQLException
	{
		if (c.isKilled())
			return;
		if (rows[0] == null)
			return;
		ProcessJoin[] jointhreads = null;
		/*
		 * Create threads for joining
		 */
		jointhreads = new ProcessJoin[Runtime.getRuntime().availableProcessors()];
		/*
		 * If not doing aggregation, can limit the evaluation range of the join
		 */
		RowSet[][] splitRows = new RowSet[jointhreads.length][];
		if (!containsAggregates && !isSort && topn >= 0)
		{
			rows[0].setTopN(topn);
		}
		/*
		 * Split the rowsets to allow for multi-threading
		 */
		splitRowSets(splitRows, rows);
		for (int i = 0; i < splitRows.length; i++)
		{
			if (splitRows[i] == null)
				continue;
			boolean offered = false;
			while (!offered)
			{
				try
				{
					ProcessJoinSplit pjs = new ProcessJoinSplit();
					pjs.startingSet = splitRows[i][0];
					pjs.rows = splitRows[i];
					offered = joinRowQueue.offer(pjs, 30, TimeUnit.SECONDS);
				} catch (InterruptedException e)
				{
					logger.error(e);
				}
			}
		}
		ProcessJoinSplit nullSet = new ProcessJoinSplit();
		for (int i = 0; i < jointhreads.length; i++)
		{
			if (splitRows[i] == null)
				continue;
			jointhreads[i] = new ProcessJoin(this, currentTable);
			jointhreads[i].start();
		}
		for (int i = 0; i < jointhreads.length; i++)
		{
			boolean offered = joinRowQueue.offer(nullSet);
			if (!offered)
				i--;
		}
		for (int i = 0; i < jointhreads.length; i++)
		{
			if (jointhreads[i] == null)
				continue;
			try
			{
				jointhreads[i].join();
			} catch (InterruptedException e)
			{
				logger.error(e);
				e.printStackTrace();
			}
		}
		for (int i = 0; i < jointhreads.length; i++)
		{
			if (jointhreads[i] == null)
				continue;
			SQLException ex = jointhreads[i].getException();
			if (ex != null)
				throw ex;
		}
		int joinsize = 0;
		for (int i = 0; i < jointhreads.length; i++)
		{
			if (jointhreads[i] == null)
				continue;
			RowSet[] thjoinedrows = jointhreads[i].getJoinedrows();
			joinsize += thjoinedrows[0].size();
		}
		for (int j = 0; j < joinedrows.length; j++)
		{
			joinedrows[j] = new RowSet(new TIntArrayList(joinsize));
		}
		for (int i = 0; i < jointhreads.length; i++)
		{
			if (jointhreads[i] == null)
				continue;
			RowSet[] thjoinedrows = jointhreads[i].getJoinedrows();
			for (int j = 0; j < joinedrows.length; j++)
			{
				int size = thjoinedrows[j].size();
				for (int k = 0; k < size; k++)
					joinedrows[j].add(thjoinedrows[j].get(k));
			}
		}
	}

	protected final void evalJoin(int[] rownums, int currentTable, RowSet[] joinedrows, RowSet[] rows, int rownum) throws SQLException
	{
		rownums[currentTable] = rownum;
		if (currentTable == positions.length - 1)
		{
			// If this is the last join condition
			for (int i = 0; i < rownums.length; i++)
			{
				joinedrows[i].add(rownums[i]);
			}
		} else
		{
			RowSet joinRows = null;
			if (positions[currentTable + 1].joinCondition.conditionTree == null)
				joinRows = rows[currentTable + 1];
			else
				joinRows = getJoinRows(currentTable, rownums, rows, positions[currentTable + 1], positions[currentTable + 1].joinCondition.joinrowinfo);
			if (maxOne.get(currentTable + 1) && joinRows != null && joinRows.size() > 1)
			{
				/*
				 * If MAXONE is used, join with at most one row
				 */
				int firstrow = joinRows.getFirstRowNum();
				if (firstrow >= 0)
				{
					TIntArrayList tlist = new TIntArrayList(1);
					tlist.add(firstrow);
					joinRows = new RowSet(tlist);
				}
			}
			// Otherwise, lookup row in next join condition
			if (c.isKilled())
				return;
			if (joinRows == null || joinRows.size() == 0)
			{
				if (positions[currentTable + 1].joinCondition.joinDirection == MemgrammarParser.LEFTOUTERJOIN)
				{
					evalJoin(rownums, currentTable + 1, joinedrows, rows, -1);
				}
				return;
			}
			RowSetIterator rsit = joinRows.getIterator();
			int rnum;
			while (true)
			{
				rnum = rsit.getNextRow();
				if (rnum < 0)
					break;
				evalJoin(rownums, currentTable + 1, joinedrows, rows, rnum);
			}
		}
	}

	private final void getJoinTablesOrAliases(Tree condition, Set<String> tset) throws SQLException
	{
		if (condition.getType() == MemgrammarParser.EQUALS)
		{
			QueryPosition qp = findIdentifierTable(condition.getChild(0));
			String alias = findIdentifierAlias(condition.getChild(0));
			if (qp != null)
				tset.add(alias != null ? alias : qp.table.name);
			qp = findIdentifierTable(condition.getChild(1));
			alias = findIdentifierAlias(condition.getChild(1));
			if (qp != null)
				tset.add(alias != null ? alias : qp.table.name);
		} else if (condition.getType() == MemgrammarParser.ISNULL)
		{
			Table t = findIdentifierTable(condition.getChild(0)).table;
			String alias = findIdentifierAlias(condition.getChild(0));
			if (t != null)
				tset.add(alias != null ? alias : t.name);
		} else if (condition.getType() == MemgrammarParser.AND || condition.getType() == MemgrammarParser.OR)
		{
			getJoinTablesOrAliases(condition.getChild(0), tset);
			getJoinTablesOrAliases(condition.getChild(1), tset);
		} else
		{
			throw new SQLException("Illegal join condition");
		}
	}

	private final boolean isEquiJoinOnPrimaryKey(Tree condition, Table t) throws SQLException
	{
		if (condition.getType() == MemgrammarParser.EQUALS)
		{
			QueryPosition qp = findIdentifierTable(condition.getChild(0));
			if (qp != null && qp.table == t)
			{
				for (int i = 0; i < t.tmd.primarykey.length; i++)
				{
					if (t.tmd.primarykey[i])
					{
						if (condition.getChild(0).getText().equals(t.tmd.columnNames[i]))
							return true;
					}
				}
				return false;
			}
			qp = findIdentifierTable(condition.getChild(1));
			if (qp != null && qp.table == t)
			{
				for (int i = 0; i < t.tmd.primarykey.length; i++)
				{
					if (t.tmd.primarykey[i])
					{
						if (condition.getChild(1).getText().equals(t.tmd.columnNames[i]))
							return true;
					}
				}
				return false;
			}
		}
		return false;
	}

	private final String getJoinRowKey(int currentTable, Tree condition, int[] rowset, Object[] rows, JoinRowInfo jri) throws SQLException
	{
		if (condition.getType() == MemgrammarParser.EQUALS || condition.getType() == MemgrammarParser.ISNULL
				|| condition.getType() == MemgrammarParser.ISNOTNULL)
		{
			Object o = null;
			for (int i = 0; i <= currentTable; i++)
			{
				if (jri.qp1 == null || positions[i].table.name.equals(jri.qp1.table.name))
				{
					if (jri.qp1 == null)
					{
						o = jri.literal1;
					} else if (jri.qp1.table.getNumRows() > 0 && rowset[i] >= 0)
					{
						o = jri.qp1.table.getTableValue(rowset[i], jri.index1);
					}
					if (o == null)
						return (jri.qp2 != null ? jri.qp2.table.name : "") + jri.index2 + "=null";
					else
						return (jri.qp2 != null ? jri.qp2.table.name : "") + jri.index2 + "=" + o.toString();
				} else if (jri.qp2 == null || positions[i].table.name.equals(jri.qp2.table.name))
				{
					if (jri.qp2 == null)
					{
						o = jri.literal2;
					} else if (jri.qp2.table.getNumRows() > 0 && rowset[i] >= 0)
					{
						o = jri.qp2.table.getTableValue(rowset[i], jri.index2);
					}
					if (o == null)
						return (jri.qp1 != null ? jri.qp1.table.name : "") + jri.index1 + "=null";
					else
						return (jri.qp1 != null ? jri.qp1.table.name : "") + jri.index1 + "=" + o.toString();
				}
			}
			return null;
		} else if (condition.getType() == MemgrammarParser.AND)
		{
			return getJoinRowKey(currentTable, condition.getChild(0), rowset, rows, jri.left) + "&"
					+ getJoinRowKey(currentTable, condition.getChild(1), rowset, rows, jri.right);
		} else if (condition.getType() == MemgrammarParser.OR)
		{
			return getJoinRowKey(currentTable, condition.getChild(0), rowset, rows, jri.left) + "|"
					+ getJoinRowKey(currentTable, condition.getChild(1), rowset, rows, jri.right);
		} else
		{
			throw new SQLException("Illegal join condition");
		}
	}
	private Map<String, RowSet> rowSetCache = Collections.synchronizedMap(new HashMap<String, RowSet>());

	private final RowSet getJoinRows(int currentTable, int[] rowset, Object[] rows, QueryPosition qp, JoinRowInfo jri) throws SQLException
	{
		String key = null;
		if (jri.condition.getType() == MemgrammarParser.EQUALS || jri.condition.getType() == MemgrammarParser.ISNULL
				|| jri.condition.getType() == MemgrammarParser.ISNOTNULL)
		{
			Object o = null;
			RowSet toJoinRows = null;
			if (jri.oneTable)
			{
				/*
				 * If there's only one table in the condition, then the result cannot depend on the row, so cache it for
				 * re-use
				 */
				if (rowSetCache.size() > 0)
				{
					key = getJoinRowKey(currentTable, jri.condition, rowset, rows, jri);
					RowSet inexactSet = rowSetCache.get(key);
					if (inexactSet != null)
						/*
						 * Make sure that this rowset is cloned if needed because in joins there may be a need to modify
						 * the rowset
						 */
						return new RowSet(inexactSet, true);
				}
			}
			for (int i = 0; i <= currentTable; i++)
			{
				if (jri.qp1 == null || (positions[i] == jri.qp1 && jri.qp2 != null))
				{
					if (jri.qp1 == null)
					{
						/*
						 * Handle the case of a join where the first operand is a literal
						 */
						o = jri.literal1;
						RowSet joinset = null;
						if (jri.qp2 == positions[currentTable])
						{
							/*
							 * If the literal is for the table before this one, make sure the current row is valid
							 */
							toJoinRows = (RowSet) rows[currentTable + 1];
							joinset = getEqualsRows(jri.index2, o, jri.qp2.table, (RowSet) rows[jri.qp2.index]);
							if (!joinset.contains(rowset[currentTable]))
								return null;
							joinset = toJoinRows;
						} else
						{
							/*
							 * Otherwise if the literal applies to this table, get the rows that match
							 */
							toJoinRows = (RowSet) rows[jri.qp2.index];
							joinset = getEqualsRows(jri.index2, o, jri.qp2.table, (RowSet) rows[jri.qp2.index]);
						}
						if (key == null)
							key = getJoinRowKey(currentTable, jri.condition, rowset, rows, jri);
						rowSetCache.put(key, joinset);
						return joinset;
					} else if (jri.qp1.table.getNumRows() > 0 && rowset[i] >= 0)
					{
						o = jri.qp1.table.getTableValueForJoin(rowset[i], jri.index1);
						toJoinRows = (RowSet) rows[jri.qp2.index];
						if (o != null)
						{
							if (o instanceof Integer)
							{
								if (jri.qp1.table.isReferenceColumn(jri.index1))
								{
									/*
									 * Make sure surrogate key matches key store (lookup value from surrogate key and
									 * get other surrogate key)
									 */
									if (jri.keyStore1 != null && jri.keyStore2 != null && jri.keyStore1 != jri.keyStore2)
									{
										Object val = jri.qp1.table.getSurrogateKeyValue((Integer) o, jri.index1);
										o = jri.qp2.table.getObjectSurrogateKey(val, jri.index2, db.caseSensitive, false);
									}
									/*
									 * If the other column is not a reference column convert
									 */
									if (!jri.qp2.table.isReferenceColumn(jri.index2))
									{
										switch (jri.qp2.table.tmd.dataTypes[jri.index2])
										{
										case Types.INTEGER:
											Object val = jri.qp1.table.getSurrogateKeyValue((Integer) o, jri.index1);
											o = Integer.valueOf(val.toString());
											break;
										case Types.BIGINT:
											val = jri.qp1.table.getSurrogateKeyValue((Integer) o, jri.index1);
											o = Long.valueOf(val.toString());
											break;
										case Types.FLOAT:
											val = jri.qp1.table.getSurrogateKeyValue((Integer) o, jri.index1);
											o = Float.valueOf(val.toString());
											break;
										case Types.DOUBLE:
											val = jri.qp1.table.getSurrogateKeyValue((Integer) o, jri.index1);
											o = Double.valueOf(val.toString());
											break;
										}
									}
								} else if (jri.qp2.table.isReferenceColumn(jri.index2))
								{
									/*
									 * If the other column is not a reference column convert to a non-reference value
									 */
									o = jri.qp2.table.getObjectSurrogateKey(o.toString(), jri.index2, db.caseSensitive, false);
								}
							}
							RowSet joinset = getEqualsRows(jri.index2, o, jri.qp2.table, toJoinRows);
							return joinset;
						}
					}
				} else if (jri.qp2 == null || (positions[i] == jri.qp2 && jri.qp1 != null))
				{
					if (jri.qp2 == null)
					{
						/*
						 * Handle the case of a join where the second operand is a literal
						 */
						o = jri.literal2;
						RowSet joinset = null;
						if (jri.qp1 == positions[currentTable])
						{
							/*
							 * If the literal is for the table before this one, make sure the current row is valid
							 */
							toJoinRows = (RowSet) rows[currentTable + 1];
							joinset = getEqualsRows(jri.index1, o, jri.qp1.table, (RowSet) rows[jri.qp1.index]);
							if (!joinset.contains(rowset[currentTable]))
								return null;
							joinset = toJoinRows;
						} else
						{
							/*
							 * Otherwise if the literal applies to this table, get the rows that match
							 */
							toJoinRows = (RowSet) rows[jri.qp1.index];
							joinset = getEqualsRows(jri.index1, o, jri.qp1.table, (RowSet) rows[jri.qp1.index]);
						}
						if (key == null)
							key = getJoinRowKey(currentTable, jri.condition, rowset, rows, jri);
						rowSetCache.put(key, joinset);
						return joinset;
					} else if (jri.qp2.table.getNumRows() > 0 && rowset[i] >= 0)
					{
						o = jri.qp2.table.getTableValueForJoin(rowset[i], jri.index2);
						toJoinRows = (RowSet) rows[jri.qp1.index];
						if (jri.qp2.table instanceof CalculatedTime)
						{
							/*
							 * Expand out any rowsets for calculated tables on the left side of a join
							 */
							RowSet calcrows = ((CalculatedTime) jri.qp2.table).getRowList(jri.index2, o, true);
							RowSetIterator rsit = calcrows.getIterator();
							int rnum = 0;
							RowSet joinset = new RowSet();
							while ((rnum = rsit.getNextRow()) >= 0)
							{
								o = jri.qp2.table.getTableValueForJoin(rnum, jri.index2);
								if (o != null)
								{
									joinset.addAll(getEqualsRows(jri.index1, o, jri.qp1.table, toJoinRows));
								}
							}
							return joinset;
						} else
						{
							if (o != null)
							{
								if (o instanceof Integer)
								{
									if (jri.qp2.table.isReferenceColumn(jri.index2))
									{
										// Make sure surrogate key matches key store (lookup value from surrogate key
										// and
										// get other surrogate key)
										if (jri.keyStore1 != null && jri.keyStore2 != null && jri.keyStore1 != jri.keyStore2)
										{
											Object val = jri.qp2.table.getSurrogateKeyValue((Integer) o, jri.index2);
											o = jri.qp1.table.getObjectSurrogateKey(val, jri.index1, db.caseSensitive, false);
										}
										/*
										 * If the other column is not a reference column convert
										 */
										if (!jri.qp1.table.isReferenceColumn(jri.index1))
										{
											switch (jri.qp1.table.tmd.dataTypes[jri.index1])
											{
											case Types.INTEGER:
												Object val = jri.qp2.table.getSurrogateKeyValue((Integer) o, jri.index2);
												o = Integer.valueOf(val.toString());
												break;
											case Types.BIGINT:
												val = jri.qp2.table.getSurrogateKeyValue((Integer) o, jri.index2);
												o = Long.valueOf(val.toString());
												break;
											case Types.FLOAT:
												val = jri.qp2.table.getSurrogateKeyValue((Integer) o, jri.index2);
												o = Float.valueOf(val.toString());
												break;
											case Types.DOUBLE:
												val = jri.qp2.table.getSurrogateKeyValue((Integer) o, jri.index2);
												o = Double.valueOf(val.toString());
												break;
											}
										}
									} else if (jri.qp1.table.isReferenceColumn(jri.index1))
									{
										/*
										 * If the other column is not a reference column convert to a non-reference
										 * value
										 */
										o = jri.qp1.table.getObjectSurrogateKey(o.toString(), jri.index1, db.caseSensitive, false);
									}
								}
								RowSet joinset = getEqualsRows(jri.index1, o, jri.qp1.table, toJoinRows);
								return joinset;
							}
						}
					}
				}
			}
			return null;
		} else if (jri.numBooleans > 0)
		{
			if (jri.numBooleans == 1 || jri.numBooleans != jri.numAndBooleans)
			{
				/*
				 * Case where there is one boolean operator, or there are OR conditions
				 */
				RowSet joinset1 = null;
				RowSet joinset2 = null;
				boolean leftvalid = false;
				boolean rightvalid = false;
				if (jri.left.joinExpression != null)
				{
					Object o = jri.left.joinExpression.evaluate(rowset);
					if (o instanceof Boolean)
						leftvalid = ((Boolean) o);
				} else
				{
					joinset1 = getJoinRows(currentTable, rowset, rows, null, jri.left);
					leftvalid = true;
				}
				if (jri.condition.getType() != MemgrammarParser.AND || joinset1 != null)
				{
					if (jri.right.joinExpression != null)
					{
						Object o = jri.right.joinExpression.evaluate(rowset);
						if (o instanceof Boolean)
							rightvalid = ((Boolean) o);
					} else
					{
						joinset2 = getJoinRows(currentTable, rowset, rows, null, jri.right);
						rightvalid = true;
					}
				}
				RowSet joinset = null;
				if (joinset1 != null && joinset2 != null)
				{
					if (jri.condition.getType() == MemgrammarParser.OR)
					{
						RowSet baseSet = joinset1.getBaseSet();
						joinset1.setBaseSet(null);
						joinset2.setBaseSet(null);
						joinset = new RowSet(joinset1, true);
						joinset.addAll(joinset2);
						joinset.setBaseSet(baseSet);
						return joinset;
					} else
					{
						int size1 = joinset1.size();
						int size2 = joinset2.size();
						if (size1 > 15 || size2 > 15)
						{
							/*
							 * If this is a compound join and one side of the join is HIGHLY unselective, then evaluate
							 * that side as a filter as it is going to be much more efficient to evaluate it as a filter
							 * than to do an intersection (note this is really only true for sets that are based on
							 * differencepackedlists as they require expensive lookups).
							 */
							if (size1 > size2 && !joinset1.isFastLookupType())
							{
								if (jri.leftWeakExpression == null)
								{
									synchronized (jri)
									{
										if (jri.leftWeakExpression == null)
											jri.leftWeakExpression = Expression.parseExpression(this, jri.left.condition);
									}
								}
								RowSetIterator rsit = joinset2.getIterator();
								int rnum = 0;
								TIntArrayList newList = new TIntArrayList(joinset2.size());
								while ((rnum = rsit.getNextRow()) >= 0)
								{
									boolean valid = false;
									rowset[currentTable + 1] = rnum;
									Object o = jri.leftWeakExpression.evaluate(rowset);
									if (o instanceof Boolean)
										valid = ((Boolean) o);
									if (valid)
										newList.add(rnum);
								}
								return new RowSet(newList);
							} else if (size1 <= size2 && !joinset2.isFastLookupType())
							{
								if (jri.rightWeakExpression == null)
								{
									synchronized (jri)
									{
										if (jri.rightWeakExpression == null)
											jri.rightWeakExpression = Expression.parseExpression(this, jri.right.condition);
									}
								}
								RowSetIterator rsit = joinset1.getIterator();
								int rnum = 0;
								TIntArrayList newList = new TIntArrayList(joinset1.size());
								while ((rnum = rsit.getNextRow()) >= 0)
								{
									boolean valid = false;
									rowset[currentTable + 1] = rnum;
									Object o = jri.rightWeakExpression.evaluate(rowset);
									if (o instanceof Boolean)
										valid = ((Boolean) o);
									if (valid)
										newList.add(rnum);
								}
								if (newList.size() == 0)
								{
									Object o = positions[0].table.getTableValueForJoin(rowset[0], 1);
									rsit = joinset1.getIterator();
									rnum = 0;
									newList = new TIntArrayList(joinset1.size());
									while ((rnum = rsit.getNextRow()) >= 0)
									{
										boolean valid = false;
										rowset[currentTable + 1] = rnum;
										o = jri.rightWeakExpression.evaluate(rowset);
										if (o instanceof Boolean)
											valid = ((Boolean) o);
										if (valid)
											newList.add(rnum);
									}
								}
								return new RowSet(newList);
							}
						}
						/*
						 * One of the tables is a fast lookup so intersection is fast
						 */
						RowSet baseSet = joinset1.getBaseSet();
						joinset1.setBaseSet(null);
						joinset2.setBaseSet(null);
						joinset = joinset1.getIntersection(joinset2, positions[currentTable].table.getNumRows());
						joinset.setBaseSet(baseSet);
					}
				} else
				{
					if (jri.condition.getType() == MemgrammarParser.OR)
					{
						if (leftvalid || rightvalid)
							return (RowSet) rows[currentTable];
						else
							return RowSet.getEmptySet();
					} else
					{
						if (jri.left.joinExpression != null && joinset2 != null)
						{
							if (leftvalid)
								return joinset2;
							else
								return RowSet.getEmptySet();
						} else if (jri.right.joinExpression != null && joinset1 != null)
						{
							if (rightvalid)
								return joinset1;
							else
								return RowSet.getEmptySet();
						} else
							return RowSet.getEmptySet();
					}
				}
				return joinset;
			} else
			{
				/*
				 * Case where there are more than one and operator - complex join - can process more optimally
				 */
				List<JoinClauseResult> results = new ArrayList<JoinClauseResult>();
				gatherResults(jri, results, currentTable, rowset, rows);
				RowSet joinset = null;
				int len = results.size();
				for (int i = 0; i < len; i++)
				{
					/*
					 * Find the lowest cardinality set to start with.
					 */
					int curlow = Integer.MAX_VALUE;
					int index = 0;
					boolean invalid = false;
					for (int j = 0; j < len; j++)
					{
						if (results.get(j).used)
							continue;
						RowSet jset = results.get(j).joinset;
						if (jset == null)
						{
							if (i > 0)
								continue;
							if (!results.get(j).valid)
							{
								invalid = true;
								break;
							}
						} else if (jset.size() < curlow)
						{
							curlow = results.get(j).size;
							index = j;
						}
					}
					if (invalid)
						return RowSet.getEmptySet();
					results.get(index).used = true;
					if (joinset == null)
						joinset = results.get(index).joinset;
					else
					{
						RowSet nextSet = results.get(index).joinset;
						Expression exp = results.get(index).expression;
						if (exp != null)
						{
							/*
							 * Expression will be null if there is a constant term, in which case ignore this as it must
							 * be valid at this point
							 */
							if (nextSet.isFastLookupType() || results.get(index).size < 15)
							{
								/*
								 * If the next set is a fast lookup or small, do an intersection
								 */
								RowSet baseSet = joinset.getBaseSet();
								joinset.setBaseSet(null);
								if (nextSet != null)
								{
									nextSet.setBaseSet(null);
									joinset = joinset.getIntersection(nextSet, positions[currentTable].table.getNumRows());
								}
								joinset.setBaseSet(baseSet);
								if (joinset.isEmpty())
									return joinset;
							} else
							{
								/*
								 * Otherwise, evaluate the expression
								 */
								RowSetIterator rsit = joinset.getIterator();
								int rnum = 0;
								TIntArrayList newList = new TIntArrayList(joinset.size());
								while ((rnum = rsit.getNextRow()) >= 0)
								{
									boolean valid = false;
									rowset[currentTable + 1] = rnum;
									Object o = exp.evaluate(rowset);
									if (o instanceof Boolean)
										valid = ((Boolean) o);
									if (valid)
										newList.add(rnum);
								}
								joinset.setList(newList);
							}
						}
					}
				}
				return joinset;
			}
		} else
		{
			throw new SQLException("Illegal join condition");
		}
	}

	/**
	 * Gather results for complex joins
	 * 
	 * @param jri
	 * @param results
	 * @param currentTable
	 * @param rownums
	 * @param rows
	 * @throws SQLException
	 */
	private void gatherResults(JoinRowInfo jri, List<JoinClauseResult> results, int currentTable, int[] rowset, Object[] rows) throws SQLException
	{
		if (jri.left.joinExpression != null)
		{
			Object o = jri.left.joinExpression.evaluate(rowset);
			JoinClauseResult result = new JoinClauseResult();
			if (o instanceof Boolean)
				result.valid = ((Boolean) o);
			results.add(result);
		} else if (jri.left.condition.getType() == MemgrammarParser.AND || jri.left.condition.getType() == MemgrammarParser.OR)
		{
			gatherResults(jri.left, results, currentTable, rowset, rows);
		} else
		{
			JoinClauseResult result = new JoinClauseResult();
			result.joinset = getJoinRows(currentTable, rowset, rows, null, jri.left);
			if (result.joinset != null)
			{
				result.size = result.joinset.size();
				if (jri.leftWeakExpression == null)
				{
					synchronized (jri)
					{
						if (jri.leftWeakExpression == null)
							jri.leftWeakExpression = Expression.parseExpression(this, jri.left.condition);
					}
				}
				result.expression = jri.leftWeakExpression;
			}
			results.add(result);
		}
		if (jri.right.joinExpression != null)
		{
			Object o = jri.right.joinExpression.evaluate(rowset);
			JoinClauseResult result = new JoinClauseResult();
			if (o instanceof Boolean)
				result.valid = ((Boolean) o);
			results.add(result);
		} else if (jri.right.condition.getType() == MemgrammarParser.AND || jri.right.condition.getType() == MemgrammarParser.OR)
		{
			gatherResults(jri.right, results, currentTable, rowset, rows);
		} else
		{
			JoinClauseResult result = new JoinClauseResult();
			result.joinset = getJoinRows(currentTable, rowset, rows, null, jri.right);
			if (result.joinset != null)
			{
				result.size = result.joinset.size();
				if (jri.rightWeakExpression == null)
				{
					synchronized (jri)
					{
						if (jri.rightWeakExpression == null)
							jri.rightWeakExpression = Expression.parseExpression(this, jri.right.condition);
					}
				}
				result.expression = jri.rightWeakExpression;
			}
			results.add(result);
		}
	}

	public RowSet getEqualsRows(int lookupIndex, Object value, Table table, RowSet toJoinRows)
	{
		if (lookupIndex < 0)
		{
			return null;
		}
		RowSet rows = null;
		if (lookupIndex >= 0)
		{
			if (value != null)
				rows = table.getRowListForJoin(lookupIndex, value, toJoinRows);
			else
			{
				/*
				 * Find all null rows
				 */
				TIntArrayList rlist = new TIntArrayList();
				for (int i = 0; i < table.getNumRows(); i++)
				{
					Object o = table.getTableValue(i, lookupIndex);
					if (o == null)
						rlist.add(i);
				}
				rows = new RowSet(rlist);
			}
		}
		return rows;
	}

	protected final Table findTableFromNameOrAlias(String alias)
	{
		for (int i = 0; i < positions.length; i++)
		{
			if (positions[i].alias != null && positions[i].alias.equals(alias))
				return positions[i].table;
			if (positions[i].table.name.equals(alias))
				return positions[i].table;
		}
		return null;
	}

	public int rowsProcessed()
	{
		return rowsProcessed;
	}

	public Object[] getParameters()
	{
		return parameters;
	}

	public Object getNextParameter()
	{
		if (parameters != null && parameterIndex < parameters.length)
			return parameters[parameterIndex++];
		return null;
	}

	public Command getCommand()
	{
		return c;
	}

	public Map<Integer, List<Expression>> getExpressionCache()
	{
		return expressionCache;
	}
}
