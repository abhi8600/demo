package memdb.sql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.expressions.Expression;
import memdb.expressions.UnsupportedException;
import memdb.rowset.RowSet;

import org.antlr.runtime.tree.Tree;

public class Where
{
	private Tree t;
	private Expression exp;
	private WhereStatement wstmt;

	public Where(WhereStatement wstmt, Tree t) throws SQLException
	{
		this.wstmt = wstmt;
		this.t = t;
		exp = Expression.parseExpression(wstmt, t);
	}

	public void reParse() throws SQLException
	{
		exp = Expression.parseExpression(wstmt, t);
	}

	public static List<Where> parseWhere(WhereStatement wstmt, Tree t) throws SQLException
	{
		if (t == null)
			return null;
		List<Where> whereList = new ArrayList<Where>();
		parseWhere(wstmt, t.getChild(0), whereList);
		return whereList;
	}

	private static void parseWhere(WhereStatement wstmt, Tree t, List<Where> whereList) throws SQLException
	{
		if (t.getType() == MemgrammarParser.PAREN)
		{
			parseWhere(wstmt, t.getChild(0), whereList);
		} else if (t.getType() == MemgrammarParser.AND)
		{
			parseWhere(wstmt, t.getChild(0), whereList);
			parseWhere(wstmt, t.getChild(1), whereList);
		} else
		{
			Where w = getWhere(wstmt, t);
			boolean match = false;
			for (Where existingw : whereList)
			{
				if (w.equals(existingw))
				{
					match = true;
					break;
				}
			}
			if (!match)
				whereList.add(w);
		}
	}

	private static Where getWhere(WhereStatement wstmt, Tree t) throws SQLException
	{
		if (t.getType() == MemgrammarParser.PAREN)
			return getWhere(wstmt, t.getChild(0));
		return new Where(wstmt, t);
	}

	public static boolean isPredicate(Tree t)
	{
		if (t.getType() == MemgrammarParser.EQUALS || t.getType() == MemgrammarParser.NOTEQUALS || t.getType() == MemgrammarParser.LT
				|| t.getType() == MemgrammarParser.LTEQ || t.getType() == MemgrammarParser.GT || t.getType() == MemgrammarParser.GTEQ)
			return true;
		return false;
	}

	public RowSet getBaseRows(RowSet baseset) throws SQLException, UnsupportedException
	{
		if (exp != null)
			return exp.getBaseRows(baseset);
		return null;
	}

	public boolean isBaseRowsParallel()
	{
		if (exp != null)
			return exp.isBaseRowsParallel();
		return false;
	}

	public Object evaluate(int[] rowset) throws SQLException
	{
		if (exp != null)
			return exp.evaluate(rowset);
		return null;
	}

	public void getTables(Set<QueryPosition> tableSet)
	{
		if (exp != null)
			exp.getTables(tableSet);
	}

	public boolean containsAggregate(AtomicInteger acount)
	{
		if (exp != null)
			exp.containsAggregate(acount);
		return false;
	}

	public String toString()
	{
		return t.toStringTree();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Where)
		{
			Where w = (Where) obj;
			return equalTree(this.t, w.t);
		}
		return super.equals(obj);
	}

	private boolean equalTree(Tree tree1, Tree tree2)
	{
		if (tree1.getType() != tree2.getType())
			return false;
		if (!tree1.getText().toLowerCase().equals(tree2.getText().toLowerCase()))
			return false;
		if (tree1.getChildCount() != tree2.getChildCount())
			return false;
		for (int i = 0; i < tree1.getChildCount(); i++)
		{
			if (!equalTree(tree1.getChild(i), tree2.getChild(i)))
				return false;
		}
		return true;
	}
}
