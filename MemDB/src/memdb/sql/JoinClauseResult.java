package memdb.sql;

import memdb.expressions.Expression;
import memdb.rowset.RowSet;

class JoinClauseResult
{
	public boolean valid;
	public RowSet joinset;
	public int size;
	public boolean used;
	public Expression expression;
}