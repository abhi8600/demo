package memdb.sql;

import gnu.trove.list.array.TIntArrayList;

import java.sql.SQLException;

import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;

class ProcessJoin extends Thread
{
	/**
	 * 
	 */
	private WhereStatement wstmt;
	private int[] rownums;
	private int currentTable;
	private RowSet[] joinedrows;
	@SuppressWarnings("unused")
	private JoinRowInfo[] jrilist;
	private SQLException ex = null;

	public RowSet[] getJoinedrows()
	{
		return joinedrows;
	}

	public ProcessJoin(WhereStatement wstmt, int currentTable)
	{
		this.wstmt = wstmt;
		this.rownums = new int[this.wstmt.positions.length];
		this.currentTable = currentTable;
		this.joinedrows = new RowSet[this.wstmt.positions.length];
		for (int i = 0; i < this.wstmt.positions.length; i++)
		{
			joinedrows[i] = new RowSet(new TIntArrayList());
		}
	}

	public void run()
	{
		while (true)
		{
			try
			{
				ProcessJoinSplit pjs = this.wstmt.joinRowQueue.take();
				RowSet rowobj = pjs.startingSet;
				if (rowobj == null)
					return;
				RowSetIterator rsit = rowobj.getIterator();
				while (true)
				{
					int rownum = rsit.getNextRow();
					if (rownum < 0)
						break;
					this.wstmt.evalJoin(rownums, currentTable, joinedrows, pjs.rows, rownum);
				}
			} catch (InterruptedException e)
			{
				Select.logger.error(e);
				continue;
			} catch (SQLException e)
			{
				ex = e;
				return;
			}
		}
	}

	public SQLException getException()
	{
		return ex;
	}
}