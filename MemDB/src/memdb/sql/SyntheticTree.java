package memdb.sql;

import java.util.List;

import org.antlr.runtime.tree.Tree;

public class SyntheticTree implements Tree
{
	private String text;
	private Tree child;
	private int type;

	public SyntheticTree(String text, int type)
	{
		this.text = text;
		this.type = type;
	}

	@Override
	public void addChild(Tree child)
	{
		this.child = child;
	}

	@Override
	public Object deleteChild(int arg0)
	{
		return null;
	}

	@Override
	public Tree dupNode()
	{
		return null;
	}

	@Override
	public void freshenParentAndChildIndexes()
	{
	}

	@Override
	public Tree getAncestor(int arg0)
	{
		return null;
	}

	@Override
	public List getAncestors()
	{
		return null;
	}

	@Override
	public int getCharPositionInLine()
	{
		return 0;
	}

	@Override
	public Tree getChild(int arg0)
	{
		return child;
	}

	@Override
	public int getChildCount()
	{
		return child != null ? 1 : 0;
	}

	@Override
	public int getChildIndex()
	{
		return 0;
	}

	@Override
	public int getLine()
	{
		return 0;
	}

	@Override
	public Tree getParent()
	{
		return null;
	}

	@Override
	public String getText()
	{
		return text;
	}

	@Override
	public int getTokenStartIndex()
	{
		return 0;
	}

	@Override
	public int getTokenStopIndex()
	{
		return 0;
	}

	@Override
	public int getType()
	{
		return type;
	}

	@Override
	public boolean hasAncestor(int arg0)
	{
		return false;
	}

	@Override
	public boolean isNil()
	{
		return false;
	}

	@Override
	public void replaceChildren(int arg0, int arg1, Object arg2)
	{
	}

	@Override
	public void setChild(int position, Tree child)
	{
		this.child = child;
	}

	@Override
	public void setChildIndex(int arg0)
	{
	}

	@Override
	public void setParent(Tree arg0)
	{
	}

	@Override
	public void setTokenStartIndex(int arg0)
	{
	}

	@Override
	public void setTokenStopIndex(int arg0)
	{
	}

	@Override
	public String toStringTree()
	{
		return null;
	}
}
