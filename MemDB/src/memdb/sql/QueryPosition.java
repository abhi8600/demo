package memdb.sql;

import memdb.rowset.RowSet;
import memdb.storage.Table;

public class QueryPosition
{
	public int index;
	public int originalindex;
	public Table table;
	public String alias;
	public JoinCondition joinCondition;
	public RowSet rowset;
	public double filterAmount;
	public boolean duplicate;

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
		QueryPosition newqp = new QueryPosition();
		newqp.index = index;
		newqp.originalindex = originalindex;
		newqp.table = table;
		newqp.alias = alias;
		newqp.joinCondition = joinCondition;
		newqp.rowset = rowset;
		newqp.filterAmount = filterAmount;
		newqp.duplicate = duplicate;
		return newqp;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof QueryPosition)
		{
			QueryPosition qp = (QueryPosition) obj;
			if (alias == null && qp.alias == null)
			{
			} else if (alias != null && qp.alias != null)
			{
				return qp.alias.equals(alias);
			} else
				return qp.table == table;
		}
		return super.equals(obj);
	}
}
