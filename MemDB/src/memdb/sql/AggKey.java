package memdb.sql;

public class AggKey
{
	private byte[] key;

	public AggKey(byte[] buff, int length)
	{
		key = new byte[length];
		for (int i = 0; i < length; i++)
			key[i] = buff[i];
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < key.length; i++)
			sb.append("[" + key[i] + "]");
		return sb.toString();
	}

	@Override
	public int hashCode()
	{
		int result = 0;
		for (int i = 0; i < key.length; i++)
		{
			int byteno = i % 4;
			switch (byteno)
			{
			case 0:
				result = result ^ key[i];
				break;
			case 1:
				result = result ^ (key[i] << 8);
				break;
			case 2:
				result = result ^ (key[i] << 16);
				break;
			case 3:
				result = result ^ (key[i] << 24);
				break;
			}
		}
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof AggKey)
		{
			AggKey key2 = (AggKey) obj;
			if (key.length != key2.key.length)
				return false;
			for (int i = 0; i < key.length; i++)
				if (key[i] != key2.key[i])
					return false;
			return true;
		}
		return super.equals(obj);
	}
}
