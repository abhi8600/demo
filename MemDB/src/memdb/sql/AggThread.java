package memdb.sql;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.expressions.Expression;
import memdb.rowset.RowSetIterator;

import org.apache.log4j.Logger;

class AggThread implements Runnable
{
	/**
	 * 
	 */
	static Logger logger = Logger.getLogger(AggThread.class);
	public SQLException ex;
	private Select select;
	private int childCount;
	private int obCount;
	private List<Expression> colList;
	private List<Expression> orderByList;
	private int aggSplit;
	private Having having;
	private boolean[] containsAggregate;
	private boolean[] containsObAggregate;
	private RowSetIterator[] rsit;
	private byte[] buff = new byte[1024 * 20];
	private int numAggregations;
	private Map<AggKey, QueryResultRow>[] valMap;
	private StreamingResultSet valList;
	private AtomicInteger threadsRunning;

	public AggThread(int aggSplit, RowSetIterator[] rsit, Select select, int childCount, int obCount, List<Expression> colList, List<Expression> orderByList,
			Map<AggKey, QueryResultRow>[] valMap, StreamingResultSet valList, Having having, boolean[] containsAggregate, boolean[] containsObAggregate,
			int numAggregations, AtomicInteger threadsRunning)
	{
		this.aggSplit = aggSplit;
		this.rsit = rsit;
		this.select = select;
		this.childCount = childCount;
		this.obCount = obCount;
		this.colList = colList;
		this.orderByList = orderByList;
		this.having = having;
		this.containsAggregate = containsAggregate;
		this.containsObAggregate = containsObAggregate;
		this.numAggregations = numAggregations;
		this.valMap = valMap;
		this.valList = valList;
		this.threadsRunning = threadsRunning;
	}

	private int[] getNextRowNum()
	{
		if (this.select.c.isKilled())
			return null;
		int[] result = new int[rsit.length];
		int negcount = 0;
		for (int i = 0; i < rsit.length; i++)
		{
			result[i] = rsit[i].getNextRow();
			if (result[i] < 0)
				negcount++;
		}
		if (negcount == rsit.length)
			return null;
		return result;
	}

	public void run()
	{
		while (true)
		{
			int[] rowset = getNextRowNum();
			if (rowset == null)
			{
				break;
			}
			int index = 0;
			try
			{
				Object[] currow = new Object[childCount + 1];
				currow[childCount] = rowset;
				Object[] obrow = new Object[obCount];
				if (valMap != null)
				{
					// Get aggregation key
					for (int j = 0; j < colList.size(); j++)
					{
						Expression exp = colList.get(j);
						if (containsAggregate[j])
							continue;
						if (exp instanceof memdb.expressions.Identifier)
							// Optimization to just use surrogate keys for the sake of the agg key
							currow[j] = ((memdb.expressions.Identifier) exp).evaluateAggKey(rowset);
						else
							currow[j] = exp.evaluate(rowset);
						Object o = currow[j];
						if (o instanceof Date)
							o = ((Date) o).getTime();
						else if ((o instanceof Double))
							o = Double.doubleToLongBits((Double) o);
						if (o instanceof Integer)
						{
							int into = (Integer) o;
							byte byte4 = (byte) ((into & 0xFF000000L) >>> 24);
							byte byte3 = (byte) ((into & 0x00FF0000L) >>> 16);
							byte byte2 = (byte) ((into & 0x0000FF00L) >>> 8);
							byte byte1 = (byte) ((into & 0x000000FFL));
							buff[index++] = byte4;
							buff[index++] = byte3;
							buff[index++] = byte2;
							buff[index++] = byte1;
						} else if (o instanceof Long)
						{
							long l = (Long) o;
							byte byte1 = (byte) ((l & 0x00000000000000FFL));
							byte byte2 = (byte) ((l & 0x000000000000FF00L) >>> 8);
							byte byte3 = (byte) ((l & 0x0000000000FF0000L) >>> 16);
							byte byte4 = (byte) ((l & 0x00000000FF000000L) >>> 24);
							byte byte5 = (byte) ((l & 0x000000FF00000000L) >>> 32);
							byte byte6 = (byte) ((l & 0x0000FF0000000000L) >>> 40);
							byte byte7 = (byte) ((l & 0x00FF000000000000L) >>> 48);
							byte byte8 = (byte) ((l & 0xFF00000000000000L) >>> 56);
							buff[index++] = byte8;
							buff[index++] = byte7;
							buff[index++] = byte6;
							buff[index++] = byte5;
							buff[index++] = byte4;
							buff[index++] = byte3;
							buff[index++] = byte2;
							buff[index++] = byte1;
						} else if (o == null)
							buff[index++] = (byte) 0;
						else
						{
							byte[] blist = o.toString().getBytes();
							for (int i = 0; i < blist.length; i++)
								buff[index++] = blist[i];
						}
					}
					for (int j = 0; j < orderByList.size(); j++)
					{
						Expression exp = orderByList.get(j);
						if (containsObAggregate[j])
							continue;
						Object o = null;
						if (exp instanceof memdb.expressions.Identifier)
							// Optimization to just use surrogate keys for the sake of the agg key
							o = ((memdb.expressions.Identifier) exp).evaluateAggKey(rowset);
						else
							o = exp.evaluate(rowset);
						obrow[j] = o;
						if (o instanceof Date)
							o = ((Date) o).getTime();
						else if ((o instanceof Double))
							o = Double.doubleToLongBits((Double) o);
						if (o instanceof Integer)
						{
							int into = (Integer) o;
							byte byte4 = (byte) ((into & 0xFF000000L) >>> 24);
							byte byte3 = (byte) ((into & 0x00FF0000L) >>> 16);
							byte byte2 = (byte) ((into & 0x0000FF00L) >>> 8);
							byte byte1 = (byte) ((into & 0x000000FFL));
							buff[index++] = byte4;
							buff[index++] = byte3;
							buff[index++] = byte2;
							buff[index++] = byte1;
						} else if (o instanceof Long)
						{
							long l = (Long) o;
							byte byte1 = (byte) ((l & 0x00000000000000FFL));
							byte byte2 = (byte) ((l & 0x000000000000FF00L) >>> 8);
							byte byte3 = (byte) ((l & 0x0000000000FF0000L) >>> 16);
							byte byte4 = (byte) ((l & 0x00000000FF000000L) >>> 24);
							byte byte5 = (byte) ((l & 0x000000FF00000000L) >>> 32);
							byte byte6 = (byte) ((l & 0x0000FF0000000000L) >>> 40);
							byte byte7 = (byte) ((l & 0x00FF000000000000L) >>> 48);
							byte byte8 = (byte) ((l & 0xFF00000000000000L) >>> 56);
							buff[index++] = byte8;
							buff[index++] = byte7;
							buff[index++] = byte6;
							buff[index++] = byte5;
							buff[index++] = byte4;
							buff[index++] = byte3;
							buff[index++] = byte2;
							buff[index++] = byte1;
						} else if (o == null)
							buff[index++] = (byte) 0;
						else
						{
							byte[] blist = o.toString().getBytes();
							for (int i = 0; i < blist.length; i++)
								buff[index++] = blist[i];
						}
					}
					AggKey key = new AggKey(buff, index);
					QueryResultRow qrr = null;
					QueryResultRow newResultRow = new QueryResultRow();
					qrr = valMap[aggSplit].get(key);
					if (qrr == null)
					{
						qrr = newResultRow;
						if (numAggregations > 0)
							qrr.aggRow = new Object[numAggregations];
						qrr.dataRow = currow;
						// Populate surrogate key elements (per optimization earlier to just use surrogate key for agg
						// key)
						for (int j = 0; j < colList.size(); j++)
						{
							Expression exp = colList.get(j);
							if (containsAggregate[j])
								continue;
							if (exp instanceof memdb.expressions.Identifier)
								currow[j] = exp.evaluate(rowset);
						}
						qrr.sortRow = obrow;
						// Populate surrogate key elements (per optimization earlier to just use surrogate key for agg
						// key)
						for (int j = 0; j < orderByList.size(); j++)
						{
							Expression exp = orderByList.get(j);
							if (containsAggregate[j])
								continue;
							if (exp instanceof memdb.expressions.Identifier)
								obrow[j] = exp.evaluate(rowset);
						}
						valMap[aggSplit].put(key, qrr);
					}
					// Accumulate aggregations
					for (int j = 0; j < colList.size(); j++)
					{
						Expression exp = colList.get(j);
						if (!containsAggregate[j])
							continue;
						exp.aggregate(qrr.aggRow, rowset);
					}
					if (having != null)
					{
						having.aggregate(qrr.aggRow, rowset);
					}
					for (int j = 0; j < orderByList.size(); j++)
					{
						Expression exp = orderByList.get(j);
						if (!containsObAggregate[j])
							continue;
						exp.aggregate(qrr.aggRow, rowset);
					}
				} else
				{
					/*
					 * If not aggregating, just record the row results
					 */
					QueryResultRow qrr = new QueryResultRow();
					for (int j = 0; j < colList.size(); j++)
					{
						Expression exp = colList.get(j);
						currow[j] = exp.evaluate(rowset);
					}
					for (int j = 0; j < orderByList.size(); j++)
					{
						Expression exp = orderByList.get(j);
						obrow[j] = exp.evaluate(rowset);
					}
					// Save all the computed key values
					qrr.dataRow = currow;
					qrr.sortRow = obrow;
					valList.addRow(qrr);
				}
			} catch (SQLException e)
			{
				this.ex = e;
				break;
			}
		}
		if (threadsRunning.decrementAndGet() == 0)
		{
			if (valList != null)
			{
				valList.setNoMoreRows();
			} else
			{
				synchronized (valMap)
				{
					valMap.notifyAll();
				}
			}
		}
	}
}