package memdb.sql;

import java.sql.SQLException;

import memdb.DateParser;
import memdb.expressions.Expression;
import memdb.storage.SurrogateKeyStore;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;

public class JoinRowInfo
{
	public Tree condition;
	public QueryPosition qp1;
	public QueryPosition qp2;
	public int index1;
	public int index2;
	public Object literal1;
	public Object literal2;
	public SurrogateKeyStore keyStore1;
	public SurrogateKeyStore keyStore2;
	public boolean oneTable;
	public JoinRowInfo left;
	public JoinRowInfo right;
	public Expression joinExpression;
	public Expression leftWeakExpression;
	public Expression rightWeakExpression;
	public Table tableAtPosition;
	public int numBooleans;
	public int numAndBooleans;
	private DateParser dp = new DateParser();
	private WhereStatement wstmt;

	public JoinRowInfo(WhereStatement wstmt, Tree conditionTree, Table tableAtPosition) throws SQLException
	{
		this.wstmt = wstmt;
		if (conditionTree == null)
			return;
		this.tableAtPosition = tableAtPosition;
		condition = conditionTree;
		if (condition.getType() == MemgrammarParser.AND || condition.getType() == MemgrammarParser.OR)
		{
			left = new JoinRowInfo(wstmt, condition.getChild(0), tableAtPosition);
			right = new JoinRowInfo(wstmt, condition.getChild(1), tableAtPosition);
			countOperations(this);
			return;
		} else
		{
			left = null;
			right = null;
		}
		String clause1 = null;
		String clause2 = null;
		if (conditionTree.getType() == MemgrammarParser.ISNULL || conditionTree.getType() == MemgrammarParser.ISNOTNULL)
		{
			clause1 = conditionTree.getChild(0).getText();
			literal1 = null;
		} else
		{
			clause1 = condition.getChild(0).getText();
			clause2 = condition.getChild(1).getText();
			literal1 = getLiteral(condition.getChild(0));
			literal2 = getLiteral(condition.getChild(1));
		}
		if (literal1 == null && condition.getChild(0).getType() != MemgrammarParser.NULL)
		{
			qp1 = wstmt.findIdentifierTable(condition.getChild(0));
			if (qp1 == null)
				throw new SQLException("Unknown table: " + condition.getChild(0).getText());
			index1 = qp1.table.tmd.getColumnIndex(clause1);
			keyStore1 = qp1.table.getKeyStore();
			if (index1 < 0)
				throw new SQLException("Unknown column: " + clause1);
		}
		if (clause2 != null && literal2 == null && condition.getChild(1).getType() != MemgrammarParser.NULL)
		{
			qp2 = wstmt.findIdentifierTable(conditionTree.getChild(1));
			if (qp2 == null)
				throw new SQLException("Unknown table: " + condition.getChild(1).getText());
			index2 = qp2.table.tmd.getColumnIndex(clause2);
			keyStore2 = qp2.table.getKeyStore();
			if (index2 < 0)
				throw new SQLException("Unknown column: " + clause2);
		}
		if (qp1 == null && qp2 == null)
			throw new SQLException("Invalid join condition: " + clause1 + "=" + clause2);
		oneTable = (qp1 != null ? 1 : 0) + (qp2 != null ? 1 : 0) <= 1;
		if ((qp1 != null && qp1.table != tableAtPosition) && (qp2 != null && qp2.table != tableAtPosition))
		{
			/*
			 * If this join condition does not include the table at this position, then it cannot be used in
			 * createjoinedrowset which always assumes that one of the tables is present. This join condition must be a
			 * condition on previous tables in the query and as a result, this condition should be treated like a filter
			 */
			joinExpression = Expression.parseExpression(wstmt, condition);
		}
	}

	/**
	 * Count the number and type of clauses
	 * 
	 * @param jri
	 * @param result
	 */
	private void countOperations(JoinRowInfo jri)
	{
		if (jri.left != null)
		{
			countOperations(jri.left);
		}
		if (jri.right != null)
		{
			countOperations(jri.right);
		}
		if (jri.condition.getType() == MemgrammarParser.AND || jri.condition.getType() == MemgrammarParser.OR)
		{
			numBooleans++;
			if (jri.condition.getType() == MemgrammarParser.AND)
				numAndBooleans++;
		}
	}

	private Object getLiteral(Tree t)
	{
		int type = t.getType();
		switch (type)
		{
		case MemgrammarParser.INTEGER:
			return Integer.valueOf(t.getText());
		case MemgrammarParser.FLOAT:
			return Double.valueOf(t.getText());
		case MemgrammarParser.STRING:
			return memdb.util.Util.getString(t.getText());
		case MemgrammarParser.DATETIME:
			return dp.parseUsingPossibleFormats(t.getText(), wstmt.getCommand().getClientTimeZone());
		case MemgrammarParser.BOOLEAN:
			return Boolean.valueOf(t.getText());
		case MemgrammarParser.NULL:
			return null;
		}
		return null;
	}
}