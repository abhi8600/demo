package memdb.sql;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import memdb.Command;
import memdb.Database;
import memdb.DateParser;
import memdb.expressions.Expression;
import memdb.rowset.RowSetIterator;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;

public class Update extends WhereStatement
{
	private List<Expression> updateExpressionList = new ArrayList<Expression>();
	private List<Table> updateTableList = new ArrayList<Table>();
	private List<Integer> columnIndexList = new ArrayList<Integer>();
	private int numExpressions = 0;

	public Update(Command c, Database db, Tree t, Object[] parameters) throws SQLException, InterruptedException
	{
		super(c, db, t, parameters);
		Tree setlist = t.getChild(0);
		for (int i = 0; i < setlist.getChildCount(); i++)
		{
			Tree column = setlist.getChild(i).getChild(0);
			boolean found = false;
			for (int j = 0; j < positions.length; j++)
			{
				/*
				 * Because the query positions may have been reordered for optimization, start with the original order.
				 * For updates, the default table should be the first one listed.
				 */
				for (int k = 0; k < positions.length; k++)
				{
					if (positions[k].originalindex != j)
						continue;
					Table curt = positions[k].table;
					if (column.getChildCount() > 0)
					{
						curt = findTableFromNameOrAlias(column.getChild(0).getText());
						if (curt == null)
							throw new SQLException("Table not found: " + column.getChild(0).getText());
					}
					int index = curt.tmd.getColumnIndex(column.getText());
					if (index < 0)
						continue;
					found = true;
					updateTableList.add(curt);
					columnIndexList.add(index);
					updateExpressionList.add(Expression.parseExpression(this, setlist.getChild(i).getChild(1)));
					break;
				}
			}
			if (!found)
				throw new SQLException("Column not found: " + column.getText());
		}
	}

	private class UpdateRow
	{
		int[] rowset;
		int joinedTableIndex;
		int updateExpressionIndex;
		int colIndex;
	}

	public void update() throws SQLException
	{
		try
		{
			processRows();
			if (joinedrows == null || joinedrows.length == 0)
				return;
			numExpressions = updateTableList.size();
			LinkedBlockingQueue<UpdateRow> rowsToProcess = new LinkedBlockingQueue<UpdateRow>(1000);
			UpdateRowClass[] updateThreads = new UpdateRowClass[Runtime.getRuntime().availableProcessors()];
			for (int i = 0; i < updateThreads.length; i++)
			{
				updateThreads[i] = new UpdateRowClass(rowsToProcess);
				updateThreads[i].start();
			}
			Set<Table> updatedTables = new HashSet<Table>();
			for (int joinedTableIndex = 0; joinedTableIndex < joinedrows.length; joinedTableIndex++)
			{
				try
				{
					positions[joinedTableIndex].table.latch.awaitWrite();
					for (int updateExpressionIndex = 0; updateExpressionIndex < numExpressions; updateExpressionIndex++)
					{
						if (positions[joinedTableIndex].table != updateTableList.get(updateExpressionIndex))
							continue;
						RowSetIterator[] rsit = new RowSetIterator[joinedrows.length];
						for (int j = 0; j < joinedrows.length; j++)
							rsit[j] = joinedrows[j].getIterator();
						int[] numColumns = new int[positions.length];
						for (int i = 0; i < numColumns.length; i++)
						{
							numColumns[i] = positions[i].table.getNumColumns();
						}
						while (true)
						{
							int[] rowset = new int[joinedrows.length];
							int countRows = 0;
							for (int l = 0; l < rowset.length; l++)
							{
								rowset[l] = rsit[l].getNextRow();
								if (rowset[l] >= 0)
									countRows++;
							}
							if (countRows == 0)
								break;
							if (c.isKilled())
								break;
							rowsProcessed++;
							updatedTables.add(positions[joinedTableIndex].table);
							UpdateRow ur = new UpdateRow();
							ur.rowset = rowset;
							ur.joinedTableIndex = joinedTableIndex;
							ur.updateExpressionIndex = updateExpressionIndex;
							ur.colIndex = columnIndexList.get(updateExpressionIndex);
							rowsToProcess.put(ur);
						}
					}
				} catch (InterruptedException e)
				{
					logger.error(e);
				} finally
				{
					positions[joinedTableIndex].table.latch.releaseWrite();
				}
			}
			for (int i = 0; i < updateThreads.length; i++)
			{
				/*
				 * Add entries with null values to mark end of queue
				 */
				UpdateRow ur = new UpdateRow();
				try
				{
					rowsToProcess.put(ur);
				} catch (InterruptedException e)
				{
					logger.error(e);
					return;
				}
			}
			for (int i = 0; i < updateThreads.length; i++)
			{
				try
				{
					updateThreads[i].join();
				} catch (InterruptedException e)
				{
					logger.error(e);
					continue;
				}
			}
			for (Table t : updatedTables)
			{
				t.clearCache();
				t.pack();
			}
		} finally
		{
			releaseTables();
		}
	}

	private class UpdateRowClass extends Thread
	{
		LinkedBlockingQueue<UpdateRow> rowsToUpdate;

		public UpdateRowClass(LinkedBlockingQueue<UpdateRow> rowsToUpdate)
		{
			this.rowsToUpdate = rowsToUpdate;
		}

		@Override
		public void run()
		{
			while (true)
			{
				UpdateRow ur;
				try
				{
					ur = rowsToUpdate.take();
				} catch (InterruptedException e)
				{
					logger.error(e);
					return;
				}
				if (ur.rowset == null)
					return;
				try
				{
					Object newValue;
					newValue = updateExpressionList.get(ur.updateExpressionIndex).evaluate(ur.rowset);
					int type = positions[ur.joinedTableIndex].table.tmd.dataTypes[ur.colIndex];
					if (type == Types.DATE || type == Types.TIMESTAMP || type == Types.TIME)
					{
						if (newValue instanceof String)
						{
							newValue = (new DateParser()).parseUsingPossibleFormats((String) newValue, c.getClientTimeZone());
						}
					}
					positions[ur.joinedTableIndex].table.updateRow(ur.rowset[ur.joinedTableIndex], ur.colIndex, newValue);
				} catch (SQLException ex)
				{
					/*
					 * This should likely be an error
					 */
				}
			}
		}
	}
}
