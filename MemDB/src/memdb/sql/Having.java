package memdb.sql;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.expressions.Expression;

import org.antlr.runtime.tree.Tree;

public class Having
{
	private Select select;
	private Tree t;
	private Expression exp;

	public Having(Select select, Tree t) throws SQLException
	{
		this.select = select;
		this.t = t;
		exp = Expression.parseExpression(select, t.getChild(0));
	}

	public Object evaluate(int[] rowset) throws SQLException
	{
		if (exp != null)
			return exp.evaluate(rowset);
		return null;
	}

	public boolean containsAggregate(AtomicInteger acount)
	{
		if (exp != null)
			return exp.containsAggregate(acount);
		return false;
	}

	public boolean containsNonAggregate()
	{
		if (exp != null)
			return exp.containsNonAggregate();
		return false;
	}

	public void setAggRow(Object[] aggRow)
	{
		if (exp != null)
			exp.setAggRow(aggRow);
	}

	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		if (exp != null)
		{
			exp.aggregate(aggRow, rowset);
		}
	}

	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		if (exp != null)
		{
			exp.consolidate(primaryAggRow, toConsolidateAggRow);
		}
	}
}
