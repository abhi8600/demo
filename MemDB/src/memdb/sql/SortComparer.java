package memdb.sql;

import java.util.Comparator;
import java.util.List;

class SortComparer implements Comparator<QueryResultRow>
{
	List<Boolean> sortByDescending;

	public SortComparer(List<Boolean> sortByDescending)
	{
		this.sortByDescending = sortByDescending;
	}

	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	@Override
	public int compare(QueryResultRow o1, QueryResultRow o2)
	{
		Object[] sortdata1 = o1.sortRow;
		Object[] sortdata2 = o2.sortRow;
		for (int i = 0; i < sortdata1.length; i++)
		{
			int dir = sortByDescending.get(i) ? -1 : 1;
			Object val1 = sortdata1[i];
			Object val2 = sortdata2[i];
			if (val1 == null && val2 != null)
				return -dir;
			else if (val2 == null && val1 != null)
				return dir;
			else if (val1 == null) //both val1 and val2 are nulls so continue comparing other values if applicable
				continue;
			if (val1.getClass() == val2.getClass())
			{
				int result = ((Comparable) val1).compareTo((Comparable) val2);
				if (result != 0)
					return result * dir;
			} else if (val1 instanceof Number && val2 instanceof Number)
			{
				int result = (new Double((((Number) val1).doubleValue()))).compareTo(((Number) val2).doubleValue());
				if (result != 0)
					return result * dir;
			}
		}
		return 0;
	}
}