package memdb.sql;

import gnu.trove.iterator.TIntIterator;

import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.Command;
import memdb.Database;
import memdb.DateParser;
import memdb.ParallelAddRow;
import memdb.ThreadPool;
import memdb.expressions.Expression;
import memdb.expressions.Identifier;
import memdb.jdbcserver.RemoteResultSetImpl;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;
import memdb.storage.Table;
import memdb.storage.TableMetadata;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

public class Select extends WhereStatement
{
	static Logger logger = Logger.getLogger(Select.class);
	private static int numThreads = Runtime.getRuntime().availableProcessors();
	@SuppressWarnings("unused")
	private List<Integer> joinTypes = new ArrayList<Integer>();
	private String insertSchema;
	private List<Expression> colList = new ArrayList<Expression>();
	private List<Expression> orderByList = new ArrayList<Expression>();
	private List<Boolean> orderByDescending = new ArrayList<Boolean>();
	private List<String> labelList = new ArrayList<String>();
	private Having having = null;
	private boolean[] containsAggregate = null;
	private boolean[] containsObAggregate = null;
	private AtomicInteger numAggregations = new AtomicInteger(0);
	private String outfilename = null;
	public Object iteratorSet = null;
	public AtomicInteger rowcount = new AtomicInteger(0);
	public TIntIterator intit = null;
	private int[] colindices;
	private Table insertIntoTable;
	private String createTableAsName;
	private boolean createAsTempTable;
	private boolean isDistinct = false;
	private int insertIntoTableSize = 0;
	private boolean isInInClause = false;
	private boolean hasGroupBy = false;
	private DateParser dp = new DateParser();
	private StreamingResultSet processedResults = null;

	public Select(Command c, Database db, Tree t, String schema, Object[] parameters) throws SQLException, InterruptedException
	{
		super(c, db, t, parameters);
		this.insertSchema = schema;
		Tree projectionList = t.getChild(0);
		Tree orderByClause = null;
		Tree havingClause = null;
		Tree topClause = null;
		for (int i = 2; i < t.getChildCount(); i++)
		{
			if (t.getChild(i).getType() == MemgrammarParser.ORDERBY)
				orderByClause = t.getChild(i);
			if (t.getChild(i).getType() == MemgrammarParser.HAVING)
				havingClause = t.getChild(i);
			if (t.getChild(i).getType() == MemgrammarParser.TOP)
				topClause = t.getChild(i);
			if (t.getChild(i).getType() == MemgrammarParser.OUTFILE)
				outfilename = memdb.util.Util.getString(t.getChild(i).getChild(0).getText());
			if (t.getChild(i).getType() == MemgrammarParser.DISTINCT)
				isDistinct = true;
			if (t.getChild(i).getType() == MemgrammarParser.GROUPBY)
				hasGroupBy = true;
		}
		for (int i = 0; i < projectionList.getChildCount(); i++)
		{
			if (isInInClause && colList.size() == 1)
				break;
			Tree child = projectionList.getChild(i);
			if (child.getType() == MemgrammarParser.MULT)
			{
				if (isInInClause)
					continue;
				Tree alias = null;
				List<Table> wildtables = new ArrayList<Table>();
				if (child.getChildCount() > 0)
				{
					alias = child.getChild(0);
					Table at = findTableFromNameOrAlias(alias.getText());
					if (at != null)
						wildtables.add(at);
				} else
				{
					for (QueryPosition qp : positions)
						wildtables.add(qp.table);
				}
				for (Table st : wildtables)
				{
					for (int j = 0; j < st.tmd.columnNames.length; j++)
					{
						SyntheticTree syntree = new SyntheticTree(st.tmd.columnNames[j], MemgrammarParser.IDENT);
						SyntheticTree synchild = new SyntheticTree(st.name, MemgrammarParser.IDENT);
						syntree.addChild(synchild);
						Expression exp = new Identifier(this, (Tree) syntree);
						colList.add(exp);
						labelList.add(st.tmd.columnNames[j]);
					}
				}
			} else
			{
				Expression exp = Expression.parseExpression(this, child.getChild(0));
				colList.add(exp);
				if (child.getChildCount() > 1)
				{
					String s = child.getChild(1).getText();
					labelList.add(s.substring(1, s.length() - 1));
				} else if (child.getChildCount() == 1)
				{
					String s = child.getChild(0).getText();
					labelList.add(s);
				} else
					labelList.add(null);
			}
		}
		if (orderByClause != null && !isInInClause)
		{
			for (int i = 0; i < orderByClause.getChildCount(); i++)
			{
				Tree child = orderByClause.getChild(i);
				Expression exp = Expression.parseExpression(this, child.getChild(0));
				orderByList.add(exp);
				boolean descending = false;
				if (child.getChildCount() > 1)
				{
					if (child.getChild(1).getText().toLowerCase().equals("descending") || child.getChild(1).getText().toLowerCase().equals("desc"))
					{
						descending = true;
					}
				}
				orderByDescending.add(descending);
			}
		}
		// Figure out which columns are aggregates
		containsAggregate = new boolean[colList.size()];
		for (int i = 0; i < colList.size(); i++)
		{
			containsAggregate[i] = colList.get(i).containsAggregate(numAggregations);
			if (containsAggregate[i])
				containsAggregates = true;
		}
		containsObAggregate = new boolean[orderByList.size()];
		for (int i = 0; i < orderByList.size(); i++)
		{
			containsObAggregate[i] = orderByList.get(i).containsAggregate(numAggregations);
			if (containsObAggregate[i])
				containsAggregates = true;
		}
		// Parse having
		if (havingClause != null)
		{
			having = new Having(this, havingClause);
			having.containsAggregate(numAggregations);
		}
		if (topClause != null)
		{
			topn = Integer.valueOf(topClause.getChild(0).getText());
		}
	}

	public final Expression findLabelProjection(String labelName)
	{
		if (labelList == null)
			return null;
		for (int i = 0; i < labelList.size(); i++)
		{
			if (labelList.get(i) != null && labelList.get(i).equals(labelName))
				return colList.get(i);
		}
		return null;
	}

	public RemoteResultSetImpl process() throws SQLException
	{
		isSort = orderByList.size() > 0;
		boolean success = false;
		ThreadPool tp = new ThreadPool();
		try
		{
			// Execute joins and filtering (via the where statement construct)
			processRows();
			// Do aggregations
			StreamingResultSet aggResults = processAggregations(tp);
			// Setup result metadata
			TableMetadata resultmd = new TableMetadata(colList.size());
			for (int i = 0; i < colList.size(); i++)
			{
				if (labelList.get(i) != null)
				{
					resultmd.columnNames[i] = labelList.get(i);
				}
				resultmd.dataTypes[i] = colList.get(i).getType();
				resultmd.widths[i] = colList.get(i).getWidth();
			}
			// Create a table if necessary
			if (createTableAsName != null)
			{
				try
				{
					boolean diskBased = false;
					boolean compressed = false;
					for (int i = 0; i < positions.length; i++)
					{
						diskBased |= positions[i].table.tmd.diskBasedKeyStore;
						compressed |= positions[i].table.tmd.compressedKeyStore;
					}
					insertIntoTable = db.createTable(insertSchema, createTableAsName, createAsTempTable, insertSchema, diskBased, compressed);
				} catch (ClassNotFoundException e)
				{
					logger.error(e);
					throw new SQLException("Error creating table " + e.getMessage());
				} catch (InstantiationException e)
				{
					logger.error(e);
					throw new SQLException("Error creating table " + e.getMessage());
				} catch (IllegalAccessException e)
				{
					logger.error(e);
					throw new SQLException("Error creating table " + e.getMessage());
				}
				insertIntoTable.tmd = resultmd;
			}
			// Setup the table to be inserted into if necessary
			if (insertIntoTable != null)
			{
				insertIntoTableSize = insertIntoTable.getNumColumns();
				if (colindices == null)
				{
					colindices = new int[insertIntoTableSize];
					for (int i = 0; i < colindices.length; i++)
						colindices[i] = i;
				}
			}
			/*
			 * Lock the table being inserted into if necessary
			 */
			boolean alreadyReadLocked = false;
			for (QueryPosition qp : positions)
			{
				if (qp.table == insertIntoTable)
				{
					alreadyReadLocked = true;
				}
			}
			if (insertIntoTable != null)
			{
				if (!alreadyReadLocked)
				{
					/*
					 * See if this table is being inserted from itself, if so a read lock is already in place
					 */
					try
					{
						insertIntoTable.latch.awaitRead();
					} catch (InterruptedException e)
					{
						logger.error(e);
						throw new SQLException("Error inserting into table: " + e.getMessage());
					}
				}
				try
				{
					insertIntoTable.latch.awaitWrite();
				} catch (InterruptedException e)
				{
					logger.error(e);
					insertIntoTable.latch.releaseRead();
					throw new SQLException("Error awaiting write lock for table insert: " + e.getMessage());
				}
			}
			RowRange rowsProcessedRange = null;
			ParallelAddRow par = null;
			AtomicInteger addCount = null;
			LinkedBlockingQueue<Object[]> insertRowQueue = null;
			if (insertIntoTable != null)
			{
				rowsProcessedRange = new RowRange();
				rowsProcessedRange.start = insertIntoTable.getNumRows();
				addCount = new AtomicInteger();
				// Use server time zone since that is data source
				par = new ParallelAddRow(insertIntoTable, null, addCount, TimeZone.getDefault(), rowsProcessedRange);
				insertRowQueue = par.getRowQueue();
			}
			try
			{
				processedResults = new StreamingResultSet(null);
				processedResults.setRunWhenDone(new ReleaseClass());
				// Take care of all aggregates case (like select count(*) where there are no rows)
				if (topn >= 0 && !isSort)
					aggResults.setTopN(topn);
				AtomicInteger threadsRunning = new AtomicInteger(numThreads);
				for (int i = 0; i < numThreads; i++)
				{
					ProcessAggregatedResults ps = new ProcessAggregatedResults(aggResults, processedResults, insertRowQueue, threadsRunning, par, isSort);
					tp.executeLocal(ps);
				}
				if (isSort)
				{
					/*
					 * Now sort
					 */
					SortComparer sc = new SortComparer(orderByDescending);
					List<QueryResultRow> sortedList = processedResults.getList();
					Collections.sort(sortedList, sc);
					processedResults = new StreamingResultSet(sortedList);
					if (topn >= 0)
						processedResults.setTopN(topn);
				}
			} finally
			{
				if (insertIntoTable != null)
				{
					synchronized (processedResults)
					{
						try
						{
							processedResults.wait();
						} catch (InterruptedException e)
						{
							logger.error(e);
						}
					}
					insertIntoTable.indexRange(rowsProcessedRange);
					insertIntoTable.latch.releaseWrite();
					if (!alreadyReadLocked)
						insertIntoTable.latch.releaseRead();
				}
			}
			// Clear expression memory
			joinedrows = null;
			if (insertIntoTable != null && !createAsTempTable)
			{
				insertIntoTable.clearCache();
				insertIntoTable.pack();
				success = true;
				releaseTables();
				return null;
			}
			joinedrows = null;
			RemoteResultSetImpl rrsi = null;
			try
			{
				rrsi = new RemoteResultSetImpl(null, resultmd);
				rrsi.setResult(processedResults);
			} catch (RemoteException e)
			{
				logger.error(e);
			} catch (SQLException e)
			{
				logger.error(e);
				throw e;
			}
			success = true;
			return rrsi;
		} finally
		{
			tp.delete();
			if (!success)
				releaseTables();
		}
	}

	private class ReleaseClass implements Runnable
	{
		@Override
		public void run()
		{
			releaseTables();
		}
	}

	@SuppressWarnings("unchecked")
	private StreamingResultSet processAggregations(ThreadPool tp) throws SQLException
	{
		/*
		 * Split the joined rows to avoid thread contention
		 */
		RowSetIterator[][] rsit = new RowSetIterator[numThreads][];
		RowSet[][] splitRowSets = new RowSet[numThreads][];
		for (int j = 0; j < joinedrows.length; j++)
		{
			RowSet[] splits = joinedrows[j].splitRowSet(numThreads);
			for (int k = 0; k < numThreads; k++)
			{
				if (splitRowSets[k] == null)
					splitRowSets[k] = new RowSet[joinedrows.length];
				splitRowSets[k][j] = splits[k];
			}
		}
		boolean noAggregate = !hasGroupBy && !containsAggregates && !isDistinct;
		Map<AggKey, QueryResultRow>[] valMap = null;
		StreamingResultSet valList = null;
		if (!noAggregate)
		{
			valMap = new Map[numThreads];
		} else
		{
			valList = new StreamingResultSet(null);
		}
		for (int i = 0; i < numThreads; i++)
		{
			if (valMap != null)
				valMap[i] = new HashMap<AggKey, QueryResultRow>();
			rsit[i] = new RowSetIterator[joinedrows.length];
			for (int j = 0; j < joinedrows.length; j++)
			{
				if (splitRowSets[i][j] != null)
					rsit[i][j] = splitRowSets[i][j].getIterator();
			}
		}
		int childCount = colList.size();
		int obCount = orderByList.size();
		if (insertIntoTable != null && colindices.length != colList.size())
			throw new SQLException("Select and insert column lists of different sizes");
		/*
		 * Process aggregations
		 */
		AtomicInteger threadsRunning = new AtomicInteger(numThreads);
		for (int i = 0; i < numThreads; i++)
		{
			if (rsit[i][0] == null)
			{
				threadsRunning.decrementAndGet();
				continue;
			}
			tp.executeLocal(new AggThread(i, rsit[i], this, childCount, obCount, colList, orderByList, valMap, valList, having, containsAggregate,
					containsObAggregate, numAggregations.get(), threadsRunning));
		}
		/*
		 * Need to merge the results
		 */
		Map<AggKey, QueryResultRow> mergedAgg;
		if (!noAggregate)
		{
			try
			{
				synchronized (valMap)
				{
					valMap.wait();
				}
			} catch (InterruptedException e)
			{
				logger.error(e);
				return new StreamingResultSet(null);
			}
			mergedAgg = mergeAggregations(colList, valMap);
			Collection<QueryResultRow> mergeResults = mergedAgg.values();
			if (mergeResults.size() == 0)
			{
				// Handle the case of no rows and aggregates only
				int size = colList.size();
				boolean allAggs = true;
				for (int i = 0; i < colList.size(); i++)
				{
					if (!containsAggregate[i])
					{
						allAggs = false;
						break;
					}
				}
				if (allAggs)
				{
					QueryResultRow row = new QueryResultRow();
					row.dataRow = new Object[size + 1];
					for (int j = 0; j < size; j++)
					{
						Expression exp = colList.get(j);
						exp.setAggRow(null);
						row.dataRow[j] = exp.evaluate(null);
					}
					row.dataRow[size] = null;
					mergedAgg.put(null, row);
				}
			}
			return new StreamingResultSet(mergedAgg.values());
		} else
		{
			return valList;
		}
	}

	private Map<AggKey, QueryResultRow> mergeAggregations(List<Expression> colList, Map<AggKey, QueryResultRow>[] valMap)
	{
		Map<AggKey, QueryResultRow> merged = new HashMap<AggKey, QueryResultRow>();
		for (Map<AggKey, QueryResultRow> entry : valMap)
		{
			for (AggKey key : entry.keySet())
			{
				QueryResultRow curRow = merged.get(key);
				if (curRow == null)
				{
					merged.put(key, entry.get(key));
				} else
				{
					for (int j = 0; j < colList.size(); j++)
					{
						Expression exp = colList.get(j);
						if (!containsAggregate[j])
							continue;
						try
						{
							exp.consolidate(curRow.aggRow, entry.get(key).aggRow);
						} catch (SQLException e)
						{
							logger.error(e);
							continue;
						}
					}
					for (int j = 0; j < orderByList.size(); j++)
					{
						Expression exp = orderByList.get(j);
						if (!containsObAggregate[j])
							continue;
						try
						{
							exp.consolidate(curRow.aggRow, entry.get(key).aggRow);
						} catch (SQLException e)
						{
							logger.error(e);
							continue;
						}
					}
					if (having != null)
					{
						try
						{
							having.consolidate(curRow.aggRow, entry.get(key).aggRow);
						} catch (SQLException e)
						{
							logger.error(e);
							continue;
						}
					}
				}
			}
			entry.clear();
		}
		return merged;
	}

	private class ProcessAggregatedResults implements Runnable
	{
		private StreamingResultSet aggResults;
		private StreamingResultSet processedResults;
		private LinkedBlockingQueue<Object[]> insertRowQueue;
		private AtomicInteger threadsRunning;
		private ParallelAddRow par;
		private boolean saveSort;

		public ProcessAggregatedResults(StreamingResultSet aggResults, StreamingResultSet processedResults, LinkedBlockingQueue<Object[]> insertRowQueue,
				AtomicInteger threadsRunning, ParallelAddRow par, boolean saveSort)
		{
			this.aggResults = aggResults;
			this.processedResults = processedResults;
			this.insertRowQueue = insertRowQueue;
			this.threadsRunning = threadsRunning;
			this.par = par;
			this.saveSort = saveSort;
		}

		@Override
		public void run()
		{
			while (true)
			{
				QueryResultRow qrr = aggResults.next();
				if (qrr == null)
					break;
				try
				{
					if (having != null)
					{
						/*
						 * Filter for having clause
						 */
						synchronized (having)
						{
							having.setAggRow(qrr.aggRow);
							Object o = having.evaluate((int[]) qrr.dataRow[qrr.dataRow.length - 1]);
							if (!((Boolean) o))
								continue;
						}
					}
					for (int j = 0; j < colList.size(); j++)
					{
						Expression exp = colList.get(j);
						if (!containsAggregate[j])
							continue;
						synchronized (exp)
						{
							exp.setAggRow(qrr.aggRow);
							try
							{
								qrr.dataRow[j] = exp.evaluate((int[]) qrr.dataRow[qrr.dataRow.length - 1]);
							} catch (SQLException e)
							{
								logger.error(e);
							}
						}
					}
					for (int j = 0; j < orderByList.size(); j++)
					{
						Expression exp = orderByList.get(j);
						if (!containsObAggregate[j])
							continue;
						synchronized (exp)
						{
							exp.setAggRow(qrr.aggRow);
							try
							{
								qrr.sortRow[j] = exp.evaluate((int[]) qrr.dataRow[qrr.dataRow.length - 1]);
							} catch (SQLException e)
							{
								logger.error(e);
							}
						}
					}
				} catch (Exception e)
				{
					logger.error(e);
					continue;
				}
				if (insertIntoTable != null)
				{
					/*
					 * Insert into any result set as needed
					 */
					Object[] newRow = new Object[insertIntoTableSize];
					for (int i = 0; i < colindices.length; i++)
					{
						Object o = qrr.dataRow[i];
						if (insertIntoTable.tmd.dataTypes[colindices[i]] == Types.BIGINT)
						{
							if (o instanceof Integer)
								o = ((Integer) o).longValue();
							else if (o instanceof Double)
								o = ((Double) o).longValue();
						} else if (insertIntoTable.tmd.dataTypes[colindices[i]] == Types.INTEGER)
						{
							if (o instanceof Double)
								o = ((Double) o).intValue();
							else if (o instanceof Long)
								o = ((Long) o).intValue();
						} else if (insertIntoTable.tmd.dataTypes[colindices[i]] == Types.DATE)
						{
							if (o instanceof String)
								o = dp.parseUsingPossibleFormats(o.toString(), c.getClientTimeZone());
						}
						newRow[colindices[i]] = o;
					}
					try
					{
						insertRowQueue.offer(newRow, 5, TimeUnit.MINUTES);
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
					qrr.sortRow = null;
				} else
				{
					if (!processedResults.addRow(qrr))
						break;
					qrr.aggRow = null;
					if (!saveSort)
						qrr.sortRow = null;
				}
				rowsProcessed++;
			}
			if (threadsRunning.decrementAndGet() == 0)
			{
				if (insertIntoTable == null)
					processedResults.setNoMoreRows();
				else
				{
					par.join();
					synchronized (processedResults)
					{
						processedResults.notifyAll();
					}
				}
			}
		}
	}

	public String getOutfilename()
	{
		return outfilename;
	}

	public int[] getColindices()
	{
		return colindices;
	}

	public void setColindices(int[] colindices)
	{
		this.colindices = colindices;
	}

	public Table getInsertIntoTable()
	{
		return insertIntoTable;
	}

	public void setInsertIntoTable(Table insertIntoTable)
	{
		this.insertIntoTable = insertIntoTable;
	}

	public String getCreateTableAsName()
	{
		return createTableAsName;
	}

	public void setCreateTableAsName(String createTableAsName)
	{
		this.createTableAsName = createTableAsName;
	}

	public boolean isInInClause()
	{
		return isInInClause;
	}

	public void setInInClause(boolean isInInClause)
	{
		this.isInInClause = isInInClause;
	}

	public boolean isCreateAsTempTable()
	{
		return createAsTempTable;
	}

	public void setCreateAsTempTable(boolean createAsTempTable)
	{
		this.createAsTempTable = createAsTempTable;
	}
}
