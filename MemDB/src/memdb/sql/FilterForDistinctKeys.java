package memdb.sql;

import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import memdb.expressions.Expression;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;

public class FilterForDistinctKeys extends Thread
{
	private RowSetIterator rsit;
	private int[] rowset;
	private Set<String> uniqueKeys;
	private TIntSet newrowset;
	private List<Expression> distinctForKeyList;

	public FilterForDistinctKeys(RowSet rs, QueryPosition[] tables, Set<String> uniqueKeys, TIntSet newRowSet, List<Expression> distinctForKeyList)
	{
		this.rsit = rs.getIterator();
		rowset = new int[tables.length];
		for (int i = 1; i < rowset.length; i++)
			rowset[i] = -1;
		int[] numColumns = new int[tables.length];
		for (int i = 0; i < tables.length; i++)
			numColumns[i] = tables[i].table.getNumColumns();
		this.uniqueKeys = uniqueKeys;
		this.newrowset = newRowSet;
		this.distinctForKeyList = distinctForKeyList;
	}

	private synchronized int getNextRow()
	{
		return rsit.getNextRow();
	}

	public static RowSet filterKeys(RowSet rs, QueryPosition[] tables, List<Expression> distinctForKeyList)
	{
		FilterForDistinctKeys[] filterthreads = new FilterForDistinctKeys[Runtime.getRuntime().availableProcessors()];
		Set<String> uniqueKeys = Collections.synchronizedSet(new HashSet<String>());
		TIntSet newrowset = new TIntHashSet(rs.size());
		for (int i = 0; i < filterthreads.length; i++)
		{
			filterthreads[i] = new FilterForDistinctKeys(rs, tables, uniqueKeys, newrowset, distinctForKeyList);
			filterthreads[i].start();
		}
		for (int i = 0; i < filterthreads.length; i++)
		{
			try
			{
				filterthreads[i].join();
			} catch (InterruptedException e)
			{
			}
		}
		return new RowSet(newrowset);
	}

	public void run()
	{
		while (true)
		{
			int rownum = getNextRow();
			if (rownum < 0)
				break;
			StringBuilder sb = new StringBuilder();
			rowset[0] = rownum;
			for (Expression exp : distinctForKeyList)
			{
				Object o = null;
				try
				{
					o = exp.evaluate(rowset);
					if (o != null)
						sb.append('|' + o.toString());
				} catch (SQLException e)
				{
				}
			}
			String key = sb.toString();
			if (!uniqueKeys.contains(key))
			{
				uniqueKeys.add(key);
				synchronized (newrowset)
				{
					newrowset.add(rownum);
				}
			}
		}
	}
}
