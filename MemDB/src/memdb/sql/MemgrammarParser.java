// $ANTLR 3.4 C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g 2012-10-14 23:28:41
 package memdb.sql; 

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class MemgrammarParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ADD", "ALTER", "AND", "AS", "ASCENDING", "AVG", "BIGINTTYPE", "BOOLEAN", "CASE", "CAST", "COALESCE", "COLLIST", "COLUMN", "COLUMNS", "COMPRESSED", "COMPRESSEDKEYSTORE", "COUNT", "CREATE", "DATEADD", "DATEDIFF", "DATEPART", "DATETIME", "DATETIMETYPE", "DATETYPE", "DAY", "DEBUG", "DECIMALTYPE", "DELETE", "DELIMITEDBY", "DESCENDING", "DISK", "DISKKEYSTORE", "DISTINCT", "DISTINCTFORKEY", "DIV", "DROP", "DUMPDB", "DUMPDDL", "DUMPTABLE", "ELSE", "ENCLOSEDBY", "END", "EQUALS", "EXIT", "EXTRACT", "EscapeSequence", "FIELDS", "FLOAT", "FLOATTYPE", "FLUSH", "FOREIGNKEY", "FROM", "FULLOUTERJOIN", "GETDATE", "GROUPBY", "GT", "GTEQ", "HAVING", "HOUR", "HexDigit", "IDENT", "IFEXISTS", "IN", "INDIR", "INFOTABLES", "INNERJOIN", "INSERT", "INTEGER", "INTEGERTYPE", "INTO", "ISNOTNULL", "ISNULL", "ISNULLEXP", "KILL", "LEFTOUTERJOIN", "LEFT_PAREN", "LEN", "LENGTH", "LIKE", "LINE_COMMENT", "LOADDATAINFILE", "LOADDB", "LOADDDL", "LOADTABLE", "LT", "LTEQ", "MAX", "MAXONE", "MEMORY", "MIN", "MINUS", "MINUTE", "ML_COMMENT", "MOD", "MONTH", "MULT", "NEGATE", "NOINDEX", "NOT", "NOTEQUALS", "NOTIN", "NOTLIKE", "NULL", "OBJECTSIZE", "ON", "OR", "ORDERBY", "OUTDIR", "OUTFILE", "PARAMETER", "PAREN", "PLUS", "POSITION", "POW", "PRIMARYKEY", "PROCESSLIST", "PROJECTION", "PROJECTIONLIST", "QUARTER", "QUIT", "REMOTEQUERY", "RENAME", "RIGHTOUTERJOIN", "RIGHT_PAREN", "SCHEMA", "SECOND", "SELECT", "SET", "SHOW", "SKIPFIRST", "STRING", "SUBQUERY", "SUBSTR", "SUM", "SURROGATEKEYS", "SYSTEMSTATUS", "TABLE", "TABLEALIAS", "TABLES", "TERMINATEDBY", "THEN", "TIMESTAMPADD", "TIMESTAMPDIFF", "TO", "TOLOWER", "TOP", "TOUPPER", "TRANSIENT", "TRUNCATE", "UNCOMPRESSED", "UPDATE", "UnicodeEscape", "VALUES", "VARCHARTYPE", "WEEK", "WHEN", "WHERE", "WS", "YEAR", "','", "'.'", "';'"
    };

    public static final int EOF=-1;
    public static final int T__163=163;
    public static final int T__164=164;
    public static final int T__165=165;
    public static final int ADD=4;
    public static final int ALTER=5;
    public static final int AND=6;
    public static final int AS=7;
    public static final int ASCENDING=8;
    public static final int AVG=9;
    public static final int BIGINTTYPE=10;
    public static final int BOOLEAN=11;
    public static final int CASE=12;
    public static final int CAST=13;
    public static final int COALESCE=14;
    public static final int COLLIST=15;
    public static final int COLUMN=16;
    public static final int COLUMNS=17;
    public static final int COMPRESSED=18;
    public static final int COMPRESSEDKEYSTORE=19;
    public static final int COUNT=20;
    public static final int CREATE=21;
    public static final int DATEADD=22;
    public static final int DATEDIFF=23;
    public static final int DATEPART=24;
    public static final int DATETIME=25;
    public static final int DATETIMETYPE=26;
    public static final int DATETYPE=27;
    public static final int DAY=28;
    public static final int DEBUG=29;
    public static final int DECIMALTYPE=30;
    public static final int DELETE=31;
    public static final int DELIMITEDBY=32;
    public static final int DESCENDING=33;
    public static final int DISK=34;
    public static final int DISKKEYSTORE=35;
    public static final int DISTINCT=36;
    public static final int DISTINCTFORKEY=37;
    public static final int DIV=38;
    public static final int DROP=39;
    public static final int DUMPDB=40;
    public static final int DUMPDDL=41;
    public static final int DUMPTABLE=42;
    public static final int ELSE=43;
    public static final int ENCLOSEDBY=44;
    public static final int END=45;
    public static final int EQUALS=46;
    public static final int EXIT=47;
    public static final int EXTRACT=48;
    public static final int EscapeSequence=49;
    public static final int FIELDS=50;
    public static final int FLOAT=51;
    public static final int FLOATTYPE=52;
    public static final int FLUSH=53;
    public static final int FOREIGNKEY=54;
    public static final int FROM=55;
    public static final int FULLOUTERJOIN=56;
    public static final int GETDATE=57;
    public static final int GROUPBY=58;
    public static final int GT=59;
    public static final int GTEQ=60;
    public static final int HAVING=61;
    public static final int HOUR=62;
    public static final int HexDigit=63;
    public static final int IDENT=64;
    public static final int IFEXISTS=65;
    public static final int IN=66;
    public static final int INDIR=67;
    public static final int INFOTABLES=68;
    public static final int INNERJOIN=69;
    public static final int INSERT=70;
    public static final int INTEGER=71;
    public static final int INTEGERTYPE=72;
    public static final int INTO=73;
    public static final int ISNOTNULL=74;
    public static final int ISNULL=75;
    public static final int ISNULLEXP=76;
    public static final int KILL=77;
    public static final int LEFTOUTERJOIN=78;
    public static final int LEFT_PAREN=79;
    public static final int LEN=80;
    public static final int LENGTH=81;
    public static final int LIKE=82;
    public static final int LINE_COMMENT=83;
    public static final int LOADDATAINFILE=84;
    public static final int LOADDB=85;
    public static final int LOADDDL=86;
    public static final int LOADTABLE=87;
    public static final int LT=88;
    public static final int LTEQ=89;
    public static final int MAX=90;
    public static final int MAXONE=91;
    public static final int MEMORY=92;
    public static final int MIN=93;
    public static final int MINUS=94;
    public static final int MINUTE=95;
    public static final int ML_COMMENT=96;
    public static final int MOD=97;
    public static final int MONTH=98;
    public static final int MULT=99;
    public static final int NEGATE=100;
    public static final int NOINDEX=101;
    public static final int NOT=102;
    public static final int NOTEQUALS=103;
    public static final int NOTIN=104;
    public static final int NOTLIKE=105;
    public static final int NULL=106;
    public static final int OBJECTSIZE=107;
    public static final int ON=108;
    public static final int OR=109;
    public static final int ORDERBY=110;
    public static final int OUTDIR=111;
    public static final int OUTFILE=112;
    public static final int PARAMETER=113;
    public static final int PAREN=114;
    public static final int PLUS=115;
    public static final int POSITION=116;
    public static final int POW=117;
    public static final int PRIMARYKEY=118;
    public static final int PROCESSLIST=119;
    public static final int PROJECTION=120;
    public static final int PROJECTIONLIST=121;
    public static final int QUARTER=122;
    public static final int QUIT=123;
    public static final int REMOTEQUERY=124;
    public static final int RENAME=125;
    public static final int RIGHTOUTERJOIN=126;
    public static final int RIGHT_PAREN=127;
    public static final int SCHEMA=128;
    public static final int SECOND=129;
    public static final int SELECT=130;
    public static final int SET=131;
    public static final int SHOW=132;
    public static final int SKIPFIRST=133;
    public static final int STRING=134;
    public static final int SUBQUERY=135;
    public static final int SUBSTR=136;
    public static final int SUM=137;
    public static final int SURROGATEKEYS=138;
    public static final int SYSTEMSTATUS=139;
    public static final int TABLE=140;
    public static final int TABLEALIAS=141;
    public static final int TABLES=142;
    public static final int TERMINATEDBY=143;
    public static final int THEN=144;
    public static final int TIMESTAMPADD=145;
    public static final int TIMESTAMPDIFF=146;
    public static final int TO=147;
    public static final int TOLOWER=148;
    public static final int TOP=149;
    public static final int TOUPPER=150;
    public static final int TRANSIENT=151;
    public static final int TRUNCATE=152;
    public static final int UNCOMPRESSED=153;
    public static final int UPDATE=154;
    public static final int UnicodeEscape=155;
    public static final int VALUES=156;
    public static final int VARCHARTYPE=157;
    public static final int WEEK=158;
    public static final int WHEN=159;
    public static final int WHERE=160;
    public static final int WS=161;
    public static final int YEAR=162;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MemgrammarParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MemgrammarParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return MemgrammarParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g"; }


        public void displayRecognitionError(String[] tokenNames,
                                            RecognitionException e) {
            String hdr = getErrorHeader(e);
            String msg = getErrorMessage(e, tokenNames);
        }


    public static class statementlist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statementlist"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:307:1: statementlist : ( statement ';' )+ ;
    public final MemgrammarParser.statementlist_return statementlist() throws RecognitionException {
        MemgrammarParser.statementlist_return retval = new MemgrammarParser.statementlist_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal2=null;
        MemgrammarParser.statement_return statement1 =null;


        CommonTree char_literal2_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:308:2: ( ( statement ';' )+ )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:308:4: ( statement ';' )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:308:4: ( statement ';' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==ALTER||LA1_0==CREATE||LA1_0==DELETE||(LA1_0 >= DROP && LA1_0 <= DUMPTABLE)||LA1_0==EXIT||LA1_0==FLUSH||LA1_0==INSERT||LA1_0==KILL||(LA1_0 >= LOADDATAINFILE && LA1_0 <= LOADTABLE)||LA1_0==QUIT||LA1_0==RENAME||LA1_0==SELECT||LA1_0==SHOW||LA1_0==TRUNCATE||LA1_0==UPDATE) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:308:5: statement ';'
            	    {
            	    pushFollow(FOLLOW_statement_in_statementlist6344);
            	    statement1=statement();

            	    state._fsp--;

            	    adaptor.addChild(root_0, statement1.getTree());

            	    char_literal2=(Token)match(input,165,FOLLOW_165_in_statementlist6346); 
            	    char_literal2_tree = 
            	    (CommonTree)adaptor.create(char_literal2)
            	    ;
            	    adaptor.addChild(root_0, char_literal2_tree);


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statementlist"


    public static class statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:310:1: statement : ( selectquery | dropquery | showquery | createquery | insertquery | loaddata | deletequery | updatequery | alterquery | QUIT ^| EXIT ^| FLUSH ^| kill | dumptable | loadtable | dumpddl | loadddl | dumpdb | loaddb | renametable );
    public final MemgrammarParser.statement_return statement() throws RecognitionException {
        MemgrammarParser.statement_return retval = new MemgrammarParser.statement_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token QUIT12=null;
        Token EXIT13=null;
        Token FLUSH14=null;
        MemgrammarParser.selectquery_return selectquery3 =null;

        MemgrammarParser.dropquery_return dropquery4 =null;

        MemgrammarParser.showquery_return showquery5 =null;

        MemgrammarParser.createquery_return createquery6 =null;

        MemgrammarParser.insertquery_return insertquery7 =null;

        MemgrammarParser.loaddata_return loaddata8 =null;

        MemgrammarParser.deletequery_return deletequery9 =null;

        MemgrammarParser.updatequery_return updatequery10 =null;

        MemgrammarParser.alterquery_return alterquery11 =null;

        MemgrammarParser.kill_return kill15 =null;

        MemgrammarParser.dumptable_return dumptable16 =null;

        MemgrammarParser.loadtable_return loadtable17 =null;

        MemgrammarParser.dumpddl_return dumpddl18 =null;

        MemgrammarParser.loadddl_return loadddl19 =null;

        MemgrammarParser.dumpdb_return dumpdb20 =null;

        MemgrammarParser.loaddb_return loaddb21 =null;

        MemgrammarParser.renametable_return renametable22 =null;


        CommonTree QUIT12_tree=null;
        CommonTree EXIT13_tree=null;
        CommonTree FLUSH14_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:2: ( selectquery | dropquery | showquery | createquery | insertquery | loaddata | deletequery | updatequery | alterquery | QUIT ^| EXIT ^| FLUSH ^| kill | dumptable | loadtable | dumpddl | loadddl | dumpdb | loaddb | renametable )
            int alt2=20;
            switch ( input.LA(1) ) {
            case SELECT:
                {
                alt2=1;
                }
                break;
            case DROP:
            case TRUNCATE:
                {
                alt2=2;
                }
                break;
            case SHOW:
                {
                alt2=3;
                }
                break;
            case CREATE:
                {
                alt2=4;
                }
                break;
            case INSERT:
                {
                alt2=5;
                }
                break;
            case LOADDATAINFILE:
                {
                alt2=6;
                }
                break;
            case DELETE:
                {
                alt2=7;
                }
                break;
            case UPDATE:
                {
                alt2=8;
                }
                break;
            case ALTER:
                {
                alt2=9;
                }
                break;
            case QUIT:
                {
                alt2=10;
                }
                break;
            case EXIT:
                {
                alt2=11;
                }
                break;
            case FLUSH:
                {
                alt2=12;
                }
                break;
            case KILL:
                {
                alt2=13;
                }
                break;
            case DUMPTABLE:
                {
                alt2=14;
                }
                break;
            case LOADTABLE:
                {
                alt2=15;
                }
                break;
            case DUMPDDL:
                {
                alt2=16;
                }
                break;
            case LOADDDL:
                {
                alt2=17;
                }
                break;
            case DUMPDB:
                {
                alt2=18;
                }
                break;
            case LOADDB:
                {
                alt2=19;
                }
                break;
            case RENAME:
                {
                alt2=20;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:4: selectquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_selectquery_in_statement6358);
                    selectquery3=selectquery();

                    state._fsp--;

                    adaptor.addChild(root_0, selectquery3.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:18: dropquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_dropquery_in_statement6362);
                    dropquery4=dropquery();

                    state._fsp--;

                    adaptor.addChild(root_0, dropquery4.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:30: showquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_showquery_in_statement6366);
                    showquery5=showquery();

                    state._fsp--;

                    adaptor.addChild(root_0, showquery5.getTree());

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:42: createquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_createquery_in_statement6370);
                    createquery6=createquery();

                    state._fsp--;

                    adaptor.addChild(root_0, createquery6.getTree());

                    }
                    break;
                case 5 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:56: insertquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_insertquery_in_statement6374);
                    insertquery7=insertquery();

                    state._fsp--;

                    adaptor.addChild(root_0, insertquery7.getTree());

                    }
                    break;
                case 6 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:70: loaddata
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loaddata_in_statement6378);
                    loaddata8=loaddata();

                    state._fsp--;

                    adaptor.addChild(root_0, loaddata8.getTree());

                    }
                    break;
                case 7 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:81: deletequery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_deletequery_in_statement6382);
                    deletequery9=deletequery();

                    state._fsp--;

                    adaptor.addChild(root_0, deletequery9.getTree());

                    }
                    break;
                case 8 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:95: updatequery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_updatequery_in_statement6386);
                    updatequery10=updatequery();

                    state._fsp--;

                    adaptor.addChild(root_0, updatequery10.getTree());

                    }
                    break;
                case 9 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:311:109: alterquery
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_alterquery_in_statement6390);
                    alterquery11=alterquery();

                    state._fsp--;

                    adaptor.addChild(root_0, alterquery11.getTree());

                    }
                    break;
                case 10 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:3: QUIT ^
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    QUIT12=(Token)match(input,QUIT,FOLLOW_QUIT_in_statement6397); 
                    QUIT12_tree = 
                    (CommonTree)adaptor.create(QUIT12)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(QUIT12_tree, root_0);


                    }
                    break;
                case 11 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:11: EXIT ^
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    EXIT13=(Token)match(input,EXIT,FOLLOW_EXIT_in_statement6402); 
                    EXIT13_tree = 
                    (CommonTree)adaptor.create(EXIT13)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(EXIT13_tree, root_0);


                    }
                    break;
                case 12 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:19: FLUSH ^
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    FLUSH14=(Token)match(input,FLUSH,FOLLOW_FLUSH_in_statement6407); 
                    FLUSH14_tree = 
                    (CommonTree)adaptor.create(FLUSH14)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(FLUSH14_tree, root_0);


                    }
                    break;
                case 13 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:28: kill
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_kill_in_statement6412);
                    kill15=kill();

                    state._fsp--;

                    adaptor.addChild(root_0, kill15.getTree());

                    }
                    break;
                case 14 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:35: dumptable
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_dumptable_in_statement6416);
                    dumptable16=dumptable();

                    state._fsp--;

                    adaptor.addChild(root_0, dumptable16.getTree());

                    }
                    break;
                case 15 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:47: loadtable
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loadtable_in_statement6420);
                    loadtable17=loadtable();

                    state._fsp--;

                    adaptor.addChild(root_0, loadtable17.getTree());

                    }
                    break;
                case 16 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:59: dumpddl
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_dumpddl_in_statement6424);
                    dumpddl18=dumpddl();

                    state._fsp--;

                    adaptor.addChild(root_0, dumpddl18.getTree());

                    }
                    break;
                case 17 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:69: loadddl
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loadddl_in_statement6428);
                    loadddl19=loadddl();

                    state._fsp--;

                    adaptor.addChild(root_0, loadddl19.getTree());

                    }
                    break;
                case 18 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:79: dumpdb
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_dumpdb_in_statement6432);
                    dumpdb20=dumpdb();

                    state._fsp--;

                    adaptor.addChild(root_0, dumpdb20.getTree());

                    }
                    break;
                case 19 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:88: loaddb
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_loaddb_in_statement6436);
                    loaddb21=loaddb();

                    state._fsp--;

                    adaptor.addChild(root_0, loaddb21.getTree());

                    }
                    break;
                case 20 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:312:97: renametable
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_renametable_in_statement6440);
                    renametable22=renametable();

                    state._fsp--;

                    adaptor.addChild(root_0, renametable22.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statement"


    public static class loaddata_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loaddata"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:1: loaddata : LOADDATAINFILE STRING INTO TABLE ( schema '.' )? ident ( loadoption )* ( collist )? -> ^( LOADDATAINFILE STRING ^( ident ( schema )? ) ( loadoption )* ( collist )? ) ;
    public final MemgrammarParser.loaddata_return loaddata() throws RecognitionException {
        MemgrammarParser.loaddata_return retval = new MemgrammarParser.loaddata_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LOADDATAINFILE23=null;
        Token STRING24=null;
        Token INTO25=null;
        Token TABLE26=null;
        Token char_literal28=null;
        MemgrammarParser.schema_return schema27 =null;

        MemgrammarParser.ident_return ident29 =null;

        MemgrammarParser.loadoption_return loadoption30 =null;

        MemgrammarParser.collist_return collist31 =null;


        CommonTree LOADDATAINFILE23_tree=null;
        CommonTree STRING24_tree=null;
        CommonTree INTO25_tree=null;
        CommonTree TABLE26_tree=null;
        CommonTree char_literal28_tree=null;
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_INTO=new RewriteRuleTokenStream(adaptor,"token INTO");
        RewriteRuleTokenStream stream_LOADDATAINFILE=new RewriteRuleTokenStream(adaptor,"token LOADDATAINFILE");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_loadoption=new RewriteRuleSubtreeStream(adaptor,"rule loadoption");
        RewriteRuleSubtreeStream stream_collist=new RewriteRuleSubtreeStream(adaptor,"rule collist");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:9: ( LOADDATAINFILE STRING INTO TABLE ( schema '.' )? ident ( loadoption )* ( collist )? -> ^( LOADDATAINFILE STRING ^( ident ( schema )? ) ( loadoption )* ( collist )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:11: LOADDATAINFILE STRING INTO TABLE ( schema '.' )? ident ( loadoption )* ( collist )?
            {
            LOADDATAINFILE23=(Token)match(input,LOADDATAINFILE,FOLLOW_LOADDATAINFILE_in_loaddata6447);  
            stream_LOADDATAINFILE.add(LOADDATAINFILE23);


            STRING24=(Token)match(input,STRING,FOLLOW_STRING_in_loaddata6449);  
            stream_STRING.add(STRING24);


            INTO25=(Token)match(input,INTO,FOLLOW_INTO_in_loaddata6451);  
            stream_INTO.add(INTO25);


            TABLE26=(Token)match(input,TABLE,FOLLOW_TABLE_in_loaddata6453);  
            stream_TABLE.add(TABLE26);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:44: ( schema '.' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==IDENT) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==164) ) {
                    alt3=1;
                }
            }
            switch (alt3) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:45: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_loaddata6456);
                    schema27=schema();

                    state._fsp--;

                    stream_schema.add(schema27.getTree());

                    char_literal28=(Token)match(input,164,FOLLOW_164_in_loaddata6458);  
                    stream_164.add(char_literal28);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_loaddata6462);
            ident29=ident();

            state._fsp--;

            stream_ident.add(ident29.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:64: ( loadoption )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==DELIMITEDBY||LA4_0==ENCLOSEDBY||LA4_0==SKIPFIRST||LA4_0==TERMINATEDBY) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:64: loadoption
            	    {
            	    pushFollow(FOLLOW_loadoption_in_loaddata6464);
            	    loadoption30=loadoption();

            	    state._fsp--;

            	    stream_loadoption.add(loadoption30.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:76: ( collist )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==LEFT_PAREN) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:76: collist
                    {
                    pushFollow(FOLLOW_collist_in_loaddata6467);
                    collist31=collist();

                    state._fsp--;

                    stream_collist.add(collist31.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: collist, ident, LOADDATAINFILE, loadoption, schema, STRING
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 314:84: -> ^( LOADDATAINFILE STRING ^( ident ( schema )? ) ( loadoption )* ( collist )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:87: ^( LOADDATAINFILE STRING ^( ident ( schema )? ) ( loadoption )* ( collist )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_LOADDATAINFILE.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:111: ^( ident ( schema )? )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:119: ( schema )?
                if ( stream_schema.hasNext() ) {
                    adaptor.addChild(root_2, stream_schema.nextTree());

                }
                stream_schema.reset();

                adaptor.addChild(root_1, root_2);
                }

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:128: ( loadoption )*
                while ( stream_loadoption.hasNext() ) {
                    adaptor.addChild(root_1, stream_loadoption.nextTree());

                }
                stream_loadoption.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:314:140: ( collist )?
                if ( stream_collist.hasNext() ) {
                    adaptor.addChild(root_1, stream_collist.nextTree());

                }
                stream_collist.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loaddata"


    public static class loadoption_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loadoption"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:316:1: loadoption : ( delimitedby | enclosedby | terminatedby | SKIPFIRST );
    public final MemgrammarParser.loadoption_return loadoption() throws RecognitionException {
        MemgrammarParser.loadoption_return retval = new MemgrammarParser.loadoption_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SKIPFIRST35=null;
        MemgrammarParser.delimitedby_return delimitedby32 =null;

        MemgrammarParser.enclosedby_return enclosedby33 =null;

        MemgrammarParser.terminatedby_return terminatedby34 =null;


        CommonTree SKIPFIRST35_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:317:2: ( delimitedby | enclosedby | terminatedby | SKIPFIRST )
            int alt6=4;
            switch ( input.LA(1) ) {
            case DELIMITEDBY:
                {
                alt6=1;
                }
                break;
            case ENCLOSEDBY:
                {
                alt6=2;
                }
                break;
            case TERMINATEDBY:
                {
                alt6=3;
                }
                break;
            case SKIPFIRST:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:317:4: delimitedby
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_delimitedby_in_loadoption6497);
                    delimitedby32=delimitedby();

                    state._fsp--;

                    adaptor.addChild(root_0, delimitedby32.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:317:18: enclosedby
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_enclosedby_in_loadoption6501);
                    enclosedby33=enclosedby();

                    state._fsp--;

                    adaptor.addChild(root_0, enclosedby33.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:317:31: terminatedby
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_terminatedby_in_loadoption6505);
                    terminatedby34=terminatedby();

                    state._fsp--;

                    adaptor.addChild(root_0, terminatedby34.getTree());

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:317:46: SKIPFIRST
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    SKIPFIRST35=(Token)match(input,SKIPFIRST,FOLLOW_SKIPFIRST_in_loadoption6509); 
                    SKIPFIRST35_tree = 
                    (CommonTree)adaptor.create(SKIPFIRST35)
                    ;
                    adaptor.addChild(root_0, SKIPFIRST35_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loadoption"


    public static class delimitedby_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "delimitedby"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:319:1: delimitedby : DELIMITEDBY ^ STRING ;
    public final MemgrammarParser.delimitedby_return delimitedby() throws RecognitionException {
        MemgrammarParser.delimitedby_return retval = new MemgrammarParser.delimitedby_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DELIMITEDBY36=null;
        Token STRING37=null;

        CommonTree DELIMITEDBY36_tree=null;
        CommonTree STRING37_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:320:2: ( DELIMITEDBY ^ STRING )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:320:4: DELIMITEDBY ^ STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            DELIMITEDBY36=(Token)match(input,DELIMITEDBY,FOLLOW_DELIMITEDBY_in_delimitedby6518); 
            DELIMITEDBY36_tree = 
            (CommonTree)adaptor.create(DELIMITEDBY36)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(DELIMITEDBY36_tree, root_0);


            STRING37=(Token)match(input,STRING,FOLLOW_STRING_in_delimitedby6521); 
            STRING37_tree = 
            (CommonTree)adaptor.create(STRING37)
            ;
            adaptor.addChild(root_0, STRING37_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "delimitedby"


    public static class enclosedby_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "enclosedby"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:322:1: enclosedby : ENCLOSEDBY ^ STRING ;
    public final MemgrammarParser.enclosedby_return enclosedby() throws RecognitionException {
        MemgrammarParser.enclosedby_return retval = new MemgrammarParser.enclosedby_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ENCLOSEDBY38=null;
        Token STRING39=null;

        CommonTree ENCLOSEDBY38_tree=null;
        CommonTree STRING39_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:323:2: ( ENCLOSEDBY ^ STRING )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:323:4: ENCLOSEDBY ^ STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            ENCLOSEDBY38=(Token)match(input,ENCLOSEDBY,FOLLOW_ENCLOSEDBY_in_enclosedby6531); 
            ENCLOSEDBY38_tree = 
            (CommonTree)adaptor.create(ENCLOSEDBY38)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(ENCLOSEDBY38_tree, root_0);


            STRING39=(Token)match(input,STRING,FOLLOW_STRING_in_enclosedby6534); 
            STRING39_tree = 
            (CommonTree)adaptor.create(STRING39)
            ;
            adaptor.addChild(root_0, STRING39_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "enclosedby"


    public static class terminatedby_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "terminatedby"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:325:1: terminatedby : TERMINATEDBY ^ STRING ;
    public final MemgrammarParser.terminatedby_return terminatedby() throws RecognitionException {
        MemgrammarParser.terminatedby_return retval = new MemgrammarParser.terminatedby_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token TERMINATEDBY40=null;
        Token STRING41=null;

        CommonTree TERMINATEDBY40_tree=null;
        CommonTree STRING41_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:326:2: ( TERMINATEDBY ^ STRING )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:326:4: TERMINATEDBY ^ STRING
            {
            root_0 = (CommonTree)adaptor.nil();


            TERMINATEDBY40=(Token)match(input,TERMINATEDBY,FOLLOW_TERMINATEDBY_in_terminatedby6544); 
            TERMINATEDBY40_tree = 
            (CommonTree)adaptor.create(TERMINATEDBY40)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(TERMINATEDBY40_tree, root_0);


            STRING41=(Token)match(input,STRING,FOLLOW_STRING_in_terminatedby6547); 
            STRING41_tree = 
            (CommonTree)adaptor.create(STRING41)
            ;
            adaptor.addChild(root_0, STRING41_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "terminatedby"


    public static class kill_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "kill"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:328:1: kill : KILL ^ INTEGER ;
    public final MemgrammarParser.kill_return kill() throws RecognitionException {
        MemgrammarParser.kill_return retval = new MemgrammarParser.kill_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token KILL42=null;
        Token INTEGER43=null;

        CommonTree KILL42_tree=null;
        CommonTree INTEGER43_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:328:6: ( KILL ^ INTEGER )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:328:8: KILL ^ INTEGER
            {
            root_0 = (CommonTree)adaptor.nil();


            KILL42=(Token)match(input,KILL,FOLLOW_KILL_in_kill6556); 
            KILL42_tree = 
            (CommonTree)adaptor.create(KILL42)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(KILL42_tree, root_0);


            INTEGER43=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_kill6559); 
            INTEGER43_tree = 
            (CommonTree)adaptor.create(INTEGER43)
            ;
            adaptor.addChild(root_0, INTEGER43_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "kill"


    public static class createquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "createquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:331:1: createquery : ( CREATE TABLE ( schema '.' )? ident AS REMOTEQUERY LEFT_PAREN STRING ',' STRING ',' STRING ',' STRING ',' STRING RIGHT_PAREN ( primarykeyspec )? -> ^( CREATE ^( ident ( schema )? ) ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ( primarykeyspec )? ) | CREATE TABLE ( schema '.' )? ident AS selectquery -> ^( CREATE ^( ident ( schema )? ) selectquery ) | CREATE TABLE ( schema '.' )? ident LEFT_PAREN columnDefinition ( ',' columnDefinition )* RIGHT_PAREN ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? -> ^( CREATE ^( ident ( schema )? ) ( columnDefinition )+ ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? ) );
    public final MemgrammarParser.createquery_return createquery() throws RecognitionException {
        MemgrammarParser.createquery_return retval = new MemgrammarParser.createquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token CREATE44=null;
        Token TABLE45=null;
        Token char_literal47=null;
        Token AS49=null;
        Token REMOTEQUERY50=null;
        Token LEFT_PAREN51=null;
        Token STRING52=null;
        Token char_literal53=null;
        Token STRING54=null;
        Token char_literal55=null;
        Token STRING56=null;
        Token char_literal57=null;
        Token STRING58=null;
        Token char_literal59=null;
        Token STRING60=null;
        Token RIGHT_PAREN61=null;
        Token CREATE63=null;
        Token TABLE64=null;
        Token char_literal66=null;
        Token AS68=null;
        Token CREATE70=null;
        Token TABLE71=null;
        Token char_literal73=null;
        Token LEFT_PAREN75=null;
        Token char_literal77=null;
        Token RIGHT_PAREN79=null;
        Token TRANSIENT80=null;
        Token DISK81=null;
        Token MEMORY82=null;
        Token COMPRESSED83=null;
        Token UNCOMPRESSED84=null;
        Token DISKKEYSTORE85=null;
        Token COMPRESSEDKEYSTORE86=null;
        MemgrammarParser.schema_return schema46 =null;

        MemgrammarParser.ident_return ident48 =null;

        MemgrammarParser.primarykeyspec_return primarykeyspec62 =null;

        MemgrammarParser.schema_return schema65 =null;

        MemgrammarParser.ident_return ident67 =null;

        MemgrammarParser.selectquery_return selectquery69 =null;

        MemgrammarParser.schema_return schema72 =null;

        MemgrammarParser.ident_return ident74 =null;

        MemgrammarParser.columnDefinition_return columnDefinition76 =null;

        MemgrammarParser.columnDefinition_return columnDefinition78 =null;


        CommonTree CREATE44_tree=null;
        CommonTree TABLE45_tree=null;
        CommonTree char_literal47_tree=null;
        CommonTree AS49_tree=null;
        CommonTree REMOTEQUERY50_tree=null;
        CommonTree LEFT_PAREN51_tree=null;
        CommonTree STRING52_tree=null;
        CommonTree char_literal53_tree=null;
        CommonTree STRING54_tree=null;
        CommonTree char_literal55_tree=null;
        CommonTree STRING56_tree=null;
        CommonTree char_literal57_tree=null;
        CommonTree STRING58_tree=null;
        CommonTree char_literal59_tree=null;
        CommonTree STRING60_tree=null;
        CommonTree RIGHT_PAREN61_tree=null;
        CommonTree CREATE63_tree=null;
        CommonTree TABLE64_tree=null;
        CommonTree char_literal66_tree=null;
        CommonTree AS68_tree=null;
        CommonTree CREATE70_tree=null;
        CommonTree TABLE71_tree=null;
        CommonTree char_literal73_tree=null;
        CommonTree LEFT_PAREN75_tree=null;
        CommonTree char_literal77_tree=null;
        CommonTree RIGHT_PAREN79_tree=null;
        CommonTree TRANSIENT80_tree=null;
        CommonTree DISK81_tree=null;
        CommonTree MEMORY82_tree=null;
        CommonTree COMPRESSED83_tree=null;
        CommonTree UNCOMPRESSED84_tree=null;
        CommonTree DISKKEYSTORE85_tree=null;
        CommonTree COMPRESSEDKEYSTORE86_tree=null;
        RewriteRuleTokenStream stream_AS=new RewriteRuleTokenStream(adaptor,"token AS");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_CREATE=new RewriteRuleTokenStream(adaptor,"token CREATE");
        RewriteRuleTokenStream stream_UNCOMPRESSED=new RewriteRuleTokenStream(adaptor,"token UNCOMPRESSED");
        RewriteRuleTokenStream stream_COMPRESSED=new RewriteRuleTokenStream(adaptor,"token COMPRESSED");
        RewriteRuleTokenStream stream_DISK=new RewriteRuleTokenStream(adaptor,"token DISK");
        RewriteRuleTokenStream stream_COMPRESSEDKEYSTORE=new RewriteRuleTokenStream(adaptor,"token COMPRESSEDKEYSTORE");
        RewriteRuleTokenStream stream_TRANSIENT=new RewriteRuleTokenStream(adaptor,"token TRANSIENT");
        RewriteRuleTokenStream stream_DISKKEYSTORE=new RewriteRuleTokenStream(adaptor,"token DISKKEYSTORE");
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_MEMORY=new RewriteRuleTokenStream(adaptor,"token MEMORY");
        RewriteRuleTokenStream stream_REMOTEQUERY=new RewriteRuleTokenStream(adaptor,"token REMOTEQUERY");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_columnDefinition=new RewriteRuleSubtreeStream(adaptor,"rule columnDefinition");
        RewriteRuleSubtreeStream stream_primarykeyspec=new RewriteRuleSubtreeStream(adaptor,"rule primarykeyspec");
        RewriteRuleSubtreeStream stream_selectquery=new RewriteRuleSubtreeStream(adaptor,"rule selectquery");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:2: ( CREATE TABLE ( schema '.' )? ident AS REMOTEQUERY LEFT_PAREN STRING ',' STRING ',' STRING ',' STRING ',' STRING RIGHT_PAREN ( primarykeyspec )? -> ^( CREATE ^( ident ( schema )? ) ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ( primarykeyspec )? ) | CREATE TABLE ( schema '.' )? ident AS selectquery -> ^( CREATE ^( ident ( schema )? ) selectquery ) | CREATE TABLE ( schema '.' )? ident LEFT_PAREN columnDefinition ( ',' columnDefinition )* RIGHT_PAREN ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? -> ^( CREATE ^( ident ( schema )? ) ( columnDefinition )+ ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? ) )
            int alt19=3;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==CREATE) ) {
                int LA19_1 = input.LA(2);

                if ( (LA19_1==TABLE) ) {
                    int LA19_2 = input.LA(3);

                    if ( (LA19_2==IDENT) ) {
                        switch ( input.LA(4) ) {
                        case 164:
                            {
                            int LA19_4 = input.LA(5);

                            if ( (LA19_4==IDENT) ) {
                                int LA19_7 = input.LA(6);

                                if ( (LA19_7==AS) ) {
                                    int LA19_5 = input.LA(7);

                                    if ( (LA19_5==REMOTEQUERY) ) {
                                        alt19=1;
                                    }
                                    else if ( (LA19_5==SELECT) ) {
                                        alt19=2;
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 19, 5, input);

                                        throw nvae;

                                    }
                                }
                                else if ( (LA19_7==LEFT_PAREN) ) {
                                    alt19=3;
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 19, 7, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 19, 4, input);

                                throw nvae;

                            }
                            }
                            break;
                        case AS:
                            {
                            int LA19_5 = input.LA(5);

                            if ( (LA19_5==REMOTEQUERY) ) {
                                alt19=1;
                            }
                            else if ( (LA19_5==SELECT) ) {
                                alt19=2;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 19, 5, input);

                                throw nvae;

                            }
                            }
                            break;
                        case LEFT_PAREN:
                            {
                            alt19=3;
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 19, 3, input);

                            throw nvae;

                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 19, 2, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;

            }
            switch (alt19) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:4: CREATE TABLE ( schema '.' )? ident AS REMOTEQUERY LEFT_PAREN STRING ',' STRING ',' STRING ',' STRING ',' STRING RIGHT_PAREN ( primarykeyspec )?
                    {
                    CREATE44=(Token)match(input,CREATE,FOLLOW_CREATE_in_createquery6569);  
                    stream_CREATE.add(CREATE44);


                    TABLE45=(Token)match(input,TABLE,FOLLOW_TABLE_in_createquery6571);  
                    stream_TABLE.add(TABLE45);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:17: ( schema '.' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==IDENT) ) {
                        int LA7_1 = input.LA(2);

                        if ( (LA7_1==164) ) {
                            alt7=1;
                        }
                    }
                    switch (alt7) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:18: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_createquery6574);
                            schema46=schema();

                            state._fsp--;

                            stream_schema.add(schema46.getTree());

                            char_literal47=(Token)match(input,164,FOLLOW_164_in_createquery6576);  
                            stream_164.add(char_literal47);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_createquery6580);
                    ident48=ident();

                    state._fsp--;

                    stream_ident.add(ident48.getTree());

                    AS49=(Token)match(input,AS,FOLLOW_AS_in_createquery6582);  
                    stream_AS.add(AS49);


                    REMOTEQUERY50=(Token)match(input,REMOTEQUERY,FOLLOW_REMOTEQUERY_in_createquery6584);  
                    stream_REMOTEQUERY.add(REMOTEQUERY50);


                    LEFT_PAREN51=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_createquery6586);  
                    stream_LEFT_PAREN.add(LEFT_PAREN51);


                    STRING52=(Token)match(input,STRING,FOLLOW_STRING_in_createquery6588);  
                    stream_STRING.add(STRING52);


                    char_literal53=(Token)match(input,163,FOLLOW_163_in_createquery6590);  
                    stream_163.add(char_literal53);


                    STRING54=(Token)match(input,STRING,FOLLOW_STRING_in_createquery6592);  
                    stream_STRING.add(STRING54);


                    char_literal55=(Token)match(input,163,FOLLOW_163_in_createquery6594);  
                    stream_163.add(char_literal55);


                    STRING56=(Token)match(input,STRING,FOLLOW_STRING_in_createquery6596);  
                    stream_STRING.add(STRING56);


                    char_literal57=(Token)match(input,163,FOLLOW_163_in_createquery6598);  
                    stream_163.add(char_literal57);


                    STRING58=(Token)match(input,STRING,FOLLOW_STRING_in_createquery6600);  
                    stream_STRING.add(STRING58);


                    char_literal59=(Token)match(input,163,FOLLOW_163_in_createquery6602);  
                    stream_163.add(char_literal59);


                    STRING60=(Token)match(input,STRING,FOLLOW_STRING_in_createquery6604);  
                    stream_STRING.add(STRING60);


                    RIGHT_PAREN61=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_createquery6606);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN61);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:126: ( primarykeyspec )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==PRIMARYKEY) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:332:126: primarykeyspec
                            {
                            pushFollow(FOLLOW_primarykeyspec_in_createquery6608);
                            primarykeyspec62=primarykeyspec();

                            state._fsp--;

                            stream_primarykeyspec.add(primarykeyspec62.getTree());

                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: REMOTEQUERY, ident, STRING, schema, CREATE, STRING, STRING, primarykeyspec, STRING, STRING
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 332:142: -> ^( CREATE ^( ident ( schema )? ) ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ( primarykeyspec )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:333:3: ^( CREATE ^( ident ( schema )? ) ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ( primarykeyspec )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_CREATE.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:333:12: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:333:20: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:333:29: ^( REMOTEQUERY STRING STRING STRING STRING STRING )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(
                        stream_REMOTEQUERY.nextNode()
                        , root_2);

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:333:79: ( primarykeyspec )?
                        if ( stream_primarykeyspec.hasNext() ) {
                            adaptor.addChild(root_1, stream_primarykeyspec.nextTree());

                        }
                        stream_primarykeyspec.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:3: CREATE TABLE ( schema '.' )? ident AS selectquery
                    {
                    CREATE63=(Token)match(input,CREATE,FOLLOW_CREATE_in_createquery6647);  
                    stream_CREATE.add(CREATE63);


                    TABLE64=(Token)match(input,TABLE,FOLLOW_TABLE_in_createquery6649);  
                    stream_TABLE.add(TABLE64);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:16: ( schema '.' )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0==IDENT) ) {
                        int LA9_1 = input.LA(2);

                        if ( (LA9_1==164) ) {
                            alt9=1;
                        }
                    }
                    switch (alt9) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:17: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_createquery6652);
                            schema65=schema();

                            state._fsp--;

                            stream_schema.add(schema65.getTree());

                            char_literal66=(Token)match(input,164,FOLLOW_164_in_createquery6654);  
                            stream_164.add(char_literal66);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_createquery6658);
                    ident67=ident();

                    state._fsp--;

                    stream_ident.add(ident67.getTree());

                    AS68=(Token)match(input,AS,FOLLOW_AS_in_createquery6660);  
                    stream_AS.add(AS68);


                    pushFollow(FOLLOW_selectquery_in_createquery6662);
                    selectquery69=selectquery();

                    state._fsp--;

                    stream_selectquery.add(selectquery69.getTree());

                    // AST REWRITE
                    // elements: ident, CREATE, schema, selectquery
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 334:51: -> ^( CREATE ^( ident ( schema )? ) selectquery )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:54: ^( CREATE ^( ident ( schema )? ) selectquery )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_CREATE.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:63: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:334:71: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_1, stream_selectquery.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:3: CREATE TABLE ( schema '.' )? ident LEFT_PAREN columnDefinition ( ',' columnDefinition )* RIGHT_PAREN ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )?
                    {
                    CREATE70=(Token)match(input,CREATE,FOLLOW_CREATE_in_createquery6683);  
                    stream_CREATE.add(CREATE70);


                    TABLE71=(Token)match(input,TABLE,FOLLOW_TABLE_in_createquery6685);  
                    stream_TABLE.add(TABLE71);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:16: ( schema '.' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==IDENT) ) {
                        int LA10_1 = input.LA(2);

                        if ( (LA10_1==164) ) {
                            alt10=1;
                        }
                    }
                    switch (alt10) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:17: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_createquery6688);
                            schema72=schema();

                            state._fsp--;

                            stream_schema.add(schema72.getTree());

                            char_literal73=(Token)match(input,164,FOLLOW_164_in_createquery6690);  
                            stream_164.add(char_literal73);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_createquery6694);
                    ident74=ident();

                    state._fsp--;

                    stream_ident.add(ident74.getTree());

                    LEFT_PAREN75=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_createquery6696);  
                    stream_LEFT_PAREN.add(LEFT_PAREN75);


                    pushFollow(FOLLOW_columnDefinition_in_createquery6698);
                    columnDefinition76=columnDefinition();

                    state._fsp--;

                    stream_columnDefinition.add(columnDefinition76.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:64: ( ',' columnDefinition )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==163) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:65: ',' columnDefinition
                    	    {
                    	    char_literal77=(Token)match(input,163,FOLLOW_163_in_createquery6701);  
                    	    stream_163.add(char_literal77);


                    	    pushFollow(FOLLOW_columnDefinition_in_createquery6703);
                    	    columnDefinition78=columnDefinition();

                    	    state._fsp--;

                    	    stream_columnDefinition.add(columnDefinition78.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    RIGHT_PAREN79=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_createquery6707);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN79);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:100: ( TRANSIENT )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==TRANSIENT) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:100: TRANSIENT
                            {
                            TRANSIENT80=(Token)match(input,TRANSIENT,FOLLOW_TRANSIENT_in_createquery6709);  
                            stream_TRANSIENT.add(TRANSIENT80);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:111: ( DISK )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==DISK) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:111: DISK
                            {
                            DISK81=(Token)match(input,DISK,FOLLOW_DISK_in_createquery6712);  
                            stream_DISK.add(DISK81);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:117: ( MEMORY )?
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==MEMORY) ) {
                        alt14=1;
                    }
                    switch (alt14) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:117: MEMORY
                            {
                            MEMORY82=(Token)match(input,MEMORY,FOLLOW_MEMORY_in_createquery6715);  
                            stream_MEMORY.add(MEMORY82);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:125: ( COMPRESSED )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==COMPRESSED) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:125: COMPRESSED
                            {
                            COMPRESSED83=(Token)match(input,COMPRESSED,FOLLOW_COMPRESSED_in_createquery6718);  
                            stream_COMPRESSED.add(COMPRESSED83);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:137: ( UNCOMPRESSED )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0==UNCOMPRESSED) ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:137: UNCOMPRESSED
                            {
                            UNCOMPRESSED84=(Token)match(input,UNCOMPRESSED,FOLLOW_UNCOMPRESSED_in_createquery6721);  
                            stream_UNCOMPRESSED.add(UNCOMPRESSED84);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:151: ( DISKKEYSTORE )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==DISKKEYSTORE) ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:151: DISKKEYSTORE
                            {
                            DISKKEYSTORE85=(Token)match(input,DISKKEYSTORE,FOLLOW_DISKKEYSTORE_in_createquery6724);  
                            stream_DISKKEYSTORE.add(DISKKEYSTORE85);


                            }
                            break;

                    }


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:165: ( COMPRESSEDKEYSTORE )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==COMPRESSEDKEYSTORE) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:335:165: COMPRESSEDKEYSTORE
                            {
                            COMPRESSEDKEYSTORE86=(Token)match(input,COMPRESSEDKEYSTORE,FOLLOW_COMPRESSEDKEYSTORE_in_createquery6727);  
                            stream_COMPRESSEDKEYSTORE.add(COMPRESSEDKEYSTORE86);


                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: CREATE, MEMORY, COMPRESSEDKEYSTORE, COMPRESSED, UNCOMPRESSED, TRANSIENT, schema, ident, DISKKEYSTORE, DISK, columnDefinition
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 335:185: -> ^( CREATE ^( ident ( schema )? ) ( columnDefinition )+ ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:3: ^( CREATE ^( ident ( schema )? ) ( columnDefinition )+ ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ( DISKKEYSTORE )? ( COMPRESSEDKEYSTORE )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_CREATE.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:12: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:20: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        if ( !(stream_columnDefinition.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_columnDefinition.hasNext() ) {
                            adaptor.addChild(root_1, stream_columnDefinition.nextTree());

                        }
                        stream_columnDefinition.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:47: ( TRANSIENT )?
                        if ( stream_TRANSIENT.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_TRANSIENT.nextNode()
                            );

                        }
                        stream_TRANSIENT.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:58: ( DISK )?
                        if ( stream_DISK.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_DISK.nextNode()
                            );

                        }
                        stream_DISK.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:64: ( MEMORY )?
                        if ( stream_MEMORY.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_MEMORY.nextNode()
                            );

                        }
                        stream_MEMORY.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:72: ( COMPRESSED )?
                        if ( stream_COMPRESSED.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_COMPRESSED.nextNode()
                            );

                        }
                        stream_COMPRESSED.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:84: ( UNCOMPRESSED )?
                        if ( stream_UNCOMPRESSED.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_UNCOMPRESSED.nextNode()
                            );

                        }
                        stream_UNCOMPRESSED.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:98: ( DISKKEYSTORE )?
                        if ( stream_DISKKEYSTORE.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_DISKKEYSTORE.nextNode()
                            );

                        }
                        stream_DISKKEYSTORE.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:336:112: ( COMPRESSEDKEYSTORE )?
                        if ( stream_COMPRESSEDKEYSTORE.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_COMPRESSEDKEYSTORE.nextNode()
                            );

                        }
                        stream_COMPRESSEDKEYSTORE.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "createquery"


    public static class alterquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "alterquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:338:1: alterquery : ALTER TABLE ( schema '.' )? ident alterdef ( ',' alterdef )* -> ^( ALTER ^( ident ( schema )? ) ( alterdef )+ ) ;
    public final MemgrammarParser.alterquery_return alterquery() throws RecognitionException {
        MemgrammarParser.alterquery_return retval = new MemgrammarParser.alterquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ALTER87=null;
        Token TABLE88=null;
        Token char_literal90=null;
        Token char_literal93=null;
        MemgrammarParser.schema_return schema89 =null;

        MemgrammarParser.ident_return ident91 =null;

        MemgrammarParser.alterdef_return alterdef92 =null;

        MemgrammarParser.alterdef_return alterdef94 =null;


        CommonTree ALTER87_tree=null;
        CommonTree TABLE88_tree=null;
        CommonTree char_literal90_tree=null;
        CommonTree char_literal93_tree=null;
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_ALTER=new RewriteRuleTokenStream(adaptor,"token ALTER");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_alterdef=new RewriteRuleSubtreeStream(adaptor,"rule alterdef");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:2: ( ALTER TABLE ( schema '.' )? ident alterdef ( ',' alterdef )* -> ^( ALTER ^( ident ( schema )? ) ( alterdef )+ ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:4: ALTER TABLE ( schema '.' )? ident alterdef ( ',' alterdef )*
            {
            ALTER87=(Token)match(input,ALTER,FOLLOW_ALTER_in_alterquery6777);  
            stream_ALTER.add(ALTER87);


            TABLE88=(Token)match(input,TABLE,FOLLOW_TABLE_in_alterquery6779);  
            stream_TABLE.add(TABLE88);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:16: ( schema '.' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==IDENT) ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1==164) ) {
                    alt20=1;
                }
            }
            switch (alt20) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:17: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_alterquery6782);
                    schema89=schema();

                    state._fsp--;

                    stream_schema.add(schema89.getTree());

                    char_literal90=(Token)match(input,164,FOLLOW_164_in_alterquery6784);  
                    stream_164.add(char_literal90);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_alterquery6788);
            ident91=ident();

            state._fsp--;

            stream_ident.add(ident91.getTree());

            pushFollow(FOLLOW_alterdef_in_alterquery6790);
            alterdef92=alterdef();

            state._fsp--;

            stream_alterdef.add(alterdef92.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:45: ( ',' alterdef )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==163) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:46: ',' alterdef
            	    {
            	    char_literal93=(Token)match(input,163,FOLLOW_163_in_alterquery6793);  
            	    stream_163.add(char_literal93);


            	    pushFollow(FOLLOW_alterdef_in_alterquery6795);
            	    alterdef94=alterdef();

            	    state._fsp--;

            	    stream_alterdef.add(alterdef94.getTree());

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            // AST REWRITE
            // elements: alterdef, ident, schema, ALTER
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 339:62: -> ^( ALTER ^( ident ( schema )? ) ( alterdef )+ )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:65: ^( ALTER ^( ident ( schema )? ) ( alterdef )+ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_ALTER.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:73: ^( ident ( schema )? )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:339:81: ( schema )?
                if ( stream_schema.hasNext() ) {
                    adaptor.addChild(root_2, stream_schema.nextTree());

                }
                stream_schema.reset();

                adaptor.addChild(root_1, root_2);
                }

                if ( !(stream_alterdef.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_alterdef.hasNext() ) {
                    adaptor.addChild(root_1, stream_alterdef.nextTree());

                }
                stream_alterdef.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "alterquery"


    public static class insertquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "insertquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:341:1: insertquery : ( INSERT INTO ( schema '.' )? ident ( collist )? selectquery -> ^( INSERT ^( ident ( schema )? ) ( collist )? selectquery ) | INSERT INTO ( schema '.' )? ident ( collist )? VALUES LEFT_PAREN insertliteral ( ',' insertliteral )* RIGHT_PAREN -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( VALUES ( insertliteral )* ) ) | INSERT INTO ( schema '.' )? ident ( collist )? REMOTEQUERY STRING STRING STRING STRING STRING -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ) );
    public final MemgrammarParser.insertquery_return insertquery() throws RecognitionException {
        MemgrammarParser.insertquery_return retval = new MemgrammarParser.insertquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INSERT95=null;
        Token INTO96=null;
        Token char_literal98=null;
        Token INSERT102=null;
        Token INTO103=null;
        Token char_literal105=null;
        Token VALUES108=null;
        Token LEFT_PAREN109=null;
        Token char_literal111=null;
        Token RIGHT_PAREN113=null;
        Token INSERT114=null;
        Token INTO115=null;
        Token char_literal117=null;
        Token REMOTEQUERY120=null;
        Token STRING121=null;
        Token STRING122=null;
        Token STRING123=null;
        Token STRING124=null;
        Token STRING125=null;
        MemgrammarParser.schema_return schema97 =null;

        MemgrammarParser.ident_return ident99 =null;

        MemgrammarParser.collist_return collist100 =null;

        MemgrammarParser.selectquery_return selectquery101 =null;

        MemgrammarParser.schema_return schema104 =null;

        MemgrammarParser.ident_return ident106 =null;

        MemgrammarParser.collist_return collist107 =null;

        MemgrammarParser.insertliteral_return insertliteral110 =null;

        MemgrammarParser.insertliteral_return insertliteral112 =null;

        MemgrammarParser.schema_return schema116 =null;

        MemgrammarParser.ident_return ident118 =null;

        MemgrammarParser.collist_return collist119 =null;


        CommonTree INSERT95_tree=null;
        CommonTree INTO96_tree=null;
        CommonTree char_literal98_tree=null;
        CommonTree INSERT102_tree=null;
        CommonTree INTO103_tree=null;
        CommonTree char_literal105_tree=null;
        CommonTree VALUES108_tree=null;
        CommonTree LEFT_PAREN109_tree=null;
        CommonTree char_literal111_tree=null;
        CommonTree RIGHT_PAREN113_tree=null;
        CommonTree INSERT114_tree=null;
        CommonTree INTO115_tree=null;
        CommonTree char_literal117_tree=null;
        CommonTree REMOTEQUERY120_tree=null;
        CommonTree STRING121_tree=null;
        CommonTree STRING122_tree=null;
        CommonTree STRING123_tree=null;
        CommonTree STRING124_tree=null;
        CommonTree STRING125_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_INSERT=new RewriteRuleTokenStream(adaptor,"token INSERT");
        RewriteRuleTokenStream stream_INTO=new RewriteRuleTokenStream(adaptor,"token INTO");
        RewriteRuleTokenStream stream_REMOTEQUERY=new RewriteRuleTokenStream(adaptor,"token REMOTEQUERY");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleTokenStream stream_VALUES=new RewriteRuleTokenStream(adaptor,"token VALUES");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_insertliteral=new RewriteRuleSubtreeStream(adaptor,"rule insertliteral");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_selectquery=new RewriteRuleSubtreeStream(adaptor,"rule selectquery");
        RewriteRuleSubtreeStream stream_collist=new RewriteRuleSubtreeStream(adaptor,"rule collist");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:2: ( INSERT INTO ( schema '.' )? ident ( collist )? selectquery -> ^( INSERT ^( ident ( schema )? ) ( collist )? selectquery ) | INSERT INTO ( schema '.' )? ident ( collist )? VALUES LEFT_PAREN insertliteral ( ',' insertliteral )* RIGHT_PAREN -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( VALUES ( insertliteral )* ) ) | INSERT INTO ( schema '.' )? ident ( collist )? REMOTEQUERY STRING STRING STRING STRING STRING -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ) )
            int alt29=3;
            alt29 = dfa29.predict(input);
            switch (alt29) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:4: INSERT INTO ( schema '.' )? ident ( collist )? selectquery
                    {
                    INSERT95=(Token)match(input,INSERT,FOLLOW_INSERT_in_insertquery6825);  
                    stream_INSERT.add(INSERT95);


                    INTO96=(Token)match(input,INTO,FOLLOW_INTO_in_insertquery6827);  
                    stream_INTO.add(INTO96);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:16: ( schema '.' )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==IDENT) ) {
                        int LA22_1 = input.LA(2);

                        if ( (LA22_1==164) ) {
                            alt22=1;
                        }
                    }
                    switch (alt22) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:17: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_insertquery6830);
                            schema97=schema();

                            state._fsp--;

                            stream_schema.add(schema97.getTree());

                            char_literal98=(Token)match(input,164,FOLLOW_164_in_insertquery6832);  
                            stream_164.add(char_literal98);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_insertquery6836);
                    ident99=ident();

                    state._fsp--;

                    stream_ident.add(ident99.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:36: ( collist )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==LEFT_PAREN) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:36: collist
                            {
                            pushFollow(FOLLOW_collist_in_insertquery6838);
                            collist100=collist();

                            state._fsp--;

                            stream_collist.add(collist100.getTree());

                            }
                            break;

                    }


                    pushFollow(FOLLOW_selectquery_in_insertquery6841);
                    selectquery101=selectquery();

                    state._fsp--;

                    stream_selectquery.add(selectquery101.getTree());

                    // AST REWRITE
                    // elements: schema, INSERT, selectquery, ident, collist
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 342:57: -> ^( INSERT ^( ident ( schema )? ) ( collist )? selectquery )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:60: ^( INSERT ^( ident ( schema )? ) ( collist )? selectquery )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_INSERT.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:69: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:77: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:342:86: ( collist )?
                        if ( stream_collist.hasNext() ) {
                            adaptor.addChild(root_1, stream_collist.nextTree());

                        }
                        stream_collist.reset();

                        adaptor.addChild(root_1, stream_selectquery.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:3: INSERT INTO ( schema '.' )? ident ( collist )? VALUES LEFT_PAREN insertliteral ( ',' insertliteral )* RIGHT_PAREN
                    {
                    INSERT102=(Token)match(input,INSERT,FOLLOW_INSERT_in_insertquery6865);  
                    stream_INSERT.add(INSERT102);


                    INTO103=(Token)match(input,INTO,FOLLOW_INTO_in_insertquery6867);  
                    stream_INTO.add(INTO103);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:15: ( schema '.' )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==IDENT) ) {
                        int LA24_1 = input.LA(2);

                        if ( (LA24_1==164) ) {
                            alt24=1;
                        }
                    }
                    switch (alt24) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:16: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_insertquery6870);
                            schema104=schema();

                            state._fsp--;

                            stream_schema.add(schema104.getTree());

                            char_literal105=(Token)match(input,164,FOLLOW_164_in_insertquery6872);  
                            stream_164.add(char_literal105);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_insertquery6876);
                    ident106=ident();

                    state._fsp--;

                    stream_ident.add(ident106.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:35: ( collist )?
                    int alt25=2;
                    int LA25_0 = input.LA(1);

                    if ( (LA25_0==LEFT_PAREN) ) {
                        alt25=1;
                    }
                    switch (alt25) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:35: collist
                            {
                            pushFollow(FOLLOW_collist_in_insertquery6878);
                            collist107=collist();

                            state._fsp--;

                            stream_collist.add(collist107.getTree());

                            }
                            break;

                    }


                    VALUES108=(Token)match(input,VALUES,FOLLOW_VALUES_in_insertquery6881);  
                    stream_VALUES.add(VALUES108);


                    LEFT_PAREN109=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_insertquery6883);  
                    stream_LEFT_PAREN.add(LEFT_PAREN109);


                    pushFollow(FOLLOW_insertliteral_in_insertquery6885);
                    insertliteral110=insertliteral();

                    state._fsp--;

                    stream_insertliteral.add(insertliteral110.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:76: ( ',' insertliteral )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==163) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:77: ',' insertliteral
                    	    {
                    	    char_literal111=(Token)match(input,163,FOLLOW_163_in_insertquery6888);  
                    	    stream_163.add(char_literal111);


                    	    pushFollow(FOLLOW_insertliteral_in_insertquery6890);
                    	    insertliteral112=insertliteral();

                    	    state._fsp--;

                    	    stream_insertliteral.add(insertliteral112.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);


                    RIGHT_PAREN113=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_insertquery6894);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN113);


                    // AST REWRITE
                    // elements: INSERT, VALUES, ident, insertliteral, collist, schema
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 343:109: -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( VALUES ( insertliteral )* ) )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:112: ^( INSERT ^( ident ( schema )? ) ( collist )? ^( VALUES ( insertliteral )* ) )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_INSERT.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:121: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:129: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:138: ( collist )?
                        if ( stream_collist.hasNext() ) {
                            adaptor.addChild(root_1, stream_collist.nextTree());

                        }
                        stream_collist.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:147: ^( VALUES ( insertliteral )* )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(
                        stream_VALUES.nextNode()
                        , root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:343:156: ( insertliteral )*
                        while ( stream_insertliteral.hasNext() ) {
                            adaptor.addChild(root_2, stream_insertliteral.nextTree());

                        }
                        stream_insertliteral.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:3: INSERT INTO ( schema '.' )? ident ( collist )? REMOTEQUERY STRING STRING STRING STRING STRING
                    {
                    INSERT114=(Token)match(input,INSERT,FOLLOW_INSERT_in_insertquery6923);  
                    stream_INSERT.add(INSERT114);


                    INTO115=(Token)match(input,INTO,FOLLOW_INTO_in_insertquery6925);  
                    stream_INTO.add(INTO115);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:15: ( schema '.' )?
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( (LA27_0==IDENT) ) {
                        int LA27_1 = input.LA(2);

                        if ( (LA27_1==164) ) {
                            alt27=1;
                        }
                    }
                    switch (alt27) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:16: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_insertquery6928);
                            schema116=schema();

                            state._fsp--;

                            stream_schema.add(schema116.getTree());

                            char_literal117=(Token)match(input,164,FOLLOW_164_in_insertquery6930);  
                            stream_164.add(char_literal117);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_insertquery6934);
                    ident118=ident();

                    state._fsp--;

                    stream_ident.add(ident118.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:35: ( collist )?
                    int alt28=2;
                    int LA28_0 = input.LA(1);

                    if ( (LA28_0==LEFT_PAREN) ) {
                        alt28=1;
                    }
                    switch (alt28) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:35: collist
                            {
                            pushFollow(FOLLOW_collist_in_insertquery6936);
                            collist119=collist();

                            state._fsp--;

                            stream_collist.add(collist119.getTree());

                            }
                            break;

                    }


                    REMOTEQUERY120=(Token)match(input,REMOTEQUERY,FOLLOW_REMOTEQUERY_in_insertquery6939);  
                    stream_REMOTEQUERY.add(REMOTEQUERY120);


                    STRING121=(Token)match(input,STRING,FOLLOW_STRING_in_insertquery6941);  
                    stream_STRING.add(STRING121);


                    STRING122=(Token)match(input,STRING,FOLLOW_STRING_in_insertquery6943);  
                    stream_STRING.add(STRING122);


                    STRING123=(Token)match(input,STRING,FOLLOW_STRING_in_insertquery6945);  
                    stream_STRING.add(STRING123);


                    STRING124=(Token)match(input,STRING,FOLLOW_STRING_in_insertquery6947);  
                    stream_STRING.add(STRING124);


                    STRING125=(Token)match(input,STRING,FOLLOW_STRING_in_insertquery6949);  
                    stream_STRING.add(STRING125);


                    // AST REWRITE
                    // elements: ident, STRING, STRING, schema, collist, INSERT, STRING, STRING, REMOTEQUERY, STRING
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 344:91: -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( REMOTEQUERY STRING STRING STRING STRING STRING ) )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:94: ^( INSERT ^( ident ( schema )? ) ( collist )? ^( REMOTEQUERY STRING STRING STRING STRING STRING ) )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_INSERT.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:103: ^( ident ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:111: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:120: ( collist )?
                        if ( stream_collist.hasNext() ) {
                            adaptor.addChild(root_1, stream_collist.nextTree());

                        }
                        stream_collist.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:344:129: ^( REMOTEQUERY STRING STRING STRING STRING STRING )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(
                        stream_REMOTEQUERY.nextNode()
                        , root_2);

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_2, 
                        stream_STRING.nextNode()
                        );

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "insertquery"


    public static class insertliteral_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "insertliteral"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:346:1: insertliteral : ( literal | PARAMETER );
    public final MemgrammarParser.insertliteral_return insertliteral() throws RecognitionException {
        MemgrammarParser.insertliteral_return retval = new MemgrammarParser.insertliteral_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PARAMETER127=null;
        MemgrammarParser.literal_return literal126 =null;


        CommonTree PARAMETER127_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:347:2: ( literal | PARAMETER )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==BOOLEAN||LA30_0==DATETIME||LA30_0==FLOAT||LA30_0==INTEGER||LA30_0==NULL||LA30_0==STRING) ) {
                alt30=1;
            }
            else if ( (LA30_0==PARAMETER) ) {
                alt30=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }
            switch (alt30) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:347:4: literal
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_literal_in_insertliteral6990);
                    literal126=literal();

                    state._fsp--;

                    adaptor.addChild(root_0, literal126.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:347:14: PARAMETER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    PARAMETER127=(Token)match(input,PARAMETER,FOLLOW_PARAMETER_in_insertliteral6994); 
                    PARAMETER127_tree = 
                    (CommonTree)adaptor.create(PARAMETER127)
                    ;
                    adaptor.addChild(root_0, PARAMETER127_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "insertliteral"


    public static class collist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "collist"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:1: collist : LEFT_PAREN colspec ( ',' colspec )* RIGHT_PAREN -> ^( COLLIST ( colspec )+ ) ;
    public final MemgrammarParser.collist_return collist() throws RecognitionException {
        MemgrammarParser.collist_return retval = new MemgrammarParser.collist_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LEFT_PAREN128=null;
        Token char_literal130=null;
        Token RIGHT_PAREN132=null;
        MemgrammarParser.colspec_return colspec129 =null;

        MemgrammarParser.colspec_return colspec131 =null;


        CommonTree LEFT_PAREN128_tree=null;
        CommonTree char_literal130_tree=null;
        CommonTree RIGHT_PAREN132_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleSubtreeStream stream_colspec=new RewriteRuleSubtreeStream(adaptor,"rule colspec");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:9: ( LEFT_PAREN colspec ( ',' colspec )* RIGHT_PAREN -> ^( COLLIST ( colspec )+ ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:11: LEFT_PAREN colspec ( ',' colspec )* RIGHT_PAREN
            {
            LEFT_PAREN128=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_collist7004);  
            stream_LEFT_PAREN.add(LEFT_PAREN128);


            pushFollow(FOLLOW_colspec_in_collist7006);
            colspec129=colspec();

            state._fsp--;

            stream_colspec.add(colspec129.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:30: ( ',' colspec )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==163) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:31: ',' colspec
            	    {
            	    char_literal130=(Token)match(input,163,FOLLOW_163_in_collist7009);  
            	    stream_163.add(char_literal130);


            	    pushFollow(FOLLOW_colspec_in_collist7011);
            	    colspec131=colspec();

            	    state._fsp--;

            	    stream_colspec.add(colspec131.getTree());

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            RIGHT_PAREN132=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_collist7015);  
            stream_RIGHT_PAREN.add(RIGHT_PAREN132);


            // AST REWRITE
            // elements: colspec
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 349:57: -> ^( COLLIST ( colspec )+ )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:349:60: ^( COLLIST ( colspec )+ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(COLLIST, "COLLIST")
                , root_1);

                if ( !(stream_colspec.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_colspec.hasNext() ) {
                    adaptor.addChild(root_1, stream_colspec.nextTree());

                }
                stream_colspec.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "collist"


    public static class literal_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "literal"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:351:1: literal : ( INTEGER | FLOAT | STRING | BOOLEAN | DATETIME | NULL );
    public final MemgrammarParser.literal_return literal() throws RecognitionException {
        MemgrammarParser.literal_return retval = new MemgrammarParser.literal_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set133=null;

        CommonTree set133_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:351:8: ( INTEGER | FLOAT | STRING | BOOLEAN | DATETIME | NULL )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set133=(Token)input.LT(1);

            if ( input.LA(1)==BOOLEAN||input.LA(1)==DATETIME||input.LA(1)==FLOAT||input.LA(1)==INTEGER||input.LA(1)==NULL||input.LA(1)==STRING ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set133)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "literal"


    public static class columnType_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "columnType"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:353:1: columnType : ( INTEGERTYPE | VARCHARTYPE | FLOATTYPE | DATETYPE | DATETIMETYPE | DECIMALTYPE | BIGINTTYPE );
    public final MemgrammarParser.columnType_return columnType() throws RecognitionException {
        MemgrammarParser.columnType_return retval = new MemgrammarParser.columnType_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set134=null;

        CommonTree set134_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:354:2: ( INTEGERTYPE | VARCHARTYPE | FLOATTYPE | DATETYPE | DATETIMETYPE | DECIMALTYPE | BIGINTTYPE )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set134=(Token)input.LT(1);

            if ( input.LA(1)==BIGINTTYPE||(input.LA(1) >= DATETIMETYPE && input.LA(1) <= DATETYPE)||input.LA(1)==DECIMALTYPE||input.LA(1)==FLOATTYPE||input.LA(1)==INTEGERTYPE||input.LA(1)==VARCHARTYPE ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set134)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "columnType"


    public static class columnDefinition_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "columnDefinition"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:356:1: columnDefinition : IDENT columnType ( LEFT_PAREN INTEGER RIGHT_PAREN )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? -> ^( IDENT columnType ( INTEGER )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ) ;
    public final MemgrammarParser.columnDefinition_return columnDefinition() throws RecognitionException {
        MemgrammarParser.columnDefinition_return retval = new MemgrammarParser.columnDefinition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENT135=null;
        Token LEFT_PAREN137=null;
        Token INTEGER138=null;
        Token RIGHT_PAREN139=null;
        Token PRIMARYKEY140=null;
        Token FOREIGNKEY141=null;
        Token NOINDEX142=null;
        Token TRANSIENT143=null;
        Token DISK144=null;
        Token MEMORY145=null;
        Token COMPRESSED146=null;
        Token UNCOMPRESSED147=null;
        MemgrammarParser.columnType_return columnType136 =null;


        CommonTree IDENT135_tree=null;
        CommonTree LEFT_PAREN137_tree=null;
        CommonTree INTEGER138_tree=null;
        CommonTree RIGHT_PAREN139_tree=null;
        CommonTree PRIMARYKEY140_tree=null;
        CommonTree FOREIGNKEY141_tree=null;
        CommonTree NOINDEX142_tree=null;
        CommonTree TRANSIENT143_tree=null;
        CommonTree DISK144_tree=null;
        CommonTree MEMORY145_tree=null;
        CommonTree COMPRESSED146_tree=null;
        CommonTree UNCOMPRESSED147_tree=null;
        RewriteRuleTokenStream stream_INTEGER=new RewriteRuleTokenStream(adaptor,"token INTEGER");
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_MEMORY=new RewriteRuleTokenStream(adaptor,"token MEMORY");
        RewriteRuleTokenStream stream_NOINDEX=new RewriteRuleTokenStream(adaptor,"token NOINDEX");
        RewriteRuleTokenStream stream_UNCOMPRESSED=new RewriteRuleTokenStream(adaptor,"token UNCOMPRESSED");
        RewriteRuleTokenStream stream_COMPRESSED=new RewriteRuleTokenStream(adaptor,"token COMPRESSED");
        RewriteRuleTokenStream stream_PRIMARYKEY=new RewriteRuleTokenStream(adaptor,"token PRIMARYKEY");
        RewriteRuleTokenStream stream_DISK=new RewriteRuleTokenStream(adaptor,"token DISK");
        RewriteRuleTokenStream stream_FOREIGNKEY=new RewriteRuleTokenStream(adaptor,"token FOREIGNKEY");
        RewriteRuleTokenStream stream_TRANSIENT=new RewriteRuleTokenStream(adaptor,"token TRANSIENT");
        RewriteRuleSubtreeStream stream_columnType=new RewriteRuleSubtreeStream(adaptor,"rule columnType");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:2: ( IDENT columnType ( LEFT_PAREN INTEGER RIGHT_PAREN )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? -> ^( IDENT columnType ( INTEGER )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:4: IDENT columnType ( LEFT_PAREN INTEGER RIGHT_PAREN )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )?
            {
            IDENT135=(Token)match(input,IDENT,FOLLOW_IDENT_in_columnDefinition7099);  
            stream_IDENT.add(IDENT135);


            pushFollow(FOLLOW_columnType_in_columnDefinition7101);
            columnType136=columnType();

            state._fsp--;

            stream_columnType.add(columnType136.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:21: ( LEFT_PAREN INTEGER RIGHT_PAREN )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==LEFT_PAREN) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:22: LEFT_PAREN INTEGER RIGHT_PAREN
                    {
                    LEFT_PAREN137=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_columnDefinition7104);  
                    stream_LEFT_PAREN.add(LEFT_PAREN137);


                    INTEGER138=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_columnDefinition7106);  
                    stream_INTEGER.add(INTEGER138);


                    RIGHT_PAREN139=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_columnDefinition7108);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN139);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:55: ( PRIMARYKEY )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==PRIMARYKEY) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:55: PRIMARYKEY
                    {
                    PRIMARYKEY140=(Token)match(input,PRIMARYKEY,FOLLOW_PRIMARYKEY_in_columnDefinition7112);  
                    stream_PRIMARYKEY.add(PRIMARYKEY140);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:67: ( FOREIGNKEY )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==FOREIGNKEY) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:67: FOREIGNKEY
                    {
                    FOREIGNKEY141=(Token)match(input,FOREIGNKEY,FOLLOW_FOREIGNKEY_in_columnDefinition7115);  
                    stream_FOREIGNKEY.add(FOREIGNKEY141);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:79: ( NOINDEX )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==NOINDEX) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:79: NOINDEX
                    {
                    NOINDEX142=(Token)match(input,NOINDEX,FOLLOW_NOINDEX_in_columnDefinition7118);  
                    stream_NOINDEX.add(NOINDEX142);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:88: ( TRANSIENT )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==TRANSIENT) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:88: TRANSIENT
                    {
                    TRANSIENT143=(Token)match(input,TRANSIENT,FOLLOW_TRANSIENT_in_columnDefinition7121);  
                    stream_TRANSIENT.add(TRANSIENT143);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:99: ( DISK )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==DISK) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:99: DISK
                    {
                    DISK144=(Token)match(input,DISK,FOLLOW_DISK_in_columnDefinition7124);  
                    stream_DISK.add(DISK144);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:105: ( MEMORY )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==MEMORY) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:105: MEMORY
                    {
                    MEMORY145=(Token)match(input,MEMORY,FOLLOW_MEMORY_in_columnDefinition7127);  
                    stream_MEMORY.add(MEMORY145);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:113: ( COMPRESSED )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==COMPRESSED) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:113: COMPRESSED
                    {
                    COMPRESSED146=(Token)match(input,COMPRESSED,FOLLOW_COMPRESSED_in_columnDefinition7130);  
                    stream_COMPRESSED.add(COMPRESSED146);


                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:125: ( UNCOMPRESSED )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==UNCOMPRESSED) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:357:125: UNCOMPRESSED
                    {
                    UNCOMPRESSED147=(Token)match(input,UNCOMPRESSED,FOLLOW_UNCOMPRESSED_in_columnDefinition7133);  
                    stream_UNCOMPRESSED.add(UNCOMPRESSED147);


                    }
                    break;

            }


            // AST REWRITE
            // elements: TRANSIENT, IDENT, FOREIGNKEY, COMPRESSED, MEMORY, DISK, columnType, NOINDEX, PRIMARYKEY, INTEGER, UNCOMPRESSED
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 357:138: -> ^( IDENT columnType ( INTEGER )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:3: ^( IDENT columnType ( INTEGER )? ( PRIMARYKEY )? ( FOREIGNKEY )? ( NOINDEX )? ( TRANSIENT )? ( DISK )? ( MEMORY )? ( COMPRESSED )? ( UNCOMPRESSED )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_IDENT.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_columnType.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:22: ( INTEGER )?
                if ( stream_INTEGER.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_INTEGER.nextNode()
                    );

                }
                stream_INTEGER.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:31: ( PRIMARYKEY )?
                if ( stream_PRIMARYKEY.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_PRIMARYKEY.nextNode()
                    );

                }
                stream_PRIMARYKEY.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:43: ( FOREIGNKEY )?
                if ( stream_FOREIGNKEY.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_FOREIGNKEY.nextNode()
                    );

                }
                stream_FOREIGNKEY.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:55: ( NOINDEX )?
                if ( stream_NOINDEX.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_NOINDEX.nextNode()
                    );

                }
                stream_NOINDEX.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:64: ( TRANSIENT )?
                if ( stream_TRANSIENT.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_TRANSIENT.nextNode()
                    );

                }
                stream_TRANSIENT.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:75: ( DISK )?
                if ( stream_DISK.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_DISK.nextNode()
                    );

                }
                stream_DISK.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:81: ( MEMORY )?
                if ( stream_MEMORY.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_MEMORY.nextNode()
                    );

                }
                stream_MEMORY.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:89: ( COMPRESSED )?
                if ( stream_COMPRESSED.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_COMPRESSED.nextNode()
                    );

                }
                stream_COMPRESSED.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:358:101: ( UNCOMPRESSED )?
                if ( stream_UNCOMPRESSED.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_UNCOMPRESSED.nextNode()
                    );

                }
                stream_UNCOMPRESSED.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "columnDefinition"


    public static class alterdef_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "alterdef"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:1: alterdef : ( ADD ( COLUMN )? columnDefinition -> ^( ADD columnDefinition ) | DROP ( COLUMN )? IDENT -> ^( DROP IDENT ) | ALTER ( COLUMN )? columnDefinition -> ^( ALTER columnDefinition ) );
    public final MemgrammarParser.alterdef_return alterdef() throws RecognitionException {
        MemgrammarParser.alterdef_return retval = new MemgrammarParser.alterdef_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ADD148=null;
        Token COLUMN149=null;
        Token DROP151=null;
        Token COLUMN152=null;
        Token IDENT153=null;
        Token ALTER154=null;
        Token COLUMN155=null;
        MemgrammarParser.columnDefinition_return columnDefinition150 =null;

        MemgrammarParser.columnDefinition_return columnDefinition156 =null;


        CommonTree ADD148_tree=null;
        CommonTree COLUMN149_tree=null;
        CommonTree DROP151_tree=null;
        CommonTree COLUMN152_tree=null;
        CommonTree IDENT153_tree=null;
        CommonTree ALTER154_tree=null;
        CommonTree COLUMN155_tree=null;
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");
        RewriteRuleTokenStream stream_ALTER=new RewriteRuleTokenStream(adaptor,"token ALTER");
        RewriteRuleTokenStream stream_DROP=new RewriteRuleTokenStream(adaptor,"token DROP");
        RewriteRuleTokenStream stream_ADD=new RewriteRuleTokenStream(adaptor,"token ADD");
        RewriteRuleTokenStream stream_COLUMN=new RewriteRuleTokenStream(adaptor,"token COLUMN");
        RewriteRuleSubtreeStream stream_columnDefinition=new RewriteRuleSubtreeStream(adaptor,"rule columnDefinition");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:9: ( ADD ( COLUMN )? columnDefinition -> ^( ADD columnDefinition ) | DROP ( COLUMN )? IDENT -> ^( DROP IDENT ) | ALTER ( COLUMN )? columnDefinition -> ^( ALTER columnDefinition ) )
            int alt44=3;
            switch ( input.LA(1) ) {
            case ADD:
                {
                alt44=1;
                }
                break;
            case DROP:
                {
                alt44=2;
                }
                break;
            case ALTER:
                {
                alt44=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;

            }

            switch (alt44) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:11: ADD ( COLUMN )? columnDefinition
                    {
                    ADD148=(Token)match(input,ADD,FOLLOW_ADD_in_alterdef7178);  
                    stream_ADD.add(ADD148);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:15: ( COLUMN )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==COLUMN) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:15: COLUMN
                            {
                            COLUMN149=(Token)match(input,COLUMN,FOLLOW_COLUMN_in_alterdef7180);  
                            stream_COLUMN.add(COLUMN149);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_columnDefinition_in_alterdef7183);
                    columnDefinition150=columnDefinition();

                    state._fsp--;

                    stream_columnDefinition.add(columnDefinition150.getTree());

                    // AST REWRITE
                    // elements: columnDefinition, ADD
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 360:40: -> ^( ADD columnDefinition )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:360:43: ^( ADD columnDefinition )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_ADD.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_columnDefinition.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:361:3: DROP ( COLUMN )? IDENT
                    {
                    DROP151=(Token)match(input,DROP,FOLLOW_DROP_in_alterdef7197);  
                    stream_DROP.add(DROP151);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:361:8: ( COLUMN )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0==COLUMN) ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:361:8: COLUMN
                            {
                            COLUMN152=(Token)match(input,COLUMN,FOLLOW_COLUMN_in_alterdef7199);  
                            stream_COLUMN.add(COLUMN152);


                            }
                            break;

                    }


                    IDENT153=(Token)match(input,IDENT,FOLLOW_IDENT_in_alterdef7202);  
                    stream_IDENT.add(IDENT153);


                    // AST REWRITE
                    // elements: IDENT, DROP
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 361:22: -> ^( DROP IDENT )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:361:25: ^( DROP IDENT )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_DROP.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_IDENT.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:362:3: ALTER ( COLUMN )? columnDefinition
                    {
                    ALTER154=(Token)match(input,ALTER,FOLLOW_ALTER_in_alterdef7216);  
                    stream_ALTER.add(ALTER154);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:362:9: ( COLUMN )?
                    int alt43=2;
                    int LA43_0 = input.LA(1);

                    if ( (LA43_0==COLUMN) ) {
                        alt43=1;
                    }
                    switch (alt43) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:362:9: COLUMN
                            {
                            COLUMN155=(Token)match(input,COLUMN,FOLLOW_COLUMN_in_alterdef7218);  
                            stream_COLUMN.add(COLUMN155);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_columnDefinition_in_alterdef7221);
                    columnDefinition156=columnDefinition();

                    state._fsp--;

                    stream_columnDefinition.add(columnDefinition156.getTree());

                    // AST REWRITE
                    // elements: ALTER, columnDefinition
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 362:34: -> ^( ALTER columnDefinition )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:362:37: ^( ALTER columnDefinition )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_ALTER.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_columnDefinition.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "alterdef"


    public static class primarykeyspec_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "primarykeyspec"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:364:1: primarykeyspec : PRIMARYKEY IDENT ( ',' IDENT )* -> ^( PRIMARYKEY IDENT ( IDENT )* ) ;
    public final MemgrammarParser.primarykeyspec_return primarykeyspec() throws RecognitionException {
        MemgrammarParser.primarykeyspec_return retval = new MemgrammarParser.primarykeyspec_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PRIMARYKEY157=null;
        Token IDENT158=null;
        Token char_literal159=null;
        Token IDENT160=null;

        CommonTree PRIMARYKEY157_tree=null;
        CommonTree IDENT158_tree=null;
        CommonTree char_literal159_tree=null;
        CommonTree IDENT160_tree=null;
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");
        RewriteRuleTokenStream stream_PRIMARYKEY=new RewriteRuleTokenStream(adaptor,"token PRIMARYKEY");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:2: ( PRIMARYKEY IDENT ( ',' IDENT )* -> ^( PRIMARYKEY IDENT ( IDENT )* ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:4: PRIMARYKEY IDENT ( ',' IDENT )*
            {
            PRIMARYKEY157=(Token)match(input,PRIMARYKEY,FOLLOW_PRIMARYKEY_in_primarykeyspec7238);  
            stream_PRIMARYKEY.add(PRIMARYKEY157);


            IDENT158=(Token)match(input,IDENT,FOLLOW_IDENT_in_primarykeyspec7240);  
            stream_IDENT.add(IDENT158);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:21: ( ',' IDENT )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==163) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:22: ',' IDENT
            	    {
            	    char_literal159=(Token)match(input,163,FOLLOW_163_in_primarykeyspec7243);  
            	    stream_163.add(char_literal159);


            	    IDENT160=(Token)match(input,IDENT,FOLLOW_IDENT_in_primarykeyspec7245);  
            	    stream_IDENT.add(IDENT160);


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);


            // AST REWRITE
            // elements: IDENT, IDENT, PRIMARYKEY
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 365:34: -> ^( PRIMARYKEY IDENT ( IDENT )* )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:37: ^( PRIMARYKEY IDENT ( IDENT )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_PRIMARYKEY.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_IDENT.nextNode()
                );

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:365:56: ( IDENT )*
                while ( stream_IDENT.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_IDENT.nextNode()
                    );

                }
                stream_IDENT.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "primarykeyspec"


    public static class showquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "showquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:367:1: showquery : ( SHOW ^ TABLES | SHOW ^ PROCESSLIST | SHOW ^ SURROGATEKEYS | SHOW ^ SYSTEMSTATUS | SHOW TABLE ( schema '.' )? ident ( DEBUG )? -> ^( SHOW ident ( schema )? ( DEBUG )? ) );
    public final MemgrammarParser.showquery_return showquery() throws RecognitionException {
        MemgrammarParser.showquery_return retval = new MemgrammarParser.showquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SHOW161=null;
        Token TABLES162=null;
        Token SHOW163=null;
        Token PROCESSLIST164=null;
        Token SHOW165=null;
        Token SURROGATEKEYS166=null;
        Token SHOW167=null;
        Token SYSTEMSTATUS168=null;
        Token SHOW169=null;
        Token TABLE170=null;
        Token char_literal172=null;
        Token DEBUG174=null;
        MemgrammarParser.schema_return schema171 =null;

        MemgrammarParser.ident_return ident173 =null;


        CommonTree SHOW161_tree=null;
        CommonTree TABLES162_tree=null;
        CommonTree SHOW163_tree=null;
        CommonTree PROCESSLIST164_tree=null;
        CommonTree SHOW165_tree=null;
        CommonTree SURROGATEKEYS166_tree=null;
        CommonTree SHOW167_tree=null;
        CommonTree SYSTEMSTATUS168_tree=null;
        CommonTree SHOW169_tree=null;
        CommonTree TABLE170_tree=null;
        CommonTree char_literal172_tree=null;
        CommonTree DEBUG174_tree=null;
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_SHOW=new RewriteRuleTokenStream(adaptor,"token SHOW");
        RewriteRuleTokenStream stream_DEBUG=new RewriteRuleTokenStream(adaptor,"token DEBUG");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:368:2: ( SHOW ^ TABLES | SHOW ^ PROCESSLIST | SHOW ^ SURROGATEKEYS | SHOW ^ SYSTEMSTATUS | SHOW TABLE ( schema '.' )? ident ( DEBUG )? -> ^( SHOW ident ( schema )? ( DEBUG )? ) )
            int alt48=5;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==SHOW) ) {
                switch ( input.LA(2) ) {
                case TABLES:
                    {
                    alt48=1;
                    }
                    break;
                case PROCESSLIST:
                    {
                    alt48=2;
                    }
                    break;
                case SURROGATEKEYS:
                    {
                    alt48=3;
                    }
                    break;
                case SYSTEMSTATUS:
                    {
                    alt48=4;
                    }
                    break;
                case TABLE:
                    {
                    alt48=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 48, 1, input);

                    throw nvae;

                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;

            }
            switch (alt48) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:368:4: SHOW ^ TABLES
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    SHOW161=(Token)match(input,SHOW,FOLLOW_SHOW_in_showquery7267); 
                    SHOW161_tree = 
                    (CommonTree)adaptor.create(SHOW161)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(SHOW161_tree, root_0);


                    TABLES162=(Token)match(input,TABLES,FOLLOW_TABLES_in_showquery7270); 
                    TABLES162_tree = 
                    (CommonTree)adaptor.create(TABLES162)
                    ;
                    adaptor.addChild(root_0, TABLES162_tree);


                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:368:19: SHOW ^ PROCESSLIST
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    SHOW163=(Token)match(input,SHOW,FOLLOW_SHOW_in_showquery7274); 
                    SHOW163_tree = 
                    (CommonTree)adaptor.create(SHOW163)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(SHOW163_tree, root_0);


                    PROCESSLIST164=(Token)match(input,PROCESSLIST,FOLLOW_PROCESSLIST_in_showquery7277); 
                    PROCESSLIST164_tree = 
                    (CommonTree)adaptor.create(PROCESSLIST164)
                    ;
                    adaptor.addChild(root_0, PROCESSLIST164_tree);


                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:368:39: SHOW ^ SURROGATEKEYS
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    SHOW165=(Token)match(input,SHOW,FOLLOW_SHOW_in_showquery7281); 
                    SHOW165_tree = 
                    (CommonTree)adaptor.create(SHOW165)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(SHOW165_tree, root_0);


                    SURROGATEKEYS166=(Token)match(input,SURROGATEKEYS,FOLLOW_SURROGATEKEYS_in_showquery7284); 
                    SURROGATEKEYS166_tree = 
                    (CommonTree)adaptor.create(SURROGATEKEYS166)
                    ;
                    adaptor.addChild(root_0, SURROGATEKEYS166_tree);


                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:368:61: SHOW ^ SYSTEMSTATUS
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    SHOW167=(Token)match(input,SHOW,FOLLOW_SHOW_in_showquery7288); 
                    SHOW167_tree = 
                    (CommonTree)adaptor.create(SHOW167)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(SHOW167_tree, root_0);


                    SYSTEMSTATUS168=(Token)match(input,SYSTEMSTATUS,FOLLOW_SYSTEMSTATUS_in_showquery7291); 
                    SYSTEMSTATUS168_tree = 
                    (CommonTree)adaptor.create(SYSTEMSTATUS168)
                    ;
                    adaptor.addChild(root_0, SYSTEMSTATUS168_tree);


                    }
                    break;
                case 5 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:3: SHOW TABLE ( schema '.' )? ident ( DEBUG )?
                    {
                    SHOW169=(Token)match(input,SHOW,FOLLOW_SHOW_in_showquery7297);  
                    stream_SHOW.add(SHOW169);


                    TABLE170=(Token)match(input,TABLE,FOLLOW_TABLE_in_showquery7299);  
                    stream_TABLE.add(TABLE170);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:14: ( schema '.' )?
                    int alt46=2;
                    int LA46_0 = input.LA(1);

                    if ( (LA46_0==IDENT) ) {
                        int LA46_1 = input.LA(2);

                        if ( (LA46_1==164) ) {
                            alt46=1;
                        }
                    }
                    switch (alt46) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:15: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_showquery7302);
                            schema171=schema();

                            state._fsp--;

                            stream_schema.add(schema171.getTree());

                            char_literal172=(Token)match(input,164,FOLLOW_164_in_showquery7304);  
                            stream_164.add(char_literal172);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_showquery7308);
                    ident173=ident();

                    state._fsp--;

                    stream_ident.add(ident173.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:34: ( DEBUG )?
                    int alt47=2;
                    int LA47_0 = input.LA(1);

                    if ( (LA47_0==DEBUG) ) {
                        alt47=1;
                    }
                    switch (alt47) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:34: DEBUG
                            {
                            DEBUG174=(Token)match(input,DEBUG,FOLLOW_DEBUG_in_showquery7310);  
                            stream_DEBUG.add(DEBUG174);


                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: schema, SHOW, ident, DEBUG
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 369:41: -> ^( SHOW ident ( schema )? ( DEBUG )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:44: ^( SHOW ident ( schema )? ( DEBUG )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_SHOW.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:57: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_1, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:369:65: ( DEBUG )?
                        if ( stream_DEBUG.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_DEBUG.nextNode()
                            );

                        }
                        stream_DEBUG.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "showquery"


    public static class dropquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dropquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:371:1: dropquery : ( DROP TABLE ( schema '.' )? IDENT ( IFEXISTS )? -> ^( DROP ^( IDENT ( schema )? ) ( IFEXISTS )? ) | TRUNCATE TABLE ( schema '.' )? IDENT ( IFEXISTS )? -> ^( TRUNCATE ^( IDENT ( schema )? ) ( IFEXISTS )? ) );
    public final MemgrammarParser.dropquery_return dropquery() throws RecognitionException {
        MemgrammarParser.dropquery_return retval = new MemgrammarParser.dropquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DROP175=null;
        Token TABLE176=null;
        Token char_literal178=null;
        Token IDENT179=null;
        Token IFEXISTS180=null;
        Token TRUNCATE181=null;
        Token TABLE182=null;
        Token char_literal184=null;
        Token IDENT185=null;
        Token IFEXISTS186=null;
        MemgrammarParser.schema_return schema177 =null;

        MemgrammarParser.schema_return schema183 =null;


        CommonTree DROP175_tree=null;
        CommonTree TABLE176_tree=null;
        CommonTree char_literal178_tree=null;
        CommonTree IDENT179_tree=null;
        CommonTree IFEXISTS180_tree=null;
        CommonTree TRUNCATE181_tree=null;
        CommonTree TABLE182_tree=null;
        CommonTree char_literal184_tree=null;
        CommonTree IDENT185_tree=null;
        CommonTree IFEXISTS186_tree=null;
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_IFEXISTS=new RewriteRuleTokenStream(adaptor,"token IFEXISTS");
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_DROP=new RewriteRuleTokenStream(adaptor,"token DROP");
        RewriteRuleTokenStream stream_TRUNCATE=new RewriteRuleTokenStream(adaptor,"token TRUNCATE");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:2: ( DROP TABLE ( schema '.' )? IDENT ( IFEXISTS )? -> ^( DROP ^( IDENT ( schema )? ) ( IFEXISTS )? ) | TRUNCATE TABLE ( schema '.' )? IDENT ( IFEXISTS )? -> ^( TRUNCATE ^( IDENT ( schema )? ) ( IFEXISTS )? ) )
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==DROP) ) {
                alt53=1;
            }
            else if ( (LA53_0==TRUNCATE) ) {
                alt53=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;

            }
            switch (alt53) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:4: DROP TABLE ( schema '.' )? IDENT ( IFEXISTS )?
                    {
                    DROP175=(Token)match(input,DROP,FOLLOW_DROP_in_dropquery7335);  
                    stream_DROP.add(DROP175);


                    TABLE176=(Token)match(input,TABLE,FOLLOW_TABLE_in_dropquery7337);  
                    stream_TABLE.add(TABLE176);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:15: ( schema '.' )?
                    int alt49=2;
                    int LA49_0 = input.LA(1);

                    if ( (LA49_0==IDENT) ) {
                        int LA49_1 = input.LA(2);

                        if ( (LA49_1==164) ) {
                            alt49=1;
                        }
                    }
                    switch (alt49) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:16: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_dropquery7340);
                            schema177=schema();

                            state._fsp--;

                            stream_schema.add(schema177.getTree());

                            char_literal178=(Token)match(input,164,FOLLOW_164_in_dropquery7342);  
                            stream_164.add(char_literal178);


                            }
                            break;

                    }


                    IDENT179=(Token)match(input,IDENT,FOLLOW_IDENT_in_dropquery7346);  
                    stream_IDENT.add(IDENT179);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:35: ( IFEXISTS )?
                    int alt50=2;
                    int LA50_0 = input.LA(1);

                    if ( (LA50_0==IFEXISTS) ) {
                        alt50=1;
                    }
                    switch (alt50) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:35: IFEXISTS
                            {
                            IFEXISTS180=(Token)match(input,IFEXISTS,FOLLOW_IFEXISTS_in_dropquery7348);  
                            stream_IFEXISTS.add(IFEXISTS180);


                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: IDENT, IFEXISTS, DROP, schema
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 372:45: -> ^( DROP ^( IDENT ( schema )? ) ( IFEXISTS )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:48: ^( DROP ^( IDENT ( schema )? ) ( IFEXISTS )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_DROP.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:55: ^( IDENT ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(
                        stream_IDENT.nextNode()
                        , root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:63: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:372:72: ( IFEXISTS )?
                        if ( stream_IFEXISTS.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_IFEXISTS.nextNode()
                            );

                        }
                        stream_IFEXISTS.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:3: TRUNCATE TABLE ( schema '.' )? IDENT ( IFEXISTS )?
                    {
                    TRUNCATE181=(Token)match(input,TRUNCATE,FOLLOW_TRUNCATE_in_dropquery7371);  
                    stream_TRUNCATE.add(TRUNCATE181);


                    TABLE182=(Token)match(input,TABLE,FOLLOW_TABLE_in_dropquery7373);  
                    stream_TABLE.add(TABLE182);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:18: ( schema '.' )?
                    int alt51=2;
                    int LA51_0 = input.LA(1);

                    if ( (LA51_0==IDENT) ) {
                        int LA51_1 = input.LA(2);

                        if ( (LA51_1==164) ) {
                            alt51=1;
                        }
                    }
                    switch (alt51) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:19: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_dropquery7376);
                            schema183=schema();

                            state._fsp--;

                            stream_schema.add(schema183.getTree());

                            char_literal184=(Token)match(input,164,FOLLOW_164_in_dropquery7378);  
                            stream_164.add(char_literal184);


                            }
                            break;

                    }


                    IDENT185=(Token)match(input,IDENT,FOLLOW_IDENT_in_dropquery7382);  
                    stream_IDENT.add(IDENT185);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:38: ( IFEXISTS )?
                    int alt52=2;
                    int LA52_0 = input.LA(1);

                    if ( (LA52_0==IFEXISTS) ) {
                        alt52=1;
                    }
                    switch (alt52) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:38: IFEXISTS
                            {
                            IFEXISTS186=(Token)match(input,IFEXISTS,FOLLOW_IFEXISTS_in_dropquery7384);  
                            stream_IFEXISTS.add(IFEXISTS186);


                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: IDENT, TRUNCATE, schema, IFEXISTS
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 373:48: -> ^( TRUNCATE ^( IDENT ( schema )? ) ( IFEXISTS )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:51: ^( TRUNCATE ^( IDENT ( schema )? ) ( IFEXISTS )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_TRUNCATE.nextNode()
                        , root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:62: ^( IDENT ( schema )? )
                        {
                        CommonTree root_2 = (CommonTree)adaptor.nil();
                        root_2 = (CommonTree)adaptor.becomeRoot(
                        stream_IDENT.nextNode()
                        , root_2);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:70: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_2, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        adaptor.addChild(root_1, root_2);
                        }

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:373:79: ( IFEXISTS )?
                        if ( stream_IFEXISTS.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_IFEXISTS.nextNode()
                            );

                        }
                        stream_IFEXISTS.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dropquery"


    public static class selectquery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "selectquery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:375:1: selectquery : SELECT ( top )? ( DISTINCT )? projectionlist ( intooutfile )? from ( where )? ( groupby )? ( having )? ( orderby )? -> ^( SELECT projectionlist from ( where )? ( groupby )? ( having )? ( orderby )? ( top )? ( DISTINCT )? ( intooutfile )? ) ;
    public final MemgrammarParser.selectquery_return selectquery() throws RecognitionException {
        MemgrammarParser.selectquery_return retval = new MemgrammarParser.selectquery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SELECT187=null;
        Token DISTINCT189=null;
        MemgrammarParser.top_return top188 =null;

        MemgrammarParser.projectionlist_return projectionlist190 =null;

        MemgrammarParser.intooutfile_return intooutfile191 =null;

        MemgrammarParser.from_return from192 =null;

        MemgrammarParser.where_return where193 =null;

        MemgrammarParser.groupby_return groupby194 =null;

        MemgrammarParser.having_return having195 =null;

        MemgrammarParser.orderby_return orderby196 =null;


        CommonTree SELECT187_tree=null;
        CommonTree DISTINCT189_tree=null;
        RewriteRuleTokenStream stream_SELECT=new RewriteRuleTokenStream(adaptor,"token SELECT");
        RewriteRuleTokenStream stream_DISTINCT=new RewriteRuleTokenStream(adaptor,"token DISTINCT");
        RewriteRuleSubtreeStream stream_intooutfile=new RewriteRuleSubtreeStream(adaptor,"rule intooutfile");
        RewriteRuleSubtreeStream stream_projectionlist=new RewriteRuleSubtreeStream(adaptor,"rule projectionlist");
        RewriteRuleSubtreeStream stream_groupby=new RewriteRuleSubtreeStream(adaptor,"rule groupby");
        RewriteRuleSubtreeStream stream_having=new RewriteRuleSubtreeStream(adaptor,"rule having");
        RewriteRuleSubtreeStream stream_orderby=new RewriteRuleSubtreeStream(adaptor,"rule orderby");
        RewriteRuleSubtreeStream stream_from=new RewriteRuleSubtreeStream(adaptor,"rule from");
        RewriteRuleSubtreeStream stream_where=new RewriteRuleSubtreeStream(adaptor,"rule where");
        RewriteRuleSubtreeStream stream_top=new RewriteRuleSubtreeStream(adaptor,"rule top");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:2: ( SELECT ( top )? ( DISTINCT )? projectionlist ( intooutfile )? from ( where )? ( groupby )? ( having )? ( orderby )? -> ^( SELECT projectionlist from ( where )? ( groupby )? ( having )? ( orderby )? ( top )? ( DISTINCT )? ( intooutfile )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:4: SELECT ( top )? ( DISTINCT )? projectionlist ( intooutfile )? from ( where )? ( groupby )? ( having )? ( orderby )?
            {
            SELECT187=(Token)match(input,SELECT,FOLLOW_SELECT_in_selectquery7410);  
            stream_SELECT.add(SELECT187);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:11: ( top )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==TOP) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:11: top
                    {
                    pushFollow(FOLLOW_top_in_selectquery7412);
                    top188=top();

                    state._fsp--;

                    stream_top.add(top188.getTree());

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:16: ( DISTINCT )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==DISTINCT) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:16: DISTINCT
                    {
                    DISTINCT189=(Token)match(input,DISTINCT,FOLLOW_DISTINCT_in_selectquery7415);  
                    stream_DISTINCT.add(DISTINCT189);


                    }
                    break;

            }


            pushFollow(FOLLOW_projectionlist_in_selectquery7418);
            projectionlist190=projectionlist();

            state._fsp--;

            stream_projectionlist.add(projectionlist190.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:41: ( intooutfile )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==INTO) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:41: intooutfile
                    {
                    pushFollow(FOLLOW_intooutfile_in_selectquery7420);
                    intooutfile191=intooutfile();

                    state._fsp--;

                    stream_intooutfile.add(intooutfile191.getTree());

                    }
                    break;

            }


            pushFollow(FOLLOW_from_in_selectquery7423);
            from192=from();

            state._fsp--;

            stream_from.add(from192.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:59: ( where )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==WHERE) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:59: where
                    {
                    pushFollow(FOLLOW_where_in_selectquery7425);
                    where193=where();

                    state._fsp--;

                    stream_where.add(where193.getTree());

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:66: ( groupby )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==GROUPBY) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:66: groupby
                    {
                    pushFollow(FOLLOW_groupby_in_selectquery7428);
                    groupby194=groupby();

                    state._fsp--;

                    stream_groupby.add(groupby194.getTree());

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:75: ( having )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==HAVING) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:75: having
                    {
                    pushFollow(FOLLOW_having_in_selectquery7431);
                    having195=having();

                    state._fsp--;

                    stream_having.add(having195.getTree());

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:83: ( orderby )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==ORDERBY) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:376:83: orderby
                    {
                    pushFollow(FOLLOW_orderby_in_selectquery7434);
                    orderby196=orderby();

                    state._fsp--;

                    stream_orderby.add(orderby196.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: from, having, top, groupby, where, DISTINCT, projectionlist, SELECT, intooutfile, orderby
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 377:3: -> ^( SELECT projectionlist from ( where )? ( groupby )? ( having )? ( orderby )? ( top )? ( DISTINCT )? ( intooutfile )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:6: ^( SELECT projectionlist from ( where )? ( groupby )? ( having )? ( orderby )? ( top )? ( DISTINCT )? ( intooutfile )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_SELECT.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_projectionlist.nextTree());

                adaptor.addChild(root_1, stream_from.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:35: ( where )?
                if ( stream_where.hasNext() ) {
                    adaptor.addChild(root_1, stream_where.nextTree());

                }
                stream_where.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:42: ( groupby )?
                if ( stream_groupby.hasNext() ) {
                    adaptor.addChild(root_1, stream_groupby.nextTree());

                }
                stream_groupby.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:51: ( having )?
                if ( stream_having.hasNext() ) {
                    adaptor.addChild(root_1, stream_having.nextTree());

                }
                stream_having.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:59: ( orderby )?
                if ( stream_orderby.hasNext() ) {
                    adaptor.addChild(root_1, stream_orderby.nextTree());

                }
                stream_orderby.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:68: ( top )?
                if ( stream_top.hasNext() ) {
                    adaptor.addChild(root_1, stream_top.nextTree());

                }
                stream_top.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:73: ( DISTINCT )?
                if ( stream_DISTINCT.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_DISTINCT.nextNode()
                    );

                }
                stream_DISTINCT.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:377:83: ( intooutfile )?
                if ( stream_intooutfile.hasNext() ) {
                    adaptor.addChild(root_1, stream_intooutfile.nextTree());

                }
                stream_intooutfile.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "selectquery"


    public static class deletequery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "deletequery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:379:1: deletequery : DELETE from ( where )? -> ^( DELETE from ( where )? ) ;
    public final MemgrammarParser.deletequery_return deletequery() throws RecognitionException {
        MemgrammarParser.deletequery_return retval = new MemgrammarParser.deletequery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DELETE197=null;
        MemgrammarParser.from_return from198 =null;

        MemgrammarParser.where_return where199 =null;


        CommonTree DELETE197_tree=null;
        RewriteRuleTokenStream stream_DELETE=new RewriteRuleTokenStream(adaptor,"token DELETE");
        RewriteRuleSubtreeStream stream_from=new RewriteRuleSubtreeStream(adaptor,"rule from");
        RewriteRuleSubtreeStream stream_where=new RewriteRuleSubtreeStream(adaptor,"rule where");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:2: ( DELETE from ( where )? -> ^( DELETE from ( where )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:4: DELETE from ( where )?
            {
            DELETE197=(Token)match(input,DELETE,FOLLOW_DELETE_in_deletequery7479);  
            stream_DELETE.add(DELETE197);


            pushFollow(FOLLOW_from_in_deletequery7481);
            from198=from();

            state._fsp--;

            stream_from.add(from198.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:16: ( where )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==WHERE) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:16: where
                    {
                    pushFollow(FOLLOW_where_in_deletequery7483);
                    where199=where();

                    state._fsp--;

                    stream_where.add(where199.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: where, from, DELETE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 380:23: -> ^( DELETE from ( where )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:26: ^( DELETE from ( where )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_DELETE.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_from.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:380:40: ( where )?
                if ( stream_where.hasNext() ) {
                    adaptor.addChild(root_1, stream_where.nextTree());

                }
                stream_where.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "deletequery"


    public static class updatequery_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "updatequery"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:382:1: updatequery : UPDATE ( tablespec )? SET ( colset ( ',' colset )* ) from ( where )? -> ^( UPDATE ^( SET colset ( colset )* ) from ( where )? ) ;
    public final MemgrammarParser.updatequery_return updatequery() throws RecognitionException {
        MemgrammarParser.updatequery_return retval = new MemgrammarParser.updatequery_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token UPDATE200=null;
        Token SET202=null;
        Token char_literal204=null;
        MemgrammarParser.tablespec_return tablespec201 =null;

        MemgrammarParser.colset_return colset203 =null;

        MemgrammarParser.colset_return colset205 =null;

        MemgrammarParser.from_return from206 =null;

        MemgrammarParser.where_return where207 =null;


        CommonTree UPDATE200_tree=null;
        CommonTree SET202_tree=null;
        CommonTree char_literal204_tree=null;
        RewriteRuleTokenStream stream_UPDATE=new RewriteRuleTokenStream(adaptor,"token UPDATE");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_SET=new RewriteRuleTokenStream(adaptor,"token SET");
        RewriteRuleSubtreeStream stream_colset=new RewriteRuleSubtreeStream(adaptor,"rule colset");
        RewriteRuleSubtreeStream stream_tablespec=new RewriteRuleSubtreeStream(adaptor,"rule tablespec");
        RewriteRuleSubtreeStream stream_from=new RewriteRuleSubtreeStream(adaptor,"rule from");
        RewriteRuleSubtreeStream stream_where=new RewriteRuleSubtreeStream(adaptor,"rule where");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:2: ( UPDATE ( tablespec )? SET ( colset ( ',' colset )* ) from ( where )? -> ^( UPDATE ^( SET colset ( colset )* ) from ( where )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:4: UPDATE ( tablespec )? SET ( colset ( ',' colset )* ) from ( where )?
            {
            UPDATE200=(Token)match(input,UPDATE,FOLLOW_UPDATE_in_updatequery7505);  
            stream_UPDATE.add(UPDATE200);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:11: ( tablespec )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==IDENT||LA62_0==INFOTABLES||LA62_0==LEFT_PAREN) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:11: tablespec
                    {
                    pushFollow(FOLLOW_tablespec_in_updatequery7507);
                    tablespec201=tablespec();

                    state._fsp--;

                    stream_tablespec.add(tablespec201.getTree());

                    }
                    break;

            }


            SET202=(Token)match(input,SET,FOLLOW_SET_in_updatequery7510);  
            stream_SET.add(SET202);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:26: ( colset ( ',' colset )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:27: colset ( ',' colset )*
            {
            pushFollow(FOLLOW_colset_in_updatequery7513);
            colset203=colset();

            state._fsp--;

            stream_colset.add(colset203.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:34: ( ',' colset )*
            loop63:
            do {
                int alt63=2;
                int LA63_0 = input.LA(1);

                if ( (LA63_0==163) ) {
                    alt63=1;
                }


                switch (alt63) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:35: ',' colset
            	    {
            	    char_literal204=(Token)match(input,163,FOLLOW_163_in_updatequery7516);  
            	    stream_163.add(char_literal204);


            	    pushFollow(FOLLOW_colset_in_updatequery7518);
            	    colset205=colset();

            	    state._fsp--;

            	    stream_colset.add(colset205.getTree());

            	    }
            	    break;

            	default :
            	    break loop63;
                }
            } while (true);


            }


            pushFollow(FOLLOW_from_in_updatequery7523);
            from206=from();

            state._fsp--;

            stream_from.add(from206.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:54: ( where )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==WHERE) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:54: where
                    {
                    pushFollow(FOLLOW_where_in_updatequery7525);
                    where207=where();

                    state._fsp--;

                    stream_where.add(where207.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: where, from, SET, colset, colset, UPDATE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 383:61: -> ^( UPDATE ^( SET colset ( colset )* ) from ( where )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:64: ^( UPDATE ^( SET colset ( colset )* ) from ( where )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_UPDATE.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:73: ^( SET colset ( colset )* )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(
                stream_SET.nextNode()
                , root_2);

                adaptor.addChild(root_2, stream_colset.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:86: ( colset )*
                while ( stream_colset.hasNext() ) {
                    adaptor.addChild(root_2, stream_colset.nextTree());

                }
                stream_colset.reset();

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_1, stream_from.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:383:100: ( where )?
                if ( stream_where.hasNext() ) {
                    adaptor.addChild(root_1, stream_where.nextTree());

                }
                stream_where.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "updatequery"


    public static class renametable_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "renametable"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:385:1: renametable : RENAME TABLE ( schema '.' )? ident TO ( schema '.' )? ident -> ^( RENAME ^( ident ( schema )? ) ^( ident ( schema )? ) ) ;
    public final MemgrammarParser.renametable_return renametable() throws RecognitionException {
        MemgrammarParser.renametable_return retval = new MemgrammarParser.renametable_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token RENAME208=null;
        Token TABLE209=null;
        Token char_literal211=null;
        Token TO213=null;
        Token char_literal215=null;
        MemgrammarParser.schema_return schema210 =null;

        MemgrammarParser.ident_return ident212 =null;

        MemgrammarParser.schema_return schema214 =null;

        MemgrammarParser.ident_return ident216 =null;


        CommonTree RENAME208_tree=null;
        CommonTree TABLE209_tree=null;
        CommonTree char_literal211_tree=null;
        CommonTree TO213_tree=null;
        CommonTree char_literal215_tree=null;
        RewriteRuleTokenStream stream_TABLE=new RewriteRuleTokenStream(adaptor,"token TABLE");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_TO=new RewriteRuleTokenStream(adaptor,"token TO");
        RewriteRuleTokenStream stream_RENAME=new RewriteRuleTokenStream(adaptor,"token RENAME");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:2: ( RENAME TABLE ( schema '.' )? ident TO ( schema '.' )? ident -> ^( RENAME ^( ident ( schema )? ) ^( ident ( schema )? ) ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:4: RENAME TABLE ( schema '.' )? ident TO ( schema '.' )? ident
            {
            RENAME208=(Token)match(input,RENAME,FOLLOW_RENAME_in_renametable7556);  
            stream_RENAME.add(RENAME208);


            TABLE209=(Token)match(input,TABLE,FOLLOW_TABLE_in_renametable7558);  
            stream_TABLE.add(TABLE209);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:17: ( schema '.' )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==IDENT) ) {
                int LA65_1 = input.LA(2);

                if ( (LA65_1==164) ) {
                    alt65=1;
                }
            }
            switch (alt65) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:18: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_renametable7561);
                    schema210=schema();

                    state._fsp--;

                    stream_schema.add(schema210.getTree());

                    char_literal211=(Token)match(input,164,FOLLOW_164_in_renametable7563);  
                    stream_164.add(char_literal211);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_renametable7567);
            ident212=ident();

            state._fsp--;

            stream_ident.add(ident212.getTree());

            TO213=(Token)match(input,TO,FOLLOW_TO_in_renametable7569);  
            stream_TO.add(TO213);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:40: ( schema '.' )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==IDENT) ) {
                int LA66_1 = input.LA(2);

                if ( (LA66_1==164) ) {
                    alt66=1;
                }
            }
            switch (alt66) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:41: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_renametable7572);
                    schema214=schema();

                    state._fsp--;

                    stream_schema.add(schema214.getTree());

                    char_literal215=(Token)match(input,164,FOLLOW_164_in_renametable7574);  
                    stream_164.add(char_literal215);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_renametable7578);
            ident216=ident();

            state._fsp--;

            stream_ident.add(ident216.getTree());

            // AST REWRITE
            // elements: schema, ident, schema, RENAME, ident
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 386:60: -> ^( RENAME ^( ident ( schema )? ) ^( ident ( schema )? ) )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:63: ^( RENAME ^( ident ( schema )? ) ^( ident ( schema )? ) )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_RENAME.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:72: ^( ident ( schema )? )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:80: ( schema )?
                if ( stream_schema.hasNext() ) {
                    adaptor.addChild(root_2, stream_schema.nextTree());

                }
                stream_schema.reset();

                adaptor.addChild(root_1, root_2);
                }

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:89: ^( ident ( schema )? )
                {
                CommonTree root_2 = (CommonTree)adaptor.nil();
                root_2 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_2);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:386:97: ( schema )?
                if ( stream_schema.hasNext() ) {
                    adaptor.addChild(root_2, stream_schema.nextTree());

                }
                stream_schema.reset();

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "renametable"


    public static class colset_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "colset"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:388:1: colset : colspec EQUALS logicalExpression -> ^( EQUALS colspec logicalExpression ) ;
    public final MemgrammarParser.colset_return colset() throws RecognitionException {
        MemgrammarParser.colset_return retval = new MemgrammarParser.colset_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EQUALS218=null;
        MemgrammarParser.colspec_return colspec217 =null;

        MemgrammarParser.logicalExpression_return logicalExpression219 =null;


        CommonTree EQUALS218_tree=null;
        RewriteRuleTokenStream stream_EQUALS=new RewriteRuleTokenStream(adaptor,"token EQUALS");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        RewriteRuleSubtreeStream stream_colspec=new RewriteRuleSubtreeStream(adaptor,"rule colspec");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:388:8: ( colspec EQUALS logicalExpression -> ^( EQUALS colspec logicalExpression ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:388:10: colspec EQUALS logicalExpression
            {
            pushFollow(FOLLOW_colspec_in_colset7607);
            colspec217=colspec();

            state._fsp--;

            stream_colspec.add(colspec217.getTree());

            EQUALS218=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_colset7609);  
            stream_EQUALS.add(EQUALS218);


            pushFollow(FOLLOW_logicalExpression_in_colset7611);
            logicalExpression219=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression219.getTree());

            // AST REWRITE
            // elements: colspec, logicalExpression, EQUALS
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 388:43: -> ^( EQUALS colspec logicalExpression )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:388:46: ^( EQUALS colspec logicalExpression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_EQUALS.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_colspec.nextTree());

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "colset"


    public static class intooutfile_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "intooutfile"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:390:1: intooutfile : INTO OUTFILE STRING -> ^( OUTFILE STRING ) ;
    public final MemgrammarParser.intooutfile_return intooutfile() throws RecognitionException {
        MemgrammarParser.intooutfile_return retval = new MemgrammarParser.intooutfile_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INTO220=null;
        Token OUTFILE221=null;
        Token STRING222=null;

        CommonTree INTO220_tree=null;
        CommonTree OUTFILE221_tree=null;
        CommonTree STRING222_tree=null;
        RewriteRuleTokenStream stream_OUTFILE=new RewriteRuleTokenStream(adaptor,"token OUTFILE");
        RewriteRuleTokenStream stream_INTO=new RewriteRuleTokenStream(adaptor,"token INTO");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:391:2: ( INTO OUTFILE STRING -> ^( OUTFILE STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:391:4: INTO OUTFILE STRING
            {
            INTO220=(Token)match(input,INTO,FOLLOW_INTO_in_intooutfile7632);  
            stream_INTO.add(INTO220);


            OUTFILE221=(Token)match(input,OUTFILE,FOLLOW_OUTFILE_in_intooutfile7634);  
            stream_OUTFILE.add(OUTFILE221);


            STRING222=(Token)match(input,STRING,FOLLOW_STRING_in_intooutfile7636);  
            stream_STRING.add(STRING222);


            // AST REWRITE
            // elements: OUTFILE, STRING
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 391:24: -> ^( OUTFILE STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:391:27: ^( OUTFILE STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_OUTFILE.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "intooutfile"


    public static class top_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "top"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:393:1: top : TOP ^ INTEGER ;
    public final MemgrammarParser.top_return top() throws RecognitionException {
        MemgrammarParser.top_return retval = new MemgrammarParser.top_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token TOP223=null;
        Token INTEGER224=null;

        CommonTree TOP223_tree=null;
        CommonTree INTEGER224_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:393:5: ( TOP ^ INTEGER )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:393:7: TOP ^ INTEGER
            {
            root_0 = (CommonTree)adaptor.nil();


            TOP223=(Token)match(input,TOP,FOLLOW_TOP_in_top7652); 
            TOP223_tree = 
            (CommonTree)adaptor.create(TOP223)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(TOP223_tree, root_0);


            INTEGER224=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_top7655); 
            INTEGER224_tree = 
            (CommonTree)adaptor.create(INTEGER224)
            ;
            adaptor.addChild(root_0, INTEGER224_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "top"


    public static class from_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "from"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:1: from : FROM tablespec ( distinctforkey )? ( fromjoin )* -> ^( FROM tablespec ( distinctforkey )? ( fromjoin )* ) ;
    public final MemgrammarParser.from_return from() throws RecognitionException {
        MemgrammarParser.from_return retval = new MemgrammarParser.from_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token FROM225=null;
        MemgrammarParser.tablespec_return tablespec226 =null;

        MemgrammarParser.distinctforkey_return distinctforkey227 =null;

        MemgrammarParser.fromjoin_return fromjoin228 =null;


        CommonTree FROM225_tree=null;
        RewriteRuleTokenStream stream_FROM=new RewriteRuleTokenStream(adaptor,"token FROM");
        RewriteRuleSubtreeStream stream_distinctforkey=new RewriteRuleSubtreeStream(adaptor,"rule distinctforkey");
        RewriteRuleSubtreeStream stream_tablespec=new RewriteRuleSubtreeStream(adaptor,"rule tablespec");
        RewriteRuleSubtreeStream stream_fromjoin=new RewriteRuleSubtreeStream(adaptor,"rule fromjoin");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:6: ( FROM tablespec ( distinctforkey )? ( fromjoin )* -> ^( FROM tablespec ( distinctforkey )? ( fromjoin )* ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:8: FROM tablespec ( distinctforkey )? ( fromjoin )*
            {
            FROM225=(Token)match(input,FROM,FOLLOW_FROM_in_from7664);  
            stream_FROM.add(FROM225);


            pushFollow(FOLLOW_tablespec_in_from7666);
            tablespec226=tablespec();

            state._fsp--;

            stream_tablespec.add(tablespec226.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:23: ( distinctforkey )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==DISTINCTFORKEY) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:23: distinctforkey
                    {
                    pushFollow(FOLLOW_distinctforkey_in_from7668);
                    distinctforkey227=distinctforkey();

                    state._fsp--;

                    stream_distinctforkey.add(distinctforkey227.getTree());

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:39: ( fromjoin )*
            loop68:
            do {
                int alt68=2;
                int LA68_0 = input.LA(1);

                if ( (LA68_0==INNERJOIN||LA68_0==LEFTOUTERJOIN||LA68_0==RIGHTOUTERJOIN) ) {
                    alt68=1;
                }


                switch (alt68) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:395:39: fromjoin
            	    {
            	    pushFollow(FOLLOW_fromjoin_in_from7671);
            	    fromjoin228=fromjoin();

            	    state._fsp--;

            	    stream_fromjoin.add(fromjoin228.getTree());

            	    }
            	    break;

            	default :
            	    break loop68;
                }
            } while (true);


            // AST REWRITE
            // elements: tablespec, distinctforkey, FROM, fromjoin
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 395:49: -> ^( FROM tablespec ( distinctforkey )? ( fromjoin )* )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:396:3: ^( FROM tablespec ( distinctforkey )? ( fromjoin )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_FROM.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_tablespec.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:396:20: ( distinctforkey )?
                if ( stream_distinctforkey.hasNext() ) {
                    adaptor.addChild(root_1, stream_distinctforkey.nextTree());

                }
                stream_distinctforkey.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:396:36: ( fromjoin )*
                while ( stream_fromjoin.hasNext() ) {
                    adaptor.addChild(root_1, stream_fromjoin.nextTree());

                }
                stream_fromjoin.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "from"


    public static class fromjoin_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "fromjoin"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:1: fromjoin : joindir ( MAXONE )? tablespec ( joinspec )? -> ^( joindir tablespec ( joinspec )? ( MAXONE )? ) ;
    public final MemgrammarParser.fromjoin_return fromjoin() throws RecognitionException {
        MemgrammarParser.fromjoin_return retval = new MemgrammarParser.fromjoin_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token MAXONE230=null;
        MemgrammarParser.joindir_return joindir229 =null;

        MemgrammarParser.tablespec_return tablespec231 =null;

        MemgrammarParser.joinspec_return joinspec232 =null;


        CommonTree MAXONE230_tree=null;
        RewriteRuleTokenStream stream_MAXONE=new RewriteRuleTokenStream(adaptor,"token MAXONE");
        RewriteRuleSubtreeStream stream_tablespec=new RewriteRuleSubtreeStream(adaptor,"rule tablespec");
        RewriteRuleSubtreeStream stream_joinspec=new RewriteRuleSubtreeStream(adaptor,"rule joinspec");
        RewriteRuleSubtreeStream stream_joindir=new RewriteRuleSubtreeStream(adaptor,"rule joindir");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:9: ( joindir ( MAXONE )? tablespec ( joinspec )? -> ^( joindir tablespec ( joinspec )? ( MAXONE )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:11: joindir ( MAXONE )? tablespec ( joinspec )?
            {
            pushFollow(FOLLOW_joindir_in_fromjoin7698);
            joindir229=joindir();

            state._fsp--;

            stream_joindir.add(joindir229.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:19: ( MAXONE )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==MAXONE) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:19: MAXONE
                    {
                    MAXONE230=(Token)match(input,MAXONE,FOLLOW_MAXONE_in_fromjoin7700);  
                    stream_MAXONE.add(MAXONE230);


                    }
                    break;

            }


            pushFollow(FOLLOW_tablespec_in_fromjoin7703);
            tablespec231=tablespec();

            state._fsp--;

            stream_tablespec.add(tablespec231.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:37: ( joinspec )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==ON) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:37: joinspec
                    {
                    pushFollow(FOLLOW_joinspec_in_fromjoin7705);
                    joinspec232=joinspec();

                    state._fsp--;

                    stream_joinspec.add(joinspec232.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: joinspec, joindir, MAXONE, tablespec
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 398:47: -> ^( joindir tablespec ( joinspec )? ( MAXONE )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:50: ^( joindir tablespec ( joinspec )? ( MAXONE )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(stream_joindir.nextNode(), root_1);

                adaptor.addChild(root_1, stream_tablespec.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:70: ( joinspec )?
                if ( stream_joinspec.hasNext() ) {
                    adaptor.addChild(root_1, stream_joinspec.nextTree());

                }
                stream_joinspec.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:398:80: ( MAXONE )?
                if ( stream_MAXONE.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_MAXONE.nextNode()
                    );

                }
                stream_MAXONE.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "fromjoin"


    public static class distinctforkey_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "distinctforkey"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:400:1: distinctforkey : DISTINCTFORKEY LEFT_PAREN logicalExpression ( ',' logicalExpression )* RIGHT_PAREN -> ^( DISTINCTFORKEY logicalExpression ( logicalExpression )* ) ;
    public final MemgrammarParser.distinctforkey_return distinctforkey() throws RecognitionException {
        MemgrammarParser.distinctforkey_return retval = new MemgrammarParser.distinctforkey_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DISTINCTFORKEY233=null;
        Token LEFT_PAREN234=null;
        Token char_literal236=null;
        Token RIGHT_PAREN238=null;
        MemgrammarParser.logicalExpression_return logicalExpression235 =null;

        MemgrammarParser.logicalExpression_return logicalExpression237 =null;


        CommonTree DISTINCTFORKEY233_tree=null;
        CommonTree LEFT_PAREN234_tree=null;
        CommonTree char_literal236_tree=null;
        CommonTree RIGHT_PAREN238_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_DISTINCTFORKEY=new RewriteRuleTokenStream(adaptor,"token DISTINCTFORKEY");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:401:2: ( DISTINCTFORKEY LEFT_PAREN logicalExpression ( ',' logicalExpression )* RIGHT_PAREN -> ^( DISTINCTFORKEY logicalExpression ( logicalExpression )* ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:401:4: DISTINCTFORKEY LEFT_PAREN logicalExpression ( ',' logicalExpression )* RIGHT_PAREN
            {
            DISTINCTFORKEY233=(Token)match(input,DISTINCTFORKEY,FOLLOW_DISTINCTFORKEY_in_distinctforkey7731);  
            stream_DISTINCTFORKEY.add(DISTINCTFORKEY233);


            LEFT_PAREN234=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_distinctforkey7733);  
            stream_LEFT_PAREN.add(LEFT_PAREN234);


            pushFollow(FOLLOW_logicalExpression_in_distinctforkey7735);
            logicalExpression235=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression235.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:401:48: ( ',' logicalExpression )*
            loop71:
            do {
                int alt71=2;
                int LA71_0 = input.LA(1);

                if ( (LA71_0==163) ) {
                    alt71=1;
                }


                switch (alt71) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:401:49: ',' logicalExpression
            	    {
            	    char_literal236=(Token)match(input,163,FOLLOW_163_in_distinctforkey7738);  
            	    stream_163.add(char_literal236);


            	    pushFollow(FOLLOW_logicalExpression_in_distinctforkey7740);
            	    logicalExpression237=logicalExpression();

            	    state._fsp--;

            	    stream_logicalExpression.add(logicalExpression237.getTree());

            	    }
            	    break;

            	default :
            	    break loop71;
                }
            } while (true);


            RIGHT_PAREN238=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_distinctforkey7744);  
            stream_RIGHT_PAREN.add(RIGHT_PAREN238);


            // AST REWRITE
            // elements: DISTINCTFORKEY, logicalExpression, logicalExpression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 401:85: -> ^( DISTINCTFORKEY logicalExpression ( logicalExpression )* )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:402:3: ^( DISTINCTFORKEY logicalExpression ( logicalExpression )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_DISTINCTFORKEY.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:402:38: ( logicalExpression )*
                while ( stream_logicalExpression.hasNext() ) {
                    adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                }
                stream_logicalExpression.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "distinctforkey"


    public static class joindir_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joindir"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:404:1: joindir : ( INNERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN );
    public final MemgrammarParser.joindir_return joindir() throws RecognitionException {
        MemgrammarParser.joindir_return retval = new MemgrammarParser.joindir_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set239=null;

        CommonTree set239_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:404:9: ( INNERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set239=(Token)input.LT(1);

            if ( input.LA(1)==INNERJOIN||input.LA(1)==LEFTOUTERJOIN||input.LA(1)==RIGHTOUTERJOIN ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set239)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joindir"


    public static class tablespec_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "tablespec"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:406:1: tablespec : ( INFOTABLES -> ^( INFOTABLES ) | ( schema '.' )? ident ( tablealias )? -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? ) | LEFT_PAREN selectquery RIGHT_PAREN tablealias -> ^( SUBQUERY selectquery tablealias ) | LEFT_PAREN ( schema '.' )? ident RIGHT_PAREN ( tablealias )? -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? ) );
    public final MemgrammarParser.tablespec_return tablespec() throws RecognitionException {
        MemgrammarParser.tablespec_return retval = new MemgrammarParser.tablespec_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INFOTABLES240=null;
        Token char_literal242=null;
        Token LEFT_PAREN245=null;
        Token RIGHT_PAREN247=null;
        Token LEFT_PAREN249=null;
        Token char_literal251=null;
        Token RIGHT_PAREN253=null;
        MemgrammarParser.schema_return schema241 =null;

        MemgrammarParser.ident_return ident243 =null;

        MemgrammarParser.tablealias_return tablealias244 =null;

        MemgrammarParser.selectquery_return selectquery246 =null;

        MemgrammarParser.tablealias_return tablealias248 =null;

        MemgrammarParser.schema_return schema250 =null;

        MemgrammarParser.ident_return ident252 =null;

        MemgrammarParser.tablealias_return tablealias254 =null;


        CommonTree INFOTABLES240_tree=null;
        CommonTree char_literal242_tree=null;
        CommonTree LEFT_PAREN245_tree=null;
        CommonTree RIGHT_PAREN247_tree=null;
        CommonTree LEFT_PAREN249_tree=null;
        CommonTree char_literal251_tree=null;
        CommonTree RIGHT_PAREN253_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_INFOTABLES=new RewriteRuleTokenStream(adaptor,"token INFOTABLES");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_tablealias=new RewriteRuleSubtreeStream(adaptor,"rule tablealias");
        RewriteRuleSubtreeStream stream_selectquery=new RewriteRuleSubtreeStream(adaptor,"rule selectquery");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:406:10: ( INFOTABLES -> ^( INFOTABLES ) | ( schema '.' )? ident ( tablealias )? -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? ) | LEFT_PAREN selectquery RIGHT_PAREN tablealias -> ^( SUBQUERY selectquery tablealias ) | LEFT_PAREN ( schema '.' )? ident RIGHT_PAREN ( tablealias )? -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? ) )
            int alt76=4;
            switch ( input.LA(1) ) {
            case INFOTABLES:
                {
                alt76=1;
                }
                break;
            case IDENT:
                {
                alt76=2;
                }
                break;
            case LEFT_PAREN:
                {
                int LA76_3 = input.LA(2);

                if ( (LA76_3==SELECT) ) {
                    alt76=3;
                }
                else if ( (LA76_3==IDENT) ) {
                    alt76=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 76, 3, input);

                    throw nvae;

                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;

            }

            switch (alt76) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:406:12: INFOTABLES
                    {
                    INFOTABLES240=(Token)match(input,INFOTABLES,FOLLOW_INFOTABLES_in_tablespec7778);  
                    stream_INFOTABLES.add(INFOTABLES240);


                    // AST REWRITE
                    // elements: INFOTABLES
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 406:23: -> ^( INFOTABLES )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:406:26: ^( INFOTABLES )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_INFOTABLES.nextNode()
                        , root_1);

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:3: ( schema '.' )? ident ( tablealias )?
                    {
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:3: ( schema '.' )?
                    int alt72=2;
                    int LA72_0 = input.LA(1);

                    if ( (LA72_0==IDENT) ) {
                        int LA72_1 = input.LA(2);

                        if ( (LA72_1==164) ) {
                            alt72=1;
                        }
                    }
                    switch (alt72) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:4: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_tablespec7792);
                            schema241=schema();

                            state._fsp--;

                            stream_schema.add(schema241.getTree());

                            char_literal242=(Token)match(input,164,FOLLOW_164_in_tablespec7794);  
                            stream_164.add(char_literal242);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_tablespec7798);
                    ident243=ident();

                    state._fsp--;

                    stream_ident.add(ident243.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:23: ( tablealias )?
                    int alt73=2;
                    int LA73_0 = input.LA(1);

                    if ( (LA73_0==IDENT) ) {
                        alt73=1;
                    }
                    switch (alt73) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:23: tablealias
                            {
                            pushFollow(FOLLOW_tablealias_in_tablespec7800);
                            tablealias244=tablealias();

                            state._fsp--;

                            stream_tablealias.add(tablealias244.getTree());

                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: ident, tablealias, schema
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 407:35: -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:38: ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:46: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_1, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:54: ( ^( TABLEALIAS tablealias ) )?
                        if ( stream_tablealias.hasNext() ) {
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:407:54: ^( TABLEALIAS tablealias )
                            {
                            CommonTree root_2 = (CommonTree)adaptor.nil();
                            root_2 = (CommonTree)adaptor.becomeRoot(
                            (CommonTree)adaptor.create(TABLEALIAS, "TABLEALIAS")
                            , root_2);

                            adaptor.addChild(root_2, stream_tablealias.nextTree());

                            adaptor.addChild(root_1, root_2);
                            }

                        }
                        stream_tablealias.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:408:3: LEFT_PAREN selectquery RIGHT_PAREN tablealias
                    {
                    LEFT_PAREN245=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_tablespec7823);  
                    stream_LEFT_PAREN.add(LEFT_PAREN245);


                    pushFollow(FOLLOW_selectquery_in_tablespec7825);
                    selectquery246=selectquery();

                    state._fsp--;

                    stream_selectquery.add(selectquery246.getTree());

                    RIGHT_PAREN247=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_tablespec7827);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN247);


                    pushFollow(FOLLOW_tablealias_in_tablespec7829);
                    tablealias248=tablealias();

                    state._fsp--;

                    stream_tablealias.add(tablealias248.getTree());

                    // AST REWRITE
                    // elements: selectquery, tablealias
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 408:49: -> ^( SUBQUERY selectquery tablealias )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:408:52: ^( SUBQUERY selectquery tablealias )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(SUBQUERY, "SUBQUERY")
                        , root_1);

                        adaptor.addChild(root_1, stream_selectquery.nextTree());

                        adaptor.addChild(root_1, stream_tablealias.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:3: LEFT_PAREN ( schema '.' )? ident RIGHT_PAREN ( tablealias )?
                    {
                    LEFT_PAREN249=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_tablespec7845);  
                    stream_LEFT_PAREN.add(LEFT_PAREN249);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:14: ( schema '.' )?
                    int alt74=2;
                    int LA74_0 = input.LA(1);

                    if ( (LA74_0==IDENT) ) {
                        int LA74_1 = input.LA(2);

                        if ( (LA74_1==164) ) {
                            alt74=1;
                        }
                    }
                    switch (alt74) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:15: schema '.'
                            {
                            pushFollow(FOLLOW_schema_in_tablespec7848);
                            schema250=schema();

                            state._fsp--;

                            stream_schema.add(schema250.getTree());

                            char_literal251=(Token)match(input,164,FOLLOW_164_in_tablespec7850);  
                            stream_164.add(char_literal251);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_ident_in_tablespec7854);
                    ident252=ident();

                    state._fsp--;

                    stream_ident.add(ident252.getTree());

                    RIGHT_PAREN253=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_tablespec7856);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN253);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:46: ( tablealias )?
                    int alt75=2;
                    int LA75_0 = input.LA(1);

                    if ( (LA75_0==IDENT) ) {
                        alt75=1;
                    }
                    switch (alt75) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:46: tablealias
                            {
                            pushFollow(FOLLOW_tablealias_in_tablespec7858);
                            tablealias254=tablealias();

                            state._fsp--;

                            stream_tablealias.add(tablealias254.getTree());

                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: schema, tablealias, ident
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 409:58: -> ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:61: ^( ident ( schema )? ( ^( TABLEALIAS tablealias ) )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(stream_ident.nextNode(), root_1);

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:69: ( schema )?
                        if ( stream_schema.hasNext() ) {
                            adaptor.addChild(root_1, stream_schema.nextTree());

                        }
                        stream_schema.reset();

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:77: ( ^( TABLEALIAS tablealias ) )?
                        if ( stream_tablealias.hasNext() ) {
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:409:77: ^( TABLEALIAS tablealias )
                            {
                            CommonTree root_2 = (CommonTree)adaptor.nil();
                            root_2 = (CommonTree)adaptor.becomeRoot(
                            (CommonTree)adaptor.create(TABLEALIAS, "TABLEALIAS")
                            , root_2);

                            adaptor.addChild(root_2, stream_tablealias.nextTree());

                            adaptor.addChild(root_1, root_2);
                            }

                        }
                        stream_tablealias.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "tablespec"


    public static class schema_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "schema"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:411:1: schema : IDENT -> ^( SCHEMA IDENT ) ;
    public final MemgrammarParser.schema_return schema() throws RecognitionException {
        MemgrammarParser.schema_return retval = new MemgrammarParser.schema_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENT255=null;

        CommonTree IDENT255_tree=null;
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:411:7: ( IDENT -> ^( SCHEMA IDENT ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:411:10: IDENT
            {
            IDENT255=(Token)match(input,IDENT,FOLLOW_IDENT_in_schema7885);  
            stream_IDENT.add(IDENT255);


            // AST REWRITE
            // elements: IDENT
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 411:16: -> ^( SCHEMA IDENT )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:411:19: ^( SCHEMA IDENT )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(SCHEMA, "SCHEMA")
                , root_1);

                adaptor.addChild(root_1, 
                stream_IDENT.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "schema"


    public static class tablealias_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "tablealias"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:413:1: tablealias : IDENT ;
    public final MemgrammarParser.tablealias_return tablealias() throws RecognitionException {
        MemgrammarParser.tablealias_return retval = new MemgrammarParser.tablealias_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENT256=null;

        CommonTree IDENT256_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:413:11: ( IDENT )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:413:13: IDENT
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENT256=(Token)match(input,IDENT,FOLLOW_IDENT_in_tablealias7900); 
            IDENT256_tree = 
            (CommonTree)adaptor.create(IDENT256)
            ;
            adaptor.addChild(root_0, IDENT256_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "tablealias"


    public static class joinspec_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joinspec"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:415:1: joinspec : ON joinOr -> ^( ON joinOr ) ;
    public final MemgrammarParser.joinspec_return joinspec() throws RecognitionException {
        MemgrammarParser.joinspec_return retval = new MemgrammarParser.joinspec_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ON257=null;
        MemgrammarParser.joinOr_return joinOr258 =null;


        CommonTree ON257_tree=null;
        RewriteRuleTokenStream stream_ON=new RewriteRuleTokenStream(adaptor,"token ON");
        RewriteRuleSubtreeStream stream_joinOr=new RewriteRuleSubtreeStream(adaptor,"rule joinOr");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:415:9: ( ON joinOr -> ^( ON joinOr ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:415:11: ON joinOr
            {
            ON257=(Token)match(input,ON,FOLLOW_ON_in_joinspec7908);  
            stream_ON.add(ON257);


            pushFollow(FOLLOW_joinOr_in_joinspec7910);
            joinOr258=joinOr();

            state._fsp--;

            stream_joinOr.add(joinOr258.getTree());

            // AST REWRITE
            // elements: ON, joinOr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 415:21: -> ^( ON joinOr )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:415:24: ^( ON joinOr )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_ON.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_joinOr.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joinspec"


    public static class joinOr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joinOr"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:418:1: joinOr : joinAnd ( OR ^ joinAnd )* ;
    public final MemgrammarParser.joinOr_return joinOr() throws RecognitionException {
        MemgrammarParser.joinOr_return retval = new MemgrammarParser.joinOr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token OR260=null;
        MemgrammarParser.joinAnd_return joinAnd259 =null;

        MemgrammarParser.joinAnd_return joinAnd261 =null;


        CommonTree OR260_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:418:7: ( joinAnd ( OR ^ joinAnd )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:418:10: joinAnd ( OR ^ joinAnd )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_joinAnd_in_joinOr7930);
            joinAnd259=joinAnd();

            state._fsp--;

            adaptor.addChild(root_0, joinAnd259.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:418:18: ( OR ^ joinAnd )*
            loop77:
            do {
                int alt77=2;
                int LA77_0 = input.LA(1);

                if ( (LA77_0==OR) ) {
                    alt77=1;
                }


                switch (alt77) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:418:19: OR ^ joinAnd
            	    {
            	    OR260=(Token)match(input,OR,FOLLOW_OR_in_joinOr7933); 
            	    OR260_tree = 
            	    (CommonTree)adaptor.create(OR260)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(OR260_tree, root_0);


            	    pushFollow(FOLLOW_joinAnd_in_joinOr7936);
            	    joinAnd261=joinAnd();

            	    state._fsp--;

            	    adaptor.addChild(root_0, joinAnd261.getTree());

            	    }
            	    break;

            	default :
            	    break loop77;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joinOr"


    public static class joinAnd_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joinAnd"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:420:1: joinAnd : joinOn ( AND ^ joinOn )* ;
    public final MemgrammarParser.joinAnd_return joinAnd() throws RecognitionException {
        MemgrammarParser.joinAnd_return retval = new MemgrammarParser.joinAnd_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token AND263=null;
        MemgrammarParser.joinOn_return joinOn262 =null;

        MemgrammarParser.joinOn_return joinOn264 =null;


        CommonTree AND263_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:420:8: ( joinOn ( AND ^ joinOn )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:420:10: joinOn ( AND ^ joinOn )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_joinOn_in_joinAnd7945);
            joinOn262=joinOn();

            state._fsp--;

            adaptor.addChild(root_0, joinOn262.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:420:17: ( AND ^ joinOn )*
            loop78:
            do {
                int alt78=2;
                int LA78_0 = input.LA(1);

                if ( (LA78_0==AND) ) {
                    alt78=1;
                }


                switch (alt78) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:420:18: AND ^ joinOn
            	    {
            	    AND263=(Token)match(input,AND,FOLLOW_AND_in_joinAnd7948); 
            	    AND263_tree = 
            	    (CommonTree)adaptor.create(AND263)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(AND263_tree, root_0);


            	    pushFollow(FOLLOW_joinOn_in_joinAnd7951);
            	    joinOn264=joinOn();

            	    state._fsp--;

            	    adaptor.addChild(root_0, joinOn264.getTree());

            	    }
            	    break;

            	default :
            	    break loop78;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joinAnd"


    public static class joinOn_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joinOn"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:422:1: joinOn : primaryJoin ( snowflake ^ primaryJoin )? ;
    public final MemgrammarParser.joinOn_return joinOn() throws RecognitionException {
        MemgrammarParser.joinOn_return retval = new MemgrammarParser.joinOn_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.primaryJoin_return primaryJoin265 =null;

        MemgrammarParser.snowflake_return snowflake266 =null;

        MemgrammarParser.primaryJoin_return primaryJoin267 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:422:8: ( primaryJoin ( snowflake ^ primaryJoin )? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:422:10: primaryJoin ( snowflake ^ primaryJoin )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_primaryJoin_in_joinOn7961);
            primaryJoin265=primaryJoin();

            state._fsp--;

            adaptor.addChild(root_0, primaryJoin265.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:422:22: ( snowflake ^ primaryJoin )?
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==ON) ) {
                alt79=1;
            }
            switch (alt79) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:422:23: snowflake ^ primaryJoin
                    {
                    pushFollow(FOLLOW_snowflake_in_joinOn7964);
                    snowflake266=snowflake();

                    state._fsp--;

                    root_0 = (CommonTree)adaptor.becomeRoot(snowflake266.getTree(), root_0);

                    pushFollow(FOLLOW_primaryJoin_in_joinOn7967);
                    primaryJoin267=primaryJoin();

                    state._fsp--;

                    adaptor.addChild(root_0, primaryJoin267.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joinOn"


    public static class snowflake_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "snowflake"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:424:1: snowflake : ON -> AND ;
    public final MemgrammarParser.snowflake_return snowflake() throws RecognitionException {
        MemgrammarParser.snowflake_return retval = new MemgrammarParser.snowflake_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ON268=null;

        CommonTree ON268_tree=null;
        RewriteRuleTokenStream stream_ON=new RewriteRuleTokenStream(adaptor,"token ON");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:425:2: ( ON -> AND )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:425:4: ON
            {
            ON268=(Token)match(input,ON,FOLLOW_ON_in_snowflake7978);  
            stream_ON.add(ON268);


            // AST REWRITE
            // elements: 
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 425:7: -> AND
            {
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(AND, "AND")
                );

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "snowflake"


    public static class primaryJoin_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "primaryJoin"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:427:1: primaryJoin : ( '(' ! joinOr ')' !| joincond );
    public final MemgrammarParser.primaryJoin_return primaryJoin() throws RecognitionException {
        MemgrammarParser.primaryJoin_return retval = new MemgrammarParser.primaryJoin_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal269=null;
        Token char_literal271=null;
        MemgrammarParser.joinOr_return joinOr270 =null;

        MemgrammarParser.joincond_return joincond272 =null;


        CommonTree char_literal269_tree=null;
        CommonTree char_literal271_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:428:2: ( '(' ! joinOr ')' !| joincond )
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0==LEFT_PAREN) ) {
                alt80=1;
            }
            else if ( (LA80_0==AND||LA80_0==BOOLEAN||LA80_0==DATETIME||LA80_0==FLOAT||LA80_0==GROUPBY||LA80_0==HAVING||LA80_0==IDENT||LA80_0==INNERJOIN||LA80_0==INTEGER||LA80_0==LEFTOUTERJOIN||LA80_0==NULL||(LA80_0 >= ON && LA80_0 <= ORDERBY)||(LA80_0 >= RIGHTOUTERJOIN && LA80_0 <= RIGHT_PAREN)||LA80_0==STRING||LA80_0==WHERE||LA80_0==165) ) {
                alt80=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 80, 0, input);

                throw nvae;

            }
            switch (alt80) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:428:4: '(' ! joinOr ')' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal269=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_primaryJoin7991); 

                    pushFollow(FOLLOW_joinOr_in_primaryJoin7994);
                    joinOr270=joinOr();

                    state._fsp--;

                    adaptor.addChild(root_0, joinOr270.getTree());

                    char_literal271=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_primaryJoin7996); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:429:4: joincond
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_joincond_in_primaryJoin8002);
                    joincond272=joincond();

                    state._fsp--;

                    adaptor.addChild(root_0, joincond272.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "primaryJoin"


    public static class joincond_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joincond"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:431:1: joincond : equijoin ;
    public final MemgrammarParser.joincond_return joincond() throws RecognitionException {
        MemgrammarParser.joincond_return retval = new MemgrammarParser.joincond_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.equijoin_return equijoin273 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:432:2: ( equijoin )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:432:5: equijoin
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_equijoin_in_joincond8014);
            equijoin273=equijoin();

            state._fsp--;

            adaptor.addChild(root_0, equijoin273.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joincond"


    public static class equijoin_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equijoin"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:434:1: equijoin : ( joinpredicate EQUALS joinpredicate -> ^( EQUALS joinpredicate joinpredicate ) || colspec ISNULL -> ^( ISNULL colspec ) );
    public final MemgrammarParser.equijoin_return equijoin() throws RecognitionException {
        MemgrammarParser.equijoin_return retval = new MemgrammarParser.equijoin_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EQUALS275=null;
        Token ISNULL278=null;
        MemgrammarParser.joinpredicate_return joinpredicate274 =null;

        MemgrammarParser.joinpredicate_return joinpredicate276 =null;

        MemgrammarParser.colspec_return colspec277 =null;


        CommonTree EQUALS275_tree=null;
        CommonTree ISNULL278_tree=null;
        RewriteRuleTokenStream stream_EQUALS=new RewriteRuleTokenStream(adaptor,"token EQUALS");
        RewriteRuleTokenStream stream_ISNULL=new RewriteRuleTokenStream(adaptor,"token ISNULL");
        RewriteRuleSubtreeStream stream_joinpredicate=new RewriteRuleSubtreeStream(adaptor,"rule joinpredicate");
        RewriteRuleSubtreeStream stream_colspec=new RewriteRuleSubtreeStream(adaptor,"rule colspec");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:434:9: ( joinpredicate EQUALS joinpredicate -> ^( EQUALS joinpredicate joinpredicate ) || colspec ISNULL -> ^( ISNULL colspec ) )
            int alt81=3;
            switch ( input.LA(1) ) {
            case IDENT:
                {
                switch ( input.LA(2) ) {
                case 164:
                    {
                    int LA81_4 = input.LA(3);

                    if ( (LA81_4==IDENT) ) {
                        switch ( input.LA(4) ) {
                        case EQUALS:
                            {
                            alt81=1;
                            }
                            break;
                        case ISNULL:
                            {
                            alt81=3;
                            }
                            break;
                        case 164:
                            {
                            int LA81_7 = input.LA(5);

                            if ( (LA81_7==IDENT) ) {
                                int LA81_8 = input.LA(6);

                                if ( (LA81_8==EQUALS) ) {
                                    alt81=1;
                                }
                                else if ( (LA81_8==ISNULL) ) {
                                    alt81=3;
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 81, 8, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 81, 7, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 81, 6, input);

                            throw nvae;

                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 81, 4, input);

                        throw nvae;

                    }
                    }
                    break;
                case EQUALS:
                    {
                    alt81=1;
                    }
                    break;
                case ISNULL:
                    {
                    alt81=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 81, 1, input);

                    throw nvae;

                }

                }
                break;
            case BOOLEAN:
            case DATETIME:
            case FLOAT:
            case INTEGER:
            case NULL:
            case STRING:
                {
                alt81=1;
                }
                break;
            case AND:
            case GROUPBY:
            case HAVING:
            case INNERJOIN:
            case LEFTOUTERJOIN:
            case ON:
            case OR:
            case ORDERBY:
            case RIGHTOUTERJOIN:
            case RIGHT_PAREN:
            case WHERE:
            case 165:
                {
                alt81=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 81, 0, input);

                throw nvae;

            }

            switch (alt81) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:434:11: joinpredicate EQUALS joinpredicate
                    {
                    pushFollow(FOLLOW_joinpredicate_in_equijoin8021);
                    joinpredicate274=joinpredicate();

                    state._fsp--;

                    stream_joinpredicate.add(joinpredicate274.getTree());

                    EQUALS275=(Token)match(input,EQUALS,FOLLOW_EQUALS_in_equijoin8023);  
                    stream_EQUALS.add(EQUALS275);


                    pushFollow(FOLLOW_joinpredicate_in_equijoin8025);
                    joinpredicate276=joinpredicate();

                    state._fsp--;

                    stream_joinpredicate.add(joinpredicate276.getTree());

                    // AST REWRITE
                    // elements: joinpredicate, EQUALS, joinpredicate
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 434:46: -> ^( EQUALS joinpredicate joinpredicate )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:434:49: ^( EQUALS joinpredicate joinpredicate )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_EQUALS.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_joinpredicate.nextTree());

                        adaptor.addChild(root_1, stream_joinpredicate.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:434:88: 
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:435:3: colspec ISNULL
                    {
                    pushFollow(FOLLOW_colspec_in_equijoin8042);
                    colspec277=colspec();

                    state._fsp--;

                    stream_colspec.add(colspec277.getTree());

                    ISNULL278=(Token)match(input,ISNULL,FOLLOW_ISNULL_in_equijoin8044);  
                    stream_ISNULL.add(ISNULL278);


                    // AST REWRITE
                    // elements: colspec, ISNULL
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 435:18: -> ^( ISNULL colspec )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:435:21: ^( ISNULL colspec )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_ISNULL.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_colspec.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equijoin"


    public static class joinpredicate_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "joinpredicate"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:437:1: joinpredicate : ( colspec | literal );
    public final MemgrammarParser.joinpredicate_return joinpredicate() throws RecognitionException {
        MemgrammarParser.joinpredicate_return retval = new MemgrammarParser.joinpredicate_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.colspec_return colspec279 =null;

        MemgrammarParser.literal_return literal280 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:438:2: ( colspec | literal )
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==IDENT) ) {
                alt82=1;
            }
            else if ( (LA82_0==BOOLEAN||LA82_0==DATETIME||LA82_0==FLOAT||LA82_0==INTEGER||LA82_0==NULL||LA82_0==STRING) ) {
                alt82=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 82, 0, input);

                throw nvae;

            }
            switch (alt82) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:438:4: colspec
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_colspec_in_joinpredicate8061);
                    colspec279=colspec();

                    state._fsp--;

                    adaptor.addChild(root_0, colspec279.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:438:14: literal
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_literal_in_joinpredicate8065);
                    literal280=literal();

                    state._fsp--;

                    adaptor.addChild(root_0, literal280.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "joinpredicate"


    public static class colspec_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "colspec"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:440:1: colspec : ( ident '.' ident '.' IDENT -> ^( IDENT ident ident ) | ident '.' IDENT -> ^( IDENT ident ) | IDENT );
    public final MemgrammarParser.colspec_return colspec() throws RecognitionException {
        MemgrammarParser.colspec_return retval = new MemgrammarParser.colspec_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal282=null;
        Token char_literal284=null;
        Token IDENT285=null;
        Token char_literal287=null;
        Token IDENT288=null;
        Token IDENT289=null;
        MemgrammarParser.ident_return ident281 =null;

        MemgrammarParser.ident_return ident283 =null;

        MemgrammarParser.ident_return ident286 =null;


        CommonTree char_literal282_tree=null;
        CommonTree char_literal284_tree=null;
        CommonTree IDENT285_tree=null;
        CommonTree char_literal287_tree=null;
        CommonTree IDENT288_tree=null;
        CommonTree IDENT289_tree=null;
        RewriteRuleTokenStream stream_IDENT=new RewriteRuleTokenStream(adaptor,"token IDENT");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:440:9: ( ident '.' ident '.' IDENT -> ^( IDENT ident ident ) | ident '.' IDENT -> ^( IDENT ident ) | IDENT )
            int alt83=3;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==IDENT) ) {
                int LA83_1 = input.LA(2);

                if ( (LA83_1==164) ) {
                    int LA83_2 = input.LA(3);

                    if ( (LA83_2==IDENT) ) {
                        int LA83_4 = input.LA(4);

                        if ( (LA83_4==EOF||(LA83_4 >= AND && LA83_4 <= ASCENDING)||LA83_4==DESCENDING||LA83_4==DIV||LA83_4==ELSE||(LA83_4 >= END && LA83_4 <= EQUALS)||LA83_4==FROM||(LA83_4 >= GROUPBY && LA83_4 <= HAVING)||LA83_4==IN||LA83_4==INNERJOIN||(LA83_4 >= INTO && LA83_4 <= ISNULL)||LA83_4==LEFTOUTERJOIN||LA83_4==LIKE||(LA83_4 >= LT && LA83_4 <= LTEQ)||LA83_4==MINUS||LA83_4==MOD||LA83_4==MULT||(LA83_4 >= NOTEQUALS && LA83_4 <= NOTLIKE)||(LA83_4 >= ON && LA83_4 <= ORDERBY)||LA83_4==PLUS||LA83_4==POW||(LA83_4 >= RIGHTOUTERJOIN && LA83_4 <= RIGHT_PAREN)||LA83_4==STRING||LA83_4==THEN||(LA83_4 >= WHEN && LA83_4 <= WHERE)||LA83_4==163||LA83_4==165) ) {
                            alt83=2;
                        }
                        else if ( (LA83_4==164) ) {
                            alt83=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 83, 4, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 83, 2, input);

                        throw nvae;

                    }
                }
                else if ( (LA83_1==EOF||(LA83_1 >= AND && LA83_1 <= ASCENDING)||LA83_1==DESCENDING||LA83_1==DIV||LA83_1==ELSE||(LA83_1 >= END && LA83_1 <= EQUALS)||LA83_1==FROM||(LA83_1 >= GROUPBY && LA83_1 <= HAVING)||LA83_1==IN||LA83_1==INNERJOIN||(LA83_1 >= INTO && LA83_1 <= ISNULL)||LA83_1==LEFTOUTERJOIN||LA83_1==LIKE||(LA83_1 >= LT && LA83_1 <= LTEQ)||LA83_1==MINUS||LA83_1==MOD||LA83_1==MULT||(LA83_1 >= NOTEQUALS && LA83_1 <= NOTLIKE)||(LA83_1 >= ON && LA83_1 <= ORDERBY)||LA83_1==PLUS||LA83_1==POW||(LA83_1 >= RIGHTOUTERJOIN && LA83_1 <= RIGHT_PAREN)||LA83_1==STRING||LA83_1==THEN||(LA83_1 >= WHEN && LA83_1 <= WHERE)||LA83_1==163||LA83_1==165) ) {
                    alt83=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 83, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 83, 0, input);

                throw nvae;

            }
            switch (alt83) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:440:11: ident '.' ident '.' IDENT
                    {
                    pushFollow(FOLLOW_ident_in_colspec8074);
                    ident281=ident();

                    state._fsp--;

                    stream_ident.add(ident281.getTree());

                    char_literal282=(Token)match(input,164,FOLLOW_164_in_colspec8076);  
                    stream_164.add(char_literal282);


                    pushFollow(FOLLOW_ident_in_colspec8078);
                    ident283=ident();

                    state._fsp--;

                    stream_ident.add(ident283.getTree());

                    char_literal284=(Token)match(input,164,FOLLOW_164_in_colspec8080);  
                    stream_164.add(char_literal284);


                    IDENT285=(Token)match(input,IDENT,FOLLOW_IDENT_in_colspec8082);  
                    stream_IDENT.add(IDENT285);


                    // AST REWRITE
                    // elements: ident, ident, IDENT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 440:37: -> ^( IDENT ident ident )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:440:40: ^( IDENT ident ident )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_IDENT.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:441:3: ident '.' IDENT
                    {
                    pushFollow(FOLLOW_ident_in_colspec8098);
                    ident286=ident();

                    state._fsp--;

                    stream_ident.add(ident286.getTree());

                    char_literal287=(Token)match(input,164,FOLLOW_164_in_colspec8100);  
                    stream_164.add(char_literal287);


                    IDENT288=(Token)match(input,IDENT,FOLLOW_IDENT_in_colspec8102);  
                    stream_IDENT.add(IDENT288);


                    // AST REWRITE
                    // elements: ident, IDENT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 441:19: -> ^( IDENT ident )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:441:22: ^( IDENT ident )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_IDENT.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:442:3: IDENT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    IDENT289=(Token)match(input,IDENT,FOLLOW_IDENT_in_colspec8116); 
                    IDENT289_tree = 
                    (CommonTree)adaptor.create(IDENT289)
                    ;
                    adaptor.addChild(root_0, IDENT289_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "colspec"


    public static class where_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "where"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:444:1: where : WHERE logicalExpression -> ^( WHERE logicalExpression ) ;
    public final MemgrammarParser.where_return where() throws RecognitionException {
        MemgrammarParser.where_return retval = new MemgrammarParser.where_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token WHERE290=null;
        MemgrammarParser.logicalExpression_return logicalExpression291 =null;


        CommonTree WHERE290_tree=null;
        RewriteRuleTokenStream stream_WHERE=new RewriteRuleTokenStream(adaptor,"token WHERE");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:444:7: ( WHERE logicalExpression -> ^( WHERE logicalExpression ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:444:9: WHERE logicalExpression
            {
            WHERE290=(Token)match(input,WHERE,FOLLOW_WHERE_in_where8124);  
            stream_WHERE.add(WHERE290);


            pushFollow(FOLLOW_logicalExpression_in_where8126);
            logicalExpression291=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression291.getTree());

            // AST REWRITE
            // elements: logicalExpression, WHERE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 444:33: -> ^( WHERE logicalExpression )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:444:36: ^( WHERE logicalExpression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_WHERE.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "where"


    public static class groupby_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "groupby"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:446:1: groupby : GROUPBY groupbyprojectionlist -> ^( GROUPBY groupbyprojectionlist ) ;
    public final MemgrammarParser.groupby_return groupby() throws RecognitionException {
        MemgrammarParser.groupby_return retval = new MemgrammarParser.groupby_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token GROUPBY292=null;
        MemgrammarParser.groupbyprojectionlist_return groupbyprojectionlist293 =null;


        CommonTree GROUPBY292_tree=null;
        RewriteRuleTokenStream stream_GROUPBY=new RewriteRuleTokenStream(adaptor,"token GROUPBY");
        RewriteRuleSubtreeStream stream_groupbyprojectionlist=new RewriteRuleSubtreeStream(adaptor,"rule groupbyprojectionlist");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:446:9: ( GROUPBY groupbyprojectionlist -> ^( GROUPBY groupbyprojectionlist ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:446:11: GROUPBY groupbyprojectionlist
            {
            GROUPBY292=(Token)match(input,GROUPBY,FOLLOW_GROUPBY_in_groupby8142);  
            stream_GROUPBY.add(GROUPBY292);


            pushFollow(FOLLOW_groupbyprojectionlist_in_groupby8144);
            groupbyprojectionlist293=groupbyprojectionlist();

            state._fsp--;

            stream_groupbyprojectionlist.add(groupbyprojectionlist293.getTree());

            // AST REWRITE
            // elements: groupbyprojectionlist, GROUPBY
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 446:41: -> ^( GROUPBY groupbyprojectionlist )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:446:44: ^( GROUPBY groupbyprojectionlist )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_GROUPBY.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_groupbyprojectionlist.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "groupby"


    public static class orderby_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orderby"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:448:1: orderby : ORDERBY orderbylist -> ^( ORDERBY orderbylist ) ;
    public final MemgrammarParser.orderby_return orderby() throws RecognitionException {
        MemgrammarParser.orderby_return retval = new MemgrammarParser.orderby_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ORDERBY294=null;
        MemgrammarParser.orderbylist_return orderbylist295 =null;


        CommonTree ORDERBY294_tree=null;
        RewriteRuleTokenStream stream_ORDERBY=new RewriteRuleTokenStream(adaptor,"token ORDERBY");
        RewriteRuleSubtreeStream stream_orderbylist=new RewriteRuleSubtreeStream(adaptor,"rule orderbylist");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:448:9: ( ORDERBY orderbylist -> ^( ORDERBY orderbylist ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:448:11: ORDERBY orderbylist
            {
            ORDERBY294=(Token)match(input,ORDERBY,FOLLOW_ORDERBY_in_orderby8160);  
            stream_ORDERBY.add(ORDERBY294);


            pushFollow(FOLLOW_orderbylist_in_orderby8162);
            orderbylist295=orderbylist();

            state._fsp--;

            stream_orderbylist.add(orderbylist295.getTree());

            // AST REWRITE
            // elements: orderbylist, ORDERBY
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 448:31: -> ^( ORDERBY orderbylist )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:448:34: ^( ORDERBY orderbylist )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_ORDERBY.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_orderbylist.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orderby"


    public static class having_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "having"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:450:1: having : HAVING logicalExpression -> ^( HAVING logicalExpression ) ;
    public final MemgrammarParser.having_return having() throws RecognitionException {
        MemgrammarParser.having_return retval = new MemgrammarParser.having_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token HAVING296=null;
        MemgrammarParser.logicalExpression_return logicalExpression297 =null;


        CommonTree HAVING296_tree=null;
        RewriteRuleTokenStream stream_HAVING=new RewriteRuleTokenStream(adaptor,"token HAVING");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:450:8: ( HAVING logicalExpression -> ^( HAVING logicalExpression ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:450:10: HAVING logicalExpression
            {
            HAVING296=(Token)match(input,HAVING,FOLLOW_HAVING_in_having8178);  
            stream_HAVING.add(HAVING296);


            pushFollow(FOLLOW_logicalExpression_in_having8180);
            logicalExpression297=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression297.getTree());

            // AST REWRITE
            // elements: HAVING, logicalExpression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 450:35: -> ^( HAVING logicalExpression )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:450:38: ^( HAVING logicalExpression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_HAVING.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "having"


    public static class direction_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "direction"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:452:1: direction : ( ASCENDING | DESCENDING );
    public final MemgrammarParser.direction_return direction() throws RecognitionException {
        MemgrammarParser.direction_return retval = new MemgrammarParser.direction_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set298=null;

        CommonTree set298_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:453:2: ( ASCENDING | DESCENDING )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set298=(Token)input.LT(1);

            if ( input.LA(1)==ASCENDING||input.LA(1)==DESCENDING ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set298)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "direction"


    public static class projectionlist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "projectionlist"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:455:1: projectionlist : projection ( ',' projection )* -> ^( PROJECTIONLIST ( projection )* ) ;
    public final MemgrammarParser.projectionlist_return projectionlist() throws RecognitionException {
        MemgrammarParser.projectionlist_return retval = new MemgrammarParser.projectionlist_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal300=null;
        MemgrammarParser.projection_return projection299 =null;

        MemgrammarParser.projection_return projection301 =null;


        CommonTree char_literal300_tree=null;
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleSubtreeStream stream_projection=new RewriteRuleSubtreeStream(adaptor,"rule projection");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:2: ( projection ( ',' projection )* -> ^( PROJECTIONLIST ( projection )* ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:4: projection ( ',' projection )*
            {
            pushFollow(FOLLOW_projection_in_projectionlist8210);
            projection299=projection();

            state._fsp--;

            stream_projection.add(projection299.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:15: ( ',' projection )*
            loop84:
            do {
                int alt84=2;
                int LA84_0 = input.LA(1);

                if ( (LA84_0==163) ) {
                    alt84=1;
                }


                switch (alt84) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:16: ',' projection
            	    {
            	    char_literal300=(Token)match(input,163,FOLLOW_163_in_projectionlist8213);  
            	    stream_163.add(char_literal300);


            	    pushFollow(FOLLOW_projection_in_projectionlist8215);
            	    projection301=projection();

            	    state._fsp--;

            	    stream_projection.add(projection301.getTree());

            	    }
            	    break;

            	default :
            	    break loop84;
                }
            } while (true);


            // AST REWRITE
            // elements: projection
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 456:33: -> ^( PROJECTIONLIST ( projection )* )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:36: ^( PROJECTIONLIST ( projection )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(PROJECTIONLIST, "PROJECTIONLIST")
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:456:53: ( projection )*
                while ( stream_projection.hasNext() ) {
                    adaptor.addChild(root_1, stream_projection.nextTree());

                }
                stream_projection.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "projectionlist"


    public static class groupbyprojectionlist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "groupbyprojectionlist"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:458:1: groupbyprojectionlist : groupbyprojection ( ',' groupbyprojection )* -> ^( PROJECTIONLIST ( groupbyprojection )* ) ;
    public final MemgrammarParser.groupbyprojectionlist_return groupbyprojectionlist() throws RecognitionException {
        MemgrammarParser.groupbyprojectionlist_return retval = new MemgrammarParser.groupbyprojectionlist_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal303=null;
        MemgrammarParser.groupbyprojection_return groupbyprojection302 =null;

        MemgrammarParser.groupbyprojection_return groupbyprojection304 =null;


        CommonTree char_literal303_tree=null;
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleSubtreeStream stream_groupbyprojection=new RewriteRuleSubtreeStream(adaptor,"rule groupbyprojection");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:2: ( groupbyprojection ( ',' groupbyprojection )* -> ^( PROJECTIONLIST ( groupbyprojection )* ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:4: groupbyprojection ( ',' groupbyprojection )*
            {
            pushFollow(FOLLOW_groupbyprojection_in_groupbyprojectionlist8235);
            groupbyprojection302=groupbyprojection();

            state._fsp--;

            stream_groupbyprojection.add(groupbyprojection302.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:22: ( ',' groupbyprojection )*
            loop85:
            do {
                int alt85=2;
                int LA85_0 = input.LA(1);

                if ( (LA85_0==163) ) {
                    alt85=1;
                }


                switch (alt85) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:23: ',' groupbyprojection
            	    {
            	    char_literal303=(Token)match(input,163,FOLLOW_163_in_groupbyprojectionlist8238);  
            	    stream_163.add(char_literal303);


            	    pushFollow(FOLLOW_groupbyprojection_in_groupbyprojectionlist8240);
            	    groupbyprojection304=groupbyprojection();

            	    state._fsp--;

            	    stream_groupbyprojection.add(groupbyprojection304.getTree());

            	    }
            	    break;

            	default :
            	    break loop85;
                }
            } while (true);


            // AST REWRITE
            // elements: groupbyprojection
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 459:47: -> ^( PROJECTIONLIST ( groupbyprojection )* )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:50: ^( PROJECTIONLIST ( groupbyprojection )* )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(PROJECTIONLIST, "PROJECTIONLIST")
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:459:67: ( groupbyprojection )*
                while ( stream_groupbyprojection.hasNext() ) {
                    adaptor.addChild(root_1, stream_groupbyprojection.nextTree());

                }
                stream_groupbyprojection.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "groupbyprojectionlist"


    public static class orderbylist_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orderbylist"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:461:1: orderbylist : orderbycondition ( ',' ! orderbycondition )* ;
    public final MemgrammarParser.orderbylist_return orderbylist() throws RecognitionException {
        MemgrammarParser.orderbylist_return retval = new MemgrammarParser.orderbylist_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal306=null;
        MemgrammarParser.orderbycondition_return orderbycondition305 =null;

        MemgrammarParser.orderbycondition_return orderbycondition307 =null;


        CommonTree char_literal306_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:462:2: ( orderbycondition ( ',' ! orderbycondition )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:462:4: orderbycondition ( ',' ! orderbycondition )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_orderbycondition_in_orderbylist8260);
            orderbycondition305=orderbycondition();

            state._fsp--;

            adaptor.addChild(root_0, orderbycondition305.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:462:21: ( ',' ! orderbycondition )*
            loop86:
            do {
                int alt86=2;
                int LA86_0 = input.LA(1);

                if ( (LA86_0==163) ) {
                    alt86=1;
                }


                switch (alt86) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:462:22: ',' ! orderbycondition
            	    {
            	    char_literal306=(Token)match(input,163,FOLLOW_163_in_orderbylist8263); 

            	    pushFollow(FOLLOW_orderbycondition_in_orderbylist8266);
            	    orderbycondition307=orderbycondition();

            	    state._fsp--;

            	    adaptor.addChild(root_0, orderbycondition307.getTree());

            	    }
            	    break;

            	default :
            	    break loop86;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orderbylist"


    public static class projection_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "projection"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:464:1: projection : ( ident '.' ident '.' MULT -> ^( MULT ident ident ) | ident '.' MULT -> ^( MULT ident ) | MULT -> ^( MULT ) | logicalExpression ( ( AS )? STRING )? -> ^( PROJECTION logicalExpression ( STRING )? ) );
    public final MemgrammarParser.projection_return projection() throws RecognitionException {
        MemgrammarParser.projection_return retval = new MemgrammarParser.projection_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal309=null;
        Token char_literal311=null;
        Token MULT312=null;
        Token char_literal314=null;
        Token MULT315=null;
        Token MULT316=null;
        Token AS318=null;
        Token STRING319=null;
        MemgrammarParser.ident_return ident308 =null;

        MemgrammarParser.ident_return ident310 =null;

        MemgrammarParser.ident_return ident313 =null;

        MemgrammarParser.logicalExpression_return logicalExpression317 =null;


        CommonTree char_literal309_tree=null;
        CommonTree char_literal311_tree=null;
        CommonTree MULT312_tree=null;
        CommonTree char_literal314_tree=null;
        CommonTree MULT315_tree=null;
        CommonTree MULT316_tree=null;
        CommonTree AS318_tree=null;
        CommonTree STRING319_tree=null;
        RewriteRuleTokenStream stream_AS=new RewriteRuleTokenStream(adaptor,"token AS");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_MULT=new RewriteRuleTokenStream(adaptor,"token MULT");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:465:2: ( ident '.' ident '.' MULT -> ^( MULT ident ident ) | ident '.' MULT -> ^( MULT ident ) | MULT -> ^( MULT ) | logicalExpression ( ( AS )? STRING )? -> ^( PROJECTION logicalExpression ( STRING )? ) )
            int alt89=4;
            switch ( input.LA(1) ) {
            case IDENT:
                {
                int LA89_1 = input.LA(2);

                if ( (LA89_1==164) ) {
                    int LA89_4 = input.LA(3);

                    if ( (LA89_4==MULT) ) {
                        alt89=2;
                    }
                    else if ( (LA89_4==IDENT) ) {
                        int LA89_6 = input.LA(4);

                        if ( ((LA89_6 >= AND && LA89_6 <= AS)||LA89_6==DIV||LA89_6==EQUALS||LA89_6==FROM||(LA89_6 >= GT && LA89_6 <= GTEQ)||LA89_6==IN||(LA89_6 >= INTO && LA89_6 <= ISNULL)||LA89_6==LIKE||(LA89_6 >= LT && LA89_6 <= LTEQ)||LA89_6==MINUS||LA89_6==MOD||LA89_6==MULT||(LA89_6 >= NOTEQUALS && LA89_6 <= NOTLIKE)||LA89_6==OR||LA89_6==PLUS||LA89_6==POW||LA89_6==STRING||LA89_6==163) ) {
                            alt89=4;
                        }
                        else if ( (LA89_6==164) ) {
                            int LA89_7 = input.LA(5);

                            if ( (LA89_7==MULT) ) {
                                alt89=1;
                            }
                            else if ( (LA89_7==IDENT) ) {
                                alt89=4;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 89, 7, input);

                                throw nvae;

                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 89, 6, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 89, 4, input);

                        throw nvae;

                    }
                }
                else if ( ((LA89_1 >= AND && LA89_1 <= AS)||LA89_1==DIV||LA89_1==EQUALS||LA89_1==FROM||(LA89_1 >= GT && LA89_1 <= GTEQ)||LA89_1==IN||(LA89_1 >= INTO && LA89_1 <= ISNULL)||LA89_1==LIKE||(LA89_1 >= LT && LA89_1 <= LTEQ)||LA89_1==MINUS||LA89_1==MOD||LA89_1==MULT||(LA89_1 >= NOTEQUALS && LA89_1 <= NOTLIKE)||LA89_1==OR||LA89_1==PLUS||LA89_1==POW||LA89_1==STRING||LA89_1==163) ) {
                    alt89=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 89, 1, input);

                    throw nvae;

                }
                }
                break;
            case MULT:
                {
                alt89=3;
                }
                break;
            case AVG:
            case BOOLEAN:
            case CASE:
            case CAST:
            case COALESCE:
            case COUNT:
            case DATEADD:
            case DATEDIFF:
            case DATEPART:
            case DATETIME:
            case EXTRACT:
            case FLOAT:
            case GETDATE:
            case INTEGER:
            case ISNULLEXP:
            case LEFT_PAREN:
            case LEN:
            case LENGTH:
            case MAX:
            case MIN:
            case MINUS:
            case NOT:
            case NULL:
            case OBJECTSIZE:
            case PARAMETER:
            case POSITION:
            case STRING:
            case SUBSTR:
            case SUM:
            case TIMESTAMPADD:
            case TIMESTAMPDIFF:
            case TOLOWER:
            case TOUPPER:
                {
                alt89=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 89, 0, input);

                throw nvae;

            }

            switch (alt89) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:465:4: ident '.' ident '.' MULT
                    {
                    pushFollow(FOLLOW_ident_in_projection8277);
                    ident308=ident();

                    state._fsp--;

                    stream_ident.add(ident308.getTree());

                    char_literal309=(Token)match(input,164,FOLLOW_164_in_projection8279);  
                    stream_164.add(char_literal309);


                    pushFollow(FOLLOW_ident_in_projection8281);
                    ident310=ident();

                    state._fsp--;

                    stream_ident.add(ident310.getTree());

                    char_literal311=(Token)match(input,164,FOLLOW_164_in_projection8283);  
                    stream_164.add(char_literal311);


                    MULT312=(Token)match(input,MULT,FOLLOW_MULT_in_projection8285);  
                    stream_MULT.add(MULT312);


                    // AST REWRITE
                    // elements: ident, MULT, ident
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 465:29: -> ^( MULT ident ident )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:465:32: ^( MULT ident ident )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_MULT.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:466:3: ident '.' MULT
                    {
                    pushFollow(FOLLOW_ident_in_projection8301);
                    ident313=ident();

                    state._fsp--;

                    stream_ident.add(ident313.getTree());

                    char_literal314=(Token)match(input,164,FOLLOW_164_in_projection8303);  
                    stream_164.add(char_literal314);


                    MULT315=(Token)match(input,MULT,FOLLOW_MULT_in_projection8305);  
                    stream_MULT.add(MULT315);


                    // AST REWRITE
                    // elements: ident, MULT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 466:18: -> ^( MULT ident )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:466:21: ^( MULT ident )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_MULT.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, stream_ident.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:467:3: MULT
                    {
                    MULT316=(Token)match(input,MULT,FOLLOW_MULT_in_projection8319);  
                    stream_MULT.add(MULT316);


                    // AST REWRITE
                    // elements: MULT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 467:8: -> ^( MULT )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:467:11: ^( MULT )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_MULT.nextNode()
                        , root_1);

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:3: logicalExpression ( ( AS )? STRING )?
                    {
                    pushFollow(FOLLOW_logicalExpression_in_projection8331);
                    logicalExpression317=logicalExpression();

                    state._fsp--;

                    stream_logicalExpression.add(logicalExpression317.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:21: ( ( AS )? STRING )?
                    int alt88=2;
                    int LA88_0 = input.LA(1);

                    if ( (LA88_0==AS||LA88_0==STRING) ) {
                        alt88=1;
                    }
                    switch (alt88) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:22: ( AS )? STRING
                            {
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:22: ( AS )?
                            int alt87=2;
                            int LA87_0 = input.LA(1);

                            if ( (LA87_0==AS) ) {
                                alt87=1;
                            }
                            switch (alt87) {
                                case 1 :
                                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:22: AS
                                    {
                                    AS318=(Token)match(input,AS,FOLLOW_AS_in_projection8334);  
                                    stream_AS.add(AS318);


                                    }
                                    break;

                            }


                            STRING319=(Token)match(input,STRING,FOLLOW_STRING_in_projection8337);  
                            stream_STRING.add(STRING319);


                            }
                            break;

                    }


                    // AST REWRITE
                    // elements: logicalExpression, STRING
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 468:35: -> ^( PROJECTION logicalExpression ( STRING )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:38: ^( PROJECTION logicalExpression ( STRING )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(PROJECTION, "PROJECTION")
                        , root_1);

                        adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:468:69: ( STRING )?
                        if ( stream_STRING.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_STRING.nextNode()
                            );

                        }
                        stream_STRING.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "projection"


    public static class groupbyprojection_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "groupbyprojection"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:470:1: groupbyprojection : logicalExpression ;
    public final MemgrammarParser.groupbyprojection_return groupbyprojection() throws RecognitionException {
        MemgrammarParser.groupbyprojection_return retval = new MemgrammarParser.groupbyprojection_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.logicalExpression_return logicalExpression320 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:471:2: ( logicalExpression )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:471:4: logicalExpression
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_logicalExpression_in_groupbyprojection8361);
            logicalExpression320=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression320.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "groupbyprojection"


    public static class orderbycondition_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orderbycondition"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:473:1: orderbycondition : logicalExpression ( direction )? -> ^( PROJECTION logicalExpression ( direction )? ) ;
    public final MemgrammarParser.orderbycondition_return orderbycondition() throws RecognitionException {
        MemgrammarParser.orderbycondition_return retval = new MemgrammarParser.orderbycondition_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.logicalExpression_return logicalExpression321 =null;

        MemgrammarParser.direction_return direction322 =null;


        RewriteRuleSubtreeStream stream_direction=new RewriteRuleSubtreeStream(adaptor,"rule direction");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:2: ( logicalExpression ( direction )? -> ^( PROJECTION logicalExpression ( direction )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:4: logicalExpression ( direction )?
            {
            pushFollow(FOLLOW_logicalExpression_in_orderbycondition8370);
            logicalExpression321=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression321.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:23: ( direction )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==ASCENDING||LA90_0==DESCENDING) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:23: direction
                    {
                    pushFollow(FOLLOW_direction_in_orderbycondition8373);
                    direction322=direction();

                    state._fsp--;

                    stream_direction.add(direction322.getTree());

                    }
                    break;

            }


            // AST REWRITE
            // elements: direction, logicalExpression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 474:34: -> ^( PROJECTION logicalExpression ( direction )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:37: ^( PROJECTION logicalExpression ( direction )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(PROJECTION, "PROJECTION")
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:474:68: ( direction )?
                if ( stream_direction.hasNext() ) {
                    adaptor.addChild(root_1, stream_direction.nextTree());

                }
                stream_direction.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orderbycondition"


    public static class ident_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ident"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:476:1: ident : IDENT ;
    public final MemgrammarParser.ident_return ident() throws RecognitionException {
        MemgrammarParser.ident_return retval = new MemgrammarParser.ident_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IDENT323=null;

        CommonTree IDENT323_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:476:7: ( IDENT )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:476:9: IDENT
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENT323=(Token)match(input,IDENT,FOLLOW_IDENT_in_ident8393); 
            IDENT323_tree = 
            (CommonTree)adaptor.create(IDENT323)
            ;
            adaptor.addChild(root_0, IDENT323_tree);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ident"


    public static class aggrule_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "aggrule"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:478:1: aggrule : ( SUM | AVG | COUNT | MIN | MAX );
    public final MemgrammarParser.aggrule_return aggrule() throws RecognitionException {
        MemgrammarParser.aggrule_return retval = new MemgrammarParser.aggrule_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set324=null;

        CommonTree set324_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:478:9: ( SUM | AVG | COUNT | MIN | MAX )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set324=(Token)input.LT(1);

            if ( input.LA(1)==AVG||input.LA(1)==COUNT||input.LA(1)==MAX||input.LA(1)==MIN||input.LA(1)==SUM ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set324)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "aggrule"


    public static class casewhen_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "casewhen"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:1: casewhen : CASE ( whenexp )+ ( elseexp )? END -> ^( CASE ( whenexp )+ ( elseexp )? ) ;
    public final MemgrammarParser.casewhen_return casewhen() throws RecognitionException {
        MemgrammarParser.casewhen_return retval = new MemgrammarParser.casewhen_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token CASE325=null;
        Token END328=null;
        MemgrammarParser.whenexp_return whenexp326 =null;

        MemgrammarParser.elseexp_return elseexp327 =null;


        CommonTree CASE325_tree=null;
        CommonTree END328_tree=null;
        RewriteRuleTokenStream stream_END=new RewriteRuleTokenStream(adaptor,"token END");
        RewriteRuleTokenStream stream_CASE=new RewriteRuleTokenStream(adaptor,"token CASE");
        RewriteRuleSubtreeStream stream_elseexp=new RewriteRuleSubtreeStream(adaptor,"rule elseexp");
        RewriteRuleSubtreeStream stream_whenexp=new RewriteRuleSubtreeStream(adaptor,"rule whenexp");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:9: ( CASE ( whenexp )+ ( elseexp )? END -> ^( CASE ( whenexp )+ ( elseexp )? ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:11: CASE ( whenexp )+ ( elseexp )? END
            {
            CASE325=(Token)match(input,CASE,FOLLOW_CASE_in_casewhen8424);  
            stream_CASE.add(CASE325);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:16: ( whenexp )+
            int cnt91=0;
            loop91:
            do {
                int alt91=2;
                int LA91_0 = input.LA(1);

                if ( (LA91_0==WHEN) ) {
                    alt91=1;
                }


                switch (alt91) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:16: whenexp
            	    {
            	    pushFollow(FOLLOW_whenexp_in_casewhen8426);
            	    whenexp326=whenexp();

            	    state._fsp--;

            	    stream_whenexp.add(whenexp326.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt91 >= 1 ) break loop91;
                        EarlyExitException eee =
                            new EarlyExitException(91, input);
                        throw eee;
                }
                cnt91++;
            } while (true);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:25: ( elseexp )?
            int alt92=2;
            int LA92_0 = input.LA(1);

            if ( (LA92_0==ELSE) ) {
                alt92=1;
            }
            switch (alt92) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:25: elseexp
                    {
                    pushFollow(FOLLOW_elseexp_in_casewhen8429);
                    elseexp327=elseexp();

                    state._fsp--;

                    stream_elseexp.add(elseexp327.getTree());

                    }
                    break;

            }


            END328=(Token)match(input,END,FOLLOW_END_in_casewhen8432);  
            stream_END.add(END328);


            // AST REWRITE
            // elements: CASE, elseexp, whenexp
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 480:38: -> ^( CASE ( whenexp )+ ( elseexp )? )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:41: ^( CASE ( whenexp )+ ( elseexp )? )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_CASE.nextNode()
                , root_1);

                if ( !(stream_whenexp.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_whenexp.hasNext() ) {
                    adaptor.addChild(root_1, stream_whenexp.nextTree());

                }
                stream_whenexp.reset();

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:480:57: ( elseexp )?
                if ( stream_elseexp.hasNext() ) {
                    adaptor.addChild(root_1, stream_elseexp.nextTree());

                }
                stream_elseexp.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "casewhen"


    public static class whenexp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "whenexp"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:482:1: whenexp : WHEN logicalExpression THEN logicalExpression -> ^( WHEN logicalExpression logicalExpression ) ;
    public final MemgrammarParser.whenexp_return whenexp() throws RecognitionException {
        MemgrammarParser.whenexp_return retval = new MemgrammarParser.whenexp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token WHEN329=null;
        Token THEN331=null;
        MemgrammarParser.logicalExpression_return logicalExpression330 =null;

        MemgrammarParser.logicalExpression_return logicalExpression332 =null;


        CommonTree WHEN329_tree=null;
        CommonTree THEN331_tree=null;
        RewriteRuleTokenStream stream_THEN=new RewriteRuleTokenStream(adaptor,"token THEN");
        RewriteRuleTokenStream stream_WHEN=new RewriteRuleTokenStream(adaptor,"token WHEN");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:482:9: ( WHEN logicalExpression THEN logicalExpression -> ^( WHEN logicalExpression logicalExpression ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:482:11: WHEN logicalExpression THEN logicalExpression
            {
            WHEN329=(Token)match(input,WHEN,FOLLOW_WHEN_in_whenexp8452);  
            stream_WHEN.add(WHEN329);


            pushFollow(FOLLOW_logicalExpression_in_whenexp8454);
            logicalExpression330=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression330.getTree());

            THEN331=(Token)match(input,THEN,FOLLOW_THEN_in_whenexp8456);  
            stream_THEN.add(THEN331);


            pushFollow(FOLLOW_logicalExpression_in_whenexp8458);
            logicalExpression332=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression332.getTree());

            // AST REWRITE
            // elements: logicalExpression, logicalExpression, WHEN
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 482:57: -> ^( WHEN logicalExpression logicalExpression )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:482:60: ^( WHEN logicalExpression logicalExpression )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_WHEN.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "whenexp"


    public static class elseexp_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "elseexp"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:484:1: elseexp : ELSE ! logicalExpression ;
    public final MemgrammarParser.elseexp_return elseexp() throws RecognitionException {
        MemgrammarParser.elseexp_return retval = new MemgrammarParser.elseexp_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ELSE333=null;
        MemgrammarParser.logicalExpression_return logicalExpression334 =null;


        CommonTree ELSE333_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:484:9: ( ELSE ! logicalExpression )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:484:11: ELSE ! logicalExpression
            {
            root_0 = (CommonTree)adaptor.nil();


            ELSE333=(Token)match(input,ELSE,FOLLOW_ELSE_in_elseexp8476); 

            pushFollow(FOLLOW_logicalExpression_in_elseexp8479);
            logicalExpression334=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression334.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "elseexp"


    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:486:1: expression : logicalExpression EOF !;
    public final MemgrammarParser.expression_return expression() throws RecognitionException {
        MemgrammarParser.expression_return retval = new MemgrammarParser.expression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF336=null;
        MemgrammarParser.logicalExpression_return logicalExpression335 =null;


        CommonTree EOF336_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:487:2: ( logicalExpression EOF !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:487:5: logicalExpression EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_logicalExpression_in_expression8489);
            logicalExpression335=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression335.getTree());

            EOF336=(Token)match(input,EOF,FOLLOW_EOF_in_expression8491); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class logicalExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "logicalExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:489:1: logicalExpression : booleanAndExpression ( OR ^ booleanAndExpression )* ;
    public final MemgrammarParser.logicalExpression_return logicalExpression() throws RecognitionException {
        MemgrammarParser.logicalExpression_return retval = new MemgrammarParser.logicalExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token OR338=null;
        MemgrammarParser.booleanAndExpression_return booleanAndExpression337 =null;

        MemgrammarParser.booleanAndExpression_return booleanAndExpression339 =null;


        CommonTree OR338_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:490:2: ( booleanAndExpression ( OR ^ booleanAndExpression )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:490:4: booleanAndExpression ( OR ^ booleanAndExpression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_booleanAndExpression_in_logicalExpression8505);
            booleanAndExpression337=booleanAndExpression();

            state._fsp--;

            adaptor.addChild(root_0, booleanAndExpression337.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:490:25: ( OR ^ booleanAndExpression )*
            loop93:
            do {
                int alt93=2;
                int LA93_0 = input.LA(1);

                if ( (LA93_0==OR) ) {
                    alt93=1;
                }


                switch (alt93) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:490:26: OR ^ booleanAndExpression
            	    {
            	    OR338=(Token)match(input,OR,FOLLOW_OR_in_logicalExpression8508); 
            	    OR338_tree = 
            	    (CommonTree)adaptor.create(OR338)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(OR338_tree, root_0);


            	    pushFollow(FOLLOW_booleanAndExpression_in_logicalExpression8511);
            	    booleanAndExpression339=booleanAndExpression();

            	    state._fsp--;

            	    adaptor.addChild(root_0, booleanAndExpression339.getTree());

            	    }
            	    break;

            	default :
            	    break loop93;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "logicalExpression"


    public static class booleanAndExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "booleanAndExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:492:1: booleanAndExpression : equalityExpression ( AND ^ equalityExpression )* ;
    public final MemgrammarParser.booleanAndExpression_return booleanAndExpression() throws RecognitionException {
        MemgrammarParser.booleanAndExpression_return retval = new MemgrammarParser.booleanAndExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token AND341=null;
        MemgrammarParser.equalityExpression_return equalityExpression340 =null;

        MemgrammarParser.equalityExpression_return equalityExpression342 =null;


        CommonTree AND341_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:493:2: ( equalityExpression ( AND ^ equalityExpression )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:493:4: equalityExpression ( AND ^ equalityExpression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_equalityExpression_in_booleanAndExpression8523);
            equalityExpression340=equalityExpression();

            state._fsp--;

            adaptor.addChild(root_0, equalityExpression340.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:493:23: ( AND ^ equalityExpression )*
            loop94:
            do {
                int alt94=2;
                int LA94_0 = input.LA(1);

                if ( (LA94_0==AND) ) {
                    alt94=1;
                }


                switch (alt94) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:493:24: AND ^ equalityExpression
            	    {
            	    AND341=(Token)match(input,AND,FOLLOW_AND_in_booleanAndExpression8526); 
            	    AND341_tree = 
            	    (CommonTree)adaptor.create(AND341)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(AND341_tree, root_0);


            	    pushFollow(FOLLOW_equalityExpression_in_booleanAndExpression8529);
            	    equalityExpression342=equalityExpression();

            	    state._fsp--;

            	    adaptor.addChild(root_0, equalityExpression342.getTree());

            	    }
            	    break;

            	default :
            	    break loop94;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "booleanAndExpression"


    public static class equalityExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equalityExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:496:1: equalityExpression : nullExpression ( ( EQUALS | NOTEQUALS ) ^ nullExpression )? ;
    public final MemgrammarParser.equalityExpression_return equalityExpression() throws RecognitionException {
        MemgrammarParser.equalityExpression_return retval = new MemgrammarParser.equalityExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set344=null;
        MemgrammarParser.nullExpression_return nullExpression343 =null;

        MemgrammarParser.nullExpression_return nullExpression345 =null;


        CommonTree set344_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:497:2: ( nullExpression ( ( EQUALS | NOTEQUALS ) ^ nullExpression )? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:497:4: nullExpression ( ( EQUALS | NOTEQUALS ) ^ nullExpression )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_nullExpression_in_equalityExpression8542);
            nullExpression343=nullExpression();

            state._fsp--;

            adaptor.addChild(root_0, nullExpression343.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:497:19: ( ( EQUALS | NOTEQUALS ) ^ nullExpression )?
            int alt95=2;
            int LA95_0 = input.LA(1);

            if ( (LA95_0==EQUALS||LA95_0==NOTEQUALS) ) {
                alt95=1;
            }
            switch (alt95) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:497:20: ( EQUALS | NOTEQUALS ) ^ nullExpression
                    {
                    set344=(Token)input.LT(1);

                    set344=(Token)input.LT(1);

                    if ( input.LA(1)==EQUALS||input.LA(1)==NOTEQUALS ) {
                        input.consume();
                        root_0 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(set344)
                        , root_0);
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    pushFollow(FOLLOW_nullExpression_in_equalityExpression8552);
                    nullExpression345=nullExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, nullExpression345.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equalityExpression"


    public static class nullExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "nullExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:499:1: nullExpression : relationalExpression ( ( ISNULL | ISNOTNULL ) ^)? ;
    public final MemgrammarParser.nullExpression_return nullExpression() throws RecognitionException {
        MemgrammarParser.nullExpression_return retval = new MemgrammarParser.nullExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set347=null;
        MemgrammarParser.relationalExpression_return relationalExpression346 =null;


        CommonTree set347_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:500:2: ( relationalExpression ( ( ISNULL | ISNOTNULL ) ^)? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:500:4: relationalExpression ( ( ISNULL | ISNOTNULL ) ^)?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_relationalExpression_in_nullExpression8563);
            relationalExpression346=relationalExpression();

            state._fsp--;

            adaptor.addChild(root_0, relationalExpression346.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:500:25: ( ( ISNULL | ISNOTNULL ) ^)?
            int alt96=2;
            int LA96_0 = input.LA(1);

            if ( ((LA96_0 >= ISNOTNULL && LA96_0 <= ISNULL)) ) {
                alt96=1;
            }
            switch (alt96) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:500:26: ( ISNULL | ISNOTNULL ) ^
                    {
                    set347=(Token)input.LT(1);

                    set347=(Token)input.LT(1);

                    if ( (input.LA(1) >= ISNOTNULL && input.LA(1) <= ISNULL) ) {
                        input.consume();
                        root_0 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(set347)
                        , root_0);
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nullExpression"


    public static class relationalExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relationalExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:503:1: relationalExpression : inExpression ( ( LT | LTEQ | GT | GTEQ | NOTLIKE | LIKE ) ^ inExpression )? ;
    public final MemgrammarParser.relationalExpression_return relationalExpression() throws RecognitionException {
        MemgrammarParser.relationalExpression_return retval = new MemgrammarParser.relationalExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set349=null;
        MemgrammarParser.inExpression_return inExpression348 =null;

        MemgrammarParser.inExpression_return inExpression350 =null;


        CommonTree set349_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:504:2: ( inExpression ( ( LT | LTEQ | GT | GTEQ | NOTLIKE | LIKE ) ^ inExpression )? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:504:4: inExpression ( ( LT | LTEQ | GT | GTEQ | NOTLIKE | LIKE ) ^ inExpression )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_inExpression_in_relationalExpression8583);
            inExpression348=inExpression();

            state._fsp--;

            adaptor.addChild(root_0, inExpression348.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:504:17: ( ( LT | LTEQ | GT | GTEQ | NOTLIKE | LIKE ) ^ inExpression )?
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( ((LA97_0 >= GT && LA97_0 <= GTEQ)||LA97_0==LIKE||(LA97_0 >= LT && LA97_0 <= LTEQ)||LA97_0==NOTLIKE) ) {
                alt97=1;
            }
            switch (alt97) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:504:19: ( LT | LTEQ | GT | GTEQ | NOTLIKE | LIKE ) ^ inExpression
                    {
                    set349=(Token)input.LT(1);

                    set349=(Token)input.LT(1);

                    if ( (input.LA(1) >= GT && input.LA(1) <= GTEQ)||input.LA(1)==LIKE||(input.LA(1) >= LT && input.LA(1) <= LTEQ)||input.LA(1)==NOTLIKE ) {
                        input.consume();
                        root_0 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(set349)
                        , root_0);
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    pushFollow(FOLLOW_inExpression_in_relationalExpression8602);
                    inExpression350=inExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, inExpression350.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relationalExpression"


    public static class inExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "inExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:506:1: inExpression : ( additiveExpression ( in ^ LEFT_PAREN ! selectquery RIGHT_PAREN !)? | colspec ( in LEFT_PAREN literal ( ',' literal )* RIGHT_PAREN ) -> ^( in colspec literal ( literal )* ) );
    public final MemgrammarParser.inExpression_return inExpression() throws RecognitionException {
        MemgrammarParser.inExpression_return retval = new MemgrammarParser.inExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LEFT_PAREN353=null;
        Token RIGHT_PAREN355=null;
        Token LEFT_PAREN358=null;
        Token char_literal360=null;
        Token RIGHT_PAREN362=null;
        MemgrammarParser.additiveExpression_return additiveExpression351 =null;

        MemgrammarParser.in_return in352 =null;

        MemgrammarParser.selectquery_return selectquery354 =null;

        MemgrammarParser.colspec_return colspec356 =null;

        MemgrammarParser.in_return in357 =null;

        MemgrammarParser.literal_return literal359 =null;

        MemgrammarParser.literal_return literal361 =null;


        CommonTree LEFT_PAREN353_tree=null;
        CommonTree RIGHT_PAREN355_tree=null;
        CommonTree LEFT_PAREN358_tree=null;
        CommonTree char_literal360_tree=null;
        CommonTree RIGHT_PAREN362_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleSubtreeStream stream_in=new RewriteRuleSubtreeStream(adaptor,"rule in");
        RewriteRuleSubtreeStream stream_colspec=new RewriteRuleSubtreeStream(adaptor,"rule colspec");
        RewriteRuleSubtreeStream stream_literal=new RewriteRuleSubtreeStream(adaptor,"rule literal");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:507:2: ( additiveExpression ( in ^ LEFT_PAREN ! selectquery RIGHT_PAREN !)? | colspec ( in LEFT_PAREN literal ( ',' literal )* RIGHT_PAREN ) -> ^( in colspec literal ( literal )* ) )
            int alt100=2;
            int LA100_0 = input.LA(1);

            if ( (LA100_0==AVG||(LA100_0 >= BOOLEAN && LA100_0 <= COALESCE)||LA100_0==COUNT||(LA100_0 >= DATEADD && LA100_0 <= DATETIME)||LA100_0==EXTRACT||LA100_0==FLOAT||LA100_0==GETDATE||LA100_0==INTEGER||LA100_0==ISNULLEXP||(LA100_0 >= LEFT_PAREN && LA100_0 <= LENGTH)||LA100_0==MAX||(LA100_0 >= MIN && LA100_0 <= MINUS)||LA100_0==NOT||(LA100_0 >= NULL && LA100_0 <= OBJECTSIZE)||LA100_0==PARAMETER||LA100_0==POSITION||LA100_0==STRING||(LA100_0 >= SUBSTR && LA100_0 <= SUM)||(LA100_0 >= TIMESTAMPADD && LA100_0 <= TIMESTAMPDIFF)||LA100_0==TOLOWER||LA100_0==TOUPPER) ) {
                alt100=1;
            }
            else if ( (LA100_0==IDENT) ) {
                switch ( input.LA(2) ) {
                case 164:
                    {
                    int LA100_3 = input.LA(3);

                    if ( (LA100_3==IDENT) ) {
                        switch ( input.LA(4) ) {
                        case EOF:
                        case AND:
                        case AS:
                        case ASCENDING:
                        case DESCENDING:
                        case DIV:
                        case ELSE:
                        case END:
                        case EQUALS:
                        case FROM:
                        case GROUPBY:
                        case GT:
                        case GTEQ:
                        case HAVING:
                        case INTO:
                        case ISNOTNULL:
                        case ISNULL:
                        case LIKE:
                        case LT:
                        case LTEQ:
                        case MINUS:
                        case MOD:
                        case MULT:
                        case NOTEQUALS:
                        case NOTLIKE:
                        case OR:
                        case ORDERBY:
                        case PLUS:
                        case POW:
                        case RIGHT_PAREN:
                        case STRING:
                        case THEN:
                        case WHEN:
                        case 163:
                        case 165:
                            {
                            alt100=1;
                            }
                            break;
                        case IN:
                        case NOTIN:
                            {
                            int LA100_4 = input.LA(5);

                            if ( (LA100_4==LEFT_PAREN) ) {
                                int LA100_6 = input.LA(6);

                                if ( (LA100_6==SELECT) ) {
                                    alt100=1;
                                }
                                else if ( (LA100_6==BOOLEAN||LA100_6==DATETIME||LA100_6==FLOAT||LA100_6==INTEGER||LA100_6==NULL||LA100_6==STRING) ) {
                                    alt100=2;
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 100, 6, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 100, 4, input);

                                throw nvae;

                            }
                            }
                            break;
                        case 164:
                            {
                            int LA100_7 = input.LA(5);

                            if ( (LA100_7==IDENT) ) {
                                int LA100_9 = input.LA(6);

                                if ( (LA100_9==EOF||(LA100_9 >= AND && LA100_9 <= ASCENDING)||LA100_9==DESCENDING||LA100_9==DIV||LA100_9==ELSE||(LA100_9 >= END && LA100_9 <= EQUALS)||LA100_9==FROM||(LA100_9 >= GROUPBY && LA100_9 <= HAVING)||(LA100_9 >= INTO && LA100_9 <= ISNULL)||LA100_9==LIKE||(LA100_9 >= LT && LA100_9 <= LTEQ)||LA100_9==MINUS||LA100_9==MOD||LA100_9==MULT||LA100_9==NOTEQUALS||LA100_9==NOTLIKE||(LA100_9 >= OR && LA100_9 <= ORDERBY)||LA100_9==PLUS||LA100_9==POW||LA100_9==RIGHT_PAREN||LA100_9==STRING||LA100_9==THEN||LA100_9==WHEN||LA100_9==163||LA100_9==165) ) {
                                    alt100=1;
                                }
                                else if ( (LA100_9==IN||LA100_9==NOTIN) ) {
                                    int LA100_4 = input.LA(7);

                                    if ( (LA100_4==LEFT_PAREN) ) {
                                        int LA100_6 = input.LA(8);

                                        if ( (LA100_6==SELECT) ) {
                                            alt100=1;
                                        }
                                        else if ( (LA100_6==BOOLEAN||LA100_6==DATETIME||LA100_6==FLOAT||LA100_6==INTEGER||LA100_6==NULL||LA100_6==STRING) ) {
                                            alt100=2;
                                        }
                                        else {
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 100, 6, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 100, 4, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 100, 9, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 100, 7, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 100, 5, input);

                            throw nvae;

                        }

                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 100, 3, input);

                        throw nvae;

                    }
                    }
                    break;
                case EOF:
                case AND:
                case AS:
                case ASCENDING:
                case DESCENDING:
                case DIV:
                case ELSE:
                case END:
                case EQUALS:
                case FROM:
                case GROUPBY:
                case GT:
                case GTEQ:
                case HAVING:
                case INTO:
                case ISNOTNULL:
                case ISNULL:
                case LIKE:
                case LT:
                case LTEQ:
                case MINUS:
                case MOD:
                case MULT:
                case NOTEQUALS:
                case NOTLIKE:
                case OR:
                case ORDERBY:
                case PLUS:
                case POW:
                case RIGHT_PAREN:
                case STRING:
                case THEN:
                case WHEN:
                case 163:
                case 165:
                    {
                    alt100=1;
                    }
                    break;
                case IN:
                case NOTIN:
                    {
                    int LA100_4 = input.LA(3);

                    if ( (LA100_4==LEFT_PAREN) ) {
                        int LA100_6 = input.LA(4);

                        if ( (LA100_6==SELECT) ) {
                            alt100=1;
                        }
                        else if ( (LA100_6==BOOLEAN||LA100_6==DATETIME||LA100_6==FLOAT||LA100_6==INTEGER||LA100_6==NULL||LA100_6==STRING) ) {
                            alt100=2;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 100, 6, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 100, 4, input);

                        throw nvae;

                    }
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 100, 2, input);

                    throw nvae;

                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 100, 0, input);

                throw nvae;

            }
            switch (alt100) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:508:3: additiveExpression ( in ^ LEFT_PAREN ! selectquery RIGHT_PAREN !)?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_additiveExpression_in_inExpression8617);
                    additiveExpression351=additiveExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, additiveExpression351.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:508:22: ( in ^ LEFT_PAREN ! selectquery RIGHT_PAREN !)?
                    int alt98=2;
                    int LA98_0 = input.LA(1);

                    if ( (LA98_0==IN||LA98_0==NOTIN) ) {
                        alt98=1;
                    }
                    switch (alt98) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:508:23: in ^ LEFT_PAREN ! selectquery RIGHT_PAREN !
                            {
                            pushFollow(FOLLOW_in_in_inExpression8620);
                            in352=in();

                            state._fsp--;

                            root_0 = (CommonTree)adaptor.becomeRoot(in352.getTree(), root_0);

                            LEFT_PAREN353=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_inExpression8623); 

                            pushFollow(FOLLOW_selectquery_in_inExpression8626);
                            selectquery354=selectquery();

                            state._fsp--;

                            adaptor.addChild(root_0, selectquery354.getTree());

                            RIGHT_PAREN355=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_inExpression8628); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:3: colspec ( in LEFT_PAREN literal ( ',' literal )* RIGHT_PAREN )
                    {
                    pushFollow(FOLLOW_colspec_in_inExpression8637);
                    colspec356=colspec();

                    state._fsp--;

                    stream_colspec.add(colspec356.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:11: ( in LEFT_PAREN literal ( ',' literal )* RIGHT_PAREN )
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:12: in LEFT_PAREN literal ( ',' literal )* RIGHT_PAREN
                    {
                    pushFollow(FOLLOW_in_in_inExpression8640);
                    in357=in();

                    state._fsp--;

                    stream_in.add(in357.getTree());

                    LEFT_PAREN358=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_inExpression8642);  
                    stream_LEFT_PAREN.add(LEFT_PAREN358);


                    pushFollow(FOLLOW_literal_in_inExpression8644);
                    literal359=literal();

                    state._fsp--;

                    stream_literal.add(literal359.getTree());

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:34: ( ',' literal )*
                    loop99:
                    do {
                        int alt99=2;
                        int LA99_0 = input.LA(1);

                        if ( (LA99_0==163) ) {
                            alt99=1;
                        }


                        switch (alt99) {
                    	case 1 :
                    	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:35: ',' literal
                    	    {
                    	    char_literal360=(Token)match(input,163,FOLLOW_163_in_inExpression8647);  
                    	    stream_163.add(char_literal360);


                    	    pushFollow(FOLLOW_literal_in_inExpression8649);
                    	    literal361=literal();

                    	    state._fsp--;

                    	    stream_literal.add(literal361.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop99;
                        }
                    } while (true);


                    RIGHT_PAREN362=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_inExpression8653);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN362);


                    }


                    // AST REWRITE
                    // elements: literal, in, colspec, literal
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 509:62: -> ^( in colspec literal ( literal )* )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:65: ^( in colspec literal ( literal )* )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(stream_in.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_colspec.nextTree());

                        adaptor.addChild(root_1, stream_literal.nextTree());

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:509:86: ( literal )*
                        while ( stream_literal.hasNext() ) {
                            adaptor.addChild(root_1, stream_literal.nextTree());

                        }
                        stream_literal.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "inExpression"


    public static class additiveExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "additiveExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:511:1: additiveExpression : multiplicativeExpression ( ( PLUS | MINUS ) ^ multiplicativeExpression )* ;
    public final MemgrammarParser.additiveExpression_return additiveExpression() throws RecognitionException {
        MemgrammarParser.additiveExpression_return retval = new MemgrammarParser.additiveExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set364=null;
        MemgrammarParser.multiplicativeExpression_return multiplicativeExpression363 =null;

        MemgrammarParser.multiplicativeExpression_return multiplicativeExpression365 =null;


        CommonTree set364_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:512:2: ( multiplicativeExpression ( ( PLUS | MINUS ) ^ multiplicativeExpression )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:512:4: multiplicativeExpression ( ( PLUS | MINUS ) ^ multiplicativeExpression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression8676);
            multiplicativeExpression363=multiplicativeExpression();

            state._fsp--;

            adaptor.addChild(root_0, multiplicativeExpression363.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:512:29: ( ( PLUS | MINUS ) ^ multiplicativeExpression )*
            loop101:
            do {
                int alt101=2;
                int LA101_0 = input.LA(1);

                if ( (LA101_0==MINUS||LA101_0==PLUS) ) {
                    alt101=1;
                }


                switch (alt101) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:512:31: ( PLUS | MINUS ) ^ multiplicativeExpression
            	    {
            	    set364=(Token)input.LT(1);

            	    set364=(Token)input.LT(1);

            	    if ( input.LA(1)==MINUS||input.LA(1)==PLUS ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot(
            	        (CommonTree)adaptor.create(set364)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression8687);
            	    multiplicativeExpression365=multiplicativeExpression();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multiplicativeExpression365.getTree());

            	    }
            	    break;

            	default :
            	    break loop101;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "additiveExpression"


    public static class multiplicativeExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multiplicativeExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:515:1: multiplicativeExpression : powerExpression ( ( MULT | DIV | MOD ) ^ powerExpression )* ;
    public final MemgrammarParser.multiplicativeExpression_return multiplicativeExpression() throws RecognitionException {
        MemgrammarParser.multiplicativeExpression_return retval = new MemgrammarParser.multiplicativeExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set367=null;
        MemgrammarParser.powerExpression_return powerExpression366 =null;

        MemgrammarParser.powerExpression_return powerExpression368 =null;


        CommonTree set367_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:516:2: ( powerExpression ( ( MULT | DIV | MOD ) ^ powerExpression )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:516:4: powerExpression ( ( MULT | DIV | MOD ) ^ powerExpression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_powerExpression_in_multiplicativeExpression8701);
            powerExpression366=powerExpression();

            state._fsp--;

            adaptor.addChild(root_0, powerExpression366.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:516:20: ( ( MULT | DIV | MOD ) ^ powerExpression )*
            loop102:
            do {
                int alt102=2;
                int LA102_0 = input.LA(1);

                if ( (LA102_0==DIV||LA102_0==MOD||LA102_0==MULT) ) {
                    alt102=1;
                }


                switch (alt102) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:516:22: ( MULT | DIV | MOD ) ^ powerExpression
            	    {
            	    set367=(Token)input.LT(1);

            	    set367=(Token)input.LT(1);

            	    if ( input.LA(1)==DIV||input.LA(1)==MOD||input.LA(1)==MULT ) {
            	        input.consume();
            	        root_0 = (CommonTree)adaptor.becomeRoot(
            	        (CommonTree)adaptor.create(set367)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_powerExpression_in_multiplicativeExpression8714);
            	    powerExpression368=powerExpression();

            	    state._fsp--;

            	    adaptor.addChild(root_0, powerExpression368.getTree());

            	    }
            	    break;

            	default :
            	    break loop102;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multiplicativeExpression"


    public static class powerExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "powerExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:519:1: powerExpression : unaryExpression ( POW ^ unaryExpression )* ;
    public final MemgrammarParser.powerExpression_return powerExpression() throws RecognitionException {
        MemgrammarParser.powerExpression_return retval = new MemgrammarParser.powerExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token POW370=null;
        MemgrammarParser.unaryExpression_return unaryExpression369 =null;

        MemgrammarParser.unaryExpression_return unaryExpression371 =null;


        CommonTree POW370_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:520:2: ( unaryExpression ( POW ^ unaryExpression )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:520:4: unaryExpression ( POW ^ unaryExpression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_unaryExpression_in_powerExpression8729);
            unaryExpression369=unaryExpression();

            state._fsp--;

            adaptor.addChild(root_0, unaryExpression369.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:520:20: ( POW ^ unaryExpression )*
            loop103:
            do {
                int alt103=2;
                int LA103_0 = input.LA(1);

                if ( (LA103_0==POW) ) {
                    alt103=1;
                }


                switch (alt103) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:520:22: POW ^ unaryExpression
            	    {
            	    POW370=(Token)match(input,POW,FOLLOW_POW_in_powerExpression8733); 
            	    POW370_tree = 
            	    (CommonTree)adaptor.create(POW370)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(POW370_tree, root_0);


            	    pushFollow(FOLLOW_unaryExpression_in_powerExpression8736);
            	    unaryExpression371=unaryExpression();

            	    state._fsp--;

            	    adaptor.addChild(root_0, unaryExpression371.getTree());

            	    }
            	    break;

            	default :
            	    break loop103;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "powerExpression"


    public static class unaryExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unaryExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:523:1: unaryExpression : ( primaryExpression | NOT ^ primaryExpression | MINUS primaryExpression -> ^( NEGATE primaryExpression ) );
    public final MemgrammarParser.unaryExpression_return unaryExpression() throws RecognitionException {
        MemgrammarParser.unaryExpression_return retval = new MemgrammarParser.unaryExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NOT373=null;
        Token MINUS375=null;
        MemgrammarParser.primaryExpression_return primaryExpression372 =null;

        MemgrammarParser.primaryExpression_return primaryExpression374 =null;

        MemgrammarParser.primaryExpression_return primaryExpression376 =null;


        CommonTree NOT373_tree=null;
        CommonTree MINUS375_tree=null;
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleSubtreeStream stream_primaryExpression=new RewriteRuleSubtreeStream(adaptor,"rule primaryExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:524:2: ( primaryExpression | NOT ^ primaryExpression | MINUS primaryExpression -> ^( NEGATE primaryExpression ) )
            int alt104=3;
            switch ( input.LA(1) ) {
            case AVG:
            case BOOLEAN:
            case CASE:
            case CAST:
            case COALESCE:
            case COUNT:
            case DATEADD:
            case DATEDIFF:
            case DATEPART:
            case DATETIME:
            case EXTRACT:
            case FLOAT:
            case GETDATE:
            case IDENT:
            case INTEGER:
            case ISNULLEXP:
            case LEFT_PAREN:
            case LEN:
            case LENGTH:
            case MAX:
            case MIN:
            case NULL:
            case OBJECTSIZE:
            case PARAMETER:
            case POSITION:
            case STRING:
            case SUBSTR:
            case SUM:
            case TIMESTAMPADD:
            case TIMESTAMPDIFF:
            case TOLOWER:
            case TOUPPER:
                {
                alt104=1;
                }
                break;
            case NOT:
                {
                alt104=2;
                }
                break;
            case MINUS:
                {
                alt104=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 104, 0, input);

                throw nvae;

            }

            switch (alt104) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:524:4: primaryExpression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression8750);
                    primaryExpression372=primaryExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, primaryExpression372.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:525:8: NOT ^ primaryExpression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    NOT373=(Token)match(input,NOT,FOLLOW_NOT_in_unaryExpression8759); 
                    NOT373_tree = 
                    (CommonTree)adaptor.create(NOT373)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(NOT373_tree, root_0);


                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression8762);
                    primaryExpression374=primaryExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, primaryExpression374.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:526:8: MINUS primaryExpression
                    {
                    MINUS375=(Token)match(input,MINUS,FOLLOW_MINUS_in_unaryExpression8771);  
                    stream_MINUS.add(MINUS375);


                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression8773);
                    primaryExpression376=primaryExpression();

                    state._fsp--;

                    stream_primaryExpression.add(primaryExpression376.getTree());

                    // AST REWRITE
                    // elements: primaryExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 526:32: -> ^( NEGATE primaryExpression )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:526:35: ^( NEGATE primaryExpression )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(NEGATE, "NEGATE")
                        , root_1);

                        adaptor.addChild(root_1, stream_primaryExpression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unaryExpression"


    public static class primaryExpression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "primaryExpression"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:528:1: primaryExpression : ( value | LEFT_PAREN logicalExpression RIGHT_PAREN -> ^( PAREN logicalExpression ) );
    public final MemgrammarParser.primaryExpression_return primaryExpression() throws RecognitionException {
        MemgrammarParser.primaryExpression_return retval = new MemgrammarParser.primaryExpression_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LEFT_PAREN378=null;
        Token RIGHT_PAREN380=null;
        MemgrammarParser.value_return value377 =null;

        MemgrammarParser.logicalExpression_return logicalExpression379 =null;


        CommonTree LEFT_PAREN378_tree=null;
        CommonTree RIGHT_PAREN380_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:529:2: ( value | LEFT_PAREN logicalExpression RIGHT_PAREN -> ^( PAREN logicalExpression ) )
            int alt105=2;
            int LA105_0 = input.LA(1);

            if ( (LA105_0==AVG||(LA105_0 >= BOOLEAN && LA105_0 <= COALESCE)||LA105_0==COUNT||(LA105_0 >= DATEADD && LA105_0 <= DATETIME)||LA105_0==EXTRACT||LA105_0==FLOAT||LA105_0==GETDATE||LA105_0==IDENT||LA105_0==INTEGER||LA105_0==ISNULLEXP||(LA105_0 >= LEN && LA105_0 <= LENGTH)||LA105_0==MAX||LA105_0==MIN||(LA105_0 >= NULL && LA105_0 <= OBJECTSIZE)||LA105_0==PARAMETER||LA105_0==POSITION||LA105_0==STRING||(LA105_0 >= SUBSTR && LA105_0 <= SUM)||(LA105_0 >= TIMESTAMPADD && LA105_0 <= TIMESTAMPDIFF)||LA105_0==TOLOWER||LA105_0==TOUPPER) ) {
                alt105=1;
            }
            else if ( (LA105_0==LEFT_PAREN) ) {
                alt105=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 105, 0, input);

                throw nvae;

            }
            switch (alt105) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:529:4: value
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_value_in_primaryExpression8797);
                    value377=value();

                    state._fsp--;

                    adaptor.addChild(root_0, value377.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:530:3: LEFT_PAREN logicalExpression RIGHT_PAREN
                    {
                    LEFT_PAREN378=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_primaryExpression8803);  
                    stream_LEFT_PAREN.add(LEFT_PAREN378);


                    pushFollow(FOLLOW_logicalExpression_in_primaryExpression8805);
                    logicalExpression379=logicalExpression();

                    state._fsp--;

                    stream_logicalExpression.add(logicalExpression379.getTree());

                    RIGHT_PAREN380=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_primaryExpression8807);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN380);


                    // AST REWRITE
                    // elements: logicalExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 530:44: -> ^( PAREN logicalExpression )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:530:47: ^( PAREN logicalExpression )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        (CommonTree)adaptor.create(PAREN, "PAREN")
                        , root_1);

                        adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "primaryExpression"


    public static class in_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "in"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:532:1: in : ( IN | NOTIN );
    public final MemgrammarParser.in_return in() throws RecognitionException {
        MemgrammarParser.in_return retval = new MemgrammarParser.in_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set381=null;

        CommonTree set381_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:532:4: ( IN | NOTIN )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set381=(Token)input.LT(1);

            if ( input.LA(1)==IN||input.LA(1)==NOTIN ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set381)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "in"


    public static class value_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "value"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:534:1: value : ( INTEGER | FLOAT | DATETIME | BOOLEAN | STRING | casewhen | aggrule LEFT_PAREN ( DISTINCT )? logicalExpression RIGHT_PAREN -> ^( aggrule logicalExpression ( DISTINCT )? ) | COUNT LEFT_PAREN '*' RIGHT_PAREN -> ^( COUNT '*' ) | colspec | datemethod | stringmethod | utilmethod | PARAMETER | NULL );
    public final MemgrammarParser.value_return value() throws RecognitionException {
        MemgrammarParser.value_return retval = new MemgrammarParser.value_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INTEGER382=null;
        Token FLOAT383=null;
        Token DATETIME384=null;
        Token BOOLEAN385=null;
        Token STRING386=null;
        Token LEFT_PAREN389=null;
        Token DISTINCT390=null;
        Token RIGHT_PAREN392=null;
        Token COUNT393=null;
        Token LEFT_PAREN394=null;
        Token char_literal395=null;
        Token RIGHT_PAREN396=null;
        Token PARAMETER401=null;
        Token NULL402=null;
        MemgrammarParser.casewhen_return casewhen387 =null;

        MemgrammarParser.aggrule_return aggrule388 =null;

        MemgrammarParser.logicalExpression_return logicalExpression391 =null;

        MemgrammarParser.colspec_return colspec397 =null;

        MemgrammarParser.datemethod_return datemethod398 =null;

        MemgrammarParser.stringmethod_return stringmethod399 =null;

        MemgrammarParser.utilmethod_return utilmethod400 =null;


        CommonTree INTEGER382_tree=null;
        CommonTree FLOAT383_tree=null;
        CommonTree DATETIME384_tree=null;
        CommonTree BOOLEAN385_tree=null;
        CommonTree STRING386_tree=null;
        CommonTree LEFT_PAREN389_tree=null;
        CommonTree DISTINCT390_tree=null;
        CommonTree RIGHT_PAREN392_tree=null;
        CommonTree COUNT393_tree=null;
        CommonTree LEFT_PAREN394_tree=null;
        CommonTree char_literal395_tree=null;
        CommonTree RIGHT_PAREN396_tree=null;
        CommonTree PARAMETER401_tree=null;
        CommonTree NULL402_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_COUNT=new RewriteRuleTokenStream(adaptor,"token COUNT");
        RewriteRuleTokenStream stream_MULT=new RewriteRuleTokenStream(adaptor,"token MULT");
        RewriteRuleTokenStream stream_DISTINCT=new RewriteRuleTokenStream(adaptor,"token DISTINCT");
        RewriteRuleSubtreeStream stream_aggrule=new RewriteRuleSubtreeStream(adaptor,"rule aggrule");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:535:2: ( INTEGER | FLOAT | DATETIME | BOOLEAN | STRING | casewhen | aggrule LEFT_PAREN ( DISTINCT )? logicalExpression RIGHT_PAREN -> ^( aggrule logicalExpression ( DISTINCT )? ) | COUNT LEFT_PAREN '*' RIGHT_PAREN -> ^( COUNT '*' ) | colspec | datemethod | stringmethod | utilmethod | PARAMETER | NULL )
            int alt107=14;
            switch ( input.LA(1) ) {
            case INTEGER:
                {
                alt107=1;
                }
                break;
            case FLOAT:
                {
                alt107=2;
                }
                break;
            case DATETIME:
                {
                alt107=3;
                }
                break;
            case BOOLEAN:
                {
                alt107=4;
                }
                break;
            case STRING:
                {
                alt107=5;
                }
                break;
            case CASE:
                {
                alt107=6;
                }
                break;
            case COUNT:
                {
                int LA107_7 = input.LA(2);

                if ( (LA107_7==LEFT_PAREN) ) {
                    int LA107_15 = input.LA(3);

                    if ( (LA107_15==MULT) ) {
                        alt107=8;
                    }
                    else if ( (LA107_15==AVG||(LA107_15 >= BOOLEAN && LA107_15 <= COALESCE)||LA107_15==COUNT||(LA107_15 >= DATEADD && LA107_15 <= DATETIME)||LA107_15==DISTINCT||LA107_15==EXTRACT||LA107_15==FLOAT||LA107_15==GETDATE||LA107_15==IDENT||LA107_15==INTEGER||LA107_15==ISNULLEXP||(LA107_15 >= LEFT_PAREN && LA107_15 <= LENGTH)||LA107_15==MAX||(LA107_15 >= MIN && LA107_15 <= MINUS)||LA107_15==NOT||(LA107_15 >= NULL && LA107_15 <= OBJECTSIZE)||LA107_15==PARAMETER||LA107_15==POSITION||LA107_15==STRING||(LA107_15 >= SUBSTR && LA107_15 <= SUM)||(LA107_15 >= TIMESTAMPADD && LA107_15 <= TIMESTAMPDIFF)||LA107_15==TOLOWER||LA107_15==TOUPPER) ) {
                        alt107=7;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 107, 15, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 107, 7, input);

                    throw nvae;

                }
                }
                break;
            case AVG:
            case MAX:
            case MIN:
            case SUM:
                {
                alt107=7;
                }
                break;
            case IDENT:
                {
                alt107=9;
                }
                break;
            case DATEADD:
            case DATEDIFF:
            case DATEPART:
            case EXTRACT:
            case GETDATE:
            case TIMESTAMPADD:
            case TIMESTAMPDIFF:
                {
                alt107=10;
                }
                break;
            case LEN:
            case LENGTH:
            case POSITION:
            case SUBSTR:
            case TOLOWER:
            case TOUPPER:
                {
                alt107=11;
                }
                break;
            case CAST:
            case COALESCE:
            case ISNULLEXP:
            case OBJECTSIZE:
                {
                alt107=12;
                }
                break;
            case PARAMETER:
                {
                alt107=13;
                }
                break;
            case NULL:
                {
                alt107=14;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 107, 0, input);

                throw nvae;

            }

            switch (alt107) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:535:5: INTEGER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    INTEGER382=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value8839); 
                    INTEGER382_tree = 
                    (CommonTree)adaptor.create(INTEGER382)
                    ;
                    adaptor.addChild(root_0, INTEGER382_tree);


                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:536:4: FLOAT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    FLOAT383=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_value8844); 
                    FLOAT383_tree = 
                    (CommonTree)adaptor.create(FLOAT383)
                    ;
                    adaptor.addChild(root_0, FLOAT383_tree);


                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:537:5: DATETIME
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    DATETIME384=(Token)match(input,DATETIME,FOLLOW_DATETIME_in_value8850); 
                    DATETIME384_tree = 
                    (CommonTree)adaptor.create(DATETIME384)
                    ;
                    adaptor.addChild(root_0, DATETIME384_tree);


                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:538:4: BOOLEAN
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    BOOLEAN385=(Token)match(input,BOOLEAN,FOLLOW_BOOLEAN_in_value8855); 
                    BOOLEAN385_tree = 
                    (CommonTree)adaptor.create(BOOLEAN385)
                    ;
                    adaptor.addChild(root_0, BOOLEAN385_tree);


                    }
                    break;
                case 5 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:539:4: STRING
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    STRING386=(Token)match(input,STRING,FOLLOW_STRING_in_value8860); 
                    STRING386_tree = 
                    (CommonTree)adaptor.create(STRING386)
                    ;
                    adaptor.addChild(root_0, STRING386_tree);


                    }
                    break;
                case 6 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:540:4: casewhen
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_casewhen_in_value8865);
                    casewhen387=casewhen();

                    state._fsp--;

                    adaptor.addChild(root_0, casewhen387.getTree());

                    }
                    break;
                case 7 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:541:4: aggrule LEFT_PAREN ( DISTINCT )? logicalExpression RIGHT_PAREN
                    {
                    pushFollow(FOLLOW_aggrule_in_value8870);
                    aggrule388=aggrule();

                    state._fsp--;

                    stream_aggrule.add(aggrule388.getTree());

                    LEFT_PAREN389=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_value8872);  
                    stream_LEFT_PAREN.add(LEFT_PAREN389);


                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:541:23: ( DISTINCT )?
                    int alt106=2;
                    int LA106_0 = input.LA(1);

                    if ( (LA106_0==DISTINCT) ) {
                        alt106=1;
                    }
                    switch (alt106) {
                        case 1 :
                            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:541:23: DISTINCT
                            {
                            DISTINCT390=(Token)match(input,DISTINCT,FOLLOW_DISTINCT_in_value8874);  
                            stream_DISTINCT.add(DISTINCT390);


                            }
                            break;

                    }


                    pushFollow(FOLLOW_logicalExpression_in_value8877);
                    logicalExpression391=logicalExpression();

                    state._fsp--;

                    stream_logicalExpression.add(logicalExpression391.getTree());

                    RIGHT_PAREN392=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_value8879);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN392);


                    // AST REWRITE
                    // elements: aggrule, DISTINCT, logicalExpression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 541:63: -> ^( aggrule logicalExpression ( DISTINCT )? )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:541:66: ^( aggrule logicalExpression ( DISTINCT )? )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(stream_aggrule.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:541:94: ( DISTINCT )?
                        if ( stream_DISTINCT.hasNext() ) {
                            adaptor.addChild(root_1, 
                            stream_DISTINCT.nextNode()
                            );

                        }
                        stream_DISTINCT.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 8 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:542:4: COUNT LEFT_PAREN '*' RIGHT_PAREN
                    {
                    COUNT393=(Token)match(input,COUNT,FOLLOW_COUNT_in_value8895);  
                    stream_COUNT.add(COUNT393);


                    LEFT_PAREN394=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_value8897);  
                    stream_LEFT_PAREN.add(LEFT_PAREN394);


                    char_literal395=(Token)match(input,MULT,FOLLOW_MULT_in_value8899);  
                    stream_MULT.add(char_literal395);


                    RIGHT_PAREN396=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_value8901);  
                    stream_RIGHT_PAREN.add(RIGHT_PAREN396);


                    // AST REWRITE
                    // elements: COUNT, MULT
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 542:37: -> ^( COUNT '*' )
                    {
                        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:542:40: ^( COUNT '*' )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_COUNT.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_MULT.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 9 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:543:4: colspec
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_colspec_in_value8914);
                    colspec397=colspec();

                    state._fsp--;

                    adaptor.addChild(root_0, colspec397.getTree());

                    }
                    break;
                case 10 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:544:4: datemethod
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_datemethod_in_value8919);
                    datemethod398=datemethod();

                    state._fsp--;

                    adaptor.addChild(root_0, datemethod398.getTree());

                    }
                    break;
                case 11 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:545:4: stringmethod
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_stringmethod_in_value8924);
                    stringmethod399=stringmethod();

                    state._fsp--;

                    adaptor.addChild(root_0, stringmethod399.getTree());

                    }
                    break;
                case 12 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:546:4: utilmethod
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_utilmethod_in_value8929);
                    utilmethod400=utilmethod();

                    state._fsp--;

                    adaptor.addChild(root_0, utilmethod400.getTree());

                    }
                    break;
                case 13 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:547:4: PARAMETER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    PARAMETER401=(Token)match(input,PARAMETER,FOLLOW_PARAMETER_in_value8934); 
                    PARAMETER401_tree = 
                    (CommonTree)adaptor.create(PARAMETER401)
                    ;
                    adaptor.addChild(root_0, PARAMETER401_tree);


                    }
                    break;
                case 14 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:548:4: NULL
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    NULL402=(Token)match(input,NULL,FOLLOW_NULL_in_value8939); 
                    NULL402_tree = 
                    (CommonTree)adaptor.create(NULL402)
                    ;
                    adaptor.addChild(root_0, NULL402_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "value"


    public static class stringmethod_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stringmethod"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:550:1: stringmethod : ( substring | length | position | toupper | tolower );
    public final MemgrammarParser.stringmethod_return stringmethod() throws RecognitionException {
        MemgrammarParser.stringmethod_return retval = new MemgrammarParser.stringmethod_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.substring_return substring403 =null;

        MemgrammarParser.length_return length404 =null;

        MemgrammarParser.position_return position405 =null;

        MemgrammarParser.toupper_return toupper406 =null;

        MemgrammarParser.tolower_return tolower407 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:2: ( substring | length | position | toupper | tolower )
            int alt108=5;
            switch ( input.LA(1) ) {
            case SUBSTR:
                {
                alt108=1;
                }
                break;
            case LEN:
            case LENGTH:
                {
                alt108=2;
                }
                break;
            case POSITION:
                {
                alt108=3;
                }
                break;
            case TOUPPER:
                {
                alt108=4;
                }
                break;
            case TOLOWER:
                {
                alt108=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 108, 0, input);

                throw nvae;

            }

            switch (alt108) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:4: substring
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_substring_in_stringmethod8950);
                    substring403=substring();

                    state._fsp--;

                    adaptor.addChild(root_0, substring403.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:16: length
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_length_in_stringmethod8954);
                    length404=length();

                    state._fsp--;

                    adaptor.addChild(root_0, length404.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:25: position
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_position_in_stringmethod8958);
                    position405=position();

                    state._fsp--;

                    adaptor.addChild(root_0, position405.getTree());

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:36: toupper
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_toupper_in_stringmethod8962);
                    toupper406=toupper();

                    state._fsp--;

                    adaptor.addChild(root_0, toupper406.getTree());

                    }
                    break;
                case 5 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:551:46: tolower
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_tolower_in_stringmethod8966);
                    tolower407=tolower();

                    state._fsp--;

                    adaptor.addChild(root_0, tolower407.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stringmethod"


    public static class substring_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "substring"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:553:1: substring : SUBSTR ^ '(' ! logicalExpression ',' ! logicalExpression ( ',' ! logicalExpression )? ')' !;
    public final MemgrammarParser.substring_return substring() throws RecognitionException {
        MemgrammarParser.substring_return retval = new MemgrammarParser.substring_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token SUBSTR408=null;
        Token char_literal409=null;
        Token char_literal411=null;
        Token char_literal413=null;
        Token char_literal415=null;
        MemgrammarParser.logicalExpression_return logicalExpression410 =null;

        MemgrammarParser.logicalExpression_return logicalExpression412 =null;

        MemgrammarParser.logicalExpression_return logicalExpression414 =null;


        CommonTree SUBSTR408_tree=null;
        CommonTree char_literal409_tree=null;
        CommonTree char_literal411_tree=null;
        CommonTree char_literal413_tree=null;
        CommonTree char_literal415_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:554:2: ( SUBSTR ^ '(' ! logicalExpression ',' ! logicalExpression ( ',' ! logicalExpression )? ')' !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:554:4: SUBSTR ^ '(' ! logicalExpression ',' ! logicalExpression ( ',' ! logicalExpression )? ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            SUBSTR408=(Token)match(input,SUBSTR,FOLLOW_SUBSTR_in_substring8976); 
            SUBSTR408_tree = 
            (CommonTree)adaptor.create(SUBSTR408)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(SUBSTR408_tree, root_0);


            char_literal409=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_substring8979); 

            pushFollow(FOLLOW_logicalExpression_in_substring8982);
            logicalExpression410=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression410.getTree());

            char_literal411=(Token)match(input,163,FOLLOW_163_in_substring8984); 

            pushFollow(FOLLOW_logicalExpression_in_substring8987);
            logicalExpression412=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression412.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:554:58: ( ',' ! logicalExpression )?
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==163) ) {
                alt109=1;
            }
            switch (alt109) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:554:59: ',' ! logicalExpression
                    {
                    char_literal413=(Token)match(input,163,FOLLOW_163_in_substring8990); 

                    pushFollow(FOLLOW_logicalExpression_in_substring8993);
                    logicalExpression414=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression414.getTree());

                    }
                    break;

            }


            char_literal415=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_substring8997); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "substring"


    public static class length_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "length"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:555:1: length : ( LENGTH | LEN ) ^ '(' ! logicalExpression ')' !;
    public final MemgrammarParser.length_return length() throws RecognitionException {
        MemgrammarParser.length_return retval = new MemgrammarParser.length_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token set416=null;
        Token char_literal417=null;
        Token char_literal419=null;
        MemgrammarParser.logicalExpression_return logicalExpression418 =null;


        CommonTree set416_tree=null;
        CommonTree char_literal417_tree=null;
        CommonTree char_literal419_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:555:8: ( ( LENGTH | LEN ) ^ '(' ! logicalExpression ')' !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:555:10: ( LENGTH | LEN ) ^ '(' ! logicalExpression ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            set416=(Token)input.LT(1);

            set416=(Token)input.LT(1);

            if ( (input.LA(1) >= LEN && input.LA(1) <= LENGTH) ) {
                input.consume();
                root_0 = (CommonTree)adaptor.becomeRoot(
                (CommonTree)adaptor.create(set416)
                , root_0);
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            char_literal417=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_length9014); 

            pushFollow(FOLLOW_logicalExpression_in_length9017);
            logicalExpression418=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression418.getTree());

            char_literal419=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_length9019); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "length"


    public static class position_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "position"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:556:1: position : POSITION ^ '(' ! logicalExpression ',' ! logicalExpression ')' !;
    public final MemgrammarParser.position_return position() throws RecognitionException {
        MemgrammarParser.position_return retval = new MemgrammarParser.position_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token POSITION420=null;
        Token char_literal421=null;
        Token char_literal423=null;
        Token char_literal425=null;
        MemgrammarParser.logicalExpression_return logicalExpression422 =null;

        MemgrammarParser.logicalExpression_return logicalExpression424 =null;


        CommonTree POSITION420_tree=null;
        CommonTree char_literal421_tree=null;
        CommonTree char_literal423_tree=null;
        CommonTree char_literal425_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:557:2: ( POSITION ^ '(' ! logicalExpression ',' ! logicalExpression ')' !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:557:4: POSITION ^ '(' ! logicalExpression ',' ! logicalExpression ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            POSITION420=(Token)match(input,POSITION,FOLLOW_POSITION_in_position9028); 
            POSITION420_tree = 
            (CommonTree)adaptor.create(POSITION420)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(POSITION420_tree, root_0);


            char_literal421=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_position9031); 

            pushFollow(FOLLOW_logicalExpression_in_position9034);
            logicalExpression422=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression422.getTree());

            char_literal423=(Token)match(input,163,FOLLOW_163_in_position9036); 

            pushFollow(FOLLOW_logicalExpression_in_position9039);
            logicalExpression424=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression424.getTree());

            char_literal425=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_position9041); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "position"


    public static class toupper_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "toupper"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:558:1: toupper : TOUPPER ^ '(' ! logicalExpression ')' !;
    public final MemgrammarParser.toupper_return toupper() throws RecognitionException {
        MemgrammarParser.toupper_return retval = new MemgrammarParser.toupper_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token TOUPPER426=null;
        Token char_literal427=null;
        Token char_literal429=null;
        MemgrammarParser.logicalExpression_return logicalExpression428 =null;


        CommonTree TOUPPER426_tree=null;
        CommonTree char_literal427_tree=null;
        CommonTree char_literal429_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:558:8: ( TOUPPER ^ '(' ! logicalExpression ')' !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:558:10: TOUPPER ^ '(' ! logicalExpression ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            TOUPPER426=(Token)match(input,TOUPPER,FOLLOW_TOUPPER_in_toupper9048); 
            TOUPPER426_tree = 
            (CommonTree)adaptor.create(TOUPPER426)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(TOUPPER426_tree, root_0);


            char_literal427=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_toupper9051); 

            pushFollow(FOLLOW_logicalExpression_in_toupper9054);
            logicalExpression428=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression428.getTree());

            char_literal429=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_toupper9056); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "toupper"


    public static class tolower_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "tolower"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:560:1: tolower : TOLOWER ^ '(' ! logicalExpression ')' !;
    public final MemgrammarParser.tolower_return tolower() throws RecognitionException {
        MemgrammarParser.tolower_return retval = new MemgrammarParser.tolower_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token TOLOWER430=null;
        Token char_literal431=null;
        Token char_literal433=null;
        MemgrammarParser.logicalExpression_return logicalExpression432 =null;


        CommonTree TOLOWER430_tree=null;
        CommonTree char_literal431_tree=null;
        CommonTree char_literal433_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:560:8: ( TOLOWER ^ '(' ! logicalExpression ')' !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:560:10: TOLOWER ^ '(' ! logicalExpression ')' !
            {
            root_0 = (CommonTree)adaptor.nil();


            TOLOWER430=(Token)match(input,TOLOWER,FOLLOW_TOLOWER_in_tolower9064); 
            TOLOWER430_tree = 
            (CommonTree)adaptor.create(TOLOWER430)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(TOLOWER430_tree, root_0);


            char_literal431=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_tolower9067); 

            pushFollow(FOLLOW_logicalExpression_in_tolower9070);
            logicalExpression432=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression432.getTree());

            char_literal433=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_tolower9072); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "tolower"


    public static class utilmethod_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "utilmethod"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:562:1: utilmethod : ( cast | isnull | coalesce | objectsize );
    public final MemgrammarParser.utilmethod_return utilmethod() throws RecognitionException {
        MemgrammarParser.utilmethod_return retval = new MemgrammarParser.utilmethod_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.cast_return cast434 =null;

        MemgrammarParser.isnull_return isnull435 =null;

        MemgrammarParser.coalesce_return coalesce436 =null;

        MemgrammarParser.objectsize_return objectsize437 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:563:2: ( cast | isnull | coalesce | objectsize )
            int alt110=4;
            switch ( input.LA(1) ) {
            case CAST:
                {
                alt110=1;
                }
                break;
            case ISNULLEXP:
                {
                alt110=2;
                }
                break;
            case COALESCE:
                {
                alt110=3;
                }
                break;
            case OBJECTSIZE:
                {
                alt110=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 110, 0, input);

                throw nvae;

            }

            switch (alt110) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:563:4: cast
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_cast_in_utilmethod9084);
                    cast434=cast();

                    state._fsp--;

                    adaptor.addChild(root_0, cast434.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:563:11: isnull
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_isnull_in_utilmethod9088);
                    isnull435=isnull();

                    state._fsp--;

                    adaptor.addChild(root_0, isnull435.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:563:20: coalesce
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_coalesce_in_utilmethod9092);
                    coalesce436=coalesce();

                    state._fsp--;

                    adaptor.addChild(root_0, coalesce436.getTree());

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:563:31: objectsize
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_objectsize_in_utilmethod9096);
                    objectsize437=objectsize();

                    state._fsp--;

                    adaptor.addChild(root_0, objectsize437.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "utilmethod"


    public static class cast_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "cast"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:565:1: cast : CAST ^ LEFT_PAREN ! logicalExpression AS ! ( INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | VARCHARTYPE | DATETYPE | DATETIMETYPE ) RIGHT_PAREN !;
    public final MemgrammarParser.cast_return cast() throws RecognitionException {
        MemgrammarParser.cast_return retval = new MemgrammarParser.cast_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token CAST438=null;
        Token LEFT_PAREN439=null;
        Token AS441=null;
        Token set442=null;
        Token RIGHT_PAREN443=null;
        MemgrammarParser.logicalExpression_return logicalExpression440 =null;


        CommonTree CAST438_tree=null;
        CommonTree LEFT_PAREN439_tree=null;
        CommonTree AS441_tree=null;
        CommonTree set442_tree=null;
        CommonTree RIGHT_PAREN443_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:565:6: ( CAST ^ LEFT_PAREN ! logicalExpression AS ! ( INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | VARCHARTYPE | DATETYPE | DATETIMETYPE ) RIGHT_PAREN !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:565:8: CAST ^ LEFT_PAREN ! logicalExpression AS ! ( INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | VARCHARTYPE | DATETYPE | DATETIMETYPE ) RIGHT_PAREN !
            {
            root_0 = (CommonTree)adaptor.nil();


            CAST438=(Token)match(input,CAST,FOLLOW_CAST_in_cast9105); 
            CAST438_tree = 
            (CommonTree)adaptor.create(CAST438)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(CAST438_tree, root_0);


            LEFT_PAREN439=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_cast9108); 

            pushFollow(FOLLOW_logicalExpression_in_cast9111);
            logicalExpression440=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression440.getTree());

            AS441=(Token)match(input,AS,FOLLOW_AS_in_cast9113); 

            set442=(Token)input.LT(1);

            if ( input.LA(1)==BIGINTTYPE||(input.LA(1) >= DATETIMETYPE && input.LA(1) <= DATETYPE)||input.LA(1)==DECIMALTYPE||input.LA(1)==FLOATTYPE||input.LA(1)==INTEGERTYPE||input.LA(1)==VARCHARTYPE ) {
                input.consume();
                adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set442)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            RIGHT_PAREN443=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_cast9144); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "cast"


    public static class isnull_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "isnull"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:567:1: isnull : ISNULLEXP ^ LEFT_PAREN ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !;
    public final MemgrammarParser.isnull_return isnull() throws RecognitionException {
        MemgrammarParser.isnull_return retval = new MemgrammarParser.isnull_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token ISNULLEXP444=null;
        Token LEFT_PAREN445=null;
        Token char_literal447=null;
        Token RIGHT_PAREN449=null;
        MemgrammarParser.logicalExpression_return logicalExpression446 =null;

        MemgrammarParser.logicalExpression_return logicalExpression448 =null;


        CommonTree ISNULLEXP444_tree=null;
        CommonTree LEFT_PAREN445_tree=null;
        CommonTree char_literal447_tree=null;
        CommonTree RIGHT_PAREN449_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:567:8: ( ISNULLEXP ^ LEFT_PAREN ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:567:10: ISNULLEXP ^ LEFT_PAREN ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !
            {
            root_0 = (CommonTree)adaptor.nil();


            ISNULLEXP444=(Token)match(input,ISNULLEXP,FOLLOW_ISNULLEXP_in_isnull9153); 
            ISNULLEXP444_tree = 
            (CommonTree)adaptor.create(ISNULLEXP444)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(ISNULLEXP444_tree, root_0);


            LEFT_PAREN445=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_isnull9156); 

            pushFollow(FOLLOW_logicalExpression_in_isnull9159);
            logicalExpression446=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression446.getTree());

            char_literal447=(Token)match(input,163,FOLLOW_163_in_isnull9161); 

            pushFollow(FOLLOW_logicalExpression_in_isnull9164);
            logicalExpression448=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression448.getTree());

            RIGHT_PAREN449=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_isnull9166); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "isnull"


    public static class coalesce_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "coalesce"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:1: coalesce : COALESCE LEFT_PAREN logicalExpression ( ',' logicalExpression )+ RIGHT_PAREN -> ^( COALESCE logicalExpression ( logicalExpression )+ ) ;
    public final MemgrammarParser.coalesce_return coalesce() throws RecognitionException {
        MemgrammarParser.coalesce_return retval = new MemgrammarParser.coalesce_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token COALESCE450=null;
        Token LEFT_PAREN451=null;
        Token char_literal453=null;
        Token RIGHT_PAREN455=null;
        MemgrammarParser.logicalExpression_return logicalExpression452 =null;

        MemgrammarParser.logicalExpression_return logicalExpression454 =null;


        CommonTree COALESCE450_tree=null;
        CommonTree LEFT_PAREN451_tree=null;
        CommonTree char_literal453_tree=null;
        CommonTree RIGHT_PAREN455_tree=null;
        RewriteRuleTokenStream stream_LEFT_PAREN=new RewriteRuleTokenStream(adaptor,"token LEFT_PAREN");
        RewriteRuleTokenStream stream_163=new RewriteRuleTokenStream(adaptor,"token 163");
        RewriteRuleTokenStream stream_RIGHT_PAREN=new RewriteRuleTokenStream(adaptor,"token RIGHT_PAREN");
        RewriteRuleTokenStream stream_COALESCE=new RewriteRuleTokenStream(adaptor,"token COALESCE");
        RewriteRuleSubtreeStream stream_logicalExpression=new RewriteRuleSubtreeStream(adaptor,"rule logicalExpression");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:9: ( COALESCE LEFT_PAREN logicalExpression ( ',' logicalExpression )+ RIGHT_PAREN -> ^( COALESCE logicalExpression ( logicalExpression )+ ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:11: COALESCE LEFT_PAREN logicalExpression ( ',' logicalExpression )+ RIGHT_PAREN
            {
            COALESCE450=(Token)match(input,COALESCE,FOLLOW_COALESCE_in_coalesce9174);  
            stream_COALESCE.add(COALESCE450);


            LEFT_PAREN451=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_coalesce9176);  
            stream_LEFT_PAREN.add(LEFT_PAREN451);


            pushFollow(FOLLOW_logicalExpression_in_coalesce9178);
            logicalExpression452=logicalExpression();

            state._fsp--;

            stream_logicalExpression.add(logicalExpression452.getTree());

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:49: ( ',' logicalExpression )+
            int cnt111=0;
            loop111:
            do {
                int alt111=2;
                int LA111_0 = input.LA(1);

                if ( (LA111_0==163) ) {
                    alt111=1;
                }


                switch (alt111) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:50: ',' logicalExpression
            	    {
            	    char_literal453=(Token)match(input,163,FOLLOW_163_in_coalesce9181);  
            	    stream_163.add(char_literal453);


            	    pushFollow(FOLLOW_logicalExpression_in_coalesce9183);
            	    logicalExpression454=logicalExpression();

            	    state._fsp--;

            	    stream_logicalExpression.add(logicalExpression454.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt111 >= 1 ) break loop111;
                        EarlyExitException eee =
                            new EarlyExitException(111, input);
                        throw eee;
                }
                cnt111++;
            } while (true);


            RIGHT_PAREN455=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_coalesce9187);  
            stream_RIGHT_PAREN.add(RIGHT_PAREN455);


            // AST REWRITE
            // elements: logicalExpression, logicalExpression, COALESCE
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 569:86: -> ^( COALESCE logicalExpression ( logicalExpression )+ )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:569:89: ^( COALESCE logicalExpression ( logicalExpression )+ )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_COALESCE.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                if ( !(stream_logicalExpression.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_logicalExpression.hasNext() ) {
                    adaptor.addChild(root_1, stream_logicalExpression.nextTree());

                }
                stream_logicalExpression.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "coalesce"


    public static class objectsize_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "objectsize"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:571:1: objectsize : OBJECTSIZE ^ LEFT_PAREN ! logicalExpression RIGHT_PAREN !;
    public final MemgrammarParser.objectsize_return objectsize() throws RecognitionException {
        MemgrammarParser.objectsize_return retval = new MemgrammarParser.objectsize_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token OBJECTSIZE456=null;
        Token LEFT_PAREN457=null;
        Token RIGHT_PAREN459=null;
        MemgrammarParser.logicalExpression_return logicalExpression458 =null;


        CommonTree OBJECTSIZE456_tree=null;
        CommonTree LEFT_PAREN457_tree=null;
        CommonTree RIGHT_PAREN459_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:571:11: ( OBJECTSIZE ^ LEFT_PAREN ! logicalExpression RIGHT_PAREN !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:571:13: OBJECTSIZE ^ LEFT_PAREN ! logicalExpression RIGHT_PAREN !
            {
            root_0 = (CommonTree)adaptor.nil();


            OBJECTSIZE456=(Token)match(input,OBJECTSIZE,FOLLOW_OBJECTSIZE_in_objectsize9205); 
            OBJECTSIZE456_tree = 
            (CommonTree)adaptor.create(OBJECTSIZE456)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(OBJECTSIZE456_tree, root_0);


            LEFT_PAREN457=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_objectsize9208); 

            pushFollow(FOLLOW_logicalExpression_in_objectsize9211);
            logicalExpression458=logicalExpression();

            state._fsp--;

            adaptor.addChild(root_0, logicalExpression458.getTree());

            RIGHT_PAREN459=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_objectsize9213); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "objectsize"


    public static class datemethod_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "datemethod"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:573:1: datemethod : ( dateadd | datediff | datepart | getdate );
    public final MemgrammarParser.datemethod_return datemethod() throws RecognitionException {
        MemgrammarParser.datemethod_return retval = new MemgrammarParser.datemethod_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MemgrammarParser.dateadd_return dateadd460 =null;

        MemgrammarParser.datediff_return datediff461 =null;

        MemgrammarParser.datepart_return datepart462 =null;

        MemgrammarParser.getdate_return getdate463 =null;



        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:574:2: ( dateadd | datediff | datepart | getdate )
            int alt112=4;
            switch ( input.LA(1) ) {
            case DATEADD:
            case TIMESTAMPADD:
                {
                alt112=1;
                }
                break;
            case DATEDIFF:
            case TIMESTAMPDIFF:
                {
                alt112=2;
                }
                break;
            case DATEPART:
            case EXTRACT:
                {
                alt112=3;
                }
                break;
            case GETDATE:
                {
                alt112=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 112, 0, input);

                throw nvae;

            }

            switch (alt112) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:574:4: dateadd
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_dateadd_in_datemethod9224);
                    dateadd460=dateadd();

                    state._fsp--;

                    adaptor.addChild(root_0, dateadd460.getTree());

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:574:14: datediff
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_datediff_in_datemethod9228);
                    datediff461=datediff();

                    state._fsp--;

                    adaptor.addChild(root_0, datediff461.getTree());

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:574:25: datepart
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_datepart_in_datemethod9232);
                    datepart462=datepart();

                    state._fsp--;

                    adaptor.addChild(root_0, datepart462.getTree());

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:574:36: getdate
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_getdate_in_datemethod9236);
                    getdate463=getdate();

                    state._fsp--;

                    adaptor.addChild(root_0, getdate463.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "datemethod"


    public static class datediff_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "datediff"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:576:1: datediff : ( DATEDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !| TIMESTAMPDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !);
    public final MemgrammarParser.datediff_return datediff() throws RecognitionException {
        MemgrammarParser.datediff_return retval = new MemgrammarParser.datediff_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DATEDIFF464=null;
        Token LEFT_PAREN465=null;
        Token set466=null;
        Token char_literal467=null;
        Token char_literal469=null;
        Token RIGHT_PAREN471=null;
        Token TIMESTAMPDIFF472=null;
        Token LEFT_PAREN473=null;
        Token set474=null;
        Token char_literal475=null;
        Token char_literal477=null;
        Token RIGHT_PAREN479=null;
        MemgrammarParser.logicalExpression_return logicalExpression468 =null;

        MemgrammarParser.logicalExpression_return logicalExpression470 =null;

        MemgrammarParser.logicalExpression_return logicalExpression476 =null;

        MemgrammarParser.logicalExpression_return logicalExpression478 =null;


        CommonTree DATEDIFF464_tree=null;
        CommonTree LEFT_PAREN465_tree=null;
        CommonTree set466_tree=null;
        CommonTree char_literal467_tree=null;
        CommonTree char_literal469_tree=null;
        CommonTree RIGHT_PAREN471_tree=null;
        CommonTree TIMESTAMPDIFF472_tree=null;
        CommonTree LEFT_PAREN473_tree=null;
        CommonTree set474_tree=null;
        CommonTree char_literal475_tree=null;
        CommonTree char_literal477_tree=null;
        CommonTree RIGHT_PAREN479_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:577:2: ( DATEDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !| TIMESTAMPDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !)
            int alt113=2;
            int LA113_0 = input.LA(1);

            if ( (LA113_0==DATEDIFF) ) {
                alt113=1;
            }
            else if ( (LA113_0==TIMESTAMPDIFF) ) {
                alt113=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 113, 0, input);

                throw nvae;

            }
            switch (alt113) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:577:4: DATEDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    DATEDIFF464=(Token)match(input,DATEDIFF,FOLLOW_DATEDIFF_in_datediff9246); 
                    DATEDIFF464_tree = 
                    (CommonTree)adaptor.create(DATEDIFF464)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(DATEDIFF464_tree, root_0);


                    LEFT_PAREN465=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_datediff9250); 

                    set466=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set466)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    char_literal467=(Token)match(input,163,FOLLOW_163_in_datediff9271); 

                    pushFollow(FOLLOW_logicalExpression_in_datediff9274);
                    logicalExpression468=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression468.getTree());

                    char_literal469=(Token)match(input,163,FOLLOW_163_in_datediff9276); 

                    pushFollow(FOLLOW_logicalExpression_in_datediff9279);
                    logicalExpression470=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression470.getTree());

                    RIGHT_PAREN471=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_datediff9281); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:578:3: TIMESTAMPDIFF ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    TIMESTAMPDIFF472=(Token)match(input,TIMESTAMPDIFF,FOLLOW_TIMESTAMPDIFF_in_datediff9288); 
                    TIMESTAMPDIFF472_tree = 
                    (CommonTree)adaptor.create(TIMESTAMPDIFF472)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(TIMESTAMPDIFF472_tree, root_0);


                    LEFT_PAREN473=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_datediff9292); 

                    set474=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set474)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    char_literal475=(Token)match(input,163,FOLLOW_163_in_datediff9313); 

                    pushFollow(FOLLOW_logicalExpression_in_datediff9316);
                    logicalExpression476=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression476.getTree());

                    char_literal477=(Token)match(input,163,FOLLOW_163_in_datediff9318); 

                    pushFollow(FOLLOW_logicalExpression_in_datediff9321);
                    logicalExpression478=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression478.getTree());

                    RIGHT_PAREN479=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_datediff9323); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "datediff"


    public static class dateadd_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dateadd"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:580:1: dateadd : ( DATEADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !| TIMESTAMPADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !);
    public final MemgrammarParser.dateadd_return dateadd() throws RecognitionException {
        MemgrammarParser.dateadd_return retval = new MemgrammarParser.dateadd_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DATEADD480=null;
        Token LEFT_PAREN481=null;
        Token set482=null;
        Token char_literal483=null;
        Token char_literal485=null;
        Token RIGHT_PAREN487=null;
        Token TIMESTAMPADD488=null;
        Token LEFT_PAREN489=null;
        Token set490=null;
        Token char_literal491=null;
        Token char_literal493=null;
        Token RIGHT_PAREN495=null;
        MemgrammarParser.logicalExpression_return logicalExpression484 =null;

        MemgrammarParser.logicalExpression_return logicalExpression486 =null;

        MemgrammarParser.logicalExpression_return logicalExpression492 =null;

        MemgrammarParser.logicalExpression_return logicalExpression494 =null;


        CommonTree DATEADD480_tree=null;
        CommonTree LEFT_PAREN481_tree=null;
        CommonTree set482_tree=null;
        CommonTree char_literal483_tree=null;
        CommonTree char_literal485_tree=null;
        CommonTree RIGHT_PAREN487_tree=null;
        CommonTree TIMESTAMPADD488_tree=null;
        CommonTree LEFT_PAREN489_tree=null;
        CommonTree set490_tree=null;
        CommonTree char_literal491_tree=null;
        CommonTree char_literal493_tree=null;
        CommonTree RIGHT_PAREN495_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:581:2: ( DATEADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !| TIMESTAMPADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !)
            int alt114=2;
            int LA114_0 = input.LA(1);

            if ( (LA114_0==DATEADD) ) {
                alt114=1;
            }
            else if ( (LA114_0==TIMESTAMPADD) ) {
                alt114=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 114, 0, input);

                throw nvae;

            }
            switch (alt114) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:581:4: DATEADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    DATEADD480=(Token)match(input,DATEADD,FOLLOW_DATEADD_in_dateadd9333); 
                    DATEADD480_tree = 
                    (CommonTree)adaptor.create(DATEADD480)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(DATEADD480_tree, root_0);


                    LEFT_PAREN481=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_dateadd9337); 

                    set482=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set482)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    char_literal483=(Token)match(input,163,FOLLOW_163_in_dateadd9358); 

                    pushFollow(FOLLOW_logicalExpression_in_dateadd9361);
                    logicalExpression484=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression484.getTree());

                    char_literal485=(Token)match(input,163,FOLLOW_163_in_dateadd9363); 

                    pushFollow(FOLLOW_logicalExpression_in_dateadd9366);
                    logicalExpression486=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression486.getTree());

                    RIGHT_PAREN487=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_dateadd9368); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:582:3: TIMESTAMPADD ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression ',' ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    TIMESTAMPADD488=(Token)match(input,TIMESTAMPADD,FOLLOW_TIMESTAMPADD_in_dateadd9375); 
                    TIMESTAMPADD488_tree = 
                    (CommonTree)adaptor.create(TIMESTAMPADD488)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(TIMESTAMPADD488_tree, root_0);


                    LEFT_PAREN489=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_dateadd9379); 

                    set490=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set490)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    char_literal491=(Token)match(input,163,FOLLOW_163_in_dateadd9400); 

                    pushFollow(FOLLOW_logicalExpression_in_dateadd9403);
                    logicalExpression492=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression492.getTree());

                    char_literal493=(Token)match(input,163,FOLLOW_163_in_dateadd9405); 

                    pushFollow(FOLLOW_logicalExpression_in_dateadd9408);
                    logicalExpression494=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression494.getTree());

                    RIGHT_PAREN495=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_dateadd9410); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dateadd"


    public static class datepart_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "datepart"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:584:1: datepart : ( DATEPART ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression RIGHT_PAREN !| EXTRACT ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) FROM ! logicalExpression RIGHT_PAREN !);
    public final MemgrammarParser.datepart_return datepart() throws RecognitionException {
        MemgrammarParser.datepart_return retval = new MemgrammarParser.datepart_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DATEPART496=null;
        Token LEFT_PAREN497=null;
        Token set498=null;
        Token char_literal499=null;
        Token RIGHT_PAREN501=null;
        Token EXTRACT502=null;
        Token LEFT_PAREN503=null;
        Token set504=null;
        Token FROM505=null;
        Token RIGHT_PAREN507=null;
        MemgrammarParser.logicalExpression_return logicalExpression500 =null;

        MemgrammarParser.logicalExpression_return logicalExpression506 =null;


        CommonTree DATEPART496_tree=null;
        CommonTree LEFT_PAREN497_tree=null;
        CommonTree set498_tree=null;
        CommonTree char_literal499_tree=null;
        CommonTree RIGHT_PAREN501_tree=null;
        CommonTree EXTRACT502_tree=null;
        CommonTree LEFT_PAREN503_tree=null;
        CommonTree set504_tree=null;
        CommonTree FROM505_tree=null;
        CommonTree RIGHT_PAREN507_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:585:2: ( DATEPART ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression RIGHT_PAREN !| EXTRACT ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) FROM ! logicalExpression RIGHT_PAREN !)
            int alt115=2;
            int LA115_0 = input.LA(1);

            if ( (LA115_0==DATEPART) ) {
                alt115=1;
            }
            else if ( (LA115_0==EXTRACT) ) {
                alt115=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 115, 0, input);

                throw nvae;

            }
            switch (alt115) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:585:4: DATEPART ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) ',' ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    DATEPART496=(Token)match(input,DATEPART,FOLLOW_DATEPART_in_datepart9420); 
                    DATEPART496_tree = 
                    (CommonTree)adaptor.create(DATEPART496)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(DATEPART496_tree, root_0);


                    LEFT_PAREN497=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_datepart9424); 

                    set498=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set498)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    char_literal499=(Token)match(input,163,FOLLOW_163_in_datepart9445); 

                    pushFollow(FOLLOW_logicalExpression_in_datepart9448);
                    logicalExpression500=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression500.getTree());

                    RIGHT_PAREN501=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_datepart9450); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:586:3: EXTRACT ^ LEFT_PAREN ! ( YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND ) FROM ! logicalExpression RIGHT_PAREN !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    EXTRACT502=(Token)match(input,EXTRACT,FOLLOW_EXTRACT_in_datepart9457); 
                    EXTRACT502_tree = 
                    (CommonTree)adaptor.create(EXTRACT502)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(EXTRACT502_tree, root_0);


                    LEFT_PAREN503=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_datepart9460); 

                    set504=(Token)input.LT(1);

                    if ( input.LA(1)==DAY||input.LA(1)==HOUR||input.LA(1)==MINUTE||input.LA(1)==MONTH||input.LA(1)==QUARTER||input.LA(1)==SECOND||input.LA(1)==WEEK||input.LA(1)==YEAR ) {
                        input.consume();
                        adaptor.addChild(root_0, 
                        (CommonTree)adaptor.create(set504)
                        );
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    FROM505=(Token)match(input,FROM,FOLLOW_FROM_in_datepart9481); 

                    pushFollow(FOLLOW_logicalExpression_in_datepart9484);
                    logicalExpression506=logicalExpression();

                    state._fsp--;

                    adaptor.addChild(root_0, logicalExpression506.getTree());

                    RIGHT_PAREN507=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_datepart9486); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "datepart"


    public static class getdate_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "getdate"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:588:1: getdate : GETDATE ^ LEFT_PAREN ! RIGHT_PAREN !;
    public final MemgrammarParser.getdate_return getdate() throws RecognitionException {
        MemgrammarParser.getdate_return retval = new MemgrammarParser.getdate_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token GETDATE508=null;
        Token LEFT_PAREN509=null;
        Token RIGHT_PAREN510=null;

        CommonTree GETDATE508_tree=null;
        CommonTree LEFT_PAREN509_tree=null;
        CommonTree RIGHT_PAREN510_tree=null;

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:588:9: ( GETDATE ^ LEFT_PAREN ! RIGHT_PAREN !)
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:588:11: GETDATE ^ LEFT_PAREN ! RIGHT_PAREN !
            {
            root_0 = (CommonTree)adaptor.nil();


            GETDATE508=(Token)match(input,GETDATE,FOLLOW_GETDATE_in_getdate9497); 
            GETDATE508_tree = 
            (CommonTree)adaptor.create(GETDATE508)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(GETDATE508_tree, root_0);


            LEFT_PAREN509=(Token)match(input,LEFT_PAREN,FOLLOW_LEFT_PAREN_in_getdate9500); 

            RIGHT_PAREN510=(Token)match(input,RIGHT_PAREN,FOLLOW_RIGHT_PAREN_in_getdate9503); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "getdate"


    public static class dumptable_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dumptable"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:1: dumptable : DUMPTABLE ( schema '.' )? ident OUTDIR STRING -> ^( DUMPTABLE ( schema '.' )? ident STRING ) ;
    public final MemgrammarParser.dumptable_return dumptable() throws RecognitionException {
        MemgrammarParser.dumptable_return retval = new MemgrammarParser.dumptable_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DUMPTABLE511=null;
        Token char_literal513=null;
        Token OUTDIR515=null;
        Token STRING516=null;
        MemgrammarParser.schema_return schema512 =null;

        MemgrammarParser.ident_return ident514 =null;


        CommonTree DUMPTABLE511_tree=null;
        CommonTree char_literal513_tree=null;
        CommonTree OUTDIR515_tree=null;
        CommonTree STRING516_tree=null;
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_DUMPTABLE=new RewriteRuleTokenStream(adaptor,"token DUMPTABLE");
        RewriteRuleTokenStream stream_OUTDIR=new RewriteRuleTokenStream(adaptor,"token OUTDIR");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:11: ( DUMPTABLE ( schema '.' )? ident OUTDIR STRING -> ^( DUMPTABLE ( schema '.' )? ident STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:13: DUMPTABLE ( schema '.' )? ident OUTDIR STRING
            {
            DUMPTABLE511=(Token)match(input,DUMPTABLE,FOLLOW_DUMPTABLE_in_dumptable9512);  
            stream_DUMPTABLE.add(DUMPTABLE511);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:23: ( schema '.' )?
            int alt116=2;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==IDENT) ) {
                int LA116_1 = input.LA(2);

                if ( (LA116_1==164) ) {
                    alt116=1;
                }
            }
            switch (alt116) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:24: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_dumptable9515);
                    schema512=schema();

                    state._fsp--;

                    stream_schema.add(schema512.getTree());

                    char_literal513=(Token)match(input,164,FOLLOW_164_in_dumptable9517);  
                    stream_164.add(char_literal513);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_dumptable9521);
            ident514=ident();

            state._fsp--;

            stream_ident.add(ident514.getTree());

            OUTDIR515=(Token)match(input,OUTDIR,FOLLOW_OUTDIR_in_dumptable9523);  
            stream_OUTDIR.add(OUTDIR515);


            STRING516=(Token)match(input,STRING,FOLLOW_STRING_in_dumptable9525);  
            stream_STRING.add(STRING516);


            // AST REWRITE
            // elements: STRING, DUMPTABLE, 164, schema, ident
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 590:56: -> ^( DUMPTABLE ( schema '.' )? ident STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:59: ^( DUMPTABLE ( schema '.' )? ident STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_DUMPTABLE.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:590:71: ( schema '.' )?
                if ( stream_164.hasNext()||stream_schema.hasNext() ) {
                    adaptor.addChild(root_1, stream_schema.nextTree());

                    adaptor.addChild(root_1, 
                    stream_164.nextNode()
                    );

                }
                stream_164.reset();
                stream_schema.reset();

                adaptor.addChild(root_1, stream_ident.nextTree());

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dumptable"


    public static class loadtable_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loadtable"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:1: loadtable : LOADTABLE ( schema '.' )? ident INDIR STRING -> ^( LOADTABLE ( schema '.' )? ident STRING ) ;
    public final MemgrammarParser.loadtable_return loadtable() throws RecognitionException {
        MemgrammarParser.loadtable_return retval = new MemgrammarParser.loadtable_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LOADTABLE517=null;
        Token char_literal519=null;
        Token INDIR521=null;
        Token STRING522=null;
        MemgrammarParser.schema_return schema518 =null;

        MemgrammarParser.ident_return ident520 =null;


        CommonTree LOADTABLE517_tree=null;
        CommonTree char_literal519_tree=null;
        CommonTree INDIR521_tree=null;
        CommonTree STRING522_tree=null;
        RewriteRuleTokenStream stream_INDIR=new RewriteRuleTokenStream(adaptor,"token INDIR");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_LOADTABLE=new RewriteRuleTokenStream(adaptor,"token LOADTABLE");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:11: ( LOADTABLE ( schema '.' )? ident INDIR STRING -> ^( LOADTABLE ( schema '.' )? ident STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:13: LOADTABLE ( schema '.' )? ident INDIR STRING
            {
            LOADTABLE517=(Token)match(input,LOADTABLE,FOLLOW_LOADTABLE_in_loadtable9548);  
            stream_LOADTABLE.add(LOADTABLE517);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:23: ( schema '.' )?
            int alt117=2;
            int LA117_0 = input.LA(1);

            if ( (LA117_0==IDENT) ) {
                int LA117_1 = input.LA(2);

                if ( (LA117_1==164) ) {
                    alt117=1;
                }
            }
            switch (alt117) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:24: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_loadtable9551);
                    schema518=schema();

                    state._fsp--;

                    stream_schema.add(schema518.getTree());

                    char_literal519=(Token)match(input,164,FOLLOW_164_in_loadtable9553);  
                    stream_164.add(char_literal519);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_loadtable9557);
            ident520=ident();

            state._fsp--;

            stream_ident.add(ident520.getTree());

            INDIR521=(Token)match(input,INDIR,FOLLOW_INDIR_in_loadtable9559);  
            stream_INDIR.add(INDIR521);


            STRING522=(Token)match(input,STRING,FOLLOW_STRING_in_loadtable9561);  
            stream_STRING.add(STRING522);


            // AST REWRITE
            // elements: STRING, schema, ident, LOADTABLE, 164
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 591:55: -> ^( LOADTABLE ( schema '.' )? ident STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:58: ^( LOADTABLE ( schema '.' )? ident STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_LOADTABLE.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:591:70: ( schema '.' )?
                if ( stream_schema.hasNext()||stream_164.hasNext() ) {
                    adaptor.addChild(root_1, stream_schema.nextTree());

                    adaptor.addChild(root_1, 
                    stream_164.nextNode()
                    );

                }
                stream_schema.reset();
                stream_164.reset();

                adaptor.addChild(root_1, stream_ident.nextTree());

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loadtable"


    public static class dumpddl_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dumpddl"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:1: dumpddl : DUMPDDL ( schema '.' )? ident OUTDIR STRING -> ^( DUMPDDL ( schema '.' )? ident STRING ) ;
    public final MemgrammarParser.dumpddl_return dumpddl() throws RecognitionException {
        MemgrammarParser.dumpddl_return retval = new MemgrammarParser.dumpddl_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DUMPDDL523=null;
        Token char_literal525=null;
        Token OUTDIR527=null;
        Token STRING528=null;
        MemgrammarParser.schema_return schema524 =null;

        MemgrammarParser.ident_return ident526 =null;


        CommonTree DUMPDDL523_tree=null;
        CommonTree char_literal525_tree=null;
        CommonTree OUTDIR527_tree=null;
        CommonTree STRING528_tree=null;
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_DUMPDDL=new RewriteRuleTokenStream(adaptor,"token DUMPDDL");
        RewriteRuleTokenStream stream_OUTDIR=new RewriteRuleTokenStream(adaptor,"token OUTDIR");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:9: ( DUMPDDL ( schema '.' )? ident OUTDIR STRING -> ^( DUMPDDL ( schema '.' )? ident STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:11: DUMPDDL ( schema '.' )? ident OUTDIR STRING
            {
            DUMPDDL523=(Token)match(input,DUMPDDL,FOLLOW_DUMPDDL_in_dumpddl9585);  
            stream_DUMPDDL.add(DUMPDDL523);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:19: ( schema '.' )?
            int alt118=2;
            int LA118_0 = input.LA(1);

            if ( (LA118_0==IDENT) ) {
                int LA118_1 = input.LA(2);

                if ( (LA118_1==164) ) {
                    alt118=1;
                }
            }
            switch (alt118) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:20: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_dumpddl9588);
                    schema524=schema();

                    state._fsp--;

                    stream_schema.add(schema524.getTree());

                    char_literal525=(Token)match(input,164,FOLLOW_164_in_dumpddl9590);  
                    stream_164.add(char_literal525);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_dumpddl9594);
            ident526=ident();

            state._fsp--;

            stream_ident.add(ident526.getTree());

            OUTDIR527=(Token)match(input,OUTDIR,FOLLOW_OUTDIR_in_dumpddl9596);  
            stream_OUTDIR.add(OUTDIR527);


            STRING528=(Token)match(input,STRING,FOLLOW_STRING_in_dumpddl9598);  
            stream_STRING.add(STRING528);


            // AST REWRITE
            // elements: 164, STRING, schema, DUMPDDL, ident
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 593:52: -> ^( DUMPDDL ( schema '.' )? ident STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:55: ^( DUMPDDL ( schema '.' )? ident STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_DUMPDDL.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:593:65: ( schema '.' )?
                if ( stream_164.hasNext()||stream_schema.hasNext() ) {
                    adaptor.addChild(root_1, stream_schema.nextTree());

                    adaptor.addChild(root_1, 
                    stream_164.nextNode()
                    );

                }
                stream_164.reset();
                stream_schema.reset();

                adaptor.addChild(root_1, stream_ident.nextTree());

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dumpddl"


    public static class loadddl_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loadddl"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:1: loadddl : LOADDDL ( schema '.' )? ident INDIR STRING -> ^( LOADDDL ( schema '.' )? ident STRING ) ;
    public final MemgrammarParser.loadddl_return loadddl() throws RecognitionException {
        MemgrammarParser.loadddl_return retval = new MemgrammarParser.loadddl_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LOADDDL529=null;
        Token char_literal531=null;
        Token INDIR533=null;
        Token STRING534=null;
        MemgrammarParser.schema_return schema530 =null;

        MemgrammarParser.ident_return ident532 =null;


        CommonTree LOADDDL529_tree=null;
        CommonTree char_literal531_tree=null;
        CommonTree INDIR533_tree=null;
        CommonTree STRING534_tree=null;
        RewriteRuleTokenStream stream_LOADDDL=new RewriteRuleTokenStream(adaptor,"token LOADDDL");
        RewriteRuleTokenStream stream_INDIR=new RewriteRuleTokenStream(adaptor,"token INDIR");
        RewriteRuleTokenStream stream_164=new RewriteRuleTokenStream(adaptor,"token 164");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");
        RewriteRuleSubtreeStream stream_schema=new RewriteRuleSubtreeStream(adaptor,"rule schema");
        RewriteRuleSubtreeStream stream_ident=new RewriteRuleSubtreeStream(adaptor,"rule ident");
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:9: ( LOADDDL ( schema '.' )? ident INDIR STRING -> ^( LOADDDL ( schema '.' )? ident STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:11: LOADDDL ( schema '.' )? ident INDIR STRING
            {
            LOADDDL529=(Token)match(input,LOADDDL,FOLLOW_LOADDDL_in_loadddl9621);  
            stream_LOADDDL.add(LOADDDL529);


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:19: ( schema '.' )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==IDENT) ) {
                int LA119_1 = input.LA(2);

                if ( (LA119_1==164) ) {
                    alt119=1;
                }
            }
            switch (alt119) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:20: schema '.'
                    {
                    pushFollow(FOLLOW_schema_in_loadddl9624);
                    schema530=schema();

                    state._fsp--;

                    stream_schema.add(schema530.getTree());

                    char_literal531=(Token)match(input,164,FOLLOW_164_in_loadddl9626);  
                    stream_164.add(char_literal531);


                    }
                    break;

            }


            pushFollow(FOLLOW_ident_in_loadddl9630);
            ident532=ident();

            state._fsp--;

            stream_ident.add(ident532.getTree());

            INDIR533=(Token)match(input,INDIR,FOLLOW_INDIR_in_loadddl9632);  
            stream_INDIR.add(INDIR533);


            STRING534=(Token)match(input,STRING,FOLLOW_STRING_in_loadddl9634);  
            stream_STRING.add(STRING534);


            // AST REWRITE
            // elements: LOADDDL, STRING, ident, 164, schema
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 594:51: -> ^( LOADDDL ( schema '.' )? ident STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:54: ^( LOADDDL ( schema '.' )? ident STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_LOADDDL.nextNode()
                , root_1);

                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:594:64: ( schema '.' )?
                if ( stream_164.hasNext()||stream_schema.hasNext() ) {
                    adaptor.addChild(root_1, stream_schema.nextTree());

                    adaptor.addChild(root_1, 
                    stream_164.nextNode()
                    );

                }
                stream_164.reset();
                stream_schema.reset();

                adaptor.addChild(root_1, stream_ident.nextTree());

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loadddl"


    public static class dumpdb_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dumpdb"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:596:1: dumpdb : DUMPDB OUTDIR STRING -> ^( DUMPDB STRING ) ;
    public final MemgrammarParser.dumpdb_return dumpdb() throws RecognitionException {
        MemgrammarParser.dumpdb_return retval = new MemgrammarParser.dumpdb_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token DUMPDB535=null;
        Token OUTDIR536=null;
        Token STRING537=null;

        CommonTree DUMPDB535_tree=null;
        CommonTree OUTDIR536_tree=null;
        CommonTree STRING537_tree=null;
        RewriteRuleTokenStream stream_DUMPDB=new RewriteRuleTokenStream(adaptor,"token DUMPDB");
        RewriteRuleTokenStream stream_OUTDIR=new RewriteRuleTokenStream(adaptor,"token OUTDIR");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:596:8: ( DUMPDB OUTDIR STRING -> ^( DUMPDB STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:596:10: DUMPDB OUTDIR STRING
            {
            DUMPDB535=(Token)match(input,DUMPDB,FOLLOW_DUMPDB_in_dumpdb9658);  
            stream_DUMPDB.add(DUMPDB535);


            OUTDIR536=(Token)match(input,OUTDIR,FOLLOW_OUTDIR_in_dumpdb9660);  
            stream_OUTDIR.add(OUTDIR536);


            STRING537=(Token)match(input,STRING,FOLLOW_STRING_in_dumpdb9662);  
            stream_STRING.add(STRING537);


            // AST REWRITE
            // elements: STRING, DUMPDB
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 596:30: -> ^( DUMPDB STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:596:33: ^( DUMPDB STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_DUMPDB.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dumpdb"


    public static class loaddb_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "loaddb"
    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:597:1: loaddb : LOADDB INDIR STRING -> ^( LOADDB STRING ) ;
    public final MemgrammarParser.loaddb_return loaddb() throws RecognitionException {
        MemgrammarParser.loaddb_return retval = new MemgrammarParser.loaddb_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token LOADDB538=null;
        Token INDIR539=null;
        Token STRING540=null;

        CommonTree LOADDB538_tree=null;
        CommonTree INDIR539_tree=null;
        CommonTree STRING540_tree=null;
        RewriteRuleTokenStream stream_INDIR=new RewriteRuleTokenStream(adaptor,"token INDIR");
        RewriteRuleTokenStream stream_LOADDB=new RewriteRuleTokenStream(adaptor,"token LOADDB");
        RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");

        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:597:8: ( LOADDB INDIR STRING -> ^( LOADDB STRING ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:597:10: LOADDB INDIR STRING
            {
            LOADDB538=(Token)match(input,LOADDB,FOLLOW_LOADDB_in_loaddb9676);  
            stream_LOADDB.add(LOADDB538);


            INDIR539=(Token)match(input,INDIR,FOLLOW_INDIR_in_loaddb9678);  
            stream_INDIR.add(INDIR539);


            STRING540=(Token)match(input,STRING,FOLLOW_STRING_in_loaddb9680);  
            stream_STRING.add(STRING540);


            // AST REWRITE
            // elements: LOADDB, STRING
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (CommonTree)adaptor.nil();
            // 597:29: -> ^( LOADDB STRING )
            {
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:597:32: ^( LOADDB STRING )
                {
                CommonTree root_1 = (CommonTree)adaptor.nil();
                root_1 = (CommonTree)adaptor.becomeRoot(
                stream_LOADDB.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_STRING.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }

        	catch (RecognitionException e) 
        	{
        		throw e;
        	}

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "loaddb"

    // Delegated rules


    protected DFA29 dfa29 = new DFA29(this);
    static final String DFA29_eotS =
        "\26\uffff";
    static final String DFA29_eofS =
        "\26\uffff";
    static final String DFA29_minS =
        "\1\106\1\111\1\100\1\117\2\100\3\uffff\1\117\1\177\2\100\1\174\2"+
        "\177\2\100\2\177\1\100\1\177";
    static final String DFA29_maxS =
        "\1\106\1\111\1\100\1\u00a4\2\100\3\uffff\1\u009c\1\u00a4\2\100\1"+
        "\u009c\2\u00a4\2\100\1\u00a3\1\u00a4\1\100\1\u00a3";
    static final String DFA29_acceptS =
        "\6\uffff\1\1\1\2\1\3\15\uffff";
    static final String DFA29_specialS =
        "\26\uffff}>";
    static final String[] DFA29_transitionS = {
            "\1\1",
            "\1\2",
            "\1\3",
            "\1\5\54\uffff\1\10\5\uffff\1\6\31\uffff\1\7\7\uffff\1\4",
            "\1\11",
            "\1\12",
            "",
            "",
            "",
            "\1\5\54\uffff\1\10\5\uffff\1\6\31\uffff\1\7",
            "\1\15\43\uffff\1\14\1\13",
            "\1\16",
            "\1\17",
            "\1\10\5\uffff\1\6\31\uffff\1\7",
            "\1\15\43\uffff\1\14\1\20",
            "\1\15\43\uffff\1\14\1\21",
            "\1\22",
            "\1\23",
            "\1\15\43\uffff\1\14",
            "\1\15\43\uffff\1\14\1\24",
            "\1\25",
            "\1\15\43\uffff\1\14"
    };

    static final short[] DFA29_eot = DFA.unpackEncodedString(DFA29_eotS);
    static final short[] DFA29_eof = DFA.unpackEncodedString(DFA29_eofS);
    static final char[] DFA29_min = DFA.unpackEncodedStringToUnsignedChars(DFA29_minS);
    static final char[] DFA29_max = DFA.unpackEncodedStringToUnsignedChars(DFA29_maxS);
    static final short[] DFA29_accept = DFA.unpackEncodedString(DFA29_acceptS);
    static final short[] DFA29_special = DFA.unpackEncodedString(DFA29_specialS);
    static final short[][] DFA29_transition;

    static {
        int numStates = DFA29_transitionS.length;
        DFA29_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA29_transition[i] = DFA.unpackEncodedString(DFA29_transitionS[i]);
        }
    }

    class DFA29 extends DFA {

        public DFA29(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 29;
            this.eot = DFA29_eot;
            this.eof = DFA29_eof;
            this.min = DFA29_min;
            this.max = DFA29_max;
            this.accept = DFA29_accept;
            this.special = DFA29_special;
            this.transition = DFA29_transition;
        }
        public String getDescription() {
            return "341:1: insertquery : ( INSERT INTO ( schema '.' )? ident ( collist )? selectquery -> ^( INSERT ^( ident ( schema )? ) ( collist )? selectquery ) | INSERT INTO ( schema '.' )? ident ( collist )? VALUES LEFT_PAREN insertliteral ( ',' insertliteral )* RIGHT_PAREN -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( VALUES ( insertliteral )* ) ) | INSERT INTO ( schema '.' )? ident ( collist )? REMOTEQUERY STRING STRING STRING STRING STRING -> ^( INSERT ^( ident ( schema )? ) ( collist )? ^( REMOTEQUERY STRING STRING STRING STRING STRING ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_statement_in_statementlist6344 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000002000000000L});
    public static final BitSet FOLLOW_165_in_statementlist6346 = new BitSet(new long[]{0x0020878080200022L,0x2800000000F02040L,0x0000000005000014L});
    public static final BitSet FOLLOW_selectquery_in_statement6358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dropquery_in_statement6362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_showquery_in_statement6366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_createquery_in_statement6370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_insertquery_in_statement6374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loaddata_in_statement6378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_deletequery_in_statement6382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_updatequery_in_statement6386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_alterquery_in_statement6390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUIT_in_statement6397 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EXIT_in_statement6402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLUSH_in_statement6407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_kill_in_statement6412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dumptable_in_statement6416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loadtable_in_statement6420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dumpddl_in_statement6424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loadddl_in_statement6428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dumpdb_in_statement6432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_loaddb_in_statement6436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_renametable_in_statement6440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOADDATAINFILE_in_loaddata6447 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_loaddata6449 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_INTO_in_loaddata6451 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_loaddata6453 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_loaddata6456 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_loaddata6458 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_loaddata6462 = new BitSet(new long[]{0x0000100100000002L,0x0000000000008000L,0x0000000000008020L});
    public static final BitSet FOLLOW_loadoption_in_loaddata6464 = new BitSet(new long[]{0x0000100100000002L,0x0000000000008000L,0x0000000000008020L});
    public static final BitSet FOLLOW_collist_in_loaddata6467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_delimitedby_in_loadoption6497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_enclosedby_in_loadoption6501 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_terminatedby_in_loadoption6505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SKIPFIRST_in_loadoption6509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DELIMITEDBY_in_delimitedby6518 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_delimitedby6521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ENCLOSEDBY_in_enclosedby6531 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_enclosedby6534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TERMINATEDBY_in_terminatedby6544 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_terminatedby6547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_KILL_in_kill6556 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_INTEGER_in_kill6559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CREATE_in_createquery6569 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_createquery6571 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_createquery6574 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_createquery6576 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_createquery6580 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_AS_in_createquery6582 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
    public static final BitSet FOLLOW_REMOTEQUERY_in_createquery6584 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_createquery6586 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_createquery6588 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_createquery6590 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_createquery6592 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_createquery6594 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_createquery6596 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_createquery6598 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_createquery6600 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_createquery6602 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_createquery6604 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_createquery6606 = new BitSet(new long[]{0x0000000000000002L,0x0040000000000000L});
    public static final BitSet FOLLOW_primarykeyspec_in_createquery6608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CREATE_in_createquery6647 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_createquery6649 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_createquery6652 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_createquery6654 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_createquery6658 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_AS_in_createquery6660 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_selectquery_in_createquery6662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CREATE_in_createquery6683 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_createquery6685 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_createquery6688 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_createquery6690 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_createquery6694 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_createquery6696 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_columnDefinition_in_createquery6698 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_createquery6701 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_columnDefinition_in_createquery6703 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_createquery6707 = new BitSet(new long[]{0x0000000C000C0002L,0x0000000010000000L,0x0000000002800000L});
    public static final BitSet FOLLOW_TRANSIENT_in_createquery6709 = new BitSet(new long[]{0x0000000C000C0002L,0x0000000010000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_DISK_in_createquery6712 = new BitSet(new long[]{0x00000008000C0002L,0x0000000010000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_MEMORY_in_createquery6715 = new BitSet(new long[]{0x00000008000C0002L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_COMPRESSED_in_createquery6718 = new BitSet(new long[]{0x0000000800080002L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_UNCOMPRESSED_in_createquery6721 = new BitSet(new long[]{0x0000000800080002L});
    public static final BitSet FOLLOW_DISKKEYSTORE_in_createquery6724 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_COMPRESSEDKEYSTORE_in_createquery6727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALTER_in_alterquery6777 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_alterquery6779 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_alterquery6782 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_alterquery6784 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_alterquery6788 = new BitSet(new long[]{0x0000008000000030L});
    public static final BitSet FOLLOW_alterdef_in_alterquery6790 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_alterquery6793 = new BitSet(new long[]{0x0000008000000030L});
    public static final BitSet FOLLOW_alterdef_in_alterquery6795 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_INSERT_in_insertquery6825 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_INTO_in_insertquery6827 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_insertquery6830 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_insertquery6832 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_insertquery6836 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L,0x0000000000000004L});
    public static final BitSet FOLLOW_collist_in_insertquery6838 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_selectquery_in_insertquery6841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INSERT_in_insertquery6865 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_INTO_in_insertquery6867 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_insertquery6870 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_insertquery6872 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_insertquery6876 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L,0x0000000010000000L});
    public static final BitSet FOLLOW_collist_in_insertquery6878 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_VALUES_in_insertquery6881 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_insertquery6883 = new BitSet(new long[]{0x0008000002000800L,0x0002040000000080L,0x0000000000000040L});
    public static final BitSet FOLLOW_insertliteral_in_insertquery6885 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_insertquery6888 = new BitSet(new long[]{0x0008000002000800L,0x0002040000000080L,0x0000000000000040L});
    public static final BitSet FOLLOW_insertliteral_in_insertquery6890 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_insertquery6894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INSERT_in_insertquery6923 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_INTO_in_insertquery6925 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_insertquery6928 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_insertquery6930 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_insertquery6934 = new BitSet(new long[]{0x0000000000000000L,0x1000000000008000L});
    public static final BitSet FOLLOW_collist_in_insertquery6936 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
    public static final BitSet FOLLOW_REMOTEQUERY_in_insertquery6939 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_insertquery6941 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_insertquery6943 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_insertquery6945 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_insertquery6947 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_insertquery6949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_literal_in_insertliteral6990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PARAMETER_in_insertliteral6994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_collist7004 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_colspec_in_collist7006 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_collist7009 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_colspec_in_collist7011 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_collist7015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_columnDefinition7099 = new BitSet(new long[]{0x001000004C000400L,0x0000000000000100L,0x0000000020000000L});
    public static final BitSet FOLLOW_columnType_in_columnDefinition7101 = new BitSet(new long[]{0x0040000400040002L,0x0040002010008000L,0x0000000002800000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_columnDefinition7104 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_INTEGER_in_columnDefinition7106 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_columnDefinition7108 = new BitSet(new long[]{0x0040000400040002L,0x0040002010000000L,0x0000000002800000L});
    public static final BitSet FOLLOW_PRIMARYKEY_in_columnDefinition7112 = new BitSet(new long[]{0x0040000400040002L,0x0000002010000000L,0x0000000002800000L});
    public static final BitSet FOLLOW_FOREIGNKEY_in_columnDefinition7115 = new BitSet(new long[]{0x0000000400040002L,0x0000002010000000L,0x0000000002800000L});
    public static final BitSet FOLLOW_NOINDEX_in_columnDefinition7118 = new BitSet(new long[]{0x0000000400040002L,0x0000000010000000L,0x0000000002800000L});
    public static final BitSet FOLLOW_TRANSIENT_in_columnDefinition7121 = new BitSet(new long[]{0x0000000400040002L,0x0000000010000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_DISK_in_columnDefinition7124 = new BitSet(new long[]{0x0000000000040002L,0x0000000010000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_MEMORY_in_columnDefinition7127 = new BitSet(new long[]{0x0000000000040002L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_COMPRESSED_in_columnDefinition7130 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_UNCOMPRESSED_in_columnDefinition7133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ADD_in_alterdef7178 = new BitSet(new long[]{0x0000000000010000L,0x0000000000000001L});
    public static final BitSet FOLLOW_COLUMN_in_alterdef7180 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_columnDefinition_in_alterdef7183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DROP_in_alterdef7197 = new BitSet(new long[]{0x0000000000010000L,0x0000000000000001L});
    public static final BitSet FOLLOW_COLUMN_in_alterdef7199 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_alterdef7202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALTER_in_alterdef7216 = new BitSet(new long[]{0x0000000000010000L,0x0000000000000001L});
    public static final BitSet FOLLOW_COLUMN_in_alterdef7218 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_columnDefinition_in_alterdef7221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRIMARYKEY_in_primarykeyspec7238 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_primarykeyspec7240 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_primarykeyspec7243 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_primarykeyspec7245 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_SHOW_in_showquery7267 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_TABLES_in_showquery7270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHOW_in_showquery7274 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L});
    public static final BitSet FOLLOW_PROCESSLIST_in_showquery7277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHOW_in_showquery7281 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_SURROGATEKEYS_in_showquery7284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHOW_in_showquery7288 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_SYSTEMSTATUS_in_showquery7291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SHOW_in_showquery7297 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_showquery7299 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_showquery7302 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_showquery7304 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_showquery7308 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_DEBUG_in_showquery7310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DROP_in_dropquery7335 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_dropquery7337 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_dropquery7340 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_dropquery7342 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_dropquery7346 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000002L});
    public static final BitSet FOLLOW_IFEXISTS_in_dropquery7348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TRUNCATE_in_dropquery7371 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_dropquery7373 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_dropquery7376 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_dropquery7378 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_dropquery7382 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000002L});
    public static final BitSet FOLLOW_IFEXISTS_in_dropquery7384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SELECT_in_selectquery7410 = new BitSet(new long[]{0x0209001003D07A00L,0x00120C4864039081L,0x0000000000760340L});
    public static final BitSet FOLLOW_top_in_selectquery7412 = new BitSet(new long[]{0x0209001003D07A00L,0x00120C4864039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_DISTINCT_in_selectquery7415 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4864039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_projectionlist_in_selectquery7418 = new BitSet(new long[]{0x0080000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_intooutfile_in_selectquery7420 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_from_in_selectquery7423 = new BitSet(new long[]{0x2400000000000002L,0x0000400000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_where_in_selectquery7425 = new BitSet(new long[]{0x2400000000000002L,0x0000400000000000L});
    public static final BitSet FOLLOW_groupby_in_selectquery7428 = new BitSet(new long[]{0x2000000000000002L,0x0000400000000000L});
    public static final BitSet FOLLOW_having_in_selectquery7431 = new BitSet(new long[]{0x0000000000000002L,0x0000400000000000L});
    public static final BitSet FOLLOW_orderby_in_selectquery7434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DELETE_in_deletequery7479 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_from_in_deletequery7481 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_where_in_deletequery7483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_UPDATE_in_updatequery7505 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008011L,0x0000000000000008L});
    public static final BitSet FOLLOW_tablespec_in_updatequery7507 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_SET_in_updatequery7510 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_colset_in_updatequery7513 = new BitSet(new long[]{0x0080000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_updatequery7516 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_colset_in_updatequery7518 = new BitSet(new long[]{0x0080000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_from_in_updatequery7523 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_where_in_updatequery7525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RENAME_in_renametable7556 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_TABLE_in_renametable7558 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_renametable7561 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_renametable7563 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_renametable7567 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_TO_in_renametable7569 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_renametable7572 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_renametable7574 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_renametable7578 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colspec_in_colset7607 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_EQUALS_in_colset7609 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_colset7611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTO_in_intooutfile7632 = new BitSet(new long[]{0x0000000000000000L,0x0001000000000000L});
    public static final BitSet FOLLOW_OUTFILE_in_intooutfile7634 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_intooutfile7636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TOP_in_top7652 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_INTEGER_in_top7655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FROM_in_from7664 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008011L});
    public static final BitSet FOLLOW_tablespec_in_from7666 = new BitSet(new long[]{0x0000002000000002L,0x4000000000004020L});
    public static final BitSet FOLLOW_distinctforkey_in_from7668 = new BitSet(new long[]{0x0000000000000002L,0x4000000000004020L});
    public static final BitSet FOLLOW_fromjoin_in_from7671 = new BitSet(new long[]{0x0000000000000002L,0x4000000000004020L});
    public static final BitSet FOLLOW_joindir_in_fromjoin7698 = new BitSet(new long[]{0x0000000000000000L,0x0000000008008011L});
    public static final BitSet FOLLOW_MAXONE_in_fromjoin7700 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008011L});
    public static final BitSet FOLLOW_tablespec_in_fromjoin7703 = new BitSet(new long[]{0x0000000000000002L,0x0000100000000000L});
    public static final BitSet FOLLOW_joinspec_in_fromjoin7705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DISTINCTFORKEY_in_distinctforkey7731 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_distinctforkey7733 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_distinctforkey7735 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_distinctforkey7738 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_distinctforkey7740 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_distinctforkey7744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INFOTABLES_in_tablespec7778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_schema_in_tablespec7792 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_tablespec7794 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_tablespec7798 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_tablealias_in_tablespec7800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_tablespec7823 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_selectquery_in_tablespec7825 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_tablespec7827 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_tablealias_in_tablespec7829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_tablespec7845 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_tablespec7848 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_tablespec7850 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_tablespec7854 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_tablespec7856 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_tablealias_in_tablespec7858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_schema7885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_tablealias7900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ON_in_joinspec7908 = new BitSet(new long[]{0x0008000002000800L,0x0000040000008081L,0x0000000000000040L});
    public static final BitSet FOLLOW_joinOr_in_joinspec7910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_joinAnd_in_joinOr7930 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_OR_in_joinOr7933 = new BitSet(new long[]{0x0008000002000800L,0x0000040000008081L,0x0000000000000040L});
    public static final BitSet FOLLOW_joinAnd_in_joinOr7936 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_joinOn_in_joinAnd7945 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_AND_in_joinAnd7948 = new BitSet(new long[]{0x0008000002000800L,0x0000040000008081L,0x0000000000000040L});
    public static final BitSet FOLLOW_joinOn_in_joinAnd7951 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_primaryJoin_in_joinOn7961 = new BitSet(new long[]{0x0000000000000002L,0x0000100000000000L});
    public static final BitSet FOLLOW_snowflake_in_joinOn7964 = new BitSet(new long[]{0x0008000002000800L,0x0000040000008081L,0x0000000000000040L});
    public static final BitSet FOLLOW_primaryJoin_in_joinOn7967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ON_in_snowflake7978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_primaryJoin7991 = new BitSet(new long[]{0x0008000002000800L,0x0000040000008081L,0x0000000000000040L});
    public static final BitSet FOLLOW_joinOr_in_primaryJoin7994 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_primaryJoin7996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_joincond_in_primaryJoin8002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_equijoin_in_joincond8014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_joinpredicate_in_equijoin8021 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_EQUALS_in_equijoin8023 = new BitSet(new long[]{0x0008000002000800L,0x0000040000000081L,0x0000000000000040L});
    public static final BitSet FOLLOW_joinpredicate_in_equijoin8025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colspec_in_equijoin8042 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_ISNULL_in_equijoin8044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colspec_in_joinpredicate8061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_literal_in_joinpredicate8065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ident_in_colspec8074 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_colspec8076 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_colspec8078 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_colspec8080 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_colspec8082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ident_in_colspec8098 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_colspec8100 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_IDENT_in_colspec8102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_colspec8116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHERE_in_where8124 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_where8126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GROUPBY_in_groupby8142 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_groupbyprojectionlist_in_groupby8144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ORDERBY_in_orderby8160 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_orderbylist_in_orderby8162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HAVING_in_having8178 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_having8180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_projection_in_projectionlist8210 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_projectionlist8213 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4864039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_projection_in_projectionlist8215 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_groupbyprojection_in_groupbyprojectionlist8235 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_groupbyprojectionlist8238 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_groupbyprojection_in_groupbyprojectionlist8240 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_orderbycondition_in_orderbylist8260 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_orderbylist8263 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_orderbycondition_in_orderbylist8266 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_ident_in_projection8277 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_projection8279 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_projection8281 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_projection8283 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_MULT_in_projection8285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ident_in_projection8301 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_projection8303 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_MULT_in_projection8305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MULT_in_projection8319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalExpression_in_projection8331 = new BitSet(new long[]{0x0000000000000082L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_AS_in_projection8334 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_projection8337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalExpression_in_groupbyprojection8361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalExpression_in_orderbycondition8370 = new BitSet(new long[]{0x0000000200000102L});
    public static final BitSet FOLLOW_direction_in_orderbycondition8373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENT_in_ident8393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CASE_in_casewhen8424 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_whenexp_in_casewhen8426 = new BitSet(new long[]{0x0000280000000000L,0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_elseexp_in_casewhen8429 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_END_in_casewhen8432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHEN_in_whenexp8452 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_whenexp8454 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_THEN_in_whenexp8456 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_whenexp8458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ELSE_in_elseexp8476 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_elseexp8479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logicalExpression_in_expression8489 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_expression8491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanAndExpression_in_logicalExpression8505 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_OR_in_logicalExpression8508 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_booleanAndExpression_in_logicalExpression8511 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_equalityExpression_in_booleanAndExpression8523 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_AND_in_booleanAndExpression8526 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_equalityExpression_in_booleanAndExpression8529 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_nullExpression_in_equalityExpression8542 = new BitSet(new long[]{0x0000400000000002L,0x0000008000000000L});
    public static final BitSet FOLLOW_set_in_equalityExpression8545 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_nullExpression_in_equalityExpression8552 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_relationalExpression_in_nullExpression8563 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000C00L});
    public static final BitSet FOLLOW_set_in_nullExpression8566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_inExpression_in_relationalExpression8583 = new BitSet(new long[]{0x1800000000000002L,0x0000020003040000L});
    public static final BitSet FOLLOW_set_in_relationalExpression8587 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_inExpression_in_relationalExpression8602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_additiveExpression_in_inExpression8617 = new BitSet(new long[]{0x0000000000000002L,0x0000010000000004L});
    public static final BitSet FOLLOW_in_in_inExpression8620 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_inExpression8623 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_selectquery_in_inExpression8626 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_inExpression8628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colspec_in_inExpression8637 = new BitSet(new long[]{0x0000000000000000L,0x0000010000000004L});
    public static final BitSet FOLLOW_in_in_inExpression8640 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_inExpression8642 = new BitSet(new long[]{0x0008000002000800L,0x0000040000000080L,0x0000000000000040L});
    public static final BitSet FOLLOW_literal_in_inExpression8644 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_inExpression8647 = new BitSet(new long[]{0x0008000002000800L,0x0000040000000080L,0x0000000000000040L});
    public static final BitSet FOLLOW_literal_in_inExpression8649 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_inExpression8653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression8676 = new BitSet(new long[]{0x0000000000000002L,0x0008000040000000L});
    public static final BitSet FOLLOW_set_in_additiveExpression8680 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression8687 = new BitSet(new long[]{0x0000000000000002L,0x0008000040000000L});
    public static final BitSet FOLLOW_powerExpression_in_multiplicativeExpression8701 = new BitSet(new long[]{0x0000004000000002L,0x0000000A00000000L});
    public static final BitSet FOLLOW_set_in_multiplicativeExpression8705 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_powerExpression_in_multiplicativeExpression8714 = new BitSet(new long[]{0x0000004000000002L,0x0000000A00000000L});
    public static final BitSet FOLLOW_unaryExpression_in_powerExpression8729 = new BitSet(new long[]{0x0000000000000002L,0x0020000000000000L});
    public static final BitSet FOLLOW_POW_in_powerExpression8733 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_unaryExpression_in_powerExpression8736 = new BitSet(new long[]{0x0000000000000002L,0x0020000000000000L});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression8750 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_in_unaryExpression8759 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C0024039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression8762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_unaryExpression8771 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C0024039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression8773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_value_in_primaryExpression8797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_primaryExpression8803 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_primaryExpression8805 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_primaryExpression8807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_in_value8839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOAT_in_value8844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATETIME_in_value8850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BOOLEAN_in_value8855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_value8860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_casewhen_in_value8865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_aggrule_in_value8870 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_value8872 = new BitSet(new long[]{0x0209001003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_DISTINCT_in_value8874 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_value8877 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_value8879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COUNT_in_value8895 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_value8897 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_MULT_in_value8899 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_value8901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_colspec_in_value8914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_datemethod_in_value8919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_stringmethod_in_value8924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_utilmethod_in_value8929 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PARAMETER_in_value8934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NULL_in_value8939 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_substring_in_stringmethod8950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_length_in_stringmethod8954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_position_in_stringmethod8958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_toupper_in_stringmethod8962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_tolower_in_stringmethod8966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SUBSTR_in_substring8976 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_substring8979 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_substring8982 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_substring8984 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_substring8987 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_substring8990 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_substring8993 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_substring8997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_length9005 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_length9014 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_length9017 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_length9019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_POSITION_in_position9028 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_position9031 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_position9034 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_position9036 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_position9039 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_position9041 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TOUPPER_in_toupper9048 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_toupper9051 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_toupper9054 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_toupper9056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TOLOWER_in_tolower9064 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_tolower9067 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_tolower9070 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_tolower9072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_cast_in_utilmethod9084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_isnull_in_utilmethod9088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_coalesce_in_utilmethod9092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectsize_in_utilmethod9096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CAST_in_cast9105 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_cast9108 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_cast9111 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_AS_in_cast9113 = new BitSet(new long[]{0x001000004C000400L,0x0000000000000100L,0x0000000020000000L});
    public static final BitSet FOLLOW_set_in_cast9116 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_cast9144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISNULLEXP_in_isnull9153 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_isnull9156 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_isnull9159 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_isnull9161 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_isnull9164 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_isnull9166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COALESCE_in_coalesce9174 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_coalesce9176 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_coalesce9178 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_coalesce9181 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_coalesce9183 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_coalesce9187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OBJECTSIZE_in_objectsize9205 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_objectsize9208 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_objectsize9211 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_objectsize9213 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_dateadd_in_datemethod9224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_datediff_in_datemethod9228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_datepart_in_datemethod9232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_getdate_in_datemethod9236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATEDIFF_in_datediff9246 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_datediff9250 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_datediff9253 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_datediff9271 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datediff9274 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_datediff9276 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datediff9279 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_datediff9281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TIMESTAMPDIFF_in_datediff9288 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_datediff9292 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_datediff9295 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_datediff9313 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datediff9316 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_datediff9318 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datediff9321 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_datediff9323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATEADD_in_dateadd9333 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_dateadd9337 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_dateadd9340 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_dateadd9358 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_dateadd9361 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_dateadd9363 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_dateadd9366 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_dateadd9368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TIMESTAMPADD_in_dateadd9375 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_dateadd9379 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_dateadd9382 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_dateadd9400 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_dateadd9403 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_dateadd9405 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_dateadd9408 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_dateadd9410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATEPART_in_datepart9420 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_datepart9424 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_datepart9427 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_163_in_datepart9445 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datepart9448 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_datepart9450 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EXTRACT_in_datepart9457 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_datepart9460 = new BitSet(new long[]{0x4000000010000000L,0x0400000480000000L,0x0000000440000002L});
    public static final BitSet FOLLOW_set_in_datepart9463 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_FROM_in_datepart9481 = new BitSet(new long[]{0x0209000003D07A00L,0x00120C4064039081L,0x0000000000560340L});
    public static final BitSet FOLLOW_logicalExpression_in_datepart9484 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_datepart9486 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GETDATE_in_getdate9497 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_LEFT_PAREN_in_getdate9500 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_RIGHT_PAREN_in_getdate9503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPTABLE_in_dumptable9512 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_dumptable9515 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_dumptable9517 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_dumptable9521 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_OUTDIR_in_dumptable9523 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_dumptable9525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOADTABLE_in_loadtable9548 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_loadtable9551 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_loadtable9553 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_loadtable9557 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_INDIR_in_loadtable9559 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_loadtable9561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPDDL_in_dumpddl9585 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_dumpddl9588 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_dumpddl9590 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_dumpddl9594 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_OUTDIR_in_dumpddl9596 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_dumpddl9598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOADDDL_in_loadddl9621 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_schema_in_loadddl9624 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_164_in_loadddl9626 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_ident_in_loadddl9630 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_INDIR_in_loadddl9632 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_loadddl9634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DUMPDB_in_dumpdb9658 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_OUTDIR_in_dumpdb9660 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_dumpdb9662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LOADDB_in_loaddb9676 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_INDIR_in_loaddb9678 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_STRING_in_loaddb9680 = new BitSet(new long[]{0x0000000000000002L});

}