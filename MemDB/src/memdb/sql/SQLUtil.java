package memdb.sql;

import java.sql.Types;

public class SQLUtil
{
	public static boolean compatibleTypes(int a, int b)
	{
		if (a == b)
			return true;
		if ((a == Types.DECIMAL && b == Types.DOUBLE) || (a == Types.DECIMAL && b == Types.FLOAT) || (a == Types.DECIMAL && b == Types.INTEGER)
				|| (b == Types.DECIMAL && a == Types.DOUBLE) || (b == Types.DECIMAL && a == Types.FLOAT) || (b == Types.DECIMAL && a == Types.INTEGER)
				|| (a == Types.INTEGER && b == Types.DOUBLE) || (b == Types.INTEGER && a == Types.DOUBLE) || (a == Types.INTEGER && b == Types.FLOAT)
				|| (b == Types.INTEGER && a == Types.FLOAT) || (a == Types.DATE && b == Types.TIMESTAMP) || (b == Types.DATE && a == Types.TIMESTAMP)
				|| (a == Types.DATE && b == Types.TIME) || (b == Types.DATE && a == Types.TIME) || (a == Types.TIME && b == Types.TIMESTAMP)
				|| (b == Types.TIME && a == Types.TIMESTAMP) || (a == Types.INTEGER && b == Types.BIGINT) || (a == Types.BIGINT && b == Types.INTEGER))
			return true;
		return false;
	}

	public static boolean like(String a, String b)
	{
		/*
		 * Create the cards by splitting using a RegEx. If more speed is desired, a simpler character based splitting
		 * can be done.
		 */
		String[] cards = b.split("\\%");
		// Iterate over the cards.
		boolean first = true;
		for (String card : cards)
		{
			int idx = a.indexOf(card);
			// Card not detected in the text.
			if (idx == -1)
			{
				return false;
			}
			if (first && idx > 0)
				return false;
			first = false;
			// Move ahead, towards the right of the text.
			a = a.substring(idx + card.length());
		}
		return true;
	}
}
