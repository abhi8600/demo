package memdb.sql;

import java.sql.SQLException;

import memdb.Command;
import memdb.Database;
import memdb.rowset.RowSet;

import org.antlr.runtime.tree.Tree;

public class Delete extends WhereStatement
{
	public Delete(Command c, Database db, Tree t, Object[] parameters) throws SQLException, InterruptedException
	{
		super(c, db, t, parameters);
	}

	public void delete() throws SQLException, InterruptedException
	{
		try
		{
			processRows();
			if (joinedrows == null || joinedrows.length == 0)
				return;
			RowSet rs = joinedrows[0];
			try
			{
				positions[0].table.latch.awaitWrite();
				positions[0].table.deleteRows(rs);
			} finally
			{
				positions[0].table.latch.releaseWrite();
			}
		} finally
		{
			releaseTables();
		}
	}
}
