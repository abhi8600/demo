package memdb.sql;

import java.sql.SQLException;

import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;

public class JoinCondition
{
	public int joinDirection;
	public Tree conditionTree;
	public JoinRowInfo joinrowinfo;

	public JoinCondition(int joinDirection, Tree conditionTree, WhereStatement wstmt, Table tableAtPosition) throws SQLException
	{
		this.joinDirection = joinDirection;
		this.conditionTree = conditionTree;
		joinrowinfo = new JoinRowInfo(wstmt, conditionTree, tableAtPosition);
	}
}
