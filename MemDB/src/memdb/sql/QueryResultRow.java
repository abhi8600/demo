package memdb.sql;

import java.io.Serializable;

public class QueryResultRow implements Serializable
{
	private static final long serialVersionUID = 1L;
	public Object[] dataRow;
	public Object[] sortRow;
	public Object[] aggRow;
}
