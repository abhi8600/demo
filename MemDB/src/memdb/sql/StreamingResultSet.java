package memdb.sql;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class StreamingResultSet
{
	static Logger logger = Logger.getLogger(StreamingResultSet.class);
	private static int QUEUE_SIZE = 10000;
	private ArrayDeque<QueryResultRow> results;
	private Collection<QueryResultRow> bulkResults;
	private Iterator<QueryResultRow> bulkResultsIterator;
	private boolean hasRowsRemaining = true;
	private int topn = -1;
	private Runnable runWhenDone;

	public StreamingResultSet(Collection<QueryResultRow> bulkResults)
	{
		if (bulkResults != null)
		{
			this.bulkResults = bulkResults;
			this.bulkResultsIterator = bulkResults.iterator();
		} else
			results = new ArrayDeque<QueryResultRow>(QUEUE_SIZE);
	}

	public void setTopN(int n)
	{
		topn = n;
	}

	/**
	 * Set action when no more input rows are available. This allows release of resources based on this stream.
	 * 
	 * @param runWhenDone
	 */
	public void setRunWhenDone(Runnable runWhenDone)
	{
		this.runWhenDone = runWhenDone;
	}

	public boolean addRow(QueryResultRow row)
	{
		synchronized (results)
		{
			while (results.size() > QUEUE_SIZE)
			{
				try
				{
					results.wait();
				} catch (InterruptedException e)
				{
				}
			}
			results.add(row);
			if (row.dataRow == null && runWhenDone != null)
				runWhenDone();
			results.notifyAll();
		}
		return true;
	}

	public QueryResultRow next()
	{
		if (topn == 0)
			return null;
		if (results != null)
		{
			if (topn == 0)
				return null;
			QueryResultRow qrr = null;
			synchronized (results)
			{
				while (results.size() == 0 && hasRowsRemaining)
				{
					try
					{
						results.wait();
					} catch (InterruptedException e)
					{
					}
				}
				if (!hasRowsRemaining)
					return null;
				qrr = results.remove();
				if (qrr.dataRow == null)
				{
					hasRowsRemaining = false;
					results.notifyAll();
					return null;
				}
				results.notifyAll();
				if (topn > 0)
					topn--;
				return qrr;
			}
		} else
		{
			if (!hasRowsRemaining)
				return null;
			synchronized (bulkResultsIterator)
			{
				if (topn == 0)
					return null;
				if (!bulkResultsIterator.hasNext())
				{
					hasRowsRemaining = false;
					return null;
				}
				if (topn > 0)
					topn--;
				return bulkResultsIterator.next();
			}
		}
	}

	public List<QueryResultRow> getList()
	{
		if (bulkResults != null)
		{
			if (bulkResults instanceof List)
				return (List<QueryResultRow>) bulkResults;
			return new ArrayList<QueryResultRow>(bulkResults);
		}
		List<QueryResultRow> resultList = new ArrayList<QueryResultRow>();
		while (true)
		{
			QueryResultRow qrr = next();
			if (qrr == null)
				break;
			resultList.add(qrr);
		}
		return resultList;
	}

	/**
	 * Run when there are no more input rows
	 */
	private void runWhenDone()
	{
		if (runWhenDone != null)
			runWhenDone.run();
	}

	public void setNoMoreRows()
	{
		QueryResultRow qrr = new QueryResultRow();
		synchronized (results)
		{
			while (results.size() > QUEUE_SIZE)
			{
				try
				{
					results.wait();
				} catch (InterruptedException e)
				{
				}
			}
			results.add(qrr);
		}
		runWhenDone();
		synchronized (results)
		{
			results.notifyAll();
		}
		synchronized (this)
		{
			this.notifyAll();
		}
	}
}
