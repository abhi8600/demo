// $ANTLR 3.4 C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g 2012-10-14 23:28:41
 package memdb.sql; 

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MemgrammarLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__163=163;
    public static final int T__164=164;
    public static final int T__165=165;
    public static final int ADD=4;
    public static final int ALTER=5;
    public static final int AND=6;
    public static final int AS=7;
    public static final int ASCENDING=8;
    public static final int AVG=9;
    public static final int BIGINTTYPE=10;
    public static final int BOOLEAN=11;
    public static final int CASE=12;
    public static final int CAST=13;
    public static final int COALESCE=14;
    public static final int COLLIST=15;
    public static final int COLUMN=16;
    public static final int COLUMNS=17;
    public static final int COMPRESSED=18;
    public static final int COMPRESSEDKEYSTORE=19;
    public static final int COUNT=20;
    public static final int CREATE=21;
    public static final int DATEADD=22;
    public static final int DATEDIFF=23;
    public static final int DATEPART=24;
    public static final int DATETIME=25;
    public static final int DATETIMETYPE=26;
    public static final int DATETYPE=27;
    public static final int DAY=28;
    public static final int DEBUG=29;
    public static final int DECIMALTYPE=30;
    public static final int DELETE=31;
    public static final int DELIMITEDBY=32;
    public static final int DESCENDING=33;
    public static final int DISK=34;
    public static final int DISKKEYSTORE=35;
    public static final int DISTINCT=36;
    public static final int DISTINCTFORKEY=37;
    public static final int DIV=38;
    public static final int DROP=39;
    public static final int DUMPDB=40;
    public static final int DUMPDDL=41;
    public static final int DUMPTABLE=42;
    public static final int ELSE=43;
    public static final int ENCLOSEDBY=44;
    public static final int END=45;
    public static final int EQUALS=46;
    public static final int EXIT=47;
    public static final int EXTRACT=48;
    public static final int EscapeSequence=49;
    public static final int FIELDS=50;
    public static final int FLOAT=51;
    public static final int FLOATTYPE=52;
    public static final int FLUSH=53;
    public static final int FOREIGNKEY=54;
    public static final int FROM=55;
    public static final int FULLOUTERJOIN=56;
    public static final int GETDATE=57;
    public static final int GROUPBY=58;
    public static final int GT=59;
    public static final int GTEQ=60;
    public static final int HAVING=61;
    public static final int HOUR=62;
    public static final int HexDigit=63;
    public static final int IDENT=64;
    public static final int IFEXISTS=65;
    public static final int IN=66;
    public static final int INDIR=67;
    public static final int INFOTABLES=68;
    public static final int INNERJOIN=69;
    public static final int INSERT=70;
    public static final int INTEGER=71;
    public static final int INTEGERTYPE=72;
    public static final int INTO=73;
    public static final int ISNOTNULL=74;
    public static final int ISNULL=75;
    public static final int ISNULLEXP=76;
    public static final int KILL=77;
    public static final int LEFTOUTERJOIN=78;
    public static final int LEFT_PAREN=79;
    public static final int LEN=80;
    public static final int LENGTH=81;
    public static final int LIKE=82;
    public static final int LINE_COMMENT=83;
    public static final int LOADDATAINFILE=84;
    public static final int LOADDB=85;
    public static final int LOADDDL=86;
    public static final int LOADTABLE=87;
    public static final int LT=88;
    public static final int LTEQ=89;
    public static final int MAX=90;
    public static final int MAXONE=91;
    public static final int MEMORY=92;
    public static final int MIN=93;
    public static final int MINUS=94;
    public static final int MINUTE=95;
    public static final int ML_COMMENT=96;
    public static final int MOD=97;
    public static final int MONTH=98;
    public static final int MULT=99;
    public static final int NEGATE=100;
    public static final int NOINDEX=101;
    public static final int NOT=102;
    public static final int NOTEQUALS=103;
    public static final int NOTIN=104;
    public static final int NOTLIKE=105;
    public static final int NULL=106;
    public static final int OBJECTSIZE=107;
    public static final int ON=108;
    public static final int OR=109;
    public static final int ORDERBY=110;
    public static final int OUTDIR=111;
    public static final int OUTFILE=112;
    public static final int PARAMETER=113;
    public static final int PAREN=114;
    public static final int PLUS=115;
    public static final int POSITION=116;
    public static final int POW=117;
    public static final int PRIMARYKEY=118;
    public static final int PROCESSLIST=119;
    public static final int PROJECTION=120;
    public static final int PROJECTIONLIST=121;
    public static final int QUARTER=122;
    public static final int QUIT=123;
    public static final int REMOTEQUERY=124;
    public static final int RENAME=125;
    public static final int RIGHTOUTERJOIN=126;
    public static final int RIGHT_PAREN=127;
    public static final int SCHEMA=128;
    public static final int SECOND=129;
    public static final int SELECT=130;
    public static final int SET=131;
    public static final int SHOW=132;
    public static final int SKIPFIRST=133;
    public static final int STRING=134;
    public static final int SUBQUERY=135;
    public static final int SUBSTR=136;
    public static final int SUM=137;
    public static final int SURROGATEKEYS=138;
    public static final int SYSTEMSTATUS=139;
    public static final int TABLE=140;
    public static final int TABLEALIAS=141;
    public static final int TABLES=142;
    public static final int TERMINATEDBY=143;
    public static final int THEN=144;
    public static final int TIMESTAMPADD=145;
    public static final int TIMESTAMPDIFF=146;
    public static final int TO=147;
    public static final int TOLOWER=148;
    public static final int TOP=149;
    public static final int TOUPPER=150;
    public static final int TRANSIENT=151;
    public static final int TRUNCATE=152;
    public static final int UNCOMPRESSED=153;
    public static final int UPDATE=154;
    public static final int UnicodeEscape=155;
    public static final int VALUES=156;
    public static final int VARCHARTYPE=157;
    public static final int WEEK=158;
    public static final int WHEN=159;
    public static final int WHERE=160;
    public static final int WS=161;
    public static final int YEAR=162;

    	List<RecognitionException> exceptions = new ArrayList<RecognitionException>();

    	@Override
    	public void reportError(RecognitionException e)
    	{
    	    exceptions.add(e);
    	}

    	public boolean hasError()
    	{
    		return exceptions.size() > 0;
    	}

    	public RecognitionException errorMessage()
    	{
    		return exceptions.get(0);
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MemgrammarLexer() {} 
    public MemgrammarLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MemgrammarLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g"; }

    // $ANTLR start "T__163"
    public final void mT__163() throws RecognitionException {
        try {
            int _type = T__163;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:28:8: ( ',' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:28:10: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__163"

    // $ANTLR start "T__164"
    public final void mT__164() throws RecognitionException {
        try {
            int _type = T__164;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:29:8: ( '.' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:29:10: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__164"

    // $ANTLR start "T__165"
    public final void mT__165() throws RecognitionException {
        try {
            int _type = T__165;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:30:8: ( ';' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:30:10: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__165"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:6: ( '\\'' ( EscapeSequence | ( options {greedy=false; } :~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )* '\\'' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:10: '\\'' ( EscapeSequence | ( options {greedy=false; } :~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )* '\\''
            {
            match('\''); 

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:15: ( EscapeSequence | ( options {greedy=false; } :~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='\\') ) {
                    alt1=1;
                }
                else if ( ((LA1_0 >= ' ' && LA1_0 <= '&')||(LA1_0 >= '(' && LA1_0 <= '[')||(LA1_0 >= ']' && LA1_0 <= '\uFFFF')) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:17: EscapeSequence
            	    {
            	    mEscapeSequence(); 


            	    }
            	    break;
            	case 2 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:34: ( options {greedy=false; } :~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) )
            	    {
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:34: ( options {greedy=false; } :~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) )
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:64:61: ~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' )
            	    {
            	    if ( (input.LA(1) >= ' ' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:68:2: ( ( '-' )? ( '0' .. '9' )+ )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:68:4: ( '-' )? ( '0' .. '9' )+
            {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:68:4: ( '-' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='-') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:68:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:68:9: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:2: ( ( '-' )? ( '0' .. '9' )* '.' ( '0' .. '9' )+ )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:4: ( '-' )? ( '0' .. '9' )* '.' ( '0' .. '9' )+
            {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:4: ( '-' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='-') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:4: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:9: ( '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            match('.'); 

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:71:25: ( '0' .. '9' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOAT"

    // $ANTLR start "DATETIME"
    public final void mDATETIME() throws RecognitionException {
        try {
            int _type = DATETIME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:3: ( '#' (~ '#' )* '#' | ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='#') ) {
                alt8=1;
            }
            else if ( (LA8_0=='N'||LA8_0=='n') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:5: '#' (~ '#' )* '#'
                    {
                    match('#'); 

                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:9: (~ '#' )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\"')||(LA7_0 >= '$' && LA7_0 <= '\uFFFF')) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\"')||(input.LA(1) >= '$' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);


                    match('#'); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:23: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) )
                    {
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:23: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) )
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:74:24: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETIME"

    // $ANTLR start "BOOLEAN"
    public final void mBOOLEAN() throws RecognitionException {
        try {
            int _type = BOOLEAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:77:2: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' ) | ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='T'||LA9_0=='t') ) {
                alt9=1;
            }
            else if ( (LA9_0=='F'||LA9_0=='f') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:77:4: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:78:4: ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BOOLEAN"

    // $ANTLR start "NULL"
    public final void mNULL() throws RecognitionException {
        try {
            int _type = NULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:80:6: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:80:8: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NULL"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:82:5: ( '||' | ( ( 'O' | 'o' ) ( 'R' | 'r' ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='|') ) {
                alt10=1;
            }
            else if ( (LA10_0=='O'||LA10_0=='o') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }
            switch (alt10) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:82:8: '||'
                    {
                    match("||"); 



                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:82:15: ( ( 'O' | 'o' ) ( 'R' | 'r' ) )
                    {
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:82:15: ( ( 'O' | 'o' ) ( 'R' | 'r' ) )
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:82:16: ( 'O' | 'o' ) ( 'R' | 'r' )
                    {
                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:84:6: ( '&&' | ( ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='&') ) {
                alt11=1;
            }
            else if ( (LA11_0=='A'||LA11_0=='a') ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:84:9: '&&'
                    {
                    match("&&"); 



                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:84:16: ( ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
                    {
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:84:16: ( ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:84:17: ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' )
                    {
                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "EQUALS"
    public final void mEQUALS() throws RecognitionException {
        try {
            int _type = EQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:87:2: ( '=' | '==' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='=') ) {
                int LA12_1 = input.LA(2);

                if ( (LA12_1=='=') ) {
                    alt12=2;
                }
                else {
                    alt12=1;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:87:4: '='
                    {
                    match('='); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:87:10: '=='
                    {
                    match("=="); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQUALS"

    // $ANTLR start "NOTEQUALS"
    public final void mNOTEQUALS() throws RecognitionException {
        try {
            int _type = NOTEQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:89:2: ( '!=' | '<>' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='!') ) {
                alt13=1;
            }
            else if ( (LA13_0=='<') ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }
            switch (alt13) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:89:4: '!='
                    {
                    match("!="); 



                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:89:11: '<>'
                    {
                    match("<>"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTEQUALS"

    // $ANTLR start "ISNULL"
    public final void mISNULL() throws RecognitionException {
        try {
            int _type = ISNULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:90:7: ( ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:91:2: ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNULL"

    // $ANTLR start "ISNOTNULL"
    public final void mISNOTNULL() throws RecognitionException {
        try {
            int _type = ISNOTNULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:93:2: ( ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:94:2: ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNOTNULL"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:95:4: ( '<' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:95:6: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "LTEQ"
    public final void mLTEQ() throws RecognitionException {
        try {
            int _type = LTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:96:6: ( '<=' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:96:8: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LTEQ"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:97:4: ( '>' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:97:6: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "GTEQ"
    public final void mGTEQ() throws RecognitionException {
        try {
            int _type = GTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:98:6: ( '>=' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:98:8: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GTEQ"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:99:6: ( '+' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:99:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:100:7: ( '-' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:100:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "MULT"
    public final void mMULT() throws RecognitionException {
        try {
            int _type = MULT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:101:6: ( '*' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:101:8: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MULT"

    // $ANTLR start "DIV"
    public final void mDIV() throws RecognitionException {
        try {
            int _type = DIV;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:102:5: ( '/' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:102:7: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "MOD"
    public final void mMOD() throws RecognitionException {
        try {
            int _type = MOD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:103:5: ( '%' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:103:7: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MOD"

    // $ANTLR start "POW"
    public final void mPOW() throws RecognitionException {
        try {
            int _type = POW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:104:5: ( '^' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:104:7: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POW"

    // $ANTLR start "NOT"
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:105:5: ( '!' | ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='!') ) {
                alt14=1;
            }
            else if ( (LA14_0=='N'||LA14_0=='n') ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:105:7: '!'
                    {
                    match('!'); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:105:13: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "SELECT"
    public final void mSELECT() throws RecognitionException {
        try {
            int _type = SELECT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:107:8: ( ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:107:10: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SELECT"

    // $ANTLR start "SHOW"
    public final void mSHOW() throws RecognitionException {
        try {
            int _type = SHOW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:108:6: ( ( 'S' | 's' ) ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'W' | 'w' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:108:8: ( 'S' | 's' ) ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'W' | 'w' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SHOW"

    // $ANTLR start "TABLES"
    public final void mTABLES() throws RecognitionException {
        try {
            int _type = TABLES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:109:8: ( ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:109:10: ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TABLES"

    // $ANTLR start "SYSTEMSTATUS"
    public final void mSYSTEMSTATUS() throws RecognitionException {
        try {
            int _type = SYSTEMSTATUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:110:13: ( ( 'S' | 's' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'M' | 'm' ) WS ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'U' | 'u' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:111:3: ( 'S' | 's' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'M' | 'm' ) WS ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'U' | 'u' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SYSTEMSTATUS"

    // $ANTLR start "DROP"
    public final void mDROP() throws RecognitionException {
        try {
            int _type = DROP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:112:6: ( ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'P' | 'p' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:112:8: ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DROP"

    // $ANTLR start "IFEXISTS"
    public final void mIFEXISTS() throws RecognitionException {
        try {
            int _type = IFEXISTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:113:9: ( ( 'I' | 'i' ) ( 'F' | 'f' ) WS ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:113:11: ( 'I' | 'i' ) ( 'F' | 'f' ) WS ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IFEXISTS"

    // $ANTLR start "TRUNCATE"
    public final void mTRUNCATE() throws RecognitionException {
        try {
            int _type = TRUNCATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:114:9: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:114:11: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TRUNCATE"

    // $ANTLR start "TABLE"
    public final void mTABLE() throws RecognitionException {
        try {
            int _type = TABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:115:7: ( ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:115:9: ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TABLE"

    // $ANTLR start "RENAME"
    public final void mRENAME() throws RecognitionException {
        try {
            int _type = RENAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:116:8: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:116:10: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RENAME"

    // $ANTLR start "DELETE"
    public final void mDELETE() throws RecognitionException {
        try {
            int _type = DELETE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:117:8: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:117:10: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DELETE"

    // $ANTLR start "INSERT"
    public final void mINSERT() throws RecognitionException {
        try {
            int _type = INSERT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:118:8: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:118:10: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INSERT"

    // $ANTLR start "UPDATE"
    public final void mUPDATE() throws RecognitionException {
        try {
            int _type = UPDATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:119:8: ( ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:119:10: ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UPDATE"

    // $ANTLR start "CREATE"
    public final void mCREATE() throws RecognitionException {
        try {
            int _type = CREATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:120:8: ( ( 'C' | 'c' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:120:10: ( 'C' | 'c' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CREATE"

    // $ANTLR start "EXIT"
    public final void mEXIT() throws RecognitionException {
        try {
            int _type = EXIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:121:6: ( ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:121:8: ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXIT"

    // $ANTLR start "QUIT"
    public final void mQUIT() throws RecognitionException {
        try {
            int _type = QUIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:122:6: ( ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'I' | 'i' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:122:8: ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'I' | 'i' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUIT"

    // $ANTLR start "KILL"
    public final void mKILL() throws RecognitionException {
        try {
            int _type = KILL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:123:6: ( ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:123:8: ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "KILL"

    // $ANTLR start "ALTER"
    public final void mALTER() throws RecognitionException {
        try {
            int _type = ALTER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:124:7: ( ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:124:9: ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ALTER"

    // $ANTLR start "PROCESSLIST"
    public final void mPROCESSLIST() throws RecognitionException {
        try {
            int _type = PROCESSLIST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:126:2: ( ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:126:4: ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PROCESSLIST"

    // $ANTLR start "SET"
    public final void mSET() throws RecognitionException {
        try {
            int _type = SET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:128:5: ( ( 'S' | 's' ) ( 'E' | 'e' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:128:7: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SET"

    // $ANTLR start "FLUSH"
    public final void mFLUSH() throws RecognitionException {
        try {
            int _type = FLUSH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:129:7: ( ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'S' | 's' ) ( 'H' | 'h' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:129:9: ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'S' | 's' ) ( 'H' | 'h' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLUSH"

    // $ANTLR start "INTO"
    public final void mINTO() throws RecognitionException {
        try {
            int _type = INTO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:130:6: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'O' | 'o' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:130:8: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'O' | 'o' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTO"

    // $ANTLR start "VALUES"
    public final void mVALUES() throws RecognitionException {
        try {
            int _type = VALUES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:131:8: ( ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:131:10: ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VALUES"

    // $ANTLR start "INFOTABLES"
    public final void mINFOTABLES() throws RecognitionException {
        try {
            int _type = INFOTABLES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:133:2: ( 'INFORMATION_SCHEMA.TABLES' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:133:4: 'INFORMATION_SCHEMA.TABLES'
            {
            match("INFORMATION_SCHEMA.TABLES"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INFOTABLES"

    // $ANTLR start "REMOTEQUERY"
    public final void mREMOTEQUERY() throws RecognitionException {
        try {
            int _type = REMOTEQUERY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:135:2: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:135:4: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REMOTEQUERY"

    // $ANTLR start "LOADDATAINFILE"
    public final void mLOADDATAINFILE() throws RecognitionException {
        try {
            int _type = LOADDATAINFILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:137:2: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) WS ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'A' | 'a' ) WS ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:137:4: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) WS ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'A' | 'a' ) WS ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOADDATAINFILE"

    // $ANTLR start "OUTFILE"
    public final void mOUTFILE() throws RecognitionException {
        try {
            int _type = OUTFILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:138:9: ( ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:138:11: ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OUTFILE"

    // $ANTLR start "FIELDS"
    public final void mFIELDS() throws RecognitionException {
        try {
            int _type = FIELDS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:139:8: ( ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:139:10: ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FIELDS"

    // $ANTLR start "COLUMNS"
    public final void mCOLUMNS() throws RecognitionException {
        try {
            int _type = COLUMNS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:140:9: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:140:11: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLUMNS"

    // $ANTLR start "COLUMN"
    public final void mCOLUMN() throws RecognitionException {
        try {
            int _type = COLUMN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:141:8: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:141:10: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLUMN"

    // $ANTLR start "ADD"
    public final void mADD() throws RecognitionException {
        try {
            int _type = ADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:142:5: ( ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:142:7: ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ADD"

    // $ANTLR start "ENCLOSEDBY"
    public final void mENCLOSEDBY() throws RecognitionException {
        try {
            int _type = ENCLOSEDBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:144:2: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:144:4: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ENCLOSEDBY"

    // $ANTLR start "DELIMITEDBY"
    public final void mDELIMITEDBY() throws RecognitionException {
        try {
            int _type = DELIMITEDBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:146:2: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:146:4: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DELIMITEDBY"

    // $ANTLR start "TERMINATEDBY"
    public final void mTERMINATEDBY() throws RecognitionException {
        try {
            int _type = TERMINATEDBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:148:2: ( ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:148:4: ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TERMINATEDBY"

    // $ANTLR start "PRIMARYKEY"
    public final void mPRIMARYKEY() throws RecognitionException {
        try {
            int _type = PRIMARYKEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:150:2: ( ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'Y' | 'y' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:150:4: ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'Y' | 'y' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PRIMARYKEY"

    // $ANTLR start "FOREIGNKEY"
    public final void mFOREIGNKEY() throws RecognitionException {
        try {
            int _type = FOREIGNKEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:152:2: ( ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'N' | 'n' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:152:4: ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'N' | 'n' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FOREIGNKEY"

    // $ANTLR start "SURROGATEKEYS"
    public final void mSURROGATEKEYS() throws RecognitionException {
        try {
            int _type = SURROGATEKEYS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:153:14: ( ( 'S' | 's' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'G' | 'g' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:154:3: ( 'S' | 's' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'G' | 'g' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) WS ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SURROGATEKEYS"

    // $ANTLR start "SKIPFIRST"
    public final void mSKIPFIRST() throws RecognitionException {
        try {
            int _type = SKIPFIRST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:156:2: ( ( 'S' | 's' ) ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'P' | 'p' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:156:4: ( 'S' | 's' ) ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'P' | 'p' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SKIPFIRST"

    // $ANTLR start "NOINDEX"
    public final void mNOINDEX() throws RecognitionException {
        try {
            int _type = NOINDEX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:157:9: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'X' | 'x' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:157:11: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'X' | 'x' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOINDEX"

    // $ANTLR start "FROM"
    public final void mFROM() throws RecognitionException {
        try {
            int _type = FROM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:159:6: ( ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:159:8: ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FROM"

    // $ANTLR start "WHERE"
    public final void mWHERE() throws RecognitionException {
        try {
            int _type = WHERE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:160:7: ( ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:160:9: ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHERE"

    // $ANTLR start "ORDERBY"
    public final void mORDERBY() throws RecognitionException {
        try {
            int _type = ORDERBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:161:9: ( ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:161:11: ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ORDERBY"

    // $ANTLR start "GROUPBY"
    public final void mGROUPBY() throws RecognitionException {
        try {
            int _type = GROUPBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:162:9: ( ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:162:11: ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GROUPBY"

    // $ANTLR start "HAVING"
    public final void mHAVING() throws RecognitionException {
        try {
            int _type = HAVING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:163:8: ( ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:163:10: ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HAVING"

    // $ANTLR start "INNERJOIN"
    public final void mINNERJOIN() throws RecognitionException {
        try {
            int _type = INNERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:165:2: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:165:4: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INNERJOIN"

    // $ANTLR start "LEFTOUTERJOIN"
    public final void mLEFTOUTERJOIN() throws RecognitionException {
        try {
            int _type = LEFTOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:167:2: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:167:4: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEFTOUTERJOIN"

    // $ANTLR start "RIGHTOUTERJOIN"
    public final void mRIGHTOUTERJOIN() throws RecognitionException {
        try {
            int _type = RIGHTOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:169:2: ( ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'H' | 'h' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:169:4: ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'H' | 'h' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RIGHTOUTERJOIN"

    // $ANTLR start "FULLOUTERJOIN"
    public final void mFULLOUTERJOIN() throws RecognitionException {
        try {
            int _type = FULLOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:171:2: ( ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:171:4: ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FULLOUTERJOIN"

    // $ANTLR start "MAXONE"
    public final void mMAXONE() throws RecognitionException {
        try {
            int _type = MAXONE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:173:8: ( ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:173:10: ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MAXONE"

    // $ANTLR start "DISTINCTFORKEY"
    public final void mDISTINCTFORKEY() throws RecognitionException {
        try {
            int _type = DISTINCTFORKEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:175:2: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:175:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISTINCTFORKEY"

    // $ANTLR start "TRANSIENT"
    public final void mTRANSIENT() throws RecognitionException {
        try {
            int _type = TRANSIENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:177:2: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:177:4: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TRANSIENT"

    // $ANTLR start "ON"
    public final void mON() throws RecognitionException {
        try {
            int _type = ON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:179:4: ( ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:179:6: ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ON"

    // $ANTLR start "IN"
    public final void mIN() throws RecognitionException {
        try {
            int _type = IN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:180:4: ( ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:180:6: ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IN"

    // $ANTLR start "NOTIN"
    public final void mNOTIN() throws RecognitionException {
        try {
            int _type = NOTIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:181:7: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:181:9: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTIN"

    // $ANTLR start "NOTLIKE"
    public final void mNOTLIKE() throws RecognitionException {
        try {
            int _type = NOTLIKE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:182:9: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:182:11: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTLIKE"

    // $ANTLR start "LIKE"
    public final void mLIKE() throws RecognitionException {
        try {
            int _type = LIKE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:183:6: ( ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:183:8: ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LIKE"

    // $ANTLR start "SUM"
    public final void mSUM() throws RecognitionException {
        try {
            int _type = SUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:185:5: ( ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:185:7: ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SUM"

    // $ANTLR start "AVG"
    public final void mAVG() throws RecognitionException {
        try {
            int _type = AVG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:186:5: ( ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'G' | 'g' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:186:7: ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AVG"

    // $ANTLR start "COUNT"
    public final void mCOUNT() throws RecognitionException {
        try {
            int _type = COUNT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:187:7: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:187:9: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COUNT"

    // $ANTLR start "DISTINCT"
    public final void mDISTINCT() throws RecognitionException {
        try {
            int _type = DISTINCT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:188:10: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:189:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISTINCT"

    // $ANTLR start "MIN"
    public final void mMIN() throws RecognitionException {
        try {
            int _type = MIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:190:5: ( ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:190:7: ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MIN"

    // $ANTLR start "MAX"
    public final void mMAX() throws RecognitionException {
        try {
            int _type = MAX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:191:5: ( ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:191:7: ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MAX"

    // $ANTLR start "CASE"
    public final void mCASE() throws RecognitionException {
        try {
            int _type = CASE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:192:6: ( ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:192:8: ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CASE"

    // $ANTLR start "WHEN"
    public final void mWHEN() throws RecognitionException {
        try {
            int _type = WHEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:193:6: ( ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:193:8: ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHEN"

    // $ANTLR start "THEN"
    public final void mTHEN() throws RecognitionException {
        try {
            int _type = THEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:194:6: ( ( 'T' | 't' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:194:8: ( 'T' | 't' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "THEN"

    // $ANTLR start "ELSE"
    public final void mELSE() throws RecognitionException {
        try {
            int _type = ELSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:195:6: ( ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:195:8: ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ELSE"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:196:5: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:196:7: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "TOP"
    public final void mTOP() throws RecognitionException {
        try {
            int _type = TOP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:197:5: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'P' | 'p' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:197:7: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOP"

    // $ANTLR start "YEAR"
    public final void mYEAR() throws RecognitionException {
        try {
            int _type = YEAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:199:6: ( ( 'Y' | 'y' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:199:8: ( 'Y' | 'y' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "YEAR"

    // $ANTLR start "QUARTER"
    public final void mQUARTER() throws RecognitionException {
        try {
            int _type = QUARTER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:200:9: ( ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:200:11: ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUARTER"

    // $ANTLR start "MONTH"
    public final void mMONTH() throws RecognitionException {
        try {
            int _type = MONTH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:201:7: ( ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:201:9: ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MONTH"

    // $ANTLR start "WEEK"
    public final void mWEEK() throws RecognitionException {
        try {
            int _type = WEEK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:202:6: ( ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:202:8: ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WEEK"

    // $ANTLR start "DAY"
    public final void mDAY() throws RecognitionException {
        try {
            int _type = DAY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:203:5: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:203:7: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DAY"

    // $ANTLR start "HOUR"
    public final void mHOUR() throws RecognitionException {
        try {
            int _type = HOUR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:205:6: ( ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:205:8: ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HOUR"

    // $ANTLR start "MINUTE"
    public final void mMINUTE() throws RecognitionException {
        try {
            int _type = MINUTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:206:8: ( ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:206:10: ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUTE"

    // $ANTLR start "SECOND"
    public final void mSECOND() throws RecognitionException {
        try {
            int _type = SECOND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:207:8: ( ( 'S' | 's' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:207:10: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SECOND"

    // $ANTLR start "VARCHARTYPE"
    public final void mVARCHARTYPE() throws RecognitionException {
        try {
            int _type = VARCHARTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:209:13: ( ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:209:15: ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VARCHARTYPE"

    // $ANTLR start "INTEGERTYPE"
    public final void mINTEGERTYPE() throws RecognitionException {
        try {
            int _type = INTEGERTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:210:13: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:210:15: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGERTYPE"

    // $ANTLR start "BIGINTTYPE"
    public final void mBIGINTTYPE() throws RecognitionException {
        try {
            int _type = BIGINTTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:211:12: ( ( 'B' | 'b' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:211:14: ( 'B' | 'b' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BIGINTTYPE"

    // $ANTLR start "FLOATTYPE"
    public final void mFLOATTYPE() throws RecognitionException {
        try {
            int _type = FLOATTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:212:11: ( ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:212:13: ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOATTYPE"

    // $ANTLR start "DECIMALTYPE"
    public final void mDECIMALTYPE() throws RecognitionException {
        try {
            int _type = DECIMALTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:213:13: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:213:15: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DECIMALTYPE"

    // $ANTLR start "DATETYPE"
    public final void mDATETYPE() throws RecognitionException {
        try {
            int _type = DATETYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:214:10: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:214:12: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETYPE"

    // $ANTLR start "DATETIMETYPE"
    public final void mDATETIMETYPE() throws RecognitionException {
        try {
            int _type = DATETIMETYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:215:14: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:215:16: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETIMETYPE"

    // $ANTLR start "TIMESTAMPADD"
    public final void mTIMESTAMPADD() throws RecognitionException {
        try {
            int _type = TIMESTAMPADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:218:2: ( ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:218:4: ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TIMESTAMPADD"

    // $ANTLR start "TIMESTAMPDIFF"
    public final void mTIMESTAMPDIFF() throws RecognitionException {
        try {
            int _type = TIMESTAMPDIFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:220:2: ( ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:220:4: ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TIMESTAMPDIFF"

    // $ANTLR start "EXTRACT"
    public final void mEXTRACT() throws RecognitionException {
        try {
            int _type = EXTRACT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:221:8: ( ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:221:10: ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXTRACT"

    // $ANTLR start "DATEDIFF"
    public final void mDATEDIFF() throws RecognitionException {
        try {
            int _type = DATEDIFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:222:9: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:222:11: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEDIFF"

    // $ANTLR start "DATEADD"
    public final void mDATEADD() throws RecognitionException {
        try {
            int _type = DATEADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:223:8: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:223:10: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEADD"

    // $ANTLR start "DATEPART"
    public final void mDATEPART() throws RecognitionException {
        try {
            int _type = DATEPART;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:224:9: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:224:11: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEPART"

    // $ANTLR start "GETDATE"
    public final void mGETDATE() throws RecognitionException {
        try {
            int _type = GETDATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:225:9: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:225:11: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETDATE"

    // $ANTLR start "SUBSTR"
    public final void mSUBSTR() throws RecognitionException {
        try {
            int _type = SUBSTR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:226:7: ( ( 'S' | 's' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:226:11: ( 'S' | 's' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SUBSTR"

    // $ANTLR start "LENGTH"
    public final void mLENGTH() throws RecognitionException {
        try {
            int _type = LENGTH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:227:7: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'T' | 't' ) ( 'H' | 'h' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:227:11: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'T' | 't' ) ( 'H' | 'h' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LENGTH"

    // $ANTLR start "LEN"
    public final void mLEN() throws RecognitionException {
        try {
            int _type = LEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:228:4: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:228:8: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEN"

    // $ANTLR start "POSITION"
    public final void mPOSITION() throws RecognitionException {
        try {
            int _type = POSITION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:229:9: ( ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:229:11: ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POSITION"

    // $ANTLR start "OBJECTSIZE"
    public final void mOBJECTSIZE() throws RecognitionException {
        try {
            int _type = OBJECTSIZE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:231:2: ( ( 'O' | 'o' ) ( 'B' | 'b' ) ( 'J' | 'j' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'Z' | 'z' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:231:4: ( 'O' | 'o' ) ( 'B' | 'b' ) ( 'J' | 'j' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'Z' | 'z' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Z'||input.LA(1)=='z' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OBJECTSIZE"

    // $ANTLR start "TOUPPER"
    public final void mTOUPPER() throws RecognitionException {
        try {
            int _type = TOUPPER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:232:8: ( ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'P' | 'p' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:232:10: ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'P' | 'p' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOUPPER"

    // $ANTLR start "TOLOWER"
    public final void mTOLOWER() throws RecognitionException {
        try {
            int _type = TOLOWER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:233:8: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:233:10: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOLOWER"

    // $ANTLR start "ISNULLEXP"
    public final void mISNULLEXP() throws RecognitionException {
        try {
            int _type = ISNULLEXP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:234:10: ( ( 'I' | 'i' ) ( 'S' | 's' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:234:12: ( 'I' | 'i' ) ( 'S' | 's' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNULLEXP"

    // $ANTLR start "COALESCE"
    public final void mCOALESCE() throws RecognitionException {
        try {
            int _type = COALESCE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:235:9: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:235:11: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COALESCE"

    // $ANTLR start "CAST"
    public final void mCAST() throws RecognitionException {
        try {
            int _type = CAST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:237:6: ( ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:237:8: ( 'C' | 'c' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CAST"

    // $ANTLR start "DEBUG"
    public final void mDEBUG() throws RecognitionException {
        try {
            int _type = DEBUG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:238:7: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'B' | 'b' ) ( 'U' | 'u' ) ( 'G' | 'g' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:238:9: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'B' | 'b' ) ( 'U' | 'u' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DEBUG"

    // $ANTLR start "DUMPTABLE"
    public final void mDUMPTABLE() throws RecognitionException {
        try {
            int _type = DUMPTABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:240:11: ( ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:240:13: ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DUMPTABLE"

    // $ANTLR start "LOADTABLE"
    public final void mLOADTABLE() throws RecognitionException {
        try {
            int _type = LOADTABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:241:11: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:241:13: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOADTABLE"

    // $ANTLR start "DUMPDDL"
    public final void mDUMPDDL() throws RecognitionException {
        try {
            int _type = DUMPDDL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:242:9: ( ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:242:11: ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DUMPDDL"

    // $ANTLR start "LOADDDL"
    public final void mLOADDDL() throws RecognitionException {
        try {
            int _type = LOADDDL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:243:9: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'L' | 'l' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:243:11: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOADDDL"

    // $ANTLR start "DUMPDB"
    public final void mDUMPDB() throws RecognitionException {
        try {
            int _type = DUMPDB;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:244:8: ( ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'B' | 'b' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:244:10: ( 'D' | 'd' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'B' | 'b' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DUMPDB"

    // $ANTLR start "LOADDB"
    public final void mLOADDB() throws RecognitionException {
        try {
            int _type = LOADDB;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:245:8: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'B' | 'b' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:245:10: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'B' | 'b' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOADDB"

    // $ANTLR start "OUTDIR"
    public final void mOUTDIR() throws RecognitionException {
        try {
            int _type = OUTDIR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:247:8: ( ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:247:10: ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OUTDIR"

    // $ANTLR start "INDIR"
    public final void mINDIR() throws RecognitionException {
        try {
            int _type = INDIR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:248:7: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'R' | 'r' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:248:9: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INDIR"

    // $ANTLR start "TO"
    public final void mTO() throws RecognitionException {
        try {
            int _type = TO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:249:4: ( ( 'T' | 't' ) ( 'O' | 'o' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:249:6: ( 'T' | 't' ) ( 'O' | 'o' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TO"

    // $ANTLR start "DISK"
    public final void mDISK() throws RecognitionException {
        try {
            int _type = DISK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:251:2: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'K' | 'k' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:251:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'K' | 'k' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISK"

    // $ANTLR start "MEMORY"
    public final void mMEMORY() throws RecognitionException {
        try {
            int _type = MEMORY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:252:8: ( ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'Y' | 'y' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:252:10: ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MEMORY"

    // $ANTLR start "COMPRESSED"
    public final void mCOMPRESSED() throws RecognitionException {
        try {
            int _type = COMPRESSED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:254:2: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:254:4: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMPRESSED"

    // $ANTLR start "UNCOMPRESSED"
    public final void mUNCOMPRESSED() throws RecognitionException {
        try {
            int _type = UNCOMPRESSED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:256:2: ( ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:256:4: ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UNCOMPRESSED"

    // $ANTLR start "DISKKEYSTORE"
    public final void mDISKKEYSTORE() throws RecognitionException {
        try {
            int _type = DISKKEYSTORE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:258:2: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'K' | 'k' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:258:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'K' | 'k' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISKKEYSTORE"

    // $ANTLR start "COMPRESSEDKEYSTORE"
    public final void mCOMPRESSEDKEYSTORE() throws RecognitionException {
        try {
            int _type = COMPRESSEDKEYSTORE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:260:2: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:260:4: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'K' | 'k' ) ( 'E' | 'e' ) ( 'Y' | 'y' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMPRESSEDKEYSTORE"

    // $ANTLR start "LEFT_PAREN"
    public final void mLEFT_PAREN() throws RecognitionException {
        try {
            int _type = LEFT_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:262:11: ( '(' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:262:14: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEFT_PAREN"

    // $ANTLR start "RIGHT_PAREN"
    public final void mRIGHT_PAREN() throws RecognitionException {
        try {
            int _type = RIGHT_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:263:12: ( ')' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:263:15: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RIGHT_PAREN"

    // $ANTLR start "PARAMETER"
    public final void mPARAMETER() throws RecognitionException {
        try {
            int _type = PARAMETER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:266:2: ( '?' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:266:4: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PARAMETER"

    // $ANTLR start "ASCENDING"
    public final void mASCENDING() throws RecognitionException {
        try {
            int _type = ASCENDING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:269:2: ( ( 'A' | 'a' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:269:4: ( 'A' | 'a' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:269:34: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='E'||LA15_0=='e') ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:269:35: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ASCENDING"

    // $ANTLR start "DESCENDING"
    public final void mDESCENDING() throws RecognitionException {
        try {
            int _type = DESCENDING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:272:2: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )? )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:272:4: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:272:44: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='E'||LA16_0=='e') ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:272:45: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DESCENDING"

    // $ANTLR start "AS"
    public final void mAS() throws RecognitionException {
        try {
            int _type = AS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:274:4: ( ( 'A' | 'a' ) ( 'S' | 's' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:274:6: ( 'A' | 'a' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AS"

    // $ANTLR start "IDENT"
    public final void mIDENT() throws RecognitionException {
        try {
            int _type = IDENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:277:2: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ':' | '$' | '#' | '0' .. '9' )* )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:277:4: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ':' | '$' | '#' | '0' .. '9' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:277:33: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ':' | '$' | '#' | '0' .. '9' )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0 >= '#' && LA17_0 <= '$')||(LA17_0 >= '0' && LA17_0 <= ':')||(LA17_0 >= 'A' && LA17_0 <= 'Z')||LA17_0=='_'||(LA17_0 >= 'a' && LA17_0 <= 'z')) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            	    {
            	    if ( (input.LA(1) >= '#' && input.LA(1) <= '$')||(input.LA(1) >= '0' && input.LA(1) <= ':')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "EscapeSequence"
    public final void mEscapeSequence() throws RecognitionException {
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:280:2: ( '\\\\' ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:280:4: '\\\\' ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape )
            {
            match('\\'); 

            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:281:4: ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape )
            int alt18=6;
            switch ( input.LA(1) ) {
            case 'n':
                {
                alt18=1;
                }
                break;
            case 'r':
                {
                alt18=2;
                }
                break;
            case 't':
                {
                alt18=3;
                }
                break;
            case '\'':
                {
                alt18=4;
                }
                break;
            case '\\':
                {
                alt18=5;
                }
                break;
            case 'u':
                {
                alt18=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }

            switch (alt18) {
                case 1 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:282:5: 'n'
                    {
                    match('n'); 

                    }
                    break;
                case 2 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:283:4: 'r'
                    {
                    match('r'); 

                    }
                    break;
                case 3 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:284:4: 't'
                    {
                    match('t'); 

                    }
                    break;
                case 4 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:285:4: '\\''
                    {
                    match('\''); 

                    }
                    break;
                case 5 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:286:4: '\\\\'
                    {
                    match('\\'); 

                    }
                    break;
                case 6 :
                    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:287:4: UnicodeEscape
                    {
                    mUnicodeEscape(); 


                    }
                    break;

            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EscapeSequence"

    // $ANTLR start "UnicodeEscape"
    public final void mUnicodeEscape() throws RecognitionException {
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:291:6: ( 'u' HexDigit HexDigit HexDigit HexDigit )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:291:12: 'u' HexDigit HexDigit HexDigit HexDigit
            {
            match('u'); 

            mHexDigit(); 


            mHexDigit(); 


            mHexDigit(); 


            mHexDigit(); 


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UnicodeEscape"

    // $ANTLR start "HexDigit"
    public final void mHexDigit() throws RecognitionException {
        try {
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:294:2: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HexDigit"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:298:2: ( ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' ) )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:298:5: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "LINE_COMMENT"
    public final void mLINE_COMMENT() throws RecognitionException {
        try {
            int _type = LINE_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:302:2: ( '//' ( . )* '\\n' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:302:4: '//' ( . )* '\\n'
            {
            match("//"); 



            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:302:9: ( . )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0=='\n') ) {
                    alt19=2;
                }
                else if ( ((LA19_0 >= '\u0000' && LA19_0 <= '\t')||(LA19_0 >= '\u000B' && LA19_0 <= '\uFFFF')) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:302:9: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            match('\n'); 

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINE_COMMENT"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:304:2: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:304:6: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 



            // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:304:11: ( options {greedy=false; } : . )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0=='*') ) {
                    int LA20_1 = input.LA(2);

                    if ( (LA20_1=='/') ) {
                        alt20=2;
                    }
                    else if ( ((LA20_1 >= '\u0000' && LA20_1 <= '.')||(LA20_1 >= '0' && LA20_1 <= '\uFFFF')) ) {
                        alt20=1;
                    }


                }
                else if ( ((LA20_0 >= '\u0000' && LA20_0 <= ')')||(LA20_0 >= '+' && LA20_0 <= '\uFFFF')) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:304:38: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            match("*/"); 



            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    public void mTokens() throws RecognitionException {
        // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:8: ( T__163 | T__164 | T__165 | STRING | INTEGER | FLOAT | DATETIME | BOOLEAN | NULL | OR | AND | EQUALS | NOTEQUALS | ISNULL | ISNOTNULL | LT | LTEQ | GT | GTEQ | PLUS | MINUS | MULT | DIV | MOD | POW | NOT | SELECT | SHOW | TABLES | SYSTEMSTATUS | DROP | IFEXISTS | TRUNCATE | TABLE | RENAME | DELETE | INSERT | UPDATE | CREATE | EXIT | QUIT | KILL | ALTER | PROCESSLIST | SET | FLUSH | INTO | VALUES | INFOTABLES | REMOTEQUERY | LOADDATAINFILE | OUTFILE | FIELDS | COLUMNS | COLUMN | ADD | ENCLOSEDBY | DELIMITEDBY | TERMINATEDBY | PRIMARYKEY | FOREIGNKEY | SURROGATEKEYS | SKIPFIRST | NOINDEX | FROM | WHERE | ORDERBY | GROUPBY | HAVING | INNERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN | FULLOUTERJOIN | MAXONE | DISTINCTFORKEY | TRANSIENT | ON | IN | NOTIN | NOTLIKE | LIKE | SUM | AVG | COUNT | DISTINCT | MIN | MAX | CASE | WHEN | THEN | ELSE | END | TOP | YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND | VARCHARTYPE | INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | DATETYPE | DATETIMETYPE | TIMESTAMPADD | TIMESTAMPDIFF | EXTRACT | DATEDIFF | DATEADD | DATEPART | GETDATE | SUBSTR | LENGTH | LEN | POSITION | OBJECTSIZE | TOUPPER | TOLOWER | ISNULLEXP | COALESCE | CAST | DEBUG | DUMPTABLE | LOADTABLE | DUMPDDL | LOADDDL | DUMPDB | LOADDB | OUTDIR | INDIR | TO | DISK | MEMORY | COMPRESSED | UNCOMPRESSED | DISKKEYSTORE | COMPRESSEDKEYSTORE | LEFT_PAREN | RIGHT_PAREN | PARAMETER | ASCENDING | DESCENDING | AS | IDENT | WS | LINE_COMMENT | ML_COMMENT )
        int alt21=151;
        alt21 = dfa21.predict(input);
        switch (alt21) {
            case 1 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:10: T__163
                {
                mT__163(); 


                }
                break;
            case 2 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:17: T__164
                {
                mT__164(); 


                }
                break;
            case 3 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:24: T__165
                {
                mT__165(); 


                }
                break;
            case 4 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:31: STRING
                {
                mSTRING(); 


                }
                break;
            case 5 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:38: INTEGER
                {
                mINTEGER(); 


                }
                break;
            case 6 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:46: FLOAT
                {
                mFLOAT(); 


                }
                break;
            case 7 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:52: DATETIME
                {
                mDATETIME(); 


                }
                break;
            case 8 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:61: BOOLEAN
                {
                mBOOLEAN(); 


                }
                break;
            case 9 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:69: NULL
                {
                mNULL(); 


                }
                break;
            case 10 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:74: OR
                {
                mOR(); 


                }
                break;
            case 11 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:77: AND
                {
                mAND(); 


                }
                break;
            case 12 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:81: EQUALS
                {
                mEQUALS(); 


                }
                break;
            case 13 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:88: NOTEQUALS
                {
                mNOTEQUALS(); 


                }
                break;
            case 14 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:98: ISNULL
                {
                mISNULL(); 


                }
                break;
            case 15 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:105: ISNOTNULL
                {
                mISNOTNULL(); 


                }
                break;
            case 16 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:115: LT
                {
                mLT(); 


                }
                break;
            case 17 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:118: LTEQ
                {
                mLTEQ(); 


                }
                break;
            case 18 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:123: GT
                {
                mGT(); 


                }
                break;
            case 19 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:126: GTEQ
                {
                mGTEQ(); 


                }
                break;
            case 20 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:131: PLUS
                {
                mPLUS(); 


                }
                break;
            case 21 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:136: MINUS
                {
                mMINUS(); 


                }
                break;
            case 22 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:142: MULT
                {
                mMULT(); 


                }
                break;
            case 23 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:147: DIV
                {
                mDIV(); 


                }
                break;
            case 24 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:151: MOD
                {
                mMOD(); 


                }
                break;
            case 25 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:155: POW
                {
                mPOW(); 


                }
                break;
            case 26 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:159: NOT
                {
                mNOT(); 


                }
                break;
            case 27 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:163: SELECT
                {
                mSELECT(); 


                }
                break;
            case 28 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:170: SHOW
                {
                mSHOW(); 


                }
                break;
            case 29 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:175: TABLES
                {
                mTABLES(); 


                }
                break;
            case 30 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:182: SYSTEMSTATUS
                {
                mSYSTEMSTATUS(); 


                }
                break;
            case 31 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:195: DROP
                {
                mDROP(); 


                }
                break;
            case 32 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:200: IFEXISTS
                {
                mIFEXISTS(); 


                }
                break;
            case 33 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:209: TRUNCATE
                {
                mTRUNCATE(); 


                }
                break;
            case 34 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:218: TABLE
                {
                mTABLE(); 


                }
                break;
            case 35 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:224: RENAME
                {
                mRENAME(); 


                }
                break;
            case 36 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:231: DELETE
                {
                mDELETE(); 


                }
                break;
            case 37 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:238: INSERT
                {
                mINSERT(); 


                }
                break;
            case 38 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:245: UPDATE
                {
                mUPDATE(); 


                }
                break;
            case 39 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:252: CREATE
                {
                mCREATE(); 


                }
                break;
            case 40 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:259: EXIT
                {
                mEXIT(); 


                }
                break;
            case 41 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:264: QUIT
                {
                mQUIT(); 


                }
                break;
            case 42 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:269: KILL
                {
                mKILL(); 


                }
                break;
            case 43 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:274: ALTER
                {
                mALTER(); 


                }
                break;
            case 44 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:280: PROCESSLIST
                {
                mPROCESSLIST(); 


                }
                break;
            case 45 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:292: SET
                {
                mSET(); 


                }
                break;
            case 46 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:296: FLUSH
                {
                mFLUSH(); 


                }
                break;
            case 47 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:302: INTO
                {
                mINTO(); 


                }
                break;
            case 48 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:307: VALUES
                {
                mVALUES(); 


                }
                break;
            case 49 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:314: INFOTABLES
                {
                mINFOTABLES(); 


                }
                break;
            case 50 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:325: REMOTEQUERY
                {
                mREMOTEQUERY(); 


                }
                break;
            case 51 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:337: LOADDATAINFILE
                {
                mLOADDATAINFILE(); 


                }
                break;
            case 52 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:352: OUTFILE
                {
                mOUTFILE(); 


                }
                break;
            case 53 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:360: FIELDS
                {
                mFIELDS(); 


                }
                break;
            case 54 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:367: COLUMNS
                {
                mCOLUMNS(); 


                }
                break;
            case 55 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:375: COLUMN
                {
                mCOLUMN(); 


                }
                break;
            case 56 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:382: ADD
                {
                mADD(); 


                }
                break;
            case 57 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:386: ENCLOSEDBY
                {
                mENCLOSEDBY(); 


                }
                break;
            case 58 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:397: DELIMITEDBY
                {
                mDELIMITEDBY(); 


                }
                break;
            case 59 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:409: TERMINATEDBY
                {
                mTERMINATEDBY(); 


                }
                break;
            case 60 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:422: PRIMARYKEY
                {
                mPRIMARYKEY(); 


                }
                break;
            case 61 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:433: FOREIGNKEY
                {
                mFOREIGNKEY(); 


                }
                break;
            case 62 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:444: SURROGATEKEYS
                {
                mSURROGATEKEYS(); 


                }
                break;
            case 63 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:458: SKIPFIRST
                {
                mSKIPFIRST(); 


                }
                break;
            case 64 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:468: NOINDEX
                {
                mNOINDEX(); 


                }
                break;
            case 65 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:476: FROM
                {
                mFROM(); 


                }
                break;
            case 66 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:481: WHERE
                {
                mWHERE(); 


                }
                break;
            case 67 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:487: ORDERBY
                {
                mORDERBY(); 


                }
                break;
            case 68 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:495: GROUPBY
                {
                mGROUPBY(); 


                }
                break;
            case 69 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:503: HAVING
                {
                mHAVING(); 


                }
                break;
            case 70 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:510: INNERJOIN
                {
                mINNERJOIN(); 


                }
                break;
            case 71 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:520: LEFTOUTERJOIN
                {
                mLEFTOUTERJOIN(); 


                }
                break;
            case 72 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:534: RIGHTOUTERJOIN
                {
                mRIGHTOUTERJOIN(); 


                }
                break;
            case 73 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:549: FULLOUTERJOIN
                {
                mFULLOUTERJOIN(); 


                }
                break;
            case 74 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:563: MAXONE
                {
                mMAXONE(); 


                }
                break;
            case 75 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:570: DISTINCTFORKEY
                {
                mDISTINCTFORKEY(); 


                }
                break;
            case 76 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:585: TRANSIENT
                {
                mTRANSIENT(); 


                }
                break;
            case 77 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:595: ON
                {
                mON(); 


                }
                break;
            case 78 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:598: IN
                {
                mIN(); 


                }
                break;
            case 79 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:601: NOTIN
                {
                mNOTIN(); 


                }
                break;
            case 80 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:607: NOTLIKE
                {
                mNOTLIKE(); 


                }
                break;
            case 81 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:615: LIKE
                {
                mLIKE(); 


                }
                break;
            case 82 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:620: SUM
                {
                mSUM(); 


                }
                break;
            case 83 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:624: AVG
                {
                mAVG(); 


                }
                break;
            case 84 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:628: COUNT
                {
                mCOUNT(); 


                }
                break;
            case 85 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:634: DISTINCT
                {
                mDISTINCT(); 


                }
                break;
            case 86 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:643: MIN
                {
                mMIN(); 


                }
                break;
            case 87 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:647: MAX
                {
                mMAX(); 


                }
                break;
            case 88 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:651: CASE
                {
                mCASE(); 


                }
                break;
            case 89 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:656: WHEN
                {
                mWHEN(); 


                }
                break;
            case 90 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:661: THEN
                {
                mTHEN(); 


                }
                break;
            case 91 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:666: ELSE
                {
                mELSE(); 


                }
                break;
            case 92 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:671: END
                {
                mEND(); 


                }
                break;
            case 93 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:675: TOP
                {
                mTOP(); 


                }
                break;
            case 94 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:679: YEAR
                {
                mYEAR(); 


                }
                break;
            case 95 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:684: QUARTER
                {
                mQUARTER(); 


                }
                break;
            case 96 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:692: MONTH
                {
                mMONTH(); 


                }
                break;
            case 97 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:698: WEEK
                {
                mWEEK(); 


                }
                break;
            case 98 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:703: DAY
                {
                mDAY(); 


                }
                break;
            case 99 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:707: HOUR
                {
                mHOUR(); 


                }
                break;
            case 100 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:712: MINUTE
                {
                mMINUTE(); 


                }
                break;
            case 101 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:719: SECOND
                {
                mSECOND(); 


                }
                break;
            case 102 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:726: VARCHARTYPE
                {
                mVARCHARTYPE(); 


                }
                break;
            case 103 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:738: INTEGERTYPE
                {
                mINTEGERTYPE(); 


                }
                break;
            case 104 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:750: BIGINTTYPE
                {
                mBIGINTTYPE(); 


                }
                break;
            case 105 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:761: FLOATTYPE
                {
                mFLOATTYPE(); 


                }
                break;
            case 106 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:771: DECIMALTYPE
                {
                mDECIMALTYPE(); 


                }
                break;
            case 107 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:783: DATETYPE
                {
                mDATETYPE(); 


                }
                break;
            case 108 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:792: DATETIMETYPE
                {
                mDATETIMETYPE(); 


                }
                break;
            case 109 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:805: TIMESTAMPADD
                {
                mTIMESTAMPADD(); 


                }
                break;
            case 110 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:818: TIMESTAMPDIFF
                {
                mTIMESTAMPDIFF(); 


                }
                break;
            case 111 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:832: EXTRACT
                {
                mEXTRACT(); 


                }
                break;
            case 112 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:840: DATEDIFF
                {
                mDATEDIFF(); 


                }
                break;
            case 113 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:849: DATEADD
                {
                mDATEADD(); 


                }
                break;
            case 114 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:857: DATEPART
                {
                mDATEPART(); 


                }
                break;
            case 115 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:866: GETDATE
                {
                mGETDATE(); 


                }
                break;
            case 116 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:874: SUBSTR
                {
                mSUBSTR(); 


                }
                break;
            case 117 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:881: LENGTH
                {
                mLENGTH(); 


                }
                break;
            case 118 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:888: LEN
                {
                mLEN(); 


                }
                break;
            case 119 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:892: POSITION
                {
                mPOSITION(); 


                }
                break;
            case 120 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:901: OBJECTSIZE
                {
                mOBJECTSIZE(); 


                }
                break;
            case 121 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:912: TOUPPER
                {
                mTOUPPER(); 


                }
                break;
            case 122 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:920: TOLOWER
                {
                mTOLOWER(); 


                }
                break;
            case 123 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:928: ISNULLEXP
                {
                mISNULLEXP(); 


                }
                break;
            case 124 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:938: COALESCE
                {
                mCOALESCE(); 


                }
                break;
            case 125 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:947: CAST
                {
                mCAST(); 


                }
                break;
            case 126 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:952: DEBUG
                {
                mDEBUG(); 


                }
                break;
            case 127 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:958: DUMPTABLE
                {
                mDUMPTABLE(); 


                }
                break;
            case 128 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:968: LOADTABLE
                {
                mLOADTABLE(); 


                }
                break;
            case 129 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:978: DUMPDDL
                {
                mDUMPDDL(); 


                }
                break;
            case 130 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:986: LOADDDL
                {
                mLOADDDL(); 


                }
                break;
            case 131 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:994: DUMPDB
                {
                mDUMPDB(); 


                }
                break;
            case 132 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1001: LOADDB
                {
                mLOADDB(); 


                }
                break;
            case 133 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1008: OUTDIR
                {
                mOUTDIR(); 


                }
                break;
            case 134 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1015: INDIR
                {
                mINDIR(); 


                }
                break;
            case 135 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1021: TO
                {
                mTO(); 


                }
                break;
            case 136 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1024: DISK
                {
                mDISK(); 


                }
                break;
            case 137 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1029: MEMORY
                {
                mMEMORY(); 


                }
                break;
            case 138 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1036: COMPRESSED
                {
                mCOMPRESSED(); 


                }
                break;
            case 139 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1047: UNCOMPRESSED
                {
                mUNCOMPRESSED(); 


                }
                break;
            case 140 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1060: DISKKEYSTORE
                {
                mDISKKEYSTORE(); 


                }
                break;
            case 141 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1073: COMPRESSEDKEYSTORE
                {
                mCOMPRESSEDKEYSTORE(); 


                }
                break;
            case 142 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1092: LEFT_PAREN
                {
                mLEFT_PAREN(); 


                }
                break;
            case 143 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1103: RIGHT_PAREN
                {
                mRIGHT_PAREN(); 


                }
                break;
            case 144 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1115: PARAMETER
                {
                mPARAMETER(); 


                }
                break;
            case 145 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1125: ASCENDING
                {
                mASCENDING(); 


                }
                break;
            case 146 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1135: DESCENDING
                {
                mDESCENDING(); 


                }
                break;
            case 147 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1146: AS
                {
                mAS(); 


                }
                break;
            case 148 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1149: IDENT
                {
                mIDENT(); 


                }
                break;
            case 149 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1155: WS
                {
                mWS(); 


                }
                break;
            case 150 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1158: LINE_COMMENT
                {
                mLINE_COMMENT(); 


                }
                break;
            case 151 :
                // C:\\workspace\\MemDB\\prosrc\\memdb\\sql\\Memgrammar.g:1:1171: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;

        }

    }


    protected DFA21 dfa21 = new DFA21(this);
    static final String DFA21_eotS =
        "\2\uffff\1\60\2\uffff\1\62\1\63\1\uffff\3\56\1\uffff\1\56\1\uffff"+
        "\1\56\1\uffff\1\113\1\115\1\56\1\123\2\uffff\1\126\2\uffff\22\56"+
        "\11\uffff\6\56\1\u0089\6\56\1\13\1\56\1\u0093\5\56\1\u009a\4\uffff"+
        "\2\56\2\u00a3\5\uffff\50\56\1\7\1\113\7\56\1\u00ea\1\uffff\11\56"+
        "\1\uffff\1\56\1\15\1\56\1\u00f7\1\u00f8\1\u00fa\1\uffff\1\56\2\uffff"+
        "\5\56\1\uffff\1\56\1\u0104\4\56\1\u0109\10\56\1\u0114\21\56\1\u0127"+
        "\16\56\1\u0137\6\56\1\u0140\1\u0142\4\56\1\uffff\1\56\1\u014a\1"+
        "\u014b\4\56\1\u0150\1\uffff\5\56\1\u0156\6\56\2\uffff\1\56\1\uffff"+
        "\1\56\1\uffff\1\56\1\u0162\5\56\1\uffff\1\56\1\u0169\2\56\1\uffff"+
        "\2\56\1\u016e\4\56\1\u0174\1\56\1\u0177\1\uffff\1\u017c\14\56\1"+
        "\u018a\1\u018b\1\u018c\2\56\1\uffff\1\u018f\1\u0190\1\56\1\u0192"+
        "\10\56\1\u019d\2\56\1\uffff\1\56\1\u01a1\1\u01a2\3\56\1\u01a6\1"+
        "\56\1\uffff\1\56\1\uffff\2\56\1\u01ab\1\56\2\uffff\1\56\2\uffff"+
        "\2\56\1\u01b1\1\56\1\uffff\1\56\1\u014b\1\u01b4\1\u01b5\1\56\2\uffff"+
        "\4\56\1\u01bb\2\56\2\uffff\1\56\1\uffff\3\56\1\u01c2\2\56\1\uffff"+
        "\4\56\1\uffff\3\56\1\u01cc\1\56\1\uffff\2\56\1\uffff\4\56\1\uffff"+
        "\6\56\1\u01db\3\56\1\u01df\2\56\3\uffff\2\56\2\uffff\1\56\1\uffff"+
        "\7\56\1\uffff\1\u01ed\1\u01ee\2\uffff\1\56\1\u01f0\2\uffff\3\56"+
        "\1\uffff\2\56\1\u01f6\1\56\1\uffff\4\56\1\u01fc\1\uffff\2\56\2\uffff"+
        "\1\56\1\uffff\1\56\1\u0201\1\56\1\uffff\1\56\1\u0204\1\u0205\2\56"+
        "\2\uffff\1\u0208\1\u0209\4\56\1\u020e\2\56\1\uffff\11\56\1\u021a"+
        "\1\u021b\1\56\1\uffff\1\u021d\1\uffff\1\56\1\u021f\1\u0221\1\uffff"+
        "\10\56\1\u022a\3\56\1\u022e\2\uffff\1\u022f\2\uffff\1\56\1\u0231"+
        "\1\u0232\1\u0233\1\uffff\1\u0234\1\u0235\1\u0236\2\56\1\uffff\3"+
        "\56\1\u023c\1\uffff\2\56\2\uffff\1\u023f\1\56\3\uffff\3\56\1\uffff"+
        "\1\56\1\u0245\5\56\1\u024b\2\56\1\u024e\2\uffff\1\56\1\uffff\1\56"+
        "\1\uffff\1\u0251\1\uffff\2\56\1\u0254\1\56\1\u0256\3\56\1\uffff"+
        "\1\u025a\1\56\1\u025c\2\uffff\1\u025d\6\uffff\1\u025e\3\56\2\uffff"+
        "\2\56\1\uffff\5\56\1\uffff\1\56\1\u026b\1\56\1\u026d\1\u026e\1\uffff"+
        "\1\u026f\1\56\1\uffff\2\56\1\uffff\1\u0273\1\56\1\uffff\1\56\1\uffff"+
        "\1\56\1\uffff\1\u0277\1\uffff\1\56\3\uffff\1\u0279\3\56\1\u00fa"+
        "\2\56\1\u0280\1\u0281\3\56\1\uffff\1\56\3\uffff\1\u0286\2\56\1\uffff"+
        "\1\56\1\uffff\1\56\1\uffff\1\u028b\1\uffff\3\56\1\u028f\1\56\4\uffff"+
        "\1\u0174\2\56\1\uffff\2\56\1\u0296\1\56\2\uffff\2\56\1\uffff\3\56"+
        "\1\u029d\2\56\1\uffff\1\u02a0\1\u02a1\3\56\1\u02a5\1\uffff\1\u02a6"+
        "\1\56\2\uffff\1\u02a8\2\56\2\uffff\1\56\1\uffff\1\56\1\u02ad\2\56"+
        "\1\uffff\6\56\1\u02b6\2\uffff";
    static final String DFA21_eofS =
        "\u02b7\uffff";
    static final String DFA21_minS =
        "\1\11\1\uffff\1\60\2\uffff\2\56\1\uffff\1\117\2\101\1\uffff\1\102"+
        "\1\uffff\1\104\1\uffff\2\75\1\106\1\75\2\uffff\1\52\2\uffff\1\105"+
        "\1\101\1\105\1\116\1\101\1\114\1\125\1\111\1\117\1\101\1\106\3\105"+
        "\2\101\1\105\1\111\11\uffff\1\111\1\114\1\101\1\102\1\122\1\105"+
        "\1\43\1\115\1\114\1\117\1\122\1\117\1\114\1\43\1\124\1\43\1\112"+
        "\1\104\1\124\1\104\1\107\1\43\4\uffff\2\11\2\43\5\uffff\1\103\1"+
        "\117\1\123\1\102\1\111\1\117\1\102\1\123\1\124\2\115\1\107\1\104"+
        "\1\103\1\105\1\101\1\123\1\111\1\103\1\123\1\101\1\114\1\111\1\123"+
        "\1\114\1\101\1\113\1\106\2\105\1\117\1\124\1\126\1\125\1\130\2\116"+
        "\1\115\1\101\1\107\1\43\1\11\1\116\1\114\1\105\1\116\1\114\1\115"+
        "\1\116\1\43\1\uffff\1\105\2\123\1\101\1\105\1\115\1\114\1\105\1"+
        "\104\1\uffff\1\105\1\43\1\105\3\43\1\uffff\1\125\1\116\1\uffff\2"+
        "\105\1\117\1\105\1\111\1\uffff\1\105\1\43\1\117\1\127\1\124\1\122"+
        "\1\43\1\123\2\120\1\105\1\111\1\125\1\103\1\113\1\43\1\105\1\120"+
        "\1\101\1\117\1\110\1\101\1\105\1\117\1\101\1\125\1\116\1\114\1\120"+
        "\1\105\1\124\1\122\1\114\1\43\1\105\1\124\1\122\1\114\1\103\1\115"+
        "\1\111\1\125\1\103\1\104\3\105\1\124\1\43\1\116\1\113\1\125\1\104"+
        "\1\111\1\122\2\43\1\124\1\117\1\122\2\111\1\104\2\43\1\103\1\123"+
        "\1\105\1\111\1\43\1\uffff\1\123\1\105\1\110\1\124\1\111\1\43\1\11"+
        "\1\122\2\111\1\103\1\122\2\uffff\1\116\1\uffff\1\114\1\117\1\122"+
        "\1\43\1\107\3\122\1\103\1\uffff\1\116\1\43\1\105\1\117\1\uffff\1"+
        "\124\1\106\1\43\1\124\2\115\1\107\1\43\1\111\1\43\1\uffff\1\43\1"+
        "\104\1\115\3\124\1\122\1\115\1\124\1\115\1\124\1\105\1\122\3\43"+
        "\1\101\1\117\1\uffff\2\43\1\124\1\43\1\105\1\101\1\124\1\105\1\110"+
        "\1\11\1\122\1\123\1\43\1\11\1\124\1\uffff\1\105\2\43\1\120\1\101"+
        "\1\116\1\43\1\116\1\uffff\1\124\1\uffff\1\110\1\122\1\43\1\116\2"+
        "\uffff\1\105\2\uffff\1\101\1\111\1\43\1\116\1\uffff\1\124\3\43\1"+
        "\107\2\uffff\1\11\1\114\1\122\1\124\1\43\1\104\1\114\2\uffff\1\124"+
        "\1\uffff\1\105\1\115\1\11\1\43\1\124\1\104\1\uffff\1\115\1\107\1"+
        "\122\1\111\1\uffff\1\105\1\111\1\101\1\43\1\116\1\uffff\1\116\1"+
        "\105\1\uffff\2\111\1\104\1\101\1\uffff\1\101\1\102\2\105\1\11\1"+
        "\105\1\43\1\120\1\105\1\116\1\43\1\123\1\105\3\uffff\1\103\1\123"+
        "\2\uffff\1\105\1\uffff\1\123\1\122\1\111\1\123\2\101\1\102\1\uffff"+
        "\2\43\2\uffff\1\110\1\43\2\uffff\1\11\1\124\1\107\1\uffff\2\105"+
        "\1\43\1\131\1\uffff\1\124\1\130\1\124\1\105\1\43\1\uffff\2\101\2"+
        "\uffff\1\116\1\uffff\1\105\1\43\1\123\1\uffff\1\111\2\43\1\122\1"+
        "\101\2\uffff\2\43\1\11\1\101\1\111\1\122\1\43\1\124\1\114\1\uffff"+
        "\1\104\1\103\1\131\1\115\1\106\1\104\1\122\1\102\1\114\2\43\1\121"+
        "\1\uffff\1\43\1\uffff\1\122\2\43\1\uffff\1\103\1\123\1\124\1\105"+
        "\1\122\1\123\1\131\1\117\1\43\1\122\1\102\1\114\1\43\2\uffff\1\43"+
        "\2\uffff\1\105\3\43\1\uffff\3\43\1\105\1\116\1\uffff\1\124\1\115"+
        "\1\11\1\43\1\uffff\1\111\1\116\2\uffff\1\43\1\124\3\uffff\1\124"+
        "\1\116\1\123\1\uffff\1\105\1\43\1\111\1\124\1\123\1\105\1\106\1"+
        "\43\1\124\1\114\1\43\2\uffff\1\125\1\uffff\1\105\1\uffff\1\43\1"+
        "\uffff\1\105\1\123\1\43\1\104\1\43\1\114\1\11\1\116\1\uffff\1\43"+
        "\1\114\1\43\2\uffff\1\43\6\uffff\1\43\1\124\1\105\1\120\2\uffff"+
        "\1\132\1\107\1\uffff\1\111\1\105\1\107\1\124\1\104\1\uffff\1\116"+
        "\1\43\1\124\2\43\1\uffff\1\43\1\105\1\uffff\1\105\1\123\1\uffff"+
        "\1\43\1\105\1\uffff\1\11\1\uffff\1\111\1\uffff\1\43\1\uffff\1\105"+
        "\3\uffff\1\43\1\104\1\101\1\105\1\43\1\117\1\11\2\43\1\11\1\107"+
        "\1\117\1\uffff\1\117\3\uffff\1\43\1\122\1\123\1\uffff\1\104\1\uffff"+
        "\1\123\1\uffff\1\43\1\uffff\1\11\1\104\1\111\1\43\1\116\4\uffff"+
        "\1\43\2\122\1\uffff\1\131\1\105\1\43\1\124\2\uffff\1\104\1\106\1"+
        "\uffff\1\137\1\113\1\105\1\43\1\104\1\105\1\uffff\2\43\1\106\1\123"+
        "\1\105\1\43\1\uffff\1\43\1\131\2\uffff\1\43\1\103\1\131\2\uffff"+
        "\1\123\1\uffff\1\110\1\43\1\124\1\105\1\uffff\1\117\1\115\1\122"+
        "\1\101\1\105\1\56\1\43\2\uffff";
    static final String DFA21_maxS =
        "\1\174\1\uffff\1\71\2\uffff\2\71\1\uffff\1\165\1\162\1\165\1\uffff"+
        "\1\165\1\uffff\1\166\1\uffff\1\75\1\76\1\163\1\75\2\uffff\1\57\2"+
        "\uffff\1\171\1\165\1\151\1\160\1\162\1\170\1\165\1\151\1\162\1\141"+
        "\1\163\1\157\1\150\1\162\2\157\1\145\1\151\11\uffff\1\167\1\154"+
        "\1\165\1\142\1\162\1\145\1\172\1\155\1\154\1\165\1\162\1\157\1\154"+
        "\1\172\1\164\1\172\1\152\1\144\1\164\1\144\1\147\1\172\4\uffff\1"+
        "\156\1\40\2\172\5\uffff\1\164\1\157\1\163\1\162\1\151\1\157\2\163"+
        "\1\171\1\155\1\156\1\147\1\160\1\143\1\145\1\165\1\163\1\164\1\144"+
        "\1\163\1\151\1\154\1\157\1\163\1\162\1\167\2\156\2\145\1\157\1\164"+
        "\1\166\1\165\1\170\2\156\1\155\1\141\1\147\2\172\1\156\1\154\2\156"+
        "\1\154\1\155\1\156\1\172\1\uffff\1\145\2\163\1\141\1\145\1\155\1"+
        "\154\1\145\1\146\1\uffff\1\145\1\172\1\145\3\172\1\uffff\1\165\1"+
        "\156\1\uffff\1\145\1\157\1\117\1\145\1\151\1\uffff\1\145\1\172\1"+
        "\157\1\167\1\164\1\162\1\172\1\163\2\160\2\151\1\165\1\143\1\164"+
        "\1\172\1\145\1\160\1\141\1\157\1\150\1\141\1\145\1\157\1\141\1\165"+
        "\1\156\1\154\1\160\2\164\1\162\1\154\1\172\1\145\1\164\1\162\1\154"+
        "\1\143\1\155\1\151\1\165\1\143\1\144\3\145\1\164\1\172\1\162\1\153"+
        "\1\165\1\144\1\151\1\162\2\172\1\164\1\157\1\162\1\151\1\154\1\144"+
        "\2\172\1\143\1\163\1\145\1\151\1\172\1\uffff\1\163\1\145\1\150\1"+
        "\164\1\151\1\172\1\40\1\162\2\151\1\143\1\162\2\uffff\1\156\1\uffff"+
        "\1\154\1\165\1\162\1\172\1\147\1\122\2\162\1\143\1\uffff\1\156\1"+
        "\172\1\145\1\157\1\uffff\1\164\1\146\1\172\1\164\2\155\1\147\1\172"+
        "\1\151\1\172\1\uffff\1\172\1\164\1\155\3\164\1\162\1\155\1\164\1"+
        "\155\1\164\1\145\1\162\3\172\1\141\1\157\1\uffff\2\172\1\164\1\172"+
        "\1\145\1\141\1\164\1\145\1\150\1\164\1\162\1\163\1\172\1\40\1\164"+
        "\1\uffff\1\145\2\172\1\160\1\141\1\156\1\172\1\156\1\uffff\1\164"+
        "\1\uffff\1\150\1\162\1\172\1\156\2\uffff\1\145\2\uffff\1\141\1\151"+
        "\1\172\1\156\1\uffff\1\164\3\172\1\147\2\uffff\1\40\1\154\1\162"+
        "\1\164\1\172\1\144\1\154\2\uffff\1\164\1\uffff\1\145\1\115\1\40"+
        "\1\172\1\164\1\144\1\uffff\1\155\1\147\1\162\1\151\1\uffff\1\145"+
        "\1\151\1\141\1\172\1\156\1\uffff\1\156\1\145\1\uffff\2\151\1\144"+
        "\1\141\1\uffff\1\141\1\144\2\145\1\40\1\145\1\172\1\160\1\145\1"+
        "\156\1\172\1\163\1\145\3\uffff\1\143\1\163\2\uffff\1\145\1\uffff"+
        "\1\163\1\162\1\151\1\163\2\141\1\144\1\uffff\2\172\2\uffff\1\150"+
        "\1\172\2\uffff\1\40\1\164\1\147\1\uffff\2\145\1\172\1\171\1\uffff"+
        "\1\164\1\170\1\164\1\145\1\172\1\uffff\2\141\2\uffff\1\156\1\uffff"+
        "\1\145\1\172\1\163\1\uffff\1\151\2\172\1\162\1\101\2\uffff\2\172"+
        "\1\40\1\141\1\151\1\162\1\172\1\164\1\154\1\uffff\1\144\1\143\1"+
        "\171\1\155\1\146\1\144\1\162\1\142\1\154\2\172\1\161\1\uffff\1\172"+
        "\1\uffff\1\162\2\172\1\uffff\1\143\1\163\1\164\1\145\1\162\1\163"+
        "\1\171\1\157\1\172\1\162\1\142\1\154\1\172\2\uffff\1\172\2\uffff"+
        "\1\145\3\172\1\uffff\3\172\1\145\1\156\1\uffff\1\164\1\155\1\40"+
        "\1\172\1\uffff\1\151\1\156\2\uffff\1\172\1\124\3\uffff\1\164\1\156"+
        "\1\163\1\uffff\1\145\1\172\1\151\1\164\1\163\1\145\1\146\1\172\1"+
        "\164\1\154\1\172\2\uffff\1\165\1\uffff\1\145\1\uffff\1\172\1\uffff"+
        "\1\145\1\163\1\172\1\144\1\172\1\154\1\40\1\156\1\uffff\1\172\1"+
        "\154\1\172\2\uffff\1\172\6\uffff\1\172\1\164\1\145\1\160\2\uffff"+
        "\1\172\1\147\1\uffff\1\111\1\145\1\147\1\164\1\144\1\uffff\1\156"+
        "\1\172\1\164\2\172\1\uffff\1\172\1\145\1\uffff\1\145\1\163\1\uffff"+
        "\1\172\1\145\1\uffff\1\40\1\uffff\1\151\1\uffff\1\172\1\uffff\1"+
        "\145\3\uffff\1\172\2\144\1\145\1\172\1\117\1\40\2\172\1\40\1\147"+
        "\1\157\1\uffff\1\157\3\uffff\1\172\1\162\1\163\1\uffff\1\144\1\uffff"+
        "\1\163\1\uffff\1\172\1\uffff\1\40\1\144\1\151\1\172\1\116\4\uffff"+
        "\1\172\2\162\1\uffff\1\171\1\145\1\172\1\164\2\uffff\1\144\1\146"+
        "\1\uffff\1\137\1\153\1\145\1\172\1\144\1\145\1\uffff\2\172\1\146"+
        "\1\123\1\145\1\172\1\uffff\1\172\1\171\2\uffff\1\172\1\103\1\171"+
        "\2\uffff\1\163\1\uffff\1\110\1\172\1\164\1\105\1\uffff\1\157\1\115"+
        "\1\162\1\101\1\145\1\56\1\172\2\uffff";
    static final String DFA21_acceptS =
        "\1\uffff\1\1\1\uffff\1\3\1\4\2\uffff\1\7\3\uffff\1\12\1\uffff\1"+
        "\13\1\uffff\1\14\4\uffff\1\24\1\26\1\uffff\1\30\1\31\22\uffff\1"+
        "\u008e\1\u008f\1\u0090\1\u0094\1\u0095\1\2\1\6\1\25\1\5\26\uffff"+
        "\1\15\1\32\1\21\1\20\4\uffff\1\23\1\22\1\u0096\1\u0097\1\27\62\uffff"+
        "\1\u0087\11\uffff\1\115\6\uffff\1\u0093\2\uffff\1\40\5\uffff\1\116"+
        "\106\uffff\1\135\14\uffff\1\70\1\123\1\uffff\1\u0091\11\uffff\1"+
        "\55\4\uffff\1\122\12\uffff\1\142\22\uffff\1\134\17\uffff\1\166\10"+
        "\uffff\1\127\1\uffff\1\126\4\uffff\1\117\1\120\1\uffff\1\11\1\10"+
        "\4\uffff\1\132\5\uffff\1\101\1\111\7\uffff\1\16\1\17\1\uffff\1\57"+
        "\6\uffff\1\34\4\uffff\1\37\5\uffff\1\u0092\2\uffff\1\u0088\4\uffff"+
        "\1\153\15\uffff\1\130\1\175\1\50\2\uffff\1\133\1\51\1\uffff\1\52"+
        "\7\uffff\1\63\2\uffff\1\121\1\107\2\uffff\1\131\1\141\3\uffff\1"+
        "\143\4\uffff\1\136\5\uffff\1\42\2\uffff\1\56\1\151\1\uffff\1\103"+
        "\3\uffff\1\53\5\uffff\1\106\1\u0086\11\uffff\1\176\14\uffff\1\110"+
        "\1\uffff\1\171\3\uffff\1\124\15\uffff\1\172\1\65\1\uffff\1\102\1"+
        "\104\4\uffff\1\140\5\uffff\1\35\4\uffff\1\u0085\2\uffff\1\173\1"+
        "\45\2\uffff\1\33\1\145\1\36\3\uffff\1\44\13\uffff\1\u0083\1\43\1"+
        "\uffff\1\46\1\uffff\1\47\1\uffff\1\67\10\uffff\1\60\3\uffff\1\u0084"+
        "\1\165\1\uffff\1\105\1\112\1\144\1\u0089\1\150\1\100\4\uffff\1\75"+
        "\1\64\2\uffff\1\147\5\uffff\1\152\5\uffff\1\161\2\uffff\1\u0081"+
        "\2\uffff\1\66\2\uffff\1\157\1\uffff\1\137\1\uffff\1\74\1\uffff\1"+
        "\146\1\uffff\1\u0082\1\163\1\41\14\uffff\1\125\1\uffff\1\154\1\160"+
        "\1\162\3\uffff\1\174\1\uffff\1\71\1\uffff\1\167\1\uffff\1\114\5"+
        "\uffff\1\76\1\164\1\77\1\72\3\uffff\1\177\4\uffff\1\u0080\1\73\2"+
        "\uffff\1\170\6\uffff\1\u008a\6\uffff\1\62\2\uffff\1\54\1\155\3\uffff"+
        "\1\u008c\1\u008b\1\uffff\1\156\4\uffff\1\113\7\uffff\1\61\1\u008d";
    static final String DFA21_specialS =
        "\u02b7\uffff}>";
    static final String[] DFA21_transitionS = {
            "\2\57\1\uffff\2\57\22\uffff\1\57\1\20\1\uffff\1\7\1\uffff\1"+
            "\27\1\15\1\4\1\53\1\54\1\25\1\24\1\1\1\5\1\2\1\26\12\6\1\uffff"+
            "\1\3\1\21\1\17\1\23\1\55\1\uffff\1\16\1\52\1\35\1\32\1\36\1"+
            "\12\1\46\1\47\1\22\1\56\1\40\1\44\1\50\1\10\1\14\1\41\1\37\1"+
            "\33\1\31\1\11\1\34\1\42\1\45\1\56\1\51\1\56\3\uffff\1\30\1\56"+
            "\1\uffff\1\16\1\52\1\35\1\32\1\36\1\12\1\46\1\47\1\43\1\56\1"+
            "\40\1\44\1\50\1\10\1\14\1\41\1\37\1\33\1\31\1\11\1\34\1\42\1"+
            "\45\1\56\1\51\1\56\1\uffff\1\13",
            "",
            "\12\61",
            "",
            "",
            "\1\61\1\uffff\12\6",
            "\1\61\1\uffff\12\6",
            "",
            "\1\64\5\uffff\1\65\31\uffff\1\64\5\uffff\1\65",
            "\1\67\3\uffff\1\70\2\uffff\1\71\1\73\5\uffff\1\72\2\uffff\1"+
            "\66\16\uffff\1\67\3\uffff\1\70\2\uffff\1\71\1\73\5\uffff\1\72"+
            "\2\uffff\1\66",
            "\1\74\12\uffff\1\75\2\uffff\1\76\2\uffff\1\77\2\uffff\1\100"+
            "\13\uffff\1\74\12\uffff\1\75\2\uffff\1\76\2\uffff\1\77\2\uffff"+
            "\1\100",
            "",
            "\1\104\13\uffff\1\103\3\uffff\1\101\2\uffff\1\102\14\uffff"+
            "\1\104\13\uffff\1\103\3\uffff\1\101\2\uffff\1\102",
            "",
            "\1\107\7\uffff\1\106\1\uffff\1\105\4\uffff\1\111\2\uffff\1"+
            "\110\15\uffff\1\107\7\uffff\1\106\1\uffff\1\105\4\uffff\1\111"+
            "\2\uffff\1\110",
            "",
            "\1\112",
            "\1\114\1\112",
            "\1\117\7\uffff\1\120\4\uffff\1\116\22\uffff\1\117\7\uffff\1"+
            "\121\4\uffff\1\116",
            "\1\122",
            "",
            "",
            "\1\125\4\uffff\1\124",
            "",
            "",
            "\1\127\2\uffff\1\130\2\uffff\1\133\11\uffff\1\132\3\uffff\1"+
            "\131\13\uffff\1\127\2\uffff\1\130\2\uffff\1\133\11\uffff\1\132"+
            "\3\uffff\1\131",
            "\1\137\3\uffff\1\135\3\uffff\1\136\10\uffff\1\134\2\uffff\1"+
            "\140\13\uffff\1\137\3\uffff\1\135\3\uffff\1\136\10\uffff\1\134"+
            "\2\uffff\1\140",
            "\1\141\3\uffff\1\142\33\uffff\1\141\3\uffff\1\142",
            "\1\144\1\uffff\1\143\35\uffff\1\144\1\uffff\1\143",
            "\1\147\15\uffff\1\146\2\uffff\1\145\16\uffff\1\147\15\uffff"+
            "\1\146\2\uffff\1\145",
            "\1\152\1\uffff\1\151\11\uffff\1\150\23\uffff\1\152\1\uffff"+
            "\1\151\11\uffff\1\150",
            "\1\153\37\uffff\1\153",
            "\1\154\37\uffff\1\154",
            "\1\156\2\uffff\1\155\34\uffff\1\156\2\uffff\1\155",
            "\1\157\37\uffff\1\157",
            "\1\117\7\uffff\1\121\4\uffff\1\116\22\uffff\1\117\7\uffff\1"+
            "\121\4\uffff\1\116",
            "\1\162\3\uffff\1\161\5\uffff\1\160\25\uffff\1\162\3\uffff\1"+
            "\161\5\uffff\1\160",
            "\1\164\2\uffff\1\163\34\uffff\1\164\2\uffff\1\163",
            "\1\166\14\uffff\1\165\22\uffff\1\166\14\uffff\1\165",
            "\1\167\15\uffff\1\170\21\uffff\1\167\15\uffff\1\170",
            "\1\171\3\uffff\1\174\3\uffff\1\172\5\uffff\1\173\21\uffff\1"+
            "\171\3\uffff\1\174\3\uffff\1\172\5\uffff\1\173",
            "\1\175\37\uffff\1\175",
            "\1\176\37\uffff\1\176",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0081\12\uffff\1\u0080\2\uffff\1\177\21\uffff\1\u0081\12"+
            "\uffff\1\u0080\2\uffff\1\177",
            "\1\u0082\37\uffff\1\u0082",
            "\1\u0084\23\uffff\1\u0083\13\uffff\1\u0084\23\uffff\1\u0083",
            "\1\u0085\37\uffff\1\u0085",
            "\1\u0086\37\uffff\1\u0086",
            "\1\u0087\37\uffff\1\u0087",
            "\2\56\13\uffff\13\56\6\uffff\17\56\1\u0088\12\56\4\uffff\1"+
            "\56\1\uffff\17\56\1\u0088\12\56",
            "\1\u008a\37\uffff\1\u008a",
            "\1\u008b\37\uffff\1\u008b",
            "\1\u008d\5\uffff\1\u008c\31\uffff\1\u008d\5\uffff\1\u008c",
            "\1\u008e\37\uffff\1\u008e",
            "\1\u008f\37\uffff\1\u008f",
            "\1\u0090\37\uffff\1\u0090",
            "\2\56\13\uffff\13\56\6\uffff\3\56\1\u0091\26\56\4\uffff\1\56"+
            "\1\uffff\3\56\1\u0091\26\56",
            "\1\u0092\37\uffff\1\u0092",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0094\37\uffff\1\u0094",
            "\1\u0095\37\uffff\1\u0095",
            "\1\u0096\37\uffff\1\u0096",
            "\1\u0097\37\uffff\1\u0097",
            "\1\u0098\37\uffff\1\u0098",
            "\2\56\13\uffff\13\56\6\uffff\2\56\1\u0099\27\56\4\uffff\1\56"+
            "\1\uffff\2\56\1\u0099\27\56",
            "",
            "",
            "",
            "",
            "\2\u009c\1\uffff\2\u009c\22\uffff\1\u009c\55\uffff\1\u009b"+
            "\37\uffff\1\u009b",
            "\2\u009d\1\uffff\2\u009d\22\uffff\1\u009d",
            "\2\56\13\uffff\13\56\6\uffff\3\56\1\u00a2\1\56\1\u00a0\7\56"+
            "\1\u00a1\4\56\1\u009e\1\u009f\6\56\4\uffff\1\56\1\uffff\3\56"+
            "\1\u00a2\11\56\1\u00a1\4\56\1\u009e\1\u009f\6\56",
            "\2\56\13\uffff\13\56\6\uffff\3\56\1\u00a2\11\56\1\u00a1\4\56"+
            "\1\u009e\1\u009f\6\56\4\uffff\1\56\1\uffff\3\56\1\u00a2\11\56"+
            "\1\u00a1\4\56\1\u009e\1\u009f\6\56",
            "",
            "",
            "",
            "",
            "",
            "\1\u00a6\10\uffff\1\u00a4\7\uffff\1\u00a5\16\uffff\1\u00a6"+
            "\10\uffff\1\u00a4\7\uffff\1\u00a5",
            "\1\u00a7\37\uffff\1\u00a7",
            "\1\u00a8\37\uffff\1\u00a8",
            "\1\u00ab\12\uffff\1\u00aa\4\uffff\1\u00a9\17\uffff\1\u00ab"+
            "\12\uffff\1\u00aa\4\uffff\1\u00a9",
            "\1\u00ac\37\uffff\1\u00ac",
            "\1\u00ad\37\uffff\1\u00ad",
            "\1\u00b0\1\u00af\10\uffff\1\u00ae\6\uffff\1\u00b1\16\uffff"+
            "\1\u00b0\1\u00af\10\uffff\1\u00ae\6\uffff\1\u00b1",
            "\1\u00b2\37\uffff\1\u00b2",
            "\1\u00b4\4\uffff\1\u00b3\32\uffff\1\u00b4\4\uffff\1\u00b3",
            "\1\u00b5\37\uffff\1\u00b5",
            "\1\u00b7\1\u00b6\36\uffff\1\u00b7\1\u00b6",
            "\1\u00b8\37\uffff\1\u00b8",
            "\1\u00b9\13\uffff\1\u00ba\23\uffff\1\u00b9\13\uffff\1\u00ba",
            "\1\u00bb\37\uffff\1\u00bb",
            "\1\u00bc\37\uffff\1\u00bc",
            "\1\u00bf\12\uffff\1\u00bd\1\u00c0\7\uffff\1\u00be\13\uffff"+
            "\1\u00bf\12\uffff\1\u00bd\1\u00c0\7\uffff\1\u00be",
            "\1\u00c1\37\uffff\1\u00c1",
            "\1\u00c2\12\uffff\1\u00c3\24\uffff\1\u00c2\12\uffff\1\u00c3",
            "\1\u00c4\1\u00c5\36\uffff\1\u00c4\1\u00c5",
            "\1\u00c6\37\uffff\1\u00c6",
            "\1\u00c8\7\uffff\1\u00c7\27\uffff\1\u00c8\7\uffff\1\u00c7",
            "\1\u00c9\37\uffff\1\u00c9",
            "\1\u00cb\5\uffff\1\u00ca\31\uffff\1\u00cb\5\uffff\1\u00ca",
            "\1\u00cc\37\uffff\1\u00cc",
            "\1\u00cd\5\uffff\1\u00ce\31\uffff\1\u00cd\5\uffff\1\u00ce",
            "\1\u00cf\25\uffff\1\u00d0\11\uffff\1\u00cf\25\uffff\1\u00d0",
            "\1\u00d2\2\uffff\1\u00d1\34\uffff\1\u00d2\2\uffff\1\u00d1",
            "\1\u00d3\7\uffff\1\u00d4\27\uffff\1\u00d3\7\uffff\1\u00d4",
            "\1\u00d5\37\uffff\1\u00d5",
            "\1\u00d6\37\uffff\1\u00d6",
            "\1\u00d7\37\uffff\1\u00d7",
            "\1\u00d8\37\uffff\1\u00d8",
            "\1\u00d9\37\uffff\1\u00d9",
            "\1\u00da\37\uffff\1\u00da",
            "\1\u00db\37\uffff\1\u00db",
            "\1\u00dc\37\uffff\1\u00dc",
            "\1\u00dd\37\uffff\1\u00dd",
            "\1\u00de\37\uffff\1\u00de",
            "\1\u00df\37\uffff\1\u00df",
            "\1\u00e0\37\uffff\1\u00e0",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\u00e1\1\uffff\2\u00e1\22\uffff\1\u00e1\2\uffff\2\56\13\uffff"+
            "\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00e2\37\uffff\1\u00e2",
            "\1\u00e3\37\uffff\1\u00e3",
            "\1\u00e4\10\uffff\1\u00e5\26\uffff\1\u00e4\10\uffff\1\u00e5",
            "\1\u00e6\37\uffff\1\u00e6",
            "\1\u00e7\37\uffff\1\u00e7",
            "\1\u00e8\37\uffff\1\u00e8",
            "\1\u00e9\37\uffff\1\u00e9",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u00eb\37\uffff\1\u00eb",
            "\1\u00ec\37\uffff\1\u00ec",
            "\1\u00ed\37\uffff\1\u00ed",
            "\1\u00ee\37\uffff\1\u00ee",
            "\1\u00ef\37\uffff\1\u00ef",
            "\1\u00f0\37\uffff\1\u00f0",
            "\1\u00f1\37\uffff\1\u00f1",
            "\1\u00f2\37\uffff\1\u00f2",
            "\1\u00f4\1\uffff\1\u00f3\35\uffff\1\u00f4\1\uffff\1\u00f3",
            "",
            "\1\u00f5\37\uffff\1\u00f5",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u00f6\37\uffff\1\u00f6",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\4\56\1\u00f9\25\56\4\uffff\1\56"+
            "\1\uffff\4\56\1\u00f9\25\56",
            "",
            "\1\u00fb\37\uffff\1\u00fb",
            "\1\u00fc\37\uffff\1\u00fc",
            "",
            "\1\u00fd\37\uffff\1\u00fd",
            "\1\u00ff\11\uffff\1\u00fe\25\uffff\1\u00ff\11\uffff\1\u00fe",
            "\1\u0100",
            "\1\u0101\37\uffff\1\u0101",
            "\1\u0102\37\uffff\1\u0102",
            "",
            "\1\u0103\37\uffff\1\u0103",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0105\37\uffff\1\u0105",
            "\1\u0106\37\uffff\1\u0106",
            "\1\u0107\37\uffff\1\u0107",
            "\1\u0108\37\uffff\1\u0108",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u010a\37\uffff\1\u010a",
            "\1\u010b\37\uffff\1\u010b",
            "\1\u010c\37\uffff\1\u010c",
            "\1\u010d\3\uffff\1\u010e\33\uffff\1\u010d\3\uffff\1\u010e",
            "\1\u010f\37\uffff\1\u010f",
            "\1\u0110\37\uffff\1\u0110",
            "\1\u0111\37\uffff\1\u0111",
            "\1\u0113\10\uffff\1\u0112\26\uffff\1\u0113\10\uffff\1\u0112",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0115\37\uffff\1\u0115",
            "\1\u0116\37\uffff\1\u0116",
            "\1\u0117\37\uffff\1\u0117",
            "\1\u0118\37\uffff\1\u0118",
            "\1\u0119\37\uffff\1\u0119",
            "\1\u011a\37\uffff\1\u011a",
            "\1\u011b\37\uffff\1\u011b",
            "\1\u011c\37\uffff\1\u011c",
            "\1\u011d\37\uffff\1\u011d",
            "\1\u011e\37\uffff\1\u011e",
            "\1\u011f\37\uffff\1\u011f",
            "\1\u0120\37\uffff\1\u0120",
            "\1\u0121\37\uffff\1\u0121",
            "\1\u0122\16\uffff\1\u0123\20\uffff\1\u0122\16\uffff\1\u0123",
            "\1\u0124\37\uffff\1\u0124",
            "\1\u0125\37\uffff\1\u0125",
            "\1\u0126\37\uffff\1\u0126",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0128\37\uffff\1\u0128",
            "\1\u0129\37\uffff\1\u0129",
            "\1\u012a\37\uffff\1\u012a",
            "\1\u012b\37\uffff\1\u012b",
            "\1\u012c\37\uffff\1\u012c",
            "\1\u012d\37\uffff\1\u012d",
            "\1\u012e\37\uffff\1\u012e",
            "\1\u012f\37\uffff\1\u012f",
            "\1\u0130\37\uffff\1\u0130",
            "\1\u0131\37\uffff\1\u0131",
            "\1\u0132\37\uffff\1\u0132",
            "\1\u0133\37\uffff\1\u0133",
            "\1\u0134\37\uffff\1\u0134",
            "\1\u0135\37\uffff\1\u0135",
            "\2\56\13\uffff\13\56\6\uffff\6\56\1\u0136\23\56\4\uffff\1\56"+
            "\1\uffff\6\56\1\u0136\23\56",
            "\1\u0139\3\uffff\1\u0138\33\uffff\1\u0139\3\uffff\1\u0138",
            "\1\u013a\37\uffff\1\u013a",
            "\1\u013b\37\uffff\1\u013b",
            "\1\u013c\37\uffff\1\u013c",
            "\1\u013d\37\uffff\1\u013d",
            "\1\u013e\37\uffff\1\u013e",
            "\2\56\13\uffff\13\56\6\uffff\16\56\1\u013f\13\56\4\uffff\1"+
            "\56\1\uffff\16\56\1\u013f\13\56",
            "\2\56\13\uffff\13\56\6\uffff\24\56\1\u0141\5\56\4\uffff\1\56"+
            "\1\uffff\24\56\1\u0141\5\56",
            "\1\u0143\37\uffff\1\u0143",
            "\1\u0144\37\uffff\1\u0144",
            "\1\u0145\37\uffff\1\u0145",
            "\1\u0146\37\uffff\1\u0146",
            "\1\u0147\2\uffff\1\u0148\34\uffff\1\u0147\2\uffff\1\u0148",
            "\1\u0149\37\uffff\1\u0149",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u014c\37\uffff\1\u014c",
            "\1\u014d\37\uffff\1\u014d",
            "\1\u014e\37\uffff\1\u014e",
            "\1\u014f\37\uffff\1\u014f",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u0151\37\uffff\1\u0151",
            "\1\u0152\37\uffff\1\u0152",
            "\1\u0153\37\uffff\1\u0153",
            "\1\u0154\37\uffff\1\u0154",
            "\1\u0155\37\uffff\1\u0155",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\u0157\1\uffff\2\u0157\22\uffff\1\u0157",
            "\1\u0158\37\uffff\1\u0158",
            "\1\u0159\37\uffff\1\u0159",
            "\1\u015a\37\uffff\1\u015a",
            "\1\u015b\37\uffff\1\u015b",
            "\1\u015c\37\uffff\1\u015c",
            "",
            "",
            "\1\u015d\37\uffff\1\u015d",
            "",
            "\1\u015e\37\uffff\1\u015e",
            "\1\u0160\5\uffff\1\u015f\31\uffff\1\u0160\5\uffff\1\u015f",
            "\1\u0161\37\uffff\1\u0161",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0163\37\uffff\1\u0163",
            "\1\u0164",
            "\1\u0165\37\uffff\1\u0165",
            "\1\u0166\37\uffff\1\u0166",
            "\1\u0167\37\uffff\1\u0167",
            "",
            "\1\u0168\37\uffff\1\u0168",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u016a\37\uffff\1\u016a",
            "\1\u016b\37\uffff\1\u016b",
            "",
            "\1\u016c\37\uffff\1\u016c",
            "\1\u016d\37\uffff\1\u016d",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u016f\37\uffff\1\u016f",
            "\1\u0170\37\uffff\1\u0170",
            "\1\u0171\37\uffff\1\u0171",
            "\1\u0172\37\uffff\1\u0172",
            "\2\56\13\uffff\13\56\6\uffff\4\56\1\u0173\25\56\4\uffff\1\56"+
            "\1\uffff\4\56\1\u0173\25\56",
            "\1\u0175\37\uffff\1\u0175",
            "\2\56\13\uffff\13\56\6\uffff\12\56\1\u0176\17\56\4\uffff\1"+
            "\56\1\uffff\12\56\1\u0176\17\56",
            "",
            "\2\56\13\uffff\13\56\6\uffff\1\u017a\2\56\1\u0179\13\56\1\u017b"+
            "\3\56\1\u0178\6\56\4\uffff\1\56\1\uffff\1\u017a\2\56\1\u0179"+
            "\13\56\1\u017b\3\56\1\u0178\6\56",
            "\1\u017e\17\uffff\1\u017d\17\uffff\1\u017e\17\uffff\1\u017d",
            "\1\u017f\37\uffff\1\u017f",
            "\1\u0180\37\uffff\1\u0180",
            "\1\u0181\37\uffff\1\u0181",
            "\1\u0182\37\uffff\1\u0182",
            "\1\u0183\37\uffff\1\u0183",
            "\1\u0184\37\uffff\1\u0184",
            "\1\u0185\37\uffff\1\u0185",
            "\1\u0186\37\uffff\1\u0186",
            "\1\u0187\37\uffff\1\u0187",
            "\1\u0188\37\uffff\1\u0188",
            "\1\u0189\37\uffff\1\u0189",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u018d\37\uffff\1\u018d",
            "\1\u018e\37\uffff\1\u018e",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0191\37\uffff\1\u0191",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0193\37\uffff\1\u0193",
            "\1\u0194\37\uffff\1\u0194",
            "\1\u0195\37\uffff\1\u0195",
            "\1\u0196\37\uffff\1\u0196",
            "\1\u0197\37\uffff\1\u0197",
            "\2\u019a\1\uffff\2\u019a\22\uffff\1\u019a\43\uffff\1\u0199"+
            "\17\uffff\1\u0198\17\uffff\1\u0199\17\uffff\1\u0198",
            "\1\u019b\37\uffff\1\u019b",
            "\1\u019c\37\uffff\1\u019c",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\u019e\1\uffff\2\u019e\22\uffff\1\u019e",
            "\1\u019f\37\uffff\1\u019f",
            "",
            "\1\u01a0\37\uffff\1\u01a0",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01a3\37\uffff\1\u01a3",
            "\1\u01a4\37\uffff\1\u01a4",
            "\1\u01a5\37\uffff\1\u01a5",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01a7\37\uffff\1\u01a7",
            "",
            "\1\u01a8\37\uffff\1\u01a8",
            "",
            "\1\u01a9\37\uffff\1\u01a9",
            "\1\u01aa\37\uffff\1\u01aa",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01ac\37\uffff\1\u01ac",
            "",
            "",
            "\1\u01ad\37\uffff\1\u01ad",
            "",
            "",
            "\1\u01ae\37\uffff\1\u01ae",
            "\1\u01af\37\uffff\1\u01af",
            "\2\56\13\uffff\13\56\6\uffff\22\56\1\u01b0\7\56\4\uffff\1\56"+
            "\1\uffff\22\56\1\u01b0\7\56",
            "\1\u01b2\37\uffff\1\u01b2",
            "",
            "\1\u01b3\37\uffff\1\u01b3",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01b6\37\uffff\1\u01b6",
            "",
            "",
            "\2\u01b7\1\uffff\2\u01b7\22\uffff\1\u01b7",
            "\1\u01b8\37\uffff\1\u01b8",
            "\1\u01b9\37\uffff\1\u01b9",
            "\1\u01ba\37\uffff\1\u01ba",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01bc\37\uffff\1\u01bc",
            "\1\u01bd\37\uffff\1\u01bd",
            "",
            "",
            "\1\u01be\37\uffff\1\u01be",
            "",
            "\1\u01bf\37\uffff\1\u01bf",
            "\1\u01c0",
            "\2\u01c1\1\uffff\2\u01c1\22\uffff\1\u01c1",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01c3\37\uffff\1\u01c3",
            "\1\u01c4\37\uffff\1\u01c4",
            "",
            "\1\u01c5\37\uffff\1\u01c5",
            "\1\u01c6\37\uffff\1\u01c6",
            "\1\u01c7\37\uffff\1\u01c7",
            "\1\u01c8\37\uffff\1\u01c8",
            "",
            "\1\u01c9\37\uffff\1\u01c9",
            "\1\u01ca\37\uffff\1\u01ca",
            "\1\u01cb\37\uffff\1\u01cb",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01cd\37\uffff\1\u01cd",
            "",
            "\1\u01ce\37\uffff\1\u01ce",
            "\1\u01cf\37\uffff\1\u01cf",
            "",
            "\1\u01d0\37\uffff\1\u01d0",
            "\1\u01d1\37\uffff\1\u01d1",
            "\1\u01d2\37\uffff\1\u01d2",
            "\1\u01d3\37\uffff\1\u01d3",
            "",
            "\1\u01d4\37\uffff\1\u01d4",
            "\1\u01d6\1\uffff\1\u01d5\35\uffff\1\u01d6\1\uffff\1\u01d5",
            "\1\u01d7\37\uffff\1\u01d7",
            "\1\u01d8\37\uffff\1\u01d8",
            "\2\u01d9\1\uffff\2\u01d9\22\uffff\1\u01d9",
            "\1\u01da\37\uffff\1\u01da",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01dc\37\uffff\1\u01dc",
            "\1\u01dd\37\uffff\1\u01dd",
            "\1\u01de\37\uffff\1\u01de",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01e0\37\uffff\1\u01e0",
            "\1\u01e1\37\uffff\1\u01e1",
            "",
            "",
            "",
            "\1\u01e2\37\uffff\1\u01e2",
            "\1\u01e3\37\uffff\1\u01e3",
            "",
            "",
            "\1\u01e4\37\uffff\1\u01e4",
            "",
            "\1\u01e5\37\uffff\1\u01e5",
            "\1\u01e6\37\uffff\1\u01e6",
            "\1\u01e7\37\uffff\1\u01e7",
            "\1\u01e8\37\uffff\1\u01e8",
            "\1\u01e9\37\uffff\1\u01e9",
            "\1\u01ea\37\uffff\1\u01ea",
            "\1\u01ec\1\uffff\1\u01eb\35\uffff\1\u01ec\1\uffff\1\u01eb",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\1\u01ef\37\uffff\1\u01ef",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\2\u01f1\1\uffff\2\u01f1\22\uffff\1\u01f1",
            "\1\u01f2\37\uffff\1\u01f2",
            "\1\u01f3\37\uffff\1\u01f3",
            "",
            "\1\u01f4\37\uffff\1\u01f4",
            "\1\u01f5\37\uffff\1\u01f5",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u01f7\37\uffff\1\u01f7",
            "",
            "\1\u01f8\37\uffff\1\u01f8",
            "\1\u01f9\37\uffff\1\u01f9",
            "\1\u01fa\37\uffff\1\u01fa",
            "\1\u01fb\37\uffff\1\u01fb",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u01fd\37\uffff\1\u01fd",
            "\1\u01fe\37\uffff\1\u01fe",
            "",
            "",
            "\1\u01ff\37\uffff\1\u01ff",
            "",
            "\1\u0200\37\uffff\1\u0200",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0202\37\uffff\1\u0202",
            "",
            "\1\u0203\37\uffff\1\u0203",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0206\37\uffff\1\u0206",
            "\1\u0207",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\u020a\1\uffff\2\u020a\22\uffff\1\u020a",
            "\1\u020b\37\uffff\1\u020b",
            "\1\u020c\37\uffff\1\u020c",
            "\1\u020d\37\uffff\1\u020d",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u020f\37\uffff\1\u020f",
            "\1\u0210\37\uffff\1\u0210",
            "",
            "\1\u0211\37\uffff\1\u0211",
            "\1\u0212\37\uffff\1\u0212",
            "\1\u0213\37\uffff\1\u0213",
            "\1\u0214\37\uffff\1\u0214",
            "\1\u0215\37\uffff\1\u0215",
            "\1\u0216\37\uffff\1\u0216",
            "\1\u0217\37\uffff\1\u0217",
            "\1\u0218\37\uffff\1\u0218",
            "\1\u0219\37\uffff\1\u0219",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u021c\37\uffff\1\u021c",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u021e\37\uffff\1\u021e",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\22\56\1\u0220\7\56\4\uffff\1\56"+
            "\1\uffff\22\56\1\u0220\7\56",
            "",
            "\1\u0222\37\uffff\1\u0222",
            "\1\u0223\37\uffff\1\u0223",
            "\1\u0224\37\uffff\1\u0224",
            "\1\u0225\37\uffff\1\u0225",
            "\1\u0226\37\uffff\1\u0226",
            "\1\u0227\37\uffff\1\u0227",
            "\1\u0228\37\uffff\1\u0228",
            "\1\u0229\37\uffff\1\u0229",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u022b\37\uffff\1\u022b",
            "\1\u022c\37\uffff\1\u022c",
            "\1\u022d\37\uffff\1\u022d",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\1\u0230\37\uffff\1\u0230",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0237\37\uffff\1\u0237",
            "\1\u0238\37\uffff\1\u0238",
            "",
            "\1\u0239\37\uffff\1\u0239",
            "\1\u023a\37\uffff\1\u023a",
            "\2\u023b\1\uffff\2\u023b\22\uffff\1\u023b",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u023d\37\uffff\1\u023d",
            "\1\u023e\37\uffff\1\u023e",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0240",
            "",
            "",
            "",
            "\1\u0241\37\uffff\1\u0241",
            "\1\u0242\37\uffff\1\u0242",
            "\1\u0243\37\uffff\1\u0243",
            "",
            "\1\u0244\37\uffff\1\u0244",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0246\37\uffff\1\u0246",
            "\1\u0247\37\uffff\1\u0247",
            "\1\u0248\37\uffff\1\u0248",
            "\1\u0249\37\uffff\1\u0249",
            "\1\u024a\37\uffff\1\u024a",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u024c\37\uffff\1\u024c",
            "\1\u024d\37\uffff\1\u024d",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\1\u024f\37\uffff\1\u024f",
            "",
            "\1\u0250\37\uffff\1\u0250",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u0252\37\uffff\1\u0252",
            "\1\u0253\37\uffff\1\u0253",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0255\37\uffff\1\u0255",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0257\37\uffff\1\u0257",
            "\2\u0258\1\uffff\2\u0258\22\uffff\1\u0258",
            "\1\u0259\37\uffff\1\u0259",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u025b\37\uffff\1\u025b",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "",
            "",
            "",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u025f\37\uffff\1\u025f",
            "\1\u0260\37\uffff\1\u0260",
            "\1\u0261\37\uffff\1\u0261",
            "",
            "",
            "\1\u0262\37\uffff\1\u0262",
            "\1\u0263\37\uffff\1\u0263",
            "",
            "\1\u0264",
            "\1\u0265\37\uffff\1\u0265",
            "\1\u0266\37\uffff\1\u0266",
            "\1\u0267\37\uffff\1\u0267",
            "\1\u0268\37\uffff\1\u0268",
            "",
            "\1\u0269\37\uffff\1\u0269",
            "\2\56\13\uffff\13\56\6\uffff\5\56\1\u026a\24\56\4\uffff\1\56"+
            "\1\uffff\5\56\1\u026a\24\56",
            "\1\u026c\37\uffff\1\u026c",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0270\37\uffff\1\u0270",
            "",
            "\1\u0271\37\uffff\1\u0271",
            "\1\u0272\37\uffff\1\u0272",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0274\37\uffff\1\u0274",
            "",
            "\2\u0275\1\uffff\2\u0275\22\uffff\1\u0275",
            "",
            "\1\u0276\37\uffff\1\u0276",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\1\u0278\37\uffff\1\u0278",
            "",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u027a\37\uffff\1\u027a",
            "\1\u027b\2\uffff\1\u027c\34\uffff\1\u027b\2\uffff\1\u027c",
            "\1\u027d\37\uffff\1\u027d",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u027e",
            "\2\u027f\1\uffff\2\u027f\22\uffff\1\u027f",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\u0282\1\uffff\2\u0282\22\uffff\1\u0282",
            "\1\u0283\37\uffff\1\u0283",
            "\1\u0284\37\uffff\1\u0284",
            "",
            "\1\u0285\37\uffff\1\u0285",
            "",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0287\37\uffff\1\u0287",
            "\1\u0288\37\uffff\1\u0288",
            "",
            "\1\u0289\37\uffff\1\u0289",
            "",
            "\1\u028a\37\uffff\1\u028a",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\2\u028c\1\uffff\2\u028c\22\uffff\1\u028c",
            "\1\u028d\37\uffff\1\u028d",
            "\1\u028e\37\uffff\1\u028e",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0290",
            "",
            "",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u0291\37\uffff\1\u0291",
            "\1\u0292\37\uffff\1\u0292",
            "",
            "\1\u0293\37\uffff\1\u0293",
            "\1\u0294\37\uffff\1\u0294",
            "\2\56\13\uffff\13\56\6\uffff\12\56\1\u0295\17\56\4\uffff\1"+
            "\56\1\uffff\12\56\1\u0295\17\56",
            "\1\u0297\37\uffff\1\u0297",
            "",
            "",
            "\1\u0298\37\uffff\1\u0298",
            "\1\u0299\37\uffff\1\u0299",
            "",
            "\1\u029a",
            "\1\u029b\37\uffff\1\u029b",
            "\1\u029c\37\uffff\1\u029c",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u029e\37\uffff\1\u029e",
            "\1\u029f\37\uffff\1\u029f",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u02a2\37\uffff\1\u02a2",
            "\1\u02a3",
            "\1\u02a4\37\uffff\1\u02a4",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u02a7\37\uffff\1\u02a7",
            "",
            "",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u02a9",
            "\1\u02aa\37\uffff\1\u02aa",
            "",
            "",
            "\1\u02ab\37\uffff\1\u02ab",
            "",
            "\1\u02ac",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "\1\u02ae\37\uffff\1\u02ae",
            "\1\u02af",
            "",
            "\1\u02b0\37\uffff\1\u02b0",
            "\1\u02b1",
            "\1\u02b2\37\uffff\1\u02b2",
            "\1\u02b3",
            "\1\u02b4\37\uffff\1\u02b4",
            "\1\u02b5",
            "\2\56\13\uffff\13\56\6\uffff\32\56\4\uffff\1\56\1\uffff\32"+
            "\56",
            "",
            ""
    };

    static final short[] DFA21_eot = DFA.unpackEncodedString(DFA21_eotS);
    static final short[] DFA21_eof = DFA.unpackEncodedString(DFA21_eofS);
    static final char[] DFA21_min = DFA.unpackEncodedStringToUnsignedChars(DFA21_minS);
    static final char[] DFA21_max = DFA.unpackEncodedStringToUnsignedChars(DFA21_maxS);
    static final short[] DFA21_accept = DFA.unpackEncodedString(DFA21_acceptS);
    static final short[] DFA21_special = DFA.unpackEncodedString(DFA21_specialS);
    static final short[][] DFA21_transition;

    static {
        int numStates = DFA21_transitionS.length;
        DFA21_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA21_transition[i] = DFA.unpackEncodedString(DFA21_transitionS[i]);
        }
    }

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__163 | T__164 | T__165 | STRING | INTEGER | FLOAT | DATETIME | BOOLEAN | NULL | OR | AND | EQUALS | NOTEQUALS | ISNULL | ISNOTNULL | LT | LTEQ | GT | GTEQ | PLUS | MINUS | MULT | DIV | MOD | POW | NOT | SELECT | SHOW | TABLES | SYSTEMSTATUS | DROP | IFEXISTS | TRUNCATE | TABLE | RENAME | DELETE | INSERT | UPDATE | CREATE | EXIT | QUIT | KILL | ALTER | PROCESSLIST | SET | FLUSH | INTO | VALUES | INFOTABLES | REMOTEQUERY | LOADDATAINFILE | OUTFILE | FIELDS | COLUMNS | COLUMN | ADD | ENCLOSEDBY | DELIMITEDBY | TERMINATEDBY | PRIMARYKEY | FOREIGNKEY | SURROGATEKEYS | SKIPFIRST | NOINDEX | FROM | WHERE | ORDERBY | GROUPBY | HAVING | INNERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN | FULLOUTERJOIN | MAXONE | DISTINCTFORKEY | TRANSIENT | ON | IN | NOTIN | NOTLIKE | LIKE | SUM | AVG | COUNT | DISTINCT | MIN | MAX | CASE | WHEN | THEN | ELSE | END | TOP | YEAR | QUARTER | MONTH | WEEK | DAY | HOUR | MINUTE | SECOND | VARCHARTYPE | INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | DATETYPE | DATETIMETYPE | TIMESTAMPADD | TIMESTAMPDIFF | EXTRACT | DATEDIFF | DATEADD | DATEPART | GETDATE | SUBSTR | LENGTH | LEN | POSITION | OBJECTSIZE | TOUPPER | TOLOWER | ISNULLEXP | COALESCE | CAST | DEBUG | DUMPTABLE | LOADTABLE | DUMPDDL | LOADDDL | DUMPDB | LOADDB | OUTDIR | INDIR | TO | DISK | MEMORY | COMPRESSED | UNCOMPRESSED | DISKKEYSTORE | COMPRESSEDKEYSTORE | LEFT_PAREN | RIGHT_PAREN | PARAMETER | ASCENDING | DESCENDING | AS | IDENT | WS | LINE_COMMENT | ML_COMMENT );";
        }
    }
 

}