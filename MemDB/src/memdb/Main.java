package memdb;

import gnu.trove.map.TIntObjectMap;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TIntObjectProcedure;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;
import java.util.Date;

import memdb.jdbcserver.RemoteResultSetImpl;
import memdb.sql.QueryResultRow;
import memdb.sql.Select;
import memdb.sql.WhereStatement;
import memdb.storage.Table;
import memdb.storage.TableMetadata;
import memdb.tcpserver.TcpServer;
import memdb.util.Util;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * 
 */
/**
 * @author bpeters
 * 
 */
@SuppressWarnings("unused")
public class Main
{
	private static Logger logger = Logger.getLogger(Main.class);

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args)
	{
		String databasePath = null;
		int jdbcport = 20000;
		int tcpport = 30000;
		Logger.getLogger(WhereStatement.class).setLevel(Level.TRACE);
		Logger.getLogger(Select.class).setLevel(Level.DEBUG);
		Logger.getLogger(Command.class).setLevel(Level.ALL);
		Logger.getLogger(Main.class).setLevel(Level.ALL);
		Logger.getLogger(TableLatch.class).setLevel(Level.INFO);
		Logger.getLogger(ParallelAddRow.class).setLevel(Level.ALL);
		for (String s : args)
		{
			try
			{
				if (s.startsWith("database="))
				{
					databasePath = s.substring("database=".length());
				}
				if (s.startsWith("jdbcport="))
				{
					jdbcport = Integer.valueOf(s.substring("jdbcport=".length()));
				}
				if (s.startsWith("tcpport="))
				{
					tcpport = Integer.valueOf(s.substring("tcpport=".length()));
				}
			} catch (Exception ex)
			{
				System.out.println("Invalid parameter: " + s);
			}
		}
		if (databasePath == null)
		{
			System.out.println("Must specify a database");
			System.out.println("Usage: memdb database={path of database} [jdbcport={portnum}] [tcpport={portnum}]");
			System.exit(1);
		}
		File f = new File(databasePath);
		if (!f.exists())
		{
			f.mkdir();
		}
		if (!f.exists() || !f.isDirectory())
		{
			System.out.println("Invalid database specified");
			System.exit(1);
		}
		Database db = startDatabase(databasePath, jdbcport, tcpport);
		boolean runInServiceMode = Boolean.parseBoolean(System.getProperty("RUN_IN_SERVICE_MODE"));
		System.out.println("runInServiceMode = " + runInServiceMode);
		logger.debug("runInServiceMode = " + runInServiceMode);
		if (runInServiceMode)
		{
			try
			{
				while (true)
				{
					Thread.currentThread();
					Thread.sleep(5000);
				}
			} catch (Exception e)
			{
				System.out.println(e.getMessage() + " " + e.getStackTrace());
			}
		} else
		{
			runcommand(db);
		}
		System.out.println("Exiting MemDB");
	}

	public static Database startDatabase(String databasePath, int jdbcport, int tcpport)
	{
		Database db = new Database(databasePath);
		System.out.println("Starting MemDB");
		String rti = Util.getRuntimeInfo();
		System.out.println(rti);
		logger.debug(rti);
		try
		{
			loadDatabase(db);
		} catch (Exception e)
		{
			logger.error(e);
			System.out.println("Error: " + e.getMessage());
			System.exit(1);
		}
		Server sv = new Server(db, jdbcport);
		TcpServer tcpserv = new TcpServer(db, tcpport);
		try
		{
			tcpserv.start();
		} catch (IOException e)
		{
			System.out.println("Unable to start tcp server on port: " + tcpport);
			logger.error(e);
		}
		return db;
	}

	public static void runcommand(Database db)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while (true)
		{
			System.out.print("cmd> ");
			try
			{
				String command = reader.readLine();
				if (command == null)
				{
					System.exit(1);
				}
				command = command.trim();
				if (command.length() == 0 || command.startsWith("//"))
					continue;
				if (command.startsWith("execute "))
				{
					String fname = command.substring("execute ".length());
					File f = new File(fname);
					if (!f.exists())
					{
						System.out.println("Command file [" + fname + "] not found");
						continue;
					}
					BufferedReader creader = new BufferedReader(new FileReader(f));
					String line = null;
					while ((line = creader.readLine()) != null)
					{
						line = line.trim();
						if (line.length() == 0)
							continue;
						System.out.println(line);
						executeCommand(db, line);
					}
					creader.close();
				} else
					executeCommand(db, command);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void executeCommand(Database db, String command) throws RemoteException
	{
		Command c = null;
		long start = System.currentTimeMillis();
		Object results = null;
		try
		{
			TaskThread tt = TaskThread.launchNewCommand(command, db, TimeZone.getDefault(), System.out);
			tt.join();
			results = tt.getResults();
			c = tt.getCommand();
		} catch (SQLException ex)
		{
			if (ex.getMessage().equals("exit"))
				System.exit(0);
			System.out.println(ex.getMessage());
			return;
		} catch (java.lang.OutOfMemoryError ex)
		{
			logger.error(ex);
			return;
		} catch (InterruptedException e)
		{
			logger.error(e);
			return;
		}
		long end = System.currentTimeMillis();
		PrintStream outstream = System.out;
		String outFileName = c.getOutFileName();
		if (outFileName != null)
		{
			try
			{
				outstream = new PrintStream(new FileOutputStream(outFileName));
			} catch (FileNotFoundException e)
			{
				System.out.println("Error: " + e.getMessage());
				return;
			}
		}
		if (results != null && results instanceof RemoteResultSetImpl)
		{
			RemoteResultSetImpl rs = (RemoteResultSetImpl) results;
			int length;
			int[] types;
			try
			{
				length = rs.getColumnNames().length;
				types = new int[length];
				for (int i = 0; i < length; i++)
				{
					Object o = rs.getColumnNames()[i];
					types[i] = rs.getColumnTypes()[i];
					outstream.print((i > 0 ? '\t' : "") + (o == null ? "" : o.toString()));
				}
				outstream.println();
				while(true)
				{
					Object [] row = rs.getNextRow();
					if(row == null)
					{
						break;
					}
					length = row.length;
					for (int i = 0; i < length; i++)
					{
						Object o = row[i];
						String ostr = null;
						if(o != null)
						{
							if((types[i] == Types.DATE || types[i] == Types.TIMESTAMP) && (o instanceof Date)) 
							{
								ostr = DateUtil.convertToDBFormat((Date)o, TimeZone.getDefault());
							}
							else if ((types[i] == Types.TIMESTAMP) && (o instanceof Timestamp))
							{
								ostr = DateUtil.convertToDBFormat(new Date(((Timestamp)o).getTime()), TimeZone.getDefault());
							}
							else
							{
								ostr = Util.replaceStr(o.toString(), "\t", "\\\t");
								if(ostr.contains("\n"))
								{
									ostr = "\"" + ostr + "\"";
								}
							}
						}
						outstream.print((i > 0 ? '\t' : "") + (ostr != null ? ostr : ""));
					}
					outstream.println();
				}
			} catch (SQLException e)
			{
				logger.error(e);
				System.out.println(e.getMessage());
			}
		}
		if (outFileName != null)
			outstream.close();
		System.out.println("Duration: " + (double) (end - start) / 1000.0 + "s");
	}

	public static Database loadDatabase(Database db) throws InterruptedException, IOException, ClassNotFoundException
	{
		db.readExternal();
		return db;
	}
}
