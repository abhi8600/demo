package memdb.expressions;

import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.DateParser;
import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class DateMethods extends Expression
{
	private int field;
	private Expression offset;
	private Expression dateval;
	private Expression startdate;
	private Expression enddate;
	private Calendar c1 = GregorianCalendar.getInstance(TimeZone.getDefault());
	private Calendar c2 = GregorianCalendar.getInstance(TimeZone.getDefault());
	private Calendar c = GregorianCalendar.getInstance(TimeZone.getDefault());
	@SuppressWarnings("unused")
	private SimpleDateFormat sdf = new SimpleDateFormat();
	private DateParser dp1 = new DateParser();
	private DateParser dp2 = new DateParser();
	private WhereStatement wstmt;

	public DateMethods(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		this.wstmt = wstmt;
		switch (type)
		{
		case MemgrammarParser.GETDATE:
			break;
		case MemgrammarParser.DATEADD:
		case MemgrammarParser.TIMESTAMPADD:
			field = getFieldType(t.getChild(0).getType());
			offset = Expression.parseExpression(wstmt, t.getChild(1));
			if (offset.getType() != Types.INTEGER)
				throw new SQLException("Invalid offset in DATEADD/TIMESTAMPADD");
			dateval = Expression.parseExpression(wstmt, t.getChild(2));
			if (dateval.getType() != Types.DATE && dateval.getType() != Types.TIMESTAMP && dateval.getType() != Types.VARCHAR)
				throw new SQLException("Invalid date in DATEADD/TIMESTAMPADD");
			if (t.getChild(2).getType() == MemgrammarParser.STRING)
				((Literal) dateval).setDate(dp1.parseUsingPossibleFormats(t.getChild(2).getText(), wstmt.getCommand().getClientTimeZone()));
			break;
		case MemgrammarParser.DATEDIFF:
		case MemgrammarParser.TIMESTAMPDIFF:
			field = getFieldType(t.getChild(0).getType());
			startdate = Expression.parseExpression(wstmt, t.getChild(1));
			if (startdate.getType() != Types.DATE && startdate.getType() != Types.TIMESTAMP && startdate.getType() != Types.VARCHAR)
				throw new SQLException("Invalid date in DATEDIFF/TIMESTAMPDIFF");
			enddate = Expression.parseExpression(wstmt, t.getChild(2));
			if (enddate.getType() != Types.DATE && enddate.getType() != Types.TIMESTAMP && enddate.getType() != Types.VARCHAR)
				throw new SQLException("Invalid date in DATEDIFF/TIMESTAMPDIFF");
			if (t.getChild(1).getType() == MemgrammarParser.STRING)
				((Literal) startdate).setDate((new DateParser()).parseUsingPossibleFormats(t.getChild(1).getText(), wstmt.getCommand().getClientTimeZone()));
			if (t.getChild(2).getType() == MemgrammarParser.STRING)
				((Literal) enddate).setDate(dp1.parseUsingPossibleFormats(t.getChild(2).getText(), wstmt.getCommand().getClientTimeZone()));
			break;
		case MemgrammarParser.DATEPART:
		case MemgrammarParser.EXTRACT:
			field = getFieldType(t.getChild(0).getType());
			dateval = Expression.parseExpression(wstmt, t.getChild(1));
			int dtype = dateval.getType();
			if (t.getChild(1).getType() == MemgrammarParser.STRING)
				((Literal) dateval).setDate(dp1.parseUsingPossibleFormats(t.getChild(1).getText(), wstmt.getCommand().getClientTimeZone()));
			else if (dtype != Types.DATE && dtype != Types.TIMESTAMP)
				throw new SQLException("Invalid date in DATEPART/EXTRACT");
			break;
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset)
	{
		return null;
	}
	private Date lastdate;
	private Integer lastoffset;
	private Object lastval;

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		synchronized (this)
		{
			switch (type)
			{
			case MemgrammarParser.GETDATE:
				return new Date();
			case MemgrammarParser.DATEADD:
			case MemgrammarParser.TIMESTAMPADD:
				Object offseto = offset.evaluate(rowset);
				Object dateo = dateval.evaluate(rowset);
				if (dateo instanceof String)
					dateo = dp1.parseUsingPossibleFormats(dateo.toString(), wstmt.getCommand().getClientTimeZone());
				if (dateo == null)
					return null;
				if (lastoffset != (Integer) offseto || !((Date) dateo).equals(lastdate))
				{
					c.setTime((Date) dateo);
					if (field == -1)
						c.add(Calendar.MONTH, ((Integer) offseto) * 3);
					else
						c.add(field, (Integer) offseto);
					lastval = c.getTime();
				}
				return lastval;
			case MemgrammarParser.DATEDIFF:
			case MemgrammarParser.TIMESTAMPDIFF:
				Object starto = startdate.evaluate(rowset);
				Object endo = enddate.evaluate(rowset);
				if (endo == null || starto == null)
					return null;
				if (starto instanceof String)
					starto = dp1.parseUsingPossibleFormats(starto.toString(), wstmt.getCommand().getClientTimeZone());
				if (endo instanceof String)
					endo = dp2.parseUsingPossibleFormats(endo.toString(), wstmt.getCommand().getClientTimeZone());
				c1.setTime((Date) starto);
				c2.setTime((Date) endo);
				c1.set(Calendar.HOUR_OF_DAY, 0);
				c1.set(Calendar.MINUTE, 0);
				c1.set(Calendar.SECOND, 0);
				c1.set(Calendar.MILLISECOND, 0);
				c2.set(Calendar.HOUR_OF_DAY, 0);
				c2.set(Calendar.MINUTE, 0);
				c2.set(Calendar.SECOND, 0);
				c2.set(Calendar.MILLISECOND, 0);
				if (field == Calendar.YEAR)
				{
					int year1 = c1.get(Calendar.YEAR);
					int year2 = c2.get(Calendar.YEAR);
					return year2 - year1;
				} else if (field == -1)
				{
					int quarter1 = c1.get(Calendar.YEAR) * 4 + c1.get(Calendar.MONTH) / 3;
					int quarter2 = c2.get(Calendar.YEAR) * 4 + c2.get(Calendar.MONTH) / 3;
					return quarter2 - quarter1;
				} else if (field == Calendar.MONTH)
				{
					int month1 = (c1.get(Calendar.YEAR) * 12) + c1.get(Calendar.MONTH);
					int month2 = (c2.get(Calendar.YEAR) * 12) + c2.get(Calendar.MONTH);
					return month2 - month1;
				} else if (field == Calendar.WEEK_OF_YEAR)
				{
					if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
						return c2.get(Calendar.WEEK_OF_YEAR) - c1.get(Calendar.WEEK_OF_YEAR);
					int weekDiff = 0, weekDiffStart = 0, weekDiffEnd = 0;
					int multiplier = 1;
					Calendar clonedc1 = (Calendar) c1.clone();
					Calendar clonedc2 = (Calendar) c2.clone();
					if (c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
					{
						multiplier = -1;
						Calendar temp = clonedc1;
						clonedc1 = clonedc2;
						clonedc2 = temp;
					}
					Calendar endOfYearDate = (Calendar) clonedc1.clone();
					weekDiffStart = endOfYearDate.get(Calendar.WEEK_OF_YEAR);
					weekDiffEnd = endOfYearDate.getActualMaximum(Calendar.WEEK_OF_YEAR);
					weekDiff += weekDiffEnd - weekDiffStart;
					endOfYearDate.set(endOfYearDate.get(Calendar.YEAR) + 1, 0, 1);
					while (endOfYearDate.get(Calendar.YEAR) < clonedc2.get(Calendar.YEAR))
					{
						weekDiffEnd = endOfYearDate.getActualMaximum(Calendar.WEEK_OF_YEAR);
						weekDiff += weekDiffEnd;
						endOfYearDate.set(Calendar.YEAR, endOfYearDate.get(Calendar.YEAR) + 1);
					}
					weekDiff += clonedc2.get(Calendar.WEEK_OF_YEAR);
					return weekDiff * multiplier;
				} else if (field == Calendar.DAY_OF_MONTH)
				{
					if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
						return c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR);
					int dayDiff = 0, dayDiffStart = 0, dayDiffEnd = 0;
					int multiplier = 1;
					Calendar clonedc1 = (Calendar) c1.clone();
					Calendar clonedc2 = (Calendar) c2.clone();
					if (c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
					{
						multiplier = -1;
						Calendar temp = clonedc1;
						clonedc1 = clonedc2;
						clonedc2 = temp;
					}
					Calendar endOfYearDate = (Calendar) clonedc1.clone();
					dayDiffStart = endOfYearDate.get(Calendar.DAY_OF_YEAR);
					dayDiffEnd = endOfYearDate.getActualMaximum(Calendar.DAY_OF_YEAR);
					dayDiff += dayDiffEnd - dayDiffStart;
					endOfYearDate.set(endOfYearDate.get(Calendar.YEAR) + 1, 0, 1);
					while (endOfYearDate.get(Calendar.YEAR) < clonedc2.get(Calendar.YEAR))
					{
						dayDiffEnd = endOfYearDate.getActualMaximum(Calendar.DAY_OF_YEAR);
						dayDiff += dayDiffEnd;
						endOfYearDate.set(Calendar.YEAR, endOfYearDate.get(Calendar.YEAR) + 1);
					}
					dayDiff += clonedc2.get(Calendar.DAY_OF_YEAR);
					return dayDiff * multiplier;
				} else if (field == Calendar.HOUR)
				{
					Date dt2 = (Date) endo;
					Date dt1 = (Date) starto;
					long offset = getZoneOffsetBetweenDates(dt2, dt1);
					return (int) ((dt2.getTime() + offset - dt1.getTime()) / (1000 * 60 * 60));
				} else if (field == Calendar.MINUTE)
				{
					Date dt2 = (Date) endo;
					Date dt1 = (Date) starto;
					long offset = getZoneOffsetBetweenDates(dt2, dt1);
					return (int) ((dt2.getTime() + offset - dt1.getTime()) / (1000 * 60));
				} else if (field == Calendar.SECOND)
				{
					Date dt2 = (Date) endo;
					Date dt1 = (Date) starto;
					long offset = getZoneOffsetBetweenDates(dt2, dt1);
					return (int) ((dt2.getTime() + offset - dt1.getTime()) / 1000);
				}
				return c.getTime();
			case MemgrammarParser.DATEPART:
			case MemgrammarParser.EXTRACT:
				dateo = dateval.evaluate(rowset);
				if (dateo instanceof String)
					dateo = dp1.parseUsingPossibleFormats(dateo.toString(), wstmt.getCommand().getClientTimeZone());
				if (dateo != null)
				{
					c.setTime((Date) dateo);
					if (field == -1)
						return (c.get(Calendar.MONTH) / 3) + 1;
					if (field == Calendar.MONTH)
						return c.get(field) + 1;
					return c.get(field);
				}
			}
			return null;
		}
	}

	private int getFieldType(int field)
	{
		switch (field)
		{
		case MemgrammarParser.YEAR:
			return Calendar.YEAR;
		case MemgrammarParser.QUARTER:
			return -1;
		case MemgrammarParser.MONTH:
			return Calendar.MONTH;
		case MemgrammarParser.WEEK:
			return Calendar.WEEK_OF_YEAR;
		case MemgrammarParser.DAY:
			return Calendar.DAY_OF_MONTH;
		case MemgrammarParser.HOUR:
			return Calendar.HOUR;
		case MemgrammarParser.MINUTE:
			return Calendar.MINUTE;
		case MemgrammarParser.SECOND:
			return Calendar.SECOND;
		}
		return 0;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		if (dateval != null && dateval.containsAggregate(acount))
			return true;
		if (startdate != null && startdate.containsAggregate(acount))
			return true;
		if (enddate != null && enddate.containsAggregate(acount))
			return true;
		return false;
	}

	@Override
	public int getType()
	{
		switch (type)
		{
		case MemgrammarParser.DATEADD:
		case MemgrammarParser.TIMESTAMPADD:
		case MemgrammarParser.GETDATE:
			return Types.TIMESTAMP;
		case MemgrammarParser.DATEDIFF:
		case MemgrammarParser.TIMESTAMPDIFF:
			return Types.INTEGER;
		case MemgrammarParser.DATEPART:
		case MemgrammarParser.EXTRACT:
			return Types.INTEGER;
		}
		return Types.INTEGER;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	private long getZoneOffsetBetweenDates(Date dt2, Date dt1)
	{
		c1.setTime(dt1);
		c2.setTime(dt2);
		long offset1 = c1.getTimeZone().getOffset(dt1.getTime());
		long offset2 = c2.getTimeZone().getOffset(dt2.getTime());
		return (offset2 - offset1);
	}

	@Override
	public boolean containsNonAggregate()
	{
		if (offset != null && offset.containsNonAggregate())
			return true;
		if (dateval != null && dateval.containsNonAggregate())
			return true;
		if (startdate != null && startdate.containsNonAggregate())
			return true;
		if (enddate != null && enddate.containsNonAggregate())
			return true;
		return false;
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		if (offset != null)
			offset.setAggRow(aggRow);
		if (dateval != null)
			dateval.setAggRow(aggRow);
		if (startdate != null)
			startdate.setAggRow(aggRow);
		if (enddate != null)
			enddate.setAggRow(aggRow);
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		if (offset != null)
			offset.aggregate(aggRow, rowset);
		if (dateval != null)
			dateval.aggregate(aggRow, rowset);
		if (startdate != null)
			startdate.aggregate(aggRow, rowset);
		if (enddate != null)
			enddate.aggregate(aggRow, rowset);
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		if (offset != null)
			offset.consolidate(primaryAggRow, toConsolidateAggRow);
		if (dateval != null)
			dateval.consolidate(primaryAggRow, toConsolidateAggRow);
		if (startdate != null)
			startdate.consolidate(primaryAggRow, toConsolidateAggRow);
		if (enddate != null)
			enddate.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
