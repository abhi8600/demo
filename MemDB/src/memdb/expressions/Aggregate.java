package memdb.expressions;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class Aggregate extends Expression
{
	private Expression child = null;
	private boolean distinct = false;
	private boolean countall = false;
	private int index = -1;

	public Aggregate(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		if (t.getType() == MemgrammarParser.COUNT && t.getChild(0).getText().equals("*") && t.getChild(0).getChildCount() == 0)
		{
			countall = true;
		} else
		{
			child = Expression.parseExpression(wstmt, t.getChild(0));
			if (t.getChildCount() > 1 && t.getChild(1).getType() == MemgrammarParser.DISTINCT)
			{
				if (type == MemgrammarParser.COUNT)
					distinct = true;
				else
					throw new SQLException("DISTINCT only valid for COUNT aggregation rule");
			}
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset)
	{
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		if (aggRow == null)
		{
			if (t.getType() == MemgrammarParser.COUNT)
			{
				aggRow = new Object[1];
				if (distinct)
				{
					aggRow[0] = null;
				} else
				{
					aggRow[0] = new AtomicInteger(0);
				}
			} else
			{
				return null;
			}
		}
		if (!wstmt.isPostJoin())
			throw new SQLException("Cannot utilize an aggregate function within another aggregate function");
		if (distinct)
		{
			Set<Object> amap = (Set<Object>) aggRow[index];
			if (amap == null)
				return 0;
			return amap.size();
		}
		switch (type)
		{
		case MemgrammarParser.COUNT:
		case MemgrammarParser.SUM:
		case MemgrammarParser.MIN:
		case MemgrammarParser.MAX:
			Object o = aggRow[index];
			if (o instanceof AtomicInteger)
				return ((AtomicInteger) o).get();
			else if (o instanceof DoubleValue)
				return ((DoubleValue) o).value;
			else if (o == null && type == MemgrammarParser.SUM)
				return null;
			else if (o == null && type == MemgrammarParser.COUNT)
				return null;
			return o;
		case MemgrammarParser.AVG:
			if (aggRow[index] == null)
				return null;
			Object o1 = ((Object[]) aggRow[index])[0];
			Object o2 = ((Object[]) aggRow[index])[1];
			if (o1 instanceof AtomicInteger)
				return ((double) ((AtomicInteger) o1).get()) / ((double) ((AtomicInteger) o2).get());
			else if (o1 instanceof DoubleValue)
				return ((DoubleValue) o1).value / ((double) ((AtomicInteger) o2).get());
			else if (o1 == null && type == MemgrammarParser.SUM)
				return 0D;
			else if (o1 == null && type == MemgrammarParser.COUNT)
				return 0;
			return 0;
		}
		return null;
	}

	@SuppressWarnings(
	{ "unchecked" })
	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		if (index < 0)
			return;
		Object o = countall ? null : child.evaluate(rowset);
		if (distinct)
		{
			if (o != null)
			{
				Set<Object> curSet = (Set<Object>) aggRow[index];
				if (curSet == null)
				{
					curSet = new HashSet<Object>();
					curSet.add(o);
					aggRow[index] = curSet;
				} else
				{
					curSet.add(o);
				}
			}
		} else
		{
			switch (type)
			{
			case MemgrammarParser.COUNT:
				if (o != null || countall)
				{
					AtomicInteger ai = null;
					ai = (AtomicInteger) aggRow[index];
					if (ai == null)
					{
						ai = (AtomicInteger) aggRow[index];
						if (ai == null)
						{
							ai = new AtomicInteger(0);
							aggRow[index] = ai;
						}
					}
					ai.incrementAndGet();
				}
				break;
			case MemgrammarParser.SUM:
			case MemgrammarParser.AVG:
				if (o != null)
				{
					if (type == MemgrammarParser.AVG)
					{
						if (aggRow[index] == null)
							aggRow[index] = new Object[2];
						AtomicInteger ai = null;
						ai = (AtomicInteger) ((Object[]) aggRow[index])[1];
						if (ai == null)
						{
							if (ai == null)
							{
								ai = new AtomicInteger(0);
								((Object[]) aggRow[index])[1] = ai;
							}
						}
						ai.incrementAndGet();
					}
					Double d = null;
					if (o instanceof Float)
					{
						d = ((Float) o).doubleValue();
					} else if (o instanceof Integer)
					{
						d = ((Integer) o).doubleValue();
					} else if (o instanceof Double)
					{
						d = ((Double) o);
					} else if (o instanceof Long)
					{
						d = ((Long) o).doubleValue();
					} else if (o instanceof BigDecimal)
					{
						BigDecimal bd = ((BigDecimal) o);
						if (bd != null)
						{
							BigDecimal bval = null;
							if (type == MemgrammarParser.AVG)
							{
								bval = (BigDecimal) ((Object[]) aggRow[index])[0];
								if (bval == null)
									((Object[]) aggRow[index])[0] = bd;
								else
									bval.add(bd);
							} else
							{
								bval = (BigDecimal) aggRow[index];
								if (bval == null)
									aggRow[index] = bd;
								else
									aggRow[index] = bval.add(bd);
							}
						}
						break;
					} else
						d = Double.NaN;
					if (!Double.isNaN(d))
					{
						DoubleValue dv = null;
						if (type == MemgrammarParser.AVG)
						{
							dv = (DoubleValue) ((Object[]) aggRow[index])[0];
							if (dv == null)
							{
								dv = (DoubleValue) ((Object[]) aggRow[index])[0];
								if (dv == null)
								{
									dv = new DoubleValue();
									((Object[]) aggRow[index])[0] = dv;
								}
							}
						} else
						{
							dv = (DoubleValue) aggRow[index];
							if (dv == null)
							{
								dv = (DoubleValue) aggRow[index];
								if (dv == null)
								{
									dv = new DoubleValue();
									aggRow[index] = dv;
								}
							}
						}
						dv.value += d;
					}
				}
				break;
			case MemgrammarParser.MIN:
				if (o != null)
				{
					Double d = null;
					if (o instanceof Float)
					{
						d = ((Float) o).doubleValue();
					} else if (o instanceof Integer)
					{
						d = ((Integer) o).doubleValue();
					} else if (o instanceof Long)
					{
						d = ((Long) o).doubleValue();
					} else if (o instanceof Double)
					{
						d = ((Double) o);
					} else if (o instanceof BigDecimal)
					{
						BigDecimal bd = ((BigDecimal) o);
						if (bd != null)
						{
							BigDecimal bval = (BigDecimal) aggRow[index];
							if (bval == null)
								aggRow[index] = bd;
							else
								aggRow[index] = bval.min(bd);
						}
						break;
					} else if (o instanceof Date)
					{
						Date dt = (Date) aggRow[index];
						if (dt == null)
						{
							aggRow[index] = o;
						} else if (((Date) o).compareTo(dt) < 0)
							aggRow[index] = o;
						break;
					} else if (o instanceof String)
					{
						String s = (String) aggRow[index];
						if (s == null)
						{
							aggRow[index] = o;
						} else if (o.toString().compareTo(s) < 0)
							aggRow[index] = o;
						break;
					} else
						d = 0.0;
					if (!Double.isNaN(d))
					{
						DoubleValue dv = null;
						dv = (DoubleValue) aggRow[index];
						if (dv == null)
						{
							dv = (DoubleValue) aggRow[index];
							if (dv == null)
							{
								dv = new DoubleValue();
								dv.value = d;
								aggRow[index] = dv;
							}
						}
						if (d < dv.value)
							dv.value = d;
					}
				}
				break;
			case MemgrammarParser.MAX:
				if (o != null)
				{
					Double d = null;
					if (o instanceof Float)
					{
						d = ((Float) o).doubleValue();
					} else if (o instanceof Integer)
					{
						d = ((Integer) o).doubleValue();
					} else if (o instanceof Long)
					{
						d = ((Long) o).doubleValue();
					} else if (o instanceof Double)
					{
						d = ((Double) o);
					} else if (o instanceof BigDecimal)
					{
						BigDecimal bd = ((BigDecimal) o);
						if (bd != null)
						{
							BigDecimal bval = (BigDecimal) aggRow[index];
							if (bval == null)
								aggRow[index] = bd;
							else
								aggRow[index] = bval.max(bd);
						}
						break;
					} else if (o instanceof String)
					{
						String s = (String) aggRow[index];
						if (s == null)
						{
							aggRow[index] = o;
						} else if (o.toString().compareTo(s) > 0)
							aggRow[index] = o;
						break;
					} else if (o instanceof Date)
					{
						Date dt = (Date) aggRow[index];
						if (dt == null)
						{
							aggRow[index] = o;
						} else if (((Date) o).compareTo(dt) > 0)
							aggRow[index] = o;
						break;
					} else
						d = 0.0;
					if (!Double.isNaN(d))
					{
						DoubleValue dv = null;
						dv = (DoubleValue) aggRow[index];
						if (dv == null)
						{
							dv = new DoubleValue();
							dv.value = d;
							aggRow[index] = dv;
						}
						if (d > dv.value)
							dv.value = d;
					}
				}
				break;
			}
		}
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		index = acount.getAndIncrement();
		return true;
	}

	@Override
	public int getType()
	{
		if (type == MemgrammarParser.COUNT)
			return Types.INTEGER;
		if (child.getType() == Types.INTEGER && type != MemgrammarParser.AVG)
			return Types.INTEGER;
		if (child.getType() == Types.DECIMAL)
			return Types.DECIMAL;
		if (type == MemgrammarParser.MAX || type == MemgrammarParser.MIN)
			return child.getType();
		return Types.DOUBLE;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		DoubleValue dv = null;
		BigDecimal bd = null;
		Object o = null;
		if (toConsolidateAggRow[index] == null)
			return;
		if (distinct)
		{
			Set<Object> curSet = (Set<Object>) primaryAggRow[index];
			if (curSet == null)
			{
				curSet = new HashSet<Object>();
				primaryAggRow[index] = curSet;
			}
			for (Object to : (Set<Object>) toConsolidateAggRow[index])
				curSet.add(to);
		} else
		{
			switch (type)
			{
			case MemgrammarParser.COUNT:
				AtomicInteger ai1 = (AtomicInteger) primaryAggRow[index];
				AtomicInteger ai2 = (AtomicInteger) toConsolidateAggRow[index];
				if (ai1 == null)
				{
					ai1 = new AtomicInteger(0);
					primaryAggRow[index] = ai1;
				}
				if (ai2 != null)
					ai1.addAndGet(ai2.get());
				break;
			case MemgrammarParser.SUM:
			case MemgrammarParser.AVG:
				o = toConsolidateAggRow[index];
				if (o != null)
				{
					if (type == MemgrammarParser.AVG)
					{
						if (primaryAggRow[index] == null)
							primaryAggRow[index] = new Object[2];
						ai1 = (AtomicInteger) ((Object[]) primaryAggRow[index])[1];
						ai2 = (AtomicInteger) ((Object[]) toConsolidateAggRow[index])[1];
						if (ai1 == null)
						{
							ai1 = new AtomicInteger(0);
							((Object[]) primaryAggRow[index])[1] = ai1;
						}
						if (ai2 != null)
							ai1.addAndGet(ai2.get());
					}
					if (type == MemgrammarParser.AVG)
					{
						if (o instanceof BigDecimal)
						{
							bd = (BigDecimal) ((Object[]) primaryAggRow[index])[0];
							if (bd == null)
								((Object[]) primaryAggRow[index])[0] = (BigDecimal) toConsolidateAggRow[index];
							else
								((Object[]) primaryAggRow[index])[0] = bd.add((BigDecimal) toConsolidateAggRow[index]);
						} else
						{
							dv = (DoubleValue) ((Object[]) primaryAggRow[index])[0];
							if (dv == null)
							{
								dv = new DoubleValue();
								((Object[]) primaryAggRow[index])[0] = dv;
							}
							dv.value += ((DoubleValue) ((Object[]) toConsolidateAggRow[index])[0]).value;
						}
					} else
					{
						if (o instanceof BigDecimal)
						{
							bd = (BigDecimal) primaryAggRow[index];
							if (bd == null)
								primaryAggRow[index] = (BigDecimal) toConsolidateAggRow[index];
							else
								primaryAggRow[index] = bd.add((BigDecimal) toConsolidateAggRow[index]);
						} else
						{
							dv = (DoubleValue) primaryAggRow[index];
							if (dv == null)
							{
								dv = new DoubleValue();
								primaryAggRow[index] = dv;
							}
							dv.value += ((DoubleValue) toConsolidateAggRow[index]).value;
						}
					}
				}
				break;
			case MemgrammarParser.MIN:
				o = toConsolidateAggRow[index];
				if (o != null)
				{
					Double d = null;
					if (o instanceof DoubleValue)
					{
						d = ((DoubleValue) o).value;
					} else if (o instanceof BigDecimal)
					{
						bd = ((BigDecimal) o);
						if (bd != null)
						{
							BigDecimal bval = (BigDecimal) primaryAggRow[index];
							if (bval == null)
								primaryAggRow[index] = bd;
							else
								primaryAggRow[index] = bval.min(bd);
						}
						break;
					} else if (o instanceof Date)
					{
						Date dt = (Date) primaryAggRow[index];
						if (dt == null)
						{
							primaryAggRow[index] = o;
						} else if (((Date) o).compareTo(dt) < 0)
							primaryAggRow[index] = o;
						break;
					} else if (o instanceof String)
					{
						String s = (String) primaryAggRow[index];
						if (s == null)
						{
							primaryAggRow[index] = o;
						} else if (o.toString().compareTo(s) < 0)
							primaryAggRow[index] = o;
						break;
					} else
						d = 0.0;
					dv = (DoubleValue) primaryAggRow[index];
					if (dv == null)
					{
						dv = new DoubleValue();
						dv.value = d;
						primaryAggRow[index] = dv;
					}
					if (d < dv.value)
						dv.value = d;
				}
				break;
			case MemgrammarParser.MAX:
				o = toConsolidateAggRow[index];
				if (o != null)
				{
					Double d = null;
					if (o instanceof DoubleValue)
					{
						d = ((DoubleValue) o).value;
					} else if (o instanceof BigDecimal)
					{
						bd = ((BigDecimal) o);
						if (bd != null)
						{
							BigDecimal bval = (BigDecimal) primaryAggRow[index];
							if (bval == null)
								primaryAggRow[index] = bd;
							else
								primaryAggRow[index] = bval.max(bd);
						}
						break;
					} else if (o instanceof Date)
					{
						Date dt = (Date) primaryAggRow[index];
						if (dt == null)
						{
							primaryAggRow[index] = o;
						} else if (((Date) o).compareTo(dt) > 0)
							primaryAggRow[index] = o;
						break;
					} else if (o instanceof String)
					{
						String s = (String) primaryAggRow[index];
						if (s == null)
						{
							primaryAggRow[index] = o;
						} else if (o.toString().compareTo(s) > 0)
							primaryAggRow[index] = o;
						break;
					} else
						d = 0.0;
					dv = (DoubleValue) primaryAggRow[index];
					if (dv == null)
					{
						dv = new DoubleValue();
						dv.value = d;
						primaryAggRow[index] = dv;
					}
					if (d > dv.value)
						dv.value = d;
				}
				break;
			}
		}
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
