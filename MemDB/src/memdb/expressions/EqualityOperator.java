package memdb.expressions;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TByteList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.Command;
import memdb.DateParser;
import memdb.DateUtil;
import memdb.jdbcserver.RemoteResultSetImpl;
import memdb.rowset.RowSet;
import memdb.rowset.RowSet.RowSetType;
import memdb.rowset.RowSetIterator;
import memdb.sql.MemgrammarParser;
import memdb.sql.QueryPosition;
import memdb.sql.QueryResultRow;
import memdb.sql.Select;
import memdb.sql.SQLUtil;
import memdb.sql.WhereStatement;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;

@SuppressWarnings("unused")
public class EqualityOperator extends Expression
{
	private Expression left;
	private Expression right;
	private List<Expression> inexps;
	private DateParser dp1 = new DateParser();
	private DateParser dp2 = new DateParser();
	private static int OVER_THREAD = 4;
	private boolean caseSensitive = true;
	private WhereStatement wstmt;
	private boolean baseRowsParallel = false;

	public EqualityOperator(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		this.wstmt = wstmt;
		this.caseSensitive = wstmt.getDb().caseSensitive;
		left = Expression.parseExpression(wstmt, t.getChild(0));
		if (t.getType() == MemgrammarParser.IN || t.getType() == MemgrammarParser.NOTIN)
		{
			inexps = new ArrayList<Expression>();
			for (int i = 1; i < t.getChildCount(); i++)
			{
				inexps.add(Expression.parseExpression(wstmt, t.getChild(i)));
			}
		} else if (t.getChild(1).getType() == MemgrammarParser.IN)
		{
			return;
		}
		right = Expression.parseExpression(wstmt, t.getChild(1));
		baseRowsParallel = true;
		switch (type)
		{
		case MemgrammarParser.LIKE:
		case MemgrammarParser.NOTLIKE:
			if (right.getType() != Types.VARCHAR)
			{
				throw new SQLException("Invalid LIKE/NOTLIKE");
			}
		}
	}

	private Object getComparisonValue(int type, Object value)
	{
		try
		{
			if (type == Types.FLOAT)
			{
				if (!(value instanceof Float))
					if (value instanceof Integer)
						value = ((Integer) value).floatValue();
					else if (value instanceof Long)
						value = ((Long) value).floatValue();
					else if (value instanceof Double)
						value = ((Double) value).floatValue();
					else
						value = Float.valueOf(value.toString());
			} else if (type == Types.DOUBLE)
			{
				if (!(value instanceof Double))
					if (value instanceof Integer)
						value = ((Integer) value).doubleValue();
					else if (value instanceof Long)
						value = ((Long) value).doubleValue();
					else if (value instanceof Float)
						value = ((Double) value).doubleValue();
					else
						value = Float.valueOf(value.toString());
			} else if (type == Types.INTEGER)
			{
				if (!(value instanceof Integer))
					if (value instanceof Long)
						value = ((Long) value).intValue();
					else if (value instanceof Double)
						value = ((Double) value).intValue();
					else if (value instanceof Float)
						value = ((Float) value).intValue();
					else
						value = Integer.valueOf(value.toString());
			} else if (type == Types.BIGINT)
			{
				if (!(value instanceof Long))
				{
					if (value instanceof Integer)
						value = ((Integer) value).longValue();
					else if (value instanceof Float)
						value = ((Float) value).longValue();
					else if (value instanceof Double)
						value = ((Double) value).longValue();
					else
						value = Long.valueOf(value.toString());
				}
			} else if (type == Types.DECIMAL)
			{
				if (value != null && !(value instanceof BigDecimal))
				{
					if (value instanceof Integer)
						value = new BigDecimal((Integer) value);
					else if (value instanceof Float)
						value = new BigDecimal((Float) value);
					else if (value instanceof Double)
						value = new BigDecimal((Double) value);
					else
						value = BigDecimal.valueOf(Long.valueOf(value.toString()));
				}
			} else if (type == Types.DATE || type == Types.TIMESTAMP)
			{
				if (!(value instanceof Date))
					value = dp1.parseUsingPossibleFormats(value.toString(), wstmt.getCommand().getClientTimeZone());
			} else if (type == Types.VARCHAR)
			{
				value = value.toString();
			}
		} catch (NumberFormatException nfe)
		{
			logger.error("Unable to parse number value: " + value);
			return null;
		}
		return value;
	}

	@Override
	public RowSet getBaseRows(RowSet baseset) throws SQLException, UnsupportedException
	{
		Table table = null;
		Tree identTree = null;
		List<Object> values = null;
		try
		{
			if (t.getType() == MemgrammarParser.IN || t.getType() == MemgrammarParser.NOTIN)
			{
				table = wstmt.findIdentifierTable(t.getChild(0)).table;
				identTree = t.getChild(0);
				values = new ArrayList<Object>();
				for (int i = 0; i < inexps.size(); i++)
				{
					if (inexps.get(i) == null)
						continue;
					values.add(inexps.get(i).evaluate(null));
				}
				// Placeholder if doing a set-based filter
				if (t.getChild(1).getType() == MemgrammarParser.SELECT)
					values.add(null);
			} else if (t.getChild(0).getType() == MemgrammarParser.IDENT)
			{
				table = wstmt.findIdentifierTable(t.getChild(0)).table;
				identTree = t.getChild(0);
				if (right != null)
				{
					values = new ArrayList<Object>();
					Object val = right.evaluate(null);
					if (val != null)
						values.add(val);
					else
						throw new UnsupportedException();
				}
			} else if (t.getChild(1).getType() == MemgrammarParser.IDENT)
			{
				table = wstmt.findIdentifierTable(t.getChild(1)).table;
				identTree = t.getChild(1);
				values = new ArrayList<Object>();
				Object val = left.evaluate(null);
				if (val != null)
					values.add(val);
				else
					throw new UnsupportedException();
			} else
				throw new UnsupportedException();
		} catch (SQLException e)
		{
			logger.error(e);
			return null;
		}
		if (table == null || values == null || table.getNumRows() == 0)
			return null;
		int index = table.tmd.getColumnIndex(identTree.getText());
		if (index < 0)
		{
			return null;
		}
		RowSet rList = null;
		for (Object value : values)
		{
			if (value != null)
				value = this.getComparisonValue(table.tmd.dataTypes[index], value);
			RowSet rows = null;
			RowSet rowList = null;
			switch (type)
			{
			case MemgrammarParser.EQUALS:
			case MemgrammarParser.IN:
				if (t.getChild(1).getType() == MemgrammarParser.SELECT)
				{
					try
					{
						Select subselect;
						try
						{
							subselect = new Select(new Command(-1, wstmt.getCommand().getClientTimeZone()), wstmt.getDb(), t.getChild(1), null, null);
						} catch (InterruptedException e)
						{
							throw new SQLException("Unable to select from table");
						}
						subselect.setInInClause(true);
						RemoteResultSetImpl impl = subselect.process();
						if (impl.getMetaData().getColumnCount() > 0)
						{
							if (rList != null)
								rows = rList;
							else
								rows = new RowSet(RowSetType.BitSet, table.getNumRows());
							QueryResultRow[] results;
							results = impl.getResults(true);
							if (results == null)
								continue;
							EqualityInThread eit = new EqualityInThread(results, table, baseset, rows, index);
							eit.run();
						}
						if (rList == null)
							rList = rows;
						continue;
					} catch (SQLException e)
					{
						throw e;
					}
				}
				rowList = table.getRowList(index, value);
				if (rowList != null)
				{
					int rlsize = rowList.size();
					if (baseset != null)
					{
						if (rList == null)
							rows = new RowSet(new BitSet(table.getNumRows()));
						else
							rows = rList;
						RowSetIterator rsit = rowList.getIterator();
						int rnum = 0;
						while ((rnum = rsit.getNextRow()) >= 0)
						{
							if (baseset.contains(rnum))
								rows.add(rnum);
						}
						rows.addAll(rowList);
					} else
					{
						if (rList == null)
							rows = new RowSet(new BitSet(table.getNumRows()));
						else
							rows = rList;
						RowSetIterator rsit = rowList.getIterator();
						int rnum = 0;
						while ((rnum = rsit.getNextRow()) >= 0)
							rows.add(rnum);
					}
					if (rList == null)
						rList = rows;
				}
				continue;
			case MemgrammarParser.NOTEQUALS:
			case MemgrammarParser.NOTIN:
				rowList = table.getRowList(index, value);
				if (rowList != null)
				{
					/*
					 * If the filter isn't sufficiently selective (more than 80%), defer to after the join
					 */
					if (rowList.size() > table.getNumRows() / 5)
						throw new UnsupportedException();
					rows = new RowSet(table.getNumRows());
					if (rList != null)
					{
						if (baseset != null)
						{
							rList.getIntersection(baseset, -1);
						} else
						{
							baseset = rList;
						}
					}
					int numrows = table.getNumRows();
					if (baseset == null)
					{
						for (int i = 0; i < numrows; i++)
						{
							if (!rowList.contains(i))
								rows.add(i);
						}
					} else
					{
						for (int i = 0; i < numrows; i++)
						{
							if (baseset.contains(i) && !rowList.contains(i))
								rows.add(i);
						}
					}
					rList = rows;
					continue;
				}
				/*
				 * Rather than building a list of all rows in the table, just invalidate the filter and ignore it until
				 * after
				 */
				throw new UnsupportedException();
			case MemgrammarParser.LT:
			case MemgrammarParser.GT:
			case MemgrammarParser.LTEQ:
			case MemgrammarParser.GTEQ:
			case MemgrammarParser.LIKE:
			case MemgrammarParser.NOTLIKE:
				rows = new RowSet(RowSetType.BitSet, table.getNumRows());
				InequalityTest it = new InequalityTest(table, index, t, value, rows, null);
				it.run();
				if (baseset != null)
				{
					baseset.getBitSet(table.getNumRows());
					rows = rows.getIntersection(baseset, table.getNumRows());
				}
				if (rList == null)
					rList = rows;
				else
					rList.addAll(rows);
				continue;
			}
		}
		return rList;
	}
	private QueryResultRow[] results = null;

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		switch (type)
		{
		case MemgrammarParser.EQUALS:
			return left.equals(right, rowset);
		case MemgrammarParser.NOTEQUALS:
			return !left.equals(right, rowset);
		case MemgrammarParser.GT:
			return left.compareTo(right, rowset) > 0;
		case MemgrammarParser.GTEQ:
			return left.compareTo(right, rowset) >= 0;
		case MemgrammarParser.LT:
			return left.compareTo(right, rowset) < 0;
		case MemgrammarParser.LTEQ:
			return left.compareTo(right, rowset) <= 0;
		case MemgrammarParser.LIKE:
			Object oa = left.evaluate(rowset);
			Object ob = right == null ? null : right.evaluate(rowset);
			if (!caseSensitive)
			{
				return SQLUtil.like(oa.toString().toUpperCase(), ob.toString().toUpperCase());
			} else
			{
				return SQLUtil.like(oa.toString(), ob.toString());
			}
		case MemgrammarParser.NOTLIKE:
			oa = left.evaluate(rowset);
			ob = right == null ? null : right.evaluate(rowset);
			if (!caseSensitive)
			{
				return !SQLUtil.like(oa.toString().toUpperCase(), ob.toString().toUpperCase());
			} else
			{
				return !SQLUtil.like(oa.toString().toUpperCase(), ob.toString().toUpperCase());
			}
		case MemgrammarParser.IN:
		case MemgrammarParser.NOTIN:
			if (t.getChild(1).getType() != MemgrammarParser.SELECT)
			{
				boolean found = false;
				Object value = left.evaluate(rowset);
				boolean caseInsensitive = !wstmt.getDb().caseSensitive;
				if (left.getType() == Types.VARCHAR && value instanceof String)
				{
					value = ((String) value).trim();
					if (caseInsensitive)
						value = ((String) value).toUpperCase();
				}
				if (value != null)
				{
					for (int i = 0; i < inexps.size(); i++)
					{
						Object expValue = inexps.get(i).evaluate(rowset);
						if (caseInsensitive && expValue instanceof String)
						{
							expValue = ((String) expValue).toUpperCase();
						}
						if (value.equals(expValue))
						{
							found = true;
							break;
						}
					}
				}
				if (type == MemgrammarParser.IN)
					return found;
				else
					return !found;
			}
			Select subselect = null;
			if (results == null)
			{
				try
				{
					subselect = new Select(new Command(-1, wstmt.getCommand().getClientTimeZone()), wstmt.getDb(), t.getChild(1), null, null);
				} catch (InterruptedException e)
				{
					throw new SQLException("Unable to select from table");
				}
				RemoteResultSetImpl impl = subselect.process();
				if (impl != null && impl.getMetaData().getColumnCount() > 0)
					results = impl.getResults(true);
			}
			if (results != null)
			{
				boolean found = false;
				Object value = left.evaluate(rowset);
				for (QueryResultRow qr : results)
				{
					if (value.equals(qr.dataRow[0]))
					{
						found = true;
						break;
					}
				}
				if (type == MemgrammarParser.IN)
					return found;
				else
					return !found;
			}
		}
		return false;
	}

	@Override
	public void getTables(Set<QueryPosition> tableSet)
	{
		left.getTables(tableSet);
		if (right != null)
			right.getTables(tableSet);
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		return left.containsAggregate(acount) || (right != null && right.containsAggregate(acount));
	}

	@Override
	public int getType()
	{
		return Types.BOOLEAN;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		return left.containsNonAggregate() || (right != null && right.containsNonAggregate());
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		left.setAggRow(aggRow);
		if (right != null)
			right.setAggRow(aggRow);
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		left.aggregate(aggRow, rowset);
		if (right != null)
			right.aggregate(aggRow, rowset);
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		left.consolidate(primaryAggRow, toConsolidateAggRow);
		if (right != null)
			right.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return baseRowsParallel;
	}
}
