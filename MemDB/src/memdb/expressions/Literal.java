package memdb.expressions;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.DateParser;
import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class Literal extends Expression
{
	private Object o = null;
	private WhereStatement wstmt;

	public Literal(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		this.wstmt = wstmt;
		if (t.getType() == MemgrammarParser.PARAMETER && wstmt.getParameters() == null)
			throw new SQLException("Invalid use of parameter in unprepared statement");
	}

	public void setDate(Date d)
	{
		this.o = d;
	}

	@Override
	public RowSet getBaseRows(RowSet baseset)
	{
		return null;
	}

	@Override
	public Object evaluate(int[] rowset)
	{
		if (type == MemgrammarParser.NULL)
		{
			return null;
		}
		if (o != null)
			return o;
		if (type == MemgrammarParser.PARAMETER)
		{
			Object[] parameters = wstmt.getParameters();
			if (parameters == null)
				return null;
			o = wstmt.getNextParameter();
			return o;
		}
		if (type == MemgrammarParser.STRING)
		{
			o = text.substring(1, text.length() - 1);
			return o;
		} else if (type == MemgrammarParser.DATETIME)
		{
			String s = text.substring(1, text.length() - 1);
			DateParser dp = new DateParser();
			o = dp.parseUsingPossibleFormats(s, wstmt.getCommand().getClientTimeZone());
			return o;
		} else if (type == MemgrammarParser.INTEGER)
		{
			o = Integer.valueOf(text);
			return o;
		} else if (type == MemgrammarParser.FLOAT)
		{
			o = Double.valueOf(text);
			return o;
		} else if (type == MemgrammarParser.BOOLEAN)
		{
			o = Boolean.valueOf(text);
			return o;
		} else
			return text;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		return false;
	}

	@Override
	public int getType()
	{
		switch (type)
		{
		case MemgrammarParser.INTEGER:
			return Types.INTEGER;
		case MemgrammarParser.STRING:
			return Types.VARCHAR;
		case MemgrammarParser.FLOAT:
			return Types.DOUBLE;
		case MemgrammarParser.DATETIME:
			return Types.DATE;
		case MemgrammarParser.BOOLEAN:
			return Types.BOOLEAN;
		}
		return Types.VARCHAR;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		return false;
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
