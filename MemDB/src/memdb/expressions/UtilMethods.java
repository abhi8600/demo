package memdb.expressions;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.DateParser;
import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;
import memdb.util.Util;

import org.antlr.runtime.tree.Tree;

public class UtilMethods extends Expression
{
	Expression par1;
	Expression left;
	Expression right;
	List<Expression> elist;
	int totype;

	public static UtilMethods CreateCast(WhereStatement wstmt, Expression input, int resultType) throws SQLException
	{
		UtilMethods um = new UtilMethods(wstmt, null);
		um.par1 = input;
		um.totype = resultType;
		return um;
	}

	public UtilMethods(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		switch (type)
		{
		case MemgrammarParser.CAST:
			par1 = Expression.parseExpression(wstmt, t.getChild(0));
			totype = t.getChild(1).getType();
			break;
		case MemgrammarParser.ISNULLEXP:
			left = Expression.parseExpression(wstmt, t.getChild(0));
			right = Expression.parseExpression(wstmt, t.getChild(1));
			int leftType = left.getType();
			int rightType = right.getType();
			if (leftType != rightType)
			{
				boolean ok = false;
				if (leftType == Types.BIGINT && rightType == Types.INTEGER)
					ok = true;
				else if (rightType == Types.BIGINT && leftType == Types.INTEGER)
					ok = true;
				else if (rightType == Types.VARCHAR)
				{
					right = UtilMethods.CreateCast(wstmt, right, leftType);
					ok = true;
				}
				if (!ok)
				{
					throw new SQLException("ISNULL must contain parameters of the same type");
				}
			}
			break;
		case MemgrammarParser.COALESCE:
			elist = new ArrayList<Expression>();
			for (int i = 0; i < t.getChildCount(); i++)
				elist.add(Expression.parseExpression(wstmt, t.getChild(i)));
			break;
		case MemgrammarParser.OBJECTSIZE:
			// Return the size of a table
			left = Expression.parseExpression(wstmt, t.getChild(0));
			break;
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset) throws SQLException
	{
		return null;
	}

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		try
		{
			switch (type)
			{
			case MemgrammarParser.CAST:
				Object o = par1.evaluate(rowset);
				if (o == null)
					return null;
				if (totype == MemgrammarParser.VARCHARTYPE)
					return o.toString();
				if (totype == MemgrammarParser.DATETYPE || totype == MemgrammarParser.DATETIMETYPE)
				{
					DateParser dp = new DateParser();
					return dp.parseUsingPossibleFormats(o.toString(), wstmt.getCommand().getClientTimeZone());
				}
				try
				{
					if (totype == MemgrammarParser.BIGINTTYPE)
						return Double.valueOf(o.toString()).longValue();
					if (totype == MemgrammarParser.INTEGERTYPE)
						return Double.valueOf(o.toString()).intValue();
					if (totype == MemgrammarParser.FLOATTYPE)
						return Double.valueOf(o.toString());
					if (totype == MemgrammarParser.DECIMALTYPE)
						return BigDecimal.valueOf(Double.valueOf(o.toString()));
				} catch (NumberFormatException ex)
				{
					if (totype == MemgrammarParser.BIGINTTYPE)
						return 0L;
					if (totype == MemgrammarParser.INTEGERTYPE)
						return 0;
					if (totype == MemgrammarParser.FLOATTYPE)
						return 0.0D;
					if (totype == MemgrammarParser.DECIMALTYPE)
						return new BigDecimal(0.0);
				}
				break;
			case MemgrammarParser.ISNULLEXP:
				Object lefto = left.evaluate(rowset);
				if (lefto == null)
					return right.evaluate(rowset);
				return lefto;
			case MemgrammarParser.COALESCE:
				for (int i = 0; i < elist.size(); i++)
				{
					o = elist.get(i).evaluate(rowset);
					if (o != null)
						return o;
				}
				return null;
			case MemgrammarParser.OBJECTSIZE:
				lefto = left.evaluate(rowset);
				if (lefto == null || !(lefto instanceof String))
					return null;
				String s = (String) lefto;
				String[] parts = s.split("[.]");
				if (parts.length == 2)
				{
					File f = new File(wstmt.getDb().path + File.separator + parts[0] + File.separator + parts[1]);
					long len = f.exists() ? Util.getFileSize(f) : 0;
					return len;
				}
				return (long) 0;
			}
		} catch (Exception ex)
		{
			throw new SQLException(ex.getMessage());
		}
		return null;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		if (par1 != null && par1.containsAggregate(acount))
			return true;
		if (left != null && left.containsAggregate(acount))
			return true;
		if (right != null && right.containsAggregate(acount))
			return true;
		if (elist != null)
		{
			for (int i = 0; i < elist.size(); i++)
				if (elist.get(i).containsAggregate(acount))
					return true;
		}
		return false;
	}

	@Override
	public int getType()
	{
		switch (type)
		{
		case MemgrammarParser.CAST:
			switch (totype)
			{
			case MemgrammarParser.VARCHARTYPE:
				return Types.VARCHAR;
			case MemgrammarParser.INTEGERTYPE:
				return Types.INTEGER;
			case MemgrammarParser.FLOATTYPE:
				return Types.DOUBLE;
			case MemgrammarParser.DATETYPE:
				return Types.DATE;
			case MemgrammarParser.DATETIMETYPE:
				return Types.TIMESTAMP;
			case MemgrammarParser.BIGINTTYPE:
				return Types.BIGINT;
			case MemgrammarParser.DECIMALTYPE:
				return Types.DECIMAL;
			}
			return Types.VARCHAR;
		case MemgrammarParser.ISNULLEXP:
			int leftType = left.getType();
			int rightType = right.getType();
			if (leftType != rightType)
			{
				if (leftType == Types.BIGINT && rightType == Types.INTEGER)
					return Types.BIGINT;
				else if (rightType == Types.BIGINT && leftType == Types.INTEGER)
					return Types.BIGINT;
			}
			return leftType;
		case MemgrammarParser.COALESCE:
			return elist.get(0).getType();
		case MemgrammarParser.OBJECTSIZE:
			return Types.BIGINT;
		}
		return Types.VARCHAR;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		if (par1 != null && par1.containsNonAggregate())
			return true;
		if (left != null && left.containsNonAggregate())
			return true;
		if (right != null && right.containsNonAggregate())
			return true;
		if (elist != null)
		{
			for (int i = 0; i < elist.size(); i++)
				if (elist.get(i).containsNonAggregate())
					return true;
		}
		return false;
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		if (par1 != null)
			par1.setAggRow(aggRow);
		if (left != null)
			left.setAggRow(aggRow);
		if (right != null)
			right.setAggRow(aggRow);
		if (elist != null)
		{
			for (int i = 0; i < elist.size(); i++)
				elist.get(i).setAggRow(aggRow);
		}
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		if (par1 != null)
			par1.aggregate(aggRow, rowset);
		if (left != null)
			left.aggregate(aggRow, rowset);
		if (right != null)
			right.aggregate(aggRow, rowset);
		if (elist != null)
		{
			for (int i = 0; i < elist.size(); i++)
				elist.get(i).aggregate(aggRow, rowset);
		}
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		if (par1 != null)
			par1.consolidate(primaryAggRow, toConsolidateAggRow);
		if (left != null)
			left.consolidate(primaryAggRow, toConsolidateAggRow);
		if (right != null)
			right.consolidate(primaryAggRow, toConsolidateAggRow);
		if (elist != null)
		{
			for (int i = 0; i < elist.size(); i++)
				elist.get(i).consolidate(primaryAggRow, toConsolidateAggRow);
		}
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
