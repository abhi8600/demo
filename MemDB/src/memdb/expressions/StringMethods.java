package memdb.expressions;

import java.sql.SQLException;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class StringMethods extends Expression
{
	Expression str;
	Expression par1;
	Expression par2;

	public StringMethods(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		switch (type)
		{
		case MemgrammarParser.SUBSTR:
			str = Expression.parseExpression(wstmt, t.getChild(0));
			if (str.getType() != Types.VARCHAR)
				throw new SQLException("Must supply a Varchar as the first parameter to substring");
			par1 = Expression.parseExpression(wstmt, t.getChild(1));
			if (par1.getType() != Types.INTEGER)
				throw new SQLException("Must supply an Integer as the second parameter to substring");
			if (t.getChildCount() > 2)
			{
				par2 = Expression.parseExpression(wstmt, t.getChild(2));
				if (par2.getType() != Types.INTEGER)
					throw new SQLException("Must supply an Integer as the third parameter to substring");
			}
			break;
		case MemgrammarParser.TOUPPER:
		case MemgrammarParser.TOLOWER:
			str = Expression.parseExpression(wstmt, t.getChild(0));
			if (str.getType() != Types.VARCHAR)
				throw new SQLException("Must supply a Varchar as the parameter to toupper/tolower");
			break;
		case MemgrammarParser.LENGTH:
		case MemgrammarParser.LEN:
			str = Expression.parseExpression(wstmt, t.getChild(0));
			if (str.getType() != Types.VARCHAR)
				throw new SQLException("Must supply a Varchar as the parameter to length");
			break;
		case MemgrammarParser.POSITION:
			str = Expression.parseExpression(wstmt, t.getChild(0));
			if (str.getType() != Types.VARCHAR)
				throw new SQLException("Must supply a Varchar as the first parameter to substring");
			par1 = Expression.parseExpression(wstmt, t.getChild(1));
			if (par1.getType() != Types.VARCHAR)
				throw new SQLException("Must supply a Varchar as the second parameter to position");
			break;
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset) throws SQLException
	{
		return null;
	}

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		String s = null;
		Object o = null;
		switch (type)
		{
		case MemgrammarParser.SUBSTR:
			o = str.evaluate(rowset);
			if (o == null)
				return null;
			if (o instanceof String)
				s = (String) o;
			else
				s = o.toString();
			o = par1.evaluate(rowset);
			int i1 = 0;
			if (o instanceof Integer)
				i1 = (Integer) o;
			int i2 = 0;
			if (par2 != null)
			{
				o = par2.evaluate(rowset);
				if (o instanceof Integer)
					i2 = (Integer) o;
				int start = i1 - 1;
				int end1 = s.length() - 1;
				int end2 = i2 + start;
				return s.substring(start < 0 ? 0 : start, end2 < end1 ? end2 : end1);
			} else
			{
				int start = i1 - 1;
				return s.substring(start < 0 ? 0 : start);
			}
		case MemgrammarParser.TOUPPER:
			o = str.evaluate(rowset);
			if (o == null)
				return null;
			if (o instanceof String)
				s = (String) o;
			else
				s = o.toString();
			return s.toUpperCase();
		case MemgrammarParser.TOLOWER:
			o = str.evaluate(rowset);
			if (o == null)
				return null;
			if (o instanceof String)
				s = (String) o;
			else
				s = o.toString();
			return s.toLowerCase();
		case MemgrammarParser.LENGTH:
			o = str.evaluate(rowset);
			if (o == null)
				return 0;
			if (o instanceof String)
				s = (String) o;
			else
				s = o.toString();
			return s.length();
		case MemgrammarParser.POSITION:
			o = str.evaluate(rowset);
			if (o == null)
				return -1;
			if (o instanceof String)
				s = (String) o;
			else
				s = o.toString();
			Object o2 = par1.evaluate(rowset);
			String s2 = null;
			if (o instanceof String)
				s2 = (String) o2;
			if (o2 == null)
				return -1;
			else
				s2 = o2.toString();
			return s2.indexOf(s);
		}
		return null;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		return false;
	}

	@Override
	public int getType()
	{
		switch (type)
		{
		case MemgrammarParser.SUBSTR:
		case MemgrammarParser.TOUPPER:
		case MemgrammarParser.TOLOWER:
			return Types.VARCHAR;
		case MemgrammarParser.LENGTH:
		case MemgrammarParser.LEN:
		case MemgrammarParser.POSITION:
			return Types.INTEGER;
		}
		return Types.VARCHAR;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		if (str != null && !str.containsNonAggregate())
			return true;
		if (par1 != null && !par1.containsNonAggregate())
			return true;
		if (par2 != null && !par2.containsNonAggregate())
			return true;
		return false;
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		if (str != null)
			str.setAggRow(aggRow);
		if (par1 != null)
			par1.setAggRow(aggRow);
		if (par2 != null)
			par2.setAggRow(aggRow);
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		if (str != null)
			str.aggregate(aggRow, rowset);
		if (par1 != null)
			par1.aggregate(aggRow, rowset);
		if (par2 != null)
			par2.aggregate(aggRow, rowset);
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		if (str != null)
			str.consolidate(primaryAggRow, toConsolidateAggRow);
		if (par1 != null)
			par1.consolidate(primaryAggRow, toConsolidateAggRow);
		if (par2 != null)
			par2.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
