package memdb.expressions;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class Case extends Expression
{
	private List<Expression> whenexps = new ArrayList<Expression>();
	private List<Expression> thenexps = new ArrayList<Expression>();
	private Expression elseexp;
	private int dataType = Integer.MIN_VALUE;

	public Case(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		for (int i = 0; i < t.getChildCount(); i++)
		{
			Tree child = t.getChild(i);
			if (child.getType() != MemgrammarParser.WHEN && i == t.getChildCount() - 1)
			{
				elseexp = Expression.parseExpression(wstmt, child);
				if (elseexp != null)
				{
					if (dataType == Integer.MIN_VALUE)
						dataType = elseexp.getType();
					else if (!memdb.sql.SQLUtil.compatibleTypes(dataType, elseexp.getType()))
					{
						if (dataType == Types.VARCHAR || elseexp.getType() == Types.VARCHAR)
						{
							elseexp = UtilMethods.CreateCast(wstmt, elseexp, dataType);
						} else
							throw new SQLException("Incompatible return types in CASE/WHEN");
					}
				}
			} else
			{
				Expression whenexp = Expression.parseExpression(wstmt, child.getChild(0));
				if (whenexp.getType() != Types.BOOLEAN)
					throw new SQLException("Invalid condition in CASE/WHEN");
				Expression thenexp = Expression.parseExpression(wstmt, child.getChild(1));
				if (dataType == Integer.MIN_VALUE)
					dataType = thenexp.getType();
				else if (!memdb.sql.SQLUtil.compatibleTypes(dataType, thenexp.getType()))
				{
					if (dataType == Types.VARCHAR || thenexp.getType() == Types.VARCHAR)
					{
						thenexp = UtilMethods.CreateCast(wstmt, thenexp, dataType);
					} else
						throw new SQLException("Incompatible return types in CASE/WHEN");
				}
				whenexps.add(whenexp);
				thenexps.add(thenexp);
			}
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset)
	{
		return null;
	}

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		for (int i = 0; i < whenexps.size(); i++)
		{
			if ((Boolean) whenexps.get(i).evaluate(rowset))
				return thenexps.get(i).evaluate(rowset);
		}
		if (elseexp != null)
			return elseexp.evaluate(rowset);
		return null;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		for (int i = 0; i < whenexps.size(); i++)
		{
			if (whenexps.get(i).containsAggregate(acount))
				return true;
			if (thenexps.get(i).containsAggregate(acount))
				return true;
		}
		if (elseexp != null && elseexp.containsAggregate(acount))
			return true;
		return false;
	}

	@Override
	public int getType()
	{
		return dataType;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		for (int i = 0; i < whenexps.size(); i++)
		{
			if (whenexps.get(i).containsNonAggregate())
				return true;
			if (thenexps.get(i).containsNonAggregate())
				return true;
		}
		if (elseexp != null && elseexp.containsNonAggregate())
			return true;
		return false;
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		for (int i = 0; i < whenexps.size(); i++)
		{
			whenexps.get(i).setAggRow(aggRow);
			thenexps.get(i).setAggRow(aggRow);
		}
		if (elseexp != null)
			elseexp.setAggRow(aggRow);
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		for (int i = 0; i < whenexps.size(); i++)
		{
			whenexps.get(i).aggregate(aggRow, rowset);
			thenexps.get(i).aggregate(aggRow, rowset);
		}
		if (elseexp != null)
			elseexp.aggregate(aggRow, rowset);
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		for (int i = 0; i < whenexps.size(); i++)
		{
			whenexps.get(i).consolidate(primaryAggRow, toConsolidateAggRow);
			thenexps.get(i).consolidate(primaryAggRow, toConsolidateAggRow);
		}
		if (elseexp != null)
			elseexp.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
