package memdb.expressions;

import java.sql.SQLException;
import java.sql.Types;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.rowset.RowSet;
import memdb.rowset.RowSet.RowSetType;
import memdb.rowset.RowSetIterator;
import memdb.sql.MemgrammarParser;
import memdb.sql.QueryPosition;
import memdb.sql.WhereStatement;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;

public class BooleanOperator extends Expression
{
	private Expression left;
	private Expression right;

	public BooleanOperator(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		left = Expression.parseExpression(wstmt, t.getChild(0));
		if (type == MemgrammarParser.ISNULL || type == MemgrammarParser.ISNOTNULL)
		{
		} else
		{
			right = Expression.parseExpression(wstmt, t.getChild(1));
		}
	}

	@Override
	public RowSet getBaseRows(RowSet baseset) throws SQLException, UnsupportedException
	{
		if (type == MemgrammarParser.ISNULL)
			throw new UnsupportedException();
		if (type == MemgrammarParser.ISNOTNULL)
		{
			Table table = wstmt.findIdentifierTable(t.getChild(0)).table;
			Tree identTree = t.getChild(0);
			int index = table.tmd.getColumnIndex(identTree.getText());
			int dtype = table.tmd.dataTypes[index];
			if (table.getNumRows() == 0)
				return null;
			boolean isReference = table.isReferenceColumn(index);
			RowSet rows = new RowSet(RowSetType.BitSet, table.getNumRows());
			if (baseset == null)
			{
				for (int i = 0; i < table.getNumRows(); i++)
				{
					Object o = table.getTableValue(i, index);
					if (isReference)
					{
						if (o != null)
							rows.add(i);
					} else
					{
						if (!table.isNull(dtype, o))
							rows.add(i);
					}
				}
			} else
			{
				RowSetIterator intit = baseset.getIterator();
				int i = 0;
				while (true)
				{
					i = intit.getNextRow();
					if (i < 0)
						break;
					Object o = table.getTableValue(i, index);
					if (isReference)
					{
						if (o != null)
							rows.add(i);
					} else
					{
						if (!table.isNull(dtype, o))
							rows.add(i);
					}
				}
			}
			if (rows.size() == 0)
				return null;
			return rows;
		}
		switch (type)
		{
		case MemgrammarParser.AND:
			RowSet a = left.getBaseRows(baseset);
			RowSet b = null;
			if (right != null)
				b = right.getBaseRows(baseset);
			else
				return a;
			return getIntersection(a, b);
		case MemgrammarParser.OR:
			a = left.getBaseRows(null);
			b = null;
			RowSet result = null;
			if (right != null)
			{
				b = right.getBaseRows(null);
				result = getUnion(a, b);
			} else
			{
				result = a;
			}
			if (baseset != null)
				return getIntersection(result, baseset);
			return result;
		}
		return null;
	}

	public final RowSet getIntersection(RowSet a, RowSet b)
	{
		if (a == null && b == null)
			return null;
		if (a == null)
			return b;
		if (b == null)
			return a;
		return a.getIntersection(b, -1);
	}

	public final RowSet getUnion(RowSet a, RowSet b)
	{
		if (a == null && b == null)
			return null;
		if (a == null)
			return b;
		if (b == null)
			return a;
		a.addAll(b);
		return a;
	}

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		Object oa = left.evaluate(rowset);
		Object ob = null;
		if (type == MemgrammarParser.ISNULL || type == MemgrammarParser.ISNOTNULL)
		{
			if (type == MemgrammarParser.ISNULL)
			{
				return oa == null;
			} else if (type == MemgrammarParser.ISNOTNULL)
			{
				return oa != null;
			}
		} else
		{
			ob = right.evaluate(rowset);
			if (oa == null || ob == null)
				throw new SQLException("Cannot perform and/or on null values");
			if (!(oa instanceof Boolean && ob instanceof Boolean))
				throw new SQLException("Invalid operand for and/or");
		}
		switch (type)
		{
		case MemgrammarParser.AND:
			return ((Boolean) oa) && ((Boolean) ob);
		case MemgrammarParser.OR:
			return ((Boolean) oa) || ((Boolean) ob);
		case MemgrammarParser.ISNULL:
			return oa == null;
		case MemgrammarParser.ISNOTNULL:
			return oa != null;
		}
		return false;
	}

	@Override
	public void getTables(Set<QueryPosition> tableSet)
	{
		left.getTables(tableSet);
		if (right != null)
			right.getTables(tableSet);
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		boolean result = left.containsAggregate(acount);
		if (right != null)
			result |= right.containsAggregate(acount);
		return result;
	}

	@Override
	public boolean containsNonAggregate()
	{
		return left.containsNonAggregate() || (right != null && right.containsNonAggregate());
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		left.setAggRow(aggRow);
		if (right != null)
			right.setAggRow(aggRow);
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		left.aggregate(aggRow, rowset);
		if (right != null)
			right.aggregate(aggRow, rowset);
	}

	@Override
	public int getType()
	{
		return Types.BOOLEAN;
	}

	@Override
	public int getWidth()
	{
		return super.getWidth();
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		left.consolidate(primaryAggRow, toConsolidateAggRow);
		if (right != null)
			right.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
