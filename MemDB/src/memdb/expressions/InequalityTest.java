package memdb.expressions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.ThreadPool;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;
import memdb.sql.MemgrammarParser;
import memdb.sql.SQLUtil;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

class InequalityTest extends Thread
{
	static Logger logger = Logger.getLogger(InequalityTest.class);
	private Table table;
	private int predicateType;
	private Object value;
	private RowSet rows;
	private BitSet rowSet;
	private int colindex;
	private RowSetIterator bit;
	private AtomicInteger curRowNum;
	private Iterator<Object> keyIterator;
	private RowSet baseset;
	private CountDownLatch cdl;

	public InequalityTest(Table table, int colindex, Tree predicate, Object value, RowSet rows, RowSet baseset)
	{
		this.table = table;
		if (value instanceof BigDecimal)
			value = ((BigDecimal) value).doubleValue();
		this.value = value;
		this.rows = rows;
		this.colindex = colindex;
		this.predicateType = predicate.getType();
		this.baseset = baseset;
		int numColumnKeys = table.getKeyIteratorSize(colindex);
		if ((baseset == null && numColumnKeys < table.getNumRows() && numColumnKeys >= 0)
				|| (baseset != null && numColumnKeys > 0 && numColumnKeys < baseset.size()))
		{
			keyIterator = table.getKeyIterator(colindex);
		} else if (baseset != null)
			bit = baseset.getIterator();
		else
			curRowNum = new AtomicInteger(table.getNumRows() - 1);
	}

	private synchronized NextValue getNextValue()
	{
		NextValue nv = new NextValue();
		int rownum = -1;
		if (bit != null)
		{
			rownum = bit.getNextRow();
			nv.rowNum = rownum;
		} else if (curRowNum != null)
		{
			rownum = curRowNum.getAndDecrement();
			nv.rowNum = rownum;
		} else if (keyIterator != null)
		{
			if (keyIterator.hasNext())
			{
				nv.value = keyIterator.next();
			} else
				nv.rowNum = -1;
		} else
		{
			nv.rowNum = -1;
		}
		return nv;
	}

	private static class NextValue
	{
		Integer rowNum;
		Object value;
	}

	public void run()
	{
		List<NextValue> nvlist = new ArrayList<NextValue>();
		while (true)
		{
			NextValue nv;
			nv = getNextValue();
			if (nv.rowNum != null && nv.rowNum < 0)
				break;
			nvlist.add(nv);
		}
		cdl = new CountDownLatch(nvlist.size());
		for (NextValue nv : nvlist)
		{
			ThreadPool.execute(new AddValue(nv));
		}
		try
		{
			cdl.await();
		} catch (InterruptedException e)
		{
			logger.error(e.getMessage());
			return;
		}
		if (rowSet != null)
		{
			// Merge row sets
			if (rowSet == null || rowSet.size() == 0)
				return;
			rows.addAll(new RowSet(rowSet));
		}
	}

	private class AddValue implements Runnable
	{
		private NextValue nv;

		public AddValue(NextValue nv)
		{
			this.nv = nv;
		}

		@Override
		public void run()
		{
			if (nv.rowNum == null && nv.value != null && (nv.value instanceof Integer) && (table.isReferenceColumn(colindex)))
			{
				// Stored value is a surrogate key so look up the actual value for comparison
				nv.value = table.getSurrogateKeyValue((Integer) nv.value, colindex);
			}
			if (nv.value == null)
				nv.value = table.getTableValue(nv.rowNum, colindex);
			Object o = nv.value;
			boolean valid = false;
			if (o instanceof BigDecimal)
				o = ((BigDecimal) o).doubleValue();
			if (predicateType == MemgrammarParser.LT)
			{
				if (o == null)
					valid = true;
				else if (value instanceof Integer)
					valid = ((Integer) value) > ((Integer) o);
				else if (value instanceof Long)
					valid = ((Long) value) > ((Long) o);
				else if (value instanceof String)
					valid = ((String) value).compareTo((String) o) > 0;
				else if (value instanceof Float)
					valid = ((Float) value) > ((Float) o);
				else if (value instanceof Double)
					valid = ((Double) value) > ((Double) o);
				else if (value instanceof Date)
				{
					if (o instanceof Date)
						valid = ((Date) value).compareTo((Date) o) > 0;
					else if (o instanceof Long)
						valid = ((Date) value).compareTo(new Date((Long) o)) > 0;
				}
			} else if (predicateType == MemgrammarParser.LTEQ)
			{
				if (o == null)
					valid = true;
				else if (value instanceof Integer)
					valid = ((Integer) value) >= ((Integer) o);
				else if (value instanceof Long)
					valid = ((Long) value) >= ((Long) o);
				else if (value instanceof String)
					valid = ((String) value).compareTo((String) o) >= 0;
				else if (value instanceof Float)
					valid = ((Float) value) >= ((Float) o);
				else if (value instanceof Double)
					valid = ((Double) value) >= ((Double) o);
				else if (value instanceof Date)
				{
					if (o instanceof Date)
						valid = ((Date) value).compareTo((Date) o) >= 0;
					else if (o instanceof Long)
						valid = ((Date) value).compareTo(new Date((Long) o)) >= 0;
				}
			} else if (predicateType == MemgrammarParser.GT)
			{
				if (o == null)
					valid = false;
				else if (value instanceof Integer)
					valid = ((Integer) value) < ((Integer) o);
				else if (value instanceof Long)
					valid = ((Long) value) < ((Long) o);
				else if (value instanceof String)
					valid = ((String) value).compareTo((String) o) < 0;
				else if (value instanceof Float)
					valid = ((Float) value) < ((Float) o);
				else if (value instanceof Double)
					valid = ((Double) value) < ((Double) o);
				else if (value instanceof Date)
				{
					if (o instanceof Date)
						valid = ((Date) value).compareTo((Date) o) < 0;
					else if (o instanceof Long)
						valid = ((Date) value).compareTo(new Date((Long) o)) < 0;
				}
			} else if (predicateType == MemgrammarParser.GTEQ)
			{
				if (o == null)
					valid = false;
				else if (value instanceof Integer)
					valid = ((Integer) value) <= ((Integer) o);
				else if (value instanceof Long)
					valid = ((Long) value) <= ((Long) o);
				else if (value instanceof String)
					valid = ((String) value).compareTo((String) o) <= 0;
				else if (value instanceof Float)
					valid = ((Float) value) <= ((Float) o);
				else if (value instanceof Double)
					valid = ((Double) value) <= ((Double) o);
				else if (value instanceof Date)
				{
					if (o instanceof Date)
						valid = ((Date) value).compareTo((Date) o) <= 0;
					else if (o instanceof Long)
						valid = ((Date) value).compareTo(new Date((Long) o)) <= 0;
				}
			} else if (predicateType == MemgrammarParser.LIKE)
			{
				if (o == null)
					valid = false;
				else
				{
					if (!table.database.caseSensitive)
						valid = SQLUtil.like(o.toString().toUpperCase(), value.toString().toUpperCase());
					else
						valid = SQLUtil.like(o.toString(), value.toString());
				}
			} else if (predicateType == MemgrammarParser.NOTLIKE)
			{
				if (o == null)
					valid = false;
				else
				{
					if (!table.database.caseSensitive)
						valid = !SQLUtil.like(o.toString().toUpperCase(), value.toString().toUpperCase());
					else
						valid = !SQLUtil.like(o.toString(), value.toString());
				}
			}
			if (valid)
			{
				/*
				 * Add valid row to new set
				 */
				if (baseset != null)
				{
					if (nv.rowNum != null)
					{
						if (baseset.contains(nv.rowNum))
						{
							synchronized (rows)
							{
								rows.add(nv.rowNum);
							}
						}
					} else
					{
						RowSet rs = table.getRowList(colindex, nv.value);
						if (rs != null)
						{
							rs.getIntersection(baseset, table.getNumRows());
							BitSet bits = rs.getBitSet(table.getNumRows());
							synchronized (this)
							{
								if (rowSet == null)
									rowSet = bits;
								else
									rowSet.or(bits);
							}
						}
					}
				} else
				{
					if (nv.rowNum != null)
					{
						synchronized (rows)
						{
							rows.add(nv.rowNum);
						}
					} else
					{
						RowSet rs = table.getRowList(colindex, nv.value);
						if (rs != null)
						{
							BitSet bits = rs.getBitSet(table.getNumRows());
							synchronized (this)
							{
								if (rowSet == null)
									rowSet = bits;
								else
									rowSet.or(bits);
							}
						}
					}
				}
			}
			cdl.countDown();
		}
	}

	public RowSet getRows()
	{
		return rows;
	}
}