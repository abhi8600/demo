package memdb.expressions;

import java.sql.SQLException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.LRUCache;
import memdb.rowset.RowSet;
import memdb.sql.QueryPosition;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class Identifier extends Expression
{
	private QueryPosition qp;
	private String name;
	private int columnIndex;
	private Expression labelExpression;
	private LRUCache<byte[], Object> cache = new LRUCache<byte[], Object>(5000, false);

	public Identifier(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		this.name = t.getText();
		if (wstmt instanceof memdb.sql.Select && t.getChildCount() == 0)
		{
			labelExpression = ((memdb.sql.Select) wstmt).findLabelProjection(name);
			if (labelExpression != null)
				return;
		}
		this.qp = wstmt.findIdentifierTable(t);
		if (qp == null)
			throw new SQLException("Column not found: " + name);
		columnIndex = qp.table.tmd.getColumnIndex(name);
		if (columnIndex < 0)
			throw new SQLException("Column not found: " + name);
	}

	@Override
	public void getTables(Set<QueryPosition> tableSet)
	{
		if (labelExpression != null)
		{
			labelExpression.getTables(tableSet);
			return;
		}
		tableSet.add(qp);
	}
	private boolean isEmpty = false;
	private boolean isFirst = true;

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		if (isEmpty)
			return null;
		if (labelExpression != null)
			return labelExpression.evaluate(rowset);
		if (qp.index < 0 || rowset == null)
			return null;
		if (isFirst)
		{
			isFirst = false;
			if (qp.table.getNumRows() == 0)
				isEmpty = true;
		}
		byte[] key = new byte[rowset.length * 4];
		int index = 0;
		for (int i = 0; i < rowset.length; i++)
		{
			int into = (Integer) rowset[i];
			byte byte4 = (byte) ((into & 0xFF000000L) >>> 24);
			byte byte3 = (byte) ((into & 0x00FF0000L) >>> 16);
			byte byte2 = (byte) ((into & 0x0000FF00L) >>> 8);
			byte byte1 = (byte) ((into & 0x000000FFL));
			key[index++] = byte4;
			key[index++] = byte3;
			key[index++] = byte2;
			key[index++] = byte1;
		}
		Object val = cache.get(key);
		if (val != null)
			return val;
		val = qp.table.getTableValue(rowset[qp.index], columnIndex);
		cache.put(key, val);
		return val;
	}

	public Object evaluateAggKey(int[] rowset) throws SQLException
	{
		if (isEmpty)
			return null;
		if (labelExpression != null)
		{
			if (labelExpression instanceof memdb.expressions.Identifier)
				return ((memdb.expressions.Identifier) labelExpression).evaluateAggKey(rowset);
			else
				return labelExpression.evaluate(rowset);
		}
		if (qp.index < 0 || rowset == null)
			return null;
		if (isFirst)
		{
			isFirst = false;
			if (qp.table.getNumRows() == 0)
				isEmpty = true;
		}
		return qp.table.getTableValueForJoin(rowset[qp.index], columnIndex);
	}

	@Override
	public RowSet getBaseRows(RowSet baseset) throws SQLException, UnsupportedException
	{
		if (labelExpression != null)
			return labelExpression.getBaseRows(baseset);
		return null;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		if (labelExpression != null)
			return labelExpression.containsAggregate(acount);
		return false;
	}

	@Override
	public int getType()
	{
		if (labelExpression != null)
			return labelExpression.getType();
		return qp.table.tmd.dataTypes[columnIndex];
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		if (labelExpression != null)
			return labelExpression.containsNonAggregate();
		return true;
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
	}

	@Override
	public void consolidate(Object[] aggRow1, Object[] aggRow2) throws SQLException
	{
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
