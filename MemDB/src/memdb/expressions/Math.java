package memdb.expressions;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.WhereStatement;

import org.antlr.runtime.tree.Tree;

public class Math extends Expression
{
	private Expression left;
	private Expression right;

	public Math(WhereStatement wstmt, Tree t) throws SQLException
	{
		super(wstmt, t);
		left = Expression.parseExpression(wstmt, t.getChild(0));
		if (t.getChildCount() > 1)
			right = Expression.parseExpression(wstmt, t.getChild(1));
	}

	@Override
	public RowSet getBaseRows(RowSet baseset)
	{
		return null;
	}

	@Override
	public void aggregate(Object[] aggRow, int[] rowset) throws SQLException
	{
		left.aggregate(aggRow, rowset);
		if (right != null)
			right.aggregate(aggRow, rowset);
	}

	@Override
	public Object evaluate(int[] rowset) throws SQLException
	{
		Object oa = left.evaluate(rowset);
		if (right != null)
		{
			Object ob = right.evaluate(rowset);
			return eval(oa, ob);
		}
		return oa;
	}

	private Object eval(Object oa, Object ob)
	{
		if (type == MemgrammarParser.NEGATE)
		{
			if (oa instanceof Integer)
			{
				Integer na = (Integer) oa;
				return -na;
			}
			if (oa instanceof Number)
			{
				Number na = (Number) oa;
				return -na.doubleValue();
			}
			if (oa instanceof BigDecimal)
			{
				BigDecimal na = (BigDecimal) oa;
				return na.negate();
			}
		}
		if (oa == null || ob == null)
			return null;
		if (oa instanceof Integer && ob instanceof Integer)
		{
			Integer na = (Integer) oa;
			Integer nb = (Integer) ob;
			switch (type)
			{
			case MemgrammarParser.PLUS:
				return na + nb;
			case MemgrammarParser.MINUS:
				return na - nb;
			case MemgrammarParser.MULT:
				return na * nb;
			case MemgrammarParser.DIV:
				return na / nb;
			}
		}
		if (oa instanceof Number && ob instanceof Number)
		{
			Number na = (Number) oa;
			Number nb = (Number) ob;
			switch (type)
			{
			case MemgrammarParser.PLUS:
				return na.doubleValue() + nb.doubleValue();
			case MemgrammarParser.MINUS:
				return na.doubleValue() - nb.doubleValue();
			case MemgrammarParser.MULT:
				return na.doubleValue() * nb.doubleValue();
			case MemgrammarParser.DIV:
				return na.doubleValue() / nb.doubleValue();
			}
		}
		if (oa instanceof BigDecimal && ob instanceof BigDecimal)
		{
			BigDecimal na = (BigDecimal) oa;
			BigDecimal nb = (BigDecimal) ob;
			switch (type)
			{
			case MemgrammarParser.PLUS:
				return na.add(nb);
			case MemgrammarParser.MINUS:
				return na.subtract(nb);
			case MemgrammarParser.MULT:
				return na.multiply(nb);
			case MemgrammarParser.DIV:
				return na.divide(nb);
			}
		}
		if (oa instanceof BigDecimal || ob instanceof BigDecimal)
		{
			Number na = oa instanceof BigDecimal ? ((BigDecimal) oa).doubleValue() : (Number) oa;
			Number nb = ob instanceof BigDecimal ? ((BigDecimal) ob).doubleValue() : (Number) ob;
			switch (type)
			{
			case MemgrammarParser.PLUS:
				return na.doubleValue() + nb.doubleValue();
			case MemgrammarParser.MINUS:
				return na.doubleValue() - nb.doubleValue();
			case MemgrammarParser.MULT:
				return na.doubleValue() * nb.doubleValue();
			case MemgrammarParser.DIV:
				return na.doubleValue() / nb.doubleValue();
			}
		}
		if (oa instanceof String || ob instanceof String)
		{
			return oa.toString() + ob.toString();
		}
		return null;
	}

	@Override
	public boolean containsAggregate(AtomicInteger acount)
	{
		return left.containsAggregate(acount) || right.containsAggregate(acount);
	}

	@Override
	public int getType()
	{
		if (type == MemgrammarParser.PLUS && (left.getType() == Types.VARCHAR || right.getType() == Types.VARCHAR))
			return Types.VARCHAR;
		if (left.getType() == Types.INTEGER && right.getType() == Types.INTEGER)
			return Types.INTEGER;
		if (left.getType() == Types.DECIMAL && right.getType() == Types.DECIMAL)
			return Types.DECIMAL;
		return Types.DOUBLE;
	}

	@Override
	public int getWidth()
	{
		return 0;
	}

	@Override
	public boolean containsNonAggregate()
	{
		return left.containsNonAggregate() || right.containsNonAggregate();
	}

	@Override
	public void setAggRow(Object[] aggRow)
	{
		super.setAggRow(aggRow);
		left.setAggRow(aggRow);
		right.setAggRow(aggRow);
	}

	@Override
	public void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException
	{
		left.consolidate(primaryAggRow, toConsolidateAggRow);
		if (right != null)
			right.consolidate(primaryAggRow, toConsolidateAggRow);
	}

	@Override
	public boolean isBaseRowsParallel()
	{
		return false;
	}
}
