package memdb.expressions;

import java.util.concurrent.CountDownLatch;

import memdb.ThreadPool;
import memdb.rowset.RowSet;
import memdb.sql.QueryResultRow;
import memdb.storage.Table;

import org.apache.log4j.Logger;

public class EqualityInThread extends Thread
{
	static Logger logger = Logger.getLogger(EqualityInThread.class);
	private Table table;
	private int index;
	private QueryResultRow[] results;
	private RowSet baseSet;
	private RowSet rows;
	private CountDownLatch cdl;

	public EqualityInThread(QueryResultRow[] results, Table table, RowSet baseSet, RowSet rows, int index)
	{
		this.results = results;
		this.table = table;
		this.baseSet = baseSet;
		this.rows = rows;
		this.index = index;
	}

	public void run()
	{
		cdl = new CountDownLatch(results.length);
		for (int i = 0; i < results.length; i++)
		{
			Object val = results[i].dataRow[0];
			ThreadPool.execute(new AddInValue(val));
		}
		try
		{
			cdl.await();
		} catch (InterruptedException e)
		{
			logger.error(e.getMessage());
		}
	}

	private class AddInValue implements Runnable
	{
		Object val;

		public AddInValue(Object val)
		{
			this.val = val;
		}

		@Override
		public void run()
		{
			RowSet rowList = table.getRowList(index, val);
			if (rowList != null)
			{
				if (baseSet != null)
				{
					synchronized (rows)
					{
						rowList.addAllToSet(rows, baseSet);
					}
				} else
				{
					synchronized (rows)
					{
						rows.addAll(rowList);
					}
				}
			}
			cdl.countDown();
		}
	}
}
