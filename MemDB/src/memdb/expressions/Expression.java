package memdb.expressions;

import gnu.trove.list.TByteList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.DateParser;
import memdb.DateUtil;
import memdb.rowset.RowSet;
import memdb.sql.MemgrammarParser;
import memdb.sql.QueryPosition;
import memdb.sql.Select;
import memdb.sql.WhereStatement;
import memdb.storage.Table;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

@SuppressWarnings("unused")
public abstract class Expression
{
	public static boolean EVALUATE_NULLS_AS_FALSE = true;
	protected static Logger logger = Logger.getLogger(Expression.class);
	protected Tree t;
	protected WhereStatement wstmt;
	protected int type;
	protected String text;
	protected Object[] aggRow;
	private DateParser dp = new DateParser();

	public Expression(WhereStatement wstmt, Tree t)
	{
		this.t = t;
		this.wstmt = wstmt;
		if (t != null)
		{
			this.type = t.getType();
			this.text = t.getText();
		}
		List<Expression> elist = wstmt.getExpressionCache().get(type);
		if (elist == null)
		{
			elist = new ArrayList<Expression>();
			wstmt.getExpressionCache().put(type, elist);
		}
		elist.add(this);
	}

	public abstract RowSet getBaseRows(RowSet curset) throws SQLException, UnsupportedException;

	/*
	 * Whether or not this pre-filter should be run separately in parallel, or whether it is potentially slow and
	 * therefore would benefit from reducing the set it's applied to via prior filters
	 */
	public abstract boolean isBaseRowsParallel();

	public abstract Object evaluate(int[] rowset) throws SQLException;

	public void setAggRow(Object[] aggRow)
	{
		this.aggRow = aggRow;
	}

	public boolean equals(Object o, int[] rowset)
	{
		try
		{
			Object thisvalue = evaluate(rowset);
			Object thatvalue = o instanceof Expression ? ((Expression) o).evaluate(rowset) : o;
			if (thisvalue == null || thatvalue == null)
			{
				if (EVALUATE_NULLS_AS_FALSE)
					return false;
				else
					throw new SQLException("Error attempting to compare a null value");
			}
			if (thatvalue != null && thisvalue.getClass() != thatvalue.getClass())
			{
				if (thisvalue instanceof BigDecimal)
					thisvalue = ((BigDecimal) thisvalue).doubleValue();
				if (thatvalue instanceof BigDecimal)
					thatvalue = ((BigDecimal) thatvalue).doubleValue();
				if (thisvalue instanceof Integer && thatvalue instanceof Double)
					thisvalue = ((Integer) thisvalue).doubleValue();
				else if (thisvalue instanceof Double && thatvalue instanceof Integer)
					thatvalue = ((Integer) thatvalue).doubleValue();
				else if (thisvalue instanceof Long && thatvalue instanceof Integer)
					thatvalue = ((Integer) thatvalue).longValue();
				else if (thatvalue instanceof Long && thisvalue instanceof Integer)
					thisvalue = ((Long) thatvalue).longValue();
				else if (thisvalue instanceof Integer && thatvalue instanceof String)
					thisvalue = ((Integer) thisvalue).toString();
				else if (thatvalue instanceof Integer && thisvalue instanceof String)
					thatvalue = ((Integer) thisvalue).toString();
				else if (thisvalue instanceof Date && thatvalue instanceof String)
					thatvalue = dp.parseUsingPossibleFormats(thatvalue.toString(), wstmt.getCommand().getClientTimeZone());
				else if (thatvalue instanceof Date && thisvalue instanceof String)
					thisvalue = dp.parseUsingPossibleFormats(thisvalue.toString(), wstmt.getCommand().getClientTimeZone());
			} else if (thisvalue instanceof String)
			{
				String thiss = ((String) thisvalue).trim();
				String thats = ((String) thatvalue).trim();
				if (!wstmt.getDb().caseSensitive)
					return thiss.equalsIgnoreCase(thats);
				else
					return thiss.equals(thats);
			}
			return thisvalue.equals(thatvalue);
		} catch (SQLException e)
		{
			logger.error(e);
		}
		return false;
	}

	public int compareTo(Object o, int[] rowset)
	{
		try
		{
			Object thisvalue = evaluate(rowset);
			Object thatvalue = ((Expression) o).evaluate(rowset);
			if (thisvalue == null || thatvalue == null)
				return -1;
			if (thisvalue.getClass() != thatvalue.getClass())
			{
				if (thisvalue instanceof BigDecimal)
					thisvalue = ((BigDecimal) thisvalue).doubleValue();
				if (thatvalue instanceof BigDecimal)
					thatvalue = ((BigDecimal) thatvalue).doubleValue();
				if (thisvalue instanceof Integer && thatvalue instanceof Double)
					thisvalue = ((Integer) thisvalue).doubleValue();
				else if (thisvalue instanceof Double && thatvalue instanceof Integer)
					thatvalue = ((Integer) thatvalue).doubleValue();
				else if (thisvalue instanceof Long && thatvalue instanceof Integer)
					thatvalue = ((Integer) thatvalue).longValue();
				else if (thatvalue instanceof Long && thisvalue instanceof Integer)
					thisvalue = ((Integer) thatvalue).longValue();
				else if (thisvalue instanceof Date && thatvalue instanceof String)
					thatvalue = dp.parseUsingPossibleFormats(thatvalue.toString(), wstmt.getCommand().getClientTimeZone());
				else if (thatvalue instanceof Date && thisvalue instanceof String)
					thisvalue = dp.parseUsingPossibleFormats(thisvalue.toString(), wstmt.getCommand().getClientTimeZone());
			}
			if (thatvalue == null)
				return -1;
			if (thisvalue.getClass() == thatvalue.getClass())
			{
				if (thisvalue instanceof Integer)
					return ((Integer) thisvalue).compareTo((Integer) thatvalue);
				if (thisvalue instanceof Long)
					return ((Long) thisvalue).compareTo((Long) thatvalue);
				if (thisvalue instanceof String)
					return (((String) thisvalue).trim()).compareTo(((String) thatvalue).trim());
				if (thisvalue instanceof Double)
					return ((Double) thisvalue).compareTo((Double) thatvalue);
				if (thisvalue instanceof Date)
					return ((Date) thisvalue).compareTo((Date) thatvalue);
				if (thisvalue instanceof Boolean)
					return ((Boolean) thisvalue).compareTo((Boolean) thatvalue);
				if (thisvalue instanceof Float)
					return ((Float) thisvalue).compareTo((Float) thatvalue);
			}
			return 0;
		} catch (SQLException e)
		{
			logger.error(e);
		}
		return 0;
	}

	public abstract void aggregate(Object[] aggRow, int[] rowset) throws SQLException;

	public abstract void consolidate(Object[] primaryAggRow, Object[] toConsolidateAggRow) throws SQLException;

	public abstract boolean containsAggregate(AtomicInteger acount);

	public abstract boolean containsNonAggregate();

	public void getTables(Set<QueryPosition> tableSet)
	{
	}

	public static Expression parseExpression(WhereStatement wstmt, Tree t) throws SQLException
	{
		Expression e = null;
		switch (t.getType())
		{
		case MemgrammarParser.IDENT:
			/*
			 * See if the identifier was already used. If so, re-use (so as not to access the same data twice)
			 */
			List<Expression> elist = wstmt.getExpressionCache().get(t.getType());
			if (elist != null)
			{
				for (Expression exp : elist)
				{
					Tree t2 = exp.getTree();
					if (t.getText().equals(t2.getText()))
					{
						if (t.getChildCount() == t2.getChildCount())
						{
							if (t.getChildCount() == 1 && t.getChild(0).getText().equals(t2.getChild(0).getText()))
								return exp;
							else if (t.getChildCount() == 0)
								return exp;
						}
					}
				}
			}
			return new Identifier(wstmt, t);
		case MemgrammarParser.EQUALS:
		case MemgrammarParser.NOTEQUALS:
		case MemgrammarParser.LT:
		case MemgrammarParser.GT:
		case MemgrammarParser.LTEQ:
		case MemgrammarParser.GTEQ:
		case MemgrammarParser.IN:
		case MemgrammarParser.NOTIN:
		case MemgrammarParser.LIKE:
		case MemgrammarParser.NOTLIKE:
			return new EqualityOperator(wstmt, t);
		case MemgrammarParser.STRING:
		case MemgrammarParser.DATETIME:
		case MemgrammarParser.INTEGER:
		case MemgrammarParser.FLOAT:
		case MemgrammarParser.BOOLEAN:
		case MemgrammarParser.PARAMETER:
		case MemgrammarParser.NULL:
			return new Literal(wstmt, t);
		case MemgrammarParser.OR:
		case MemgrammarParser.AND:
		case MemgrammarParser.ISNOTNULL:
		case MemgrammarParser.ISNULL:
			return new BooleanOperator(wstmt, t);
		case MemgrammarParser.SUM:
		case MemgrammarParser.COUNT:
		case MemgrammarParser.AVG:
		case MemgrammarParser.MIN:
		case MemgrammarParser.MAX:
			return new Aggregate(wstmt, t);
		case MemgrammarParser.PLUS:
		case MemgrammarParser.MINUS:
		case MemgrammarParser.MULT:
		case MemgrammarParser.DIV:
		case MemgrammarParser.NEGATE:
			return new Math(wstmt, t);
		case MemgrammarParser.PAREN:
			return parseExpression(wstmt, t.getChild(0));
		case MemgrammarParser.CASE:
			return new Case(wstmt, t);
		case MemgrammarParser.DATEADD:
		case MemgrammarParser.TIMESTAMPADD:
		case MemgrammarParser.DATEDIFF:
		case MemgrammarParser.TIMESTAMPDIFF:
		case MemgrammarParser.DATEPART:
		case MemgrammarParser.EXTRACT:
		case MemgrammarParser.GETDATE:
			return new DateMethods(wstmt, t);
		case MemgrammarParser.SUBSTR:
		case MemgrammarParser.TOUPPER:
		case MemgrammarParser.TOLOWER:
		case MemgrammarParser.LENGTH:
		case MemgrammarParser.LEN:
		case MemgrammarParser.POSITION:
			return new StringMethods(wstmt, t);
		case MemgrammarParser.CAST:
		case MemgrammarParser.ISNULLEXP:
		case MemgrammarParser.COALESCE:
		case MemgrammarParser.OBJECTSIZE:
			return new UtilMethods(wstmt, t);
		}
		return e;
	}

	public int getType()
	{
		return Types.VARCHAR;
	}

	public int getWidth()
	{
		return 80;
	}

	public Tree getTree()
	{
		return t;
	}
}
