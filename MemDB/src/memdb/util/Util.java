package memdb.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.Properties;

import memdb.util.logging.LogUtil;

import org.apache.log4j.Logger;

public class Util
{
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static final String MEMDB_HOME = "memdb.home";
	private static Logger logger = Logger.getLogger(Util.class);
	private static String hostName = null;
	private static String ipAddress = null;
	private static Map<String, Properties> propertiesMap = new HashMap<String, Properties>();
	private static String buildString;

	public static final String getString(String identifier)
	{
		if (identifier.length() >= 2 && identifier.charAt(0) == '\'' && identifier.charAt(identifier.length() - 1) == '\'')
		{
			return identifier.substring(1, identifier.length() - 1);
		}
		return identifier;
	}

	/**
	 * Replace instances of a substring within another string
	 * 
	 * @param a
	 *            String to process
	 * @param b
	 *            String to search for
	 * @param c
	 *            String to replace with
	 * @return New string
	 */
	public static final String replaceStr(String a, String b, String c)
	{
		if (a == null || b == null || c == null || a.indexOf(b) < 0)
			return a;
		StringBuilder sb = new StringBuilder(a);
		int len = b.length();
		int i = sb.indexOf(b);
		while (i >= 0)
		{
			sb.replace(i, i + len, c);
			i = sb.indexOf(b, i + c.length());
		}
		return (sb.toString());
	}

	public static long getDecimalLong(BigDecimal bd)
	{
		long scale = bd.scale();
		Long val = bd.unscaledValue().longValue();
		if (scale > 127)
			return 0;
		if (scale < -128)
			return 0;
		boolean neg = false;
		if (val < 0)
		{
			val = -val;
			neg = true;
		}
		val = val & 0x007FFFFFFFFFFFFFL;
		val = (val << 8) | (scale & 0xFFL);
		if (neg)
			val = val | 0x8000000000000000L;
		return val;
	}

	public static final BigDecimal getLongDecimal(long val)
	{
		boolean neg = false;
		if ((val & 0x8000000000000000L) != 0)
		{
			neg = true;
			val = val & (0x7FFFFFFFFFFFFFFFL);
		}
		int scale = (int) (val & 0xFF);
		Long unscaledvalue = val >>> 8;
		if (neg)
			unscaledvalue = -unscaledvalue;
		BigInteger bi = new BigInteger(unscaledvalue.toString());
		BigDecimal bd = new BigDecimal(bi, scale);
		return bd;
	}

	/**
	 * Read a line from a buffered reader taking quotation marks into effect (allows newlines in fields that are quoted)
	 * 
	 * @return
	 * @throws UnterminatedQuoteException
	 * @throws IOException
	 */
	public static final StringBuilder readLine(StringBuilder sb, BufferedReader br, boolean recognizeQuotes, char quotec, boolean isIgnoreBackslash, char sepc,
			String terminatedby) throws IOException
	{
		sb.setLength(0);
		int i = 0;
		boolean quote = false;
		boolean lastEscape = false;
		boolean lastSep = true;
		int terminatedbychar = 0;
		// Read one ahead to only use quotes if before or after a separator
		i = br.read();
		if (i < 0)
			return null;
		int nexti = 0;
		char firstterm = terminatedby.charAt(0);
		int len = terminatedby.length();
		char[] termby = terminatedby.toCharArray();
		while (i >= 0)
		{
			br.mark(255);
			nexti = br.read();
			char c = (char) i;
			char nc = (char) nexti;
			if (isIgnoreBackslash && c == '\\')
				continue;
			if (recognizeQuotes && c == quotec && ((lastSep && !quote) || (nc == sepc || nc == firstterm)))
			{
				lastSep = false;
				if (!lastEscape)
				{
					quote = !quote;
				}
				sb.append(c);
			} else if (!quote)
			{
				if (c == termby[terminatedbychar++])
				{
					if (len > 1)
					{
						if (terminatedbychar >= (len - 1) && nc == termby[terminatedbychar])
						{
							return sb;
						}
					} else
					{
						br.reset();
						return sb;
					}
				} else
				{
					sb.append(c);
					terminatedbychar = 0;
					lastSep = c == sepc;
				}
			} else
			{
				lastSep = false;
				sb.append(c);
			}
			if (c == '\\' && !lastEscape)
				lastEscape = true;
			else
				lastEscape = false;
			i = nexti;
		}
		br.reset();
		if (sb.length() > 0)
			return sb;
		return null;
	}

	public static final int binarySearch(int[] sorted, int first, int upto, int key)
	{
		while (first < upto)
		{
			int mid = (first + upto) / 2; // Compute mid point.
			if (key < sorted[mid])
			{
				upto = mid; // repeat search in bottom half.
			} else if (key > sorted[mid])
			{
				first = mid + 1; // Repeat search in top half.
			} else
			{
				return mid; // Found it. return position
			}
		}
		return -1; // Failed to find key
	}

	/**
	 * Replace illegal characters in a string for the purposes of creating a new physical name. Additional character is
	 * appended to the end to ensure to reserved word collisions
	 * 
	 * @param s
	 * @return
	 */
	public static String replaceWithNewPhysicalString(String s)
	{
		return (replaceWithPhysicalString(s) + '$');
	}

	/**
	 * Replace illegal characters in a string with ones that are valid for SQL database physical names. Replace spaces
	 * with underscores, dollar signs with D, number signs with N and forward slashes with underscores
	 * 
	 * @param s
	 * @return
	 */
	public static final String replaceWithPhysicalString(String s)
	{
		if (s == null)
			return null;
		if (s.length() == 0)
			return s;
		boolean nonAsciiCharsPresent = containsNonAsciiCharacters(s);
		char c = s.charAt(0);
		if ((c >= '0' && c <= '9'))
		{
			s = 'N' + s;
		}
		if (nonAsciiCharsPresent)
		{
			// Replace only white space characters for other language characters.
			s = s.replace('$', 'D').replace('#', 'N').replaceAll("[\\s]", "_");
		} else
		{
			// Replace all non-word characters for english.
			s = s.replace('$', 'D').replace('#', 'N').replaceAll("[^\\w]", "_");
		}
		return (s);
	}

	/*
	 * Determine if a given string contains any non-ascii characters.
	 * 
	 * @param s
	 * 
	 * @return
	 */
	private static final boolean containsNonAsciiCharacters(String s)
	{
		boolean result = false;
		if (s == null)
			return false;
		char c = 0;
		for (int i = 0; i < s.length(); i++)
		{
			c = s.charAt(i);
			if (c > 0x007F)
			{
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * log code and java version information
	 */
	public static final String getRuntimeInfo()
	{
		StringBuilder rti = new StringBuilder();
		rti.append(LINE_SEPARATOR);
		rti.append("========");
		rti.append(LINE_SEPARATOR);
		rti.append("Build Information: " + Util.getBuildString());
		rti.append(LINE_SEPARATOR);
		rti.append("memdb.home: " + System.getProperty("memdb.home"));
		rti.append(LINE_SEPARATOR);
		rti.append("Host: " + Util.getHostName() + "/" + Util.getIPAddress());
		rti.append(LINE_SEPARATOR);
		rti.append("OS: " + System.getProperty("os.name") + " / " + System.getProperty("os.version") + " / " + System.getProperty("sun.os.patch.level"));
		rti.append(LINE_SEPARATOR);
		rti.append("Architecture: " + System.getProperty("os.arch"));
		rti.append(LINE_SEPARATOR);
		rti.append("Java VM Name: " + System.getProperty("java.vm.name"));
		rti.append(LINE_SEPARATOR);
		rti.append("Java VM Vendor: " + System.getProperty("java.vm.vendor"));
		rti.append(LINE_SEPARATOR);
		rti.append("Java version: " + System.getProperty("java.version") + " / " + System.getProperty("java.runtime.version") + " / "
				+ System.getProperty("java.specification.version") + " / " + System.getProperty("java.vm.version"));
		rti.append(LINE_SEPARATOR);
		rti.append("Java maximum memory setting: " + (int) (((double) Runtime.getRuntime().maxMemory()) / 1000000) + " Mbytes");
		rti.append(LINE_SEPARATOR);
		rti.append("Available processors: " + Runtime.getRuntime().availableProcessors());
		rti.append(LINE_SEPARATOR);
		rti.append("Log File: " + LogUtil.getLogfileName());
		rti.append(LINE_SEPARATOR);
		rti.append("Database time zone: " + TimeZone.getDefault().getDisplayName());
		rti.append(LINE_SEPARATOR);
		rti.append("========");
		return rti.toString();
	}

	/**
	 * return the build and release numbers (from the build.number file)
	 * 
	 * @return
	 */
	public static String getBuildString()
	{
		if (buildString != null)
			return buildString;
		Properties props = null;
		try
		{
			props = getProperties("build.number", true);
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			props = new Properties();
		}
		String buildNumber = props.getProperty("build.number");
		String releaseNumber = props.getProperty("release.number");
		buildString = "Release: " + releaseNumber + ", Build: " + buildNumber;
		return buildString;
	}

	/**
	 * read in properties file
	 * 
	 * @param file
	 * @return
	 */
	public static Properties getProperties(String file)
	{
		return getProperties(file, false);
	}

	/**
	 * read in a properties file, optionally search for it in 'standard' locations (smi.home/conf, classpath)
	 * 
	 * @param file
	 * @param search
	 * @return
	 */
	public static Properties getProperties(String file, boolean search)
	{
		// 0) See if this property file has already been loaded
		if (propertiesMap.containsKey(file))
		{
			return propertiesMap.get(file);
		}
		Properties result = new Properties();
		// 1) try the provided filename
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			result.load(fis);
			logger.debug("Using properties file: " + file);
		} catch (FileNotFoundException e)
		{
			logger.debug("Properties file was not found: " + file);
		} catch (IOException e)
		{
			logger.debug("IOException reading properties file: " + file);
		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();
				} catch (Exception e)
				{
				}
			}
		}
		// If no 'search' requested, end search here.
		if (!search)
		{
			logger.debug("No properties file - " + file + " in current directory and no request to continue search.");
			return null;
		}
		// 2) Try <memdb.home>/conf
		if (result.isEmpty())
		{
			// But only if the 1st file path didn't succeed...
			String smiHome = System.getProperty(Util.MEMDB_HOME);
			if (smiHome != null && smiHome.length() > 0)
			{
				String path = smiHome + "/conf/" + file;
				fis = null;
				try
				{
					fis = new FileInputStream(path);
					result.load(fis);
					logger.debug("Using properties file: " + path);
					return result;
				} catch (FileNotFoundException e)
				{
					logger.debug("smi.home properties file (" + path + ") was not found.");
				} catch (IOException e)
				{
					logger.debug("smi.home properties file (" + path + ") found but errors reading file");
				} finally
				{
					if (fis != null)
					{
						try
						{
							fis.close();
						} catch (Exception e)
						{
						}
					}
				}
			}
			// 3) try the classpath
			logger.trace("Continuing search for properties file in classpath.");
			InputStream in = null;
			try
			{
				in = (new Util()).getClass().getClassLoader().getResourceAsStream(file);
				if (in != null)
				{
					result.load(in);
					logger.debug("Using properties file: " + file + " (found via the classpath)");
				}
			} catch (IOException e)
			{
				logger.debug("classpath properties file (" + file + ") not found.");
			} finally
			{
				if (in != null)
				{
					try
					{
						in.close();
					} catch (Exception e)
					{
					}
				}
			}
		}
		// Report results of search
		if (result.isEmpty())
		{
			logger.debug("Properties file (" + file + ") not found or empty after full search was performed.");
		} else
		{
			// Add to properties cache
			propertiesMap.put(file, result);
			if (logger.isDebugEnabled())
				printProperties(result);
		}
		return result;
	}

	/**
	 * return the hostname for this host, if problems return 'localhost'
	 * 
	 * @return
	 */
	public static String getHostName()
	{
		if (hostName == null)
		{
			try
			{
				hostName = java.net.InetAddress.getLocalHost().getHostName();
			} catch (IOException ie)
			{
				hostName = "localhost";
			}
		}
		return hostName;
	}

	private static String getIPAddress()
	{
		if (ipAddress == null)
		{
			try
			{
				InetAddress tmp = java.net.InetAddress.getLocalHost();
				ipAddress = tmp.toString();
			} catch (IOException ie)
			{
				ipAddress = "localhost/127.0.0.l";
			}
		}
		return ipAddress;
	}

	/**
	 * dump the properties to the log file
	 * 
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}

	// Reads in content of entire file and places in string - use this method for small files only
	public static String getFileContent(File f) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = null;
		do
		{
			line = br.readLine();
			sb.append(line);
			sb.append(Util.LINE_SEPARATOR);
		} while (line != null);
		return sb.toString();
	}

	public static final boolean equalByteArrays(byte[] arr1, byte[] arr2)
	{
		if (arr1.length != arr2.length)
			return false;
		for (int i = 0; i < arr1.length; i++)
		{
			if (arr1[i] != arr2[i])
				return false;
		}
		return true;
	}

	public static final int compareByteArrays(byte[] arr1, byte[] arr2)
	{
		if (arr1.length < arr2.length)
			return -1;
		else if (arr1.length > arr2.length)
			return 1;
		for (int i = 0; i < arr1.length; i++)
		{
			if (arr1[i] < arr2[i])
				return -1;
			else if (arr1[i] > arr2[i])
				return 1;
		}
		return 0;
	}

	public static long getFileSize(File f)
	{
		long result = 0;
		if (f.isDirectory())
		{
			File[] farr = f.listFiles();
			if (farr != null)
				for (File cf : farr)
					result += getFileSize(cf);
		} else
			result += f.length();
		return result;
	}

	public static void deleteFile(File f)
	{
		File[] children = f.listFiles();
		if (children != null)
			for (File child : children)
			{
				deleteFile(child);
			}
		f.delete();
	}
}
