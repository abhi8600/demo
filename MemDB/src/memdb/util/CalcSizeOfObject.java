package memdb.util;

import java.lang.instrument.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

public class CalcSizeOfObject
{
	private static Instrumentation inst;

	public static void premain(String options, Instrumentation inst)
	{
		CalcSizeOfObject.inst = inst;
	}

	public static long sizeOf(Object object)
	{
		if (inst == null)
			throw new IllegalStateException("Instrumentation is null");
		return inst.getObjectSize(object);
	}

	public static long deepSizeOf(Object objectToSize, Set<Object> excludes)
	{
		Map<Object, Object> doneObj = new IdentityHashMap<Object, Object>();
		if (excludes != null)
			for (Object o : excludes)
				doneObj.put(o, null);
		return deepSizeOf(objectToSize, doneObj, 0);
	}

	private static long deepSizeOf(Object o, Map<Object, Object> doneObj, int depth)
	{
		if (o == null)
		{
			return 0;
		}
		long size = 0;
		if (doneObj.containsKey(o))
		{
			return 0;
		}
		doneObj.put(o, null);
		size = sizeOf(o);
		if (o instanceof Object[])
		{
			for (Object obj : (Object[]) o)
			{
				size += deepSizeOf(obj, doneObj, depth + 1);
			}
		} else
		{
			Field[] fields = o.getClass().getDeclaredFields();
			for (Field field : fields)
			{
				field.setAccessible(true);
				Object obj;
				try
				{
					obj = field.get(o);
				} catch (IllegalArgumentException e)
				{
					throw new RuntimeException(e);
				} catch (IllegalAccessException e)
				{
					throw new RuntimeException(e);
				}
				if (isComputable(field))
				{
					size += deepSizeOf(obj, doneObj, depth + 1);
				}
			}
		}
		return size;
	}

	private static boolean isAPrimitiveType(@SuppressWarnings("rawtypes") Class c)
	{
		if (c == java.lang.Boolean.TYPE)
			return true;
		if (c == java.lang.Character.TYPE)
			return true;
		if (c == java.lang.Byte.TYPE)
			return true;
		if (c == java.lang.Short.TYPE)
			return true;
		if (c == java.lang.Integer.TYPE)
			return true;
		if (c == java.lang.Long.TYPE)
			return true;
		if (c == java.lang.Float.TYPE)
			return true;
		if (c == java.lang.Double.TYPE)
			return true;
		if (c == java.lang.Void.TYPE)
			return true;
		return false;
	}

	private static boolean isComputable(Field field)
	{
		int modificatori = field.getModifiers();
		if (isAPrimitiveType(field.getType()))
			return false;
		else if (Modifier.isStatic(modificatori))
			return false;
		else
			return true;
	}
}
