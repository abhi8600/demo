package memdb.util;

import java.io.File;

import memdb.Database;
import memdb.storage.Table;

/**
 * Because the OS may not immediately delete files (given that they are memory mapped) need to come by later after the
 * resources are released (garbage collected) and delete them. This includes any file that may be memory-mapped (any
 * block store file)
 * 
 * @author bpeters
 * 
 */
public class Reaper implements Runnable
{
	private Database db;

	public Reaper(Database db)
	{
		this.db = db;
	}

	@Override
	public void run()
	{
		File f = new File(db.path);
		File[] dbfiles = f.listFiles();
		if (dbfiles != null)
			for (File cf : dbfiles)
			{
				if (!cf.isDirectory())
					continue;
				// Look for deleted schemas
				String schema = cf.getName();
				boolean found = false;
				for (Table t : db.tables)
				{
					if (t.schema.equals(schema))
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					try
					{
						Util.deleteFile(cf);
					} catch (Exception ex)
					{
					}
				} else
				{
					File[] tableFiles = cf.listFiles();
					if (tableFiles != null)
						for (File tf : tableFiles)
						{
							if (!tf.isDirectory())
								continue;
							// Look for deleted schemas
							boolean foundt = false;
							for (Table t : db.tables)
							{
								if (t.schema.equals(schema) && t.name.equals(tf.getName()))
								{
									foundt = true;
									break;
								}
							}
							if (!foundt)
							{
								try
								{
									Util.deleteFile(tf);
								} catch (Exception ex)
								{
								}
							}
						}
				}
			}
	}
}
