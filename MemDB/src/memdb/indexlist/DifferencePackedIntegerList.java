package memdb.indexlist;

import gnu.trove.list.array.TIntArrayList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import memdb.rowset.RowSet;
import memdb.sql.RowRange;
import memdb.storage.Compression;
import memdb.storage.Compression.Method;

public class DifferencePackedIntegerList extends IndexList implements Externalizable
{
	private static final long serialVersionUID = 1L;
	private byte numBits = -1;
	private int min;
	private int firstValue;
	private byte[] buffer;
	private byte remainder;

	public DifferencePackedIntegerList()
	{
	}

	private void setupDifferenceList(int[] difflist)
	{
		/*
		 * Pack an integer list into a byte array using the minimum number of bits
		 */
		if (difflist.length == 0)
		{
			numBits = 0;
			buffer = new byte[]
			{ 0, 0, 0, 0 };
			return;
		}
		if (difflist.length == 1)
		{
			min = difflist[0];
			buffer = null;
			numBits = 0;
			return;
		}
		int min = difflist[0];
		int max = difflist[0];
		for (int i = 1; i < difflist.length; i++)
		{
			if (difflist[i] < min)
				min = difflist[i];
			if (difflist[i] > max)
				max = difflist[i];
		}
		this.min = min;
		calcNumBits(max - min);
		if (numBits == 0)
		{
			buffer = new byte[4];
			int len = difflist.length + 1;
			buffer[0] = (byte) (len & 0xFF);
			buffer[1] = (byte) ((len >>> 8) & 0xFF);
			buffer[2] = (byte) ((len >>> 16) & 0xFF);
			buffer[3] = (byte) ((len >>> 24) & 0xFF);
			return;
		}
		// Need one fewer entries since only storing differences
		int len = ((difflist.length * numBits) / Byte.SIZE) + (numBits % Byte.SIZE != 0 ? 1 : 0);
		buffer = new byte[len];
		// May not take up all of the last byte
		remainder = (byte) (buffer.length * Byte.SIZE / numBits - difflist.length);
		int numBytesPerValue = (numBits - 1) / Byte.SIZE + 1;
		for (int i = 0, curbit = 0; i < difflist.length; i++, curbit += numBits)
		{
			int curbyte = curbit / Byte.SIZE;
			byte bitoffset = (byte) (curbit % Byte.SIZE);
			long val = difflist[i] - min;
			val = val << bitoffset;
			for (int j = 0, startbits = numBits; j < numBytesPerValue; j++, val = val >>> Byte.SIZE, curbyte++, startbits -= Byte.SIZE)
			{
				buffer[curbyte] |= (byte) (val & 0xFF);
				if (bitoffset + (startbits > Byte.SIZE ? Byte.SIZE : startbits) > Byte.SIZE)
					buffer[curbyte + 1] |= (byte) ((val >>> Byte.SIZE) & 0xFF);
			}
		}
		if (buffer.length > 500 && numBits > 1)
		{
			/*
			 * For really long lists do a bit of extra compression
			 */
			byte[] compressed = Compression.compressObject(Method.SNAPPY_NO_DESERIALIZE, buffer);
			if (compressed.length < (buffer.length * 7) / 10)
			{
				buffer = compressed;
				numBits = (byte) -numBits;
			}
		}
	}

	public DifferencePackedIntegerList(IndexList intlist)
	{
		intlist.sort();
		int[] difflist = new int[intlist.size() - 1];
		firstValue = intlist.get(0);
		for (int i = 1; i <= difflist.length; i++)
		{
			difflist[i - 1] = intlist.get(i) - intlist.get(i - 1);
		}
		setupDifferenceList(difflist);
		/*
		 * Code to validate that the packed results match the input - only used for finite debugging so commented out
		 * regularly
		 * 
		 * TIntArrayList tlist = getIntArrayList(-1); if (tlist.size() != intlist.size()) { int a = size();
		 * setupDifferenceList(difflist); tlist = getIntArrayList(-1); a = 2; } for (int i = 0; i < tlist.size(); i++) {
		 * if (tlist.get(i) != intlist.get(i)) { int a = get(i); int b = intlist.get(i); setupDifferenceList(difflist);
		 * tlist = getIntArrayList(-1); a = 2; } }
		 */
	}

	private void calcNumBits(int maxValue)
	{
		numBits = 0;
		for (int i = 0; i < Integer.SIZE; i++)
		{
			if ((maxValue & 1) > 0)
				numBits = (byte) (i + 1);
			maxValue = maxValue >>> 1;
		}
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeByte(numBits);
		out.writeInt(min);
		out.writeInt(firstValue);
		out.writeByte(remainder);
		if (buffer == null)
			out.writeInt(-1);
		else
		{
			out.writeInt(buffer.length);
			for (int i = 0; i < buffer.length; i++)
				out.writeByte(buffer[i]);
		}
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		numBits = in.readByte();
		min = in.readInt();
		firstValue = in.readInt();
		remainder = in.readByte();
		int len = in.readInt();
		if (len >= 0)
		{
			buffer = new byte[len];
			for (int i = 0; i < len; i++)
				buffer[i] = in.readByte();
		} else
			buffer = null;
	}

	public int size()
	{
		if (buffer == null)
			return 2;
		if (numBits == -1)
			return 0;
		if (numBits == 0)
		{
			int b3 = buffer[3];
			if (b3 < 0)
				b3 += 256;
			int b2 = buffer[2];
			if (b2 < 0)
				b2 += 256;
			int b1 = buffer[1];
			if (b1 < 0)
				b1 += 256;
			int b0 = buffer[0];
			if (b0 < 0)
				b0 += 256;
			return (b3 << 24) + (b2 << 16) + (b1 << 8) + b0;
		}
		if (numBits < 0)
		{
			numBits = (byte) -numBits;
			buffer = (byte[]) Compression.decompressObject(Method.SNAPPY_NO_DESERIALIZE, buffer);
		}
		return buffer.length * Byte.SIZE / numBits + 1 - remainder;
	}

	public final int getDifference(int index)
	{
		if (index == 0)
			return firstValue;
		if (numBits == 0)
			return min;
		if (numBits < 0)
		{
			numBits = (byte) -numBits;
			buffer = (byte[]) Compression.decompressObject(Method.SNAPPY_NO_DESERIALIZE, buffer);
		}
		index--;
		int startbit = index * numBits;
		int curbyte = startbit / Byte.SIZE;
		int endbyte = (startbit + numBits - 1) / Byte.SIZE;
		byte bitoffset = (byte) (startbit % Byte.SIZE);
		byte rembits = (byte) ((startbit + numBits - 1) % Byte.SIZE + 1);
		long result = 0;
		for (int bytenum = curbyte, pos = 0; bytenum <= endbyte; bytenum++, pos++)
		{
			long curval = buffer[bytenum];
			if (curval < 0)
			{
				curval += 256;
			}
			if (bytenum == endbyte)
			{
				curval &= ((1 << rembits) - 1);
			}
			result |= curval << (Byte.SIZE * pos);
		}
		result = result >>> bitoffset;
		return (int) (result + min);
	}

	public TIntArrayList getIntArrayList(int topn)
	{
		if (topn >= 0)
			return getIntArrayList(0, topn - 1);
		else
			return getIntArrayList(-1, 0);
	}

	public TIntArrayList getIntArrayList(int start, int stop)
	{
		int size = size();
		TIntArrayList list = new TIntArrayList(start >= 0 ? stop - start + 1 : size);
		if (buffer == null)
		{
			list.add(firstValue);
			list.add(min + firstValue);
		} else
		{
			int lasti = 0;
			if (start >= 0 && stop + 1 < size)
				size = stop + 1;
			for (int i = 0; i < size; i++)
			{
				if (i == 0)
					lasti = getDifference(i);
				else
					lasti = getDifference(i) + lasti;
				list.add(lasti);
			}
		}
		return list;
	}

	public BitSet getBitSet(int bitSetSize)
	{
		BitSet bits = new BitSet(bitSetSize);
		if (buffer == null)
		{
			bits.set(firstValue);
			bits.set(min + firstValue);
		} else
		{
			int lasti = 0;
			int size = size();
			for (int i = 0; i < size; i++)
			{
				if (i == 0)
					lasti = getDifference(i);
				else
					lasti = getDifference(i) + lasti;
				bits.set(lasti);
			}
		}
		return bits;
	}

	public List<Object> getList()
	{
		List<Object> list = new ArrayList<Object>();
		int lasti = 0;
		for (int i = 0; i < size(); i++)
		{
			if (i == 0)
				lasti = getDifference(i);
			else
				lasti = getDifference(i) + lasti;
			list.add(lasti);
		}
		return list;
	}

	public boolean contains(int value)
	{
		if (buffer == null)
		{
			return (value == firstValue) || (value == min + firstValue);
		}
		int lasti = 0;
		int size = size();
		for (int i = 0; i < size; i++)
		{
			if (i == 0)
				lasti = getDifference(i);
			else
				lasti = getDifference(i) + lasti;
			if (lasti == value)
				return true;
			if (lasti > value)
				return false;
		}
		return false;
	}

	@Override
	public RowSet getRowSet()
	{
		return new RowSet(getIntArrayList(-1));
	}

	@Override
	public RowSet getRowSet(RowSet baseSet)
	{
		RowRange range = null;
		if (baseSet != null)
			range = baseSet.getRange();
		if (range == null)
			return new RowSet(this, baseSet);
		else
			return new RowSet(this, range.start, range.stop);
	}

	@Override
	public TIntArrayList getIntArrayList()
	{
		return getIntArrayList(-1);
	}

	@Override
	public final int get(int index)
	{
		int lasti = 0;
		int size = size();
		for (int i = 0; i < size; i++)
		{
			if (i == 0)
				lasti = getDifference(i);
			else
				lasti = getDifference(i) + lasti;
			if (i == index)
				return lasti;
		}
		return 0;
	}

	@Override
	public IndexList addSorted(int value)
	{
		TIntArrayList list = getIntArrayList(-1);
		int index = list.binarySearch(value);
		if (index < 0)
		{
			int insertionPoint = -(index + 1);
			if (insertionPoint >= list.size())
				list.add(value);
			else
				list.insert(insertionPoint, value);
		}
		IndexList result = new IntArrayIndexList(list);
		return result;
	}

	@Override
	public IndexList add(int value)
	{
		IndexList result = new IntArrayIndexList(getIntArrayList(-1));
		result.add(value);
		return result;
	}

	@Override
	public IndexList remove(int value)
	{
		IndexList result = new IntArrayIndexList(getIntArrayList(-1));
		result.remove(value);
		return result;
	}

	@Override
	public void sort()
	{
		TIntArrayList newlist = getIntArrayList();
		newlist.sort();
		int[] difflist = new int[newlist.size() - 1];
		firstValue = newlist.get(0);
		for (int i = 1; i <= difflist.length; i++)
		{
			difflist[i - 1] = newlist.get(i) - newlist.get(i - 1);
		}
		setupDifferenceList(difflist);
	}
}
