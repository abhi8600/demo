package memdb.indexlist;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import gnu.trove.list.array.TIntArrayList;
import memdb.rowset.RowSet;

public class RawArrayIndexList extends IndexList
{
	private static final long serialVersionUID = 1L;
	private int[] array;

	public RawArrayIndexList(int[] array)
	{
		this.array = array;
	}

	@Override
	public RowSet getRowSet()
	{
		return new RowSet(array);
	}

	@Override
	public RowSet getRowSet(RowSet baseSet)
	{
		return new RowSet(array, baseSet);
	}

	@Override
	public TIntArrayList getIntArrayList()
	{
		TIntArrayList list = new TIntArrayList(array);
		return list;
	}

	@Override
	public int size()
	{
		return array.length;
	}

	@Override
	public int get(int index)
	{
		return array[index];
	}

	@Override
	public IndexList addSorted(int value)
	{
		TIntArrayList list = getIntArrayList();
		int index = list.binarySearch(value);
		if (index < 0)
		{
			int insertionPoint = -(index + 1);
			if (insertionPoint >= list.size())
				list.add(value);
			else
				list.insert(insertionPoint, value);
		}
		IndexList result = new IntArrayIndexList(list);
		return result;
	}

	@Override
	public IndexList add(int value)
	{
		IndexList result = new IntArrayIndexList(getIntArrayList());
		result.add(value);
		return result;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(array);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		array = (int[]) in.readObject();
	}

	@Override
	public IndexList remove(int value)
	{
		IndexList result = new IntArrayIndexList(getIntArrayList());
		result.remove(value);
		return result;
	}

	@Override
	public void sort()
	{
		TIntArrayList templist = new TIntArrayList(array);
		templist.sort();
		array = templist.toArray();
	}
}
