package memdb.indexlist;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import gnu.trove.list.array.TIntArrayList;
import memdb.rowset.RowSet;

public class IntArrayIndexList extends IndexList
{
	private static final long serialVersionUID = 1L;
	private TIntArrayList list;

	public IntArrayIndexList()
	{
		list = new TIntArrayList();
	}

	public IntArrayIndexList(int size)
	{
		list = new TIntArrayList(size);
	}

	public IntArrayIndexList(TIntArrayList list)
	{
		this.list = list;
	}

	public IntArrayIndexList(int[] list)
	{
		this.list = new TIntArrayList(list.length);
		for (int i = 0; i < list.length; i++)
			this.list.add(list[i]);
	}

	@Override
	public RowSet getRowSet()
	{
		return new RowSet(list);
	}

	@Override
	public RowSet getRowSet(RowSet baseSet)
	{
		return new RowSet(list, baseSet);
	}

	@Override
	public TIntArrayList getIntArrayList()
	{
		return list;
	}

	@Override
	public int size()
	{
		return list.size();
	}

	@Override
	public int get(int index)
	{
		return list.get(index);
	}

	/*
	 * Add sorted to the list (non-Javadoc)
	 * 
	 * @see memdb.indexlist.IndexList#addSorted(int)
	 */
	@Override
	public IndexList addSorted(int value)
	{
		int index = list.binarySearch(value);
		if (index < 0)
		{
			int insertionPoint = -(index + 1);
			if (insertionPoint >= list.size())
				list.add(value);
			else
				list.insert(insertionPoint, value);
		}
		return this;
	}

	/*
	 * Value will be discarded if it's not in order (non-Javadoc)
	 * 
	 * @see memdb.indexlist.IndexList#add(int)
	 */
	@Override
	public IndexList add(int value)
	{
		// Ensure lists are always sorted
		int pos = list.binarySearch(value);
		if (pos < 0)
			list.insert(-pos - 1, value);
		return this;
	}

	/**
	 * Value must be highest so this is just added to the end without a check. If not true, then problems will occur.
	 * Also, not checking for duplicates.
	 * 
	 * @param value
	 * @return
	 */
	public IndexList addValueGuaranteedToBeHighest(int value)
	{
		list.add(value);
		return this;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(list);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		list = (TIntArrayList) in.readObject();
	}

	@Override
	public IndexList remove(int value)
	{
		list.remove(value);
		return this;
	}

	@Override
	public void sort()
	{
		list.sort();
	}
}
