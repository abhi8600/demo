/**
 * 
 */
package memdb.indexlist;

import java.io.Externalizable;

import gnu.trove.list.array.TIntArrayList;
import memdb.rowset.RowSet;

/**
 * @author bpeters
 * 
 */
public abstract class IndexList implements Externalizable
{
	private static final long serialVersionUID = 1L;

	public abstract RowSet getRowSet();

	public abstract RowSet getRowSet(RowSet baseSet);

	public abstract TIntArrayList getIntArrayList();

	public abstract int size();

	public abstract int get(int index);

	public abstract IndexList addSorted(int value);

	public abstract IndexList add(int value);

	public abstract IndexList remove(int value);

	public abstract void sort();
}
