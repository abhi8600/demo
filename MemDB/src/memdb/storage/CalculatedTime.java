package memdb.storage;

import gnu.trove.list.array.TIntArrayList;

import java.io.Externalizable;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import memdb.Database;
import memdb.rowset.RowSet;
import memdb.sql.RowRange;

/**
 * Calculated time table - instead of storing it. Works for dates past 1900 (same has pre-built time tables) Row number
 * is number of days since Jan 1 1900
 * 
 * @author bpeters
 * 
 */
public class CalculatedTime extends Table implements Externalizable
{
	private static final long serialVersionUID = 1L;

	public enum TimeLevel
	{
		Day, Week, Month, Quarter, HalfYear
	};
	public int DATE;
	public int DAY_NUMBER;
	public int DAY_SEQ_NUMBER;
	public int DAY;
	public int DAY_OF_WEEK;
	public int DAY_OF_WEEK_IN_MONTH;
	public int DAY_OF_MONTH;
	public int DAY_OF_YEAR;
	public int DAY_ID;
	public int WEEK_NUMBER;
	public int WEEK_SEQ_NUMBER;
	public int WEEK_OF_MONTH;
	public int WEEK_OF_YEAR;
	public int WEEK_START_DATE;
	public int WEEK_END_DATE;
	public int WEEK_ID;
	public int MONTH_NUMBER;
	public int MONTH_SEQ_NUMBER;
	public int MONTH;
	public int YEAR_MONTH;
	public int MONTH_OF_YEAR;
	public int TRAILING_3_MONTH_NAME;
	public int NUM_WEEKS_IN_MONTH;
	public int MONTH_ID;
	public int QUARTER_NUMBER;
	public int QUARTER_SEQ_NUMBER;
	public int QUARTER;
	public int YEAR_QUARTER;
	public int QUARTER_ID;
	public int HALF_YEAR_NUMBER;
	public int HALF_YEAR_SEQ_NUMBER;
	public int HALF_YEAR;
	public int YEAR_HALF;
	public int HALF_YEAR_ID;
	public int YEAR;
	public int YEAR_SEQ_NUMBER;
	public int YEAR_ID;
	public int DAY_TIME_PERIOD_ID;
	private TimeLevel level;
	private TimeZone tz = TimeZone.getDefault();
	private Calendar minDay = Calendar.getInstance(tz);
	private Calendar maxDay = Calendar.getInstance(tz);
	private long minDayMillis;
	private int minDayOfWeek;
	private int minMonth;
	private int minYear;
	private int numDays;
	private int numWeeks;
	private int numMonths;
	private int numQuarters;
	private int numHalfYears;
	private Locale locale;
	private TimePeriod tp;
	private Calendar[] calcache;
	private Calendar[] shiftedcalcache;

	public CalculatedTime(Database d, Locale locale, String schema, String tableName, TimePeriod tp, TimeLevel level)
	{
		this.locale = locale;
		this.tp = tp;
		this.level = level;
		tmd = new TableMetadata(4);
		initializeNew(d, null, false, false);
		this.name = tableName;
		this.schema = schema;
		minDay.setMinimalDaysInFirstWeek(7);
		minDay.set(1900, 0, 1, 0, 0);
		minDay.setLenient(false);
		minDay.getTime();
		// Re-set fields to zero as calendar can have randomness
		minDay.set(Calendar.MINUTE, 0);
		minDay.set(Calendar.SECOND, 0);
		minDay.set(Calendar.MILLISECOND, 0);
		minDayMillis = minDay.getTimeInMillis();
		minDayOfWeek = minDay.get(Calendar.DAY_OF_WEEK);
		minMonth = minDay.get(Calendar.MONTH);
		minYear = minDay.get(Calendar.YEAR);
		maxDay.setMinimalDaysInFirstWeek(7);
		maxDay.set(2100, 11, 31, 0, 0);
		maxDay.setLenient(false);
		List<String> nameList = new ArrayList<String>();
		List<Integer> typeList = new ArrayList<Integer>();
		List<Integer> widthList = new ArrayList<Integer>();
		int count = 0;
		// Date$ is part of all time tables
		DATE = count++;
		nameList.add("Date$");
		typeList.add(Types.DATE);
		widthList.add(0);
		if (level == TimeLevel.Day)
		{
			DAY_NUMBER = count++;
			nameList.add("Day_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY_SEQ_NUMBER = count++;
			nameList.add("Day_Seq_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY = count++;
			nameList.add("Day$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			DAY_OF_WEEK = count++;
			nameList.add("Day_of_Week$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY_OF_WEEK_IN_MONTH = count++;
			nameList.add("Day_of_Week_in_Month$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY_OF_MONTH = count++;
			nameList.add("Day_of_Month$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY_OF_YEAR = count++;
			nameList.add("Day_of_Year$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			DAY_ID = count++;
			nameList.add("Day_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			numDays = (int) ((maxDay.getTimeInMillis() - minDay.getTimeInMillis()) / (60 * 60 * 24 * 1000));
		}
		if (level == TimeLevel.Week || level == TimeLevel.Day)
		{
			WEEK_NUMBER = count++;
			nameList.add("Week_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			WEEK_SEQ_NUMBER = count++;
			nameList.add("Week_Seq_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			WEEK_OF_MONTH = count++;
			nameList.add("Week_of_Month$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			WEEK_OF_YEAR = count++;
			nameList.add("Week_of_Year$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			WEEK_START_DATE = count++;
			nameList.add("Week_Start_Date$");
			typeList.add(Types.DATE);
			widthList.add(0);
			WEEK_END_DATE = count++;
			nameList.add("Week_End_Date$");
			typeList.add(Types.DATE);
			widthList.add(0);
			WEEK_ID = count++;
			nameList.add("Week_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			numWeeks = (int) ((maxDay.getTimeInMillis() - minDay.getTimeInMillis()) / (60 * 60 * 24 * 1000 * 7));
		}
		if (level == TimeLevel.Month || level == TimeLevel.Week || level == TimeLevel.Day)
		{
			MONTH_NUMBER = count++;
			nameList.add("Month_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			MONTH_SEQ_NUMBER = count++;
			nameList.add("Month_Seq_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			MONTH = count++;
			nameList.add("Month$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			YEAR_MONTH = count++;
			nameList.add("Year_Month$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			MONTH_OF_YEAR = count++;
			nameList.add("Month_of_Year$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			TRAILING_3_MONTH_NAME = count++;
			nameList.add("Trailing_3_Month_Name$");
			typeList.add(Types.VARCHAR);
			widthList.add(10);
			NUM_WEEKS_IN_MONTH = count++;
			nameList.add("Num_Weeks_In_Month$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			MONTH_ID = count++;
			nameList.add("Month_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			numMonths = (maxDay.get(Calendar.YEAR) - minDay.get(Calendar.YEAR) + 1) * 12;
		}
		if (level == TimeLevel.Quarter || level == TimeLevel.Month || level == TimeLevel.Week || level == TimeLevel.Day)
		{
			QUARTER_NUMBER = count++;
			nameList.add("Quarter_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			QUARTER_SEQ_NUMBER = count++;
			nameList.add("Quarter_Seq_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			QUARTER = count++;
			nameList.add("Quarter$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			YEAR_QUARTER = count++;
			nameList.add("Year_Quarter$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			QUARTER_ID = count++;
			nameList.add("Quarter_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			numQuarters = (maxDay.get(Calendar.YEAR) - minDay.get(Calendar.YEAR) + 1) * 4;
		}
		if (level == TimeLevel.HalfYear || level == TimeLevel.Quarter || level == TimeLevel.Month || level == TimeLevel.Week || level == TimeLevel.Day)
		{
			HALF_YEAR_NUMBER = count++;
			nameList.add("Half_Year_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			HALF_YEAR_SEQ_NUMBER = count++;
			nameList.add("Half_Year_Seq_Number$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			HALF_YEAR = count++;
			nameList.add("Half$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			YEAR_HALF = count++;
			nameList.add("Year_Half$");
			typeList.add(Types.VARCHAR);
			widthList.add(100);
			HALF_YEAR_ID = count++;
			nameList.add("Half_Year_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
			numHalfYears = (maxDay.get(Calendar.YEAR) - minDay.get(Calendar.YEAR) + 1) * 2;
		}
		YEAR = count++;
		nameList.add("Year$");
		typeList.add(Types.INTEGER);
		widthList.add(0);
		YEAR_SEQ_NUMBER = count++;
		nameList.add("Year_Seq_Number$");
		typeList.add(Types.INTEGER);
		widthList.add(0);
		YEAR_ID = count++;
		nameList.add("Year_ID$");
		typeList.add(Types.INTEGER);
		widthList.add(0);
		if (tp != null)
		{
			DAY_TIME_PERIOD_ID = count++;
			if (level == TimeLevel.Day)
				nameList.add(tp.Prefix + "_Day_ID$");
			else if (level == TimeLevel.Week)
				nameList.add(tp.Prefix + "_Week_ID$");
			else if (level == TimeLevel.Month)
				nameList.add(tp.Prefix + "_Month_ID$");
			else if (level == TimeLevel.Quarter)
				nameList.add(tp.Prefix + "_Quarter_ID$");
			else if (level == TimeLevel.HalfYear)
				nameList.add(tp.Prefix + "_Half_Year_ID$");
			typeList.add(Types.INTEGER);
			widthList.add(0);
		}
		tmd.columnNames = new String[nameList.size()];
		nameList.toArray(tmd.columnNames);
		tmd.dataTypes = new int[typeList.size()];
		for (int i = 0; i < tmd.dataTypes.length; i++)
			tmd.dataTypes[i] = typeList.get(i);
		tmd.widths = new int[widthList.size()];
		for (int i = 0; i < tmd.widths.length; i++)
			tmd.widths[i] = widthList.get(i);
		calcache = new Calendar[getNumRows() + 10];
	}

	@Override
	public void initializeNew(Database d, String keyStoreName, boolean diskBased, boolean compressed)
	{
	}

	@Override
	public void drop()
	{
	}

	@Override
	public void readExternal() throws IOException, ClassNotFoundException, InterruptedException
	{
	}

	@Override
	public void writeExternal() throws IOException
	{
	}

	@Override
	public int getNumRows()
	{
		if (level == TimeLevel.Day)
			return numDays;
		else if (level == TimeLevel.Week)
			return numWeeks;
		else if (level == TimeLevel.Month)
			return numMonths;
		else if (level == TimeLevel.Quarter)
			return numQuarters;
		else if (level == TimeLevel.HalfYear)
			return numHalfYears;
		return 0;
	}

	private Calendar getDateFromRow(int rowNumber)
	{
		Calendar cal = rowNumber >= 0 ? calcache[rowNumber] : null;
		if (cal != null)
			return cal;
		if (level == TimeLevel.Day)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.DAY_OF_YEAR, rowNumber);
			if (rowNumber >= 0)
				calcache[rowNumber] = newcal;
			return newcal;
		} else if (level == TimeLevel.Week)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.WEEK_OF_YEAR, rowNumber);
			if (rowNumber >= 0)
				calcache[rowNumber] = newcal;
			return newcal;
		} else if (level == TimeLevel.Month)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.YEAR, rowNumber / 12);
			newcal.add(Calendar.MONTH, (rowNumber % 12) - 1);
			newcal.set(Calendar.DAY_OF_MONTH, 1);
			if (rowNumber >= 0)
				calcache[rowNumber] = newcal;
			return newcal;
		} else if (level == TimeLevel.Quarter)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.YEAR, rowNumber / 4);
			newcal.add(Calendar.MONTH, (rowNumber % 4) * 3);
			newcal.set(Calendar.DAY_OF_MONTH, 1);
			if (rowNumber >= 0)
				calcache[rowNumber] = newcal;
			return newcal;
		} else if (level == TimeLevel.HalfYear)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.YEAR, rowNumber / 2);
			newcal.add(Calendar.MONTH, (rowNumber % 2) * 6);
			newcal.set(Calendar.DAY_OF_MONTH, 1);
			if (rowNumber >= 0)
				calcache[rowNumber] = newcal;
			return newcal;
		}
		return (Calendar) minDay.clone();
	}

	@Override
	public Object getTableValue(int rowNumber, int columnNumber)
	{
		if (rowNumber < 0)
			return null;
		Calendar newcal = getDateFromRow(rowNumber);
		if (columnNumber == DATE)
		{
			return newcal.getTime();
		} else if (columnNumber == DAY_NUMBER)
		{
			return (newcal.get(Calendar.YEAR) * 10000) + ((newcal.get(Calendar.MONTH) + 1) * 100) + newcal.get(Calendar.DAY_OF_MONTH);
		} else if (columnNumber == DAY_SEQ_NUMBER)
		{
			return rowNumber - 5;
		} else if (columnNumber == DAY)
		{
			return newcal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);
		} else if (columnNumber == DAY_OF_WEEK)
		{
			return newcal.get(Calendar.DAY_OF_WEEK);
		} else if (columnNumber == DAY_OF_WEEK_IN_MONTH)
		{
			return newcal.get(Calendar.DAY_OF_WEEK_IN_MONTH);
		} else if (columnNumber == DAY_OF_MONTH)
		{
			return newcal.get(Calendar.DAY_OF_MONTH);
		} else if (columnNumber == DAY_OF_YEAR)
		{
			return newcal.get(Calendar.DAY_OF_YEAR);
		} else if (columnNumber == DAY_ID)
		{
			return rowNumber - 5;
		} else if (columnNumber == DAY_TIME_PERIOD_ID)
		{
			if (level == TimeLevel.Day)
				return rowNumber - 5;
			else if (level == TimeLevel.Week)
				return rowNumber - 1;
			else if (level == TimeLevel.Month)
			{
				if (tp.ShiftAmount == 0)
					return rowNumber;
				else
				{
					if (tp.ShiftPeriod == TimePeriod.PERIOD_MONTH)
						return rowNumber - tp.ShiftAmount;
					else if (tp.ShiftPeriod == TimePeriod.PERIOD_QUARTER)
						return rowNumber - (tp.ShiftAmount * 3);
					else if (tp.ShiftPeriod == TimePeriod.PERIOD_YEAR)
						return rowNumber - (tp.ShiftAmount * 12);
				}
			} else if (level == TimeLevel.Quarter)
			{
				if (tp.ShiftAmount == 0)
					return rowNumber + 1;
				else
				{
					if (tp.ShiftPeriod == TimePeriod.PERIOD_QUARTER)
						return rowNumber + 1 - tp.ShiftAmount;
					else if (tp.ShiftPeriod == TimePeriod.PERIOD_YEAR)
						return rowNumber + 1 - (tp.ShiftAmount * 4);
					return rowNumber + 1;
				}
			} else if (level == TimeLevel.HalfYear)
			{
				if (tp.ShiftAmount == 0)
					return rowNumber + 1;
				else
				{
					if (tp.ShiftPeriod == TimePeriod.PERIOD_YEAR)
						return rowNumber + 1 - (tp.ShiftAmount * 2);
					return rowNumber + 1;
				}
			}
		} else if (columnNumber == WEEK_NUMBER)
		{
			return newcal.get(Calendar.YEAR) * 10000 + (newcal.get(Calendar.MONTH) + 1) * 100 + newcal.get(Calendar.WEEK_OF_MONTH);
		} else if (columnNumber == WEEK_SEQ_NUMBER)
		{
			int numWeeks = newcal.get(Calendar.WEEK_OF_YEAR);
			Calendar addcal = (Calendar) newcal.clone();
			for (int year = newcal.get(Calendar.YEAR); year > 1900; year--)
			{
				addcal.add(Calendar.YEAR, -1);
				numWeeks += addcal.getWeeksInWeekYear();
			}
			return numWeeks;
		} else if (columnNumber == WEEK_OF_MONTH)
		{
			return newcal.get(Calendar.WEEK_OF_MONTH);
		} else if (columnNumber == WEEK_OF_YEAR)
		{
			return newcal.get(Calendar.WEEK_OF_YEAR);
		} else if (columnNumber == WEEK_START_DATE)
		{
			return newcal.getTime();
		} else if (columnNumber == WEEK_END_DATE)
		{
			return newcal.getTime();
		} else if (columnNumber == WEEK_ID)
		{
			int numWeeks = newcal.get(Calendar.WEEK_OF_YEAR);
			Calendar addcal = (Calendar) newcal.clone();
			for (int year = newcal.get(Calendar.YEAR); year > 1900; year--)
			{
				addcal.add(Calendar.YEAR, -1);
				numWeeks += addcal.getWeeksInWeekYear();
			}
			return numWeeks;
		} else if (columnNumber == MONTH_NUMBER)
		{
			return newcal.get(Calendar.YEAR) * 100 + (newcal.get(Calendar.MONTH) + 1);
		} else if (columnNumber == MONTH_SEQ_NUMBER)
		{
			return newcal.get(Calendar.MONTH) + (12 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == MONTH)
		{
			return newcal.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
		} else if (columnNumber == YEAR_MONTH)
		{
			int month = newcal.get(Calendar.MONTH) + 1;
			return newcal.get(Calendar.YEAR) + "/" + (month < 10 ? "0" : "") + month;
		} else if (columnNumber == MONTH_OF_YEAR)
		{
			return newcal.get(Calendar.MONTH) + 1;
		} else if (columnNumber == TRAILING_3_MONTH_NAME)
		{
			StringBuilder sb = new StringBuilder();
			Calendar addcal = (Calendar) newcal.clone();
			int year = newcal.get(Calendar.YEAR) - 1900;
			year = year % 100;
			int month = newcal.get(Calendar.MONTH) + 1;
			sb.append((month < 10 ? "0" : "") + month + '/' + (year < 10 ? "0" : "") + year);
			addcal.add(Calendar.MONTH, -2);
			year = addcal.get(Calendar.YEAR) - 1900;
			year = year % 100;
			month = addcal.get(Calendar.MONTH) + 1;
			sb.insert(0, (month < 10 ? "0" : "") + month + '/' + (year < 10 ? "0" : "") + year + " - ");
			return sb.toString();
		} else if (columnNumber == NUM_WEEKS_IN_MONTH)
		{
			Calendar addcal = (Calendar) newcal.clone();
			addcal.add(Calendar.MONTH, 1);
			addcal.add(Calendar.DAY_OF_MONTH, -addcal.get(Calendar.DAY_OF_MONTH));
			return addcal.get(Calendar.WEEK_OF_MONTH) - 1;
		} else if (columnNumber == MONTH_ID)
		{
			return newcal.get(Calendar.MONTH) + (12 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == QUARTER_NUMBER)
		{
			return newcal.get(Calendar.YEAR) * 10 + (newcal.get(Calendar.MONTH) / 3 + 1);
		} else if (columnNumber == QUARTER_SEQ_NUMBER)
		{
			return (newcal.get(Calendar.MONTH) / 3) + (4 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == QUARTER)
		{
			return "Q" + (newcal.get(Calendar.MONTH) / 3 + 1);
		} else if (columnNumber == YEAR_QUARTER)
		{
			return newcal.get(Calendar.YEAR) + "/Q" + (newcal.get(Calendar.MONTH) / 3 + 1);
		} else if (columnNumber == QUARTER_ID)
		{
			return (newcal.get(Calendar.MONTH) / 3) + (4 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == HALF_YEAR_NUMBER)
		{
			return newcal.get(Calendar.YEAR) * 10 + (newcal.get(Calendar.MONTH) / 6 + 1);
		} else if (columnNumber == HALF_YEAR_SEQ_NUMBER)
		{
			return (newcal.get(Calendar.MONTH) / 6) + (2 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == HALF_YEAR)
		{
			return "H" + (newcal.get(Calendar.MONTH) / 6 + 1);
		} else if (columnNumber == YEAR_HALF)
		{
			return newcal.get(Calendar.YEAR) + "/H" + (newcal.get(Calendar.MONTH) / 6 + 1);
		} else if (columnNumber == HALF_YEAR_ID)
		{
			return (newcal.get(Calendar.MONTH) / 6) + (2 * (newcal.get(Calendar.YEAR) - 1900)) + 1;
		} else if (columnNumber == YEAR)
		{
			return newcal.get(Calendar.YEAR);
		} else if (columnNumber == YEAR_SEQ_NUMBER)
		{
			return newcal.get(Calendar.YEAR) - 1900 + 1;
		} else if (columnNumber == YEAR_ID)
		{
			return newcal.get(Calendar.YEAR) - 1900 + 1;
		}
		return null;
	}

	@Override
	public void clearCache()
	{
	}

	@Override
	public void pack()
	{
	}

	private final int getRowFromDate(Date d)
	{
		if (level == TimeLevel.Day)
		{
			// Correct for missing daylight savings time on Feb 9, 1942 - seems like a Java bug
			long millis = d.getTime();
			if (tz.useDaylightTime() && millis > -880214400000L)
				millis += 3600000;
			return (int) ((millis - minDayMillis) / (60 * 60 * 24 * 1000));
		} else if (level == TimeLevel.Week)
		{
			// Correct for missing daylight savings time on Feb 9, 1942 - seems like a Java bug
			long millis = d.getTime();
			if (tz.useDaylightTime() && millis > -880214400000L)
				millis += 3600000;
			return (int) ((millis - minDayMillis) / (60 * 60 * 24 * 1000 * 7));
		} else if (level == TimeLevel.Month)
		{
			Calendar newcal = Calendar.getInstance();
			newcal.setTime(d);
			return (newcal.get(Calendar.YEAR) - minYear) * 12 + newcal.get(Calendar.MONTH) - minMonth + 1;
		} else if (level == TimeLevel.Quarter)
		{
			Calendar newcal = Calendar.getInstance();
			newcal.setTime(d);
			return (newcal.get(Calendar.YEAR) - minYear) * 4 + ((newcal.get(Calendar.MONTH) - minMonth) / 3);
		} else if (level == TimeLevel.HalfYear)
		{
			Calendar newcal = Calendar.getInstance();
			newcal.setTime(d);
			return (newcal.get(Calendar.YEAR) - minYear) * 2 + ((newcal.get(Calendar.MONTH) - minMonth) / 6);
		}
		return 0;
	}

	private final int getRowFromDate(long millis)
	{
		if (level == TimeLevel.Day)
			return (int) ((millis - minDayMillis) / (60 * 60 * 24 * 1000));
		else if (level == TimeLevel.Week)
			return (int) ((millis - minDayMillis) / (60 * 60 * 24 * 1000 * 7));
		else if (level == TimeLevel.Month)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.setTimeInMillis(millis);
			return (newcal.get(Calendar.YEAR) - minYear) * 12 + newcal.get(Calendar.MONTH) - minMonth + 1;
		} else if (level == TimeLevel.Quarter)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.setTimeInMillis(millis);
			return (newcal.get(Calendar.YEAR) - minYear) * 4 + (newcal.get(Calendar.MONTH) - minMonth) / 3;
		} else if (level == TimeLevel.HalfYear)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.setTimeInMillis(millis);
			return (newcal.get(Calendar.YEAR) - minYear) * 2 + (newcal.get(Calendar.MONTH) - minMonth) / 6;
		}
		return 0;
	}

	private void incrementDate(Calendar newcal)
	{
		if (level == TimeLevel.Day)
			newcal.add(Calendar.DATE, 1);
		else if (level == TimeLevel.Week)
			newcal.add(Calendar.DATE, 7);
		else if (level == TimeLevel.Month)
			newcal.set(Calendar.MONTH, newcal.get(Calendar.MONTH) + 1);
		else if (level == TimeLevel.Quarter)
			newcal.set(Calendar.MONTH, newcal.get(Calendar.MONTH) + 3);
		else if (level == TimeLevel.HalfYear)
			newcal.set(Calendar.MONTH, newcal.get(Calendar.MONTH) + 6);
		else
			newcal.add(Calendar.DATE, 1);
		newcal.getTime();
	}

	public RowSet getRowList(int columnNumber, Object key)
	{
		return getRowList(columnNumber, key, false);
	}

	public RowSet getRowList(int columnNumber, Object key, boolean leftSideOfJoin)
	{
		if (columnNumber == DATE)
		{
			if (key instanceof Date)
			{
				return new RowSet(new int[]
				{ getRowFromDate((Date) key) });
			}
		} else if (columnNumber == DAY_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int day = val % 100 - 1;
				int month = (int) (val / 100) % 100 - 1;
				int year = (int) val / 10000;
				Calendar newcal = (Calendar) minDay.clone();
				newcal.set(year, month, day, 0, 0, 0);
				return new RowSet(new int[]
				{ getRowFromDate(newcal.getTime()) });
			}
		} else if (columnNumber == DAY_SEQ_NUMBER)
		{
			if (key instanceof Integer)
			{
				return new RowSet(new int[]
				{ ((Integer) key) + 5 });
			}
		} else if (columnNumber == DAY)
		{
			int numRows = getNumRows();
			TIntArrayList list = new TIntArrayList(numRows / 7);
			int offset = 0;
			if (!(key instanceof String))
				return new RowSet();
			String skey = (String) key;
			if (skey.toUpperCase().equals("MONDAY"))
				offset = 1;
			if (skey.toUpperCase().equals("TUESDAY"))
				offset = 2;
			if (skey.toUpperCase().equals("WEDNESDAY"))
				offset = 3;
			if (skey.toUpperCase().equals("THURSDAY"))
				offset = 4;
			if (skey.toUpperCase().equals("FRIDAY"))
				offset = 5;
			if (skey.toUpperCase().equals("SATURDAY"))
				offset = 6;
			int min = offset - minDayOfWeek + 1;
			if (min < 0)
				min += 7;
			for (int row = min; row < numRows; row += 7)
				list.add(row);
			return new RowSet(list);
		} else if (columnNumber == DAY_OF_WEEK)
		{
			int numRows = getNumRows();
			if (!(key instanceof Integer))
				return new RowSet();
			int min = ((Integer) key) - minDayOfWeek;
			if (min < 0)
				min += 7;
			TIntArrayList list = new TIntArrayList((numRows - min) / 7);
			for (int row = min; row < numRows; row += 7)
				list.add(row);
			return new RowSet(list);
		} else if (columnNumber == DAY_OF_WEEK_IN_MONTH)
		{
			TIntArrayList list = new TIntArrayList();
			if (!(key instanceof Integer))
				return new RowSet();
			Calendar newcal = (Calendar) minDay.clone();
			while (newcal.compareTo(maxDay) < 0)
			{
				if (newcal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == (Integer) key)
				{
					list.add(getRowFromDate(newcal.getTime()));
				}
				newcal.add(Calendar.DATE, 1);
			}
			return new RowSet(list);
		} else if (columnNumber == DAY_OF_MONTH)
		{
			TIntArrayList list = new TIntArrayList();
			if (!(key instanceof Integer))
				return new RowSet();
			Calendar newcal = Calendar.getInstance();
			int startyear = minDay.get(Calendar.YEAR);
			int endyear = maxDay.get(Calendar.YEAR);
			for (int year = startyear; year <= endyear; year++)
			{
				for (int month = 0; month < 12; month++)
				{
					newcal.set(year, month, ((Integer) key) - 1, 0, 0, 0);
					list.add(getRowFromDate(newcal.getTime()));
				}
			}
			return new RowSet(list);
		} else if (columnNumber == DAY_OF_YEAR)
		{
			TIntArrayList list = new TIntArrayList();
			if (!(key instanceof Integer))
				return new RowSet();
			int startyear = minDay.get(Calendar.YEAR);
			int endyear = maxDay.get(Calendar.YEAR);
			Calendar newcal = Calendar.getInstance();
			for (int year = startyear; year <= endyear; year++)
			{
				newcal.set(year, 0, 1, 0, 0, 0);
				newcal.add(Calendar.DAY_OF_YEAR, ((Integer) key) - 1);
				list.add(getRowFromDate(newcal.getTime()));
			}
			return new RowSet(list);
		} else if (columnNumber == DAY_ID)
		{
			if (key instanceof Integer)
			{
				return new RowSet(new int[]
				{ ((Integer) key) + 5 });
			}
			if (key instanceof Long)
			{
				return new RowSet(new int[]
				{ ((Long) key).intValue() + 5 });
			}
		} else if (columnNumber == DAY_TIME_PERIOD_ID)
		{
			int id = 0;
			if (level == TimeLevel.Day)
			{
				if (key instanceof Integer)
				{
					id = ((Integer) key) + 5;
				}
				if (key instanceof Long)
				{
					id = ((Long) key).intValue() + 5;
				}
			} else if (level == TimeLevel.Quarter)
			{
				if (key instanceof Integer)
				{
					id = ((Integer) key) - 1;
				}
				if (key instanceof Long)
				{
					id = ((Long) key).intValue() - 1;
				}
			} else
			{
				if (key instanceof Integer)
				{
					id = ((Integer) key);
				}
				if (key instanceof Long)
				{
					id = ((Long) key).intValue();
				}
			}
			if (tp != null)
			{
				Calendar newcal = null;
				if (tp.ShiftAmount != 0)
				{
					if (shiftedcalcache == null)
						shiftedcalcache = new Calendar[calcache.length];
					if (id >= 0)
						newcal = shiftedcalcache[id];
					if (newcal == null)
					{
						newcal = (Calendar) getDateFromRow(id).clone();
						newcal.add(tp.getCalendarShiftPeriod(), tp.getCalendarShiftAmount());
						if (id >= 0)
							shiftedcalcache[id] = newcal;
					}
				} else
					newcal = (Calendar) getDateFromRow(id);
				if (tp.AggregationType == TimePeriod.AGG_TYPE_NONE)
				{
					return new RowSet(new int[]
					{ getRowFromDate(newcal.getTimeInMillis()) });
				} else if (tp.AggregationType == TimePeriod.AGG_TYPE_TRAILING)
				{
					if (leftSideOfJoin)
					{
						Calendar newcalstart = (Calendar) newcal.clone();
						newcalstart.add(tp.getCalendarAggregationPeriod(), -tp.getCalendarAggregationAmount() + 1);
						return new RowSet(getRowFromDate(newcalstart.getTimeInMillis()), getRowFromDate(newcal.getTimeInMillis()));
					} else
					{
						Calendar newcalend = (Calendar) newcal.clone();
						newcalend.add(tp.getCalendarAggregationPeriod(), tp.getCalendarAggregationAmount() - 1);
						return new RowSet(getRowFromDate(newcal.getTimeInMillis()), getRowFromDate(newcalend.getTimeInMillis()));
					}
				} else if (tp.AggregationType == TimePeriod.AGG_TYPE_TO_DATE)
				{
					Calendar newcalstart = (Calendar) newcal.clone();
					Calendar newcalend = (Calendar) newcal.clone();
					tp.setToDateStartAndEnd(newcalstart, newcalend, leftSideOfJoin);
					return new RowSet(getRowFromDate(newcalstart.getTimeInMillis()), getRowFromDate(newcalend.getTimeInMillis()));
				}
			}
			return new RowSet(new int[]
			{ ((Integer) key) + 5 });
		} else if (columnNumber == WEEK_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int week = val % 100;
				int month = (int) (val / 100) % 100;
				int year = (int) val / 10000;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, month, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, month, 1, 0, 0, 0);
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					if (newcal.get(Calendar.WEEK_OF_MONTH) == week)
					{
						list.add(getRowFromDate(newcal.getTime()));
					}
					newcal.add(Calendar.DATE, 1);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == WEEK_SEQ_NUMBER)
		{
			Calendar newcal = (Calendar) minDay.clone();
			newcal.add(Calendar.WEEK_OF_YEAR, (Integer) key);
			TIntArrayList list = new TIntArrayList();
			int row = getRowFromDate(newcal.getTime());
			for (int i = 0; i < 7; i++)
			{
				list.add(row + i - 1);
			}
			return new RowSet(list);
		} else if (columnNumber == WEEK_OF_MONTH)
		{
			if (key instanceof Integer)
			{
				Calendar newcal = (Calendar) minDay.clone();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(maxDay) < 0)
				{
					if (newcal.get(Calendar.WEEK_OF_MONTH) == (Integer) key)
					{
						list.add(getRowFromDate(newcal.getTime()));
					}
					newcal.add(Calendar.DATE, 1);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == WEEK_OF_YEAR)
		{
			Calendar newcal = (Calendar) minDay.clone();
			TIntArrayList list = new TIntArrayList();
			while (newcal.compareTo(maxDay) < 0)
			{
				if (newcal.get(Calendar.WEEK_OF_YEAR) == (Integer) key)
				{
					list.add(getRowFromDate(newcal.getTime()));
				}
				newcal.add(Calendar.DATE, 1);
			}
			return new RowSet(list);
		} else if (columnNumber == WEEK_START_DATE)
		{
			if (key instanceof Date)
			{
				TIntArrayList list = new TIntArrayList();
				int row = getRowFromDate((Date) key);
				for (int i = 0; i < 7; i++)
					list.add(row + i);
				return new RowSet(list);
			}
		} else if (columnNumber == WEEK_END_DATE)
		{
			if (key instanceof Date)
			{
				TIntArrayList list = new TIntArrayList();
				int row = getRowFromDate((Date) key);
				for (int i = 6; i >= 0; i--)
					list.add(row - i);
				return new RowSet(list);
			}
		} else if (columnNumber == WEEK_ID)
		{
			Calendar newcal = (Calendar) minDay.clone();
			if (key instanceof Integer)
				newcal.add(Calendar.WEEK_OF_YEAR, (Integer) key);
			else if (key instanceof Long)
				newcal.add(Calendar.WEEK_OF_YEAR, ((Long) key).intValue());
			TIntArrayList list = new TIntArrayList();
			int row = getRowFromDate(newcal.getTime());
			for (int i = 0; i < 7; i++)
			{
				list.add(row + i - 1);
			}
			return new RowSet(list);
		} else if (columnNumber == MONTH_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int month = (int) val % 100 - 1;
				int year = (int) val / 100;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, month, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, month, 1, 0, 0, 0);
				endcal.add(Calendar.MONTH, 1);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == MONTH_SEQ_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int year = val / 12 + 1900;
				int month = (val % 12) - 1;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, month, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, month, 1, 0, 0, 0);
				endcal.add(Calendar.MONTH, 1);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == MONTH)
		{
			if (key instanceof String)
			{
				String skey = (String) key;
				int offset = 0;
				if (skey.toUpperCase().equals("FEBRUARY"))
					offset = 1;
				if (skey.toUpperCase().equals("MARCH"))
					offset = 2;
				if (skey.toUpperCase().equals("APRIL"))
					offset = 3;
				if (skey.toUpperCase().equals("MAY"))
					offset = 4;
				if (skey.toUpperCase().equals("JUNE"))
					offset = 5;
				if (skey.toUpperCase().equals("JULY"))
					offset = 6;
				if (skey.toUpperCase().equals("AUGUST"))
					offset = 7;
				if (skey.toUpperCase().equals("SEPTEMBER"))
					offset = 8;
				if (skey.toUpperCase().equals("OCTOBER"))
					offset = 9;
				if (skey.toUpperCase().equals("NOVEMBER"))
					offset = 10;
				if (skey.toUpperCase().equals("DECEMBER"))
					offset = 11;
				int startyear = minDay.get(Calendar.YEAR);
				int endyear = maxDay.get(Calendar.YEAR);
				Calendar newcal = Calendar.getInstance();
				Calendar endcal = Calendar.getInstance();
				TIntArrayList list = new TIntArrayList();
				for (int year = startyear; year <= endyear; year++)
				{
					newcal.set(year, offset, 1, 0, 0, 0);
					endcal.set(year, offset + 1, 1, 0, 0, 0);
					while (newcal.compareTo(endcal) < 0)
					{
						list.add(getRowFromDate(newcal.getTime()));
						incrementDate(newcal);
					}
				}
				return new RowSet(list);
			}
		} else if (columnNumber == YEAR_MONTH)
		{
			if (key instanceof String)
			{
				String[] parts = ((String) key).split("/");
				if (parts.length == 2)
				{
					int month = 0;
					int year = 0;
					boolean success = true;
					try
					{
						month = (int) Integer.valueOf(parts[1]) - 1;
					} catch (Exception ex)
					{
						success = false;
					}
					try
					{
						year = (int) Integer.valueOf(parts[0]);
					} catch (Exception ex)
					{
						success = false;
					}
					if (success)
					{
						Calendar newcal = Calendar.getInstance();
						newcal.set(year, month, 1, 0, 0, 0);
						Calendar endcal = Calendar.getInstance();
						endcal.set(year, month, 1, 0, 0, 0);
						endcal.add(Calendar.MONTH, 1);
						newcal.getTime();
						endcal.getTime();
						TIntArrayList list = new TIntArrayList();
						while (newcal.compareTo(endcal) < 0)
						{
							list.add(getRowFromDate(newcal.getTime()));
							incrementDate(newcal);
						}
						return new RowSet(list);
					}
				}
			}
		} else if (columnNumber == MONTH_OF_YEAR)
		{
			if (key instanceof Integer)
			{
				int offset = ((Integer) key) - 1;
				Calendar newcal = Calendar.getInstance();
				Calendar endcal = Calendar.getInstance();
				TIntArrayList list = new TIntArrayList();
				int startyear = minDay.get(Calendar.YEAR);
				int endyear = maxDay.get(Calendar.YEAR);
				for (int year = startyear; year <= endyear; year++)
				{
					newcal.set(year, offset, 1, 0, 0, 0);
					endcal.set(year, offset + 1, 1, 0, 0, 0);
					while (newcal.compareTo(endcal) < 0)
					{
						list.add(getRowFromDate(newcal.getTime()));
						incrementDate(newcal);
					}
				}
				return new RowSet(list);
			}
		} else if (columnNumber == TRAILING_3_MONTH_NAME)
		{
			// Currently not supported
		} else if (columnNumber == NUM_WEEKS_IN_MONTH)
		{
			if (key instanceof Integer)
			{
				// Currently not supported
			}
		} else if (columnNumber == MONTH_ID)
		{
			if (key instanceof Integer || key instanceof Long)
			{
				int val = 0;
				if (key instanceof Integer)
					val = (Integer) key;
				else if (key instanceof Long)
					val = ((Long) key).intValue();
				int year = val / 12 + 1900;
				int month = (val % 12) - 1;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, month, 1, 1, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, month, 1, 0, 0, 0);
				endcal.add(Calendar.MONTH, 1);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == QUARTER_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int quarter = (int) val % 10;
				int year = (int) val / 10;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (quarter - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (quarter) * 3, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == QUARTER_SEQ_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int year = val / 4 + 1900;
				int quarter = val % 4;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (quarter - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (quarter) * 3, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == QUARTER)
		{
			if (key instanceof String)
			{
				int quarter = 0;
				boolean success = true;
				try
				{
					quarter = (int) Integer.valueOf(((String) key).substring(1)) - 1;
				} catch (Exception ex)
				{
					success = false;
				}
				if (success)
				{
					Calendar newcal = Calendar.getInstance();
					Calendar endcal = Calendar.getInstance();
					TIntArrayList list = new TIntArrayList();
					int startyear = minDay.get(Calendar.YEAR);
					int endyear = maxDay.get(Calendar.YEAR);
					for (int year = startyear; year <= endyear; year++)
					{
						newcal.set(year, quarter * 3, 1, 0, 0, 0);
						endcal.set(year, (quarter + 1) * 3, 0, 0, 0, 0);
						endcal.getTime();
						while (newcal.compareTo(endcal) < 0)
						{
							list.add(getRowFromDate(newcal.getTime()));
							incrementDate(newcal);
						}
					}
					return new RowSet(list);
				}
			}
		} else if (columnNumber == YEAR_QUARTER)
		{
			if (key instanceof String)
			{
				String[] parts = ((String) key).split("/");
				boolean success = true;
				if (parts.length != 2)
					success = false;
				int quarter = 0;
				try
				{
					quarter = (int) Integer.valueOf(parts[1].substring(1)) - 1;
				} catch (Exception ex)
				{
					success = false;
				}
				int year = 0;
				try
				{
					year = (int) Integer.valueOf(parts[0]);
				} catch (Exception ex)
				{
					success = false;
				}
				if (success)
				{
					Calendar newcal = Calendar.getInstance();
					newcal.set(year, (quarter - 1) * 3, 1, 0, 0, 0);
					Calendar endcal = Calendar.getInstance();
					endcal.set(year, (quarter) * 3, 0, 0, 0, 0);
					newcal.getTime();
					endcal.getTime();
					TIntArrayList list = new TIntArrayList();
					while (newcal.compareTo(endcal) < 0)
					{
						list.add(getRowFromDate(newcal.getTime()));
						incrementDate(newcal);
					}
					return new RowSet(list);
				}
			}
		} else if (columnNumber == QUARTER_ID)
		{
			if (key instanceof Integer || key instanceof Long)
			{
				int val = 0;
				if (key instanceof Integer)
					val = (Integer) key;
				else if (key instanceof Long)
					val = ((Long) key).intValue();
				int year = val / 4 + 1900;
				int quarter = val % 4;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (quarter - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (quarter) * 3, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == HALF_YEAR_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int hy = (int) val % 10;
				int year = (int) val / 10;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (hy - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (hy) * 3, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == HALF_YEAR_SEQ_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				int year = val / 2 + 1900;
				int hy = val % 2;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (hy - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (hy) * 3, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == HALF_YEAR)
		{
			if (key instanceof String)
			{
				int hy = 0;
				boolean success = true;
				try
				{
					hy = (int) Integer.valueOf(((String) key).substring(1)) - 1;
				} catch (Exception ex)
				{
					success = false;
				}
				if (success)
				{
					Calendar newcal = Calendar.getInstance();
					Calendar endcal = Calendar.getInstance();
					TIntArrayList list = new TIntArrayList();
					int startyear = minDay.get(Calendar.YEAR);
					int endyear = maxDay.get(Calendar.YEAR);
					for (int year = startyear; year <= endyear; year++)
					{
						newcal.set(year, hy * 6, 1, 0, 0, 0);
						endcal.set(year, (hy + 1) * 6, 0, 0, 0, 0);
						while (newcal.compareTo(endcal) < 0)
						{
							list.add(getRowFromDate(newcal.getTime()));
							incrementDate(newcal);
						}
					}
					return new RowSet(list);
				}
			}
		} else if (columnNumber == YEAR_HALF)
		{
			if (key instanceof String)
			{
				boolean success = true;
				String[] parts = ((String) key).split("/");
				if (parts.length != 2)
					success = false;
				int hy = 0;
				try
				{
					hy = (int) Integer.valueOf(parts[1].substring(1)) - 1;
				} catch (Exception ex)
				{
					success = false;
				}
				int year = 0;
				try
				{
					year = (int) Integer.valueOf(parts[0]);
				} catch (Exception ex)
				{
					success = false;
				}
				if (success)
				{
					Calendar newcal = Calendar.getInstance();
					newcal.set(year, (hy - 1) * 3, 1, 0, 0, 0);
					Calendar endcal = Calendar.getInstance();
					endcal.set(year, (hy) * 3, 0, 0, 0, 0);
					newcal.getTime();
					endcal.getTime();
					TIntArrayList list = new TIntArrayList();
					while (newcal.compareTo(endcal) < 0)
					{
						list.add(getRowFromDate(newcal.getTime()));
						incrementDate(newcal);
					}
					return new RowSet(list);
				}
			}
		} else if (columnNumber == HALF_YEAR_ID)
		{
			if (key instanceof Integer || key instanceof Long)
			{
				int val = 0;
				if (key instanceof Integer)
					val = (Integer) key;
				else if (key instanceof Long)
					val = ((Long) key).intValue();
				int year = val / 6 + 1900;
				int hy = val % 6;
				Calendar newcal = Calendar.getInstance();
				newcal.set(year, (hy - 1) * 3, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(year, (hy) * 3, 0, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				TIntArrayList list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == YEAR)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				TIntArrayList list = new TIntArrayList();
				Calendar newcal = Calendar.getInstance();
				newcal.set(val, 0, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(val + 1, 0, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == YEAR_SEQ_NUMBER)
		{
			if (key instanceof Integer)
			{
				int val = (Integer) key;
				TIntArrayList list = new TIntArrayList();
				Calendar newcal = Calendar.getInstance();
				newcal.set(val + 1899, 0, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(val + 1900, 0, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		} else if (columnNumber == YEAR_ID)
		{
			if (key instanceof Integer || key instanceof Long)
			{
				int val = 0;
				if (key instanceof Integer)
					val = (Integer) key;
				else if (key instanceof Long)
					val = ((Long) key).intValue();
				TIntArrayList list = new TIntArrayList();
				Calendar newcal = Calendar.getInstance();
				newcal.set(val + 1899, 0, 1, 0, 0, 0);
				Calendar endcal = Calendar.getInstance();
				endcal.set(val + 1900, 0, 1, 0, 0, 0);
				newcal.getTime();
				endcal.getTime();
				list = new TIntArrayList();
				while (newcal.compareTo(endcal) < 0)
				{
					list.add(getRowFromDate(newcal.getTime()));
					incrementDate(newcal);
				}
				return new RowSet(list);
			}
		}
		return new RowSet();
	}

	@Override
	public boolean isReferenceColumn(int columnNumber)
	{
		return false;
	}

	@Override
	public void deleteRows(RowSet rs) throws SQLException
	{
	}

	@Override
	public void updateRow(int rowNum, int colNum, Object newValue) throws SQLException
	{
	}

	@Override
	public void shrinkCache()
	{
	}

	@Override
	public boolean isNull(int dataType, Object value)
	{
		return false;
	}

	@Override
	public void release() throws IOException
	{
	}

	@Override
	public void reserve() throws IOException, ClassNotFoundException, InterruptedException
	{
	}

	@Override
	public int addColumn() throws SQLException
	{
		return YEAR_ID + 1;
	}

	@Override
	public int dropColumn(int position) throws SQLException
	{
		return YEAR_ID + 1;
	}

	@Override
	public void updateColumnData(int position, TimeZone tz) throws SQLException
	{
	}

	/**
	 * Return the week ID column name
	 * 
	 * @return
	 */
	public String getWeekIDColumnName()
	{
		return ("Week ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	public String getMonthIDColumnName()
	{
		return ("Month ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	public String getQuarterIDColumnName()
	{
		return ("Quarter ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	public String getHalfyearIDColumnName()
	{
		return ("Half Year ID");
	}

	/**
	 * Return the year ID column name
	 * 
	 * @return
	 */
	public String getYearIDColumnName()
	{
		return ("Year ID");
	}

	public static String getTimeLevelTableSuffix(TimeLevel tl, boolean shortVersion)
	{
		if (!shortVersion)
		{
			if (tl == TimeLevel.Day)
				return "DAY";
			else if (tl == TimeLevel.Week)
				return "WEEK";
			else if (tl == TimeLevel.Month)
				return "MONTH";
			else if (tl == TimeLevel.Quarter)
				return "QUARTER";
			else if (tl == TimeLevel.HalfYear)
				return "HALFYEAR";
			return null;
		} else
		{
			if (tl == TimeLevel.Day)
				return "DY";
			else if (tl == TimeLevel.Week)
				return "WK";
			else if (tl == TimeLevel.Month)
				return "MN";
			else if (tl == TimeLevel.Quarter)
				return "QT";
			else if (tl == TimeLevel.HalfYear)
				return "HY";
			return null;
		}
	}

	@Override
	public RowSet getRowList(int columnNumber, Object key, RowSet baseSet)
	{
		RowSet result = getRowList(columnNumber, key);
		if (baseSet == null)
			return result;
		return result.getIntersection(baseSet, getNumRows());
	}

	@Override
	public Object getTableValueForJoin(int rowNumber, int columnNumber)
	{
		return getTableValue(rowNumber, columnNumber);
	}

	@Override
	public RowSet getRowListForJoin(int columnNumber, Object key, RowSet baseSet)
	{
		RowSet result = getRowList(columnNumber, key);
		if (baseSet == null)
			return result;
		return result.getIntersection(baseSet, getNumRows());
	}

	@Override
	public Iterator<Object> getKeyIterator(int columnNumber)
	{
		return null;
	}

	@Override
	public int getKeyIteratorSize(int columnNumber)
	{
		return -1;
	}

	@Override
	public void indexRange(RowRange range)
	{
	}

	@Override
	public Object getSurrogateKeyValue(int surrogateKey, int columnNumber)
	{
		return null;
	}

	@Override
	public int getObjectSurrogateKey(Object value, int columnNumber, boolean caseSensitive, boolean createIfNotPresent)
	{
		return 0;
	}

	@Override
	public SurrogateKeyStore getKeyStore()
	{
		return null;
	}

	@Override
	public void updateColumnMetadata(int columnNumber)
	{
	}

	@Override
	public long getColumnSize(int columnNumber)
	{
		return 0;
	}
}
