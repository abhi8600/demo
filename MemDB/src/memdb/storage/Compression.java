package memdb.storage;

import gnu.trove.list.array.TByteArrayList;
import gnu.trove.list.array.TIntArrayList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;
import org.xerial.snappy.Snappy;

import sun.misc.IOUtils;

public class Compression
{
	private static Logger logger = Logger.getLogger(Compression.class);

	public enum Method
	{
		LZMA, ZIP, SNAPPY, SNAPPY_NO_DESERIALIZE, ZIP_NO_DESERIALIZE, LZMA_NO_DESERIALIZE
	};

	public static byte[] compressObject(Method m, Serializable o)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{
			byte[] inputbytes = null;
			if (o instanceof byte[])
				inputbytes = (byte[]) o;
			else
			{
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(o);
				oos.flush();
				baos.flush();
				inputbytes = baos.toByteArray();
				baos.close();
				oos.close();
			}
			if (m == Method.ZIP || m == Method.ZIP_NO_DESERIALIZE)
			{
				baos = new ByteArrayOutputStream();
				GZIPOutputStream gz = new GZIPOutputStream(baos);
				gz.write(inputbytes);
				gz.finish();
				baos.flush();
				return baos.toByteArray();
			} else if (m == Method.LZMA || m == Method.LZMA_NO_DESERIALIZE)
			{
				ByteArrayInputStream bais = new ByteArrayInputStream(inputbytes);
				baos = new ByteArrayOutputStream();
				SevenZip.Compression.LZMA.Encoder encoder = new SevenZip.Compression.LZMA.Encoder();
				BufferedInputStream bis = new BufferedInputStream(bais);
				BufferedOutputStream bos = new BufferedOutputStream(baos);
				encoder.SetEndMarkerMode(true);
				encoder.WriteCoderProperties(bos);
				long len = -1;
				for (int i = 0; i < 8; i++)
					bos.write((int) (len >>> (8 * i)) & 0xFF);
				encoder.Code(bis, bos, -1, -1, null);
				bos.flush();
				bos.close();
				bis.close();
				byte[] buffer = baos.toByteArray();
				return buffer;
			} else if (m == Method.SNAPPY || m == Method.SNAPPY_NO_DESERIALIZE)
			{
				byte[] buffer = Snappy.compress(inputbytes);
				return buffer;
			}
		} catch (IOException e)
		{
			logger.error(e);
		}
		return null;
	}

	public static Serializable decompressObject(Method m, byte[] buffer)
	{
		if (m == Method.LZMA || m == Method.LZMA_NO_DESERIALIZE)
		{
			ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
			BufferedInputStream bis = new BufferedInputStream(bais);
			BufferedOutputStream bos = new BufferedOutputStream(baos);
			int propertiesSize = 5;
			byte[] properties = new byte[propertiesSize];
			try
			{
				if (bis.read(properties, 0, propertiesSize) != propertiesSize)
					logger.error("input .lzma file is too short");
			} catch (IOException e1)
			{
				logger.error(e1);
				return null;
			} catch (Exception e1)
			{
				logger.error(e1);
				return null;
			}
			if (!decoder.SetDecoderProperties(properties))
			{
				logger.error("Incorrect stream properties");
				return null;
			}
			try
			{
				long outSize = 0;
				for (int i = 0; i < 8; i++)
				{
					int v = bis.read();
					if (v < 0)
					{
						logger.error("Can't read stream size");
						return null;
					}
					outSize |= ((long) v) << (8 * i);
				}
				decoder.Code(bis, bos, outSize);
				bos.flush();
				bos.close();
				bis.close();
			} catch (IOException e)
			{
				logger.error(e);
				return null;
			}
			byte[] arr = baos.toByteArray();
			if (m == Method.LZMA_NO_DESERIALIZE)
				return arr;
			bais = new ByteArrayInputStream(arr);
			Serializable result = null;
			try
			{
				ObjectInputStream ois = new ObjectInputStream(bais);
				result = (Serializable) ois.readObject();
				ois.close();
			} catch (IOException e)
			{
				logger.error(e);
				return null;
			} catch (ClassNotFoundException e)
			{
				logger.error(e);
				return null;
			}
			return result;
		} else if (m == Method.ZIP || m == Method.ZIP_NO_DESERIALIZE)
		{
			ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			TByteArrayList outlist = new TByteArrayList(buffer.length * 10);
			byte[] resultbuf;
			try
			{
				GZIPInputStream gs = new GZIPInputStream(bais);
				int nextByte;
				while ((nextByte = gs.read()) != -1)
					outlist.add((byte) nextByte);
				resultbuf = outlist.toArray();
			} catch (IOException e)
			{
				logger.error(e);
				return new byte[0];
			}
			if (m == Method.ZIP_NO_DESERIALIZE)
				return resultbuf;
			Serializable result = null;
			try
			{
				bais = new ByteArrayInputStream(resultbuf);
				ObjectInputStream ois = new ObjectInputStream(bais);
				result = (Serializable) ois.readObject();
				ois.close();
			} catch (IOException e)
			{
				logger.error(e);
				return null;
			} catch (ClassNotFoundException e)
			{
				logger.error(e);
				return null;
			}
			return result;
		} else if (m == Method.SNAPPY || m == Method.SNAPPY_NO_DESERIALIZE)
		{
			byte[] output = null;
			try
			{
				output = Snappy.uncompress(buffer);
			} catch (IOException e)
			{
				logger.error(e);
				return null;
			}
			if (m == Method.SNAPPY_NO_DESERIALIZE)
				return output;
			ObjectInputStream ois;
			Serializable result = null;
			try
			{
				ByteArrayInputStream bais = new ByteArrayInputStream(output);
				ois = new ObjectInputStream(bais);
				result = (Serializable) ois.readObject();
				ois.close();
			} catch (IOException e)
			{
				logger.error(e);
				return null;
			} catch (ClassNotFoundException e)
			{
				logger.error(e);
				return null;
			}
			return result;
		}
		return null;
	}
}
