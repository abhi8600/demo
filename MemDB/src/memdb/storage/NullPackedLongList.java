package memdb.storage;

import gnu.trove.list.TLongList;
import gnu.trove.list.array.TLongArrayList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public class NullPackedLongList implements Externalizable
{
	private static final long serialVersionUID = 1L;
	private byte numBits = -1;
	private long min;
	private byte[] buffer;
	private long nullValue = -1;
	private byte remainder;

	public NullPackedLongList()
	{
	}

	public NullPackedLongList(long[] longlist)
	{
		/*
		 * Pack an integer list into a byte array using the minimum number of bits
		 */
		if (longlist.length == 0)
		{
			numBits = 0;
			buffer = new byte[]
			{ 0, 0, 0, 0 };
			return;
		}
		long min = longlist[0];
		long max = longlist[0];
		for (int i = 1; i < longlist.length; i++)
		{
			if (longlist[i] == Long.MAX_VALUE)
				continue;
			if (longlist[i] < min)
				min = longlist[i];
			if (longlist[i] > max)
				max = longlist[i];
		}
		this.min = min;
		calcNumBits(max - min);
		if (numBits == 0)
		{
			buffer = new byte[4];
			buffer[0] = (byte) (longlist.length & 0xFF);
			buffer[1] = (byte) ((longlist.length >>> 8) & 0xFF);
			buffer[2] = (byte) ((longlist.length >>> 16) & 0xFF);
			buffer[3] = (byte) ((longlist.length >>> 24) & 0xFF);
			return;
		}
		int len = (longlist.length * numBits / Byte.SIZE) + ((longlist.length * numBits) % Byte.SIZE != 0 ? 1 : 0);
		buffer = new byte[len];
		remainder = (byte) (buffer.length * Byte.SIZE / numBits - longlist.length);
		int numBytesPerValue = (numBits - 1) / Byte.SIZE + 1;
		for (int i = 0, curbit = 0; i < longlist.length; i++, curbit += numBits)
		{
			int curbyte = curbit / Byte.SIZE;
			byte bitoffset = (byte) (curbit % Byte.SIZE);
			long val = longlist[i];
			if (val == Long.MAX_VALUE)
				val = nullValue;
			else
				val = val - min;
			byte overflowbyte = (byte) ((val >>> (64 - bitoffset)) & 0xFF);
			val = val << bitoffset;
			for (int j = 0, startbits = numBits; j < numBytesPerValue; j++, val = val >>> Byte.SIZE, curbyte++, startbits -= Byte.SIZE)
			{
				if (j == 1)
					// Bring back overflow
					val |= ((long) overflowbyte) << 56;
				buffer[curbyte] |= (byte) (val & 0xFF);
				if (bitoffset + (startbits > Byte.SIZE ? Byte.SIZE : startbits) > Byte.SIZE)
					buffer[curbyte + 1] |= (byte) ((val >>> Byte.SIZE) & 0xFF);
			}
		}
		/*
		 * Code to validate that the packed results match the input - only used for finite debugging so commented out
		 * regularly
		 * 
		 * List<Object> tlist = getObjectList(); if (tlist.size() != longlist.length) { int a = size(); } for (int i =
		 * 0; i < tlist.size(); i++) { if ((Long) tlist.get(i) != longlist[i]) { int a = 1; } }
		 */
	}

	public NullPackedLongList(TLongList longlist)
	{
		/*
		 * Pack an integer list into a byte array using the minimum number of bits
		 */
		int size = longlist.size();
		if (size == 0)
		{
			numBits = 0;
			buffer = new byte[]
			{ 0, 0, 0, 0 };
			return;
		}
		long min = longlist.get(0);
		long max = longlist.get(0);
		for (int i = 1; i < size; i++)
		{
			if (longlist.get(i) == Long.MAX_VALUE)
				continue;
			if (longlist.get(i) < min)
				min = longlist.get(i);
			else if (longlist.get(i) > max)
				max = longlist.get(i);
		}
		this.min = min;
		calcNumBits(max - min);
		if (numBits == 0)
		{
			buffer = new byte[4];
			buffer[0] = (byte) (size & 0xFF);
			buffer[1] = (byte) ((size >>> 8) & 0xFF);
			buffer[2] = (byte) ((size >>> 16) & 0xFF);
			buffer[3] = (byte) ((size >>> 24) & 0xFF);
			return;
		}
		int len = (size * numBits / Byte.SIZE) + ((size * numBits) % Byte.SIZE != 0 ? 1 : 0);
		buffer = new byte[len];
		remainder = (byte) (buffer.length * Byte.SIZE / numBits - size);
		int numBytesPerValue = (numBits - 1) / Byte.SIZE + 1;
		for (int i = 0, curbit = 0; i < size; i++, curbit += numBits)
		{
			int curbyte = curbit / Byte.SIZE;
			byte bitoffset = (byte) (curbit % Byte.SIZE);
			long val = longlist.get(i);
			if (val == Long.MAX_VALUE)
				val = nullValue;
			else
				val = val - min;
			byte overflowbyte = (byte) ((val >>> (64 - bitoffset)) & 0xFF);
			val = val << bitoffset;
			for (int j = 0, startbits = numBits; j < numBytesPerValue; j++, val = val >>> Byte.SIZE, curbyte++, startbits -= Byte.SIZE)
			{
				if (j == 1)
					// Bring back overflow
					val |= ((long) overflowbyte) << 56;
				buffer[curbyte] |= (byte) (val & 0xFF);
				if (bitoffset + (startbits > Byte.SIZE ? Byte.SIZE : startbits) > Byte.SIZE)
					buffer[curbyte + 1] |= (byte) ((val >>> Byte.SIZE) & 0xFF);
			}
		}
		/*
		 * Code to validate that the packed results match the input - only used for finite debugging so commented out
		 * regularly
		 * 
		 * List<Object> tlist = getObjectList(); if (tlist.size() != longlist.size()) { int a = size(); } for (int i =
		 * 0; i < tlist.size(); i++) { if ((Long) tlist.get(i) != longlist.get(i)) { int a = 1; } }
		 */
	}

	private void calcNumBits(long maxValue)
	{
		maxValue = maxValue + 1;
		nullValue = maxValue;
		for (int i = 0; i < Long.SIZE; i++)
		{
			if ((maxValue & 1) > 0)
				numBits = (byte) (i + 1);
			maxValue = maxValue >>> 1;
		}
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeByte(numBits);
		out.writeLong(min);
		out.writeLong(nullValue);
		out.writeByte(remainder);
		out.writeInt(buffer.length);
		for (int i = 0; i < buffer.length; i++)
			out.writeByte(buffer[i]);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		numBits = in.readByte();
		min = in.readLong();
		nullValue = in.readLong();
		remainder = in.readByte();
		int len = in.readInt();
		buffer = new byte[len];
		for (int i = 0; i < len; i++)
			buffer[i] = in.readByte();
	}

	public int size()
	{
		if (numBits < 0)
			return 0;
		if (numBits == 0)
		{
			int b3 = buffer[3];
			if (b3 < 0)
				b3 += 256;
			int b2 = buffer[2];
			if (b2 < 0)
				b2 += 256;
			int b1 = buffer[1];
			if (b1 < 0)
				b1 += 256;
			int b0 = buffer[0];
			if (b0 < 0)
				b0 += 256;
			return (b3 << 24) + (b2 << 16) + (b1 << 8) + b0;
		}
		return buffer.length * Byte.SIZE / numBits - remainder;
	}

	public long get(int index)
	{
		if (numBits == 0)
			return min;
		int startbit = index * numBits;
		int curbyte = startbit / Byte.SIZE;
		int endbyte = (startbit + numBits - 1) / Byte.SIZE;
		byte bitoffset = (byte) (startbit % Byte.SIZE);
		byte rembits = (byte) ((startbit + numBits - 1) % Byte.SIZE + 1);
		int remmask = (1 << bitoffset) - 1;
		long resultlower = 0;
		long resultupper = 0;
		for (int bytenum = curbyte, pos = 0; bytenum <= endbyte; bytenum++, pos++)
		{
			long curval = bytenum >= buffer.length ? 0 : buffer[bytenum];
			if (curval < 0)
			{
				curval += 256;
			}
			if (bytenum == endbyte)
			{
				curval &= ((1 << rembits) - 1);
			}
			if (pos < 4)
				resultlower |= curval << (Byte.SIZE * pos);
			else
				resultupper |= curval << (Byte.SIZE * (pos - 4));
		}
		resultlower = resultlower >> bitoffset;
		long shiftin = resultupper & remmask;
		resultlower |= shiftin << (32 - bitoffset);
		resultupper = resultupper >> bitoffset;
		long result = resultlower | (resultupper << 32);
		if (result == nullValue)
			return Long.MAX_VALUE;
		return (long) (result + min);
	}

	public TLongArrayList getLongArrayList()
	{
		int size = size();
		TLongArrayList list = new TLongArrayList(size * 3 / 2);
		for (int i = 0; i < size; i++)
			list.add(get(i));
		return list;
	}

	public long[] getLongArray()
	{
		long[] arr = new long[size()];
		int index = 0;
		for (int i = 0; i < arr.length; i++)
		{
			// Insert content of get here
			if (numBits == 0)
			{
				arr[index++] = min;
				continue;
			}
			int startbit = i * numBits;
			int curbyte = startbit / Byte.SIZE;
			int endbyte = (startbit + numBits - 1) / Byte.SIZE;
			byte bitoffset = (byte) (startbit % Byte.SIZE);
			byte rembits = (byte) ((startbit + numBits - 1) % Byte.SIZE + 1);
			int remmask = (1 << bitoffset) - 1;
			long resultlower = 0;
			long resultupper = 0;
			for (int bytenum = curbyte, pos = 0; bytenum <= endbyte; bytenum++, pos++)
			{
				long curval = bytenum >= buffer.length ? 0 : buffer[bytenum];
				if (curval < 0)
				{
					curval += 256;
				}
				if (bytenum == endbyte)
				{
					curval &= ((1 << rembits) - 1);
				}
				if (pos < 4)
					resultlower |= curval << (Byte.SIZE * pos);
				else
					resultupper |= curval << (Byte.SIZE * (pos - 4));
			}
			resultlower = resultlower >> bitoffset;
			long shiftin = resultupper & remmask;
			resultlower |= shiftin << (32 - bitoffset);
			resultupper = resultupper >> bitoffset;
			long result = resultlower | (resultupper << 32);
			if (result == nullValue)
				arr[index++] = Long.MAX_VALUE;
			else
				arr[index++] = (long) (result + min);
		}
		return arr;
	}

	public List<Object> getObjectList()
	{
		int size = size();
		List<Object> list = new ArrayList<Object>(size * 3 / 2);
		for (int i = 0; i < size; i++)
		{
			// Insert content of get here
			if (numBits == 0)
			{
				list.add(min);
				continue;
			}
			int startbit = i * numBits;
			int curbyte = startbit / Byte.SIZE;
			int endbyte = (startbit + numBits - 1) / Byte.SIZE;
			byte bitoffset = (byte) (startbit % Byte.SIZE);
			byte rembits = (byte) ((startbit + numBits - 1) % Byte.SIZE + 1);
			int remmask = (1 << bitoffset) - 1;
			long resultlower = 0;
			long resultupper = 0;
			for (int bytenum = curbyte, pos = 0; bytenum <= endbyte; bytenum++, pos++)
			{
				long curval = bytenum >= buffer.length ? 0 : buffer[bytenum];
				if (curval < 0)
				{
					curval += 256;
				}
				if (bytenum == endbyte)
				{
					curval &= ((1 << rembits) - 1);
				}
				if (pos < 4)
					resultlower |= curval << (Byte.SIZE * pos);
				else
					resultupper |= curval << (Byte.SIZE * (pos - 4));
			}
			resultlower = resultlower >> bitoffset;
			long shiftin = resultupper & remmask;
			resultlower |= shiftin << (32 - bitoffset);
			resultupper = resultupper >> bitoffset;
			long result = resultlower | (resultupper << 32);
			if (result == nullValue)
				list.add(Long.MAX_VALUE);
			else
				list.add((long) (result + min));
		}
		return list;
	}

	public boolean contains(long value)
	{
		return find(value) >= 0;
	}

	public int find(long value)
	{
		if (numBits == 0)
		{
			if (min == value)
			{
				if (buffer.length == 4 && buffer[0] == 0 && buffer[1] == 0 && buffer[2] == 0 && buffer[3] == 0)
					return -1;
				else
					return 0;
			} else
				return -1;
		}
		int min = 0;
		int max = size() - 1;
		int mid = 0;
		long val = 0;
		do
		{
			mid = (min + max) / 2;
			val = get(mid);
			if (val == value)
				return mid;
			if (value > val)
				min = mid + 1;
			else
				max = mid - 1;
		} while (min <= max);
		return -1;
	}
}
