package memdb.storage;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Iterator;
import java.util.TimeZone;

import memdb.Database;
import memdb.TableLatch;
import memdb.rowset.RowSet;
import memdb.sql.RowRange;

public abstract class Table implements Externalizable
{
	public static int FLOATNULL = Float.floatToIntBits(Float.NaN);
	public String schema = "dbo";
	public Database database;
	public String name;
	public boolean tempTable;
	public TableMetadata tmd = null;
	protected boolean changed = false;
	public TableLatch latch = new TableLatch(this);
	private int estimatedRows = -1;

	public void rename(String newName)
	{
		name = newName;
	}

	public abstract void initializeNew(Database d, String keyStoreName, boolean diskKeyStore, boolean compressedKeyStore);

	public abstract void drop();

	public abstract void readExternal() throws IOException, ClassNotFoundException, InterruptedException;

	public abstract void updateColumnMetadata(int columnNumber);

	public abstract void writeExternal() throws IOException;

	public abstract int getNumRows();

	public abstract long getColumnSize(int columnNumber);

	public abstract Object getTableValue(int rowNumber, int columnNumber);

	public abstract Object getTableValueForJoin(int rowNumber, int columnNumber);

	public abstract void clearCache();

	public int getNumColumns()
	{
		return tmd.getNumColumns();
	}

	public abstract void pack();

	public void addRow(Object[] row, TimeZone tz, RowRange rowRange)
	{
		if (tmd.primarykey != null)
		{
			boolean hasKey = false;
			for (int i = 0; i < tmd.primarykey.length; i++)
			{
				if (tmd.primarykey[i] && row[i] == null && (tmd.dataTypes[i] == Types.BIGINT || tmd.dataTypes[i] == Types.INTEGER))
				{
					hasKey = true;
				}
			}
			if (hasKey)
			{
				int surrogateKey = tmd.primaryKeyIndex.getAndIncrement();
				for (int i = 0; i < tmd.primarykey.length; i++)
				{
					if (tmd.primarykey[i] && row[i] == null && (tmd.dataTypes[i] == Types.BIGINT || tmd.dataTypes[i] == Types.INTEGER))
					{
						row[i] = surrogateKey;
					}
				}
			}
		}
	}

	public abstract RowSet getRowListForJoin(int columnNumber, Object key, RowSet baseSet);

	public abstract RowSet getRowList(int columnNumber, Object key);

	public abstract RowSet getRowList(int columnNumber, Object key, RowSet baseSet);

	public abstract boolean isReferenceColumn(int columnNumber);

	public abstract void deleteRows(RowSet rs) throws SQLException;

	public abstract void updateRow(int rowNum, int colNum, Object newValue) throws SQLException;

	public abstract void shrinkCache();

	public void flush() throws IOException
	{
		try
		{
			if (!tempTable)
				writeExternal();
			else
				drop();
		} catch (IOException e)
		{
			throw e;
		}
	}

	public abstract boolean isNull(int dataType, Object value);

	public abstract void release() throws IOException;

	public abstract void reserve() throws IOException, ClassNotFoundException, InterruptedException;

	public abstract Iterator<Object> getKeyIterator(int columnNumber);

	public abstract int getKeyIteratorSize(int columnNumber);

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(name);
		out.writeObject(schema);
		out.writeObject(tmd);
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		name = (String) in.readObject();
		schema = (String) in.readObject();
		tmd = (TableMetadata) in.readObject();
	}

	public abstract int addColumn() throws SQLException;

	public abstract int dropColumn(int position) throws SQLException;

	public abstract void updateColumnData(int position, TimeZone tz) throws SQLException, IOException;

	/**
	 * Bulk index a range of rows. This saves memory by only indexing a few columns at a time, leaving relatively few
	 * unpacked indexes at a given time (reducing memory overhead).
	 * 
	 * @param range
	 */
	public abstract void indexRange(RowRange range);

	public boolean hasBeenChangedAndNotFlushed()
	{
		return changed;
	}

	public String getCreateDDL(String schema)
	{
		return tmd.getCreateDDL(schema, name);
	}

	public abstract Object getSurrogateKeyValue(int surrogateKey, int columnNumber);

	public abstract int getObjectSurrogateKey(Object value, int columnNumber, boolean caseSensitive, boolean createIfNotPresent);

	public abstract SurrogateKeyStore getKeyStore();

	public int getEstimatedRows()
	{
		return estimatedRows;
	}

	public void setEstimatedRows(int estimatedRows)
	{
		this.estimatedRows = estimatedRows;
	}

	public boolean isChanged()
	{
		return changed;
	}

	public void setChanged(boolean changed)
	{
		this.changed = changed;
	}

	public long getMemoryUsed()
	{
		return 0;
	}
}
