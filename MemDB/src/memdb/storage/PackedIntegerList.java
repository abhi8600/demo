package memdb.storage;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

public class PackedIntegerList implements Externalizable
{
	private static final long serialVersionUID = 1L;
	private byte numBits = -1;
	private int min;
	private byte[] buffer;
	private byte remainder;

	public PackedIntegerList()
	{
	}

	public PackedIntegerList(int[] intlist)
	{
		/*
		 * Pack an integer list into a byte array using the minimum number of bits
		 */
		if (intlist.length == 0)
		{
			numBits = 0;
			buffer = new byte[]
			{ 0, 0, 0, 0 };
			return;
		}
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		int count = 0;
		for (int i = 0; i < intlist.length; i++)
		{
			if (intlist[i] == Integer.MIN_VALUE)
				continue;
			if (intlist[i] < min)
				min = intlist[i];
			if (intlist[i] > max)
				max = intlist[i];
			count++;
		}
		this.min = min;
		if (count == 0)
		{
			min = Integer.MIN_VALUE;
			max = min;
		}
		numBits = 0;
		calcNumBits(max - min);
		if (numBits == 0)
		{
			buffer = new byte[4];
			buffer[0] = (byte) (intlist.length & 0xFF);
			buffer[1] = (byte) ((intlist.length >>> 8) & 0xFF);
			buffer[2] = (byte) ((intlist.length >>> 16) & 0xFF);
			buffer[3] = (byte) ((intlist.length >>> 24) & 0xFF);
			return;
		}
		int len = (intlist.length * numBits / Byte.SIZE) + ((intlist.length * numBits) % Byte.SIZE != 0 ? 1 : 0);
		buffer = new byte[len];
		remainder = (byte) (buffer.length * Byte.SIZE / numBits - intlist.length);
		int numBytesPerValue = (numBits - 1) / Byte.SIZE + 1;
		for (int i = 0, curbit = 0; i < intlist.length; i++, curbit += numBits)
		{
			int curbyte = curbit / Byte.SIZE;
			byte bitoffset = (byte) (curbit % Byte.SIZE);
			long val = intlist[i];
			val = val - min;
			val = val << bitoffset;
			for (int j = 0, startbits = numBits; j < numBytesPerValue; j++, val = val >>> Byte.SIZE, curbyte++, startbits -= Byte.SIZE)
			{
				buffer[curbyte] |= (byte) (val & 0xFF);
				if (bitoffset + (startbits > Byte.SIZE ? Byte.SIZE : startbits) > Byte.SIZE)
					buffer[curbyte + 1] |= (byte) ((val >>> Byte.SIZE) & 0xFF);
			}
		}
		/*
		 * Code to validate that the packed results match the input - only used for finite debugging so commented out
		 * regularly
		 * 
		 * List<Object> tlist = getObjectList(); if (tlist.size() != intlist.length) { int a = size(); } for (int i = 0;
		 * i < tlist.size(); i++) { if ((Integer) tlist.get(i) != intlist[i]) { int a = 1; tlist = getObjectList(); } }
		 */
	}

	public PackedIntegerList(TIntList intlist)
	{
		/*
		 * Pack an integer list into a byte array using the minimum number of bits
		 */
		int size = intlist.size();
		if (size == 0)
		{
			numBits = 0;
			buffer = new byte[]
			{ 0, 0, 0, 0 };
			return;
		}
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		int count = 0;
		for (int i = 0; i < size; i++)
		{
			int val = intlist.get(i);
			if (val == Integer.MIN_VALUE)
				continue;
			if (val < min)
				min = val;
			else if (val > max)
				max = val;
			count++;
		}
		this.min = min;
		if (count == 0)
		{
			min = Integer.MIN_VALUE;
			max = min;
		}
		numBits = 0;
		calcNumBits(max - min);
		if (numBits == 0)
		{
			buffer = new byte[4];
			buffer[0] = (byte) (size & 0xFF);
			buffer[1] = (byte) ((size >>> 8) & 0xFF);
			buffer[2] = (byte) ((size >>> 16) & 0xFF);
			buffer[3] = (byte) ((size >>> 24) & 0xFF);
			return;
		}
		int len = (size * numBits / Byte.SIZE) + ((size * numBits) % Byte.SIZE != 0 ? 1 : 0);
		buffer = new byte[len];
		remainder = (byte) (buffer.length * Byte.SIZE / numBits - size);
		int numBytesPerValue = (numBits - 1) / Byte.SIZE + 1;
		for (int i = 0, curbit = 0; i < size; i++, curbit += numBits)
		{
			int curbyte = curbit / Byte.SIZE;
			byte bitoffset = (byte) (curbit % Byte.SIZE);
			long val = intlist.get(i);
			val = val - min;
			val = val << bitoffset;
			for (int j = 0, startbits = numBits; j < numBytesPerValue; j++, val = val >>> Byte.SIZE, curbyte++, startbits -= Byte.SIZE)
			{
				buffer[curbyte] |= (byte) (val & 0xFF);
				if (bitoffset + (startbits > Byte.SIZE ? Byte.SIZE : startbits) > Byte.SIZE)
					buffer[curbyte + 1] |= (byte) ((val >>> Byte.SIZE) & 0xFF);
			}
		}
		/*
		 * Code to validate that the packed results match the input - only used for finite debugging so commented out
		 * regularly
		 * 
		 * List<Object> tlist = getObjectList(); if (tlist.size() != intlist.size()) { int a = size(); } for (int i = 0;
		 * i < tlist.size(); i++) { if ((Integer) tlist.get(i) != intlist.get(i)) { int a = 1; } }
		 */
	}

	private void calcNumBits(int maxValue)
	{
		for (int i = 0; i < Integer.SIZE; i++)
		{
			if ((maxValue & 1) > 0)
				numBits = (byte) (i + 1);
			maxValue = maxValue >>> 1;
		}
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeByte(numBits);
		out.writeInt(min);
		out.writeInt(buffer.length);
		for (int i = 0; i < buffer.length; i++)
			out.writeByte(buffer[i]);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		numBits = in.readByte();
		min = in.readInt();
		int len = in.readInt();
		buffer = new byte[len];
		for (int i = 0; i < len; i++)
			buffer[i] = in.readByte();
	}

	public int size()
	{
		if (numBits < 0)
			return 0;
		if (numBits == 0)
		{
			int b3 = buffer[3];
			if (b3 < 0)
				b3 += 256;
			int b2 = buffer[2];
			if (b2 < 0)
				b2 += 256;
			int b1 = buffer[1];
			if (b1 < 0)
				b1 += 256;
			int b0 = buffer[0];
			if (b0 < 0)
				b0 += 256;
			return (b3 << 24) + (b2 << 16) + (b1 << 8) + b0;
		}
		return buffer.length * Byte.SIZE / numBits - remainder;
	}

	public int get(int index)
	{
		if (numBits == 0)
			return min;
		int startbit = index * numBits;
		int curbyte = startbit / Byte.SIZE;
		int endbyte = (startbit + numBits - 1) / Byte.SIZE;
		byte bitoffset = (byte) (startbit % Byte.SIZE);
		byte rembits = (byte) ((startbit + numBits - 1) % Byte.SIZE + 1);
		long result = 0;
		for (int bytenum = curbyte, pos = 0; bytenum <= endbyte; bytenum++, pos++)
		{
			long curval = buffer[bytenum];
			if (curval < 0)
			{
				curval += 256;
			}
			if (bytenum == endbyte)
			{
				curval &= ((1 << rembits) - 1);
			}
			result |= curval << (Byte.SIZE * pos);
		}
		result = result >>> bitoffset;
		return (int) (result + min);
	}

	public TIntArrayList getIntArrayList()
	{
		int size = size();
		TIntArrayList list = new TIntArrayList(size);
		for (int i = 0; i < size; i++)
			list.add(get(i));
		return list;
	}

	public int[] getIntArray()
	{
		int[] arr = new int[size()];
		for (int i = 0; i < arr.length; i++)
			arr[i] = get(i);
		return arr;
	}

	public List<Integer> getList()
	{
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < size(); i++)
			list.add(get(i));
		return list;
	}

	public List<Object> getObjectList()
	{
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < size(); i++)
			list.add(get(i));
		return list;
	}

	public boolean contains(int value)
	{
		if (numBits == 0)
			return min == value;
		int min = 0;
		int max = size() - 1;
		int mid = 0;
		int val = 0;
		do
		{
			mid = (min + max) / 2;
			val = get(mid);
			if (val == value)
				return true;
			if (value > val)
				min = mid + 1;
			else
				max = mid - 1;
		} while (min <= max);
		return false;
	}

	public int numBytesStorage()
	{
		return buffer.length + 6;
	}
}
