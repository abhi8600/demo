package memdb.storage;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TIntObjectProcedure;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import memdb.Database;
import memdb.rowset.RowSet;
import memdb.sql.RowRange;

/*
 * Limitations of Basic Tables
 * - Case sensitive only
 * - Limited to 32-bit values (floats and ints, doubles and longs are truncated)
 * - No compression
 */
public class BasicTable extends Table
{
	// Private - per storage method
	@SuppressWarnings("rawtypes")
	List<TIntObjectMap> columns = null;
	private Object[][] values = null;
	@SuppressWarnings("rawtypes")
	private List<TObjectIntMap> colValueMap = null;
	private List<int[]> rows = new ArrayList<int[]>();

	@SuppressWarnings("rawtypes")
	public void initializeNew(Database d, String keyStoreName, boolean diskKeyStore, boolean compressedKeyStore)
	{
		rows = new ArrayList<int[]>();
		columns = new ArrayList<TIntObjectMap>();
		colValueMap = new ArrayList<TObjectIntMap>();
		tmd.lineNumber.set(0);
		changed = true;
	}

	public void rename(String newName)
	{
		super.rename(newName);
		File f = new File(database.path + File.separator + schema + File.separator + name + ".columns");
		f.renameTo(new File(database.path + File.separator + schema + File.separator + newName + ".columns"));
		f = new File(database.path + File.separator + schema + File.separator + name + ".values");
		f.renameTo(new File(database.path + File.separator + schema + File.separator + newName + ".values"));
		f = new File(database.path + File.separator + schema + File.separator + name + ".rows");
		f.renameTo(new File(database.path + File.separator + schema + File.separator + newName + ".rows"));
		f = new File(database.path + File.separator + schema + File.separator + name + ".md");
		f.renameTo(new File(database.path + File.separator + schema + File.separator + newName + ".md"));
	}

	public void drop()
	{
		if (schema.equals("INFORMATION_SCHEMA"))
			return;
		rows.clear();
		rows = null;
		columns.clear();
		columns = null;
		values = null;
		colValueMap.clear();
		colValueMap = null;
		tmd.lineNumber.set(0);
		File f = new File(database.path);
		if (!f.exists())
		{
			return;
		}
		f = new File(database.path + File.separator + schema);
		if (!f.exists())
		{
			return;
		}
		f = new File(database.path + File.separator + schema + File.separator + name + ".columns");
		f.delete();
		f = new File(database.path + File.separator + schema + File.separator + name + ".values");
		f.delete();
		f = new File(database.path + File.separator + schema + File.separator + name + ".rows");
		f.delete();
		f = new File(database.path + File.separator + schema + File.separator + name + ".md");
		f.delete();
	}

	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	public void readExternal() throws IOException, ClassNotFoundException
	{
		if (schema.equals("INFORMATION_SCHEMA"))
			return;
		File f = new File(database.path + File.separator + schema);
		if (!f.exists())
			throw new IOException("Database not found: " + database.path + File.separator + schema);
		FileInputStream fos = new FileInputStream(database.path + File.separator + schema + File.separator + name + ".columns");
		ObjectInputStream saveFile = new ObjectInputStream(fos);
		columns = (List<TIntObjectMap>) saveFile.readObject();
		fos.close();
		fos = new FileInputStream(database.path + File.separator + schema + File.separator + name + ".values");
		saveFile = new ObjectInputStream(fos);
		colValueMap = (List<TObjectIntMap>) saveFile.readObject();
		fos.close();
		fos = new FileInputStream(database.path + File.separator + schema + File.separator + name + ".rows");
		saveFile = new ObjectInputStream(fos);
		rows = (List<int[]>) saveFile.readObject();
		fos.close();
		fos = new FileInputStream(database.path + File.separator + schema + File.separator + name + ".md");
		saveFile = new ObjectInputStream(fos);
		tmd = (TableMetadata) saveFile.readObject();
		fos.close();
		createValues();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(colValueMap);
		oos.close();
		baos.close();
		changed = false;
	}

	@SuppressWarnings("rawtypes")
	private void createValues()
	{
		values = new Object[colValueMap.size()][];
		for (int i = 0; i < colValueMap.size(); i++)
		{
			TObjectIntMap colMap = colValueMap.get(i);
			if (colMap == null)
				continue;
			values[i] = new Object[colMap.size()];
			for (Object key : colMap.keys())
			{
				int index = colMap.get(key);
				values[i][index] = key;
			}
		}
		changed = true;
	}

	public void writeExternal() throws IOException
	{
		if (!changed)
			return;
		if (schema.equals("INFORMATION_SCHEMA"))
			return;
		/*
		 * When delete is added, will need to make sure there are no "missing" column indexes. Will have to do a pass
		 * over each column index and renumber them if there are holes. This is required because building the values
		 * structure on load assumes that there are as many elements in the structure as the top index
		 */
		File f = new File(database.path);
		if (!f.exists())
		{
			f.mkdir();
		}
		f = new File(database.path + File.separator + schema);
		if (!f.exists())
		{
			f.mkdir();
		}
		FileOutputStream fos = new FileOutputStream(database.path + File.separator + schema + File.separator + name + ".columns");
		ObjectOutputStream saveFile = new ObjectOutputStream(fos);
		saveFile.writeObject(columns);
		fos.close();
		fos = new FileOutputStream(database.path + File.separator + schema + File.separator + name + ".values");
		saveFile = new ObjectOutputStream(fos);
		saveFile.writeObject(colValueMap);
		fos.close();
		fos = new FileOutputStream(database.path + File.separator + schema + File.separator + name + ".rows");
		saveFile = new ObjectOutputStream(fos);
		saveFile.writeObject(rows);
		fos.close();
		fos = new FileOutputStream(database.path + File.separator + schema + File.separator + name + ".md");
		saveFile = new ObjectOutputStream(fos);
		saveFile.writeObject(tmd);
		fos.close();
		changed = false;
	}

	public int getNumRows()
	{
		return rows.size();
	}

	@SuppressWarnings("unchecked")
	public void pack()
	{
		for (int i = 0; i < getNumColumns(); i++)
		{
			@SuppressWarnings("rawtypes")
			TIntObjectMap iomap = columns.get(i);
			PackList pl = new PackList(iomap);
			iomap.forEachEntry(pl);
		}
		createValues();
		changed = true;
	}

	@SuppressWarnings("rawtypes")
	private class PackList implements TIntObjectProcedure
	{
		TIntObjectMap iomap;

		public PackList(TIntObjectMap iomap)
		{
			this.iomap = iomap;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute(int key, Object value)
		{
			if (value instanceof int[])
				return true;
			List<Integer> llist = (List<Integer>) value;
			int[] newlist = new int[llist.size()];
			for (int i = 0; i < llist.size(); i++)
				newlist[i] = llist.get(i);
			iomap.put(key, newlist);
			return true;
		}
	}

	@SuppressWarnings(
	{ "rawtypes", "unchecked" })
	public synchronized void addRow(Object[] row, TimeZone tz, RowRange rowRange)
	{
		super.addRow(row, tz, rowRange);
		changed = true;
		int[] rowarr = new int[tmd.dataTypes.length];
		for (int i = 0; i < row.length; i++)
		{
			Object key = row[i];
			if (key instanceof Double)
			{
				key = ((Double) key).floatValue();
			} else if (key instanceof BigDecimal)
			{
				key = ((BigDecimal) key).floatValue();
			}
			TIntObjectMap iomap = null;
			TObjectIntMap oimap = null;
			if (rows.size() == 0)
			{
				iomap = new TIntObjectHashMap(100);
				columns.add(iomap);
				if (key instanceof Float)
				{
					colValueMap.add(null);
				} else
				{
					oimap = new TObjectIntHashMap(100);
					colValueMap.add(oimap);
				}
			} else
			{
				iomap = columns.get(i);
				oimap = colValueMap.get(i);
			}
			if (key != null)
			{
				List<Integer> rowlist;
				int keyint;
				if (key instanceof Float)
				{
					keyint = Float.floatToIntBits((Float) key);
					rowlist = (List<Integer>) iomap.get(keyint);
				} else if (key instanceof Integer)
				{
					rowlist = (List<Integer>) iomap.get((Integer) key);
					keyint = (Integer) key;
				} else
				{
					if (tmd.dataTypes[i] == Types.VARCHAR)
					{
						String s = key.toString();
						int len = s.length();
						if (len > tmd.widths[i])
							tmd.widths[i] = len;
					}
					if (!oimap.containsKey(key))
						oimap.put(key, oimap.size());
					keyint = oimap.get(key);
					rowlist = (List<Integer>) iomap.get(keyint);
				}
				if (rowlist == null)
				{
					rowlist = new ArrayList<Integer>(1);
					rowlist.add(tmd.lineNumber.get());
					iomap.put(keyint, rowlist);
				} else
				{
					rowlist.add(tmd.lineNumber.get());
				}
				rowarr[i] = keyint;
			} else
			{
				if (tmd.dataTypes[i] == Types.INTEGER)
				{
					rowarr[i] = Integer.MIN_VALUE;
				} else if (tmd.dataTypes[i] == Types.DOUBLE)
				{
					rowarr[i] = FLOATNULL;
				} else if (tmd.dataTypes[i] == Types.FLOAT)
				{
					rowarr[i] = FLOATNULL;
				} else if (tmd.dataTypes[i] == Types.DECIMAL)
				{
					rowarr[i] = FLOATNULL;
				} else
					rowarr[i] = -1;
			}
		}
		tmd.lineNumber.incrementAndGet();
		rows.add(rowarr);
		changed = true;
	}

	@SuppressWarnings("rawtypes")
	public RowSet getRowList(int columnNumber, Object key)
	{
		TObjectIntMap oimap = colValueMap.get(columnNumber);
		if (oimap != null)
		{
			if (oimap.containsKey(key))
			{
				TIntObjectMap iomap = columns.get(columnNumber);
				if (iomap != null)
				{
					int[] curlist = (int[]) iomap.get(oimap.get(key));
					TIntArrayList newlist = new TIntArrayList(curlist.length);
					for (int i = 0; i < curlist.length; i++)
					{
						newlist.add(curlist[i]);
					}
					return new RowSet(newlist);
				}
				return null;
			}
		}
		return null;
	}

	private Object getRowValue(int columnNumber, int surrogateKey)
	{
		return values[columnNumber][surrogateKey];
	}

	public boolean isReferenceColumn(int columnNumber)
	{
		return colValueMap.get(columnNumber) != null;
	}

	@Override
	public Object getTableValue(int rowNumber, int columnNumber)
	{
		return getTableValueForJoin(rowNumber, columnNumber);
	}

	@Override
	public Object getTableValueForJoin(int rowNumber, int columnNumber)
	{
		if (tmd.dataTypes[columnNumber] == Types.INTEGER || tmd.dataTypes[columnNumber] == Types.FLOAT || tmd.dataTypes[columnNumber] == Types.DOUBLE
				|| tmd.dataTypes[columnNumber] == Types.DECIMAL)
		{
			Float f = Float.intBitsToFloat(rows.get(rowNumber)[columnNumber]);
			if (Float.isNaN(f))
				return null;
			return f;
		}
		int key = rows.get(rowNumber)[columnNumber];
		if (key >= 0)
		{
			return getRowValue(columnNumber, key);
		}
		return null;
	}

	@Override
	public void shrinkCache()
	{
	}

	@Override
	public void deleteRows(RowSet rs) throws SQLException
	{
		throw new SQLException("Delete is unsupported for basic tables");
	}

	@Override
	public void updateRow(int rowNum, int colNum, Object newValue) throws SQLException
	{
		throw new SQLException("Update is unsupported for basic tables");
	}

	@Override
	public boolean isNull(int dataType, Object value)
	{
		return value == null;
	}

	@Override
	public void clearCache()
	{
	}

	@Override
	public void release()
	{
	}

	@Override
	public void reserve() throws IOException, ClassNotFoundException, InterruptedException
	{
	}

	/*
	 * @Override public boolean hasRowKeySet(int columnNumber) { return false; }
	 * 
	 * @Override public TIntSet getRowKeys(int columnNumber) { return null; }
	 * 
	 * @Override public TLongSet getLongRowKeys(int columnNumber) { return null; }
	 */
	@Override
	public int addColumn() throws SQLException
	{
		throw new SQLException("Add column not supported");
	}

	@Override
	public int dropColumn(int position) throws SQLException
	{
		throw new SQLException("Drop column not supported");
	}

	@Override
	public void updateColumnData(int position, TimeZone tz) throws SQLException
	{
		throw new SQLException("Change column not supported");
	}

	@Override
	public RowSet getRowList(int columnNumber, Object key, RowSet baseSet)
	{
		RowSet result = getRowList(columnNumber, key);
		if (baseSet == null)
			return result;
		return result.getIntersection(baseSet, getNumRows());
	}

	@Override
	public RowSet getRowListForJoin(int columnNumber, Object key, RowSet baseSet)
	{
		RowSet result = getRowList(columnNumber, key);
		if (baseSet == null)
			return result;
		return result.getIntersection(baseSet, getNumRows());
	}

	@Override
	public Iterator<Object> getKeyIterator(int columnNumber)
	{
		return null;
	}

	@Override
	public int getKeyIteratorSize(int columnNumber)
	{
		return -1;
	}

	@Override
	public void indexRange(RowRange range)
	{
	}

	@Override
	public Object getSurrogateKeyValue(int surrogateKey, int columnNumber)
	{
		return null;
	}

	@Override
	public int getObjectSurrogateKey(Object value, int columnNumber, boolean caseSensitive, boolean createIfNotPresent)
	{
		return 0;
	}

	@Override
	public SurrogateKeyStore getKeyStore()
	{
		return null;
	}

	@Override
	public void updateColumnMetadata(int columnNumber)
	{
	}

	@Override
	public long getColumnSize(int columnNumber)
	{
		return 0;
	}
}
