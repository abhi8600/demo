package memdb.storage;

import java.io.IOException;

import memdb.Database;

public abstract class SurrogateKeyStore
{
	public abstract String getName();

	public abstract void initialize(Database database, String storeName, boolean diskBased, boolean compressed) throws ClassNotFoundException, IOException;
}
