package memdb.storage;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.util.Util;

public class TableMetadata implements Externalizable
{
	private static final long serialVersionUID = 1L;
	private static Map<Integer, String> dataTypeStringMap = new HashMap<Integer, String>();
	static
	{
		dataTypeStringMap.put(Types.VARCHAR, "VARCHAR");
		dataTypeStringMap.put(Types.INTEGER, "INTEGER");
		dataTypeStringMap.put(Types.DOUBLE, "FLOAT");
		dataTypeStringMap.put(Types.TIMESTAMP, "DATETIME");
		dataTypeStringMap.put(Types.DATE, "DATE");
		dataTypeStringMap.put(Types.BIGINT, "BIGINT");
		dataTypeStringMap.put(Types.DECIMAL, "DECIMAL");
	}
	public int version;
	public String tableName;
	public String[] columnNames;
	public int[] dataTypes;
	public int[] widths;
	public boolean[] primarykey;
	public boolean[] noindex;
	public boolean[] transientcolumns;
	public boolean[] foreignkey;
	public boolean[] diskBased;
	public boolean[] compressed;
	public AtomicInteger lineNumber = new AtomicInteger(0);
	public AtomicInteger primaryKeyIndex = new AtomicInteger(0);
	public boolean transientTable;
	public String surrogateKeyStoreName;
	public boolean diskBasedKeyStore;
	public boolean compressedKeyStore;
	public boolean defaultDisk;
	public boolean defaultCompressed;

	public TableMetadata()
	{
	}

	public TableMetadata(int columnCount)
	{
		columnNames = new String[columnCount];
		dataTypes = new int[columnCount];
		widths = new int[columnCount];
		primarykey = new boolean[columnCount];
		noindex = new boolean[columnCount];
		foreignkey = new boolean[columnCount];
		diskBased = new boolean[columnCount];
		compressed = new boolean[columnCount];
		transientcolumns = new boolean[columnCount];
	}

	public int getColumnIndex(String name)
	{
		int foundIndex = -1;
		for (int j = 0; j < columnNames.length; j++)
		{
			if (columnNames[j].equals(name))
			{
				return j;
			}
		}
		return foundIndex;
	}

	public int getNumColumns()
	{
		return dataTypes.length;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(version);
		out.writeObject(tableName);
		out.writeObject(columnNames);
		out.writeObject(dataTypes);
		out.writeObject(widths);
		out.writeObject(primarykey);
		out.writeObject(foreignkey);
		out.writeObject(noindex);
		out.writeObject(transientcolumns);
		out.writeObject(diskBased);
		out.writeObject(compressed);
		out.writeObject(lineNumber);
		out.writeObject(primaryKeyIndex);
		out.writeObject(transientTable);
		out.writeObject(surrogateKeyStoreName);
		out.writeBoolean(diskBasedKeyStore);
		out.writeBoolean(compressedKeyStore);
		out.writeBoolean(defaultDisk);
		out.writeBoolean(defaultCompressed);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		version = (Integer) in.readObject();
		tableName = (String) in.readObject();
		columnNames = (String[]) in.readObject();
		dataTypes = (int[]) in.readObject();
		widths = (int[]) in.readObject();
		primarykey = (boolean[]) in.readObject();
		foreignkey = (boolean[]) in.readObject();
		noindex = (boolean[]) in.readObject();
		transientcolumns = (boolean[]) in.readObject();
		diskBased = (boolean[]) in.readObject();
		compressed = (boolean[]) in.readObject();
		lineNumber = (AtomicInteger) in.readObject();
		primaryKeyIndex = (AtomicInteger) in.readObject();
		transientTable = (Boolean) in.readObject();
		surrogateKeyStoreName = (String) in.readObject();
		diskBasedKeyStore = in.readBoolean();
		compressedKeyStore = in.readBoolean();
		defaultDisk = in.readBoolean();
		defaultCompressed = in.readBoolean();
	}

	/**
	 * Add a blank column to the end of the table metadata
	 * 
	 * @return
	 */
	public int addColumn()
	{
		int columnCount = columnNames.length + 1;
		String[] newColumnNames = new String[columnCount];
		int[] newDataTypes = new int[columnCount];
		int[] newWidths = new int[columnCount];
		boolean[] newPrimarykey = new boolean[columnCount];
		boolean[] newNoindex = new boolean[columnCount];
		boolean[] newForeignkey = new boolean[columnCount];
		boolean[] newTransientcolumns = new boolean[columnCount];
		boolean[] newDiskBased = new boolean[columnCount];
		boolean[] newCompressed = new boolean[columnCount];
		for (int i = 0; i < columnNames.length; i++)
		{
			newColumnNames[i] = columnNames[i];
			newDataTypes[i] = dataTypes[i];
			newWidths[i] = widths[i];
			newPrimarykey[i] = primarykey[i];
			newNoindex[i] = noindex[i];
			newForeignkey[i] = foreignkey[i];
			newTransientcolumns[i] = transientcolumns[i];
			newDiskBased[i] = diskBased[i];
			newCompressed[i] = compressed[i];
		}
		columnNames = newColumnNames;
		dataTypes = newDataTypes;
		widths = newWidths;
		primarykey = newPrimarykey;
		noindex = newNoindex;
		foreignkey = newForeignkey;
		transientcolumns = newTransientcolumns;
		diskBased = newDiskBased;
		compressed = newCompressed;
		return columnCount;
	}

	/**
	 * Drop a column from the metadata
	 * 
	 * @return
	 * @throws SQLException
	 */
	public int dropColumn(int position)
	{
		if (position < 0)
			return columnNames.length;
		int columnCount = columnNames.length - 1;
		String[] newColumnNames = new String[columnCount];
		int[] newDataTypes = new int[columnCount];
		int[] newWidths = new int[columnCount];
		boolean[] newPrimarykey = new boolean[columnCount];
		boolean[] newNoindex = new boolean[columnCount];
		boolean[] newForeignkey = new boolean[columnCount];
		boolean[] newTransientcolumns = new boolean[columnCount];
		boolean[] newDiskBased = new boolean[columnCount];
		boolean[] newCompressed = new boolean[columnCount];
		int count = 0;
		for (int i = 0; i < columnNames.length; i++)
		{
			if (i == position)
				continue;
			newColumnNames[count] = columnNames[i];
			newDataTypes[count] = dataTypes[i];
			newWidths[count] = widths[i];
			newPrimarykey[count] = primarykey[i];
			newNoindex[count] = noindex[i];
			newForeignkey[count] = foreignkey[i];
			newTransientcolumns[count++] = transientcolumns[i];
			newDiskBased[count++] = diskBased[i];
			newCompressed[count++] = compressed[i];
		}
		columnNames = newColumnNames;
		dataTypes = newDataTypes;
		widths = newWidths;
		primarykey = newPrimarykey;
		noindex = newNoindex;
		foreignkey = newForeignkey;
		transientcolumns = newTransientcolumns;
		diskBased = newDiskBased;
		compressed = newCompressed;
		return columnNames.length;
	}

	public String getCreateDDL(String tableSchema, String tname)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ");
		if ((tableSchema != null) && (tableSchema.length() > 0))
		{
			sb.append(tableSchema);
			sb.append(".");
		}
		sb.append(tname);
		sb.append(Util.LINE_SEPARATOR);
		sb.append(" (");
		sb.append(Util.LINE_SEPARATOR);
		for (int i = 0; i < columnNames.length; i++)
		{
			if (i > 0)
			{
				sb.append(",");
				sb.append(Util.LINE_SEPARATOR);
			}
			sb.append(" ");
			sb.append(columnNames[i]);
			sb.append(" ");
			sb.append(getDataTypeString(dataTypes[i]));
			if (dataTypes[i] == Types.VARCHAR && widths[i] > 0)
			{
				sb.append("(");
				sb.append(widths[i]);
				sb.append(")");
			}
			if (primarykey[i])
			{
				sb.append(" PRIMARY KEY");
			}
			if (noindex[i])
			{
				sb.append(" NOINDEX");
			}
			if (foreignkey[i])
			{
				sb.append(" FOREIGN KEY");
			}
			if (transientcolumns[i])
			{
				sb.append(" TRANSIENT");
			}
			if (diskBased[i])
			{
				sb.append(" DISK");
			}
			if (compressed[i])
			{
				sb.append(" COMPRESSED");
			}
		}
		sb.append(Util.LINE_SEPARATOR);
		sb.append(" )");
		if (this.transientTable)
		{
			sb.append(Util.LINE_SEPARATOR);
			sb.append(" TRANSIENT");
		}
		if (this.defaultDisk)
		{
			sb.append(Util.LINE_SEPARATOR);
			sb.append(" DISK");
		}
		if (this.defaultCompressed)
		{
			sb.append(Util.LINE_SEPARATOR);
			sb.append(" COMPRESSED");
		}
		if (this.diskBasedKeyStore)
		{
			sb.append(Util.LINE_SEPARATOR);
			sb.append(" DISKKEYSTORE");
		}
		if (this.compressedKeyStore)
		{
			sb.append(Util.LINE_SEPARATOR);
			sb.append(" COMPRESSEDKEYSTORE");
		}
		return sb.toString();
	}

	public static String getDataTypeString(int type)
	{
		return dataTypeStringMap.get(type);
	}
}
