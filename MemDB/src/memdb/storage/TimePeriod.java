/**
 * 
 */
package memdb.storage;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author bpeters
 * 
 */
public class TimePeriod implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int AGG_TYPE_NONE = 0;
	public static final int AGG_TYPE_TRAILING = 1;
	public static final int AGG_TYPE_TO_DATE = 2;
	public static final int PERIOD_DAY = 0;
	public static final int PERIOD_WEEK = 1;
	public static final int PERIOD_MONTH = 2;
	public static final int PERIOD_QUARTER = 3;
	public static final int PERIOD_HALF_YEAR = 4;
	public static final int PERIOD_YEAR = 5;
	public int AggregationType;
	public int AggregationPeriod;
	public int AggregationNumPeriods;
	public int ShiftAmount;
	public int ShiftPeriod;
	public boolean ShiftAgo = true;
	public String Prefix;

	public TimePeriod(int AggregationType, int AggregationPeriod, int AggregationNumPeriods, int ShiftAmount, int ShiftPeriod, boolean ShiftAgo)
	{
		this.AggregationType = AggregationType;
		this.AggregationPeriod = AggregationPeriod;
		this.AggregationNumPeriods = AggregationNumPeriods;
		this.ShiftAmount = ShiftAmount;
		this.ShiftPeriod = ShiftPeriod;
		this.ShiftAgo = ShiftAgo;
		updatePrefix();
	}

	public void updatePrefix()
	{
		Prefix = "";
		if (ShiftAmount != 0)
		{
			if (ShiftAmount != 1)
				Prefix += ShiftAmount;
			if (ShiftAgo)
			{
				if (ShiftPeriod == PERIOD_WEEK)
					Prefix += "WAGO ";
				else if (ShiftPeriod == PERIOD_MONTH)
					Prefix += "MAGO ";
				else if (ShiftPeriod == PERIOD_QUARTER)
					Prefix += "QAGO ";
				else if (ShiftPeriod == PERIOD_HALF_YEAR)
					Prefix += "HYAGO ";
				else if (ShiftPeriod == PERIOD_YEAR)
					Prefix += "YAGO ";
			} else
			{
				if (ShiftPeriod == PERIOD_WEEK)
					Prefix += "NW ";
				else if (ShiftPeriod == PERIOD_MONTH)
					Prefix += "NM ";
				else if (ShiftPeriod == PERIOD_QUARTER)
					Prefix += "NQ ";
				else if (ShiftPeriod == PERIOD_HALF_YEAR)
					Prefix += "NHY ";
				else if (ShiftPeriod == PERIOD_YEAR)
					Prefix += "NY ";
			}
		}
		if (AggregationType == AGG_TYPE_TRAILING)
		{
			Prefix += "T";
			if (AggregationNumPeriods == 12)
				Prefix += "T";
			else
				Prefix += AggregationNumPeriods;
			if (AggregationPeriod == PERIOD_WEEK)
				Prefix += "W";
			else if (AggregationPeriod == PERIOD_MONTH)
				Prefix += "M";
			else if (AggregationPeriod == PERIOD_QUARTER)
				Prefix += "Q";
			else if (AggregationPeriod == PERIOD_HALF_YEAR)
				Prefix += "HY";
			else if (AggregationPeriod == PERIOD_YEAR)
				Prefix += "Y";
		} else if (AggregationType == AGG_TYPE_TO_DATE)
		{
			if (AggregationPeriod == PERIOD_WEEK)
				Prefix += "WTD";
			else if (AggregationPeriod == PERIOD_MONTH)
				Prefix += "MTD";
			else if (AggregationPeriod == PERIOD_QUARTER)
				Prefix += "QTD";
			else if (AggregationPeriod == PERIOD_HALF_YEAR)
				Prefix += "HYTD";
			else if (AggregationPeriod == PERIOD_YEAR)
				Prefix += "YTD";
		}
		Prefix = Prefix.trim();
	}

	public int getCalendarShiftPeriod()
	{
		return getCalendarPeriod(ShiftPeriod);
	}

	public int getCalendarAggregationPeriod()
	{
		return getCalendarPeriod(AggregationPeriod);
	}

	private int getCalendarPeriod(int p)
	{
		if (p == PERIOD_DAY)
			return Calendar.DAY_OF_MONTH;
		else if (p == PERIOD_WEEK)
			return Calendar.WEEK_OF_YEAR;
		else if (p == PERIOD_MONTH)
			return Calendar.MONTH;
		else if (p == PERIOD_QUARTER)
			return Calendar.MONTH;
		else if (p == PERIOD_HALF_YEAR)
			return Calendar.MONTH;
		else if (p == PERIOD_YEAR)
			return Calendar.YEAR;
		return Calendar.DAY_OF_MONTH;
	}

	public int getCalendarShiftAmount()
	{
		return getCalendarAmount(ShiftPeriod, ShiftAmount, ShiftAgo);
	}

	public int getCalendarAggregationAmount()
	{
		return getCalendarAmount(AggregationPeriod, AggregationNumPeriods, true);
	}

	private int getCalendarAmount(int p, int amount, boolean ago)
	{
		if (p == PERIOD_DAY)
			return (ago ? 1 : -1) * amount;
		else if (p == PERIOD_WEEK)
			return (ago ? 1 : -1) * amount;
		else if (p == PERIOD_MONTH)
			return (ago ? 1 : -1) * amount;
		else if (p == PERIOD_QUARTER)
			return (ago ? 1 : -1) * amount * 3;
		else if (p == PERIOD_HALF_YEAR)
			return (ago ? 1 : -1) * amount * 6;
		else if (p == PERIOD_YEAR)
			return (ago ? 1 : -1) * amount;
		return 0;
	}

	public void setToDateStartAndEnd(Calendar startCal, Calendar endCal, boolean leftSideOfJoin)
	{
		if (leftSideOfJoin)
		{
			/*
			 * Current calender is the end point, so find the beginning of the period
			 */
			if (AggregationPeriod == PERIOD_DAY)
				return;
			else if (AggregationPeriod == PERIOD_WEEK)
				startCal.set(Calendar.DAY_OF_WEEK, startCal.getFirstDayOfWeek());
			else if (AggregationPeriod == PERIOD_MONTH)
				startCal.set(Calendar.DAY_OF_MONTH, 1);
			else if (AggregationPeriod == PERIOD_QUARTER)
			{
				int curmonth = startCal.get(Calendar.MONTH);
				curmonth = ((int) (curmonth / 3)) * 3;
				startCal.set(Calendar.MONTH, curmonth);
				startCal.set(Calendar.DAY_OF_MONTH, 1);
			} else if (AggregationPeriod == PERIOD_HALF_YEAR)
			{
				int curmonth = startCal.get(Calendar.MONTH);
				curmonth = ((int) (curmonth / 6)) * 6;
				startCal.set(Calendar.MONTH, curmonth);
				startCal.set(Calendar.DAY_OF_MONTH, 1);
			} else if (AggregationPeriod == PERIOD_YEAR)
			{
				startCal.set(Calendar.MONTH, 0);
				startCal.set(Calendar.DAY_OF_MONTH, 1);
			}
		} else
		{
			/*
			 * Current calendar is the beginning point so find the end of the period
			 */
			if (AggregationPeriod == PERIOD_DAY)
				return;
			else if (AggregationPeriod == PERIOD_WEEK)
			{
				endCal.set(Calendar.DAY_OF_WEEK, endCal.getFirstDayOfWeek() - 1);
			} else if (AggregationPeriod == PERIOD_MONTH)
				endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			else if (AggregationPeriod == PERIOD_QUARTER)
			{
				int curmonth = endCal.get(Calendar.MONTH);
				curmonth = ((int) (curmonth / 3)) * 3;
				endCal.set(Calendar.MONTH, curmonth + 2);
				endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			} else if (AggregationPeriod == PERIOD_HALF_YEAR)
			{
				int curmonth = endCal.get(Calendar.MONTH);
				curmonth = ((int) (curmonth / 6)) * 6;
				endCal.set(Calendar.MONTH, curmonth + 5);
				endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			} else if (AggregationPeriod == PERIOD_YEAR)
			{
				endCal.set(Calendar.MONTH, 11);
				endCal.set(Calendar.DAY_OF_MONTH, 31);
			}
		}
	}
}
