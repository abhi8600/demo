package memdb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.storage.BasicTable;
import memdb.storage.CalculatedTime;
import memdb.storage.CalculatedTime.TimeLevel;
import memdb.storage.SurrogateKeyStore;
import memdb.storage.Table;
import memdb.storage.TableMetadata;
import memdb.storage.TimePeriod;

import org.apache.log4j.Logger;

public class Database implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int NUM_TABLE_LOAD_THREADS = 2;
	private static Logger logger = Logger.getLogger(Database.class);
	private static String DB_PROPERTIES_FILE_NAME = "db.properties";
	public static int CURRENT_VERSION = 1;
	public String path;
	public Table[] tables = new Table[0];
	public boolean caseSensitive = false;
	public TimeLevel[] timeLevels;
	public TimePeriod[] timePeriods;
	public boolean noTransientTables = false;
	public Map<String, SurrogateKeyStore> keyStores = new HashMap<String, SurrogateKeyStore>();
	/*
	 * Accuracy in ms of datetime values (default is 1000ms, or 1 sec). Lower precision yields higher compression
	 */
	public int dateTimePrecision = 1000;

	public Database(String path)
	{
		this.path = path;
		if (path != null)
		{
			try
			{
				getDatabaseProperties();
			} catch (IOException e)
			{
				logger.error(e.getMessage());
			}
		}
		if (path != null)
			createCalculatedTables();
	}

	public SurrogateKeyStore getKeyStore(boolean advanced, String keyStoreName, boolean diskBased, boolean compressed)
	{
		synchronized (keyStores)
		{
			SurrogateKeyStore sks = keyStores.get(keyStoreName);
			if (sks != null)
				return sks;
			if (advanced)
			{
				try
				{
					@SuppressWarnings("rawtypes")
					Class c = Class.forName("memdb.storage.AdvancedSurrogateKeyStore");
					if (c != null)
					{
						try
						{
							sks = (SurrogateKeyStore) c.newInstance();
							sks.initialize(this, keyStoreName, diskBased, compressed);
							keyStores.put(keyStoreName, sks);
							return sks;
						} catch (InstantiationException e)
						{
							logger.error(e);
						} catch (IllegalAccessException e)
						{
							logger.error(e);
						} catch (IOException e)
						{
							logger.error(e);
						}
					}
				} catch (ClassNotFoundException ex)
				{
				}
			}
		}
		return null;
	}

	private void createCalculatedTables()
	{
		/*
		 * Add time tables
		 */
		for (TimeLevel tl : timeLevels)
		{
			Table t = new CalculatedTime(this, new Locale("en"), "dbo", "DW_DM_TIME_" + CalculatedTime.getTimeLevelTableSuffix(tl, false), null, tl);
			t.database = this;
			addTable(t);
			for (TimePeriod tp : timePeriods)
			{
				t = new CalculatedTime(this, new Locale("en"), "dbo", "DW_DM_" + tp.Prefix + "_" + CalculatedTime.getTimeLevelTableSuffix(tl, true), tp, tl);
				t.database = this;
				addTable(t);
			}
		}
	}

	private void getDatabaseProperties() throws IOException
	{
		File propfile = new File(path + File.separator + "memdb.ini");
		Properties prop = new Properties();
		if (propfile.exists())
		{
			prop.load(new FileInputStream(propfile));
			String val = prop.getProperty("case-sensitive");
			if (val != null && val.toUpperCase().equals("TRUE"))
				this.caseSensitive = true;
			val = prop.getProperty("time-levels");
			if (val != null)
			{
				String[] levels = val.split(",");
				List<TimeLevel> tlevels = new ArrayList<TimeLevel>();
				for (String s : levels)
				{
					if (s.toLowerCase().equals("day"))
						tlevels.add(TimeLevel.Day);
					else if (s.toLowerCase().equals("week"))
						tlevels.add(TimeLevel.Week);
					else if (s.toLowerCase().equals("month"))
						tlevels.add(TimeLevel.Month);
					else if (s.toLowerCase().equals("quarter"))
						tlevels.add(TimeLevel.Quarter);
					else if (s.toLowerCase().equals("halfyear"))
						tlevels.add(TimeLevel.HalfYear);
				}
				timeLevels = new TimeLevel[tlevels.size()];
				tlevels.toArray(timeLevels);
			}
			val = prop.getProperty("time-periods");
			if (val != null)
			{
				String[] periodValues = val.split(",");
				List<TimePeriod> periods = new ArrayList<TimePeriod>();
				for (String s : periodValues)
				{
					String[] tpparts = s.split(":");
					int aggType = TimePeriod.AGG_TYPE_NONE;
					int aggPeriod = TimePeriod.PERIOD_MONTH;
					int aggNum = 0;
					int shiftAmount = 0;
					int shiftPeriod = TimePeriod.PERIOD_MONTH;
					boolean shiftAgo = true;
					for (String part : tpparts)
					{
						if (part.toLowerCase().equals("ttm"))
						{
							aggType = TimePeriod.AGG_TYPE_TRAILING;
							aggPeriod = TimePeriod.PERIOD_MONTH;
							aggNum = 12;
						} else if (s.toLowerCase().equals("t3m"))
						{
							aggType = TimePeriod.AGG_TYPE_TRAILING;
							aggPeriod = TimePeriod.PERIOD_MONTH;
							aggNum = 3;
						} else if (s.toLowerCase().equals("mago"))
						{
							shiftAmount = 1;
							shiftPeriod = TimePeriod.PERIOD_MONTH;
							shiftAgo = true;
						} else if (s.toLowerCase().equals("qago"))
						{
							shiftAmount = 1;
							shiftPeriod = TimePeriod.PERIOD_QUARTER;
							shiftAgo = true;
						} else if (s.toLowerCase().equals("yago"))
						{
							shiftAmount = 1;
							shiftPeriod = TimePeriod.PERIOD_YEAR;
							shiftAgo = true;
						} else if (s.toLowerCase().equals("mtd"))
						{
							aggType = TimePeriod.AGG_TYPE_TO_DATE;
							aggPeriod = TimePeriod.PERIOD_MONTH;
							aggNum = 1;
						} else if (s.toLowerCase().equals("qtd"))
						{
							aggType = TimePeriod.AGG_TYPE_TO_DATE;
							aggPeriod = TimePeriod.PERIOD_QUARTER;
							aggNum = 1;
						} else if (s.toLowerCase().equals("ytd"))
						{
							aggType = TimePeriod.AGG_TYPE_TO_DATE;
							aggPeriod = TimePeriod.PERIOD_YEAR;
							aggNum = 1;
						}
					}
					periods.add(new TimePeriod(aggType, aggPeriod, aggNum, shiftAmount, shiftPeriod, shiftAgo));
				}
				timePeriods = new TimePeriod[periods.size()];
				periods.toArray(timePeriods);
			} else
				timePeriods = new TimePeriod[0];
			val = prop.getProperty("time-zone");
			if (val != null)
				TimeZone.setDefault(TimeZone.getTimeZone(val));
			else
				TimeZone.setDefault(TimeZone.getTimeZone("GMT-0:00"));
			val = prop.getProperty("no-transient-tables");
			if (val != null)
				noTransientTables = true;
			val = prop.getProperty("date-time-ms-precision");
			if (val != null)
			{
				try
				{
					dateTimePrecision = Integer.parseInt(val);
				} catch (Exception e)
				{
				}
			}
		} else
		{
			PrintWriter writer = new PrintWriter(new FileWriter(propfile));
			writer.println("case-sensitive=true");
			writer.println("time-levels=day,week,month,quarter");
			timeLevels = new TimeLevel[]
			{ TimeLevel.Day, TimeLevel.Week, TimeLevel.Month, TimeLevel.Quarter };
			writer.println("time-periods=mago,ttm,mago:ttm");
			writer.println("date-time-ms-precision=1000");
			timePeriods = new TimePeriod[]
			{ new TimePeriod(TimePeriod.AGG_TYPE_NONE, TimePeriod.PERIOD_DAY, 0, 1, TimePeriod.PERIOD_MONTH, true),
					new TimePeriod(TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 12, 0, TimePeriod.PERIOD_MONTH, false),
					new TimePeriod(TimePeriod.AGG_TYPE_TRAILING, TimePeriod.PERIOD_MONTH, 12, 1, TimePeriod.PERIOD_MONTH, true) };
			writer.close();
		}
	}

	public void deleteTable(Table t)
	{
		int foundindex = -1;
		for (int i = 0; i < tables.length; i++)
		{
			if (tables[i] == t)
			{
				foundindex = i;
				break;
			}
		}
		if (foundindex >= 0)
		{
			Table[] newtables = new Table[tables.length - 1];
			int count = 0;
			for (int i = 0; i < tables.length; i++)
			{
				if (i != foundindex)
					newtables[count++] = tables[i];
			}
			tables = newtables;
		}
		t.drop();
		if (!t.schema.equals("INFORMATION_SCHEMA"))
			createInformationSchemaTables();
	}

	public Table findTable(String schema, String name)
	{
		Table t = null;
		for (Table st : tables)
		{
			if ((schema == null || st.schema.equals(schema)) && st.name.equals(name))
			{
				t = st;
			}
		}
		return t;
	}

	private Table getNewTable() throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		@SuppressWarnings("rawtypes")
		Class c = null;
		try
		{
			c = Class.forName("memdb.storage.AdvancedTable");
		} catch (ClassNotFoundException ex)
		{
			c = Class.forName("memdb.storage.BasicTable");
		}
		return (Table) c.newInstance();
	}

	public Table createTable(String schema, String name, boolean tempTable, String keyStoreName, boolean diskKeyStore, boolean compressedKeyStore)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		Table t = findTable(schema, name);
		if (t != null)
			deleteTable(t);
		t = getNewTable();
		t.database = this;
		t.schema = schema;
		t.name = name;
		t.tempTable = tempTable;
		t.initializeNew(this, keyStoreName, diskKeyStore, compressedKeyStore);
		if (!tempTable)
			addTable(t);
		return t;
	}

	public Database remoteClone() throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		Database newd = new Database(null);
		for (Table t : tables)
		{
			Table newt = new BasicTable();
			newt.name = t.name;
			newt.tmd = t.tmd;
			newt.schema = t.schema;
			newd.addTable(newt);
		}
		return newd;
	}

	private void addTable(Table t)
	{
		Table[] newtables = new Table[tables.length + 1];
		for (int i = 0; i < tables.length; i++)
			newtables[i] = tables[i];
		newtables[tables.length] = t;
		tables = newtables;
		if (t.schema.equals("INFORMATION_SCHEMA"))
			return;
		createInformationSchemaTables();
	}

	public void readExternal() throws IOException, ClassNotFoundException, InterruptedException
	{
		File dbdir = new File(path);
		File[] dbfiles = dbdir.listFiles();
		tables = new Table[0];
		getDatabaseProperties();
		for (File dbf : dbfiles)
		{
			if (dbf.isDirectory())
			{
				File[] tfiles = dbf.listFiles();
				for (File t : tfiles)
				{
					if (t.isFile())
					{
						String name = t.getName();
						if (name.endsWith(".md"))
						{
							try
							{
								createTable(dbf.getName(), name.substring(0, name.length() - 3), false, null, false, false);
							} catch (InstantiationException e)
							{
								logger.error(e);
							} catch (IllegalAccessException e)
							{
								logger.error(e);
							}
						}
					}
				}
			} else if (dbf.isFile() && dbf.getName().equalsIgnoreCase(DB_PROPERTIES_FILE_NAME))
			{
				readDBPropertiesFromExternal(dbf);
			}
		}
		curTableIndex.set(0);
		List<LoadTableThread> tlist = new ArrayList<LoadTableThread>();
		for (int i = 0; i < NUM_TABLE_LOAD_THREADS; i++)
		{
			LoadTableThread t = new LoadTableThread();
			tlist.add(t);
		}
		for (int i = 0; i < NUM_TABLE_LOAD_THREADS; i++)
		{
			tlist.get(i).start();
		}
		for (int i = 0; i < NUM_TABLE_LOAD_THREADS; i++)
		{
			try
			{
				tlist.get(i).join();
				if (tlist.get(i).error != null && tlist.get(i).error instanceof IOException)
				{
					throw (IOException) tlist.get(i).error;
				}
				if (tlist.get(i).error != null && tlist.get(i).error instanceof ClassNotFoundException)
				{
					throw (ClassNotFoundException) tlist.get(i).error;
				}
				if (tlist.get(i).error != null && tlist.get(i).error instanceof InterruptedException)
				{
					throw (InterruptedException) tlist.get(i).error;
				}
			} catch (InterruptedException e)
			{
				logger.error(e);
				throw e;
			}
		}
		// Use less cache if less memory
		long availmemory = Runtime.getRuntime().maxMemory() - (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
		if (availmemory < 10000000000L)
			shrinkCache();
		if (availmemory < 5000000000L)
			shrinkCache();
		if (availmemory < 3000000000L)
			shrinkCache();
		if (availmemory < 2000000000L)
			shrinkCache();
		if (availmemory < 1000000000L)
			shrinkCache();
		createCalculatedTables();
	}

	private void shrinkCache()
	{
		if (tables == null)
			return;
		for (Table t : tables)
		{
			t.shrinkCache();
		}
	}
	AtomicInteger curTableIndex = new AtomicInteger(0);

	private class LoadTableThread extends Thread
	{
		public Exception error;

		public void run()
		{
			while (true)
			{
				int index = curTableIndex.getAndIncrement();
				if (index >= tables.length)
					return;
				Table t = tables[index];
				if (t.schema.equals("INFORMATION_SCHEMA"))
					continue;
				System.out.println("Loading table: " + t.schema + "." + t.name);
				try
				{
					t.readExternal();
				} catch (IOException e)
				{
					logger.error(e);
					error = e;
					return;
				} catch (ClassNotFoundException e)
				{
					logger.error(e);
					error = e;
					return;
				} catch (InterruptedException e)
				{
					logger.error(e);
					error = e;
					return;
				}
			}
		}
	}

	public void writeExternal() throws IOException
	{
		File f = new File(path);
		if (!f.exists())
		{
			f.mkdir();
		}
		writeDBPropertiesToExternal();
	}

	public void flush() throws IOException
	{
		this.writeDBPropertiesToExternal();
		if (tables == null)
			return;
		for (Table t : tables)
		{
			t.flush();
		}
	}

	public void readDBPropertiesFromExternal(File f) throws IOException
	{
		ObjectInputStream ois = null;
		try
		{
			ois = new ObjectInputStream(new FileInputStream(f));
			Boolean serializedCaseSensitive = (Boolean) ois.readObject();
			if (caseSensitive != serializedCaseSensitive)
			{
				logger.warn("Database was serialized with case sensitive setting '" + serializedCaseSensitive + "', as opposed to value '" + caseSensitive
						+ "' in memdb.ini. The setting in memdb.ini will be ignored.");
				caseSensitive = serializedCaseSensitive;
			}
			TimeZone serializedTimezone = (TimeZone) ois.readObject();
			if (!serializedTimezone.equals(TimeZone.getDefault()))
			{
				logger.warn("Database was serialized with timezone setting '" + serializedTimezone.getDisplayName() + "', as opposed to value '"
						+ TimeZone.getDefault().getDisplayName() + "' in memdb.ini. The setting in memdb.ini will be ignored.");
				TimeZone.setDefault(serializedTimezone);
			}
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		} finally
		{
			// Close the ObjectInputStream
			try
			{
				if (ois != null)
				{
					ois.close();
				}
			} catch (IOException ex)
			{
				logger.error(ex.toString(), ex);
			}
		}
	}

	public void writeDBPropertiesToExternal() throws IOException
	{
		File dir = new File(path);
		File dbPropertiesFile = new File(dir, DB_PROPERTIES_FILE_NAME);
		ObjectOutputStream oos = null;
		try
		{
			oos = new ObjectOutputStream(new FileOutputStream(dbPropertiesFile));
			oos.writeObject(caseSensitive);
			oos.writeObject(TimeZone.getDefault());
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		} finally
		{
			// Close the ObjectOutputStream
			try
			{
				if (oos != null)
				{
					oos.close();
				}
			} catch (IOException ex)
			{
				logger.error(ex.toString(), ex);
			}
		}
	}

	public void pack() throws IOException
	{
		if (tables == null)
			return;
		for (Table t : tables)
		{
			t.pack();
		}
	}

	public void checkMemory()
	{
	}

	public void createInformationSchemaTables()
	{
		/*
		 * Add information schema tables
		 */
		Table t = findTable("INFORMATION_SCHEMA", "TABLES");
		if (t != null)
			deleteTable(t);
		t = new BasicTable();
		t.database = this;
		t.tmd = new TableMetadata(4);
		t.initializeNew(this, null, false, false);
		t.schema = "INFORMATION_SCHEMA";
		t.name = "TABLES";
		t.tmd.columnNames = new String[]
		{ "TABLE_CATALOG", "TABLE_SCHEMA", "TABLE_NAME", "TABLE_TYPE" };
		t.tmd.dataTypes = new int[]
		{ Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		t.tmd.widths = new int[]
		{ 100, 100, 100, 100 };
		addTable(t);
		for (Table dbt : tables)
		{
			t.addRow(new Object[]
			{ dbt.schema, dbt.schema, dbt.name, "BASE TABLE" }, TimeZone.getDefault(), null);
		}
		t.pack();
	}

	public long memUsed()
	{
		long result = 0;
		if (tables == null)
			return result;
		for (Table t : tables)
		{
			result += t.getMemoryUsed();
		}
		return result;
	}
}
