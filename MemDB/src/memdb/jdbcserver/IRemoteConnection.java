/**
 * IRemoteConnection - This interface exposes the basic Connection methods remotely
 */
package memdb.jdbcserver;

import java.rmi.*;
import java.sql.*;
import java.util.TimeZone;

public interface IRemoteConnection extends Remote
{
	public IRemoteStatement createStatement(TimeZone clientTimeZone) throws RemoteException, SQLException;

	public IRemoteStatement prepareStatement(String sql, TimeZone clientTimeZone) throws RemoteException, SQLException;

	public DBMetaData getMetaData() throws RemoteException, SQLException;

	public TimeZone getServerTimeZone() throws RemoteException;

	void closeConnection() throws RemoteException, SQLException;
}