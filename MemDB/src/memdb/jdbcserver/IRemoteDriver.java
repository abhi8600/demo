/**
 * IRemoteDriver - This interface exposes the basic Driver method remotly. 
 */
package memdb.jdbcserver;

import java.rmi.*;
import java.sql.*;

public interface IRemoteDriver extends Remote
{
	IRemoteConnection getConnection(String username, String password) throws RemoteException, SQLException;
}