/**
 * RemoteConnectionImpl - This class performs the role of main RMI server in Type-III
 * Driver. The client driver connects to the object of this class via RMI and then gets 
 * the Connection. 
 */
package memdb.jdbcserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;

import memdb.Database;


public class RemoteDriverImpl extends UnicastRemoteObject implements IRemoteDriver
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Database db;

	public RemoteDriverImpl(Database db) throws RemoteException
	{
		super();
		this.db = db;
	}

	public IRemoteConnection getConnection(String username, String password) throws RemoteException, SQLException
	{
		RemoteConnectionImpl ConnectionInstance = new RemoteConnectionImpl(db);
		return (IRemoteConnection) ConnectionInstance;
	}
}