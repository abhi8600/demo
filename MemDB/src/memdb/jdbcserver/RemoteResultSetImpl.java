/**
 * RemoteResultSetImpl - This class contains the actual JDBC-ODBC ResultSet. 
 * It acts as a remote wrapper over the JDBC-ODBC ResultSet.
 */
package memdb.jdbcserver;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.RowSetMetaDataImpl;

import memdb.sql.QueryResultRow;
import memdb.sql.StreamingResultSet;
import memdb.storage.TableMetadata;

import org.apache.log4j.Logger;

public class RemoteResultSetImpl extends UnicastRemoteObject implements IRemoteResultSet, ResultSet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(RemoteResultSetImpl.class);
	private List<Object[]> data;
	private StreamingResultSet resultRows;
	private TableMetadata tmd;
	private int curRow = -1;
	private Map<String, Integer> colMap = null;
	private boolean released = false;

	/**
	 * Constructor for creating RemoteResultSetImpl with JDBC-ODBC ResultSet
	 */
	public void setResult(StreamingResultSet resultRows)
	{
		this.resultRows = resultRows;
	}

	public RemoteResultSetImpl() throws RemoteException
	{
		this.data = new ArrayList<Object[]>();
		this.tmd = new TableMetadata(1);
		tmd.columnNames = new String[]
		{ "Col0" };
		tmd.dataTypes = new int[]
		{ Types.INTEGER };
	}

	public RemoteResultSetImpl(List<Object[]> data, TableMetadata tmd) throws RemoteException, SQLException
	{
		super();
		this.data = data;
		this.tmd = tmd;
		colMap = new HashMap<String, Integer>();
		for (int i = 0; i < tmd.columnNames.length; i++)
			colMap.put(tmd.columnNames[i], i + 1);
	}

	public Object[] getNextRow() throws RemoteException, SQLException
	{
		if (data == null && resultRows == null)
			return null;
		if (data != null && ++curRow >= data.size())
			return null;
		Object[] dobj = null;
		if (resultRows != null)
		{
			QueryResultRow qrr = resultRows.next();
			if (qrr == null)
				return null;
			dobj = qrr.dataRow;
		} else
			dobj = (Object[]) data.get(curRow);
		Object[] result = new Object[dobj.length - 1];
		for (int i = 0; i < result.length; i++)
			result[i] = dobj[i];
		return result;
	}

	public QueryResultRow[] getResults(boolean getAllResults)
	{
		int size = 0;
		if (resultRows == null)
		{
			if (data == null)
				return null;
			size = data.size();
			if (++curRow >= size)
				return null;
			int start = curRow;
			int stop = curRow + 10000;
			if (getAllResults)
				stop = size - 1;
			if (stop >= size)
				stop = size - 1;
			curRow = stop;
			QueryResultRow[] rrows = new QueryResultRow[stop - start + 1];
			for (int i = start; i <= stop; i++)
			{
				rrows[i - start] = new QueryResultRow();
				rrows[i - start].dataRow = data.get(i);
			}
			return rrows;
		} else
		{
			List<QueryResultRow> rrows = new ArrayList<QueryResultRow>();
			for (int i = 0; i < 10000; i++)
			{
				QueryResultRow qrr = resultRows.next();
				if (qrr == null)
					break;
				rrows.add(qrr);
				curRow++;
			}
			QueryResultRow[] results = new QueryResultRow[rrows.size()];
			rrows.toArray(results);
			return results;
		}
	}

	public void close() throws SQLException
	{
		release();
	}

	private void release()
	{
		if (!released)
		{
			released = true;
		}
	}

	@Override
	public String[] getColumnNames() throws RemoteException, SQLException
	{
		if (tmd != null)
			return tmd.columnNames;
		return null;
	}

	@Override
	public int[] getColumnTypes() throws RemoteException, SQLException
	{
		if (tmd != null)
			return tmd.dataTypes;
		return null;
	}

	public void setData(List<Object[]> data)
	{
		this.data = data;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException
	{
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException
	{
		return null;
	}

	@Override
	public boolean absolute(int row) throws SQLException
	{
		return false;
	}

	@Override
	public void afterLast() throws SQLException
	{
	}

	@Override
	public void beforeFirst() throws SQLException
	{
	}

	@Override
	public void cancelRowUpdates() throws SQLException
	{
	}

	@Override
	public void clearWarnings() throws SQLException
	{
	}

	@Override
	public void deleteRow() throws SQLException
	{
	}

	@Override
	public int findColumn(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public boolean first() throws SQLException
	{
		return false;
	}

	@Override
	public Array getArray(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Array getArray(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getAsciiStream(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getAsciiStream(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException
	{
		return null;
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getBinaryStream(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getBinaryStream(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Blob getBlob(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Blob getBlob(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public boolean getBoolean(int columnIndex) throws SQLException
	{
		return false;
	}

	@Override
	public boolean getBoolean(String columnLabel) throws SQLException
	{
		return false;
	}

	@Override
	public byte getByte(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public byte getByte(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public byte[] getBytes(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public byte[] getBytes(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Reader getCharacterStream(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Reader getCharacterStream(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Clob getClob(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Clob getClob(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public int getConcurrency() throws SQLException
	{
		return 0;
	}

	@Override
	public String getCursorName() throws SQLException
	{
		return null;
	}

	@Override
	public Date getDate(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Date getDate(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Date getDate(int columnIndex, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public Date getDate(String columnLabel, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public double getDouble(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public double getDouble(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public int getFetchDirection() throws SQLException
	{
		return 0;
	}

	@Override
	public int getFetchSize() throws SQLException
	{
		return 0;
	}

	@Override
	public float getFloat(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public float getFloat(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public int getHoldability() throws SQLException
	{
		return 0;
	}

	@Override
	public int getInt(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public int getInt(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public long getLong(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public long getLong(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException
	{
		RowSetMetaDataImpl rsmd = new RowSetMetaDataImpl();
		rsmd.setColumnCount(tmd.columnNames.length);
		for (int i = 0; i < tmd.columnNames.length; i++)
		{
			rsmd.setColumnName(i + 1, tmd.columnNames[i]);
			rsmd.setColumnType(i + 1, tmd.dataTypes[i]);
			rsmd.setColumnDisplaySize(i + 1, tmd.widths[i]);
		}
		return rsmd;
	}

	@Override
	public Reader getNCharacterStream(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Reader getNCharacterStream(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public NClob getNClob(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public String getNString(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public String getNString(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Object getObject(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Object getObject(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException
	{
		return null;
	}

	@Override
	public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException
	{
		return null;
	}

	@Override
	public Ref getRef(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Ref getRef(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public int getRow() throws SQLException
	{
		return 0;
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public short getShort(int columnIndex) throws SQLException
	{
		return 0;
	}

	@Override
	public short getShort(String columnLabel) throws SQLException
	{
		return 0;
	}

	@Override
	public Statement getStatement() throws SQLException
	{
		return null;
	}

	@Override
	public String getString(int columnIndex) throws SQLException
	{
		Object columnData = data.get(curRow)[columnIndex - 1];
		if (columnData instanceof String)
			return (String) columnData;
		else if (columnData == null)
		{
			return null;
		} else
		{
			return columnData.toString();
		}
	}

	@Override
	public String getString(String columnLabel) throws SQLException
	{
		int index = colMap.get(columnLabel);
		return getString(index);
	}

	@Override
	public Time getTime(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Time getTime(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Time getTime(int columnIndex, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public Time getTime(String columnLabel, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public Timestamp getTimestamp(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public Timestamp getTimestamp(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException
	{
		return null;
	}

	@Override
	public int getType() throws SQLException
	{
		return 0;
	}

	@Override
	public URL getURL(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public URL getURL(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getUnicodeStream(int columnIndex) throws SQLException
	{
		return null;
	}

	@Override
	public InputStream getUnicodeStream(String columnLabel) throws SQLException
	{
		return null;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException
	{
		return null;
	}

	@Override
	public void insertRow() throws SQLException
	{
	}

	@Override
	public boolean isAfterLast() throws SQLException
	{
		return false;
	}

	@Override
	public boolean isBeforeFirst() throws SQLException
	{
		return false;
	}

	@Override
	public boolean isClosed() throws SQLException
	{
		return false;
	}

	@Override
	public boolean isFirst() throws SQLException
	{
		return false;
	}

	@Override
	public boolean isLast() throws SQLException
	{
		return false;
	}

	@Override
	public boolean last() throws SQLException
	{
		return false;
	}

	@Override
	public void moveToCurrentRow() throws SQLException
	{
	}

	@Override
	public void moveToInsertRow() throws SQLException
	{
	}

	@Override
	public boolean next() throws SQLException
	{
		if (data == null)
			return false;
		if (++curRow >= data.size())
			return false;
		return true;
	}

	@Override
	public boolean previous() throws SQLException
	{
		return false;
	}

	@Override
	public void refreshRow() throws SQLException
	{
	}

	@Override
	public boolean relative(int rows) throws SQLException
	{
		return false;
	}

	@Override
	public boolean rowDeleted() throws SQLException
	{
		return false;
	}

	@Override
	public boolean rowInserted() throws SQLException
	{
		return false;
	}

	@Override
	public boolean rowUpdated() throws SQLException
	{
		return false;
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException
	{
	}

	@Override
	public void setFetchSize(int rows) throws SQLException
	{
	}

	@Override
	public void updateArray(int columnIndex, Array x) throws SQLException
	{
	}

	@Override
	public void updateArray(String columnLabel, Array x) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException
	{
	}

	@Override
	public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void updateBlob(int columnIndex, Blob x) throws SQLException
	{
	}

	@Override
	public void updateBlob(String columnLabel, Blob x) throws SQLException
	{
	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException
	{
	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException
	{
	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException
	{
	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException
	{
	}

	@Override
	public void updateBoolean(int columnIndex, boolean x) throws SQLException
	{
	}

	@Override
	public void updateBoolean(String columnLabel, boolean x) throws SQLException
	{
	}

	@Override
	public void updateByte(int columnIndex, byte x) throws SQLException
	{
	}

	@Override
	public void updateByte(String columnLabel, byte x) throws SQLException
	{
	}

	@Override
	public void updateBytes(int columnIndex, byte[] x) throws SQLException
	{
	}

	@Override
	public void updateBytes(String columnLabel, byte[] x) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException
	{
	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateClob(int columnIndex, Clob x) throws SQLException
	{
	}

	@Override
	public void updateClob(String columnLabel, Clob x) throws SQLException
	{
	}

	@Override
	public void updateClob(int columnIndex, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateClob(String columnLabel, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateDate(int columnIndex, Date x) throws SQLException
	{
	}

	@Override
	public void updateDate(String columnLabel, Date x) throws SQLException
	{
	}

	@Override
	public void updateDouble(int columnIndex, double x) throws SQLException
	{
	}

	@Override
	public void updateDouble(String columnLabel, double x) throws SQLException
	{
	}

	@Override
	public void updateFloat(int columnIndex, float x) throws SQLException
	{
	}

	@Override
	public void updateFloat(String columnLabel, float x) throws SQLException
	{
	}

	@Override
	public void updateInt(int columnIndex, int x) throws SQLException
	{
	}

	@Override
	public void updateInt(String columnLabel, int x) throws SQLException
	{
	}

	@Override
	public void updateLong(int columnIndex, long x) throws SQLException
	{
	}

	@Override
	public void updateLong(String columnLabel, long x) throws SQLException
	{
	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException
	{
	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException
	{
	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateNClob(int columnIndex, NClob nClob) throws SQLException
	{
	}

	@Override
	public void updateNClob(String columnLabel, NClob nClob) throws SQLException
	{
	}

	@Override
	public void updateNClob(int columnIndex, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateNClob(String columnLabel, Reader reader) throws SQLException
	{
	}

	@Override
	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void updateNString(int columnIndex, String nString) throws SQLException
	{
	}

	@Override
	public void updateNString(String columnLabel, String nString) throws SQLException
	{
	}

	@Override
	public void updateNull(int columnIndex) throws SQLException
	{
	}

	@Override
	public void updateNull(String columnLabel) throws SQLException
	{
	}

	@Override
	public void updateObject(int columnIndex, Object x) throws SQLException
	{
	}

	@Override
	public void updateObject(String columnLabel, Object x) throws SQLException
	{
	}

	@Override
	public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException
	{
	}

	@Override
	public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException
	{
	}

	@Override
	public void updateRef(int columnIndex, Ref x) throws SQLException
	{
	}

	@Override
	public void updateRef(String columnLabel, Ref x) throws SQLException
	{
	}

	@Override
	public void updateRow() throws SQLException
	{
	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException
	{
	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException
	{
	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException
	{
	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException
	{
	}

	@Override
	public void updateShort(int columnIndex, short x) throws SQLException
	{
	}

	@Override
	public void updateShort(String columnLabel, short x) throws SQLException
	{
	}

	@Override
	public void updateString(int columnIndex, String x) throws SQLException
	{
	}

	@Override
	public void updateString(String columnLabel, String x) throws SQLException
	{
	}

	@Override
	public void updateTime(int columnIndex, Time x) throws SQLException
	{
	}

	@Override
	public void updateTime(String columnLabel, Time x) throws SQLException
	{
	}

	@Override
	public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException
	{
	}

	@Override
	public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException
	{
	}

	@Override
	public boolean wasNull() throws SQLException
	{
		return false;
	}

	public TableMetadata getTmd()
	{
		return tmd;
	}

	public void setTmd(TableMetadata tmd)
	{
		this.tmd = tmd;
	}

	@Override
	public <T> T getObject(int arg0, Class<T> arg1) throws SQLException
	{
		return null;
	}

	@Override
	public <T> T getObject(String arg0, Class<T> arg1) throws SQLException
	{
		return null;
	}

	@Override
	protected void finalize() throws Throwable
	{
		super.finalize();
		close();
	}
}