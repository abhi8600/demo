/**
 * 
 */
package memdb.jdbcserver;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import memdb.Database;
import memdb.storage.Table;
import memdb.storage.TableMetadata;

import org.apache.log4j.Logger;


/**
 * @author bpeters
 * 
 */
public class DBMetaData implements java.sql.DatabaseMetaData, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DBMetaData.class);
	private Database db;

	public DBMetaData(Database db)
	{
		try
		{
			this.db = db.remoteClone();
		} catch (ClassNotFoundException e)
		{
			logger.error(e);
		} catch (InstantiationException e)
		{
			logger.error(e);
		} catch (IllegalAccessException e)
		{
			logger.error(e);
		}
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException
	{
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException
	{
		return null;
	}

	@Override
	public boolean allProceduresAreCallable() throws SQLException
	{
		return false;
	}

	@Override
	public boolean allTablesAreSelectable() throws SQLException
	{
		return false;
	}

	@Override
	public boolean autoCommitFailureClosesAllResultSets() throws SQLException
	{
		return false;
	}

	@Override
	public boolean dataDefinitionCausesTransactionCommit() throws SQLException
	{
		return false;
	}

	@Override
	public boolean dataDefinitionIgnoredInTransactions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean deletesAreDetected(int type) throws SQLException
	{
		return false;
	}

	@Override
	public boolean doesMaxRowSizeIncludeBlobs() throws SQLException
	{
		return false;
	}

	@Override
	public ResultSet getAttributes(String catalog, String schemaPattern, String typeNamePattern, String attributeNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCatalogSeparator() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCatalogTerm() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getCatalogs() throws SQLException
	{
		Set<String> schemas = new HashSet<String>();
		for (Table t : db.tables)
		{
			schemas.add(t.schema);
		}
		List<Object[]> data = new ArrayList<Object[]>();
		for (String s : schemas)
			data.add(new Object[]
			{ s });
		TableMetadata tmd = new TableMetadata(1);
		tmd.columnNames = new String[]
		{ "Schema" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR };
		tmd.widths = new int[]
		{ 80 };
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public ResultSet getClientInfoProperties() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException
	{
		List<Object[]> data = new ArrayList<Object[]>();
		TableMetadata tmd = new TableMetadata(22);
		tmd.columnNames = new String[]
		{ "TABLE_CAT", "TABLE_SCHEM", "TABLE_NAME", "COLUMN_NAME", "DATA_TYPE", "TYPE_NAME", "COLUMN_SIZE", "BUFFER_LENGTH", "DECIMAL_DIGITS",
				"NUM_PREC_RADIX", "NULLABLE", "REMARKS", "COLUMN_DEF", "SQL_DATA_TYPE", "SQL_DATETIME_SUB", "CHAR_OCTET_LENGTH", "ORDINAL_POSITION",
				"IS_NULLABLE", "SCOPE_CATLOG", "SCOPE_SCHEMA", "SCOPE_TABLE", "SOURCE_DATA_TYPE" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		tmd.widths = new int[]
		{ 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };
		for (Table t : db.tables)
		{
			if (catalog != null)
			{
				if (!java.util.regex.Pattern.matches(catalog, t.schema))
					continue;
			}
			if (schemaPattern != null)
			{
				if (!java.util.regex.Pattern.matches(schemaPattern, t.schema))
					continue;
			}
			if (tableNamePattern != null)
			{
				if (!java.util.regex.Pattern.matches(tableNamePattern, t.name))
					continue;
			}
			for (int i = 0; i < t.tmd.columnNames.length; i++)
			{
				String dtype = "VARCHAR";
				if (t.tmd.dataTypes[i] == Types.INTEGER)
					dtype = "INTEGER";
				else if (t.tmd.dataTypes[i] == Types.DOUBLE)
					dtype = "DOUBLE";
				else if (t.tmd.dataTypes[i] == Types.FLOAT)
					dtype = "FLOAT";
				else if (t.tmd.dataTypes[i] == Types.DATE)
					dtype = "DATE";
				else if (t.tmd.dataTypes[i] == Types.TIMESTAMP)
					dtype = "TIMESTAMP";
				else if (t.tmd.dataTypes[i] == Types.BOOLEAN)
					dtype = "BOOLEAN";
				else if (t.tmd.dataTypes[i] == Types.DECIMAL)
					dtype = "DECIMAL";
				else if (t.tmd.dataTypes[i] == Types.BIGINT)
					dtype = "BIGINT";
				data.add(new Object[]
				{ t.schema, t.schema, t.name, t.tmd.columnNames[i], t.tmd.dataTypes[i], dtype, t.tmd.widths[i], null, 0, 10, 0, null, null, 0, 0, 255, i + 1,
						"YES", null, null, null, null });
			}
		}
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public Connection getConnection() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getCrossReference(String parentCatalog, String parentSchema, String parentTable, String foreignCatalog, String foreignSchema,
			String foreignTable) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getDatabaseMajorVersion() throws SQLException
	{
		return 1;
	}

	@Override
	public int getDatabaseMinorVersion() throws SQLException
	{
		return 0;
	}

	@Override
	public String getDatabaseProductName() throws SQLException
	{
		return "MemDB";
	}

	@Override
	public String getDatabaseProductVersion() throws SQLException
	{
		return "MemDB 1.0";
	}

	@Override
	public int getDefaultTransactionIsolation() throws SQLException
	{
		return 0;
	}

	@Override
	public int getDriverMajorVersion()
	{
		return 1;
	}

	@Override
	public int getDriverMinorVersion()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getDriverName() throws SQLException
	{
		return "MemDB";
	}

	@Override
	public String getDriverVersion() throws SQLException
	{
		return "MemDB 1.0 Driver";
	}

	@Override
	public ResultSet getExportedKeys(String catalog, String schema, String table) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExtraNameCharacters() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIdentifierQuoteString() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getImportedKeys(String catalog, String schema, String table) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getJDBCMajorVersion() throws SQLException
	{
		return 0;
	}

	@Override
	public int getJDBCMinorVersion() throws SQLException
	{
		return 0;
	}

	@Override
	public int getMaxBinaryLiteralLength() throws SQLException
	{
		return 64;
	}

	@Override
	public int getMaxCatalogNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxCharLiteralLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnsInGroupBy() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnsInIndex() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnsInOrderBy() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnsInSelect() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxColumnsInTable() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxConnections() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxCursorNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxIndexLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxProcedureNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxRowSize() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxSchemaNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxStatementLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxStatements() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxTableNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxTablesInSelect() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMaxUserNameLength() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getNumericFunctions() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getPrimaryKeys(String catalog, String schema, String table) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern) throws SQLException
	{
		List<Object[]> data = new ArrayList<Object[]>();
		TableMetadata tmd = new TableMetadata(1);
		tmd.columnNames = new String[]
		{ "Name" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR };
		tmd.widths = new int[]
		{ 80 };
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public String getProcedureTerm() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern) throws SQLException
	{
		List<Object[]> data = new ArrayList<Object[]>();
		TableMetadata tmd = new TableMetadata(1);
		tmd.columnNames = new String[]
		{ "Name" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR };
		tmd.widths = new int[]
		{ 80 };
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public int getResultSetHoldability() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RowIdLifetime getRowIdLifetime() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSQLKeywords() throws SQLException
	{
		return ("select");
	}

	@Override
	public int getSQLStateType() throws SQLException
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getSchemaTerm() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getSchemas() throws SQLException
	{
		return getSchemas(null, null);
	}

	@Override
	public ResultSet getSchemas(String catalog, String schemaPattern) throws SQLException
	{
		Set<String> schemas = new HashSet<String>();
		for (Table t : db.tables)
		{
			schemas.add(t.schema);
		}
		List<Object[]> data = new ArrayList<Object[]>();
		for (String s : schemas)
			data.add(new Object[]
			{ s });
		TableMetadata tmd = new TableMetadata(1);
		tmd.columnNames = new String[]
		{ "Schema" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR };
		tmd.widths = new int[]
		{ 80 };
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public String getSearchStringEscape() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStringFunctions() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getSuperTables(String catalog, String schemaPattern, String tableNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getSuperTypes(String catalog, String schemaPattern, String typeNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSystemFunctions() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getTableTypes() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String[] types) throws SQLException
	{
		List<Object[]> data = new ArrayList<Object[]>();
		TableMetadata tmd = new TableMetadata(10);
		tmd.columnNames = new String[]
		{ "TABLE_CAT", "TABLE_SCHEM", "TABLE_NAME", "TABLE_TYPE", "REMARKS", "TYPE_CAT", "TYPE_SCHEM", "TYPE_NAME", "SELF_REFERENCING_COL_NAME",
				"REF_GENERATION" };
		tmd.dataTypes = new int[]
		{ Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		tmd.widths = new int[]
		{ 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };
		for (Table t : db.tables)
		{
			if (catalog != null && !t.schema.equals(catalog))
				continue;
			if (schemaPattern != null && !t.schema.equals(schemaPattern))
				continue;
			if (tableNamePattern != null && !t.name.equals(tableNamePattern))
				continue;
			data.add(new Object[]
			{ t.schema, t.schema, t.name, "TABLE", null, null, null, null, null, null });
		}
		RemoteResultSetImpl rrsi = null;
		try
		{
			rrsi = new RemoteResultSetImpl(data, tmd);
		} catch (RemoteException e)
		{
		}
		return rrsi;
	}

	@Override
	public String getTimeDateFunctions() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getTypeInfo() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern, int[] types) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getURL() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUserName() throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet getVersionColumns(String catalog, String schema, String table) throws SQLException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insertsAreDetected(int type) throws SQLException
	{
		return false;
	}

	@Override
	public boolean isCatalogAtStart() throws SQLException
	{
		return false;
	}

	@Override
	public boolean isReadOnly() throws SQLException
	{
		return false;
	}

	@Override
	public boolean locatorsUpdateCopy() throws SQLException
	{
		return false;
	}

	@Override
	public boolean nullPlusNonNullIsNull() throws SQLException
	{
		return false;
	}

	@Override
	public boolean nullsAreSortedAtEnd() throws SQLException
	{
		return false;
	}

	@Override
	public boolean nullsAreSortedAtStart() throws SQLException
	{
		return false;
	}

	@Override
	public boolean nullsAreSortedHigh() throws SQLException
	{
		return false;
	}

	@Override
	public boolean nullsAreSortedLow() throws SQLException
	{
		return true;
	}

	@Override
	public boolean othersDeletesAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean othersInsertsAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean othersUpdatesAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean ownDeletesAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean ownInsertsAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean ownUpdatesAreVisible(int type) throws SQLException
	{
		return true;
	}

	@Override
	public boolean storesLowerCaseIdentifiers() throws SQLException
	{
		return false;
	}

	@Override
	public boolean storesLowerCaseQuotedIdentifiers() throws SQLException
	{
		return false;
	}

	@Override
	public boolean storesMixedCaseIdentifiers() throws SQLException
	{
		return true;
	}

	@Override
	public boolean storesMixedCaseQuotedIdentifiers() throws SQLException
	{
		return true;
	}

	@Override
	public boolean storesUpperCaseIdentifiers() throws SQLException
	{
		return false;
	}

	@Override
	public boolean storesUpperCaseQuotedIdentifiers() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsANSI92EntryLevelSQL() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsANSI92FullSQL() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsANSI92IntermediateSQL() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsAlterTableWithAddColumn() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsAlterTableWithDropColumn() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsBatchUpdates() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCatalogsInDataManipulation() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCatalogsInIndexDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCatalogsInPrivilegeDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCatalogsInProcedureCalls() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCatalogsInTableDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsColumnAliasing() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsConvert() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsConvert(int fromType, int toType) throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsCoreSQLGrammar() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsCorrelatedSubqueries() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsDataDefinitionAndDataManipulationTransactions() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsDataManipulationTransactionsOnly() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsDifferentTableCorrelationNames() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsExpressionsInOrderBy() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsExtendedSQLGrammar() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsFullOuterJoins() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsGetGeneratedKeys() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsGroupBy() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsGroupByBeyondSelect() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsGroupByUnrelated() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsIntegrityEnhancementFacility() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsLikeEscapeClause() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsLimitedOuterJoins() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsMinimumSQLGrammar() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsMixedCaseIdentifiers() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsMixedCaseQuotedIdentifiers() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsMultipleOpenResults() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsMultipleResultSets() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsMultipleTransactions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsNamedParameters() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsNonNullableColumns() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOpenCursorsAcrossCommit() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOpenCursorsAcrossRollback() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOpenStatementsAcrossCommit() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOpenStatementsAcrossRollback() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOrderByUnrelated() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsOuterJoins() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsPositionedDelete() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsPositionedUpdate() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsResultSetConcurrency(int type, int concurrency) throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsResultSetHoldability(int holdability) throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsResultSetType(int type) throws SQLException
	{
		return type == ResultSet.TYPE_SCROLL_INSENSITIVE;
	}

	@Override
	public boolean supportsSavepoints() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSchemasInDataManipulation() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSchemasInIndexDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSchemasInPrivilegeDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSchemasInProcedureCalls() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSchemasInTableDefinitions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSelectForUpdate() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsStatementPooling() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsStoredFunctionsUsingCallSyntax() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsStoredProcedures() throws SQLException
	{
		return true;
	}

	@Override
	public boolean supportsSubqueriesInComparisons() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSubqueriesInExists() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSubqueriesInIns() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsSubqueriesInQuantifieds() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsTableCorrelationNames() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsTransactionIsolationLevel(int level) throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsTransactions() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsUnion() throws SQLException
	{
		return false;
	}

	@Override
	public boolean supportsUnionAll() throws SQLException
	{
		return false;
	}

	@Override
	public boolean updatesAreDetected(int type) throws SQLException
	{
		return false;
	}

	@Override
	public boolean usesLocalFilePerTable() throws SQLException
	{
		return false;
	}

	@Override
	public boolean usesLocalFiles() throws SQLException
	{
		return false;
	}

	@Override
	public ResultSet getPseudoColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException
	{
		return null;
	}

	@Override
	public boolean generatedKeyAlwaysReturned() throws SQLException
	{
		return false;
	}
}
