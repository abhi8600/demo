/**
 * RemoteStatementImpl - This class contains the actual JDBC-ODBC statement.
 * It acts as a remote wrapper over the JDBC-ODBC statement.
 */
package memdb.jdbcserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.TimeZone;

import memdb.Command;
import memdb.Database;
import memdb.TaskThread;

public class RemoteStatementImpl extends UnicastRemoteObject implements IRemoteStatement
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Database db;
	private Command c;

	/**
	 * Constructor for RemoteStatementImpl with JDBC-ODBC Statement
	 */
	public RemoteStatementImpl(Database db, TimeZone clientTimeZone) throws RemoteException
	{
		super();
		this.db = db;
		this.c = new Command(-2, clientTimeZone);
	}

	public RemoteStatementImpl(Database db, String sql, TimeZone clientTimeZone) throws RemoteException, SQLException
	{
		super();
		this.db = db;
		this.c = new Command(-2, clientTimeZone);
		c.parseCommand(sql);
	}

	/**
	 * This method gets a JDBC-ODBC ResultSet and then returns a reference to the remote interface of the
	 * RemoteResultSetImpl object holding JDBC-ODBC ResultSet.
	 */
	public IRemoteResultSet executeQuery(Object[] parameters) throws RemoteException, SQLException
	{
		if (!c.validPrepared())
			throw new SQLException("Invalid prepared statement");
		c.setParameters(parameters);
		return executeRemoteQuery((String) null);
	}

	/**
	 * This method gets a JDBC-ODBC ResultSet and then returns a reference to the remote interface of the
	 * RemoteResultSetImpl object holding JDBC-ODBC ResultSet.
	 */
	public IRemoteResultSet executeQuery(String query) throws RemoteException, SQLException
	{
		c.clear();
		return executeRemoteQuery(query);
	}

	private IRemoteResultSet executeRemoteQuery(String query) throws RemoteException, SQLException
	{
		Object results = null;
		try
		{
			TaskThread tt = null;
			if (query == null)
				tt = TaskThread.launchNewCommand(c, db, null);
			else
			{
				tt = TaskThread.launchNewCommand(query, db, TimeZone.getDefault(), null);
				c = tt.getCommand();
			}
			while (tt.isAlive())
			{
				try
				{
					tt.join(2000);
				} catch (InterruptedException ie)
				{
					System.out.println("running(" + tt.getState() + "): " + query);
				}
			}
			results = tt.getResults();
		} catch (SQLException ex)
		{
			throw ex;
		}
		if (results == null)
		{
			return null;
		}
		if (results instanceof String)
		{
			throw new SQLException(results.toString());
		}
		RemoteResultSetImpl remoteRs = (RemoteResultSetImpl) results;
		return (IRemoteResultSet) remoteRs;
	}

	public int executeUpdate(Object[] parameters) throws SQLException, RemoteException
	{
		if (!c.validPrepared())
			throw new SQLException("Invalid prepared statement");
		c.setParameters(parameters);
		return executeRemoteUpdate((String) null);
	}

	/**
	 * This method executes the data modify statement
	 */
	public int executeUpdate(String query) throws RemoteException, SQLException
	{
		c.clear();
		return executeRemoteUpdate(query);
	}

	private int executeRemoteUpdate(String query) throws RemoteException, SQLException
	{
		Object results = null;
		int rowsProcessed = 0;
		try
		{
			TaskThread tt = null;
			if (query == null)
				tt = TaskThread.launchNewCommand(c, db, null);
			else
			{
				tt = TaskThread.launchNewCommand(query, db, c.getClientTimeZone(), null);
				c = tt.getCommand();
			}
			try
			{
				tt.join();
				results = tt.getResults();
			} catch (InterruptedException e)
			{
				return 0;
			}
			rowsProcessed = c.getRowsProcessed();
		} catch (SQLException ex)
		{
			throw ex;
		}
		if (results instanceof String)
		{
			throw new SQLException(results.toString());
		}
		return rowsProcessed;
	}

	public void close() throws RemoteException, SQLException
	{
		c = null;
	}
}