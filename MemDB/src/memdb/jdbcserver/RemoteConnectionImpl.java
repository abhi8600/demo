/**
 * RemoteConnectionImpl - This class contains the actual JDBC-ODBC connection. 
 * It acts as a remote wrapper over the JDBC-ODBC connection.
 */
package memdb.jdbcserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.TimeZone;

import memdb.Database;

public class RemoteConnectionImpl extends UnicastRemoteObject implements IRemoteConnection
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Database db;

	public RemoteConnectionImpl(Database db) throws RemoteException
	{
		super();
		this.db = db;
	}

	public IRemoteStatement createStatement(TimeZone clientTimeZone) throws RemoteException, SQLException
	{
		RemoteStatementImpl StmtImplInstance = new RemoteStatementImpl(db, clientTimeZone);
		return (IRemoteStatement) StmtImplInstance;
	}

	public DBMetaData getMetaData() throws RemoteException
	{
		DBMetaData dbm = new DBMetaData(db);
		return dbm;
	}

	public void closeConnection() throws RemoteException, SQLException
	{
	}

	@Override
	public IRemoteStatement prepareStatement(String sql, TimeZone clientTimeZone) throws RemoteException, SQLException
	{
		RemoteStatementImpl StmtImplInstance = new RemoteStatementImpl(db, sql, clientTimeZone);
		return (IRemoteStatement) StmtImplInstance;
	}

	@Override
	public TimeZone getServerTimeZone() throws RemoteException
	{
		return TimeZone.getDefault();
	}
}