/**
 * IRemoteStatement - This interface exposes the basic Statement methods remotely
 */
package memdb.jdbcserver;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

public interface IRemoteStatement extends Remote
{
	IRemoteResultSet executeQuery(String Query) throws RemoteException, SQLException;
	
	IRemoteResultSet executeQuery(Object[] parameters) throws RemoteException, SQLException;

	int executeUpdate(String Query) throws RemoteException, SQLException;
	
	int executeUpdate(Object[] parameters) throws RemoteException, SQLException;

	void close() throws RemoteException, SQLException;
}