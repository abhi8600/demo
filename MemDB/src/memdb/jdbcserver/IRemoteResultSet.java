/**
 * IRemoteResultSet - This interface exposes the basic ResultSet methods remotly. 
 */
package memdb.jdbcserver;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;

import memdb.sql.QueryResultRow;
import memdb.storage.TableMetadata;


public interface IRemoteResultSet extends Remote
{
	Object[] getNextRow() throws RemoteException, SQLException;

	void close() throws RemoteException, SQLException;

	String[] getColumnNames() throws RemoteException, SQLException;

	int[] getColumnTypes() throws RemoteException, SQLException;

	public  QueryResultRow[] getResults(boolean getAllResults) throws RemoteException;

	public TableMetadata getTmd() throws RemoteException;
}