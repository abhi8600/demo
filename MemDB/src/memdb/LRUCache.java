package memdb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

public class LRUCache<K, V>
{
	private static Logger logger = Logger.getLogger(LRUCache.class);
	private static final float hashTableLoadFactor = 0.9f;
	private static int MAX_CACHE_SIZE = 150000;
	private static double MIN_FREE_RATIO_TO_EXPAND = 0.3;
	private Map<K, LRUCacheObject<V>> map;
	private TreeMap<Long, K> stampMap;
	protected int cacheSize;
	private long stamp = 0;
	private boolean useStamps;

	/**
	 * Creates a new LRU cache.
	 * 
	 * @param cacheSize
	 *            the maximum number of entries that will be kept in this cache.
	 */
	public LRUCache(int cacheSize, boolean useStamps)
	{
		int hashTableCapacity = (int) Math.ceil(cacheSize / hashTableLoadFactor) + 1;
		this.cacheSize = cacheSize;
		map = new HashMap<K, LRUCacheObject<V>>(hashTableCapacity, hashTableLoadFactor);
		this.useStamps = useStamps;
		if (useStamps)
			stampMap = new TreeMap<Long, K>();
	}

	private void removeLast()
	{
		synchronized (map)
		{
			int numToRemove = map.size() / 3;
			if (useStamps)
			{
				for (int i = 0; i < numToRemove; i++)
				{
					Entry<Long, K> entry = stampMap.firstEntry();
					stampMap.remove(entry.getKey());
					map.remove(entry.getValue());
				}
			} else
			{
				int count = 0;
				List<K> keyList = new ArrayList<K>();
				for (Entry<K, LRUCacheObject<V>> en : map.entrySet())
				{
					if (count++ >= numToRemove)
						break;
					keyList.add(en.getKey());
				}
				for (K key : keyList)
					map.remove(key);
			}
		}
	}

	public int numElements()
	{
		return map.size();
	}

	public V get(K key)
	{
		LRUCacheObject<V> lco = map.get(key);
		if (lco != null)
		{
			if (useStamps)
			{
				synchronized (stampMap)
				{
					stampMap.remove(lco.stamp);
					lco.stamp = stamp++;
					stampMap.put(lco.stamp, key);
				}
			}
			return lco.o;
		}
		return null;
	}

	public void put(K key, V value)
	{
		LRUCacheObject<V> lco = map.get(key);
		boolean incrementStamp = true;
		if (lco == null)
		{
			synchronized (map)
			{
				lco = map.get(key);
				if (lco == null)
				{
					lco = new LRUCacheObject<V>();
					map.put(key, lco);
					if (map.size() > LRUCache.this.cacheSize)
					{
						long freemem = Runtime.getRuntime().maxMemory() + Runtime.getRuntime().freeMemory() - Runtime.getRuntime().totalMemory();
						double freeRatio = (double) freemem / (double) Runtime.getRuntime().maxMemory();
						if (freeRatio > MIN_FREE_RATIO_TO_EXPAND && LRUCache.this.cacheSize < MAX_CACHE_SIZE)
						{
							logger.debug("Expanding cache: " + freemem + '-' + LRUCache.this.cacheSize);
							LRUCache.this.cacheSize = LRUCache.this.cacheSize * 10 / 9;
						} else
						{
							// Remove
							removeLast();
						}
					}
					if (useStamps)
					{
						lco.stamp = stamp++;
						synchronized (stampMap)
						{
							stampMap.put(lco.stamp, key);
						}
						incrementStamp = false;
					}
				}
			}
		}
		if (useStamps && incrementStamp)
		{
			synchronized (stampMap)
			{
				stampMap.remove(lco.stamp);
				lco.stamp = stamp++;
				stampMap.put(lco.stamp, key);
			}
		}
		lco.o = value;
	}

	/**
	 * Clears the cache.
	 */
	public void clear()
	{
		synchronized (map)
		{
			map.clear();
		}
	}

	public int usedEntries()
	{
		return map.size();
	}

	public void remove(K key)
	{
		try
		{
			synchronized (map)
			{
				map.remove(key);
			}
		} catch (NullPointerException ex)
		{
			// This is unsynchronized so it could already be removed
		}
	}

	public void restoreToSize()
	{
		synchronized (map)
		{
			while (map.size() >= cacheSize)
			{
				K key = map.keySet().iterator().next();
				if (key != null)
				{
					removeLast();
				}
			}
		}
	}

	public Set<Entry<K, LRUCacheObject<V>>> getEntrySet()
	{
		synchronized (map)
		{
			Set<Entry<K, LRUCacheObject<V>>> eset = new HashSet<Entry<K, LRUCacheObject<V>>>(map.entrySet());
			return eset;
		}
	}

	public Collection<Map.Entry<K, LRUCacheObject<V>>> getAll()
	{
		return new ArrayList<Map.Entry<K, LRUCacheObject<V>>>(map.entrySet());
	}

	public int getCacheSize()
	{
		return cacheSize;
	}

	public void setCacheSize(int cacheSize)
	{
		this.cacheSize = cacheSize;
		restoreToSize();
	}
}
