/**
 * 
 */
package memdb;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.jdbcserver.RemoteResultSetImpl;

import org.apache.log4j.Logger;

/**
 * @author bpeters
 * 
 */
public class TaskThread extends Thread
{
	// Static variables
	static Logger logger = Logger.getLogger(TaskThread.class);
	private static Map<Integer, TaskThread> threadMap = Collections.synchronizedMap(new HashMap<Integer, TaskThread>());
	private static AtomicInteger threadCount = new AtomicInteger(0);
	// Local variables
	private String command;
	private Command c;
	private Database db;
	private PrintStream out;
	private int threadNum = 0;
	private long starttime;
	private RemoteResultSetImpl results;
	private SQLException ex;

	public static TaskThread launchNewCommand(String command, Database db, TimeZone clientTimeZone, PrintStream out)
	{
		TaskThread t = new TaskThread(command, db, clientTimeZone, out);
		t.start();
		return t;
	}

	public static TaskThread launchNewCommand(Command c, Database db, PrintStream out)
	{
		TaskThread t = new TaskThread(c, db, out);
		t.start();
		return t;
	}

	public static Set<Entry<Integer, TaskThread>> getThreads()
	{
		return threadMap.entrySet();
	}

	public TaskThread(String command, Database db, TimeZone clientTimeZone, PrintStream out)
	{
		this.command = command;
		this.db = db;
		this.out = out;
		this.threadNum = threadCount.getAndIncrement();
		this.starttime = System.currentTimeMillis();
		this.c = new Command(threadNum, clientTimeZone);
		threadMap.put(threadNum, this);
	}

	public TaskThread(Command c, Database db, PrintStream out)
	{
		this.c = c;
		this.command = c.getCommandString();
		this.db = db;
		this.out = out;
		this.threadNum = threadCount.getAndIncrement();
		this.starttime = System.currentTimeMillis();
		c.setThreadNum(threadNum);
		threadMap.put(threadNum, this);
	}

	public void run()
	{
		try
		{
			results = c.process(db, command, out);
		} catch (SQLException e)
		{
			ex = e;
		}
		threadMap.remove(threadNum);
	}

	public static void killTaskThread(int threadNumber)
	{
		TaskThread tt = threadMap.get(threadNumber);
		if (tt != null)
		{
			threadMap.remove(threadNumber);
			tt.c.setKilled(true);
		}
	}

	public String getCommandString()
	{
		return command;
	}

	public long getStarttime()
	{
		return starttime;
	}

	public int getThreadNum()
	{
		return threadNum;
	}

	public Command getCommand()
	{
		return c;
	}

	public RemoteResultSetImpl getResults() throws SQLException
	{
		if (ex != null)
			throw ex;
		return results;
	}
}
