package memdb;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;
import java.util.TimeZone;

public class DateParser
{
	/**
	 * 
	 */
	/**
	 * @param dateUtil
	 */
	public DateParser()
	{
	}
	private Stack<SimpleDateFormat> formatCache = new Stack<SimpleDateFormat>();
	private String lastExpression;
	private Date lastDate;
	private SimpleCache<String, Date> cache = new SimpleCache<String, Date>(1000);

	@SuppressWarnings("hiding")
	private static class SimpleCache<String, Date> extends LinkedHashMap<String, Date>
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SimpleCache(int size)
		{
			super(size);
		}

		@Override
		protected boolean removeEldestEntry(Map.Entry<String, Date> eldest)
		{
			if (this.size() > 1000)
				return true;
			return false;
		}
	}

	public Date parseUsingPossibleFormats(String expression, TimeZone tz)
	{
		Date d = null;
		expression = expression.trim();
		if (expression.length() > 2 && expression.charAt(0) == '\'' && expression.charAt(expression.length() - 1) == '\'')
		{
			expression = expression.substring(1, expression.length() - 1);
		}
		if (expression.length() == 0)
			return null;
		if (lastExpression != null && lastExpression.equals(expression))
			return lastDate;
		lastExpression = expression;
		d = cache.get(expression);
		if (d != null)
			return d;
		ParsePosition pos = new ParsePosition(0);
		if (formatCache.size() > 0)
		{
			for (SimpleDateFormat lastsdf : formatCache)
			{
				d = lastsdf.parse(expression, pos);
				if (d == null)
					continue;
				lastDate = d;
				cache.put(expression, d);
				return d;
			}
		}
		for (SimpleDateFormat sdf : DateUtil.possibleDateFormats)
		{
			synchronized (sdf)
			{
				sdf.setTimeZone(tz);
				// sdf is not thread safe, so protect the access.
				d = sdf.parse(expression, pos);
			}
			if (d == null)
				continue;
			formatCache.add((SimpleDateFormat) sdf.clone());
			lastDate = d;
			cache.put(expression, d);
			return d;
		}
		return null; // unable to parse using any known date formats
	}
}