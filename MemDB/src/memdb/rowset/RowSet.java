package memdb.rowset;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.NoSuchElementException;

import memdb.indexlist.DifferencePackedIntegerList;
import memdb.sql.RowRange;

import org.apache.log4j.Logger;

public class RowSet implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(RowSet.class);

	public enum RowSetType
	{
		Range, Set, List, ArrayList, BitSet, PackedList
	};
	private RowRange range;
	private TIntSet set;
	private TIntArrayList list;
	private int[] rowList;
	private boolean isNull;
	private RowSetType type;
	private BitSet bits;
	private DifferencePackedIntegerList packedList;
	private RowSet baseSet;

	public RowSet()
	{
		set = new TIntHashSet();
		type = RowSetType.Set;
	}

	public RowSet(RowSet oldSetWithoutCopyingBaseSet, boolean cloneForModification)
	{
		if (!cloneForModification)
		{
			this.range = oldSetWithoutCopyingBaseSet.range;
			this.set = oldSetWithoutCopyingBaseSet.set;
			this.list = oldSetWithoutCopyingBaseSet.list;
			this.rowList = oldSetWithoutCopyingBaseSet.rowList;
			this.isNull = oldSetWithoutCopyingBaseSet.isNull;
			this.type = oldSetWithoutCopyingBaseSet.type;
			this.bits = oldSetWithoutCopyingBaseSet.bits;
			this.packedList = oldSetWithoutCopyingBaseSet.packedList;
		} else
		{
			if (oldSetWithoutCopyingBaseSet.range != null)
				this.range = oldSetWithoutCopyingBaseSet.range;
			if (oldSetWithoutCopyingBaseSet.set != null)
				this.set = new TIntHashSet(oldSetWithoutCopyingBaseSet.set);
			if (oldSetWithoutCopyingBaseSet.list != null)
				this.list = new TIntArrayList(oldSetWithoutCopyingBaseSet.list);
			if (oldSetWithoutCopyingBaseSet.rowList != null)
				this.rowList = oldSetWithoutCopyingBaseSet.rowList;
			if (oldSetWithoutCopyingBaseSet.packedList != null)
				this.packedList = oldSetWithoutCopyingBaseSet.packedList;
			this.isNull = oldSetWithoutCopyingBaseSet.isNull;
			this.type = oldSetWithoutCopyingBaseSet.type;
			this.bits = oldSetWithoutCopyingBaseSet.bits;
		}
	}

	public RowSet(RowSetType newtype, int size)
	{
		type = newtype;
		switch (newtype)
		{
		case Set:
			set = new TIntHashSet(size);
			break;
		case List:
			rowList = new int[size];
			break;
		case ArrayList:
			list = new TIntArrayList(size);
			break;
		case Range:
			range = new RowRange();
			range.start = 0;
			range.stop = size - 1;
			break;
		case BitSet:
			bits = new BitSet(size);
			break;
		case PackedList:
			type = RowSetType.ArrayList;
			list = new TIntArrayList(size);
			break;
		}
	}

	public RowSet(TIntSet set)
	{
		this.set = set;
		type = RowSetType.Set;
	}

	public RowSet(TIntSet set, RowSet baseSet)
	{
		this.set = set;
		type = RowSetType.Set;
		this.baseSet = baseSet;
	}

	public RowSet(TIntArrayList list)
	{
		this.list = list;
		type = RowSetType.ArrayList;
	}

	public RowSet(TIntArrayList list, RowSet baseSet)
	{
		this.list = list;
		type = RowSetType.ArrayList;
		this.baseSet = baseSet;
	}

	public RowSet(int size)
	{
		set = new TIntHashSet(size);
		type = RowSetType.Set;
	}

	public RowSet(int start, int stop)
	{
		range = new RowRange();
		range.start = start;
		range.stop = stop;
		type = RowSetType.Range;
	}

	public RowSet(int start, int stop, RowSet baseSet)
	{
		range = new RowRange();
		range.start = start;
		range.stop = stop;
		type = RowSetType.Range;
		this.baseSet = baseSet;
	}

	public RowSet(int[] list)
	{
		this.rowList = list;
		type = RowSetType.List;
	}

	public RowSet(int[] list, RowSet baseSet)
	{
		this.rowList = list;
		type = RowSetType.List;
		this.baseSet = baseSet;
	}

	public RowSet(BitSet bits)
	{
		this.bits = bits;
		type = RowSetType.BitSet;
	}

	public RowSet(BitSet bits, RowSet baseSet)
	{
		this.bits = bits;
		type = RowSetType.BitSet;
		this.baseSet = baseSet;
	}

	public RowSet(DifferencePackedIntegerList packedList, RowSet baseSet)
	{
		this.packedList = packedList;
		type = RowSetType.PackedList;
		this.baseSet = baseSet;
	}

	public RowSet(DifferencePackedIntegerList packedList, int start, int stop)
	{
		this.packedList = packedList;
		type = RowSetType.PackedList;
		range = new RowRange();
		range.start = start;
		range.stop = stop;
	}

	public static RowSet getEmptySet()
	{
		RowSet rs = new RowSet(0, -1);
		return rs;
	}

	public void setList(TIntArrayList list)
	{
		this.list = list;
		this.set = null;
		this.range = null;
		this.rowList = null;
		type = RowSetType.ArrayList;
	}

	public final void addAll(RowSet newset)
	{
		if (newset == null)
			return;
		switch (type)
		{
		case BitSet:
			switch (newset.type)
			{
			case BitSet:
				bits.or(newset.bits);
				break;
			case Set:
			case List:
			case ArrayList:
			case PackedList:
				RowSetIterator rsit = newset.getIterator();
				int rnum = 0;
				while (true)
				{
					rnum = rsit.getNextRow();
					if (rnum < 0)
						break;
					bits.set(rnum);
				}
				break;
			case Range:
				for (int i = newset.range.start; i <= newset.range.stop; i++)
					bits.set(i);
			}
			break;
		case ArrayList:
			convertToSetFromList();
		case Set:
			switch (newset.type)
			{
			case Set:
				set.addAll(newset.set);
				break;
			case ArrayList:
				set.addAll(newset.list);
				break;
			case List:
				set.addAll(newset.rowList);
				break;
			case Range:
				for (int i = newset.range.start; i <= newset.range.stop; i++)
					set.add(i);
				break;
			case BitSet:
				for (int i = newset.bits.nextSetBit(0); i >= 0; i = newset.bits.nextSetBit(i + 1))
				{
					set.add(i);
				}
				break;
			case PackedList:
				RowSetIterator rsit = newset.getIterator();
				int rnum = 0;
				while (true)
				{
					rnum = rsit.getNextRow();
					if (rnum < 0)
						break;
					set.add(rnum);
				}
				break;
			}
			break;
		case Range:
			set = new TIntHashSet(range.stop - range.start);
			for (int i = range.start; i <= range.stop; i++)
				set.add(i);
			range = null;
			switch (newset.type)
			{
			case Set:
				set.addAll(newset.set);
				break;
			case Range:
				for (int i = newset.range.start; i <= newset.range.stop; i++)
					set.add(i);
				break;
			case ArrayList:
				break;
			case BitSet:
				break;
			case List:
				break;
			case PackedList:
				break;
			default:
				break;
			}
			type = RowSetType.Set;
			break;
		case List:
			break;
		case PackedList:
			break;
		default:
			break;
		}
	}

	public final void convertToSetFromRange()
	{
		if (type != RowSetType.Range)
			return;
		set = new TIntHashSet(range.stop - range.start + 1);
		if (baseSet == null)
		{
			for (int i = range.start; i <= range.stop; i++)
				set.add(i);
		} else
		{
			for (int i = range.start; i <= range.stop; i++)
				if (baseSet.contains(i))
					set.add(i);
		}
		baseSet = null;
		range = null;
		type = RowSetType.Set;
	}

	public final void convertToBitSetFromRange(int size)
	{
		if (type != RowSetType.Range)
			return;
		bits = new BitSet(size);
		if (baseSet == null)
		{
			for (int i = range.start; i <= range.stop; i++)
				bits.set(i);
		} else
		{
			for (int i = range.start; i <= range.stop; i++)
				if (baseSet.contains(i))
					bits.set(i);
		}
		baseSet = null;
		range = null;
		set = null;
		type = RowSetType.BitSet;
	}

	public final TIntSet getSet()
	{
		switch (type)
		{
		case Range:
			convertToSetFromRange();
			break;
		case List:
			convertToSetFromRowList();
			break;
		case ArrayList:
		case PackedList:
			convertToSetFromList();
			break;
		case BitSet:
			convertToSetFromBitSet();
			break;
		case Set:
			break;
		default:
			break;
		}
		if (baseSet != null)
		{
			set.retainAll(baseSet.getSet());
			baseSet = null;
		}
		return set;
	}

	public final BitSet getBitSet(int size)
	{
		switch (type)
		{
		case Range:
			convertToBitSetFromRange(size);
			break;
		case List:
			convertToBitSetFromRowList(size);
			break;
		case ArrayList:
		case PackedList:
			convertToBitSetFromList(size);
			break;
		case Set:
			convertToBitSetFromSet(size);
			break;
		case BitSet:
			return bits;
		default:
			break;
		}
		if (baseSet != null)
		{
			for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
				if (!baseSet.contains(i))
					bits.clear(i);
			baseSet = null;
		}
		return bits;
	}

	/**
	 * Add all in this set that are in a baseset to another rowset.
	 * 
	 * @param rows
	 * @param baseset
	 */
	public final void addAllToSet(RowSet rows, RowSet baseset)
	{
		switch (type)
		{
		case BitSet:
			switch (baseset.type)
			{
			case BitSet:
				BitSet newbits = (BitSet) bits.clone();
				newbits.and(baseset.bits);
				switch (rows.type)
				{
				case BitSet:
					rows.bits.or(newbits);
					break;
				default:
					for (int i = newbits.nextSetBit(0); i >= 0; i = newbits.nextSetBit(i + 1))
					{
						rows.add(i);
					}
				}
				break;
			default:
				for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
				{
					if (baseset.contains(i))
					{
						rows.add(i);
					}
				}
			}
			break;
		case Range:
			for (int i = range.start; i <= range.stop; i++)
				if (baseset.contains(i))
				{
					rows.add(i);
				}
			break;
		case List:
			switch (baseset.type)
			{
			case BitSet:
				for (int i : rowList)
					if (baseset.bits.get(i))
						rows.add(i);
				break;
			default:
				for (int i : rowList)
					if (baseset.contains(i))
					{
						rows.add(i);
					}
				break;
			}
			break;
		case ArrayList:
			switch (baseset.type)
			{
			case BitSet:
				for (int i = 0; i < list.size(); i++)
					if (baseset.bits.get(list.get(i)))
					{
						rows.add(list.get(i));
					}
				break;
			default:
				for (int i = 0; i < list.size(); i++)
					if (baseset.contains(list.get(i)))
					{
						rows.add(list.get(i));
					}
				break;
			}
			break;
		case PackedList:
			TIntArrayList newlist = packedList.getIntArrayList(-1);
			switch (baseset.type)
			{
			case BitSet:
				for (int i = 0; i < newlist.size(); i++)
					if (baseset.bits.get(newlist.get(i)))
					{
						rows.add(newlist.get(i));
					}
				break;
			default:
				for (int i = 0; i < newlist.size(); i++)
					if (baseset.contains(newlist.get(i)))
					{
						rows.add(newlist.get(i));
					}
				break;
			}
			break;
		case Set:
			TIntIterator tintit = set.iterator();
			try
			{
				int i = 0;
				while (true)
				{
					i = tintit.next();
					if (baseset.contains(i))
					{
						rows.add(i);
					}
				}
			} catch (NoSuchElementException ex)
			{
			}
			break;
		}
	}

	private final void convertToSetFromRowList()
	{
		if (type != RowSetType.List)
			return;
		set = new TIntHashSet(rowList.length);
		if (baseSet == null)
		{
			for (int i = 0; i < rowList.length; i++)
				set.add(i);
		} else
		{
			for (int i = 0; i < rowList.length; i++)
				if (baseSet.contains(i))
					set.add(i);
		}
		baseSet = null;
		rowList = null;
		type = RowSetType.Set;
	}

	private final void convertToBitSetFromRowList(int size)
	{
		if (type != RowSetType.List)
			return;
		bits = new BitSet(size);
		if (baseSet == null)
		{
			for (int i = 0; i < rowList.length; i++)
				bits.set(rowList[i]);
		} else
		{
			for (int i = 0; i < rowList.length; i++)
				if (baseSet.contains(i))
					bits.set(rowList[i]);
		}
		baseSet = null;
		rowList = null;
		set = null;
		type = RowSetType.BitSet;
	}

	private final void convertToSetFromBitSet()
	{
		set = new TIntHashSet(rowList.length);
		if (baseSet == null)
		{
			for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
			{
				set.add(i);
			}
		} else
		{
			for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
				if (baseSet.contains(i))
					set.add(i);
		}
		baseSet = null;
		bits = null;
		type = RowSetType.Set;
	}

	private final void convertToBitSetFromSet(int size)
	{
		bits = new BitSet(size);
		if (baseSet != null)
		{
			TIntIterator tintit = set.iterator();
			try
			{
				int i = 0;
				while (true)
				{
					i = tintit.next();
					if (baseSet.contains(i))
					{
						bits.set(i);
					}
				}
			} catch (NoSuchElementException ex)
			{
			}
		} else
		{
			TIntIterator tintit = set.iterator();
			try
			{
				int i = 0;
				while (true)
				{
					i = tintit.next();
					bits.set(i);
				}
			} catch (NoSuchElementException ex)
			{
			}
		}
		baseSet = null;
		set = null;
		type = RowSetType.BitSet;
	}

	private final void convertToSetFromList()
	{
		set = getNewSetFromList();
		if (set == null)
			return;
		list = null;
		packedList = null;
		type = RowSetType.Set;
	}

	private final void convertToBitSetFromList(int size)
	{
		bits = getNewBitSetFromList(size);
		if (bits == null)
			return;
		list = null;
		packedList = null;
		set = null;
		type = RowSetType.BitSet;
	}

	private final TIntSet getNewSetFromList()
	{
		TIntSet set;
		switch (type)
		{
		case ArrayList:
			set = new TIntHashSet(list.size());
			if (baseSet == null)
			{
				for (int i = 0; i < list.size(); i++)
					set.add(list.get(i));
			} else
			{
				for (int i = 0; i < list.size(); i++)
				{
					int val = list.get(i);
					if (baseSet.contains(val))
						set.add(val);
				}
				baseSet = null;
			}
			return set;
		case PackedList:
			TIntArrayList newlist = packedList.getIntArrayList(-1);
			set = new TIntHashSet(newlist.size());
			if (baseSet == null)
			{
				for (int i = 0; i < newlist.size(); i++)
					set.add(newlist.get(i));
			} else
			{
				for (int i = 0; i < newlist.size(); i++)
				{
					int val = newlist.get(i);
					if (baseSet.contains(val))
						set.add(val);
				}
				baseSet = null;
			}
			return set;
		case List:
			set = new TIntHashSet(rowList.length);
			if (baseSet == null)
			{
				for (int i = 0; i < rowList.length; i++)
					set.add(rowList[i]);
			} else
			{
				for (int i = 0; i < rowList.length; i++)
				{
					int val = rowList[i];
					if (baseSet.contains(val))
						set.add(val);
				}
				baseSet = null;
			}
			return set;
		default:
			return null;
		}
	}

	private final BitSet getNewBitSetFromList(int size)
	{
		BitSet bits = null;
		switch (type)
		{
		case ArrayList:
			bits = new BitSet(size);
			if (baseSet == null)
			{
				for (int i = 0; i < list.size(); i++)
					bits.set(list.get(i));
			} else
			{
				for (int i = 0; i < list.size(); i++)
				{
					int val = list.get(i);
					if (baseSet.contains(val))
						bits.set(val);
				}
				baseSet = null;
			}
			return bits;
		case PackedList:
			bits = packedList.getBitSet(size);
			if (baseSet != null)
			{
				for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
				{
					if (!baseSet.contains(i))
						bits.clear(i);
				}
				baseSet = null;
			}
			return bits;
		case List:
			bits = new BitSet(size);
			if (baseSet == null)
			{
				for (int i = 0; i < rowList.length; i++)
					bits.set(rowList[i]);
			} else
			{
				for (int i = 0; i < rowList.length; i++)
				{
					int val = rowList[i];
					if (baseSet.contains(val))
						bits.set(val);
				}
				baseSet = null;
			}
			return bits;
		default:
			return null;
		}
	}

	protected final BitSet getNewBitSet(int maxValue)
	{
		BitSet bits;
		switch (type)
		{
		case ArrayList:
			bits = new BitSet(maxValue);
			for (int i = 0; i < list.size(); i++)
				bits.set(list.get(i));
			return bits;
		case PackedList:
			TIntArrayList newlist = packedList.getIntArrayList(-1);
			bits = new BitSet(maxValue);
			for (int i = 0; i < newlist.size(); i++)
				bits.set(newlist.get(i));
			return bits;
		case List:
			bits = new BitSet(maxValue);
			set = new TIntHashSet(rowList.length);
			for (int i = 0; i < rowList.length; i++)
				bits.set(rowList[i]);
			return bits;
		case Set:
			bits = new BitSet(maxValue);
			TIntIterator tintit = set.iterator();
			try
			{
				while (true)
				{
					int i = tintit.next();
					bits.set(i);
				}
			} catch (NoSuchElementException nsex)
			{
			}
			return bits;
		default:
			return null;
		}
	}

	public final int get(int rownum)
	{
		switch (type)
		{
		case ArrayList:
			return list.getQuick(rownum);
		case PackedList:
			TIntArrayList newlist = packedList.getIntArrayList(0, rownum);
			return newlist.get(rownum);
		case List:
			return rowList[rownum];
		case Range:
			return range.start + rownum;
		case BitSet:
			break;
		case Set:
			break;
		default:
			break;
		}
		return rownum;
	}

	public final void add(int rownum)
	{
		switch (type)
		{
		case Set:
			set.add(rownum);
			break;
		case Range:
			if (rownum == range.stop + 1)
			{
				range.stop++;
				return;
			} else if (rownum == range.start - 1)
			{
				range.start--;
				return;
			}
			convertToSetFromRange();
			set.add(rownum);
			break;
		case ArrayList:
			list.add(rownum);
			break;
		case PackedList:
			convertToSetFromList();
			set.add(rownum);
			break;
		case BitSet:
			bits.set(rownum);
		case List:
			break;
		}
	}

	public final int getFirstRowNum()
	{
		if (baseSet != null)
		{
			RowSetIterator rsit = getIterator();
			return rsit.getNextRow();
		}
		switch (type)
		{
		case Set:
			TIntIterator tint = set.iterator();
			if (tint.hasNext())
				return set.iterator().next();
			else
				return -1;
		case ArrayList:
			if (list.size() > 0)
				return list.get(0);
			else
				return -1;
		case PackedList:
			if (packedList.size() > 0)
				return packedList.getDifference(0);
			else
				return -1;
		case List:
			return rowList[0];
		case Range:
			if (range.stop >= range.start)
				return range.start;
			else
				return -1;
		case BitSet:
			return bits.nextSetBit(0);
		}
		return -1;
	}

	private class SetEntry implements Comparable<SetEntry>
	{
		int size;
		RowSet set;

		public SetEntry(int size, RowSet set)
		{
			this.size = size;
			this.set = set;
		}

		@Override
		public int compareTo(SetEntry o)
		{
			if (size < o.size)
				return -1;
			if (size == o.size)
				return 0;
			return 1;
		}
	}

	public final RowSet getIntersection(RowSet newset, int maxValue)
	{
		if (baseSet == null && newset.baseSet == null)
		{
			if (size() <= newset.size())
				return getBasicIntersection(newset, maxValue);
			else
				return newset.getBasicIntersection(this, maxValue);
		}
		ArrayList<SetEntry> setlist = new ArrayList<SetEntry>();
		setlist.add(new SetEntry(size(), this));
		if (baseSet != null)
			setlist.add(new SetEntry(baseSet.size(), baseSet));
		setlist.add(new SetEntry(newset.size(), newset));
		if (newset.baseSet != null)
			setlist.add(new SetEntry(newset.baseSet.size(), newset.baseSet));
		Collections.sort(setlist);
		RowSet curSet = null;
		RowSet lastSet = null;
		for (SetEntry entry : setlist)
		{
			if (entry.set == lastSet)
				continue;
			if (curSet == null)
			{
				curSet = new RowSet(entry.set, false);
				lastSet = entry.set;
			} else
			{
				curSet = curSet.getBasicIntersection(new RowSet(entry.set, false), maxValue);
				if (curSet.size() == 0)
					break;
				lastSet = entry.set;
			}
		}
		return curSet;
	}

	private final RowSet getBasicIntersection(RowSet newset, int maxValue)
	{
		switch (type)
		{
		case PackedList:
			int size = packedList.size();
			if (size == 1)
			{
				if (newset.contains(packedList.getDifference(0)))
					return this;
				else
					return getEmptySet();
			}
			TIntHashSet dupeset = new TIntHashSet();
			int curRow = 0;
			int lastval = 0;
			switch (newset.type)
			{
			case Set:
				while (curRow < size)
				{
					int val = packedList.getDifference(curRow++);
					if (curRow == 0)
					{
						lastval = val;
					} else
					{
						lastval = lastval + val;
					}
					if (newset.set.contains(lastval))
						dupeset.add(lastval);
				}
				return new RowSet(dupeset);
			case Range:
				while (curRow < size)
				{
					int val = packedList.getDifference(curRow++);
					if (curRow == 0)
					{
						lastval = val;
					} else
					{
						lastval = lastval + val;
					}
					if (newset.range.start <= lastval && newset.range.stop >= lastval)
						dupeset.add(lastval);
				}
				return new RowSet(dupeset);
			case PackedList:
				while (curRow < size)
				{
					int val = packedList.getDifference(curRow++);
					if (curRow == 0)
					{
						lastval = val;
					} else
					{
						lastval = lastval + val;
					}
					if (newset.packedList.contains(lastval))
						dupeset.add(lastval);
				}
				return new RowSet(dupeset);
			case BitSet:
				while (curRow < size)
				{
					int val = packedList.getDifference(curRow++);
					if (curRow == 0)
					{
						lastval = val;
					} else
					{
						lastval = lastval + val;
					}
					if (newset.bits.get(lastval))
						dupeset.add(lastval);
				}
				return new RowSet(dupeset);
			default:
				while (curRow < size)
				{
					int val = packedList.getDifference(curRow++);
					if (curRow == 0)
					{
						lastval = val;
					} else
					{
						lastval = lastval + val;
					}
					if (newset.contains(lastval))
						dupeset.add(lastval);
				}
				return new RowSet(dupeset);
			}
		case Set:
			switch (newset.type)
			{
			case Set:
				dupeset = new TIntHashSet(set);
				dupeset.retainAll(newset.set);
				return new RowSet(dupeset);
			case Range:
				TIntArrayList tlist = new TIntArrayList(Math.min(set.size(), newset.range.stop - newset.range.start + 1));
				for (int i = newset.range.start; i <= newset.range.stop; i++)
				{
					if (set.contains(i))
						tlist.add(i);
				}
				return new RowSet(tlist);
			case BitSet:
				BitSet newbits = new BitSet(newset.bits.size());
				TIntIterator tintit = set.iterator();
				while (tintit.hasNext())
				{
					int i = tintit.next();
					if (newset.bits.get(i))
						newbits.set(i);
				}
				return new RowSet(newbits);
			case ArrayList:
				dupeset = new TIntHashSet(set.size());
				tintit = set.iterator();
				while (tintit.hasNext())
				{
					int i = tintit.next();
					if (newset.list.binarySearch(i) >= 0)
						dupeset.add(i);
				}
				return new RowSet(dupeset);
			case PackedList:
				dupeset = new TIntHashSet();
				size = newset.packedList.size();
				int lastVal = 0;
				for (int i = 0; i < size; i++)
				{
					if (i == 0)
						lastVal = newset.packedList.getDifference(i);
					else
						lastVal = lastVal + newset.packedList.getDifference(i);
					if (set.contains(lastVal))
						dupeset.add(lastVal);
				}
				return new RowSet(dupeset);
			case List:
				break;
			default:
				break;
			}
			break;
		case Range:
			TIntArrayList tlist = new TIntArrayList(Math.min(range.stop - range.start + 1, newset.size()));
			for (int i = range.start; i <= range.stop; i++)
			{
				if (newset.set != null && newset.set.contains(i))
					tlist.add(i);
				else if (newset.range != null && newset.range.start <= i && newset.range.stop >= i)
					tlist.add(i);
				else if (newset.list != null && newset.list.contains(i))
					tlist.add(i);
			}
			return new RowSet(tlist);
		case ArrayList:
			int sz = list.size();
			if (sz == 1)
			{
				if (newset.contains(list.get(0)))
					return this;
				else
					return getEmptySet();
			}
			switch (newset.type)
			{
			case BitSet:
				tlist = new TIntArrayList(sz);
				for (int i = 0; i < sz; i++)
				{
					int val = list.get(i);
					if (newset.bits.get(list.get(i)))
						tlist.add(val);
				}
				break;
			case Set:
				if (sz < 100000 || maxValue < 0)
				{
					TIntSet tset = getNewSetFromList();
					tset.retainAll(newset.set);
					return new RowSet(tset);
				} else
				{
					BitSet tbits = getNewBitSet(maxValue);
					BitSet tbits2 = newset.getNewBitSet(maxValue);
					tbits.and(tbits2);
					return new RowSet(tbits);
				}
			case Range:
				tlist = new TIntArrayList(Math.min(sz, newset.size()));
				int start = newset.range.start;
				int stop = newset.range.stop;
				for (int i = 0; i < sz; i++)
				{
					int val = list.get(i);
					if (start <= val && stop >= val)
						tlist.add(val);
				}
				break;
			default:
				tlist = new TIntArrayList(Math.min(sz, newset.size()));
				for (int i = 0; i < sz; i++)
				{
					int val = list.get(i);
					if (newset.contains(val))
						tlist.add(val);
				}
				break;
			}
			return new RowSet(tlist);
		case List:
			sz = rowList.length;
			if (sz == 1)
			{
				if (newset.contains(rowList[0]))
					return this;
				else
					return getEmptySet();
			}
			switch (newset.type)
			{
			case BitSet:
				tlist = new TIntArrayList(sz);
				for (int i = 0; i < sz; i++)
				{
					int val = rowList[i];
					if (newset.bits.get(val))
						tlist.add(val);
				}
				break;
			case Set:
				if (sz < 100000 || maxValue < 0)
				{
					TIntSet tset = getNewSetFromList();
					tset.retainAll(newset.set);
					return new RowSet(tset);
				} else
				{
					BitSet tbits = getNewBitSet(maxValue);
					BitSet tbits2 = newset.getNewBitSet(maxValue);
					tbits.and(tbits2);
					return new RowSet(tbits);
				}
			case Range:
				tlist = new TIntArrayList(Math.min(sz, newset.size()));
				int start = newset.range.start;
				int stop = newset.range.stop;
				for (int i = 0; i < sz; i++)
				{
					int val = rowList[i];
					if (start <= val && stop >= val)
						tlist.add(val);
				}
				return new RowSet(tlist);
			default:
				tlist = new TIntArrayList(Math.min(sz, newset.size()));
				for (int i = 0; i < sz; i++)
				{
					int val = type == RowSetType.ArrayList ? list.get(i) : rowList[i];
					if (newset.contains(val))
						tlist.add(val);
				}
				break;
			}
			return new RowSet(tlist);
		case BitSet:
			switch (newset.type)
			{
			case BitSet:
				BitSet newbits = (BitSet) bits.clone();
				newbits.and(newset.bits);
				return new RowSet(newbits);
			case ArrayList:
				break;
			case List:
				break;
			case PackedList:
				break;
			case Range:
				newbits = (BitSet) bits.clone();
				if (newset.range.start > 0)
					newbits.clear(0, newset.range.start - 1);
				if (newset.range.stop < newbits.length())
					newbits.clear(newset.range.stop + 1, newbits.length());
				return new RowSet(newbits);
			case Set:
				newbits = new BitSet(bits.size());
				TIntIterator tintit = newset.set.iterator();
				while (tintit.hasNext())
				{
					int i = tintit.next();
					if (bits.get(i))
						newbits.set(i);
				}
				return new RowSet(newbits);
			default:
				break;
			}
		}
		return new RowSet();
	}

	public final boolean contains(int rownum)
	{
		if (baseSet != null)
			if (!baseSet.contains(rownum))
				return false;
		switch (type)
		{
		case BitSet:
			return bits.get(rownum);
		case Range:
			return range.start <= rownum && range.stop >= rownum;
		case ArrayList:
			return list.binarySearch(rownum) >= 0;
		case List:
			return memdb.util.Util.binarySearch(rowList, 0, rowList.length, rownum) >= 0;
		case Set:
			return set.contains(rownum);
		case PackedList:
			return packedList.contains(rownum);
		}
		return false;
	}

	public final boolean isEmpty()
	{
		if (baseSet != null && baseSet.isEmpty())
			return true;
		switch (type)
		{
		case Range:
			return range.stop < range.start;
		case Set:
			return set.size() == 0;
		case List:
			return rowList.length == 0;
		case ArrayList:
			return list.size() == 0;
		case PackedList:
			return packedList.size() == 0;
		case BitSet:
			return bits.cardinality() == 0;
		}
		return true;
	}

	public final int size()
	{
		switch (type)
		{
		case Range:
			return range.stop - range.start + 1;
		case Set:
			return set.size();
		case ArrayList:
			return list.size();
		case PackedList:
			return packedList.size();
		case List:
			return rowList.length;
		case BitSet:
			return bits.cardinality();
		}
		return 0;
	}

	/**
	 * Constrict the rowset to the top n values
	 * 
	 * @param n
	 */
	public void setTopN(int n)
	{
		switch (type)
		{
		case Range:
			range.stop = Math.min(range.stop, range.start + n);
			break;
		case Set:
			TIntSet newset = new TIntHashSet(n);
			TIntIterator tintit = set.iterator();
			int count = n;
			while (count > 0 && tintit.hasNext())
			{
				newset.add(tintit.next());
				count--;
			}
			set = newset;
			break;
		case List:
			if (n <= rowList.length)
			{
				int[] newRowList = new int[n];
				for (int i = 0; i < n; i++)
					newRowList[i] = rowList[i];
				rowList = newRowList;
			}
			break;
		case ArrayList:
			TIntArrayList newlist = new TIntArrayList(n);
			tintit = list.iterator();
			count = n;
			while (count > 0 && tintit.hasNext())
			{
				newlist.add(tintit.next());
				count--;
			}
			list = newlist;
			break;
		case PackedList:
			if (n <= rowList.length)
			{
				list = packedList.getIntArrayList(n);
				packedList = null;
				type = RowSetType.ArrayList;
			}
			break;
		case BitSet:
			break;
		default:
			break;
		}
	}

	public final RowSetIterator getIterator()
	{
		switch (type)
		{
		case Set:
			return new IntIteratorRowSetIterator(set.iterator(), baseSet);
		case ArrayList:
			return new ListRowSetIterator(list, baseSet);
		case List:
			return new RowListRowSetIterator(rowList, baseSet);
		case Range:
			return new RangeRowSetIterator(range, baseSet);
		case BitSet:
			return new BitSetRowSetIterator(bits, baseSet);
		case PackedList:
			return new PackedListRowSetIterator(packedList, baseSet, range);
		}
		return null;
	}

	public RowSet[] splitRowSet(int numRowSets)
	{
		RowSet[] result = new RowSet[numRowSets];
		int lastrow = 0;
		switch (type)
		{
		case Range:
			int increment = (range.stop - range.start) / numRowSets;
			for (int i = 0; i < numRowSets; i++)
			{
				int newrow = lastrow + increment;
				if (newrow > range.stop)
					newrow = range.stop;
				if (lastrow >= newrow)
				{
					if (lastrow == newrow)
						result[i] = new RowSet(lastrow, newrow);
					else
						result[i] = new RowSet();
					if (newrow >= range.stop)
						break;
					else
					{
						lastrow = newrow + 1;
						continue;
					}
				}
				if (i == numRowSets - 1)
				{
					newrow = range.stop;
				}
				result[i] = new RowSet(lastrow, newrow);
				lastrow = newrow + 1;
			}
			break;
		case BitSet:
			for (int i = 0; i < numRowSets; i++)
			{
				result[i] = new RowSet(new BitSet(bits.size()));
			}
			int curSet = 0;
			for (int i = bits.nextSetBit(0); i >= 0; i = bits.nextSetBit(i + 1))
			{
				result[curSet++].bits.set(i);
				if (curSet >= numRowSets)
					curSet = 0;
			}
			break;
		case Set:
		case ArrayList:
		case PackedList:
			int size = 0;
			if (type == RowSetType.Set)
				size = set.size();
			else if (type == RowSetType.ArrayList)
				size = list.size();
			else
				size = packedList.size();
			if (size == 0)
			{
				result[0] = RowSet.getEmptySet();
				return result;
			}
			increment = Math.max(1, size / numRowSets);
			TIntIterator it = null;
			if (type == RowSetType.Set)
				it = set.iterator();
			for (int i = 0; i < numRowSets; i++)
			{
				int newrow = lastrow + increment;
				if (newrow > size)
					break;
				if (i == numRowSets - 1)
				{
					newrow = size;
					if (newrow < lastrow)
					{
						result[i] = new RowSet(0);
						break;
					}
				}
				TIntArrayList newset = new TIntArrayList(newrow - lastrow);
				if (type == RowSetType.Set)
				{
					for (int j = lastrow; j < newrow; j++)
					{
						try
						{
							newset.add(it.next());
						} catch (NoSuchElementException ex)
						{
							break;
						}
					}
				} else
				{
					TIntArrayList newlist = null;
					if (type == RowSetType.PackedList)
						newlist = packedList.getIntArrayList(newrow);
					for (int j = lastrow; j < newrow; j++)
					{
						try
						{
							if (type == RowSetType.ArrayList)
								newset.add(list.get(j));
							else
								newset.add(newlist.get(j));
						} catch (ArrayIndexOutOfBoundsException ex)
						{
							break;
						}
					}
				}
				result[i] = new RowSet(newset);
				lastrow = newrow;
			}
			break;
		case List:
			break;
		}
		return result;
	}

	public final boolean isNull()
	{
		return isNull;
	}

	public final void setNull(boolean isNull)
	{
		this.isNull = isNull;
	}

	public String toString()
	{
		switch (type)
		{
		case Set:
			return "Set: " + set.size();
		case List:
			return "List: " + rowList.length;
		case ArrayList:
			return "ArrayList: " + list.size();
		case PackedList:
			return "PackedList: " + packedList.size();
		case Range:
			return "Range: " + range.start + "-" + range.stop;
		case BitSet:
			return "BitSet: " + bits.size();
		}
		return "Unknown RowSet Type";
	}

	/**
	 * Returns whether doing a "contains" lookup of a value is fast
	 * 
	 * @return
	 */
	public boolean isFastLookupType()
	{
		switch (type)
		{
		case Set:
			return true;
		case List:
			return true;
		case ArrayList:
			return true;
		case PackedList:
			return false;
		case Range:
			return true;
		case BitSet:
			return true;
		}
		return false;
	}

	public RowSet getBaseSet()
	{
		return baseSet;
	}

	public void setBaseSet(RowSet baseSet)
	{
		this.baseSet = baseSet;
	}

	public RowRange getRange()
	{
		switch (type)
		{
		case Range:
			return range;
		case ArrayList:
			break;
		case BitSet:
			break;
		case List:
			break;
		case PackedList:
			break;
		case Set:
			break;
		default:
			break;
		}
		return null;
	}
}
