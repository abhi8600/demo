package memdb.rowset;

import gnu.trove.list.array.TIntArrayList;

public class ListRowSetIterator extends RowSetIterator
{
	private int curRow = 0;
	private TIntArrayList list;
	private int size;
	private RowSet baseSet;

	public ListRowSetIterator(TIntArrayList list, RowSet baseSet)
	{
		this.list = list;
		this.size = list.size();
		this.baseSet = baseSet;
	}

	@Override
	public synchronized final int getNextRow()
	{
		if (baseSet == null)
		{
			if (curRow < size)
				return list.get(curRow++);
			return -1;
		}
		while (true)
		{
			if (curRow >= size)
				return -1;
			int val = list.get(curRow++);
			if (baseSet.contains(val))
				return val;
		}
	}
}
