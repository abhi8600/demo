package memdb.rowset;

import java.util.BitSet;

public class BitSetRowSetIterator extends RowSetIterator
{
	private BitSet bits;
	private int curRow;
	private RowSet baseSet;

	public BitSetRowSetIterator(BitSet bits, RowSet baseSet)
	{
		this.bits = bits;
		this.curRow = bits.nextSetBit(0);
		this.baseSet = baseSet;
	}

	@Override
	public final synchronized int getNextRow()
	{
		if (baseSet == null)
		{
			if (curRow < 0)
				return -1;
			int result = curRow;
			curRow = bits.nextSetBit(curRow + 1);
			return result;
		}
		while (true)
		{
			if (curRow < 0)
				return -1;
			int result = curRow;
			curRow = bits.nextSetBit(curRow + 1);
			if (baseSet.contains(result))
				return result;
		}
	}
}
