package memdb.rowset;

public class RowListRowSetIterator extends RowSetIterator
{
	private int[] rowList;
	private int curRow = 0;
	private RowSet baseSet;

	public RowListRowSetIterator(int[] rowList, RowSet baseSet)
	{
		this.rowList = rowList;
		this.baseSet = baseSet;
	}

	@Override
	public final synchronized int getNextRow()
	{
		if (baseSet == null)
		{
			if (curRow >= rowList.length)
				return -1;
			return rowList[curRow++];
		}
		while (true)
		{
			if (curRow >= rowList.length)
				return -1;
			int val = rowList[curRow++];
			if (baseSet.contains(val))
				return val;
		}
	}
}
