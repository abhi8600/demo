package memdb.rowset;

import memdb.indexlist.DifferencePackedIntegerList;
import memdb.sql.RowRange;

public class PackedListRowSetIterator extends RowSetIterator
{
	private int curRow = 0;
	private DifferencePackedIntegerList list;
	private int size;
	private int lastval;
	private RowSet baseSet;
	private int start = -1;
	private int stop;

	public PackedListRowSetIterator(DifferencePackedIntegerList list, RowSet baseSet, RowRange range)
	{
		this.list = list;
		this.size = list.size();
		this.baseSet = baseSet;
		if (range != null)
		{
			this.start = range.start;
			this.stop = range.stop;
		}
	}

	public PackedListRowSetIterator(DifferencePackedIntegerList list, int start, int stop)
	{
		this.list = list;
		this.size = list.size();
		this.start = start;
		this.stop = stop;
	}

	@Override
	public final synchronized int getNextRow()
	{
		if (start >= 0)
		{
			while (true)
			{
				if (curRow >= size)
					return -1;
				int result = list.getDifference(curRow++);
				if (curRow == 0)
				{
					lastval = result;
				} else
				{
					result = lastval + result;
					lastval = result;
				}
				if (result > stop)
				{
					return -1;
				}
				if (result >= start)
					return result;
			}
		} else if (baseSet == null)
		{
			if (curRow < size)
			{
				int val = list.getDifference(curRow++);
				if (curRow == 0)
				{
					lastval = val;
					return val;
				} else
				{
					lastval = lastval + val;
					return lastval;
				}
			}
			return -1;
		} else
		{
			while (true)
			{
				if (curRow >= size)
					return -1;
				int result = list.getDifference(curRow++);
				if (curRow == 0)
				{
					lastval = result;
				} else
				{
					result = lastval + result;
					lastval = result;
				}
				if (baseSet.contains(result))
					return result;
			}
		}
	}
}
