/**
 * 
 */
package memdb.rowset;


/**
 * @author bpeters
 * 
 */
public abstract class RowSetIterator
{
	/**
	 * Gets the next row number in the iterator. Returns -1 if no more.
	 * 
	 * @return
	 */
	public abstract int getNextRow();
}
