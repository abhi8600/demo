package memdb.rowset;

import memdb.sql.RowRange;

public class RangeRowSetIterator extends RowSetIterator
{
	private int curRow = 0;
	private RowRange range;
	private RowSet baseSet;

	public RangeRowSetIterator(RowRange range, RowSet baseSet)
	{
		this.range = range;
		curRow = range.start;
		this.baseSet = baseSet;
	}

	@Override
	public final synchronized int getNextRow()
	{
		if (baseSet == null)
		{
			if (curRow > range.stop)
				return -1;
			return curRow++;
		}
		while (true)
		{
			if (curRow > range.stop)
				return -1;
			int val = curRow++;
			if (baseSet.contains(val))
				return val;
		}
	}
}
