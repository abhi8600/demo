/**
 * 
 */
package memdb.rowset;

import gnu.trove.iterator.TIntIterator;

/**
 * @author bpeters
 * 
 */
public class IntIteratorRowSetIterator extends RowSetIterator
{
	private TIntIterator tintit;
	private RowSet baseSet;

	public IntIteratorRowSetIterator(TIntIterator tintit, RowSet baseSet)
	{
		this.tintit = tintit;
		this.baseSet = baseSet;
	}

	@Override
	public final synchronized int getNextRow()
	{
		if (baseSet == null)
		{
			if (tintit.hasNext())
			{
				return tintit.next();
			}
			return -1;
		}
		while (true)
		{
			if (tintit.hasNext())
			{
				int val = tintit.next();
				if (baseSet.contains(val))
					return val;
			} else
				return -1;
		}
	}
}
