/**
 * DBDriver - This class implements the java.sql.Driver interface and acts as a
 * Client driver. It communicates with the Remote Driver.
 * 
 * The driver URL is jdbc:DBDriver: + <Remote server name/IP Address>.
 * 
 * Any Java program can use this driver for JDBC purpose by specifying 
 * this URL format.
 */
package memdb.jdbcclient;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import memdb.Database;
import memdb.Main;
import memdb.jdbcserver.IRemoteConnection;
import memdb.jdbcserver.IRemoteDriver;
import memdb.jdbcserver.RemoteConnectionImpl;

public class DBDriver implements java.sql.Driver
{
	// Remote driver
	static IRemoteDriver remoteDriver = null;
	// Driver URL prefix.
	private static final String URL_PREFIX = "jdbc:memdb://";
	private static final String INPROCESS_PREFIX = "inprocess://";
	private static final int MAJOR_VERSION = 1;
	private static final int MINOR_VERSION = 0;
	private static Database db;
	static
	{
		try
		{
			// Register the DBDriver with DriverManager
			DBDriver driverInst = new DBDriver();
			DriverManager.registerDriver(driverInst);
			// System.setSecurityManager(new RMISecurityManager());
		} catch (Exception e)
		{
		}
	}

	/**
	 * It returns the URL prefix for using DBDriver
	 */
	public static String getURLPrefix()
	{
		return URL_PREFIX;
	}

	/**
	 * This method create a remote connection and then returns the DBConnection object holding a Remote Connection
	 */
	public Connection connect(String url, Properties loginProp) throws SQLException
	{
		DBConnection localConInstance = null;
		if (url.toLowerCase().startsWith(INPROCESS_PREFIX))
		{
			if (db == null)
				db = Main.startDatabase(url.substring(INPROCESS_PREFIX.length()), 20000, 30000);
			try
			{
				return new DBConnection(new RemoteConnectionImpl(db), this, url, loginProp);
			} catch (RemoteException e)
			{
				throw new SQLException(e.getMessage());
			}
		}
		if (acceptsURL(url))
		{
			try
			{
				// Create the local DBConnection holding remote Connenction
				localConInstance = new DBConnection(connectRemote(url, loginProp), this, url, loginProp);
			} catch (RemoteException e)
			{
				throw new SQLException(e.getMessage());
			}
		}
		return (Connection) localConInstance;
	}

	public IRemoteConnection connectRemote(String url, Properties loginProp) throws SQLException
	{
		try
		{
			// extract the remote server location coming with URL
			String serverName = url.substring(URL_PREFIX.length(), url.length());
			// connect to remote server only if not already connected
			connectRemote(serverName);
			// Get login properties
			String username = loginProp.getProperty("username");
			String password = loginProp.getProperty("password");
			// Get the remote Connection
			IRemoteConnection remoteConInstance = (IRemoteConnection) remoteDriver.getConnection(username, password);
			return remoteConInstance;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		} catch (Exception ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	/**
	 * This method makes the one time connection to the RMI server
	 */
	private void connectRemote(String serverName) throws Exception
	{
		try
		{
			remoteDriver = (IRemoteDriver) Naming.lookup("//" + serverName + "/MemDB");
		} catch (Exception ex)
		{
			throw ex;
		}
	}

	/**
	 * This method returns true if the given URL starts with the DBDriver url. It is called by DriverManager.
	 */
	public boolean acceptsURL(String url) throws SQLException
	{
		return url.startsWith(URL_PREFIX);
	}

	public int getMajorVersion()
	{
		return MAJOR_VERSION;
	}

	public int getMinorVersion()
	{
		return MINOR_VERSION;
	}

	public java.sql.DriverPropertyInfo[] getPropertyInfo(String url, Properties loginProps) throws SQLException
	{
		return new DriverPropertyInfo[0];
	}

	public boolean jdbcCompliant()
	{
		return false;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException
	{
		// TODO Auto-generated method stub
		return null;
	}
}