/**
 * DBStatement - It implements java.sql.Statement. This class acts as the client 
 * Statement. It communicates with the remote Statement.
 */
package memdb.jdbcclient;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import memdb.jdbcserver.IRemoteResultSet;
import memdb.jdbcserver.IRemoteStatement;

public class DBStatement implements java.sql.Statement, java.sql.PreparedStatement
{
	// Remote Statement
	private IRemoteStatement remoteStmt;
	private DBResultSet localRsInstance = null;
	private Object[] parameters;
	private DBConnection conn;

	/**
	 * Constructor for creating the DBStatement
	 */
	public DBStatement(IRemoteStatement stmt, DBConnection conn)
	{
		this.remoteStmt = stmt;
		this.conn = conn;
	}

	public void prepare()
	{
		parameters = new Object[20];
	}

	/**
	 * This method executes the SQL query via Remote Statement and then returns the DBResultSet holding remote
	 * ResultSet.
	 */
	public ResultSet executeQuery(String sqlQuery) throws SQLException
	{
		try
		{
			IRemoteResultSet remoteRsInstance = (IRemoteResultSet) remoteStmt.executeQuery(sqlQuery);
			if (remoteRsInstance == null)
				return null;
			localRsInstance = new DBResultSet(remoteRsInstance, conn);
			return (ResultSet) localRsInstance;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	/**
	 * This method executes the SQL data write/modify statement via Remote Statement
	 */
	public int executeUpdate(String sqlQuery) throws SQLException
	{
		try
		{
			int updateCount = remoteStmt.executeUpdate(sqlQuery);
			return updateCount;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	/**
	 * This method closes the Remote Statement
	 */
	public void close() throws SQLException
	{
		try
		{
			remoteStmt.close();
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	// Not supported methods
	public int getMaxFieldSize() throws SQLException
	{
		return Integer.MAX_VALUE;
	}

	public void setMaxFieldSize(int max) throws SQLException
	{
	}

	public int getMaxRows() throws SQLException
	{
		return Integer.MAX_VALUE;
	}

	public void setMaxRows(int max) throws SQLException
	{
	}

	public void setEscapeProcessing(boolean enable) throws SQLException
	{
		throw (new SQLException("setEscapeProcessing Not Supported"));
	}

	public int getQueryTimeout() throws SQLException
	{
		return Integer.MAX_VALUE;
	}

	public void setQueryTimeout(int seconds) throws SQLException
	{
	}

	public void cancel() throws SQLException
	{
		throw (new SQLException("cancel Not Supported"));
	}

	public SQLWarning getWarnings() throws SQLException
	{
		return null;
	}

	public void clearWarnings() throws SQLException
	{
	}

	public void setCursorName(String name) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean execute(String sql) throws SQLException
	{
		return executeQuery(sql) != null;
	}

	public ResultSet getResultSet() throws SQLException
	{
		return (ResultSet) localRsInstance;
	}

	public int getUpdateCount() throws SQLException
	{
		return 0;
	}

	public boolean getMoreResults() throws SQLException
	{
		throw (new SQLException("getMoreResults Not Supported"));
	}

	public void setFetchDirection(int direction) throws SQLException
	{
	}

	public int getFetchDirection() throws SQLException
	{
		return ResultSet.FETCH_FORWARD;
	}

	public void setFetchSize(int rows) throws SQLException
	{
	}

	public int getFetchSize() throws SQLException
	{
		return Integer.MAX_VALUE;
	}

	public int getResultSetConcurrency() throws SQLException
	{
		throw (new SQLException("getResultSetConcurrency Not Supported"));
	}

	public int getResultSetType() throws SQLException
	{
		throw (new SQLException("getResultSetType Not Supported"));
	}

	public void addBatch(String sql) throws SQLException
	{
		throw (new SQLException("addBatch Not Supported"));
	}

	public void clearBatch() throws SQLException
	{
		throw (new SQLException("clearBatch Not Supported"));
	}

	public int[] executeBatch() throws SQLException
	{
		throw (new SQLException("executeBatch Not Supported"));
	}

	public Connection getConnection() throws SQLException
	{
		throw (new SQLException("getConnection Not Supported"));
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException
	{
		throw (new SQLException("isWrapperFor Not Supported"));
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException
	{
		throw (new SQLException("unwrap Not Supported"));
	}

	@Override
	public boolean execute(String arg0, int arg1) throws SQLException
	{
		throw (new SQLException("execute Not Supported"));
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException
	{
		throw (new SQLException("execute Not Supported"));
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException
	{
		throw (new SQLException("execute Not Supported"));
	}

	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException
	{
		throw (new SQLException("executeUpdate Not Supported"));
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException
	{
		throw (new SQLException("executeUpdate Not Supported"));
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException
	{
		throw (new SQLException("executeUpdate Not Supported"));
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException
	{
		throw (new SQLException("getGeneratedKeys Not Supported"));
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException
	{
		throw (new SQLException("getMoreResults Not Supported"));
	}

	@Override
	public int getResultSetHoldability() throws SQLException
	{
		throw (new SQLException("getResultSetHoldability Not Supported"));
	}

	@Override
	public boolean isClosed() throws SQLException
	{
		throw (new SQLException("isClosed Not Supported"));
	}

	@Override
	public boolean isPoolable() throws SQLException
	{
		throw (new SQLException("isPoolable Not Supported"));
	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException
	{
		throw (new SQLException("setPoolable Not Supported"));
	}

	@Override
	public void closeOnCompletion() throws SQLException
	{
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException
	{
		return false;
	}

	@Override
	public ResultSet executeQuery() throws SQLException
	{
		try
		{
			IRemoteResultSet remoteRsInstance = (IRemoteResultSet) remoteStmt.executeQuery(parameters);
			if (remoteRsInstance == null)
				throw new SQLException("No resultset returned");
			localRsInstance = new DBResultSet(remoteRsInstance, conn);
			return (ResultSet) localRsInstance;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	@Override
	public int executeUpdate() throws SQLException
	{
		try
		{
			int updateCount = remoteStmt.executeUpdate(parameters);
			return updateCount;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = null;
	}

	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setShort(int parameterIndex, short x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setInt(int parameterIndex, int x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setLong(int parameterIndex, long x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setString(int parameterIndex, String x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setBytes(int parameterIndex, byte[] x) throws SQLException
	{
	}

	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException
	{
	}

	@Override
	public void clearParameters() throws SQLException
	{
		for (int i = 0; i < parameters.length; i++)
			parameters[i] = null;
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public boolean execute() throws SQLException
	{
		try
		{
			remoteStmt.executeUpdate(parameters);
			return true;
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	@Override
	public void addBatch() throws SQLException
	{
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException
	{
	}

	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException
	{
	}

	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException
	{
	}

	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException
	{
	}

	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException
	{
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException
	{
		return null;
	}

	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = null;
	}

	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException
	{
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException
	{
		return null;
	}

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException
	{
	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException
	{
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException
	{
	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException
	{
	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException
	{
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException
	{
	}

	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException
	{
		if (parameterIndex <= parameters.length && parameterIndex >= 1)
			parameters[parameterIndex - 1] = x;
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException
	{
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException
	{
	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException
	{
	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException
	{
	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException
	{
	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException
	{
	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException
	{
	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException
	{
	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException
	{
	}
}
