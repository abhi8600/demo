/**
 * DBResultSet - It implements java.sql.ResultSet. This class acts as the client 
 * ResultSet. It communicates with the remote ResultSet.
 */
package memdb.jdbcclient;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.sql.rowset.RowSetMetaDataImpl;

import memdb.jdbcserver.IRemoteResultSet;
import memdb.sql.QueryResultRow;

public class DBResultSet implements java.sql.ResultSet
{
	// Remote ResultSet
	private IRemoteResultSet remoteResultSet;
	// The current ResultSet data row
	private Object[] row;
	private Map<String, Integer> colMap = null;
	private String[] colNames = null;
	private int[] colTypes = null;
	private boolean wasNull;
	private QueryResultRow[] data = null;
	private int curRow = 0;
	private TimeZone defaulttz = TimeZone.getDefault();
	private TimeZone databasetz;

	/**
	 * Constructor for creating the DBResultSet with IRemoteResultSet
	 */
	public DBResultSet(IRemoteResultSet rsInstance, DBConnection conn) throws SQLException
	{
		remoteResultSet = rsInstance;
		this.databasetz = conn.getServerTimeZone();
		try
		{
			colNames = remoteResultSet.getColumnNames();
			colTypes = remoteResultSet.getColumnTypes();
		} catch (RemoteException e)
		{
			throw new SQLException(e.getMessage());
		}
		colMap = new HashMap<String, Integer>();
		for (int i = 0; i < colNames.length; i++)
			colMap.put(colNames[i], i);
	}

	/**
	 * This method gets the Next data row from the remote ResultSet It return false if all the data has already been
	 * iterated
	 */
	public boolean next() throws SQLException
	{
		if (data == null)
		{
			try
			{
				data = remoteResultSet.getResults(false);
				if (data != null)
				{
					for (int i = 0; i < data.length; i++)
					{
						for (int j = 0; j < data[i].dataRow.length; j++)
						{
							/*
							 * Adjust for the fact that time is kept in GMT on server
							 */
							if (data[i].dataRow[j] instanceof Date)
							{
								long tm = ((Date) data[i].dataRow[j]).getTime();
								tm -= defaulttz.getOffset(tm) - databasetz.getOffset(tm);
								data[i].dataRow[j] = new Date(tm);
							}
						}
					}
				}
			} catch (Exception ex)
			{
				return false;
			}
		}
		if (data == null || data.length == 0)
			return false;
		if (curRow == data.length)
		{
			try
			{
				data = remoteResultSet.getResults(false);
				if (data == null || data.length == 0)
					return false;
			} catch (Exception ex)
			{
				return false;
			}
			curRow = 0;
		}
		row = data[curRow++].dataRow;
		if (row == null)
		{
			return false;
		}
		return true;
	}

	/**
	 * This method closes the remote ResultSet
	 */
	public void close() throws SQLException
	{
		try
		{
			// close the remote ResultSet
			remoteResultSet.close();
		} catch (RemoteException ex)
		{
			throw (new SQLException(ex.getMessage()));
		}
	}

	// getXXX data Methods from the ResultSet
	public String getString(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof String)
			return (String) columnData;
		else if (columnData == null)
		{
			wasNull = true;
			return null;
		} else
		{
			return columnData.toString();
		}
	}

	public String getString(String columnName) throws SQLException
	{
		int index = colMap.get(columnName);
		return getString(index);
	}

	public float getFloat(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Float)
			return (Float) columnData;
		if (columnData instanceof Double)
			return ((Double) columnData).floatValue();
		else if (columnData == null)
		{
			wasNull = true;
			return 0;
		} else
		{
			return Float.valueOf(columnData.toString());
		}
	}

	public float getFloat(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getFloat(columnIndex);
	}

	public int getInt(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Integer)
		{
			int result = (Integer) columnData;
			if (result == Integer.MIN_VALUE)
				wasNull = true;
			return result;
		} else if (columnData instanceof Float)
			return ((Float) columnData).intValue();
		else if (columnData instanceof Double)
			return ((Double) columnData).intValue();
		else if (columnData == null)
		{
			wasNull = true;
			return 0;
		} else
		{
			return Integer.valueOf(columnData.toString());
		}
	}

	public int getInt(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getInt(columnIndex);
	}

	public boolean getBoolean(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Boolean)
			return (Boolean) columnData;
		else if (columnData instanceof Integer)
			return ((Integer) columnData) > 0;
		else if (columnData instanceof Float)
			return ((Float) columnData) > 0;
		else if (columnData instanceof Double)
			return ((Double) columnData) > 0;
		else if (columnData == null)
		{
			wasNull = true;
			return false;
		} else
		{
			return Boolean.valueOf(columnData.toString());
		}
	}

	public boolean getBoolean(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getBoolean(columnIndex);
	}

	public short getShort(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Short)
			return (Short) columnData;
		else if (columnData instanceof Integer)
			return ((Integer) columnData).shortValue();
		else if (columnData instanceof Float)
			return ((Float) columnData).shortValue();
		else if (columnData instanceof Double)
			return ((Double) columnData).shortValue();
		else if (columnData == null)
		{
			wasNull = true;
			return Short.MIN_VALUE;
		} else
		{
			return Short.valueOf(columnData.toString());
		}
	}

	public short getShort(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getShort(columnIndex);
	}

	public long getLong(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Long)
		{
			long result = (Long) columnData;
			if (result == Long.MAX_VALUE)
				wasNull = true;
			return result;
		} else if (columnData instanceof Integer)
			return ((Integer) columnData).longValue();
		else if (columnData instanceof Float)
			return ((Float) columnData).longValue();
		else if (columnData instanceof Double)
			return ((Double) columnData).longValue();
		else if (columnData == null)
		{
			wasNull = true;
			return Long.MAX_VALUE;
		} else
		{
			return Long.valueOf(columnData.toString());
		}
	}

	public long getLong(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getLong(columnIndex);
	}

	public double getDouble(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof Double)
			return (Double) columnData;
		else if (columnData instanceof Integer)
			return ((Integer) columnData).doubleValue();
		else if (columnData instanceof Float)
			return ((Float) columnData).doubleValue();
		else if (columnData instanceof Double)
			return ((Double) columnData).doubleValue();
		else if (columnData == null)
		{
			wasNull = true;
			return Double.NaN;
		} else
		{
			return Double.valueOf(columnData.toString());
		}
	}

	public double getDouble(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getDouble(columnIndex);
	}

	public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public byte[] getBytes(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public java.sql.Date getDate(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof java.sql.Date)
			return (java.sql.Date) columnData;
		else if (columnData instanceof Date)
		{
			long tm = ((Date) columnData).getTime();
			tm -= defaulttz.getOffset(tm) - databasetz.getOffset(tm);
			return new java.sql.Date(tm);
		} else if (columnData == null)
		{
			wasNull = true;
			return null;
		} else
		{
			return java.sql.Date.valueOf(columnData.toString());
		}
	}

	public java.sql.Date getDate(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getDate(columnIndex);
	}

	public java.sql.Time getTime(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof java.util.Date)
		{
			long tm = ((Date) columnData).getTime();
			tm -= defaulttz.getOffset(tm) - databasetz.getOffset(tm);
			return new java.sql.Time(tm);
		} else if (columnData instanceof java.sql.Time)
			return (java.sql.Time) columnData;
		else if (columnData instanceof Timestamp)
			return new java.sql.Time(((Timestamp) columnData).getTime());
		else if (columnData == null)
		{
			wasNull = true;
			return null;
		} else
		{
			return java.sql.Time.valueOf(columnData.toString());
		}
	}

	public java.sql.Time getTime(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getTime(columnIndex);
	}

	public java.sql.Timestamp getTimestamp(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object columnData = row[columnIndex - 1];
		if (columnData instanceof java.util.Date)
		{
			long tm = ((Date) columnData).getTime();
			tm -= defaulttz.getOffset(tm) - databasetz.getOffset(tm);
			return new java.sql.Timestamp(tm);
		} else if (columnData instanceof java.sql.Time)
			return new java.sql.Timestamp(((java.sql.Time) columnData).getTime());
		else if (columnData instanceof Timestamp)
			return new java.sql.Timestamp(((Timestamp) columnData).getTime());
		else if (columnData == null)
		{
			wasNull = true;
			return null;
		} else
		{
			return java.sql.Timestamp.valueOf(columnData.toString());
		}
	}

	public java.sql.Timestamp getTimestamp(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getTimestamp(columnIndex);
	}

	public InputStream getAsciiStream(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public InputStream getUnicodeStream(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public InputStream getBinaryStream(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Object getObject(int columnIndex) throws SQLException
	{
		wasNull = false;
		Object obj = (Object) row[columnIndex - 1];
		if (obj == null)
		{
			wasNull = true;
			return null;
		} else if (obj instanceof Long && ((Long) obj) == Long.MAX_VALUE)
		{
			wasNull = true;
			return null;
		} else if (obj instanceof java.util.Date)
		{
			long tm = ((Date) obj).getTime();
			tm -= defaulttz.getOffset(tm) - databasetz.getOffset(tm);
			return new java.sql.Time(tm);
		}
		return obj;
	}

	public BigDecimal getBigDecimal(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public byte getByte(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public BigDecimal getBigDecimal(String columnName, int scale) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public byte[] getBytes(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Object getObject(String columnName) throws SQLException
	{
		int columnIndex = colMap.get(columnName);
		return getObject(columnIndex);
	}

	public BigDecimal getBigDecimal(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public InputStream getAsciiStream(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public InputStream getUnicodeStream(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public InputStream getBinaryStream(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public SQLWarning getWarnings() throws SQLException
	{
		return null;
	}

	public void clearWarnings() throws SQLException
	{
	}

	public String getCursorName() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public ResultSetMetaData getMetaData() throws SQLException
	{
		RowSetMetaDataImpl rsmd = new RowSetMetaDataImpl();
		rsmd.setColumnCount(colNames.length);
		for (int i = 0; i < colNames.length; i++)
		{
			rsmd.setColumnName(i + 1, colNames[i]);
			rsmd.setColumnType(i + 1, colTypes[i]);
		}
		return rsmd;
	}

	public int findColumn(String columnName) throws SQLException
	{
		return colMap.get(columnName);
	}

	public Reader getCharacterStream(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Reader getCharacterStream(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean isBeforeFirst() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean isAfterLast() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean isFirst() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean isLast() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void beforeFirst() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void afterLast() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean first() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean last() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public int getRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean absolute(int row) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean relative(int rows) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean previous() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void setFetchDirection(int direction) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public int getFetchDirection() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void setFetchSize(int rows) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public int getFetchSize() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public int getType() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public int getConcurrency() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean rowUpdated() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean rowInserted() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public boolean rowDeleted() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateNull(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBoolean(int columnIndex, boolean x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateByte(int columnIndex, byte x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateShort(int columnIndex, short x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateInt(int columnIndex, int x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateLong(int columnIndex, long x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateFloat(int columnIndex, float x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateDouble(int columnIndex, double x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateString(int columnIndex, String x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBytes(int columnIndex, byte[] x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateDate(int columnIndex, java.sql.Date x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateTime(int columnIndex, Time x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateObject(int columnIndex, Object x, int scale) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateObject(int columnIndex, Object x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateNull(String columnName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateByte(String columnName, byte x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateShort(String columnName, short x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateInt(String columnName, int x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateLong(String columnName, long x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateFloat(String columnName, float x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateDouble(String columnName, double x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBigDecimal(String columnName, BigDecimal x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateString(String columnName, String x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBytes(String columnName, byte[] x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateDate(String columnName, java.sql.Date x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateTime(String columnName, Time x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateTimestamp(String columnName, Timestamp x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateAsciiStream(String columnName, InputStream x, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateBinaryStream(String columnName, InputStream x, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateCharacterStream(String columnName, Reader reader, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateObject(String columnName, Object x, int scale) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateObject(String columnName, Object x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void insertRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void deleteRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void refreshRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void cancelRowUpdates() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void moveToInsertRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void moveToCurrentRow() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Statement getStatement() throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public java.sql.Date getDate(int columnIndex, Calendar cal) throws SQLException
	{
		return getDate(columnIndex);
	}

	public java.sql.Date getDate(String columnName, Calendar cal) throws SQLException
	{
		return getDate(columnName);
	}

	public Time getTime(int columnIndex, Calendar cal) throws SQLException
	{
		return getTime(columnIndex);
	}

	public Time getTime(String columnName, Calendar cal) throws SQLException
	{
		return getTime(columnName);
	}

	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException
	{
		return getTimestamp(columnIndex);
	}

	public Timestamp getTimestamp(String columnName, Calendar cal) throws SQLException
	{
		return getTimestamp(columnName);
	}

	public boolean wasNull() throws SQLException
	{
		return wasNull;
	}

	public void updateBoolean(String columnName, boolean x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Ref getRef(int i) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Blob getBlob(int i) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Clob getClob(int i) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Array getArray(int i) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Ref getRef(String colName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Blob getBlob(String colName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Clob getClob(String colName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	public Array getArray(String colName) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException
	{
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException
	{
		return null;
	}

	@Override
	public int getHoldability() throws SQLException
	{
		return 0;
	}

	@Override
	public Reader getNCharacterStream(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public Reader getNCharacterStream(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public NClob getNClob(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public NClob getNClob(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public String getNString(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public String getNString(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public RowId getRowId(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public RowId getRowId(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public SQLXML getSQLXML(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public SQLXML getSQLXML(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public URL getURL(int arg0) throws SQLException
	{
		return null;
	}

	@Override
	public URL getURL(String arg0) throws SQLException
	{
		return null;
	}

	@Override
	public boolean isClosed() throws SQLException
	{
		return false;
	}

	@Override
	public void updateArray(int arg0, Array arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateArray(String arg0, Array arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateAsciiStream(int arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateAsciiStream(String arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateAsciiStream(String arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(int arg0, Blob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(String arg0, Blob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(int arg0, Clob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(String arg0, Clob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(int arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(String arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(int arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateClob(String arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(int arg0, NClob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(String arg0, NClob arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(int arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(String arg0, Reader arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(int arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNClob(String arg0, Reader arg1, long arg2) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNString(int arg0, String arg1) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateNString(String columnLabel, String nString) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateRef(int columnIndex, Ref x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateRef(String columnLabel, Ref x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public byte getByte(int columnIndex) throws SQLException
	{
		throw (new SQLException("Not Supported"));
	}

	@Override
	public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException
	{
		return null;
	}

	@Override
	public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException
	{
		return null;
	}

	@Override
	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException
	{
		return null;
	}

	@Override
	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException
	{
		return null;
	}
}