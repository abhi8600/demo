/**
 * DBConnection - This class implements the java.sql.Connection interface and acts as a
 * Client connection. It communicates with the Remote connection.
 */
package memdb.jdbcclient;

import java.rmi.RemoteException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.Executor;

import memdb.jdbcserver.DBMetaData;
import memdb.jdbcserver.IRemoteConnection;
import memdb.jdbcserver.IRemoteStatement;

public class DBConnection implements Connection
{
	// Remote connection
	private IRemoteConnection remoteConnection;
	private String catalog = "dbo";
	private String url;
	private Properties loginProp;
	private DBDriver driver;
	private TimeZone defaulttz = TimeZone.getDefault();
	private TimeZone serverTimeZone;

	/**
	 * constructor for creating the DBConnection instance with IRemoteConnection
	 */
	public DBConnection(IRemoteConnection remCon, DBDriver driver, String url, Properties loginProp) throws RemoteException
	{
		this.remoteConnection = remCon;
		this.driver = driver;
		this.url = url;
		this.loginProp = loginProp;
		this.serverTimeZone = remCon.getServerTimeZone();
	}

	/**
	 * This method creates a remote statement and then returns DBStatement object holding the remote statement
	 */
	public Statement createStatement() throws SQLException
	{
		try
		{
			IRemoteStatement remStmt = (IRemoteStatement) remoteConnection.createStatement(defaulttz);
			DBStatement localStmtInstance = new DBStatement(remStmt, this);
			return (Statement) localStmtInstance;
		} catch (RemoteException ex)
		{
			// Retry if failed
			remoteConnection = driver.connectRemote(url, loginProp);
			IRemoteStatement remStmt;
			try
			{
				remStmt = (IRemoteStatement) remoteConnection.createStatement(defaulttz);
				DBStatement localStmtInstance = new DBStatement(remStmt, this);
				return (Statement) localStmtInstance;
			} catch (RemoteException e)
			{
				throw (new SQLException("RemoteException:" + e.getMessage()));
			}
		} catch (Exception ex)
		{
			throw (new SQLException("RemoteException:" + ex.getMessage()));
		}
	}

	/**
	 * This method closes the remote Connection
	 */
	public void close() throws SQLException
	{
		try
		{
			remoteConnection.closeConnection();
			remoteConnection = null;
		} catch (RemoteException ex)
		{
			throw ((new SQLException("RemoteException:" + ex.getMessage())));
		}
	}

	// Not supported methods
	public String nativeSQL(String sql) throws SQLException
	{
		throw (new SQLException("nativeSQL Not Supported"));
	}

	public void setAutoCommit(boolean autoCommit) throws SQLException
	{
	}

	public boolean getAutoCommit() throws SQLException
	{
		return true;
	}

	public void commit() throws SQLException
	{
		throw (new SQLException("commit Not Supported"));
	}

	public void rollback() throws SQLException
	{
		throw (new SQLException("rollback Not Supported"));
	}

	public boolean isClosed() throws SQLException
	{
		return remoteConnection == null;
	}

	public DatabaseMetaData getMetaData() throws SQLException
	{
		try
		{
			DBMetaData dbm = remoteConnection.getMetaData();
			return dbm;
		} catch (RemoteException e)
		{
			// retry if issue
			remoteConnection = driver.connectRemote(url, loginProp);
			DBMetaData dbm;
			try
			{
				dbm = remoteConnection.getMetaData();
			} catch (RemoteException e1)
			{
				throw new SQLException("Unable to connect to server to retrieve metadata");
			}
			return dbm;
		}
	}

	public void setReadOnly(boolean readOnly) throws SQLException
	{
	}

	public boolean isReadOnly() throws SQLException
	{
		return false;
	}

	public void setCatalog(String catalog) throws SQLException
	{
		this.catalog = catalog;
	}

	public String getCatalog() throws SQLException
	{
		return catalog;
	}

	public void setTransactionIsolation(int level) throws SQLException
	{
		throw (new SQLException("setTransactionIsolation Not Supported"));
	}

	public int getTransactionIsolation() throws SQLException
	{
		return Connection.TRANSACTION_REPEATABLE_READ;
	}

	public SQLWarning getWarnings() throws SQLException
	{
		return null;
	}

	public void clearWarnings() throws SQLException
	{
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException
	{
		try
		{
			IRemoteStatement remStmt = (IRemoteStatement) remoteConnection.prepareStatement(sql, defaulttz);
			DBStatement localStmtInstance = new DBStatement(remStmt, this);
			localStmtInstance.prepare();
			return (PreparedStatement) localStmtInstance;
		} catch (RemoteException ex)
		{
			// retry if issue
			remoteConnection = driver.connectRemote(url, loginProp);
			IRemoteStatement remStmt;
			try
			{
				remStmt = (IRemoteStatement) remoteConnection.prepareStatement(sql, defaulttz);
				DBStatement localStmtInstance = new DBStatement(remStmt, this);
				localStmtInstance.prepare();
				return (PreparedStatement) localStmtInstance;
			} catch (RemoteException e)
			{
				throw (new SQLException("RemoteException:" + ex.getMessage()));
			}
		} catch (Exception ex)
		{
			throw (new SQLException("RemoteException:" + ex.getMessage()));
		}
	}

	public CallableStatement prepareCall(String sql) throws SQLException
	{
		throw (new SQLException("prepareCall Not Supported"));
	}

	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException
	{
		return prepareStatement(sql);
	}

	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException
	{
		throw (new SQLException("prepareCall Not Supported"));
	}

	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException
	{
		return createStatement();
	}

	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	public Map getTypeMap() throws SQLException
	{
		throw (new SQLException("getTypeMap Not Supported"));
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException
	{
		throw (new SQLException("isWrapperFor Not Supported"));
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException
	{
		throw (new SQLException("unwrap Not Supported"));
	}

	@Override
	public Array createArrayOf(String arg0, Object[] arg1) throws SQLException
	{
		throw (new SQLException("createArrayOf Not Supported"));
	}

	@Override
	public Blob createBlob() throws SQLException
	{
		throw (new SQLException("createBlob Not Supported"));
	}

	@Override
	public Clob createClob() throws SQLException
	{
		throw (new SQLException("createClob Not Supported"));
	}

	@Override
	public NClob createNClob() throws SQLException
	{
		throw (new SQLException("createNClob Not Supported"));
	}

	@Override
	public SQLXML createSQLXML() throws SQLException
	{
		throw (new SQLException("createSQLXML Not Supported"));
	}

	@Override
	public Statement createStatement(int arg0, int arg1, int arg2) throws SQLException
	{
		throw (new SQLException("createStatement Not Supported"));
	}

	@Override
	public Struct createStruct(String arg0, Object[] arg1) throws SQLException
	{
		throw (new SQLException("createStruct Not Supported"));
	}

	@Override
	public Properties getClientInfo() throws SQLException
	{
		throw (new SQLException("getClientInfo Not Supported"));
	}

	@Override
	public String getClientInfo(String arg0) throws SQLException
	{
		throw (new SQLException("getClientInfo Not Supported"));
	}

	@Override
	public int getHoldability() throws SQLException
	{
		throw (new SQLException("getHoldability Not Supported"));
	}

	@Override
	public boolean isValid(int arg0) throws SQLException
	{
		throw (new SQLException("isValid Not Supported"));
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2, int arg3) throws SQLException
	{
		throw (new SQLException("prepareCall Not Supported"));
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1) throws SQLException
	{
		throw (new SQLException("prepareStatement Not Supported"));
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int[] arg1) throws SQLException
	{
		throw (new SQLException("prepareStatement Not Supported"));
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, String[] arg1) throws SQLException
	{
		throw (new SQLException("prepareStatement Not Supported"));
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2, int arg3) throws SQLException
	{
		throw (new SQLException("prepareStatement Not Supported"));
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException
	{
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException
	{
		throw (new SQLException("rollback Not Supported"));
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException
	{
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException
	{
	}

	@Override
	public void setHoldability(int holdability) throws SQLException
	{
	}

	@Override
	public Savepoint setSavepoint() throws SQLException
	{
		throw (new SQLException("setSavepoint Not Supported"));
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException
	{
		throw (new SQLException("setSavepoint Not Supported"));
	}

	@Override
	public void abort(Executor arg0) throws SQLException
	{
	}

	@Override
	public int getNetworkTimeout() throws SQLException
	{
		return 0;
	}

	@Override
	public String getSchema() throws SQLException
	{
		return null;
	}

	@Override
	public void setNetworkTimeout(Executor arg0, int arg1) throws SQLException
	{
	}

	@Override
	public void setSchema(String arg0) throws SQLException
	{
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException
	{
	}

	public TimeZone getServerTimeZone()
	{
		return serverTimeZone;
	}
}
