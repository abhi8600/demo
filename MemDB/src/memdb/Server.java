package memdb;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import memdb.jdbcserver.RemoteDriverImpl;
import memdb.util.Reaper;

import org.apache.log4j.Logger;

public class Server
{
	private static Logger logger = Logger.getLogger(Server.class);
	private Database db;
	private int port;

	public Server(Database db, int port)
	{
		this.setDb(db);
		this.setPort(port);
		System.setSecurityManager(new RMISecurityManager()
		{
			public void checkConnect(String host, int port)
			{
			}

			public void checkConnect(String host, int port, Object context)
			{
			}
		});
		Registry registry = null;
		try
		{
			registry = LocateRegistry.getRegistry("localhost", port);
		} catch (RemoteException e)
		{
			logger.info("No registry found. Not starting jdbc server.");
		}
		if (registry != null)
		{
			try
			{
				// Create an object of RemoteDriverImpl to register with RMI naming service
				// After this the Type-III client driver layer can access this object.
				RemoteDriverImpl serverInstance = new RemoteDriverImpl(db);
				registry.rebind("MemDB", serverInstance);
				logger.info("JDBC server started on port " + port);
			} catch (Exception ex)
			{
				logger.error(ex);
			}
		}
		try
		{
			BackgroundThread bt = new BackgroundThread();
			bt.start();
			BackgroundThread.setRunningThread(bt);
			try
			{
				Class.forName("memdb.storage.AdvancedTable");
				// Add a background task to shrink object cache
				bt.addRunnable((Runnable) Class.forName("memdb.storage.ObjectCache").newInstance());
			} catch (ClassNotFoundException ex)
			{
			}
			// Add a background task to reap deleted objects that the system couldn't immediately release
			bt.addRunnable(new Reaper(db), 60000);
			logger.info("Remote Driver server has started successfully...");
		} catch (Exception ex)
		{
			logger.error(ex);
		}
	}

	public int getPort()
	{
		return port;
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public Database getDb()
	{
		return db;
	}

	public void setDb(Database db)
	{
		this.db = db;
	}
}
