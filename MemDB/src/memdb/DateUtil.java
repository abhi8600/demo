/**
 * $Id: DateUtil.java,v 1.4 2012-08-25 22:06:10 birst\bpeters Exp $
 *
 * Copyright (C) 2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package memdb;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

/**
 * @author sshringeri
 * 
 */
public class DateUtil
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(DateUtil.class);
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DB_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String[] possibleDateFormatPatterns = new String[]
	{
	/*
	 * Parsing a datestring using a SimpleDateFormat format pattern: Begin: excerpt from
	 * http://java.sun.com/j2se/1.4.2/docs/api/java/text/SimpleDateFormat.html Text Based Parsing: For formatting, if
	 * the number of pattern letters is 4 or more, the full form is used; otherwise a short or abbreviated form is used
	 * if available. For parsing, both forms are accepted, independent of the number of pattern letters.
	 * 
	 * Number Based Parsing: For formatting, the number of pattern letters is the minimum number of digits, and shorter
	 * numbers are zero-padded to this amount. For parsing, the number of pattern letters is ignored unless it's needed
	 * to separate two adjacent fields.
	 * 
	 * Date letters parsing: always interpreted as a number. (hence using "d" and "dd" has no difference)
	 * 
	 * Month letters parsing: If the number of pattern letters is 3 or more, the month is interpreted as text;
	 * otherwise, it is interpreted as a number. (hence using "M" and "MM" has no difference, "MMM" will use abbreviated
	 * month format eg."JAN" and "MMMM" will use full textual format ie. "January")
	 * 
	 * Year letters parsing: For formatting, if the number of pattern letters is 2, the year is truncated to 2 digits;
	 * otherwise it is interpreted as a number. For parsing, if the number of pattern letters is more than 2, the year
	 * is interpreted literally, regardless of the number of digits. So using the pattern "MM/dd/yyyy", "01/11/12"
	 * parses to Jan 11, 12 A.D. For parsing with the abbreviated year pattern ("y" or "yy"), SimpleDateFormat must
	 * interpret the abbreviated year relative to some century. It does this by adjusting dates to be within 80 years
	 * before and 20 years after the time the SimpleDateFormat instance is created. End: excerpt
	 * 
	 * Note that a single "y" can be used to parse 1-4 year letters and it uses the nearest century to convert the date.
	 * In the same way single "h" and "H" can be used to parse hours.
	 * 
	 * In a nutshell, when we have format M/d/Y, we dont need other permutations of the same format like MM/dd/yyyy or
	 * MM/dd/yy and so on. So, commenting out the other formats that are redundant.
	 */
	"M/d/y h:mm:ss.SSS a", "M/d/y H:mm:ss.SSS", "M/d/y h:mm:ss a",
	"M/d/y H:mm:ss",
	"M/d/y h:mm a",
	"M/d/y H:mm",
	"M-d-y h:mm:ss.SSS a", "M-d-y H:mm:ss.SSS", "M-d-y h:mm:ss a",
			"M-d-y H:mm:ss",
			"M-d-y h:mm a", // Should be added
			"M-d-y H:mm",
			"y-M-d h:mm:ss.SSS a", "y-M-d H:mm:ss.SSS", "y-M-d h:mm:ss a",
			"y-M-d H:mm:ss",
			"y-M-d h:mm a",
			"y-M-d H:mm",
			"y/M/d h:mm:ss.SSS a", "y/M/d H:mm:ss.SSS", "y/M/d h:mm:ss a", "y/M/d H:mm:ss", "y/M/d h:mm a", "y/M/d H:mm", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", // SFDC
			"yyyy-MM-dd'T'HH:mm:ss.SSS", "yyyy-MM-dd'T'H:mm:ss", // Securian
			"yyyy/MM/dd'T'H:mm:ss", // should be added
			"yyyy-MM-dd't'H:mm:ss", // should be added
			"yyyy/MM/dd't'H:mm:ss", // should be added
			"yyyy-M-d", // added to stop M-d-y from matching 0001-01-01 (stoopid heuristics)
			"M/d/y",// "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy", "MM/dd/yy", "M/d/yy",
			"M-d-y", // "M-d-yyyy", "MM-d-yyyy", "M-dd-yyyy", "MM-dd-yyyy", "M-d-yy",
			"y/M/d",// "yyyy/MM/dd",
			"y-M-d",// "yyyy-MM-dd",
			"d/M/y h:mm:ss.SSS a", "d/M/y H:mm:ss.SSS", "d/M/y h:mm:ss a",// should be added
			"d/M/y H:mm:ss",// should be added
			"d/M/y h:mm a",// should be added
			"d/M/y H:mm",// should be added
			"d-M-y h:mm:ss.SSS a", "d-M-y H:mm:ss.SSS", "d-M-y h:mm:ss a",// should be added
			"d-M-y H:mm:ss",// should be added
			"d-M-y h:mm a",// should be added
			"d-M-y H:mm",// should be added
			"d/M/y",// should be added
			"d-M-y",// "dd-MM-yyyy", "dd-M-yyyy", "d-MM-yyyy", "d-M-yyyy",
			"d-MMM-y",// "dd-MMM-yyyy", "d-MMM-yyyy", "dd-MMM-yy", "d-MMM-yy",
			"EEEE, MMMM d, y",
			// "EEEE, MMMM dd, yyyy",
			"EEEE d MMMM y",
			// "EEEE dd MMMM yyyy",
			"MMMM d, y",// "MMM dd, yyyy", "MMM d, yyyy",
			"MMM d yyyy h:mma",
			"d-MMM",// "dd-MMM",
			"MMM-yy", "yyyyMMdd", "yyyyMM" };
	static List<SimpleDateFormat> possibleDateFormats = getPossibleDateFormats();

	public static List<SimpleDateFormat> getPossibleDateFormats()
	{
		List<SimpleDateFormat> result = new ArrayList<SimpleDateFormat>(possibleDateFormatPatterns.length);
		for (String s : possibleDateFormatPatterns)
		{
			/*
			 * DateFormat class has a method called setLenient(boolean) which is true by default, which means that while
			 * parsing of a datestring using the format of SimpleDateFormat class, if the given datestring does not
			 * exactly match the format, it tries to apply date arithmetic (adding or deducting days/month/year etc) and
			 * tries to achieve a valid date. For eg. string "13/12/2008" is parsed as "January 1 2009" with
			 * "MM/dd/yyyy" pattern instead of using "dd/MM/yyyy" pattern that follows. Our purpose is to find out the
			 * best pattern that matches all the dates of a column and then using that format to convert datestrings to
			 * date objects without changing the actual date. Only when we set the Lenient flag to false, parsing a
			 * string with the given format raises ParseException if it does not match the pattern exactly. And hence,
			 * we need to set the Lenient flag to false before adding each object of SimpleDateFormat to the ArrayList.
			 */
			SimpleDateFormat sdf = new SimpleDateFormat(s);
			sdf.setLenient(false);
			sdf.setTimeZone(TimeZone.getDefault());
			result.add(sdf);
		}
		return result;
	}

	public static String convertToDBFormat(String expression, List<SimpleDateFormat> formats, TimeZone processingTimeZone, TimeZone userTimeZone,
			boolean isDateTime)
	{
		if (formats == null)
		{
			formats = new ArrayList<SimpleDateFormat>();
		}
		SimpleDateFormat DBformat = new SimpleDateFormat(DB_FORMAT_STRING);
		DBformat.setTimeZone(processingTimeZone);
		DBformat.setLenient(false);
		formats.add(DBformat);
		formats.addAll(DateUtil.getPossibleDateFormats());
		for (SimpleDateFormat sdf : formats)
		{
			try
			{
				sdf = (SimpleDateFormat) sdf.clone();
				sdf.setTimeZone(userTimeZone);
				Date dt = sdf.parse(expression);
				if (isDateTime)
					DBformat.setTimeZone(processingTimeZone);
				else
					DBformat.setTimeZone(userTimeZone);
				expression = DBformat.format(dt);
			} catch (ParseException e)
			{
				continue;
			}
			break;
		}
		return expression;
	}

	public static String convertToDefaultFormat(Calendar cal, TimeZone processingTimeZone)
	{
		return convertToFormat(cal, processingTimeZone, DEFAULT_DATETIME_FORMAT);
	}

	public static String convertToDefaultFormat(Date date, TimeZone processingTimeZone)
	{
		return convertToFormat(date, processingTimeZone, DEFAULT_DATETIME_FORMAT);
	}

	public static String convertToDBFormat(Calendar cal, TimeZone processingTimeZone)
	{
		return convertToFormat(cal, processingTimeZone, DB_FORMAT_STRING);
	}

	public static String convertToDBFormat(Date date, TimeZone processingTimeZone)
	{
		return convertToFormat(date, processingTimeZone, DB_FORMAT_STRING);
	}

	public static String convertToFormat(Calendar cal, TimeZone processingTimeZone, String format)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(processingTimeZone);
		sdf.setLenient(false);
		return sdf.format(cal.getTime());
	}

	public static String convertToFormat(Date date, TimeZone processingTimeZone, String format)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(processingTimeZone);
		sdf.setLenient(false);
		return sdf.format(date);
	}

	public static SimpleDateFormat getShortDateFormat(Locale locale)
	{
		if (locale == null)
			locale = Locale.getDefault();
		SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale);
		return sdf;
	}

	public static SimpleDateFormat getShortDateTimeFormat(Locale locale)
	{
		if (locale == null)
			locale = Locale.getDefault();
		SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
		return sdf;
	}

	/*
	 * This method is called for all date columns in report in order to apply reverse timezone shift on date values. So
	 * that, when jasper gets date and applies delta for displaying in report, the Date does not change.
	 */
	public static Date applyReverseDeltaForDate(Date date, TimeZone processingTZ, TimeZone dispTZ)
	{
		long pOffset = processingTZ.getOffset(date.getTime());
		long dOffset = dispTZ.getOffset(date.getTime());
		return new Date(date.getTime() + pOffset - dOffset);
	}
}
