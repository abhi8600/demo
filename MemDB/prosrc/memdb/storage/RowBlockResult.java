package memdb.storage;

import java.util.List;

public class RowBlockResult
{
	int startIndex;
	int stopIndex;
	List<Object> results;
	int[] intArrayResults;
	long[] longArrayResults;
}
