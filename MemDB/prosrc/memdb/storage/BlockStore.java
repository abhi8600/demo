package memdb.storage;

import gnu.trove.list.TLongList;
import gnu.trove.list.array.TLongArrayList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import memdb.storage.Compression.Method;
import memdb.util.CalcSizeOfObject;
import memdb.util.Util;

import org.apache.log4j.Logger;

public class BlockStore
{
	private static Logger logger = Logger.getLogger(BlockStore.class);
	private long objectHashCode = (long) ObjectHash.getObjectHash() << 32;
	protected Object[] blocks;
	private boolean selfSynchronize = false;
	protected boolean diskBased = true;
	private boolean compressed = true;
	private Method method = Compression.Method.ZIP_NO_DESERIALIZE;
	// Disk file end indices for blocks
	private TLongList blockEndIndices;
	// Mapped byte buffer
	MappedFileBuffer mappedData;
	// Index of the current data file
	private int fileCount = 0;
	// Dynamically growing block store
	private boolean dynamic = false;

	public BlockStore(int numBlocks, boolean diskBased, boolean compressed, Method method)
	{
		this.diskBased = diskBased;
		this.compressed = compressed;
		this.method = method;
		if (numBlocks < 0)
		{
			blocks = new Object[0];
			dynamic = true;
			return;
		}
		blocks = new Object[numBlocks];
	}

	public int numBlocks()
	{
		return blocks.length;
	}

	public void readExternal(String metadataFileName, String dataFileNameMask) throws IOException, ClassNotFoundException
	{
		File f_metadata = new File(metadataFileName);
		if (!f_metadata.exists())
			return;
		FileInputStream fis = new FileInputStream(f_metadata);
		ObjectInputStream oi = new ObjectInputStream(new BufferedInputStream(fis));
		int length = oi.readInt();
		blocks = new Object[length];
		diskBased = oi.readBoolean();
		fileCount = oi.readInt();
		blockEndIndices = (TLongList) oi.readObject();
		fis.close();
		File f_data = new File(Util.replaceStr(dataFileNameMask, "%", ((Integer) fileCount).toString()));
		mappedData = new MappedFileBuffer();
		mappedData.getMappedBuffers(f_data, false, 0);
		for (int i = 0; i < length; i++)
		{
			if (!diskBased)
				getBlockEntry(i, true, false);
		}
		if (!diskBased)
			mappedData = null;
	}

	private byte[] getSerializedBlock(Object block)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream mos;
		try
		{
			mos = new ObjectOutputStream(baos);
			mos.writeObject(block);
			mos.flush();
			baos.flush();
		} catch (IOException e)
		{
			logger.error(e);
			return new byte[0];
		}
		byte[] arr = baos.toByteArray();
		if (compressed)
		{
			arr = Compression.compressObject(method, arr);
		}
		return arr;
	}

	public final Object getBlockEntry(int blockNum, boolean pinForUpdate, boolean serialized)
	{
		Object block = null;
		if (dynamic && blockNum >= blocks.length)
		{
			synchronized (this)
			{
				// Re-check after synchronization
				if (blockNum >= blocks.length)
				{
					block = ((BlockStoreUser) this).getInitializedBlock();
					Object[] newBlocks = new Object[blocks.length + 1];
					newBlocks[newBlocks.length - 1] = block;
					for (int i = 0; i < blocks.length; i++)
						newBlocks[i] = blocks[i];
					blocks = newBlocks;
					return block;
				}
			}
		}
		block = blocks[blockNum];
		if (block != null)
		{
			if (serialized)
				return getSerializedBlock(block);
			else
				return block;
		}
		if (blockEndIndices == null)
		{
			/*
			 * Hasn't been serialized before so no block exists, need to create a new one
			 */
			synchronized (this)
			{
				block = ((BlockStoreUser) this).getInitializedBlock();
				blocks[blockNum] = block;
				if (serialized)
					return getSerializedBlock(block);
				else
					return block;
			}
		}
		long key = 0;
		if (!serialized)
		{
			key = objectHashCode + blockNum;
			block = ObjectCache.cache.get(key);
			if (block != null)
			{
				if (pinForUpdate)
				{
					synchronized (this)
					{
						// Make sure it's still there
						block = ObjectCache.cache.get(key);
						if (block != null)
						{
							ObjectCache.cache.remove(key);
							blocks[blockNum] = block;
							return block;
						}
					}
				} else
					return block;
			}
		}
		if (selfSynchronize)
		{
			// Read into memory
			synchronized (this)
			{
				block = blocks[blockNum];
				if (block != null)
				{
					if (serialized)
						return getSerializedBlock(block);
					else
						return block;
				}
				block = getEntryFromDisk(blockNum, serialized);
				if (!serialized)
				{
					if (pinForUpdate)
						/*
						 * If pinning, make sure that at least the block is synchronized if not the entire rowvaluedata
						 * structure
						 */
						blocks[blockNum] = block;
					else
						ObjectCache.cache.put(key, block);
				}
				return block;
			}
		} else
		{
			block = getEntryFromDisk(blockNum, serialized);
			if (!serialized)
			{
				if (pinForUpdate)
					/*
					 * If pinning, make sure that at least the block is synchronized if not the entire rowvaluedata
					 * structure
					 */
					blocks[blockNum] = block;
				else
					ObjectCache.cache.put(key, block);
			}
			return block;
		}
	}

	private Object getEntryFromDisk(int blockNum, boolean serialized)
	{
		long start = blockNum == 0 ? 0 : (blockEndIndices.get(blockNum - 1) + 1);
		long end = blockEndIndices.get(blockNum);
		try
		{
			byte[] buff = new byte[(int) (end - start + 1)];
			synchronized (mappedData)
			{
				mappedData.getMappedData(start, buff);
			}
			if (compressed && !serialized)
				buff = (byte[]) Compression.decompressObject(method, buff);
			if (serialized)
				return buff;
			ByteArrayInputStream bais = new ByteArrayInputStream(buff);
			ObjectInputStream ois;
			ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (Exception e)
		{
			logger.error(e);
			return null;
		}
	}

	public int getFileCount()
	{
		return fileCount;
	}

	/**
	 * Data file name has a mask where "%" is to be replaced by file count
	 * 
	 * @param metadataFileName
	 * @param dataFileNameMask
	 * @throws IOException
	 */
	public void serializeToDisk(String metadataFileName, String dataFileNameMask) throws IOException
	{
		File f_incremental = new File(Util.replaceStr(dataFileNameMask, "%", "") + "_new");
		TLongList newBlockEndIndices = new TLongArrayList();
		/*
		 * For incremental inserts, use memory mapped buffer if possible. If not, stream to a new file and then merge
		 */
		long lastUnchangedBlockEndIndex = -1;
		int lastUnchangedBlockNum = -1;
		boolean hasChangedBlock = false;
		for (int i = 0; i < blocks.length; i++)
		{
			if (blocks[i] == null)
			{
				if (blockEndIndices != null)
				{
					lastUnchangedBlockEndIndex = blockEndIndices.get(i);
					lastUnchangedBlockNum = i;
				}
			} else
			{
				hasChangedBlock = true;
				break;
			}
		}
		// If nothing has been added, do not serialze
		if (!hasChangedBlock && blockEndIndices == null)
			return;
		long mappedLength = 0;
		if (lastUnchangedBlockNum >= 0)
		{
			for (int i = 0; i <= lastUnchangedBlockNum; i++)
				newBlockEndIndices.add(blockEndIndices.get(i));
			mappedLength = blockEndIndices.get(lastUnchangedBlockNum) + 1;
		}
		// Serialize to the next file count
		RandomAccessFile raf = new RandomAccessFile(f_incremental, "rw");
		FileChannel serializeFC = raf.getChannel();
		long curPosition = 0;
		Map<Integer, byte[]> lookAheadMap = new HashMap<Integer, byte[]>();
		for (int i = lastUnchangedBlockNum + 1; i < blocks.length; i++)
		{
			// Save each block from the first changed block on
			long position = curPosition + lastUnchangedBlockEndIndex + 1;
			byte[] arr = null;
			if (lookAheadMap.containsKey(i))
			{
				arr = lookAheadMap.get(i);
				lookAheadMap.remove(i);
			} else
				arr = (byte[]) getBlockEntry(i, false, true);
			/*
			 * If there's enough room in memory, use it, otherwise, spill to the new file
			 */
			if (mappedData != null && position + arr.length < mappedData.capacity())
			{
				/*
				 * Make sure we don't overwrite any blocks ahead of us (if we are about to, pull them out of the
				 * memory-mapped file and into regular memory
				 */
				if (blockEndIndices != null)
				{
					for (int lookAheadNum = i + 1; lookAheadNum < blocks.length && lookAheadNum < blockEndIndices.size(); lookAheadNum++)
					{
						if (lookAheadMap.containsKey(lookAheadNum))
							continue;
						long lastart = blockEndIndices.get(lookAheadNum - 1) + 1;
						if (lastart < position + arr.length)
						{
							byte[] larr = (byte[]) getBlockEntry(lookAheadNum, false, true);
							lookAheadMap.put(lookAheadNum, larr);
						} else
							break;
					}
				}
				mappedData.putMappedData(position, arr);
				mappedLength = position + arr.length;
			} else
			{
				ByteBuffer bb = ByteBuffer.wrap(arr);
				serializeFC.write(bb);
			}
			curPosition += arr.length;
			newBlockEndIndices.add(curPosition + lastUnchangedBlockEndIndex);
			// Clear memory if disk-based
			if (diskBased)
				blocks[i] = null;
		}
		serializeFC.close();
		raf.close();
		blockEndIndices = newBlockEndIndices;
		// Merge old file with new file if needed
		File f_new = null;
		if (f_incremental.length() > 0)
		{
			do
			{
				fileCount++;
				f_new = new File(Util.replaceStr(dataFileNameMask, "%", ((Integer) fileCount).toString()));
			} while (f_new.exists());
			FileOutputStream fos_new = null;
			FileInputStream fis_incremental = null;
			FileChannel dest = null;
			try
			{
				fos_new = new FileOutputStream(f_new);
				dest = fos_new.getChannel();
				if (mappedData != null && mappedLength > 0)
					mappedData.writeBuffersToChannel(dest, mappedLength);
				if (mappedData != null)
					mappedData.close();
				long f_inc_len = f_incremental.length();
				fis_incremental = new FileInputStream(f_incremental);
				FileChannel source = fis_incremental.getChannel();
				source.transferTo(0, f_inc_len, dest);
				source.close();
			} finally
			{
				if (dest != null)
				{
					dest.close();
				}
				if (fos_new != null)
					fos_new.close();
				if (fis_incremental != null)
					fis_incremental.close();
			}
		}
		f_incremental.delete();
		writeMetaData(metadataFileName);
		if (diskBased && blockEndIndices.size() > 0 && f_new != null)
		{
			int curRows = ((BlockStoreUser) this).getNumRows();
			int estRows = ((BlockStoreUser) this).getEstimatedNumRows();
			long lastDataPosition = (int) (blockEndIndices.get(blockEndIndices.size() - 1));
			if (curRows > 0 && estRows > 0 && estRows > curRows)
			{
				// Try to pre-allocate to work entirely within the mapped file
				double estimate = ((double) estRows) / ((double) curRows);
				estimate *= lastDataPosition;
				lastDataPosition = (long) estimate;
			}
			if (mappedData == null)
				mappedData = new MappedFileBuffer();
			mappedData.getMappedBuffers(f_new, true, lastDataPosition);
		}
		// Purge old data files
		for (int i = fileCount - 1; i >= 0; i--)
		{
			File f_purge = new File(Util.replaceStr(dataFileNameMask, "%", ((Integer) i).toString()));
			if (!f_purge.exists())
				break;
			f_purge.delete();
		}
		ObjectCache.clearForObject(this);
	}

	protected void writeMetaData(String metadataFileName) throws IOException
	{
		File f_metadata = new File(metadataFileName);
		FileOutputStream fos = new FileOutputStream(f_metadata);
		ObjectOutputStream oo = new ObjectOutputStream(new BufferedOutputStream(fos));
		oo.writeInt(blocks.length);
		oo.writeBoolean(diskBased);
		oo.writeInt(fileCount);
		oo.writeObject(blockEndIndices);
		oo.flush();
		fos.close();
	}

	/**
	 * Return out of range
	 * 
	 * @param blockNum
	 * @param user
	 * @return
	 */
	public boolean packBlock(int blockNum, boolean majorPack)
	{
		if (blockNum >= blocks.length)
			return false;
		if (blocks[blockNum] == null)
			return true;
		((BlockStoreUser) this).packBlockImpl(blocks[blockNum], majorPack);
		return true;
	}

	public boolean isSelfSynchronize()
	{
		return selfSynchronize;
	}

	public void setSelfSynchronize(boolean selfSynchronize)
	{
		this.selfSynchronize = selfSynchronize;
	}

	@Override
	public int hashCode()
	{
		return (int) (objectHashCode >>> 32);
	}

	public void clearBlockStore()
	{
		for (int i = 0; i < blocks.length; i++)
			blocks[i] = null;
		blockEndIndices = null;
	}

	public boolean isDiskBased()
	{
		return diskBased;
	}

	public boolean setDiskBased(boolean isDiskBased) throws IOException
	{
		boolean changed = false;
		if (this.diskBased != isDiskBased)
		{
			changed = true;
		}
		this.diskBased = isDiskBased;
		return changed;
	}

	public long getMemoryUsed(Table t)
	{
		if (this.diskBased)
			return 0;
		Set<Object> ignore = new HashSet<Object>();
		ignore.add(t);
		return CalcSizeOfObject.deepSizeOf(this, ignore);
	}
}
