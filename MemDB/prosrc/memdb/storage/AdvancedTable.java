package memdb.storage;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TObjectIntProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.TLongSet;

import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.Database;
import memdb.DateParser;
import memdb.ParallelAddRow;
import memdb.ThreadPool;
import memdb.rowset.RowSet;
import memdb.sql.RowRange;
import memdb.storage.Table;
import memdb.storage.TableMetadata;
import memdb.util.Util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@SuppressWarnings("unused")
public class AdvancedTable extends Table
{
	/**
	 * A note on case-insensitivity - all string values are mapped to a common surrogate key so if a new string is added
	 * that matches an existing one with case insensitivity, the original string is used.
	 */
	private static int NUM_COLUMN_LOAD_THREADS = 4;
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(AdvancedTable.class);
	// Private - per storage method
	protected Column[] columns = null;
	protected boolean[] referenceColumns = null;
	private boolean trans = false;
	private AtomicInteger referenceCount = new AtomicInteger(0);
	private AdvancedSurrogateKeyStore keyStore;

	public AdvancedTable()
	{
		Logger.getLogger(ObjectCache.class).setLevel(Level.DEBUG);
		Logger.getLogger(RowValueData.class).setLevel(Level.DEBUG);
		Logger.getLogger(SurrogateKeyToRowListMap.class).setLevel(Level.DEBUG);
		Logger.getLogger(Column.class).setLevel(Level.ALL);
		Logger.getLogger(BlockStore.class).setLevel(Level.ALL);
	}

	public void initializeNew(Database d, String keyStoreName, boolean diskKeyStore, boolean compressedKeyStore)
	{
		if (columns != null)
		{
			for (Column c : columns)
				c.drop();
		}
		columns = new Column[0];
		referenceColumns = new boolean[0];
		if (tmd != null)
		{
			tmd.lineNumber.set(0);
			if (keyStoreName == null)
				tmd.surrogateKeyStoreName = schema;
			else
				tmd.surrogateKeyStoreName = keyStoreName;
			tmd.diskBasedKeyStore = diskKeyStore;
			tmd.compressedKeyStore = compressedKeyStore;
		}
		keyStore = (AdvancedSurrogateKeyStore) d.getKeyStore(true, tmd != null ? tmd.surrogateKeyStoreName : keyStoreName, diskKeyStore, compressedKeyStore);
		changed = true;
	}

	public void clearCache()
	{
		if (columns != null)
		{
			for (Column c : columns)
				c.clearCache();
		}
	}

	@Override
	public void rename(String newName)
	{
		File f = new File(database.path + File.separator + schema + File.separator + name + ".md");
		if (f.exists())
		{
			f.renameTo(new File(database.path + File.separator + schema + File.separator + newName + ".md"));
		}
		f = new File(database.path + File.separator + schema + File.separator + name);
		if (f.exists())
		{
			f.renameTo(new File(database.path + File.separator + schema + File.separator + newName));
		}
		this.name = newName;
	}

	public void drop()
	{
		if (columns != null)
		{
			for (Column c : columns)
			{
				c.drop();
			}
		}
		referenceColumns = new boolean[0];
		if (tmd != null)
			tmd.lineNumber.set(0);
		File f = new File(database.path);
		if (!f.exists())
		{
			return;
		}
		f = new File(database.path + File.separator + schema + File.separator + name + ".md");
		if (f.exists())
		{
			f.delete();
		}
		f = new File(database.path + File.separator + schema + File.separator + name);
		if (f.exists() && f.isDirectory())
		{
			f.delete();
		}
	}

	public synchronized void readExternal() throws IOException, ClassNotFoundException, InterruptedException
	{
		readExternal(false);
	}

	public synchronized void updateColumnMetadata(int columnNumber)
	{
		Column c = columns[columnNumber];
		try
		{
			c.updateMetadata();
			writeMetadata();
		} catch (IOException e)
		{
			logger.error(e);
			return;
		} catch (ClassNotFoundException e)
		{
			logger.error(e);
			return;
		}
	}

	private synchronized void readExternal(boolean forceLoad) throws IOException, ClassNotFoundException, InterruptedException
	{
		File f = new File(database.path + File.separator + schema);
		if (!f.exists())
			throw new IOException("Database not found: " + database.path + File.separator + schema);
		FileInputStream fos = new FileInputStream(database.path + File.separator + schema + File.separator + name + ".md");
		ObjectInputStream saveFile = new ObjectInputStream(fos);
		tmd = (TableMetadata) saveFile.readObject();
		if (tmd.transientTable && database.noTransientTables)
			tmd.transientTable = false;
		fos.close();
		if (!tmd.transientTable || forceLoad)
		{
			columns = new Column[tmd.columnNames.length];
			curColIndex.set(0);
			for (int i = 0; i < tmd.columnNames.length; i++)
			{
				columns[i] = new Column(this, i);
			}
			referenceColumns = new boolean[columns.length];
			List<LoadColumnThread> tlist = new ArrayList<LoadColumnThread>();
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				LoadColumnThread t = new LoadColumnThread(this);
				tlist.add(t);
			}
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				tlist.get(i).start();
			}
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				try
				{
					tlist.get(i).join();
					if (tlist.get(i).error != null && tlist.get(i).error instanceof IOException)
					{
						throw (IOException) tlist.get(i).error;
					}
					if (tlist.get(i).error != null && tlist.get(i).error instanceof ClassNotFoundException)
					{
						throw (ClassNotFoundException) tlist.get(i).error;
					}
					if (tlist.get(i).error != null && tlist.get(i).error instanceof InterruptedException)
					{
						throw (InterruptedException) tlist.get(i).error;
					}
				} catch (InterruptedException e)
				{
					logger.error(e);
					saveFile.close();
					throw e;
				}
			}
			keyStore = (AdvancedSurrogateKeyStore) database.getKeyStore(true, tmd.surrogateKeyStoreName, tmd.diskBasedKeyStore, tmd.compressedKeyStore);
		}
		changed = false;
	}
	AtomicInteger curColIndex = new AtomicInteger(0);

	private class LoadColumnThread extends Thread
	{
		private Table table;
		public Exception error = null;

		public LoadColumnThread(Table table)
		{
			this.table = table;
		}

		public void run()
		{
			while (true)
			{
				int index = curColIndex.getAndIncrement();
				if (index >= columns.length)
					return;
				Column c = columns[index];
				try
				{
					c.readExternal(table, database.path + File.separator + schema + File.separator + name, index);
					referenceColumns[index] = c.isReferenceColumn();
				} catch (IOException e)
				{
					error = e;
					logger.error(e);
					return;
				} catch (ClassNotFoundException e)
				{
					error = e;
					logger.error(e);
					return;
				}
			}
		}
	}

	private void writeMetadata() throws IOException
	{
		FileOutputStream fos = new FileOutputStream(database.path + File.separator + schema + File.separator + name + ".md");
		ObjectOutputStream saveFile = new ObjectOutputStream(fos);
		saveFile.writeObject(tmd);
		saveFile.flush();
		fos.close();
	}

	public synchronized void writeExternal() throws IOException
	{
		if (!changed)
		{
			return;
		}
		if (columns == null && tmd.transientTable)
			return;
		keyStore.writeExternal();
		synchronized (columns)
		{
			/*
			 * When delete is added, will need to make sure there are no "missing" column indexes. Will have to do a
			 * pass over each column index and renumber them if there are holes. This is required because building the
			 * values structure on load assumes that there are as many elements in the structure as the top index
			 */
			File f = new File(database.path);
			if (!f.exists())
			{
				f.mkdir();
			}
			f = new File(database.path + File.separator + schema);
			if (!f.exists())
			{
				f.mkdir();
			}
			tmd.surrogateKeyStoreName = keyStore.getName();
			writeMetadata();
			if (columns == null)
				columns = new Column[tmd.columnNames.length];
			else if (columns.length < tmd.columnNames.length)
			{
				Column[] newcolumns = new Column[tmd.columnNames.length];
				boolean[] newreferenceColumns = new boolean[tmd.columnNames.length];
				for (int i = 0; i < columns.length; i++)
				{
					newcolumns[i] = columns[i];
					newreferenceColumns[i] = referenceColumns[i];
				}
				columns = newcolumns;
				referenceColumns = newreferenceColumns;
			}
			List<SaveColumnThread> tlist = new ArrayList<SaveColumnThread>();
			curColIndex.set(0);
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				SaveColumnThread t = new SaveColumnThread(this);
				tlist.add(t);
			}
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				tlist.get(i).start();
			}
			for (int i = 0; i < NUM_COLUMN_LOAD_THREADS; i++)
			{
				try
				{
					tlist.get(i).join();
					if (tlist.get(i).error != null && tlist.get(i).error instanceof IOException)
					{
						throw (IOException) tlist.get(i).error;
					}
					if (tlist.get(i).error != null && tlist.get(i).error instanceof InterruptedException)
					{
						throw (InterruptedException) tlist.get(i).error;
					}
				} catch (InterruptedException e)
				{
					logger.error(e);
				}
			}
			keyStore.writeExternal();
			changed = false;
		}
	}

	private class SaveColumnThread extends Thread
	{
		private Table table;
		public Exception error = null;

		public SaveColumnThread(Table table)
		{
			this.table = table;
		}

		public void run()
		{
			while (true)
			{
				int index = curColIndex.getAndIncrement();
				if (index >= columns.length)
					return;
				if (columns[index] == null)
					columns[index] = new Column(table, index);
				try
				{
					columns[index].save();
				} catch (IOException e)
				{
					error = e;
					logger.error(e);
					return;
				}
				referenceColumns[index] = columns[index].isReferenceColumn();
			}
		}
	}

	public int getNumRows()
	{
		return tmd.lineNumber.get();
	}

	public synchronized void pack()
	{
		if (!changed)
			return;
		if (columns == null && tmd.transientTable)
			return;
		synchronized (columns)
		{
			packColumn.set(0);
			List<PackThread> tlist = new ArrayList<PackThread>();
			for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++)
				tlist.add(new PackThread());
			for (PackThread pa : tlist)
				pa.start();
			for (PackThread pa : tlist)
			{
				try
				{
					pa.join();
				} catch (InterruptedException e)
				{
					logger.error(e);
					continue;
				}
			}
			referenceColumns = new boolean[columns.length];
			for (int i = 0; i < columns.length; i++)
				referenceColumns[i] = columns[i].isReferenceColumn();
		}
	}
	AtomicInteger packColumn = new AtomicInteger(0);

	private class PackThread extends Thread
	{
		public void run()
		{
			while (true)
			{
				int index = packColumn.getAndIncrement();
				if (index >= columns.length)
					break;
				Column c = columns[index];
				if (c == null)
					continue;
				c.pack();
			}
		}
	}

	public void addRow(Object[] row, TimeZone tz, RowRange rowRange)
	{
		super.addRow(row, tz, rowRange);
		int rownum;
		synchronized (this)
		{
			/*
			 * Presently, only one row can be added at a time. This ensures the same row number is used for all columns.
			 * This can be improved in the future by deciding the row number first and then altering the add methods to
			 * ensure that number is used. Right now they just append.
			 */
			if (row.length > columns.length)
			{
				Column[] newcolumns = new Column[row.length];
				boolean[] newreferenceColumns = new boolean[row.length];
				for (int i = 0; i < columns.length; i++)
				{
					newcolumns[i] = columns[i];
					newreferenceColumns[i] = referenceColumns[i];
				}
				columns = newcolumns;
				referenceColumns = newreferenceColumns;
			}
			rownum = tmd.lineNumber.getAndIncrement();
			if (rowRange != null)
			{
				if (rownum < rowRange.start || rowRange.start < 0)
					rowRange.start = rownum;
				else
					rowRange.stop = rownum;
			}
			for (int i = 0; i < row.length && i < tmd.columnNames.length; i++)
			{
				if (columns[i] == null)
				{
					columns[i] = new Column(this, i);
					referenceColumns[i] = columns[i].isReferenceColumn();
				}
			}
		}
		for (int i = 0; i < row.length && i < tmd.columnNames.length; i++)
		{
			Object key = row[i];
			columns[i].addRow(rownum, key, tz, rowRange == null);
		}
		changed = true;
	}

	@Override
	public RowSet getRowList(int columnNumber, Object key)
	{
		if (columns == null || columns.length <= columnNumber)
			return null;
		if (key == null)
			return null;
		return columns[columnNumber].getColumnIndexList(key, null, false);
	}

	@Override
	public RowSet getRowList(int columnNumber, Object key, RowSet baseSet)
	{
		if (columns == null || columns.length <= columnNumber)
			return null;
		if (key == null)
			return null;
		return columns[columnNumber].getColumnIndexList(key, baseSet, false);
	}

	@Override
	public RowSet getRowListForJoin(int columnNumber, Object key, RowSet baseSet)
	{
		if (columns == null || columns.length <= columnNumber)
			return null;
		if (key == null)
			return null;
		return columns[columnNumber].getColumnIndexList(key, baseSet, true);
	}

	private Object getRowValue(int columnNumber, int surrogateKey)
	{
		return columns[columnNumber].getRowValue(surrogateKey);
	}

	public boolean isReferenceColumn(int columnNumber)
	{
		return referenceColumns[columnNumber];
	}

	@Override
	public Object getTableValue(int rowNumber, int columnNumber)
	{
		Object key = getTableValueForJoin(rowNumber, columnNumber);
		if (referenceColumns.length > 0 && referenceColumns[columnNumber])
		{
			if (key != null && ((Integer) key) >= 0 && columns != null)
			{
				Column c = columns[columnNumber];
				return c.lookupValue((Integer) key);
			} else
				return null;
		}
		return key;
	}

	@Override
	public Object getTableValueForJoin(int rowNumber, int columnNumber)
	{
		if (columns == null || columns.length == 0)
			return null;
		Column c = columns[columnNumber];
		switch (tmd.dataTypes[columnNumber])
		{
		case Types.INTEGER:
			return c.getRowValue(rowNumber);
		case Types.FLOAT:
			Object o = c.getRowValue(rowNumber);
			if (o != null)
				return Float.intBitsToFloat((Integer) o);
			return Table.FLOATNULL;
		case Types.DOUBLE:
			o = c.getRowValue(rowNumber);
			if (o != null)
			{
				double d = Double.longBitsToDouble((Long) o);
				if (Double.isNaN(d))
					return null;
				return d;
			}
			return null;
		case Types.BIGINT:
			return c.getRowValue(rowNumber);
		case Types.DATE:
		case Types.TIMESTAMP:
			Long l = (Long) c.getRowValue(rowNumber);
			if (l == null)
				return null;
			return new Date(l);
		case Types.DECIMAL:
			l = (Long) c.getRowValue(rowNumber);
			if (l == null)
				return new BigDecimal(0);
			return Util.getLongDecimal(l);
		}
		Object key = c.getRowValue(rowNumber);
		return key;
	}

	@Override
	public void shrinkCache()
	{
		if (columns == null)
			return;
		for (Column c : columns)
			c.shrinkCache();
	}

	@Override
	public synchronized void deleteRows(RowSet rs)
	{
		if (columns.length == 0)
			return;
		deleteColumn.set(0);
		int curRows = tmd.lineNumber.get();
		TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete = new TIntIntHashMap();
		int newRowNum = 0;
		for (int rownum = 0; rownum < curRows; rownum++)
		{
			if (!rs.contains(rownum))
			{
				mapOfOldRowNumbersToNewNumbersAfterDelete.put(rownum, newRowNum);
				newRowNum++;
			}
		}
		List<DeleteThread> tlist = new ArrayList<DeleteThread>();
		for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++)
			tlist.add(new DeleteThread(mapOfOldRowNumbersToNewNumbersAfterDelete));
		for (DeleteThread pa : tlist)
			pa.start();
		for (DeleteThread pa : tlist)
		{
			try
			{
				pa.join();
			} catch (InterruptedException e)
			{
				logger.error(e);
				continue;
			}
		}
		tmd.lineNumber.set(columns[0].getCmd().getCurrentCount());
		changed = true;
	}
	AtomicInteger deleteColumn = new AtomicInteger(0);

	private class DeleteThread extends Thread
	{
		private TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete;

		public DeleteThread(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
		{
			this.mapOfOldRowNumbersToNewNumbersAfterDelete = mapOfOldRowNumbersToNewNumbersAfterDelete;
		}

		public void run()
		{
			while (true)
			{
				int index = deleteColumn.getAndIncrement();
				if (index >= columns.length)
					break;
				Column c = columns[index];
				c.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
			}
		}
	}

	public void updateRow(int rowNum, int colNum, Object newValue)
	{
		Column c = columns[colNum];
		c.updateRow(rowNum, newValue);
		changed = true;
	}

	@Override
	public boolean isNull(int dataType, Object value)
	{
		if (value == null)
			return true;
		if (dataType == Types.INTEGER)
		{
			return ((Integer) value) == Integer.MIN_VALUE;
		} else if (dataType == Types.DOUBLE)
		{
			return Double.doubleToLongBits((Double) value) == Column.LONG_NAN;
		} else if (dataType == Types.BIGINT)
		{
			return ((Long) value) == Long.MAX_VALUE;
		} else if (dataType == Types.FLOAT)
		{
			return ((Float) value) == Table.FLOATNULL;
		} else if (dataType == Types.DATE)
		{
			return ((Date) value).getTime() == Long.MAX_VALUE;
		}
		return false;
	}

	@Override
	public void release() throws IOException
	{
		if (columns == null)
			return;
		synchronized (this)
		{
			if (referenceCount.decrementAndGet() == 0)
			{
				if (tmd.transientTable)
				{
					if (changed)
					{
						writeExternal();
						changed = false;
					}
					for (int i = 0; i < columns.length; i++)
						columns[i].release();
					columns = null;
				}
			}
		}
	}

	@Override
	public void reserve() throws IOException, ClassNotFoundException, InterruptedException
	{
		if (columns == null
				|| (tmd.transientTable && tmd.columnNames.length > 0 && (columns.length == 0 || tmd.lineNumber.get() > columns[0].getCmd().getCurrentCount())))
		{
			if (!changed && (columns == null || columns.length == 0))
				readExternal(true);
		}
		referenceCount.incrementAndGet();
	}

	@Override
	public int addColumn() throws SQLException
	{
		Column[] newcols = new Column[columns.length + 1];
		boolean[] newref = new boolean[referenceColumns.length + 1];
		for (int i = 0; i < columns.length; i++)
		{
			newcols[i] = columns[i];
			newref[i] = referenceColumns[i];
		}
		columns = newcols;
		referenceColumns = newref;
		columns[columns.length - 1] = new Column(this, columns.length - 1);
		referenceColumns[columns.length - 1] = columns[columns.length - 1].isReferenceColumn();
		/*
		 * Fill in nulls
		 */
		int numRows = getNumRows();
		for (int i = 0; i < numRows; i++)
		{
			columns[columns.length - 1].addRow(i, null, TimeZone.getDefault(), true);
		}
		changed = true;
		return columns.length;
	}

	@Override
	public int dropColumn(int position) throws SQLException
	{
		columns[position].drop();
		Column[] newcols = new Column[columns.length - 1];
		boolean[] newref = new boolean[referenceColumns.length - 1];
		int count = 0;
		for (int i = 0; i < columns.length; i++)
		{
			if (i == position)
				continue;
			newcols[count] = columns[i];
			newref[count++] = referenceColumns[i];
		}
		columns = newcols;
		referenceColumns = newref;
		changed = true;
		return columns.length;
	}

	@Override
	public void updateColumnData(int position, TimeZone tz) throws SQLException, IOException
	{
		Column oldc = columns[position];
		Column newc = new Column(this, position);
		Table oldTable = oldc.getTable();
		oldc.clearCache();
		columns[position] = newc;
		int numRows = getNumRows();
		DateParser dp = null;
		changed = true;
		if (tmd.dataTypes[position] == Types.DATE || tmd.dataTypes[position] == Types.TIMESTAMP)
			dp = new DateParser();
		for (int i = 0; i < numRows; i++)
		{
			Object o = oldc.getRowValue(i);
			if (tmd.dataTypes[position] == Types.INTEGER)
			{
				if (o instanceof Integer)
				{
				} else if (o instanceof Double)
				{
					o = ((Double) o).intValue();
				} else if (o instanceof Float)
				{
					o = ((Float) o).intValue();
				} else if (o instanceof Long)
				{
					o = ((Long) o).intValue();
				} else if (o instanceof BigDecimal)
				{
					o = ((BigDecimal) o).intValue();
				} else if (o instanceof String)
				{
					try
					{
						o = Integer.valueOf((String) o);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.DOUBLE)
			{
				if (o instanceof Integer)
				{
					o = ((Integer) o).doubleValue();
				} else if (o instanceof Double)
				{
				} else if (o instanceof Float)
				{
					o = ((Float) o).doubleValue();
				} else if (o instanceof Long)
				{
					o = ((Long) o).doubleValue();
				} else if (o instanceof BigDecimal)
				{
					o = ((BigDecimal) o).doubleValue();
				} else if (o instanceof String)
				{
					try
					{
						o = Double.valueOf((String) o);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.FLOAT)
			{
				if (o instanceof Integer)
				{
					o = ((Integer) o).floatValue();
				} else if (o instanceof Double)
				{
					o = ((Double) o).floatValue();
				} else if (o instanceof Float)
				{
				} else if (o instanceof Long)
				{
					o = ((Long) o).floatValue();
				} else if (o instanceof BigDecimal)
				{
					o = ((BigDecimal) o).floatValue();
				} else if (o instanceof String)
				{
					try
					{
						o = Float.valueOf((String) o);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.BIGINT)
			{
				if (o instanceof Integer)
				{
					o = ((Integer) o).longValue();
				} else if (o instanceof Double)
				{
					o = ((Double) o).longValue();
				} else if (o instanceof Float)
				{
					o = ((Float) o).longValue();
				} else if (o instanceof Long)
				{
				} else if (o instanceof BigDecimal)
				{
					o = ((BigDecimal) o).longValue();
				} else if (o instanceof String)
				{
					try
					{
						o = Long.valueOf((String) o);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.DECIMAL)
			{
				if (o instanceof Integer)
				{
					o = new BigDecimal(((Integer) o).doubleValue());
				} else if (o instanceof Double)
				{
					o = new BigDecimal((Double) o);
				} else if (o instanceof Float)
				{
					o = new BigDecimal(((Float) o).doubleValue());
				} else if (o instanceof Long)
				{
					o = new BigDecimal(((Long) o).doubleValue());
				} else if (o instanceof BigDecimal)
				{
				} else if (o instanceof String)
				{
					try
					{
						o = new BigDecimal((String) o);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.DATE)
			{
				if (o instanceof Long)
				{
					o = new Date((Long) o);
				} else if (o instanceof String)
				{
					try
					{
						o = dp.parseUsingPossibleFormats((String) o, tz);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.TIMESTAMP)
			{
				if (o instanceof Long)
				{
					o = new Date((Long) o);
				} else if (o instanceof String)
				{
					try
					{
						o = dp.parseUsingPossibleFormats((String) o, tz);
					} catch (Exception ex)
					{
						o = null;
					}
				} else
					o = null;
			} else if (tmd.dataTypes[position] == Types.VARCHAR)
			{
				if (!(o instanceof String))
				{
					if (oldc.isReferenceColumn() && o instanceof Integer)
					{
						o = oldTable.getSurrogateKeyValue((Integer) o, position);
					} else
					{
						o = o.toString();
					}
				}
			}
			newc.addRow(i, o, tz, true);
		}
		oldc.clear();
		newc.pack();
		newc.save();
	}

	@Override
	public Iterator<Object> getKeyIterator(int columnNumber)
	{
		return columns[columnNumber].getKeyIterator();
	}

	@Override
	public int getKeyIteratorSize(int columnNumber)
	{
		return columns[columnNumber].getKeyIteratorSize();
	}

	@Override
	public void indexRange(RowRange range)
	{
		ThreadPool tp = new ThreadPool();
		for (int i = 0; i < getNumColumns(); i++)
			columns[i].indexRange(tp, range);
		tp.terminateAndAwait();
	}

	@Override
	public Object getSurrogateKeyValue(int surrogateKey, int columnNumber)
	{
		return keyStore.getSurrogateKeyValue(surrogateKey, this, columnNumber);
	}

	@Override
	public int getObjectSurrogateKey(Object value, int columnNumber, boolean caseSensitive, boolean createIfNotPresent)
	{
		return keyStore.getObjectSurrogateKey(value, this, columnNumber, caseSensitive, createIfNotPresent);
	}

	@Override
	public SurrogateKeyStore getKeyStore()
	{
		return keyStore;
	}

	@Override
	public long getColumnSize(int columnNumber)
	{
		if (columnNumber >= columns.length)
			return 0;
		Column c = columns[columnNumber];
		long len = c.getMemoryUsed();
		if (len > 0)
		{
			/*
			 * If it's memory-based, then return memory size. Note this assumes all structures, both index and values
			 * are memory
			 */
			return len;
		}
		File f = new File(database.path + File.separator + schema + File.separator + name + File.separator + tmd.columnNames[columnNumber]);
		len = f.exists() ? Util.getFileSize(f) : 0;
		return len;
	}

	@Override
	public long getMemoryUsed()
	{
		long result = 0;
		if (columns != null)
		{
			for (Column c : columns)
				result += c.getMemoryUsed();
		}
		return result;
	}
}
