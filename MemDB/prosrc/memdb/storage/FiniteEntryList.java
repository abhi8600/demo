package memdb.storage;

import gnu.trove.iterator.TIntIntIterator;
import gnu.trove.iterator.TLongIntIterator;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TLongIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TLongIntHashMap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import memdb.keyindex.IntKeyIndex;
import memdb.keyindex.KeyIndex;
import memdb.keyindex.LongKeyIndex;

public class FiniteEntryList implements Externalizable
{
	public static final long serialVersionUID = 1L;
	// Primary key map
	TIntIntMap intPrimaryKeyMap;
	TLongIntMap longPrimaryKeyMap;
	// First entry list
	KeyIndex baseIndex;
	// Final packed list
	NullPackedLongList longKeyList;
	NullPackedIntegerList intKeyList;
	NullPackedIntegerList intList;

	public FiniteEntryList()
	{
	}

	public FiniteEntryList(boolean isPrimaryKey, boolean isLong)
	{
		if (isLong)
		{
			if (isPrimaryKey)
			{
				longPrimaryKeyMap = new TLongIntHashMap(20, (float) 0.8, -1L, -1);
			} else
			{
				baseIndex = new LongKeyIndex();
			}
		} else
		{
			if (isPrimaryKey)
			{
				intPrimaryKeyMap = new TIntIntHashMap(20, (float) 0.8, -1, -1);
			} else
			{
				baseIndex = new IntKeyIndex();
			}
		}
	}

	public int get(Object encodedValue)
	{
		int row = -1;
		if (intPrimaryKeyMap != null || longPrimaryKeyMap != null)
		{
			if (intPrimaryKeyMap != null)
			{
				if (encodedValue instanceof Integer)
					return intPrimaryKeyMap.get((Integer) encodedValue);
				else
					return -1;
			} else
			{
				if (encodedValue instanceof Long)
					return longPrimaryKeyMap.get((Long) encodedValue);
				else
					return -1;
			}
		}
		if (longKeyList != null || intKeyList != null)
		{
			if (longKeyList != null)
			{
				row = longKeyList.find((Long) encodedValue);
				if (row < 0)
					return -1;
				return intList.get(row);
			} else
			{
				row = intKeyList.find((Integer) encodedValue);
				if (row < 0)
					return -1;
				return intList.get(row);
			}
		}
		return baseIndex.getIndexForKey(encodedValue);
	}

	public Object getKey(int index)
	{
		if (baseIndex != null)
			return baseIndex.getKeyAtIndex(index);
		else if (intKeyList != null)
			return intKeyList.get(index);
		else if (longKeyList != null)
			return longKeyList.get(index);
		else if (intPrimaryKeyMap != null)
			return intPrimaryKeyMap.keys()[index];
		else if (longPrimaryKeyMap != null)
			return longPrimaryKeyMap.keys()[index];
		return null;
	}

	public void remove(Object encodedValue)
	{
		if (intPrimaryKeyMap != null)
		{
			if (encodedValue instanceof Integer)
				intPrimaryKeyMap.remove((Integer) encodedValue);
		} else if (longPrimaryKeyMap != null)
		{
			if (encodedValue instanceof Long)
				longPrimaryKeyMap.remove((Long) encodedValue);
		} else if (longKeyList != null)
		{
			synchronized (this)
			{
				int row = longKeyList.find((Long) encodedValue);
				if (row >= 0)
				{
					TLongArrayList oldLongKeys = longKeyList.getLongArrayList();
					oldLongKeys.remove(row);
					longKeyList = new NullPackedLongList(oldLongKeys);
					TIntArrayList oldInts = intList.getIntArrayList();
					oldInts.remove(row);
					intList = new NullPackedIntegerList(oldInts);
					return;
				}
			}
		} else if (intKeyList != null)
		{
			synchronized (this)
			{
				int row = intKeyList.find((Integer) encodedValue);
				if (row >= 0)
				{
					TIntArrayList oldIntKeys = intKeyList.getIntArrayList();
					oldIntKeys.remove(row);
					intKeyList = new NullPackedIntegerList(oldIntKeys);
					TIntArrayList oldInts = intList.getIntArrayList();
					oldInts.remove(row);
					intList = new NullPackedIntegerList(oldInts);
					return;
				}
			}
		}
		if (baseIndex != null)
		{
			synchronized (this)
			{
				baseIndex.removeKey(encodedValue);
			}
		}
	}

	/**
	 * Adds a new row number for a given key
	 * 
	 * @param key
	 * @param rowNums
	 * @return -1 if the row was added, an existing row number if it can't be added because the slot is full
	 */
	public int addRowNum(Object key, int rowNum)
	{
		if (intPrimaryKeyMap != null || longPrimaryKeyMap != null)
		{
			if (intPrimaryKeyMap != null)
			{
				if (intPrimaryKeyMap.containsKey((Integer) key))
					return intPrimaryKeyMap.get((Integer) key);
				intPrimaryKeyMap.put((Integer) key, rowNum);
				return -1;
			} else
			{
				if (longPrimaryKeyMap.containsKey((Long) key))
					return longPrimaryKeyMap.get((Long) key);
				longPrimaryKeyMap.put((Long) key, rowNum);
				return -1;
			}
		}
		int position = baseIndex.getIndexForKey(key);
		if (position >= 0)
			return position;
		baseIndex.addKeyIndex(key, rowNum);
		return -1;
	}

	public void removeKey(Object key)
	{
		if (intPrimaryKeyMap != null || longPrimaryKeyMap != null)
		{
			if (intPrimaryKeyMap != null)
				intPrimaryKeyMap.remove((Integer) key);
			else
				longPrimaryKeyMap.remove((Long) key);
			return;
		}
		baseIndex.removeKey(key);
	}

	public int size()
	{
		if (intPrimaryKeyMap != null)
			return intPrimaryKeyMap.size();
		else if (longPrimaryKeyMap != null)
			return longPrimaryKeyMap.size();
		else if (longKeyList != null)
			return longKeyList.size();
		else if (intKeyList != null)
			return intKeyList.size();
		else if (baseIndex != null)
			return baseIndex.size();
		return 0;
	}

	public void pack()
	{
		if (baseIndex != null)
		{
			baseIndex.sort();
			if (baseIndex instanceof LongKeyIndex)
				longKeyList = ((LongKeyIndex) baseIndex).getPackedKeys();
			else
				intKeyList = ((IntKeyIndex) baseIndex).getPackedKeys();
			intList = baseIndex.getPackedIndexes();
			baseIndex = null;
		}
	}

	public void unpackForModification()
	{
		if (longKeyList != null || intKeyList != null)
		{
			synchronized (this)
			{
				unpack();
			}
		}
	}

	private void unpack()
	{
		if (longKeyList != null)
			baseIndex = new LongKeyIndex(longKeyList, intList);
		else if (intKeyList != null)
			baseIndex = new IntKeyIndex(intKeyList, intList);
		intList = null;
		longKeyList = null;
		intKeyList = null;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		if (baseIndex != null)
		{
			synchronized (this)
			{
				pack();
			}
		}
		out.writeObject(longKeyList);
		out.writeObject(intKeyList);
		out.writeObject(intList);
		out.writeObject(intPrimaryKeyMap);
		out.writeObject(longPrimaryKeyMap);
		out.writeObject(baseIndex);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		longKeyList = (NullPackedLongList) in.readObject();
		intKeyList = (NullPackedIntegerList) in.readObject();
		intList = (NullPackedIntegerList) in.readObject();
		intPrimaryKeyMap = (TIntIntMap) in.readObject();
		longPrimaryKeyMap = (TLongIntMap) in.readObject();
		baseIndex = (KeyIndex) in.readObject();
	}

	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		if (intPrimaryKeyMap != null || longPrimaryKeyMap != null)
		{
			if (intPrimaryKeyMap != null)
			{
				TIntIntMap newMap = new TIntIntHashMap();
				TIntIntIterator tintit = intPrimaryKeyMap.iterator();
				for (int i = intPrimaryKeyMap.size(); i-- > 0;)
				{
					tintit.advance();
					int key = tintit.key();
					int value = tintit.value();
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(value))
					{
						newMap.put(key, value);
					}
				}
				intPrimaryKeyMap = newMap;
			} else
			{
				TLongIntMap newMap = new TLongIntHashMap();
				TLongIntIterator tintit = longPrimaryKeyMap.iterator();
				for (int i = longPrimaryKeyMap.size(); i-- > 0;)
				{
					tintit.advance();
					long key = tintit.key();
					int value = tintit.value();
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(value))
					{
						newMap.put(key, value);
					}
				}
				longPrimaryKeyMap = newMap;
			}
			return;
		}
		baseIndex.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
	}

	public int getKeyIteratorSize()
	{
		int result = 0;
		if (intPrimaryKeyMap != null)
			return intPrimaryKeyMap.size();
		else if (longPrimaryKeyMap != null)
			return longPrimaryKeyMap.size();
		else if (baseIndex != null)
			result = baseIndex.size();
		else if (intKeyList != null)
			result = intKeyList.size();
		else if (longKeyList != null)
			result = longKeyList.size();
		return result;
	}
}
