package memdb.storage;

public interface BlockStoreUser
{
	public Object getInitializedBlock();

	public void packBlockImpl(Object block, boolean majorPack);

	public int getNumRows();

	public int getEstimatedNumRows();
}
