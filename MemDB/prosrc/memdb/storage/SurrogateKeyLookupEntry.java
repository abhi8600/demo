package memdb.storage;

import gnu.trove.list.TByteList;
import gnu.trove.list.array.TByteArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.Charset;

import memdb.util.Util;

public class SurrogateKeyLookupEntry implements Externalizable
{
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("rawtypes")
	// Object value;
	TIntObjectMap multivaluemap;
	TIntIntMap map;
	TByteList data;
	byte[] dataarr;

	@SuppressWarnings("rawtypes")
	public SurrogateKeyLookupEntry()
	{
		multivaluemap = new TIntObjectHashMap();
		map = new TIntIntHashMap(10, 0.9f, -1, -1);
		data = new TByteArrayList();
		dataarr = null;
	}

	@SuppressWarnings("rawtypes")
	public SurrogateKeyLookupEntry(boolean initialize)
	{
		if (!initialize)
			return;
		multivaluemap = new TIntObjectHashMap();
		map = new TIntIntHashMap(10, 0.9f, -1, -1);
		data = new TByteArrayList();
		dataarr = null;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(multivaluemap);
		out.writeObject(map);
		out.writeObject(data);
		out.writeObject(dataarr);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		multivaluemap = (TIntObjectMap) in.readObject();
		map = (TIntIntMap) in.readObject();
		data = (TByteList) in.readObject();
		dataarr = (byte[]) in.readObject();
	}

	public Object getStoredValue(IntValue hash, boolean caseInsensitive, Object value, byte[] valueArr, Charset cset)
	{
		Object storedValue = null;
		boolean equalValues = false;
		do
		{
			/*
			 * If there is a hash collision, increment
			 */
			int hashInt;
			if (storedValue != null)
				hashInt = ++hash.val;
			else
				hashInt = hash.val;
			/*
			 * Stored value can be an integer (pointer to a byte[]) or a ColumnSurrogateKeyValueEntry[]
			 */
			int position = 0;
			boolean successful = false;
			while (!successful)
			{
				try
				{
					position = map.get(hashInt);
					successful = true;
				} catch (ArrayIndexOutOfBoundsException e)
				{
					continue;
				}
			}
			if (position == map.getNoEntryValue())
				storedValue = multivaluemap.get(hashInt);
			else
			{
				if (data != null)
				{
					int len = 0;
					for (int i = position; i < data.size() && data.get(i) != 0; i++, len++)
					{
					}
					storedValue = new byte[len];
					for (int i = 0; i < len; i++)
						((byte[]) storedValue)[i] = data.get(i + position);
				} else
				{
					int len = 0;
					for (int i = position; i < dataarr.length && dataarr[i] != 0; i++, len++)
					{
					}
					storedValue = new byte[len];
					for (int i = 0; i < len; i++)
						((byte[]) storedValue)[i] = dataarr[i + position];
				}
			}
			equalValues = false;
			if (storedValue != null)
			{
				if (caseInsensitive && valueArr != null)
				{
					String a = null;
					if (storedValue instanceof byte[])
						a = new String((byte[]) storedValue, cset);
					else if (storedValue instanceof ColumnSurrogateKeyValueEntry[])
						a = new String((byte[]) ((ColumnSurrogateKeyValueEntry[]) storedValue)[0].value);
					equalValues = a.equalsIgnoreCase((String) value);
				} else
				{
					if (valueArr != null)
						equalValues = Util.equalByteArrays((byte[]) storedValue, valueArr);
					else
						equalValues = Util.equalByteArrays((byte[]) storedValue, (byte[]) value);
				}
			}
		} while (storedValue != null && !equalValues);
		return storedValue;
	}

	public void setSingleValue(int hash, byte[] arr)
	{
		if (data == null)
		{
			data = dataarr != null ? new TByteArrayList(dataarr) : new TByteArrayList();
			dataarr = null;
		}
		map.put(hash, data.size());
		data.add(arr);
		data.add((byte) 0);
	}

	@SuppressWarnings("unchecked")
	public void setMultiValuedKey(int hash, ColumnSurrogateKeyValueEntry[] entries)
	{
		int position = map.get(hash);
		if (position != map.getNoEntryValue())
		{
			map.remove(hash);
			/*
			 * Don't actually remove it from the data - later on can check to see if compaction is needed
			 */
		}
		multivaluemap.put(hash, entries);
	}

	public Object getValue(int surrogateKey, Table table, int columnNumber, @SuppressWarnings("rawtypes") TObjectIntMap tableColumnToKeyMap, Charset cset)
	{
		int position = map.get(surrogateKey);
		if (position != map.getNoEntryValue())
		{
			int len = 0;
			if (data != null)
			{
				for (int i = position; i < data.size() && data.get(i) != 0; i++, len++)
				{
				}
				byte[] arr = new byte[len];
				for (int i = 0; i < len; i++)
					((byte[]) arr)[i] = data.get(i + position);
				return new String(arr, cset);
			} else
			{
				for (int i = position; i < dataarr.length && dataarr[i] != 0; i++, len++)
				{
				}
				byte[] arr = new byte[len];
				for (int i = 0; i < len; i++)
					((byte[]) arr)[i] = dataarr[i + position];
				return new String(arr, cset);
			}
		}
		Object o = multivaluemap.get(surrogateKey);
		if (o instanceof ColumnSurrogateKeyValueEntry[])
		{
			int valueIndex = 0;
			if (table != null)
			{
				String key = table.schema + "." + table.name + "|" + columnNumber;
				int columnCode = -1;
				if (tableColumnToKeyMap.containsKey(key))
					columnCode = tableColumnToKeyMap.get(key);
				for (int i = 1; i < ((ColumnSurrogateKeyValueEntry[]) o).length; i++)
				{
					if (((ColumnSurrogateKeyValueEntry[]) o)[i] != null && ((ColumnSurrogateKeyValueEntry[]) o)[i].columnCode == columnCode)
					{
						valueIndex = i;
						break;
					}
				}
			}
			o = ((ColumnSurrogateKeyValueEntry[]) o)[valueIndex].value;
			if (o instanceof byte[])
				return new String((byte[]) o, cset);
		}
		return null;
	}

	public void pack()
	{
		if (data != null)
		{
			if (data.size() > 0)
				dataarr = data.toArray();
			else
				dataarr = null;
			data = null;
		}
	}
}
