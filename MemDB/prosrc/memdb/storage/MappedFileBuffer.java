package memdb.storage;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

public class MappedFileBuffer
{
	public static int PCT_CAPACITY = 10;
	public static int MAX_BYTES_PER_BUFFER = 1 << 30;
	private MappedByteBuffer[] mappedData = null;
	private RandomAccessFile raf = null;
	private FileChannel fc = null;

	public long capacity()
	{
		if (mappedData == null)
			return 0;
		long result = 0;
		for (int i = 0; i < mappedData.length - 1; i++)
			result += MAX_BYTES_PER_BUFFER;
		result += mappedData[mappedData.length - 1].capacity();
		return result;
	}

	public void getMappedBuffers(File f, boolean ensureCapacity, long lastDataPosition) throws IOException
	{
		try
		{
			raf = new RandomAccessFile(f, "rw");
			if (ensureCapacity)
			{
				long curlength = raf.length();
				long desiredlength = lastDataPosition * (100 + PCT_CAPACITY) / 100;
				// expand by a min amount
				if (desiredlength - curlength < 1024)
					desiredlength = curlength + 1024;
				if (curlength < desiredlength)
				{
					raf.setLength(desiredlength);
				}
			}
			// Now map the file to memory
			fc = raf.getChannel();
			long length = fc.size();
			int numBuffers = (int) (length / MAX_BYTES_PER_BUFFER) + 1;
			mappedData = new MappedByteBuffer[numBuffers];
			long start = 0;
			for (int i = 0; i < numBuffers; i++)
			{
				long buflen = length - start;
				if (buflen > MAX_BYTES_PER_BUFFER)
					buflen = MAX_BYTES_PER_BUFFER;
				mappedData[i] = fc.map(MapMode.READ_WRITE, start, buflen);
				start += buflen;
			}
		} finally
		{
			fc.close();
			raf.close();
		}
	}

	public void close() throws IOException
	{
		if (raf != null)
			raf.close();
		if (fc != null)
			fc.close();
		for (int i = 0; i < mappedData.length; i++)
			mappedData[i] = null;
	}

	public void writeBuffersToChannel(FileChannel dest, long length) throws IOException
	{
		if (mappedData == null)
			return;
		long start = 0;
		for (int i = 0; i < mappedData.length; i++)
		{
			long buflen = length - start;
			if (buflen > MAX_BYTES_PER_BUFFER)
				buflen = MAX_BYTES_PER_BUFFER;
			mappedData[i].position(0);
			mappedData[i].limit((int) buflen);
			dest.write(mappedData[i]);
			start += buflen;
		}
	}

	public void getMappedData(long start, byte[] buff)
	{
		int mappedBufferIndex = (int) (start / MAX_BYTES_PER_BUFFER);
		int mappedBufferPosition = (int) (start % MAX_BYTES_PER_BUFFER);
		int remainingBytes = buff.length;
		int targetBufferPosition = 0;
		while (remainingBytes > 0)
		{
			int len = MAX_BYTES_PER_BUFFER - mappedBufferPosition;
			if (remainingBytes < len)
				len = remainingBytes;
			mappedData[mappedBufferIndex].position(mappedBufferPosition);
			mappedData[mappedBufferIndex].get(buff, targetBufferPosition, len);
			mappedBufferIndex++;
			targetBufferPosition += len;
			remainingBytes -= len;
			mappedBufferPosition = 0;
		}
	}

	public void putMappedData(long start, byte[] buff)
	{
		int mappedBufferIndex = (int) (start / MAX_BYTES_PER_BUFFER);
		int mappedBufferPosition = (int) (start % MAX_BYTES_PER_BUFFER);
		int remainingBytes = buff.length;
		int targetBufferPosition = 0;
		while (remainingBytes > 0)
		{
			int len = MAX_BYTES_PER_BUFFER - mappedBufferPosition;
			if (remainingBytes < len)
				len = remainingBytes;
			mappedData[mappedBufferIndex].position(mappedBufferPosition);
			mappedData[mappedBufferIndex].put(buff, targetBufferPosition, len);
			mappedBufferIndex++;
			targetBufferPosition += len;
			remainingBytes -= len;
			mappedBufferPosition = 0;
		}
	}
}
