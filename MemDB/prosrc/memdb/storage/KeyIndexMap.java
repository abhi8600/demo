package memdb.storage;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.iterator.TLongIterator;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TLongObjectProcedure;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.indexlist.DifferencePackedIntegerList;
import memdb.indexlist.IndexList;
import memdb.indexlist.IntArrayIndexList;

import org.apache.log4j.Logger;

public class KeyIndexMap implements Externalizable
{
	static Logger logger = Logger.getLogger(KeyIndexMap.class);
	public static final long serialVersionUID = 1L;
	private int FINITE_ENTRIES;
	// Multi-entry map object
	@SuppressWarnings("rawtypes")
	TLongObjectMap lomap;
	@SuppressWarnings("rawtypes")
	TIntObjectMap iomap;
	FiniteEntryList[] felist;

	public KeyIndexMap()
	{
	}

	/*
	 */
	@SuppressWarnings("rawtypes")
	public KeyIndexMap(int rowCount, boolean primaryKey, boolean longKeyType)
	{
		if (longKeyType)
		{
			lomap = new TLongObjectHashMap();
		} else
		{
			iomap = new TIntObjectHashMap();
		}
		if (rowCount < 1000000)
			FINITE_ENTRIES = 0;
		else
			FINITE_ENTRIES = 2;
		felist = new FiniteEntryList[FINITE_ENTRIES];
		for (int i = 0; i < FINITE_ENTRIES; i++)
			felist[i] = new FiniteEntryList(i == 0 && primaryKey, longKeyType);
	}

	public Object get(Object encodedValue)
	{
		if (lomap != null)
		{
			if (encodedValue instanceof Integer)
				encodedValue = (long) ((Integer) encodedValue);
		} else
		{
			if (encodedValue instanceof Long)
				encodedValue = (int) ((Long) encodedValue).intValue();
		}
		if (lomap != null)
		{
			try
			{
				Object o = lomap.get((Long) encodedValue);
				if (o != null)
					return o;
			} catch (Exception ex)
			{
				/*
				 * Synchronization issue so synchronize. Don't synchronize by default as it slows things way down. But
				 * on the off chance that the map is being re-indexed during the get, synchronization will prevent an
				 * issue
				 */
				synchronized (lomap)
				{
					Object o = lomap.get((Long) encodedValue);
					if (o != null)
						return o;
				}
			}
		} else if (iomap != null)
		{
			try
			{
				Object o = iomap.get((Integer) encodedValue);
				if (o != null)
					return o;
			} catch (Exception ex)
			{
				// Synchronization issue so synchronize
				synchronized (iomap)
				{
					Object o = iomap.get((Integer) encodedValue);
					if (o != null)
						return o;
				}
			}
		}
		int[] result = new int[FINITE_ENTRIES];
		int count = 0;
		for (int i = 0; i < FINITE_ENTRIES; i++)
		{
			int rownum = felist[i].get(encodedValue);
			if (rownum < 0)
				break;
			result[i] = rownum;
			count++;
		}
		if (count == 1)
			return result[0];
		if (count == FINITE_ENTRIES)
			return result;
		else
		{
			int[] newresult = new int[count];
			for (int i = 0; i < count; i++)
				newresult[i] = result[i];
			return newresult;
		}
	}

	public void remove(Object encodedValue)
	{
		if (lomap != null)
		{
			if (encodedValue instanceof Integer)
				encodedValue = (long) ((Integer) encodedValue);
		} else
		{
			if (encodedValue instanceof Long)
				encodedValue = (int) ((Long) encodedValue).intValue();
		}
		for (int i = 0; i < FINITE_ENTRIES; i++)
			felist[i].remove(encodedValue);
		if (lomap != null)
			synchronized (lomap)
			{
				lomap.remove((Long) encodedValue);
			}
		else if (iomap != null)
			synchronized (iomap)
			{
				iomap.remove((Integer) encodedValue);
			}
	}

	@SuppressWarnings(
	{ "unchecked" })
	public void addRowNum(Object key, int rowNum)
	{
		Object entry = null;
		boolean islong = lomap != null;
		if (islong)
		{
			if (key instanceof Integer)
				key = (long) ((Integer) key);
			entry = lomap.get((Long) key);
		} else
		{
			if (key instanceof Long)
				key = (int) ((Long) key).intValue();
			entry = iomap.get((Integer) key);
		}
		if (entry == null)
		{
			synchronized (this)
			{
				if (islong)
					entry = lomap.get((Long) key);
				else
					entry = iomap.get((Integer) key);
				if (entry == null)
				{
					int[] currentIndices = new int[FINITE_ENTRIES];
					boolean added = false;
					for (int i = 0; i < FINITE_ENTRIES; i++)
					{
						/*
						 * Continue adding rows unless not needed
						 */
						felist[i].unpackForModification();
						int result = felist[i].addRowNum(key, rowNum);
						if (result >= 0)
						{
							// An entry already exists, break if it's a dupe
							if (result == rowNum)
							{
								added = true;
								break;
							}
							currentIndices[i] = result;
						} else
						{
							// successfully added
							added = true;
							break;
						}
					}
					if (!added)
					{
						/*
						 * If we still haven't added the row to a finite key list, then we need to convert to the map
						 */
						entry = new IntArrayIndexList(currentIndices);
						((IntArrayIndexList) entry).addValueGuaranteedToBeHighest(rowNum);
						if (islong)
						{
							lomap.put((Long) key, entry);
						} else
						{
							iomap.put((Integer) key, entry);
						}
						/*
						 * Remove rows from finite element lists
						 */
						for (int i = 0; i < FINITE_ENTRIES; i++)
						{
							felist[i].removeKey(key);
						}
						return;
					}
				}
			}
		}
		/*
		 * If we've reached this point, then we need to add the rownum to a multi-value SurrogateKeyToRowListEntry
		 */
		if (entry != null)
			synchronized (entry)
			{
				IntArrayIndexList indexList = null;
				if (entry instanceof DifferencePackedIntegerList)
				{
					synchronized (this)
					{
						if (lomap != null)
						{
							entry = lomap.get((Long) key);
							if (entry instanceof DifferencePackedIntegerList)
							{
								indexList = new IntArrayIndexList(((DifferencePackedIntegerList) entry).getIntArrayList());
								lomap.put((Long) key, indexList);
							} else
								indexList = (IntArrayIndexList) entry;
						} else
						{
							entry = iomap.get((Integer) key);
							if (entry instanceof DifferencePackedIntegerList)
							{
								indexList = new IntArrayIndexList(((DifferencePackedIntegerList) entry).getIntArrayList());
								iomap.put((Integer) key, indexList);
							} else
								indexList = (IntArrayIndexList) entry;
							iomap.put((Integer) key, indexList);
						}
					}
				} else if (entry instanceof int[])
				{
					synchronized (this)
					{
						if (lomap != null)
						{
							entry = lomap.get((Long) key);
							if (entry instanceof int[])
							{
								indexList = new IntArrayIndexList((int[]) entry);
								lomap.put((Long) key, indexList);
							} else
								indexList = (IntArrayIndexList) entry;
						} else
						{
							entry = iomap.get((Integer) key);
							if (entry instanceof int[])
							{
								indexList = new IntArrayIndexList((int[]) entry);
								iomap.put((Integer) key, indexList);
							} else
								indexList = (IntArrayIndexList) entry;
							iomap.put((Integer) key, indexList);
						}
					}
				} else
					indexList = (IntArrayIndexList) entry;
				indexList.addValueGuaranteedToBeHighest(rowNum);
			}
	}

	public int size()
	{
		int size = lomap != null ? lomap.size() : iomap.size();
		for (int i = 0; i < FINITE_ENTRIES; i++)
			size += felist[i].size();
		return size;
	}

	@SuppressWarnings("unchecked")
	public void pack(boolean majorPack)
	{
		if (!majorPack)
			return;
		if (lomap != null)
		{
			PackList pl = new PackList();
			lomap.forEachEntry(pl);
		} else if (iomap != null)
		{
			IntPackList ipl = new IntPackList();
			iomap.forEachEntry(ipl);
		}
		if (felist != null)
		{
			for (int i = 0; i < FINITE_ENTRIES; i++)
				felist[i].pack();
		}
	}

	@SuppressWarnings("rawtypes")
	private class PackList implements TLongObjectProcedure
	{
		public PackList()
		{
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute(long key, Object value)
		{
			if (value instanceof IntArrayIndexList)
			{
				int len = ((IndexList) value).size();
				if (len > 4)
					lomap.put(key, new DifferencePackedIntegerList((IndexList) value));
				else if (len > 0)
					// For small lists, overhead of differencepacked list is too large
					lomap.put(key, ((IndexList) value).getIntArrayList().toArray());
				else
					lomap.remove(key);
			}
			return true;
		}
	}

	@SuppressWarnings("rawtypes")
	private class IntPackList implements TIntObjectProcedure
	{
		public IntPackList()
		{
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute(int key, Object value)
		{
			if (value instanceof IntArrayIndexList)
			{
				int len = ((IndexList) value).size();
				if (len > 4)
					iomap.put(key, new DifferencePackedIntegerList((IndexList) value));
				else if (len > 0)
					// For small lists, overhead of differencepacked list is too large
					iomap.put(key, ((IndexList) value).getIntArrayList().toArray());
				else
					iomap.remove(key);
			}
			return true;
		}
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeInt(FINITE_ENTRIES);
		out.writeObject(lomap);
		out.writeObject(iomap);
		out.writeObject(felist);
	}

	@SuppressWarnings("rawtypes")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		FINITE_ENTRIES = in.readInt();
		lomap = (TLongObjectMap) in.readObject();
		iomap = (TIntObjectMap) in.readObject();
		felist = (FiniteEntryList[]) in.readObject();
	}

	/*
	 * Delete by only copying rows that exist after the delete (non-Javadoc)
	 * 
	 * @see memdb.storage.KeyIndexMap#deleteRows(gnu.trove.map.TIntIntMap, gnu.trove.set.TIntSet,
	 * gnu.trove.set.TLongSet)
	 */
	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		for (int i = 0; i < FINITE_ENTRIES; i++)
			felist[i].unpackForModification();
		synchronized (this)
		{
			for (int i = 0; i < FINITE_ENTRIES; i++)
				felist[i].deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
			if (lomap != null)
			{
				TLongObjectMap newlomap = new TLongObjectHashMap(lomap.size());
				lomap.forEachEntry(new delLongList(newlomap, mapOfOldRowNumbersToNewNumbersAfterDelete));
				lomap = newlomap;
			}
			if (iomap != null)
			{
				TIntObjectMap newiomap = new TIntObjectHashMap(iomap.size());
				iomap.forEachEntry(new delIntList(newiomap, mapOfOldRowNumbersToNewNumbersAfterDelete));
				iomap = newiomap;
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private class delLongList implements TLongObjectProcedure
	{
		TLongObjectMap newlomap;
		TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete;

		public delLongList(TLongObjectMap newlomap, TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
		{
			this.newlomap = newlomap;
			this.mapOfOldRowNumbersToNewNumbersAfterDelete = mapOfOldRowNumbersToNewNumbersAfterDelete;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute(long key, Object olist)
		{
			if (olist == null)
				return true;
			if (olist instanceof IndexList)
			{
				IndexList list = (IndexList) olist;
				int len = list.size();
				IntArrayIndexList newlist = new IntArrayIndexList();
				for (int i = 0; i < len; i++)
				{
					int rownum = list.get(i);
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rownum))
					{
						newlist = (IntArrayIndexList) newlist.addValueGuaranteedToBeHighest(mapOfOldRowNumbersToNewNumbersAfterDelete.get(rownum));
					}
				}
				if (newlist.size() != 0)
					newlomap.put(key, newlist);
			} else
			{
				int[] list = (int[]) olist;
				int len = list.length;
				IntArrayIndexList newlist = new IntArrayIndexList();
				for (int i = 0; i < len; i++)
				{
					int rownum = list[i];
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rownum))
					{
						newlist = (IntArrayIndexList) newlist.addValueGuaranteedToBeHighest(mapOfOldRowNumbersToNewNumbersAfterDelete.get(rownum));
					}
				}
				if (newlist.size() != 0)
					newlomap.put(key, newlist);
			}
			return true;
		}
	}

	@SuppressWarnings("rawtypes")
	private class delIntList implements TIntObjectProcedure
	{
		TIntObjectMap newiomap;
		TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete;

		public delIntList(TIntObjectMap newiomap, TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
		{
			this.newiomap = newiomap;
			this.mapOfOldRowNumbersToNewNumbersAfterDelete = mapOfOldRowNumbersToNewNumbersAfterDelete;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute(int key, Object olist)
		{
			if (olist == null)
				return true;
			if (olist instanceof IndexList)
			{
				IndexList list = (IndexList) olist;
				int len = list.size();
				IntArrayIndexList newlist = new IntArrayIndexList();
				for (int i = 0; i < len; i++)
				{
					int rownum = list.get(i);
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rownum))
					{
						newlist = (IntArrayIndexList) newlist.addValueGuaranteedToBeHighest(mapOfOldRowNumbersToNewNumbersAfterDelete.get(rownum));
					}
				}
				if (newlist.size() != 0)
					newiomap.put(key, newlist);
			} else
			{
				int[] list = (int[]) olist;
				int len = list.length;
				IntArrayIndexList newlist = new IntArrayIndexList();
				for (int i = 0; i < len; i++)
				{
					int rownum = list[i];
					if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rownum))
					{
						newlist = (IntArrayIndexList) newlist.addValueGuaranteedToBeHighest(mapOfOldRowNumbersToNewNumbersAfterDelete.get(rownum));
					}
				}
				if (newlist.size() != 0)
					newiomap.put(key, newlist);
			}
			return true;
		}
	}

	@SuppressWarnings(
	{ "unchecked" })
	public void updateList(Object key, Object value)
	{
		if (lomap != null)
			lomap.put((Long) key, value);
		else
			iomap.put((Integer) key, value);
	}

	public Iterator<Object> getKeyIterator()
	{
		RowKeyIterator rkit = new RowKeyIterator();
		return rkit;
	}

	public int getKeyIteratorSize()
	{
		int result = 0;
		for (int i = 0; i < FINITE_ENTRIES; i++)
			result += felist[i].getKeyIteratorSize();
		if (lomap != null)
			result += lomap.size();
		else
			result += iomap.size();
		return result;
	}

	/**
	 * Iterator to get a list of unique keys
	 * 
	 * @author bpeters
	 * 
	 */
	public class RowKeyIterator implements Iterator<Object>
	{
		private int phase;
		private AtomicInteger curIndex = new AtomicInteger();
		private int[] uniqueSizes;
		private TIntIterator tintit;
		private TLongIterator tlongit;

		public RowKeyIterator()
		{
			if (lomap != null)
				tlongit = lomap.keySet().iterator();
			else
				tintit = iomap.keySet().iterator();
			uniqueSizes = new int[FINITE_ENTRIES];
			for (int i = 0; i < FINITE_ENTRIES; i++)
				uniqueSizes[i] = felist[i].size();
		}

		@Override
		public boolean hasNext()
		{
			if (phase < uniqueSizes.length)
			{
				if (curIndex.get() < uniqueSizes[phase])
					return true;
			}
			if (tintit != null)
				return tintit.hasNext();
			else
				return tlongit.hasNext();
		}

		@Override
		public Object next()
		{
			if (phase < uniqueSizes.length)
			{
				int val = curIndex.getAndIncrement();
				if (val < uniqueSizes[phase])
				{
					return felist[phase].getKey(val);
				} else
					phase++;
			}
			if (tintit != null)
			{
				if (!tintit.hasNext())
					return null;
				return tintit.next();
			} else
			{
				if (!tlongit.hasNext())
					return null;
				return tlongit.next();
			}
		}

		@Override
		public void remove()
		{
		}
	}
}
