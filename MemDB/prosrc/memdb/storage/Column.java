package memdb.storage;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TObjectIntProcedure;
import gnu.trove.set.TIntSet;
import gnu.trove.set.TLongSet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.Database;
import memdb.DateParser;
import memdb.ThreadPool;
import memdb.indexlist.DifferencePackedIntegerList;
import memdb.indexlist.IndexList;
import memdb.indexlist.IntArrayIndexList;
import memdb.rowset.RowSet;
import memdb.rowset.RowSetIterator;
import memdb.sql.RowRange;
import memdb.storage.Table;
import memdb.storage.Compression.Method;
import memdb.util.Util;

import org.apache.log4j.Logger;

@SuppressWarnings("unused")
public class Column
{
	private static final long serialVersionUID = 1L;
	public static final long LONG_NAN = Double.doubleToLongBits(Double.NaN);
	private static final int ROW_INCREMENT = 100000;
	private static Logger logger = Logger.getLogger(Column.class);
	private SurrogateKeyToRowListMap rowLookupLists = null;
	private RowValueData rowValues;
	private Table table;
	private int columnNumber;
	private ColumnMetadata cmd;
	private DateParser dp = null;
	private boolean referenceColumn = false;
	private Object nullValue = null;
	private AtomicInteger referenceCount = new AtomicInteger(0);
	private long objectHashCode = (long) hashCode() << 32;
	private boolean caseSensitive = true;
	private Semaphore tablePermits = new Semaphore(Runtime.getRuntime().availableProcessors(), true);
	private boolean changed = false;
	private AtomicInteger addedUnpackedRows = new AtomicInteger(0);
	private long memUsed = -1;

	private void calcReferenceColumn()
	{
		boolean notReferenceColumn = (table.tmd.dataTypes[columnNumber] == Types.FLOAT) || (table.tmd.dataTypes[columnNumber] == Types.INTEGER)
				|| table.tmd.dataTypes[columnNumber] == Types.DECIMAL || (table.tmd.dataTypes[columnNumber] == Types.DOUBLE)
				|| (table.tmd.dataTypes[columnNumber] == Types.BIGINT) || (table.tmd.dataTypes[columnNumber] == Types.DATE)
				|| (table.tmd.dataTypes[columnNumber] == Types.TIMESTAMP) || table.tmd.noindex[columnNumber];
		this.referenceColumn = !notReferenceColumn;
		this.nullValue = getNullValue(table.tmd.dataTypes[columnNumber]);
	}

	private boolean isIndexed()
	{
		boolean noIndex = (table.tmd.dataTypes[columnNumber] == Types.FLOAT) || table.tmd.dataTypes[columnNumber] == Types.DECIMAL
				|| (table.tmd.dataTypes[columnNumber] == Types.DOUBLE) || table.tmd.noindex[columnNumber];
		return !noIndex;
	}

	public Column(Table table, int columnNumber)
	{
		this.table = table;
		this.columnNumber = columnNumber;
		this.cmd = new ColumnMetadata();
		calcReferenceColumn();
		/*
		 * Don't have row lookup indexes for column types that are not likely to be filtered by equality
		 */
		if (!isIndexed())
		{
		} else
		{
			rowLookupLists = new SurrogateKeyToRowListMap(this);
			rowLookupLists.setColumn(this);
		}
		this.cmd.setType(table.tmd.dataTypes[columnNumber]);
		this.rowValues = new RowValueData(this);
		this.caseSensitive = table.database.caseSensitive;
		changed = false;
	}

	private String getPath()
	{
		String path = table.database.path + File.separator + table.schema;
		File f = new File(path);
		if (!f.exists())
			f.mkdir();
		path += File.separator + table.name;
		f = new File(path);
		if (!f.exists())
			f.mkdir();
		path += File.separator + table.tmd.columnNames[columnNumber];
		f = new File(path);
		if (!f.exists())
			f.mkdir();
		return path;
	}

	public void drop()
	{
		if (!table.tempTable)
		{
			String path = getPath();
			File f = new File(path);
			for (File child : f.listFiles())
			{
				child.delete();
			}
			f.delete();
		}
		clear();
		changed = false;
	}

	public Column()
	{
	}

	private boolean acquireFreezeTableLock()
	{
		int numPermits = Runtime.getRuntime().availableProcessors();
		try
		{
			tablePermits.acquire(numPermits);
		} catch (InterruptedException e)
		{
			logger.error(e);
			return false;
		}
		return true;
	}

	private void releaseFreezeTableLock()
	{
		int numPermits = Runtime.getRuntime().availableProcessors();
		tablePermits.release(numPermits);
	}

	public void release()
	{
		rowValues.release();
	}

	public void readExternal(Table table, String path, int columnNumber) throws IOException, ClassNotFoundException
	{
		if (!acquireFreezeTableLock())
			return;
		try
		{
			this.table = table;
			this.columnNumber = columnNumber;
			calcReferenceColumn();
			String pathName = getPath();
			long ctime = System.currentTimeMillis();
			File f = new File(pathName);
			if (!f.exists())
				throw new IOException("Path not found: " + f.getAbsolutePath());
			f = new File(pathName + File.separator + "metadata.memdb");
			if (!f.exists())
				return;
			if (rowLookupLists != null)
				rowLookupLists = SurrogateKeyToRowListMap.readExternal(this, pathName);
			long ntime = System.currentTimeMillis();
			if (ntime - ctime > 1000)
				logger.trace(table.name + "." + table.tmd.columnNames[columnNumber] + ": surrogate key to row lookup lists: " + (ntime - ctime) / 1000.0 + "s");
			ctime = ntime;
			if (rowLookupLists != null)
				rowLookupLists.setColumn(this);
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream readFile = new ObjectInputStream(new BufferedInputStream(fis));
			cmd = (ColumnMetadata) readFile.readObject();
			fis.close();
			ntime = System.currentTimeMillis();
			if (ntime - ctime > 1000)
				logger.trace(table.name + "." + table.tmd.columnNames[columnNumber] + ": column meta data: " + (ntime - ctime) / 1000.0 + "s");
			ctime = ntime;
			rowValues = RowValueData.readExternal(this, pathName);
			rowValues.setColumn(this);
			ntime = System.currentTimeMillis();
			if (ntime - ctime > 1000)
				logger.trace(table.name + "." + table.tmd.columnNames[columnNumber] + ": row values: " + (ntime - ctime) / 1000.0 + "s");
			ctime = ntime;
			this.caseSensitive = table.database.caseSensitive;
		} finally
		{
			releaseFreezeTableLock();
		}
		changed = false;
	}

	public synchronized void save() throws IOException
	{
		if (!changed)
			return;
		if (!acquireFreezeTableLock())
			return;
		calcReferenceColumn();
		String pathName = getPath();
		File f = new File(pathName);
		if (!f.exists())
		{
			f.mkdir();
		}
		if (rowLookupLists != null)
			rowLookupLists.serializeToDisk(pathName);
		if (rowValues != null)
			rowValues.serializeToDisk(pathName);
		FileOutputStream fos = new FileOutputStream(pathName + File.separator + "metadata.memdb");
		ObjectOutputStream saveFile = new ObjectOutputStream(new BufferedOutputStream(fos));
		saveFile.writeObject(cmd);
		saveFile.flush();
		fos.close();
		releaseFreezeTableLock();
		changed = false;
	}

	public void updateMetadata() throws IOException, ClassNotFoundException
	{
		String pathName = getPath();
		if (rowLookupLists != null)
			rowLookupLists.updateMetaData(pathName);
		if (rowValues != null)
			rowValues.updateMetaData(pathName);
	}

	public void clear()
	{
		if (!acquireFreezeTableLock())
			return;
		rowValues.clear();
		rowValues = new RowValueData(this);
		if (rowLookupLists != null)
		{
			rowLookupLists.clear();
			rowLookupLists = new SurrogateKeyToRowListMap(this);
		}
		cmd = new ColumnMetadata();
		cmd.setType(table.tmd.dataTypes[columnNumber]);
		releaseFreezeTableLock();
		memUsed = -1;
		changed = true;
	}

	public synchronized void clearCache()
	{
		ObjectCache.clearForObject(rowValues);
		if (rowLookupLists != null)
			ObjectCache.clearForObject(rowLookupLists);
	}
	AtomicInteger crlvindex = new AtomicInteger(0);

	public void pack()
	{
		changed = true;
		memUsed = -1;
		pack(true);
	}

	private void pack(boolean majorPack)
	{
		if (!acquireFreezeTableLock())
			return;
		crlvindex.set(0);
		Thread rllthread = rowLookupLists != null ? rowLookupLists.asynchPack(majorPack) : null;
		Thread rvthread = rowValues.asynchPack();
		if (rllthread != null)
			rllthread.start();
		if (rvthread != null)
			rvthread.start();
		try
		{
			if (rllthread != null)
				rllthread.join();
			if (rvthread != null)
				rvthread.join();
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
		try
		{
			String path = getPath();
			if (addedUnpackedRows.get() > 100000 || majorPack)
			{
				rowValues.packToDisk(path);
				if (rowLookupLists != null)
					rowLookupLists.packToDisk(path);
				addedUnpackedRows.set(0);
			}
		} catch (IOException e)
		{
			logger.error(e);
		}
		releaseFreezeTableLock();
	}

	public RowSet getColumnIndexList(Object key, RowSet baseSet, boolean isJoin)
	{
		if (key == null)
		{
			if (this.isLongColumn())
				key = Long.MAX_VALUE;
			else
				key = Integer.MIN_VALUE;
		}
		Object encodedValue = key;
		if (isReferenceColumn())
		{
			if (!isJoin)
			{
				encodedValue = getObjectSurrogateKey(key, false);
				if (((Integer) encodedValue) == AdvancedSurrogateKeyStore.NULL_VALUE)
					return null;
			}
		} else
		{
			if (key instanceof Double)
				encodedValue = Double.doubleToLongBits((Double) key);
			else if (key instanceof Float)
				encodedValue = Float.floatToIntBits(((Float) key));
			else if (key instanceof Date)
				encodedValue = ((Date) key).getTime();
		}
		KeyIndexMap kim = getIndexList(encodedValue);
		if (kim == null)
		{
			// No row index is kept, so search
			int numRows = rowValues.getNumRows();
			TIntArrayList list = new TIntArrayList(numRows);
			if (isReferenceColumn() && isJoin)
			{
				key = table.getSurrogateKeyValue((Integer) key, columnNumber);
			}
			for (int i = 0; i < numRows; i++)
			{
				Object ko = rowValues.getRowValue(i);
				if (key.equals(ko))
					list.add(i);
			}
			return new RowSet(list);
		}
		Object entry = kim.get(encodedValue);
		if (entry == null)
			return null;
		if (entry != null)
		{
			if (entry instanceof Integer)
			{
				if (baseSet != null)
				{
					if (baseSet.contains((Integer) entry))
						return new RowSet(new int[]
						{ (Integer) entry });
					else
						return RowSet.getEmptySet();
				}
				return new RowSet(new int[]
				{ (Integer) entry });
			} else if (entry instanceof int[])
			{
				int[] arr = (int[]) entry;
				if (baseSet == null)
					return new RowSet(arr);
				TIntArrayList newList = new TIntArrayList(arr.length);
				for (int i = 0; i < arr.length; i++)
					if (baseSet.contains(arr[i]))
						newList.add(arr[i]);
				return new RowSet(newList);
			}
			return ((IndexList) entry).getRowSet(baseSet);
		} else
		{
			return new RowSet(0, getCmd().getCurrentCount() - 1);
		}
	}

	private final KeyIndexMap getIndexList(Object encodedValue)
	{
		if (rowLookupLists == null)
		{
			if (encodedValue != null)
			{
				/*
				 * No index exists so we must to a full scan
				 */
				KeyIndexMap kim = null;
				kim = new KeyIndexMap(0, table.tmd.primarykey[columnNumber], isLongColumn());
				int numRows = table.getNumRows();
				Object key = encodedValue;
				if (referenceColumn)
				{
					key = table.getSurrogateKeyValue((Integer) encodedValue, columnNumber);
				}
				for (int i = 0; i < numRows; i++)
				{
					Object val = rowValues.getRowValue(i);
					if (key.equals(val))
					{
						kim.addRowNum(encodedValue, i);
					}
				}
				return kim;
			}
			return null;
		}
		return rowLookupLists.getIndexMap(encodedValue, false);
	}

	public final Object getRowValue(int rowNum)
	{
		Object o = rowValues.getRowValue(rowNum);
		if (nullValue != null && o != null && o.equals(nullValue))
			return null;
		return o;
	}

	public final void addRow(int rownum, Object actualValue, TimeZone tz, boolean processIndices)
	{
		boolean error = false;
		changed = true;
		memUsed = -1;
		try
		{
			tablePermits.acquire();
		} catch (InterruptedException e)
		{
			logger.error(e);
			return;
		}
		int rowcount = 0;
		try
		{
			Object encodedValue = null;
			if (actualValue != null)
			{
				if (actualValue instanceof Integer)
				{
					if (isLongColumn())
						actualValue = ((Integer) actualValue).longValue();
					encodedValue = actualValue;
				} else if (actualValue instanceof Long)
				{
					encodedValue = actualValue;
				} else if (actualValue instanceof Float)
					encodedValue = Float.floatToIntBits((Float) actualValue);
				else if (actualValue instanceof Double)
					encodedValue = Double.doubleToLongBits((Double) actualValue);
				else if (actualValue instanceof Date)
					encodedValue = ((Date) actualValue).getTime();
				else if (actualValue instanceof Timestamp)
					encodedValue = ((Timestamp) actualValue).getTime();
				else if (actualValue instanceof BigDecimal)
					encodedValue = Util.getDecimalLong((BigDecimal) actualValue);
				else
				{
					if (table.tmd.dataTypes[columnNumber] == Types.VARCHAR)
					{
						if (!(actualValue instanceof String))
							actualValue = actualValue.toString();
						int len = ((String) actualValue).length();
						if (len > table.tmd.widths[columnNumber])
							table.tmd.widths[columnNumber] = len;
						encodedValue = table.getObjectSurrogateKey(actualValue, columnNumber, caseSensitive, true);
					} else if (table.tmd.dataTypes[columnNumber] == Types.DATE || table.tmd.dataTypes[columnNumber] == Types.TIMESTAMP)
					{
						if (dp == null)
							dp = new DateParser();
						Date d = null;
						synchronized (dp)
						{
							d = dp.parseUsingPossibleFormats(actualValue.toString(), tz);
						}
						if (d != null)
							encodedValue = d.getTime();
						else
							encodedValue = 0;
					} else
					{
						encodedValue = getNullValue(table.tmd.dataTypes[columnNumber]);
					}
				}
			}
			rowValues.addValue(rownum, encodedValue);
			/*
			 * Add row to the index (if building index row by row);
			 */
			if (rowLookupLists != null && actualValue != null && processIndices)
			{
				rowLookupLists.addRow(rownum, encodedValue);
			}
			rowcount = cmd.incrementAndGetRowCount();
			addedUnpackedRows.incrementAndGet();
		} catch (Exception ex)
		{
			logger.error(ex);
		} finally
		{
			tablePermits.release();
			if (rowcount % ROW_INCREMENT == 0)
			{
				long availmemory = Runtime.getRuntime().maxMemory() - (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
				boolean shrink = availmemory < 4000000000L;
				if (shrink)
				{
					shrinkCache();
				}
				boolean pack = false;
				if (rowcount > 0 && rowcount % (ROW_INCREMENT * 4) == 0)
				{
					if (rowcount % (ROW_INCREMENT * 10) == 0)
						pack = true;
				}
				if (pack)
					pack(false);
			}
		}
	}

	public static final Object getNullValue(int type)
	{
		Object val = null;
		if (type == Types.INTEGER)
		{
			val = Integer.MIN_VALUE;
		} else if (type == Types.DOUBLE)
		{
			val = LONG_NAN;
		} else if (type == Types.BIGINT)
		{
			val = Long.MAX_VALUE;
		} else if (type == Types.FLOAT)
		{
			val = Table.FLOATNULL;
		} else if (type == Types.DECIMAL)
		{
			val = null;
		} else if (type == Types.DATE || type == Types.TIMESTAMP)
		{
			val = Long.MAX_VALUE;
		} else
			val = -1;
		return val;
	}

	public final boolean isReferenceColumn()
	{
		return referenceColumn;
	}

	public final boolean isPrimaryKey()
	{
		return table.tmd.primarykey[columnNumber];
	}

	public final boolean isLongColumn()
	{
		return table.tmd.dataTypes[columnNumber] == Types.BIGINT || table.tmd.dataTypes[columnNumber] == Types.DOUBLE
				|| table.tmd.dataTypes[columnNumber] == Types.DECIMAL || table.tmd.dataTypes[columnNumber] == Types.DATE
				|| table.tmd.dataTypes[columnNumber] == Types.TIMESTAMP;
	}

	public Object lookupValue(int surrogateKey)
	{
		if (!referenceColumn)
			return null;
		if (surrogateKey < 0)
			return null;
		return table.getSurrogateKeyValue(surrogateKey, columnNumber);
	}

	public ColumnMetadata getCmd()
	{
		return cmd;
	}

	public void setCmd(ColumnMetadata cmd)
	{
		this.cmd = cmd;
	}

	public int getObjectSurrogateKey(Object value, boolean createIfNotPresent)
	{
		if (!referenceColumn)
		{
			if (value instanceof Long)
				return ((Long) value).intValue();
			if (value instanceof Integer)
				return (Integer) value;
			return 0;
		}
		return table.getObjectSurrogateKey(value, columnNumber, caseSensitive, createIfNotPresent);
	}

	public final int getColumnNumber()
	{
		return columnNumber;
	}

	public synchronized void shrinkCache()
	{
		if (rowLookupLists != null)
			rowLookupLists.shrinkCache();
	}

	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		changed = true;
		if (!acquireFreezeTableLock())
			return;
		try
		{
			memUsed = -1;
			if (rowLookupLists != null)
				rowLookupLists.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete, isLongColumn());
			int newsize = rowValues.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
			cmd.setRowCount(newsize);
		} catch (Exception ex)
		{
			logger.error(ex);
		}
		releaseFreezeTableLock();
	}

	public void updateRow(int rowNum, Object newActual)
	{
		changed = true;
		memUsed = -1;
		Object newEncoded = null;
		if (referenceColumn && newActual != null)
			newEncoded = getObjectSurrogateKey(newActual, true);
		boolean isLong = isLongColumn();
		if (isLong)
		{
			if (newActual instanceof Integer)
				newActual = ((Integer) newActual).longValue();
			else if (newActual instanceof Date)
				newActual = ((Date) newActual).getTime();
		}
		if (newActual == null)
			newActual = getNullValue(table.tmd.dataTypes[columnNumber]);
		try
		{
			Object oldEncoded = getRowValue(rowNum);
			Object oldActual = oldEncoded;
			if (referenceColumn && oldEncoded instanceof Integer)
				oldActual = lookupValue((Integer) oldEncoded);
			if (oldActual == null)
				oldActual = getNullValue(table.tmd.dataTypes[columnNumber]);
			if (!newActual.equals(oldActual))
			{
				if (isLong)
					oldEncoded = oldActual;
				if (referenceColumn)
					rowValues.updateRow(rowNum, newActual == null ? null : newEncoded);
				else
					rowValues.updateRow(rowNum, newActual);
				if (rowLookupLists != null)
				{
					if (referenceColumn)
						rowLookupLists.updateRow(rowNum, oldEncoded, oldActual, newEncoded, newActual, isPrimaryKey());
					else
						rowLookupLists.updateRow(rowNum, oldEncoded, oldActual, newActual, newActual, isPrimaryKey());
				}
			}
		} catch (Exception ex)
		{
			logger.error(ex);
		}
	}

	public AtomicInteger getReferenceCount()
	{
		return referenceCount;
	}

	public Iterator<Object> getKeyIterator()
	{
		if (rowLookupLists != null)
		{
			return rowLookupLists.getKeyIterator();
		}
		return null;
	}

	public int getKeyIteratorSize()
	{
		if (rowLookupLists != null)
		{
			return rowLookupLists.getKeyIteratorSize();
		}
		return -1;
	}

	public String getName()
	{
		return table.tmd.columnNames[columnNumber];
	}

	public Table getTable()
	{
		return table;
	}

	@Override
	public int hashCode()
	{
		return (int) (objectHashCode >>> 32);
	}

	public void indexRange(ThreadPool tp, RowRange range)
	{
		if (rowLookupLists == null)
			return;
		acquireFreezeTableLock();
		try
		{
			if (range.start == 0 && range.stop == table.getNumRows() - 1)
			{
				/*
				 * Re-initialized the index if doing it from scratch. If the initial indexing is for a larger table, a
				 * much larger number of blocks will be used
				 */
				rowLookupLists = new SurrogateKeyToRowListMap(this);
				rowLookupLists.setColumn(this);
			}
			int numIndexBlocks = rowLookupLists.getNumIndexBlocks();
			int numBlockGroups = Runtime.getRuntime().availableProcessors();
			int blockGroupSize = numIndexBlocks / numBlockGroups / 4;
			if (blockGroupSize == 0)
				blockGroupSize = 1;
			/*
			 * Index block by block so as not to leave large numbers of unpacked blocks (which are vastly larger) in
			 * memory
			 */
			int count = 0;
			for (int indexBlock = 0; indexBlock < numIndexBlocks; indexBlock += blockGroupSize)
			{
				count++;
			}
			CountDownLatch cdl = new CountDownLatch(count);
			for (int indexBlock = 0; indexBlock < numIndexBlocks; indexBlock += blockGroupSize)
			{
				IndexRangeClass irc = new IndexRangeClass(range, indexBlock, blockGroupSize, numIndexBlocks, this, cdl);
				tp.executeLocal(irc);
			}
			cdl.await();
			try
			{
				rowLookupLists.serializeToDisk(getPath());
			} catch (IOException e)
			{
				logger.error(e);
			}
		} catch (InterruptedException e)
		{
			logger.error(e);
		} finally
		{
			this.releaseFreezeTableLock();
		}
	}

	private static class IndexRangeClass implements Runnable
	{
		private RowRange range;
		private int indexBlock;
		private int blockGroupSize;
		private int numIndexBlocks;
		private Column c;
		private CountDownLatch cdl;

		public IndexRangeClass(RowRange range, int indexBlock, int blockGroupSize, int numIndexBlocks, Column c, CountDownLatch cdl)
		{
			this.range = range;
			this.indexBlock = indexBlock;
			this.blockGroupSize = blockGroupSize;
			this.numIndexBlocks = numIndexBlocks;
			this.c = c;
			this.cdl = cdl;
		}

		@Override
		public void run()
		{
			try
			{
				RowBlockResult rbr = null;
				Object encodedValue = null;
				long startTime = System.currentTimeMillis();
				for (int rownum = range.start; rownum <= range.stop; rownum++)
				{
					if (rbr == null || rownum > rbr.stopIndex || rownum < rbr.startIndex)
						rbr = c.rowValues.getRowBlock(rownum);
					if (rbr == null)
						continue;
					if (rbr.intArrayResults != null)
						encodedValue = rbr.intArrayResults[rownum - rbr.startIndex];
					else if (rbr.longArrayResults != null)
						encodedValue = rbr.longArrayResults[rownum - rbr.startIndex];
					else
						encodedValue = rbr.results.get(rownum - rbr.startIndex);
					if (encodedValue != null)
					{
						int blockNum = c.rowLookupLists.getBlockNum(encodedValue);
						if (blockNum >= indexBlock && blockNum < (indexBlock + blockGroupSize))
						{
							c.rowLookupLists.addRow(rownum, encodedValue);
							c.changed = true;
							c.memUsed = -1;
						}
					}
				}
				for (int i = indexBlock; (i < indexBlock + blockGroupSize) && i < numIndexBlocks; i++)
					c.rowLookupLists.packIndexBlock(i);
				logger.trace("Indexed column " + c.columnNumber + " block group " + (indexBlock / blockGroupSize) + " - "
						+ (System.currentTimeMillis() - startTime) + "ms");
			} finally
			{
				cdl.countDown();
			}
		}
	}

	public void clearIndex()
	{
		rowLookupLists.clear();
	}

	public long getMemoryUsed()
	{
		if (memUsed >= 0)
			return memUsed;
		memUsed = 0;
		if (rowLookupLists != null)
			memUsed += rowLookupLists.getMemoryUsed(table);
		if (rowValues != null)
			memUsed += rowValues.getMemoryUsed(table);
		return memUsed;
	}
}
