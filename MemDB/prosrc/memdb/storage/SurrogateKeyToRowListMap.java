package memdb.storage;

import gnu.trove.map.TIntIntMap;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import memdb.Main;
import memdb.indexlist.IndexList;
import memdb.indexlist.IntArrayIndexList;
import memdb.storage.Compression.Method;

import org.apache.log4j.Logger;

public class SurrogateKeyToRowListMap extends BlockStore implements BlockStoreUser
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Main.class);
	private static int NUM_BLOCKS_TINY = 8; // 4 bit - Tiny tables (minimizes overhead - saves most space)
	private static int NUM_BLOCKS_SMALL = 64; // 7 bit - Small tables (minimizes overhead - saves some space)
	private static int NUM_BLOCKS_LARGE = 2048; // 11 bit - large tables (performance - overhead well amortized)
	private Column c;

	public SurrogateKeyToRowListMap(Column c)
	{
		super(c.getTable().getNumRows() > 1000000 ? NUM_BLOCKS_LARGE : (c.getTable().getNumRows() > 25000 ? NUM_BLOCKS_SMALL : NUM_BLOCKS_TINY),
				c.getTable().tmd.diskBased[c.getColumnNumber()], c.getTable().tmd.compressed[c.getColumnNumber()], Method.SNAPPY_NO_DESERIALIZE);
		this.c = c;
	}

	public void clear()
	{
		super.clearBlockStore();
	}

	public void shrinkCache()
	{
	}

	public int getNumIndexBlocks()
	{
		return blocks.length;
	}

	public static SurrogateKeyToRowListMap readExternal(Column c, String path) throws IOException, ClassNotFoundException
	{
		SurrogateKeyToRowListMap sm = new SurrogateKeyToRowListMap(c);
		String metadataFileName = path + File.separator + "memdb.rowsmd";
		sm.readExternal(metadataFileName, path + File.separator + "m%.rows");
		return sm;
	}

	public void updateMetaData(String path) throws IOException, ClassNotFoundException
	{
		String metadataFileName = path + File.separator + "memdb.rowsmd";
		boolean changed = setDiskBased(c.getTable().tmd.diskBased[c.getColumnNumber()]);
		if (changed)
		{
			writeMetaData(metadataFileName);
			readExternal(metadataFileName, path + File.separator + "m%.rows");
		}
	}

	public final KeyIndexMap getIndexMap(Object key, boolean pinForUpdate)
	{
		int hash = key.hashCode();
		if (blocks.length == NUM_BLOCKS_LARGE)
			hash = (hash & 0x7FF) ^ ((hash >>> 11) & 0x7FF) ^ ((hash >>> 22) & 0x7FF); // 11 bit
		else if (blocks.length == NUM_BLOCKS_SMALL)
			hash = ((hash & 0x3F) ^ ((hash >>> 7) & 0x3F) ^ ((hash >>> 14) & 0x3F)) & 0x3F; // 7 bit
		else
			hash = ((hash & 0x7) ^ ((hash >>> 4) & 0x7) ^ ((hash >>> 8) & 0x7) ^ ((hash >>> 12) & 0x7)) & 0x7; // 3 bit
		return (KeyIndexMap) getBlockEntry(hash, pinForUpdate, false);
	}

	public int getBlockNum(Object key)
	{
		int hash = key.hashCode();
		if (blocks.length == NUM_BLOCKS_LARGE)
			hash = (hash & 0x7FF) ^ ((hash >>> 11) & 0x7FF) ^ ((hash >>> 22) & 0x7FF); // 11 bit
		else if (blocks.length == NUM_BLOCKS_SMALL)
			hash = ((hash & 0x3F) ^ ((hash >>> 7) & 0x3F) ^ ((hash >>> 14) & 0x3F)) & 0x3F; // 7 bit
		else
			hash = ((hash & 0x7) ^ ((hash >>> 4) & 0x7) ^ ((hash >>> 8) & 0x7) ^ ((hash >>> 12) & 0x7)) & 0x7; // 3 bit
		return hash;
	}

	public void serializeToDisk(String path) throws IOException
	{
		super.serializeToDisk(path + File.separator + "memdb.rowsmd", path + File.separator + "m%.rows");
	}

	public void packToDisk(String path) throws IOException
	{
		if (!isDiskBased())
			return;
		// If disk-based, serialize regularly
		super.serializeToDisk(path + File.separator + "memdb.rowsmd", path + File.separator + "m%.rows");
	}

	public Thread asynchPack(boolean majorPack)
	{
		/*
		 * Don't compress primary key values
		 */
		if (!c.getTable().tmd.primarykey[c.getColumnNumber()] && !c.getTable().tmd.foreignkey[c.getColumnNumber()])
		{
			return new PackRowLookupLists(this, majorPack);
		}
		return null;
	}

	private class PackRowLookupLists extends Thread
	{
		private boolean majorPack = false;
		private SurrogateKeyToRowListMap sklm;

		public PackRowLookupLists(SurrogateKeyToRowListMap sklm, boolean majorPack)
		{
			this.sklm = sklm;
			this.majorPack = majorPack;
		}

		public void run()
		{
			int count = 0;
			while (true)
			{
				if (!sklm.packBlock(count++, majorPack))
					break;
			}
		}
	}

	public void packIndexBlock(int blockNum)
	{
		if (!c.getTable().tmd.primarykey[c.getColumnNumber()] && !c.getTable().tmd.foreignkey[c.getColumnNumber()])
			packBlock(blockNum, true);
	}

	/**
	 * Take a given list of row numbers and add a result - returning a potentially different kind of object containing
	 * the new list
	 * 
	 * @param rownum
	 * @param rowlist
	 * @return
	 */
	public void addRow(int rownum, Object encodedValue)
	{
		KeyIndexMap kim = null;
		kim = getIndexMap(encodedValue, true);
		kim.addRowNum(encodedValue, rownum);
	}

	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete, boolean islong)
	{
		ObjectCache.clearForObject(this);
		KeyIndexMap kim = null;
		for (int i = 0; i < numBlocks(); i++)
		{
			kim = (KeyIndexMap) getBlockEntry(i, true, false);
			if (kim != null)
				kim.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
		}
		Thread t = asynchPack(true);
		if (t != null)
			t.run();
	}

	public void updateRow(int rowNum, Object oldEncoded, Object oldActual, Object newEncoded, Object newActual, boolean primarykey)
	{
		if (oldActual != null && oldEncoded != null)
		{
			if (oldActual instanceof Date)
				oldActual = ((Date) oldActual).getTime();
			if (newActual instanceof Date)
				newActual = ((Date) newActual).getTime();
			KeyIndexMap kim = null;
			synchronized (this)
			{
				kim = getIndexMap(oldEncoded, true);
				// kim.unpackForModification();
			}
			Object entry = kim.get(oldEncoded);
			if (entry != null)
			{
				if (entry instanceof Integer)
				{
					synchronized (kim)
					{
						kim.remove(oldEncoded);
					}
				} else if (entry instanceof int[])
				{
					synchronized (kim)
					{
						if (((int[]) entry).length == 1)
							kim.remove(oldEncoded);
						else
						{
							IntArrayIndexList newlist = new IntArrayIndexList((int[]) entry);
							newlist.remove(rowNum);
							kim.updateList(oldEncoded, newlist);
						}
					}
				} else
				{
					synchronized (entry)
					{
						IndexList list = (IndexList) entry;
						IndexList newlist = list.remove(rowNum);
						synchronized (kim)
						{
							if (newlist.size() == 0)
								kim.remove(oldEncoded);
							else if (list != newlist)
								kim.updateList(oldEncoded, newlist);
						}
					}
				}
			}
		}
		if (newEncoded != null)
		{
			KeyIndexMap newkim = null;
			synchronized (this)
			{
				newkim = getIndexMap(newEncoded, true);
			}
			newkim.addRowNum(newEncoded, rowNum);
		}
	}

	public Column getColumn()
	{
		return c;
	}

	public void setColumn(Column c)
	{
		this.c = c;
	}

	public Iterator<Object> getKeyIterator()
	{
		RowKeyIterator rkit = new RowKeyIterator();
		return rkit;
	}

	public int getKeyIteratorSize()
	{
		int result = 0;
		for (int i = 0; i < numBlocks(); i++)
		{
			KeyIndexMap kim = (KeyIndexMap) getBlockEntry(i, false, false);
			result += kim.getKeyIteratorSize();
		}
		return result;
	}

	/**
	 * An iterator of row keys
	 * 
	 * @author bpeters
	 * 
	 */
	public class RowKeyIterator implements Iterator<Object>
	{
		private int curBlock;
		private Iterator<Object> curIterator;

		public RowKeyIterator()
		{
			curBlock = 0;
		}

		@Override
		public boolean hasNext()
		{
			if (curBlock < numBlocks())
			{
				if (curIterator == null)
				{
					KeyIndexMap kim = (KeyIndexMap) getBlockEntry(curBlock, false, false);
					curIterator = kim.getKeyIterator();
				}
				if (curIterator.hasNext())
					return true;
				if (curBlock < numBlocks() - 1)
				{
					for (int i = curBlock + 1; i < numBlocks(); i++)
					{
						KeyIndexMap kim = (KeyIndexMap) getBlockEntry(i, false, false);
						if (kim.getKeyIteratorSize() > 0)
							return true;
					}
				}
			}
			return false;
		}

		@Override
		public Object next()
		{
			Object result = null;
			if (curIterator == null)
			{
				KeyIndexMap kim = (KeyIndexMap) getBlockEntry(curBlock, false, false);
				curIterator = kim.getKeyIterator();
			}
			do
			{
				result = curIterator.next();
				if (result == null)
				{
					if ((++curBlock) < numBlocks())
					{
						KeyIndexMap kim = (KeyIndexMap) getBlockEntry(curBlock, false, false);
						curIterator = kim.getKeyIterator();
					}
				}
			} while (curBlock < numBlocks() && result == null);
			return result;
		}

		@Override
		public void remove()
		{
		}
	}

	@Override
	public Object getInitializedBlock()
	{
		if (c == null)
			return null;
		return new KeyIndexMap(c.getTable().getNumRows(), c.isPrimaryKey(), c.isLongColumn());
	}

	@Override
	public void packBlockImpl(Object block, boolean majorPack)
	{
		((KeyIndexMap) block).pack(majorPack);
	}

	@Override
	public int getNumRows()
	{
		if (c == null)
			return -1;
		return c.getTable().getNumRows();
	}

	@Override
	public int getEstimatedNumRows()
	{
		if (c == null)
			return -1;
		return c.getTable().getEstimatedRows();
	}
}
