package memdb.storage;

import gnu.trove.list.array.TIntArrayList;

import java.io.Serializable;

public class PackedStringBlock implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public byte[] array;
	public TIntArrayList indexList;
}
