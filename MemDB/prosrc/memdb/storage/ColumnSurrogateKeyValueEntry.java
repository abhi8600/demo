package memdb.storage;

import java.io.Serializable;

public class ColumnSurrogateKeyValueEntry implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int columnCode;
	public Object value;
}
