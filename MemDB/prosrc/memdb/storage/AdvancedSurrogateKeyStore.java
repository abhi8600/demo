package memdb.storage;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import memdb.Database;

import org.apache.log4j.Logger;

public class AdvancedSurrogateKeyStore extends SurrogateKeyStore
{
	public static int NULL_VALUE = Integer.MIN_VALUE;
	private static Logger logger = Logger.getLogger(AdvancedSurrogateKeyStore.class);
	private SurrogateKeyValueLookup keyLookup;
	private Database database;
	private AtomicBoolean changed = new AtomicBoolean();
	private String storeName = "common";

	public void initialize(Database database, String storeName, boolean diskBased, boolean compressed) throws ClassNotFoundException, IOException
	{
		this.database = database;
		if (storeName != null)
			this.storeName = storeName;
		keyLookup = new SurrogateKeyValueLookup(diskBased, compressed);
		readExternal();
	}

	private String getPath()
	{
		String path = database.path + File.separator + storeName;
		File f = new File(path);
		f = new File(path);
		if (storeName != null && !f.exists())
			f.mkdir();
		return path;
	}

	public void writeExternal() throws IOException
	{
		synchronized (this)
		{
			if (!changed.get())
				return;
			int numPermits = Runtime.getRuntime().availableProcessors();
			try
			{
				keyLookup.lock.acquire(numPermits);
			} catch (InterruptedException e)
			{
				logger.error(e);
				return;
			}
			String pathName = getPath();
			keyLookup.serializeToDisk(pathName);
			keyLookup.lock.release(numPermits);
			changed.set(false);
		}
	}

	public void delete()
	{
		File f = new File(getPath());
		if (f.exists() && f.isDirectory())
		{
			f.delete();
		}
	}

	private void readExternal() throws IOException, ClassNotFoundException
	{
		synchronized (this)
		{
			String pathName = getPath();
			File f = new File(pathName);
			if (!f.exists())
				f.mkdir();
			keyLookup.readExternal(pathName);
		}
		changed.set(false);
	}

	public synchronized void pack()
	{
		synchronized (this)
		{
			int numPermits = Runtime.getRuntime().availableProcessors();
			try
			{
				keyLookup.lock.acquire(numPermits);
			} catch (InterruptedException e)
			{
				logger.error(e);
				return;
			}
			Thread skeyLookupThread = keyLookup.asynchPack();
			skeyLookupThread.start();
			try
			{
				skeyLookupThread.join();
			} catch (InterruptedException e)
			{
				logger.error(e);
			}
			keyLookup.lock.release(numPermits);
		}
		changed.set(true);
	}

	public int getObjectSurrogateKey(Object value, Table table, int columnNumber, boolean caseSensitive, boolean createIfNotPresent)
	{
		int result = keyLookup.getObjectSurrogateKey(value, createIfNotPresent, table, columnNumber, !caseSensitive);
		if (createIfNotPresent && (result >= 0 || !caseSensitive))
			changed.set(true);
		if (result == AdvancedSurrogateKeyStore.NULL_VALUE)
			return result;
		// Negative means not new
		return result < 0 ? -result : result;
	}

	public Object getSurrogateKeyValue(int surrogateKey, Table table, int columnNumber)
	{
		return keyLookup.getSurrogateKeyValue(surrogateKey, table, columnNumber);
	}

	@Override
	public String getName()
	{
		return storeName;
	}
}
