package memdb.storage;

import gnu.trove.list.TIntList;
import gnu.trove.list.TLongList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TLongIntMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongIntHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;

import java.io.File;
import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import memdb.storage.Compression.Method;

import org.apache.log4j.Logger;

public class RowValueData extends BlockStore implements BlockStoreUser
{
	private static Logger logger = Logger.getLogger(RowValueData.class);
	private static final int BLOCK_SIZE_BITS = 12;
	private static final int BLOCK_LENGTH = 1 << BLOCK_SIZE_BITS;
	@SuppressWarnings("unused")
	private int type;
	private boolean isLong;
	private long objectHashCode = (long) ObjectHash.getObjectHash() << 32;
	private Column c;
	private int dataType;
	// precision for dates and times (in ms) - less precision yields higher compression
	private int dateTimePrecision;
	// Fast cache to ensure that each thread can access sequentially without using the more expensive LRU cache
	private TLongIntMap lastBlockNumbers;
	@SuppressWarnings("rawtypes")
	private TLongObjectMap lastBlocks;

	@SuppressWarnings("rawtypes")
	public RowValueData(Column c)
	{
		super(-1, c.getTable().tmd.diskBased[c.getColumnNumber()], c.getTable().tmd.compressed[c.getColumnNumber()], Method.SNAPPY_NO_DESERIALIZE);
		super.setSelfSynchronize(true);
		if (c != null)
		{
			this.isLong = c.isLongColumn();
			this.c = c;
			if (c.getCmd() != null)
				this.type = c.getCmd().getType();
			Table t = c.getTable();
			this.dataType = t.tmd.dataTypes[c.getColumnNumber()];
			// Make sure we can process Birst transactional queries with the right precision
			if (t.name.equals("TXN_COMMAND_HISTORY"))
				dateTimePrecision = 1;
			else
				dateTimePrecision = t.database.dateTimePrecision;
			lastBlockNumbers = new TLongIntHashMap(Runtime.getRuntime().availableProcessors(), 0.75f, Long.MIN_VALUE, Integer.MIN_VALUE);
			lastBlocks = new TLongObjectHashMap();
		}
	}

	public int getNumRows()
	{
		return c.getTable().getNumRows();
	}

	public static RowValueData readExternal(Column c, String path) throws IOException, ClassNotFoundException
	{
		RowValueData rvd = new RowValueData(c);
		String metadataFileName = path + File.separator + "memdb.valuemd";
		rvd.readExternal(metadataFileName, path + File.separator + "m%.values");
		return rvd;
	}

	public void updateMetaData(String path) throws IOException, ClassNotFoundException
	{
		String metadataFileName = path + File.separator + "memdb.valuemd";
		boolean changed = setDiskBased(c.getTable().tmd.diskBased[c.getColumnNumber()]);
		if (changed)
		{
			writeMetaData(metadataFileName);
			readExternal(metadataFileName, path + File.separator + "m%.values");
		}
	}

	public void release()
	{
		synchronized (lastBlockNumbers)
		{
			lastBlockNumbers.clear();
			lastBlocks.clear();
		}
	}

	public void packToDisk(String path) throws IOException
	{
		if (!isDiskBased())
			return;
		// If disk-based, serialize regularly
		serializeToDisk(path);
	}

	public void serializeToDisk(String path) throws IOException
	{
		super.serializeToDisk(path + File.separator + "memdb.valuemd", path + File.separator + "m%.values");
		synchronized (lastBlockNumbers)
		{
			lastBlockNumbers.clear();
			lastBlocks.clear();
		}
	}

	public void setColumn(Column c)
	{
		if (c != null)
		{
			this.c = c;
			this.isLong = c.isLongColumn();
			if (c.getCmd() != null)
			{
				this.type = c.getCmd().getType();
			}
			this.dataType = c.getTable().tmd.dataTypes[c.getColumnNumber()];
		}
	}

	public void clear()
	{
		clearBlockStore();
		synchronized (lastBlockNumbers)
		{
			lastBlockNumbers.clear();
			lastBlocks.clear();
		}
	}

	public Thread asynchPack()
	{
		return new PackRowValues();
	}

	private class PackRowValues extends Thread
	{
		public void run()
		{
			synchronized (lastBlockNumbers)
			{
				lastBlockNumbers.clear();
				lastBlocks.clear();
			}
			for (int i = 0; i < numBlocks(); i++)
			{
				packBlock(i, true);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Object getRowValue(int rowNum)
	{
		int blockNum = rowNum >>> BLOCK_SIZE_BITS;
		int index = rowNum & ((1 << BLOCK_SIZE_BITS) - 1);
		if (blockNum >= numBlocks())
			return null;
		long tid = Thread.currentThread().getId();
		RowBlock block = null;
		if (((BlockStore) this).diskBased)
		{
			/*
			 * Used multiple levels of caching including last-used at the thread level when disk-based. This is because
			 * access is generally sequential
			 */
			synchronized (lastBlockNumbers)
			{
				if (lastBlockNumbers.get(tid) == blockNum)
					block = (RowBlock) lastBlocks.get(tid);
			}
			if (block == null)
			{
				block = (RowBlock) getBlockEntry(blockNum, false, false);
				synchronized (lastBlockNumbers)
				{
					if (lastBlockNumbers.get(tid) == blockNum)
						block = (RowBlock) lastBlocks.get(tid);
					else
					{
						lastBlockNumbers.put(tid, blockNum);
						lastBlocks.put(tid, block);
					}
				}
			}
		} else
		{
			block = (RowBlock) getBlockEntry(blockNum, false, false);
		}
		try
		{
			Object o = block.entry;
			Object result = null;
			if (o instanceof List)
			{
				result = ((List<Object>) o).get(index);
			} else if (o instanceof long[])
			{
				result = ((long[]) o)[index];
			} else if (o instanceof int[])
			{
				result = ((int[]) o)[index];
			} else if (o instanceof NullPackedIntegerList)
			{
				result = ((NullPackedIntegerList) o).get(index);
			} else if (o instanceof NullPackedLongList)
			{
				result = ((NullPackedLongList) o).get(index);
			}
			if (result instanceof Long)
			{
				if (dataType == Types.DATE)
				{
					if ((Long) result != Long.MAX_VALUE)
						result = ((Long) result) * 3600 * dateTimePrecision;
				} else if (dataType == Types.TIMESTAMP)
				{
					if ((Long) result != Long.MAX_VALUE)
						result = ((Long) result) * dateTimePrecision;
				}
			}
			return result;
		} catch (Exception ex)
		{
			logger.error(ex);
			return null;
		}
	}

	/**
	 * Optimization to return an entire block of vlaues at one time. Makes things like indexing where you need to hit a
	 * lot of values faster.
	 * 
	 * @param rowNum
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public RowBlockResult getRowBlock(int rowNum)
	{
		int blockNum = rowNum >>> BLOCK_SIZE_BITS;
		if (blockNum >= numBlocks())
			return null;
		long tid = Thread.currentThread().getId();
		RowBlock block = null;
		if (((BlockStore) this).diskBased)
		{
			/*
			 * Used multiple levels of caching including last-used at the thread level when disk-based. This is because
			 * access is generally sequential
			 */
			synchronized (lastBlockNumbers)
			{
				if (lastBlockNumbers.get(tid) == blockNum)
					block = (RowBlock) lastBlocks.get(tid);
				else
				{
					block = (RowBlock) getBlockEntry(blockNum, false, false);
					lastBlockNumbers.put(tid, blockNum);
					lastBlocks.put(tid, block);
				}
			}
		} else
		{
			block = (RowBlock) getBlockEntry(blockNum, false, false);
		}
		try
		{
			Object o = block.entry;
			RowBlockResult result = new RowBlockResult();
			result.startIndex = blockNum * BLOCK_LENGTH;
			result.stopIndex = result.startIndex + BLOCK_LENGTH - 1;
			if (o instanceof List)
			{
				if (dataType == Types.DATE || dataType == Types.TIMESTAMP)
					result.results = new ArrayList<Object>((List<Object>) o);
				else
					result.results = ((List<Object>) o);
			} else if (o instanceof long[])
			{
				result.results = new ArrayList<Object>(((long[]) o).length);
				for (int i = 0; i < ((long[]) o).length; i++)
					result.results.add(((long[]) o)[i]);
			} else if (o instanceof int[])
			{
				result.results = new ArrayList<Object>(((int[]) o).length);
				for (int i = 0; i < ((int[]) o).length; i++)
					result.results.add(((int[]) o)[i]);
			} else if (o instanceof NullPackedIntegerList)
			{
				result.intArrayResults = ((NullPackedIntegerList) o).getIntArray();
			} else if (o instanceof NullPackedLongList)
			{
				result.longArrayResults = ((NullPackedLongList) o).getLongArray();
			}
			if (isLong)
			{
				if (dataType == Types.DATE)
				{
					if (result.longArrayResults != null)
					{
						for (int i = 0; i < result.longArrayResults.length; i++)
							if ((Long) result.longArrayResults[i] != Long.MAX_VALUE)
								result.longArrayResults[i] = result.longArrayResults[i] * 3600L * ((long) dateTimePrecision);
					} else
					{
						for (int i = 0; i < result.results.size(); i++)
							if (result.results.get(i) != null && (Long) result.results.get(i) != Long.MAX_VALUE)
								result.results.set(i, ((Long) result.results.get(i)) * 3600L * ((long) dateTimePrecision));
					}
				} else if (dataType == Types.TIMESTAMP)
				{
					if (result.longArrayResults != null)
					{
						for (int i = 0; i < result.longArrayResults.length; i++)
							if ((Long) result.longArrayResults[i] != Long.MAX_VALUE)
								result.longArrayResults[i] = result.longArrayResults[i] * ((long) dateTimePrecision);
					} else
					{
						for (int i = 0; i < result.results.size(); i++)
							if (result.results.get(i) != null && (Long) result.results.get(i) != Long.MAX_VALUE)
								result.results.set(i, ((Long) result.results.get(i)) * ((long) dateTimePrecision));
					}
				}
			}
			return result;
		} catch (Exception ex)
		{
			logger.error(ex);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public int addValue(int rownum, Object val)
	{
		RowBlock block = null;
		int blockNum = rownum / BLOCK_LENGTH;
		block = (RowBlock) getBlockEntry(blockNum, true, false);
		synchronized (block)
		{
			if (block.entry instanceof NullPackedIntegerList)
			{
				block.entry = ((NullPackedIntegerList) block.entry).getObjectList();
			} else if (block.entry instanceof NullPackedLongList)
			{
				block.entry = ((NullPackedLongList) block.entry).getObjectList();
			}
			List<Object> list = ((List<Object>) block.entry);
			int index = rownum % BLOCK_LENGTH;
			int cursize = list.size();
			if (cursize <= index)
			{
				/*
				 * Expand the list if needed
				 */
				for (int i = cursize; i <= index; i++)
				{
					list.add(null);
				}
			}
			// Compact the values
			if (val instanceof Long)
			{
				if ((Long) val != Long.MAX_VALUE)
				{
					if (dataType == Types.DATE)
					{
						if (((Long) val) == 0)
							val = Long.MAX_VALUE;
						else
							val = ((Long) val) / (3600 * dateTimePrecision);
					} else if (dataType == Types.TIMESTAMP)
					{
						if (((Long) val) == 0)
							val = Long.MAX_VALUE;
						else
							val = ((Long) val) / dateTimePrecision;
					}
				}
			}
			list.set(index, val);
		}
		return rownum;
	}

	public synchronized int deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		int newsize = 0;
		synchronized (this)
		{
			TIntList newIntList = new TIntArrayList();
			TLongList newLongList = new TLongArrayList();
			for (int i = 0; i < numBlocks(); i++)
			{
				RowBlock rb = (RowBlock) getBlockEntry(i, true, false);
				Object o = rb.entry;
				if (o instanceof int[])
				{
					int[] block = (int[]) o;
					for (int j = 0; j < block.length; j++)
					{
						if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(i * BLOCK_LENGTH + j))
						{
							newIntList.add(block[j]);
						}
					}
				} else if (o instanceof long[])
				{
					long[] block = (long[]) o;
					for (int j = 0; j < block.length; j++)
					{
						if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(i * BLOCK_LENGTH + j))
						{
							newLongList.add(block[j]);
						}
					}
				} else if (o instanceof List)
				{
					@SuppressWarnings("unchecked")
					List<Object> block = (List<Object>) o;
					for (int j = 0; j < block.size(); j++)
					{
						if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(i * BLOCK_LENGTH + j))
						{
							Object bo = block.get(j);
							if (bo == null)
							{
								if (isLong)
									newLongList.add(Long.MAX_VALUE);
								else
									newIntList.add(Integer.MIN_VALUE);
							} else
							{
								if (bo instanceof Integer)
									newIntList.add((Integer) bo);
								else
									newLongList.add((Long) bo);
							}
						}
					}
				} else if (o instanceof NullPackedIntegerList)
				{
					List<Object> block = ((NullPackedIntegerList) o).getObjectList();
					for (int j = 0; j < block.size(); j++)
					{
						if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(i * BLOCK_LENGTH + j))
						{
							newIntList.add((Integer) block.get(j));
						}
					}
				} else if (o instanceof NullPackedLongList)
				{
					List<Object> block = ((NullPackedLongList) o).getObjectList();
					for (int j = 0; j < block.size(); j++)
					{
						if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(i * BLOCK_LENGTH + j))
						{
							newLongList.add((Long) block.get(j));
						}
					}
				}
			}
			clearBlockStore();
			if (newIntList.size() > 0)
			{
				newsize = newIntList.size();
				for (int i = 0; i < newsize; i++)
				{
					addValue(i, newIntList.get(i));
				}
			} else
			{
				newsize = newLongList.size();
				for (int i = 0; i < newsize; i++)
				{
					addValue(i, newLongList.get(i));
				}
			}
		}
		asynchPack().run();
		ObjectCache.clearForObject(this);
		synchronized (lastBlockNumbers)
		{
			lastBlockNumbers.clear();
			lastBlocks.clear();
		}
		return newsize;
	}

	@SuppressWarnings("unchecked")
	public void updateRow(int rowNum, Object newValue)
	{
		int blockNum = rowNum >>> BLOCK_SIZE_BITS;
		int index = rowNum & ((1 << BLOCK_SIZE_BITS) - 1);
		if (blockNum >= numBlocks())
			return;
		if (isLong)
		{
			if (newValue instanceof Date)
			{
				newValue = ((Date) newValue).getTime();
			}
			if (newValue instanceof Double)
			{
				newValue = (Long) Double.doubleToLongBits((Double) newValue);
			}
		}
		if (newValue instanceof Long)
		{
			if ((Long) newValue != Long.MAX_VALUE)
			{
				if (dataType == Types.DATE)
					newValue = ((Long) newValue) / (3600 * dateTimePrecision);
				if (dataType == Types.TIMESTAMP)
					newValue = ((Long) newValue) / dateTimePrecision;
			}
		}
		RowBlock block = (RowBlock) getBlockEntry(blockNum, true, false);
		synchronized (block)
		{
			if (block.entry instanceof List)
			{
				((List<Object>) block.entry).set(index, newValue);
				return;
			} else if (block.entry instanceof int[])
			{
				if (newValue instanceof Integer)
					((int[]) block.entry)[index] = ((Integer) newValue);
				return;
			} else if (block.entry instanceof long[])
			{
				if (newValue instanceof Long)
					((long[]) block.entry)[index] = ((Long) newValue);
				return;
			} else if (block.entry instanceof NullPackedIntegerList)
			{
				List<Object> newlist = ((NullPackedIntegerList) block.entry).getObjectList();
				block.entry = newlist;
				if (newValue instanceof Integer)
					newlist.set(index, (Integer) newValue);
				return;
			} else if (block.entry instanceof NullPackedLongList)
			{
				List<Object> newlist = ((NullPackedLongList) block.entry).getObjectList();
				block.entry = newlist;
				if (newValue instanceof Long)
					newlist.set(index, newValue);
				return;
			}
		}
	}

	@Override
	public int hashCode()
	{
		return (int) (objectHashCode >>> 32);
	}

	@Override
	public Object getInitializedBlock()
	{
		return new RowBlock();
	}

	@Override
	public void packBlockImpl(Object blockObject, boolean majorPack)
	{
		RowBlock block = (RowBlock) blockObject;
		if (block.entry == null)
			return;
		if (block.entry instanceof List)
		{
			if (isLong)
			{
				@SuppressWarnings("unchecked")
				List<Long> ilist = (List<Long>) block.entry;
				long[] data = new long[ilist.size()];
				for (int j = 0; j < data.length; j++)
				{
					Object d = ilist.get(j);
					if (d != null)
					{
						if (d instanceof Long)
							data[j] = (Long) d;
						else if (d instanceof Integer)
							data[j] = ((Integer) d);
					} else
						data[j] = Long.MAX_VALUE;
				}
				block.entry = new NullPackedLongList(data);
			} else
			{
				@SuppressWarnings("unchecked")
				List<Integer> ilist = (List<Integer>) block.entry;
				int[] data = new int[ilist.size()];
				for (int j = 0; j < data.length; j++)
				{
					Integer val = ilist.get(j);
					if (val == null)
						data[j] = Integer.MIN_VALUE;
					else
						data[j] = val;
				}
				block.entry = new NullPackedIntegerList(data);
			}
		}
	}

	@Override
	public int getEstimatedNumRows()
	{
		if (c == null)
			return -1;
		return c.getTable().getEstimatedRows();
	}

	public Column getColumn()
	{
		return c;
	}
}
