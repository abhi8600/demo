/**
 * 
 */
package memdb.storage;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import memdb.storage.Compression.Method;
import memdb.util.Util;

import org.apache.log4j.Logger;

/**
 * @author bpeters
 * 
 */
public class SurrogateKeyValueLookup extends BlockStore implements BlockStoreUser
{
	private static Logger logger = Logger.getLogger(SurrogateKeyValueLookup.class);
	private static final int NUM_BITS_PER_BLOCK = 14;// 16384
	private static Charset cset = Charset.forName("UTF-8");
	private boolean changed = false;
	public Semaphore lock = new Semaphore(Runtime.getRuntime().availableProcessors(), true);
	private CSKVECompare cskveompare = new CSKVECompare();
	@SuppressWarnings("rawtypes")
	private TObjectIntMap tableColumnToKeyMap = new TObjectIntHashMap();

	public SurrogateKeyValueLookup(boolean diskBased, boolean compressed)
	{
		super(1 << NUM_BITS_PER_BLOCK, diskBased, compressed, Method.SNAPPY_NO_DESERIALIZE);
		super.setSelfSynchronize(true);
	}

	public void readExternal(String path) throws IOException, ClassNotFoundException
	{
		super.readExternal(path + File.separator + "memdb.skeysmd", path + File.separator + "m%.skeys");
		changed = false;
		return;
	}

	public void serializeToDisk(String path) throws IOException
	{
		if (!changed)
			return;
		super.serializeToDisk(path + File.separator + "memdb.skeysmd", path + File.separator + "m%.skeys");
		changed = false;
	}

	public Thread asynchPack()
	{
		changed = true;
		return new CompressValues(this);
	}
	private AtomicInteger curIndex = new AtomicInteger(0);

	private class CompressValues extends Thread
	{
		private SurrogateKeyValueLookup skvl;

		public CompressValues(SurrogateKeyValueLookup skvl)
		{
			this.skvl = skvl;
		}

		public void run()
		{
			CompressThread[] tlist = new CompressThread[Runtime.getRuntime().availableProcessors()];
			curIndex.set(0);
			synchronized (this)
			{
				for (int i = 0; i < tlist.length; i++)
				{
					tlist[i] = new CompressThread(skvl);
					tlist[i].start();
				}
				for (int i = 0; i < tlist.length; i++)
				{
					try
					{
						tlist[i].join();
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
				}
			}
		}
	}

	private class CompressThread extends Thread
	{
		SurrogateKeyValueLookup skvl;

		public CompressThread(SurrogateKeyValueLookup skvl)
		{
			this.skvl = skvl;
		}

		public void run()
		{
			while (true)
			{
				int index = curIndex.getAndIncrement();
				if (!skvl.packBlock(index, true))
					break;
			}
		}
	}

	public int getObjectSurrogateKey(Object value, boolean createIfNotPresent, Table table, int columnNumber, boolean caseInsensitive)
	{
		int hash;
		/*
		 * Hash all case insensitive values to the same entry
		 */
		if (caseInsensitive && value instanceof String)
			hash = ((String) value).toUpperCase().hashCode();
		else
			hash = value.hashCode();
		if (hash == 1)
			hash++;
		/*
		 * Don't allow negative hash codes because negatives are used for signaling
		 */
		if (hash < 0)
			hash = -hash;
		int blocknum = hash & ((1 << NUM_BITS_PER_BLOCK) - 1);
		SurrogateKeyLookupEntry skle = (SurrogateKeyLookupEntry) getBlockEntry(blocknum, createIfNotPresent, false);
		if (skle == null)
			return AdvancedSurrogateKeyStore.NULL_VALUE;
		byte[] valueArr = null;
		if (value instanceof String)
			valueArr = ((String) value).getBytes(cset);
		else
		{
			/*
			 * Presently we do not store surrogate keys for anything other than strings. When we do, we must be able to
			 * encode them as byte arrays
			 */
			return AdvancedSurrogateKeyStore.NULL_VALUE;
		}
		IntValue ahash = new IntValue();
		ahash.val = hash;
		Object storedValue = skle.getStoredValue(ahash, caseInsensitive, value, valueArr, cset);
		if (storedValue != null)
		{
			/*
			 * If already have it, don't bother synchronizing (optimization)
			 */
			if (createIfNotPresent)
			{
				if (!caseInsensitive)
					return ahash.val;
				else
				{
					String a = null;
					if (storedValue instanceof byte[])
						a = new String((byte[]) storedValue);
					else if (storedValue instanceof ColumnSurrogateKeyValueEntry[])
						a = new String((byte[]) ((ColumnSurrogateKeyValueEntry[]) storedValue)[0].value);
					if (a != null && a.equalsIgnoreCase((String) value))
						return ahash.val;
				}
			} else
				return ahash.val;
		}
		synchronized (skle)
		{
			/*
			 * If not null, synchronize and re-get
			 */
			ahash.val = hash;
			storedValue = skle.getStoredValue(ahash, caseInsensitive, value, valueArr, cset);
			hash = ahash.val;
			if (storedValue == null)
			{
				if (createIfNotPresent)
				{
					skle.setSingleValue(hash, valueArr);
					changed = true;
					return hash;
				} else
				{
					return AdvancedSurrogateKeyStore.NULL_VALUE;
				}
			} else
			{
				if (createIfNotPresent)
				{
					if (caseInsensitive && storedValue instanceof byte[] && valueArr != null && !Util.equalByteArrays((byte[]) storedValue, valueArr))
					{
						/*
						 * If this is case-insensitive search, then need to store multiple lookups, one per column
						 */
						ColumnSurrogateKeyValueEntry[] entries = new ColumnSurrogateKeyValueEntry[2];
						ColumnSurrogateKeyValueEntry cskve1 = new ColumnSurrogateKeyValueEntry();
						cskve1.columnCode = -1;
						cskve1.value = storedValue;
						ColumnSurrogateKeyValueEntry cskve2 = new ColumnSurrogateKeyValueEntry();
						cskve2.columnCode = getColumnCode(table, columnNumber);
						cskve2.value = valueArr;
						if (cskveompare.compare(cskve1, cskve2) <= 0)
						{
							entries[0] = cskve1;
							entries[1] = cskve2;
						} else
						{
							entries[1] = cskve1;
							entries[0] = cskve2;
						}
						skle.setMultiValuedKey(hash, entries);
						changed = true;
						return hash;
					} else if (storedValue instanceof ColumnSurrogateKeyValueEntry[] && valueArr != null)
					{
						/*
						 * If this is case-insensitive search, add to multiple lookup list if necessary
						 */
						ColumnSurrogateKeyValueEntry[] oldarr = (ColumnSurrogateKeyValueEntry[]) storedValue;
						ColumnSurrogateKeyValueEntry cskve = new ColumnSurrogateKeyValueEntry();
						cskve.columnCode = getColumnCode(table, columnNumber);
						cskve.value = valueArr;
						int pos = Arrays.binarySearch((ColumnSurrogateKeyValueEntry[]) storedValue, cskve, cskveompare);
						if (pos < 0)
						{
							// Add to the list (sorted)
							ColumnSurrogateKeyValueEntry[] entries = new ColumnSurrogateKeyValueEntry[oldarr.length + 1];
							int position = 0;
							for (int i = 0; i < oldarr.length; i++)
							{
								if (i == position)
								{
									if (cskveompare.compare(oldarr[i], cskve) <= 0)
										entries[position++] = oldarr[i];
									else
									{
										entries[position++] = cskve;
										entries[position++] = oldarr[i];
									}
								} else
									entries[position++] = oldarr[i];
							}
							if (position == oldarr.length)
								entries[oldarr.length] = cskve;
							skle.setMultiValuedKey(hash, entries);
							changed = true;
							return hash;
						}
					}
					// Negative value indicates that the value existed already
					return -hash;
				} else
					return hash;
			}
		}
	}

	private class CSKVECompare implements Comparator<ColumnSurrogateKeyValueEntry>
	{
		@SuppressWarnings(
		{ "unchecked", "rawtypes" })
		@Override
		public int compare(ColumnSurrogateKeyValueEntry o1, ColumnSurrogateKeyValueEntry o2)
		{
			if (o1 == null)
			{
				if (o2 == null)
					return 0;
				return -1;
			} else if (o2 == null)
				return 1;
			if (o1.columnCode == o2.columnCode || o1.columnCode == -1 || o2.columnCode == -1)
			{
				if (o1.value instanceof byte[] && o2.value instanceof byte[])
				{
					return Util.compareByteArrays((byte[]) o1.value, (byte[]) o2.value);
				}
				return ((Comparable) o1.value).compareTo((Comparable) o2.value);
			}
			return o1.columnCode < o2.columnCode ? -1 : 1;
		}
	}

	@SuppressWarnings("unchecked")
	private int getColumnCode(Table table, int columnNumber)
	{
		String key = table.schema + "." + table.name + "|" + columnNumber;
		int columnCode;
		synchronized (tableColumnToKeyMap)
		{
			if (!tableColumnToKeyMap.containsKey(key))
			{
				columnCode = tableColumnToKeyMap.size();
				tableColumnToKeyMap.put(key, columnCode);
			} else
				columnCode = tableColumnToKeyMap.get(key);
		}
		return columnCode;
	}

	public Object getSurrogateKeyValue(int surrogateKey, Table table, int columnNumber)
	{
		int blocknum = surrogateKey & ((1 << NUM_BITS_PER_BLOCK) - 1);
		SurrogateKeyLookupEntry skle = null;
		skle = (SurrogateKeyLookupEntry) getBlockEntry(blocknum, false, false);
		return skle.getValue(surrogateKey, table, columnNumber, tableColumnToKeyMap, cset);
	}

	@Override
	public Object getInitializedBlock()
	{
		return new SurrogateKeyLookupEntry(true);
	}

	@Override
	public void packBlockImpl(Object block, boolean majorPack)
	{
		((SurrogateKeyLookupEntry) block).pack();
	}

	@Override
	public int getNumRows()
	{
		return -1;
	}

	@Override
	public int getEstimatedNumRows()
	{
		return -1;
	}
}
