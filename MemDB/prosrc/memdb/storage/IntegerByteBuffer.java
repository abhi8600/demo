package memdb.storage;

import java.util.ArrayList;
import java.util.List;

public class IntegerByteBuffer
{
	private List<Byte> byteList = null;
	private byte[] buffer = null;
	private int index = 0;

	public void initialize()
	{
		byteList = new ArrayList<Byte>(1000);
	}

	public void initialize(byte[] buffer)
	{
		this.buffer = buffer;
		index = 0;
	}

	public void addIntegerToList(long l)
	{
		byte byte4 = (byte) ((l & 0xFF000000L) >>> 24);
		byte byte3 = (byte) ((l & 0x00FF0000L) >>> 16);
		byte byte2 = (byte) ((l & 0x0000FF00L) >>> 8);
		byte byte1 = (byte) ((l & 0x000000FFL));
		byteList.add(byte4);
		byteList.add(byte3);
		byteList.add(byte2);
		byteList.add(byte1);
	}

	public void addNonNegativeIntegerToList(long l)
	{
		long nl = -l;
		if (l >= 0 && l <= 0xFFL)
		{
			byteList.add((byte) (Byte.MIN_VALUE + 1));
			byte byte1 = (byte) ((l & 0x000000FFL));
			byteList.add(byte1);
		} else if (l >= 0 && l <= 0xFFFFL)
		{
			byteList.add((byte) (Byte.MIN_VALUE + 2));
			byte byte2 = (byte) ((l & 0x0000FF00L) >>> 8);
			byte byte1 = (byte) ((l & 0x000000FFL));
			byteList.add(byte2);
			byteList.add(byte1);
		} else if (nl > 0 && nl <= 0xFFL)
		{
			byteList.add((byte) (Byte.MIN_VALUE + 3));
			byte byte1 = (byte) ((nl & 0x000000FFL));
			byteList.add(byte1);
		} else if (nl > 0 && nl <= 0xFFFFL)
		{
			byteList.add((byte) (Byte.MIN_VALUE + 4));
			byte byte2 = (byte) ((nl & 0x0000FF00L) >>> 8);
			byte byte1 = (byte) ((nl & 0x000000FFL));
			byteList.add(byte2);
			byteList.add(byte1);
		} else
		{
			byte byte4 = (byte) ((l & 0xFF000000L) >>> 24);
			byte byte3 = (byte) ((l & 0x00FF0000L) >>> 16);
			byte byte2 = (byte) ((l & 0x0000FF00L) >>> 8);
			byte byte1 = (byte) ((l & 0x000000FFL));
			byteList.add(byte4);
			byteList.add(byte3);
			byteList.add(byte2);
			byteList.add(byte1);
		}
	}

	public void addLongToList(long l)
	{
		byte byte1 = (byte) ((l & 0x00000000000000FFL));
		byte byte2 = (byte) ((l & 0x000000000000FF00L) >>> 8);
		byte byte3 = (byte) ((l & 0x0000000000FF0000L) >>> 16);
		byte byte4 = (byte) ((l & 0x00000000FF000000L) >>> 24);
		byte byte5 = (byte) ((l & 0x000000FF00000000L) >>> 32);
		byte byte6 = (byte) ((l & 0x0000FF0000000000L) >>> 40);
		byte byte7 = (byte) ((l & 0x00FF000000000000L) >>> 48);
		byte byte8 = (byte) ((l & 0xFF00000000000000L) >>> 56);
		byteList.add(byte8);
		byteList.add(byte7);
		byteList.add(byte6);
		byteList.add(byte5);
		byteList.add(byte4);
		byteList.add(byte3);
		byteList.add(byte2);
		byteList.add(byte1);
	}

	public byte[] getBuffer()
	{
		buffer = new byte[byteList.size()];
		for (int j = 0; j < byteList.size(); j++)
			buffer[j] = byteList.get(j);
		return buffer;
	}

	public int readInteger()
	{
		int b = buffer[index++];
		long result = (long) ((b < 0 ? b + 256 : b) & 0xFF) << 24;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 16;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 8;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF);
		return (int) result;
	}

	public int readNonNegativeInteger()
	{
		if (index >= buffer.length)
			return 0;
		byte b = buffer[index++];
		if (b < 0)
		{
			if (b == Byte.MIN_VALUE + 1)
			{
				long result = (buffer[index++] & 0xFF);
				return (int) result;
			} else if (b == Byte.MIN_VALUE + 2)
			{
				long result = (((long) buffer[index++]) & 0xFF) << 8;
				result += (((long) buffer[index++]) & 0xFF);
				return (int) result;
			} else if (b == Byte.MIN_VALUE + 3)
			{
				long result = (buffer[index++] & 0xFF);
				return (int) -result;
			} else if (b == Byte.MIN_VALUE + 4)
			{
				long result = (((long) buffer[index++]) & 0xFF) << 8;
				result += (((long) buffer[index++]) & 0xFF);
				return (int) -result;
			}
		}
		long result = (b & 0xFF) << 24;
		result += (buffer[index++] & 0xFF) << 16;
		result += (buffer[index++] & 0xFF) << 8;
		result += (buffer[index++] & 0xFF);
		return (int) result;
	}

	public long readLong()
	{
		if (index >= buffer.length)
			return 0;
		int b = buffer[index++];
		long result = (long) ((b < 0 ? b + 256 : b) & 0xFF) << 56;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 48;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 40;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 32;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 24;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 16;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF) << 8;
		b = buffer[index++];
		result += (long) ((b < 0 ? b + 256 : b) & 0xFF);
		return result;
	}
}
