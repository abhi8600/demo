package memdb.storage;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;

public class RowBlock implements Externalizable
{
	private static final long serialVersionUID = 1L;
	public Object entry = new ArrayList<Object>();

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject((Serializable) entry);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		entry = in.readObject();
	}
}
