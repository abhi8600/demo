package memdb.storage;

import java.util.concurrent.atomic.AtomicInteger;

public class ObjectHash
{
	private static AtomicInteger currentHash = new AtomicInteger(0);

	public static int getObjectHash()
	{
		return currentHash.getAndIncrement();
	}
}
