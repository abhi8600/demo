/**
 * 
 */
package memdb.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import memdb.LRUCache;
import memdb.LRUCacheObject;

import org.apache.log4j.Logger;

/**
 * @author bpeters
 * 
 */
public class ObjectCache implements Runnable
{
	public static int MIN_CACHE_SIZE = 1000;
	public static int LOW_MEM_MIN_CACHE_SIZE = 500;
	public static int LOW_MEM = 500000000;
	public static int SMALL_MEM = 1000000000;
	public static double FREE_RATIO = 0.4; // Free memory if less than 40% available
	private static Logger logger = Logger.getLogger(ObjectCache.class);
	public static LRUCache<Long, Object> cache = new LRUCache<Long, Object>(MIN_CACHE_SIZE, false);

	/*
	 * Cache key should be objectHashCode + value.hashCode()
	 */
	public static void clearForObject(Object typeObject)
	{
		int code = typeObject.hashCode();
		Set<Entry<Long, LRUCacheObject<Object>>> entrySet = cache.getEntrySet();
		List<Long> keys = new ArrayList<Long>();
		for (Entry<Long, LRUCacheObject<Object>> e : entrySet)
		{
			Long key = e.getKey();
			if (key >>> 32 == code)
			{
				keys.add(key);
			}
		}
		for (Long key : keys)
			cache.remove(key);
	}

	public void run()
	{
		// Shrink/grow cache periodically if running low/high on memory
		long freemem = Runtime.getRuntime().maxMemory() + Runtime.getRuntime().freeMemory() - Runtime.getRuntime().totalMemory();
		logger.trace(freemem);
		double freeRatio = (double) freemem / Runtime.getRuntime().maxMemory();
		if (freeRatio < FREE_RATIO || freemem < SMALL_MEM)
		{
			System.gc();
			if (freeRatio < FREE_RATIO || freemem < SMALL_MEM)
			{
				int cacheSize = cache.getCacheSize();
				int lastCacheSize = cacheSize;
				cacheSize = cacheSize * 7 / 10;
				if (cacheSize < LOW_MEM_MIN_CACHE_SIZE && freemem < LOW_MEM)
					cacheSize = LOW_MEM_MIN_CACHE_SIZE;
				else if (cacheSize < MIN_CACHE_SIZE)
					cacheSize = MIN_CACHE_SIZE;
				if (cacheSize != lastCacheSize)
				{
					logger.debug("Shrinking cache:" + freemem + '-' + cacheSize);
					cache.setCacheSize(cacheSize);
				}
			}
		}
	}
}
