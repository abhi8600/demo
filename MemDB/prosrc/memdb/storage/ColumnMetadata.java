package memdb.storage;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.concurrent.atomic.AtomicInteger;

public class ColumnMetadata implements Externalizable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AtomicInteger rowCount = new AtomicInteger();
	private int type;
	private boolean transientColumn = false;

	public int getCurrentCount()
	{
		return rowCount.get();
	}

	public void setRowCount(int currentCount)
	{
		this.rowCount.set(currentCount);
	}

	public void incrementRowCount()
	{
		this.rowCount.incrementAndGet();
	}

	public int getAndIncrementRowCount()
	{
		return rowCount.getAndIncrement();
	}

	public int incrementAndGetRowCount()
	{
		return rowCount.incrementAndGet();
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public boolean isTransientColumn()
	{
		return transientColumn;
	}

	public void setTransientColumn(boolean transientColumn)
	{
		this.transientColumn = transientColumn;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		rowCount = (AtomicInteger) in.readObject();
		type = in.readInt();
		transientColumn = in.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(rowCount);
		out.writeInt(type);
		out.writeBoolean(transientColumn);
	}
}
