package memdb.storage;

import java.io.Serializable;

public class PackedColumnToSurrogateKeyMap implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public byte[] packedObjects;
	public byte[] packedSurrogateKeys;
}
