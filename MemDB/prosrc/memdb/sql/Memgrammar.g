grammar Memgrammar;

options 
{
	output=AST;
	ASTLabelType=CommonTree;
	language=Java;
}

tokens
{
	NEGATE;
	PAREN;
	PROJECTIONLIST;
	PROJECTION;
	SCHEMA;
	TABLEALIAS;
	COLLIST;
	SUBQUERY;
}

@lexer::header { package memdb.sql; }
@header { package memdb.sql; }

@rulecatch 
{
	catch (RecognitionException e) 
	{
		throw e;
	}
} 

@lexer::members
{
	List<RecognitionException> exceptions = new ArrayList<RecognitionException>();

	@Override
	public void reportError(RecognitionException e)
	{
	    exceptions.add(e);
	}

	public boolean hasError()
	{
		return exceptions.size() > 0;
	}

	public RecognitionException errorMessage()
	{
		return exceptions.get(0);
	}
}

@members
{
    public void displayRecognitionError(String[] tokenNames,
                                        RecognitionException e) {
        String hdr = getErrorHeader(e);
        String msg = getErrorMessage(e, tokenNames);
    }
}

STRING
    	:  	'\'' ( EscapeSequence | (options {greedy=false;} : ~('\u0000'..'\u001f' | '\\' | '\'' ) ) )* '\'';


INTEGER	
	:	'-'? ('0'..'9')+;

FLOAT
	:	'-'? ('0'..'9')* '.' ('0'..'9')+;

DATETIME
 	:	'#' (~'#')* '#' | (('N'|'n') ('O'|'o') ('W'|'w'));

BOOLEAN
	:	('T'|'t') ('R'|'r') ('U'|'u') ('E'|'e')
	|	('F'|'f') ('A'|'a') ('L'|'l') ('S'|'s') ('E'|'e');

NULL	:	('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');

OR 	: 	'||' | (('O'|'o') ('R'|'r'));

AND 	: 	'&&' | (('A'|'a') ('N'|'n') ('D'|'d'));

EQUALS	
	:	'=' | '==';
NOTEQUALS 
	:	'!=' | '<>';
ISNULL:
	('I'|'i') ('S'|'s') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
ISNOTNULL
	:
	('I'|'i') ('S'|'s') WS ('N'|'n') ('O'|'o') ('T'|'t') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
LT	:	'<';
LTEQ	:	'<=';
GT	:	'>';
GTEQ	:	'>=';
PLUS	:	'+';
MINUS	:	'-';
MULT	:	'*';
DIV	:	'/';
MOD	:	'%';
POW	:	'^';
NOT	:	'!' | ('N'|'n') ('O'|'o') ('T'|'t');

SELECT	:	('S'|'s') ('E'|'e') ('L'|'l') ('E'|'e') ('C'|'c') ('T'|'t');
SHOW	:	('S'|'s') ('H'|'h') ('O'|'o') ('W'|'w');
TABLES	:	('T'|'t') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e') ('S'|'s');
SYSTEMSTATUS:
		('S'|'s')('Y'|'y') ('S'|'s') ('T'|'t') ('E'|'e') ('M'|'m') WS ('S'|'s') ('T'|'t') ('A'|'a') ('T'|'t') ('U'|'u') ('S'|'s');
DROP	:	('D'|'d') ('R'|'r') ('O'|'o') ('P'|'p');
IFEXISTS:	('I'|'i') ('F'|'f') WS ('E'|'e') ('X'|'x') ('I'|'i') ('S'|'s') ('T'|'t') ('S'|'s');
TRUNCATE:	('T'|'t') ('R'|'r') ('U'|'u') ('N'|'n') ('C'|'c') ('A'|'a') ('T'|'t') ('E'|'e');
TABLE	:	('T'|'t') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
RENAME	:	('R'|'r') ('E'|'e') ('N'|'n') ('A'|'a') ('M'|'m') ('E'|'e');
DELETE	:	('D'|'d') ('E'|'e') ('L'|'l') ('E'|'e') ('T'|'t') ('E'|'e');
INSERT	:	('I'|'i') ('N'|'n') ('S'|'s') ('E'|'e') ('R'|'r') ('T'|'t');
UPDATE	:	('U'|'u') ('P'|'p') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e');
CREATE	:	('C'|'c') ('R'|'r') ('E'|'e') ('A'|'a') ('T'|'t') ('E'|'e');
EXIT	:	('E'|'e') ('X'|'x') ('I'|'i') ('T'|'t');
QUIT	:	('Q'|'q') ('U'|'u') ('I'|'i') ('T'|'t');
KILL	:	('K'|'k') ('I'|'i') ('L'|'l') ('L'|'l');
ALTER	:	('A'|'a') ('L'|'l') ('T'|'t') ('E'|'e') ('R'|'r');
PROCESSLIST
	:	('P'|'p') ('R'|'r') ('O'|'o') ('C'|'c') ('E'|'e') ('S'|'s') ('S'|'s') ('L'|'l') ('I'|'i') ('S'|'s') ('T'|'t');
	
SET	:	('S'|'s') ('E'|'e') ('T'|'t');
FLUSH	:	('F'|'f') ('L'|'l') ('U'|'u') ('S'|'s') ('H'|'h');
INTO	:	('I'|'i') ('N'|'n') ('T'|'t') ('O'|'o');
VALUES	:	('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e') ('S'|'s');
INFOTABLES
	:	'INFORMATION_SCHEMA.TABLES';
REMOTEQUERY
	:	('R'|'r') ('E'|'e') ('M'|'m') ('O'|'o') ('T'|'t') ('E'|'e') ('Q'|'q') ('U'|'u') ('E'|'e') ('R'|'r') ('Y'|'y');
LOADDATAINFILE	
	:	('L'|'l') ('O'|'o') ('A'|'a') ('D'|'d') WS ('D'|'d') ('A'|'a') ('T'|'t') ('A'|'a') WS ('I'|'i') ('N'|'n') ('F'|'f') ('I'|'i') ('L'|'l') ('E'|'e');
OUTFILE	:	('O'|'o') ('U'|'u') ('T'|'t') ('F'|'f') ('I'|'i') ('L'|'l') ('E'|'e');
FIELDS	:	('L'|'l') ('I'|'i') ('N'|'n') ('E'|'e') ('S'|'s');
COLUMNS	:	('C'|'c') ('O'|'o') ('L'|'l') ('U'|'u') ('M'|'m') ('N'|'n') ('S'|'s');
COLUMN	:	('C'|'c') ('O'|'o') ('L'|'l') ('U'|'u') ('M'|'m') ('N'|'n');
ADD	:	('A'|'a') ('D'|'d') ('D'|'d');
ENCLOSEDBY
	:	('E'|'e') ('N'|'n') ('C'|'c') ('L'|'l') ('O'|'o') ('S'|'s') ('E'|'e') ('D'|'d') WS ('B'|'b') ('Y'|'y');
DELIMITEDBY
	:	('D'|'d') ('E'|'e') ('L'|'l') ('I'|'i') ('M'|'m') ('I'|'i') ('T'|'t') ('E'|'e') ('D'|'d') WS ('B'|'b') ('Y'|'y');
TERMINATEDBY
	:	('T'|'t') ('E'|'e') ('R'|'r') ('M'|'m') ('I'|'i') ('N'|'n') ('A'|'a') ('T'|'t') ('E'|'e') ('D'|'d') WS ('B'|'b') ('Y'|'y');
PRIMARYKEY
	:	('P'|'p') ('R'|'r') ('I'|'i') ('M'|'m') ('A'|'a') ('R'|'r') ('Y'|'y') WS ('K'|'k') ('E'|'e') ('Y'|'y');
FOREIGNKEY
	:	('F'|'f') ('O'|'o') ('R'|'r') ('E'|'e') ('I'|'i') ('G'|'g') ('N'|'n')  WS ('K'|'k') ('E'|'e') ('Y'|'y');
SURROGATEKEYS:	
		('S'|'s') ('U'|'u') ('R'|'r') ('R'|'r') ('O'|'o') ('G'|'g') ('A'|'a') ('T'|'t') ('E'|'e') WS ('K'|'k') ('E'|'e') ('Y'|'y') ('S'|'s');
SKIPFIRST
	:	('S'|'s') ('K'|'k') ('I'|'i') ('P'|'p') ('F'|'f') ('I'|'i') ('R'|'r') ('S'|'s') ('T'|'t');
NOINDEX	:	('N'|'n') ('O'|'o') ('I'|'i') ('N'|'n') ('D'|'d') ('E'|'e') ('X'|'x');
	
FROM	:	('F'|'f') ('R'|'r') ('O'|'o') ('M'|'m');
WHERE	:	('W'|'w') ('H'|'h') ('E'|'e') ('R'|'r') ('E'|'e');
ORDERBY	:	('O'|'o') ('R'|'r') ('D'|'d') ('E'|'e') ('R'|'r') WS ('B'|'b') ('Y'|'y');
GROUPBY	:	('G'|'g') ('R'|'r') ('O'|'o') ('U'|'u') ('P'|'p') WS ('B'|'b') ('Y'|'y');
HAVING	:	('H'|'h') ('A'|'a') ('V'|'v') ('I'|'i') ('N'|'n') ('G'|'g');
INNERJOIN
	:	('I'|'i') ('N'|'n') ('N'|'n') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
LEFTOUTERJOIN
	:	('L'|'l') ('E'|'e') ('F'|'f') ('T'|'t') WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
RIGHTOUTERJOIN
	:	('R'|'r') ('I'|'i') ('G'|'g') ('H'|'h') ('T'|'t') WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
FULLOUTERJOIN
	:	('F'|'f') ('U'|'u') ('L'|'l') ('L'|'l') WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
	
MAXONE	:	('M'|'m') ('A'|'a') ('X'|'x') ('O'|'o') ('N'|'n') ('E'|'e');
DISTINCTFORKEY
	:	('D'|'d') ('I'|'i') ('S'|'s') ('T'|'t') ('I'|'i') ('N'|'n') ('C'|'c') ('T'|'t') ('F'|'f') ('O'|'o') ('R'|'r') ('K'|'k') ('E'|'e') ('Y'|'y');
TRANSIENT
	:	('T'|'t') ('R'|'r') ('A'|'a') ('N'|'n') ('S'|'s') ('I'|'i') ('E'|'e') ('N'|'n') ('T'|'t');
	
ON	:	('O'|'o') ('N'|'n');
IN	:	('I'|'i') ('N'|'n');
NOTIN	:	('N'|'n') ('O'|'o') ('T'|'t') WS ('I'|'i') ('N'|'n');
NOTLIKE	:	('N'|'n') ('O'|'o') ('T'|'t') WS ('L'|'l') ('I'|'i') ('K'|'k') ('E'|'e');
LIKE	:	('L'|'l') ('I'|'i') ('K'|'k') ('E'|'e');

SUM	:	('S'|'s') ('U'|'u') ('M'|'m');
AVG	:	('A'|'a') ('V'|'v') ('G'|'g');
COUNT	:	('C'|'c') ('O'|'o') ('U'|'u') ('N'|'n') ('T'|'t');
DISTINCT	:
		 ('D'|'d') ('I'|'i') ('S'|'s') ('T'|'t') ('I'|'i') ('N'|'n') ('C'|'c') ('T'|'t');
MIN	:	('M'|'m') ('I'|'i') ('N'|'n');
MAX	:	('M'|'m') ('A'|'a') ('X'|'x');
CASE	:	('C'|'c') ('A'|'a') ('S'|'s') ('E'|'e');
WHEN	:	('W'|'w') ('H'|'h') ('E'|'e') ('N'|'n');
THEN	:	('T'|'t') ('H'|'h') ('E'|'e') ('N'|'n');
ELSE	:	('E'|'e') ('L'|'l') ('S'|'s') ('E'|'e');
END	:	('E'|'e') ('N'|'n') ('D'|'d');
TOP	:	('T'|'t') ('O'|'o') ('P'|'p');

YEAR	:	('Y'|'y') ('E'|'e') ('A'|'a') ('R'|'r');	
QUARTER	:	('Q'|'q') ('U'|'u') ('A'|'a') ('R'|'r') ('T'|'t') ('E'|'e') ('R'|'r');
MONTH	:	('M'|'m') ('O'|'o') ('N'|'n') ('T'|'t') ('H'|'h');
WEEK	:	('W'|'w') ('E'|'e') ('E'|'e') ('K'|'k');
DAY	:	('D'|'d') ('A'|'a') ('Y'|'y');
	
HOUR	:	('H'|'h') ('O'|'o') ('U'|'u') ('R'|'r');	
MINUTE	:	('M'|'m') ('I'|'i') ('N'|'n') ('U'|'u') ('T'|'t') ('E'|'e');
SECOND	:	('S'|'s') ('E'|'e') ('C'|'c') ('O'|'o') ('N'|'n') ('D'|'d');

VARCHARTYPE	:	('V'|'v') ('A'|'a') ('R'|'r') ('C'|'c') ('H'|'h') ('A'|'a') ('R'|'r');
INTEGERTYPE	:	('I'|'i') ('N'|'n') ('T'|'t') ('E'|'e') ('G'|'g') ('E'|'e') ('R'|'r');
BIGINTTYPE	:	('B'|'b') ('I'|'i') ('G'|'g') ('I'|'i') ('N'|'n') ('T'|'t');
FLOATTYPE	:	('F'|'f') ('L'|'l') ('O'|'o') ('A'|'a') ('T'|'t');
DECIMALTYPE	:	('D'|'d') ('E'|'e') ('C'|'c') ('I'|'i') ('M'|'m') ('A'|'a') ('L'|'l');
DATETYPE	:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e');
DATETIMETYPE	:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e');

TIMESTAMPADD
	:	('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e') ('S'|'s') ('T'|'t') ('A'|'a') ('M'|'m') ('P'|'p') ('A'|'a') ('D'|'d') ('D'|'d');
TIMESTAMPDIFF
	:	('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e') ('S'|'s') ('T'|'t') ('A'|'a') ('M'|'m') ('P'|'p') ('D'|'d') ('I'|'i') ('F'|'f') ('F'|'f');
EXTRACT:	('E'|'e') ('X'|'x') ('T'|'t') ('R'|'r') ('A'|'a') ('C'|'c') ('T'|'t');
DATEDIFF:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('D'|'d') ('I'|'i') ('F'|'f') ('F'|'f');
DATEADD:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('A'|'a') ('D'|'d') ('D'|'d');
DATEPART:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('P'|'p') ('A'|'a') ('R'|'r') ('T'|'t');
GETDATE	:	('G'|'g') ('E'|'e') ('T'|'t') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e');
SUBSTR:	 	('S'|'s') ('U'|'u') ('B'|'b') ('S'|'s') ('T'|'t') ('R'|'r') ('I'|'i') ('N'|'n') ('G'|'g');
LENGTH:	 	('L'|'l') ('E'|'e') ('N'|'n') ('G'|'g') ('T'|'t') ('H'|'h');
LEN:	 	('L'|'l') ('E'|'e') ('N'|'n');
POSITION:	('P'|'p') ('O'|'o') ('S'|'s') ('I'|'i') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');
OBJECTSIZE
	:	('O'|'o') ('B'|'b') ('J'|'j') ('E'|'e') ('C'|'c') ('T'|'t') ('S'|'s') ('I'|'i') ('Z'|'z') ('E'|'e');
TOUPPER:	('U'|'u') ('P'|'p') ('P'|'p') ('E'|'e') ('R'|'r');
TOLOWER:	('L'|'l') ('O'|'o') ('W'|'w') ('E'|'e') ('R'|'r');
ISNULLEXP:	('I'|'i') ('S'|'s') ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
COALESCE:	('C'|'c') ('O'|'o') ('A'|'a') ('L'|'l') ('E'|'e') ('S'|'s') ('C'|'c') ('E'|'e');

CAST	:	('C'|'c') ('A'|'a') ('S'|'s') ('T'|'t');
DEBUG	:	('D'|'d') ('E'|'e') ('B'|'b') ('U'|'u') ('G'|'g');

DUMPTABLE	:	('D'|'d') ('U'|'u') ('M'|'m') ('P'|'p') ('T'|'t') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
LOADTABLE	:	('L'|'l') ('O'|'o') ('A'|'a') ('D'|'d') ('T'|'t') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
DUMPDDL	:	('D'|'d') ('U'|'u') ('M'|'m') ('P'|'p') ('D'|'d') ('D'|'d') ('L'|'l');
LOADDDL	:	('L'|'l') ('O'|'o') ('A'|'a') ('D'|'d') ('D'|'d') ('D'|'d') ('L'|'l');
DUMPDB	:	('D'|'d') ('U'|'u') ('M'|'m') ('P'|'p') ('D'|'d') ('B'|'b');
LOADDB	:	('L'|'l') ('O'|'o') ('A'|'a') ('D'|'d') ('D'|'d') ('B'|'b');

OUTDIR	:	('O'|'o') ('U'|'u') ('T'|'t') ('D'|'d') ('I'|'i') ('R'|'r');
INDIR	:	('I'|'i') ('N'|'n') ('D'|'d') ('I'|'i') ('R'|'r');
TO	:	('T'|'t') ('O'|'o');
DISK
	:	('D'|'d') ('I'|'i') ('S'|'s') ('K'|'k');
MEMORY	:	('M'|'m') ('E'|'e') ('M'|'m') ('O'|'o') ('R'|'r') ('Y'|'y');
COMPRESSED
	:	('C'|'c') ('O'|'o') ('M'|'m') ('P'|'p') ('R'|'r') ('E'|'e') ('S'|'s') ('S'|'s') ('E'|'e') ('D'|'d');
UNCOMPRESSED
	:	('U'|'u') ('N'|'n') ('C'|'c') ('O'|'o') ('M'|'m') ('P'|'p') ('R'|'r') ('E'|'e') ('S'|'s') ('S'|'s') ('E'|'e') ('D'|'d');
DISKKEYSTORE
	:	('D'|'d') ('I'|'i') ('S'|'s') ('K'|'k') ('K'|'k') ('E'|'e') ('Y'|'y') ('S'|'s') ('T'|'t') ('O'|'o') ('R'|'r') ('E'|'e');
COMPRESSEDKEYSTORE
	:	('C'|'c') ('O'|'o') ('M'|'m') ('P'|'p') ('R'|'r') ('E'|'e') ('S'|'s') ('S'|'s') ('E'|'e') ('D'|'d') ('K'|'k') ('E'|'e') ('Y'|'y') ('S'|'s') ('T'|'t') ('O'|'o') ('R'|'r') ('E'|'e');	
		
LEFT_PAREN: 	'(';
RIGHT_PAREN: 	')';
	
PARAMETER
	:	'?';
	
ASCENDING
	:	('A'|'a') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;
	
DESCENDING
	:	('D'|'d') ('E'|'e') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;

AS	:	('A'|'a') ('S'|'s');

IDENT
	:	('a'..'z' | 'A'..'Z' | '_' ) ('a'..'z' | 'A'..'Z' | '_' | ':' | '$' | '#' | '0'..'9')*;

fragment EscapeSequence 
	:	'\\'
  	(	
  		'n' 
	|	'r' 
	|	't'
	|	'\'' 
	|	'\\'
	|	UnicodeEscape
	);

fragment UnicodeEscape
    	:    	'u' HexDigit HexDigit HexDigit HexDigit ;

fragment HexDigit 
	: 	('0'..'9'|'a'..'f'|'A'..'F') ;

/* Ignore white spaces */	
WS	
	:  (' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;};

// Comments
LINE_COMMENT 
	: '//' .* '\n' {$channel=HIDDEN;};
ML_COMMENT
	:   '/*' (options {greedy=false;} : .)* '*/' {$channel=HIDDEN;};

	
statementlist
	:	(statement ';')+; 

statement
	:	selectquery | dropquery | showquery | createquery | insertquery | loaddata | deletequery | updatequery | alterquery | 
		QUIT^ | EXIT^ | FLUSH^ | kill | dumptable | loadtable | dumpddl | loadddl | dumpdb | loaddb | renametable;

loaddata:	LOADDATAINFILE STRING INTO TABLE (schema '.')? ident loadoption* collist?-> ^(LOADDATAINFILE STRING ^(ident schema?) loadoption* collist?);

loadoption
	:	delimitedby | enclosedby | terminatedby | SKIPFIRST;

delimitedby
	:	DELIMITEDBY^ STRING;
	
enclosedby
	:	ENCLOSEDBY^ STRING;
	
terminatedby
	:	TERMINATEDBY^ STRING;
	
kill	:	KILL^ INTEGER;

// RemoteQuery('driverclass','connectstring','username','password','query')
createquery
	:	CREATE TABLE (schema '.')? ident AS REMOTEQUERY LEFT_PAREN STRING ',' STRING ',' STRING ',' STRING ',' STRING RIGHT_PAREN primarykeyspec? ->
		^(CREATE ^(ident schema?) ^(REMOTEQUERY STRING STRING STRING STRING STRING) primarykeyspec?) |
		CREATE TABLE (schema '.')? ident AS selectquery -> ^(CREATE ^(ident schema?) selectquery) |
		CREATE TABLE (schema '.')? ident LEFT_PAREN columnDefinition (',' columnDefinition)* RIGHT_PAREN TRANSIENT? DISK? MEMORY? COMPRESSED? UNCOMPRESSED? DISKKEYSTORE? COMPRESSEDKEYSTORE? -> 
		^(CREATE ^(ident schema?) columnDefinition+ TRANSIENT? DISK? MEMORY? COMPRESSED? UNCOMPRESSED? DISKKEYSTORE? COMPRESSEDKEYSTORE?);

alterquery
	:	ALTER TABLE (schema '.')? ident alterdef (',' alterdef )* -> ^(ALTER ^(ident schema?) alterdef+);
		
insertquery
	:	INSERT INTO (schema '.')? ident collist? selectquery -> ^(INSERT ^(ident schema?) collist? selectquery) |
		INSERT INTO (schema '.')? ident collist? VALUES LEFT_PAREN insertliteral (',' insertliteral)* RIGHT_PAREN -> ^(INSERT ^(ident schema?) collist? ^(VALUES insertliteral*)) |
		INSERT INTO (schema '.')? ident collist? REMOTEQUERY STRING STRING STRING STRING STRING -> ^(INSERT ^(ident schema?) collist? ^(REMOTEQUERY STRING STRING STRING STRING STRING));
		
insertliteral
	:	literal | PARAMETER;
		
collist	:	LEFT_PAREN colspec (',' colspec)* RIGHT_PAREN -> ^(COLLIST colspec+);
		
literal:	INTEGER | FLOAT | STRING | BOOLEAN | DATETIME | NULL;
		
columnType
	:	INTEGERTYPE | VARCHARTYPE | FLOATTYPE | DATETYPE | DATETIMETYPE | DECIMALTYPE | BIGINTTYPE;
		
columnDefinition
	:	IDENT columnType (LEFT_PAREN INTEGER RIGHT_PAREN)? PRIMARYKEY? FOREIGNKEY? NOINDEX? TRANSIENT? DISK? MEMORY? COMPRESSED? UNCOMPRESSED?-> 
		^(IDENT columnType INTEGER? PRIMARYKEY? FOREIGNKEY? NOINDEX? TRANSIENT? DISK? MEMORY? COMPRESSED? UNCOMPRESSED?);

alterdef:	ADD COLUMN? columnDefinition -> ^(ADD columnDefinition) |
		DROP COLUMN? IDENT -> ^(DROP IDENT) |
		ALTER COLUMN? columnDefinition -> ^(ALTER columnDefinition);

primarykeyspec
	:	PRIMARYKEY IDENT (',' IDENT)* -> ^(PRIMARYKEY IDENT IDENT*);

showquery
	:	SHOW^ TABLES | SHOW^ PROCESSLIST | SHOW^ SURROGATEKEYS | SHOW^ SYSTEMSTATUS |
		SHOW TABLE (schema '.')? ident DEBUG? -> ^(SHOW ident schema? DEBUG?);
	
dropquery
	:	DROP TABLE (schema '.')? IDENT IFEXISTS? -> ^(DROP ^(IDENT schema?) IFEXISTS?) |
		TRUNCATE TABLE (schema '.')? IDENT IFEXISTS? -> ^(TRUNCATE ^(IDENT schema?) IFEXISTS?);

selectquery
	:	SELECT top? DISTINCT? projectionlist intooutfile? from where? groupby? having? orderby?
		-> ^(SELECT projectionlist from where? groupby? having? orderby? top? DISTINCT? intooutfile?);
		
deletequery
	:	DELETE from where? -> ^(DELETE from where?);
	
updatequery
	:	UPDATE tablespec? SET (colset (',' colset)*) from where? -> ^(UPDATE ^(SET colset colset*) from where?);
	
renametable
	:	RENAME TABLE (schema '.')? ident TO (schema '.')? ident -> ^(RENAME ^(ident schema?) ^(ident schema?));
	
colset	:	colspec EQUALS logicalExpression -> ^(EQUALS colspec logicalExpression);
		
intooutfile
	:	INTO OUTFILE STRING -> ^(OUTFILE STRING);

top	:	TOP^ INTEGER;
	
from	:	FROM tablespec distinctforkey? fromjoin* -> 
		^(FROM tablespec distinctforkey? fromjoin*);
		
fromjoin:	joindir MAXONE? tablespec joinspec? -> ^(joindir tablespec joinspec? MAXONE?);
		
distinctforkey
	:	DISTINCTFORKEY LEFT_PAREN logicalExpression (',' logicalExpression)* RIGHT_PAREN ->
		^(DISTINCTFORKEY logicalExpression logicalExpression*);
		
joindir	:	INNERJOIN|LEFTOUTERJOIN|RIGHTOUTERJOIN;

tablespec:	INFOTABLES -> ^(INFOTABLES ) |
		(schema '.')? ident tablealias? -> ^(ident schema? ^(TABLEALIAS tablealias)?) |
		LEFT_PAREN selectquery RIGHT_PAREN tablealias -> ^(SUBQUERY selectquery tablealias) |
		LEFT_PAREN (schema '.')? ident RIGHT_PAREN tablealias? -> ^(ident schema? ^(TABLEALIAS tablealias)?);
		
schema:		IDENT -> ^(SCHEMA IDENT);

tablealias:	IDENT;
	
joinspec:	ON joinOr -> ^(ON  joinOr);
		

joinOr:		joinAnd (OR^ joinAnd)*;

joinAnd:	joinOn (AND^ joinOn)*;

joinOn	:	primaryJoin (snowflake^ primaryJoin)?;

snowflake
	:	ON -> AND;

primaryJoin
	:	'('! joinOr ')'!
	|	joincond ;

joincond	
	: 	equijoin;

equijoin:	joinpredicate EQUALS joinpredicate -> ^(EQUALS joinpredicate joinpredicate) ||
		colspec ISNULL -> ^(ISNULL colspec);

joinpredicate
	:	colspec | literal;
	
colspec	:	ident '.' ident '.' IDENT -> ^(IDENT ident ident) |
		ident '.' IDENT -> ^(IDENT ident) |
		IDENT;

where	:	WHERE logicalExpression -> ^(WHERE logicalExpression);

groupby	:	GROUPBY groupbyprojectionlist -> ^(GROUPBY groupbyprojectionlist);

orderby	:	ORDERBY orderbylist -> ^(ORDERBY orderbylist);

having	:	HAVING logicalExpression -> ^(HAVING logicalExpression);

direction
	:	ASCENDING | DESCENDING;

projectionlist
	:	projection (',' projection)* -> ^(PROJECTIONLIST projection*);

groupbyprojectionlist
	:	groupbyprojection (',' groupbyprojection)* -> ^(PROJECTIONLIST groupbyprojection*);

orderbylist
	:	orderbycondition (','! orderbycondition)*;

projection
	:	ident '.' ident '.' MULT -> ^(MULT ident ident) |
		ident '.' MULT -> ^(MULT ident) |
		MULT -> ^(MULT) |
		logicalExpression (AS? STRING)? -> ^(PROJECTION logicalExpression STRING?);
		
groupbyprojection
	:	logicalExpression;

orderbycondition
	:	logicalExpression  direction? -> ^(PROJECTION logicalExpression direction?);

ident	:	IDENT;

aggrule	:	SUM | AVG | COUNT | MIN | MAX;

casewhen:	CASE whenexp+ elseexp? END -> ^(CASE whenexp+ elseexp?);

whenexp	:	WHEN logicalExpression THEN logicalExpression -> ^(WHEN logicalExpression logicalExpression);

elseexp	:	ELSE! logicalExpression;

expression
	: 	logicalExpression EOF!; 
		
logicalExpression	
	:	booleanAndExpression (OR^ booleanAndExpression)*;
	
booleanAndExpression
	:	equalityExpression (AND^ equalityExpression)* ;


equalityExpression
	:	nullExpression ((EQUALS|NOTEQUALS)^ nullExpression)?;

nullExpression
	:	relationalExpression ((ISNULL|ISNOTNULL)^)?;


relationalExpression
	:	inExpression ( (LT|LTEQ|GT|GTEQ|NOTLIKE|LIKE)^ inExpression )?;

inExpression
	:	
		additiveExpression (in^ LEFT_PAREN! selectquery RIGHT_PAREN!)? |
		colspec (in LEFT_PAREN literal (',' literal)* RIGHT_PAREN) -> ^(in colspec literal literal*);

additiveExpression
	:	multiplicativeExpression ( (PLUS|MINUS)^ multiplicativeExpression )*;


multiplicativeExpression 
	:	powerExpression ( (MULT|DIV|MOD)^ powerExpression )*;
	

powerExpression 
	:	unaryExpression ( POW^ unaryExpression )*;
	

unaryExpression
	:	primaryExpression
    	|	NOT^ primaryExpression
    	|	MINUS primaryExpression -> ^(NEGATE primaryExpression);
    	  
primaryExpression
	:	value |
		LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(PAREN logicalExpression);
	
in	:	IN | NOTIN;

value	
	: 	INTEGER
	|	FLOAT
	| 	DATETIME
	|	BOOLEAN
	|	STRING
	|	casewhen
	|	aggrule LEFT_PAREN DISTINCT? logicalExpression RIGHT_PAREN -> ^(aggrule logicalExpression DISTINCT?)
	|	COUNT LEFT_PAREN '*' RIGHT_PAREN -> ^(COUNT '*')
	|	colspec
	|	datemethod
	|	stringmethod
	|	utilmethod
	|	PARAMETER
	|	NULL;
		
stringmethod
	:	substring | length | position | toupper | tolower;
	
substring
	:	SUBSTR^ '('! logicalExpression ','! logicalExpression (','! logicalExpression)? ')'!;
length	:	(LENGTH | LEN)^ '('! logicalExpression ')'!;
position
	:	POSITION^ '('! logicalExpression ','! logicalExpression ')'!;
toupper:	TOUPPER^ '('! logicalExpression ')'!;

tolower:	TOLOWER^ '('! logicalExpression ')'!;	
	
utilmethod
	:	cast | isnull | coalesce | objectsize;
	
cast	:	CAST^ LEFT_PAREN! logicalExpression AS! (INTEGERTYPE | BIGINTTYPE | FLOATTYPE | DECIMALTYPE | VARCHARTYPE | DATETYPE | DATETIMETYPE) RIGHT_PAREN!;

isnull	:	ISNULLEXP^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;

coalesce:	COALESCE LEFT_PAREN logicalExpression (',' logicalExpression)+ RIGHT_PAREN -> ^(COALESCE logicalExpression logicalExpression+);

objectsize:	OBJECTSIZE^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
	
datemethod
	:	dateadd | datediff | datepart | getdate;
	
datediff
	:	DATEDIFF^ 	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression RIGHT_PAREN! |
		TIMESTAMPDIFF^ 	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

dateadd
	:	DATEADD^ 	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression RIGHT_PAREN! |
		TIMESTAMPADD^ 	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

datepart
	:	DATEPART^ 	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) ','! logicalExpression RIGHT_PAREN! |
		EXTRACT^	LEFT_PAREN! (YEAR|QUARTER|MONTH|WEEK|DAY|HOUR|MINUTE|SECOND) FROM! logicalExpression RIGHT_PAREN!;
		
getdate	:	GETDATE^ LEFT_PAREN! RIGHT_PAREN!;

dumptable	:	DUMPTABLE (schema '.')? ident OUTDIR STRING-> ^(DUMPTABLE (schema '.')? ident STRING);
loadtable	:	LOADTABLE (schema '.')? ident INDIR STRING-> ^(LOADTABLE (schema '.')? ident STRING);

dumpddl	:	DUMPDDL (schema '.')? ident OUTDIR STRING-> ^(DUMPDDL (schema '.')? ident STRING);
loadddl	:	LOADDDL (schema '.')? ident INDIR STRING-> ^(LOADDDL (schema '.')? ident STRING);

dumpdb	:	DUMPDB OUTDIR STRING-> ^(DUMPDB STRING);
loaddb	:	LOADDB INDIR STRING-> ^(LOADDB STRING);
