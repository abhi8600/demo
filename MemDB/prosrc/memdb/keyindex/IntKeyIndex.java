package memdb.keyindex;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntIntMap;
import memdb.storage.NullPackedIntegerList;

public class IntKeyIndex extends KeyIndex
{
	private Node node;

	private static class Node
	{
		public static int MAX_SIZE = 100;
		public int split = -1;
		public Node left;
		public Node right;
		public TIntArrayList intKeyList;
		public TIntArrayList indexList;

		public Node()
		{
			intKeyList = new TIntArrayList();
			indexList = new TIntArrayList();
		}

		public Node(TIntArrayList keyList, TIntArrayList indexList)
		{
			this.intKeyList = keyList;
			this.indexList = indexList;
		}

		public int addKeyIndex(Object key, int index)
		{
			if (split >= 0)
			{
				if ((Integer) key >= split)
					return right.addKeyIndex(key, index);
				else
					return left.addKeyIndex(key, index);
			}
			int position = intKeyList.binarySearch((Integer) key);
			if (position >= 0)
			{
				return indexList.get(position);
			} else
			{
				int len = intKeyList.size();
				if (len >= MAX_SIZE)
				{
					/*
					 * Split the node
					 */
					int[] lkeyarr = intKeyList.toArray(0, len / 2);
					int[] lindexarr = indexList.toArray(0, len / 2);
					int[] rkeyarr = intKeyList.toArray(len / 2, len - (len / 2));
					int[] rindexarr = indexList.toArray(len / 2, len - (len / 2));
					left = new Node(new TIntArrayList(lkeyarr), new TIntArrayList(lindexarr));
					right = new Node(new TIntArrayList(rkeyarr), new TIntArrayList(rindexarr));
					split = rkeyarr[0];
					intKeyList = null;
					indexList = null;
				} else
				{
					/*
					 * Add to this node
					 */
					int insertpoint = -position - 1;
					if (insertpoint == intKeyList.size())
					{
						intKeyList.add((Integer) key);
						indexList.add(index);
					} else
					{
						intKeyList.insert(insertpoint, (Integer) key);
						indexList.insert(insertpoint, index);
					}
				}
				return -1;
			}
		}

		public void removeKey(Object key)
		{
			if (split >= 0)
			{
				if ((Integer) key >= split)
					right.removeKey(key);
				else
					left.removeKey(key);
				return;
			}
			int position = intKeyList.binarySearch((Integer) key);
			if (position >= 0)
			{
				intKeyList.removeAt(position);
				indexList.removeAt(position);
			}
		}

		public int getIndexForKey(Object key)
		{
			if (split >= 0)
			{
				if ((Integer) key >= split)
					return right.getIndexForKey(key);
				else
					return left.getIndexForKey(key);
			}
			int row = intKeyList.binarySearch((Integer) key);
			if (row < 0)
				return -1;
			return indexList.get(row);
		}

		public Object getKeyAtIndex(int index)
		{
			return getKeyAtIndex(index, 0);
		}

		private Object getKeyAtIndex(int index, int start)
		{
			if (split >= 0)
			{
				Object result = left.getKeyAtIndex(index, start);
				if (result instanceof Integer)
				{
					return getKeyAtIndex(index, (Integer) result);
				} else
					return result;
			}
			int len = intKeyList.size();
			if (index - start < len)
			{
				return intKeyList.get(index - start);
			} else
			{
				return getKeyAtIndex(index, start + len);
			}
		}

		public void getFullKeyList(int[] keyList)
		{
			getFullKeyList(keyList, 0);
		}

		private int getFullKeyList(int[] keyList, int start)
		{
			if (split >= 0)
			{
				int leftSize = left.getFullKeyList(keyList, start);
				int size = right.getFullKeyList(keyList, leftSize);
				return size;
			}
			int[] arr = this.intKeyList.toArray();
			for (int i = 0; i < arr.length; i++)
				keyList[start + i] = arr[i];
			return start + arr.length;
		}

		public void getFullIndexList(int[] indexList)
		{
			getFullIndexList(indexList, 0);
		}

		private int getFullIndexList(int[] indexList, int start)
		{
			if (split >= 0)
			{
				int leftSize = left.getFullIndexList(indexList, start);
				int size = right.getFullIndexList(indexList, leftSize);
				return size;
			}
			int[] arr = this.indexList.toArray();
			for (int i = 0; i < arr.length; i++)
				indexList[start + i] = arr[i];
			return start + arr.length;
		}

		public int size()
		{
			if (split >= 0)
			{
				return left.size() + right.size();
			}
			return intKeyList.size();
		}

		public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
		{
			if (split >= 0)
			{
				left.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
				right.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
				return;
			}
			TIntArrayList newIndexList = new TIntArrayList(indexList.size());
			TIntArrayList newIntKeyList = new TIntArrayList(intKeyList.size());
			for (int i = 0; i < indexList.size(); i++)
			{
				int rowNum = indexList.get(i);
				if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rowNum))
				{
					newIntKeyList.add(intKeyList.get(i));
					indexList.add(rowNum);
				}
			}
			indexList = newIndexList;
			intKeyList = newIntKeyList;
		}
	}

	public IntKeyIndex()
	{
		node = new Node();
	}

	public IntKeyIndex(NullPackedIntegerList keyList, NullPackedIntegerList packedIndexList)
	{
		TIntArrayList intKeyList = keyList.getIntArrayList();
		TIntArrayList indexList = packedIndexList.getIntArrayList();
		node = new Node();
		int len = intKeyList.size();
		for (int i = 0; i < len; i++)
		{
			node.addKeyIndex(intKeyList.get(i), indexList.get(i));
		}
	}

	@Override
	public int getIndexForKey(Object key)
	{
		return node.getIndexForKey(key);
	}

	@Override
	public Object getKeyAtIndex(int index)
	{
		return node.getKeyAtIndex(index);
	}

	@Override
	public void removeKey(Object key)
	{
		node.removeKey(key);
	}

	@Override
	public void addKeyIndex(Object key, int index)
	{
		node.addKeyIndex(key, index);
	}

	@Override
	public int size()
	{
		return node.size();
	}

	@Override
	public void sort()
	{
	}

	public NullPackedIntegerList getPackedKeys()
	{
		int[] keyArr = new int[node.size()];
		node.getFullKeyList(keyArr);
		TIntArrayList newList = new TIntArrayList(keyArr);
		return new NullPackedIntegerList(newList);
	}

	@Override
	public NullPackedIntegerList getPackedIndexes()
	{
		int[] indexArr = new int[node.size()];
		node.getFullIndexList(indexArr);
		TIntArrayList newList = new TIntArrayList(indexArr);
		return new NullPackedIntegerList(newList);
	}

	@Override
	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		node.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
	}
}
