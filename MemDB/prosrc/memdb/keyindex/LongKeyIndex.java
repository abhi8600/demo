package memdb.keyindex;

import memdb.storage.NullPackedIntegerList;
import memdb.storage.NullPackedLongList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.TIntIntMap;

public class LongKeyIndex extends KeyIndex
{
	private Node node;

	private static class Node
	{
		public static int MAX_SIZE = 100;
		public long split = -1L;
		public Node left;
		public Node right;
		public TLongArrayList longKeyList;
		public TIntArrayList indexList;

		public Node()
		{
			longKeyList = new TLongArrayList();
			indexList = new TIntArrayList();
		}

		public Node(TLongArrayList keyList, TIntArrayList indexList)
		{
			this.longKeyList = keyList;
			this.indexList = indexList;
		}

		public int addKeyIndex(Object key, int index)
		{
			if (split >= 0)
			{
				if ((Long) key >= split)
					return right.addKeyIndex(key, index);
				else
					return left.addKeyIndex(key, index);
			}
			int position = longKeyList.binarySearch((Long) key);
			if (position >= 0)
			{
				return indexList.get(position);
			} else
			{
				int len = longKeyList.size();
				if (len >= MAX_SIZE)
				{
					/*
					 * Split the node
					 */
					long[] lkeyarr = longKeyList.toArray(0, len / 2);
					int[] lindexarr = indexList.toArray(0, len / 2);
					long[] rkeyarr = longKeyList.toArray(len / 2, len - (len / 2));
					int[] rindexarr = indexList.toArray(len / 2, len - (len / 2));
					left = new Node(new TLongArrayList(lkeyarr), new TIntArrayList(lindexarr));
					right = new Node(new TLongArrayList(rkeyarr), new TIntArrayList(rindexarr));
					split = rkeyarr[0];
					longKeyList = null;
					indexList = null;
				} else
				{
					/*
					 * Add to this node
					 */
					int insertpoint = -position - 1;
					if (insertpoint == longKeyList.size())
					{
						longKeyList.add((Long) key);
						indexList.add(index);
					} else
					{
						longKeyList.insert(insertpoint, (Long) key);
						indexList.insert(insertpoint, index);
					}
				}
				return -1;
			}
		}

		public void removeKey(Object key)
		{
			if (split >= 0)
			{
				if ((Long) key >= split)
					right.removeKey(key);
				else
					left.removeKey(key);
				return;
			}
			int position = longKeyList.binarySearch((Long) key);
			if (position >= 0)
			{
				longKeyList.removeAt(position);
				indexList.removeAt(position);
			}
		}

		public int getIndexForKey(Object key)
		{
			if (split >= 0)
			{
				if ((Long) key >= split)
					return right.getIndexForKey(key);
				else
					return left.getIndexForKey(key);
			}
			int row = longKeyList.binarySearch((Long) key);
			if (row < 0)
				return -1;
			return indexList.get(row);
		}

		public Object getKeyAtIndex(int index)
		{
			return getKeyAtIndex(index, 0);
		}

		private Object getKeyAtIndex(int index, int start)
		{
			if (split >= 0)
			{
				Object result = left.getKeyAtIndex(index, start);
				if (result instanceof Integer)
				{
					return getKeyAtIndex(index, (Integer) result);
				} else
					return result;
			}
			int len = longKeyList.size();
			if (index - start < len)
			{
				return longKeyList.get(index - start);
			} else
			{
				return getKeyAtIndex(index, start + len);
			}
		}

		public void getFullKeyList(long[] keyList)
		{
			getFullKeyList(keyList, 0);
		}

		private int getFullKeyList(long[] keyList, int start)
		{
			if (split >= 0)
			{
				int leftSize = left.getFullKeyList(keyList, start);
				int size = right.getFullKeyList(keyList, leftSize);
				return size;
			}
			long[] arr = this.longKeyList.toArray();
			for (int i = 0; i < arr.length; i++)
				keyList[start + i] = arr[i];
			return start + arr.length;
		}

		public void getFullIndexList(int[] indexList)
		{
			getFullIndexList(indexList, 0);
		}

		private int getFullIndexList(int[] indexList, int start)
		{
			if (split >= 0)
			{
				int leftSize = left.getFullIndexList(indexList, start);
				int size = right.getFullIndexList(indexList, leftSize);
				return size;
			}
			int[] arr = this.indexList.toArray();
			for (int i = 0; i < arr.length; i++)
				indexList[start + i] = arr[i];
			return start + arr.length;
		}

		public int size()
		{
			if (split >= 0)
			{
				return left.size() + right.size();
			}
			return longKeyList.size();
		}

		public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
		{
			if (split >= 0)
			{
				left.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
				right.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
				return;
			}
			TIntArrayList newIndexList = new TIntArrayList(indexList.size());
			TLongArrayList newLongKeyList = new TLongArrayList(longKeyList.size());
			for (int i = 0; i < indexList.size(); i++)
			{
				int rowNum = indexList.get(i);
				if (mapOfOldRowNumbersToNewNumbersAfterDelete.containsKey(rowNum))
				{
					newLongKeyList.add(longKeyList.get(i));
					indexList.add(rowNum);
				}
			}
			indexList = newIndexList;
			longKeyList = newLongKeyList;
		}
	}

	public LongKeyIndex()
	{
		node = new Node();
	}

	public LongKeyIndex(NullPackedLongList keyList, NullPackedIntegerList packedIndexList)
	{
		TLongArrayList longKeyList = keyList.getLongArrayList();
		TIntArrayList indexList = packedIndexList.getIntArrayList();
		node = new Node();
		int len = longKeyList.size();
		for (int i = 0; i < len; i++)
		{
			node.addKeyIndex(longKeyList.get(i), indexList.get(i));
		}
	}

	@Override
	public int getIndexForKey(Object key)
	{
		return node.getIndexForKey(key);
	}

	@Override
	public Object getKeyAtIndex(int index)
	{
		return node.getKeyAtIndex(index);
	}

	@Override
	public void removeKey(Object key)
	{
		node.removeKey(key);
	}

	@Override
	public void addKeyIndex(Object key, int index)
	{
		node.addKeyIndex(key, index);
	}

	@Override
	public int size()
	{
		return node.size();
	}

	@Override
	public void sort()
	{
	}

	public NullPackedLongList getPackedKeys()
	{
		long[] keyArr = new long[node.size()];
		node.getFullKeyList(keyArr);
		TLongArrayList newList = new TLongArrayList(keyArr);
		return new NullPackedLongList(newList);
	}

	@Override
	public NullPackedIntegerList getPackedIndexes()
	{
		int[] indexArr = new int[node.size()];
		node.getFullIndexList(indexArr);
		TIntArrayList newList = new TIntArrayList(indexArr);
		return new NullPackedIntegerList(newList);
	}

	@Override
	public void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete)
	{
		node.deleteRows(mapOfOldRowNumbersToNewNumbersAfterDelete);
	}
}
