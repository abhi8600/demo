/**
 * 
 */
package memdb.keyindex;

import gnu.trove.map.TIntIntMap;
import memdb.storage.NullPackedIntegerList;

/**
 * @author bpeters
 * 
 */
public abstract class KeyIndex
{
	public abstract int getIndexForKey(Object key);

	public abstract Object getKeyAtIndex(int index);

	public abstract void removeKey(Object key);

	public abstract void addKeyIndex(Object key, int index);

	public abstract int size();

	public abstract void sort();

	public abstract NullPackedIntegerList getPackedIndexes();

	public abstract void deleteRows(TIntIntMap mapOfOldRowNumbersToNewNumbersAfterDelete);
}
