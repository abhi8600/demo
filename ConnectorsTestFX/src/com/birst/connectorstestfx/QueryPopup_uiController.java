/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx;

import com.birst.connectorstestfx.beans.Context;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author asharma
 */


public class QueryPopup_uiController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    @FXML
    public TextField objectName;
    @FXML
    public TextArea queryTextArea,mappingTextArea;
    @FXML
    public Label statusLabel;
    @FXML
    public Button cancelButton,saveButton;
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public boolean validateQueryObject(){
        if(objectName.getText().trim().length()==0){
                 queryTextArea.setText("Please enter a valid object name");
                 return false;
        }
        boolean isQueryValidated=false;
        try {
           String spaceID=Context.getInstance().getCurrentConfig().spaceID;
           String connectorControllerURI = Context.getInstance().getCurrentConfig().connectorControllerURI;
           String connectorString = Context.getInstance().getConnectorString();
           String spaceDirectory = Context.getInstance().getCurrentConfig().spaceDirectory;
           isQueryValidated = ConnectorsTestApp.validateQueryObject(Context.getInstance().getXmlCredentials(),spaceID,spaceDirectory, ConnectorsTestApp.getConnectorInfo(connectorString, spaceID, connectorControllerURI),objectName.getText().trim(),queryTextArea.getText().trim());
           System.out.println("isQueryValidated : " + isQueryValidated);
           if(isQueryValidated){
                statusLabel.setText("Query Validation Success!");
           }else{
               statusLabel.setText("Query Validation Failed!");
           }
        } catch (Exception ex) {
            Logger.getLogger(QueryPopup_uiController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isQueryValidated;
    }
    
    public void getQueryObject(){
        if(objectName.getText().trim().length()==0){
                 queryTextArea.setText("Please enter a valid object name");
                 return;
        }
        try {
           String spaceID=Context.getInstance().getCurrentConfig().spaceID;
           String connectorControllerURI = Context.getInstance().getCurrentConfig().connectorControllerURI;
           String connectorString = Context.getInstance().getConnectorString();
           String spaceDirectory = Context.getInstance().getCurrentConfig().spaceDirectory;
           Map<String,String> queryMap = ConnectorsTestApp.getQueryObject(Context.getInstance().getXmlCredentials(),spaceID,spaceDirectory, ConnectorsTestApp.getConnectorInfo(connectorString, spaceID, connectorControllerURI),objectName.getText().trim());
           if(queryMap!=null && !queryMap.isEmpty()){
               String queryText = queryMap.get("Query");
               String mappingText = queryMap.get("Mapping");
               queryTextArea.setText(queryText);
               mappingTextArea.setText(mappingText);
           }else{
               statusLabel.setText("Unable to Find the Object " + objectName.getText().trim() + ". Please refer logs for details");
           }
        } catch (Exception ex) {
            Logger.getLogger(QueryPopup_uiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveQueryObject(){
        if(validateQueryObject()){
             Stage stage = (Stage) saveButton.getScene().getWindow();
             Map<String,String> queryObjectMap = new HashMap<String,String>();
             queryObjectMap.put("Name",objectName.getText().trim());
             queryObjectMap.put("Query",queryTextArea.getText().trim());
             queryObjectMap.put("Mapping",mappingTextArea.getText().trim());
             Context.getInstance().setQueryObject(queryObjectMap);
             stage.hide();
        }
    }
    
    public void cancelQueryObject(){
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
}
