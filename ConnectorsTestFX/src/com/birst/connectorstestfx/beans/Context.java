/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author asharma
 */
public class Context {
    private final static Context instance = new Context();

    public static Context getInstance() {
        return instance;
    }

    private ConfigProperties configProperties = new ConfigProperties();
    private List<Map<String, String>> connectorsList = new ArrayList<Map<String,String>>();
    private String connectorString = null;
    private String connectorName;
    private String objectsForExtraction =null;
    private List<Map<String, String>> connectorMetaData = new ArrayList<Map<String,String>>();
    private String xmlCredentials = null;
    private Map<String,String> queryObject = new HashMap<String,String>();

    public Map<String, String> getQueryObject() {
        return queryObject;
    }

    public void setQueryObject(Map<String, String> queryObject) {
        this.queryObject = queryObject;
    }
    

    public String getXmlCredentials() {
        return xmlCredentials;
    }

    public void setXmlCredentials(String xmlCredentials) {
        this.xmlCredentials = xmlCredentials;
    }

    public String getObjectsForExtraction() {
        return objectsForExtraction;
    }

    public void setObjectsForExtraction(String objectsForExtraction) {
        this.objectsForExtraction = objectsForExtraction;
    }

    
    
    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }
    
    

    public List<Map<String, String>> getConnectorMetaData() {
        return connectorMetaData;
    }

    public void setConnectorMetaData(List<Map<String, String>> connectorMetaData) {
        this.connectorMetaData = connectorMetaData;
    }

    
    public String getConnectorString() {
        return connectorString;
    }

    public void setConnectorString(String connectorString) {
        this.connectorString = connectorString;
    }

    public List<Map<String, String>> getConnectorsList() {
        return connectorsList;
    }

    public void setConnectorsList(List<Map<String, String>> connectorsList) {
        this.connectorsList = connectorsList;
    }
    
    public ConfigProperties getCurrentConfig() {
        return configProperties;
    }
    public void setCurrentConfig(ConfigProperties confProperties) {
        this.configProperties=confProperties;
    }
}