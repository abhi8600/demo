/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author asharma
 */
public class MainApplication extends Application {
    
    private static final Logger logger = Logger.getLogger(MainApplication.class);
    private static WriterAppender defaultLogfileAppender = null;
    public static final String DEFAULT_LOG_FORMAT = "%d{yyyy-MM-dd HH:mm:ss z} %-5p %m%n";
    
    private Stage stage;
    
    private static MainApplication instance;

    public MainApplication() {
        String log4jOverride = System.getProperty("jnlp.log4j.Override");
        if (log4jOverride != null && !log4jOverride.isEmpty())
        {
            if (log4jOverride.endsWith(".xml"))
            {
                DOMConfigurator.configure(log4jOverride);
            }
            else if (log4jOverride.endsWith(".properties"))
            {
                PropertyConfigurator.configure(log4jOverride);
            }
            else
            {
                setBasicConfiguration();
            }
        }
        else
        {
            setBasicConfiguration();
        }
        
        instance = this;
    }

    public static MainApplication getInstance() {
        return instance;
    }
    
    public  Stage getStage() {
        return stage;
    }
    
    
     
     private void setBasicConfiguration()
    {
        PatternLayout ly = new PatternLayout(DEFAULT_LOG_FORMAT);
        defaultLogfileAppender = new ConsoleAppender(ly);
        BasicConfigurator.configure(defaultLogfileAppender);
        Logger.getRootLogger().setLevel(Level.DEBUG);        
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        setBasicConfiguration();
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("Birst_ui.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
     public void showTestInterface() throws Exception {
        replaceSceneContent("Test_ui.fxml");
    }
    
     private Parent replaceSceneContent(String fxml) throws Exception {
        Parent page = (Parent) FXMLLoader.load(MainApplication.class.getResource(fxml), null, new JavaFXBuilderFactory());
        Scene scene = stage.getScene();
        if (scene == null) {
            scene = new Scene(page, 1000, 1000);
            stage.setScene(scene);
        } else {
            stage.getScene().setRoot(page);
        }
        stage.sizeToScene();
        return page;
    }
}