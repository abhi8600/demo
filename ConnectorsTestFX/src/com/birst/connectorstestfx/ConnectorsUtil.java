/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.jdom.Element;
import com.birst.connectors.ExtractionObject;
import com.birst.connectors.util.XmlUtils;

public class ConnectorsUtil {
	public static final String NODE_OBJECT_ROOT = "ExtractionObject";
	private static final Logger logger = Logger.getLogger(ConnectorsUtil.class);

	public static List<String> getSupportingFeatures(List<Map<String,String>> connectorMetaDataList){
		List<String> supportingFeatures = new ArrayList<String>();
		for (int i = 0; i < connectorMetaDataList.size(); i++) {
			Map<String, String> prop = connectorMetaDataList.get(i);
			if(!prop.isEmpty() && !prop.containsKey("Value")){
				supportingFeatures.add(prop.get("Name"));
			}
		}
		return supportingFeatures;
	}
	
	
	
	public static String formatToXml(
		List<Map<String, String>> availableMetaDataList) {
		String xmlText = "<Credentials>";
		for (int i = 0; i < availableMetaDataList.size(); i++) {
			Map<String, String> prop = availableMetaDataList.get(i);
			Iterator it = prop.entrySet().iterator();
			boolean isRequired = false;
			String name = "";
			String value = "";
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				if ("Required".equalsIgnoreCase(pairs.getKey().toString())) {
					isRequired = Boolean.parseBoolean(pairs.getValue()
							.toString());
				}
				if ("Name".equalsIgnoreCase(pairs.getKey().toString())) {
					name = pairs.getValue().toString();
				}
				if ("Value".equalsIgnoreCase(pairs.getKey().toString())) {
					value = pairs.getValue().toString();
				}
			}
			if (isRequired || (value != null && value.length() > 0)) {
				xmlText += "<" + name + ">" + XmlUtils.encode(value) + "</" + name + ">";
			}if("UseSandBoxURL".equalsIgnoreCase(name) && "false".equalsIgnoreCase(value)){
                             xmlText+="<SiteType>Production</SiteType>";
                        }else if("UseSandBoxURL".equalsIgnoreCase(name) && "true".equalsIgnoreCase(value)){
                            xmlText+="<SiteType>Sandbox</SiteType>";
                        }
			
			

		}
		
		xmlText += "</Credentials>";
		return xmlText;
	}
	
	public static OMElement parseConnectorResultElement(String result)
			throws Exception {
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
		OMElement root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		if (isSuccess) {
			for (Iterator<OMElement> resultIterator = resultEl
					.getChildElements(); resultIterator.hasNext();) {
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName())) {
					root = (OMElement) el.getChildElements().next();
				}
			}
		} else {
			String message = null;
			int errorCode = -1;// BaseException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl
					.getChildElements(); resultIterator.hasNext();) {
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName())) {
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName())) {
					errorCode = Integer.parseInt(el.getText());
				}
			}
			Exception ure = new Exception(message,
			 new Exception());
			
			 throw ure;
		}
		return root;
	}
	
	public static String generateIsIncrementalXml(Map<String,String> objectsMap){
		String xml="<IsIncrementalRoot>";
		Iterator<Entry<String, String>> it = objectsMap.entrySet().iterator();
		while(it.hasNext()){
			xml+="<ExtractionObject>";
			Map.Entry pairs = (Map.Entry) it.next();
			xml+="<ObjectName>" +pairs.getKey() +"</ObjectName>";
			xml+="<IsIncremental>" + (pairs.getValue().toString())+"</IsIncremental>";
			xml+="<Type>OBJECT</Type>";
			xml+="</ExtractionObject>";
		}
		xml+="</IsIncrementalRoot>";
		return xml;
	}
        
        public static String generateQueryObject(Map<String,String> objectsMap){
		String 	xml="<IsIncrementalRoot><ExtractionObject>";
		xml+="<ObjectName>" +objectsMap.get("Name") +"</ObjectName>";
		xml+="<Type>Query</Type>";
                xml+="<Query>"+objectsMap.get("Query") +"</Query>";
                xml+="<Mapping>"+objectsMap.get("Mapping") +"</Mapping>";
                xml+="<UseLastModified>false</UseLastModified>";
		xml+="</ExtractionObject></IsIncrementalRoot>";
		return xml;
	}
	
	public static boolean serializeObject(List<Map<String,String>> updatedMetaDataList,String connectorString){
		boolean isSuccess=false;
		logger.info("Starting Serialization of Object for " + connectorString);
		try{
			   //use buffering
		      OutputStream file = new FileOutputStream( "config//"+connectorString+".save" );
		      OutputStream buffer = new BufferedOutputStream( file );
		      ObjectOutput output = new ObjectOutputStream( buffer );
		      try{
		        output.writeObject(updatedMetaDataList);
		      }
		      finally{
		        output.close();
		      }
		      isSuccess=true;
		}catch(IOException ex){
			logger.error("Error Serializing Object " + ex);
		}
		logger.info("Serializing updatedMetaData for " + connectorString + " successful : " + isSuccess);
		return isSuccess;
	}
	
	public static List<Map<String,String>> deSerializeObject(String connectorString) throws Exception{
		logger.info("Starting De-Serialization of Object for " + connectorString);
		boolean isSuccess=false;
		ObjectInput  input=null;
		 List<Map<String,String>> recoverdConnectorMetadata = null;
		try{
			   
			  InputStream  file = new FileInputStream( "config//"+connectorString+".save" );
		      InputStream  buffer = new BufferedInputStream( file );
  	          input = new ObjectInputStream( buffer );
	    	  recoverdConnectorMetadata = ( List<Map<String,String>>)input.readObject();
		      isSuccess=true;
		}catch(IOException ex){
			logger.error("Error DeSerializing Object " + ex);
		}finally{
			try{
			if(input!=null){
			input.close();
			}}catch(Exception ex1){}
		}
		logger.info("DeSerializing updatedMetaData for " + connectorString + " successful : " + isSuccess   );
		return ((recoverdConnectorMetadata!=null) ? recoverdConnectorMetadata:null);//return null if de-serialization didnt give a proper object
	}
	
	@SuppressWarnings("unchecked")
	public static List<ExtractionObject> parseObjectsList(Element objectsRoot){
		List<ExtractionObject> response = new ArrayList<ExtractionObject>();
		List<Element> list = objectsRoot.getChildren(NODE_OBJECT_ROOT);
		if(list != null && list.size() > 0){
			for(int i=0; i<list.size(); i++){

				Element socElement = list.get(i);
				String socName = socElement.getChildText("ObjectName");
				String socType = socElement.getChildText("Type");
				String isIncremental = socElement.getChildText("IsIncremental");
                                String queryString = socElement.getChildText("Query");
                                String mapping = socElement.getChildText("Mapping");
				ExtractionObject exObject = new ExtractionObject();
				exObject.name = socName;
				exObject.type = getExtractionType(socType);
				exObject.useLastModified=Boolean.parseBoolean(isIncremental);
                                if(queryString!=null){
                                    exObject.query = queryString;
                                }if(mapping!=null){
                                    exObject.columnMappings = mapping;
                                }
				response.add(exObject);
			}
		}
		
		return response;
	}
	
	public static ExtractionObject.Types getExtractionType(String objectType)
	{
		if(objectType == null)
			return null;
		ExtractionObject.Types extractionType = ExtractionObject.Types.OBJECT;
		if(ExtractionObject.Types.QUERY.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.QUERY;
		}
		
		if(ExtractionObject.Types.OBJECT.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.OBJECT;
		}
		
		if(ExtractionObject.Types.SAVED_QUERY.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.SAVED_QUERY;
		}
		
		if(ExtractionObject.Types.FILTERED_QUERY.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.FILTERED_QUERY;
		}
		
		return extractionType;
	}
	
	public static String prettyFormat(String input, int indent) {
             Source xmlInput =null;StreamResult xmlOutput=null;
	    try {
	         xmlInput = new StreamSource(new StringReader(input));
	        StringWriter stringWriter = new StringWriter();
	        xmlOutput = new StreamResult(stringWriter);
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        transformerFactory.setAttribute("indent-number", indent);
	        Transformer transformer = transformerFactory.newTransformer(); 
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.transform(xmlInput, xmlOutput);
	       
	    } catch (Exception e) {
	        //throw new RuntimeException(e); // simple exception handling, please review it
	    } return xmlOutput.getWriter().toString();
	}
	
}
