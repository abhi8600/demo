package com.birst.connectorstestfx;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.xml.DOMConfigurator;

public class ConnectorHttpClient
{
  
  private static final Logger logger = Logger.getLogger(ConnectorHttpClient.class);

   
  public String callConnectorServlet(String url, String servletApplicationName, String servletName, boolean isPostRequest, Map<String, String> parameters)
  {
    List nvp = new ArrayList();
    if (parameters != null)
    {
      for (String key : parameters.keySet())
      {
        nvp.add(new BasicNameValuePair(key, (String)parameters.get(key)));
      }
    }

    HttpParams params = new BasicHttpParams();
    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    HttpProtocolParams.setContentCharset(params, "UTF-8");
    HttpProtocolParams.setUserAgent(params, "ConnectorsTestUtil");
    HttpProtocolParams.setUseExpectContinue(params, true);

    HttpProcessor httpproc = new BasicHttpProcessor();
    ((BasicHttpProcessor)httpproc).addInterceptor(new RequestContent());
    ((BasicHttpProcessor)httpproc).addInterceptor(new RequestTargetHost());
    ((BasicHttpProcessor)httpproc).addInterceptor(new RequestConnControl());
    ((BasicHttpProcessor)httpproc).addInterceptor(new RequestUserAgent());
    ((BasicHttpProcessor)httpproc).addInterceptor(new RequestExpectContinue());

    HttpRequestExecutor httpexecutor = new HttpRequestExecutor();

    HttpContext context = new BasicHttpContext(null);
    URI uri = null;
    try {
      uri = new URI(url);
    }
    catch (URISyntaxException e) {
      e.printStackTrace();
    }
    HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());

    DefaultHttpClientConnection conn = new DefaultHttpClientConnection();

    context.setAttribute("http.connection", conn);
    context.setAttribute("http.target_host", host);

    String responseStr = null;
    try
    {
      if (!conn.isOpen()) {
        Socket socket = new Socket(host.getHostName(), host.getPort());
        conn.bind(socket, params);
      }
      BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest("POST", "/" + servletApplicationName + "/" + servletName);
      logger.info(">> Request URI: " + request.getRequestLine().getUri());

      request.setEntity(new UrlEncodedFormEntity(nvp, "UTF-8"));
      request.setParams(params);
      httpexecutor.preProcess(request, httpproc, context);
      HttpResponse response = httpexecutor.execute(request, conn, context);
      response.setParams(params);
      httpexecutor.postProcess(response, httpproc, context);

      logger.info(">> Response Code: " + response.getStatusLine());
      responseStr = EntityUtils.toString(response.getEntity());
      logger.info(">> Response Text: "+responseStr);
    
    }
    catch (Exception ioe)
    {
      ioe.printStackTrace();
    }
    finally
    {
      try {
        conn.close();
      }
      catch (IOException ioe)
      {
        logger.error(ioe.getMessage(), ioe);
      }
    }
    return responseStr;
  }
}