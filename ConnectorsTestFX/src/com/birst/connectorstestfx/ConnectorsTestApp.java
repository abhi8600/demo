package com.birst.connectorstestfx;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;
import javax.xml.namespace.QName;
import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.birst.connectors.ConnectorConfig;
import com.birst.connectors.ExtractionObject;
import com.birst.connectors.util.XmlUtils;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * 
 * 
 * @author asharma
 * Command Line Test Utility for Connectors.
 * All inputs are Integers which are shown as CLI Menu
 * Connector.config property file is used to get following parameters
 * @param connectorControllerURI  URL where Connector Controller is Deployed
 * @param spaceID  SpaceID is required for descriptive logging
 * @param spaceDirectory  SpaceDirectory is the data directory for the connector. Space Directory should have slogs and data folders created inside it. 
 *  
 */

public class ConnectorsTestApp {

	private static final String DEFAULT_CONFIG_FILE_FOR_CONNECTORS = "config//Connectors.config";
	private static final String CONNECTORCONTROLLER_APPLICATION = "ConnectorController";
	private static final String LISTCONNECTORS_SERVLET = "ListConnectors";
	private static final String ICONNECTOR_SERVLET = "IConnectorServlet";
	private static final String CLIENT_NAME ="BirstConnectorsTestFX";
        private static final String BIRST_CONNECT_VERSION="1";
        private static final String USER_ID ="BirstConnectorsTestFX";
	
	private static Scanner sc = new Scanner(System.in);
	private static String spaceDirectory =null, connectorName=null;

	private static final Logger logger = Logger
			.getLogger(ConnectorsTestApp.class);
	//Todo SOQL Queries
	private static final String[] CONNECTOR_METHODS_ARRAY = new String[] {
			"listConnectors", "login", "getCatalog","fetchData" };

	public static class ConnectorInfo {
		String connectorServerURL;
		String connectorApplication;
	}

	public static class ConfigProperties {
		String connectorControllerURI;
		String spaceID;
		String spaceDirectory;
	}
 
 
	/**
	 * Reads the Connector.config file and returns them in a ConfigProperties Object
	 * @return ConfigProperties
	 * @throws IOException
	 */
	public static ConfigProperties readConfigFile() throws IOException {
		ConfigProperties confBean = new ConfigProperties();
		Properties props = new Properties();
		props.load(new FileInputStream(DEFAULT_CONFIG_FILE_FOR_CONNECTORS));
		confBean.connectorControllerURI = props
				.getProperty("connectorcontrollerURI");
		confBean.spaceID = props.getProperty("spaceID");
		confBean.spaceDirectory = props.getProperty("spaceDirectory");
                
		return confBean;
	}
        
        public static boolean writeConfigFile(String connectorControllerURI,String spaceID, String spaceDirectory)  {
		boolean isFileWriteSuccess=false;
                try{
		Properties props = new Properties();
		props.setProperty("connectorcontrollerURI",connectorControllerURI);
		props.setProperty("spaceID",spaceID);
		props.setProperty("spaceDirectory",spaceDirectory);
                File f = new File(DEFAULT_CONFIG_FILE_FOR_CONNECTORS);
                OutputStream out = new FileOutputStream( f );
                props.store(out,"ConnectorsTestFX version 1.0.0 beta");
                isFileWriteSuccess=true;
                }catch(IOException ex){
                    logger.error("writeConfigFile error :" + ex);
                }
		return isFileWriteSuccess;
	}
        
    
	
	/**
	 * Used Mainly to provide an interative CLI interface to test deployed connectors 
	 * @param args
	 */
	public static void main(String[] args) {

		ConnectorsTestApp testApp = new ConnectorsTestApp();
		ConfigProperties confObject = null;
		String xmlCredentials = null;
		List<Map<String, String>> availableConnectorsList = new ArrayList<Map<String, String>>();
		List<Map<String, String>> updatedConnectorMetaData =null;
		List<String> getCatalogList =null;
		List<String> userSelectedObjectsForExtraction =null;
		Map<String,String> extractionSelectionOptions = new HashMap<String,String>();
		//Todo connectorSupporting features can be used for connector specific functions like SOQL for SalesForce
		Map<String,String> connectorSupportingFeatures = new HashMap<String,String>();
		boolean isConnectorsDetailsAvailable = false;
		String connectorString = null;

		System.out.println("Birst Connectors Testing Console..");
		logger.info("Starting Connector Test Utility : " + new java.util.Date());

		try {
			logger.info("Reading Property File "
					+ DEFAULT_CONFIG_FILE_FOR_CONNECTORS);
			confObject = readConfigFile();
			logger.info("ConnectorControllerUIR: "
					+ confObject.connectorControllerURI);
			logger.info("spaceID: " + confObject.spaceID);
			logger.info("spaceDirectory: " + confObject.spaceDirectory);
			spaceDirectory = confObject.spaceDirectory;
	
			if (confObject.connectorControllerURI == null
					|| confObject.spaceID == null
					|| confObject.spaceDirectory == null) {
				System.out
						.println("ConnectorControlerURI, spaceID and spaceDirectory cannot be null. Please set the value in propertyFile and restart.");
				throw new RuntimeException("Mandatory Parameters Missing");
			}

			while (true) {
				System.out.println();
				displayTestMenu(isConnectorsDetailsAvailable);

				System.out.print("\nPlease select the Menu option  : ");
				String choice = sc.next();
				if (choice.equalsIgnoreCase("q")) {
					System.out.println("Quitting Test Console..");
					return;
				}
				try {
					int menuOption = Integer.parseInt(choice);
					if (menuOption == 1) {

						availableConnectorsList = getConnectorsList(
								confObject.spaceID,
								confObject.connectorControllerURI);
						isConnectorsDetailsAvailable = true;
						logger.info("getConnectorsList Successful. Total Number of connectors Found : "	+ availableConnectorsList.size());
						
						System.out.println("\nListing Connectors..");
						for (int j = 0; j < availableConnectorsList.size(); j++) {
							Map<String, String> connectorMap = availableConnectorsList
									.get(j);
							System.out.println(j
									+ 1
									+ " . "
									+ connectorMap.get("ConnectorName")
									+ " - "
									+ connectorMap.get("ConnectorVersion")
									+ " - "
									+ connectorMap
											.get("ConnectorConnectionString"));
						}
						System.out.print("\nPlease select the connector : ");
						int subChoice = sc.nextInt();
						connectorString = availableConnectorsList.get(
								subChoice - 1).get("ConnectorConnectionString");
						connectorName = availableConnectorsList.get(subChoice-1).get("ConnectorName");
						logger.info("Setting Connector String to " + connectorString + ". Connector Name :" + connectorName);
						System.out.println("\nSetting connectorString to "
								+ connectorString
								+ " for all future calls for this instance.");
						System.out.println("\nFetching Meta Data for "
								+ connectorString);
						List<Map<String, String>> connectorMetaData = getMetaDataForConnector(
								confObject.spaceID,
								confObject.spaceDirectory,
								getConnectorInfo(connectorString,
										confObject.spaceID,
										confObject.connectorControllerURI));
						List<Map<String,String>> deSerConnectorMetaData = ConnectorsUtil.deSerializeObject(getConnectorInfo(connectorString,
								confObject.spaceID,
								confObject.connectorControllerURI).connectorApplication);
						if(deSerConnectorMetaData!=null){
							//if all the connectorproperties length is same then use the deserialized object as it has values
							if(deSerConnectorMetaData.size()==connectorMetaData.size()){
								connectorMetaData=deSerConnectorMetaData;
							}
						}
						List<String> supportedFeatures = ConnectorsUtil.getSupportingFeatures(connectorMetaData);
						System.out.println("\nSupported Objects for " + connectorName);
						for(int i=0;i<supportedFeatures.size();i++){
							System.out.println(i+1 +". " + supportedFeatures.get(i));
						}
						System.out.println();	
						int userOption = displayMetaData(connectorMetaData);
						if (userOption == 1) {
							updatedConnectorMetaData = updateMetaData(connectorMetaData);
							xmlCredentials =ConnectorsUtil.formatToXml(updatedConnectorMetaData);
						} else {
							xmlCredentials =ConnectorsUtil.formatToXml(connectorMetaData);
							continue;
						}
					} else if (menuOption == 2 && connectorString != null) {
						boolean isConnectNSaveSuccess = saveAndConnect(xmlCredentials,confObject.spaceID,confObject.spaceDirectory,getConnectorInfo(connectorString,
								confObject.spaceID,
								confObject.connectorControllerURI));
						if(isConnectNSaveSuccess){
							boolean flag = ConnectorsUtil.serializeObject(updatedConnectorMetaData,getConnectorInfo(connectorString,
									confObject.spaceID,
									confObject.connectorControllerURI).connectorApplication);
							System.out.println("Saving user config for future use :: " +flag);
						}
					} else if (menuOption == 3 && connectorString != null) {
						getCatalogList = validateConnectionPropertiesAndGetCatalog(xmlCredentials,confObject.spaceID,confObject.spaceDirectory,getConnectorInfo(connectorString,
								confObject.spaceID,
								confObject.connectorControllerURI));
						userSelectedObjectsForExtraction = displayCatalogList(getCatalogList);
						System.out.println("\n Following Object(s) selected for Extraction");
						for(int i=0;i<userSelectedObjectsForExtraction.size();i++){
							System.out.println(i+1 +". " + userSelectedObjectsForExtraction.get(i));
						}
						
						System.out.println("\n Please use MenuOption 4 to extract these objects..\n\n");
						
					} else if (menuOption == 4 && connectorString != null) {
						extractionSelectionOptions.clear();
						for(int i=0;i<userSelectedObjectsForExtraction.size();i++){
							System.out.print(i+1 +". " + userSelectedObjectsForExtraction.get(i)+ " . Allow Incremental Extraction [1] Yes [2] No :");
							int option = sc.nextInt();
							extractionSelectionOptions.put(userSelectedObjectsForExtraction.get(i), option==1? "true" : "false");
						}
						String xmlContent = ConnectorsUtil.generateIsIncrementalXml(extractionSelectionOptions);
						saveSelectedObjects(xmlContent);
						startExtract(xmlCredentials, confObject.spaceID, confObject.spaceDirectory, getConnectorInfo(connectorString,
								confObject.spaceID,
							confObject.connectorControllerURI), xmlContent,"CLI");
						
					} 
				} catch (NumberFormatException ex) {
					logger.info("Invalid Menu Option:" + ex);
					System.out.println("Invalid Menu Option ::"
							+ ex.getMessage());
					continue;
				} catch (Exception ex) {
					System.out.println("Error Accessing the method."
							+ ex.getMessage());
					logger.error(ex);
				}

			}

		} catch (IOException ex) {
			logger.equals("ConnectorsTestApp.Main : Property File Not Found :"
					+ DEFAULT_CONFIG_FILE_FOR_CONNECTORS + ". Exception "
					+ ex);
		} catch (RuntimeException ex) {
			logger.equals("ConnectorsTestApp.Main : Runtime Exception :"
					+ DEFAULT_CONFIG_FILE_FOR_CONNECTORS + ". Exception "
					+ ex.getMessage());
		} finally {
			try {
				sc.close();
				sc = null;
			} catch (Exception ex) {

			}
		}

	}

	@SuppressWarnings("unchecked")
	/**
	 * This method returns a List of HashMaps which contain all the connnectors available from ListConnector
	 * @param spaceID	spaceID for logging
	 * @param connectorURI connectorControlURI which will be used to get a list of all connectors registered with the controller
	 * @return List<Map<String,String>> List contains HashMaps with all connector Properties like ConnectorName,ConnectorURI,ConnectorString
	 */
	public static List<Map<String, String>> getConnectorsList(String spaceID,
			String connectorURI) {

		try {
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = new HashMap<String, String>();
			argumentsMap.put("SpaceID", spaceID);
			argumentsMap.put("Client", CLIENT_NAME);
                        argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
                        argumentsMap.put("SpaceDirectory", spaceDirectory);
                        argumentsMap.put("UserID", USER_ID);
			String result = client.callConnectorServlet(connectorURI,
					CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET,
					true, argumentsMap);
			logger.info("Server Response");
			logger.info(ConnectorsUtil.prettyFormat(result,2));
			OMElement root = XmlUtils.convertToOMElement(result);

			if (root == null) {
				logger.error("getConnectorsList: Unable to covertToOMElement :"
						+ result);
				return null;
			}

			List<Map<String, String>> valueList = new ArrayList<Map<String, String>>();
			OMElement connectorsList = root;

			Iterator<?> nodeElement = connectorsList.getChildrenWithName(new QName("Connector"));

			while (nodeElement.hasNext()) {
				OMElement om = (OMElement) nodeElement.next();

				Iterator<OMElement> it = om.getChildElements();
				Map<String, String> valueMap = new HashMap<String, String>();
				while (it.hasNext()) {
					OMElement el = it.next();
					valueMap.put(el.getLocalName(), el.getText());
				}
				valueList.add(valueMap);
			}
			return valueList;

		} catch (Exception ex) {
			logger.error("getConnectorsList: Unable to getConnectorsList for "
					+ connectorURI + "  . Exception :" + ex);
			ex.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	/**
	 * Returns a list of hashmap containing all attributes that are part of the Connector Metadata. These attributes
	 * will be used for authentication, extraction and to identify the connector support for particular objects
	 * @param spaceID used for logging
	 * @param spaceDirectory used for writing to connector-config.xml files and logging
	 * @param connectorInfo  contains details about the particular connector like URI,name,String
	 * @return List<Map<String,String>> returns a List of HashMaps, each hashMap holds all the attributes of a property Like Email,Password etc
	 */
	public static List<Map<String, String>> getMetaDataForConnector(
			String spaceID, String spaceDirectory, ConnectorInfo connectorInfo)
			throws Exception {
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = new HashMap<String, String>();
		List<Map<String, String>> connectorPropertiesList = new ArrayList<Map<String, String>>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
		argumentsMap.put("Method", "getMetaData");
                argumentsMap.put("UserID", USER_ID);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		logger.info("Request For getMetaData() ["+ spaceID + " , " + spaceDirectory+"]");
		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
		OMElement root = ConnectorsUtil.parseConnectorResultElement(result);
		if (root != null) {

			Iterator<?> nodeElement = root.getChildrenWithName(new QName(
					"ConnectionProperty"));

			while (nodeElement.hasNext()) {
				OMElement om = (OMElement) nodeElement.next();

				Iterator<OMElement> it = om.getChildElements();
				Map<String, String> propertiesMap = new HashMap<String, String>();
				while (it.hasNext()) {
					OMElement el = it.next();
					propertiesMap.put(el.getLocalName(), el.getText());
				}
				connectorPropertiesList.add(propertiesMap);

			}
		}

		return connectorPropertiesList;
	}



	private static void displayTestMenu(boolean isConnectorsDetailsAvailable) {
		for (int i = 0; i < CONNECTOR_METHODS_ARRAY.length; i++) {
			System.out.println(i + 1 + ". " + CONNECTOR_METHODS_ARRAY[i]); 
			if (!isConnectorsDetailsAvailable) {
				return;
			}
		}
	}

	/**
	 * 
	 * @param connectorString unique string to identify a connector
	 * @param spaceID spaceID used for logging
	 * @param connectorURI URL where the current Connector is Deployed
	 * @return ConnectorInfo ConnectorInfo
	 */
	public static ConnectorInfo getConnectorInfo(String connectorString,
			String spaceID, String connectorURI) {
		ConnectorInfo conInfo = new ConnectorInfo();
		try {
                        connectorString = connectorString.replaceAll("\\(Default\\)","").trim();
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = new HashMap<String, String>();
			argumentsMap.put("SpaceID", spaceID);
			argumentsMap.put("Client", CLIENT_NAME);
			argumentsMap.put("ConnectionString", connectorString);
                        argumentsMap.put("UserID", USER_ID);
                        argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
                        argumentsMap.put("SpaceDirectory", spaceDirectory);
			logger.info("Request For getConnectorInfo() ["+ spaceID + " , " + spaceDirectory+"]");
			String result = client.callConnectorServlet(connectorURI,
					CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET,
					true, argumentsMap);
			logger.info("Server Response");
			logger.info(ConnectorsUtil.prettyFormat(result,2));
			OMElement root = XmlUtils.convertToOMElement(result);
			if (root == null) {
				logger.error("getConnectorInfo: Unable to covertToOMElement :"
						+ result);
				return null;
			}

			Iterator<?> nodeElement = root.getChildrenWithName(new QName(
					"Connector"));

			while (nodeElement.hasNext()) {
				OMElement om = (OMElement) nodeElement.next();

				@SuppressWarnings("unchecked")
				Iterator<OMElement> it = om.getChildElements();
				while (it.hasNext()) {
					OMElement el = it.next();
					if (el.getLocalName() != null
							&& el.getLocalName().equalsIgnoreCase(
									"ConnectorApplication")) {
						conInfo.connectorApplication = el.getText();
					}
					if (el.getLocalName() != null
							&& el.getLocalName().equalsIgnoreCase(
									"ConnectorServerURL")) {
						conInfo.connectorServerURL = el.getText();
					}
					

				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return conInfo;

	}

	@SuppressWarnings("rawtypes")
	private static int displayMetaData(
			List<Map<String, String>> connectorMetaDataList) {
		int isEditRequired = -1;
		System.out.println("Displaying MetaData For the Connector");
		System.out.println();
		for (int i = 0; i < connectorMetaDataList.size(); i++) {
			Map<String, String> prop = connectorMetaDataList.get(i);
			Iterator it = prop.entrySet().iterator();
			boolean isSecret = false;
			boolean isRequired = false;
			String name = "";
			String value = "";
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				if ("Secret".equalsIgnoreCase(pairs.getKey().toString())) {
					isSecret = Boolean
							.parseBoolean(pairs.getValue().toString());
				}
				if ("Required".equalsIgnoreCase(pairs.getKey().toString())) {
					isRequired = Boolean.parseBoolean(pairs.getValue()
							.toString());
				}
				if ("Name".equalsIgnoreCase(pairs.getKey().toString())) {
					name = pairs.getValue().toString();
				}
				if ("Value".equalsIgnoreCase(pairs.getKey().toString())) {
					value = pairs.getValue().toString();
				}
			}
			//Excluding all Connector Support Metadata like FILTER_QUERY,SAVED_QUERY etc
			if(prop.containsKey("Value")){
			System.out
					.println(((isRequired ? "*" : " ") + (" " + name) + " : " + (isSecret
							&& value != null && value.length() > 0 ? "*****"
							: value)));
			}

		}

		try {
			System.out
					.print("\n\nPlease enter [1] To Edit above values [2] To Skip : ");
			isEditRequired = sc.nextInt();
		} catch (Exception ex) {
		} finally {

		}
		return isEditRequired;

	}
	
	
	
	
	private static List<String> displayCatalogList(List<String> catalogItemList){
		
		Iterator<String> catalogIterator = catalogItemList.iterator();
		int count=1;
		String userSelectedObjects ="";
		List<String> selectedObjectList = new ArrayList<String>();
		
		while(catalogIterator.hasNext()){
			String stempItemObject =  catalogIterator.next().toString();
			System.out.print(count + ". " + String.format("%1$-" + 40 + "s", stempItemObject ));
			if(count%3==0)
				System.out.println();
			
			count++;
		}
		try {
			System.out
					.print("\n\nPlease enter Menu Option number of objects that you want to Extract (Multiple Objects to be seperated by a ,) : ");
			userSelectedObjects = sc.next();
		} catch (Exception ex) {
		} finally {

		}
		
		if(userSelectedObjects==null || userSelectedObjects.trim().length()==0){
			System.out.println("You have selected 0 Objects");
			return null;
		}
		boolean areAllObjectChoicesValid=false;
		try{
			if(userSelectedObjects.contains(",")){
				String[] sTempArray = userSelectedObjects.split(",");
				for(String s : sTempArray){
				selectedObjectList.add(catalogItemList.get(Integer.parseInt(s)-1));
			}
			}else{
				selectedObjectList.add(catalogItemList.get(Integer.parseInt(userSelectedObjects)-1));
			}
			areAllObjectChoicesValid=true;
		}catch(Exception ex){
			ex.printStackTrace();
			logger.error(ex);
		}
		if(!areAllObjectChoicesValid){
			System.out.println("Invalid Item Selection..");
			return null;
		}
		return selectedObjectList;

	}

	@SuppressWarnings("rawtypes")
	/**
	 * Allows user to update Connector Metadata Property values. For example Attributes like Email,Password,AccountID if mandatory 
	 * are to be inputted using command Line. Others can be skipped by pressing an enter key.
	 * @param connectorMetaDataList Contains Metadata recieved from the connector
	 * @return List<Map<String,String>> returns Metadata with Hashmaps having updated values of mandatory parameters like Email etc
	 */
	private static List<Map<String, String>> updateMetaData(
			List<Map<String, String>> connectorMetaDataList) {

		List<Map<String, String>> newMetaData = new ArrayList<Map<String, String>>();
		System.out
				.println("Please enter values for following paramters. Parameters marked with * are mandatory. You can skip other parameters by pressing enter key\n\n");

		try {
			for (int i = 0; i < connectorMetaDataList.size(); i++) {
				Map<String, String> prop = connectorMetaDataList.get(i);
				Iterator it = prop.entrySet().iterator();
		
				boolean isRequired = false;
				String name = "";
				String value = "";
				while (it.hasNext()) {
					Map.Entry pairs = (Map.Entry) it.next();
					if ("Required".equalsIgnoreCase(pairs.getKey().toString())) {
						isRequired = Boolean.parseBoolean(pairs.getValue()
								.toString());
					}
					if ("Name".equalsIgnoreCase(pairs.getKey().toString())) {
						name = pairs.getValue().toString();
					}
					if ("Value".equalsIgnoreCase(pairs.getKey().toString())) {
						value = pairs.getValue().toString();
					}
				}
				
				//All connnections to have to SaveAuthentication to true
				if("SaveAuthentication".equalsIgnoreCase(name)){
					prop.put("Value", "true");
				}else if(prop.containsKey("Value")){
					System.out.println("");
					System.out.print((isRequired ? "*" : " ") + (" " + name)
							+ " : ");
					String newValue = value;
					
					//Move a line down to get the first property
					if(i==0){
						sc.nextLine();
					}
					String tempValue = sc.nextLine();
					if (isRequired) {
						while ((tempValue == null || tempValue.trim().length() == 0)) {
							System.out.println(name + " is mandatory");
							System.out.print((isRequired ? "*" : " ")
									+ (" " + name) + " : ");
							tempValue = sc.nextLine();
						}
						newValue = tempValue == null ? value : tempValue;
					} else {
						newValue = tempValue == null ? value : tempValue;
					}
	
					prop.put("Value", newValue);
				}
				
				newMetaData.add(prop);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		return newMetaData;

	}
        
        public static List<Map<String, String>> updateMetaData(
			List<Map<String, String>> connectorMetaDataList,List<String[]> userSubmittedGUIValues) {

		List<Map<String, String>> newMetaData = new ArrayList<Map<String, String>>();

		try {
			for (int i = 0; i < connectorMetaDataList.size(); i++) {
				Map<String, String> prop = connectorMetaDataList.get(i);
				Iterator it = prop.entrySet().iterator();
		
				boolean isRequired = false;
				String name = "";
				String value = "";
				while (it.hasNext()) {
					Map.Entry pairs = (Map.Entry) it.next();
					if ("Required".equalsIgnoreCase(pairs.getKey().toString())) {
						isRequired = Boolean.parseBoolean(pairs.getValue()
								.toString());
					}
					if ("Name".equalsIgnoreCase(pairs.getKey().toString())) {
						name = pairs.getValue().toString();
					}
					if ("Value".equalsIgnoreCase(pairs.getKey().toString())) {
						value = pairs.getValue().toString();
					}
				}
				
				//All connnections to have to SaveAuthentication to true
				if("SaveAuthentication".equalsIgnoreCase(name)){
					prop.put("Value", "true");
				}else if(prop.containsKey("Value")){
                                    String newValue="";
					for(String[] userInput : userSubmittedGUIValues){
                                            if(userInput[0] !=null &&  userInput[0].equalsIgnoreCase(name)){
                                                newValue=userInput[1];
                                            }
                                        }
					prop.put("Value", newValue);
				}
				
				newMetaData.add(prop);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		return newMetaData;

	}


	
	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @param credentials Credentials for the connector which are sent to the ConnectorURI
	 * @param spaceID spaceID is used for logging
	 * @param spaceDirectory Used to point to the spaceDirectory, will be used by controller
	 * @param connectorInfo Contains Connector Specific Information
	 * @return boolean if save is successful or not
	 * @throws Exception
	 */
	public static boolean saveAndConnect(String credentials,String spaceID,String spaceDirectory, ConnectorInfo connectorInfo) throws Exception{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		argumentsMap.put("Method", "saveConnectorCredentials");
                argumentsMap.put("UserID", USER_ID);
		argumentsMap.put("CredentialsXML",credentials);
 		logger.info("Request For saveConnectorCredentials() ["+ spaceID + " , " + spaceDirectory+"]");
		logger.info(ConnectorsUtil.prettyFormat(credentials,2));
 		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		System.out.println("\nsaveConnectorCredentials() call to "+ connectorInfo.connectorApplication + " : " + isSuccess);
		logger.info("saveConnectorCredentials() call to "+ connectorInfo.connectorApplication + " : " + isSuccess);
		return isSuccess;
		
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @param credentials Connector Credentials in XML
	 * @param spaceID spaceID for logging
	 * @param spaceDirectory Required by Connector
	 * @param connectorInfo  Connector Info
	 * @param incrementalXml XML for all the object and attributes that need to be extracted.
	 * @throws Exception
	 */
	public static String[] startExtract(String credentials,String spaceID,String spaceDirectory, ConnectorInfo connectorInfo,String incrementalXml,String mode) throws Exception{
		ConnectorHttpClient client = new ConnectorHttpClient();
                String[] extractionDetails = new String[2];
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		argumentsMap.put("Method", "startExtract");
		argumentsMap.put("CredentialsXML",credentials);
                argumentsMap.put("UserID", USER_ID);
		argumentsMap.put("isIncrementalXml",incrementalXml);
		String uid = UUID.randomUUID().toString();
                extractionDetails[0] = uid;
		argumentsMap.put("UID",uid);
		logger.info("Request For startExtract() ["+ spaceID + " , " + spaceDirectory+"]");
		logger.info(ConnectorsUtil.prettyFormat(credentials,2));
		logger.info(ConnectorsUtil.prettyFormat(incrementalXml,2));
		
		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		System.out.println("\nstartExtract() call to "+ connectorInfo.connectorApplication + " : " + isSuccess);
		logger.info("startExtract() call to "+ connectorInfo.connectorApplication + " : " + isSuccess);
                if(isSuccess && mode.equalsIgnoreCase("CLI")){
                    monitorExtract(uid);
                }
                extractionDetails[1] = String.valueOf(isSuccess);
                return extractionDetails;
        }
        public static void monitorExtract(String uid){
		
		long startTime = System.currentTimeMillis();
		long endTime;
		System.out.println();
		System.out.print("Extraction Progress ");
		String[] statusFileArray = new String[]{"success","failed","killed"}; 
		try{
			boolean monitorResponse=true;
			while(monitorResponse){
			Thread.sleep(2000);
			System.out.print(".");
		
				for(String statusFile : statusFileArray){
					String fileName = spaceDirectory  + "\\slogs\\"+statusFile+"-extract-"+uid+".txt";
				
					File f = new File(fileName);
					if(f.exists()){
						endTime=System.currentTimeMillis();
						System.out.println();
						System.out.println(uid + " : Extraction Status " + statusFile);
						System.out.println("Total Extraction Time : " + (endTime-startTime)/1000 + " seconds");
						logger.info(uid + " : Extraction Status .." + statusFile);
						logger.info("Total Extraction Time : " + (endTime-startTime)/1000 + " seconds");
						monitorResponse=false;
						break;
					}
				}
			
			}
		}catch(Exception ex){
			logger.error("Error During monitoring Extraction ::" + ex);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * 
	 * @param credentials Credentials for SAS platform
	 * @param spaceID 
	 * @param spaceDirectory used for logging and connector
	 * @param connectorInfo
	 * @return
	 * @throws Exception
	 */
	
	public static List<String> validateConnectionPropertiesAndGetCatalog(String credentials,String spaceID,String spaceDirectory, ConnectorInfo connectorInfo) throws Exception{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		argumentsMap.put("Method", "validateConnectionPropertiesAndGetCatalog");
                argumentsMap.put("UserID", USER_ID);
		argumentsMap.put("CredentialsXML",credentials);
		logger.info("Request For validateConnectionPropertiesAndGetCatalog() ["+ spaceID + " , " + spaceDirectory+"]");
		logger.info(ConnectorsUtil.prettyFormat(credentials,2));
		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
		
		List<String> catalogItemsList = new ArrayList<String>();
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
	
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		
		
		if(isSuccess){
			if (resultEl != null) {

				Iterator<?> nodeElement = resultEl.getChildrenWithName(new QName(
						"Result"));

				while (nodeElement.hasNext()) {
					OMElement om = (OMElement) nodeElement.next();
					Iterator<OMElement> it = om.getChildElements();
					while (it.hasNext()) {
						OMElement el = it.next();
						Iterator<?> catalogElementIterator = el.getChildrenWithName(new QName("CatalogItem"));
						while(catalogElementIterator.hasNext()){
							OMElement catalogElement = (OMElement) catalogElementIterator.next();
							catalogItemsList.add(catalogElement.getText());
						}
					}
				}
			}
		}
		System.out.println("\nvalidateConnectionPropertiesAndGetCatalog() call to "+ connectorInfo.connectorApplication + " : " + isSuccess + " : Catalog Size ::"  + catalogItemsList.size());
		logger.info("validateConnectionPropertiesAndGetCatalog() call to "+ connectorInfo.connectorApplication + " : " + isSuccess+ " : Catalog Size ::"  + catalogItemsList.size());
		return catalogItemsList;
	}
	
	
	

	/**
	 * Save the objects selected for extraction in SpaceDirector/config/connector-config.xml
	 * @param xmlData Contains XML as string containing object details and if they are incremental
	 * @throws Exception
	 */
	public static void saveSelectedObjects(String xmlData) throws Exception{		
		List<ExtractionObject> objects = new ArrayList<ExtractionObject>();
		if(xmlData != null && xmlData.trim().length() > 0){
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element objectsList = d.getRootElement();
			objects = ConnectorsUtil.parseObjectsList(objectsList);
		}
		saveSelectedObjects(objects);
	}
        
        public static void saveSelectedObjects(String xmlData,String spaceDirectory,String connectorName) throws Exception{		
		List<ExtractionObject> objects = new ArrayList<>();
		if(xmlData != null && xmlData.trim().length() > 0){
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element objectsList = d.getRootElement();
			objects = ConnectorsUtil.parseObjectsList(objectsList);
                        if("SalesForce".equalsIgnoreCase(connectorName)){
                            connectorName="sfdc";
                        }
                        saveSelectedObjects(objects,spaceDirectory,connectorName);
		}
               
	}
	
	public static ConnectorConfig getConnectorSettings() throws Exception{
		return ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
	}
        
        public static ConnectorConfig getConnectorSettings(String spaceDirectory,String connectorName) throws Exception{
		return ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
	}
	
	private static void saveSelectedObjects(List<ExtractionObject> objects) throws Exception{
		// Prepopulate the saving list with non-standard objects
		// save it as a general config also
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> updated = new ArrayList<ExtractionObject>();
		if(objects != null && objects.size() > 0){
			for(ExtractionObject exObject : objects){
				ExtractionObject object = settings.findObject(exObject.name);
				if(object == null){
					object = new ExtractionObject();
					object.name = exObject.name;
					object.type = exObject.type;
                                        if(exObject.query!=null){
                                            object.query = exObject.query;
                                        }
                                        if(exObject.columnMappings!=null){
                                            object.columnMappings = exObject.columnMappings;
                                        }
				}
				if(object != null){
					updated.add(object);
				}
			}
		}
		settings.setExtractionObjects(updated);
			settings.saveConnectorConfig(spaceDirectory, connectorName);
	}
        
        private static void saveSelectedObjects(List<ExtractionObject> objects,String spaceDirectory,String connectorName) throws Exception{
		// Prepopulate the saving list with non-standard objects
		// save it as a general config also
		ConnectorConfig settings = getConnectorSettings(spaceDirectory,connectorName);
		List<ExtractionObject> updated = new ArrayList<ExtractionObject>();
		if(objects != null && objects.size() > 0){
			for(ExtractionObject exObject : objects){
				ExtractionObject object = settings.findObject(exObject.name);
				if(object == null){
					object = new ExtractionObject();
					object.name = exObject.name;
					object.type = exObject.type;
                                        if(exObject.query!=null){
                                            object.query = exObject.query;
                                        }
                                        if(exObject.columnMappings!=null){
                                            object.columnMappings = exObject.columnMappings;
                                        }
				}
				if(object != null){
					updated.add(object);
				}
			}
		}
		settings.setExtractionObjects(updated);
			settings.saveConnectorConfig(spaceDirectory, connectorName);
	}
   

       public static Map<String,String> getQueryObject(String credentials,String spaceID,String spaceDirectory, ConnectorInfo connectorInfo,String objectName) throws Exception{
		ConnectorHttpClient client = new ConnectorHttpClient();
                Map<String,String> resultsQueryMap = new HashMap<String,String>();
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		argumentsMap.put("Method", "getObjectQuery");
                argumentsMap.put("UserID", USER_ID);
                String xmlData = "<ObjectQuery>"+credentials+"<Object><Type>Query</Type><Name>"+objectName+"</Name></Object></ObjectQuery>";
		argumentsMap.put("xmlData",xmlData);
            
		logger.info("Request For getObjectQuery() ["+ spaceID + " , " + spaceDirectory+"]");
		logger.info(ConnectorsUtil.prettyFormat(credentials,2));
		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
                OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
                if(isSuccess){
                    if (resultEl != null) {

				Iterator<?> nodeElement = resultEl.getChildrenWithName(new QName("Result"));

				while (nodeElement.hasNext()) {
					OMElement om = (OMElement) nodeElement.next();
					Iterator<OMElement> it = om.getChildrenWithLocalName("Object");
					while (it.hasNext()) {
						OMElement el = it.next();
						Iterator<?> objectElementIterator = el.getChildElements();
						while(objectElementIterator.hasNext()){
							OMElement objectElement = (OMElement) objectElementIterator.next();
							System.out.println(objectElement.toString());
                                                        resultsQueryMap.put(objectElement.getLocalName(),objectElement.getText());
						}
					}
				}
			}
                }
                return resultsQueryMap;
          }
        
        public static boolean validateQueryObject(String credentials,String spaceID,String spaceDirectory, ConnectorInfo connectorInfo,String objectName,String query) throws Exception{
		ConnectorHttpClient client = new ConnectorHttpClient();
                boolean isValidQuery = false;
                Map<String,String> resultsQueryMap = new HashMap<String,String>();
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("SpaceID", spaceID);
		argumentsMap.put("SpaceDirectory", spaceDirectory);
		argumentsMap.put("Client", CLIENT_NAME);
                argumentsMap.put("BirstConnectorVersion", BIRST_CONNECT_VERSION);
		argumentsMap.put("Method", "validateObjectQuery");
                argumentsMap.put("UserID", USER_ID);
                String xmlData = "<ObjectQuery>"+credentials+"<Object><Type>Query</Type><Name>"+objectName+"</Name><Query>"+query+"</Query></Object></ObjectQuery>";
		argumentsMap.put("xmlData",xmlData);
            
		logger.info("Request For validateConnectorObjectQuery() ["+ spaceID + " , " + spaceDirectory+"]");
		logger.info(ConnectorsUtil.prettyFormat(credentials,2));
		String result = client.callConnectorServlet(
				connectorInfo.connectorServerURL,
				connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true,
				argumentsMap);
		logger.info("Server Response");
		logger.info(ConnectorsUtil.prettyFormat(result,2));
                System.out.println(result);
                OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null) {
			Exception ure = new
			Exception("Unable to parse result received from Connector",
			 new Exception());
			 throw ure;
		}
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator
				.hasNext();) {
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName())) {
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
                   if(isSuccess){
                    if (resultEl != null) {
				Iterator<?> nodeElement = resultEl.getChildrenWithName(new QName("Result"));
				while (nodeElement.hasNext()) {
					OMElement om = (OMElement) nodeElement.next();
					Iterator<OMElement> it = om.getChildrenWithLocalName("Object");
					while (it.hasNext()) {
						OMElement el = it.next();
						Iterator<?> objectElementIterator = el.getChildElements();
						while(objectElementIterator.hasNext()){
							OMElement objectElement = (OMElement) objectElementIterator.next();
							if("IsValidQuery".equalsIgnoreCase(objectElement.getLocalName())){
                                                            isValidQuery = objectElement.getText()!=null ? Boolean.parseBoolean(objectElement.getText()) : false;
                                                        }
						}
					}
				}
			}
                }
                        
             return isValidQuery;
          }
}
