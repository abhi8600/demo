/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx;

import com.birst.connectors.ConnectorConfig;
import com.birst.connectors.ExtractionObject;
import com.birst.connectorstestfx.ConnectorsTestApp.ConnectorInfo;
import static com.birst.connectorstestfx.ConnectorsTestApp.getConnectorSettings;
import static com.birst.connectorstestfx.ConnectorsTestApp.saveSelectedObjects;
import com.birst.connectorstestfx.beans.ConfigProperties;
import com.birst.connectorstestfx.beans.Context;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author asharma
 */
public class Test_uiController implements Initializable {

    @FXML
    private ChoiceBox connectorsDropDown, connectorsAPIDropDown;
    @FXML
    private FlowPane flowpane;
    @FXML
    private Label validationLabel, loginLabel;
    @FXML
    private ScrollPane scrollPane, selectedPane;
    @FXML
    private Button saveButton, extractButton, modifyButton,queryObjectButton;
    private String xmlCredentials = null,apiVersion = null;;
    @FXML
    private TextArea logsTextArea;
    @FXML
    private RadioButton showsavedObjects, showCatalogObjects;
  
    final PopupWindow popup = new PopupWindow() {};
    private Stage popupstage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        writeToLogs("Initializing Testing Console...");
        final List<String> connectorStringsArrayList = new ArrayList<String>();
        final List<String> connectorsSetList = new ArrayList<String>();

        final List<Map<String, String>> connectorsList = Context.getInstance().getConnectorsList();
        final ConfigProperties confBean = Context.getInstance().getCurrentConfig();
        
        connectorsDropDown.valueProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue ov,String value, String new_value) {
                    hideScreen();
                    if (!"Select".equalsIgnoreCase(new_value)) {
                        setupConnectorMetaData(connectorsList, confBean, new_value);
                        try {
                            ConnectorConfig confProps = ConnectorsTestApp.getConnectorSettings(confBean.spaceDirectory, new_value);
                            String connectorInstance = confProps.getConnectorAPIVersion();
                            String apiVersion = connectorInstance.substring(connectorInstance.lastIndexOf(":")+1,connectorInstance.length()).trim();
                            if(connectorInstance!= null && apiVersion!=null){
                                Context.getInstance().setConnectorString(connectorInstance);
                                connectorsAPIDropDown.getSelectionModel().select(apiVersion);
                            }
                        } catch (Exception ex) {
                        }
                    } else {
                        flowpane.getChildren().removeAll(flowpane.getChildren());
                        connectorsAPIDropDown.getItems().clear();
                    }
            }
        });

        connectorsAPIDropDown.valueProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue ov,
                    String value, String new_value) {
                if (new_value != null) {
                    String newVal = new_value.replaceAll("\\(Default\\)", "").trim();
                    String connectorName = connectorsDropDown.getSelectionModel().selectedItemProperty().getValue().toString();
                    for (int j = 0; j < connectorsList.size(); j++) {
                        Map<String, String> connectorMap = connectorsList.get(j);
                        String connectorVersion = connectorMap.get("ConnectorVersion");
                        boolean isDefaultVersion = Boolean.parseBoolean(connectorMap.get("IsDefaultAPIVersion"));
                        String sconnectorNameTemp = connectorMap.get("ConnectorName");
                        String connectorString = connectorMap.get("ConnectorConnectionString");
                        if (sconnectorNameTemp != null && sconnectorNameTemp.equalsIgnoreCase(connectorName) && connectorVersion.equalsIgnoreCase(newVal)) {
                            Context.getInstance().setConnectorString(connectorString);
                            System.out.println("Current Instance : " + connectorString);
                        }
                    }
                }
            }
        });

        writeToLogs("\nTotal Connectors Installed :" + connectorsList.size());
        connectorStringsArrayList.add("0");
        connectorsSetList.add("Select");
        for (int j = 0; j < connectorsList.size(); j++) {
            
            Map<String, String> connectorMap = connectorsList.get(j);
            String connectorDetails = connectorMap.get("ConnectorName"); //+ " - " + connectorMap.get("ConnectorVersion") + " - " + connectorMap.get("ConnectorConnectionString");
            if (!connectorsSetList.contains(connectorDetails)) {
                connectorsSetList.add(connectorDetails);
            }
            connectorStringsArrayList.add(connectorMap.get("ConnectorConnectionString"));
            writeToLogs("\nAdding - " + connectorDetails);
        }
        for (String connectorName : connectorsSetList) {
            connectorsDropDown.getItems().add(connectorName);
        }  
        connectorsDropDown.getSelectionModel().select("Salesforce");
        connectorsDropDown.requestFocus();
        
      
        queryObjectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    Stage dialogStage = new Stage();
                    dialogStage.initModality(Modality.APPLICATION_MODAL);
                    FXMLLoader fxmlLoader = new FXMLLoader(Test_uiController.class.getResource("QueryPopup_ui.fxml"));
                    dialogStage.setScene(new Scene((Parent)fxmlLoader.load()));
                    dialogStage.show();
                    dialogStage.setOnHidden(new EventHandler<WindowEvent>(){
                        public void handle(WindowEvent t) {
                                Map<String,String> queryObjectMap = Context.getInstance().getQueryObject();
                                saveQueryObjects(queryObjectMap);
                        }
                     });
                 
                } catch (IOException ex) {
                    Logger.getLogger(Test_uiController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
       
    }
    
    private void saveQueryObjects(Map<String,String> queryObject){
        String xmlContent = ConnectorsUtil.generateQueryObject(queryObject);
        writeToLogs("Generating Incremental XML to write in repository");
        writeToLogs(ConnectorsUtil.prettyFormat(xmlContent, 2));
        writeToLogs("Setting IncrementalContent for the Context.");
        Context.getInstance().setObjectsForExtraction(xmlContent);
        writeToLogs("Trying to Modify the Repository with Selected Changes");
        try {
            saveSelectedObjects(xmlContent, Context.getInstance().getCurrentConfig().spaceDirectory, Context.getInstance().getConnectorName());
            writeToLogs("Repository Modification Successful");
        } catch (Exception ex) {
            writeToLogs("Repository Modification Failed");
            writeToLogs(ex);
        }
    }
    private void setupConnectorMetaData(List<Map<String, String>> connectorsList, ConfigProperties confBean, String connector) {
        try {
            String connectorString = null;
            String connectorName = connector;
            Context.getInstance().setConnectorName(connectorName);
            writeToLogs("Setting connectorName as " + connectorName + " for all future calls.");
            connectorsAPIDropDown.getItems().clear();
            int selectIndexCount = 0;
            for (int j = 0; j < connectorsList.size(); j++) {
                Map<String, String> connectorMap = connectorsList.get(j);
                String connectorVersion = connectorMap.get("ConnectorVersion");
                boolean isDefaultVersion = Boolean.parseBoolean(connectorMap.get("IsDefaultAPIVersion"));
                String sconnectorNameTemp = connectorMap.get("ConnectorName");

                if (sconnectorNameTemp != null && sconnectorNameTemp.equalsIgnoreCase(connectorName)) {
                    connectorVersion = isDefaultVersion ? connectorVersion + " (Default)" : connectorVersion;
                    connectorsAPIDropDown.getItems().add(connectorVersion);
                    if (isDefaultVersion) {
                        connectorsAPIDropDown.getSelectionModel().select(selectIndexCount);
                        connectorString = connectorMap.get("ConnectorConnectionString");
                        writeToLogs("Selected Connector : " + connectorString);
                        Context.getInstance().setConnectorString(connectorString + " (Default)");
                    }
                    selectIndexCount++;
                }
            }
            writeToLogs("Fetching connector MetaData for " + connectorString);


            List<Map<String, String>> connectorMetaData = null;
            try {
                connectorMetaData = ConnectorsTestApp.getMetaDataForConnector(confBean.spaceID, confBean.spaceDirectory, ConnectorsTestApp.getConnectorInfo(connectorString, confBean.spaceID, confBean.connectorControllerURI));
                writeToLogs("MetaData Properties Found : " + connectorMetaData.size());
            } catch (Exception ex) {
                writeToLogs("Error : Not able to retrieve Connector MetaData for " + connectorString);
                writeToLogs(ex);
            }

            writeToLogs("Deserializing Saved Properties for " + connectorString);
            

            List<Map<String, String>> deSerConnectorMetaData = ConnectorsUtil.deSerializeObject(ConnectorsTestApp.getConnectorInfo(connectorString,
                    confBean.spaceID,
                    confBean.connectorControllerURI).connectorApplication);
            if (deSerConnectorMetaData != null) {
                //if all the connectorproperties length is same then use the deserialized object as it has values
                writeToLogs("Deserializing Saved Properties for " + connectorString + " Successful");
                if (deSerConnectorMetaData.size() == connectorMetaData.size()) {
                    connectorMetaData = deSerConnectorMetaData;
                    writeToLogs("\n Using Deserialized Properties for " + connectorString);
                } else {
                    writeToLogs("Deserializing Saved Properties for " + connectorString + " Not same as Current Metadata");
                }
            } else {
                writeToLogs("Deserializing Saved Properties for " + connectorString + " Failed. Please enter values to Save for the first time");
            }
            Context.getInstance().setConnectorMetaData(connectorMetaData);
            writeToLogs("Saving Connector Metadata for this context..");
            renderConnectorParams(connectorMetaData);
        } catch (Exception ex) {
            Logger.getLogger(Test_uiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void renderScrollPane(List<String> connectorsObjectList) {
        writeToLogs("Starting Rendering Catalog Objects");
        scrollPane.setVisible(true);
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);
        int rownum = 0, colnum = 0;
        for (String objectName : connectorsObjectList) {
            if (objectName != null && objectName.length() > 0) {
                CheckBox cBox = new CheckBox(objectName);
                cBox.setMnemonicParsing(false);
                grid.add(cBox, colnum, rownum);
                colnum++;
                if (colnum % 3 == 0) {
                    rownum++;
                    colnum = 0;
                }
            }
        }
        scrollPane.setContent(grid);
        scrollPane.setVisible(true);
        modifyButton.setVisible(true);
        writeToLogs("Rendering Catalog Objects Completed");
    }

    private void renderConnectorParams(List<Map<String, String>> connectorMetaDataList) {
        writeToLogs("\n Start Rendering connector MetaData ");
        writeToLogs("\n Total Properties for connector : " + connectorMetaDataList.size());
        List<String> supportedFeatures = ConnectorsUtil.getSupportingFeatures(connectorMetaDataList);
    
        flowpane.getChildren().removeAll(flowpane.getChildren());

        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);
        int rownum = 0, colnum = 0;
        boolean hasSandboxFields = false;
        Collections.sort(connectorMetaDataList, new Comparator<Map>() {
            public int compare(Map m1, Map m2) {
                if (m1.get("DisplayIndex") != null && m2.get("DisplayIndex") != null) {
                    try {
                        int m1DispIndex = Integer.parseInt(String.valueOf(m1.get("DisplayIndex")));
                        int m2DispIndex = Integer.parseInt(String.valueOf(m2.get("DisplayIndex")));
                        return m1DispIndex > m2DispIndex ? 1 : (m1DispIndex < m2DispIndex ? -1 : 0);
                    } catch (NumberFormatException e) {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }
        });

        if (connectorMetaDataList != null && connectorMetaDataList.size() > 0) {
            boolean hasSourceFilePrefix = false;
            for (int i = 0; i < connectorMetaDataList.size(); i++) {
                Map<String, String> prop = connectorMetaDataList.get(i);
                Iterator it = prop.entrySet().iterator();
                boolean isSecret = false;
                boolean isRequired = false;
                String name = "";
                String value = "";
                String displayLabel = "";

                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    if ("Secret".equalsIgnoreCase(pairs.getKey().toString())) {
                        isSecret = Boolean
                                .parseBoolean(pairs.getValue().toString());
                    }
                    if ("Required".equalsIgnoreCase(pairs.getKey().toString())) {
                        isRequired = Boolean.parseBoolean(pairs.getValue()
                                .toString());
                    }
                    if ("Name".equalsIgnoreCase(pairs.getKey().toString())) {
                        name = pairs.getValue().toString();
                    }
                    if ("Value".equalsIgnoreCase(pairs.getKey().toString())) {
                        value = pairs.getValue().toString();
                    }
                    if ("DisplayLabel".equalsIgnoreCase(pairs.getKey().toString())) {
                        displayLabel = pairs.getValue().toString();
                    }
                }

                if (name != null && !"SaveAuthentication".equalsIgnoreCase(name) && !supportedFeatures.contains(name) && name.toLowerCase().indexOf("sandbox") == -1 && name.toLowerCase().indexOf("sfdcclientid") == -1) {
                    Label propertyLabel = null;
                    if (isRequired) {
                        propertyLabel = displayLabel != null && displayLabel.trim().length() >= 1 ? new Label("*" + displayLabel) : new Label("*" + name);
                    } else {
                        propertyLabel = displayLabel != null && displayLabel.trim().length() >= 1 ? new Label(displayLabel) : new Label(name);
                    }
                    propertyLabel.setId("Label_" + name);
                    if (name.indexOf("SourceFileNamePrefix") != -1) {
                        grid.add(propertyLabel, 6, 0);
                        hasSourceFilePrefix = true;
                    } else {
                        grid.add(propertyLabel, colnum, rownum);
                        colnum++;
                    }
                    if (colnum != 0 && colnum % 4 == 0) {
                        rownum++;
                        colnum = 0;
                    }
                    if (!isSecret) {
                        TextField propertyTxField = new TextField();

                        if (value != null && value.length() >= 0) {
                            propertyTxField.setText(value);
                        }
                        if (isRequired) {
                            propertyTxField.setId("Mandatory_" + name);
                        } else {
                            propertyTxField.setId(name);
                        }
                        if (name.indexOf("SourceFileNamePrefix") != -1) {
                            grid.add(propertyTxField, 7, 0);
                        } else {
                            grid.add(propertyTxField, colnum, rownum);
                            colnum++;
                        }
                    } else {
                        PasswordField propertyTxField = new PasswordField();
                        if (value != null && value.length() >= 0) {
                            propertyTxField.setText(value);
                        }
                        if (isRequired) {
                            propertyTxField.setId("Mandatory_" + name);
                        } else {
                            propertyTxField.setId(name);
                        }
                        grid.add(propertyTxField, colnum, rownum);
                        colnum++;
                    }


                    if (colnum != 0 && colnum % 4 == 0) {
                        rownum++;
                        colnum = 0;
                    }
                }
                if (name != null && name.toLowerCase().indexOf("sandbox") != -1 && !hasSandboxFields) {
                    hasSandboxFields = true;
                }

            }
            writeToLogs("\n Rendering Connector MetaData Completed..");
            Button loginButton = new Button("Login");
            loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                validationLabel.setText("");
                hideScreen();
                boolean isConnectNSaveSuccess = false;
                writeToLogs("Login : Validating user input");
                List<String[]> userSubmittedValues = getUserSubmittedValues(); //0th Element Contains PropertyName, and 1st Element the value
                if (userSubmittedValues == null || userSubmittedValues.size() == 0) {
                    return;
                }
                writeToLogs("Login : updating MetaData with user Inputs");
                List< Map<String, String>> updatedMetaData = ConnectorsTestApp.updateMetaData(Context.getInstance().getConnectorMetaData(), userSubmittedValues);
                xmlCredentials = ConnectorsUtil.formatToXml(updatedMetaData);

                writeToLogs("Login : Generating Credentials XML ");
                writeToLogs(ConnectorsUtil.prettyFormat(xmlCredentials, 2));
                ConnectorInfo cInfo = ConnectorsTestApp.getConnectorInfo(Context.getInstance().getConnectorString(),
                        Context.getInstance().getCurrentConfig().spaceID,
                        Context.getInstance().getCurrentConfig().connectorControllerURI);
                try {
                    writeToLogs("Login : Requesting Login to Connector with saveAndConnect()");

                    isConnectNSaveSuccess = ConnectorsTestApp.saveAndConnect(xmlCredentials, Context.getInstance().getCurrentConfig().spaceID, Context.getInstance().getCurrentConfig().spaceDirectory, cInfo);
                    if(isConnectNSaveSuccess){
                         List<String> supportedFeatures = ConnectorsUtil.getSupportingFeatures(Context.getInstance().getConnectorMetaData());
                         Context.getInstance().setXmlCredentials(xmlCredentials);
                         if(supportedFeatures!=null && supportedFeatures.contains("QUERY")){
                             queryObjectButton.setVisible(true);
                             showsavedObjects.setVisible(true);
                             showCatalogObjects.setVisible(true);
                         }
                    }
                    writeToLogs("Login :  saveAndConnect() to " + Context.getInstance().getConnectorName() + " : " + isConnectNSaveSuccess);
                } catch (Exception ex) {
                    writeToLogs("Login :  saveAndConnect() to " + Context.getInstance().getConnectorName() + " : Failed");
                    writeToLogs(ex);
                }
                if (isConnectNSaveSuccess) {
                    writeToLogs("Connected to " + cInfo.connectorApplication);
                    boolean flag = ConnectorsUtil.serializeObject(updatedMetaData, ConnectorsTestApp.getConnectorInfo(Context.getInstance().getConnectorString(),
                            Context.getInstance().getCurrentConfig().spaceID,
                            Context.getInstance().getCurrentConfig().connectorControllerURI).connectorApplication);
                    writeToLogs("Saving user config for future use :: " + flag);
                    try {
                        writeToLogs("getCataLog() : Requesting Catalog of Objects from " + cInfo.connectorApplication);
                        List<String> remoteObjectsList = ConnectorsTestApp.validateConnectionPropertiesAndGetCatalog(xmlCredentials, Context.getInstance().getCurrentConfig().spaceID, Context.getInstance().getCurrentConfig().spaceDirectory, cInfo);
                        writeToLogs("getCataLog() : Total Objects Fetched from Catalog : " + remoteObjectsList.size());
                        renderScrollPane(remoteObjectsList);
                    } catch (Exception ex) {
                        writeToLogs("Exception occured Trying to fetch Remote Objects");
                        writeToLogs(ex);
                        validationLabel.setText("Unable to fetch Remote Objects. Please check logs for more details.");
                    }
                }
            }
        });
        if (hasSandboxFields) {
            if (hasSourceFilePrefix) {
                grid.add(new CheckBox("Use Sandbox"), 6, 1);
            } else {
                grid.add(new CheckBox("Use Sandbox"), 6, 0);
            }
        }
            grid.add(loginButton, 8, 0);
            flowpane.getChildren().add(grid);
        }
    }

    private List<String[]> getUserSubmittedValues() {
        List<String[]> mandatoryParameterValues;
        mandatoryParameterValues = new ArrayList<String[]>();
        GridPane gPane = null;
        for (int i = 0; i < flowpane.getChildren().size(); i++) {
            if (flowpane.getChildren().get(i) instanceof GridPane) {
                gPane = (GridPane) flowpane.getChildren().get(i);
            } else if (flowpane.getChildren().get(i) instanceof CheckBox) {
                CheckBox cBox = (CheckBox) flowpane.getChildren().get(i);
                String[] pObject = new String[2];
                if (cBox.isSelected()) {
                    pObject[0] = "UseSandBoxURL";
                    pObject[1] = "true";
                    mandatoryParameterValues.add(pObject);
                    if (Context.getInstance().getConnectorString().toLowerCase().indexOf("netsuite") != -1) {
                        String[] pObjectConn = new String[2];
                        pObjectConn[0] = "SandboxURL";
                        pObjectConn[1] = Context.getInstance().getCurrentConfig().netSuiteSandboxURL;
                        mandatoryParameterValues.add(pObjectConn);
                    }
                    if (Context.getInstance().getConnectorString().toLowerCase().indexOf("salesforce") != -1) {
                        String[] pObjectConn = new String[2];
                        pObjectConn[0] = "SandboxURL";
                        pObjectConn[1] = Context.getInstance().getCurrentConfig().salesForceSandboxURL;
                        mandatoryParameterValues.add(pObjectConn);
                    }
                    if (Context.getInstance().getConnectorString().toLowerCase().indexOf("marketo") != -1) {
                        String[] pObjectConn = new String[2];
                        pObjectConn[0] = "SandboxURL";
                        pObjectConn[1] = Context.getInstance().getCurrentConfig().marketoSandboxURL;
                        mandatoryParameterValues.add(pObjectConn);
                    }
                } else {
                    pObject[0] = "UseSandboxURL";
                    pObject[1] = "false";
                    mandatoryParameterValues.add(pObject);
                }
            }
        }
        for (int i = 0; i < gPane.getChildren().size(); i++) {
            if ((gPane.getChildren().get(i)) instanceof TextField) {
                String[] pObject = new String[2];
                pObject[0] = gPane.getChildren().get(i).getId();
                pObject[1] = ((TextField) gPane.getChildren().get(i)).getText().trim();
                System.out.println(pObject[0] + " : " + pObject[1] + ": " + gPane.getChildren().get(i).getId());
                if (pObject[0] != null && pObject[0].toLowerCase().indexOf("mandatory") != -1 && (pObject[1] == null || pObject[1].length() == 0)) {
                    validationLabel.setText("Mandatory Parmeters Missing");
                    writeToLogs("Login : Mandatory Parameter Missing " + pObject[0]);
                    gPane.getChildren().get(i).requestFocus();
                    return null;
                }
                pObject[0] = pObject[0].replace("Mandatory_", ""); //Removing mandatory label to compare with original metadata and update
                mandatoryParameterValues.add(pObject);
            }
        }
        writeToLogs("Login : Validation Successful for " + mandatoryParameterValues.size() + " connector properties");
        return mandatoryParameterValues;
    }

    @FXML
    private void handleSaveObjects(ActionEvent event) throws Exception {
        String xmlContent= "";
        Map<String, String> extractionSelectionOptionsMap = new HashMap<String, String>();
        List<String> queryObjects = new ArrayList<String>();
        writeToLogs("Fetching Selected Object Properties");
        GridPane gPane = (GridPane) selectedPane.getContent();
        for (int i = 0; i < gPane.getChildren().size(); i++) {
            if (gPane.getChildren().get(i) instanceof CheckBox) {
                CheckBox cBox = (CheckBox) gPane.getChildren().get(i);
                if(!cBox.isDisabled()){
                    if (cBox.isSelected()) {
                        System.out.println(cBox.getText() + " is Selected");
                        extractionSelectionOptionsMap.put(cBox.getId(), "true");
                    } else {
                        extractionSelectionOptionsMap.put(cBox.getId(), "false");
                    }
                }else{
                    queryObjects.add(cBox.getId());
                }
            }
        }
        String xmlContentObjects = ConnectorsUtil.generateIsIncrementalXml(extractionSelectionOptionsMap);
        xmlContentObjects = xmlContentObjects.replaceAll("\\<IsIncrementalRoot\\>", "").replaceAll("\\</IsIncrementalRoot\\>", "");
        
        String xmlContentQuery = "";
        if(queryObjects.size()>0){
                    String connectorConfigName = Context.getInstance().getConnectorName();
                    if("Salesforce".equalsIgnoreCase(connectorConfigName)){
                        connectorConfigName="sfdc";
                    }
                    ConnectorConfig settings = getConnectorSettings(Context.getInstance().getCurrentConfig().spaceDirectory,connectorConfigName);
                    List<ExtractionObject> extractionObjects = settings.getExtractionObjects();
                    for(String objectName : queryObjects){
                        for(ExtractionObject ex : extractionObjects){
                            if(objectName.equalsIgnoreCase(ex.name)){
                                xmlContentQuery += "<ExtractionObject>";
                                xmlContentQuery+="<ObjectName>" +ex.name +"</ObjectName>";
                                xmlContentQuery+="<Type>Query</Type>";
                                xmlContentQuery+="<Query>"+ex.query +"</Query>";
                                xmlContentQuery+="<Mapping>"+ex.columnMappings +"</Mapping>";
                                xmlContentQuery+="<UseLastModified>false</UseLastModified>";
                                xmlContentQuery+="</ExtractionObject>";
                            }
                        }
                    }

        }
        xmlContent = "<IsIncrementalRoot>"+xmlContentObjects + (xmlContentQuery.length()>0 ? xmlContentQuery : "") + "</IsIncrementalRoot>";
        writeToLogs("Generating Incremental XML to write in repository");
        writeToLogs(ConnectorsUtil.prettyFormat(xmlContent, 2));
        writeToLogs("Setting IncrementalContent for the Context.");
        Context.getInstance().setObjectsForExtraction(xmlContent);
        writeToLogs("Trying to Modify the Repository with Selected Changes");
        try {
            saveSelectedObjects(xmlContent, Context.getInstance().getCurrentConfig().spaceDirectory, Context.getInstance().getConnectorName());
            writeToLogs("Repository Modification Successful");
            extractButton.setVisible(true);
        } catch (Exception ex) {
            writeToLogs("Repository Modification Failed");
            writeToLogs(ex);
        }
    }

    @FXML
    private void handleModifyObjects(ActionEvent event) throws Exception {

        List<String> selectedObjectsList = new ArrayList<String>();
        GridPane gPane = (GridPane) scrollPane.getContent();
        for (int i = 0; i < gPane.getChildren().size(); i++) {
            CheckBox cBox = (CheckBox) gPane.getChildren().get(i);
            if (cBox.isSelected()) {
                selectedObjectsList.add(cBox.getText());
            }
        }
        String connectorConfigName = Context.getInstance().getConnectorName();
        if("Salesforce".equalsIgnoreCase(connectorConfigName)){
                connectorConfigName="sfdc";
         }
        ConnectorConfig settings = getConnectorSettings(Context.getInstance().getCurrentConfig().spaceDirectory,connectorConfigName);
        List<ExtractionObject> extractionObjects = settings.getExtractionObjects();
        List<String> queryObjectsList=  new ArrayList<String>();
                if(extractionObjects!=null){
                    for(ExtractionObject ex : extractionObjects){
                        if(ex.query!=null){
                            queryObjectsList.add(ex.name);
                        }
                    }
                }
        writeToLogs("Total Number of Objects Selected for Modification : " + selectedObjectsList.size());
        if (selectedObjectsList.size() > 0) {
            writeToLogs("Enabling selection Pane");
            selectedPane.setVisible(true);
            GridPane grid = new GridPane();
            grid.setVgap(10);
            grid.setHgap(10);
            int rownum = 0, colnum = 0;
            for (String objectName : selectedObjectsList) {
                CheckBox cBox = new CheckBox("Transactional");
                if(queryObjectsList.contains(objectName)){
                    cBox.setDisable(true);
                }
                cBox.setId(objectName);
                Label l1 = new Label(objectName);
                grid.add(l1, colnum, rownum);
                colnum++;
                grid.add(cBox, colnum, rownum);
                colnum++;
                if (colnum % 2 == 0) {
                    rownum++;
                    colnum = 0;
                }

            }
            selectedPane.setContent(grid);
            selectedPane.setVisible(true);
            saveButton.setVisible(true);
        } else {
            writeToLogs("No Objects Selected for Modification");
        }

    }

    @FXML
    private void handleExtractObjects(ActionEvent event) throws Exception {

        String spaceID = Context.getInstance().getCurrentConfig().spaceID;
        String spaceDirectory = Context.getInstance().getCurrentConfig().spaceDirectory;

        ConnectorInfo cInfo = ConnectorsTestApp.getConnectorInfo(Context.getInstance().getConnectorString(), Context.getInstance().getCurrentConfig().spaceID, Context.getInstance().getCurrentConfig().connectorControllerURI);
        final String[] extractionStatus = ConnectorsTestApp.startExtract(xmlCredentials, spaceID, Context.getInstance().getCurrentConfig().spaceDirectory, cInfo, Context.getInstance().getObjectsForExtraction().toString(), "GUI");
        writeToLogs("Requesting Start Extraction for the selected Objects : " + extractionStatus[1]);
        if (Boolean.parseBoolean(extractionStatus[1])) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    monitorExtract(extractionStatus[0]);
                }
            }).start();
        }
    }

    @FXML
    private void handleFileExport(ActionEvent event) throws Exception {
        try {
            String fileName = "BirstConnectorsTest_" + new java.util.Date().getTime() + ".log";
            writeToLogs("Trying to Export to " + fileName);
            FileWriter f = new FileWriter(new File(fileName));
            f.write(logsTextArea.getText());
            f.close();
            writeToLogs("Logs Exported Successfully to " + fileName);

        } catch (Exception ex) {
            writeToLogs("Error writing logs to disk.");
            writeToLogs(ex);
        }
    }

    public void monitorExtract(String uid) {

        long startTime = System.currentTimeMillis();
        long endTime;

        writeToLogs("Extraction Progress ");
        String[] statusFileArray = new String[]{"success", "failed", "killed"};
        try {
            boolean monitorResponse = true;
            while (monitorResponse) {
                Thread.sleep(2000);
                logsTextArea.appendText(".");
                for (String statusFile : statusFileArray) {
                    String fileName = Context.getInstance().getCurrentConfig().spaceDirectory + "\\slogs\\" + statusFile + "-extract-" + uid + ".txt";
                    File f = new File(fileName);
                    if (f.exists()) {
                        endTime = System.currentTimeMillis();
                        System.out.println();
                        writeToLogs(uid + " : Extraction Status " + statusFile);
                        writeToLogs("Total Extraction Time : " + (endTime - startTime) / 1000 + " seconds");
                        monitorResponse = false;
                        break;
                    }
                }

            }
        } catch (Exception ex) {
            writeToLogs("Error Occured During Extraction Monitoring");
            writeToLogs(ex);
        }

    }

    private void writeToLogs(String text) {
        logsTextArea.appendText("\n" + new java.util.Date() + " : " + text);
    }

    private void writeToLogs(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        logsTextArea.appendText("\n" + new java.util.Date() + " : Error : " + errors.toString());

    }

    private void hideScreen() {
        scrollPane.setVisible(false);
        selectedPane.setVisible(false);
        modifyButton.setVisible(false);
        saveButton.setVisible(false);
        extractButton.setVisible(false);
        queryObjectButton.setVisible(false);
        showsavedObjects.setVisible(false);
        showCatalogObjects.setVisible(false);
    }
    
    public void showSavedObjects() throws Exception{
               
                String connectorConfigName = Context.getInstance().getConnectorName();
                if("Salesforce".equalsIgnoreCase(connectorConfigName)){
                    connectorConfigName="sfdc";
                }
                ConnectorConfig settings = getConnectorSettings(Context.getInstance().getCurrentConfig().spaceDirectory,connectorConfigName);
                List<ExtractionObject> extractionObjects = settings.getExtractionObjects();
                List<String> savedObjects=  new ArrayList<String>();
                if(extractionObjects!=null){
                    for(ExtractionObject ex : extractionObjects){
                        savedObjects.add(ex.name);
                    }
                }
                scrollPane.setVisible(false);
                if(savedObjects!=null && savedObjects.size()>0){
                    renderScrollPane(savedObjects);
                }else{
                    writeToLogs("No Saved Objects found!");
                }
    }
    
    public void showCatalog(){
        
    }
}
