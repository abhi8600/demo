/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.connectorstestfx;

import com.birst.connectorstestfx.beans.ConfigProperties;
import com.birst.connectorstestfx.beans.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author asharma
 */
public class Birst_uiController implements Initializable {
    private static final String DEFAULT_CONFIG_FILE_FOR_CONNECTORS = "config//Connectors.config";
    @FXML
    private TextField connectorControllerURI;
    @FXML
    private TextField spaceID;
    @FXML 
    private TextField spaceDirectory;
    @FXML
    private Label validationLabel;
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws Exception {
        ConfigProperties configBean = new ConfigProperties();
        configBean.connectorControllerURI =  connectorControllerURI.getText();
        configBean.spaceID = spaceID.getText();
        configBean.spaceDirectory =spaceDirectory.getText();
        if(connectorControllerURI.getText().length()==0 || spaceID.getText().length()==0|| spaceDirectory.getText().length()==0){
            validationLabel.setText("Mandatory parameters missing.");
            return;
        }
       validationLabel.setText("Setting Configurations for the context");
      
       validationLabel.setText("Fetching Installed Connectors");
       if(Context.getInstance().getCurrentConfig().netSuiteSandboxURL!=null && Context.getInstance().getCurrentConfig().netSuiteSandboxURL!=null){
           configBean.netSuiteSandboxURL=Context.getInstance().getCurrentConfig().netSuiteSandboxURL;
       }else{
           configBean.netSuiteSandboxURL="";
       }
        if(Context.getInstance().getCurrentConfig().salesForceSandboxURL!=null && Context.getInstance().getCurrentConfig().salesForceSandboxURL!=null){
           configBean.salesForceSandboxURL=Context.getInstance().getCurrentConfig().salesForceSandboxURL;
       }else{
            configBean.salesForceSandboxURL="";
        }
        
      if(Context.getInstance().getCurrentConfig().marketoSandboxURL!=null && Context.getInstance().getCurrentConfig().marketoSandboxURL!=null){
           configBean.marketoSandboxURL=Context.getInstance().getCurrentConfig().marketoSandboxURL;
       }else{
            configBean.marketoSandboxURL="";
      }
       try{
        Context.getInstance().setCurrentConfig(configBean);
        boolean fileFlag = writeConfigFile(configBean);
        System.out.println("Updating Properties File Successful :" + fileFlag);
       }catch(Exception ex){
           ex.printStackTrace();
       }
       ConnectorsTestApp connectorApp = new ConnectorsTestApp();
       List<Map<String,String>> connectorsList = connectorApp.getConnectorsList(configBean.spaceID , configBean.connectorControllerURI);
       validationLabel.setText("Total Connectors Found :" + connectorsList.size());
       Context.getInstance().setConnectorsList(connectorsList);
       MainApplication.getInstance().showTestInterface();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         ConfigProperties configBean =null;
         
       try{
           configBean =  readConfigFile();
           if(configBean.connectorControllerURI!=null && configBean.connectorControllerURI.length()>0){
               connectorControllerURI.setText(configBean.connectorControllerURI);
           }if(configBean.spaceID!=null && configBean.spaceID.length()>0){
               spaceID.setText(configBean.spaceID);
           }if(configBean.spaceDirectory!=null && configBean.spaceDirectory.length()>0){
               spaceDirectory.setText(configBean.spaceDirectory);
           }
       }catch(Exception ex){
       }
    }    
    
    public static ConfigProperties readConfigFile() throws IOException {
		ConfigProperties confBean = new ConfigProperties();
		Properties props = new Properties();
		props.load(new FileInputStream(DEFAULT_CONFIG_FILE_FOR_CONNECTORS));
		confBean.connectorControllerURI = props
				.getProperty("connectorcontrollerURI");
		confBean.spaceID = props.getProperty("spaceID");
		confBean.spaceDirectory = props.getProperty("spaceDirectory");
                confBean.salesForceSandboxURL=props.getProperty("salesforceSandboxURL");
                confBean.netSuiteSandboxURL=props.getProperty("netSuiteSandboxURL");
                confBean.marketoSandboxURL=props.getProperty("marketoSandboxURL");
                System.out.println(confBean.netSuiteSandboxURL);
                System.out.println(confBean.marketoSandboxURL);
                Context.getInstance().setCurrentConfig(confBean);
		return confBean;
	}
    
         public static boolean writeConfigFile(ConfigProperties config)  {
		boolean isFileWriteSuccess=false;
                try{
		Properties props = new Properties();
		props.setProperty("connectorcontrollerURI",config.connectorControllerURI);
		props.setProperty("spaceID",config.spaceID);
		props.setProperty("spaceDirectory",config.spaceDirectory);
                props.setProperty("netSuiteSandboxURL",config.netSuiteSandboxURL);
                props.setProperty("salesforceSandboxURL",config.salesForceSandboxURL);
                props.setProperty("marketoSandboxURL",config.marketoSandboxURL);
                File f = new File(DEFAULT_CONFIG_FILE_FOR_CONNECTORS);
                OutputStream out = new FileOutputStream( f );
                props.store(out,"ConnectorsTestFX version 1.0.0 beta");
                isFileWriteSuccess=true;
                }catch(IOException ex){
                }
		return isFileWriteSuccess;
	}
   
}
