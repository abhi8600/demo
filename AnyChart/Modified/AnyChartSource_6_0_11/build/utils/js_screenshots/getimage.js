var page = new WebPage(), address, output, size, width, height;

/* phantom.exit(code) 
	  1: Invalid args was given to phantomjs
	  2: Htmlfile didn't create
	 10: Ok
	 11: Chart was created but didn't render
	 12: Chart was rendered but didn't draw
	 14: Chart has some error and doesn't even render
     111: Malformed XML
     112: Schema validation fail	
	 113: Mapping error
     114: Timestamp error
     121: Malformed Function
	 122: Feature not supported
	 123: There are axisPlot only
     131: Resource loading error
	 132: Security error
 */
 

if (phantom.args.length != 2) {
	phantom.exit("01");
} else {
    width = 800;
    height = 10;
    address = phantom.args[0];
    output = phantom.args[1];

    page.viewportSize = { width: width, height: height };
    page.open(address, function (status) {
        if (status !== 'success') {
            phantom.exit("02");
        }
    });
    
    var currentTime = new Date();
    var time_start = currentTime.getTime();
	var code = '14';
    page.onConsoleMessage = function(msg) {
		if (msg.indexOf('chart error') > -1){
			msg = msg.replace("chart error:", "");
			code = '1' + msg;
			phantom.exit(code);
        }else if (msg == "Feature not supported yet"){
            phantom.exit('122');
		}else if(msg == "chart create" ){
			code = '11';
		}else if(msg == "chart render"){
			code = '12';
		}else if(msg == "chart draw"){
			page.render(output);
			phantom.exit('10');			
		}else{
			console.log(msg);
			phantom.exit('13');
		};
	};
	
	function codeChecker(c){
		if(code==c){
			phantom.exit(code);
		};
	}
	if (code=='14'){
		window.setTimeout("codeChecker('14')", 80000);
	}else if (code == '11'){
		window.setTimeout("codeChecker('11')", 80000);
	}else if (code == '12'){
		window.setTimeout("codeChecker('12')", 1000);
	};
}   



