#!/usr/bin/env python

import os
import subprocess
import build_utils

SRC = os.path.join(build_utils.get_root(), "js-engine", "src", "anychart")
GJSTAGS = "/opt/local/bin/gjstags"

for root,dir,files in os.walk(SRC):
    subprocess.call((GJSTAGS,\
                     os.path.join(root,"*.js"),\
                     "-f %s" % os.path.join(root,"tags"),\
                     "-b %s" % root))
    print "Generating ctags for %s" % root
