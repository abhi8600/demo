#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
класс InputFolder(alias, path):
    string alias - название папки
    string path - путь к папке
    
класс DataIn():
    метод add(folder) добавляет папку, где:
        folder - экземпляр класса InputFolder, папка
        
класс ImagesGenerator():
    метод generate(n, output_folder, path_js_folder, data) запускает генерацию картинок, где
        n - количество потоков
        output_folder - папка, куда генерировать
        path_js_folder - папка, где лежат .js файлы
        data - экземпляр класса DataIn, входные данные
        @return list passedTests : упешные тесты
        @return list failedTests : провалившиеся тесты
        
    метод generateResize(n, output_folder, passedTests) запускает генерацию картинок с измененным размером, где
        n - количество потоков
        output_folder - папка, куда генерировать
        list passedTests - список тестов, для которых нужно запускать ресайз (успешно прошедших предыдущие этапы)
        @return list passedTests : упешные тесты
        @return list failedTests : провалившиеся тесты
        
класс ImagesComparer():
    метод сompare(passedTests, suspectedTests, comparing_folder, step) запускает сравнение картинок ,где:
        list passedTests - список тестов, для которых нужно запускать compare (успешно прошедших предыдущие этапы)
        list suspectedTests - список подозрительных тестов, которые нужно перепроверить
        comparing_folder - папка, с исходыми изображениями (с которыми сравнивать)
        step - описание текущего действия
        @return list passedTests : упешные тесты
        @return list suspectedTests : подозрительные тесты (оставшиеся подозрительными)
        @return list failedTests : провалившиеся тесты
    метод сompareResized(passedTests, comparing_folder, step) запускает сравнение картинок, где:
        list passedTests - список тестов, для которых нужно запускать compare (успешно прошедших предыдущие этапы)
        comparing_folder - папка, с исходыми изображениями (с которыми сравнивать)
        step - описание текущего действия
        @return list passedTests : упешные тесты
        @return list failedTests : провалившиеся тесты
                
класс ImageGenerationResults(baseLink, passedTests, failedTests)
    baseLink : baseLink для bamboo
    passedTests : упешные тесты
    failedTests : провалившиеся тесты
    
    метод generateXMLReport() генерирует XML отчет
    метод generateHTMLReport() генерирует HTML отчет
    метод generateFailedSamples() генерирует папку со упавшими тестами
'''
import os
import build_utils
import shutil
from glob import glob
import threading, Queue
import time
import subprocess
import hashlib
import datetime
import Image
import math, operator
import platform
from pymongo import Connection
import json

now = datetime.datetime.now()
start = time.time()
totalTests = 0
IS_WIN = (platform.system() == 'Windows')

ROOT = build_utils.get_root()
PATH_TO_OUT = os.path.join(ROOT, "out", "js_screenshots")
PATH_TO_TESTS = os.path.join(ROOT, "tests", 'automatic') 
TRIAL_PATH = os.path.join(ROOT, "out", "trial")
UTILS_PATH = os.path.join(ROOT, "build", "utils", "js_screenshots")
HTML5_vs_FLASH_DIR = '/home/anychart-builders/anychart-python-server/webapp/html5vsFlash'
#HTML5_vs_FLASH_DIR = '/home/olga/work/uwsgi/anychart_dev/svn_tests/html5vsFlash' ###

# Data base initializing:
connection = Connection()
db = connection.marishina_db
marisha_db = db.marishina_db

if IS_WIN:
    PATH_TO_PHANTOMJS = os.path.join(ROOT, "out")
else:
    PATH_TO_PHANTOMJS = '/usr/local/bin'
if not os.path.exists(PATH_TO_OUT):
    os.mkdir(PATH_TO_OUT)

def setOutput(path):
    global PATH_TO_OUT
    PATH_TO_OUT = path

def copyJsFiles(key, number):
    shutil.copy(os.path.join(TRIAL_PATH, 'AnyChart.js'), os.path.join(HTML5_vs_FLASH_DIR, 'utils', 'AnyChart.js'))
    shutil.copy(os.path.join(TRIAL_PATH, 'AnyChartHTML5.js'), os.path.join(HTML5_vs_FLASH_DIR, 'utils', 'AnyChartHTML5.js'))
    shutil.copy(os.path.join(TRIAL_PATH, 'AnyChart.swf'), os.path.join(HTML5_vs_FLASH_DIR, 'utils', 'AnyChart.swf'))
    data = str(key+"-"+number)
    open(os.path.join(HTML5_vs_FLASH_DIR, 'utils', 'buildNumber.txt'), "w").write(data)

class ReturnError(Exception):
    '''
    возвращает сообщение об ошибке и приоритет
    '''
    def __init__(self, message, priority = 'MEDIUM'):
        '''
        @constructor
        @param string message : сообщение об ошибке
        @param enum priority : выставляет приоритет ошибки ('LOW','MEDIUM','HIGH')
        '''
        self.message = message
        self.priority = priority

class GenerationError(Exception):
    '''
    возвращает сообщение и приоритет ошибки по коду  
    '''
    def __init__(self, code):
        """
        @constructor
        @param int code : код ошибки
        """
        if code == 1:
            self.message = 'Invalid args'
            self.priority = 'HIGH'
        elif code == 2:
            self.message = 'Html file error'
            self.priority = 'HIGH'
        elif code == 11:
            self.message =  'Chart created only'
            self.priority = 'MEDIUM' 
        elif code == 12:
            self.message =  'Chart rendered only'
            self.priority = 'MEDIUM' 
        elif code == 13:
            self.message =  'Chart printed error'
            self.priority = 'HIGH'
        elif code == 111:
            self.message =  'Malformed XML'
            self.priority = 'HIGH'
        elif code == 112:
            self.message =  'Schema validation fail'
            self.priority = 'HIGH'
        elif code == 113:
            self.message =  'Mapping error'
            self.priority = 'MEDIUM'
        elif code == 114:
            self.message =  'Timestamp error'
            self.priority = 'MEDIUM'
        elif code == 121:
            self.message =  'Malformed Function'
            self.priority = 'HIGH'
        elif code == 122:
            self.message =  'Feature not supported'
            self.priority = 'LOW'
        elif code == 123:
            self.message =  'Some elements not supported'
            self.priority = 'MEDIUM'
        elif code == 131:
            self.message =  'Resource loading error'
            self.priority = 'MEDIUM'
        elif code == 132:
            self.message =  'Security error'
            self.priority = 'MEDIUM'
        else:
            self.message =  'Phantom timeout' 
            self.priority = 'HIGH'
 
class InputFolder:
    '''
    Определение папки (алиас, путь)
    '''
    def __init__(self, group_name, path):
        '''
        @constructor
        @param group_name - название для группы (алиас)
        @param path - путь к файлам 
        '''
        self.group_name = group_name
        self.path = path

class DataIn:
    '''
    Определение входных данных
    '''
    def __init__(self):
        '''
        @constructor
        '''
        self.folders = []
    def add(self, Folder):
        '''
        Добавлет экземпляр класса InputFolder к списку папок в классе 
        @param Folder - экземпляр класса InputFolder
        '''
        self.folders.append(Folder)

class Item:
    '''
    Определение экземпляра html-страницы
    '''
    def __init__(self, group_name, file_name, config, output):
        '''
        @constructor
        @param string group_name : алиас группы к которой принадлежит страница
        @param string file_name : имя файла
        @param string config : XML файл (sample.xml)
        @param string output : PNG файл, куда будет снят скриншот со страницы (sample.png)
        '''
        self.group_name = group_name
        self.file_name = file_name
        self.config = config
        self.output = output
   
class FailedTest:
    '''
    Определение провалившегося теста 
    '''
    def __init__(self, sample_config_path, case_name, error_code, priority, error_message, group_name, step = None, image = None):
        '''
        @constructor
        @param string sample_config_path : XML файл (sample.xml)
        @param string case_name : имя файла 
        @param string error_code : сообщение об ошибке
        @param enum priority : приоритет ('LOW','MEDIUM','HIGH')
        @param string message : дополнительное сообщение об ошибке (если есть), если нет - пустая строка
        @param string group_name : алиас группы к которой принадлежит тест
        @param string step : описание действия, которое выполнялось, когда тест упал
        @param string image : путь до png файла содержащего картинку
        ''' 
        self.config = sample_config_path
        self.case_name = case_name
        self.group = group_name
        self.code = error_code
        self.priority = priority
        self.message = error_message
        self.step = step
        self.image = image

class FailedTests:
    '''
    Определение провалившихся тестов
    '''
    def __init__(self):
        self.tests = []
    def add(self, failedTest):
        '''
        Добавлет экземпляр класса FailedTest к списку провалившихся тестов
        @param failedTest - экземпляр класса FailedTest
        '''
        self.tests.append(failedTest)

class PassedTest:
    '''
    Определение пройденного теста 
    '''
    def __init__(self, sample_config_path, case_name, group_name, image):
        '''
        @constructor
        @param string sample_config_path : XML файл (sample.xml)
        @param string case_name : имя файла 
        @param string group_name : алиас группы к которой принадлежит тест
        @param string image : путь до png файла содержащего картинку
        ''' 
        self.config = sample_config_path
        self.case_name = case_name
        self.group = group_name
        self.image = image

class PassedTests:
    '''
    Определение всех пройденных тестов
    '''
    def __init__(self):
        self.tests = []
    def add(self, PassedTest):
        '''
        Добавлет экземпляр класса PassedTest к списку провалившихся тестов
        @param passedTest - экземпляр класса PassedTest
        '''
        self.tests.append(PassedTest)
        
class ImageGenerationResults:
    '''
    Генерация результата
    '''
    def __init__(self, linkCode, linkNumber, passedTests, failedTests):
        '''
        @constructor
        @param int totalTests : общее количество тестов
        ''' 
        self.timePassed = str(round((time.time() - start), 2))
        self.passed = passedTests
        self.failed = failedTests
        self.linkCode = linkCode
        self.linkNumber = linkNumber
        self.baseLink = 'http://dev.anychart.com/bamboo/artifact/%s/shared/build-%d/screenshots-generation-results/' %(linkCode, int(linkNumber))
        
    def generateXMLReport(self):  
        '''
        Генерирует xml отчет
        '''
        content = ''
        count = 0
        for i in self.failed:
            tmp_link = i.case_name.replace('/', '-')
            link = "%s/failed/%s" % (self.baseLink, tmp_link)
            message = '%s\n\t\t%s\n\t\t%s' % (i.code, link, i.message)
            message = '<![CDATA[%s]]>' % message
            if i.priority == 'HIGH':
                count += 1
                content += '\t<testcase classname=\"%s\" name=\"%s\" description = \"%s\">\n\t\t<failure type=\"%s\"> \n \t\t%s \n\t\t</failure>\n \t</testcase>\n' %(i.step + '-' + i.group, i.case_name, i.code, i.code, message)
            else:
                content += '\t<testcase classname=\"%s\" name=\"%s\" > </testcase>\n' %(i.step + '-' + i.group, i.case_name)
        for j in self.passed:
            content += '\t<testcase classname=\"%s\" name=\"%s\" > </testcase>\n' %(j.group, j.case_name)
        res = open(PATH_TO_OUT + '/test-results.xml', "w")
        res.write("<?xml version='1.0' encoding='utf-8'?>\n")
        res.write("<testsuite time=\"%s\" errors=\"%d\" tests=\"%d\"> \n" % (self.timePassed, count, len(self.passed + self.failed)))
        res.write(content)
        res.write("</testsuite>")
        
    def generateHTMLReport(self, all_generated, all_comparedVersion, all_comparedSelected, all_generatedResized, all_comparedResized):
        '''
        Генерирует html отчет
        '''
        content = ''
        html_report_file = os.path.join (PATH_TO_OUT, 'test-results.html')
        sum_low = 0
        sum_medium = 0
        sum_high = 0
        sum_docs = 0
        sum_gallery = 0
        sum_tests = 0
        sum_generation = 0
        sum_generationResized = 0
        sum_comparingVersion = 0
        sum_comparingSelected = 0
        sum_comparingResized = 0
        sum_empty = 0
        error_number = 0
        stepClass = ''
        for failedTest in self.failed:
            error_number += 1
            try:
                test = marisha_db.find_one({"name": failedTest.case_name.replace('.html', '')})['errors']
                if test['description']!='':
                    failedTest.message += '<p>Message: ' + test['description']
                if test['ticket']!='':
                    failedTest.message += '<p>Tickets: '
                    for t in test['ticket'].split(';'):
                        if t !='':
                            failedTest.message += '<a href = "http://dev.anychart.com/jira/browse/%s" >%s</a>; ' %(t, t)
                if test['types']!='':
                    failedTest.message += '<p>Types: ' + test['types'].replace(';', '; ')

            except Exception:
                pass
            try:
                test = marisha_db.find_one({"name": failedTest.case_name.replace('.html', '')})
                if test['history']!='':
                    failedTest.message += '<p class="history">History: ' + test['history']
            except Exception:
                pass
            if IS_WIN:
                tmp_file = failedTest.case_name.replace('\\', '-')
            else:
                tmp_file = failedTest.case_name.replace('/', '-')
            linkExample = '<a class=\"view linkExample\" href = \"%s\" target=\"blank\"> </a> ' %(os.path.join(self.baseLink, 'failed', tmp_file))
            tmp_file = os.path.join(self.baseLink, 'failed', tmp_file.replace('.html','.xml'))
            if failedTest.priority == 'LOW':
                sum_low += 1
            elif failedTest.priority == 'MEDIUM':
                sum_medium += 1
            elif failedTest.priority == 'HIGH':
                sum_high += 1
            if failedTest.group == 'docs':
                sum_docs += 1
            elif failedTest.group == 'gallery':
                sum_gallery += 1
            elif failedTest.group == 'tests':
                sum_tests += 1
                if IS_WIN:
                    tmp_file = failedTest.case_name.split('\\')[1]
                else:
                    tmp_file = failedTest.case_name.split('/')[1]
                folder_name = tmp_file.split('-')[0]
                tmp_file = tmp_file.split('-')[1]
                linkExample = '<a class=\"view linkExample\" href = \"%s\" target=\"blank\"> </a> ' %(os.path.join(self.baseLink, 'failed', folder_name, tmp_file.replace('.html',''), tmp_file))
                testCheckBox = "<input type='checkbox' class='%s/%s'>" %(folder_name, tmp_file.replace('.html',''))
            linkConfig = '<a class=\"view linkConfig\" href = \"%s\" target=\"blank\"> </a>' % tmp_file
            links = '<div class=\"links\"><p>%s %s </p></div>' %(linkExample, linkConfig)
            if failedTest.image is not None:
                href = 'http://dev.anychart.com/py/tests/diff/?key=%s&step=%s&image=%s' %(self.linkCode, failedTest.step, failedTest.image)
                linkDiff = '<a class=\"view linkDiff\" href = \"%s\" target=\"blank\"> </a>' % href
            if failedTest.step == 'Generation':
                sum_generation += 1
                stepClass = 'generation'
            if failedTest.step == 'Empty checking':
                sum_empty += 1
                stepClass = 'emptyChecker'
            elif failedTest.step == 'Comparing version':
                sum_comparingVersion += 1
                stepClass = 'comparingVersion'
                if failedTest.code == 'Different images':
                    links = '<div class=\"links\"><p>%s%s%s </p></div>' %(linkExample, linkConfig ,linkDiff)
            elif failedTest.step == 'Comparing selected':
                sum_comparingSelected += 1
                stepClass = 'comparingSelected'
                if failedTest.code == 'Different images':
                    links = '<div class=\"links\"><p>%s%s%s</p></div>' %(linkExample, linkConfig ,linkDiff)
            elif failedTest.step == 'Generation resized':
                sum_generationResized += 1
                stepClass = 'generationResized'
            elif failedTest.step == 'Comparing resized':
                sum_comparingResized += 1
                stepClass = 'comparingResized'
                if failedTest.code == 'Different images':
                    links = '<div class=\"links\"><p>%s%s%s</p></div>' %(linkExample, linkConfig ,linkDiff)
            checkBox = "<input type='checkbox' class='%s' DISABLED>" %(failedTest.case_name)
            if (failedTest.step == 'Comparing version') or (failedTest.step == 'Comparing selected' and failedTest.code == 'Image to compare missed'):
                if failedTest.group=='tests':
                    checkBox = testCheckBox
                else:
                    checkBox = "<input type='checkbox' class='%s' >" %(failedTest.case_name)
            if failedTest.message !='':
                message = '<p class=\"errorMore\" id=\"p_er_%s\"> Reason: %s.\n\t\t\t <div class=\"errorMore\" id=\"er_%s\"> %s </div>' %( str(error_number), failedTest.code, str(error_number), failedTest.message.encode('u8'), )
            else:
                message = '<p class=\"NoErrorMore\" id=\"p_er_%s\"> Reason: %s.\n\t\t\t' %(str(error_number), failedTest.code)
            content += '''\n\t\t<li class=\"%s\">\n\t\t\t  <div class='description'><p>%s<span class='step'>%s</span> %s \n\t\t\t %s \n\t\t\t</div>
            \t\t\t %s \t\t</li>''' %(failedTest.priority + ' ' + failedTest.group + ' ' + stepClass, checkBox, failedTest.step, failedTest.case_name, message, links)
        if os.path.exists(html_report_file):
            os.remove(html_report_file)
        timePassed = str(round((time.time() - start)/60, 2))
        shutil.copy(os.path.join(UTILS_PATH, "reportTemplate.html"), html_report_file)
        data = open(html_report_file).read()
        data = timePassed.join(data.split("$TIME"))
        data = now.strftime("%Y-%m-%d %H:%M").join(data.split("$DATE"))
        data = str(len(self.passed + self.failed)).join(data.split("$ALL"))
        data = str(len(self.failed)).join(data.split("$MISTAKES"))
        data = str(sum_low).join(data.split("$LOW"))
        data = str(sum_medium).join(data.split("$MEDIUM"))
        data = str(sum_high).join(data.split("$HIGH"))
        data = str(sum_docs).join(data.split("$DOCS"))
        data = str(sum_gallery).join(data.split("$GALLERY"))
        data = str(sum_tests).join(data.split("$TESTS"))
        data = str(sum_generation).join(data.split("$GENERATION_ALL"))
        data = str(sum_generationResized).join(data.split("$GENERATION_RESIZED"))
        data = str(sum_comparingVersion).join(data.split("$COMPARING_VERSION"))
        data = str(sum_comparingSelected).join(data.split("$COMPARING_SELECTED"))
        data = str(sum_comparingResized).join(data.split("$COMPARING_RESIZED"))
        data = str(sum_empty).join(data.split("$EMPTY_IMAGES"))
        data = str(all_generated).join(data.split("$GENERATED_CAME"))
        data = str(all_generatedResized).join(data.split("$GENERATION_R_CAME"))
        data = str(all_comparedVersion).join(data.split("$EMPTY_CAME"))
        data = str(all_comparedVersion - sum_empty).join(data.split("$COMPARING_CAME"))
        data = str(all_comparedSelected).join(data.split("$COMPARING_SEL_CAME"))
        data = str(all_comparedResized).join(data.split("$COMPARING_R_CAME"))
        data = self.linkNumber.join(data.split("$NUMBER"))
        data = self.linkCode.join(data.split("$CODE"))
        data = content.join(data.split("$CONTENT"))
        open(html_report_file, "w").write(data)
        
    def generateFailedSamples(self):
        '''
        Генерирует папку с провалившимися тестами (в нее не копируются тесты из группы tests)
        '''
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed', 'swf'))
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed', 'js'))
        if os.path.exists(os.path.join(PATH_TO_OUT, 'failed', 'failedResized')):
            shutil.rmtree(os.path.join(PATH_TO_OUT, 'failed', 'failedResized'))
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed', 'failedResized'))
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed', 'failedResized', 'docs'))
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed', 'failedResized', 'gallery'))
        if not os.path.exists(os.path.join(PATH_TO_OUT, 'failed', 'swf', 'AnyChart.swf')):
            shutil.copy(os.path.join(TRIAL_PATH, 'AnyChart.swf'), os.path.join(PATH_TO_OUT, 'failed', 'swf', 'AnyChart.swf'))
        shutil.copy(os.path.join(TRIAL_PATH, 'AnyChart.js'), os.path.join(PATH_TO_OUT, 'failed', 'js', 'AnyChart.js'))
        shutil.copy(os.path.join(TRIAL_PATH, 'AnyChartHTML5.js'), os.path.join(PATH_TO_OUT, 'failed', 'js', 'AnyChartHTML5.js'))
        for failedTest in self.failed:
            if failedTest.group != 'tests':
                if failedTest.step == 'Comparing resized':
                    shutil.copy(failedTest.image, os.path.join(PATH_TO_OUT, 'failed', 'failedResized', failedTest.case_name.replace('.html', '.png')))
                if IS_WIN: 
                    tmp_file = failedTest.case_name.replace('\\', '-')
                else:
                    tmp_file = failedTest.case_name.replace('/', '-')
                shutil.copy(os.path.join(UTILS_PATH, "template_failed.html"), os.path.join(PATH_TO_OUT, 'failed', tmp_file))
                config = tmp_file[:-4] + 'xml'
                html_file = os.path.join(PATH_TO_OUT, 'failed', tmp_file)
                data = open(html_file).read()
                data = config.join(data.split("$CONFIG"))
                open(html_file, "w").write(data)
                shutil.copy(failedTest.config, os.path.join(PATH_TO_OUT, 'failed', config))
            else:
                if IS_WIN:
                    case_name = failedTest.case_name.replace('tests\\', '')
                else:
                    case_name = failedTest.case_name.replace('tests/', '')
                parts = case_name.split('-')
                test_path = os.path.join(parts[0], parts[1].replace('.html', ''))
                if not os.path.exists(os.path.join(PATH_TO_OUT, 'failed', parts[0])):
                    os.mkdir(os.path.join(PATH_TO_OUT, 'failed', parts[0]))
                if not os.path.exists(os.path.join(PATH_TO_OUT, 'failed', parts[0], 'config.js')):
                    shutil.copy(os.path.join(PATH_TO_TESTS, parts[0], 'config.js'), os.path.join(PATH_TO_OUT, 'failed', parts[0], 'config.js'))
                    build_utils.replace_in_file(os.path.join(PATH_TO_OUT, 'failed', parts[0], 'config.js'), "var PATH_TO_JS_FOLDER = '[a-zA-Z\./]+';", "var PATH_TO_JS_FOLDER = '../../js/';")
                    build_utils.replace_in_file(os.path.join(PATH_TO_OUT, 'failed', parts[0], 'config.js'), "var PATH_TO_SWF_FILE = '[a-zA-Z\./]+';", "var PATH_TO_SWF_FILE = '../../swf/AnyChart.swf';")
                if not os.path.exists(os.path.join(PATH_TO_OUT, 'failed', test_path)):
                    shutil.copytree(os.path.join(PATH_TO_TESTS, test_path), os.path.join(PATH_TO_OUT, 'failed', test_path))
        
class ImagesGenerator:
    '''
    Генерирует изображения
    '''
    def __init__(self):
        '''
        @constructor
        '''
        self.queue = Queue.Queue(100)
        self.failed = FailedTests()
        self.passed = PassedTests()
         
    def getTimeout(self, config):
        '''
        Добавляет провалившийся тест в failed - экземпляр класса failedTests
        @param string config - xml файл с данными для графика
        @return int timeout - количество секунд для timeout
        '''
        lines_counter = 0
        if os.path.exists(config):
            data = open(config).readlines()
            lines_counter = len(data)
        else:
            config = config.replace('/data.xml', '')
            samples = glob(os.path.join(config, '*.xml'))
            samples = samples + glob(os.path.join(config, '*.json'))
            for f in samples:
                data = open(f).readlines()
                lines_counter += len(data)
        if lines_counter >= 3000:
            return 120        
        if lines_counter >= 1000 and lines_counter < 3000:
            return 45
        if lines_counter >= 100 and lines_counter < 1000:
            return 35
        if lines_counter < 100:
            return 30        
        
    def treatmentProcess(self, cmd, timeout):
        '''
        Запускает команду в subprocess
        @param string cmd : команда для запуска в subprocess.
        @return int exit_code : код, описывающий результат выполнения cmd.
        @return int priority : в случае ошибки выдает её приоритет, иначе NONE.
        @return string error_more :  в случае ошибки выдает дополнительные сведения об ошибке(из консоли phantomjs).
        '''
        env = os.environ.copy()
        env["DISPLAY"] = ":0"
        p = subprocess.Popen(cmd, shell=True, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        t_beginning = time.time()
        seconds_passed = 0
        error_more = ''
        printed = True
        try:
            while True:
                if p.poll() is not None:
                    exit_code = p.poll()
                    error_more = str(p.communicate()[0])
                    index = error_more.find('Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.')
                    if index > 0:
                        str_remove = error_more[index - 29: index + 133]
                        error_more = error_more.replace(str_remove, '')
                    p.stdout.close()
                    if exit_code == 10:
                        priority = 'NONE'
                        exit_code = 'Success'
                        break
                    else:
                        raise GenerationError(p.poll())
                seconds_passed = time.time() - t_beginning
                if seconds_passed > timeout/2 and printed:
                    print 'p.poll is %s. Still waiting for %s' % (p.poll(), cmd)
                    printed = False
                if seconds_passed > timeout:
                    subprocess.Popen.kill(p)
                    raise ReturnError('Process killed', priority = 'HIGH')
                    break
                time.sleep(0.1)
        except ReturnError as re:
            exit_code = re.message
            priority = re.priority
        except GenerationError as ge:
            exit_code = ge.message
            priority = ge.priority
        if exit_code == 'Chart printed error' and error_more=='':
            exit_code = 'Success'
            priority = 'NONE'
        return exit_code, priority, error_more   
        
    def generateImage (self, group_name, file_name, config, output, html_template, step):
        '''
        Запускает генерацию картинки
        @param string group_name : название группы (алиас).
        @return string file_name : имя html файла.
        @param string config : XML файл (sample.xml)
        @param string output : PNG файл, куда будет снят скриншот со страницы (sample.png)
        @param string step : описание текущего действия
        '''
        case_name = os.path.join(group_name, file_name)
        print "%s sec. %s %s " % (str(round(time.time() - start, 2)), step, case_name)
        if group_name == 'tests':
            tmp_file_name = os.path.join(file_name.split('-')[0],file_name.split('-')[1].split('.')[0], file_name.split('-')[1].split('.')[0] + '.html')
            tmp_file_name = os.path.join(PATH_TO_TESTS, tmp_file_name)
        else:
            if not os.path.exists(os.path.join(UTILS_PATH ,'tmp')):
                os.mkdir(os.path.join(UTILS_PATH ,'tmp'))
            tmp_file_name = os.path.join(UTILS_PATH ,'tmp', file_name)
            shutil.copy(os.path.join(UTILS_PATH, html_template), tmp_file_name)
            data = open(tmp_file_name).read()
            data = config.join(data.split("$CONFIG"))
            data = TRIAL_PATH.join(data.split("$PATH"))
            data = file_name.join(data.split("$TITLE"))
            open(tmp_file_name, "w").write(data) 
        if IS_WIN:
            cmd = [os.path.join(PATH_TO_PHANTOMJS,"phantomjs.exe")+" %s\\getimage.js %s %s" % (UTILS_PATH, tmp_file_name, output)]
        else:
            cmd = [os.path.join(PATH_TO_PHANTOMJS,"phantomjs")+" %s/getimage.js %s %s" % (UTILS_PATH, tmp_file_name, output)]
        timeout = self.getTimeout(config)
        exit_code, priority, error_more = self.treatmentProcess(cmd, timeout)
        if exit_code == 'Success':
            test = PassedTest(config, case_name, group_name, output)
            self.passed.add(test)            
        else:
            test = FailedTest(config, case_name, exit_code, priority, error_more, group_name, step)
            self.failed.add(test)
            try:
                self.passed.remove(test)
            except:
                pass
        if group_name!='tests':
            if os.path.exists(tmp_file_name):
                os.remove(tmp_file_name)
        
    def manageQueue(self, tread, html_template, step):
        '''
        Управление потоками в очереди 
        @param list tread : список экземпляров класса Item обрабатываемых в одном потоке(экземпляры Item - обрабатываемые html-страниц)
        @param string step : описание текущего действия
        ''' 
        while True:
            item = self.queue.get()
            for i in tread:
                self.generateImage(i.group_name, i.file_name, i.config, i.output, html_template, step)
            self.queue.task_done()

    def generateItems(self, ITEMS, NUMBER_OF_THREADS, output_folder, path_js_folder, html_template, step = None):

        n=len(ITEMS)/(NUMBER_OF_THREADS)
        TREADS = []
        for i in xrange(0, len(ITEMS), n): ###
#        n = 5
#        for i in xrange(0, 10, n):
            TREADS.append(ITEMS[i:i+n])
        i = 0
        for tread in TREADS:
            i += 1
            t = threading.Thread(target=self.manageQueue,  name= 'Thread_'+ str(i), args=[tread, html_template, step])
            t.daemon = True
            t.start()
            self.queue.put(t)	
        self.queue.join()
        
    def generate(self, NUMBER_OF_THREADS, output_folder, path_js_folder, data):
        '''
        Создание списка из всех страниц, для которых требуется сгенерировать скриншот
        @param list folders : список названий групп (список алиасов)
        @param string output_folder : путь к папке для генерации изображений
        @return passed.tests : уcпешные тесты
        @return failed.tests : провалившиеся тесты
        '''
        if os.path.exists(os.path.join(PATH_TO_OUT, 'failed')):
            shutil.rmtree(os.path.join(PATH_TO_OUT, 'failed'))
        os.mkdir(os.path.join(PATH_TO_OUT, 'failed'))
        global TRIAL_PATH 
        TRIAL_PATH = path_js_folder
        folders = data.folders
        ITEMS = [] 
        for folder in folders:
            group_name = folder.group_name
            path = folder.path
            output = os.path.join(output_folder, group_name)
            if os.path.exists(output):
                shutil.rmtree(output)
            os.mkdir(output)
            if group_name == 'tests':
                samples = glob(os.path.join(path, '*', '*', '*.html'))
                for f in samples:
                    filename = os.path.splitext(os.path.basename(f))[0]
                    if IS_WIN:
                        testname = f.split('\\')
                    else:
                        testname = f.split('/')
                    testname = str(testname[-3]) + '-' + filename
                    ITEMS.append(Item(group_name, testname+".html", f.replace(os.path.basename(f), 'data.xml'), os.path.join(output, testname+".png")))
            else:
                samples = glob(os.path.join(path,"*.xml"))
                for f in samples:
                    file_name = os.path.splitext(os.path.basename(f))[0]
                    if file_name != 'Scatter-Plot-Area-Chart-with-Date-Time-Scale-Large-Data-Set' and file_name != 'Scatter-Plot-Marker-Chart-Large-Data-Set':	### !?
                        ITEMS.append(Item(group_name, file_name+".html", f, os.path.join(output, file_name+".png")))
        self.generateItems(ITEMS, NUMBER_OF_THREADS, output_folder, path_js_folder, 'template.html', 'Generation')
        return self.passed.tests, self.failed.tests
    
    def generateResized(self, NUMBER_OF_THREADS, output_folder, passed):
        '''
        Создание списка из всех страниц, для которых уже успешно сгенерирован скриншот и теперь надо сгенерировать ресайз.
        @param list folders : список названий групп (список алиасов)
        @param string output_folder : путь к папке для генерации изображений 
        @return int len(ITEMS) : количество всех экземпляров класса Item (страниц, для которых требуется сгенерировать скриншот)
        '''
        ITEMS = []
        self.passed.tests = []
        if os.path.exists(output_folder):
            shutil.rmtree(output_folder)
        os.mkdir(output_folder)
        for f in passed:
            if f.group != 'tests':
                if not os.path.exists(os.path.join(output_folder, f.group)):
                   os.mkdir(os.path.join(output_folder, f.group)) 
                file_name = os.path.splitext(os.path.basename(f.config))[0]
                ITEMS.append(Item(f.group, file_name+".html", f.config, os.path.join(output_folder, f.group, file_name+".png")))
        if 1 < len(ITEMS) <= NUMBER_OF_THREADS:
            NUMBER_OF_THREADS = len(ITEMS) - 1
        elif len(ITEMS) == 1:
            NUMBER_OF_THREADS = 1
        elif len(ITEMS) < 1:
            return [], []
        if len(ITEMS) >= NUMBER_OF_THREADS:
            self.generateItems(ITEMS, NUMBER_OF_THREADS, os.path.join(PATH_TO_OUT, 'tmp'), TRIAL_PATH, 'resize_template.html', 'Generation resized')
        return self.passed.tests, self.failed.tests  

class ImagesComparer:
    '''
    Сравнение картинок из папок
    '''

    def emptyCkecking(self, image):
        EMPTY_FILE = os.path.join(UTILS_PATH, 'empty.png')
        im1 = Image.open(image)
        im2 = Image.open(EMPTY_FILE)
        if not im1.size == im2.size:
            im2 = im2.resize(im1.size, Image.BILINEAR)
        h1 = im1.histogram()
        h2 = im2.histogram()
        rms = math.sqrt(reduce(operator.add, map(lambda a,b: (a-b)**2, h1, h2))/len(h1))
        if rms < 1.5:
            return True
        return False

    def getmd5(self, image):
        '''
        Получение md5 картинки
        @param file image : файл png, md5 которого требуется получить
        @return md5 картинки
        '''  
        try:
            infile = open(image, 'rb')
            fileContent = infile.read()
            infile.close()
        except:
            return None
        m = hashlib.md5()
        m.update(fileContent)
        return m.hexdigest()        
    
    def diff(self, file_name, output_folder, comparing_folder):
        '''
        Сравнение картинок
        @param string file_name : имя файла (sample.png)
        @param string output_folder : папка, откуда брать картинку, чтобы сравнивать 
        @param string comparing_folder : папка, откуда брать картинку, c которой сравнивать
        @return int exit_code : код, описывающий результат выполнения сравнения
        @return enum priority : приоритет ошибки ('LOW','MEDIUM','HIGH', 'NONE'). 'NONE' - в случае, если нет ошибки
        '''  
        rightImage = os.path.join(comparing_folder, file_name)
        checkingImage = os.path.join(output_folder, file_name)
        if not os.path.exists(os.path.join(PATH_TO_OUT, 'failed', 'images')):
            os.mkdir(os.path.join(PATH_TO_OUT, 'failed','images'))
        try:
            if self.emptyCkecking(checkingImage):
                raise ReturnError('Empty image', 'HIGH')
            if self.getmd5(rightImage) is None:
                raise ReturnError('Image to compare missed', 'LOW')
            elif self.emptyCkecking(rightImage):
                raise ReturnError('Origin image is empty')
            elif self.getmd5(rightImage) == self.getmd5(checkingImage):
                return 'Success', 'NONE'
            else:
                raise ReturnError('Different images')
        except ReturnError as re:
            exit_code = re.message
            priority = re.priority
            return exit_code, priority

    def compareVersion(self, passed, comparing_folder):
        '''
        Сравнение картинок из успешных тестов генерации с картинками папки comparing_folder
        @param list passed : набор объектов класса PassedTest
        @param string comparing_folder : папка, откуда брать папки с картинками, c которыми сравнивать
        @return passed.tests : упешные тесты
        @return suspected.tests : тесты, подозреваемые на налиличие ошибок
        @return failed.tests : провалившиеся тесты
        '''
        failed = FailedTests().tests
        suspected = FailedTests().tests
        for test in passed:
            exit_code, priority = self.diff(test.case_name.replace('.html', '') + '.png', test.image.replace(str(test.case_name.replace('.html', '') + '.png'), ''), comparing_folder)
            if exit_code != 'Success':          
                passed.remove(test)
                if exit_code == 'Empty image':
                    failed.append(FailedTest(test.config, test.case_name, exit_code, priority, '', test.group, 'Empty checking', test.image))
                else:    
                    suspected.append(FailedTest(test.config, test.case_name, exit_code, priority, '', test.group, 'Comparing version', test.image))
        return passed, suspected, failed

    def compareSelected(self, passed, suspected, comparing_folder):
        '''
        Сравнение картинок из успешных тестов генерации с картинками папки comparing_folder
        @param list passed : набор объектов класса PassedTest
        @param list suspected : набор объектов класса FailedTest, подозреваемых в том, что они не прощли тест
        @param string comparing_folder : папка, откуда брать папки с картинками, c которыми сравнивать
        @return passed.tests : упешные тесты
        @return failed.tests : провалившиеся тесты
        '''
        failed = FailedTests().tests
        if len(suspected) > 0:
            deletion = []
            for test in suspected:
                exit_code, priority = self.diff(test.case_name.replace('.html', '') + '.png', test.image.replace(str(test.case_name.replace('.html', '') + '.png'), ''), comparing_folder)
                if exit_code == 'Success':
                    passed.append(PassedTest(test.config, test.case_name, test.group, test.image))
                    deletion.append(test)
            if len(deletion)>0:
                for i in deletion:
                    suspected.remove(i)
        failed += suspected
        for test in passed:
            exit_code, priority = self.diff(test.case_name.replace('.html', '') + '.png', test.image.replace(str(test.case_name.replace('.html', '') + '.png'), ''), comparing_folder)
            if exit_code != 'Success':
                passed.remove(test)
                failed.append(FailedTest(test.config, test.case_name, exit_code, "LOW", '', test.group, 'Comparing selected', test.image))
        return passed, failed

    def compareResized(self, passed, comparing_folder, step):
        '''
        Сравнение картинок из успешных тестов генерации resized с картинками папки out
        @param string comparing_folder : папка, откуда брать картинку, c которой сравнивать
        @param list passed : набор объектов класса PassedTest 
        @param string step : описание текущего действия
        @return passed.tests : упешные тесты
        @return failed.tests : провалившиеся тесты
        '''
        failed = FailedTests()
        for test in passed:
            if test.group != 'tests':
                exit_code, priority = self.diff(test.case_name.replace('.html', '.png'), test.image.replace(str(test.case_name.replace('.html', '.png')), ''), comparing_folder)
                if exit_code != 'Success':          
                    passed.remove(test)
                    failed.add(FailedTest(test.config, test.case_name, exit_code, priority, '', test.group, step, test.image))

        return passed, failed.tests 
