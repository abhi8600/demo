import os
import re

classes = 0
functions = 0

f = open('../../out/debug/AnyChartHTML5.js')
content = f.read()
f.close()

words = {}

W = re.split('\W', content)

for w in W:
    if words.has_key(w):
        words[w] = words.get(w) + 1
    else:
        words[w] = 1

for (w, count) in  sorted(words.items(), key=lambda x: x[1], reverse = True):
    if len(w) > 2 and count > 1:
        print "%s - %s" % (w, count)
