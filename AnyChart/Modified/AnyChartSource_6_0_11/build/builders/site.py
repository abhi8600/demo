#!/usr/bin/python
import build_utils
import subprocess
import shutil
import os

BASE_DIR = build_utils.get_root()
OUT_DIR = os.path.join(BASE_DIR, 'out')
INSTALL_SRC = os.path.join(BASE_DIR, 'install','src')
PATH_SITE = os.path.join(BASE_DIR, '..','site')

def __copyFile(src, dst):
    if os.path.exists(src):
       print "# copying " + src + ' to '+dst
       shutil.copy(src,dst)
    else:
         print "File "+src+" doesn't exists"
         raise BaseException()

def svn(command, src = ""):
    savedPath = os.getcwd()
    if command=="add":
        os.chdir(SITE_DEPLOY_DOWNLOAD)
        command = "status | grep '^\?'  | awk '{print $2}' | xargs svn add"
    else:
        os.chdir(PATH_SITE)
        command = command +" --username chidori --password fad4vimi"
        print "svn "+command
        if command=="up":
            print subprocess.call("rm -rf ./*")
            print "svn checkout"
    print subprocess.call("svn "+command, shell=True)
    os.chdir(savedPath)

def __copyDownload(src,dst):
    if not os.path.exists(os.path.join(SITE_DEPLOY_DOWNLOAD, dst)):
        if not os.path.exists(os.path.join(SITE_DEPLOY_DOWNLOAD, version)):
            os.mkdir(os.path.join(SITE_DEPLOY_DOWNLOAD, version))
            __copyFile(os.path.join(SITE_DEPLOY_DOWNLOAD, '6.0.1', '.htaccess'),os.path.join(SITE_DEPLOY_DOWNLOAD, version, '.htaccess'))
        os.mkdir(os.path.join(SITE_DEPLOY_DOWNLOAD, dst))

    __copyFile(os.path.join(OUT_DIR, src, 'AnyChart.swf'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChart.swf'))
    __copyFile(os.path.join(OUT_DIR, src, 'AnyChart.js'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChart.js'))
    __copyFile(os.path.join(OUT_DIR, src, 'AnyChartHTML5.js'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChartHTML5.js'))
    __copyFile(os.path.join(OUT_DIR, src, 'AnyChart.exe'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChart.exe'))
    __copyFile(os.path.join(OUT_DIR, src, 'AnyChart_plain.zip'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChart_plain.zip'))
    __copyFile(os.path.join(OUT_DIR, src, 'AnyChartFlexComponent.swc'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChartFlexComponent.swc'))
    #__copyFile(os.path.join(OUT_DIR, src, 'AnyChartGaugeFlexComponent.swc'), os.path.join(SITE_DEPLOY_DOWNLOAD, dst, 'AnyChartGaugeFlexComponent.swc'))

def main(path):
    if path:
        global PATH_SITE
        PATH_SITE = path

    #---- path require
    SITE_DEPLOY = os.path.join(PATH_SITE, 'deploy')
    SITE_ENGINE = os.path.join(PATH_SITE, 'engine')
    SITE_ENGINE_TRIAL = os.path.join(SITE_ENGINE, 'site')
    SITE_ENGINE_BUNDLE = os.path.join(SITE_ENGINE, 'debug')
    global SITE_DEPLOY_DOWNLOAD
    SITE_DEPLOY_DOWNLOAD = os.path.join(SITE_DEPLOY, 'download')
    # svn up
    print 'trying SVN UP site'
    svn('up')

    print "copying files to Site engine"
    #----- copying version files
    __copyFile(os.path.join(BASE_DIR, 'version.ini'), os.path.join(SITE_ENGINE, 'version.ini'))
    __copyFile(os.path.join(BASE_DIR, 'build', 'build.number'), os.path.join(SITE_ENGINE, 'build.number'))
    #----- copying root files
    # Preloader.swf
    __copyFile(os.path.join(OUT_DIR, 'Preloader.swf'), os.path.join(SITE_ENGINE, 'Preloader.swf'))
    # register's files
    __copyFile(os.path.join(INSTALL_SRC, 'register.bat'), os.path.join(SITE_ENGINE, 'register.bat'))
    __copyFile(os.path.join(INSTALL_SRC, 'register.sh'), os.path.join(SITE_ENGINE, 'register.sh'))
    # trial engine
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChart.swf'), os.path.join(SITE_ENGINE_TRIAL, 'AnyChart.swf'))
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChart.js'), os.path.join(SITE_ENGINE_TRIAL, 'AnyChart.js'))
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChartHTML5.js'), os.path.join(SITE_ENGINE_TRIAL, 'AnyChartHTML5.js'))
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChart.swf'), os.path.join(SITE_DEPLOY, 'swf', 'AnyChart.swf'))
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChart.js'), os.path.join(SITE_DEPLOY, 'js', 'AnyChart.js'))
    __copyFile(os.path.join(OUT_DIR, 'trial', 'AnyChartHTML5.js'), os.path.join(SITE_DEPLOY, 'js', 'AnyChartHTML5.js'))
    # non-trial engine
    __copyFile(os.path.join(OUT_DIR, 'bundle', 'AnyChart.swf'), os.path.join(SITE_ENGINE_BUNDLE, 'AnyChart.swf'))
    __copyFile(os.path.join(OUT_DIR, 'bundle', 'AnyChart.js'), os.path.join(SITE_ENGINE_BUNDLE, 'AnyChart.js'))
    __copyFile(os.path.join(OUT_DIR, 'bundle', 'AnyChartHTML5.js'), os.path.join(SITE_ENGINE_BUNDLE, 'AnyChartHTML5.js'))
    # download
    global version
    version=build_utils.get_version()
    version=version['global']+'.'+version['major']+'.'+version['minor']
    __copyDownload('trial','')
    __copyDownload('bundle',os.path.join(version,'bundle'))
    __copyDownload('charts',os.path.join(version,'chart'))
    __copyDownload('maps',os.path.join(version,'map'))

    print "copying complete!"

    # svn commit
    print 'trying SVN COMMIT site'
    svn("add")
    svn("commit -m \"binares up\"")

if __name__ == "__main__":
    main(0)
