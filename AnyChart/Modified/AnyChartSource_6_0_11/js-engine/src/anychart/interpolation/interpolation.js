/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 *  * File contains interfaces:
 * <ul>
 *  <li>@class {anychart.interpolation.IMissingInterpolator}</li>
 * <ul>
 *
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.interpolation.IMissingInterpolator}</li>
 * <ul>
 */
goog.provide('anychart.interpolation');
goog.require('anychart.utils');

//------------------------------------------------------------------------------
//
//                   IMissingInterpolator interface.
//
//------------------------------------------------------------------------------
/**
 * @interface
 */
anychart.interpolation.IMissingInterpolator = function () {
};
/**
 * Initialize missing interpolator with passed field name.
 * @param {Function} fieldGetter Function, witch returned point value
 * @param {Function} fieldSetter Function witch set point value
 */
anychart.interpolation.IMissingInterpolator.prototype.initialize = function (fieldGetter, fieldSetter) {
};
/**
 * Check point by passed index.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point data object.
 * @param {int} index Point data index.
 */
anychart.interpolation.IMissingInterpolator.prototype.checkDuringDeserialize = function (point, index) {
};
/**
 * Interpolate passed points.
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} points
 * @return {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.interpolation.IMissingInterpolator.prototype.interpolate = function (points) {
};
/**
 * Clear missing interpolator settings.
 */
anychart.interpolation.IMissingInterpolator.prototype.clear = function () {
};
//------------------------------------------------------------------------------
//
//                       SimpleMissingInterpolator class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @implements {anychart.interpolation.IMissingInterpolator}
 */
anychart.interpolation.SimpleMissingInterpolator = function () {
};
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.nonMissingPointsCount_ = null;
/**
 * @private
 * @type {Array.<int>}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.nonMissingPointsIndexes_ = null;
/**
 * @private
 * @type {Function}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.fieldGetter_ = null;
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.getFieldValue_ = function (point) {
    return this.fieldGetter_.call(point);
};
/**
 * @private
 * @type {Function}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.fieldSetter_ = null;
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 * @param {*} value
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.setFieldValue_ = function (point, value) {
    return this.fieldSetter_.call(point, value);
};
/**
 * @private
 * @type {Array.<Object>}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolatedPoints_ = null;
//------------------------------------------------------------------------------
//                  IMissingInterpolator implementation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.initialize = function (fieldGetter, fieldSetter) {
    this.clear();
    this.fieldGetter_ = fieldGetter;
    this.fieldSetter_ = fieldSetter;
};
/**
 * @inheritDoc
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.checkDuringDeserialize = function (point, index) {
    if (!this.isMissing_(point)) {
        this.nonMissingPointsCount_++;
        this.nonMissingPointsIndexes_.push(index);
    }
};
/**
 * @inheritDoc
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolate = function (points) {
    //all point missing
    if (points.length == this.nonMissingPointsIndexes_.length) return [];
    //interpolate result
    this.interpolatedPoints_ = [];
    //no values
    if (this.nonMissingPointsCount_ == 0)
        this.interpolateNoValues_(points);
    //single values
    else if (this.nonMissingPointsCount_ == 1)
        this.interpolateSingleValue_(points);
    //multiple values
    else this.interpolateMultipleValues_(points);

    return this.interpolatedPoints_;
};
/**
 * @inheritDoc
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.clear = function () {
    this.nonMissingPointsCount_ = 0;
    this.nonMissingPointsIndexes_ = [];
    this.interpolatedPoints_ = [];
};
//------------------------------------------------------------------------------
//                        Interpolations.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {Array.<Object>} points
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateNoValues_ = function (points) {
    var pointsCount = points.length;
    for (var i = 0; i < pointsCount; i++)
        this.setPointValue(points[i], 1);
};
/**
 * @private
 * @param {Array.<Object>} points
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateSingleValue_ = function (points) {

    var pointsCount = points.length;
    var value = anychart.utils.deserialization.getNum(this.getFieldValue_(points[this.nonMissingPointsIndexes_[0]]));

    for (var i = 0; i < pointsCount; i++)
        this.setPointValue(points[i], value);
};
/**
 * @private
 * @param {Array.<Object>} points
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateMultipleValues_ = function (points) {
    this.nonMissingPointsIndexes_.sort(function (a, b) {
        return a - b;
    });

    //interpolate all bounded blocks [value, missing, ..., value]
    var i;
    var nonMissingPointIndexesCount = this.nonMissingPointsIndexes_.length;
    var startNotMissingIndex = this.nonMissingPointsIndexes_[0];
    var endNonMissingIndex = this.nonMissingPointsIndexes_[nonMissingPointIndexesCount - 1];

    for (i = 0; i < (nonMissingPointIndexesCount - 1); i++) {
        var startIndex = this.nonMissingPointsIndexes_[i];
        var endIndex = this.nonMissingPointsIndexes_[i + 1];
        if ((endIndex - startIndex) > 1)
            this.interpolateBoundedBlock_(points, startIndex, endIndex);
    }

    if (startNotMissingIndex > 0)
        this.interpolateBoundedRight_(points, startNotMissingIndex);

    if (endNonMissingIndex < (points.length - 1))
        this.interpolateBoundedLeft_(points, endNonMissingIndex);
};
/**
 * @private
 * @param {Array.<Object>} points
 * @param {int} startIndex
 * @param {int} endIndex
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateBoundedBlock_ = function (points, startIndex, endIndex) {
    var des = anychart.utils.deserialization;
    var startValue = des.getNum(this.getFieldValue_(points[startIndex]));
    var dValue = des.getNum(this.getFieldValue_(points[endIndex])) - startValue;
    var dArgument = endIndex - startIndex;

    var d = dValue / dArgument;

    var start = startIndex + 1;
    for (var i = start; i < endIndex; i++)
        this.setPointValue(points[i], startValue + (i - startIndex) * d);
};
/**
 * @private
 * @param {Array.<Object>} points
 * @param {int} startValueIndex
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateBoundedRight_ = function (points, startValueIndex) {
    var des = anychart.utils.deserialization;
    var value1 = des.getNum(this.getFieldValue_(points[startValueIndex]));
    var value2 = des.getNum(this.getFieldValue_(points[startValueIndex + 1]));
    var d = (value2 - value1);
    for (var i = (startValueIndex - 1); i >= 0; i--)
        this.setPointValue(points[i], value1 - d * (startValueIndex - i));
};
/**
 * @private
 * @param {Array.<Object>} points
 * @param {int} endValueIndex
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.interpolateBoundedLeft_ = function (points, endValueIndex) {
    var des = anychart.utils.deserialization;
    var value1 = des.getNum(this.getFieldValue_(points[endValueIndex]));
    var value2 = des.getNum(this.getFieldValue_(points[endValueIndex - 1]));
    var d = (value2 - value1);
    for (var i = (endValueIndex + 1); i < points.length; i++)
        this.setPointValue(points[i], value2 - d * (i - endValueIndex + 1));
};
//------------------------------------------------------------------------------
//                             Utils.
//------------------------------------------------------------------------------
/**
 * Set point value by initialized field name.
 * @private
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 * @param {*} value
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.setPointValue = function (point, value) {
    this.setFieldValue_(point, value);
    this.interpolatedPoints_.push(point);
};
/**
 * Define, is point missing.
 * @private
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point data object.
 * @return {Boolean}
 */
anychart.interpolation.SimpleMissingInterpolator.prototype.isMissing_ = function (point) {
    return isNaN(anychart.utils.deserialization.getNum(this.getFieldValue_(point)));
};
