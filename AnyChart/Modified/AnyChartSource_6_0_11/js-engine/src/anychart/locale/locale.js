/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.locale.DateTimeLocale}</li>
 *  <li>@class {anychart.locale.NumberLocale}.</li>
 * <ul>
 */
goog.provide('anychart.locale');
goog.require('anychart.locale.dateTimeParser');
goog.require('anychart.utils');
//------------------------------------------------------------------------------
//
//                           DateTimeLocale class.
//
//------------------------------------------------------------------------------
/**
 * Represent date end time localization.
 * @constructor
 * @implements {anychart.formatting.IDateTimeInfoProvider}
 */
anychart.locale.DateTimeLocale = function() {
    this.monthNames_ = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November','December'];
    this.shortMonthNames_ = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.amString_ = 'AM';
    this.pmString_ = 'PM';
    this.shortAmString_ = 'A';
    this.shortPmString_ = 'P';
    this.weekDayNames_ = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    this.shortWeekDayNames_ = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    this.mask_ = '%yyyy/%M/%d %h:%m:%s';
    this.timeStampCache_ = {};
    this.parser_ = new anychart.locale.dateTimeParser.DateTimeParser();
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * Month names
 * @private
 * @type {Array.<string>}
 */
anychart.locale.DateTimeLocale.prototype.monthNames_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getMonthNames = function(month) {
    return this.monthNames_[month];
};

/**
 * Short month names,  3 symbols.
 * @private
 * @type {Array.<string>}
 */
anychart.locale.DateTimeLocale.prototype.shortMonthNames_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getShortMonthNames = function(month) {
    if (month != undefined) return this.shortMonthNames_[month];
    return this.shortMonthNames_;
};

/**
 * AM string
 * @private
 * @type {String}
 */
anychart.locale.DateTimeLocale.prototype.amString_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getAmString = function() {
    return this.amString_;
};

/**
 * Short AM string
 * @private
 * @type {String}
 */
anychart.locale.DateTimeLocale.prototype.shortAmString_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getShortAmString = function() {
    return this.shortAmString_;
};

/**
 * PM string
 * @private
 * @type {String}
 */
anychart.locale.DateTimeLocale.prototype.pmString_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getPmString = function() {
    return this.pmString_;
};

/**
 * Short PM string
 * @private
 * @type {String}
 */
anychart.locale.DateTimeLocale.prototype.shortPmString_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getShortPmString = function() {
    return this.shortPmString_;
};

/**
 * Week day names.
 * @private
 * @type {Array.<string>}
 */
anychart.locale.DateTimeLocale.prototype.weekDayNames_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getWeekDayNames = function(weekDay) {
    return this.weekDayNames_[weekDay];
};

/**
 * Short week day names, 3 symbols.
 * @private
 * @type {Array.<string>}
 */
anychart.locale.DateTimeLocale.prototype.shortWeekDayNames_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getShortWeekDayNames =
    function(weekDay) {
        return this.shortWeekDayNames_[weekDay];
    };

/**
 * Date/time format mask.
 * @private
 * @type {string}
 */
anychart.locale.DateTimeLocale.prototype.mask_ = null;

/** @inheritDoc */
anychart.locale.DateTimeLocale.prototype.getMask = function() {
    return this.mask_;
};
//------------------------------------------------------------------------------
//                        Parser.
//------------------------------------------------------------------------------
/**
 * DataTimeParser instance.
 * @private
 * @type {anychart.locale.dateTimeParser.DateTimeParser}
 */
anychart.locale.DateTimeLocale.prototype.parser_ = null;

/**
 * Deserialize date/time locale settings.
 * Expects the following JSON structure:
 * <p>
 *  Sample:
 *   <code>
 *      var locale = new anychart.locale.DateTimeLocale();
 *      locale.deserialize(data);
 *   </code>
 * </p>
 * <p>
 *
 * Expects the following JSON structure:
 * <code>
 *  {
 *      months : Object // Object with month names and month short names,
 *                      // described bellow;
 *      time : Object // Object with times label info, described bellow;
 *      week_days : Object // Object with week days names, described bellow;
 *      format : string // Date/time display format.
 *  }
 * </code>
 *
 * months:
 * <code>
 *  {
 *      names : string // Sting with month names separated by commas;
 *      short_names : string // String with short months names separated
 *                           // by commas.
 *  }
 * </code>
 *
 * time:
 * <code>
 *  {
 *      am_string : string // String for AM;
 *      short_am_string : string // Short AM string;
 *      pm_string : string // String for PM;
 *      short_pm_string : string // Short PM string.
 *  }
 * </code>
 *
 * week_days:
 * <code>
 *  {
 *      names : string // Sting with week days names separated by commas.
 *      short_names : string // String with short week days names separate
 *                           // commas.
 *  }
 * </code>
 * </p>
 * @param {Object} data JSON object with Date/time settings.
 */
anychart.locale.DateTimeLocale.prototype.deserialize = function(data) {
    //aliases
    var deserialize = anychart.utils.deserialization;

    //deserialize months names
    if (deserialize.hasProp(data, 'months')) {
        var month = deserialize.getProp(data, 'months');

        if (deserialize.hasProp(month, 'names'))
            this.monthNames_ = this.getArrayFromString_(deserialize.getStringProp(month, 'names'));

        if (deserialize.hasProp(month, 'short_names'))
            this.shortMonthNames_ = this.getArrayFromString_(deserialize.getStringProp(month, 'short_names'));
    }

    //deserialize week days names
    if (deserialize.hasProp(data, 'week_days')) {
        var weekDays = deserialize.getProp(data, 'week_days');
        if (deserialize.hasProp(weekDays, 'names'))
            this.weekDayNames_ = this.getArrayFromString_(deserialize.getStringProp(weekDays, 'names'));

        if (deserialize.hasProp(weekDays, 'short_names'))
            this.shortWeekDayNames_ = this.getArrayFromString_(deserialize.getStringProp(weekDays, 'short_names'));
    }

    //deserialize time
    if (deserialize.hasProp(data, 'time')) {
        var time = deserialize.getProp(data, 'time');
        if (deserialize.hasProp(time, 'am_string'))
            this.amString_ = deserialize.getProp(time, 'am_string');
        if (deserialize.hasProp(time, 'short_am_string'))
            this.shortAmString_ = deserialize.getProp(time, 'short_am_string');
        if (deserialize.hasProp(time, 'pm_string'))
            this.pmString_ = deserialize.getProp(time, 'pm_string');
        if (deserialize.hasProp(time, 'short_pm_string'))
            this.shortPmString_ = deserialize.getProp(time, 'short_pm_string');
    }

    //deserialize format
    if (deserialize.hasProp(data, 'format'))
        this.mask_ = deserialize.getStringProp(data, 'format');
};
//------------------------------------------------------------------------------
//                          Cache.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object.<String>}
 */
anychart.locale.DateTimeLocale.prototype.timeStampCache_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize date time locale.
 */
anychart.locale.DateTimeLocale.prototype.initialize = function() {
    this.parser_.initialize(this);
};

/**
 * Time stamp getter.
 * @param {String} date String with date.
 * @return {Number} Timestamp.
 */
anychart.locale.DateTimeLocale.prototype.getTimeStamp = function(date) {
    if (this.timeStampCache_[date]) return this.timeStampCache_[date];
    var timeStamp = this.parser_.getDateAsNumber(date);
    this.timeStampCache_[date] = timeStamp;
    return timeStamp;
};

/**
 * Split string by ',';
 * @private
 * @param {string} data Split string.
 * @return {Array.<string>} Array with strings.
 */
anychart.locale.DateTimeLocale.prototype.getArrayFromString_ = function(data) {
    return data.split(',');
};

//------------------------------------------------------------------------------
//
//                           NumberLocale class.
//
//------------------------------------------------------------------------------
/**
 * Represent number locale localization.
 * @constructor
 * <p>
 *  Sample:
 *  <code>
 *      var value = 10000;
 *      var numberLocale = new anychart.locale.NumberLocale();
 *      numberLocale.deserialize(data);
 *      value = numberLocale.getValue(value);
 *  </code>
 * </p>
 */
anychart.locale.NumberLocale = function() {
};

/**
 * Decimal separator.
 * @private
 * @type {String}
 */
anychart.locale.NumberLocale.prototype.decimalSeparator_ = '.';

/**
 * Thousand separator.
 * @private
 * @type {String}
 */
anychart.locale.NumberLocale.prototype.thousandsSeparator_ = '';

/**
 * Flag, use default settings.
 * @private
 * @type {boolean}
 */
anychart.locale.NumberLocale.prototype.isDefault_ = true;

/**
 * Deserialize number locale settings.
 * <p>
 *  Sample:
 *  <code>
 *   var numberLocale = new anychart.locale.NumberLocale();
 *   numberLocale.deserialize(data);
 *  </code>
 * </p>
 * Expects the following JSON structure:
 * <p>
 * <code>
 *  {
 *      decimal_separator : string // Decimal separator value;
 *      thousands_separator : string // Thousands separator value.
 *  }
 * </code>
 * </p>
 * @param {Object} data JSON object with number locale settings.
 */
anychart.locale.NumberLocale.prototype.deserialize = function(data) {
    //aliases
    var deserialization = anychart.utils.deserialization;

    if (deserialization.hasProp(data, 'decimal_separator'))
        this.decimalSeparator_ = deserialization.getProp(data,
            'decimal_separator').
            toString();

    if (deserialization.hasProp(data, 'thousands_separator'))
        this.thousandsSeparator_ = deserialization.getProp(data,
            'thousands_separator').
            toString();

    this.isDefault_ = this.decimalSeparator_ == '.' && this.thousandsSeparator_ == '';
};

/**
 * Reset settings to default.
 * <p>
 *  Sample:
 *  <code>
 *      var numberLocale = new anychart.locale.NumberLocale();
 *      numberLocale.deserialize(data);
 *      numberLocale.clear();
 *  </code>
 * </p>
 */
anychart.locale.NumberLocale.prototype.clear = function() {
    this.decimalSeparator_ = '.';
    this.thousandsSeparator_ = '';
    this.isDefault_ = true;
};

/**
 * Localize value.
 * @param {String} value Localized value.
 * @return {Number} Number.
 */
anychart.locale.NumberLocale.prototype.getValue = function(value) {
    return this.isDefault_ ? Number(value) : this.parseValue_(value);
};

/**
 * Parse value
 * @private.
 * @param {String} value Value to parse.
 * @return {Number} value.
 */
anychart.locale.NumberLocale.prototype.parseValue_ = function(value) {
    var res = String(value);
    res = res.split(this.thousandsSeparator_).join('');
    res = res.split(this.decimalSeparator_).join('.');
    return Number(res);
};
