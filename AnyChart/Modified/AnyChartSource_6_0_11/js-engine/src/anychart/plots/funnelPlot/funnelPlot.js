/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.funnelPlot.FunnelPlot}</li>.
 * <ul>
 */

//namespace
goog.provide('anychart.plots.funnelPlot');

goog.require('anychart.plots.percentBasePlot');
goog.require('anychart.plots.funnelPlot.templates');
goog.require('anychart.plots.funnelPlot.data');

//------------------------------------------------------------------------------
//
//                              FunnelPlot class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.PercentBasePlot}
 */
anychart.plots.funnelPlot.FunnelPlot = function () {
    anychart.plots.percentBasePlot.PercentBasePlot.call(this);
    this.defaultSeriesType_ = anychart.series.SeriesType.FUNNEL;
};
goog.inherits(anychart.plots.funnelPlot.FunnelPlot,
    anychart.plots.percentBasePlot.PercentBasePlot);


anychart.plots.funnelPlot.FunnelPlot.prototype.getTemplatesMerger = function () {
    return new anychart.plots.funnelPlot.templates.FunnelPlotTemplatesMerger();
};


/**
 * @inheritDoc
 * @return {Boolean}
 */
anychart.plots.funnelPlot.FunnelPlot.prototype.checkNoData = function (config) {
    var des = anychart.utils.deserialization;
    if (!des.hasProp(config, 'data')) return true;

    var data = des.getProp(config, 'data');
    return !des.hasProp(data, 'series');
};

anychart.plots.funnelPlot.FunnelPlot.prototype.createGlobalSeriesSettings = function (seriesType) {
    switch (seriesType) {
        case anychart.series.SeriesType.FUNNEL:
            return new anychart.plots.funnelPlot.data.FunnelGlobalSeriesSettings();
    }
    return null;
};
/**
 * Get series type.
 * @param {*} type Type.
 * @return {anychart.series.SeriesType} Type of the series.
 */
anychart.plots.funnelPlot.FunnelPlot.prototype.getSeriesType = function (type) {
    var des = anychart.utils.deserialization;
    switch (des.getEnumItem(type)) {
        case 'funnel':
            return anychart.series.SeriesType.FUNNEL;
            break;
        case 'pyramid':
            return anychart.series.SeriesType.PYRAMID;
            break;
        case 'cone':
            return anychart.series.SeriesType.CONE;
            break;
        default:
            return this.defaultSeriesType_;
    }
};

/**@inheritDoc*/
anychart.plots.funnelPlot.FunnelPlot.prototype.deserializeSeriesArguments_ = function (series, seriesNode) {
};
/**@inheritDoc*/
anychart.plots.funnelPlot.FunnelPlot.prototype.onAfterDeserializeData = function (config) {
};
/**@inheritDoc*/
anychart.plots.funnelPlot.FunnelPlot.prototype.initializeTopPlotContainers_ = function () {
};
/**@inheritDoc*/
anychart.plots.funnelPlot.FunnelPlot.prototype.getBackgroundBounds = function () {
    return new anychart.utils.geom.Rectangle(0, 0, this.bounds_.width, this.bounds_.height)
};
//------------------------------------------------------------------------------
//
//                           FunnelMode enum
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.funnelPlot.FunnelMode = {
    SQUARE:0,
    CIRCULAR:1
};
