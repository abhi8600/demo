/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo}</li>.
 *  <li>@class {anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo}</li>.
 * <ul>
 */
goog.provide('anychart.plots.percentBasePlot.drawingInfo');
//------------------------------------------------------------------------------
//
//                          PercentBaseDrawingInfo class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo = function() {
    this.leftBottom_ = new anychart.utils.geom.Point();
    this.leftTop_ = new anychart.utils.geom.Point();
    this.rightBottom_ = new anychart.utils.geom.Point();
    this.rightTop_ = new anychart.utils.geom.Point();
    this.bounds_ = new anychart.utils.geom.Rectangle();
};
//------------------------------------------------------------------------------
//                                   Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.bounds_ = null;
/**
 * Left bottom point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.leftBottom_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getLeftBottom = function() {
    return this.leftBottom_;
};
/**
 * Left top point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.leftTop_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getLeftTop = function() {
    return this.leftTop_;
};
/**
 * Right bottom point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.rightBottom_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getRightBottom = function() {
    return this.rightBottom_;
};
/**
 * Right top point.
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.rightTop_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getRightTop = function() {
    return this.rightTop_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.topSize3D_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.bottomSize3D_ = null;
//------------------------------------------------------------------------------
//                            Getters, methods
//------------------------------------------------------------------------------
/**
 * Getter for bound.
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getBounds = function() {
    return this.bounds_;
};
/**
 * Getter for left bottom point.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getLeftBottom = function() {
    return this.leftBottom_;
};
/**
 * Getter for left top point.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getLeftTop = function() {
    return this.leftTop_;
};
/**
 * Getter for right bottom point.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getRightBottom = function() {
    return this.rightBottom_;
};
/**
 * Getter for right top point.
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.getRightTop = function() {
    return this.rightTop_;
};
/**
 * Sets bound for drawing.
 * @param {anychart.plots.percentBasePlot.data.PercentBasePoint}
        */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.setDrawingBound = function(point) {
    this.bounds_.x = 0;
    this.bounds_.y = 0;
    this.bounds_.height = point.series_.getHeight();
    this.bounds_.width = point.series_.getWidth();
};

/**
 * Sets points for drawing.
 * @param {anychart.plots.percentBasePlot.data.PercentBasePoint} point Point.
 * @param {Boolean} inverted Inverted flag.
 * @param {Number} w Width of the graphic.
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.setSectorPoint = function(point, inverted, w) {
    var h = point.series_.getHeight();
    var x = point.series_.getSeriesBounds().x;
    var y = point.series_.getSeriesBounds().y;

    this.leftTop_.x = inverted ? (x + w / 2 - point.bottomWidth_ / 2 * w) :
            (x + w / 2 - point.topWidth_ / 2 * w);
    this.rightTop_.x = inverted ? (x + w / 2 + point.bottomWidth_ / 2 * w) :
            (x + w / 2 + point.topWidth_ / 2 * w);
    this.leftBottom_.x = inverted ? (x + w / 2 - point.topWidth_ / 2 * w) :
            (x + w / 2 - point.bottomWidth_ / 2 * w);
    this.rightBottom_.x = inverted ? (x + w / 2 + point.topWidth_ / 2 * w) :
            (x + w / 2 + point.bottomWidth_ / 2 * w);
    this.leftTop_.y = this.rightTop_.y = inverted ? (y + h -
            point.bottomHeight_ * h) : (y + point.topHeight_ * h);
    this.leftBottom_.y = this.rightBottom_.y = inverted ? (y + h -
            point.topHeight_ * h) : (y + point.bottomHeight_ * h);
};

/**
 * Sets points for drawing if label is out of bound.
 * @param {Number} x Offset.
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.prototype.updateSectorPoint = function(x) {
    this.leftBottom_.x -= x;
    this.leftTop_.x -= x;
    this.rightBottom_.x -= x;
    this.rightTop_.x -= x;
};

/**
 * Sets anchor for drawing.
 * @param {int} anchor Type of anchor.
 * @param {anychart.utils.geom.Point} pos Point positioning anchor.
 * @param {Boolean} inverted Inverted flag.
 */
anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.
        prototype.setAnchorPoint = function(anchor, pos, inverted) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.leftBottom_.y + this.leftTop_.y) / 2;
            break;
        }
        case anychart.layout.Anchor.CENTER_LEFT:
        {
            pos.x = (this.leftTop_.x + this.leftBottom_.x) / 2;
            pos.y = (this.leftTop_.y + this.leftBottom_.y) / 2;
            break;
        }
        case anychart.layout.Anchor.CENTER_RIGHT:
        {
            pos.x = (this.rightTop_.x + this.rightBottom_.x) / 2;
            pos.y = (this.rightTop_.y + this.rightBottom_.y) / 2;
            break;
        }
        case anychart.layout.Anchor.CENTER_BOTTOM:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.rightBottom_.y);
            break;
        }
        case anychart.layout.Anchor.CENTER_TOP:
        {
            pos.x = (this.rightTop_.x + this.leftTop_.x) / 2;
            pos.y = (this.rightTop_.y);
            break;
        }
        case anychart.layout.Anchor.LEFT_TOP:
        {
            pos.x = (this.leftTop_.x);
            pos.y = (this.leftTop_.y);
            break;
        }
        case anychart.layout.Anchor.RIGHT_TOP:
        {
            pos.x = (this.rightTop_.x);
            pos.y = (this.rightTop_.y);
            break;
        }
        case anychart.layout.Anchor.LEFT_BOTTOM:
        {
            pos.x = (this.leftBottom_.x);
            pos.y = (this.leftBottom_.y);
            break;
        }
        case anychart.layout.Anchor.RIGHT_BOTTOM:
        {
            pos.x = (this.rightBottom_.x);
            pos.y = (this.rightBottom_.y);
            break;
        }
    }
};

//------------------------------------------------------------------------------
//
//                     ConePyramidDIWDrawingInfo class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo}
 */
anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo = function() {
    anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo.call(this);
};
goog.inherits(anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo,
        anychart.plots.percentBasePlot.drawingInfo.PercentBaseDrawingInfo);

/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo.prototype.setSectorPoint = function(point, inverted, w) {
    var h = point.series_.getHeight();
    var x = point.series_.getSeriesBounds().x;
    var y = point.series_.getSeriesBounds().y;

    this.leftBottom_.x = x + w / 2 - point.bottomWidth_ * w / 2;
    this.rightBottom_.x = x + w / 2 + point.bottomWidth_ * w / 2;
    this.leftBottom_.y = this.rightBottom_.y = inverted ? (y + h -
            point.bottomHeight_ * h) : (y + point.bottomHeight_ * h);

    this.leftTop_.x = x + w / 2 - point.topWidth_ * w / 2;
    this.rightTop_.x = x + w / 2 + point.topWidth_ * w / 2;
    this.leftTop_.y = this.rightTop_.y = inverted ? (y + h -
            point.topHeight_ * h) : (y + point.topHeight_ * h);
};

/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.drawingInfo.ConePyramidDIWDrawingInfo.
        prototype.setAnchorPoint = function(anchor, pos, inverted) {
};
