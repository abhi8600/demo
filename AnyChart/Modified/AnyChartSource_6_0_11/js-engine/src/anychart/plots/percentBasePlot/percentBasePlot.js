/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.percentBasePlot.PercentBasePlot}.</li>
 * <ul>
 */
goog.provide('anychart.plots.percentBasePlot');
goog.require('anychart.plots.seriesPlot');
goog.require('anychart.plots.percentBasePlot.layout');
//------------------------------------------------------------------------------
//
//                              PercentBasePlot class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.percentBasePlot.PercentBasePlot = function() {
    anychart.plots.seriesPlot.SeriesPlot.call(this);
    this.is3D_ = false;
    this.layouter_ = new anychart.plots.percentBasePlot.layout.MultipleLayouter();
};
goog.inherits(anychart.plots.percentBasePlot.PercentBasePlot,
        anychart.plots.seriesPlot.SeriesPlot);

//------------------------------------------------------------------------------
//                           Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.is3D_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.is3D = function() {
    return this.is3D_;
};
/**
 * @private
 * @type {anychart.plots.percentBasePlot.layout.MultipleLayouter}
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.layouter_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.deserializeDataPlotSettings = function(config) {
    goog.base(this, 'deserializeDataPlotSettings', config);
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'enable_3d_mode'))
        this.is3D_ = false;//des.getBoolProp(config, 'enable_3d_mode');

    if (des.getProp(config, 'multiple_series_layout'))
        this.layouter_.deserialize(des.getProp(config, 'multiple_series_layout'));
};
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.initialize = function(bounds, tooltipContainer, opt_drawBackground) {
    this.baseBounds_ = bounds.clone();

    var container = goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);
    bounds = bounds.clone();
    bounds.x = 0;
    bounds.y = 0;

    if (this.background_) this.background_.applyInsideMargin(bounds);

    this.layouter_.initialize(this.series_);
    this.layouter_.setSeriesBounds(this.series_, bounds);
    return container;
};
//------------------------------------------------------------------------------
//                           Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.calculateResize = function(bounds) {
    this.baseBounds_ = bounds.clone();
    var innerBounds = bounds.clone();

    innerBounds.x = 0;
    innerBounds.y = 0;

    if (this.background_) this.background_.applyInsideMargin(innerBounds);

    this.layouter_.setSeriesBounds(this.series_, innerBounds);
    goog.base(this, 'calculateResize', bounds);
};
//------------------------------------------------------------------------------
//                      Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.setDefaultTokenValues = function() {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%DataPlotMaxYValuePointName', '');
    this.tokensHash_.add('%DataMaxYValuePointSeriesName', '');
    this.tokensHash_.add('%DataPlotMinYValuePointName', '');
    this.tokensHash_.add('%DataPlotMinYValuePointSeriesName', '');
    this.tokensHash_.add('%DataPlotMaxYSumSeriesName', '');
    this.tokensHash_.add('%DataPlotMinYSumSeriesName', '');

    this.tokensHash_.add('%DataPlotMaxYSumSeries', 0);
    this.tokensHash_.add('%DataPlotMinYSumSeries', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.getTokenType = function(token) {
    switch (token) {
        case '%DataPlotMaxYValuePointName' :
        case '%DataMaxYValuePointSeriesName' :
        case '%DataPlotMinYValuePointName' :
        case '%DataPlotMinYValuePointSeriesName' :
        case '%DataPlotMaxYSumSeriesName' :
        case '%DataPlotMinYSumSeriesName' :
            return anychart.formatting.TextFormatTokenType.TEXT;
        case '%DataPlotMaxYSumSeries' :
        case '%DataPlotMinYSumSeries' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};

//------------------------------------------------------------------------------
//                    Overrides
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.percentBasePlot.PercentBasePlot.prototype.onSeriesPointsDeserialize_ = function(series) {
    goog.base(this, 'onSeriesPointsDeserialize_', series);
    var pointsCount = series.getNumPoints();
    var allValuesAreZero = false;
    var count = 0;
    var point, value;
    for (var i = 0; i < pointsCount; i++) {
        point = series.getPointAt(i);
        value = point.getValue();
        point.setCalculationValue(value);
        if (value == 0) count++;
    }
    if (count == pointsCount) allValuesAreZero = true;
    if (allValuesAreZero) {
        for (i = 0; i < pointsCount; i++) {
            series.getPointAt(i).setCalculationValue(1);
        }
    }
};
