/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.seriesPlot.elements.LabelElement}</li>
 *  <li>@class {anychart.plots.seriesPlot.elements.MarkerElement}</li>
 *  <li>@class {anychart.plots.seriesPlot.elements.TooltipElement}</li>
 * <ul>
 */

goog.provide('anychart.plots.seriesPlot.elements');

goog.require('anychart.elements.label');
goog.require('anychart.elements.marker');
goog.require('anychart.elements.tooltip');

//-----------------------------------------------------------------------------------
//
//                          LabelElement class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent label element.
 * @public
 * @constructor
 * @extends {anychart.elements.label.BaseLabelElement}
 */
anychart.plots.seriesPlot.elements.LabelElement = function() {
    anychart.elements.label.BaseLabelElement.apply(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.elements.LabelElement, anychart.elements.label.BaseLabelElement);
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.seriesPlot.elements.LabelElement.prototype.isExtra_ = null;
/**
 * ND: Needs doc!
 * @param {Boolean} value
 */
anychart.plots.seriesPlot.elements.LabelElement.prototype.setIsExtra = function(value) {
    this.isExtra_ = value;
};

//-----------------------------------------------------------------------------------
//
//                          Utils
//
//-----------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.elements.LabelElement.prototype.canDeserializeForSeries = function(elementNode) {
    return goog.base(this, 'canDeserializeForSeries', elementNode) ||
            anychart.utils.deserialization.hasProp(elementNode, "format");
};

/**
 * Return copy of label element.
 * @public
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.plots.seriesPlot.elements.LabelElement}
 */
anychart.plots.seriesPlot.elements.LabelElement.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target = new anychart.plots.seriesPlot.elements.LabelElement();
    return goog.base(this, 'copy', opt_target);
};

//-----------------------------------------------------------------------------------
//
//                          MarkerElement class
//
//-----------------------------------------------------------------------------------

/**
 * Marker element
 * @public
 * @constructor
 * @extends {anychart.elements.marker.BaseMarkerElement}
 */
anychart.plots.seriesPlot.elements.MarkerElement = function() {
    anychart.elements.marker.BaseMarkerElement.apply(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.elements.MarkerElement, anychart.elements.marker.BaseMarkerElement);
//-----------------------------------------------------------------------------------
//
//                          Utils
//
//-----------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.elements.MarkerElement.prototype.canDeserializeForSeries = function(elementNode) {
    return goog.base(this, 'canDeserializeForSeries', elementNode) ||
            anychart.utils.deserialization.hasProp(elementNode, 'type');
};

/**
 * Return copy of marker element.
 * @public
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.plots.seriesPlot.elements.MarkerElement}
 */
anychart.plots.seriesPlot.elements.MarkerElement.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target = new anychart.plots.seriesPlot.elements.MarkerElement();
    return goog.base(this, 'copy', opt_target);
};

//-----------------------------------------------------------------------------------
//
//                          TooltipElement class
//
//-----------------------------------------------------------------------------------

/**
 * Class represent tooltip element.
 * @public
 * @constructor
 * @extends {anychart.elements.tooltip.BaseTooltipElement}
 */
anychart.plots.seriesPlot.elements.TooltipElement = function() {
    anychart.elements.tooltip.BaseTooltipElement.apply(this);
};
//inheritance
goog.inherits(anychart.plots.seriesPlot.elements.TooltipElement, anychart.elements.tooltip.BaseTooltipElement);
//-----------------------------------------------------------------------------------
//
//                          Utils
//
//-----------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.seriesPlot.elements.TooltipElement.prototype.canDeserializeForSeries = function(elementNode) {
    return goog.base(this, 'canDeserializeForSeries', elementNode) || anychart.utils.deserialization.hasProp(elementNode, 'format');

};

/**
 * Return copy of tooltip element.
 * @public
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.plots.seriesPlot.elements.MarkerElement}
 */
anychart.plots.seriesPlot.elements.TooltipElement.prototype.copy = function(opt_target) {
    if (!opt_target) opt_target = new anychart.plots.seriesPlot.elements.TooltipElement();
    return goog.base(this, 'copy', opt_target);

};

