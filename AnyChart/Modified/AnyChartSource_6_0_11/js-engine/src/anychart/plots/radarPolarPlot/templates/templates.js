/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.templates.RadarPolarPlotTemplatesMerger}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.templates');
goog.require('anychart.plots.radarPolarPlot.templates.DEFAULT_TEMPLATE');
goog.require('anychart.plots.radarPolarPlot.data.series.line.DEFAULT_TEMPLATE');
goog.require('anychart.plots.radarPolarPlot.series.marker.DEFAULT_TEMPLATE');
//------------------------------------------------------------------------------
//
//                   RadarPolarPlotTemplatesMerger class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.radarPolarPlot.templates.RadarPolarPlotTemplatesMerger = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.radarPolarPlot.templates.RadarPolarPlotTemplatesMerger,
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger);
//------------------------------------------------------------------------------
//                  Merge default template.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.templates.RadarPolarPlotTemplatesMerger.prototype.getPlotDefaultTemplate = function () {
    var res = this.mergeTemplates(
        goog.base(this, 'getPlotDefaultTemplate'),
        anychart.plots.radarPolarPlot.templates.DEFAULT_TEMPLATE
    );
    res = this.mergeTemplates(res, anychart.plots.radarPolarPlot.series.marker.DEFAULT_TEMPLATE);
    return this.mergeTemplates(res, anychart.plots.radarPolarPlot.data.series.line.DEFAULT_TEMPLATE);
};

