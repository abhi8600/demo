/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.axes.tickmarks');
goog.require('anychart.visual.stroke');
//------------------------------------------------------------------------------
//
//                          Tickmarks class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.stroke.Stroke}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks, anychart.visual.stroke.Stroke);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.isOutside_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.isOutside = function () {
    return this.isOutside_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.isInside_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.isInside = function () {
    return this.isInside_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.size_ = null;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.getSize = function () {
    return this.size_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.axes.tickmarks.Tickmarks.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    if (!this.enabled_) return;

    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'size')) this.size_ = des.getNumProp(data, 'size');
    if (des.hasProp(data, 'inside')) this.isInside_ = des.getBoolProp(data, 'inside');
    if (des.hasProp(data, 'outside')) this.isOutside_ = des.getBoolProp(data, 'outside');
};
