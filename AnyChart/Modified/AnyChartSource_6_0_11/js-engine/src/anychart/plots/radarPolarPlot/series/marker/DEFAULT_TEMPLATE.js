/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.series.marker.DEFAULT_TEMPLATE}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.series.marker.DEFAULT_TEMPLATE');
/**
 * Default template for radar polar marker series.
 * @type {Object}
 */
anychart.plots.radarPolarPlot.series.marker.DEFAULT_TEMPLATE = {
	'chart': {
		'styles': {
		},
		'data_plot_settings': {
			'marker_series': {
				'animation': {
					'style': 'defaultScaleXYCenter'
				},
				'label_settings': {
					'animation': {
						'style': 'defaultLabel'
					},
					'enabled': 'False'
				},
				'tooltip_settings': {
				}
			}
		}
	}
};