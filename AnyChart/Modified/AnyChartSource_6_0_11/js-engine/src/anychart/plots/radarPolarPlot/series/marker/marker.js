/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPolar.series.marker.MarkerPoint}</li>
 *  <li>@class {anychart.plots.radarPolarPolar.series.marker.MarkerSeries}</li>
 *  <li>@class {anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPolar.series.marker');
//------------------------------------------------------------------------------
//
//                          MarkerPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.data.RadarPolarPoint}
 */
anychart.plots.radarPolarPolar.series.marker.MarkerPoint = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPolar.series.marker.MarkerPoint,
    anychart.plots.radarPolarPlot.data.RadarPolarPoint
);
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.pixValuePoint_ = null;
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.initializeStyle = function() {
};
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'style')) {
        if (des.hasProp(data, 'marker'))
            des.setProp(data, 'marker', des.getProp(data, 'style'));
        else {
            var res = {};
            des.setProp(res, 'style', des.getProp(data, 'style'));
            des.setProp(res, '#name#', 'marker');
            des.setProp(data, 'marker', res);
        }
    }

    if (this.marker_.canDeserializeForPoint(des.getProp(data, 'marker'))) {
        this.marker_ = this.marker_.copy();
        this.marker_.deserializePointSettings(des.getProp(data, 'marker'), stylesList);
        this.style_ = this.marker_.getStyle();
    }
    this.marker_.setEnabled(true);
};
//------------------------------------------------------------------------------
//                  Bounds.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.calcPixValue = function () {
    this.pixValuePoint_ = new anychart.utils.geom.Point();
    this.getPlot().transform(this.x_, this.y_, this.pixValuePoint_);
};
//------------------------------------------------------------------------------
//                           Drawing.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.update = function () {
    this.calcPixValue();
    this.updateElements();
};
//------------------------------------------------------------------------------
//                           Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.resize = function () {
    this.calcPixValue();
    this.resizeElements();
};
//------------------------------------------------------------------------------
//                         Element position
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerPoint.prototype.setAnchorPoint = function (position, anchor, bounds, padding) {
    position.x = this.pixValuePoint_.x;
    position.y = this.pixValuePoint_.y;
};
//------------------------------------------------------------------------------
//
//                          MarkerSeries class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.radarPolarPlot.data.RadarPolarSeries}
 */
anychart.plots.radarPolarPolar.series.marker.MarkerSeries = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPolar.series.marker.MarkerSeries,
    anychart.plots.radarPolarPlot.data.RadarPolarSeries
);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerSeries.prototype.createPoint = function () {
    return new anychart.plots.radarPolarPolar.series.marker.MarkerPoint();
};
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerSeries.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'style')) {
        if (des.hasProp(data, 'marker'))
            des.setProp(data, 'marker', des.getProp(data, 'style'));
        else {
            var res = {};
            des.setProp(res, 'style', des.getProp(data, 'style'));
            des.setProp(res, '#name#', 'marker');
            des.setProp(data, 'marker', res);
        }
    }

    if (this.marker_.canDeserializeForSeries(des.getProp(data, 'marker'))) {
        this.marker_ = this.marker_.copy();
        this.marker_.deserializeSeriesSettings(des.getProp(data, 'marker'), stylesList);
        this.style_ = this.marker_.getStyle();
    }
    this.marker_.setEnabled(true);
};

//------------------------------------------------------------------------------
//
//                     MarkerGlobalSeriesSettings class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings}
 */
anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings,
    anychart.plots.seriesPlot.data.BaseSingleValueGlobalSeriesSettings
);
//------------------------------------------------------------------------------
//                  Settings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'marker_series';
};
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings.prototype.createSeries = function () {
    return new anychart.plots.radarPolarPolar.series.marker.MarkerSeries();
};
/**@inheritDoc*/
anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return null;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPolar.series.marker.MarkerGlobalSeriesSettings.prototype.deserializeMarkerElement_ = function (data, stylesList) {
    if (!this.marker_)
        this.marker_ = this.createMarkerElement();
    if (data)
        this.marker_.deserializeGlobalSettings(data, stylesList);
    else
        this.marker_.setStyle(new anychart.styles.Style(this.marker_.getStyleStateClass()));

    this.marker_.setEnabled(true);
    this.style_ = this.marker_.getStyle();
};


