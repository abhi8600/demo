/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.utils.PolarPoint}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.utils');

//------------------------------------------------------------------------------
//
//                          PolarPoint class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.utils.geom.Point}
    */
anychart.plots.radarPolarPlot.utils.PolarPoint = function (center) {
    this.center_ = center;
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.center_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.r_ = null;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.getRadius = function () {
    return this.r_;
};
/**
 * @param {Number} value
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.setRadius = function (value) {
    this.r_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.phi_ = null;
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.getPhi = function () {
    return this.phi_;
};
/**
 * @param {Number} value
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.setPhi = function (value) {
    this.phi_ = value;
};
//------------------------------------------------------------------------------
//                  Coords.
//------------------------------------------------------------------------------
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.getX = function () {
    return this.r_ * Math.cos(this.phi_ * Math.PI / 180) + this.center_.x;
};
/**
 * @return {Number}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.getY = function () {
    return this.r_ * Math.sin(this.phi_ * Math.PI / 180) + this.center_.y;
};
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.radarPolarPlot.utils.PolarPoint.prototype.getCenter = function () {
    return this.center_;
};
//------------------------------------------------------------------------------
//
//                          MathUtils class.
//
//------------------------------------------------------------------------------
/**
 * @type {Object}
 */
anychart.plots.radarPolarPlot.utils.MathUtils = {};
/**
 * //dirty hack for this bug: http://habrahabr.ru/blogs/Flash_Platform/131301/
 * Для некоторых хипстерских браузеров это так актуально.
 * @param {Number} a
 * @param {Number} b
 */
anychart.plots.radarPolarPlot.utils.MathUtils.divide = function (a, b) {
    if (b == 0) {
        if (a == 0) return NaN;
        return a > 0 ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
    }
    return a / b;
};

