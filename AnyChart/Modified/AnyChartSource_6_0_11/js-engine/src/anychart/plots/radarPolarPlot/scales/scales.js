/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.scales.LinearScale}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.scales.LogScale}</li>
 *  <li>@class {anychart.plots.radarPolarPlot.scales.TextScale}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.scales');
goog.require('anychart.scales');
//------------------------------------------------------------------------------
//
//                          LinearScale class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.scales.LinearScale = function () {
    goog.base(this);
};
goog.inherits(anychart.plots.radarPolarPlot.scales.LinearScale,
    anychart.scales.BaseScale);
//------------------------------------------------------------------------------
//                  Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.scales.LinearScale.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    this.inverted_ = false;
};
//------------------------------------------------------------------------------
//                  Calculation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.scales.LinearScale.prototype.calculate = function () {
    if (this.isMinimumAuto_ || this.isMaximumAuto_ || this.isMajorIntervalAuto_ || this.isMinorIntervalAuto_) {

        //применение разброса значений и minimumOffset с maximumOffset
        goog.base(this, 'calculate');

        //различные проверки минимума и максимума
        if (this.isMinimumAuto_ || this.isMaximumAuto_) {

            if ((this.dataRangeMaximum_ - this.dataRangeMinimum_) == 0 && this.isMinimumAuto_ && this.isMaximumAuto_) {
                this.minimum_ = this.dataRangeMinimum_ - .5;
                this.maximum_ = this.dataRangeMaximum_ + .5;
            }

            //проверка range на ноль
            if ((this.maximum_ - this.minimum_) < Number.MIN_VALUE) {
                if (this.isMaximumAuto_)
                    this.maximum_ = this.maximum_ + .2 * (this.maximum_ == 0 ? 1 : Math.abs(this.maximum_));
                if (this.isMinimumAuto_)
                    this.minimum_ = this.minimum_ - .2 * (this.minimum_ == 0 ? 1 : Math.abs(this.minimum_));
            }

            //проверка минимума на ноль
            if (this.isMinimumAuto_ && this.minimum_ > 0 && this.minimum_ / (this.maximum_ - this.minimum_) < .25)
                this.minimum_ = 0;

            //проверка максимума на нол
            if (this.isMaximumAuto_ && this.maximum_ < 0 && Math.abs(this.maximum_ / (this.maximum_ - this.minimum_)) < .25)
                this.maximum_ = 0;
        }

        //подсчет мажорного и минорного интервала
        if (this.isMajorIntervalAuto_)
            this.majorInterval_ = this.getOptimalStepSize_(this.maximum_ - this.minimum_, 7, false);

        if (this.isMinorIntervalAuto_)
            this.minorInterval_ = this.getOptimalStepSize_(this.majorInterval_, 5, true);

        //применение мажорного интервала к минимуму и максимуму
        if (this.isMinimumAuto_)
            this.minimum_ -= this.safeMod_(this.minimum_, this.majorInterval_);

        if (this.isMaximumAuto_) {
            var d = this.safeMod_(this.maximum_, this.majorInterval_);
            if (d != 0)
                this.maximum_ += this.majorInterval_ - d;
        }
    }
    this.calculateBaseValue_();
    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.scales.LinearScale.prototype.calculateBaseValue_ = function () {
    if (!this.isBaseValueAuto_) return;
    this.baseValue_ = Math.ceil(Number(this.minimum_) / Number(this.majorInterval_) - 0.00000001) * Number(this.majorInterval_);
};
//------------------------------------------------------------------------------
//                  Transformations.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.radarPolarPlot.scales.LinearScale.prototype.localTransform = function (value, isZeroAtAxisZero) {
    var ratio = (value - this.minimum_) / ( this.maximum_ - this.minimum_);
    var rangeMin = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum_;
    var pixelDistance = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;
    return Math.ceil(rangeMin + pixelDistance);
};
//------------------------------------------------------------------------------
//
//                          LogScale class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.scales.LogScale = function () {
    goog.base(this);
    this.logBase_ = 10;
    this.minorLogVals_ = [
        0, 0.301029995663981, 0.477121254719662, 0.602059991327962,
        0.698970004336019, 0.778151250383644, 0.845098040014257,
        0.903089986991944, 0.954242509439325, 1
    ];
};
goog.inherits(
    anychart.plots.radarPolarPlot.scales.LogScale,
    anychart.scales.BaseScale
);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.transformedMin_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.transformedMax_ = NaN;
/**
 * @private
 * @type {Number}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.logBase_ = NaN;
/**
 * @private
 * @type {Array.<Number>}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.minorLogVals_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.scales.LogScale.prototype.deserialize = function (data) {

    goog.base(this, 'deserialize', data);
    if (!data) return;
    var des = anychart.utils.deserialization, logPropName;

    if (des.hasProp(data, 'logarithmic_base'))
        logPropName = 'logarithmic_base';
    else if (des.hasProp(data, 'log_base'))
        logPropName = 'log_base';

    if (logPropName)
        if (des.getEnumItem(data, logPropName) == "e")
            this.logBase_ = Math.E;
        else
            this.logBase_ = des.getNumProp(data, logPropName);
};
//------------------------------------------------------------------------------
//                  Calculation.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.scales.LogScale.prototype.calculate = function () {
    if (isNaN(this.dataRangeMinimum_) && isNaN(this.dataRangeMaximum_) && this.isMinimumAuto_ && this.isMaximumAuto_) {

        this.isMinimumAuto_ = false;
        this.minimum_ = 1;

        this.isMaximumAuto_ = false;
        this.maximum_ = 10;
    }

    goog.base(this, 'calculate');
    if (this.isMajorIntervalAuto_)
        this.majorInterval_ = 1;

    if (this.minimum_ <= 0 && this.maximum_ <= 0) {
        this.minimum_ = 1;
        this.maximum_ = this.logBase_;
    } else if (this.minimum_ <= 0) {
        this.minimum_ = this.maximum_ / this.logBase_;
    } else if (this.maximum_ <= 0.0) {
        this.maximum_ = this.minimum_ * this.logBase_;
    }

    if (this.maximum_ - this.minimum_ < 1.0e-20) {
        if (this.isMaximumAuto_)
            this.maximum_ *= 2;
        if (this.isMinimumAuto_)
            this.minimum_ /= 2;
    }

    if (this.isMinimumAuto_)
        this.minimum_ = Math.pow(this.logBase_, Math.floor(Math.log(this.minimum_) / Math.log(this.logBase_)));
    if (this.isMaximumAuto_)
        this.maximum_ = Math.pow(this.logBase_, Math.ceil(Math.log(this.maximum_) / Math.log(this.logBase_)));

    this.transformedMin_ = Math.round(this.safeLog_(this.minimum_) * 1000) / 1000;
    this.transformedMax_ = Math.round(this.safeLog_(this.maximum_) * 1000) / 1000;

    this.calculateBaseValue_();
    this.calculateMinorStartInterval_();
    this.calculateIntervalCount_();
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.scales.LogScale.prototype.calculateMinorStartInterval_ = function () {
    this.minorStartInterval_ = -9;
};
/**@inheritDoc*/
anychart.plots.radarPolarPlot.scales.LogScale.prototype.calculateIntervalCount_ = function () {
    this.majorIntervalCount_ = parseInt(Math.floor(this.safeLog_(this.maximum_) + 1.0e-12)) - parseInt(this.baseValue_);

    var lastValue = this.getMajorIntervalValue(this.majorIntervalCount_);

    if (!this.linearizedContains(lastValue))
        this.majorIntervalCount_--;

    if (this.majorIntervalCount_ < 1)
        this.majorIntervalCount_ = 1;

    this.minorIntervalsCount_ = 9;
};
anychart.plots.radarPolarPlot.scales.LogScale.prototype.calculateBaseValue_ = function () {
    if (!this.isBaseValueAuto_) return;
    this.baseValue_ = Math.ceil(this.safeLog_(this.minimum_) - 0.00000001);
};
anychart.plots.radarPolarPlot.scales.LogScale.prototype.safeLog_ = function (value) {
    return ( value > 1.0e-20) ? Math.log(value) / Math.log(this.logBase_) : 0;
};
//------------------------------------------------------------------------------
//                  Transformations.
//------------------------------------------------------------------------------
anychart.plots.radarPolarPlot.scales.LogScale.prototype.transformValueToPixel = function (value) {
    var ratio = (this.safeLog_(value) - this.transformedMin_) / (this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;

    if (this.inverted_)
        return this.pixelRangeMaximum_ - pixelOffset;
    return this.pixelRangeMinimum_ + pixelOffset;
};
anychart.plots.radarPolarPlot.scales.LogScale.prototype.localTransform = function (value, isZeroAtAxisZero) {
    var ratio = (value - this.transformedMin_) / ( this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;

    var pixMax = isZeroAtAxisZero ? (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) : this.pixelRangeMaximum_;
    var pixMin = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum_;

    return (this.inverted_) ? (pixMax - pixelOffset) : (pixMin + pixelOffset);
};
anychart.plots.radarPolarPlot.scales.LogScale.prototype.getMajorIntervalValue = function (index) {
    return this.baseValue_ + index;
};

anychart.plots.radarPolarPlot.scales.LogScale.prototype.getMinorIntervalValue = function (baseValue, index) {
    console.log(baseValue + Math.floor(Number(index) / 9.0) + this.minorLogVals_[( index + 9 ) % 9])
    return baseValue + Math.floor(Number(index) / 9.0) + this.minorLogVals_[( index + 9 ) % 9];
};
//------------------------------------------------------------------------------
//                         METHODS
//------------------------------------------------------------------------------
/**
 * Safe log.
 * @private
 * @param {Number} value Value.
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.safeLog_ = function (value) {
    return (value > 1.0e-20) ? Math.log(value) / Math.log(this.logBase_) : 0;
};

/**
 * Indicates, contains passed value in linearized scale range.
 * @public
 * @override
 * @param {Number} value Target value
 * @return {boolean}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.linearizedContains = function (value) {
    return value >= this.transformedMin_ && value <= this.transformedMax_;
};

/**
 * Delinearize and return passed value.
 * @public
 * @param {Number} value Target value.
 * @return {Number}
 */
anychart.plots.radarPolarPlot.scales.LogScale.prototype.delinearize = function (value) {
    return Math.pow(this.logBase_, value);
};
//------------------------------------------------------------------------------
//
//                  TextScale class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.scales.BaseScale}
 */
anychart.plots.radarPolarPlot.scales.TextScale = function () {
    goog.base(this);
};
goog.inherits(
    anychart.plots.radarPolarPlot.scales.TextScale,
    anychart.scales.BaseScale
);
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.scales.TextScale.prototype.deserializeValue = function (value) {
    return  String(value);
};
//------------------------------------------------------------------------------
//                  Calculation.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.radarPolarPlot.scales.TextScale.prototype.calculate = function () {
    this.minimum_ = this.dataRangeMinimum_;
    this.maximum_ = this.dataRangeMaximum_ + 1;
    this.minorInterval_ = 1;
    this.baseValue_ = this.minimum_;

    if (this.isMajorIntervalAuto_)
        this.majorInterval_ = 1;

    this.majorInterval_ = parseInt(this.majorInterval_);
    if (this.majorInterval_ <= 0)
        this.majorInterval_ = 1;

    if (this.isMinorIntervalAuto())
        this.minorInterval_ = this.majorInterval_;


    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};


