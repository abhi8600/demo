/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.radarPolarPlot.templates.DEFAULT_TEMPLATE}</li>
 * <ul>
 */
goog.provide('anychart.plots.radarPolarPlot.templates.DEFAULT_TEMPLATE');

/**
 * Default settings for Radar/Polar plot.
 * @type {Object}
 */
anychart.plots.radarPolarPlot.templates.DEFAULT_TEMPLATE = {
	'chart': {
		'chart_settings': {
			'data_plot_background': {
				'enabled': 'false'
			},
			'axes': {
				'x_axis': {
					'line': {
						'enabled': 'true',
						'thickness': '1',
						'color': '#C0C0C0'
					},
					'major_grid': {
						'line': {
							'enabled': 'true',
							'color': '#C0C0C0',
							'thickness': '1'
						},
						'interlaced_fills': {
							'even': {
								'fill': {
									'enabled': 'true',
									'color': '#F5F5F5',
									'opacity': '0.5'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							},
							'odd': {
								'fill': {
									'enabled': 'true',
									'color': '#FFFFFF',
									'opacity': '0.5'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							}
						},
						'enabled': 'true',
						'draw_last_line': 'true',
						'interlaced': 'false'
					},
					'major_tickmark': {
						'enabled': 'true',
						'size': '4',
						'inside': 'false',
						'outside': 'true',
						'thickness': '1',
						'color': '#333333'
					},
					'minor_grid': [
						{
							'line': {
								'enabled': 'true',
								'thickness': '1',
								'color': '#333333'
							},
							'interlaced_fills': {
								'even': {
									'fill': {
										'enabled': 'true',
										'color': 'White'
									},
									'hatch_fill': {
										'enabled': 'false'
									}
								},
								'odd': {
									'fill': {
										'enabled': 'true',
										'color': 'White'
									},
									'hatch_fill': {
										'enabled': 'false'
									}
								}
							},
							'enabled': 'false',
							'draw_last_line': 'true',
							'interlaced': 'false'
						},
						{
							'enabled': 'false'
						}
					],
					'labels': {
						'format': {
							'value': '{%Value}'
						},
						'font': {
							'family': 'Tahoma',
							'size': '11',
							'bold': 'True'
						},
						'background': {
							'border': {
								'gradient': {
									'key': [
										{
											'position': '0',
											'color': 'rgb(221,221,221)'
										},
										{
											'position': '1',
											'color': 'rgb(208,208,208)'
										}
									],
									'angle': '90'
								},
								'enabled': 'True',
								'type': 'Gradient'
							},
							'fill': {
								'gradient': {
									'key': [
										{
											'position': '0',
											'color': 'rgb(255,255,255)'
										},
										{
											'position': '0.5',
											'color': 'rgb(243,243,243)'
										},
										{
											'position': '1',
											'color': 'rgb(255,255,255)'
										}
									],
									'angle': '90'
								},
								'type': 'Gradient'
							},
							'inside_margin': {
								'left': '2',
								'right': '2',
								'top': '0',
								'bottom': '0'
							},
							'effects': {
								'drop_shadow': {
									'enabled': 'true',
									'distance': '1',
									'opacity': '0.1'
								},
								'enabled': 'true'
							},
							'enabled': 'false'
						},
						'enabled': 'true',
						'padding': '0',
						'allow_overlap': 'true',
						'position': 'Outside',
						'show_first_label': 'true',
						'show_last_label': 'true',
						'relative_rotation': 'false'
					},
					'enabled': 'true'
				},
				'y_axis': {
					'line': {
						'enabled': 'true',
						'color': '#333333',
						'thickness': '1'
					},
					'major_grid': {
						'line': {
							'enabled': 'true',
							'thickness': '1',
							'color': '#DDDDDD'
						},
						'interlaced_fills': {
							'even': {
								'fill': {
									'enabled': 'true',
									'color': 'White'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							},
							'odd': {
								'fill': {
									'enabled': 'true',
									'color': 'Rgb(250,250,250)'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							}
						},
						'enabled': 'true',
						'draw_last_line': 'false',
						'draw_first_line': 'false',
						'interlaced': 'false'
					},
					'major_tickmark': {
						'enabled': 'true',
						'size': '4',
						'inside': 'true',
						'outside': 'false',
						'thickness': '1',
						'color': '#333333'
					},
					'minor_grid': {
						'line': {
							'enabled': 'true',
							'thickness': '1',
							'color': '#DDDDDD'
						},
						'interlaced_fills': {
							'even': {
								'fill': {
									'enabled': 'true',
									'color': 'White'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							},
							'odd': {
								'fill': {
									'enabled': 'true',
									'color': 'White'
								},
								'hatch_fill': {
									'enabled': 'false'
								}
							}
						},
						'enabled': 'false',
						'draw_last_line': 'true',
						'interlaced': 'false'
					},
					'minor_tickmark': {
						'enabled': 'false',
						'size': '2',
						'inside': 'true',
						'outside': 'false',
						'thickness': '1',
						'color': '#333333',
						'pixel_hinting': 'true'
					},
					'scale': {
					},
					'labels': {
						'format': {
							'value': '{%Value}{numDecimals:3,trailingZeros:false}'
						},
						'font': {
							'family': 'Tahoma',
							'size': '11',
							'bold': 'false'
						},
						'background': {
							'border': {
								'enabled': 'true',
								'type': 'Solid',
								'color': '#999999'
							},
							'fill': {
								'type': 'Solid',
								'color': '#FFFFFF'
							},
							'inside_margin': {
								'left': '2',
								'top': '0',
								'right': '2',
								'bottom': '0'
							},
							'enabled': 'false'
						},
						'enabled': 'true',
						'padding': '5',
						'allow_overlap': 'false',
						'position': 'Outside',
						'show_first_label': 'true',
						'show_last_label': 'true',
						'relative_rotation': 'false'
					},
					'enabled': 'true',
					'display_under_data': 'false'
				},
				'display_under_data': 'false'
			}
		},
		'data_plot_settings': {
			'radar': {
				'background': {
					'fill': {
						'enabled': 'true',
						'type': 'Solid',
						'color': '#FFFFFF',
						'opacity': '1'
					},
					'border': {
						'enabled': 'false'
					},
					'enabled': 'true'
				},
				'start_angle': '0'
			},
			'polar': {
				'background': {
					'fill': {
						'enabled': 'true',
						'type': 'Solid',
						'color': '#FFFFFF',
						'opacity': '1'
					},
					'border': {
						'enabled': 'false'
					},
					'enabled': 'true'
				}
			},
			'area_series': {
				'area_style': {
					'fill': {
						'opacity': '0.5'
					}
				},
				'animation': {
					'type': 'ScaleXYCenter'
				}
			},
			'line_series': {
				'animation': {
					'type': 'ScaleXYCenter'
				}
			}
		}
	}
};


