/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.templates.TreeMapTemplateMerger}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.templates');
goog.require('anychart.plots.seriesPlot.templates');
goog.require('anychart.plots.treeMapPlot.templates.DEFAULT_TEMPLATE');
//------------------------------------------------------------------------------
//
//                          TreeMapTemplateMerger class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.treeMapPlot.templates.TreeMapTemplateMerger = function() {
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.templates.TreeMapTemplateMerger,
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger);
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.templates.TreeMapTemplateMerger.prototype.getPlotDefaultTemplate = function() {
    return this.mergeTemplates(
        goog.base(this, 'getPlotDefaultTemplate'),
        anychart.plots.treeMapPlot.templates.DEFAULT_TEMPLATE);
};


