/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapStyleState}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapCroppedStyleState}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapHiddenStyleState}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapStyleShapes}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapStyle}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapCroppedStyle}</li>
 *  <li>@class {anychart.plots.treeMapPlot.styles.TreeMapHiddenStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.treeMapPlot.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                     TreeMapStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState = function (style, opt_stateType) {
    anychart.styles.background.BackgroundStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapStyleState,
    anychart.styles.background.BackgroundStyleState);
//------------------------------------------------------------------------------
//                            Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.background_ = null;
/**
 * @return {anychart.visual.background.RectBackground}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.getBackground = function () {
    return this.background_;
};
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.hasBackground = function () {
    return this.background_ && this.background_.isEnabled();
};
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.insideMargin_ = null;
/**
 * @return {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.getInsideMargin = function () {
    return this.insideMargin_;
};
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.outsideMargin_ = null;
/**
 * @return {anychart.layout.Margin}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.getOutsideMargin = function () {
    return this.outsideMargin_;
};
/**
 * @private
 * @type {anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.header_ = null;
/**
 * @return {anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.getHeader = function () {
    return this.header_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization;
    if (!des.hasProp(data, 'branch')) return data;

    var branch = des.getProp(data, 'branch');

    if (des.isEnabledProp(branch, 'background')) {
        this.background_ = new anychart.visual.background.RectBackground();
        this.background_.deserialize(des.getProp(branch, 'background'));
    }

    if (des.isEnabledProp(branch, 'inside_margin')) {
        this.insideMargin_ = new anychart.layout.Margin();
        this.insideMargin_.deserialize(des.getProp(branch, 'inside_margin'))
    }

    if (des.isEnabledProp(branch, 'outside_margin')) {
        this.outsideMargin_ = new anychart.layout.Margin();
        this.outsideMargin_.deserialize(des.getProp(branch, 'outside_margin'));
    }

    if (des.isEnabledProp(branch, 'header')) {
        this.header_ = this.createBranchTitle();
        this.header_.deserialize(des.getProp(branch, 'header'));
    }

    return data;
};
/**
 * @protected
 * @return {anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.createBranchTitle = function () {
    return new anychart.plots.treeMapPlot.elements.TreeMapBranchTitle();
};
//------------------------------------------------------------------------------
//                          Definers.
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleState.prototype.needUpdateHeader = function () {
    return this.header_ && this.header_.isEnabled();
};
//------------------------------------------------------------------------------
//
//                     TreeMapCroppedStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.treeMapPlot.styles.TreeMapCroppedStyleState = function (style, opt_stateType) {
    anychart.plots.treeMapPlot.styles.TreeMapStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapCroppedStyleState,
    anychart.plots.treeMapPlot.styles.TreeMapStyleState);
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapCroppedStyleState.prototype.createBranchTitle = function () {
    return new anychart.plots.treeMapPlot.elements.TreeMapCroppedBranchTitle();
};
//------------------------------------------------------------------------------
//
//                     TreeMapHiddenStyleState class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.treeMapPlot.styles.TreeMapHiddenStyleState = function (style, opt_stateType) {
    anychart.plots.treeMapPlot.styles.TreeMapStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapHiddenStyleState,
    anychart.plots.treeMapPlot.styles.TreeMapStyleState);
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapHiddenStyleState.prototype.createBranchTitle = function () {
    return new anychart.plots.treeMapPlot.elements.TreeMapHiddenBranchTitle();
};
//------------------------------------------------------------------------------
//
//                      TreeMapStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleShapes = function () {
    anychart.styles.background.BackgroundStyleShapes.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapStyleShapes,
    anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleShapes.prototype.createElement = function () {
    return this.point_.getSVGManager().createPath();
};
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapStyleShapes.prototype.updatePath = function (element) {
    element.setAttribute('d', this.point_.getSVGManager().pRectByBounds(this.point_.getBounds()));
};
//------------------------------------------------------------------------------
//
//                          TreeMapStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle = function () {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapStyle,
    anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          Style state.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.treeMapPlot.styles.TreeMapStyleState;
};
//------------------------------------------------------------------------------
//                            StyleShapes.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.createStyleShapes = function () {
    return new anychart.plots.treeMapPlot.styles.TreeMapStyleShapes();
};
//------------------------------------------------------------------------------
//                          Definers.
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.needCreateHeader = function () {
    return this.normal_.needUpdateHeader() ||
        this.pushed_.needUpdateHeader() ||
        this.hover_.needUpdateHeader() ||
        this.selectedNormal_.needUpdateHeader() ||
        this.selectedHover_.needUpdateHeader() ||
        this.missing_.needUpdateHeader();
};
/**
 * @return {anychart.plots.treeMapPlot.elements.ITreeMapBranchTitle}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.getEnabledHeader = function () {
    if (this.normal_.needUpdateHeader()) return this.normal_.getHeader();
    if (this.pushed_.needUpdateHeader()) return this.pushed_.getHeader();
    if (this.hover_.needUpdateHeader()) return this.hover_.getHeader();
    if (this.selectedNormal_.needUpdateHeader()) return this.selectedNormal_.getHeader();
    if (this.selectedHover_.needUpdateHeader()) return this.selectedHover_.getHeader();
    if (this.missing_.needUpdateHeader()) return this.missing_.getHeader();
    return null;
};
/**
 * @return {Boolean}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.needCreateBackground = function () {
    return this.normal_.hasBackground() ||
        this.pushed_.hasBackground() ||
        this.hover_.hasBackground() ||
        this.selectedNormal_.hasBackground() ||
        this.selectedHover_.hasBackground() ||
        this.missing_.hasBackground();
};
/**
 * @return {anychart.visual.background.RectBackground}
 */
anychart.plots.treeMapPlot.styles.TreeMapStyle.prototype.getEnabledBackground = function () {
    if (this.normal_.hasBackground()) return this.normal_.getBackground();
    if (this.pushed_.hasBackground()) return this.pushed_.getBackground();
    if (this.hover_.hasBackground()) return this.hover_.getBackground();
    if (this.selectedNormal_.hasBackground()) return this.selectedNormal_.getBackground();
    if (this.selectedHover_.hasBackground()) return this.selectedHover_.getBackground();
    if (this.missing_.hasBackground()) return this.missing_.getBackground();
    return null;
};
//------------------------------------------------------------------------------
//
//                          TreeCroppedMapStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.treeMapPlot.styles.TreeMapCroppedStyle = function () {
    anychart.plots.treeMapPlot.styles.TreeMapStyle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapCroppedStyle,
    anychart.plots.treeMapPlot.styles.TreeMapStyle);
/**@inheritDoc*/
anychart.plots.treeMapPlot.styles.TreeMapCroppedStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.treeMapPlot.styles.TreeMapCroppedStyleState;
};
//------------------------------------------------------------------------------
//
//                          TreeMapHiddenStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.treeMapPlot.styles.TreeMapHiddenStyle = function () {
    anychart.plots.treeMapPlot.styles.TreeMapStyle.call(this);
};
goog.inherits(anychart.plots.treeMapPlot.styles.TreeMapHiddenStyle,
    anychart.plots.treeMapPlot.styles.TreeMapStyle);
/**@inheritDoc*/
anychart.plots.treeMapPlot.styles.TreeMapHiddenStyle.prototype.getStyleStateClass = function () {
    return anychart.plots.treeMapPlot.styles.TreeMapHiddenStyleState;
};