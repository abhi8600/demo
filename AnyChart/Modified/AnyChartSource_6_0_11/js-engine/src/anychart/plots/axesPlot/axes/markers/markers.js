/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.AxisMarker}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.LineAxisMarker}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.RangeAxisMarker}</li>
 *  <li>@class {anychart.plots.axesPlot.axes.markers.AxisMarkersList}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.axes.markers');
goog.require('anychart.plots.axesPlot.axes.markers.styles');
//------------------------------------------------------------------------------
//
//                     AxisMarker class.
//
//------------------------------------------------------------------------------
/**
 * Base class for all Axis markers types.
 * @constructor
 * @param {anychart.plots.axesPlot.axes.Axis}
        */
anychart.plots.axesPlot.axes.markers.AxisMarker = function(axis) {
    this.axis_ = axis;
    this.displayUnderData_ = true;
    this.affectScaleRange_ = true;
    this.visible_ = true;
    this.initializeFormatting();
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.axis_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.axes.markers.AxisMarkersList}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.markersList_ = null;
/**
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarkersList} value
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.setMarkersList = function(value) {
    this.markersList_ = value;
};
/**
 * @private
 * @type {anychart.styles.Style}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.style_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.displayUnderData_ = null;
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.setDisplayUnderData = function(value) {
    this.displayUnderData_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.affectScaleRange_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.visible_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.sprite_ = null;
//------------------------------------------------------------------------------
//                       Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize axis marker settings.
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.deserialize = function(data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'visible'))
        this.visible_ = des.getBoolProp(data, 'visible');

    if (des.hasProp(data, 'display_under_data'))
        this.displayUnderData_ = des.getBoolProp(data, 'display_under_data');

    if (des.hasProp(data, 'affects_scale_range'))
        this.affectScaleRange_ = des.getBoolProp(data, 'affects_scale_range');

    this.deserializeStyleFromNode(data, stylesList);
};
/**
 * Deserialize axis marker style from style node.
 * @protected
 * @param {Object} data
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.deserializeStyleFromNode = function(data, stylesList) {
    var styleData = stylesList.getStyleByMerge(
            this.getStyleNodeName(),
            data,
            anychart.utils.deserialization.getStringProp(data, 'style'));
    this.style_ = this.createStyle();
    this.style_.deserialize(styleData);
};
/**
 * @protected
 * @return {anychart.styles.Style}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.createStyle = function() {
    goog.abstractMethod();
};
/**
 * @protected
 * @return {String}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.getStyleNodeName = function() {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Scale value.
//------------------------------------------------------------------------------
/**
 * @param {String} strValue
 * @return {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.getValue = function(strValue) {
    if (strValue.charAt(0) == "{") strValue = strValue.substr(1, strValue.length - 2);
    var value;
    if (this.axis_.isCategorized()) {
        value = Number(strValue);
        if (!isNaN(value)) return value - 1;
        if (this.axis_.hasCategory(strValue)) return this.axis_.getCategory(strValue).getIndex();
    }
    value = this.axis_.getScale().deserializeValue(strValue);
    if (!isNaN(value)) return value;
    return this.axis_.getTokenValue(strValue);
};
/**
 * @protected
 * @param {Number} value
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.checkScale = function(value) {
    var scale = this.axis_.getScale();
    if (isNaN(scale.getDataRangeMinimum()) || value < scale.getDataRangeMinimum())
        scale.setDataRangeMinimum(value);

    if (isNaN(scale.getDataRangeMaximum()) || value > scale.getDataRangeMaximum())
        scale.setDataRangeMaximum(value);
};
//------------------------------------------------------------------------------
//                        Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize axis marker visual.
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.initialize = function(svgManager) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    if (this.displayUnderData_)
        this.axis_.getPlot().getBeforeDataSprite().appendSprite(this.sprite_);
    else
        this.axis_.getPlot().getAfterDataSprite().appendSprite(this.sprite_);

    this.initializeLabel_(svgManager);
};
/**
 * Initialize axis marker label visual
 * @param {anychart.svg.SVGSprite} svgManager
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.initializeLabel_ = function(svgManager) {
    var label = this.getLabel();
    if (label) label.initialize(svgManager, this);
};
/**
 * @return {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.getLabel = function() {
    return this.style_.getNormal().getLabel();
};
//------------------------------------------------------------------------------
//                        Formatting.
//------------------------------------------------------------------------------
/**
 * Initialize AxisMarker formatting.
 * @protected
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.initializeFormatting = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
};
/**
 * @param {String} token
 * @return {String}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.getTokenValue = function(token) {
    if (this.tokensHash_.isDataPlotToken(token, this.axis_.getPlot()))
        return this.axis_.getPlot().getTokenValue(token);

    if (this.tokensHash_.isAxisToken(token))
        return this.axis_.getTokenValue(token);

    return this.tokensHash_.get(token);
};
/**
 *
 * @param {String} token
 * @return {int}
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.getTokenType = function(token) {
    if (this.tokensHash_.isDataPlotToken(token, this.axis_.getPlot()))
        return this.axis_.getPlot().getTokenType(token);

    if (this.tokensHash_.isAxisToken(token))
        return this.axis_.getTokenType(token);

    return anychart.formatting.TextFormatTokenType.UNKNOWN;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw axis marker.
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.draw = function(svgManager) {
    this.sprite_.clear();
};
/**
 * Draw axis marker label
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel} label
 * @param {Number} startValue
 * @param {Number} endValue
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.drawLabel = function(svgManager, label, startValue, endValue) {
    if (label.isOutside()) {
        this.setOutsideLabelPosition(label, startValue);
        label.draw(svgManager, this.markersList_.getSprite())
    } else {
        this.setInsideLabelPosition(label, startValue, endValue);
        label.draw(svgManager, this.sprite_);
    }

};
//------------------------------------------------------------------------------
//                        Label position.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel} label
 * @param {Number} value
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.setOutsideLabelPosition = function(label, value) {
    var pixPos = this.axis_.getScale().localTransform(value);
    var offset = 0;

    var positionType = anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition;
    switch (label.getPosition()) {
        case  positionType.AFTER_AXIS_LABELS:
            offset = this.axis_.getMarkersBeforeLabelsSpace();
            break;
        case  positionType.AXIS:
            offset = this.axis_.getMarkersAxisSpace();
            break;
        case  positionType.BEFORE_AXIS_LABELS:
            offset = this.axis_.getMarkersAfterLabelsSpace();
            break;
    }


    this.axis_.getViewPort().getMarkersViewPort().setOutsideMarkerLabelPosition(
            pixPos,
            label.getPadding(),
            offset,
            label.getBounds(),
            label.getPixPosition());
};
/**
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarkerLabel} label
 * @param {Number} startValue
 * @param {Number} endValue
 */
anychart.plots.axesPlot.axes.markers.AxisMarker.prototype.setInsideLabelPosition = function(label, startValue, endValue) {
    var startPixPos = this.axis_.getScale().localTransform(startValue);
    var endPixPos = this.axis_.getScale().localTransform(endValue);
    var align;

    var positionType = anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition;
    switch (label.getPosition()) {
        case positionType.NEAR:
            align = anychart.layout.AbstractAlign.NEAR;
            break;
        case positionType.CENTER:
            align = anychart.layout.AbstractAlign.CENTER;
            break;
        case positionType.FAR:
            align = anychart.layout.AbstractAlign.FAR;
            break;
    }

    this.axis_.getViewPort().getMarkersViewPort().setInsideMarkerLabelPosition(
            startPixPos,
            endPixPos,
            align,
            label.getPadding(),
            label.getBounds(),
            label.getPixPosition());
};
//------------------------------------------------------------------------------
//
//                     AxisMarkerLabelPosition class.
//
//------------------------------------------------------------------------------
/**
 * Possible axis marker label positions.
 * @enum {int}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition = {
    BEFORE_AXIS_LABELS : 0,
    AFTER_AXIS_LABELS : 1,
    AXIS : 2,
    NEAR : 3,
    CENTER : 4,
    FAR : 5
};
//------------------------------------------------------------------------------
//
//                          AxisMarkerLabel class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.markers.AxisMarker}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel = function() {
    anychart.plots.axesPlot.axes.markers.AxisMarker.call(this);
    this.textElement_ = new anychart.visual.text.TextElement();
    this.pixPosition_ = new anychart.utils.geom.Point();
    this.isOutside_ = true;
    this.padding_ = 5;
    this.position_ = anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition.BEFORE_AXIS_LABELS;
};
goog.inherits(anychart.plots.axesPlot.axes.markers.AxisMarkerLabel,
        anychart.plots.axesPlot.axes.markers.AxisMarker);
//------------------------------------------------------------------------------
//                           Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.position_ = null;
/**
 * @return {int}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.getPosition = function() {
    return this.position_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.getPadding = function() {
    return this.padding_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.pixPosition_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.getPixPosition = function() {
    return this.pixPosition_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.isOutside_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.isOutside = function() {
    return this.isOutside_;
};
/**
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.textElement_ = null;
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.textFormatter_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.getBounds = function() {
    return this.textElement_.getBounds();
};
//------------------------------------------------------------------------------
//                       Deserialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.deserialize = function(data) {
    this.textElement_.deserialize(data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getNumProp(data, 'padding');

    if (des.hasProp(data, 'format')) {
        this.textFormatter_ = new anychart.formatting.TextFormatter(des.getStringProp(data, 'format'));
    }


    if (des.hasProp(data, 'position')) {
        var positionType = anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition;
        switch (des.getEnumProp(data, 'position')) {
            case 'beforeaxislabels':
                this.position_ = positionType.BEFORE_AXIS_LABELS;
                this.isOutside_ = true;
                break;
            case 'afteraxislabels':
                this.position_ = positionType.AFTER_AXIS_LABELS;
                this.isOutside_ = true;
                break;
            case 'axis':
                this.position_ = positionType.AXIS;
                this.isOutside_ = true;
                break;
            case 'near':
                this.position_ = positionType.NEAR;
                this.isOutside_ = false;
                break;
            case 'center':
                this.position_ = positionType.CENTER;
                this.isOutside_ = false;
                break;
            case 'far':
                this.position_ = positionType.FAR;
                this.isOutside_ = false;
                break;
        }
    }
};
//------------------------------------------------------------------------------
//                        Initialization.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarker} marker
 */
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.initialize = function(svgManager, marker) {
    this.textElement_.initSize(svgManager, this.textFormatter_.getValue(marker));
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.markers.AxisMarkerLabel.prototype.draw = function(svgManager, container) {
    var textSprite = this.textElement_.createSVGText(svgManager);
    textSprite.setPosition(this.pixPosition_.x, this.pixPosition_.y);
    container.appendSprite(textSprite);
};
//------------------------------------------------------------------------------
//
//                          LineAxisMarker class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.markers.AxisMarker}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker = function(axis) {
    anychart.plots.axesPlot.axes.markers.AxisMarker.call(this, axis);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.LineAxisMarker,
        anychart.plots.axesPlot.axes.markers.AxisMarker);
//------------------------------------------------------------------------------
//                           Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.value_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.startValue_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.endValue_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.strStartValue_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.strEndValue_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.strValue_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'value'))
        this.strStartValue_ = this.strEndValue_ = this.strValue_ = des.getStringProp(data, 'value');

    if (des.hasProp(data, 'start_value'))
        this.strStartValue_ = des.getStringProp(data, 'start_value');

    if (des.hasProp(data, 'end_value'))
        this.strEndValue_ = des.getStringProp(data, 'end_value');
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.axes.markers.styles.LineAxisMarkerStyle();
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.getStyleNodeName = function() {
    return 'line_axis_marker_style';
};
//------------------------------------------------------------------------------
//                           Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.initialize = function(svgManager) {
    if (this.strStartValue_) this.startValue_ = this.getValue(this.strStartValue_);
    if (this.strEndValue_) this.endValue_ = this.getValue(this.strEndValue_);
    if (this.strValue_) this.value_ = this.getValue(this.strValue_);

    if (this.affectScaleRange_ && !this.axis_.isCategorized()) {
        if (!isNaN(this.startValue_)) this.checkScale(this.startValue_);
        if (!isNaN(this.endValue_)) this.checkScale(this.endValue_);
    }

    goog.base(this, 'initialize', svgManager);
};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.draw = function(svgManager) {
    if (!this.visible_) return;

    goog.base(this, 'draw', svgManager);

    var state = this.style_.getNormal();
    var scale = this.axis_.getScale();

    var style = state.getStroke().getSVGStroke(svgManager);
    style += svgManager.getEmptySVGFill();


    var pathData = this.axis_.getViewPort().getMarkersViewPort().getMarkerLinePathData(
            svgManager,
            scale.localTransform(this.startValue_),
            scale.localTransform(this.endValue_));
    var element = svgManager.createPath();

    element.setAttribute('d', pathData);
    element.setAttribute('style', style);

    this.sprite_.appendChild(element);

    if (state.getLabel())
        this.drawLabel(svgManager, state.getLabel(), this.startValue_, this.endValue_);
};
//------------------------------------------------------------------------------
//                              Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.getTokenValue = function(token) {
    if (token == '%Value' && !this.tokensHash_.has(token))
        this.tokensHash_.add(token, (this.startValue_ + this.endValue_) / 2);

    if ((token == '%StartValue' || token == '%Start') && !this.tokensHash_.has(token))
        this.tokensHash_.add(this.startValue_);

    if ((token == '%EndValue' || token == '%End') && !this.tokensHash_.has(token))
        this.tokensHash_.add(this.startValue_);

    return goog.base(this, 'getTokenValue', token);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.LineAxisMarker.prototype.getTokenType = function(token) {
    switch (token) {
        case '%Value' :
        case '%Start' :
        case '%StartValue' :
        case '%End' :
        case '%EndValue' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
};
//------------------------------------------------------------------------------
//
//                          RangeAxisMarker class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.markers.AxisMarker}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker = function(axis) {
    anychart.plots.axesPlot.axes.markers.AxisMarker.call(this, axis);
};
goog.inherits(anychart.plots.axesPlot.axes.markers.RangeAxisMarker,
        anychart.plots.axesPlot.axes.markers.AxisMarker);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.minimum_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.maximum_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.minimumStart_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.minimumEnd_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.maximumStart_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.maximumEnd_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMinimum_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMaximum_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMinimumStart_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMinimumEnd_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMaximumStart_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.strMaximumEnd_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);

    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'minimum'))
        this.strMinimum_ = des.getStringProp(data, 'minimum');

    if (des.hasProp(data, 'minimum_start'))
        this.strMinimumStart_ = des.getStringProp(data, 'minimum_start');

    if (des.hasProp(data, 'minimum_end'))
        this.strMinimumEnd_ = des.getStringProp(data, 'minimum_end');

    if (des.hasProp(data, 'maximum'))
        this.strMaximum_ = des.getStringProp(data, 'maximum');

    if (des.hasProp(data, 'maximum_start'))
        this.strMaximumStart_ = des.getStringProp(data, 'maximum_start');

    if (des.hasProp(data, 'maximum_end'))
        this.strMaximumEnd_ = des.getStringProp(data, 'maximum_end');
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.axes.markers.styles.RangeAxisMarkerStyle();
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.getStyleNodeName = function() {
    return 'range_axis_marker_style';
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.initialize = function(svgManager) {
    if (this.strMinimum_)
        this.minimumStart_ = this.minimumEnd_ = this.minimum_ = this.getValue(this.strMinimum_);

    if (this.strMinimumStart_)
        this.minimumStart_ = this.getValue(this.strMinimumStart_);

    if (this.strMinimumEnd_)
        this.minimumEnd_ = this.getValue(this.strMinimumEnd_);

    if (this.strMaximum_)
        this.maximumStart_ = this.maximumEnd_ = this.maximum_ = this.getValue(this.strMaximum_);

    if (this.strMaximumStart_)
        this.maximumStart_ = this.getValue(this.strMaximumStart_);

    if (this.strMaximumEnd_)
        this.maximumEnd_ = this.getValue(this.strMaximumEnd_);

    goog.base(this, 'initialize', svgManager)
};
//------------------------------------------------------------------------------
//                             Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.draw = function(svgManager) {
    if (!this.visible_) return;

    goog.base(this, 'draw', svgManager);

    var state = this.style_.getNormal();
    var scale = this.axis_.getScale();
    var viewPort = this.axis_.getViewPort().getMarkersViewPort();
    var path, style;

    var pixMinStart = scale.localTransform(this.minimumStart_);
    var pixMinEnd = scale.localTransform(this.minimumEnd_);
    var pixMaxStart = scale.localTransform(this.maximumStart_);
    var pixMaxEnd = scale.localTransform(this.maximumEnd_);

    var bounds = viewPort.getRangeMarkerBounds(pixMinStart, pixMinEnd, pixMaxStart, pixMaxEnd);
    var minStrokeElementPathData = viewPort.getMarkerLinePathData(svgManager, pixMinStart, pixMinEnd, true);
    var maxStrokeElementPathData = viewPort.getMarkerLinePathData(svgManager, pixMaxStart, pixMaxEnd, false, true);

    //fill
    if (state.getFill()) {
        style = state.getFill().getSVGFill(svgManager, bounds);
        style += svgManager.getEmptySVGStroke();
        var fillElement = svgManager.createPath();
        fillElement.setAttribute('d', minStrokeElementPathData + maxStrokeElementPathData);
        fillElement.setAttribute('style', style);
        this.sprite_.appendChild(fillElement);
    }

    //min line
    if (state.getMinimumLine()) {
        style = state.getMinimumLine().getSVGStroke(svgManager);
        style += svgManager.getEmptySVGFill();
        var minStrokeElement = svgManager.createPath();
        minStrokeElement.setAttribute('d', minStrokeElementPathData);
        minStrokeElement.setAttribute('style', style);
        this.sprite_.appendChild(minStrokeElement);
    }

    //max line
    if (state.getMaximumLine()) {
        style = state.getMaximumLine().getSVGStroke(svgManager);
        style += svgManager.getEmptySVGFill();
        var maxStrokeElement = svgManager.createPath();
        maxStrokeElement.setAttribute('d', viewPort.getMarkerLinePathData(svgManager, pixMaxStart, pixMaxEnd, true));
        maxStrokeElement.setAttribute('style', style);
        this.sprite_.appendChild(maxStrokeElement);
    }

    //hatch fill
    if (state.getHatchFill()) {
        style = state.getHatchFill().getSVGHatchFill(svgManager);
        var hatchFillElement = svgManager.createPath();
        hatchFillElement.setAttribute('d', minStrokeElementPathData + maxStrokeElementPathData);
        hatchFillElement.setAttribute('style', style);
        this.sprite_.appendChild(hatchFillElement);
    }

    if (state.getLabel())
        this.drawLabel(
                svgManager,
                state.getLabel(),
                (this.minimumStart_ + this.maximumStart_) / 2,
                (this.minimumEnd_ + this.maximumEnd_) / 2);
};
//------------------------------------------------------------------------------
//                             Formatting.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.getTokenValue = function(token) {
    if (!this.tokensHash_.has(token)) {
        if (token == '%Value')
            this.tokensHash_.add(token, (this.minimumStart_ + this.minimumEnd_ + this.maximumStart_ + this.maximumEnd_) / 4);

        if (token == '%MinValue' || token == '%Min')
            this.tokensHash_.add(token, (this.minimumStart_ + this.minimumEnd_) / 2);

        if (token == '%MaxValue' || token == '%Max')
            this.tokensHash_.add(token, (this.maximumStart_ + this.maximumEnd_) / 2);

        if (token == '%MinStartValue' || token == '%MinStart')
            this.tokensHash_.add(token, this.minimumStart_);

        if (token == '%MaxStartValue' || token == '%MaxStart')
            this.tokensHash_.add(token, this.maximumStart_);

        if (token == '%MinEndValue' || token == '%MinEnd')
            this.tokensHash_.add(token, this.minimumEnd_);

        if (token == '%MaxEndValue' || token == '%MaxEnd')
            this.tokensHash_.add(token, this.maximumEnd_);
    }

    return goog.base(this, 'getTokenValue', token);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.markers.RangeAxisMarker.prototype.getTokenType = function(token) {
    switch (token) {
        case '%Value' :
        case '%MinValue' :
        case '%Min' :
        case '%MaxValue' :
        case '%Max' :
        case '%MinStartValue' :
        case '%MinStart' :
        case '%MaxStartValue' :
        case '%MaxStart' :
        case '%MinEndValue' :
        case '%MinEnd' :
        case '%MaxEndValue' :
        case '%MaxEnd' :
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//
//                          AxisMarkersList class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList = function() {
    this.beforeAxisLabelsSpace_ = 0;
    this.axisLabelsSpace_ = 0;
    this.afterAxisLabelsSpace_ = 0;
};
//------------------------------------------------------------------------------
//                          Common properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.axis_ = null;
/**
 * @param {anychart.plots.axesPlot.axes.Axis} value
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.setAxis = function(value) {
    this.axis_ = value;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.getSprite = function() {
    return this.sprite_;
};
/**
 * @param {anychart.svg.SVGSprite}
        */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.setSprite = function(value) {
    this.sprite_ = value;
};
/**
 * @private
 * @type {Array.<anychart.plots.axesPlot.axes.markers.AxisMarker>}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.markers_ = null;
/**
 * Add marker to marker list
 * @param {anychart.plots.axesPlot.axes.markers.AxisMarker} marker
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.addMarker = function(marker) {
    marker.setMarkersList(this);
    this.markers_.push(marker);
};
//------------------------------------------------------------------------------
//                          Space properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.beforeAxisLabelsSpace_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.getBeforeAxisLabelsSpace = function() {
    return this.beforeAxisLabelsSpace_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.axisLabelsSpace_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.getAxisLabelsSpace = function() {
    return this.axisLabelsSpace_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.afterAxisLabelsSpace_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.getAfterAxisLabelsSpace = function() {
    return this.afterAxisLabelsSpace_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @param {object} data
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.deserialize = function(data, stylesList) {
    this.markers_ = [];
    var i, count, marker, markers, displayUnderData, collection;
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'lines')) {
        collection = des.getProp(data, 'lines');
        if (!des.hasProp(collection, 'line'))return;
        displayUnderData = this.deserializeDisplayUnderData_(collection);
        markers = des.getPropArray(collection, 'line');
        for (i = 0,count = markers.length; i < count; i++) {
            marker = new anychart.plots.axesPlot.axes.markers.LineAxisMarker(this.axis_);
            marker.setDisplayUnderData(displayUnderData);
            marker.deserialize(markers[i], stylesList);
            this.addMarker(marker);
        }
    }

    if (des.hasProp(data, 'ranges')) {
        collection = des.getProp(data, 'ranges');
        if (!des.hasProp(collection, 'range'))return;
        displayUnderData = this.deserializeDisplayUnderData_(collection);
        markers = des.getPropArray(collection, 'range');
        for (i = 0,count = markers.length; i < count; i++) {
            marker = new anychart.plots.axesPlot.axes.markers.RangeAxisMarker(this.axis_);
            marker.setDisplayUnderData(displayUnderData);
            marker.deserialize(markers[i], stylesList);
            this.addMarker(marker);
        }
    }
};
/**
 * @private
 * @param {Object} data
 * @return {Boolean}
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.deserializeDisplayUnderData_ = function(data) {
    var des = anychart.utils.deserialization;
    return des.hasProp(data, 'display_under_data') ? des.getBoolProp(data, 'display_under_data') : true;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize markers list
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.initialize = function(svgManager) {
    var markersCount = this.markers_.length;
    for (var i = 0; i < markersCount; i++) {
        var marker = this.markers_[i];
        marker.initialize(svgManager);

        var label = marker.getLabel();
        if (label && label.isOutside()) {
            var labelSpace;
            var position = anychart.plots.axesPlot.axes.markers.AxisMarkerLabelPosition;
            var viewPort = this.axis_.getViewPort().getLabelsViewPort();

            switch (label.getPosition()) {
                case position.AFTER_AXIS_LABELS :
                    labelSpace = viewPort.getTextSpace(label.getBounds()) + label.getPadding();
                    this.beforeAxisLabelsSpace_ = Math.max(labelSpace, this.beforeAxisLabelsSpace_);
                    break;
                case position.BEFORE_AXIS_LABELS :
                    labelSpace = viewPort.getTextSpace(label.getBounds()) + label.getPadding();
                    this.afterAxisLabelsSpace_ = Math.max(labelSpace, this.afterAxisLabelsSpace_);
                    break;
                case position.AXIS :
                    labelSpace = viewPort.getTextSpace(label.getBounds()) + label.getPadding();
                    this.axisLabelsSpace_ = Math.max(labelSpace, this.axisLabelsSpace_);
                    break;
            }
        }
    }
};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
/**
 * Draw all axes markers
 */
anychart.plots.axesPlot.axes.markers.AxisMarkersList.prototype.draw = function(svgManager) {
    var markersCount = this.markers_.length;
    for (var i = 0; i < markersCount; i++)
        this.markers_[i].draw(svgManager);
};