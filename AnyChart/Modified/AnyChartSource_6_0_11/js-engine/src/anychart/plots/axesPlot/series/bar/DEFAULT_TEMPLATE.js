goog.provide('anychart.plots.axesPlot.series.bar.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.bar.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"bar_style": [
				{
					"border": {
						"type": "Solid",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "%Color",
									"opacity": "1"
								},
								{
									"position": "1",
									"color": "Blend(DarkColor(%Color),%Color,0.7)",
									"opacity": "1"
								}
							],
							"angle": "90"
						},
						"type": "Gradient",
						"color": "%Color",
						"opacity": "0.9"
					},
					"effects": {
						"bevel": {
							"enabled": "true",
							"distance": "1"
						},
						"enabled": "True"
					},
					"states": {
						"hover": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),%White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"color": "White",
								"opacity": "0.9"
							}
						},
						"pushed": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"color": "White",
								"opacity": "0.9"
							},
							"effects": {
								"bevel": {
									"enabled": "true",
									"distance": "2",
									"angle": "225"
								}
							}
						},
						"selected_normal": {
							"border": {
								"thickness": "2",
								"color": "DarkColor(%Color)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"opacity": "0.3"
							}
						},
						"selected_hover": {
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"color": "White",
								"opacity": "0.9"
							},
							"border": {
								"thickness": "2",
								"color": "DarkColor(White)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"color": "DarkColor(White)",
								"opacity": "0.3"
							}
						},
						"missing": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)",
								"thickness": "1",
								"opacity": "0.4"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "anychart_default"
				},
				{
					"fill": {
						"color": "%Color"
					},
					"name": "AquaLight"
				},
				{
					"fill": {
						"color": "%Color"
					},
					"name": "AquaDark"
				},
				{
					"effects": {
						"bevel": {
							"enabled": "false"
						},
						"drop_shadow": {
							"enabled": "false"
						}
					},
					"fill": {
						"color": "%Color"
					},
					"border": {
						"color": "DarkColor(%Color)",
						"type": "solid",
						"thickness": "1"
					},
					"states": {
						"normal": {
							"fill": {
								"color": "%Color",
								"opacity": "1"
							}
						},
						"hover": {
							"fill": {
								"color": "White",
								"opacity": "0.8"
							}
						},
						"pushed": {
							"fill": {
								"color": "Blend(White,Black,0.9)",
								"opacity": "1"
							},
							"effects": {
								"bevel": {
									"enabled": "true",
									"distance": "2",
									"angle": "225",
									"shadow_opacity": "0.1"
								}
							}
						},
						"selected_normal": {
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"opacity": "0.3"
							},
							"fill": {
								"color": "%Color",
								"opacity": "1"
							}
						},
						"selected_hover": {
							"fill": {
								"color": "White",
								"opacity": ".8"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"color": "Black",
								"opacity": "0.3"
							}
						},
						"missing": {
							"fill": {
								"color": "White",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "cylinder"
				},
				{
					"fill": {
						"type": "solid",
						"color": "%color"
					},
					"name": "plastic"
				},
				{
					"name": "silver",
					"parent": "cylinder"
				}
			]
		},
		"data_plot_settings": {
			"bar_series": {
				"animation": {
					"enabled": "True",
					"style": "defaultScaleYBottom"
				},
				"label_settings": {
					"animation": {
						"enabled": "true",
						"style": "defaultLabel"
					},
					"enabled": "false"
				},
				"marker_settings": {
					"marker": {
						"anchor": "CenterTop",
						"valign": "Center",
						"halign": "Center"
					},
					"animation": {
						"enabled": "true",
						"style": "defaultMarker"
					},
					"enabled": "false"
				},
				"tooltip_settings": {
				},
				"group_padding": "0.5",
				"point_padding": "0.2"
			},
			"range_bar_series": {
				"bar_style": {
				},
				"animation": {
					"style": "defaultScaleYCenter"
				},
				"start_point": {
					"tooltip_settings": {
						"format": {
							"value": "{%YRangeStart}"
						},
						"position": {
							"anchor": "centerBottom",
							"valign": "bottom"
						},
						"enabled": "False"
					},
					"marker_settings": {
						"animation": {
							"style": "defaultMarker"
						}
					},
					"label_settings": {
						"animation": {
							"style": "defaultLabel"
						}
					}
				},
				"end_point": {
					"tooltip_settings": {
						"format": {
							"value": "{%YRangeEnd}"
						},
						"position": {
							"anchor": "centerTop",
							"valign": "top"
						},
						"enabled": "False"
					},
					"marker_settings": {
						"animation": {
							"style": "defaultMarker"
						}
					},
					"label_settings": {
						"animation": {
							"style": "defaultLabel"
						}
					}
				},
				"group_padding": "0.5",
				"point_padding": "0.2"
			}
		}
	}
};