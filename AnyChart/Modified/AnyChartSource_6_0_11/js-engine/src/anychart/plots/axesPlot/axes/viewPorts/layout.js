goog.provide('anychart.plots.axesPlot.axes.viewPorts.layout');

goog.require('anychart.layout');

//-----------------------------------------------------------------------------------
//
//                          LayoutViewPort class
//
//-----------------------------------------------------------------------------------

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort = function() {
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getNormalAngle = function(angle) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getInvertedAngle = function(angle) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getNormalAnchor = function(angle) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getNormalHAlign = function(hAlign, vAlign) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getNormalVAlign = function(hAlign, vAlign) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getInvertedAnchor = function(anchor) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getInvertedHAlign = function(hAlign, vAlign) {
    goog.abstractMethod();
};

anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.prototype.getInvertedVAlign = function(hAlign, vAlign) {
    goog.abstractMethod();
};

//-----------------------------------------------------------------------------------
//
//                          HorizontalLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort);

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getInvertedAngle = function(angle, baseScaleInverted) {
    return baseScaleInverted ? (angle - 180) : (-angle);
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getNormalAngle = function(angle, baseScaleInverted) {
    return baseScaleInverted ? (180 - angle) : angle;
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getNormalAnchor = function(anchor) {
    return anchor;
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getInvertedAnchor = function(anchor) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER_BOTTOM: return anychart.layout.Anchor.CENTER_TOP;
        case anychart.layout.Anchor.CENTER_TOP: return anychart.layout.Anchor.CENTER_BOTTOM;
        case anychart.layout.Anchor.LEFT_BOTTOM: return anychart.layout.Anchor.LEFT_TOP;
        case anychart.layout.Anchor.LEFT_TOP: return anychart.layout.Anchor.LEFT_BOTTOM;
        case anychart.layout.Anchor.RIGHT_BOTTOM: return anychart.layout.Anchor.RIGHT_TOP;
        case anychart.layout.Anchor.RIGHT_TOP: return anychart.layout.Anchor.RIGHT_BOTTOM;
        default: return anchor;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getNormalHAlign = function(hAlign, vAlign) {
    return hAlign;
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getNormalVAlign = function(hAlign, vAlign) {
    return vAlign;
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getInvertedHAlign = function(hAlign, vAlign) {
    return hAlign;
};

anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.prototype.getInvertedVAlign = function(hAlign, vAlign) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP: return anychart.layout.VerticalAlign.BOTTOM;
        case anychart.layout.VerticalAlign.BOTTOM: return anychart.layout.VerticalAlign.TOP;
        default: return vAlign;
    }
};

//-----------------------------------------------------------------------------------
//
//                          BottomLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.BottomLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.BottomLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort);


//-----------------------------------------------------------------------------------
//
//                          TopLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.TopLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.TopLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.HorizontalLayoutViewPort);


//-----------------------------------------------------------------------------------
//
//                          VerticalLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.LayoutViewPort);

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getInvertedAngle = function(angle, baseScaleInverted) {
    return baseScaleInverted ? (90 - angle) : (angle - 90);
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getNormalAngle = function(angle, baseScaleInverted) {
    return baseScaleInverted ? (angle - 270) : (270 - angle);
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getNormalAnchor = function(anchor) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER_BOTTOM: return anychart.layout.Anchor.CENTER_LEFT;
        case anychart.layout.Anchor.CENTER_LEFT: return anychart.layout.Anchor.CENTER_TOP;
        case anychart.layout.Anchor.CENTER_RIGHT: return anychart.layout.Anchor.CENTER_BOTTOM;
        case anychart.layout.Anchor.CENTER_TOP: return anychart.layout.Anchor.CENTER_RIGHT;
        case anychart.layout.Anchor.LEFT_BOTTOM: return anychart.layout.Anchor.LEFT_TOP;
        case anychart.layout.Anchor.LEFT_TOP: return anychart.layout.Anchor.RIGHT_TOP;
        case anychart.layout.Anchor.RIGHT_BOTTOM: return anychart.layout.Anchor.LEFT_BOTTOM;
        case anychart.layout.Anchor.RIGHT_TOP: return anychart.layout.Anchor.RIGHT_BOTTOM;
        default: return anchor;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getInvertedAnchor = function(anchor) {
    switch (anchor) {
        case anychart.layout.Anchor.CENTER_BOTTOM: return anychart.layout.Anchor.CENTER_RIGHT;
        case anychart.layout.Anchor.CENTER_LEFT: return anychart.layout.Anchor.CENTER_TOP;
        case anychart.layout.Anchor.CENTER_RIGHT: return anychart.layout.Anchor.CENTER_BOTTOM;
        case anychart.layout.Anchor.CENTER_TOP: return anychart.layout.Anchor.CENTER_LEFT;
        case anychart.layout.Anchor.LEFT_BOTTOM: return anychart.layout.Anchor.RIGHT_TOP;
        case anychart.layout.Anchor.RIGHT_TOP: return anychart.layout.Anchor.LEFT_BOTTOM;
        default: return anchor;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getNormalHAlign = function(hAlign, vAlign) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP: return anychart.layout.HorizontalAlign.RIGHT;
        case anychart.layout.VerticalAlign.BOTTOM: return anychart.layout.HorizontalAlign.LEFT;
        default: return anychart.layout.HorizontalAlign.CENTER;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getNormalVAlign = function(hAlign, vAlign) {
    switch (hAlign) {
        case anychart.layout.HorizontalAlign.LEFT: return anychart.layout.VerticalAlign.TOP;
        case anychart.layout.HorizontalAlign.RIGHT: return anychart.layout.VerticalAlign.BOTTOM;
        default: return anychart.layout.VerticalAlign.CENTER;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getInvertedHAlign = function(hAlign, vAlign) {
    switch (vAlign) {
        case anychart.layout.VerticalAlign.TOP: return anychart.layout.HorizontalAlign.LEFT;
        case anychart.layout.VerticalAlign.BOTTOM: return anychart.layout.HorizontalAlign.RIGHT;
        default: return anychart.layout.HorizontalAlign.CENTER;
    }
};

anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.prototype.getInvertedVAlign = function(hAlign, vAlign) {
    return this.getNormalVAlign(hAlign, vAlign);
};

//-----------------------------------------------------------------------------------
//
//                          LeftLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.LeftLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.LeftLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort);


//-----------------------------------------------------------------------------------
//
//                          RightLayoutViewPort class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.viewPorts.layout.RightLayoutViewPort = function() {
    anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.viewPorts.layout.RightLayoutViewPort,
              anychart.plots.axesPlot.axes.viewPorts.layout.VerticalLayoutViewPort);
