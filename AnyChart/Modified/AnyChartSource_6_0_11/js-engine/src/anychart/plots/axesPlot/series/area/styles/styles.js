/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.FillAreaShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.AreaStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.AreaStyle};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState};</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.styles.RangeAreaStyle};</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.area.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                       TopAreaStrokeShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.StrokeStyleShape}
 */
anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape = function() {
    anychart.styles.background.StrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape,
        anychart.styles.background.StrokeStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape.prototype.createElement = function() {
    return this.styleShapes_.createTopBorderShape();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape.prototype.updatePath = function() {
    this.styleShapes_.updateTopBorderShapePath(this.element_);
};
//------------------------------------------------------------------------------
//
//                       RightAreaStrokeShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.StrokeStyleShape}
 */
anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape = function() {
    anychart.styles.background.FillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape,
        anychart.styles.background.FillStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape.prototype.createElement = function() {
    return this.styleShapes_.createRightBorderShape();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape.prototype.updatePath = function() {
    this.styleShapes_.updateRightAreaStrokePath(this.element_);
};
//------------------------------------------------------------------------------
//
//                       FillAreaShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillStyleShape}
 */
anychart.plots.axesPlot.series.area.styles.FillAreaShape = function() {
    anychart.styles.background.FillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.FillAreaShape,
        anychart.styles.background.FillStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.FillAreaShape.prototype.createElement = function() {
    return this.styleShapes_.createShape();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.FillAreaShape.prototype.updatePath = function() {
    return this.styleShapes_.updateShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                       HatchFillAreaShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillStyleShape}
 */
anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape = function() {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape,
        anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape.prototype.createElement = function() {
    return this.styleShapes_.createShape();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape.prototype.updatePath = function() {
    return this.styleShapes_.updateShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                       AreaStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes = function() {
    anychart.styles.StyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.AreaStyleShapes,
        anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.topBorderShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.rightBorderShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.area.styles.FillAreaShape}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.fillShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.hatchFillShape_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();

    if (style.needCreateStrokeShape()) {
        this.topBorderShape_ = new anychart.plots.axesPlot.series.area.styles.TopAreaStrokeShape();
        this.topBorderShape_.initialize(this.point_, this);
    }

    if (style.needCreateFillShape()) {
        this.fillShape_ = new anychart.plots.axesPlot.series.area.styles.FillAreaShape();
        this.fillShape_.initialize(this.point_, this);
        this.rightBorderShape_ = new anychart.plots.axesPlot.series.area.styles.RightAreaStrokeShape();
        this.rightBorderShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.area.styles.HatchFillAreaShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.placeShapes = function() {
    if (this.fillShape_) {
        this.point_.getSprite().appendChild(this.fillShape_.getElement());
        this.point_.getSprite().appendChild(this.rightBorderShape_.getElement());
    }

    if (this.topBorderShape_)
        this.point_.getSprite().appendChild(this.topBorderShape_.getElement());


    if (this.hatchFillShape_)
        this.point_.getSprite().appendChild(this.hatchFillShape_.getElement());
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.createTopBorderShape = function() {
    return this.point_.createTopBorderShape(this.point_.getSVGManager());
};
/**
 * Update top border path
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.updateTopBorderShapePath = function(element) {
    element.setAttribute(
            'points',
            this.point_.getTopBorderShapePath(this.point_.getSVGManager()));
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.createRightBorderShape = function() {
    return this.point_.createRightBorderShape(this.point_.getSVGManager());
};
/**
 * Update right area stroke path
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.updateRightAreaStrokePath = function(element) {
    element.setAttribute('d', this.point_.getRightBorderShapePath());
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.createShape = function() {
    return this.point_.createShape(this.point_.getSVGManager());
};
/**
 * Update fill or hatch fill shape
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.updateShape = function(element) {
    element.setAttribute(
            'd',
            this.point_.getShapePath(this.point_.getSVGManager()));
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if (this.fillShape_) {
        this.fillShape_.update(opt_styleState, opt_updateData);
        this.rightBorderShape_.update(opt_styleState, opt_updateData);
    }

    if (this.topBorderShape_)
        this.topBorderShape_.update(opt_styleState, opt_updateData);

    if (this.hatchFillShape_)
        this.hatchFillShape_.update(opt_styleState, opt_updateData);
};
/**
 * Update element effects
 * @param {SVGElement} element
 * @param {anychart.styles.StyleStateWithEffects} styleState
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.prototype.updateEffects = function(element, styleState) {
    if (styleState.needUpdateEffects())
        element.setAttribute('filter', styleState.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};
//------------------------------------------------------------------------------
//
//                          AreaStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyleState = function(style, opt_stateType) {
    anychart.styles.background.BackgroundStyleState.call(this, style, opt_stateType);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.AreaStyleState,
        anychart.styles.background.BackgroundStyleState);
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.styles.AreaStyleState.prototype.deserializeBorder = function(config) {
    if (anychart.utils.deserialization.hasProp(config, 'line')) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(anychart.utils.deserialization.getProp(config, 'line'));
    }
};
//------------------------------------------------------------------------------
//
//                       Area style class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.area.styles.AreaStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.AreaStyle,
        anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.AreaStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.series.area.styles.AreaStyleState;
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.AreaStyle.prototype.createStyleShapes = function(point) {
    return new anychart.plots.axesPlot.series.area.styles.AreaStyleShapes();
};
//------------------------------------------------------------------------------
//
//                      RangeAreaBottomBorderShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape = function() {
    anychart.styles.background.StrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape,
        anychart.styles.background.StrokeStyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape.prototype.update = function(opt_styleState, opt_updateData) {
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape.prototype.updateStyle = function(opt_styleState) {
    if(!opt_styleState.needUpdateEndBorder()) return;
    goog.base(this, 'updateStyle', opt_styleState);

};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape.prototype.updatePath = function() {
    this.styleShapes_.updateBottomBorderShape(this.element_);
};
//------------------------------------------------------------------------------
//
//                          RangeAreaStyleShapes class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.area.styles.AreaStyleShapes}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes = function() {
    anychart.plots.axesPlot.series.area.styles.AreaStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes,
        anychart.plots.axesPlot.series.area.styles.AreaStyleShapes);
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.background.StrokeStyleShape}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.bottomBorderShape_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.createShapes = function() {
    goog.base(this, 'createShapes');
    if (this.point_.getStyle().needCreateEndBorder()) {
        this.bottomBorderShape_ = new anychart.plots.axesPlot.series.area.styles.RangeAreaBottomBorderShape();
        this.bottomBorderShape_.initialize(this.point_, this, this.createBottomBorderShape);
    }
};
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.placeShapes = function() {
    if (this.fillShape_) {
        this.point_.getSprite().appendChild(this.fillShape_.getElement());
        this.point_.getSprite().appendChild(this.rightBorderShape_.getElement());
    }

    if (this.topBorderShape_)
        this.point_.getSprite().appendChild(this.topBorderShape_.getElement());

    if (this.bottomBorderShape_)
        this.point_.getSprite().appendChild(this.bottomBorderShape_.getElement());

    if (this.hatchFillShape_)
        this.point_.getSprite().appendChild(this.hatchFillShape_.getElement());

};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.createBottomBorderShape = function() {
    return this.point_.createBottomBorderShape(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.updateBottomBorderShape = function(element) {
      element.setAttribute(
              'points',
              this.point_.getBottomBorderPath(this.point_.getSVGManager()));
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    goog.base(this, 'update', opt_styleState, opt_updateData);
    
    if(opt_styleState && opt_styleState.needUpdateEndBorder())
        this.bottomBorderShape_.update(opt_styleState, opt_updateData);
};
//------------------------------------------------------------------------------
//
//                          RangeAreaStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.area.styles.AreaStyleState}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState = function(style, opt_state) {
    anychart.plots.axesPlot.series.area.styles.AreaStyleState.call(this, style, opt_state);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState,
        anychart.plots.axesPlot.series.area.styles.AreaStyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState.prototype.endBorder_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState.prototype.getEndBorder = function() {
    return this.endBorder_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState.prototype.needUpdateEndBorder = function() {
    return this.endBorder_ && this.endBorder_.isEnabled();
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState.prototype.deserializeBorder = function(data) {
    var des = anychart.utils.deserialization;
    if (des.isEnabled(des.getProp(data, 'start_line'))) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(des.getProp(data, 'start_line'));
    }

    if (des.isEnabled(des.getProp(data, 'end_line'))) {
        this.endBorder_ = new anychart.visual.stroke.Stroke();
        this.endBorder_.deserialize(des.getProp(data, 'end_line'));
    }
};
//------------------------------------------------------------------------------
//
//                       RangeAreaStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.styles.RangeAreaStyle,
        anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                          StyleState.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RangeAreaStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.series.area.styles.RangeAreaStyleState;
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.styles.RangeAreaStyle.prototype.createStyleShapes = function(point) {
    return new anychart.plots.axesPlot.series.area.styles.RangeAreaStyleShapes();
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.styles.RangeAreaStyle.prototype.needCreateEndBorder = function() {
    return  this.normal_.needUpdateEndBorder() ||
            this.hover_.needUpdateEndBorder() ||
            this.pushed_.needUpdateEndBorder() ||
            this.selectedNormal_.needUpdateEndBorder() ||
            this.selectedHover_.needUpdateEndBorder() ||
            this.missing_.needUpdateEndBorder();
};