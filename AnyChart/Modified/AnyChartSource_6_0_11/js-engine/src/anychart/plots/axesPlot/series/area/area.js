/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.AreaSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.AreaPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.StepBackwardAreaSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.StepBackwardAreaPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.StepForwardAreaSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.StepForwardAreaPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.RangeAreaPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.RangeAreaSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.area');
goog.require('anychart.plots.axesPlot.data');
goog.require('anychart.plots.axesPlot.series.area.styles');
//------------------------------------------------------------------------------
//
//                          AreaGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.AxesGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings,
        anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          IGlobalSettings.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.series.area.styles.AreaStyle();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return 'area_series';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return 'area_style';
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series;
    switch (seriesType) {
        case anychart.series.SeriesType.STEP_AREA_BACKWARD:
            series = new anychart.plots.axesPlot.series.area.StepBackwardAreaSeries();
            break;
        case anychart.series.SeriesType.STEP_AREA_FORWARD:
            series = new anychart.plots.axesPlot.series.area.StepForwardAreaSeries();
            break;
        case anychart.series.SeriesType.AREA:
            series = new anychart.plots.axesPlot.series.area.AreaSeries();
            break;
        default :
            series = new anychart.plots.axesPlot.series.area.AreaSeries();
            break;
    }
    series.setType(seriesType);
    series.setClusterKey(anychart.series.SeriesType.LINE);
    return series;
};
//------------------------------------------------------------------------------
//                          Legend icon
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.hasIconDrawer = function(seriesType) {
    return false;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.drawIcon = function(seriesType, item, sprite, bounds) {
    var rgbColor = item.getColor().getColor().toRGB();
    var darkColor = anychart.utils.deserialization.getColor('darkcolor(' + rgbColor + ')');
    var style = 'stroke-thickness:1;stroke-opacity:1;stroke:' + darkColor.getColor().toRGB() + ';';
    style += 'fill-opacity:1; fill:' + rgbColor + ';';
    var topPath = this.getIconTopPath_(seriesType, sprite.getSVGManager(), bounds);
    var bottomPath = this.getIconBottomPath_(seriesType, sprite.getSVGManager(), bounds);
    this.applyIconPath_(topPath, style, sprite);
    this.applyIconPath_(bottomPath, style, sprite);
};
/**
 * @private
 * @param seriesType
 * @param svgManager
 * @param bounds
 */
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.getIconTopPath_ = function(seriesType, svgManager, bounds) {
    var path = svgManager.createPath();

    var pathData = svgManager.pMove(bounds.x, bounds.y);
    if (seriesType == anychart.series.SeriesType.RANGE_AREA) {
        var d = bounds.height / 4;
        pathData += svgManager.pLine(bounds.x + bounds.width / 2, bounds.y + d);
        pathData += svgManager.pLine(bounds.right, bounds.y);
    } else {

        path += svgManager.pCircleArc(bounds.x + bounds.width / 2, bounds.y + bounds.height / 2, bounds.right, bounds.y);
        //g.curveTo(bounds.x + bounds.width / 2, bounds.y + bounds.height / 2, bounds.right, bounds.y);
    }
    path.setAttribute('d', pathData);
    return path;
};
/**
 * @private
 * @param seriesType
 * @param svgManager
 * @param bounds
 */
anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.getIconBottomPath_ = function(seriesType, svgManager, bounds) {
    var path = svgManager.createPath();
    var pathData = '';

    if (seriesType == anychart.series.SeriesType.RANGE_AREA) {
        var d = bounds.height / 4;
        pathData += svgManager.pLine(bounds.x + bounds.width / 2, bounds.bottom - d);
        pathData += svgManager.pLine(bounds.x, bounds.bottom);
    } else {
        //g.curveTo(bounds.x + bounds.width/2, bounds.y + bounds.height/2, bounds.x, bounds.bottom);
    }
    pathData += svgManager.pLine(bounds.x, bounds.y);
    path.setAttribute('d', pathData);
    return path;
};

anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings.prototype.applyIconPath_ = function(path, style, sprite) {
    path.setAttribute('style', style);
    sprite.appendChild(path);
};
//------------------------------------------------------------------------------
//
//                          AreaSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineSeries}
 */
anychart.plots.axesPlot.series.area.AreaSeries = function() {
    anychart.plots.axesPlot.series.line.LineSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.AreaSeries,
        anychart.plots.axesPlot.series.line.LineSeries);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.needCreateGradientBounds_ = false;
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.setNeedCreateGradientRect = function(value) {
    this.needCreateGradientBounds_ = value;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.needCreateGradientRect = function() {
    return this.needCreateGradientBounds_;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaSeries.prototype.needCallOnBeforeDraw = function() {
    return true;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.valueAxisStart_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.getValueAxisStart = function() {
    return this.valueAxisStart_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.leftTop_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.rightBottom_ = null;
//------------------------------------------------------------------------------
//                          Gradient bounds.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.gradientBounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.getGradientBounds = function() {
    return this.gradientBounds_;
};

anychart.plots.axesPlot.series.area.AreaSeries.prototype.calcGradientBounds = function() {
    this.leftTop_ = new anychart.utils.geom.Point(Number.MAX_VALUE, Number.MAX_VALUE);
    this.rightBottom_ = new anychart.utils.geom.Point(-Number.MAX_VALUE, -Number.MAX_VALUE);

    var pointsCount = this.getNumPoints();
    for (var i = 0; i < pointsCount; i++) {
        var point = this.getPointAt(i);

        if (point && !point.isMissing()) {
            point.setNeedCreateDrawingPoints(true);
            point.setNeedInitializePixValues(true);

            point.calcBounds();

            this.getRectPoints_(point);
            if (point.getPreviousStackPoint() == null) {
                this.leftTop_.x = Math.min(point.getBottomEnd().x, point.getBottomStart().x, this.leftTop_.x);
                this.leftTop_.y = Math.min(point.getBottomEnd().y, point.getBottomStart().y, this.leftTop_.y);

                this.rightBottom_.x = Math.max(point.getBottomEnd().x, point.getBottomStart().x, this.rightBottom_.x);
                this.rightBottom_.y = Math.max(point.getBottomEnd().y, point.getBottomStart().y, this.rightBottom_.y);
            } else {
                this.getRectPoints_(point.getPreviousStackPoint());
            }
        }
    }

    this.gradientBounds_ = new anychart.utils.geom.Rectangle(
            this.leftTop_.x,
            this.leftTop_.y,
            this.rightBottom_.x - this.leftTop_.x,
            this.rightBottom_.y - this.leftTop_.y);

    this.needCreateGradientBounds_ = false;
};
/**
 * @private
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 */
anychart.plots.axesPlot.series.area.AreaSeries.prototype.getRectPoints_ = function(point) {
    var drawPoints = point.getDrawingPoints();

    for (var i = 0; i < drawPoints.length; i++) {
        this.leftTop_.x = Math.min(drawPoints[i].x, this.leftTop_.x);
        this.leftTop_.y = Math.min(drawPoints[i].y, this.leftTop_.y);

        this.rightBottom_.x = Math.max(drawPoints[i].x, this.rightBottom_.x);
        this.rightBottom_.y = Math.max(drawPoints[i].y, this.rightBottom_.y);
    }
};
//------------------------------------------------------------------------------
//                          On before.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaSeries.prototype.onBeforeDraw = function() {
    goog.base(this, 'onBeforeDraw');
    this.valueAxisStart_ = (this.valueAxis_.getScale().getMinimum() > 0) ? this.valueAxis_.getScale().getMinimum() : 0;
    this.calcGradientBounds();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaSeries.prototype.onBeforeResize = function() {
    this.calcGradientBounds();
};
//------------------------------------------------------------------------------
//                  Create point.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.area.AreaPoint();
};
//------------------------------------------------------------------------------
//
//                          AreaPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LinePoint}
 */
anychart.plots.axesPlot.series.area.AreaPoint = function() {
    anychart.plots.axesPlot.series.line.LinePoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.AreaPoint,
        anychart.plots.axesPlot.series.line.LinePoint);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.hasGradient_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.bottomStart_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getBottomStart = function() {
    return this.bottomStart_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.bottomEnd_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getBottomEnd = function() {
    return this.bottomEnd_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.needCreateDrawingPoints_ = true;
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.setNeedCreateDrawingPoints = function(value) {
    this.needCreateDrawingPoints_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.needInitializePixValues_ = true;
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.setNeedInitializePixValues = function(value) {
    this.needInitializePixValues_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.needCalcPixValue_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.needCalcPixValue = function() {
    return this.needCalcPixValue_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.setNeedCalcPixValue = function(value) {
    this.needCalcPixValue_ = value;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaPoint.prototype.initialize = function(svgManager) {
    if (this.isMissing()) return;

    this.series_.setNeedCreateGradientRect(this.style_.hasGradient());

    goog.base(this, 'initialize', svgManager);

    return this.sprite_;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaPoint.prototype.initializePixValue = function() {
    this.initializePixValue_();
    this.needInitializePixValues_ = false;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaPoint.prototype.initializePixValue_ = function() {
    if (!this.needInitializePixValues_) return;
    goog.base(this, 'initializePixValue_');

    if (this.isLineDrawable_ && !this.previousStackPoint_) {
        this.bottomStart_ = new anychart.utils.geom.Point();
        this.bottomEnd_ = new anychart.utils.geom.Point();

        this.series_.getValueAxis().transform(this, this.series_.getValueAxisStart(), this.bottomStart_);
        this.series_.getValueAxis().transform(this, this.series_.getValueAxisStart(), this.bottomEnd_);
    }
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createDrawingPoints = function() {
    this.createDrawingPoints_();
    this.needCreateDrawingPoints_ = false;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createDrawingPoints_ = function() {
    if (!this.needCreateDrawingPoints_) return;
    this.drawingPoints_ = [];
    var tmp,tmp1;

    if (this.hasPrev_) {
        tmp = this.series_.getPointAt(this.index_ - 1).pixValuePt_;
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.pixValuePt_.x) / 2;
        tmp1.y = (tmp.y + this.pixValuePt_.y) / 2;
        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomStart_.y = tmp1.y;
            else
                this.bottomStart_.x = tmp1.x;
        }
        this.drawingPoints_.push(tmp1);
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_)
            this.bottomStart_.y = this.pixValuePt_.y;
        else
            this.bottomStart_.x = this.pixValuePt_.x;
    }

    this.drawingPoints_.push(this.pixValuePt_);
    
    if (this.hasNext_) {
        this.series_.getPointAt(this.index_ + 1).initializePixValue();
        this.series_.getPointAt(this.index_ + 1).setNeedCalcPixValue(false);
        tmp = this.series_.getPointAt(this.index_ + 1).pixValuePt_;
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.pixValuePt_.x) / 2;
        tmp1.y = (tmp.y + this.pixValuePt_.y) / 2;
        this.drawingPoints_.push(tmp1);

        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomEnd_.y = tmp1.y;
            else
                this.bottomEnd_.x = tmp1.x;
        }
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_)
            this.bottomEnd_.y = this.pixValuePt_.y;
        else
            this.bottomEnd_.x = this.pixValuePt_.x;
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getGradientBounds = function() {
    //gradient bounds
    if (this.series_.needCreateGradientRect()) this.series_.calcGradientBounds();
    return this.series_.getGradientBounds();
};
/**
 * @param {String} svgManager
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createTopBorderShape = function(svgManager) {
    return svgManager.createPolyline();
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getTopBorderShapePath = function(svgManager) {
    var pointCount = this.drawingPoints_.length;

    var pathData = svgManager.polyLineDraw(
            this.drawingPoints_[0].x,
            this.drawingPoints_[0].y);

    for (var i = 0; i < pointCount; i++)
        pathData += svgManager.polyLineDraw(
                this.drawingPoints_[i].x,
                this.drawingPoints_[i].y);

    return pathData;
};
/**
 * @param {String} svgManager
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createRightBorderShape = function(svgManager) {
    return svgManager.createPath();
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getRightBorderShapePath = function() {
    if(!this.bottomEnd_) return 'M 0 0'; //todo: hotfix, разобраться как нормально отрисовать эту чертову палочку и почему ее вообще нужно рисовать
    var lastPoint = this.drawingPoints_[this.drawingPoints_.length - 1];
    var path;
    var svgManager = this.getSVGManager();
    if (this.isHorizontal_) {
        path = svgManager.pMove(this.bottomEnd_.x, lastPoint.y - 1);
        path += svgManager.pLine(this.bottomEnd_.x + (lastPoint.x - this.bottomEnd_.x), lastPoint.y - 1);
        path += svgManager.pLine(this.bottomEnd_.x + (lastPoint.x - this.bottomEnd_.x), lastPoint.y + 1);
        path += svgManager.pLine(this.bottomEnd_.x, lastPoint.y + 1);
        path += svgManager.pLine(this.bottomEnd_.x, lastPoint.y);
    } else {
        path = svgManager.pMove(lastPoint.x - 1, lastPoint.y);
        path += svgManager.pLine(lastPoint.x + 1, lastPoint.y);
        path += svgManager.pLine(lastPoint.x + 1, lastPoint.y + (this.bottomEnd_.y - lastPoint.y));
        path += svgManager.pLine(lastPoint.x - 1, lastPoint.y + (this.bottomEnd_.y - lastPoint.y));
        path += svgManager.pLine(lastPoint.x - 1, lastPoint.y);
    }
    return path;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createShape = function(svgManager) {
    return svgManager.createPath();
};
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getShapePath = function(svgManager) {
    return  this.createTopShapeData_(svgManager) +
            this.createBottomShapeData_(svgManager) +
            svgManager.pFinalize();
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createTopShapeData_ = function(svgManager) {
    var pointCount = this.drawingPoints_.length;

    var pathData = svgManager.pMove(this.drawingPoints_[0].x, this.drawingPoints_[0].y);
    for (var i = 1; i < pointCount; i++)
        pathData += svgManager.pLine(this.drawingPoints_[i].x, this.drawingPoints_[i].y);
    return pathData;
};

anychart.plots.axesPlot.series.area.AreaPoint.prototype.createRightBorder_ = function(svgManager) {
    var lastPoint = this.drawingPoints_.length - 1;
    var pathData = svgManager.pMove(this.drawingPoints_[lastPoint].x, this.drawingPoints_[lastPoint].y);
    pathData += this.createBottomShapeData_(svgManager);
    return pathData;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.createBottomShapeData_ = function(svgManager) {
    var pathData = '';

    if (!this.previousStackPoint_) {
        pathData += svgManager.pLine(this.bottomEnd_.x, this.bottomEnd_.y);
        pathData += svgManager.pLine(this.bottomStart_.x, this.bottomStart_.y);
    } else
        pathData = this.previousStackPoint_.getSegmentDataForBottom_(svgManager);

    return pathData;
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.axesPlot.series.area.AreaPoint.prototype.getSegmentDataForBottom_ = function(svgManager) {
    var pathData = '';
    for (var i = this.drawingPoints_.length - 1; i >= 0; i--)
        pathData += svgManager.pLine(this.drawingPoints_[i].x, this.drawingPoints_[i].y);
    return pathData;
};
//------------------------------------------------------------------------------
//
//                          StepBackwardAreaSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.area.AreaSeries}
 */
anychart.plots.axesPlot.series.area.StepBackwardAreaSeries = function() {
    anychart.plots.axesPlot.series.area.AreaSeries.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.StepBackwardAreaSeries,
        anychart.plots.axesPlot.series.area.AreaSeries);
//------------------------------------------------------------------------------
//                          Point.
//------------------------------------------------------------------------------
//@inheritDoc*/
anychart.plots.axesPlot.series.area.StepBackwardAreaSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.area.StepBackwardAreaPoint();
};
//------------------------------------------------------------------------------
//
//                          StepBackwardAreaPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.area.AreaPoint}
 */
anychart.plots.axesPlot.series.area.StepBackwardAreaPoint = function() {
    anychart.plots.axesPlot.series.area.AreaPoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.StepBackwardAreaPoint,
        anychart.plots.axesPlot.series.area.AreaPoint);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.StepBackwardAreaPoint.prototype.createDrawingPoints = function() {
    this.drawingPoints_ = [];

    var tmp;
    var tmp1;
    if (this.hasPrev_) {
        tmp = this.series_.getPointAt(this.index_ - 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ?
                new anychart.utils.geom.Point(
                        this.pixValuePt_.x,
                        (tmp.y + this.pixValuePt_.y) / 2)
                :
                new anychart.utils.geom.Point(
                        (tmp.x + this.pixValuePt_.x) / 2,
                        this.pixValuePt_.y);

        this.drawingPoints_.push(tmp1);
        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomStart_.y = tmp1.y;
            else
                this.bottomStart_.x = tmp1.x;
        }
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_)
            this.bottomStart_.y = this.pixValuePt_.y;
        else
            this.bottomStart_.x = this.pixValuePt_.x;
    }
    this.drawingPoints_.push(this.pixValuePt_);

    if (this.hasNext_) {
        this.series_.getPointAt(this.index_ + 1).initializePixValue();
        this.series_.getPointAt(this.index_ + 1).setNeedCalcPixValue(false);
        tmp = this.series_.getPointAt(this.index_ + 1).pixValuePt_;

        tmp1 = this.isHorizontal_ ?
                new anychart.utils.geom.Point(tmp.x, this.pixValuePt_.y) :
                new anychart.utils.geom.Point(this.pixValuePt_.x, tmp.y);

        this.drawingPoints_.push(tmp1);

        tmp1 = this.isHorizontal_ ?
                new anychart.utils.geom.Point(
                        tmp.x,
                        (tmp.y + this.pixValuePt_.y) / 2)
                :
                new anychart.utils.geom.Point(
                        (tmp.x + this.pixValuePt_.x) / 2,
                        tmp.y);

        this.drawingPoints_.push(tmp1);

        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomEnd_.y = tmp1.y;
            else
                this.bottomEnd_.x = tmp1.x;
        }
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_) this.bottomEnd_.y = this.pixValuePt_.y;
        else this.bottomEnd_.x = this.pixValuePt_.x;
    }
};
//------------------------------------------------------------------------------
//
//                          StepForwardAreaSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.area.StepForwardAreaSeries = function() {
    anychart.plots.axesPlot.series.area.AreaSeries.call(this);
};

goog.inherits(anychart.plots.axesPlot.series.area.StepForwardAreaSeries,
        anychart.plots.axesPlot.series.area.AreaSeries);

anychart.plots.axesPlot.series.area.StepForwardAreaSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.area.StepForwardAreaPoint();
};

//------------------------------------------------------------------------------
//
//                          StepForwardAreaPoint class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.area.StepForwardAreaPoint}
 */
anychart.plots.axesPlot.series.area.StepForwardAreaPoint = function() {
    anychart.plots.axesPlot.series.area.AreaPoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.StepForwardAreaPoint,
        anychart.plots.axesPlot.series.area.AreaPoint);

//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.StepForwardAreaPoint.prototype.createDrawingPoints = function() {
    this.drawingPoints_ = [];

    var tmp;
    var tmp1;
    if (this.hasPrev_) {
        tmp = this.series_.getPointAt(this.index_ - 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, (tmp.y + this.pixValuePt_.y) / 2) : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, tmp.y);
        this.drawingPoints_.push(tmp1);

        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomStart_.y = tmp1.y;
            else
                this.bottomStart_.x = tmp1.x;
        }

        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(tmp.x, this.pixValuePt_.y) : new anychart.utils.geom.Point(this.pixValuePt_.x, tmp.y);
        this.drawingPoints_.push(tmp1);
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_)
            this.bottomStart_.y = this.pixValuePt_.y;
        else
            this.bottomStart_.x = this.pixValuePt_.x;
    }
    this.drawingPoints_.push(this.pixValuePt_);

    if (this.hasNext_) {
        this.series_.getPointAt(this.index_ + 1).initializePixValue();
        this.series_.getPointAt(this.index_ + 1).setNeedCalcPixValue(false);
        tmp = this.series_.getPointAt(this.index_ + 1).pixValuePt_;
        tmp1 = this.isHorizontal_ ? new anychart.utils.geom.Point(this.pixValuePt_.x, (tmp.y + this.pixValuePt_.y) / 2)
                : new anychart.utils.geom.Point((tmp.x + this.pixValuePt_.x) / 2, this.pixValuePt_.y);
        this.drawingPoints_.push(tmp1);
        if (this.previousStackPoint_ == null) {
            if (this.isHorizontal_)
                this.bottomEnd_.y = tmp1.y;
            else
                this.bottomEnd_.x = tmp1.x;
        }
    } else if (this.previousStackPoint_ == null) {
        if (this.isHorizontal_)
            this.bottomEnd_.y = this.pixValuePt_.y;
        else
            this.bottomEnd_.x = this.pixValuePt_.x;
    }
};
//------------------------------------------------------------------------------
//
//                          RangeAreaPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangePoint}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint = function() {
    anychart.plots.axesPlot.data.RangePoint.apply(this);
    this.needCalcPixValue_ = true;
    this.isValuePointsInitialized_ = false;
    this.hasPrev_ = false;
    this.hasNext_ = false;
    this.startPixValuePt_ = new anychart.utils.geom.Point();
    this.endPixValuePt_ = new anychart.utils.geom.Point();
    this.needCreateDrawingPoints_ = true;
    this.needInitializePixValues_ = true;
};
goog.inherits(anychart.plots.axesPlot.series.area.RangeAreaPoint,
        anychart.plots.axesPlot.data.RangePoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.cachedElement_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needCalcPixValue_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needCalcPixValue = function() {
    return this.needCalcPixValue_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.setNeedCalcPixValue = function(value) {
    this.needCalcPixValue_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.isValuePointsInitialized_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.startPixValuePt_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getStartPixValuePoint = function() {
    return this.startPixValuePt_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.endPixValuePt_ = null;
/**
 * @return {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getEndPixValuePoint = function() {
    return this.endPixValuePt_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needCreateDrawingPoints_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needCreateDrawingPoints = function() {
    return this.needCreateDrawingPoints_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.setNeedCreateDrawingPoints = function(value) {
    this.needCreateDrawingPoints_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needInitializePixValues_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.needInitializePixValues = function() {
    return this.needInitializePixValues_;
};
/**
 * @param {Boolean} value
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.setNeedInitializePixValues = function(value) {
    this.needInitializePixValues_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.isDrawable_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.isDrawable = function() {
    return this.isDrawable_;
};
/**
 * @private
 * @type {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.startDrawingPoints_ = null;
/**
 * @return {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getStartDrawingPoints = function() {
    return this.startDrawingPoints_;
};
/**
 * @private
 * @type {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.endDrawingPoints_ = null;
/**
 * @return {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getEndDrawingPoints = function() {
    return this.endDrawingPoints_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.hasPrev_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.hasNext_ = null;
//------------------------------------------------------------------------------
//                          Initialize points
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.initializePixValue = function() {
    this.execInitializePixValues_();
    this.needInitializePixValues_ = false;
};
/**
 * @private
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.execInitializePixValues_ = function() {
    if (!this.needInitializePixValues_) return;

    var series = this.getSeries();

    if (!this.isValuePointsInitialized_) {
        this.hasPrev_ = (this.index_ > 0) &&
                (this.getSeries().getPointAt(this.index_ - 1)) &&
                (!this.getSeries().getPointAt(this.index_ - 1).isMissing());

        this.hasNext_ = (this.index_ < (this.getSeries().getNumPoints() - 1)) &&
                (this.getSeries().getPointAt(this.index_ + 1)) &&
                (!this.getSeries().getPointAt(this.index_ + 1).isMissing());

        this.isDrawable_ = this.hasPrev_ || this.hasNext_;
        this.isValuePointsInitialized_ = true;
    }

    series.getArgumentAxis().transform(this, this.getX(), this.startPixValuePt_);
    series.getValueAxis().transform(this, this.start_, this.startPixValuePt_);
    series.getArgumentAxis().transform(this, this.getX(), this.endPixValuePt_);
    series.getValueAxis().transform(this, this.end_, this.endPixValuePt_);

    var x = Math.min(this.startPixValuePt_.x, this.endPixValuePt_.x);
    var y = Math.min(this.startPixValuePt_.y, this.endPixValuePt_.y);
    this.bounds_ = new anychart.utils.geom.Rectangle(
            x,
            y,
            Math.max(this.startPixValuePt_.x, this.endPixValuePt_.x) - x,
            Math.max(this.startPixValuePt_.y, this.endPixValuePt_.y) - y);
};
//------------------------------------------------------------------------------
//                          Create points
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.createDrawingPoints = function() {
    this.execCreateDrawingPoints_();
    this.needCreateDrawingPoints_ = false;
};
/**
 * @private
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.execCreateDrawingPoints_ = function() {
    if (!this.needCreateDrawingPoints_) return;

    this.startDrawingPoints_ = [];
    this.endDrawingPoints_ = [];
    var series = this.getSeries();

    var tmp;
    var tmp1;
    if (this.hasPrev_) {
        tmp = series.getPointAt(this.index_ - 1).getStartPixValuePoint();
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.getStartPixValuePoint().x) / 2;
        tmp1.y = (tmp.y + this.getStartPixValuePoint().y) / 2;
        this.startDrawingPoints_.push(tmp1);
    }

    this.startDrawingPoints_.push(this.startPixValuePt_);

    if (this.hasNext_) {
        series.getPointAt(this.index_ + 1).initializePixValue();
        series.getPointAt(this.index_ + 1).setNeedCalcPixValue(false);
        tmp = series.getPointAt(this.index_ + 1).getStartPixValuePoint();
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.getStartPixValuePoint().x) / 2;
        tmp1.y = (tmp.y + this.getStartPixValuePoint().y) / 2;
        this.startDrawingPoints_.push(tmp1);

        tmp = series.getPointAt(this.index_ + 1).getEndPixValuePoint();
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.getEndPixValuePoint().x) / 2;
        tmp1.y = (tmp.y + this.getEndPixValuePoint().y) / 2;
        this.endDrawingPoints_.push(tmp1);
    }

    this.endDrawingPoints_.push(this.endPixValuePt_);

    if (this.hasPrev_) {
        tmp = series.getPointAt(this.index_ - 1).getEndPixValuePoint();
        tmp1 = new anychart.utils.geom.Point();
        tmp1.x = (tmp.x + this.getEndPixValuePoint().x) / 2;
        tmp1.y = (tmp.y + this.getEndPixValuePoint().y) / 2;
        this.endDrawingPoints_.push(tmp1);
    }
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.createShape = function(svgManager) {
    return svgManager.createPath();
};
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getShapePath = function(svgManager) {
    var i;
    var pathData = svgManager.pMove(
            this.startDrawingPoints_[0].x,
            this.startDrawingPoints_[0].y);

    for (i = 1; i < this.startDrawingPoints_.length; i++)
        pathData += svgManager.pLine(
                this.startDrawingPoints_[i].x,
                this.startDrawingPoints_[i].y);

    for (i = 0; i < this.endDrawingPoints_.length; i++)
        pathData += svgManager.pLine(
                this.endDrawingPoints_[i].x,
                this.endDrawingPoints_[i].y);

    return pathData;
};
/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.createTopBorderShape = function(svgManager) {
    return svgManager.createPolyline();
};
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getTopBorderShapePath = function(svgManager) {
    var polyLinePoints = svgManager.polyLineDraw(
            this.endDrawingPoints_[0].x,
            this.endDrawingPoints_[0].y);

    for (var i = 1; i < this.endDrawingPoints_.length; i++)
        polyLinePoints += svgManager.polyLineDraw(
                this.endDrawingPoints_[i].x,
                this.endDrawingPoints_[i].y);

    return polyLinePoints;
};
/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.createBottomBorderShape = function(svgManager) {
    return svgManager.createPolyline();
};
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getBottomBorderPath = function(svgManager) {
    var polyLinePoints = '';
    var pointsCount = this.startDrawingPoints_.length;
    for (var i = 0; i < pointsCount; i++)
        polyLinePoints += svgManager.polyLineDraw(
                this.startDrawingPoints_[i].x,
                this.startDrawingPoints_[i].y);
    return polyLinePoints;
};
/**
 * @param {String} svgManager
 */
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.createRightBorderShape = function(svgManager) {
    return svgManager.createPath();
};
anychart.plots.axesPlot.series.area.RangeAreaPoint.prototype.getRightBorderShapePath = function() {
    var lastStartPoint = this.startDrawingPoints_[this.startDrawingPoints_.length - 1];
    var firstEndPoint = this.endDrawingPoints_[0];
    var path;
    var svgManger = this.getSVGManager();

    if (this.isHorizontal_) {
        path = svgManger.pMove(lastStartPoint.x, lastStartPoint.y);
        path += svgManger.pLine(lastStartPoint.x, lastStartPoint.y + .1);
        path += svgManger.pLine(firstEndPoint.x, lastStartPoint.y + .1);
        path += svgManger.pLine(firstEndPoint.x, firstEndPoint.y - .1);
    } else {
        path = svgManger.pMove(lastStartPoint.x, lastStartPoint.y);
        path += svgManger.pLine(lastStartPoint.x + .1, lastStartPoint.y);
        path += svgManger.pLine(lastStartPoint.x + .1, firstEndPoint.y);
        path += svgManger.pLine(firstEndPoint.x - .1, firstEndPoint.y);
    }
    return path;

};
//------------------------------------------------------------------------------
//
//                          RangeAreaSeries class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangeSeries}
 */
anychart.plots.axesPlot.series.area.RangeAreaSeries = function() {
    anychart.plots.axesPlot.data.RangeSeries.apply(this);
    this.isSortable_ = false;
    this.needCreateGradientRect_ = true;
};
goog.inherits(anychart.plots.axesPlot.series.area.RangeAreaSeries,
        anychart.plots.axesPlot.data.RangeSeries);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.needCreateGradientRect_ = null;
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.needCreateGradientRect = function() {
    return this.needCreateGradientRect_;
};
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.setNeedCreateGradientRect = function(value) {
    this.needCreateGradientRect_ = value;
};
/**
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.gradientBounds_ = null;
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.getGradientBounds = function() {
    return this.gradientBounds_;
};

/**
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.sprite_ = null;
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.getSprite = function() {
    return this.sprite_;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.needCallOnBeforeDraw = function() {
    return true;
};
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return false;
};

anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.area.RangeAreaPoint();
};

anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.calcGradientBounds = function() {
    if (!this.needCreateGradientRect_) return;
    var lt = new anychart.utils.geom.Point(Number.MAX_VALUE, Number.MAX_VALUE);
    var rb = new anychart.utils.geom.Point(-Number.MAX_VALUE, -Number.MAX_VALUE);
    for (var j = 0; j < this.points_.length; j++) {
        var point = this.points_[j];
        if (!point.isMissing()) {
            point.setNeedCreateDrawingPoints(true);
            point.setNeedInitializePixValues(true);

            if (point.needCalcPixValue()) point.initializePixValue();
            if (point.isDrawable()) point.createDrawingPoints();

            for (var i = 0; i < point.getStartDrawingPoints().length; i++) {
                lt.x = Math.min(point.getStartDrawingPoints()[i].x, lt.x);
                lt.y = Math.min(point.getStartDrawingPoints()[i].y, lt.y);

                rb.x = Math.max(point.getStartDrawingPoints()[i].x, rb.x);
                rb.y = Math.max(point.getStartDrawingPoints()[i].y, rb.y);
            }
            for (i = 0; i < point.getEndDrawingPoints().length; i++) {
                lt.x = Math.min(point.getEndDrawingPoints()[i].x, lt.x);
                lt.y = Math.min(point.getEndDrawingPoints()[i].y, lt.y);

                rb.x = Math.max(point.getEndDrawingPoints()[i].x, rb.x);
                rb.y = Math.max(point.getEndDrawingPoints()[i].y, rb.y);
            }
        }
    }
    this.gradientBounds_ = new anychart.utils.geom.Rectangle(lt.x, lt.y, rb.x - lt.x, rb.y - lt.y);
};
//------------------------------------------------------------------------------
//                          On before.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.onBeforeDraw = function() {
    goog.base(this, 'onBeforeDraw');
    this.calcGradientBounds();
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.area.RangeAreaSeries.prototype.onBeforeResize = function() {
    this.calcGradientBounds();
};
//------------------------------------------------------------------------------
//
//                          RangeAreaGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.data.RangeGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.RangeGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings,
        anychart.plots.axesPlot.data.RangeGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series;
    switch (seriesType) {
        default :
        case anychart.series.SeriesType.RANGE_AREA:
            series = new anychart.plots.axesPlot.series.area.RangeAreaSeries();
            break;
        case anychart.series.SeriesType.RANGE_SPLINE_AREA:
            series = new anychart.plots.axesPlot.series.area.RangeAreaSeries(); //todo: change to spline as impelmented
            break;

    }
    series.type = seriesType;
    series.clusterKey = anychart.series.SeriesType.RANGE_AREA;
    return series;
};

anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.series.area.styles.RangeAreaStyle();
};

anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return 'range_area_series';
};

anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return 'range_area_style';
};
anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.pointsCanBeAnimated = function() {
    return false;
};
anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings.prototype.seriesCanBeAnimated = function() {
    return true;
};
//------------------------------------------------------------------------------
//                          Icon
//------------------------------------------------------------------------------
//todo: implement
