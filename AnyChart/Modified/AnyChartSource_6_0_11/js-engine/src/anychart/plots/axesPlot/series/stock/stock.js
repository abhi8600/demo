/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.stock.StockPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.StockSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.OHLCPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.OHLCSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.CandlestickPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.CandlestickSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.stock');
goog.require('anychart.plots.axesPlot.series.stock.styles');
goog.require('anychart.plots.axesPlot.data');
//------------------------------------------------------------------------------
//
//                          StockPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.StockPoint = function () {
    anychart.plots.axesPlot.data.AxesPlotPoint.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.StockPoint,
    anychart.plots.axesPlot.data.AxesPlotPoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.open_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.getOpen = function () {
    return this.open_;
};
/**
 * @param {Number} value
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.setOpen = function (value) {
    this.open_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.close_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.getClose = function () {
    return this.close_;
};
/**
 * @param {Number} value
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.setClose = function (value) {
    this.close_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.high_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.getHigh = function () {
    return this.high_;
};
/**
 * @param {Number} value
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.setHigh = function (value) {
    this.high_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.low_ = null;
/**
 * @return {Number}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.getLow = function () {
    return this.low_;
};
/**
 * @param {Number} value
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.setLow = function (value) {
    this.low_ = value;
};

/**
 * @protected
 * @type {Boolean}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.isUp_ = null;
anychart.plots.axesPlot.series.stock.StockPoint.prototype.isUp = function () {
    return this.isUp_;
};
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.highPoint_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.lowPoint_ = null;
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @override
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.checkMissing_ = function (data) {
    this.high_ = this.deserializeStockValue_(data, 'high', 'h');
    this.low_ = this.deserializeStockValue_(data, 'low', 'l');
    this.open_ = this.deserializeStockValue_(data, 'open', 'o');
    this.close_ = this.deserializeStockValue_(data, 'close', 'c');
    this.isMissing_ = isNaN(this.high_) || isNaN(this.low_) || isNaN(this.open_) || isNaN(this.close_);
};

/**
 * @override
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.deserializeValue_ = function (data) {
    if (!this.isMissing_) {
        this.isUp_ = this.close_ > this.open_;
        var valueAxis = this.getSeries().getValueAxis();
        valueAxis.checkScaleValue(this.high_);
        valueAxis.checkScaleValue(this.low_);
        valueAxis.checkScaleValue(this.open_);
        valueAxis.checkScaleValue(this.close_);
    }
};

/**
 * @private
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.deserializeStockValue_ = function (data, fullAtrName, shortAtrName) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, fullAtrName))
        return this.deserializeValueFromConfig_(des.getProp(data, fullAtrName));
    if (des.hasProp(data, shortAtrName))
        return this.deserializeValueFromConfig_(des.getProp(data, shortAtrName));
    return NaN;
};
//------------------------------------------------------------------------------
//                          Size
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.StockPoint.prototype.calculatePixelValues = function () {
    this.highPoint_ = new anychart.utils.geom.Point();
    this.lowPoint_ = new anychart.utils.geom.Point();

    var series = this.getSeries();
    series.getValueAxis().transform(this, this.high_, this.highPoint_);
    series.getValueAxis().transform(this, this.low_, this.lowPoint_);
    series.getArgumentAxis().transform(this, this.getX(), this.highPoint_);
    series.getArgumentAxis().transform(this, this.getX(), this.lowPoint_);
};

anychart.plots.axesPlot.series.stock.StockPoint.prototype.getWidth = function () {
    //if (this.getPlot().isScatter())
    /*return (this.getGlobalSettings().isPercentPointScetterWidth() ?
     this.getPlot().getBounds().width : 1)*
     this.getGlobalSettings().getPointScetterWidth()/2;*/ //todo :implement scatter
    return this.clusterSettings_.getClusterWidth() / 2;
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.StockPoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};

anychart.plots.axesPlot.series.stock.StockPoint.prototype.execDrawing = function () {
    goog.base(this, 'execDrawing');
    this.drawStock_(this.sprite_.getSVGManager());

};

anychart.plots.axesPlot.series.stock.StockPoint.prototype.redraw = function (opt_clear) {
    goog.base(this, 'redraw', false);
    this.updateStock_(this.sprite_.getSVGManager());
};

/**
 * @private
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.drawStock_ = function (svgManager) {
    goog.abstractMethod();
};

/**
 * @private
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.updateStock_ = function (svgManager) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Formatting initialization
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.setDefaultTokenValues = function () {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%High', 0);
    this.tokensHash_.add('%Hight', 0);
    this.tokensHash_.add('%Low', 0);
    this.tokensHash_.add('%Open', 0);
    this.tokensHash_.add('%Close', 0);
};
/**
 * @override
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.StockPoint.prototype.getTokenType = function (token) {
    switch (token) {
        case '%High':
        case '%Hight':
        case '%Low':
        case '%Open':
        case '%Close':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.StockPoint.prototype.calculatePointTokenValues = function () {
    goog.base(this, 'calculatePointTokenValues');
    this.tokensHash_.add('%High', this.high_);
    this.tokensHash_.add('%Hight', this.high_);
    this.tokensHash_.add('%Low', this.low_);
    this.tokensHash_.add('%Open', this.open_);
    this.tokensHash_.add('%Close', this.close_);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.StockPoint.prototype.recalculatePointTokenValues = function () {
};
//------------------------------------------------------------------------------
//                    Formatting token values calculation
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.StockPoint.prototype.serialize = function (opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['High'] = this.high_;
    opt_target['Low'] = this.low_;
    opt_target['Open'] = this.open_;
    opt_target['Close'] = this.close_;
    return opt_target;
};
//------------------------------------------------------------------------------
//
//                          StockSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.StockSeries = function () {
    anychart.plots.axesPlot.data.AxesPlotSeries.apply(this);
    this.isSortable_ = false;
};
goog.inherits(anychart.plots.axesPlot.series.stock.StockSeries,
    anychart.plots.axesPlot.data.AxesPlotSeries);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.StockSeries.prototype.isClusterizableOnCategorizedAxis = function () {
    return true;
};

anychart.plots.axesPlot.series.stock.StockSeries.prototype.clusterContainsOnlyOnePoint = function () {
    return true;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.StockSeries.prototype.needCallOnBeforeDraw = function () {
    return true;
};
//------------------------------------------------------------------------------
//
//                    StockGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings = function () {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings,
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.highInterpolator_ = null;
/**
 * @private
 * @type {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.lowInterpolator_ = null;
/**
 * @private
 * @type {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.openInterpolator_ = null;
/**
 * @private
 * @type {anychart.interpolation.IMissingInterpolator}
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.closeInterpolator_ = null;
//------------------------------------------------------------------------------
//                          Interpolate
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.initializeMissingInterpolators = function () {
    this.highInterpolator_ = this.createMissingInterpolator();
    this.lowInterpolator_ = this.createMissingInterpolator();
    this.openInterpolator_ = this.createMissingInterpolator();
    this.closeInterpolator_ = this.createMissingInterpolator();

    this.highInterpolator_.initialize(
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.getHigh,
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.setHigh
    );
    this.lowInterpolator_.initialize(
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.getLow,
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.setLow
    );
    this.openInterpolator_.initialize(
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.getOpen,
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.setOpen
    );
    this.closeInterpolator_.initialize(
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.getClose,
        anychart.plots.axesPlot.series.stock.StockPoint.prototype.setClose
    );
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.checkMissingPointDuringDeserialize = function (point) {
    this.highInterpolator_.checkDuringDeserialize(point, point.getIndex());
    this.lowInterpolator_.checkDuringDeserialize(point, point.getIndex());
    this.openInterpolator_.checkDuringDeserialize(point, point.getIndex());
    this.closeInterpolator_.checkDuringDeserialize(point, point.getIndex());
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.prototype.interpolateMissingPoints = function (series) {
    this.highInterpolator_.interpolate(series.getPoints());
    this.lowInterpolator_.interpolate(series.getPoints());
    this.openInterpolator_.interpolate(series.getPoints());
    this.closeInterpolator_.interpolate(series.getPoints());

    this.highInterpolator_.clear();
    this.lowInterpolator_.clear();
    this.openInterpolator_.clear();
    this.closeInterpolator_.clear();
};
//------------------------------------------------------------------------------
//
//                          OHLCPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.OHLCPoint = function () {
    anychart.plots.axesPlot.series.stock.StockPoint.apply(this);
    this.useCrispEdges_ = true;
};
goog.inherits(anychart.plots.axesPlot.series.stock.OHLCPoint,
    anychart.plots.axesPlot.series.stock.StockPoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.openPt_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.closePt_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.centerOpenPt_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.centerClosePt_ = null;

anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.useCrispEdges_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.startSegment_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.centerSegment_ = null;
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.endSegment_ = null;
//------------------------------------------------------------------------------
//                          Size
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.initialize = function (svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.calculatePixelValues = function () {
    goog.base(this, 'calculatePixelValues');

    this.openPt_ = new anychart.utils.geom.Point();
    this.closePt_ = new anychart.utils.geom.Point();
    this.centerOpenPt_ = new anychart.utils.geom.Point();
    this.centerClosePt_ = new anychart.utils.geom.Point();

    var width = this.getWidth();
    var series = this.getSeries();

    series.getValueAxis().transform(this, this.open_, this.openPt_);
    series.getValueAxis().transform(this, this.open_, this.centerOpenPt_);
    series.getValueAxis().transform(this, this.close_, this.closePt_);
    series.getValueAxis().transform(this, this.close_, this.centerClosePt_);

    series.getArgumentAxis().transform(this, this.x_, this.openPt_, -width);
    series.getArgumentAxis().transform(this, this.x_, this.centerOpenPt_);
    series.getArgumentAxis().transform(this, this.x_, this.closePt_, width);
    series.getArgumentAxis().transform(this, this.x_, this.centerClosePt_);


    var x = Math.min(
        this.openPt_.x,
        this.closePt_.x,
        this.highPoint_.x,
        this.lowPoint_.x,
        this.centerOpenPt_.x,
        this.centerClosePt_.x);

    var y = Math.min(
        this.openPt_.y,
        this.closePt_.y,
        this.highPoint_.y,
        this.lowPoint_.y,
        this.centerOpenPt_.y,
        this.centerClosePt_.y);

    this.bounds_ = new anychart.utils.geom.Rectangle(
        x,
        y,
        Math.max(
            this.openPt_.x,
            this.closePt_.x,
            this.highPoint_.x,
            this.lowPoint_.x,
            this.centerOpenPt_.x,
            this.centerClosePt_.x) - x,

        Math.max(
            this.openPt_.y,
            this.closePt_.y,
            this.highPoint_.y,
            this.lowPoint_.y,
            this.centerOpenPt_.y,
            this.centerClosePt_.y) - y);

};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 * @private
 */
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.createStartElement = function (svgManager) {
    return svgManager.createPolyline();
};

anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.getStartElementPath = function (svgManager) {
    var path = svgManager.polyLineDraw(
        this.highPoint_.x,
        this.highPoint_.y,
        this.useCrispEdges_);

    path += svgManager.polyLineDraw(
        this.lowPoint_.x,
        this.lowPoint_.y,
        this.useCrispEdges_);

    return path;
};

/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 * @private
 */
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.createOpenElement = function (svgManager) {
    return svgManager.createPolyline();
};
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.getOpenElementPath = function (svgManager) {
    var path = svgManager.polyLineDraw(
        this.openPt_.x,
        this.openPt_.y,
        this.useCrispEdges_);

    path += svgManager.polyLineDraw(
        this.centerOpenPt_.x,
        this.centerOpenPt_.y,
        this.useCrispEdges_);

    return path;
};
/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 * @private
 */
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.createCloseElement = function (svgManager) {
    return svgManager.createPolyline();
};
anychart.plots.axesPlot.series.stock.OHLCPoint.prototype.getCloseElementPath = function (svgManager) {
    var path = svgManager.polyLineDraw(
        this.closePt_.x,
        this.closePt_.y,
        this.useCrispEdges_);

    path += svgManager.polyLineDraw(
        this.centerClosePt_.x,
        this.centerClosePt_.y,
        this.useCrispEdges_);

    return path;
};
//------------------------------------------------------------------------------
//
//                          OHLCSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.stock.StockSeries}
 */
anychart.plots.axesPlot.series.stock.OHLCSeries = function () {
    anychart.plots.axesPlot.series.stock.StockSeries.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.OHLCSeries,
    anychart.plots.axesPlot.series.stock.StockSeries);
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.OHLCSeries.prototype.createPoint = function () {
    return new anychart.plots.axesPlot.series.stock.OHLCPoint();
};
//------------------------------------------------------------------------------
//
//                          OHLCGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings = function () {
    anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings,
    anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings.prototype.createSeries = function (seriesType) {
    var series = new anychart.plots.axesPlot.series.stock.OHLCSeries();
    series.setType(seriesType);
    series.setClusterKey(seriesType);
    return series;
};

anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'ohlc_series';
};

anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'ohlc_style';
};

anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings.prototype.createStyle = function () {
    return new anychart.plots.axesPlot.series.stock.styles.OHLCStyle();
};
//------------------------------------------------------------------------------
//                          Icon
//------------------------------------------------------------------------------
//todo: implement
//------------------------------------------------------------------------------
//
//                          CandlestickPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.CandlestickPoint = function () {
    anychart.plots.axesPlot.series.stock.StockPoint.apply(this);
    this.useCrispEdges_ = true;
};
goog.inherits(anychart.plots.axesPlot.series.stock.CandlestickPoint,
    anychart.plots.axesPlot.series.stock.StockPoint);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.openPt_ = null;
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.closePt_ = null;
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.openCloseRect_ = null;

anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.useCrispEdges_ = null;
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.fillSegment_ = null;
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.hatchFillSegment_ = null;
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.lineSegment_ = null;
//------------------------------------------------------------------------------
//                          Size
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.calculatePixelValues = function () {
    goog.base(this, 'calculatePixelValues');

    this.openCloseRect_ = new anychart.utils.geom.Rectangle();
    this.openPt_ = new anychart.utils.geom.Point();
    this.closePt_ = new anychart.utils.geom.Point();

    var series = this.getSeries();
    var width = this.getWidth();

    series.getValueAxis().transform(this, this.open_,
        this.openPt_);

    series.getValueAxis().transform(this, this.close_,
        this.closePt_);

    series.getArgumentAxis().transform(this, this.getX(),
        this.openPt_, -width);

    series.getArgumentAxis().transform(this, this.getX(),
        this.closePt_, width);


    this.openCloseRect_.x = Math.min(
        this.openPt_.x,
        this.closePt_.x);

    this.openCloseRect_.y = Math.min(
        this.openPt_.y,
        this.closePt_.y);

    this.openCloseRect_.width = Math.max(
        this.openPt_.x,
        this.closePt_.x) - this.openCloseRect_.x;

    this.openCloseRect_.height = Math.max(
        this.openPt_.y,
        this.closePt_.y) - this.openCloseRect_.y;

    if (this.openCloseRect_.height == 0) this.openCloseRect_.height = 1;

    var x = Math.min(
        this.openPt_.x,
        this.closePt_.x,
        this.highPoint_.x,
        this.lowPoint_.x);
    var y = Math.min(
        this.openPt_.y,
        this.closePt_.y,
        this.highPoint_.y,
        this.lowPoint_.y);
    this.bounds_ = new anychart.utils.geom.Rectangle(
        x,
        y,
        Math.max(
            this.openPt_.x,
            this.closePt_.x,
            this.highPoint_.x,
            this.lowPoint_.x) - x,
        Math.max(
            this.openPt_.y,
            this.closePt_.y,
            this.highPoint_.y,
            this.lowPoint_.y) - y
    );
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * <p>
 *      Note: SVG polyline element perform an absolute moveto operation to the first
 *      coordinate pair in the list of points.
 * </p>
 */
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.drawCandlestickLine = function (svgManager) {
    return svgManager.createPolyline();
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @return {String}
 */
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.getCandlestickLinePath = function (svgManager) {
    var path = svgManager.polyLineDraw(
        this.highPoint_.x,
        this.highPoint_.y,
        this.useCrispEdges_);

    path += svgManager.polyLineDraw(
        this.lowPoint_.x,
        this.lowPoint_.y,
        this.useCrispEdges_);

    return path;
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.drawCandlestick = function (svgManager) {
    return svgManager.createSVGElement('rect');
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.series.stock.CandlestickPoint.prototype.getCandlestickBounds = function () {
    return new anychart.utils.geom.Rectangle(
        this.openCloseRect_.x,
        this.openCloseRect_.y,
        this.openCloseRect_.width,
        this.openCloseRect_.height);
};
//------------------------------------------------------------------------------
//
//                          CandlestickSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.CandlestickSeries = function () {
    anychart.plots.axesPlot.series.stock.StockSeries.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.CandlestickSeries,
    anychart.plots.axesPlot.series.stock.StockSeries);
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.CandlestickSeries.prototype.createPoint = function () {
    return new anychart.plots.axesPlot.series.stock.CandlestickPoint();
};
//------------------------------------------------------------------------------
//
//                          CandlestickGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings = function () {
    anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings,
    anychart.plots.axesPlot.series.stock.StockGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                          Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings.prototype.createSeries = function (seriesType) {
    var series = new anychart.plots.axesPlot.series.stock.CandlestickSeries();
    series.setType(seriesType);
    series.setClusterKey(seriesType);
    return series;
};

anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings.prototype.getSettingsNodeName = function () {
    return 'candlestick_series';
};

anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings.prototype.getStyleNodeName = function () {
    return 'candlestick_style';
};

anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings.prototype.createStyle = function () {
    return new anychart.plots.axesPlot.series.stock.styles.CandlestickStyle();
};
//------------------------------------------------------------------------------
//                          Icon
//------------------------------------------------------------------------------
//todo : implement icon


