/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.scales.ValueScale}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.LinearScale}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.TextScale}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.TextDateTimeScale}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.LogScale}</li>
 *  <li>@class {anychart.plots.axesPlot.scales.DateTimeScale}</li>
 * <ul>
 */

goog.provide('anychart.plots.axesPlot.scales');

goog.require('anychart.scales');
goog.require('anychart.plots.axesPlot.scales.dateTime');
goog.require('anychart.locale');
//------------------------------------------------------------------------------
//
//                          ScaleType class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.axesPlot.scales.ScaleType = {
    LOG:0,
    TEXT:1,
    DATE_TIME:2,
    TEXT_DATE_TIME:3,
    LINEAR:4
};
//------------------------------------------------------------------------------
//
//                          ValueScale class
//
//------------------------------------------------------------------------------

/**
 * Class represent value scale.
 * @constructor
 * @extends {anychart.scales.BaseScale}
 */
anychart.plots.axesPlot.scales.ValueScale = function (axis) {
    anychart.scales.BaseScale.apply(this);
    this.axis_ = axis;
    this.isCrossingEnabled_ = false;
    this.setScaleType();
};
//inheritance
goog.inherits(anychart.plots.axesPlot.scales.ValueScale, anychart.scales.BaseScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.scales.ScaleType}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.type_ = null;
/**
 * @return {anychart.plots.axesPlot.scales.ScaleType}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.getType = function () {
    return this.type_;
};
/**
 * Set scale type, #see {anychart.plots.axesPlot.scales.ScaleType}
 * @protected
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.setScaleType = goog.abstractMethod;
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
/**
 * Indicates, is crossing enabled.
 * @private
 * @type {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.axis_ = null;

/**
 * Indicates, is crossing enabled.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.isCrossingEnabled_ = null;
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.isCrossingEnabled = function () {
    return this.isCrossingEnabled_;
};

/**
 * Sets the value where an axis will cross perpendicular axis.
 * <p>
 *  Note: Value should be withing perpendicular axis range.
 * </p>
 * @private
 * @type {string}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.crossingValue_ = null;
/**
 * @param {String} value String crossing value.
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.setCrossingValue = function (value) {
    this.crossingValue_ = value;
};

/**
 * Return crossing value as number value.
 * @return {Number}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.getNumCrossingValue = function () {
    return anychart.utils.deserialization.getNum(this.crossingValue_);
};

/**
 * Return crossing value as string.
 * @return {string}
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.getStringCrossingValue = function () {
    return this.crossingValue_;
};
//------------------------------------------------------------------------------
//
//                          Deserialization
//
//------------------------------------------------------------------------------
/**
 * Deserialize value scale settings.
 * @public
 * @override
 * @param {object} data - JSON object with value scale settings.
 * <p>
 *  Usage sample:
 *  <code>
 *       var scale = new anychart.plots.axesPlot.scales.ValueScale();
 *       scale.deserialize(data);
 *  </code>
 * </p>
 <p>
 * Expected JSON structure:
 * <code>
 *      object {
 *          crossing : string // Sets the value where an axis will cross perpendicular axis,
 *          mode : string // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minimum : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          maximum : number //  Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          major_interval : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minor_interval : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minimum_offset : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          maximum_offset : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          inverted : boolean // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          base_value : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale
 *      }
 * </code>
 * </p>
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.deserialize = function (data) {
    //super class deserialization
    goog.base(this, 'deserialize', data);
    //aliases
    var deserialization = anychart.utils.deserialization;
    if (deserialization.hasProp(data, 'crossing')) {
        this.isCrossingEnabled_ = true;
        this.crossingValue_ = deserialization.getProp(data, 'crossing');
    }
};
/**
 * Return category name, override in TextDateTimeScale.
 * @param category
 */
anychart.plots.axesPlot.scales.ValueScale.prototype.getCategoryName = function (category) {
    return category.getName();
};
//------------------------------------------------------------------------------
//
//                          LinearScale class
//
//------------------------------------------------------------------------------

/**
 * Class represent linear scale.
 * @public
 * @constructor
 * @extends {anychart.plots.axesPlot.scales.ValueScale}
 * <p>
 *  Usage sample:
 *  <code>
 *      var scale = new anychart.plots.axesPlot.scales.LinearScale(axis);
 *      scale.deserialize(data);
 *      scale.calculate();
 *      pixValue = scale.transformValueToPixel(value);
 *      value = scale.transformPixelToValue(pixValue);
 *      alert(scale.contains(value));
 *      alert(scale.getMajorIntervalValue(index));
 *      alert(scale.getMinorIntervalValue(index));
 *  </code>
 * </p>
 */
anychart.plots.axesPlot.scales.LinearScale = function (axis) {
    anychart.plots.axesPlot.scales.ValueScale.call(this, axis);
    this.showZero_ = false;
};

//inheritance
goog.inherits(anychart.plots.axesPlot.scales.LinearScale, anychart.plots.axesPlot.scales.ValueScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.setScaleType = function () {
    this.type_ = anychart.plots.axesPlot.scales.ScaleType.LINEAR;
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Indicates, is label size and count calculated.
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.labelsCalculated_ = false;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.needCalcLabels = true;

//------------------------------------------------------------------------------
//                          Override
//------------------------------------------------------------------------------
anychart.plots.axesPlot.scales.LinearScale.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'auto_calculation_mode')) {
        des.getLStringProp(data, 'auto_calculation_mode') == 'smart' ? this.isSmartMode_ = true : this.isSmartMode_ = false;
    }

    if (des.hasProp(data, 'always_show_zero')) {
        des.getBoolProp(data, 'always_show_zero') ? this.showZero_ = true : this.showZero_ = false;
    }
};

/**
 * @protected
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.checkMajorIntervalLabels = function () {
    if (this.axis_.hasLabels() && !this.axis_.allowLabelsOverlap()) {
        this.calculateBaseValue_();
        this.calculateMajorIntervalCount_();
        var maxLabels = this.axis_.getMaxLabelsCount();
        if (maxLabels <= (this.maximum_ - this.minimum_) / this.majorInterval_)
            this.majorInterval_ = this.getOptimalStepSize_(this.maximum_ - this.minimum_, maxLabels, true);
        this.labelsCalculated_ = true;
    }
};

/**
 * Calculate linear scale property's.
 * @public
 * @override
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.calculate = function () {
    //calculate auto property's
    if (this.isMinimumAuto_ || this.isMaximumAuto_ || this.isMajorIntervalAuto_ || this.isMinorIntervalAuto_) {
        //calculate base property's
        goog.base(this, 'calculate');

        //minimum and maximum auto calculation
        if (this.isMinimumAuto_ || this.isMaximumAuto_) {
            if (Number(this.dataRangeMaximum_ - this.dataRangeMinimum_) == 0 && this.isMinimumAuto_ && this.isMaximumAuto_) {
                this.minimum_ = this.dataRangeMinimum_ - .5;
                this.maximum_ = this.dataRangeMaximum_ + .5;
            }

            //data range can't be equals 0
            if ((this.maximum_ - this.minimum_) < Number.MIN_VALUE) {
                if (this.isMaximumAuto_)
                    this.maximum_ = this.maximum_ + .2 * (this.maximum_ == 0 ? 1 : Math.abs(this.maximum_));
                if (this.isMinimumAuto_)
                    this.minimum_ = this.minimum_ - .2 * (this.minimum_ == 0 ? 1 : Math.abs(this.minimum_));
            }

            //check minimum
            if (this.isMinimumAuto_ && this.minimum_ > 0 && this.minimum_ / (this.maximum_ - this.minimum_) < .25)
                this.minimum_ = 0;

            //check maximum
            if (this.isMaximumAuto_ && this.maximum_ < 0 && Math.abs(this.maximum_ / (this.maximum_ - this.minimum_)) < .25)
                this.maximum_ = 0;

            if (this.showZero_) {
                if (this.isMinimumAuto_ && this.minimum_ > 0) this.minimum_ = 0;
                if (this.isMaximumAuto_ && this.maximum_ < 0) this.maximum_ = 0;
            }
        }

        //calculate major and minor interval's
        if (this.isMajorIntervalAuto_) {
            this.majorInterval_ = this.isSmartMode_ ?
                this.getOptimalSmartStepSize_(this.maximum_ - this.minimum_, 7, false, this.numDecimals_) :
                this.getOptimalStepSize_(this.maximum_ - this.minimum_, 7, false);
            if (this.axis_.hasLabels() && !this.axis_.allowLabelsOverlap()) {
                this.calculateBaseValue_();
                this.calculateMajorIntervalCount_();
                var maxLabels = this.axis_.getMaxLabelsCount();
                if (maxLabels <= (this.maximum_ - this.minimum_) / this.majorInterval_)
                    this.majorInterval_ = this.getOptimalStepSize_(this.maximum_ - this.minimum_, maxLabels, true);
                this.labelsCalculated_ = true;
            }
        }


        if (this.isMinorIntervalAuto_)
            this.minorInterval_ = this.getOptimalStepSize_(this.majorInterval_, 5, true);

        //apply major interval
        if (this.isMinimumAuto_)
            this.minimum_ -= this.safeMod_(this.minimum_, this.majorInterval_);

        if (this.isMaximumAuto_) {
            var distance = this.safeMod_(this.maximum_, this.majorInterval_);
            if (distance != 0)
                this.maximum_ += this.majorInterval_ - distance;
        }
    }
    if (this.needCalcLabels && !this.labelsCalculated_ && this.axis_.hasLabels()) this.axis_.calculateMaxLabelSize();
    this.calculateBaseValue_();
    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};

/**
 * Calculate base value.
 * @override
 * @protected
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.calculateBaseValue_ = function () {
    if (!this.isBaseValueAuto_) return;
    this.baseValue_ = Math.ceil(Number(this.minimum_) / Number(this.majorInterval_) - 0.00000001) * Number(this.majorInterval_);
};
/**
 * Number of decimals.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.numDecimals_ = null;
/**
 * Flag for smart calculating optimal step size algorithm.
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.isSmartMode_ = false;
/**
 *
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.isSmartMode = function () {
    return this.isSmartMode_;
};
/**
 * Enables the smart calculating optimal step size algorithm.
 * @param {Number} numDecimals
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.enableSmartMode = function (numDecimals) {
    if (numDecimals != null) {
        this.isSmartMode_ = true;
        this.numDecimals_ = numDecimals;
    }
};
/**
 * Whether to show zero value on the grid.
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.showZero_ = false;
/**
 * Getter for showZero flag.
 * @return {Boolean}
 */
anychart.plots.axesPlot.scales.LinearScale.prototype.isShowZero = function () {
    return this.showZero_;
};

//------------------------------------------------------------------------------
//
//                          TextScale class
//
//------------------------------------------------------------------------------

/**
 * Class represent text scale.
 * @public
 * @constructor
 * @extends {anychart.plots.axesPlot.scales.ValueScale}
 * <p>
 *  Usage sample:
 *  <code>
 *      var scale = new anychart.plots.axesPlot.scales.TextScale();
 *      scale.deserialize(data);
 *      scale.calculate();
 *      pixValue = scale.transformValueToPixel(value);
 *      value = scale.transformPixelToValue(pixValue);
 *      alert(scale.contains(value));
 *      alert(scale.getMajorIntervalValue(index));
 *      alert(scale.getMinorIntervalValue(index));
 *  </code>
 * </p>
 */
anychart.plots.axesPlot.scales.TextScale = function (axis) {
    anychart.plots.axesPlot.scales.ValueScale.call(this, axis);
};
//inheritance
goog.inherits(anychart.plots.axesPlot.scales.TextScale,
    anychart.plots.axesPlot.scales.ValueScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.TextScale.prototype.setScaleType = function () {
    this.type_ = anychart.plots.axesPlot.scales.ScaleType.TEXT;
};
//------------------------------------------------------------------------------
//                          OVERRIDE
//------------------------------------------------------------------------------

/**
 * Calculate text scale property's.
 * @public
 * @override
 */
anychart.plots.axesPlot.scales.TextScale.prototype.calculate = function () {
    //set base property's
    this.minimum_ = this.dataRangeMinimum_ - .5;
    this.maximum_ = this.dataRangeMaximum_ + .5;
    this.minorInterval_ = 1;
    this.baseValue_ = this.minimum_;
    var isLabelsCalculated = false;
    if (this.isMajorIntervalAuto_) {
        if (this.axis_.hasLabels() && !this.axis_.allowLabelsOverlap()) {
            this.majorInterval_ = 1;
            this.calculateMajorIntervalCount_();
            var maxLabels = this.axis_.getMaxLabelsCount();

            this.majorInterval_ = Math.ceil((this.maximum_ - this.minimum_) / maxLabels);
            isLabelsCalculated = true;
        } else {
            this.majorInterval_ = anychart.utils.deserialization.getInt((this.maximum_ - this.minimum_ - 1) / 12) + 1;
        }
    } else {
        this.majorInterval_ = anychart.utils.deserialization.getInt((this.majorInterval_));
        if (this.majorInterval_ <= 0)
            this.majorInterval_ = 1;
    }
    if (this.isMinorIntervalAuto_)
        this.minorInterval_ = this.majorInterval_;

    if (!isLabelsCalculated && this.axis_.hasLabels())
        this.axis_.calculateMaxLabelSize();

    //calculate other property's
    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};
//------------------------------------------------------------------------------
//
//                          TextDateTimeScale class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.scales.TextScale}
 */
anychart.plots.axesPlot.scales.TextDateTimeScale = function (axis) {
    anychart.plots.axesPlot.scales.TextScale.call(this, axis);
};
goog.inherits(anychart.plots.axesPlot.scales.TextDateTimeScale,
    anychart.plots.axesPlot.scales.TextScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.TextDateTimeScale.prototype.setScaleType = function () {
    this.type_ = anychart.plots.axesPlot.scales.ScaleType.TEXT_DATE_TIME;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.TextDateTimeScale.prototype.deserializeValue = function (value) {
    if (String(value) == '') return NaN;
    return anychart.utils.deserialization.getDateTime(value, this.axis_.getPlot().getDateTimeLocale());
};
//------------------------------------------------------------------------------
//                          Categorization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.TextDateTimeScale.prototype.getCategoryName = function (category) {
    return anychart.utils.deserialization.getDateTime(category.getName(), this.axis_.getPlot().getDateTimeLocale());
};
//------------------------------------------------------------------------------
//                           Tickmarks.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.TextDateTimeScale.prototype.calculateMajorTicksCount = function () {
    goog.base(this, 'calculateMajorTicksCount');

    var r = this.maximum_ - this.baseValue_;
    if (r % this.majorInterval_ != 0) {
        var lastVal = this.getMajorIntervalValue(this.majorIntervalCount_);
        if (lastVal >= this.minimum_ && lastVal <= this.maximum_)
            this.majorIntervalCount_++;
    }
};
//------------------------------------------------------------------------------
//
//                          LogScale class
//
//------------------------------------------------------------------------------

/**
 * Class represent logarithmic scale.
 * @public
 * @constructor
 * @param {anychart.plots.axesPlot.axes.Axis} axis
 * @extends {anychart.plots.axesPlot.scales.ValueScale}
 * <p>
 *  Usage sample:
 *  <code>
 *      var scale = new anychart.plots.axesPlot.scales.LogScale(axis);
 *      scale.deserialize(data);
 *      scale.calculate();
 *      pixValue = scale.transformValueToPixel(value);
 *      value = scale.transformPixelToValue(pixValue);
 *      alert(scale.contains(value));
 *      alert(scale.getMajorIntervalValue(index));
 *      alert(scale.getMinorIntervalValue(index));
 *  </code>
 * </p>
 */
anychart.plots.axesPlot.scales.LogScale = function (axis) {
    anychart.plots.axesPlot.scales.ValueScale.call(this, axis);
};

//inheritance
goog.inherits(anychart.plots.axesPlot.scales.LogScale, anychart.plots.axesPlot.scales.ValueScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.LogScale.prototype.setScaleType = function () {
    this.type_ = anychart.plots.axesPlot.scales.ScaleType.LOG;
};
//------------------------------------------------------------------------------
//                          PROPERTY'S
//------------------------------------------------------------------------------
/**
 * Minor logarithmic values.
 * @public
 * @static
 * @type {Array.<number>}
 */
anychart.plots.axesPlot.scales.LogScale.MINOR_LOG_VALUES = [
    0, 0.301029995663981, 0.477121254719662, 0.602059991327962, 0.698970004336019,
    0.778151250383644, 0.845098040014257, 0.903089986991944, 0.954242509439325, 1
];

/**
 * ??
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.transformedMin_ = null;

/**
 * ??
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.transformedMax_ = null;

/**
 * Logarithm base value. Effective when scale type is set to logarithmic.
 * @private
 * @type {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.logBase_ = 10;
//------------------------------------------------------------------------------
//                          OVERRIDE
//------------------------------------------------------------------------------
/**
 * Deserialize logarithmic scale settings.
 * @public
 * @override
 * @param {object} data - JSON object with logarithmic scale settings.
 * <p>
 *  Usage sample:
 *  <code>
 *       var scale = new anychart.plots.axesPlot.scales.ValueScale();
 *       scale.deserialize(data);
 *  </code>
 * </p>
 <p>
 * Expected JSON structure:
 * <code>
 *      object {
 *          log_base : number || string // Logarithm base value. Effective when scale type is set to logarithmic.
 *          crossing : string // Deserialize by anychart.plots.axesPlot.scales.ValueScale,
 *          mode : string // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minimum : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          maximum : number //  Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          major_interval : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minor_interval : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          minimum_offset : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          maximum_offset : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          inverted : boolean // Deserialize by anychart.plots.axesPlot.scales.BaseScale,
 *          base_value : number // Deserialize by anychart.plots.axesPlot.scales.BaseScale
 *      }
 * </code>
 * </p>
 */
anychart.plots.axesPlot.scales.LogScale.prototype.deserialize = function (data) {
    if (data == null) return;
    //super class deserialization
    goog.base(this, 'deserialize', data);
    //aliases
    var deserialization = anychart.utils.deserialization;
    //Backward compatibility support with old version of attribute name
    var logPropName = (deserialization.hasProp(data, 'logarithmic_base')) ? 'logarithmic_base' : 'log_base';

    if (deserialization.hasProp(data, logPropName)) {
        var logBase = deserialization.getLString(deserialization.getProp(data, logPropName));
        if (logBase == 'e')
            this.logBase_ = Math.E;
        else
            this.logBase_ = deserialization.getNum(logBase);
    }
};
/**
 * Calculate logarithmic scale property's.
 * @public
 * @override
 */
anychart.plots.axesPlot.scales.LogScale.prototype.calculate = function () {
    //minimum and maximum auto calculation
    if (isNaN(this.dataRangeMinimum_) && isNaN(this.dataRangeMaximum_) && this.isMinimumAuto_ && this.isMaximumAuto_) {
        this.isMinimumAuto_ = false;
        this.minimum_ = 1;
        this.isMaximumAuto_ = false;
        this.maximum_ = 10;
    }
    //calculate base property's
    goog.base(this, 'calculate');

    //auto calculate major interval
    if (this.isMajorIntervalAuto_)
        this.majorInterval_ = 1;

    //check minimum and maximum values
    if (this.minimum_ <= 0 && this.maximum_ <= 0) {
        this.minimum_ = 1;
        this.maximum_ = this.logBase_;
    } else if (this.minimum < 0) {
        this.minimum = this.maximum / this.logBase_; // warn - we can lost data here
    } else if (this.minimum == 0) {
        this.minimum = 1;
    } else if (this.maximum_ <= 0.0) {
        this.maximum_ = this.minimum_ * this.logBase_;
    }
    if (this.maximum_ - this.minimum_ < 1.0e-20) {
        if (this.isMaximumAuto_)
            this.maximum_ *= 2;
        if (this.isMinimumAuto_)
            this.minimum_ /= 2;
    }
    if (this.isMinimumAuto_)
        this.minimum_ = Math.pow(this.logBase_, Math.floor(Math.log(this.minimum_) / Math.log(this.logBase_)));
    if (this.isMaximumAuto_)
        this.maximum_ = Math.pow(this.logBase_, Math.ceil(Math.log(this.maximum_) / Math.log(this.logBase_)));

    this.transformedMin_ = Math.round(this.safeLog_(this.minimum_) * 1000) / 1000;
    this.transformedMax_ = Math.round(this.safeLog_(this.maximum_) * 1000) / 1000;

    //calculate other property's
    if (this.axis_.hasLabels()) this.axis_.calculateMaxLabelSize();
    this.calculateBaseValue_();
    this.calculateIntervalCount_();
    this.calculateMinorStartInterval_();
};

/**
 * Calculate minor start interval.
 * @protected
 * @override
 */
anychart.plots.axesPlot.scales.LogScale.prototype.calculateMinorStartInterval_ = function () {
    this.minorStartInterval_ = -9;
};

/**
 * Calculate and return current major interval value.
 * @public
 * @override
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.getMajorIntervalValue = function (index) {
    return this.baseValue_ + index;
};

anychart.plots.axesPlot.scales.LogScale.prototype.getAxisValue = function (index) {
    return Math.pow(this.logBase_, this.getMajorIntervalValue(index))
};

/**
 * Calculate and return current minor interval value.
 * @public
 * @override
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.getMinorIntervalValue = function (baseValue, index) {
    return baseValue + Math.floor(Number(index) / 9.0) + anychart.plots.axesPlot.scales.LogScale.MINOR_LOG_VALUES[(index + 9) % 9];
};

/**
 * @public
 * @override
 * @param value
 * @param isZeroAtAxisZero
 */
anychart.plots.axesPlot.scales.LogScale.prototype.localTransform = function (value, isZeroAtAxisZero) {
    var ratio = (value - this.transformedMin_) / ( this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;

    var pixMax = isZeroAtAxisZero ? (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) : this.pixelRangeMaximum_;
    var pixMin = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum_;

    return (this.inverted_) ? (pixMax - pixelOffset) : (pixMin + pixelOffset);
};

/**
 * Transform and return value in pixels to internal scale value.
 * @public
 * @override
 * @param {Number} pixValue value in pixels.
 * @return {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.transformPixelToValue = function (pixValue) {
    var ratio = (pixValue - this.pixelRangeMinimum_) / (this.pixelRangeMaximum_ - this.pixelRangeMinimum_);
    var offset = (this.transformedMax_ - this.transformedMin_) * ratio;

    var pixMax = this.transformedMax_;
    var pixMin = this.transformedMin_;

    var res = this.inverted_ ? (pixMax - offset) : (pixMin + offset);

    if (res < this.transformedMin_) res = this.transformedMin_;
    else if (res > this.transformedMax_) res = this.transformedMax_;

    return this.delinearize(res);
};

/**
 * Transform from internal scale value to pixel value.
 * @public
 * @override
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.transformValueToPixel = function (value) {
    var ratio = (this.safeLog_(value) - this.transformedMin_) / (this.transformedMax_ - this.transformedMin_);
    var pixelOffset = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;
    if (this.inverted_) return this.pixelRangeMaximum_ - pixelOffset;
    return this.pixelRangeMinimum_ + pixelOffset;
};

/**
 * Sets base value to 1, if not auto.
 * @protected
 * @override
 */
anychart.plots.axesPlot.scales.LogScale.prototype.calculateBaseValue_ = function () {
    if (!this.isBaseValueAuto_) return;
    this.baseValue_ = Math.ceil(this.safeLog_(this.minimum_) - 0.00000001);
};

/**
 * Calculate minor and minor interval count.
 * @protected
 * @override
 */
anychart.plots.axesPlot.scales.LogScale.prototype.calculateIntervalCount_ = function () {
    this.majorIntervalCount_ = parseInt(Math.floor(this.safeLog_(this.maximum_) + 1.0e-12)) - parseInt(this.baseValue_);
    if (this.getMajorIntervalValue(this.majorIntervalCount_) > this.safeLog_(this.maximum_))
        this.majorIntervalCount_--;
    if (this.majorIntervalCount_ < 1)
        this.majorIntervalCount_ = 1;
    this.minorIntervalCount_ = 9;
};

//------------------------------------------------------------------------------
//                         METHODS
//------------------------------------------------------------------------------
/**
 * Safe log.
 * @private
 * @param {Number} value Value.
 */
anychart.plots.axesPlot.scales.LogScale.prototype.safeLog_ = function (value) {
    return (value > 1.0e-20) ? parseFloat((Math.log(value) / Math.log(this.logBase_)).toPrecision(14)) : 0;
};

/**
 * Indicates, contains passed value in linearized scale range.
 * @public
 * @override
 * @param {Number} value Target value
 * @return {boolean}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.linearizedContains = function (value) {
    return value >= this.transformedMin_ && value <= this.transformedMax_;
};

/**
 * Delinearize and return passed value.
 * @public
 * @param {Number} value Target value.
 * @return {Number}
 */
anychart.plots.axesPlot.scales.LogScale.prototype.delinearize = function (value) {
    return Math.pow(this.logBase_, value);
};
//------------------------------------------------------------------------------
//
//                          DateTimeScale class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.scales.ValueScale}
 */
anychart.plots.axesPlot.scales.DateTimeScale = function (axis) {
    anychart.plots.axesPlot.scales.ValueScale.call(this, axis);
    this.dateTimeRange_ = new anychart.plots.axesPlot.scales.dateTime.DateTimeRange();
    this.majorIntervalUnit_ = 0;
    this.minorIntervalUnit_ = 0;
};
goog.inherits(anychart.plots.axesPlot.scales.DateTimeScale,
    anychart.plots.axesPlot.scales.ValueScale);
//------------------------------------------------------------------------------
//                          ScaleType.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.setScaleType = function () {
    this.type_ = anychart.plots.axesPlot.scales.ScaleType.DATE_TIME;
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.majorIntervalUnit_ = null;
/**
 * @param {int}
    */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.setMajorIntervalUnit = function (value) {
    this.majorIntervalUnit_ = value;
};
/**
 * @private
 * @type {int}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.minorIntervalUnit_ = null;
/**
 * @param {int} value
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.setMinorIntervalUnit = function (value) {
    this.minorIntervalUnit_ = value;
};
/**
 * @private
 * @type {anychart.plots.axesPlot.scales.dateTime.DateTimeRange}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.dateTimeRange_ = null;
//------------------------------------------------------------------------------
//                Getters/Setters for BaseScale properties.
//------------------------------------------------------------------------------
/**
 * @param {Number} value
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.setMinorInterval = function (value) {
    this.minorInterval_ = value;
};
/**
 * @return {Number}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.getMinorInterval = function () {
    return this.minorInterval_;
};
//------------------------------------------------------------------------------
//                          Deserialize.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.scales.DateTimeScale.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    if (!data) return;

    var des = anychart.utils.deserialization;
    var unit;

    if (des.hasProp(data, 'major_interval_unit')) {
        unit = this.deserializeUnit_(des.getProp(data, 'major_interval_unit'));
        if (unit != -1)
            this.majorIntervalUnit_ = unit;
    }

    if (des.hasProp(data, 'minor_interval_unit')) {
        unit = this.deserializeUnit_(des.getProp(data, 'minor_interval_unit'));
        if (unit != -1)
            this.minorIntervalUnit_ = unit;
    }
};
/**
 * @private
 * @param {Object} data
 * @return {int}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.deserializeUnit_ = function (data) {
    switch (anychart.utils.deserialization.getEnumItem(data)) {
        case 'year':
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR;
        case 'month' :
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH;
        case 'day':
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY;
        case 'hour':
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR;
        case 'minute':
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE;
        case 'second':
            return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND;
    }
    return -1;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.deserializeValue = function (value) {
    if (String(value) === '') return NaN;
    return anychart.utils.deserialization.getDateTime(value, this.axis_.getPlot().getDateTimeLocale());
};
//------------------------------------------------------------------------------
//                            Calculation.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calculate = function () {
    goog.base(this, 'calculate');
    var isLabelsCalculated = false;

    if (this.isMajorIntervalAuto_) {
        this.majorInterval_ = this.dateTimeRange_.calculateIntervals(this, 7);
        if (this.axis_.getLabels() && !this.axis_.getLabels().allowOverlap()) {
            this.calculateBaseValue_();
            this.calculateMajorTicksCount();
            var maxLabels = this.axis_.getLabels().calcMaxLabelsCount(this.axis_.getSVGManager());
            if (maxLabels <= (this.maximum_ - this.minimum_) / this.majorInterval_) {
                this.majorInterval_ = this.dateTimeRange_.calculateIntervals(this, maxLabels);
            }
            isLabelsCalculated = true;
        }
    }

    if (this.isMaximumAuto_)
        this.maximum_ = this.getEvenStepDate_(this.maximum_, true);

    if (this.isMinimumAuto_)
        this.minimum_ = this.getEvenStepDate_(this.minimum_, false);

    this.calculateBaseValue_();
    this.calculateTicksCount();
    this.calculateMinorStartInterval_();

    if (!isLabelsCalculated && this.axis_.getLabels())
        this.axis_.getLabels().initMaxLabelSize(this.axis_.getSVGManager());
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calculateMinorStartInterval_ = function () {
    var tmp = this.minimum_ - this.baseValue_;

    switch (this.minorIntervalUnit_) {
        default:
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR:
            this.minorStartInterval_ = tmp / (365 * this.minorInterval_);
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH:
            this.minorStartInterval_ = tmp / (28 * this.minorInterval_);
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY:
            this.minorStartInterval_ = tmp / this.minorInterval_;
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR:
            this.minorStartInterval_ = tmp * 24 / this.minorInterval_;
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE:
            this.minorStartInterval_ = tmp * (24 * 60) / this.minorInterval_;
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND:
            this.minorStartInterval_ = tmp * (24 * 60 * 60) / this.minorInterval_;
            break;
    }
};
/**
 * @private
 * @param {Number} date
 * @param {Boolean} increase
 * @return {Number}
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.getEvenStepDate_ = function (date, increase) {
    return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.getEvenStepDate(this.majorIntervalUnit_, date, increase);
};
/**
 * @param range
 * @param steps
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calcStepSize = function (range, steps) {
    this.getOptimalSmartStepSize_(range, steps, false);
};

/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.getMajorIntervalValue = function (index) {
    return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.increaseDate(this.baseValue_, this.majorIntervalUnit_, index * this.majorInterval_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.getMinorIntervalValue = function (baseValue, index) {
    return anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.increaseDate(baseValue, this.minorIntervalUnit_, index * this.minorInterval_);
};
//------------------------------------------------------------------------------
//                              BaseValue.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calculateBaseValue_ = function () {
    if (!this.isBaseValueAuto_) return;

    var minDate = new Date(this.minimum_);
    var date;

    switch (this.majorIntervalUnit_) {
        default:
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR:
            date = new Date(Date.UTC(minDate.getUTCFullYear(), 0));
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH:
            date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth()));
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY:
            date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate()));
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR:
            date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate(), minDate.getUTCHours()));
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE:
            date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate(), minDate.getUTCHours(), minDate.getUTCMinutes()));
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND:
            date = minDate;
            break;
    }

    if (date.getTime() < minDate.getTime())
        this.baseValue_ = anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.increaseDate(date.getTime(), this.majorIntervalUnit_, 1);
    else
        this.baseValue_ = date.getTime();
};
//------------------------------------------------------------------------------
//                          Tickmarks count.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calculateTicksCount = function () {
    this.calculateMajorTicksCount();

    this.minorIntervalCount_ = 0;
    var base = this.getMajorIntervalValue(0);
    var end = this.getMajorIntervalValue(1);
    var temp = this.getMinorIntervalValue(base, 0);
    while (temp < end) {
        this.minorIntervalCount_++;
        temp = this.getMinorIntervalValue(base, this.minorIntervalCount_);
    }
    this.minorIntervalCount_ = Math.max(0, this.minorIntervalCount_);
};
/**
 * @protected
 */
anychart.plots.axesPlot.scales.DateTimeScale.prototype.calculateMajorTicksCount = function () {
    var minDate = new Date(this.minimum_);
    var maxDate = new Date(this.maximum_);

    var tmp = this.majorInterval_;
    var ticks = 1;

    var dYear = maxDate.getUTCFullYear() - minDate.getUTCFullYear();

    switch (this.majorIntervalUnit_) {
        default:
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.YEAR:
            ticks = dYear / tmp;
            break;
        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MONTH:
            ticks = (maxDate.getUTCMonth() - minDate.getUTCMonth() + 12 * dYear) / tmp;
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.DAY:
            ticks = (this.maximum_ - this.minimum_) / (tmp * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_DAY);
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.HOUR:
            ticks = (this.maximum_ - this.minimum_) / (tmp * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_HOUR);
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MINUTE:
            ticks = (this.maximum_ - this.minimum_) / (tmp * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_MINUTE);
            break;

        case anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.SECOND:
            ticks = (this.maximum_ - this.minimum_) / (tmp * anychart.plots.axesPlot.scales.dateTime.DateTimeUnit.MILISECONDS_PER_SECOND);
            break;
    }

    if (ticks < 1) ticks = 1;

    this.majorIntervalCount_ = Math.floor(ticks);
};
