/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.CategoryInfo};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.StackSettings};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.ClusterSettings};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.Category}.</li>
 * <ul>
 */

//namespace
goog.provide('anychart.plots.axesPlot.axes.categorization');

goog.require('anychart.plots.seriesPlot.categorization');
goog.require('anychart.plots.axesPlot.axes');

//------------------------------------------------------------------------------
//
//                          CategoryInfo class
//
//------------------------------------------------------------------------------

/**
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.categorization.BaseCategoryInfo}
 */
anychart.plots.axesPlot.axes.categorization.CategoryInfo = function (plot) {
    anychart.plots.seriesPlot.categorization.BaseCategoryInfo.call(this, plot);
};

//inheritance
goog.inherits(anychart.plots.axesPlot.axes.categorization.CategoryInfo, anychart.plots.seriesPlot.categorization.BaseCategoryInfo);

anychart.plots.axesPlot.axes.categorization.CategoryInfo.prototype.axis_ = null;
anychart.plots.axesPlot.axes.categorization.CategoryInfo.prototype.setAxis = function (value) {
    this.axis_ = value;
};
//------------------------------------------------------------------------------
//
//                          ClusterSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.ClusterSettings = function () {
    this.axes_ = {};
};

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.groupPadding_ = NaN;
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.setGroupPadding = function (value) {
    this.groupPadding_ = value;
};
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.pointPadding_ = NaN;
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.setPointPadding = function (value) {
    this.pointPadding_ = value;
};

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.numClusters_ = 0;
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.setNumClusters = function (value) {
    this.numClusters_ = value;
};
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.getNumClusters = function () {
    return this.numClusters_;
};
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.axes_ = null;

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.clusterWidth_ = NaN;
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.getClusterWidth = function () {
    return this.clusterWidth_;
};
anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.categoryWidth_ = NaN;

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.calculate = function (categoryWidth) {
    this.categoryWidth_ = categoryWidth;
    this.clusterWidth_ = categoryWidth / (this.groupPadding_ + this.numClusters_ + this.pointPadding_ * (this.numClusters_ - 1));
};

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.getOffset = function (index) {
    var offset = this.clusterWidth_ * (index * (1 + this.pointPadding_) + .5 + this.groupPadding_ / 2);
    return offset - this.categoryWidth_ / 2;
};

anychart.plots.axesPlot.axes.categorization.ClusterSettings.prototype.checkAxis = function (axis) {
    var index = this.numClusters_;

    if ((axis.getType() != anychart.plots.axesPlot.axes.AxisType.VALUE) || axis.getScale().getMode() == anychart.scales.ScaleMode.NORMAL) {
        this.numClusters_++;
        return index;
    }
    if (this.axes_[axis.getName()] != null)
        return this.axes_[axis.getName()];
    this.axes_[axis.getName()] = index;
    this.numClusters_++;
    return index;
};

//-----------------------------------------------------------------------------------
//
//                          StackSettings class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.StackSettings = function (isPercent) {
    this.isPercent_ = isPercent;
    this.positivePoints_ = [];
    this.negativePoints_ = [];
    this.points_ = [];
};

anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.isPercent_ = false;
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.positiveStackSumm_ = 0;
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.negativeStackSumm_ = 0;
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.positivePoints_ = null;
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.negativePoints_ = null;
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.points_ = null;

anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.getPrevious = function (value) {
    if (value < 0) return this.negativeStackSumm_;
    return this.positiveStackSumm_;
};

anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.addValue = function (value) {
    if (value >= 0) {
        this.positiveStackSumm_ += value;
        return this.positiveStackSumm_;
    }
    this.negativeStackSumm_ += value;
    return this.negativeStackSumm_;
};

anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.addPoint = function (pt, value) {
    var isPrevPositive = true;
    if (value == 0 && this.points_.length > 0) {
        var prevIndex = this.points_.length - 1;
        var prevPt = this.points_[prevIndex];
        while (prevPt.stackValue_ == 0 && prevIndex > 0) {
            prevIndex--;
            prevPt = this.points_[prevIndex];
        }
        isPrevPositive = (prevPt.stackValue_ >= 0);
    }
    this.points_.push(pt);
    var isPositive = value > 0;
    if (value == 0)
        isPositive = isPrevPositive;

    if (isPositive) {
        if (this.positivePoints_.length > 0)
            pt.setPreviousStackPoint(this.positivePoints_[this.positivePoints_.length - 1]);
        this.positivePoints_.push(pt);
        this.positiveStackSumm_ += value;
        return this.positiveStackSumm_;
    }
    if (this.negativePoints_.length > 0)
        pt.setPreviousStackPoint(this.negativePoints_[this.negativePoints_.length - 1]);

    this.negativePoints_.push(pt);
    this.negativeStackSumm_ += value;
    return this.negativeStackSumm_;
};

anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.setPercent = function () {
    var i;
    for (i = 0; i < this.positivePoints_.length; i++)
        this.positivePoints_[i].setStackMax(this.positiveStackSumm_);
    for (i = 0; i < this.negativePoints_.length; i++)
        this.negativePoints_[i].setStackMax(this.negativeStackSumm_);
};
/**
 * Define, is passed point last in stack.
 * @param {anychart.plots.axesPlot.data.StackablePoint} point
 */
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.isLastInStack = function (point) {
    if (this.negativePoints_.length > 0 && this.negativePoints_[this.negativePoints_.length - 1] == point) return true;
    return this.positivePoints_.length > 0 && this.positivePoints_[this.positivePoints_.length - 1] == point;
};
/**
 * @param {Number} value
 * @return {Number}
 */
anychart.plots.axesPlot.axes.categorization.StackSettings.prototype.getStackSum = function (value) {
    return this.isPercent_ ? 100 : ((value > 0) ? this.positiveStackSumm_ : this.negativeStackSumm_);
};
//-----------------------------------------------------------------------------------
//
//                          Category class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.Category = function (plot, index, name, isArgument, axis) {
    anychart.plots.axesPlot.axes.categorization.CategoryInfo.call(this, plot);
    this.index_ = index;
    this.name_ = name;
    this.isArgument_ = isArgument;
    this.axis_ = axis;

    this.sortedOverlays_ = {};
    this.sortedOverlaysList_ = [];

    this.stackSettings_ = {};
    this.percentStackSettingsList_ = [];

    this.overlays_ = {};
};

//inheritance
goog.inherits(anychart.plots.axesPlot.axes.categorization.Category, anychart.plots.axesPlot.axes.categorization.CategoryInfo);

anychart.plots.axesPlot.axes.categorization.Category.prototype.isArgument_ = true;

anychart.plots.axesPlot.axes.categorization.Category.prototype.clusterSettings_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.setClusterSettings = function (value) {
    this.clusterSettings_ = value;
};
anychart.plots.axesPlot.axes.categorization.Category.prototype.getClusterSettings = function () {
    return this.clusterSettings_;
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.sortedOverlays_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.sortedOverlaysList_ = null;

anychart.plots.axesPlot.axes.categorization.Category.prototype.stackSettings_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.percentStackSettingsList_ = null;

anychart.plots.axesPlot.axes.categorization.Category.prototype.overlays_ = null;

anychart.plots.axesPlot.axes.categorization.Category.prototype.axisLabelFormattedText_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.setAxisLabelFormattedText = function (value) {
    this.axisLabelFormattedText_ = value;
};
anychart.plots.axesPlot.axes.categorization.Category.prototype.getAxisLabelFormattedText = function () {
    return this.axisLabelFormattedText_;
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.axisLabelBounds_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.setAxisLabelBounds = function (value) {
    this.axisLabelBounds_ = value;
};
anychart.plots.axesPlot.axes.categorization.Category.prototype.getAxisLabelBounds = function () {
    return this.axisLabelBounds_;
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.axisLabelNonRotatedBounds_ = null;
anychart.plots.axesPlot.axes.categorization.Category.prototype.setAxisLabelNonRotatedBounds = function (value) {
    this.axisLabelNonRotatedBounds_ = value;
};
anychart.plots.axesPlot.axes.categorization.Category.prototype.getAxisLabelNonRotatedBounds = function () {
    return this.axisLabelNonRotatedBounds_;
};

// ordering
anychart.plots.axesPlot.axes.categorization.Category.prototype.getSortedOverlay = function (axisName, seriesType) {
    if (this.sortedOverlays_[axisName] == null) this.sortedOverlays_[axisName] = {};
    if (this.sortedOverlays_[axisName][seriesType] == null) {
        this.sortedOverlays_[axisName][seriesType] = [];
        this.sortedOverlaysList_.push(this.sortedOverlays_[axisName][seriesType]);
    }
    return this.sortedOverlays_[axisName][seriesType];
};

// stack settings
anychart.plots.axesPlot.axes.categorization.Category.prototype.getStackSettings = function (axisName, seriesType, isPercent) {
    if (this.stackSettings_[axisName] == null)
        this.stackSettings_[axisName] = {};
    if (this.stackSettings_[axisName][seriesType] == null) {
        this.stackSettings_[axisName][seriesType] = new anychart.plots.axesPlot.axes.categorization.StackSettings(isPercent);
        if (isPercent)
            this.percentStackSettingsList_.push(this.stackSettings_[axisName][seriesType]);
    }

    return this.stackSettings_[axisName][seriesType];
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.getOveralysCount = function (axisName, seriesType) {
    this.checkOverlays_(axisName, seriesType);
    return this.overlays_[axisName][seriesType];
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.setOverlaysCount = function (axisName, seriesType, count) {
    this.checkOverlays_(axisName, seriesType);
    this.overlays_[axisName][seriesType] = count;
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.checkOverlays_ = function (axisName, seriesType) {
    if (this.overlays_[axisName] == null) this.overlays_[axisName] = {};
    if (this.overlays_[axisName][seriesType] == null) this.overlays_[axisName][seriesType] = 0;
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.onAfterDeserializeData = function (plotDrawingPoints) {
    //TODO: cluster, cateogry, etc..

    for (var i = 0; i < this.percentStackSettingsList_.length; i++)
        this.percentStackSettingsList_[i].setPercent();
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.isStackSettingsExists = function (axisName, seriesType) {
    return this.stackSettings_ && this.stackSettings_[axisName] && this.stackSettings_[axisName][seriesType];
};

anychart.plots.axesPlot.axes.categorization.Category.prototype.getCategoryTokenValue = function (token) {
    return 0;
};
