/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.spline.SplinePoint}</li>.
 *  <li>@class {anychart.plots.axesPlot.series.spline.SplineSeries}</li>.
 *  <li>@class {anychart.plots.axesPlot.series.spline.SplineGlobalSeriesPoint}</li>.
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.spline');
goog.require('anychart.plots.axesPlot.series.spline.styles');
goog.require('anychart.plots.axesPlot.series.line');
//------------------------------------------------------------------------------
//
//                         SplinePoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LinePoint}
 */
anychart.plots.axesPlot.series.spline.SplinePoint = function() {
    anychart.plots.axesPlot.series.line.LinePoint.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.SplinePoint,
        anychart.plots.axesPlot.series.line.LinePoint);


//------------------------------------------------------------------------------
//                    Methods
//------------------------------------------------------------------------------
/**
 * States that the point has previous point.
 * @return {Boolean} Has or not.
 */
anychart.plots.axesPlot.series.spline.SplinePoint.prototype.hasPrev = function() {
    return this.hasPrev_;
};
/**
 * States that the point has next point.
 * @return {Boolean} Has or not.
 */
anychart.plots.axesPlot.series.spline.SplinePoint.prototype.hasNext = function() {
    return this.hasNext_;
};

anychart.plots.axesPlot.series.spline.SplinePoint.prototype.resize = function() {
    this.calcBounds();
    this.resizeElements();
};
//------------------------------------------------------------------------------
//
//                         SplineSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineSeries}
 */
anychart.plots.axesPlot.series.spline.SplineSeries = function() {
    anychart.plots.axesPlot.series.line.LineSeries.call(this);
    this.drawingInfo_ = [];
    this.splineDrawer_ = new anychart.plots.axesPlot.series.spline.SplineDrawer(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.SplineSeries,
        anychart.plots.axesPlot.series.line.LineSeries);
//------------------------------------------------------------------------------
//                    Properties
//------------------------------------------------------------------------------
/**
 * Object containing points and their control points for drawing bezier curve.
 * @type {Array.<anychart.utils.geom.Point>}
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.drawingInfo_ = null;
/**
 * Getter for drawing info.
 * @return {Array.<anychart.utils.geom.Point>} Drawing info.
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.getDrawingInfo = function() {
    return this.drawingInfo_;
};
/**
 * @type {anychart.plots.axesPlot.series.spline.SplineDrawer}
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.splineDrawer_ = null;
/**
 * Getter for spline drawer.
 * @return {anychart.plots.axesPlot.series.spline.SplineDrawer} Spline drawer.
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.getSplineDrawer = function() {
    return this.splineDrawer_;
};

/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.onBeforeDraw = function() {
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.spline.SplineSeries.prototype.onBeforeResize = function() {
    this.drawingInfo_ = [];
    this.splineDrawer_.resetState();
};

//------------------------------------------------------------------------------
//
//                         SplineDrawer class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.plots.axesPlot.series.spline.SplineSeries} series Series.
 */
anychart.plots.axesPlot.series.spline.SplineDrawer = function(series) {
    this.state_ = 0;

    this.tangentLengthPercent_ = 0.5;

    this.index_ = 0;
    this.series_ = series;
};
anychart.plots.axesPlot.series.spline.SplineDrawer.prototype.resetState = function() {
    this.state_ = 0;

    this.index_ = 0;
};
/**
 * Adds a new point for calculating control points to draw.
 * @param {Number} x x coordinate of point.
 * @param {Number} y y coordinate of point.
 */
anychart.plots.axesPlot.series.spline.SplineDrawer.prototype.processNewItem = function(x, y) {
    switch (this.state_) {
        case 3:
            if (this.x2_ == x && this.y2_ == y) break;
            this.drawNextQueuedPoint(this.x1_, this.y1_, this.x2_, this.y2_, x, y);
            this.x1_ = this.x2_;
            this.y1_ = this.y2_;
            this.x2_ = x;
            this.y2_ = y;
            break;
        case 2:
            if (this.x2_ == x && this.y2_ == y) break;
//            this.startQueuedDrawing(this.x1_, this.y1_, this.x2_, this.y2_, x, y);
            this.x1_ = this.x2_;
            this.y1_ = this.y2_;
            this.x2_ = x;
            this.y2_ = y;
            this.state_ = 3;
            break;
        case 1:
            if (this.x1_ == x && this.y1_ == y) break;
            this.startQueuedDrawing(this.x1_, this.y1_, x, y);
            this.x2_ = x;
            this.y2_ = y;
            this.state_ = 2;
            break;
        case 0:
            this.x1_ = x;
            this.y1_ = y;
            this.state_ = 1;
            break;
    }
};

/**
 * Starts calculating for drawing spline.
 */
anychart.plots.axesPlot.series.spline.SplineDrawer.prototype.startQueuedDrawing = function(p1x, p1y, p2x, p2y, p3x, p3y) {
    var cp1 = new anychart.utils.geom.Point();
    cp1.x = p2x - (p2x - p1x) * this.tangentLengthPercent_;
    cp1.y = p1y;

    this.series_.drawingInfo_.push(cp1);
    this.index_++;
};

/**
 * Calculating control points for next spline curve drawing.
 */
anychart.plots.axesPlot.series.spline.SplineDrawer.prototype.drawNextQueuedPoint = function(p1x, p1y, p2x, p2y, p3x, p3y) {
    var cp1 = new anychart.utils.geom.Point();
    cp1.x = p1x + (p2x - p1x) * this.tangentLengthPercent_;
    cp1.y = p2y;

    var cp2 = new anychart.utils.geom.Point();
    cp2.x = p3x - (p3x - p2x) * this.tangentLengthPercent_;
    cp2.y = p2y;

    var mp = new anychart.utils.geom.Point();
    mp.x = p2x;
    mp.y = p2y;

    this.series_.drawingInfo_.push([cp1, cp2, mp]);
    this.index_++;
};
/**
 * Finalizes calculating for spline drawing.
 */
anychart.plots.axesPlot.series.spline.SplineDrawer.prototype.finalizeDrawing = function() {
    var cp1 = new anychart.utils.geom.Point();
    cp1.x = this.x1_ + (this.x2_ - this.x1_) * this.tangentLengthPercent_;
    cp1.y = this.y2_;
    this.series_.drawingInfo_.push(cp1);
};

//------------------------------------------------------------------------------
//
//                         SplineGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings}
 */
anychart.plots.axesPlot.series.spline.SplineGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.spline.SplineGlobalSeriesSettings,
        anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings);

anychart.plots.axesPlot.series.spline.SplineGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series;
    switch (seriesType) {
        case anychart.series.SeriesType.SPLINE:
            series = new anychart.plots.axesPlot.series.spline.SplineSeries();
            break;
    }
    series.setType(seriesType);
    series.setClusterKey(anychart.series.SeriesType.SPLINE);
    return series;
};

anychart.plots.axesPlot.series.spline.SplineGlobalSeriesSettings.prototype.createStyle = function() {
    return new anychart.plots.axesPlot.series.spline.styles.SplineStyle();
};
