/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.OHLCStyle}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes}</li>
 *  <li>@class {anychart.plots.axesPlot.series.stock.styles.CandlestickStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.stock.styles');
goog.require('anychart.styles');
//------------------------------------------------------------------------------
//
//                          StockLineStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.definerMethod_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.getMethod_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.updateMethod_ = null;
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.initialize =
        function(point, styleShapes, drawingMethod, definerMethod, getMethod, updateMethod) {
            goog.base(this, 'initialize', point, styleShapes, drawingMethod);
            this.definerMethod_ = definerMethod;
            this.getMethod_ = getMethod;
            this.updateMethod_ = updateMethod;
        };
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.updateStyle = function(opt_styleState) {
    var style = '';
    var svgManager = this.point_.getSVGManager();
    if (this.definerMethod_.call(this.styleShapes_, opt_styleState)) {
        var stroke = this.getMethod_.call(this.styleShapes_, opt_styleState);
        style += stroke.getSVGStroke(
                svgManager,
                this.point_.getBounds(),
                this.point_.getColor().getColor());
        style += svgManager.getEmptySVGFill();
    } else style += this.point_.getSVGManager().getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.updatePath = function() {
    this.updateMethod_.call(this.styleShapes_, this.element_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                          StockFillStyleShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                      Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape.prototype.updateStyle = function(styleState) {
    var isUp = this.point_.isUp();
    if (styleState.needUpdateFillShape(isUp)) {
        var svgManager = this.point_.getSVGManager();
        var style = '';
        var color = this.point_.getColor().getColor();
        var bounds = this.point_.getBounds();
        var entry = styleState.getCurrentStateEntry(isUp);

        //fill
        if (entry.needUpdateFill())
            style += entry.getFill().getSVGFill(
                    svgManager,
                    bounds,
                    color);
        else style += svgManager.getEmptySVGFill();

        //stroke
        if (entry.needUpdateBorder())
            style += entry.getBorder().getSVGStroke(
                    svgManager,
                    bounds,
                    color);
        else style += svgManager.getEmptySVGStroke();

    } else style += svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updateFillShape(this.element_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                          StockHatchFillStyleShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                              Update.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape.prototype.updateStyle = function(styleState) {
    var isUp = this.point_.isUp();
    var style;
    if (styleState.needUpdateHatchFillShape(isUp)) {

        var entry = styleState.getCurrentStateEntry(isUp);

        if (entry.needUpdateHatchFill()) {
            style = entry.getHatchFill().getSVGHatchFill(
                    this.point_.getSVGManager(),
                    this.point_.getHatchType(),
                    this.point_.getColor().getColor());
        } else style = this.point_.getSVGManager().getEmptySVGStyle();
    }
    this.element_.setAttribute('style', style);
};
anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape.prototype.updatePath = function() {
    this.styleShapes_.updateFillShape(this.element_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                          OHLCStyleStateEntry class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry = function(style, opt_stateType) {
    anychart.styles.StyleState.call(this, style, opt_stateType)
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry,
        anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.startLine_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.getStartLine = function() {
    return this.startLine_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.needUpdateStartLine = function() {
    return this.startLine_ && this.startLine_.isEnabled();
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.openLine_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.getOpenLine = function () {
    return this.openLine_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.needUpdateOpenLine = function() {
    return this.openLine_ && this.openLine_.isEnabled();
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.closeLine_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.getCloseLine = function() {
    return this.closeLine_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.needUpdateCloseLine = function() {
    return this.closeLine_ && this.closeLine_.isEnabled();
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry.prototype.deserialize = function(data) {
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    if (des.isEnabled(des.getProp(data, 'line'))) {
        var lineData = des.getProp(data, 'line');

        this.startLine_ = new anychart.visual.stroke.Stroke();
        this.openLine_ = new anychart.visual.stroke.Stroke();
        this.closeLine_ = new anychart.visual.stroke.Stroke();

        this.startLine_.deserialize(lineData);
        this.openLine_.deserialize(lineData);
        this.closeLine_.deserialize(lineData);
    }

    if (des.isEnabled(des.getProp(data, 'open_line'))) {
        if (!this.openLine_)
            this.openLine_ = new anychart.visual.stroke.Stroke();
        this.openLine_.deserialize(des.getProp(data, 'open_line'));
    }
    if (des.isEnabled(des.getProp(data, 'close_line'))) {
        if (!this.closeLine_)
            this.closeLine_ = new anychart.visual.stroke.Stroke();
        this.closeLine_.deserialize(des.getProp(data, 'close_line'));
    }
};
//------------------------------------------------------------------------------
//
//                          OHLCStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState = function(style, opt_stateType) {
    anychart.styles.StyleStateWithEffects.call(this, style, opt_stateType);
    this.upStyleStateEntry_ = new anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry();
    this.downStyleStateEntry_ = new anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry();
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.OHLCStyleState,
        anychart.styles.StyleStateWithEffects);
//------------------------------------------------------------------------------
//                          Up style state entry
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.upStyleStateEntry_ = null;
/**
 * @return {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.getUpStyleStateEntry = function() {
    return this.upStyleStateEntry_;
};
//------------------------------------------------------------------------------
//                          Down style state entry
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.downStyleStateEntry_ = null;
/**
 * @return {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.getDownStyleStateEntry = function() {
    return this.downStyleStateEntry_;
};
//------------------------------------------------------------------------------
//                          Definers.
//------------------------------------------------------------------------------
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.needUpdateStartShape = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.needUpdateStartLine() :
            this.downStyleStateEntry_.needUpdateStartLine();
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.needUpdateOpenShape = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.needUpdateOpenLine() :
            this.downStyleStateEntry_.needUpdateOpenLine();
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.needUpdateCloseShape = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.needUpdateCloseLine() :
            this.downStyleStateEntry_.needUpdateCloseLine();
};
//------------------------------------------------------------------------------
//                              Getters.
//------------------------------------------------------------------------------
/**
 * @param {Boolean} isUp
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.getStartLine = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.getStartLine() :
            this.downStyleStateEntry_.getStartLine();
};
/**
 * @param {Boolean} isUp
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.getOpenLine = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.getOpenLine() :
            this.downStyleStateEntry_.getOpenLine();
};
/**
 * @param {Boolean} isUp
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.getCloseLine = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.getCloseLine() :
            this.downStyleStateEntry_.getCloseLine();
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.OHLCStyleState.prototype.deserialize = function(data) {
    if (!data) return data;

    var upData = anychart.utils.JSONUtils.merge(
            data,
            anychart.utils.deserialization.getProp(data, 'up'),
            'up');

    var downData = anychart.utils.JSONUtils.merge(
            data,
            anychart.utils.deserialization.getProp(data, 'down'),
            'down');

    this.getUpStyleStateEntry().deserialize(upData);
    this.getDownStyleStateEntry().deserialize(downData);

    return data;
};
//------------------------------------------------------------------------------
//
//                          OHLCStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes = function() {
    anychart.styles.StyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes,
        anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.startLineShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.openLineShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.closeLineShape_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.createShapes = function() {
    var isUp = this.point_.isUp();
    var style = this.point_.getStyle();

    if (style.needCreateStartShape(isUp)) {
        this.startLineShape_ = new anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape();
        this.startLineShape_.initialize(
                this.point_,
                this,
                this.createStartShape,
                this.needUpdateStartLine,
                this.getStartLine,
                this.updateStartShape);
    }

    if (style.needCreateOpenShape(isUp)) {
        this.openLineShape_ = new anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape();
        this.openLineShape_.initialize(
                this.point_,
                this,
                this.createOpenShape,
                this.needUpdateOpenLine,
                this.getOpenLine,
                this.updateOpenShape);
    }

    if (style.needCreateCloseShape(isUp)) {
        this.closeLineShape_ = new anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape();
        this.closeLineShape_.initialize(
                this.point_,
                this,
                this.createCloseShape,
                this.needUpdateCloseLine,
                this.getCloseLine,
                this.updateCloseShape);
    }
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.placeShapes = function() {
    var sprite = this.point_.getSprite();

    if (this.startLineShape_)
        sprite.appendChild(this.startLineShape_.getElement());

    if (this.openLineShape_)
        sprite.appendChild(this.openLineShape_.getElement());

    if (this.closeLineShape_)
        sprite.appendChild(this.closeLineShape_.getElement());
};
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if(opt_updateData) this.point_.calculatePixelValues();
    if (this.startLineShape_) this.startLineShape_.update(opt_styleState, opt_updateData);
    if (this.openLineShape_)this.openLineShape_.update(opt_styleState, opt_updateData);
    if (this.closeLineShape_)this.closeLineShape_.update(opt_styleState, opt_updateData);
};
/**
 * Update element effects
 * @param {SVGElement} element
 * @param {anychart.styles.StyleStateWithEffects} styleState
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.updateEffects = function(element, styleState) {
    if (styleState.needUpdateEffects())
        element.setAttribute('filter', styleState.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};

/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.createStartShape = function() {
    return this.point_.createStartElement(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.updateStartShape = function(element) {
    element.setAttribute(
            'points',
            this.point_.getStartElementPath(this.point_.getSVGManager()));
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.needUpdateStartLine = function(state) {
    return state.needUpdateStartShape(this.point_.isUp());
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.getStartLine = function(state) {
    return state.getStartLine(this.point_.isUp());
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.createOpenShape = function() {
    return this.point_.createOpenElement(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.updateOpenShape = function(element) {
    element.setAttribute(
            'points',
            this.point_.getOpenElementPath(this.point_.getSVGManager()));
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.needUpdateOpenLine = function(state) {
    return state.needUpdateOpenShape(this.point_.isUp());
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.getOpenLine = function(state) {
    return state.getOpenLine(this.point_.isUp());
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.createCloseShape = function() {
    return this.point_.createCloseElement(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.updateCloseShape = function(element) {
    element.setAttribute(
            'points',
            this.point_.getCloseElementPath(this.point_.getSVGManager()));
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.needUpdateCloseLine = function(state) {
    return state.needUpdateCloseShape(this.point_.isUp());
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.OHLCStyleState} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.getCloseLine = function(state) {
    return state.getCloseLine(this.point_.isUp());
};
/**
 * @param {Boolean} isUp
 * @return {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes.prototype.getCurrentStateEntry = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_ :
            this.downStyleStateEntry_;
};
//------------------------------------------------------------------------------
//
//                          OHLCStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.OHLCStyle,
        anychart.styles.Style);
//------------------------------------------------------------------------------
//                          Style shapes.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.OHLCStyle.prototype.createStyleShapes = function() {
    return new anychart.plots.axesPlot.series.stock.styles.OHLCStyleShapes();
};
//------------------------------------------------------------------------------
//                          Style state.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.OHLCStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.series.stock.styles.OHLCStyleState;
};
//------------------------------------------------------------------------------
//                              Definers.
//------------------------------------------------------------------------------
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyle.prototype.needCreateStartShape = function(isUp) {
    return  this.normal_.needUpdateStartShape(isUp) ||
            this.hover_.needUpdateStartShape(isUp) ||
            this.pushed_.needUpdateStartShape(isUp) ||
            this.selectedNormal_.needUpdateStartShape(isUp) ||
            this.selectedHover_.needUpdateStartShape(isUp) ||
            this.missing_.needUpdateStartShape(isUp);
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyle.prototype.needCreateOpenShape = function(isUp) {
    return  this.normal_.needUpdateOpenShape(isUp) ||
            this.hover_.needUpdateOpenShape(isUp) ||
            this.pushed_.needUpdateOpenShape(isUp) ||
            this.selectedNormal_.needUpdateOpenShape(isUp) ||
            this.selectedHover_.needUpdateOpenShape(isUp) ||
            this.missing_.needUpdateOpenShape(isUp);
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.OHLCStyle.prototype.needCreateCloseShape = function(isUp) {
    return  this.normal_.needUpdateCloseShape(isUp) ||
            this.hover_.needUpdateCloseShape(isUp) ||
            this.pushed_.needUpdateCloseShape(isUp) ||
            this.selectedNormal_.needUpdateCloseShape(isUp) ||
            this.selectedHover_.needUpdateCloseShape(isUp) ||
            this.missing_.needUpdateCloseShape(isUp);
};
//------------------------------------------------------------------------------
//
//                          CandlestickStyleStateEntry class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry = function() {
    anychart.styles.StyleState.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry,
        anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.fill_ = null;
/**
 * @return {anychart.visual.fill.Fill}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.getFill = function() {
    return this.fill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.needUpdateFill = function() {
    return this.fill_ && this.fill_.isEnabled();
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.border_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.getBorder = function() {
    return this.border_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.needUpdateBorder = function() {
    return this.border_ && this.border_.isEnabled();
};
/**
 * @private
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.hatchFill_ = null;
/**
 * @return {anychart.visual.hatchFill.HatchFill}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.getHatchFill = function() {
    return this.hatchFill_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.needUpdateHatchFill = function() {
    return this.hatchFill_ && this.hatchFill_.isEnabled();
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.line_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.getLine = function() {
    return this.line_;
};
/**
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.needUpdateLine = function() {
    return this.line_ && this.line_.isEnabled();
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry.prototype.deserialize = function(data) {
    if (!data) return data;
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    //Замена hatch_type в чилдренах
    if (des.hasProp(data, 'hatch_type')) {
        var hatchType = des.getProp(data, 'hatch_type');
        if (des.hasProp(data, 'hatch_fill')) {
            var hatchFill = des.getProp(data, 'hatch_fill');
            if (des.hasProp(hatchFill, 'type')) {
                hatchFill['type'] = des.getStringProp(hatchFill, 'type')
                        .replace('%hatchtype', hatchType);
            }
        }
    }

    if (des.isEnabled(des.getProp(data, 'fill'))) {
        this.fill_ = new anychart.visual.fill.Fill();
        this.fill_.deserialize(des.getProp(data, 'fill'));
    }

    if (des.isEnabled(des.getProp(data, 'border'))) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(des.getProp(data, 'border'));
    }

    if (des.isEnabled(des.getProp(data, 'hatch_fill'))) {
        this.hatchFill_ = new anychart.visual.hatchFill.HatchFill();
        this.hatchFill_.deserialize(des.getProp(data, 'hatch_fill'));
    }

    if (des.isEnabled(des.getProp(data, 'line'))) {
        this.line_ = new anychart.visual.stroke.Stroke();
        this.line_.deserialize(des.getProp(data, 'line'));
    }

    return data;
};
//------------------------------------------------------------------------------
//
//                          CandlestickStyleState class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleState}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState = function(style, opt_stateType) {
    anychart.styles.StyleStateWithEffects.call(this, style, opt_stateType);
    this.upStyleStateEntry_ = new anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry();
    this.downStyleStateEntry_ = new anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry();
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState,
        anychart.styles.StyleStateWithEffects);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.upStyleStateEntry_ = null;
/**
 * @return {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.getUpStyleStateEntry = function() {
    return this.upStyleStateEntry_;
};
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.downStyleStateEntry_ = null;
/**
 * @return {anychart.plots.axesPlot.series.stock.styles.CandlestickStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.getDownStyleStateEntry = function() {
    return this.downStyleStateEntry_;
};
//------------------------------------------------------------------------------
//                                  Definers.
//------------------------------------------------------------------------------
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.needUpdateFillShape = function(isUp) {
    var entry = isUp ? this.upStyleStateEntry_ : this.downStyleStateEntry_;
    return entry.needUpdateFill() || entry.needUpdateBorder();
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.needUpdateHatchFillShape = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.needUpdateHatchFill() :
            this.downStyleStateEntry_.needUpdateHatchFill();
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.needUpdateLineShape = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.needUpdateLine() :
            this.downStyleStateEntry_.needUpdateLine();
};
/**
 * @param {Boolean} isUp
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.getLine = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_.getLine() :
            this.downStyleStateEntry_.getLine();
};
/**
 * @param {Boolean} isUp
 * @return {anychart.plots.axesPlot.series.stock.styles.OHLCStyleStateEntry}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.getCurrentStateEntry = function(isUp) {
    return isUp ?
            this.upStyleStateEntry_ :
            this.downStyleStateEntry_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState.prototype.deserialize = function(data) {
    if (!data) return data;

    var upData = anychart.utils.JSONUtils.merge(
            data,
            anychart.utils.deserialization.getProp(data, 'up'),
            'up');

    var downData = anychart.utils.JSONUtils.merge(
            data,
            anychart.utils.deserialization.getProp(data, 'down'),
            'down');

    this.getUpStyleStateEntry().deserialize(upData);
    this.getDownStyleStateEntry().deserialize(downData);

    return data;
};
//------------------------------------------------------------------------------
//
//                          CandlestickStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShapes}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes = function() {
    anychart.styles.StyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes, 
        anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.fillShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.hatchFillShape_ = null;
/**
 * @private
 * @type {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.lineShape_ = null;
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();
    var isUp = this.point_.isUp();
    
    if(style.needCreateFillShape(isUp)) {
        this.fillShape_ = new anychart.plots.axesPlot.series.stock.styles.StockFillStyleShape();
        this.fillShape_.initialize(
                this.point_,
                this,
                this.createFillShape);
    }

    if(style.needCreateHatchFillShape(isUp)) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.stock.styles.StockHatchFillStyleShape();
        this.hatchFillShape_.initialize(
                this.point_,
                this,
                this.createFillShape);
    }

    if(style.needCreateLineShape(isUp)) {
        this.lineShape_ = new anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape();
        this.lineShape_.initialize(
                this.point_,
                this,
                this.createLineShape,
                this.needUpdateLine,
                this.getLine,
                this.updateLineShape);
    }
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.placeShapes = function() {
    var sprite = this.point_.getSprite();
    if(this.lineShape_) sprite.appendChild(this.lineShape_.getElement());
    if(this.fillShape_) sprite.appendChild(this.fillShape_.getElement());
    if(this.hatchFillShape_) sprite.appendChild(this.hatchFillShape_.getElement());

};
//------------------------------------------------------------------------------
//                            Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if(opt_updateData) this.point_.calculatePixelValues();
    
    if(this.fillShape_)
        this.fillShape_.update(opt_styleState, opt_updateData);

    if(this.hatchFillShape_)
        this.hatchFillShape_.update(opt_styleState, opt_updateData);

    if(this.lineShape_)
        this.lineShape_.update(opt_styleState, opt_updateData);
};
/**
 * Update element effects
 * @param {SVGElement} element
 * @param {anychart.styles.StyleStateWithEffects} styleState
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.updateEffects = function(element, styleState) {
    if (styleState.needUpdateEffects())
        element.setAttribute('filter', styleState.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.createFillShape = function() {
   return this.point_.drawCandlestick(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.updateFillShape = function(element) {
    var bounds = this.point_.getCandlestickBounds();
    element.setAttribute('x', bounds.x);
    element.setAttribute('y', bounds.y);
    element.setAttribute('width', bounds.width);
    element.setAttribute('height', bounds.height);
};
/**
 * @return {SVGElement}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.createLineShape = function() {
   return this.point_.drawCandlestickLine(this.point_.getSVGManager());
};
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.updateLineShape = function(element) {
    element.setAttribute(
            'points',
            this.point_.getCandlestickLinePath(this.point_.getSVGManager()));
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape} state
 * @return {Boolean}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.needUpdateLine = function(state) {
   return state.needUpdateLineShape(this.point_.isUp());
};
/**
 * @param {anychart.plots.axesPlot.series.stock.styles.StockLineStyleShape} state
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes.prototype.getLine = function(state) {
   return state.getLine(this.point_.isUp());
};
//------------------------------------------------------------------------------
//
//                          CandlestickStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.stock.styles.CandlestickStyle,
        anychart.styles.Style);
//------------------------------------------------------------------------------
//                              StyleState.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle.prototype.getStyleStateClass = function() {
    return anychart.plots.axesPlot.series.stock.styles.CandlestickStyleState;
};
//------------------------------------------------------------------------------
//                              StyleShapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle.prototype.createStyleShapes = function() {
    return new anychart.plots.axesPlot.series.stock.styles.CandlestickStyleShapes();
};
//------------------------------------------------------------------------------
//                              Definers.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle.prototype.needCreateFillShape = function(isUp) {
    return  this.normal_.needUpdateFillShape(isUp) ||
            this.hover_.needUpdateFillShape(isUp) ||
            this.pushed_.needUpdateFillShape(isUp) ||
            this.selectedNormal_.needUpdateFillShape(isUp) ||
            this.selectedHover_.needUpdateFillShape(isUp) ||
            this.missing_.needUpdateFillShape(isUp);
};
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle.prototype.needCreateHatchFillShape = function(isUp) {
    return  this.normal_.needUpdateHatchFillShape(isUp) ||
            this.hover_.needUpdateHatchFillShape(isUp) ||
            this.pushed_.needUpdateHatchFillShape(isUp) ||
            this.selectedNormal_.needUpdateHatchFillShape(isUp) ||
            this.selectedHover_.needUpdateHatchFillShape(isUp) ||
            this.missing_.needUpdateHatchFillShape(isUp);
};
anychart.plots.axesPlot.series.stock.styles.CandlestickStyle.prototype.needCreateLineShape = function(isUp) {
    return  this.normal_.needUpdateLineShape(isUp) ||
            this.hover_.needUpdateLineShape(isUp) ||
            this.pushed_.needUpdateLineShape(isUp) ||
            this.selectedNormal_.needUpdateLineShape(isUp) ||
            this.selectedHover_.needUpdateLineShape(isUp) ||
            this.missing_.needUpdateLineShape(isUp);
};