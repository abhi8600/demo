goog.provide('anychart.plots.axesPlot.series.stock.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.stock.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"ohlc_style": {
				"line": {
					"thickness": "6"
				},
				"up": {
					"line": {
						"color": "LightColor(%Color)"
					}
				},
				"down": {
					"line": {
						"color": "DarkColor(%Color)"
					}
				},
				"name": "anychart_default"
			},
			"candlestick_style": {
				"fill": {
					"type": "solid",
					"color": "%Color"
				},
				"border": {
				},
				"line": {
					"color": "Black",
					"thickness": "2"
				},
				"up": {
					"fill": {
						"color": "LightColor(%Color)"
					}
				},
				"down": {
					"fill": {
						"color": "DarkColor(%Color)"
					}
				},
				"name": "anychart_default"
			}
		},
		"data_plot_settings": {
			"ohlc_series": {
				"label_settings": {
				},
				"tooltip_settings": {
				},
				"marker_settings": {
				},
				"group_padding": "0.5",
				"point_padding": "0.2"
			},
			"candlestick_series": {
				"label_settings": {
				},
				"tooltip_settings": {
				},
				"marker_settings": {
				},
				"group_padding": "0.5",
				"point_padding": "0.2"
			}
		}
	}
};