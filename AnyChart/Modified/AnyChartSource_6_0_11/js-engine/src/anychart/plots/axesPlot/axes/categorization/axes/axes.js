/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis};</li>
 *  <li>@class {anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis};</li>
 * <ul>
 */

goog.provide('anychart.plots.axesPlot.axes.categorization.axes');

goog.require("anychart.plots.axesPlot.axes");
goog.require('anychart.plots.axesPlot.axes.categorization');
goog.require('anychart.scales');


//------------------------------------------------------------------------------
//
//                          BaseCategorizedAxis class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.Axis}
 */
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis = function() {
    anychart.plots.axesPlot.axes.Axis.call(this);
    this.categoriesList_ = [];
    this.categoriesMap_ = {};
    this.clusterSettingsList_ = [];
};

goog.inherits(anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis, anychart.plots.axesPlot.axes.Axis);

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.categoriesMap_ = null;
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.categoriesList_ = null;
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.clusterSettingsList_ = null;
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.isCategorized = function() {
    return true;
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.drawZeroLine = function() {
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.onAfterDeserializeData = function() {
    for (var i = 0; i < this.categoriesList_.length; i++)
        this.categoriesList_[i].onAfterDeserializeData();
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.setBounds = function(bounds) {

    goog.base(this, 'setBounds', bounds);
    var categoryWidth = this.scale_.transformValueToPixel(this.scale_.getMinimum() + 1) - this.scale_.transformValueToPixel(this.scale_.getMinimum());
    for (var i = 0; i < this.clusterSettingsList_.length; i++)
        this.clusterSettingsList_[i].calculate(categoryWidth);
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.createLabels_ = function(labelsConfig) {
    return null;
};
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.setLabelsSpace = function(bounds, axisBounds) {
    this.applyLabelsSpace_(bounds, axisBounds, this.scale_.getMajorIntervalCount() - 1);
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.getCategoryByIndex = function(index) {
    return this.categoriesList_[index];
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.getCategory = function(name, svgManager) {
    if (!this.hasCategory(name))
        return this.createCategory_(name, svgManager);
    return this.categoriesMap_[name];
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.hasCategory = function(name) {
    return this.categoriesMap_[name] != undefined;
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.registerDataCategory_ = function(category) {
    this.plot_.addDataCategory(category);
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    if (anychart.utils.deserialization.hasProp(data, 'tickmarks_placement'))
        this.isCenterTickmarks_ = anychart.utils.deserialization.getEnumProp(
                data, 'tickmarks_placement'
        ) == 'center';
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.createCategory_ = function(name, svgManager) {

    var category = new anychart.plots.axesPlot.axes.categorization.Category(this.plot_, this.categoriesList_.length, name, this.isArgument(), this);
    if (this.isArgument()) {
        this.plot_.addXCategory(category);
        this.registerDataCategory_(category);
    } else {
        this.plot_.addYCategory(category);
    }
    if (this.labels_ != null)
        this.labels_.addCategory(category, svgManager);

    this.checkScaleValue(category.getIndex());

    this.categoriesList_.push(category);
    this.categoriesMap_[name] = category;
    return category;
};

anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.createLabels_ = function(labelsNode) {
    if (anychart.utils.deserialization.hasProp(labelsNode, 'display_mode')) {
        var mode = anychart.utils.deserialization.getLStringProp(labelsNode, 'display_mode');
        if (mode == 'stager')
            return new anychart.plots.axesPlot.axes.categorization.axes.labels.StagerCategorizedAxisLabels(this, this.viewPort_.getLabelsViewPort(), false);
    }
    return new anychart.plots.axesPlot.axes.categorization.axes.labels.CategorizedAxisLabels(this, this.viewPort_.getLabelsViewPort(), false);
};
//------------------------------------------------------------------------------
//                          Axis scale.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.prototype.createScale_ = function(scaleNode) {
    var des = anychart.utils.deserialization;
    if(!scaleNode || !des.hasProp(scaleNode, 'type') || des.getEnumProp(scaleNode, 'type') != 'datetime')
        return new anychart.plots.axesPlot.scales.TextScale(this);
    return new anychart.plots.axesPlot.scales.TextDateTimeScale(this);
};
//------------------------------------------------------------------------------
//
//                          CategorizedAxis class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis}
 */
anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis = function() {
    anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.call(this);
    this.clusterSettingsMap_ = {};
};

goog.inherits(anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis,
        anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis);


anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis.prototype.clusterSettingsMap_ = {};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis.prototype.getType = function() {
    return anychart.plots.axesPlot.axes.AxisType.CATEGORIZED;
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis.prototype.checkSeries = function(series) {
    if (!series.isClusterizableOnCategorizedAxis()) return;
    var clusterSettings = this.clusterSettingsMap_[series.getClusterKey()];
    if (clusterSettings == null) {
        clusterSettings = new anychart.plots.axesPlot.axes.categorization.ClusterSettings();
        clusterSettings.setGroupPadding(series.getGlobalSettings().getGroupPadding());
        clusterSettings.setPointPadding(series.getGlobalSettings().getPointPadding());
        this.clusterSettingsMap_[series.getClusterKey()] = clusterSettings;
        this.clusterSettingsList_.push(clusterSettings);
    }
    series.setClusterSettings(clusterSettings);
    if (series.clusterContainsOnlyOnePoint()) {
        series.setClusterIndex(0);
        clusterSettings.setNumClusters(1);
    } else {
        series.setClusterIndex(clusterSettings.checkAxis(series.getValueAxis()));
    }
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis.prototype.transform = function(point, value, pixPoint, opt_pixOffset) {
    if (!point.getSeries().isClusterizableOnCategorizedAxis()) {
        goog.base(this, 'transform', point, value, pixPoint, opt_pixOffset);
        return;
    }

    if (isNaN(opt_pixOffset)) opt_pixOffset = 0;

    var pixValue = this.scale_.transformValueToPixel(value) + opt_pixOffset;
    var series = point.getSeries();

    pixValue += series.getClusterSettings().getOffset(series.getClusterIndex());
    this.viewPort_.setScaleValue(pixValue, pixPoint);
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis.prototype.simpleTransform = function(point, value) {
    if (!point.getSeries().isClusterizableOnCategorizedAxis())
        return goog.base(this, 'simpleTransform', point, value);

    var pixValue = this.scale_.transform(value);
    var series = point.getSeries();

    pixValue += series.getClusterSettings().getOffset(series.getClusterIndex());
    return pixValue;
};

//-----------------------------------------------------------------------------------
//
//                          CategorizedBySeriesAxis class
//
//-----------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis = function() {
    anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis.call(this);
};

goog.inherits(anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis,
        anychart.plots.axesPlot.axes.categorization.axes.BaseCategorizedAxis);

anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis.prototype.getType = function() {
    return anychart.plots.axesPlot.axes.AxisType.CATEGORIZED_BY_SERIES;
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis.prototype.checkSeries = function(series, svgManager) {
    var category = this.createCategory_(series.getName(), svgManager);
    series.setCategory(category);

    var clusterSettings = new anychart.plots.axesPlot.axes.categorization.ClusterSettings();
    clusterSettings.setGroupPadding(series.getGlobalSettings().getGroupPadding());
    clusterSettings.setPointPadding(series.getGlobalSettings().getPointPadding());

    category.setClusterSettings(clusterSettings);

    var valueAxis = series.getValueAxis();
    if (valueAxis.getType() == anychart.plots.axesPlot.axes.AxisType.VALUE &&
            valueAxis.getScale().getMode() != anychart.scales.ScaleMode.NORMAL) {
        series.setIsSingleCluster(true);
        clusterSettings.setNumClusters(1);
    }

    this.clusterSettingsList_.push(clusterSettings);
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis.prototype.transform = function(point, value, pixPoint, opt_pixOffset) {
    if (isNaN(opt_pixOffset)) opt_pixOffset = 0;

    var pixValue = this.scale_.transformValueToPixel(value) + opt_pixOffset;
    pixValue += point.getCategory().getClusterSettings().getOffset(point.getClusterIndex());
    this.viewPort_.setScaleValue(pixValue, pixPoint);
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis.prototype.simpleTransform = function(point, value) {
    var pixValue = this.scale_.transformValueToPixel(value);
    pixValue += point.getCategory().getClusterSettings().getOffset(point.getClusterIndex());

    return pixValue;
};

anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis.prototype.registerDataCategory_ = function() {
};