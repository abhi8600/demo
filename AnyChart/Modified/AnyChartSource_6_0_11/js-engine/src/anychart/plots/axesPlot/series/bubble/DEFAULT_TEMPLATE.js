goog.provide('anychart.plots.axesPlot.series.bubble.DEFAULT_TEMPLATE');

anychart.plots.axesPlot.series.bubble.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"bubble_style": [
				{
					"border": {
						"type": "Solid",
						"thickness": "1",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "%Color",
									"opacity": "0.9"
								},
								{
									"position": "1",
									"color": "Blend(DarkColor(%Color),%Color,0.7)",
									"opacity": "1"
								}
							],
							"angle": "90"
						},
						"type": "Gradient",
						"color": "%Color",
						"opacity": "1"
					},
					"effects": {
						"bevel": {
							"enabled": "false",
							"distance": "1"
						}
					},
					"states": {
						"hover": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							}
						},
						"pushed": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							},
							"effects": {
								"bevel": {
									"enabled": "true",
									"distance": "2",
									"angle": "225"
								}
							}
						},
						"selected_normal": {
							"border": {
								"thickness": "2",
								"color": "DarkColor(%Color)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"opacity": "0.3",
								"pattern_size": "6",
								"color": "Black"
							}
						},
						"selected_hover": {
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.9"
							},
							"border": {
								"thickness": "2",
								"color": "DarkColor(White)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"color": "DarkColor(White)",
								"opacity": "0.3",
								"pattern_size": "6"
							}
						},
						"missing": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)",
								"thickness": "1",
								"opacity": "0.4"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "anychart_default"
				},
				{
					"border": {
						"type": "Solid",
						"thickness": "1",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"type": "Solid",
						"color": "%Color",
						"opacity": "0.9"
					},
					"effects": {
						"bevel": {
							"enabled": "false",
							"distance": "1"
						}
					},
					"states": {
						"hover": {
							"border": {
								"type": "Solid",
								"color": "%Color",
								"thickness": "2"
							},
							"fill": {
								"type": "Solid",
								"color": "LightColor(%Color)",
								"opacity": "0.9"
							}
						},
						"pushed": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(%Color)",
								"thickness": "2"
							},
							"fill": {
								"type": "Solid",
								"color": "Blend(%Color,Dark,0.9)",
								"opacity": "0.9"
							}
						},
						"selected_normal": {
							"border": {
								"thickness": "2",
								"color": "DarkColor(%Color)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"opacity": "0.3",
								"pattern_size": "6"
							}
						},
						"selected_hover": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(%Color)",
								"thickness": "2"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"color": "DarkColor(%Color)",
								"opacity": "0.7",
								"pattern_size": "6"
							},
							"fill": {
								"type": "Solid",
								"color": "LightColor(%Color)",
								"opacity": "0.9"
							}
						},
						"missing": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)",
								"thickness": "1",
								"opacity": "0.4"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "Flat"
				},
				{
					"border": {
						"type": "Solid",
						"thickness": "1",
						"color": "DarkColor(%Color)"
					},
					"fill": {
						"type": "Solid",
						"color": "%Color",
						"opacity": "0.5"
					},
					"effects": {
						"bevel": {
							"enabled": "false",
							"distance": "1"
						}
					},
					"states": {
						"hover": {
							"border": {
								"type": "Solid",
								"color": "%Color",
								"thickness": "2"
							},
							"fill": {
								"type": "Solid",
								"color": "LightColor(%Color)",
								"opacity": "0.5"
							}
						},
						"pushed": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(%Color)",
								"thickness": "2"
							},
							"fill": {
								"type": "Solid",
								"color": "Blend(%Color,Dark,0.9)",
								"opacity": "0.5"
							}
						},
						"selected_normal": {
							"border": {
								"thickness": "2",
								"color": "DarkColor(%Color)"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"opacity": "0.3",
								"pattern_size": "6"
							}
						},
						"selected_hover": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(%Color)",
								"thickness": "2"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "2",
								"type": "ForwardDiagonal",
								"color": "DarkColor(%Color)",
								"opacity": "0.7",
								"pattern_size": "6"
							},
							"fill": {
								"type": "Solid",
								"color": "LightColor(%Color)",
								"opacity": "0.5"
							}
						},
						"missing": {
							"border": {
								"type": "Solid",
								"color": "DarkColor(White)",
								"thickness": "1",
								"opacity": "0.4"
							},
							"fill": {
								"gradient": {
									"key": [
										{
											"position": "0",
											"color": "White",
											"opacity": "1"
										},
										{
											"position": "1",
											"color": "Blend(DarkColor(White),White,0.7)",
											"opacity": "1"
										}
									],
									"angle": "90"
								},
								"type": "Gradient",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "Transparent"
				},
				{
					"effects": {
						"bevel": {
							"enabled": "false"
						},
						"drop_shadow": {
							"enabled": "false"
						}
					},
					"states": {
						"normal": {
							"fill": {
								"color": "%Color",
								"opacity": "1"
							}
						},
						"hover": {
							"fill": {
								"color": "White",
								"opacity": "0.8"
							}
						},
						"pushed": {
							"fill": {
								"color": "Blend(White,Black,0.7)",
								"opacity": "1"
							},
							"effects": {
								"bevel": {
									"enabled": "true",
									"distance": "2",
									"angle": "225"
								}
							}
						},
						"selected_normal": {
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"opacity": "0.2"
							}
						},
						"selected_hover": {
							"fill": {
								"color": "White",
								"opacity": "0.8"
							},
							"hatch_fill": {
								"enabled": "true",
								"thickness": "3",
								"type": "ForwardDiagonal",
								"color": "DarkColor(White)",
								"opacity": "0.2"
							}
						},
						"missing": {
							"fill": {
								"color": "White",
								"opacity": "0.3"
							},
							"hatch_fill": {
								"type": "Checkerboard",
								"pattern_size": "20",
								"enabled": "true",
								"opacity": "0.1"
							},
							"effects": {
								"bevel": {
									"enabled": "false",
									"distance": "1",
									"angle": "225"
								}
							}
						}
					},
					"name": "Aqua",
					"final": "true"
				}
			]
		},
		"data_plot_settings": {
			"bubble_series": {
				"animation": {
					"style": "defaultScaleXYCenter"
				},
				"label_settings": {
					"animation": {
						"style": "defaultLabel"
					},
					"enabled": "False"
				},
				"marker_settings": {
					"animation": {
						"style": "defaultMarker"
					},
					"marker": {
						"anchor": "Center",
						"v_align": "Center",
						"h_align": "Center"
					},
					"enabled": "false"
				},
				"tooltip_settings": {
				}
			}
		}
	}
};