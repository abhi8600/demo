goog.provide('anychart.plots.axesPlot.templates');

goog.require('anychart.plots.seriesPlot.templates');
goog.require('anychart.plots.axesPlot.templates.DEFAULT_TEMPLATE');

goog.require('anychart.plots.axesPlot.series.bar.DEFAULT_TEMPLATE');
goog.require('anychart.plots.axesPlot.series.bubble.DEFAULT_TEMPLATE');
goog.require('anychart.plots.axesPlot.series.line.DEFAULT_TEMPLATE');
goog.require('anychart.plots.axesPlot.series.stock.DEFAULT_TEMPLATE');
goog.require('anychart.plots.axesPlot.series.marker.DEFAULT_TEMPLATE');
goog.require('anychart.plots.axesPlot.series.heatMap.DEFAULT_TEMPLATE');

/**
 * Creates AxesPlotTemplatesMerger.
 *
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger}
 */
anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger = function(){
    anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger.apply(this);
};

goog.inherits(anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger, anychart.plots.seriesPlot.templates.SeriesPlotTemplatesMerger);

/**
 * Returns plot default template.
 *
 * @public
 * @override
 * @return {object}
 */
anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger.prototype.getPlotDefaultTemplate = function(){
    var res =  this.mergeTemplates(
            goog.base(this,'getPlotDefaultTemplate'),
            anychart.plots.axesPlot.templates.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.stock.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.line.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.bubble.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.marker.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.heatMap.DEFAULT_TEMPLATE);
    res = this.mergeTemplates(res, anychart.plots.axesPlot.series.bar.DEFAULT_TEMPLATE);
    return res;
};

/**
 * Processes merging for chart node.
 *
 * @override
 * @protected
 * @param {Object.<*>} template
 * @param {Object.<*>} chart
 * @param {Object.<*>} res - out result
 */
anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger.prototype.mergeChartNode = function (template, chart, res) {
    goog.base(this, 'mergeChartNode', template, chart, res);
    var des = anychart.utils.deserialization;
    var json = anychart.utils.JSONUtils;
    var templateExtraAxes = null;
    var chartExtraAxes = null;
    var tmp;
    if (des.hasProp(template, 'chart_settings') &&
            des.hasProp(tmp = des.getProp(template, 'chart_settings'), 'axes') &&
            des.hasProp(tmp = des.getProp(tmp, 'axes'), 'extra'))
        templateExtraAxes = des.getProp(tmp, 'extra');
    if (des.hasProp(chart, 'chart_settings') &&
            des.hasProp(tmp = des.getProp(chart, 'chart_settings'), 'axes') &&
            des.hasProp(tmp = des.getProp(tmp, 'axes'), 'extra'))
        chartExtraAxes = des.getProp(tmp, 'extra');
    var extraAxes = json.mergeWithOverride(templateExtraAxes, chartExtraAxes);
    if (extraAxes != null)
        des.getProp(des.getProp(res, 'chart_settings'), 'axes')['extra'] = extraAxes;
};

/**
 * Processes chart defaults applying.
 *
 * @override
 * @protected
 * @param {Object.<*>} res - output result
 * @param {Object.<*>} defaults
 */
anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger.prototype.applyDefaults = function(res, defaults) {
    goog.base(this, 'applyDefaults', res, defaults);
    if (res == null || defaults == null) return;

    var des = anychart.utils.deserialization;
    var json = anychart.utils.JSONUtils;
    var defaultAxis = des.getProp(defaults, 'axis');
    var tmp = null;
    if (defaultAxis != null && des.hasProp(res, 'chart_settings') &&
            des.hasProp(tmp = des.getProp(res, 'chart_settings'), 'axes')) {
        var axes = des.getProp(tmp, 'axes');
        if (des.hasProp(axes, 'x_axis'))
            axes['x_axis'] = json.merge(defaultAxis, des.getProp(axes, 'x_axis'),'x_axis');
        if (des.hasProp(axes, 'y_axis'))
            axes['y_axis'] = json.merge(defaultAxis, des.getProp(axes, 'y_axis'),'y_axis');
        if (des.hasProp(axes, 'extra')) {
            var allExtraAxes = des.getProp(axes, 'extra');

            if (!(allExtraAxes['x_axis'] instanceof Array)) // making x_axis an array
                if (allExtraAxes['x_axis'])
                    allExtraAxes['x_axis'] = [allExtraAxes['x_axis']];
                else
                    allExtraAxes['x_axis'] = [];
            var extraAxes = allExtraAxes['x_axis']; // always array
            var axesCnt = extraAxes.length;
            var i;
            for (i = 0; i < axesCnt; i++) {
                extraAxes[i] = json.merge(defaultAxis, extraAxes[i], 'x_axis');
                if (des.hasProp(extraAxes[i], 'name'))
                    extraAxes[i]['name'] = des.getLString(des.getProp(extraAxes[i], 'name'));
            }

            if (!(allExtraAxes['y_axis'] instanceof Array)) // making y_axis an array
                if (allExtraAxes['y_axis'])
                    allExtraAxes['y_axis'] = [allExtraAxes['y_axis']];
                else
                    allExtraAxes['y_axis'] = [];
            extraAxes = allExtraAxes['y_axis']; // always array
            axesCnt = extraAxes.length;
            for (i = 0; i < axesCnt; i++) {
                extraAxes[i] = json.merge(defaultAxis, extraAxes[i], 'y_axis');
                if (des.hasProp(extraAxes[i], 'name'))
                    extraAxes[i]['name'] = des.getLString(des.getProp(extraAxes[i], 'name'));
            }
        }
    }
};
