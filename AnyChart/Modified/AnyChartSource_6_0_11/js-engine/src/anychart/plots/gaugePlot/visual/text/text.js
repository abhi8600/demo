goog.provide('anychart.plots.gaugePlot.visual.text');
goog.require('anychart.visual.text');
//------------------------------------------------------------------------------
//
//                     GaugeTextPlacementMod class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod = {
    BY_POINT:0,
    BY_RECTANGLE:1,
    BY_ANCHOR:2
};
anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.deserialize = function (value) {
    switch (value) {
        default:
        case 'bypoint':
            return anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_POINT;
        case 'byrectangle':
            return anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_RECTANGLE;
        case 'byanchor':
            return anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_ANCHOR;
    }

};
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement = function () {
    anychart.visual.text.TextElement.call(this);
    this.hAlign_ = anychart.layout.HorizontalAlign.CENTER;
    this.vAlign_ = anychart.layout.VerticalAlign.CENTER;
    this.placemenetMode_ = anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.BY_POINT;
    this.padding_ = 0;
    this.isPixXY_ = false;
};
goog.inherits(anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement,
    anychart.visual.text.TextElement);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.layout.HorizontalAlign}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.hAlign_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.layout.VerticalAlign}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.vAlign_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.placemenetMode_ = null;
/**
 * @return {anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.getPlacemenetMode = function () {
    return this.placemenetMode_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.x_ = null;
/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.setX = function (value) {
    this.x_ = value;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.y_ = null;
/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.setY = function (value) {
    this.y_ = value;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.isPixXY_ = null;
/**
 * @param {Boolean} value
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.setIsPixXY = function (value) {
    this.isPixXY_ = value;
};
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.percentWidth_ = NaN;
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.percentHeight_ = NaN;
/**
 * ND: Needs doc!
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.spread_ = null;
/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.padding_ = null;
/**
 * @private
 * @type {String}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.text_ = null;
/**
 * @return {String}
 */
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.getText = function () {
    return this.text_;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;

    des.deleteProp(data, 'width');
    goog.base(this, 'deserialize', data);

    if (des.hasProp(data, 'position')) {
        var position = des.getProp(data, 'position');

        if (des.hasProp(position, 'halign'))
            this.hAlign_ = anychart.layout.HorizontalAlign.deserialize(des.getLStringProp(position, 'halign'));

        if (des.hasProp(position, 'valign'))
            this.vAlign_ = anychart.layout.VerticalAlign.deserialize(des.getLStringProp(position, 'valign'));

        if (des.hasProp(position, 'placement_mode'))
            this.placemenetMode_ = anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod.deserialize(des.getLStringProp(position, 'placement_mode'));

        if (des.hasProp(position, 'x'))
            this.x_ = des.getSimplePercentProp(position, 'x');

        if (des.hasProp(position, 'y'))
            this.y_ = des.getSimplePercentProp(position, 'y');

        if (des.hasProp(position, 'width'))
            this.percentWidth_ = des.getSimplePercentProp(position, 'width');

        if (des.hasProp(position, 'height'))
            this.percentHeight_ = des.getSimplePercentProp(position, 'height');

        if (des.hasProp(position, 'spread'))
            this.spread_ = des.getBoolProp(position, 'spread');

        if (des.hasProp(position, 'padding'))
            this.padding_ = des.getNumProp(position, 'padding');
    }

    if (des.hasProp(data, 'format'))
        this.text_ = des.getStringProp(data, 'format');
};
//------------------------------------------------------------------------------
//                  Drawing.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement.prototype.update = function (svgManager, sprite, x, y, wTargetSize, hTargetSize, opt_color, opt_noUpdateText) {
    var pMode = anychart.plots.gaugePlot.visual.text.GaugeTextPlacementMod;
    var hAlign = anychart.layout.HorizontalAlign;
    var vAlign = anychart.layout.VerticalAlign;

    x = isNaN(this.x_) ? x : ((this.isPixXY_ ? (this.x_) : (this.x_ * wTargetSize)) + x);
    y = isNaN(this.y_) ? y : ((this.isPixXY_ ? (this.y_) : (this.y_ * hTargetSize)) + y);

    var bgX = x;
    var bgY = y;

    var width = this.bounds_.width;
    var height = this.bounds_.height;

    var containerW = this.percentWidth_ * wTargetSize;
    var containerH = this.percentHeight_ * hTargetSize;

    if (isNaN(containerW)) containerW = this.nonRotatedBounds_.width;
    if (isNaN(containerH)) containerH = this.nonRotatedBounds_.height;

    switch (this.placemenetMode_) {
        case pMode.BY_POINT:
        case pMode.BY_ANCHOR:
            switch (this.hAlign_) {
                case hAlign.CENTER:
                    bgX -= containerW / 2;
                    x -= width / 2;
                    break;
                case hAlign.LEFT:
                    if (this.spread_) {
                        bgX -= containerW + this.padding_;
                        x += -containerW - this.padding_ + (containerW - width) / 2;
                    } else {
                        x -= width + this.padding_;
                    }
                    break;
                case hAlign.RIGHT:
                    if (this.spread_) {
                        bgX += this.padding_;
                        x += this.padding_ + (containerW - width) / 2;
                    } else {
                        x += this.padding_;
                    }
                    break;
            }

            switch (this.vAlign_) {
                case vAlign.TOP:
                    if (this.spread_) {
                        bgY -= containerH + this.padding_;
                        y += -containerH - this.padding_ + (containerH - height) / 2;
                    } else {
                        y -= height + this.padding_;
                    }
                    break;
                case vAlign.CENTER:
                    bgY -= ((containerH) / 2);
                    y -= height / 2;
                    break;
                case vAlign.BOTTOM:
                    if (this.spread_) {
                        bgY += this.padding_;
                        y += this.padding_ + (containerH - height) / 2;
                    } else {
                        y += this.padding_;
                    }
                    break;
            }
            break;

        case pMode.BY_RECTANGLE:
            switch (this.hAlign_) {
                case hAlign.LEFT:
                    x += this.padding_;
                    break;
                case hAlign.CENTER:
                    x += (containerW - width) / 2;
                    break;
                case hAlign.RIGHT:
                    x += containerW - width - this.padding_;
                    break;
            }

            switch (this.vAlign_) {
                case vAlign.TOP:
                    y += this.padding_;
                    break;
                case vAlign.CENTER:
                    y += (containerH - height) / 2;
                    break;
                case vAlign.BOTTOM:

                    y += containerH - height - this.padding_;
                    break;
            }
            break;
    }
    if (this.spread_) {
        if (this.background_ && this.background_.isEnabled()) {
            if (this.background_.getMargin()) {
                var margin = this.background_.getMargin();
                containerW += margin.getLeft() + margin.getRight();
                containerH += margin.getTop() + margin.getBottom();
            }
            // if thickness is even offsets line to 0.5px for normal line drawing
            var t;
            if (this.background_.getBorder())
                t = this.background_.getBorder().getThickness();
            else t = 0;
            this.background_.getSprite().setPosition(bgX - x - (t % 2 / 2), bgY - y - (t % 2 / 2));
        }
        this.nonRotatedBounds_.width = containerW;
        this.nonRotatedBounds_.height = containerH;
    }


    goog.base(this, 'update', svgManager, sprite, opt_color, opt_noUpdateText);
    sprite.setPosition(x, y);
    return new anychart.utils.geom.Point(x, y);
};
