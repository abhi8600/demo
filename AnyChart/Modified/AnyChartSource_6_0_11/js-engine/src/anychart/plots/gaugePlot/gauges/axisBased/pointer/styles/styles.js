/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.axisBased.pointer.styles');

goog.require('anychart.styles.background');
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState = function (style, opt_stateType) {
    anychart.styles.background.BackgroundStyleState.call(this, style, opt_stateType);
    this.align_ = anychart.plots.gaugePlot.layout.Align.CENTER;
    this.padding_ = 0;
    this.underElements_ = false
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState,
    anychart.styles.background.BackgroundStyleState);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.align_ = null;
/**
 * @return {anychart.plots.gaugePlot.layout.Align}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.getAlign = function () {
    return this.align_;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.getPadding = function () {
    return this.padding_
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.underElements_ = null;
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.deserialize = function (data) {
    data = goog.base(this, 'deserialize', data);
    this.deserializePosition(data);
    return data;
};
//------------------------------------------------------------------------------
//                  Rotation.
//------------------------------------------------------------------------------
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.initializeRotation = function (gauge) {
    if (this.fill_ && this.fill_.hasGradient()) gauge.setGradientRotation(this.fill_.getGradient());
    if (this.border_ && this.border_.hasGradient()) gauge.setGradientRotation(this.border_.getGradient());
};
/**
 * @protected
 * @param {Object} data
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.deserializePosition = function (data) {
    var des = anychart.utils.deserialization;
    var gaugeDes = anychart.plots.gaugePlot.deserializationUtils;

    if (des.hasProp(data, 'align'))
        this.align_ = gaugeDes.getAlignProp(data, 'align');

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getSimplePercentProp(data, 'padding');

    if (des.hasProp(data, 'under_elements'))
        this.underElements_ = des.getBoolProp(data, 'under_elements');
};
//------------------------------------------------------------------------------
//                  Rotations.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge} gauge
 */
anychart.plots.gaugePlot.gauges.axisBased.pointer.styles.PointerStyleState.prototype.initializeRotation = function (gauge) {
    if (this.fill_ && this.fill_.isGradient())
        gauge.setGradientRotation(this.fill_.getGradient());

    if (this.stroke_ && this.stroke_.isGradient())
        gauge.setGradientRotation(this.stroke_.getGradient());
};



