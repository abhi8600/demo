goog.provide('anychart.plots.gaugePlot.gauges');

goog.require('anychart.plots.gaugePlot.gauges.labels');
goog.require('anychart.plots.gaugePlot.frames');
goog.require('anychart.plots.gaugePlot.elements');

//------------------------------------------------------------------------------
//
//                       GaugeType enum.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.gaugePlot.gauges.GaugeType = {
    'BASE':0,
    'CIRCULAR':1,
    'LINEAR':2,
    'LABEL':3,
    'IMAGE':4,
    'INDICATOR':5
};
//------------------------------------------------------------------------------
//
//                      GaugeBase class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.gaugePlot.gauges.GaugeBase = function () {
    this.x = 0;
    this.y = 0;
    this.width = 1;
    this.height = 1;
    this.bounds = new anychart.utils.geom.Rectangle();

    this.margin_ = new anychart.layout.Margin(0);
    this.children = [];
    this.labels_ = [];
};
//------------------------------------------------------------------------------
//                      Gauge type.
//------------------------------------------------------------------------------
/**
 * @return {anychart.plots.gaugePlot.gauges.GaugeType}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getGaugeType = function () {
    return anychart.plots.gaugePlot.gauges.GaugeType.BASE;
};
//------------------------------------------------------------------------------
//                      Gauge name.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {string}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.name_ = null;

/**
 * @return {string} gauge name.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getName = function () {
    return this.name_;
};

/**
 * @private
 * @type {string}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.parentName_ = null;

/**
 * @return {string} parent gauge name.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getParentName = function () {
    return this.parentName_;
};

/**
 * @return {boolean}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.hasParent = function () {
    return this.parentName_ != null;
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.GaugePlot}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.plot = null;

/**
 * @param {anychart.plots.gaugePlot.GaugePlot} plot Gauge plot.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.setPlot = function (plot) {
    this.plot = plot;
};

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.x = 0;

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.y = 0;

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.width = 1;

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.height = 1;

/**
 * @protected
 * @type {Array.<anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.children = null;
/**
 * @protected
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.stylesList = null;

/**
 * @param {anychart.plots.gaugePlot.gauges.GaugeBase} gauge Child gauge.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.addChild = function (gauge) {
    this.children.push(gauge);
};

/**
 * @return {number} Count of children gauges.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.numChildren = function () {
    return this.children.length;
};

/**
 * @param {number} index Child index.
 * @return {anychart.plots.gaugePlot.gauges.GaugeBase} Child at specific index.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getChild = function (index) {
    return this.children[index];
};

/**
 * @protected
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.bounds = null;

/**
 * @return {anychart.utils.geom.Rectangle} Gauge bounds.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getBounds = function () {
    return this.bounds;
};

/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.margin_ = null;

/**
 * @private
 * @type {Array<anychart.plots.gaugePlot.gauges.labels.GaugeLabel>}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.labels_ = null;
/**
 * @type {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.selectedPointer = null;

/**
 * @param {Object} opt_res Serialized object.
 * @return {Object} Serialized object.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.serialize = function (opt_res) {
    var res = opt_res || {};
    res['children'] = [];
    for (var i = 0; i < this.children.length; i++) {
        res['children'].push(this.children[i].serialize());
    }
    res['name'] = this.name_;
    return res;
};

/**
 * @param {Object} config Gauge config.
 * @return {boolean} Is gauge config valid or not.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.isValid = function (config) {
    return true;
};

/**
 * @param {Object} data Gauge config.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    //styles list
    this.stylesList = new anychart.styles.StylesList(des.getProp(data, 'styles'));

    //common
    if (des.hasProp(data, 'name')) this.name_ = des.getStringProp(data, 'name');
    if (des.hasProp(data, 'parent')) this.parentName_ = des.getStringProp(data, 'parent');
    if (des.hasProp(data, 'x')) this.x = des.getSimplePercentProp(data, 'x');
    if (des.hasProp(data, 'y')) this.y = des.getSimplePercentProp(data, 'y');
    if (des.hasProp(data, 'width')) this.width = des.getSimplePercentProp(data, 'width');
    if (des.hasProp(data, 'height')) this.height = des.getSimplePercentProp(data, 'height');
    if (des.hasProp(data, 'margin')) this.margin_.deserialize(des.getProp(data, 'margin'));

    //frames
    this.frame = this.createFrame(des.getProp(data, 'frame'));
    if (this.frame && des.hasProp(data, 'frame'))
        this.frame.deserialize(des.getProp(data, 'frame'));

    //labels
    if (des.hasProp(data, 'labels')) {
        var labels = des.getPropArray(des.getProp(data, 'labels'), 'label');
        var labelsCount = labels.length;

        for (var i = 0; i < labelsCount; i++) {
            var label = new anychart.plots.gaugePlot.gauges.labels.GaugeLabel();
            label.deserialize(labels[i], this.stylesList);
            this.labels_.push(label);
        }

    }
};

/**
 * @param {Object} value Pointer new config.
 * @param {String} opt_pointerName Pointer name (if supported).
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.setPointerValue = function (value, opt_pointerName) {
};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.frames.GaugeFrame}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.frame = null;

/**
 * @protected
 * @param {Object} config. Gauge frame config.
 * @return {anychart.plots.gaugePlot.frames.GaugeFrame} Gauge frame.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.createFrame = goog.abstractMethod;

/**
 * @protected
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.svgManager = null;

/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.sprite = null;
/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.tooltipSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getTooltipSprite = function () {
    return this.tooltipSprite_;
};
/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.contentSprite = null;

/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.beforeContentSprite = null;
/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.labelsBeforeSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getLabelsBeforeSprite = function () {
    return this.labelsBeforeSprite_;
};
/**
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.labelsAfterSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.getLabelsAfterSprite = function () {
    return this.labelsAfterSprite_;
};

/**
 * Initialize gauge visual structure.
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {anychart.svg.SVGSprite} tooltipsContainer
 * @return {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.initialize = function (svgManager, bounds, tooltipsContainer) {
    this.svgManager = svgManager;
    this.setBounds(bounds);
    this.sprite = new anychart.svg.SVGSprite(this.svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite.setId('BaseGauge');

    this.tooltipSprite_ = tooltipsContainer;

    if (this.frame)
        this.sprite.appendSprite(this.frame.initialize(this.svgManager, this.bounds));

    //content sprite
    this.contentSprite = new anychart.svg.SVGSprite(this.svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.contentSprite.setId('BaseGaugeContentSprite');

    //before content sprite
    this.beforeContentSprite = new anychart.svg.SVGSprite(this.svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.beforeContentSprite.setId('BaseGaugeBeforeContentSprite');

    //labels before sprite
    this.labelsBeforeSprite_ = new anychart.svg.SVGSprite(this.svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.labelsBeforeSprite_.setId('GaugesLabelBeforeSprite');

    //labels after sprite
    this.labelsAfterSprite_ = new anychart.svg.SVGSprite(this.svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.labelsAfterSprite_.setId('GaugesLabelAfterSprite');

    this.contentSprite.appendSprite(this.beforeContentSprite);
    this.sprite.appendSprite(this.labelsBeforeSprite_);
    this.sprite.appendSprite(this.contentSprite);
    this.sprite.appendSprite(this.labelsAfterSprite_);

    var i, count;

    //labels
    for (i = 0, count = this.labels_.length; i < count; i++) {
        var label = this.labels_[i];
        if (label.underPoints()) this.labelsBeforeSprite_.appendSprite(label.initialize(svgManager, this.bounds));
        else this.labelsAfterSprite_.appendSprite(label.initialize(svgManager, this.bounds));
    }

    //children
    for (i = 0, count = this.children.length; i < count; i++)
        this.sprite.appendSprite(this.children[i].initialize(this.svgManager, this.bounds, tooltipsContainer));

    return this.sprite;
};

/**
 * Draw gauge.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.draw = function () {
    var i, count;
    //frame
    if (this.frame) this.frame.draw();
    //labels
    for (i = 0, count = this.labels_.length; i < count; i++)
        this.labels_[i].draw();
    //children
    for (i = 0, count = this.children.length; i < count; i++)
        this.children[i].draw();
};

/**
 * Resize gauge.
 * @param {anychart.utils.geom.Rectangle} bounds New bounds.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.resize = function (bounds) {
    var i, count;
    //bounds
    this.setBounds(bounds);

    //frame
    if (this.frame) this.frame.resize(this.bounds);

    //labels
    for (i = 0, count = this.labels_.length; i < count; i++)
        this.labels_[i].resize(this.bounds);

    //children
    for (i = 0, count = this.children.length; i < count; i++)
        this.children[i].resize(this.bounds);
};

/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds Gauge bounds.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.setBounds = function (bounds) {
    this.bounds.x = bounds.x + this.x * bounds.width;
    this.bounds.y = bounds.y + this.y * bounds.height;
    this.bounds.width = bounds.width * this.width;
    this.bounds.height = bounds.height * this.height;

    this.applyMargin(this.bounds);
};

/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} rect Target rectangle.
 */
anychart.plots.gaugePlot.gauges.GaugeBase.prototype.applyMargin = function (rect) {
    var w = rect.width;
    var h = rect.height;

    rect.setLeft(rect.getLeft() + this.margin_.getLeft() * w / 100);
    rect.setRight(rect.getRight() - this.margin_.getRight() * w / 100);
    rect.setTop(rect.getTop() + this.margin_.getTop() * h / 100);
    rect.setBottom(rect.getBottom() - this.margin_.getBottom() * h / 100);
};

/**
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.gauges.LabelGauge = function () {
    goog.base(this);
    this.textElement_ = new anychart.plots.gaugePlot.visual.text.GaugeBaseTextElement();
};
goog.inherits(anychart.plots.gaugePlot.gauges.LabelGauge,
    anychart.plots.gaugePlot.gauges.GaugeBase);

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.textElement_ = null;
anychart.plots.gaugePlot.gauges.LabelGauge.prototype.textSprite_ = null;

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    this.textElement_.deserialize(data);
};

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.initialize = function(svgManager, bounds, tooltipsContainer) {
    goog.base(this, 'initialize', svgManager, bounds, tooltipsContainer);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite.setId('LabelGauge');
    this.textElement_.initSize(svgManager, this.textElement_.getText());
    this.textSprite_ = this.textElement_.createSVGText(svgManager);
    this.textSprite_.setId('LabelGaugeTextSprite');
    this.sprite.appendSprite(this.textSprite_);
    return this.sprite;
};

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.setBounds = function(newBounds) {
    goog.base(this, 'setBounds', newBounds);
    this.bounds.x = newBounds.x;
    this.bounds.y = newBounds.y;
    this.bounds.width = newBounds.width;
    this.bounds.height = newBounds.height;
};
anychart.plots.gaugePlot.gauges.LabelGauge.prototype.draw = function() {
    this.textElement_.update(
        this.textSprite_.getSVGManager(),
        this.textSprite_,
        this.bounds.x,
        this.bounds.y,
        this.bounds.width,
        this.bounds.height
    );
};

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.resize = function(newBounds) {
    goog.base(this, 'resize', newBounds);
    this.draw();
};

anychart.plots.gaugePlot.gauges.LabelGauge.prototype.createFrame = function(data) {
    return null;
};
