goog.provide('anychart.plots.gaugePlot.layout');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.layout.Align enum
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.gaugePlot.layout.Align = {
    INSIDE: 0,
    OUTSIDE: 1,
    CENTER: 2
};

