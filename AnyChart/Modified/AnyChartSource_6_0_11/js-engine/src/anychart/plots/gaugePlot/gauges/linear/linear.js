goog.provide('anychart.plots.gaugePlot.gauges.linear');

goog.require('anychart.plots.gaugePlot.gauges.axisBased');
goog.require('anychart.plots.gaugePlot.frames.linear');
goog.require('anychart.plots.gaugePlot.gauges.linear.markers');
goog.require('anychart.plots.gaugePlot.gauges.linear.pointers');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.linear.LinearGauge class
//
//------------------------------------------------------------------------------
/**
 * Linear gauge.
 * @constructor
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge = function () {
    anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.call(this);
    this.horizontal_ = false;
    this.startMargin_ = 0;
    this.endMargin_ = 0;
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.LinearGauge,
    anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.horizontal_ = false;

/**
 * @return {boolean}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.isHorizontal = function () {
    return this.horizontal_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.startMargin_ = 0;

/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.getStartMargin = function () {
    return this.startMargin_;
};
/**
 * @param {number} value
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.setStartMargin = function (value) {
    this.startMargin_ = value;
};
/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.endMargin_ = 0;
/**
 * @return {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.getEndMargin = function () {
    return this.endMargin_;
};
/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.setEndMargin = function (value) {
    this.endMargin_ = value;
};
//------------------------------------------------------------------------------
//                      Initialize.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.initialize = function(svgManager, bounds, tooltipsContainer) {
    goog.base(this, 'initialize', svgManager, bounds, tooltipsContainer);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite.setId('LinearGauge');
    return this.sprite;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.deserialize = function (config) {
    if (anychart.utils.deserialization.hasProp(config, 'orientation'))
        this.horizontal_ = anychart.utils.deserialization.getLStringProp(config, 'orientation') == 'horizontal';
    goog.base(this, 'deserialize', config);
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.getGaugeType = function () {
    return anychart.plots.gaugePlot.gauges.GaugeType.LINEAR;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.createFrame = function () {
    return new anychart.plots.gaugePlot.frames.linear.LinearGaugeFrame(this);
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.createAxis = function () {
    return new anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis(this);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.createPointerByType = function (type) {
    switch (type) {
        case 'bar':
            return new anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer();
        case 'rangebar':
            return new anychart.plots.gaugePlot.gauges.linear.pointers.RangeBarGaugePointer();
        case 'marker':
            return new anychart.plots.gaugePlot.gauges.linear.pointers.MarkerLinearGaugePointer();
        case 'tank':
            if(this.isHorizontal())
                return new anychart.plots.gaugePlot.gauges.linear.pointers.TankHorizontalPointer();
            else
                return new anychart.plots.gaugePlot.gauges.linear.pointers.TankVerticalPointer();
        case 'thermometer':
            return new anychart.plots.gaugePlot.gauges.linear.pointers.ThermometerLinearGaugePointer();
        default:
            return new anychart.plots.gaugePlot.gauges.linear.pointers.BarLinearGaugePointer();
    }
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.setGradientRotation = function (gradient) {
    if (this.horizontal_ && gradient)
        gradient.setAngle(gradient.getAngle() + 90);
};
/**@inheritDoc*/
anychart.plots.gaugePlot.gauges.linear.LinearGauge.prototype.setBounds = function(newBounds) {
    goog.base(this, 'setBounds', newBounds);
    this.frame.autoFit(this.bounds);
};
//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis class
//
//------------------------------------------------------------------------------
/**
 * Linear gauge axis.
 * @constructor
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis = function (gauge) {
    goog.base(this, gauge);
};
goog.inherits(anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis,
    anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis);

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.pixelPosition_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.position_ = .5;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.startMargin_ = .05;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.endMargin_ = .05;


/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.deserialize = function (config, stylesList) {
    goog.base(this, 'deserialize', config, stylesList);

    if (!this.gauge.isHorizontal()) {
        this.scale.setInverted(!this.scale.getInverted());
    }

    this.checkTickmark_(this.majorTickmark);
    this.checkTickmark_(this.minorTickmark);

    if (anychart.utils.deserialization.hasProp(config, 'position'))
        this.position_ = anychart.utils.deserialization.getSimplePercentProp(config, 'position');
    if (anychart.utils.deserialization.hasProp(config, 'start_margin'))
        this.startMargin_ = anychart.utils.deserialization.getSimplePercentProp(config, 'start_margin');
    if (anychart.utils.deserialization.hasProp(config, 'end_margin'))
        this.endMargin_ = anychart.utils.deserialization.getSimplePercentProp(config, 'end_margin');
};

/**
 * @private
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.tickmark.Tickmark}
    */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.checkTickmark_ = function (tickmark) {
    if (!tickmark) return;
    if (this.gauge.isHorizontal()) {
        var tmp = tickmark.getLength();
        tickmark.setLength(tickmark.getWidth());
        tickmark.setWidth(tmp);
    } else if (tickmark.getShapeType() == anychart.plots.gaugePlot.visual.GaugeShapeType.LINE) {
        tickmark.setShapeType(anychart.plots.gaugePlot.visual.GaugeShapeType.H_LINE);
    }
};

/**
 *
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.initializeSize = function () {
    goog.base(this, 'initializeSize');

    var horizontal = this.gauge.isHorizontal();

    var gaugeBounds = this.gauge.getBounds();

    this.logicalTargetSize_ = horizontal ? gaugeBounds.width : gaugeBounds.height;
    this.logicalPerpendicularTargetSize_ = horizontal ? gaugeBounds.height : gaugeBounds.width;
    this.pixelTargetSize_ = Math.min(gaugeBounds.width, gaugeBounds.height);

    this.pixelPosition_ = (horizontal ? gaugeBounds.y : gaugeBounds.x) + this.logicalPerpendicularTargetSize_ * this.position_;

    var w = this.logicalTargetSize_;
    var start = this.startMargin_ * w;
    var end = this.endMargin_ * w;

    var w1 = this.pixelTargetSize_;
    start = Math.max(start, this.gauge.getStartMargin() * w1);
    end = Math.max(end, this.gauge.getEndMargin() * w1);

    this.pixelSize_ = this.size * w1;

    if (horizontal) {
        start += gaugeBounds.x;
        end = gaugeBounds.getRight() - end;
    } else {
        start += gaugeBounds.y;
        end = gaugeBounds.getBottom() - end;
    }

    this.scale.setPixelRange(start, end);
    this.scale.calculate();
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.pixelSize_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.pixelTargetSize_ = NaN;
/**
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getPixelTargetSize = function () {
    return this.pixelTargetSize_;
};

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.logicalTargetSize_ = NaN;

/**
 * @private
 * @type {number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.logicalPerpendicularTargetSize_ = NaN;

/**
 * @param {anychart.plots.gaugePlot.layout.Align} align
 * @param {number} padding
 * @param {number} size
 * @param {boolean} opt_isPixelSize (false)
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getPosition = function (align, padding, size, opt_isPixelSize) {
    if (opt_isPixelSize == undefined) opt_isPixelSize = false;

    var w = this.pixelTargetSize_;
    switch (align) {
        case anychart.plots.gaugePlot.layout.Align.INSIDE:
            return this.pixelPosition_ - w * (opt_isPixelSize ? padding : (padding + size)) - (opt_isPixelSize ? size : 0) - this.pixelSize_ / 2;
        case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
            return this.pixelPosition_ + w * padding + this.pixelSize_ / 2;
        default:
            return this.pixelPosition_ - (opt_isPixelSize ? 1 : w) * size / 2 + w * padding;
    }
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getScaleBarBounds = function (scaleBar) {
    var sRect = new anychart.utils.geom.Rectangle();
    var gaugeBounds = this.gauge.getBounds();
    sRect.x = gaugeBounds.x;
    sRect.y = gaugeBounds.y;
    var w = this.pixelTargetSize_;
    var start, h;

    var isBarShape = scaleBar.getShapeType() == anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;
    if (isBarShape) {
        start = this.getPosition(scaleBar.getAlign(), scaleBar.getPadding(), scaleBar.getSize());
        h = w * scaleBar.getSize();
    } else {
        start = this.getPosition(scaleBar.getAlign(), scaleBar.getPadding(), 0);
        h = 0;
    }

    var startMargin = this.startMargin_ * this.logicalTargetSize_;
    var endMargin = this.endMargin_ * this.logicalTargetSize_;

    var w1 = this.pixelTargetSize_;

    startMargin = Math.max(startMargin, this.gauge.getStartMargin() * w1);
    endMargin = Math.max(endMargin, this.gauge.getEndMargin() * w1);

    if (this.gauge.isHorizontal()) {
        sRect.y = start;
        sRect.height = h;
        sRect.x += startMargin;
        sRect.width = gaugeBounds.width - startMargin - endMargin;
    } else {
        sRect.x = start;
        sRect.width = h;
        sRect.y += startMargin;
        sRect.height = gaugeBounds.height - startMargin - endMargin;
    }
    return sRect;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getScaleBarPath = function (svgManager, scaleBar) {
    var sRect = this.getScaleBarBounds(scaleBar);
    var isBarShape = scaleBar.getShapeType() == anychart.plots.gaugePlot.gauges.axisBased.axis.ScaleBarShapeType.BAR;
    var res = '';
    if (isBarShape) {
        res += svgManager.pMove(sRect.x, sRect.y);
        res += svgManager.pLine(sRect.getRight(), sRect.y);
        res += svgManager.pLine(sRect.getRight(), sRect.getBottom());
        res += svgManager.pLine(sRect.x, sRect.getBottom());
        res += svgManager.pFinalize();
    } else {
        res += svgManager.pMove(sRect.x, sRect.y);
        res += svgManager.pLine(sRect.getRight(), sRect.getBottom());
        res += svgManager.pFinalize();
    }

    return res;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.drawTickmark = function (tickmark, pixValue, opt_color) {
    var svgManager = this.sprite.getSVGManager();
    var r = this.pixelTargetSize_;
    var w = tickmark.getLength() * r;
    var h = tickmark.getWidth() * r;
    var size = Math.min(w, h);

    var x = 0;
    var y = 0;

    if (this.gauge.isHorizontal()) {
        y = this.getPosition(tickmark.getAlign(), tickmark.getPadding(), h, true);
        x = pixValue - w / 2;
    } else {
        x = this.getPosition(tickmark.getAlign(), tickmark.getPadding(), w, true);
        y = pixValue - h / 2;
    }

    var res = tickmark.drawTickmark(svgManager, x, y, size, w, h, opt_color);
    res.setRotation(res.getRotation() + tickmark.getRotation());
    return res;
};
/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.createLabels = function(axis) {
    return new anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels(axis);
};
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getLabelPosition = function (pixValue) {
    var res = new anychart.utils.geom.Point();
    var labelBounds = this.labels.getTextElement().getBounds();

    if (this.gauge.isHorizontal()) {
        res.x = pixValue - labelBounds.width / 2;
        res.y = this.getPosition(this.labels.getAlign(), this.labels.getPadding(), labelBounds.height, true);
    } else {
        res.y = pixValue - labelBounds.height / 2;
        res.x = this.getPosition(this.labels.getAlign(), this.labels.getPadding(), labelBounds.width, true);
    }

    return res;
};
/**
 * @param {Number} position
 * @param {Number} pixelValue
 * @param {Number} point
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.setPoint = function (position, pixelValue, point) {
    if (this.gauge.isHorizontal()) {
        point.x = pixelValue;
        point.y = position;
    } else {
        point.x = position;
        point.y = pixelValue;
    }
};
/**
 * ND: Needs doc!
 * @param {uint} align
 * @param {Number} padding
 * @param {Number} width
 * @param {Boolean} isFirstPoint
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.linear.LinearGaugeAxis.prototype.getComplexPosition = function (align, padding, width, isFirstPoint) {
    var w = this.pixelTargetSize_;
    switch (align) {
        case anychart.plots.gaugePlot.layout.Align.INSIDE:
            return this.pixelPosition_ - w * (padding + (isFirstPoint ? 0 : width)) - this.pixelSize_ / 2;
        case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
            return this.pixelPosition_ + w * (padding + (isFirstPoint ? 0 : width)) + this.pixelSize_ / 2;
        default:
        case anychart.plots.gaugePlot.layout.Align.CENTER:
            return this.pixelPosition_ + w * width * (isFirstPoint ? (-.5) : (.5));
    }
};