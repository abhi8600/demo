goog.provide('anychart.plots.gaugePlot.gauges.axisBased');

goog.require('anychart.plots.gaugePlot.elements');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.pointer');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.axis');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.scales');


//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge class
//
//------------------------------------------------------------------------------
/**
 * Axis based abstract gauge.
 * @constructor
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge = function () {
    anychart.plots.gaugePlot.gauges.GaugeBase.call(this);
    //axes
    this.axes = [];
    this.axesMap_ = {};
    //pointers
    this.pointers = [];
    this.pointersMap = {};
    //elements
    this.label_ = new anychart.plots.gaugePlot.elements.GaugeLabel();
    this.label_.setGauge(this);
    this.tooltip_ = new anychart.plots.gaugePlot.elements.GaugeTooltip();
};
goog.inherits(anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge,
    anychart.plots.gaugePlot.gauges.GaugeBase);
//------------------------------------------------------------------------------
//                          Axis.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {Array.<anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis>}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.axes = null;

/**
 * @private
 * @type {Object.<string, anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis>}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.axesMap_ = null;
//------------------------------------------------------------------------------
//                        Pointers.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {Array.<anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer>}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.pointers = null;

/**
 * @protected
 * @type {Object.<string, anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer>
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.pointersMap = null;
//------------------------------------------------------------------------------
//                          Elements.
//------------------------------------------------------------------------------
/**
 * @protected
 * @type {anychart.plots.gaugePlot.elements.GaugeLabel}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.label_ = null;
/**
 * @protected
 * @type {anychart.plots.gaugePlot.elements.GaugeLabelTooltip}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.tooltip_ = null;
//------------------------------------------------------------------------------
//                      Serialization.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.serialize = function (opt_res) {
    var res = goog.base(this, "serialize", opt_res);
    res['pointers'] = [];
    for (var i = 0; i < this.pointers.length; i++)
        res['pointers'].push(this.pointers[i].serialize());
    return res;
};

/**
 * @protected
 * @param {Object} config Axis config.
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.createAxis = goog.abstractMethod;

/**
 * @protected
 * @param {string} pointerType
 * @return {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer}
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.createPointerByType = goog.abstractMethod;

/**
 * @protected
 * @param {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} axis Gauge axis.
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.registerAxis = function (axis) {
    this.axes.push(axis);
    if (axis.hasName())
        this.axesMap_[axis.getName()] = axis;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.deserialize = function (config) {
    goog.base(this, 'deserialize', config);
    var des = anychart.utils.deserialization;

    /** @type {anychart.plots.gaugePlot.gauges.axisBased.axis.GaugeAxis} */
    var axis;
    var i;
    axis = this.createAxis(des.getProp(config, 'axis'), this.stylesList);
    axis.setName('default');
    axis.deserialize(des.getProp(config, 'axis'), this.stylesList);
    this.registerAxis(axis);
    var defaultAxis = axis;

    if (des.hasProp(config, 'extra_axes')) {
        var extraAxes = des.getPropArray(des.getProp(config, 'extra_axes'), 'axis');
        for (i = 0; i < extraAxes.length; i++) {
            axis = this.createAxis(extraAxes[i], this.stylesList);
            axis.deserialize(extraAxes[i], this.stylesList);
            if (des.hasProp(extraAxes[i], 'name'))
                axis.setName(des.getLStringProp(extraAxes[i], 'name'));
            this.registerAxis(axis);
        }
    }

    if (des.hasProp(config, 'pointers')) {
        var pointers = des.getProp(config, 'pointers');
        this.label_.deserializeGlobalSettings(des.getProp(pointers, 'label'), this.stylesList);
        this.tooltip_.deserializeGlobalSettings(des.getProp(pointers, 'tooltip'), this.stylesList);

        var color = null;
        var hatchType = -1;
        var editable = false;
        var selectable = false;
        var handCursor = true;

        if (des.hasProp(pointers, 'color'))
            color = des.getColorProp(pointers, 'color');
        if (des.hasProp(pointers, 'hatch_type'))
            hatchType = anychart.visual.hatchFill.HatchFill.HatchFillType.deserialize(des.getLStringProp(pointers, 'hatch_type'));
        if (des.hasProp(pointers, 'editable'))
            editable = des.getBoolProp(pointers, 'editable');
        if (des.hasProp(pointers, 'allow_select'))
            selectable = des.getBoolProp(pointers, 'allow_select');
        if (des.hasProp(pointers, 'use_hand_cursor'))
            handCursor = des.getBoolProp(pointers, 'use_hand_cursor');

        var pointersList = des.getPropArray(pointers, 'pointer');
        for (i = 0; i < pointersList.length; i++) {
            var pointerConfig = pointersList[i];
            if (des.hasProp(pointerConfig, 'value') ||
                (des.hasProp(pointerConfig, 'start') && des.hasProp(pointerConfig, 'end'))) {
                var pointer = this.createPointerByType(des.getEnumProp(pointerConfig, 'type'));
                if (!pointer) continue;
                pointer.setGauge(this);
                pointer.setEditable(editable);
                pointer.setSelectable(selectable);
                pointer.setUseHandCursor(handCursor);
                pointer.setLabel(this.label_.copy());
                pointer.setTooltip(this.tooltip_.copy());
                if(color) pointer.setColor(color);
                pointer.setHatchType(hatchType);
                pointer.deserialize(pointerConfig, this.stylesList);
                if (des.hasProp(pointerConfig, 'axis')) {
                    var mapAxis = this.axesMap_[des.getLStringProp(pointerConfig, 'axis')];
                    if (mapAxis) pointer.setAxis(mapAxis);
                    else pointer.setAxis(defaultAxis);
                } else pointer.setAxis(defaultAxis);

                this.pointers.push(pointer);
                if (pointer.hasName()) {
                    this.pointersMap[pointer.getName()] = pointer;
                }
                this.checkPointer(pointer);
                pointer.getAxis().checkPointer(pointer);
            }
        }
    }
};

/**
 * @protected
 * @param {anychart.plots.gaugePlot.gauges.axisBased.pointer.GaugePointer} pointer Gauge pointer.
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.checkPointer = function (pointer) {
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.initialize = function (svgManager, bounds, tooltipContainer) {
    goog.base(this, 'initialize', svgManager, bounds, tooltipContainer);
    var i, count;
    for (i = 0, count = this.axes.length; i < count; i++)
        this.beforeContentSprite.appendSprite(this.axes[i].initialize(this.svgManager));
    for (i = 0, count = this.pointers.length; i < count; i++)
        this.contentSprite.appendSprite(this.pointers[i].initialize(svgManager));
    for (i = 0, count = this.axes.length; i < count; i++)
        this.axes[i].initializeSize();
    return this.sprite;
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.draw = function () {
    goog.base(this, 'draw');
    var i, count;
    for (i = 0, count = this.axes.length; i < count; i++)
        this.axes[i].draw();
    for (i = 0, count = this.pointers.length; i < count; i++)
        this.pointers[i].draw();
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.resize = function (bounds) {
    goog.base(this, 'resize', bounds);
    var i, count;
    for (i = 0, count = this.axes.length; i < count; i++)
        this.axes[i].resize();
    for (i = 0, count = this.pointers.length; i < count; i++)
        this.pointers[i].resize();
};

/** @inheritDoc */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.setPointerValue = function (value, opt_pointerName) {
    if (opt_pointerName == null) return;
    pointer = this.pointersMap[opt_pointerName];
    if (!pointer) return;
    pointer.updateValue(value);
};

/**
 * @param {anychart.visual.background.Background} background
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.setBackgroundRotation = function (background) {
    if (background) {
        if (background.hasFill() && background.getFill().getGradient())
            this.setGradientRotation(background.getFill().getGradient());
        if (background.hasBorder() && background.getBorder().getGradient())
            this.setGradientRotation(background.getBorder().getGradient());
    }
};

/**
 * @param {anychart.visual.gradient.Gradient} gradient
 */
anychart.plots.gaugePlot.gauges.axisBased.AxisBasedGauge.prototype.setGradientRotation = function (gradient) {
};


