goog.provide('anychart.plots.gaugePlot.innerTemplates');

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager class
//
//------------------------------------------------------------------------------
/**
 * Templates manager for gauge plot.
 * @constructor
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager = function () {
};

/**
 * Apply template to config.
 * @param {Object} config config.
 * @param {Array.<Object>} templatesList array with templates.
 * @param {string} defaultTemplateName default template name.
 * @return {Object} config with applied template.
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager.prototype.apply = function (config, templatesList, defaultTemplateName) {
    var actual = this.getTemplateByName_(templatesList, 'default');

    var templateName = anychart.utils.deserialization.hasProp(config, 'template') ?
        anychart.utils.deserialization.getLStringProp(config, 'template') : defaultTemplateName;

    if (templateName) {
        var targetList = [];
        var template = this.getTemplateByName_(templatesList, templateName);
        while (template) {
            targetList.push(template);
            template = anychart.utils.deserialization.hasProp(template, 'parent') ?
                this.getTemplateByName_(templatesList, anychart.utils.deserialization.getLStringProp(template, 'parent')) : null;
        }

        for (var i = targetList.length - 1; i >= 0; i--)
            actual = this.mergeGauges_(actual, targetList[i]);
    }

    return this.mergeGauges_(actual, config);
};

/**
 * Merge gauges.
 * @private
 * @param {Object} actual actual configuration.
 * @param {Object} newConfig new configuration (more priority).
 * @return {Object} merged gauge config.
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager.prototype.mergeGauges_ = function (actual, newConfig) {
    var des = anychart.utils.deserialization;
    var res = anychart.utils.JSONUtils.merge(actual, newConfig);
    /* lowercase style names */
    if (actual) this.lowerCaseStyleNames_(actual);
    if (newConfig) this.lowerCaseStyleNames_(newConfig);
    var styles = anychart.utils.JSONUtils.mergeWithOverride(des.getProp(actual, 'styles'),
        des.getProp(newConfig, 'styles'));
    if (styles) des.setProp(res, 'styles', styles);

    if (des.hasProp(res, 'defaults')) {
        var defaults = des.getProp(res, 'defaults');
        if (des.hasProp(defaults, 'axis')) {
            var defaultAxis = des.getProp(defaults, 'axis');

            //axis
            if (des.hasProp(res, 'axis')) {
                var resAxis = this.mergeAxis_(defaultAxis, des.getProp(actual, 'axis'));
                resAxis = this.mergeAxis_(resAxis, des.getProp(res, 'axis'));
                des.setProp(res, 'axis', resAxis);
            }

            //extra axis
            if (des.hasProp(res, 'extra_axes')) {
                var extraRes = [];
                var extraAxesData = des.getProp(res, 'extra_axes');
                var extraAxes = des.getPropArray(extraAxesData, 'axis');
                var axisCount = extraAxes.length;

                for (var i = 0; i < axisCount; i++)
                    extraRes.push(this.mergeAxis_(defaultAxis, extraAxes[i]));

                des.setProp(extraAxesData, 'axis', extraRes);
            }
        }
    }

    return res;
};

/**
 * @private
 * @param {Object} template Axis template.
 * @param {Object} data Axis config.
 * @return {Object} merged axis config.
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager.prototype.mergeAxis_ = function (template, data) {
    if (data == null && template != null) return template;
    if (data != null && template == null) return data;
    var res = anychart.utils.JSONUtils.merge(template, data);

    anychart.utils.deserialization.setProp(res, 'color_ranges', anychart.utils.JSONUtils.mergeWithOverrideByName(
        anychart.utils.deserialization.getProp(template, 'color_ranges'),
        anychart.utils.deserialization.getProp(data, 'color_ranges'), 'color_range'));
    anychart.utils.deserialization.setProp(res, 'custom_labels', anychart.utils.JSONUtils.mergeWithOverrideByName(
        anychart.utils.deserialization.getProp(template, 'custom_labels'),
        anychart.utils.deserialization.getProp(data, 'custom_labels'), 'custom_label'));
    anychart.utils.deserialization.setProp(res, 'trendlines', anychart.utils.JSONUtils.mergeWithOverrideByName(
        anychart.utils.deserialization.getProp(template, 'trendlines'),
        anychart.utils.deserialization.getProp(data, 'trendlines'), 'trendline'));
    return res;
};

/**
 * @private
 * @param {Object} config config which style names should be lowercased.
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager.prototype.lowerCaseStyleNames_ = function (config) {
    if (!anychart.utils.deserialization.hasProp(config, 'styles')) return;
    var styles = anychart.utils.deserialization.getPropArray(config, 'styles');
    for (var i = 0; i < styles.length; i++) {
        if (anychart.utils.deserialization.hasProp(styles[i], 'name'))
            styles[i]['name'] = anychart.utils.deserialization.getLStringProp(styles[i], 'name');
    }
};

/**
 * Get template by name.
 * @private
 * @param {Array<Object>} templates templates list.
 * @param {string} name template name.
 * @return {Object} template object.
 */
anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager.prototype.getTemplateByName_ = function (templates, name) {
    name = name.toLowerCase();
    for (var i = 0; i < templates.length; i++) {
        var tpl = templates[i];
        if (anychart.utils.deserialization.hasProp(tpl, 'name') &&
            anychart.utils.deserialization.getLStringProp(tpl, 'name') == name) {
            return tpl;
        }
    }
    return null;
};
