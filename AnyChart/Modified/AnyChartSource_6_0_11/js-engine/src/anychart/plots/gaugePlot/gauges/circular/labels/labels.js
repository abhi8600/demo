/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.gauges.circular.labels');
goog.require('anychart.plots.gaugePlot.gauges.axisBased.axis');
/**
 * Class represents circular gauge axis labels.
 * @constructor
 * @extends {anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels}
 * @param {} axis Axis.
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels = function (axis) {
    goog.base(this, axis);
    this.rotateCircular_ = true;
    this.autoOrientation_ = false;
};
goog.inherits(anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels,
    anychart.plots.gaugePlot.gauges.axisBased.axis.AxisLabels);

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.rotateCircular_ = null;

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.autoOrientation_ = null;

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.isInverted_ = null;

/**
 * @param {Object} config
 * @param {Number} axisSize
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.deserialize = function (config, axisSize) {
    var des = anychart.utils.deserialization;
    goog.base(this, 'deserialize', config, axisSize);

    if (des.hasProp(config, 'rotate_circular'))
        this.rotateCircular_ = des.getBoolProp(config, 'rotate_circular');
    if (des.hasProp(config, 'auto_orientation'))
        this.autoOrientation_ = des.getBoolProp(config, 'auto_orientation');
};

/**
 * @param {Number} size
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.getPercentFontSize = function (size) {
    return this.axis_.getPixelRadius(size);
};

/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.setAngle = function (value) {
    if (this.rotateCircular_) {
        var invert = this.autoOrientation_ && (Math.sin(value * Math.PI / 180) > 0);
        var rotation = this.textElement_.getRotationAngle();
        rotation = (!invert) ? (rotation + value + 90) : (rotation + value - 90);
        this.textElement_.setRotationAngle(-rotation);
    }
};

/**
 * @param {Number} value
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.clearAngle = function (value) {
    if (this.rotateCircular_) {
        var invert = this.autoOrientation_ && Math.sin(value * Math.PI / 180) > 0;
        var rotation = this.textElement_.getRotationAngle();
        rotation = (!invert) ? (rotation - value - 90) : (rotation - value + 90);
        this.textElement_.setRotationAngle(-rotation);
    }
};
/**
 * @param {*} pixVal
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.getRotation = function(pixVal) {
    return this.setAngle(pixVal);
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.getRotateCircularPosition_ = function (textElement, pixValue, align, padding) {
    var pos = new anychart.utils.geom.Point();

    pos.x = this.axis_.getGauge().getPixPivotPoint().x;
    pos.y = this.axis_.getGauge().getPixPivotPoint().y;
    var size = this.textElement_.getBounds().height;

    var r = this.axis_.getRadius(align, padding, 0, true);

    switch (align) {
        case anychart.plots.gaugePlot.layout.Align.INSIDE:
            r -= size / 2;
            break;
        case anychart.plots.gaugePlot.layout.Align.OUTSIDE:
            r += size / 2;
            break;
    }

    var angle = pixValue * Math.PI / 180;
    var cos = Math.round(Math.cos(angle) * 1000) / 1000;
    var sin = Math.round(Math.sin(angle) * 1000) / 1000;

    pos.x += r * cos;
    pos.y += r * sin;

    pos.x -= this.textElement_.getBounds().width / 2;
    pos.y -= this.textElement_.getBounds().height / 2;

    return pos;
};

/**
 * @private
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.getFixedPosition_ = function (textElement, pixVal, align, padding) {
    var pos = new anychart.utils.geom.Point();

    //+.01 for minimal difference with flash
    var r = this.axis_.getRadius(align, padding + .01, 0, false);
    var angle = pixVal * Math.PI / 180;

    var cos = Math.round(Math.cos(angle) * 1000) / 1000;
    var sin = Math.round(Math.sin(angle) * 1000) / 1000;

    var mAlign = anychart.plots.gaugePlot.layout.Align;

    var labelBounds = textElement.getRotatedBounds();

    var dx = labelBounds.width / 2;
    var dy = labelBounds.height / 2;

    switch (align) {
        case mAlign.OUTSIDE:
            r += dx * Math.abs(cos) + dy * Math.abs(sin);
            break;
        case mAlign.INSIDE:
            r -= dx * Math.abs(cos) + dy * Math.abs(sin);
            break;
    }

    pos.x = this.axis_.getGauge().getPixPivotPoint().x + r * cos - dx;
    pos.y = this.axis_.getGauge().getPixPivotPoint().y + r * sin - dy;

    return pos;
};


/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.getPosition = function (textElement, pixVal, align, padding) {
    if (textElement == this.textElement_ && this.rotateCircular_)
        return this.getRotateCircularPosition_(textElement, pixVal, align, padding);
    return this.getFixedPosition_(textElement, pixVal, align, padding)
};

/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.setCenter = function (textElement, sin, cos, pos) {
    var textAngle = textElement.getRotationAngle() * Math.PI / 180;

    var txtSin = Math.sin(textAngle),
        txtCos = Math.cos(textAngle);

    if (textElement != this.textElement_) {
        if (this.align != anychart.plots.gaugePlot.layout.Align.CENTER) {
            var d = (this.align == anychart.plots.gaugePlot.layout.Align.OUTSIDE) ? 1 : -1;

            if (txtCos != 0) {
                pos.x += d * this.textElement_.getNonRotatedBounds().height / 2 * cos / txtCos;
                pos.y += d * this.textElement_.getNonRotatedBounds().height / 2 * sin / txtCos;
            } else {
                pos.x -= this.textElement_.getNonRotatedBounds().height / 2;
                if (sin < 0) pos.y -= this.textElement_.getNonRotatedBounds().width
            }
        }
    }

    //center
    if (txtSin > 0 && txtCos < 0)
        txtCos = -txtCos;

    if (txtSin < 0 && txtCos < 0) {
        txtCos = -txtCos;
        txtSin = -txtSin;
    }

    if (txtSin < 0 && txtCos > 0) {
        txtSin = -txtSin;
    }

    pos.x -= (this.textElement_.getNonRotatedBounds().width / 2 * txtSin +
        this.textElement_.getNonRotatedBounds().height / 2 * txtCos);
    pos.y -= (this.textElement_.getNonRotatedBounds().width / 2 * txtCos +
        this.textElement_.getNonRotatedBounds().height / 2 * txtSin);
};

/**
 * @inheritDoc
 */
anychart.plots.gaugePlot.gauges.circular.labels.CircularAxisLabels.prototype.draw = function(svgManager, pos) {
    var sprite = goog.base(this, 'draw', svgManager, pos);
    var scale = this.axis_.getScale();
    var value = scale.localTransform(this.axis_.tmpValue_);
    if (this.rotateCircular_) {
        this.setAngle(value);
        this.textElement_.execRotation(sprite.textSprite_);
        this.clearAngle(value);
    }
    return sprite;
};
