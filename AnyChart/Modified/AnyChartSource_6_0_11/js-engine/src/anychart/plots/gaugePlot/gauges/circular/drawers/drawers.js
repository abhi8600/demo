/**
 * Copyright 2012 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer}</li>
 * <ul>
 */

goog.provide('anychart.plots.gaugePlot.gauges.circular.drawers');

//------------------------------------------------------------------------------
//
//                              NeedleDrawer class
//
//------------------------------------------------------------------------------

/**
 * Class responsive for drawing needle pointer.
 * @constructor
 *
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer = function() {};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.pixelRadius_ = null;

/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.getPixelRadius = function () {
    return this.pixelRadius_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.pixelBaseRadius_ = null;

/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.getPixelBaseRadius = function () {
    return this.pixelBaseRadius_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.pixelThickness_ = null;

/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.getPixelThickness = function () {
    return this.pixelThickness_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.pixelPointThickness_ = null;

/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.getPixelPointThickness = function () {
    return this.pixelPointThickness_;
};

/**
 * ND: Needs doc!
 * @private
 * @type {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.pixelPointRadius_ = null;

/**
 * ND: Needs doc!
 * @return {Number}
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.getPixelPointRadius = function () {
    return this.pixelPointRadius_;
};

/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGSprite} container
 * @param {anychart.plots.gaugePlot.gauges.circular.pointers.CircularGaugePointer} pointer
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.draw = function (container, pointer) {
    var styleState = pointer.getCurrentStyleState(pointer.getStyle()),
        needle = styleState.getNeedle(),
        fill = styleState.getFill(),
        axis = pointer.getAxis(),
        angle = 0,
        dAngle = 0,
        center = new anychart.utils.geom.Point(0, 0),
        pt1 = new anychart.utils.geom.Point(),
        pt2 = new anychart.utils.geom.Point(),
        pt3 = new anychart.utils.geom.Point(),
        pt4 = new anychart.utils.geom.Point(),
        pt5 = new anychart.utils.geom.Point(),
        tmp = new anychart.utils.geom.Point();

    if (fill != null && fill.type_ == anychart.visual.fill.Fill.FillType.GRADIENT && fill.gradient_.type_ == anychart.visual.gradient.Gradient.GradientType.LINEAR) {
        dAngle = fill.gradient_.angle_;
        fill.gradient_.angle_ += angle;
    }

    angle *= Math.PI / 180;

    this.pixelRadius_ = axis.getPixelRadius((needle.isRadiusAxisSizeDepend ? axis.size : 1) * needle.radius);
    this.pixelBaseRadius_ = axis.getPixelRadius((needle.isBaseRadiusAxisSizeDepend ? axis.size : 1) * needle.baseRadius);
    this.pixelThickness_ = axis.getPixelRadius((needle.isThicknessAxisSizeDepend ? axis.size : 1) * needle.thickness);
    this.pixelPointThickness_ = axis.getPixelRadius((needle.isPointThicknessAxisSizeDepend ? axis.size : 1) * needle.pointThickness);
    this.pixelPointRadius_ = axis.getPixelRadius((needle.isPointRadiusAxisSizeDepend ? axis.size : 1) * needle.pointRadius);

    var sin = Math.sin(angle),
        cos = Math.cos(angle),
        dx = this.pixelThickness_ / 2 * sin,
        dy = this.pixelThickness_ / 2 * cos;

    tmp.x = center.x + this.pixelBaseRadius_ * cos;
    tmp.y = center.y + this.pixelBaseRadius_ * sin;

    pt1.x = tmp.x - dx;
    pt1.y = tmp.y + dy;

    pt2.x = tmp.x + dx;
    pt2.y = tmp.y - dy;

    dx = this.pixelPointThickness_ / 2 * sin;
    dy = this.pixelPointThickness_ / 2 * cos;

    tmp.x = center.x + (this.pixelRadius_ - this.pixelPointRadius_) * cos;
    tmp.y = center.y + (this.pixelRadius_ - this.pixelPointRadius_) * sin;

    pt3.x = tmp.x - dx;
    pt3.y = tmp.y + dy;

    pt4.x = tmp.x + dx;
    pt4.y = tmp.y - dy;

    pt5.x = center.x + this.pixelRadius_ * cos;
    pt5.y = center.y + this.pixelRadius_ * sin;

    var pts = [pt1, pt2, pt4, pt5, pt3],
        bounds = new anychart.utils.geom.Rectangle();

    bounds.x = Math.min(pt1.x, pt2.x, pt3.x, pt4.x, pt5.x);
    bounds.y = Math.min(pt1.y, pt2.y, pt3.y, pt4.y, pt5.y);
    bounds.width = Math.max(pt1.x, pt2.x, pt3.x, pt4.x, pt5.x) - bounds.x;
    bounds.height = Math.max(pt1.y, pt2.y, pt3.y, pt4.y, pt5.y) - bounds.y;

    pointer.setValue(
        (pointer.getValue() > axis.getScale().getMaximum()) ?
            axis.getScale().getMaximum() :
            (pointer.getValue() < axis.getScale().getMinimum()) ?
                axis.getScale().getMinimum() :
                pointer.getValue());

    container.setX(axis.gauge.getPixPivotPoint().x);
    container.setY(axis.gauge.getPixPivotPoint().y);
    container.setRotation(axis.getScale().transformValueToPixel(pointer.getValue()));

    return this.drawNeedle_(pointer.getSVGManager(), pts);
};
/**
 * ND: Needs doc!
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Array<anychart.utils.geom.Point>} pts
 */
anychart.plots.gaugePlot.gauges.circular.drawers.NeedleDrawer.prototype.drawNeedle_ = function (svgManager, pts) {
    var path = '';
    path += svgManager.pMove(pts[0].x, pts[0].y);
    path += svgManager.pLine(pts[1].x, pts[1].y);
    path += svgManager.pLine(pts[2].x, pts[2].y);
    path += svgManager.pLine(pts[3].x, pts[3].y);
    path += svgManager.pLine(pts[4].x, pts[4].y);
    path += svgManager.pLine(pts[0].x, pts[0].y);
    path += svgManager.pFinalize();
    return path;
};
