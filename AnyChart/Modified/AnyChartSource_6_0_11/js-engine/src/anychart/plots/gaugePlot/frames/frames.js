/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.gaugePlot.frames.GaugeFrame}</li>
 * <ul>
 */
goog.provide('anychart.plots.gaugePlot.frames');
goog.require('anychart.plots.gaugePlot.visual');

/**
 * @enum {int}
 */
anychart.plots.gaugePlot.frames.FrameType = {
    CIRCULAR: 0,
    AUTO: 1,
    RECT: 2,
    ROUNDED_RECT: 3
};

//------------------------------------------------------------------------------
//
//     anychart.plots.gaugePlot.frames.GaugeFrame class
//
//------------------------------------------------------------------------------
/**
 * Base gauge frame.
 * @param {anychart.plots.gaugePlot.gauges.GaugeBase} gauge Gauge.
 * @constructor
 */
anychart.plots.gaugePlot.frames.GaugeFrame = function(gauge) {
    this.gauge = gauge;
    this.padding = 0.15;
    this.type = anychart.plots.gaugePlot.frames.FrameType.RECT;

};

/**
 * @protected
 * @type {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.innerStroke = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.outerStroke = null;

/**
 * @protected
 * @type {anychart.visual.background.Background}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.background = null;

/**
 * @protected
 * @type {anychart.visual.corners.Corners}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.corners = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.frames.FrameType}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.type = null;

/**
 * @protected
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.bounds = null;

/**
 * @protected
 * @type {anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.gauge = null;

/**
 * @protected
 * @type {number}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.padding = null;

/**
 * @protected
 * @type {boolean}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.cropContent = false;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.autoFit_ = true;
/**
 * ND: Needs doc!
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.allowAutoFit = function() {
    return this.autoFit_;
};

/**
 * @param {Object} config Frame config.
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.deserialize = function(config) {
    var des = anychart.utils.deserialization;
    this.padding = 0;
    if (des.hasProp(config, 'enabled') &&
        !des.getBoolProp(config, 'enabled'))
           return;

    if (des.hasProp(config, 'type')) {
        switch (des.getLStringProp(config, 'type')) {
            case 'circular': this.type = anychart.plots.gaugePlot.frames.FrameType.CIRCULAR; break;
            case 'auto': this.type = anychart.plots.gaugePlot.frames.FrameType.AUTO; break;
            case 'rectangular': this.type = anychart.plots.gaugePlot.frames.FrameType.RECT; break;
            case 'roundedrectangular': this.type = anychart.plots.gaugePlot.frames.FrameType.ROUNDED_RECT; break;
        }
    }

    if (des.hasProp(config, 'auto_fit'))
        this.autoFit_ = des.getBoolProp(config, 'auto_fit');
    if (des.hasProp(config, 'crop_content'))
        this.cropContent = des.getBoolProp(config, 'crop_content');

    if (des.hasProp(config, 'padding'))
        this.padding = des.getSimplePercentProp(config, 'padding');

    if (des.isEnabled(config, 'inner_stroke')) {
        this.innerStroke = this.createInnerStroke();
        this.innerStroke.deserialize(des.getProp(config, 'inner_stroke'));
    }

    if (des.isEnabled(config, 'outer_stroke')) {
        this.outerStroke = this.createOuterStroke();
        this.outerStroke.deserialize(des.getProp(config, 'outer_stroke'));
    }

    if (des.hasProp(config, 'background')) {
        this.background = this.createBackground();
        this.background.deserialize(des.getProp(config, 'background'));
    }

    if (des.hasProp(config, 'corners')) {
        this.corners = new anychart.visual.corners.Corners();
        this.corners.deserialize(des.getProp(config, 'corners'));
    }
};

/**
 * @protected
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.sprite = null;

/**
 * @protected
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.backgroundSprite = null;

/**
 * @protected
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.innerStrokeSprite = null;

/**
 * @protected
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.outerStrokeSprite = null;

/**
 * @protected
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.svgManager = null;

/**
 * Initialize gauge frame.
 * @param {anychart.svg.SVGManager} svgManager svg manager.
 * @param {anychart.utils.geom.Rectangle} bounds Frame bounds.
 * @return {anychart.svg.SVGSprite} frame sprite.
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.initialize = function(svgManager, bounds) {
    this.svgManager = svgManager;
    this.bounds = bounds;
    this.sprite = new anychart.svg.SVGSprite(this.svgManager);
    if (this.outerStroke && this.outerStroke.isEnabled()) {
        this.outerStrokeSprite = this.outerStroke.initialize(this.svgManager);
        this.sprite.appendSprite(this.outerStrokeSprite);
    }
    if (this.innerStroke && this.innerStroke.isEnabled()) {
        this.innerStrokeSprite = this.innerStroke.initialize(this.svgManager);
        this.sprite.appendSprite(this.innerStrokeSprite);
    }
    if (this.background && this.background.isEnabled()) {
        this.backgroundSprite = this.background.initialize(this.svgManager);
        this.sprite.appendSprite(this.backgroundSprite);
    }
    return this.sprite;
};

/**
 * Draw gauge frame.
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.draw = function() {
    if (this.outerStroke && this.outerStroke.isEnabled()) this.outerStroke.draw(this.outerStrokeSprite, this.bounds);
    if (this.innerStroke && this.innerStroke.isEnabled()) this.innerStroke.draw(this.innerStrokeSprite, this.bounds);
    if (this.background && this.background.isEnabled()) this.background.draw(this.backgroundSprite, this.bounds);
};

/**
 * Resize gauge frame.
 * @param {anychart.utils.geom.Rectangle} bounds New bounds.
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.resize = function(bounds) {
    this.bounds = bounds;
    this.draw();
};

/**
 * Auto fit frame by gauge bounds.
 * @param {anychart.utils.geom.Rectangle} bounds Gauge bounds.
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.autoFit = goog.abstractMethod;

/**
 * @protected
 * @return {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.createInnerStroke = goog.abstractMethod;

/**
 * @protected
 * @return {anychart.plots.gaugePlot.visual.BackgroundBasedStroke}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.createOuterStroke = goog.abstractMethod;

/**
 * @protected
 * @return {anychart.visual.background.ShapeBackground}
 */
anychart.plots.gaugePlot.frames.GaugeFrame.prototype.createBackground = goog.abstractMethod;
