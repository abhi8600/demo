goog.provide('anychart.plots.gaugePlot');

goog.require('anychart.plots');
goog.require('anychart.plots.gaugePlot.templates');
goog.require('anychart.plots.gaugePlot.gauges');
goog.require('anychart.plots.gaugePlot.innerTemplates');
goog.require('anychart.plots.gaugePlot.gauges.linear');
goog.require('anychart.plots.gaugePlot.gauges.circular');
goog.require('anychart.plots.gaugePlot.gauges.indicator');
//-----------------------------------------------------------------------------
//     GaugePlot: plot for gauges
//-----------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.plots.AbstractPlot}
 */
anychart.plots.gaugePlot.GaugePlot = function() {
    anychart.plots.AbstractPlot.call(this);
};
goog.inherits(anychart.plots.gaugePlot.GaugePlot,
              anychart.plots.AbstractPlot);
/**@inheritDoc*/
anychart.plots.gaugePlot.GaugePlot.prototype.updatePointData = function(groupName, pointName, data) {
    var gaugeName = arguments[0];
    var pointerName = arguments[1];

    var newValue = arguments[2];
    var gauge;
    if (gaugeName != null) {
        gaugeName = gaugeName.toLowerCase();
        gauge = this.gaugesMap_[gaugeName];
    }
    if (gauge == null)
        gauge = this.gaugesList_[0];
    if (gauge != null) {
        gauge.setPointerValue(newValue, pointerName)
    }
};
/**@inheritDoc*/
anychart.plots.gaugePlot.GaugePlot.prototype.postInitialize = function() {};

//-----------------------------------------------------------------------------
// Not used
//-----------------------------------------------------------------------------

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setPlotType = function(value) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setChartView = function(value) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setPlotCustomAttribute = function(name, value) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.onAfterDeserializeData = function(config) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setControlsList = function(value) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.drawDataIcon = function(iconSettings, item, svgManager, dx, dy) {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.initFormatting = function() {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getTokenType = function() { return anychart.formatting.TextFormatTokenType.UNKNOWN; };

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getTokenValue = function(name) { return null; };

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.clear = function() {};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.refresh = function() {};

//-----------------------------------------------------------------------------
// Chart view
//-----------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.IAnyChart}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.mainChartView_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setMainChartView = function(value) {
    this.mainChartView_ = value;
};

//-----------------------------------------------------------------------------
// Templates
//-----------------------------------------------------------------------------
/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getTemplatesMerger = function() {
    return new anychart.plots.gaugePlot.templates.GaugePlotTemplatesMerger();
};

//-----------------------------------------------------------------------------
// Gauges
//-----------------------------------------------------------------------------

/**
 * @private
 * @type {anychart.plots.gaugePlot.gauges.GaugeBase}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.baseGauge_ = null;

/**
 * @private
 * @type {Object.<anychart.plots.gaugePlot.gauges.GaugeBase>}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.gaugesMap_ = null;

/**
 * @private
 * @type {Array.<anychart.plots.gaugePlot.gauges.GaugeBase>}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.gaugesList_ = null;

//-----------------------------------------------------------------------------
// Deserialization
//-----------------------------------------------------------------------------

anychart.plots.gaugePlot.GaugePlot.prototype.innerTemplatesManager_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.deserialize = function(config, styles) {
    this.innerTemplatesManager_ = new anychart.plots.gaugePlot.innerTemplates.InnerTemplatesManager();

    this.baseGauge_ = new anychart.plots.gaugePlot.gauges.GaugeBase();
    this.gaugesList_ = [];
    this.gaugesMap_ = {};

    var des = anychart.utils.deserialization;

    var gaugesList = [];

    var circular, linear, label, image, indicator = null;
    var circularDefaultTemplate, linearDefaultTemplate, labelDefaultTemplate, imageDefaultTemplate, indicatorDefaultTemplate = null;

    if (des.hasProp(config, 'circular_default_template'))
        circularDefaultTemplate = des.getLStringProp(config, 'circular_default_template');
    if (des.hasProp(config, 'linear_default_template'))
        linearDefaultTemplate = des.getLStringProp(config, 'linear_default_template');
    if (des.hasProp(config, 'label_default_template'))
        labelDefaultTemplate = des.getLStringProp(config, 'label_default_template');
    if (des.hasProp(config, 'image_default_template'))
        imageDefaultTemplate = des.getLStringProp(config, 'image_default_template');
    if (des.hasProp(config, 'indicator_default_template'))
        indicatorDefaultTemplate = des.getLStringProp(config, 'indicator_default_template');

    if (des.hasProp(config, 'circular')) {
        circular = des.getPropArray(config, 'circular');
        gaugesList.push(circular);
    }

    if (des.hasProp(config, 'linear')) {
        if (IS_ORACLE_BUILD) throw new anychart.errors.OracleFeatureNotSupportedError();
        linear = des.getPropArray(config, 'linear');
        gaugesList.push(linear);
    }

    if (des.hasProp(config, 'label')) {
        if (IS_ORACLE_BUILD) throw new anychart.errors.OracleFeatureNotSupportedError();
        label = des.getPropArray(config, 'label');
        gaugesList.push(label);
    }

    if (des.hasProp(config, 'image')) {
        if (IS_ORACLE_BUILD) throw new anychart.errors.OracleFeatureNotSupportedError();
        image = des.getPropArray(config, 'image');
        gaugesList.push(image);
    }

    if (des.hasProp(config, 'indicator')) {
        if (IS_ORACLE_BUILD) throw new anychart.errors.OracleFeatureNotSupportedError();
        indicator = des.getPropArray(config, 'indicator');
        gaugesList.push(indicator);
    }

    var templates, i;
    if (circular) {
        templates = des.getPropArray(config, 'circular_template');
        for (i = 0; i < circular.length; i++)
            this.deserializeGauge_('circular', circular[i], templates, anychart.plots.gaugePlot.gauges.circular.CircularGauge, circularDefaultTemplate);
    }
    if (linear) {
        templates = des.getPropArray(config, 'linear_template');
        for (i = 0; i < linear.length; i++)
            this.deserializeGauge_('linear', linear[i], templates, anychart.plots.gaugePlot.gauges.linear.LinearGauge, linearDefaultTemplate);
    }
    if (label) {
        templates = des.getPropArray(config, 'label_template');
        for (i = 0; i < label.length; i++)
            this.deserializeGauge_('label', label[i], templates, anychart.plots.gaugePlot.gauges.LabelGauge, labelDefaultTemplate);
    }
    if (image) {
        templates = des.getPropArray(config, 'image_template');
        for (i = 0; i < image.length; i++)
            this.deserializeGauge_('image', image[i], templates, null, imageDefaultTemplate);
    }
    if (indicator) {
        templates = des.getPropArray(config, 'indicator_template');
        for (i = 0; i < indicator.length; i++)
            this.deserializeGauge_('indicator', indicator[i], templates, anychart.plots.gaugePlot.gauges.indicator.IndicatorGauge, indicatorDefaultTemplate);
    }

    for (i = 0; i < this.gaugesList_.length; i++) {
        var parent;
        var parentGauge = null;
        if (this.gaugesList_[i].hasParent()) {
            parent = this.gaugesList_[i].getParentName().toLowerCase();
            parentGauge = this.gaugesMap_[parent];
        }
        if (parentGauge)
            parentGauge.addChild(this.gaugesList_[i]);
        else
            this.baseGauge_.addChild(this.gaugesList_[i]);
    }
};

/**
 * @private
 * @param {String} type Gauge type.
 * @param {Object} config Gauge config.
 * @param {Array.<Object>} templates Templates list.
 * @param {Function} constructor Gauge class.
 * @param {string} defaultTemplate Default template name.
 */
anychart.plots.gaugePlot.GaugePlot.prototype.deserializeGauge_ = function(type, config, templates, constructor, defaultTemplate) {
    if (constructor == null) {
        anychart.errors.featureNotSupported(type+" gauge");
    }

    var gaugeName = null;
    var des = anychart.utils.deserialization;
    if (des.hasProp(config, 'name')) {
        gaugeName = des.getStringProp(config, 'name');
        if (this.gaugesMap_[gaugeName] != null) return;
    }else {
        gaugeName = '___gauge__' + this.gaugesList_.length;
    }

    var gauge = new constructor();
    if (!gauge.isValid(config))
        return;

    gauge.setPlot(this);

    var pointers = null;
    if (des.hasProp(config, 'pointers') && des.hasProp(des.getProp(config, 'pointers'), 'pointer')) {
        pointers = des.getPropArray(des.getProp(config, 'pointers'), 'pointer');
    }
    var actualConfig = this.innerTemplatesManager_.apply(config, templates, defaultTemplate);
    if (pointers)
        actualConfig['pointers']['pointer'] = pointers;

    gauge.deserialize(actualConfig);

    this.gaugesList_.push(gauge);
    this.gaugesMap_[gaugeName.toLowerCase()] = gauge;
};

/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.svgManager_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.initializeSVG = function(value) {
    this.svgManager_ = value;
};

/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.bounds_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getBounds = function() {
    return this.bounds_;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setBounds = function(value) {
    this.bounds_ = value;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getInnerBounds = function() { return this.bounds_; };

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.gaugePlot.GaugePlot.prototype.sprite_ = null;

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getSprite = function() {
    return this.sprite_;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.initialize = function(bounds, tooltipsContainer, opt_drawBackground) {
    this.sprite_ = new anychart.svg.SVGSprite(this.svgManager_);
    if(IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('GaugePlot');
    this.bounds_ = bounds;
    this.sprite_.appendSprite(this.baseGauge_.initialize(this.svgManager_, bounds, tooltipsContainer));
    return this.sprite_;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.draw = function() {
    this.baseGauge_.draw();
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.calculateResize = function(bounds) {
    this.bounds_ = bounds;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.execResize = function() {
    this.baseGauge_.resize(this.bounds_);
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.supportsCache = function() { return false; };

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getCachedData = function() { return null; };

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.setCachedData = function() { };

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.getTooltipsSprite = function() {
    return null;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.serialize = function(res) {
    if (res == null) res = {};
    res['gauges'] = [];
    for (var i = 0; i < this.baseGauge_.numChildren(); i++) {
        var gauge = this.baseGauge_.getChild(i);
        if (gauge)
            res['gauges'].push(gauge.serialize());
    }
    return res;
};

/** @inheritDoc */
anychart.plots.gaugePlot.GaugePlot.prototype.checkNoData = function(config) { return false; };
