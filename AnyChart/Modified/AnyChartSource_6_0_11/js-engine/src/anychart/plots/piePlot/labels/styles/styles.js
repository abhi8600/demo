/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {}</li>.
 * <ul>
 */

goog.provide('anychart.plots.piePlot.labels.styles');

/**
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyleShapes}
 */
anychart.plots.piePlot.labels.styles.PieLabelStyleShapes = function() {
    anychart.elements.label.styles.LabelElementStyleShapes.call(this);
};
goog.inherits(anychart.plots.piePlot.labels.styles.PieLabelStyleShapes,
        anychart.elements.label.styles.LabelElementStyleShapes);

//------------------------------------------------------------------------------
//                          Methods, overrides, etc
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.piePlot.labels.styles.PieLabelStyleShapes.prototype.update = function(state) {
    if (this.point_.getSeries().isOutsideLabels() && !this.point_.getPlot().getBasicLabelsInfo()[this.point_.getLabelIndex()].isEnabled()) {
        this.setVisible(false);
    } else {
        goog.base(this, 'update', state);
    }
};

/**
 * @inheritDoc
 */
anychart.plots.piePlot.labels.styles.PieLabelStyleShapes.prototype.execUpdate = function(svgManager, textElement, sprite, color) {
    textElement.update(
        svgManager,
        sprite,
        color,
        false,
        textElement.textAlign_
    )
};


//------------------------------------------------------------------------------
//
//                         PieLabelStyle class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.elements.label.styles.LabelElementStyle}
 */
anychart.plots.piePlot.labels.styles.PieLabelStyle = function() {
    anychart.elements.label.styles.LabelElementStyle.call(this);
};
goog.inherits(anychart.plots.piePlot.labels.styles.PieLabelStyle,
        anychart.elements.label.styles.LabelElementStyle);

//------------------------------------------------------------------------------
//                          Style shapes
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.piePlot.labels.styles.PieLabelStyle.prototype.createStyleShapes = function() {
    return new anychart.plots.piePlot.labels.styles.PieLabelStyleShapes();
};