/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>{anychart.plots.piePlot.templates.DEFAULT_TEMPLATE}</li>
 * <ul>
 */
goog.provide('anychart.plots.piePlot.templates.DEFAULT_TEMPLATE');

/**
 * Contains default template for all pie plots.
 *
 * @public
 * @const
 * @type {Object.<*>} - JSON
 */
anychart.plots.piePlot.templates.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"pie_style": [
				{
					"fill": {
						"type": "solid",
						"color": "%color",
						"opacity": "1"
					},
					"border": {
						"type": "solid",
						"color": "DarkColor(%color)"
					},
					"states": {
						"hover": {
							"fill": {
								"color": "Blend(White,%Color,0.3)"
							}
						}
					},
					"name": "anychart_default"
				},
				{
					"fill": {
						"color": "%Color"
					},
					"states": {
						"hover": {
							"fill": {
								"color": "Blend(White,%Color,0.9)"
							}
						},
						"pushed": {
							"color": "rgb(200,200,200)"
						},
						"selected_hover": {
							"fill": {
								"color": "Blend(White,%Color,0.3)"
							}
						},
						"selected_normal": {
							"fill": {
								"color": "Blend(White,%Color,0.3)"
							}
						}
					},
					"name": "aqua"
				}
			]
		},
		"data_plot_settings": {
			"pie_series": {
				"animation": {
					"style": "defaultPie"
				},
				"label_settings": {
					"position": {
						"padding": "20"
					},
					"animation": {
						"style": "defaultLabel"
					}
				},
				"marker_settings": {
					"animation": {
						"style": "defaultMarker"
					}
				},
				"connector": {
					"enabled": "true"
				},
				"tooltip_settings": {
				},
				"style": "Aqua"
			}
		},
		"chart_settings": {
			"data_plot_background": {
				"enabled": "false"
			}
		}
	}
};