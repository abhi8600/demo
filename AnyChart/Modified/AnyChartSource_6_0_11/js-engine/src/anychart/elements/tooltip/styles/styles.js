/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.tooltip.styles.TooltipElementStyleState}.</li>
 * <ul>
 */
goog.provide('anychart.elements.tooltip.styles');
goog.require('anychart.elements.label.styles');
//------------------------------------------------------------------------------
//
//                           TooltipElementStyleState class.
//
//------------------------------------------------------------------------------
/**
 * Class represent tooltip style state
 * @constructor
 * @param {anychart.styles.Style} style Style for this state.
 * @param {anychart.styles.StateType} opt_stateType State type.
 * @extends {anychart.elements.styles.BaseElementStyleState}
 */
anychart.elements.tooltip.styles.TooltipElementStyleState =
    function(style, opt_stateType) {
        anychart.elements.label.styles.LabelElementStyleState.call(this, style,
                opt_stateType);
    };
//inheritance
goog.inherits(anychart.elements.tooltip.styles.TooltipElementStyleState,
        anychart.elements.label.styles.LabelElementStyleState);
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize tooltip element style state settings.
 * @override
 * @param {Object} data JSON object with tooltip element style state settings.
 * @return {Object} JSON object.
 */
anychart.elements.tooltip.styles.TooltipElementStyleState.prototype.
    deserialize = function(data) {
        data = goog.base(this, 'deserialize', data);
        if (this.anchor == anychart.layout.Anchor.FLOAT &&
                this.vAlign == anychart.layout.VerticalAlign.BOTTOM)
            this.padding += 20;
        return data;
    };
//------------------------------------------------------------------------------
//
//                          TooltipElementStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.elements.tooltip.styles.TooltipElementStyle = function() {
    anychart.elements.label.styles.LabelElementStyle.call(this);
};
goog.inherits(anychart.elements.tooltip.styles.TooltipElementStyle,
        anychart.elements.label.styles.LabelElementStyle);
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.tooltip.styles.TooltipElementStyle.prototype.initialize =
    function(point, element) {
        var sprite = goog.base(this, 'initialize', point, element);
        if (sprite) sprite.setVisible(false);
        return sprite;
    };
//------------------------------------------------------------------------------
//                              StyleState.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.tooltip.styles.TooltipElementStyle.prototype.createStyle =
    function() {
        return new anychart.elements.tooltip.styles.TooltipElementStyleState;
    };
