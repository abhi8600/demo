/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview Interfaces:
 * <ul>
 *  <li>@class {anychart.elements.IElementContainer}.</li>
 * <ul>
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.ElementsSprite}</li>
 *  <li>@class {anychart.elements.BaseElement}.</li>
 * <ul>
 */
goog.provide('anychart.elements');
goog.require('anychart.formatting');
goog.require('anychart.layout');
goog.require('anychart.styles');
goog.require('anychart.utils');
//------------------------------------------------------------------------------
//
//                       IElementContainer interface.
//
//------------------------------------------------------------------------------
/**
 * IElementContainer interface implement methods to get labels, markers,
 * tooltips containers and their position.
 * @interface
 * @extends {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.elements.IElementContainer = function() {
};
goog.inherits(anychart.elements.IElementContainer,
        anychart.formatting.ITextFormatInfoProvider);
/**
 * Return labels sprite.
 * <p>
 *     Note : Param label necessary is axesPlot only for label anchor XAxis.
 * </p>
 * @param {anychart.elements.label.BaseLabelElement} label Label element.
 * instance.
 * @return {anychart.svg.SVGSprite} Container for labels.
 */
anychart.elements.IElementContainer.prototype.getLabelsSprite =
        function(label) {
        };

/**
 * Return tooltip sprite.
 * @return {anychart.svg.SVGSprite} Container for tooltips.
 */
anychart.elements.IElementContainer.prototype.getTooltipsSprite = function() {
};

/**
 * Return marker sprite.
 * @return {anychart.svg.SVGSprite} Container for markers.
 */
anychart.elements.IElementContainer.prototype.getMarkersSprite = function() {
};

/**
 * Return current element position.
 * @return {anychart.utils.geom.Point} Element position.
 */
anychart.elements.IElementContainer.prototype.getElementPosition = function() {
};
//------------------------------------------------------------------------------
//
//                          ElementsSprite.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 */
anychart.elements.ElementsSprite = function(svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
};
goog.inherits(anychart.elements.ElementsSprite, anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                            Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.elements.styles.BaseElementsStyleShapes}
 */
anychart.elements.ElementsSprite.prototype.styleShapes_ = null;
/**
 * @param {anychart.elements.styles.BaseElementsStyleShapes} value Value.
 */
anychart.elements.ElementsSprite.prototype.setStyleShapes = function(value) {
    this.styleShapes_ = value;
};
//------------------------------------------------------------------------------
//                              Visual.
//------------------------------------------------------------------------------
/**
 * Update element sprite.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 */
anychart.elements.ElementsSprite.prototype.update = function(state) {
    this.styleShapes_.update(state);
};
/**
 * Hide or show shapes.
 * @param {Boolean} value Value.
 */
anychart.elements.ElementsSprite.prototype.setVisible = function(value) {
    this.styleShapes_.setVisible(value);
};
//------------------------------------------------------------------------------
//
//                           BaseElement class.
//
//------------------------------------------------------------------------------
/**
 * BaseElement class is a base class for all anychart elements like labels,
 * tooltips and markers.
 * @constructor
 */
anychart.elements.BaseElement = function() {
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------

/**
 * Indicates, need draw element.
 * @private
 * @type {Boolean}
 */
anychart.elements.BaseElement.prototype.enabled_ = false;

/**
 * @return {Boolean} Is element enabled or not.
 */
anychart.elements.BaseElement.prototype.isEnabled = function() {
    return this.enabled_;
};

/**
 * @param {Boolean} value Value.
 */
anychart.elements.BaseElement.prototype.setEnabled = function(value) {
    this.enabled_ = value;
};

/**
 * Element style object.
 * @private
 * @type {anychart.elements.styles.BaseElementsStyle}
 */
anychart.elements.BaseElement.prototype.style_ = null;

/**
 * @return {anychart.elements.styles.BaseElementsStyle} element style.
 */
anychart.elements.BaseElement.prototype.getStyle = function() {
    return this.style_;
};
/**
 * @param {anychart.elements.styles.BaseElementsStyle} value new element style.
 */
anychart.elements.BaseElement.prototype.setStyle = function(value) {
    this.style_ = value;
};

//-------------------------------------------------------------------------------
//                          Abstract methods
//------------------------------------------------------------------------------

/**
 * Create specific style for element.
 * @return {anychart.elements.styles.BaseElementsStyle} Style.
 */
anychart.elements.BaseElement.prototype.createStyle = goog.abstractMethod;

/**
 * Return style node name.
 * @protected
 * @type {string}
 */
anychart.elements.BaseElement.prototype.getStyleNodeName = goog.abstractMethod;

//------------------------------------------------------------------------------
//                          Visualisation
//------------------------------------------------------------------------------
/**
 * @param {anychart.elements.IElementContainer} container Container for element.
 * @return {anychart.svg.SVGSprite} Sprite for element.
 */
anychart.elements.BaseElement.prototype.getElementContainer =
        function(container) {
            goog.abstractMethod();
        };
/**
 * @param {anychart.elements.IElementContainer} container Container for element.
 * @param {anychart.elements.styles.BaseElementStyleState} state Element style
 * state.
 * @param {anychart.svg.SVGSprite} sprite Element sprite.
 * @return {anychart.utils.geom.Point} Element position.
 */
anychart.elements.BaseElement.prototype.getPosition = function(container, state, sprite) {
    return container.getElementPosition(this, state,
            this.getBounds(container, state, sprite));
};
/**
 * @param {anychart.elements.IElementContainer} container Container for element.
 * @param {anychart.elements.styles.BaseElementStyleState} state Element style
 * state.
 * @param {anychart.svg.SVGSprite} sprite Element sprite.
 * @return {anychart.utils.geom.Rectangle} Element bounds.
 */
anychart.elements.BaseElement.prototype.getBounds = function(container, state, sprite) {
    return null;
};
/**
 * Set element position.
 * @protected
 * @param {anychart.utils.geom.Point} position New element position.
 * @param {anychart.svg.SVGSprite} sprite Sprite with element.
 */
anychart.elements.BaseElement.prototype.setPosition = function(position, sprite) {
    if (sprite && position) sprite.setPosition(position.x, position.y);
};

/**
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 * @return {anychart.elements.ElementsSprite} Sprite.
 */
anychart.elements.BaseElement.prototype.createSprite = function(svgManager) {
    return new anychart.elements.ElementsSprite(svgManager);
};
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
/**
 * Determine, can deserialize element for series.
 * @param {object} elementNode JSON object with element settings.
 * @return {Boolean} Is element settings exists for series.
 */
anychart.elements.BaseElement.prototype.canDeserializeForSeries = function(elementNode) {
    var des = anychart.utils.deserialization;
    if (!elementNode) return false;
    if (des.hasProp(elementNode, 'style')) return true;
    if (des.hasProp(elementNode, 'enabled')) {
        var enabled = des.getBoolProp(elementNode, 'enabled');
        return enabled != this.enabled_;
    }
    return false;
};
/**
 * Determine, can deserialize element.
 * @param {object} elementNode JSON object with element settings.
 * @return {Boolean} Can element be serialized or not.
 */
anychart.elements.BaseElement.prototype.canDeserializeForPoint = function(elementNode) {
    return elementNode && anychart.utils.deserialization.hasProp(elementNode, 'style');
};
/**
 * Deserialize global label settings.
 * @param {object} data JSON object with elements settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.elements.BaseElement.prototype.deserializeGlobalSettings =
        function(data, stylesList) {
            if (anychart.utils.deserialization.hasProp(data, 'enabled')) {
                this.enabled_ = anychart.utils.deserialization.getBoolProp(
                        data, 'enabled');
                delete data['enabled']
            }
            this.deserializeStyleFromNode_(data, stylesList);
        };
/**
 * Deserialize element settings.
 * @param {object} elementNode JSON object with element settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.elements.BaseElement.prototype.deserializeSeriesSettings =
        function(elementNode, stylesList) {
            this.deserialize_(elementNode, stylesList);
        };
/**
 * Deserialize element settings for point.
 * @param {object} elementNode JSON object with element settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.elements.BaseElement.prototype.deserializePointSettings =
        function(elementNode, stylesList) {
            this.deserialize_(elementNode, stylesList);
        };

/**
 * @private
 * @param {object} elementNode JSON object with element settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.elements.BaseElement.prototype.deserialize_ = function(elementNode, stylesList) {
    if (anychart.utils.deserialization.hasProp(elementNode, 'enabled'))
        this.enabled_ = anychart.utils.deserialization.getBoolProp(
                elementNode, 'enabled');
    if (anychart.utils.deserialization.hasProp(elementNode, 'style')) {
        var styleName = anychart.utils.deserialization.getLStringProp(
                elementNode,
                'style');
        this.deserializeStyleFromApplicator_(stylesList, styleName);
    }
};
/**
 * Deserialize style from series settings node.
 * @private
 * @param {object} data JSON object with element settings.
 * @param {anychart.styles.StylesList} stylesList Chart styles list.
 */
anychart.elements.BaseElement.prototype.deserializeStyleFromNode_ =
        function(data, stylesList) {
            var styleName =
                    anychart.utils.deserialization.getLStringProp(data, 'style');
            var actualStyle = stylesList.getStyleByMerge(this.getStyleNodeName(),
                    data, styleName);
            this.style_ = this.createStyle();
            this.style_.deserialize(actualStyle);
        };
/**
 * Deserialize style from applicator.
 * @private
 * @param {anychart.styles.StylesList} styleList Cart styles list.
 * @param {string} styleName Element style name.
 */
anychart.elements.BaseElement.prototype.deserializeStyleFromApplicator_ =
        function(styleList, styleName) {
            var nodeName = this.getStyleNodeName();
            var cashedStyle = styleList.getStyle(nodeName, styleName);
            if (cashedStyle) this.style_ = cashedStyle;
            else {
                this.style_ = this.createStyle();
                this.style_.deserialize(styleList.getStyleByApplicator(nodeName,
                        styleName));
            }
        };
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Resize element
 * @param {anychart.elements.IElementContainer} container
 * @param {anychart.elements.styles.BaseElementStyleState} state
 * @param {anychart.elements.ElementsSprite} sprite
 */
anychart.elements.BaseElement.prototype.resize = function(container, state, sprite) {
    if (!state.isEnabled()) return;
    var pos = this.getPosition(container, state, sprite);
    this.setPosition(pos, sprite);
};
//------------------------------------------------------------------------------
//                         Utils
//------------------------------------------------------------------------------
/**
 * Create copy of base element.
 * @param {anychart.elements.BaseElement} target Target object to copy.
 * @return {anychart.elements.BaseElement} Copied element.
 */
anychart.elements.BaseElement.prototype.copy = function(target) {
    target.enabled_ = this.enabled_;
    target.style_ = this.style_;
    return target;
};
