/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.formatting.TextFormatter}</li>
 *  <li>@class {anychart.formatting.TextFormatTokenType}</li>
 *  <li>@class {anychart.formatting.TokensHash}</li>
 * <ul>
 * Interfaces:
 * <ul>
 *  <li>@class {anychart.formatting.IDateTimeInfoProvider}</li>
 *  <li>@class {anychart.formatting.ITextFormatInfoProvider}.</li>
 * <ul>
 */
goog.provide('anychart.formatting');
goog.require('anychart.formatting.terms');
//------------------------------------------------------------------------------
//
//                           IDateTimeInfoProvider interface.
//
//------------------------------------------------------------------------------
/**
 * IDateTimeInfoProvider interface implementers serve in text formatting for
 * datetime token
 * terms formatting. It provides datetime parts values.
 *
 * @interface
 */
anychart.formatting.IDateTimeInfoProvider = function () {
};

/**
 * Returns full month name by it's number. Month number starts from 0-January;
 *
 * @param {int} month Month index.
 * @return {String} Month name.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getMonthNames =
    function (month) {
    };

/**
 * Returns short month name by it's number. Month number starts from 0-January;
 *
 * @param {int} month Month index.
 * @return {String} Month name.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getShortMonthNames =
    function (month) {
    };

/**
 * @return {String} AM string.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getAmString = function () {
};

/**
 * @return {String} Short am string.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getShortAmString =
    function () {
    };

/**
 * @return {String} PM String.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getPmString = function () {
};

/**
 * @return {String} Short PM String.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getShortPmString =
    function () {
    };

/**
 * Returns full week day name by it's number.
 * Week day number starts from (0 - Sunday);
 *
 * @param {int} weekDay week day index.
 * @return {String} week day name.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getWeekDayNames =
    function (weekDay) {
    };

/**
 * Returns short week day name by it's number. Week day number starts from
 * 0 - Sunday;
 *
 * @param {int} weekDay week day index.
 * @return {String} Short week day name.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getShortWeekDayNames =
    function (weekDay) {
    };

/**
 * @return {String} Default datetime formatting mask.
 */
anychart.formatting.IDateTimeInfoProvider.prototype.getMask = function () {
};

//------------------------------------------------------------------------------
//
//                           ITextFormatInfoProvider interface.
//
//------------------------------------------------------------------------------
/**
 * ITextFormatInfoProvider interface definition. This interface allows to use
 * the implementer as a provider of text format token values and types.
 * TextFormatter asks ITextFormatInfoProvider implementer about a token
 * and gets it's type and value.
 *
 * @interface
 */
anychart.formatting.ITextFormatInfoProvider = function () {
};

/**
 * Returns current token value. Can return number or string value.
 * CAUTION: In case of datetime token should return number value.
 * CAUTION: If the implementer cannot get the value of passed token
 * it should return EMPTY STRING.
 *
 * @param {string} token token name with % sign.
 * @return {number|string} Token value.
 */
anychart.formatting.ITextFormatInfoProvider.prototype.getTokenValue =
    function (token) {
    };

/**
 * Returns current token type. CAUTION: If the implementer doesn't know anything
 * about the token it should return
 * anychart.formatting.TextFormatTokenType.UNKNOWN
 * in optimization purposes.
 *
 * @param {string} token Token name.
 * @return {anychart.formatting.TextFormatTokenType} Token type.
 */
anychart.formatting.ITextFormatInfoProvider.prototype.getTokenType =
    function (token) {
    };

//------------------------------------------------------------------------------
//
//                           TextFormatterTokenType class.
//
//------------------------------------------------------------------------------
/**
 * Represents token type. Integer values of this sense are returned by
 * getTokenType() method of ITextFormatInfoProvider implementers.
 *
 * @enum {int}
 */
anychart.formatting.TextFormatTokenType = {
    TEXT:1,
    NUMBER:2,
    DATE:3,
    UNKNOWN:4
};
//------------------------------------------------------------------------------
//
//                           TextFormatter class.
//
//------------------------------------------------------------------------------

/**
 * Represents a text formatter that aggregates formatting routines for
 * everything. Consumes target ITextFormatInfoProvider and locale.
 * In construction asks target about token types, stores references to target
 * and locale and parses format string for dynamic tokens.
 * Tokens explanation can be found in AnyChart documentation.
 * @constructor
 * @param {string} format Format for parsing.
 */
anychart.formatting.TextFormatter = function (format) {
    this.format_ = format;
    this.termsQueue_ = [];
};

/**
 * Executes terms in queue and returns results concatenation.
 *
 * @param {anychart.formatting.ITextFormatInfoProvider} target Tokens provider.
 * @param {anychart.formatting.IDateTimeInfoProvider} locale Locale.
 * @return {string} Formatted string.
 */
anychart.formatting.TextFormatter.prototype.getValue = function (target, locale) {
    this.tokenValuesProvider_ = target;
    this.dateTimeValuesProvider_ = locale;
    if (this.termsQueue_.length == 0) this.parse_(this.format_);

    var result = '';
    var len = this.termsQueue_.length;
    for (var i = 0; i < len; i++)
        result += this.termsQueue_[i].getValue(
            this.tokenValuesProvider_,
            this.dateTimeValuesProvider_);
    return result;
};

/**
 * Text format value.
 * @private
 * @type {String}
 */
anychart.formatting.TextFormatter.prototype.format_ = null;

/**
 * Stores token values provider reference.
 *
 * @private
 * @type {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.formatting.TextFormatter.prototype.tokenValuesProvider_ = null;

/**
 * Stores datetime token values provider reference.
 *
 * @private
 * @type {anychart.formatting.IDateTimeInfoProvider}
 */
anychart.formatting.TextFormatter.prototype.dateTimeValuesProvider_ = null;

/**
 * Stores parsed terms.
 *
 * @private
 * @type {Array.<anychart.formatting.terms.ITerm>}
 */
anychart.formatting.TextFormatter.prototype.termsQueue_ = null;

/**
 * Parses passed format string and fills this.termsQueue_ for future execution
 * in getValue() method.
 * @private
 * @param {string} format Format for parsing.
 */
anychart.formatting.TextFormatter.prototype.parse_ = function (format) {
    var len = format.length;
    var state = 0; // state parsing string const
    var buffer = '';
    var paramsStringBuffer;
    var tokenName;
    var paramName;
    var paramValue;
    var params;
    var isEscaped = false;
    for (var i = 0; i < len; i++) {
        var c = format.charAt(i);
        switch (state) {
            case 0: // parsing string const
                if (c == '{')
                    state = 1; // found '{', is this a token?
                else
                    buffer += c;
                break;
            case 1: // found '{', is this a token?
                if (c == '%') {
                    if (buffer.length > 0) {
                        this.termsQueue_.push(
                            new anychart.formatting.terms.TermStringConst(buffer)
                        );
                    }
                    buffer = '%';
                    state = 2; // parsing token name
                } else {
                    buffer += '{' + c;
                    state = 0;
                }
                break;
            case 2: // parsing token name
                if (c == '}') {
                    if (buffer.length > 1) {
                        tokenName = buffer;
                        buffer = '';
                        state = 3; // decide what to parse now
                    } else {
                        // token with empty name is treated as a const string
                        buffer = '{%}';
                        state = 0;
                    }
                } else
                    buffer += c;
                break;
            case 3:
                // decide what to parse now - next token,
                // token params or just a string
                if (c == '{') {
                    state = 4; // may be next token or token params
                } else {
                    this.createToken_(tokenName, {});
                    buffer = c;
                    state = 0; // a string const
                }
                break;
            case 4: // decide what to parse now - next token or token params
                if (c == '%') {
                    this.createToken_(tokenName, {});
                    buffer = '%';
                    state = 2; // parsing next token name
                } else {
                    paramsStringBuffer = '{' + c;
                    buffer = c;
                    paramName = '';
                    paramValue = '';
                    params = {};
                    isEscaped = c == '\\';
                    state = 5; // parsing token params (name)
                }
                break;
            case 5:
                // parsing token param name
                // (only not escaped ':' char can interrupt param name)
                paramsStringBuffer += c;
                if (isEscaped) {
                    buffer += c;
                    isEscaped = false;
                } else if (c == '\\') {
                    isEscaped = true;
                } else if (c == ':') {
                    // name can be empty to support flash anychart parser
                    paramName = buffer;
                    buffer = '';
                    state = 6; // parsing param value
                } else if (c == '}') {
                    paramName = buffer;
                    params[paramName] = '';
                    this.createToken_(tokenName, params);
                    paramName = '';
                    paramValue = '';
                    buffer = '';
                    paramsStringBuffer = '';
                    state = 0; // params finished, going back to start state
                } else
                    buffer += c;
                break;
            case 6:
                // parsing param value (only not escaped ',' char can interrupt
                // param value, no spaces are ignored)
                paramsStringBuffer += c;
                if (isEscaped) {
                    buffer += c;
                    isEscaped = false;
                } else if (c == '\\') {
                    isEscaped = true;
                } else if (c == ',') {
                    paramValue = buffer;
                    params[paramName] = paramValue;
                    buffer = '';
                    paramName = '';
                    paramValue = '';
                    state = 5; // parsing param value
                } else if (c == '}') {
                    paramValue = buffer;
                    params[paramName] = paramValue;
                    this.createToken_(tokenName, params);
                    paramName = '';
                    paramValue = '';
                    buffer = '';
                    paramsStringBuffer = '';
                    state = 0; // params finished, going back to start state
                } else
                    buffer += c;
                break;
        }
    }
    switch (state) {
        case 0: // was parsing a string const
            if (buffer.length > 0)
                this.termsQueue_.push(
                    new anychart.formatting.terms.TermStringConst(buffer));
            break;
        case 1: // found '{' and then end of input - add '{' string const
            this.termsQueue_.push(
                new anychart.formatting.terms.TermStringConst('{'));
            break;
        case 2:
            // was parsing token name and then got an EOI, buffer contains
            // token name without '{'
            this.termsQueue_.push(
                new anychart.formatting.terms.TermStringConst('{' + buffer));
            break;
        case 3:
            // Just finished successful token without params parsing.
            //Do nothing.
            this.createToken_(tokenName, {});
            break;
        case 4: // Parsed '{' and then EOI - add '{' string const
            this.createToken_(tokenName, {});
            this.termsQueue_.push(
                new anychart.formatting.terms.TermStringConst('{'));
            break;
        case 5:
        case 6:
            // was parsing token params and got EOI before '}' -
            // treat params as a string const
            this.termsQueue_.push(
                new anychart.formatting.terms.TermStringConst(paramsStringBuffer));
            break;
    }
};

/**
 * Helper function to create named token term with params.
 * It creates corresponding ITerm and pushes it to the queue.
 * @private
 * @param {string} tokenName with "%" char at the beginning.
 * @param {Object.<string>} tokenParams - parameters array.
 */
anychart.formatting.TextFormatter.prototype.createToken_ =
    function (tokenName, tokenParams) {
        var type = this.tokenValuesProvider_.getTokenType(tokenName);
        var scale = [];
        if (tokenParams['enabled'] != undefined &&
            tokenParams['enabled'].toLowerCase() != 'true')
            this.termsQueue_.push(
                new anychart.formatting.terms.TermString(tokenName));
        else
            switch (type) {
                case anychart.formatting.TextFormatTokenType.TEXT:
                    this.termsQueue_.push(new anychart.formatting.terms.TermString(
                        tokenName,
                        tokenParams['maxChar'],
                        tokenParams['maxCharFinalChars']));
                    break;
                case anychart.formatting.TextFormatTokenType.NUMBER:
                    if (tokenParams['scale'] != undefined)
                        scale = this.parseScale_(tokenParams['scale']);
                    this.createNumberTerm_(tokenName, tokenParams, scale);
                    break;
                case anychart.formatting.TextFormatTokenType.DATE:
                    if (tokenParams['scale'] != undefined)
                        scale = this.parseScale_(tokenParams['scale']);
                    if (scale.length > 0)
                        this.createNumberTerm_(tokenName, tokenParams, scale);
                    else {
                        var dateTimeFormat = this.parseDateTimeFormat_(
                            tokenParams['dateTimeFormat'] == undefined ?
                                this.dateTimeValuesProvider_.getMask() :
                                tokenParams['dateTimeFormat']
                        );
                        this.termsQueue_.push(new anychart.formatting.terms.TermDateTimeToken(
                            tokenName,
                            tokenParams['maxChar'],
                            tokenParams['maxCharFinalChars'],
                            dateTimeFormat.aspects,
                            dateTimeFormat.consts));
                    }
                    break;
                case anychart.formatting.TextFormatTokenType.UNKNOWN:
                    // emulate empty string adding by doing nothing.
                    break;
            }
    };

/**
 * Helper function that creates number token according to it's params, parses
 * scale, sets formatters and pushes it to terms queue.
 * @private
 * @param {String} tokenName Token name.
 * @param {Object.<String>} tokenParams Token params.
 * @param {Array.<Object>} scale [{scale: number, suffix: string,
 * accumulativeScale: number}].
 */
anychart.formatting.TextFormatter.prototype.createNumberTerm_ =
    function (tokenName, tokenParams, scale) {
        var term = null;
        if (scale.length == 0)
            term = new anychart.formatting.terms.TermNumberTokenNoScale(
                tokenName,
                tokenParams['maxChar'],
                tokenParams['maxCharFinalChars']
            );
        else if (scale.length == 1)
            term = new anychart.formatting.terms.TermNumberTokenOneScale(
                tokenName,
                tokenParams['maxChar'],
                tokenParams['maxCharFinalChars'],
                scale[0]
            );
        else
            term = new anychart.formatting.terms.TermNumberTokenMultiScale(
                tokenName,
                tokenParams['maxChar'],
                tokenParams['maxCharFinalChars'],
                scale
            );
        term.setFormatters(
            tokenParams['useNegativeSign'],
            tokenParams['trailingZeros'],
            tokenParams['leadingZeros'],
            tokenParams['numDecimals'],
            tokenParams['thousandsSeparator'],
            tokenParams['decimalSeparator']
        );
        this.termsQueue_.push(term);
    };

/**
 * Parses scale param value and returns special scale array that is used by
 * corresponding terms.
 * @private
 * @param {string} scaleStr Scale string.
 * @return {Array.<object>} [{scale: number, suffix: string,
 * accumulativeScale: number}].
 */
anychart.formatting.TextFormatter.prototype.parseScale_ = function (scaleStr) {
    var len = scaleStr.length;
    var parsingValue = true;
    var index = 0;
    var scale = [];
    var buffer = '';
    for (var i = 0; i < len; i++) {
        var c = scaleStr.charAt(i);
        if (c == '(') {
            // do nothing
        } else if (c == ')') {
            if (parsingValue) {
                var numVal = Number(buffer);
                scale[index] = {
                    scale:numVal,
                    suffix:'',
                    accumulativeScale:numVal
                };
                if (index > 0)
                    scale[index].accumulativeScale *=
                        scale[index - 1].accumulativeScale;
            } else {
                scale[index].suffix = buffer;
            }
            buffer = '';
            index++;
        } else if (c == '|') {
            parsingValue = false;
            index = 0;
            buffer = '';
        } else {
            buffer += c;
        }
    }
    return scale;
};

/**
 * Parses datetime format string and returns aspects sequence to call.
 * @private
 * @param {string} format Format for parsing.
 * @return {Object} {aspects: Array.<Function>(N items),
 * consts: Array.<string>(N+1 items)}.
 */
anychart.formatting.TextFormatter.prototype.parseDateTimeFormat_ =
    function (format) {
        var len = format.length;
        var aspects = [];
        var consts = [];
        var state = 0; // reading const
        var buffer = '';
        var letter = '';
        var aspect;
        for (var i = 0; i < len; i++) {
            var c = format.charAt(i);
            switch (state) {
                case 0: // reading const
                    if (c == '%') {
                        consts.push(buffer);
                        buffer = '';
                        state = 1; // reading first token letter
                    } else
                        buffer += c;
                    break;
                case 1: // reading first token letter
                    if (c == '%') {
                        // found double '%', treating the first one as a
                        // previous constant continuation
                        consts[consts.length - 1] += '%';
                    } else {
                        letter = c;
                        buffer = c;
                        state = 2; // token letter selected, continue to
                        // parse token
                    }
                    break;
                case 2: // reading token from second letter
                    if (c == letter) {
                        buffer += c;
                    } else {
                        aspect = anychart.formatting.terms.
                            TermDateTimeTokenAspects[buffer];
                        if (aspect == undefined)
                            consts[consts.length - 1] += '%' + buffer;
                        else
                            aspects.push(aspect);
                        buffer = c;
                        state = 0;
                    }
                    break;
            }
        }
        switch (state) {
            case 0: // was reading string const
                consts.push(buffer);
                break;
            case 1: // was reading
                consts.push('%');
                break;
            case 2:
                aspect = anychart.formatting.terms.
                    TermDateTimeTokenAspects[buffer];
                if (aspect == undefined)
                    consts[consts.length - 1] += '%' + buffer;
                else {
                    aspects.push(aspect);
                    consts.push('');
                }
                break;
        }
        return {aspects:aspects, consts:consts};
    };

//------------------------------------------------------------------------------
//
//                           TokensHash class.
//
//------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.formatting.TokensHash = function () {
    this.tokens_ = {};
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object.<String>}
 */
anychart.formatting.TokensHash.prototype.tokens_ = null;
//------------------------------------------------------------------------------
//                          Methods.
//------------------------------------------------------------------------------
/**
 * @param {String} name Token name. Including %.
 * @return {String|Number} Token value.
 */
anychart.formatting.TokensHash.prototype.get = function (name) {
    return this.has(name) ? this.tokens_[name] : '';
};

/**
 * @param {String} name Token name.
 * @return {Boolean} Is token exists in cache.
 */
anychart.formatting.TokensHash.prototype.has = function (name) {
    return this.tokens_[name] != undefined;
};

/**
 * @param {String} name Token name.
 * @param {String|Number} value Token value.
 */
anychart.formatting.TokensHash.prototype.add = function (name, value) {
    this.tokens_[name] = value;
};

/**
 * @param {String} name Token name.
 * @param {Number} value Value to be added to token.
 */
anychart.formatting.TokensHash.prototype.increment = function (name, value) {
    if (this.has(name))
        this.tokens_[name] += value;
    else
        this.tokens_[name] = value;
};

/**
 * Formats color value.
 * @param {anychart.visual.color.Color} color Color value.
 * @return {String} Hex string with RGBA color.
 */
anychart.formatting.TokensHash.prototype.formatColorValue = function (color) {
    return color.toRGBAHash();
};

/**
 * Identifies that argument is token.
 * @param {String} token Token.
 * @return {Boolean} Is token.
 */
anychart.formatting.TokensHash.prototype.isToken = function (token) {
    return token && (token.indexOf('%') == 1 || token.indexOf('%') == 0);
};

/**
 * Identifies that token is token DataPlotToken.
 * @param {String} token Token.
 * @param {anychart.plots.BasePlot}
    * @return {Boolean} Is DataPlotToken.
 */
anychart.formatting.TokensHash.prototype.isDataPlotToken = function (token, plot) {
    return token.indexOf('%DataPlot') == 0 || plot.hasCustomAttribute(token);
};

/**
 * Identifies that token is token SeriesToken.
 * @param {String} token Token.
 * @param {anychart.plots.seriesPlot.data.BaseSeries}
    * @return {Boolean} Is SeriesToken.
 */
anychart.formatting.TokensHash.prototype.isSeriesToken = function (token, series) {
    return token.indexOf('%Series') == 0 || series.hasCustomAttribute(token);
};

/**
 * Identifies that token is token ThresholdToken.
 * @param {String} token Token.
 * @param {anychart.thresholds.Threshold}
    * @return {Boolean} Is ThresholdToken.
 */
anychart.formatting.TokensHash.prototype.isThresholdToken = function (token, threshold) {
    return threshold && (token.indexOf('%Threshold') == 0 || threshold.isCustomAttribute(token));
};

/**
 * Identifies that token is token ThresholdToken.
 * @param {String} token Token.
 * @param {anychart.thresholds.IFormattableThresholdItem}
    * @return {Boolean} Is ThresholdToken.
 */
anychart.formatting.TokensHash.prototype.isThresholdItemToken = function (token, thresholdItem) {
    return thresholdItem && (token.indexOf('%Threshold') == 0 || thresholdItem.isCustomAttribute(token));
};

/**
 * Identifies that token is token YAxisToken.
 * @param {String} token Token.
 * @return {Boolean} Is YAxisToken.
 */
anychart.formatting.TokensHash.prototype.isYAxisToken = function (token) {
    return token.indexOf('%YAxis') == 0;
};
/**
 * Identifies that token is token XAxisToken.
 * @param {String} token Token.
 * @return {Boolean} Is XAxisToken.
 */
anychart.formatting.TokensHash.prototype.isXAxisToken = function (token) {
    return token.indexOf('%XAxis') == 0;
};
/**
 * Define, is token belongs to axis.
 * @param {String} token
 * @return {Boolean}
 */
anychart.formatting.TokensHash.prototype.isAxisToken = function (token) {
    return token.indexOf('%Axis') == 0;
};

/**
 * Identifies that token is token CategoryToken.
 * @param {String} token Token.
 * @return {Boolean} Is CategoryToken.
 */
anychart.formatting.TokensHash.prototype.isCategoryToken = function (token) {
    return token.indexOf('%Category') == 0;
};

/**
 * Return token as axis. (%Axis...)
 * @param {String} token Token.
 * @return {String} Axis token.
 */
anychart.formatting.TokensHash.prototype.getAxisToken = function (token) {
    return token.slice(0, 1) + token.slice(2, token.length);
};
