/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.formatting.terms.BaseTerm}</li>
 *  <li>@class {anychart.formatting.terms.TermDateTimeToken}</li>
 *  <li>@class {anychart.formatting.terms.TermDateTimeTokenAspects}</li>
 *  <li>@class {anychart.formatting.terms.TermNumberTokenBase}</li>
 *  <li>@class {anychart.formatting.terms.TermNumberTokenMultiScale}</li>
 *  <li>@class {anychart.formatting.terms.TermNumberTokenNoScale}</li>
 *  <li>@class {anychart.formatting.terms.TermNumberTokenOneScale}</li>
 *  <li>@class {anychart.formatting.terms.TermString}</li>
 *  <li>@class {anychart.formatting.terms.TermStringConst}</li>
 * <ul>
 * Interfaces:
 * <ul>
 *  <li>@class {anychart.formatting.terms.ITerm}.</li>
 * <ul>
 *
 */
goog.provide('anychart.formatting.terms');
//------------------------------------------------------------------------------
//
//                           ITerm interface.
//
//------------------------------------------------------------------------------
/**
 * Describes external interface for all terms
 *
 * @interface
 */
anychart.formatting.terms.ITerm = function () {
};

/**
 * Returns formatted value of the term. This method should use valueProvider and
 * locale if necessary.
 * If valueProvider doesn't know anything about this token - return empty
 * string.
 *
 * @param {anychart.formatting.ITextFormatInfoProvider} valueProvider Provider.
 * @param {anychart.formatting.IDateTimeInfoProvider} locale Locale.
 * @return {String} Value of a term or empty string.
 */
anychart.formatting.terms.ITerm.prototype.getValue = function (valueProvider, locale) {
};
//------------------------------------------------------------------------------
//
//                           TermDateTimeTokenAspects class.
//
//------------------------------------------------------------------------------
/**
 * Datetime token term aspects enum definition
 *
 * @enum {Function}
 * @return {String} Datetime token aspect.
 */
anychart.formatting.terms.TermDateTimeTokenAspects = {
    /*
     u - unix timestamp
     */
    'u':function (date, locale) {
        return date.getTime().toString();
    },

    /*
     d - day of the month without leading zero
     dd - day of the month with leading zero
     ddd - short day name (day of the week)
     dddd - long day name (day of the week)
     */
    'd':function (date, locale) {
        return date.getUTCDate().toString();
    },
    'dd':function (date, locale) {
        var tmp = date.getUTCDate().toString();
        return tmp.length == 1 ? '0' + tmp : tmp;
    },
    'ddd':function (date, locale) {
        return locale.getShortWeekDayNames(date.getUTCDay());
    },
    'dddd':function (date, locale) {
        return locale.getWeekDayNames(date.getUTCDay());
    },

    /*
     M - month without leading zero
     MM - month with leading zero
     MMM - month as its abbr
     MMMM - month as its name
     */
    'M':function (date, locale) {
        return (date.getUTCMonth() + 1).toString();
    },
    'MM':function (date, locale) {
        var tmp = (date.getUTCMonth() + 1).toString();
        return tmp.length == 1 ? '0' + tmp : tmp;
    },
    'MMM':function (date, locale) {
        return locale.getShortMonthNames(date.getUTCMonth());
    },
    'MMMM':function (date, locale) {
        return locale.getMonthNames(date.getUTCMonth());
    },

    /*
     y - 2-digits without leading zero
     yy - 2-digits with leading zero
     yyyy - 4-digits year
     */
    'y':function (date, locale) {
        return date.getUTCFullYear().toString().substr(3, 1);
    },
    'yy':function (date, locale) {
        return date.getUTCFullYear().toString().substr(2, 2);
    },
    'yyyy':function (date, locale) {
        return date.getUTCFullYear().toString();
    },

    /*
     h - hours without leading zero (12 hour clock)
     hh - hours with leading zero (12 hour clock)
     H - hours without leading zero (24 hour clock)
     HH - hours with leading zero (24 hour clock)
     */
    'h':function (date, locale) {
        var tmp = date.getUTCHours();
        if (tmp == 0)
            return '12';
        if (tmp > 12)
            return (tmp - 12).toString();
        return tmp.toString();
    },
    'hh':function (date, locale) {
        var tmp = date.getUTCHours();
        if (tmp == 0)
            return '12';
        var s;
        if (tmp > 12) {
            s = (tmp - 12).toString();
            return (s.length == 1) ? '0' + s : s;
        }
        s = tmp.toString();
        return (s.length == 1) ? '0' + s : s;
    },
    'H':function (date, locale) {
        return date.getUTCHours().toString();
    },
    'HH':function (date, locale) {
        var tmp = date.getUTCHours().toString();
        return (tmp.length == 1) ? '0' + tmp : tmp;
    },

    /*
     m - minutes without leading zero
     mm - minutes with leading zero
     */
    'm':function (date, locale) {
        return date.getUTCMinutes().toString();
    },
    'mm':function (date, locale) {
        var tmp = date.getUTCMinutes().toString();
        return (tmp.length == 1) ? '0' + tmp : tmp;
    },

    /*
     s - seconds without leading zero
     ss - seconds with leading zero
     */
    's':function (date, locale) {
        return date.getUTCSeconds().toString();
    },
    'ss':function (date, locale) {
        var tmp = date.getUTCSeconds().toString();
        return (tmp.length == 1) ? '0' + tmp : tmp;
    },

    /*
     t - one character time-marker string (A,P)
     tt - multicharacter time-marker string (AM,PM)
     */
    't':function (date, locale) {
        var tmp = date.getUTCHours();
        if (tmp >= 12)
            return locale.getShortPmString();
        return locale.getShortAmString();
    },
    'tt':function (date, locale) {
        var tmp = date.getUTCHours();
        if (tmp >= 12)
            return locale.getPmString();
        return locale.getAmString();
    },

    /*
     z - time zone offset without leading zero
     zz - time zone offset with leading zero
     zzz - time zone offset with minutes and leading zero
     */
    'z':function (date, locale) {
        var tmp = -date.getTimezoneOffset() / 60;
        return tmp >= 0 ? '+' + tmp.toString() : tmp.toString();
    },
    'zz':function (date, locale) {
        var tmp = -date.getTimezoneOffset() / 60;
        var s = Math.abs(tmp).toString();
        s = (s.length == 1 ? '0' + s : s);
        return (tmp < 0 ? '-' + s : '+' + s);
    },
    'zzz':function (date, locale) {
        var s = (-date.getTimezoneOffset() % 60).toString();
        var tmp = -date.getTimezoneOffset() / 60;
        s = Math.abs(tmp).toString() + ':' + ((s.length == 1) ? '0' + s : s);
        s = (s.length == 4 ? '0' + s : s);
        return tmp < 0 ? '-' + s : '+' + s;
    }
};
//------------------------------------------------------------------------------
//
//                          BaseTerm class.
//
//------------------------------------------------------------------------------
/**
 *
 * Базовый класс для всех термов.
 * Содержит имя токена, а так же свойства и методы для контроля длинны итоговой строки.
 * @constructor
 * @param {String} tokenName Имя токена
 * @param {int} opt_maxChars Максимальное количество символов в итоговой строке
 * @param {String} opt_finalChars Строка которая будет подставлена в конец итоговой
 * строки в случае если длинна итоговой строки превысит максимальное количество.
 */
anychart.formatting.terms.BaseTerm = function (tokenName, opt_maxChars, opt_finalChars) {
    this.tokenName_ = tokenName;
    this.maxChars_ = opt_maxChars == undefined ? 0 : opt_maxChars;
    this.finalChars_ = opt_finalChars === undefined ? '...' : opt_finalChars;
};
//------------------------------------------------------------------------------
//                  Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.formatting.terms.BaseTerm.prototype.tokenName_ = null;
/**
 * @private
 * @type {int}
 */
anychart.formatting.terms.BaseTerm.prototype.maxChars_ = null;
/**
 * @private
 * @type {String}
 */
anychart.formatting.terms.BaseTerm.prototype.finalChars_ = null;
//------------------------------------------------------------------------------
//                  Formatting.
//------------------------------------------------------------------------------
anychart.formatting.terms.BaseTerm.prototype.formatFinalString_ = function (value) {
    if (this.maxChars_ > 0) {
        if (value.length > this.maxChars_) {
            value = value.substr(0, this.maxChars_);
            if (this.finalChars_ != null)
                value += this.finalChars_;
        }
    }
    return value;
};
//------------------------------------------------------------------------------
//
//                           TermDateTimeToken class.
//
//------------------------------------------------------------------------------
/**
 * Represents a term that returns a datetime value by token name.
 *
 * @constructor
 * @implements {anychart.formatting.terms.ITerm}
 * @extends {anychart.formatting.terms.BaseTerm}
 * @param {String} tokenName Token name.
 * @param {Array.<Function>} formatAspectsSequence Sequence of dynamic datetime
 * tokens.
 * @param {Array.<String>} formatConstsSequence Sequence of static strings, each
 * const may be an empty string.
 */
anychart.formatting.terms.TermDateTimeToken = function (tokenName, opt_maxChars, opt_finalChars, formatAspectsSequence, formatConstsSequence) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars);

    this.formatAspectsSequence_ = formatAspectsSequence ?
        formatAspectsSequence : [];
    this.formatConstsSequence_ = formatConstsSequence ?
        formatConstsSequence : [''];
};
goog.inherits(anychart.formatting.terms.TermDateTimeToken, anychart.formatting.terms.BaseTerm);
/**
 * Returns formatted date. Formatted date consists of a sequence as:
 * const0 + aspect1 + const1 + aspect1 + ... + constN + aspectN + constN+1.
 * This sequence is being retrieved and concatenated by this method.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermDateTimeToken.prototype.getValue = function (valueProvider, locale) {
    var timestamp = Number(valueProvider.getTokenValue(this.tokenName_));
    var date = new Date(isNaN(timestamp) ? 0 : timestamp);
    var res = '';
    var len = this.formatAspectsSequence_.length;
    for (var i = 0; i < len; i++)
        res += this.formatConstsSequence_[i] +
            this.formatAspectsSequence_[i](date, locale);
    res += this.formatConstsSequence_[len];

    return this.formatFinalString_(res);
};
/**
 * Stores format aspects sequence.
 *
 * @private
 * @type {Array.<Function>}
 */
anychart.formatting.terms.TermDateTimeToken.prototype.
    formatAspectsSequence_ = null;

/**
 * Stores format string consts sequence.
 *
 * @private
 * @type {Array.<Function>}
 */
anychart.formatting.terms.TermDateTimeToken.prototype.
    formatConstsSequence_ = null;
//------------------------------------------------------------------------------
//
//                           TermNumberTokenBase class.
//
//------------------------------------------------------------------------------
/**
 * Represents a base class for all number terms. It can format a number and
 * stores all formatting information.
 *
 * @constructor
 * @extends {anychart.formatting.terms.BaseTerm}
 * @param {String} tokenName Token name.
 */
anychart.formatting.terms.TermNumberTokenBase = function (tokenName, opt_maxChars, opt_finalChars) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars)
};
goog.inherits(anychart.formatting.terms.TermNumberTokenBase, anychart.formatting.terms.BaseTerm);
/**
 * Tells formatter to use minus sign instead of parentheses when the number is
 * negative.
 *
 * @private
 * @type {Boolean}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.useNegativeSign_ = true;

/**
 * Tells formatter to right pad formatted number with zeros till the fractional
 * part length would reach numDecimals_ number.
 *
 * @private
 * @type {Boolean}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.trailingZeros_ = true;

/**
 * Tells formatter to left pad formatted number with zeros till the integer part
 * length would reach leadingZeros_ number.
 *
 * @private
 * @type {int}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.leadingZeros_ = 1;

/**
 * Tells formatter to trim the fractional part to numDecimals length if it is
 * larger.
 *
 * @private
 * @type {int}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.numDecimals_ = 2;

/**
 * Tells formatter to separate thousands with this string.
 *
 * @private
 * @type {String}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.
    thousandsSeparator_ = ',';

/**
 * Tells formatter to separate integer and fractional number parts with this
 * string.
 *
 * @private
 * @type {String}
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.decimalSeparator_ = '.';

/**
 * Sets formatters. For more information about each formatter - see the
 * formatter description in it's property.
 *
 * @param {String} useNegativeSign (boolean).
 * @param {String} trailingZeros (boolean).
 * @param {String} leadingZeros (int).
 * @param {String} numDecimals (int).
 * @param {String} thousandsSeparator (string).
 * @param {String} decimalSeparator (string).
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.setFormatters = function (useNegativeSign, trailingZeros, leadingZeros, numDecimals, thousandsSeparator, decimalSeparator) {
    if (useNegativeSign !== undefined)
        this.useNegativeSign_ = (useNegativeSign.toLowerCase() == 'true');
    if (trailingZeros !== undefined)
        this.trailingZeros_ = (trailingZeros.toLowerCase() == 'true');
    if (leadingZeros !== undefined)
        this.leadingZeros_ = Number(leadingZeros);
    if (numDecimals !== undefined)
        this.numDecimals_ = Number(numDecimals);
    if (thousandsSeparator !== undefined)
        this.thousandsSeparator_ = thousandsSeparator;
    if (decimalSeparator !== undefined)
        this.decimalSeparator_ = decimalSeparator;
};

/**
 * Formats passed number according to set formatters properties and returns the
 * result.
 * Also requires negativity flag to be passed, because expects numValue to be a
 * modulus.
 *
 * @private
 * @param {Number} numValue Num value.
 * @param {Boolean} isNegative Negativity flag.
 * @return {String} Result of format number.
 */
anychart.formatting.terms.TermNumberTokenBase.prototype.formatNumber_ = function (numValue, isNegative) {
    var integer = Math.round(numValue * Math.pow(10, this.numDecimals_));

    var res = Number(integer / Math.pow(10, this.numDecimals_)).toString();
    var resInt;
    var resFrac;
    var pointPos = res.indexOf('.');
    if (pointPos == -1) {
        resInt = res;
        resFrac = '';
    } else {
        resInt = res.substr(0, pointPos);
        resFrac = res.substr(pointPos + 1, res.length - 1);
    }

    if (this.trailingZeros_ && resFrac.length < this.numDecimals_)
        while (resFrac.length < this.numDecimals_)
            resFrac += '0';
    if (this.leadingZeros_ > 1)
        while (resInt.length < this.leadingZeros_)
            resInt = '0' + resInt;
    if (this.thousandsSeparator_ && resInt.length > 3)
        for (var i = resInt.length; i > 3; i -= 3)
            resInt = resInt.substr(0, i - 3) + this.thousandsSeparator_ +
                resInt.substr(i - 3, resInt.length - 1);
    res = (resFrac != '') ?
        resInt + this.decimalSeparator_ + resFrac : resInt;
    return this.formatFinalString_((isNegative) ? (
        (this.useNegativeSign_) ? ('-' + res) : ('(' + res + ')')
        ) : res);
};
//------------------------------------------------------------------------------
//
//                           TermNumberTokenMultiScale class.
//
//------------------------------------------------------------------------------
/**
 * Represents a simple number value term that hasn't got any scaling.
 *
 * @constructor
 * @extends {anychart.formatting.terms.TermNumberTokenBase}
 * @implements {anychart.formatting.terms.ITerm}
 * @param {String} tokenName Token name.
 * @param {Array.<Object>} scaleSequence [{scale: number, suffix: string,
 * accumulativeScale: number}].
 */
anychart.formatting.terms.TermNumberTokenMultiScale = function (tokenName, opt_maxChars, opt_finalChars, scaleSequence) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars);
    this.scale_ = scaleSequence;
};
goog.inherits(anychart.formatting.terms.TermNumberTokenMultiScale,
    anychart.formatting.terms.TermNumberTokenBase);

/**
 * Stores scaling information in form that is returned by the parser.
 *
 * @private
 * @type {Array.<Object>} [{scale: number, suffix: string, accumulativeScale:
 * number}]
 */
anychart.formatting.terms.TermNumberTokenMultiScale.prototype.scale_ = null;

/**
 * Returns scaled and formatted number.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermNumberTokenMultiScale.prototype.getValue =
    function (valueProvider, locale) {
        var res = valueProvider.getTokenValue(this.tokenName_);
        var numRes = Number(res);
        if (isNaN(res)) return res;
        var neg = numRes < 0;
        if (neg) numRes = -numRes;

        var i = this.scale_.length - 1;
        var div = Math.floor(numRes / Number(this.scale_[i].accumulativeScale));
        while (div == 0 && i > 0)
            div = Math.floor(
                numRes / Number(this.scale_[--i].accumulativeScale));

        return this.formatNumber_(
            numRes / Number(this.scale_[i].accumulativeScale), neg) +
            this.scale_[i].suffix;
    };
//------------------------------------------------------------------------------
//
//                           TermNumberTokenNoScale class.
//
//------------------------------------------------------------------------------
/**
 * Represents a simple number value term that hasn't got any scaling.
 *
 * @constructor
 * @extends {anychart.formatting.terms.TermNumberTokenBase}
 * @implements {anychart.formatting.terms.ITerm}
 * @param {String} tokenName Token name.
 */
anychart.formatting.terms.TermNumberTokenNoScale = function (tokenName, opt_maxChars, opt_finalChars) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars);
};
goog.inherits(anychart.formatting.terms.TermNumberTokenNoScale,
    anychart.formatting.terms.TermNumberTokenBase);

/**
 * Returns formatted number.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermNumberTokenNoScale.prototype.getValue =
    function (valueProvider, locale) {
        var res = valueProvider.getTokenValue(this.tokenName_);
        var numRes = Number(res);
        if (isNaN(res)) return res;
        var neg = numRes < 0;
        return this.formatNumber_(neg ? -numRes : numRes, neg);
    };
//------------------------------------------------------------------------------
//
//                           TermNumberTokenOneScale class.
//
//------------------------------------------------------------------------------
/**
 * Represents a simple number value term that hasn't got any scaling.
 *
 * @constructor
 * @extends {anychart.formatting.terms.TermNumberTokenBase}
 * @implements {anychart.formatting.terms.ITerm}
 * @param {String} tokenName Token name.
 * @param {Object} scale {scale: number, suffix: string}.
 */
anychart.formatting.terms.TermNumberTokenOneScale = function (tokenName, opt_maxChars, opt_finalChars, scale) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars);
    this.scale_ = scale;
};
goog.inherits(anychart.formatting.terms.TermNumberTokenOneScale,
    anychart.formatting.terms.TermNumberTokenBase);

/**
 * Stores scaling information in form that is returned by the parser.
 *
 * @private
 * @type {Object} {scale: number, suffix: string, accumulativeScale: number}
 */
anychart.formatting.terms.TermNumberTokenOneScale.prototype.scale_ = null;

/**
 * Returns scaled and formatted number.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermNumberTokenOneScale.prototype.getValue =
    function (valueProvider, locale) {
        var res = valueProvider.getTokenValue(this.tokenName_);
        var numRes = Number(res);
        if (isNaN(res)) return res;
        var neg = numRes < 0;
        return this.formatNumber_((neg ? -numRes : numRes) /
            Number(this.scale_.scale), neg) + this.scale_.suffix;
    };
//------------------------------------------------------------------------------
//
//                           TermString class.
//
//------------------------------------------------------------------------------
/**
 * Represents a term that returns a string value by token name.
 *
 * @constructor
 * @implements {anychart.formatting.terms.ITerm}
 * @extends {anychart.formatting.terms.BaseTerm}
 */
anychart.formatting.terms.TermString = function (tokenName, opt_maxChars, opt_finalChars) {
    goog.base(this, tokenName, opt_maxChars, opt_finalChars)
};
goog.inherits(anychart.formatting.terms.TermString, anychart.formatting.terms.BaseTerm);

/**
 * Returns string value.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermString.prototype.getValue =
    function (valueProvider, locale) {
        // valueProvider can return a number as well
        var res = valueProvider.getTokenValue(this.tokenName_);
        res = res == undefined ? '' : res.toString();
        return this.formatFinalString_(res);
    };

/**
 * Stores token name.
 *
 * @private
 * @type {String}
 */
anychart.formatting.terms.TermString.prototype.tokenName_ = null;

/**
 * Stores max token value length
 *
 * @private
 * @type {int}
 */
anychart.formatting.terms.TermString.prototype.maxLength_ = Number.MAX_VALUE;
//------------------------------------------------------------------------------
//
//                           TermStringConst class.
//
//------------------------------------------------------------------------------
/**
 * Represents a term that returns constant string value, specified on
 * construction.
 *
 * @constructor
 * @implements {anychart.formatting.terms.ITerm}
 * @param {String} str String value.
 */
anychart.formatting.terms.TermStringConst = function (str) {
    this.str_ = str;
};

/**
 * Returns stored string.
 *
 * @see anychart.formatting.terms.ITerm.getValue method.
 *
 * @inheritDoc
 */
anychart.formatting.terms.TermStringConst.prototype.getValue =
    function (valueProvider, locale) {
        return this.str_;
    };

/**
 * Stores the string const.
 *
 * @private
 * @type {String}
 */
anychart.formatting.terms.TermStringConst.prototype.str_ = null;
