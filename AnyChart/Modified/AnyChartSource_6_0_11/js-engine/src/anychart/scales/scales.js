/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.scales.ScaleMode}</li>
 *  <li>@class {anychart.scales.BaseScale}</li>
 * <ul>
 */

goog.provide('anychart.scales');
goog.require('anychart.utils');
//------------------------------------------------------------------------------
//
//                          ScaleMode class.
//
//------------------------------------------------------------------------------
/**
 * Define possible scale mode values.
 * @public
 * @enum {int}
 */
anychart.scales.ScaleMode = {
    NORMAL : 0,
    STACKED : 1,
    PERCENT_STACKED : 2,
    OVERLAY : 3,
    SORTED_OVERLAY : 4
};
//------------------------------------------------------------------------------
//
//                          BaseScale class
//
//------------------------------------------------------------------------------

/**
 * Class represent base scale.
 * @public
 * @constructor
 * <p>
 *  Note: This is abstract class, you should not create instance of this class.
 * </p>
 */
anychart.scales.BaseScale = function() {
    this.setDefaults();
};
//------------------------------------------------------------------------------
//                          todo:NEED MOVE TO LINEAR SCALE
//------------------------------------------------------------------------------
/**
 * Enables smart algorithm for calculating.
 * @param {Number} value Number of decimals.
 */
anychart.scales.BaseScale.prototype.enableSmartMode = function(value) {};
/**
 * @return {Boolean} Is smart mode for scale enabled.
 */
anychart.scales.BaseScale.prototype.isSmartMode = function() {return false};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Data range minimum value.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.dataRangeMinimum_ = null;
/**
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getDataRangeMinimum = function() {
    return this.dataRangeMinimum_;
};
/**
 * @param {Number} value
 */
anychart.scales.BaseScale.prototype.setDataRangeMinimum = function(value) {
    this.dataRangeMinimum_ = value;
};

/**
 * Data range maximum value.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.dataRangeMaximum_ = null;
anychart.scales.BaseScale.prototype.getDataRangeMaximum = function() {
    return this.dataRangeMaximum_;
};
anychart.scales.BaseScale.prototype.setDataRangeMaximum = function(value) {
    this.dataRangeMaximum_ = value;
};

/**
 * Scale invert
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.inverted_ = false;
anychart.scales.BaseScale.prototype.isInverted = function() {
    return this.inverted_;
};

/**
 * Sets minimum value for the scale.
 * <p>
 *  Note: If not set - it is calculated automatically.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.minimum_ = null;

/**
 * Return scale minimum value.
 * @public
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMinimum = function() {
    return this.minimum_;
};
/**
 * Sets scale minimum value.
 * @param {Number} minimum New minimum value.
 */
anychart.scales.BaseScale.prototype.setMinimum = function(minimum) {
    this.minimum_ = minimum;
};

/**
 * Sets maximum value for the scale.
 * <p>
 *  Note: If not set - it is calculated automatically.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.maximum_ = null;

/**
 * Return scale maximum value.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMaximum = function() {
    return this.maximum_;
};

/**
 * Sets scale maximum value.
 * @param {Number} maximum New maximum value.
 */
anychart.scales.BaseScale.prototype.setMaximum = function(maximum) {
    this.maximum_ = maximum;
};
/**
 * Indicates, is minimum value calculated automatically.
 * @protected
 * @type {boolean}
 */
anychart.scales.BaseScale.prototype.isMinimumAuto_ = null;

/**
 * Indicates, is minimum value set manually.
 * @protected
 * @type {boolean}
 */
anychart.scales.BaseScale.prototype.isMinimumSet_ = null;

/**
 * Indicates, is maximum value calculated automatically.
 * @protected
 * @type {boolean}
 */
anychart.scales.BaseScale.prototype.isMaximumAuto_ = null;

/**
 * Sets the major scale step.
 * <p>
 *  Note: Major scale step used for axis labels, major tickmarks and minor grid. If not set - it is calculated automatically.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.majorInterval_ = NaN;
/**
 * @return {Number} Major interval.
 */
anychart.scales.BaseScale.prototype.getMajorInterval = function() {
    return this.majorInterval_;
};

/**
 * Indicates, is major interval value calculated automatically.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.isMajorIntervalAuto_ = null;

/**
 * Sets the minor scale step.
 * <p>
 *  Note: Minor scale step used for minor tickmarks and minor grid. If not set - it is calculated automatically.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.minorInterval_ = NaN;
/**
 * @return {Number} Minor interval.
 */
anychart.scales.BaseScale.prototype.getMinorInterval = function() {
    return this.minorInterval_;
};
/**
 * Indicates, is minor interval calculated automatically.
 * @protected
 * @type {boolean}
 */
anychart.scales.BaseScale.prototype.isMinorIntervalAuto_ = null;
/**
 * @return {Boolean}
 */
anychart.scales.BaseScale.prototype.isMinorIntervalAuto = function() {
    return this.isMinorIntervalAuto_;
};

/**
 * Sets an offset for the chart relative to minimal data point value.
 * <p>
 *  Note: Effective only when minimum attribute is set to auto. Offset is set as a ratio of the minimal value.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.minimumOffset_ = null;

/**
 * Sets an offset for the chart relative to maximum data point value.
 * <p>
 *  Note: Effective only when maximum attribute is set to auto. Offset is set as a ratio of the maximal value.
 * </p>
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.maximumOffset_ = null;
/**
 * @param {Number} value
 */
anychart.scales.BaseScale.prototype.setMaximumOffset = function(value) {
    this.maximumOffset_ = value;
};

/**
 * Sets the value where grid and axis will start.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.baseValue_ = null;

/**
 * Indicates, is base value calculated automatically.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.isBaseValueAuto_ = null;

/**
 * Sets whether an axis is inverted.
 * @protected
 * @type {boolean}
 */
anychart.scales.BaseScale.prototype.inverted_ = null;
/** @return {boolean} */
anychart.scales.BaseScale.prototype.getInverted = function() {
    return this.inverted_;
};
/** @param {boolean} value */
anychart.scales.BaseScale.prototype.setInverted = function(value) {
    this.inverted_ = value;
};

/**
 * Sets mode for the scale, this parameter defines whether chart are stacked, percent stacked, normal.
 * @protected
 * @type {int}
 */
anychart.scales.BaseScale.prototype.mode_ = null;
/**
 * @return {int}
 */
anychart.scales.BaseScale.prototype.getMode = function() {
    return this.mode_;
};
/**
 * @return {Boolean}
 */
anychart.scales.BaseScale.prototype.isPercentStacked = function() {
    return this.mode_ == anychart.scales.ScaleMode.PERCENT_STACKED;
};

anychart.scales.BaseScale.prototype.checkValue = function(value) {
    if (isNaN(this.dataRangeMinimum_) || value < this.dataRangeMinimum_)
        this.dataRangeMinimum_ = value;
    if (isNaN(this.dataRangeMaximum_) || value > this.dataRangeMaximum_)
        this.dataRangeMaximum_ = value;
};

/**
 * Minimum value of scale range in pixels.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.pixelRangeMinimum_ = null;

/**
 * Return minimum value of scale range in pixels.
 * @public
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getPixelRangeMinimum = function() {
    return this.pixelRangeMinimum_;
};

/**
 * Maximum value of scale range in pixels.
 * @protected
 * @type {Number}
 */
anychart.scales.BaseScale.prototype.pixelRangeMaximum_ = null;

/**
 * Return maximum value of scale range in pixels.
 * @public
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getPixelRangeMaximum = function() {
    return this.pixelRangeMaximum_;
};

/**
 * Set minimum and maximum values of scale range in pixels.
 * @public
 * @param {Number} minimum
 * @param {Number} maximum
 */
anychart.scales.BaseScale.prototype.setPixelRange = function(minimum, maximum) {
    this.pixelRangeMinimum_ = minimum;
    this.pixelRangeMaximum_ = maximum;
};

/**
 * Major interval count.
 * @protected
 * @type {int}
 */
anychart.scales.BaseScale.prototype.majorIntervalCount_ = null;

/**
 * Return major interval count.
 * @public
 * @return {int}
 */
anychart.scales.BaseScale.prototype.getMajorIntervalCount = function() {
    return this.majorIntervalCount_;
};

/**
 * Minor interval count.
 * @protected
 * @type {int}
 */
anychart.scales.BaseScale.prototype.minorIntervalCount_ = null;

/**
 * Return minor interval count
 * @public
 * @return {int}
 */
anychart.scales.BaseScale.prototype.getMinorIntervalCount = function() {
    return this.minorIntervalCount_;
};

/**
 * Minor interval count start value
 * @protected
 * @type {int}
 */
anychart.scales.BaseScale.prototype.minorStartInterval_ = null;

/**
 * Return minor interval start value.
 * @public
 * @return {int}
 */
anychart.scales.BaseScale.prototype.getMinorIntervalStart = function() {
    return this.minorStartInterval_;
};
//------------------------------------------------------------------------------
//                          METHODS (PUBLIC)
//------------------------------------------------------------------------------

/**
 * Deserialize base scale property's.
 * @public
 * @param {object} data JSON object with base scale settings.
 * <p>
 *  Usage sample:
 *  <code>
 *  </code>
 * </p>
 * <p>
 * Expected JSON structure:
 * <code>
 *      object {
 *          mode : string // Sets mode for the scale.
 *          minimum : number // Sets minimum value for the scale,
 *          maximum : number //  Sets maximum value for the scale,
 *          major_interval : number // Sets the major scale step,
 *          minor_interval : number // Sets the minor scale step,
 *          minimum_offset : number // Sets an offset for the chart relative to minimal data point value,
 *          maximum_offset : number // Sets an offset for the chart relative to maximum data point value,
 *          inverted : boolean // Sets whether an axis is inverted,
 *          base_value : number // Sets the value where grid and axis will start.
 *
 *      }
 * </code>
 * </p>
 */
anychart.scales.BaseScale.prototype.deserialize = function(data) {
    //aliases
    var deserialization = anychart.utils.deserialization;
    //mode
    if (deserialization.hasProp(data, 'mode')) {
        var mode = deserialization.getEnumProp(data, 'mode');
        switch (mode) {
            default:
            case 'normal':
                this.mode_ = anychart.scales.ScaleMode.NORMAL;
                break;
            case 'stacked':
                this.mode_ = anychart.scales.ScaleMode.STACKED;
                break;
            case 'percentstacked':
                this.mode_ = anychart.scales.ScaleMode.PERCENT_STACKED;
                break;
            case 'overlay':
                this.mode_ = anychart.scales.ScaleMode.OVERLAY;
                break;
            case 'sortedoverlay':
                this.mode_ = anychart.scales.ScaleMode.SORTED_OVERLAY;
                break;
        }
    }
    
    //minimum value
    if (deserialization.hasProp(data, 'minimum')) {
        var minimum = deserialization.getProp(data, 'minimum');
        this.isMinimumSet_ = true;
        if (deserialization.isAuto(minimum)) this.isMinimumAuto_ = true;
        else {
            this.isMinimumAuto_ = false;
            this.minimum_ = this.deserializeValue(minimum);
        }
    } else if (this.mode_ == anychart.scales.ScaleMode.PERCENT_STACKED) {
        this.isMinimumAuto_ = false;
        this.minimum_ = 0;
        this.isMinimumSet_ = false;
    }

    //maximum value
    if (deserialization.hasProp(data, 'maximum')) {
        var maximum = deserialization.getProp(data, 'maximum');
        if (deserialization.isAuto(maximum)) this.isMaximumAuto_ = true;
        else {
            this.isMaximumAuto_ = false;
            this.maximum_ = this.deserializeValue(maximum);
        }
    } else if (this.mode_ == anychart.scales.ScaleMode.PERCENT_STACKED) {
        this.isMaximumAuto_ = false;
        this.maximum_ = 100;
    }
    //major_interval value
    if (deserialization.hasProp(data, 'major_interval')) {
        var majorInterval = deserialization.getProp(data, 'major_interval');
        if (deserialization.isAuto(majorInterval)) this.isMajorIntervalAuto_ = true;
        else {
            this.isMajorIntervalAuto_ = false;
            this.majorInterval_ = deserialization.getNum(majorInterval);
        }
    }
    //minor_interval value
    if (deserialization.hasProp(data, 'minor_interval')) {
        var minorInterval = deserialization.getProp(data, 'minor_interval');
        if (deserialization.isAuto(minorInterval)) this.isMinorIntervalAuto_ = true;
        else {
            this.isMinorIntervalAuto_ = false;
            this.minorInterval_ = deserialization.getNum(minorInterval);
        }
    }
    //minimum_offset
    if (deserialization.hasProp(data, 'minimum_offset')) this.minimumOffset_ = deserialization.getRatioProp(data, 'minimum_offset');
    //maximum_offset
    if (deserialization.hasProp(data, 'maximum_offset')) this.maximumOffset_ = deserialization.getRatioProp(data, 'maximum_offset');
    //inverted
    if (deserialization.hasProp(data, 'inverted')) this.inverted_ = deserialization.getBoolProp(data, 'inverted');
    //base_value
    if (deserialization.hasProp(data, 'base_value')) {
        var baseValue = deserialization.getProp(data, 'base_value');
        if (deserialization.isAuto(baseValue))this.isBaseValueAuto_ = true;
        else {
            this.isBaseValueAuto_ = false;
            this.baseValue_ = this.deserializeValue(baseValue);
        }
    }
};

anychart.scales.BaseScale.prototype.deserializeValue = function(value) {
    return anychart.utils.deserialization.getNum(value);
};

/**
 * Calculate minimum and maximum scale property, based on data range property's.
 * @public
 */
anychart.scales.BaseScale.prototype.calculate = function() {
    if (isNaN(this.dataRangeMinimum_) && isNaN(this.dataRangeMaximum_) && this.isMinimumAuto_ && this.isMaximumAuto_) {
        this.isMinimumAuto_ = false;
        this.minimum_ = 0;
        this.isMaximumAuto_ = false;
        this.maximum_ = 1;
    }
    if (!this.isMaximumAuto_ && !this.isMinimumAuto_) return;
    var range = this.dataRangeMaximum_ - this.dataRangeMinimum_;
    if (this.isMinimumAuto_)
        this.minimum_ = this.getMinimumOffset_(this.dataRangeMinimum_, range);
    if (this.isMaximumAuto_)
        this.maximum_ = this.getMaximumOffset_(this.dataRangeMaximum_, range);
    if (this.maximum_ <= this.minimum_) {
        if (this.isMaximumAuto_)
            this.maximum_ = this.minimum_ + 1.0;
        else if (this.isMinimumAuto_)
            this.minimum_ = this.maximum_ - 1.0;
    }
};

/**
 * Reset all base scale settings to default.
 * @public
 */
anychart.scales.BaseScale.prototype.setDefaults = function() {
    this.minimum_ = 0;
    this.maximum_ = 1;
    this.baseValue_ = 0;
    this.minimumOffset_ = .1;
    this.maximumOffset_ = .1;
    this.inverted_ = false;
    this.dataRangeMaximum_ = NaN;
    this.dataRangeMinimum_ = NaN;

    this.isMinimumAuto_ = true;
    this.isMinimumSet_ = false;
    this.isMaximumAuto_ = true;
    this.isMajorIntervalAuto_ = true;
    this.isMinorIntervalAuto_ = true;
    this.isBaseValueAuto_ = true;

    this.mode_ = anychart.scales.ScaleMode.NORMAL;
};

/**
 * Indicates, contains passed value in scale range.
 * @param {Number} value Target value.
 * @return {boolean}
 */
anychart.scales.BaseScale.prototype.contains = function(value) {
    return anychart.utils.geom.MathUtils.contains(this.minimum_, this.maximum_, value);
};

/**
 * Indicates, contains passed value in scale range.
 * @public
 * @param {Number} value Target value.
 * @return {boolean}
 */
anychart.scales.BaseScale.prototype.linearizedContains = function(value) {
    return this.contains(value);
};

/**
 * Transform and return value in pixels to internal scale value.
 * @public
 * @param {Number} value.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.localTransform = function(value, isZeroAtAxisZero) {
    var ratio = (value - this.minimum_) / (this.maximum_ - this.minimum_);
    var rangeMax = isZeroAtAxisZero ? (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) : this.pixelRangeMaximum_;
    var rangeMin = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum_;
    var pixDistance = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;
    if (this.inverted_) return rangeMax - pixDistance;
    return rangeMin + pixDistance;
};

/**
 * Transform and return value in pixels to internal scale value.
 * @public
 * @param {Number} pixValue value in pixels.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.transformPixelToValue = function(pixValue) {
    var ratio = (pixValue - this.pixelRangeMinimum_) / (this.pixelRangeMaximum_ - this.pixelRangeMinimum_);
    var distance = (this.maximum_ - this.minimum_) * ratio;
    var tempVal = this.inverted_ ? (this.maximum_ - distance) : (this.minimum_ + distance);
    if (tempVal > this.maximum_) tempVal = this.maximum_;
    else if (tempVal < this.minimum_) tempVal = this.minimum_;
    return tempVal;
};

/**
 * Transform from internal scale value to pixel value.
 * @public
 * @param {Number} value
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.transformValueToPixel = function(value) {
    var ratio = (value - this.minimum_) / (this.maximum_ - this.minimum_);
    var pixDistance = (this.pixelRangeMaximum_ - this.pixelRangeMinimum_) * ratio;
    if (this.inverted_) return this.pixelRangeMaximum_ - pixDistance;
    return this.pixelRangeMinimum_ + pixDistance;
};

/**
 * Calculate and return current major interval value.
 * <p>
 *  Note: multiplication and division by 10 necessary to fix bug with overload float values (2.00000000001)
 * <p>
 * @public
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMajorIntervalValue = function(index) {
    return (this.baseValue_ * 10 + index * this.majorInterval_ * 10) / 10;
};

anychart.scales.BaseScale.prototype.getAxisValue = function(index) {
    return this.getMajorIntervalValue(index);
};

/**
 * Calculate and return current minor interval value.
 * <p>
 *  Note: multiplication and division by 10 necessary to fix bug with overload float values (2.00000000001)
 * <p>
 * @public
 * @param {Number} index Step index.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMinorIntervalValue = function(baseValue, index) {
    return (baseValue * 10 + index * this.minorInterval_ * 10) / 10;
};
//-----------------------------------------------------------------------------------
//                          METHODS (PROTECTED)
//-----------------------------------------------------------------------------------
/**
 * Calculate major interval count.
 * @protected
 */
anychart.scales.BaseScale.prototype.calculateMajorIntervalCount_ = function() {
    this.majorIntervalCount_ = parseInt((this.maximum_ - this.baseValue_) / this.majorInterval_ + 0.01);
    if (this.majorIntervalCount_ < 0)
        this.majorIntervalCount_ = 0;
};

/**
 * Calculate minor and minor interval count.
 * @protected
 */
anychart.scales.BaseScale.prototype.calculateIntervalCount_ = function() {
    this.calculateMajorIntervalCount_();
    this.minorIntervalCount_ = parseInt((this.majorInterval_) / this.minorInterval_ + 0.01);
    if (this.minorIntervalCount_ < 0)
        this.minorIntervalCount_ = 0;
};

/**
 * Calculate minor start interval.
 * @protected
 */
anychart.scales.BaseScale.prototype.calculateMinorStartInterval_ = function() {
    this.minorStartInterval_ = parseInt((this.minimum_ - this.baseValue_) / this.minorInterval_);
};

/**
 * Sets base value to 1, if not auto.
 * @protected
 */
anychart.scales.BaseScale.prototype.calculateBaseValue_ = function() {
    if (this.isBaseValueAuto_) return;
    this.baseValue_ = 1;
};

/**
 * Apply minimum offset value.
 * @protected
 * @param {Number} minimum Minimum value.
 * @param {Number} range Range values.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMinimumOffset_ = function(minimum, range) {
    return (minimum < 0 || minimum - this.minimumOffset_ * range >= 0) ? (minimum - this.minimumOffset_ * range) : minimum;
};

/**
 * Apply maximum offset value.
 * @protected
 * @param {Number} maximum Maximum value.
 * @param {Number} range Range values.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getMaximumOffset_ = function(maximum, range) {
    return (maximum > 0 || maximum + this.maximumOffset_ * range <= 0) ? (maximum + this.maximumOffset_ * range) : maximum;
};

/**
 * Safe mode.
 * @protected
 * @param {Number} value1 First value.
 * @param {Number} value2 Second value
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.safeMod_ = function(value1, value2) {
    if (value1 == 0) return 0;
    var ratio = value1 / value2;
    return value2 * (ratio - Math.floor(ratio));
};

/**
 * Calculate and return optimal step size.
 * @protected
 * @param {Number} range Range value.
 * @param {Number} steps Steps value
 * @param {Boolean} isBounded Defines is scale already bounded.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getOptimalStepSize_ = function(range, steps, isBounded) {
    var tempStep = range / steps;
    var mag = Math.floor(Math.log(tempStep) / Math.LN10);
    var magPow = Math.pow(10, mag);
    var magMsd = isBounded ? Math.ceil(tempStep / magPow) : anychart.utils.deserialization.getInt(tempStep / magPow + .5);

    if (magMsd > 5)
        magMsd = 10;
    else if (magMsd > 2)
        magMsd = 5;
    else if (magMsd > 1)
        magMsd = 2;

    return magMsd * magPow;
};
/**
 * Calculate and return optimal step size. Smart algorithm.
 * @protected
 * @param {Number} range Range value.
 * @param {Number} steps Steps value
 * @param {Boolean} isBounded Defines is scale already bounded.
 * @param {Number} numDecimals Number of decimals.
 * @return {Number}
 */
anychart.scales.BaseScale.prototype.getOptimalSmartStepSize_ = function(range, steps, isBounded, numDecimals) {
    var tempStep = Math.max(range / steps, Math.pow(10, -numDecimals));
    var mag = Math.floor(Math.log(tempStep) / Math.LN10);
    var magPow = Math.pow(10, mag);
    var magMsd = isBounded ? Math.ceil(tempStep / magPow) : anychart.utils.deserialization.getInt(tempStep / magPow + .5);

    if (magMsd > 5)
        magMsd = 10;
    else if (magMsd > 2)
        magMsd = 5;
    else if (magMsd > 1)
        magMsd = 2;

    return magMsd * magPow;
};
//------------------------------------------------------------------------------
//                  Resize.
//------------------------------------------------------------------------------
/**
 * Must be empty.
 */
anychart.scales.BaseScale.prototype.resize = function() {};