/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.errors.ErrorEvent};</li>
 *  <li>@class {anychart.errors.FeatureNotSupportedError};</li>
 * <ul>
 */
goog.provide('anychart.errors');
goog.require('goog.events');
//------------------------------------------------------------------------------
//
//                          EngineErrorEvent class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {goog.events.Event}
 * @param {String} type Type of event.
 * @param {int} errorCode Type of event.
 * @param {String} errorMessage Type of event.
 */
anychart.errors.ErrorEvent = function(type, errorCode, errorMessage) {
    goog.events.Event.call(this, type);
    this['errorCode'] = errorCode;
    this['errorMessage'] = errorMessage;
};
goog.inherits(anychart.errors.ErrorEvent, goog.events.Event);
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.errors.ErrorEvent.ERROR = 'anychartError';
/**
 * @type {int}
 */
anychart.errors.ErrorEvent.prototype['errorCode'] = null;
/**
 * @type {String}
 */
anychart.errors.ErrorEvent.prototype['errorMessage'] = null;
/**
 * @return {String}
 */
anychart.errors.ErrorEvent.prototype.toString = function() {
    return 'Error code: ' + this['errorCode'] + ' error message: ' + this['errorMessage'];
};
//------------------------------------------------------------------------------
//                          To string.
//------------------------------------------------------------------------------
/**
 * @return {String}
 */
anychart.errors.ErrorEvent.prototype.toString = function() {
    return 'Error message: ' + this['errorMessage'];
};
//------------------------------------------------------------------------------
//                          AxesPlot only.
//------------------------------------------------------------------------------
/**
 * @type {String}
 */
anychart.errors.axesPlotOnly_ = 'This method can be used in axes plot only.';
/**
 * Feature not supported error
 */
anychart.errors.axesPlotOnly = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 1, anychart.errors.axesPlotOnly_);
};

//------------------------------------------------------------------------------//
//                                                                              //
//                         Configurations Errors                                //
//                                                                              //
//------------------------------------------------------------------------------//

/**
 * @private
 * @type {String}
 */
anychart.errors.malformedXMLMessage = 'Malformed XML. XML parsing error.';
/**
 * Malformed XML.
 */
anychart.errors.malformedXML = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 11, anychart.errors.malformedXMLMessage)
};

/**
 * @private
 * @type {String}
 */
anychart.errors.schemaValidationFailMessage_ = 'Schema validation fail. XML structure error, mandatory nodes required, schema invalidation.';
/**
 * Schema validation fail.
 */
anychart.errors.schemaValidationFail = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 12, anychart.errors.schemaValidationFailMessage_)
};

/**
 * @private
 * @type {String}
 */
anychart.errors.mappingErrorMessage_ = 'Mapping error. Node attributes required.';
/**
 * Mapping error.
 */
anychart.errors.mappingError = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 13, anychart.errors.mappingErrorMessage_)
};

/**
 * @private
 * @type {String}
 */
anychart.errors.timestampErrorMessage_ = 'Timestamp error.';
/**
 * Timestamp error.
 */
anychart.errors.timestampError = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 14, anychart.errors.timestampErrorMessage_)
};

//------------------------------------------------------------------------------//
//                                                                              //
//                         Internal Errors                                      //
//                                                                              //
//------------------------------------------------------------------------------//

/**
 * @private
 * @type {String}
 */
anychart.errors.malformedFunctionMessage_ = 'Malformed function. Access to non-existing elements.';
/**
 * Malformed function.
 */
anychart.errors.malformedFunction = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 21, anychart.errors.malformedFunctionMessage_);
};

/**
 * @private
 * @type {String}
 */
anychart.errors.featureNotSupportedMessage_ = 'Feature is not supported.';
/**
 * Feature is not supported.
 */
anychart.errors.featureNotSupported = function(featureName) {
    throw new anychart.errors.FeatureNotSupportedError(anychart.errors.ErrorEvent.ERROR, 22, anychart.errors.featureNotSupportedMessage_, featureName);
};

/**
 * @private
 * @type {String}
 */
anychart.errors.someElementsNotSupportedMessage_ = 'Some elements not supported.';
/**
 * Some elements not supported.
 */
anychart.errors.someElementsNotSupported = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 23, anychart.errors.someElementsNotSupportedMessage_);
};

//------------------------------------------------------------------------------//
//                                                                              //
//                         External Errors                                      //
//                                                                              //
//------------------------------------------------------------------------------//

/**
 * @private
 * @type {String}
 */
anychart.errors.resourceLoadingErrorMessage_ = 'Resource loading error. Can`t load resource file.';
/**
 * Resource loading error.
 */
anychart.errors.resourceLoadingError = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 31, anychart.errors.resourceLoadingErrorMessage_);
};

/**
 * @private
 * @type {String}
 */
anychart.errors.securityErrorMessage_ = 'Security error.';
/**
 * Security error.
 */
anychart.errors.securityError = function() {
    throw new anychart.errors.ErrorEvent(anychart.errors.ErrorEvent.ERROR, 32, anychart.errors.securityErrorMessage_);
};
//------------------------------------------------------------------------------
//
//                          FeatureNotSupportedError class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.errors.ErrorEvent}
 */
anychart.errors.FeatureNotSupportedError = function(type, errorCode, errorMessage, featureName) {
    anychart.errors.ErrorEvent.call(this, type, errorCode, errorMessage);
    this['featureName'] = featureName;
};

goog.inherits(anychart.errors.FeatureNotSupportedError,
    anychart.errors.ErrorEvent);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.errors.FeatureNotSupportedError.prototype['featureName'] = null;
//------------------------------------------------------------------------------
//                          To string.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.errors.FeatureNotSupportedError.prototype.toString = function() {
    return goog.base(this, 'toString') + ' Feature: ' + this['featureName'] +'.';
};
//------------------------------------------------------------------------------
//
//                 OracleFeatureNotSupportedError class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.errors.ErrorEvent}
 */
anychart.errors.OracleFeatureNotSupportedError = function() {
    goog.base(
        this,
        anychart.errors.ErrorEvent.ERROR,
        33,
        'This chart type is not supported by Oracle APEX version of AnyChart.\n'
        +'To learn more and update your version please visit AnyChart website:\n'
        +'http://www.anychart.com/products/anychart/docs/oracle_version_vs_regular.php');
};
goog.inherits(anychart.errors.OracleFeatureNotSupportedError,
    anychart.errors.ErrorEvent);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.errors.OracleFeatureNotSupportedError.prototype.url_ = 'http://www.anychart.com/products/anychart/docs/oracle_version_vs_regular.php';
/**
 * @return {String}
 */
anychart.errors.OracleFeatureNotSupportedError.prototype.getUrl = function() {
    return this.url_;
};
