/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.render.svg.SVGRenderer};</li>
 * <ul>
 */
goog.provide('anychart.render.svg');
goog.require('anychart.utils.exports');
goog.require('anychart.visual');
goog.require('anychart.chartView');
goog.require('anychart.utils.geom');
goog.require('goog.events');
//------------------------------------------------------------------------------
//
//                          SVGRenderer class
//
//------------------------------------------------------------------------------
/**
 * @export
 * @constructor
 * @implements {anychart.render.IChartRenderer}
 */
anychart.render.svg.SVGRenderer = function () {
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.render.svg.SVGRenderer.prototype.svgManager_ = null;
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.render.svg.SVGRenderer.prototype.mainChartView_ = null;
/**
 * @private
 * @type {anychart.AnyChart}
 */
anychart.render.svg.SVGRenderer.prototype.anychartBase_ = null;
/**
 * @inheritDoc
 */
anychart.render.svg.SVGRenderer.prototype.canSetConfigAfterWrite = function () {
    return true;
};
//------------------------------------------------------------------------------
//                          Initialization
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.init = function (chart) {
    this.fixIEMetaDataBug();

    if (!this.canWrite()) return;

    //base
    this.anychartBase_ = chart;
    //create engine
    this.svgManager_ = new anychart.svg.SVGManager();
    this.mainChartView_ = new anychart.chartView.MainChartView();
    this.fetchSettings_();
    this.configureEventListeners_();

    if (!this.anychartBase_['visible']) this.hide();
};
anychart.render.svg.SVGRenderer.prototype.fixIEMetaDataBug = function() {
    if (!goog.userAgent.IE) return;
    var head = document.getElementsByTagName('head')[0];
    var meta = document.createElement('meta');
    meta['name'] = 'anychart.com';
    meta['content'] = 'IE=Edge';
    meta['http-equiv'] = 'X-UA-Compatible';
    head.appendChild(meta);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.onBeforeChartCreate = function () {
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.onBeforeChartDraw = function () {
};
//------------------------------------------------------------------------------
//                          Getting SVG as base64
//------------------------------------------------------------------------------
/**
 * Gets chart SVG image as string or base64 encoded string.
 * @param {Boolean} base64 If set to true then base64 encoded string is returned, if set to false - plain string.
 * @return {String} Base64 encoded string or plain string.
 */
anychart.render.svg.SVGRenderer.prototype.getSVG = function (base64) {
    var serializer = new XMLSerializer();
    var svgNode = this.svgManager_.getSVG();
    var svgText = serializer.serializeToString(svgNode);
    var result;
    result = base64 ? anychart.utils.Base64Utils.encode(svgText) : svgText;
    return result;
};
//------------------------------------------------------------------------------
//                          Show/Hide.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.render.svg.SVGRenderer.prototype.show = function () {
    this.svgManager_.getSVG().setAttribute('visibility', 'visible');
};
/**
 * @inheritDoc
 */
anychart.render.svg.SVGRenderer.prototype.hide = function () {
    this.svgManager_.getSVG().setAttribute('visibility', 'hidden');
};
//------------------------------------------------------------------------------
//                          Embed
//------------------------------------------------------------------------------
anychart.render.svg.SVGRenderer.prototype.chartDrawed_ = false;
/**@private*/
anychart.render.svg.SVGRenderer.prototype.fetchSettings_ = function () {
    //messages
    this.mainChartView_.setWaitingForData(this.anychartBase_['messages']['waitingForData']);
    this.mainChartView_.setNoData(this.anychartBase_['messages']['noData']);
    this.mainChartView_.setLoadingTemplates(this.anychartBase_['messages']['loadingTemplates']);
    this.mainChartView_.setLoadingResources(this.anychartBase_['messages']['loadingResources']);
    this.mainChartView_.setLoadingConfig(this.anychartBase_['messages']['loadingConfig']);
    this.mainChartView_.setInitializing(this.anychartBase_['messages']['init']);
    if (this.anychartBase_['watermark'])
        this.mainChartView_.setWatermarkText(this.anychartBase_['watermark']);
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.notSupported_ = function () {
    if (window.console) window.console.log('Feature not supported yet');
};
/**@inheritDoc*/
anychart.render.svg.SVGRenderer.prototype.canWrite = function () {
    return window['SVGAngle'] != undefined;
};
/**@inheritDoc*/
anychart.render.svg.SVGRenderer.prototype.write = function (target) {
    if (!this.canWrite()) return;
    var svg = this.svgManager_.getSVG();
    svg.setAttribute('width', this.anychartBase_['width']);
    svg.setAttribute('height', this.anychartBase_['height']);
    target.appendChild(svg);
    document['body'].appendChild(this.svgManager_.getMeasureSVG());

    this.bounds_ = this.getSVGBounds();


    this.mainChartView_.initVisual(
        this.svgManager_,
        this.svgManager_.getSVG(),
        this.bounds_);

    this.chartDrawed_ = true;
    return true;
};
/**
 * @private
 * Returns percent width attribute if percent.
 * @param {SVGElement} element SVG element.
 * @return {Number} Percent width.
 */
anychart.render.svg.SVGRenderer.prototype.getPercentWidth_ = function (element) {
    var des = anychart.utils.deserialization,
        width = this.getRealSizeProp(this.anychartBase_['width']);
    return des.isPercent(width) ?
        element['parentNode']['clientWidth'] * des.getPercent(width) :
        width;
};
/**
 * @private
 * Returns percent height attribute if percent.
 * @param {SVGElement} element SVG element.
 * @return {Number} Percent height.
 */
anychart.render.svg.SVGRenderer.prototype.getPercentHeight_ = function (element) {
    var des = anychart.utils.deserialization,
        height = this.getRealSizeProp(this.anychartBase_['height']);
    return des.isPercent(height) ?
        element['parentNode']['clientHeight'] * des.getPercent(height) :
        height;
};
/**
 * @private
 * @param {String} size
 */
anychart.render.svg.SVGRenderer.prototype.getRealSizeProp = function (size) {
    return /^([0-9]+\%?)/.exec(size)[1];
};
/**
 * @private
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.render.svg.SVGRenderer.prototype.getSVGBounds = function () {
    var svgWidth,
        svgHeight,
        svg = this.svgManager_.getSVG();

    svgWidth = svg['clientWidth'];
    svgHeight = svg['clientHeight'];

    if (!svgWidth) {
        svgWidth = this.getPercentWidth_(svg);
    }
    if (!svgHeight) {
        svgHeight = this.getPercentHeight_(svg);
    }

    return new anychart.utils.geom.Rectangle(
        0,
        0,
        svgWidth,
        svgHeight);
};
/**
 * @inheritDoc
 */
anychart.render.svg.SVGRenderer.prototype.updatePrintForFirefox = function () {
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setSize = function (width, height) {
    this.svgManager_.getSVG().setAttribute('width', width);
    this.svgManager_.getSVG().setAttribute('height', height);
    this.resize(width, height);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.remove = function () {
    if (this.svgManager_.getSVG()['parentNode']) {
        this.svgManager_.getSVG()['parentNode']['removeChild'](this.svgManager_.getSVG());
        this.removeEventListeners_();
    }
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.clear = function () {
    this.mainChartView_.clearChartData();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.refresh = function () {
    this.mainChartView_.refresh();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updateData = function (path, data) {
    this.mainChartView_.updateData(path, data);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setLoading = function (opt_message) {
    this.mainChartView_.showLoadingConfigMessage(opt_message);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getInformation = function () {
    return this.mainChartView_.serialize();
};
//------------------------------------------------------------------------------
//                          Events
//------------------------------------------------------------------------------
/**
 * Configure chart event listeners.
 * @private
 */
anychart.render.svg.SVGRenderer.prototype.configureEventListeners_ = function () {
    //chart engine events
    goog.events.listen(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_DRAW,
        this.drawEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_RENDER,
        this.renderEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.ValueChangeEvent.VALUE_CHANGE,
        this.valueChangeEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.errors.ErrorEvent.ERROR,
        this.errorEventListener_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_REFRESH,
        this.refreshEventHandler_,
        false,
        this);

    //dashboard engine events
    goog.events.listen(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_DRAW,
        this.drawViewEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_RENDER,
        this.renderViewEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_REFRESH,
        this.refreshViewEventHandler_,
        false,
        this);

    //resize events
    goog.events.listen(
        this.svgManager_.getSVG(),
        goog.events.EventType.RESIZE,
        this.resizeEventHandler_,
        false,
        this);

    goog.events.listen(
        window,
        goog.events.EventType.RESIZE,
        this.resizeEventHandler_,
        false,
        this);

    //mouse events
    goog.events.listen(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEMOVE,
        this.mouseMoveEventHandler_,
        false,
        this);

    goog.events.listen(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEDOWN,
        this.mouseDownEventHandler_,
        false,
        this);

    goog.events.listen(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEUP,
        this.mouseUpEventHandler_,
        false,
        this);

    //point events
    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_CLICK,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_OVER,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_OUT,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_DOWN,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_UP,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_SELECT,
        this.pointEventHandler_,
        false,
        this);

    goog.events.listen(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_DESELECT,
        this.pointEventHandler_,
        false,
        this);
};
/**
 * Remove all component event listeners
 */
anychart.render.svg.SVGRenderer.prototype.removeEventListeners_ = function () {
    //chart engine events
    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_DRAW,
        this.drawEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_RENDER,
        this.renderEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.ValueChangeEvent.VALUE_CHANGE,
        this.valueChangeEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.errors.ErrorEvent.ERROR,
        this.errorEventListener_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.EngineEvent.ANYCHART_REFRESH,
        this.refreshEventHandler_,
        false,
        this);

    //dashboard engine events
    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_DRAW,
        this.drawViewEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_RENDER,
        this.renderViewEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.DashboardEvent.DASHBOARD_VIEW_REFRESH,
        this.refreshViewEventHandler_,
        false,
        this);

    //resize events
    goog.events.unlisten(
        this.svgManager_.getSVG(),
        goog.events.EventType.RESIZE,
        this.resizeEventHandler_,
        false,
        this);

    goog.events.unlisten(
        window,
        goog.events.EventType.RESIZE,
        this.resizeEventHandler_,
        false,
        this);

    //mouse events
    goog.events.unlisten(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEMOVE,
        this.mouseMoveEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEDOWN,
        this.mouseDownEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.svgManager_.getSVG(),
        goog.events.EventType.MOUSEUP,
        this.mouseUpEventHandler_,
        false,
        this);

    //point events
    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_CLICK,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_OVER,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_OUT,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_DOWN,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_MOUSE_UP,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_SELECT,
        this.pointEventHandler_,
        false,
        this);

    goog.events.unlisten(
        this.mainChartView_,
        anychart.events.PointEvent.POINT_DESELECT,
        this.pointEventHandler_,
        false,
        this);
};
//------------------------------------------------------------------------------
//                     Chart engine events handlers
//------------------------------------------------------------------------------
/**@private*/
anychart.render.svg.SVGRenderer.prototype.drawEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'draw'});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.renderEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'render'});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.refreshEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'refresh'});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.errorEventListener_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'error', 'errorCode':event['errorCode'], 'errorMessage':event['errorMessage']});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.valueChangeEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'valueChange', 'params':event.getParams()});
};
//------------------------------------------------------------------------------
//                     Dashboard engine events handlers
//------------------------------------------------------------------------------
/**@private*/
anychart.render.svg.SVGRenderer.prototype.drawViewEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'drawView', 'view':event.getViewId()});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.renderViewEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'renderView', 'view':event.getViewId()});
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.refreshViewEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartEvents'])
        this.anychartBase_.dispatchEvent({'type':'refreshView', 'view':event.getViewId()});
};
//------------------------------------------------------------------------------
//                  Resize events handler.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.render.svg.SVGRenderer.prototype.resizeEventHandler_ = function (event) {
    var newBounds = this.getSVGBounds();

    if (this.bounds_.width != newBounds.width || this.bounds_.height != newBounds.height) {
        this.bounds_ = newBounds.clone();

        this.resize(this.bounds_.width, this.bounds_.height);

        if (this.anychartBase_['enabledChartEvents'])
            this.anychartBase_.dispatchEvent({'type':'resize'});
    }
};
//------------------------------------------------------------------------------
//                          SVG mouse events handlers
//------------------------------------------------------------------------------
/**@private*/
anychart.render.svg.SVGRenderer.prototype.mouseMoveEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartMouseEvents'])
        this.anychartBase_.dispatchEvent({
            'type':'chartMouseMove',
            'mouseX':event.clientX,
            'mouseY':event.clientY
        });
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.mouseDownEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartMouseEvents'])
        this.anychartBase_.dispatchEvent({
            'type':'chartMouseDown',
            'mouseX':event.clientX,
            'mouseY':event.clientY
        });
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.mouseUpEventHandler_ = function (event) {
    if (this.anychartBase_['enabledChartMouseEvents'])
        this.anychartBase_.dispatchEvent({
            'type':'chartMouseUp',
            'mouseX':event.clientX,
            'mouseY':event.clientY
        });
};
//------------------------------------------------------------------------------
//                          Point events handlers
//------------------------------------------------------------------------------
/**@private*/
anychart.render.svg.SVGRenderer.prototype.pointEventHandler_ = function (event) {
    if (event.getPoint())
        var data = event.getPoint().canSerializeForEvent() ?
            event.getPoint().serializeForEvent() :
            event.getPoint().serialize();

    this.anychartBase_.dispatchEvent({
        'type':event.getType(),
        'data':data,
        'mouseX':event.getMouseX(),
        'mouseY':event.getMouseY()
    });
};
/**@private*/
anychart.render.svg.SVGRenderer.prototype.multiplePointsEventHandler_ = function (event) {
    if (!event || event.getNumPoints() == 0) return;

    var data = {};
    data['points'] = [];
    var pointsCount = event.getNumPoints();

    for (var i = 0; i < pointsCount; i++) {
        var point = event.getPointAt(i);
        data['points'].push(point.canSerializeForEvent() ?
            point.serializeForEvent() : point.serialize());
    }

    this.anychartBase_.dispatchEvent({'type':event.getType(), 'data':data});
};
//------------------------------------------------------------------------------
//                             Resize.
//------------------------------------------------------------------------------
/**
 * Resize chart.
 * @param {Number} width New chart width.
 * @param {Number} width New chart height.
 */
anychart.render.svg.SVGRenderer.prototype.resize = function (width, height) {
    if (!this.mainChartView_ || !width || width < 0 || !height || height < 0 || !this.mainChartView_) return;

    this.bounds_.width = width;
    this.bounds_.height = height;

    this.mainChartView_.resize(this.bounds_);
};
//------------------------------------------------------------------------------
//                          Config setters
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setXMLFile = function (path) {
    this.mainChartView_.setXMLFile(path);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setXMLData = function (data) {
    this.mainChartView_.setXMLData(data);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setJSONData = function (data) {
    this.mainChartView_.setJSON(data);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setJSONFile = function (path) {
    this.mainChartView_.setJSONFile(path);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setXMLDataFromString = function (data) {
    this.mainChartView_.setXMLData(data);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setXMLDataFromURL = function (data) {
    this.mainChartView_.setXMLFile(data);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setViewXMLFile = function (viewId, url) {
    this.mainChartView_.setViewXMLFile(viewId, url, null);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setViewData = function (viewId, data) {
    this.mainChartView_.setViewXMLData(viewId, data, null);
};
//------------------------------------------------------------------------------
//                          Point
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.addPoint = function (seriesId, pointData) {
    this.mainChartView_.addPoint(seriesId, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.addPointAt = function (seriesId, pointIndex, pointData) {
    this.mainChartView_.addPointAt(seriesId, pointIndex, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.removePoint = function (seriesId, pointId) {
    this.mainChartView_.removePoint(seriesId, pointId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updatePoint = function (seriesId, pointId, pointData) {
    this.mainChartView_.updatePoint(seriesId, pointId, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updatePointData = function (groupName, pointName, data) {
    //TODO: Need support for all chart types. Now only for gauge. dirty condition
    if (this.mainChartView_.chartType_ == 2)
        this.mainChartView_.updatePointData(groupName, pointName, data);
    else this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.highlightPoint = function (seriesId, pointId, highlight) {
    this.mainChartView_.highlightPoint(seriesId, pointId, highlight);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.selectPoint = function (seriesId, pointId, select) {
    this.mainChartView_.selectPoint(seriesId, pointId, select);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updatePointWithAnimation = function(seriesId, pointId, newValue, animationSettings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.startPointsAnimation = function() {
    this.notSupported_();
};
//------------------------------------------------------------------------------
//                          Series
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.addSeries = function (seriesData) {
    this.mainChartView_.addSeries(seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.addSeriesAt = function (index, seriesData) {
    this.mainChartView_.addSeriesAt(index, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.removeSeries = function (seriesId) {
    this.mainChartView_.removeSeries(seriesId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updateSeries = function (seriesId, seriesData) {
    this.mainChartView_.updateSeries(seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.showSeries = function (seriesId, seriesData) {
    this.mainChartView_.showSeries(seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.highlightSeries = function (seriesId, highlight) {
    this.mainChartView_.highlightSeries(seriesId, highlight);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.highlightCategory = function (categoryName, highlight) {
    this.mainChartView_.highlightCategory(categoryName, highlight);
};
//------------------------------------------------------------------------------
//                          Axes
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.showAxisMarker = function (markerId, seriesDate) {
    this.notSupported_();
};
//------------------------------------------------------------------------------
//                          Custom attributes
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setPlotCustomAttribute = function (attributeName, attributeValue) {
    this.mainChartView_.setPlotCustomAttribute(attributeName, attributeValue);
};
//------------------------------------------------------------------------------
//                       Dashboard external methods
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_setPlotCustomAttribute = function (viewId, attributeName, attributeValue) {
    this.mainChartView_.view_setPlotCustomAttribute(viewId, attributeName, attributeValue);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_addSeries = function (viewId, seriesData) {
    this.mainChartView_.view_addSeries(viewId, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_removeSeries = function (viewId, seriesId) {
    this.mainChartView_.view_removeSeries(viewId, seriesId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_addSeriesAt = function (viewId, index, seriesData) {
    this.mainChartView_.view_addSeriesAt(viewId, index, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_updateSeries = function (viewId, seriesId, seriesData) {
    this.mainChartView_.view_updateSeries(viewId, seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_showSeries = function (viewId, seriesId, seriesData) {
    this.mainChartView_.view_showSeries(viewId, seriesId, seriesData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_addPoint = function (viewId, seriesId, pointData) {
    this.mainChartView_.view_addPoint(viewId, seriesId, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_addPointAt = function (viewId, seriesId, pointIndex, pointData) {
    this.mainChartView_.view_addPointAt(viewId, seriesId, pointIndex, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_removePoint = function (viewId, seriesId, pointId) {
    this.mainChartView_.view_removePoint(viewId, seriesId, pointId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_updatePoint = function (viewId, seriesId, pointId, pointData) {
    this.mainChartView_.view_updatePoint(viewId, seriesId, pointId, pointData);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_clear = function (viewId) {
    this.mainChartView_.view_clearChartData(viewId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_refresh = function (viewId) {
    this.mainChartView_.view_refresh(viewId);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_highlightSeries = function (viewId, seriesId, highlight) {
    this.mainChartView_.view_highlightSeries(viewId, seriesId, highlight);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_highlightPoint = function (viewId, seriesId, pointId, highlight) {
    this.mainChartView_.view_highlightPoint(viewId, seriesId, pointId, highlight);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_highlightCategory = function (viewId, categoryName, highlight) {
    this.mainChartView_.view_highlightCategory(viewId, categoryName, highlight);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.view_selectPoint = function (viewId, seriesId, pointId, select) {
    this.mainChartView_.view_selectPoint(viewId, seriesId, pointId, select);
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.updateViewPointData = function (viewId, groupName, pointName, data) {
    this.mainChartView_.updateViewPointData(viewId, groupName, pointName, data);
};
//------------------------------------------------------------------------------
//                          Export (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getPNG = function (opt_width, opt_height) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getPDF = function (opt_width, opt_height) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.printChart = function () {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.saveAsImage = function (opt_width, opt_height) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.saveAsPDF = function (opt_width, opt_height) {
    this.notSupported_();
};
//------------------------------------------------------------------------------
//                          Scroll (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.scrollXTo = function (value) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.scrollYTo = function (value) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.scrollTo = function (xValue, yValue) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.viewScrollXTo = function (viewId, value) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.viewScrollYTo = function (viewId, value) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.viewScrollTo = function (viewId, xValue, yValue) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getXScrollInfo = function () {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getYScrollInfo = function () {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getViewXScrollInfo = function (viewId) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.getViewYScrollInfo = function (viewId) {
    this.notSupported_();
};
//------------------------------------------------------------------------------
//                          Zoom (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setXZoom = function (settings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setYZoom = function (settings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setZoom = function (xSettings, ySettings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setViewXZoom = function (viewId, settings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setViewYZoom = function (viewId, settings) {
    this.notSupported_();
};
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.setViewZoom = function (viewId, xSettings, ySettings) {
    this.notSupported_();
};
//------------------------------------------------------------------------------
//                          Animation (ignored)
//------------------------------------------------------------------------------
/**@inheritDoc */
anychart.render.svg.SVGRenderer.prototype.animate = function () {
    this.notSupported_();
};



