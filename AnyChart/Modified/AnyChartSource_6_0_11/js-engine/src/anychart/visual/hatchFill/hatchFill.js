//Copyright 2011 AnyChart. All rights reserved.
/**
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.hatchFill.HatchFill}</li>
 *     <li>@class {anychart.visual.hatchFill.HatchFill.HatchFillType}</li>
 *     <li>@class {anychart.visual.hatchFill.SVGHatchFillManager}.</li>
 * </ul>
 */

goog.provide('anychart.visual.hatchFill');

goog.require('anychart.utils');
goog.require('anychart.visual.color');
goog.require('anychart.visual.gradient');
//------------------------------------------------------------------------------
//
//                          Constructor
//
//------------------------------------------------------------------------------
/**
 * Defines HatchFill class constructor
 * The class should be used in the way like this:
 * @constructor
 */
anychart.visual.hatchFill.HatchFill = function () {
};
//------------------------------------------------------------------------------
//
//                          Hatch fill type
//
//------------------------------------------------------------------------------
/**
 * Describes possible hatch fill types. Contains deserialize function.
 *
 * @enum {int}
 * @return {Number} Deserialized from string value of hatch fill.
 */
anychart.visual.hatchFill.HatchFill.HatchFillType = {
    BACKWARD_DIAGONAL:0,
    FORWARD_DIAGONAL:1,
    HORIZONTAL:2,
    VERTICAL:3,
    DASHED_BACKWARD_DIAGONAL:4,
    GRID:5,
    DASHED_FORWARD_DIAGONAL:6,
    DASHED_HORIZONTAL:7,
    DASHED_VERTICAL:8,
    DIAGONAL_CROSS:9,
    DIAGONAL_BRICK:10,
    DIVOT:11,
    HORIZONTAL_BRICK:12,
    VERTICAL_BRICK:13,
    CHECKER_BOARD:14,
    CONFETTI:15,
    PLAID:16,
    SOLID_DIAMOND:17,
    ZIG_ZAG:18,
    WEAVE:19,
    PERCENT_05:20,
    PERCENT_10:21,
    PERCENT_20:22,
    PERCENT_25:23,
    PERCENT_30:24,
    PERCENT_40:25,
    PERCENT_50:26,
    PERCENT_60:27,
    PERCENT_70:28,
    PERCENT_75:29,
    PERCENT_80:30,
    PERCENT_90:31,
    deserialize:function (type) {
        switch (type) {
            case 'backwarddiagonal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.BACKWARD_DIAGONAL;
            case 'forwarddiagonal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.FORWARD_DIAGONAL;
            case 'horizontal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.HORIZONTAL;
            case 'vertical':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.VERTICAL;
            case 'dashedbackwarddiagonal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DASHED_BACKWARD_DIAGONAL;
            case 'dashedforwarddiagonal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DASHED_FORWARD_DIAGONAL;
            case 'dashedhorizontal':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DASHED_HORIZONTAL;
            case 'dashedvertical':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DASHED_VERTICAL;
            case 'diagonalcross':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DIAGONAL_CROSS;
            case 'diagonalbrick':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DIAGONAL_BRICK;
            case 'divot':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.DIVOT;
            case 'horizontalbrick':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.HORIZONTAL_BRICK;
            case 'verticalbrick':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.VERTICAL_BRICK;
            case 'checkerboard':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.CHECKER_BOARD;
            case 'confetti':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.CONFETTI;
            case 'plaid':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PLAID;
            case 'soliddiamond':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.SOLID_DIAMOND;
            case 'zigzag':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.ZIG_ZAG;
            case 'weave':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.WEAVE;
            case 'percent05':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_05;
            case 'percent10':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_10;
            case 'percent20':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_20;
            case 'percent25':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_25;
            case 'percent30':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_30;
            case 'percent40':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_40;
            case 'percent50':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_50;
            case 'percent60':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_60;
            case 'percent70':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_70;
            case 'percent75':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_75;
            case 'percent80':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_80;
            case 'percent90':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.PERCENT_90;
            case 'grid':
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.GRID;
            default:
                return anychart.visual.hatchFill.HatchFill.
                    HatchFillType.BACKWARD_DIAGONAL;
        }
    }
};
//------------------------------------------------------------------------------
//
//                          Properties
//
//------------------------------------------------------------------------------
/**
 * Indicates whether the hatch fill should be applied
 *
 * @private
 * @type {boolean}
 */
anychart.visual.hatchFill.HatchFill.prototype.enabled_ = true;

/**
 * Indicates whether the hatch fill should be applied.
 * @return {Boolean} Is hatch fill enabled.
 */
anychart.visual.hatchFill.HatchFill.prototype.isEnabled = function () {
    return this.enabled_;
};

/**
 * Defines hatch fill type.
 *
 * @private
 * @type {anychart.visual.hatchFill.HatchFill.HatchFillType}
 */
anychart.visual.hatchFill.HatchFill.prototype.type_ =
    anychart.visual.hatchFill.HatchFill.HatchFillType.BACKWARD_DIAGONAL;

/**
 * @private
 * @type {Boolean}
 */
anychart.visual.hatchFill.HatchFill.prototype.isDynamicHatchType_ = false;

/**
 * Return hatch fill type.
 * @return {anychart.visual.hatchFill.HatchFill.HatchFillType} Hatch fill type.
 */
anychart.visual.hatchFill.HatchFill.prototype.getType = function () {
    return this.type_;
};

/**
 * Defines color of the hatch fill
 *
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.hatchFill.HatchFill.prototype.color_ =
    anychart.utils.deserialization.getColor(null, 0.5);

/**
 * Return hatch fill color.
 * @return {anychart.visual.color.ColorContainer} Color instance.
 */
anychart.visual.hatchFill.HatchFill.prototype.getColor = function () {
    return this.color_;
};

/**
 * Defines hatch fill color opacity
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.hatchFill.HatchFill.prototype.opacity_ = .5;

/**
 * Return hatch fill opacity.
 * @return {Number} Opacity.
 */
anychart.visual.hatchFill.HatchFill.prototype.getOpacity = function () {
    return this.opacity_;
};

/**
 * Defines hatch fill lines thickness
 *
 * @private
 * @type {Number}
 */
anychart.visual.hatchFill.HatchFill.prototype.thickness_ = 1;

/**
 * Return hatch fill thickness.
 * @return {Number} Thickness.
 */
anychart.visual.hatchFill.HatchFill.prototype.getThickness = function () {
    return this.thickness_;
};

/**
 * Defines hatch fill pattern size
 *
 * @private
 * @type {Number}
 */
anychart.visual.hatchFill.HatchFill.prototype.patternSize_ = 10;

/**
 * Return hatch fill pattern size.
 * @return {Number} Pattern size.
 */
anychart.visual.hatchFill.HatchFill.prototype.getPatternSize = function () {
    return this.patternSize_;
};
//------------------------------------------------------------------------------
//
//                          Deserialization
//
//------------------------------------------------------------------------------
/**
 * Deserializes hatch fill settings from JSON object
 * Expects the following JSON structure:
 * <code>
 *  {
 *      enabled: boolean,
 *      type: string, // one of HatchFillType identifiers, deserialized by
 *      HatchFillType.deserialize()
 *      opacity: number, // [0..1], values that are not in these bounds are set
 *      to the closest bound
 *      color: string, // color string, parsed by colorParser
 *      thickness: number, // line thickness
 *      patternSize: int // affects hatch fill rarefaction
 *  }
 * </code>
 *
 * @param {object} data JSON object.
 */
anychart.visual.hatchFill.HatchFill.prototype.deserialize = function (data) {
    // aliases
    var deserialization = anychart.utils.deserialization;

    // @enabled deserialization
    this.enabled_ = deserialization.isEnabled(data);
    if (!this.enabled_) return;

    // deserializing hatchFill type
    if (deserialization.hasProp(data, 'type')) {
        var strType = deserialization.getLStringProp(data, 'type');
        this.isDynamicHatchType_ = strType == '%hatchtype';
        if (!this.isDynamicHatchType_)
            this.type_ = anychart.visual.hatchFill.HatchFill.
                HatchFillType.deserialize(
                deserialization.getEnumItem(strType));
    }

    // deserializing hatchFill opacity
    if (deserialization.hasProp(data, 'opacity'))
        this.opacity_ = deserialization.getRatio(
            deserialization.getProp(data, 'opacity'));

    // deserializing hatch fill color (opacity is counted on implicitly)
    if (deserialization.hasProp(data, 'color'))
        this.color_ = deserialization.getColor(
            deserialization.getLStringProp(
                data,
                'color'),
            this.opacity_);

    // deserializing thickness
    if (deserialization.hasProp(data, 'thickness'))
        this.thickness_ = deserialization.getNum(
            deserialization.getProp(data, 'thickness'));

    // deserializing pattern size
    if (deserialization.hasProp(data, 'pattern_size'))
        this.patternSize_ = deserialization.getInt(
            deserialization.getProp(data, 'pattern_size'));
};

/**
 * String gradient representation.
 * @return {String} String gradient representation.
 */
anychart.visual.hatchFill.HatchFill.prototype.toString = function () {
    return 'T' + this.type_ + 'C' + this.color_.getColor().toRGBAHash() +
        'TH' + this.thickness_ + 'PS' + this.patternSize_;
};
//------------------------------------------------------------------------------
//
//                          Visual
//
//------------------------------------------------------------------------------
/**
 * Gets the SVG hatch fill settings.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.visual.hatchFill.HatchFill.HatchFillType} opt_hatchType
 * Hatch type.
 * @param {anychart.visual.color.Color} opt_color Color instance.
 * @return {String} Svg hatch fill settings.
 */
anychart.visual.hatchFill.HatchFill.prototype.getSVGHatchFill = function (svgManager, opt_hatchType, opt_color) {
    if (!this.enabled_) return '';
    this.type_ = this.isDynamicHatchType_ ? opt_hatchType : this.type_;
    return 'fill:' + svgManager.getHatchFillManager().
        getSVGPattern(this, opt_color) + ';';
};

//------------------------------------------------------------------------------
//
//                          SVGHatchFillManager
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 */
anychart.visual.hatchFill.SVGHatchFillManager = function (svgManager) {
    this.svgManager_ = svgManager;
    this.patternMap_ = {};
};
//------------------------------------------------------------------------------
//
//                          Properties
//
//------------------------------------------------------------------------------
/**
 * Last gradient element id.
 * @private
 * @type {int}
 */
anychart.visual.hatchFill.SVGHatchFillManager.lastId_ = 0;

/**
 * Link to SVG manager.
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.svgManager_ = null;

/**
 * Gradient uid map.
 * @private
 * @type {Object}
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.patternMap_ = null;
//------------------------------------------------------------------------------
//
//                          Pattern generation
//
//------------------------------------------------------------------------------
/**
 * Return hatch fill id.
 * @param {anychart.visual.hatchFill.HatchFill} hatchFill Hatch fill settings.
 * @param {anychart.visual.color.Color} opt_color Color instance.
 * @return {String} Hatch fill uid.
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.getSVGPattern =
    function (hatchFill, opt_color) {
        var id = hatchFill.toString();
        if (this.patternMap_[id]) return this.patternMap_[id];
        var uid = 'hatchfill_' + String(anychart.visual.hatchFill.
            SVGHatchFillManager.lastId_++);
        var patternSize = String(hatchFill.getPatternSize());

        var element = this.svgManager_.createSVGElement('pattern');
        element.setAttribute('id', uid);
        element.setAttribute('x', 0);
        element.setAttribute('y', 0);
        element.setAttribute('width', patternSize);
        element.setAttribute('height', patternSize);
        element.setAttribute('patternUnits', 'userSpaceOnUse');
        this.createPatternPath_(element, hatchFill, opt_color);
        this.svgManager_.addDef(element);

        uid = 'url(#' + uid + ')';
        this.patternMap_[id] = uid;
        return uid;
    };

/**
 * @private
 * @param {SVGPatternElement} pattern SVG element.
 * @param {anychart.visual.hatchFill.HatchFill} hatchFill Hatch fill.
 * @param {anychart.visual.color.Color} opt_color Color instance.
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.createPatternPath_ = function (pattern, hatchFill, opt_color) {
    var patternSize = hatchFill.getPatternSize(),
        rect, s, ds, path, pathData, i, j;

    switch (hatchFill.getType()) {
        case anychart.visual.hatchFill.HatchFill.HatchFillType.BACKWARD_DIAGONAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(-1, 0, patternSize + 1, 0, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.setAttribute('patternTransform', 'rotate(-45)');
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.FORWARD_DIAGONAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(-1, 0, patternSize + 1, 0, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.setAttribute('patternTransform', 'rotate(45)');
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.HORIZONTAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(-1, patternSize / 2, patternSize + 1, patternSize / 2, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.VERTICAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(patternSize / 2, -1, patternSize / 2, patternSize + 1, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DIAGONAL_CROSS:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(0, patternSize / 2, patternSize, patternSize / 2, hatchFill.getThickness());
            pathData += this.svgManager_.pRLine(patternSize / 2, 0, patternSize / 2, patternSize, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.setAttribute('patternTransform', 'rotate(45)');
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.GRID:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(-1, patternSize / 2, patternSize + 1, patternSize / 2, hatchFill.getThickness());
            pathData += this.svgManager_.pRLine(patternSize / 2, -1, patternSize / 2, patternSize + 1, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.HORIZONTAL_BRICK:
            pathData = this.createBrick_(pattern, hatchFill, patternSize, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.VERTICAL_BRICK:
            this.createBrick_(pattern, hatchFill, patternSize, opt_color);
            pattern.setAttribute('patternTransform', 'rotate(90)');
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DIAGONAL_BRICK:
            path = this.svgManager_.createPath();
            this.createBrick_(pattern, hatchFill, patternSize, opt_color);
            pattern.setAttribute('patternTransform', 'rotate(45)');
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.CHECKER_BOARD:
            path = this.svgManager_.createRect(0, 0, patternSize / 2, patternSize / 2);
            var path2 = this.svgManager_.createRect(patternSize / 2, patternSize / 2, patternSize, patternSize);
            pattern.appendChild(path);
            pattern.appendChild(path2);
            this.applyFill_(hatchFill, path, opt_color);
            this.applyFill_(hatchFill, path2, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.CONFETTI:
            s = patternSize / 8;
            rect = new anychart.utils.geom.Rectangle(0, 0, patternSize / 4, patternSize / 4);
            this.appendRectWithFill_(this.svgManager_, 0, s * 2, rect.width, rect.height, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, s, s * 5, rect.width, rect.height, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, s * 2, 0, rect.width, rect.height, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, s * 4, s * 4, rect.width, rect.height, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, s * 5, s, rect.width, rect.height, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, s * 6, s * 6, rect.width, rect.height, hatchFill, pattern, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.PLAID:
            this.appendRectWithFill_(this.svgManager_, 0, 0, patternSize / 2, patternSize / 2, hatchFill, pattern, opt_color);
            s = patternSize / 8;
            var isSelected = false;
            for (var dx = 0; dx < 2; dx++) {
                isSelected = false;
                for (var xPos = 0; xPos < 4; xPos++) {
                    isSelected = !isSelected;
                    for (var yPos = 0; yPos < 4; yPos++) {
                        if (isSelected) {
                            this.appendRectWithFill_(
                                this.svgManager_,
                                xPos * s + dx * patternSize / 2,
                                yPos * s + patternSize / 2,
                                s,
                                s,
                                hatchFill,
                                pattern,
                                opt_color);
                        }
                        isSelected = !isSelected;
                    }
                }
            }
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.SOLID_DIAMOND:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pMove(patternSize / 2, 0);
            pathData += this.svgManager_.pLine(0, patternSize / 2);
            pathData += this.svgManager_.
                pLine(patternSize / 2, patternSize);
            pathData += this.svgManager_.
                pLine(patternSize, patternSize / 2);
            pathData += this.svgManager_.pLine(patternSize / 2, 0);
            pathData += this.svgManager_.pFinalize();
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyFill_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DASHED_FORWARD_DIAGONAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(0, 0, patternSize / 2, patternSize / 2, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DASHED_BACKWARD_DIAGONAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(patternSize / 2, 0, 0, patternSize / 2, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DASHED_HORIZONTAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(0, 0, patternSize / 2, 0, hatchFill.getThickness());
            pathData += this.svgManager_.pRLine(patternSize / 2, patternSize / 2, patternSize, patternSize / 2, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DASHED_VERTICAL:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pRLine(0, 0, 0, patternSize / 2, hatchFill.getThickness());
            pathData += this.svgManager_.pRLine(patternSize / 2, patternSize / 2, patternSize / 2, patternSize, hatchFill.getThickness());
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.DIVOT:
            var percent = 0.1;
            var innerPercent = 0.2;
            var padding = patternSize * percent;
            ds = patternSize * (1 - percent * 2 - innerPercent) / 2;

            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pMove(padding + ds, padding);
            pathData += this.svgManager_.pLine(padding, padding + ds / 2);
            pathData += this.svgManager_.pLine(padding + ds, padding + ds);
            pathData += this.svgManager_.pMove(patternSize - padding - ds, patternSize - padding - ds);
            pathData += this.svgManager_.pLine(patternSize - padding, patternSize - padding - ds / 2);
            pathData += this.svgManager_.pLine(patternSize - padding - ds, patternSize - padding);

            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.ZIG_ZAG:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pMove(0, 0);
            pathData += this.svgManager_.
                pLine(patternSize / 2, patternSize / 2);
            pathData += this.svgManager_.pLine(patternSize, 0);
            pathData += this.svgManager_.pMove(0, patternSize / 2);
            pathData += this.svgManager_.
                pLine(patternSize / 2, patternSize);
            pathData += this.svgManager_.
                pLine(patternSize, patternSize / 2);
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.WEAVE:
            path = this.svgManager_.createPath();
            pathData = this.svgManager_.pMove(0, 0);
            pathData += this.svgManager_.
                pLine(patternSize / 2, patternSize / 2);
            pathData += this.svgManager_.pLine(patternSize, 0);
            pathData += this.svgManager_.pMove(0, patternSize / 2);
            pathData += this.svgManager_.
                pLine(patternSize / 2, patternSize);
            pathData += this.svgManager_.
                pLine(patternSize, patternSize / 2);
            pathData += this.svgManager_.
                pMove(patternSize / 2, patternSize / 2);
            pathData += this.svgManager_.
                pLine(patternSize * 3 / 4, patternSize * 3 / 4);
            pathData += this.svgManager_.
                pMove(patternSize, patternSize / 2);
            pathData += this.svgManager_.
                pLine(patternSize * 3 / 4, patternSize / 4);
            path.setAttribute('d', pathData);
            pattern.appendChild(path);
            this.applyStroke_(hatchFill, path, opt_color);
            break;

        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_05:
            pattern.setAttribute('width', 8);
            pattern.setAttribute('height', 8);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 4, 4, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_10:
            pattern.setAttribute('width', 8);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 4, 2, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_20:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_25:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 2);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 1, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_30:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 3, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_40:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 8);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 4, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 4, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 5, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 5, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 6, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 6, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 7, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 7, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_50:
            pattern.setAttribute('width', 2);
            pattern.setAttribute('height', 2);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 1, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_60:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 3, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_70:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 0, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 1, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 3, 2, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 0, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 1, 3, 1, 1, hatchFill, pattern, opt_color);
            this.appendRectWithFill_(this.svgManager_, 2, 3, 1, 1, hatchFill, pattern, opt_color);
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_75:
            pattern.setAttribute('width', 4);
            pattern.setAttribute('height', 4);
            this.appendRectWithFill_(this.svgManager_, 0, 0, 4, 4, hatchFill, pattern, opt_color);
            this.appendRectWithStaticColor_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, 'white');
            this.appendRectWithStaticColor_(this.svgManager_, 2, 2, 1, 1, hatchFill, pattern, 'white');
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_80:
            pattern.setAttribute('width', 8);
            pattern.setAttribute('height', 4);

            for (i = 0; i < 8; i++)
                for (j = 0; j < 4; j++)
                    this.appendRectWithFill_(this.svgManager_, i, j, 1, 1, hatchFill, pattern, opt_color);

            this.appendRectWithStaticColor_(this.svgManager_, 0, 0, 1, 1, hatchFill, pattern, 'white');
            this.appendRectWithStaticColor_(this.svgManager_, 4, 2, 1, 1, hatchFill, pattern, 'white');
            break;
        case anychart.visual.hatchFill.HatchFill.HatchFillType.PERCENT_90:
            pattern.setAttribute('width', 8);
            pattern.setAttribute('height', 8);

            for (i = 0; i < 8; i++)
                for (j = 0; j < 8; j++)
                    this.appendRectWithFill_(this.svgManager_, i, j, 1, 1, hatchFill, pattern, opt_color);

            this.appendRectWithStaticColor_(this.svgManager_, 7, 7, 1, 1, hatchFill, pattern, 'white');
            this.appendRectWithStaticColor_(this.svgManager_, 4, 3, 1, 1, hatchFill, pattern, 'white');
            break;
    }
};
//------------------------------------------------------------------------------
//                  Utils.
//------------------------------------------------------------------------------
anychart.visual.hatchFill.SVGHatchFillManager.prototype.createBrick_ = function (pattern, hatchFill, patternSize, opt_color) {
    var path = this.svgManager_.createPath();
    var pathData = this.svgManager_.pRLine(0, 0, 0, patternSize / 2 - 1, hatchFill.getThickness());
    pathData += this.svgManager_.pRLine(0, patternSize / 2 - 1, patternSize, patternSize / 2 - 1, hatchFill.getThickness());
    pathData += this.svgManager_.pRLine(patternSize / 2, patternSize / 2 - 1, patternSize / 2, patternSize - 1, hatchFill.getThickness());
    pathData += this.svgManager_.pRLine(0, patternSize - 1, patternSize, patternSize - 1, hatchFill.getThickness());
    path.setAttribute('d', pathData);
    pattern.appendChild(path);
    this.applyStroke_(hatchFill, path, opt_color);
};
anychart.visual.hatchFill.SVGHatchFillManager.prototype.appendRectWithFill_ = function (svgManager, x, y, width, height, hatchFill, pattern, opt_color) {
    var r = svgManager.createRect(x, y, width, height);
    this.applyFill_(hatchFill, r, opt_color);
    pattern.appendChild(r);
};
anychart.visual.hatchFill.SVGHatchFillManager.prototype.appendRectWithStaticColor_ = function (svgManager, x, y, width, height, hatchFill, pattern, color) {
    var r = svgManager.createRect(x, y, width, height);
    r.setAttribute('fill', color);
    r.setAttribute('stroke', 'none');
    pattern.appendChild(r);
};
/**
 * Apply stroke settings to pattern element.
 * @private
 * @param {anychart.visual.hatchFill.HatchFill} hatchFill Hatch fill settings.
 * @param {SVGElement} pattern Pattern element.
 * @param {anychart.visual.color.Color} opt_color Point color.
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.applyStroke_ = function (hatchFill, pattern, opt_color) {
    pattern.setAttribute('fill', 'none');
    pattern.setAttribute('stroke-width', hatchFill.getThickness());
    pattern.setAttribute('stroke-opacity', hatchFill.getOpacity());
    pattern.setAttribute('stroke', hatchFill.getColor().
        getColor(opt_color).toRGB());
};

/**
 * Apply fill settings to pattern element.
 * @private
 * @param {anychart.visual.hatchFill.HatchFill} hatchFill Hatch fill settings.
 * @param {SVGElement} pattern Pattern element.
 * @param {anychart.visual.color.Color} opt_color Point color.
 */
anychart.visual.hatchFill.SVGHatchFillManager.prototype.applyFill_ = function (hatchFill, pattern, opt_color) {
    pattern.setAttribute('fill', hatchFill.getColor().getColor(opt_color).toRGB());
    pattern.setAttribute('fill-opacity', hatchFill.getOpacity());
    pattern.setAttribute('stroke', 'none');
};
//------------------------------------------------------------------------------
//                              Clear.
//------------------------------------------------------------------------------
anychart.visual.hatchFill.SVGHatchFillManager.prototype.clear = function () {
    this.patternMap_ = {};
};
