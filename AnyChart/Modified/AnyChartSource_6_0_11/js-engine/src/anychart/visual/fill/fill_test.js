goog.require('anychart.visual.fill');

$(document).ready(function() {
    module('visual/fill/Fill');

    test('Empty fill', function() {
        var fill = new anychart.visual.fill.Fill();
        ok(fill != null, 'object exists');
        ok(fill.isEnabled(), 'isEnabled()');
    });

    test('Advanced tests exists', function() {
        ok(false, 'no tests implemented');
    });
});

