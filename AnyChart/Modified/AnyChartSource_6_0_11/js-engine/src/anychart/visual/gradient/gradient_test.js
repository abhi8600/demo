goog.require('anychart.visual.gradient');

$(document).ready(function() {
    module('visual/gradient/Gradient');

    test('Empty gradient', function() {
        var gradient = new anychart.visual.gradient.Gradient();
        ok(gradient != null, 'object exists');
        
    });
    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });


    module('visual/gradient/GradientKey');

    test('Empty gradient key', function() {
        var key = new anychart.visual.gradient.GradientKey();
        ok(key != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });


    module('visual/gradient/SVGGradientManager');

    test('Empty gradient manager', function() {
        var manager = new anychart.visual.gradient.SVGGradientManager(SVGManager);
        ok(manager != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

});

