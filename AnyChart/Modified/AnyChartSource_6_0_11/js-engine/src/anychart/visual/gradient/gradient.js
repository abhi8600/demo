/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.gradient.Gradient}</li>
 *     <li>@class {anychart.visual.gradient.Gradient.GradientType}</li>
 *     <li>@class {anychart.visual.gradient.GradientKey}</li>
 *     <li>@class {anychart.visual.gradient.SVGGradientManager}.</li>
 * </ul>
 */
goog.provide('anychart.visual.gradient');

goog.require('anychart.utils');
goog.require('anychart.visual.color');

//------------------------------------------------------------------------------
//
//                          Gradient key
//
//------------------------------------------------------------------------------
/**
 * Represents one gradient key point.
 *
 * @constructor
 * @param {number=} opt_opacity [0..1] - default gradient opacity, 1 by def.
 * @param {anychart.visual.color.Color=} opt_color Color.
 * @param {number=} opt_position [0..1] - default gradient key position, 1 by
 * def.
 */
anychart.visual.gradient.GradientKey = function (opt_opacity, opt_color, opt_position) {
    if (!isNaN(opt_opacity)) this.opacity_ = opt_opacity;
    if (opt_color) this.color_ = opt_color;
    if (opt_position) this.position_ = opt_position;
};
//------------------------------------------------------------------------------
//
//                          Gradient key settings
//
//------------------------------------------------------------------------------
/**
 * Gradient key position.
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.gradient.GradientKey.prototype.position_ = NaN;

/**
 * Return gradient key position in percent.
 * @return {String} Position in percent.
 */
anychart.visual.gradient.GradientKey.prototype.getPercentPosition = function () {
    return String(this.position_ * 100) + '%';
};

/**
 * Return gradient key byte position.
 * @return {Number} Position.
 */
anychart.visual.gradient.GradientKey.prototype.getBytePosition = function () {
    return this.position_ * 255.0;
};

/**
 * Return gradient key position.
 * @return {Number} Position.
 */
anychart.visual.gradient.GradientKey.prototype.getPosition = function () {
    return this.position_;
};

/**
 * Sets gradient key position.
 * @param {Number} value Value.
 */
anychart.visual.gradient.GradientKey.prototype.setPosition = function (value) {
    this.position_ = value;
};

/**
 * Gradient key color.
 *
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.gradient.GradientKey.prototype.color_ =
    anychart.utils.deserialization.getColor(null, 1);

/**
 * Gradient key color getter.
 * @return {anychart.visual.color.ColorContainer} Color instance.
 */
anychart.visual.gradient.GradientKey.prototype.getColor = function () {
    return this.color_;
};

/**
 * Gradient key color opacity.
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.gradient.GradientKey.prototype.opacity_ = 1;

/**
 * Return gradient opacity.
 * @return {Number} Gradient opacity.
 */
anychart.visual.gradient.GradientKey.prototype.getOpacity = function () {
    return this.opacity_;
};

/**
 * Deserializes gradient_ key settings
 * Expects the next JSON structure:
 * <code>
 *  {
 *      position: number,
 *      opacity: number, // [0..1]
 *      color: string // color string that will be parsed by colorParser
 *  }
 * </code>
 *
 * @param {object} data JSON object.
 */
anychart.visual.gradient.GradientKey.prototype.deserialize = function (data) {
    // aliases
    var deserialization = anychart.utils.deserialization;

    // deserializing key position
    if (deserialization.hasProp(data, 'position'))
        this.position_ = deserialization.getRatio(
            deserialization.getProp(data, 'position'));

    // deserializing key opacity
    if (deserialization.hasProp(data, 'opacity'))
        this.opacity_ *= deserialization.getRatio(
            deserialization.getProp(data, 'opacity'));

    // deserializing key color (opacity is counted on implicitly)
    if (deserialization.hasProp(data, 'color'))
        this.color_ = deserialization.getColor(
            deserialization.getProp(data, 'color'), this.opacity_);
};


/**
 * Gradient calculated color.
 * @private
 * @type {String}
 */
anychart.visual.gradient.GradientKey.prototype.calculatedColor_ = 0;

/**
 * Return calculated gradient color value.
 * @return {String} Gradient color value.
 */
anychart.visual.gradient.GradientKey.prototype.getCalculatedColor = function () {
    return this.calculatedColor_;
};

/**
 * Calculate gradient color value.
 * @param {anychart.visual.color.Color} opt_color Color instance.
 */
anychart.visual.gradient.GradientKey.prototype.prepare = function (opt_color) {
    this.calculatedColor_ = this.color_.getColor(opt_color).toRGB();
};

/**
 * String gradient key representation.
 * @return {String} String key representation.
 */
anychart.visual.gradient.GradientKey.prototype.toString = function () {
    return '{ position: ' + this.position_ + ', opacity: ' + this.opacity_ +
        ', calculatedColor: ' + this.calculatedColor_ + '}';
};


//------------------------------------------------------------------------------
//
//                          Gradient
//
//------------------------------------------------------------------------------
/**
 * Describes gradient settings
 * The class should be used in the way like this:
 * <code>
 *  var gradient = new anychart.visual.fill.Gradient();
 *  gradient.deserialize(gradientSettingsJSON);
 *  var ctx = canvas.getContext('2d');
 *  ctx.beginPath();
 *  // do some path drawing
 *  ctx.closePath();
 *  var bounds = new anychart.utils.geom.Rectangle(...); // bounds of the path,
 *  required for gradient filling
 *  gradient.applySettings(ctx, bounds);
 * </code>
 *
 * @constructor
 * @param {Number} gradientOpacity - [0..1] default opacity for gradient keys.
 */
anychart.visual.gradient.Gradient = function (gradientOpacity) {
    this.opacity_ = gradientOpacity;
    this.keys_ = [];
};
//------------------------------------------------------------------------------
//
//                          Gradient settings
//
//------------------------------------------------------------------------------
/**
 * Describes possible gradient types.
 *
 * @enum {int}
 */
anychart.visual.gradient.Gradient.GradientType = {
    LINEAR:0,
    RADIAL:1
};

/**
 * Defines default gradient opacity.
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.gradient.Gradient.prototype.opacity_ = 1;

/**
 * Defines gradient color keys
 *
 * @private
 * @type {Array.<anychart.visual.gradient.GradientKey>}
 */
anychart.visual.gradient.Gradient.prototype.keys_ = null;

/**
 * Get gradient key by index.
 * @param {int} index Key index.
 * @return {anychart.visual.gradient.GradientKey} Gradient key.
 */
anychart.visual.gradient.Gradient.prototype.getKeyAt = function (index) {
    return this.keys_[index];
};

/**
 * Gradient key count getter.
 * @return {int} Count of gradient keys.
 */
anychart.visual.gradient.Gradient.prototype.keyCount = function () {
    return this.keys_.length;
};

/**
 * Defines gradient angle in degrees
 *
 * @private
 * @type {Number}
 */
anychart.visual.gradient.Gradient.prototype.angle_ = 0;

/**
 * Return gradient angle degrees.
 * @return {Number} Angle.
 */
anychart.visual.gradient.Gradient.prototype.getAngle = function () {
    return this.angle_;
};

/**
 * Sets gradient angle.
 * @param {Number} value Value.
 */
anychart.visual.gradient.Gradient.prototype.setAngle = function (value) {
    this.angle_ = value;
};

/**
 * Sets the location of the start of the radial gradient as radius ratio in
 * radial coordinate system.
 *
 * @private
 * @type {Number} (-1..1)
 */
anychart.visual.gradient.Gradient.prototype.focalPoint_ = 0;

/**
 * Return gradient focal point.
 * @return {Number} Focal point.
 */
anychart.visual.gradient.Gradient.prototype.getFocalPoint = function () {
    return this.focalPoint_;
};
/**
 * @param {Number} value
 */
anychart.visual.gradient.Gradient.prototype.setFocalPoint = function (value) {
    this.focalPoint_ = value;
};
/**
 * Sets gradient type
 *
 * @private
 * @type {int} (anychart.visual.gradient.Gradient.GradientType enum values)
 */
anychart.visual.gradient.Gradient.prototype.type_ =
    anychart.visual.gradient.Gradient.GradientType.LINEAR;

/**
 * Return gradient type.
 * @return {anychart.visual.gradient.Gradient.GradientType} Gradient type.
 */
anychart.visual.gradient.Gradient.prototype.getType = function () {
    return this.type_;
};
/**
 * @param {int} value
 */
anychart.visual.gradient.Gradient.prototype.setType = function (value) {
    this.type_ = value;
};
/**
 * Gradient bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.visual.gradient.Gradient.prototype.bounds_ = null;

/**
 * Get gradient bounds.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.visual.gradient.Gradient.prototype.getBounds = function () {
    return this.bounds_;
};

//------------------------------------------------------------------------------
//
//                          Deserialization
//
//------------------------------------------------------------------------------
/**
 * Deserializes gradient settings
 * Expects the next JSON structure:
 * <code>
 *  {
 *      type: string, // 'linear'|'radial', 'linear' by def
 *      angle: number, // radial gradient angle in degrees ([0..360])
 *      focal_point: number, // radial gradient focal point ([-1..1])
 *      key: array of GradientKey // gradient key points
 *  }
 * </code>
 *
 * @param {object} data JSON object.
 */
anychart.visual.gradient.Gradient.prototype.deserialize = function (data) {
    // aliases
    var deserialization = anychart.utils.deserialization;

    // deserializing gradient_ type
    if (deserialization.hasProp(data, 'type')) this.type_ =
        (deserialization.getEnumItem(
            deserialization.getProp(data, 'type')) == 'radial') ?
            anychart.visual.gradient.Gradient.GradientType.RADIAL :
            anychart.visual.gradient.Gradient.GradientType.LINEAR;

    // deserializing gradient angle
    if (deserialization.hasProp(data, 'angle')) this.angle_ =
        deserialization.getNum(deserialization.getProp(data, 'angle'));

    // deserializing gradient focal point
    if (deserialization.hasProp(data, 'focal_point'))
        this.focalPoint_ = deserialization.getExtendedRatio(
            deserialization.getProp(data, 'focal_point'));

    // deserializing gradient keys
    var keys = deserialization.getPropArray(data, 'key');
    var len = keys.length;
    for (var i = 0; i < len; i++) {
        var key = new anychart.visual.gradient.GradientKey(this.opacity_);
        key.deserialize(keys[i]);
        this.keys_[i] = key;
    }

    this.checkKeys_();
};

//TODO description
/** @private */
anychart.visual.gradient.Gradient.prototype.checkKeys_ = function () {
    var len = this.keys_.length;

    if (isNaN(this.keys_[0].getPosition()))
        this.keys_[0].setPosition(0);
    if (isNaN(this.keys_[len - 1].getPosition()))
        this.keys_[len - 1].setPosition(1);

    var i = 1;
    while (true) {
        while (i < len && !isNaN(this.keys_[i].getPosition())) i++;
        if (i == len) break;
        var start = i - 1;
        var end = len - 1;
        while (i < len && isNaN(this.keys_[i].getPosition())) i++;

        var br = this.keys_[start].getPosition();
        var tr = this.keys_[end].getPosition();
        for (var j = 1; j < i - start; j++)
            this.keys_[j].setPosition(br + j * (tr - br) / (i - start));
    }
};

/**
 * String gradient representation
 * @return {String} Gradient string representation.
 */
anychart.visual.gradient.Gradient.prototype.toString = function () {
    var res = '{ type: ' + this.type_ + ', angle: ' + this.angle_ +
        ', focal_point: ' + this.focalPoint_ + ', keys: [';
    var keysStr = [];
    for (var i = 0; i < this.keys_.length; i++) {
        keysStr.push(this.keys_[i].toString());
    }
    res += keysStr.join(', ');
    res += '],';
    res += this.bounds_.toString();
    res += '}';
    return res;
};


/**
 * Preparing gradient settings.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {anychart.visual.color.Color} opt_color Color instance.
 */
anychart.visual.gradient.Gradient.prototype.prepare = function (bounds, opt_color) {
    this.bounds_ = bounds;
    var len = this.keys_.length;
    for (var i = 0; i < len; i++)
        this.keys_[i].prepare(opt_color);
};

//------------------------------------------------------------------------------
//
//                          SVGGradientManager class
//
//------------------------------------------------------------------------------
/**
 * Gradient manager.
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 */
anychart.visual.gradient.SVGGradientManager = function (svgManager) {
    this.svgManager_ = svgManager;
    this.gradientsMap_ = {};
};
//------------------------------------------------------------------------------
//
//                          Properties
//
//------------------------------------------------------------------------------
/**
 * Last gradient element id.
 * @private
 * @type {int}
 */
anychart.visual.gradient.SVGGradientManager.lastId_ = 0;

/**
 * Link to SVG manager.
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.visual.gradient.SVGGradientManager.prototype.svgManager_ = null;

/**
 * Gradient uid map.
 * @private
 * @type {Object}
 */
anychart.visual.gradient.SVGGradientManager.prototype.gradientsMap_ = null;
//------------------------------------------------------------------------------
//
//                          Gradient generation
//
//------------------------------------------------------------------------------
/**
 * Return gradient id.
 * @param {anychart.visual.gradient.Gradient} gradient Gradient settings.
 * @param {Boolean} opt_userSpaceOnUse userSpaceOnUse.
 * @return {String} Gradient id.
 */
anychart.visual.gradient.SVGGradientManager.prototype.getSVGGradient = function (gradient, opt_userSpaceOnUse) {
        if (!opt_userSpaceOnUse) opt_userSpaceOnUse = false;
        var id = gradient.toString();
        if (this.gradientsMap_[id]) return this.gradientsMap_[id];
        var element = (gradient.getType() ==
            anychart.visual.gradient.Gradient.GradientType.LINEAR) ?
            this.createLinearGradient_(gradient, opt_userSpaceOnUse) :
            this.createRadialGradient_(gradient, opt_userSpaceOnUse);

        var uid = 'gradient_' + (anychart.visual.gradient.
            SVGGradientManager.lastId_++).toString();
        element.setAttribute('id', uid);

        uid = 'url(#' + uid + ')';

        this.svgManager_.addDef(element);
        this.gradientsMap_[id] = uid;
        return uid;
    };

/**
 * Создание линейного SVG градиента в соответствии с переданными настройками.
 * <p>
 *     Note: В SVG линейный градиет задается двумя точками (вектор градиента),
 *     собстенно помимо создания самого елемента градиента, метод вычисляет
 *     положения этих точек.
 * </p>
 * @private
 * @param {anychart.visual.gradient.Gradient} gradient Gradient settings.
 * @return {SVGLinearGradientElement} Linear SVG gradient.
 */
anychart.visual.gradient.SVGGradientManager.prototype.createLinearGradient_ = function (gradient, opt_userSpaceOnUse) {
        var element = this.svgManager_.createSVGElement('linearGradient');
        if (opt_userSpaceOnUse) {
            var angle = gradient.getAngle();
            var bounds = gradient.getBounds();
            var x1 = bounds.x;
            var y1 = bounds.y + bounds.height / 2;
            var x2 = bounds.x + bounds.width;
            var y2 = bounds.y + bounds.height / 2;

            var tx = (x1 + x2) / 2;
            var ty = (y1 + y2) / 2;

            var transform = "rotate(" + angle + ")"; // + "," + tx + ", " + ty + ")";
            element.setAttribute('x1', x1);
            element.setAttribute('y1', y1);
            element.setAttribute('x2', x2);
            element.setAttribute('y2', y2);
            element.setAttribute('gradientTransform', transform);
            element.setAttribute('spreadMethod', 'pad');
            element.setAttribute('gradientUnits', 'userSpaceOnUse');

        } else {
            var angle = anychart.utils.geom.common.
                degToRad(gradient.getAngle());
            angle -= Math.PI / 2;
            var tangent = Math.tan(angle);
            var dx = 0;
            var dy = 0;

            if (Math.abs(tangent) != Number.POSITIVE_INFINITY) {
                dx = tangent / 2;
                dy = 1 / (tangent * 2);
            }

            var swapPoints = false;
            if (Math.abs(dx) <= 0.5) {
                dy = -.5;
                swapPoints = Math.cos(angle) < 0;
            } else {
                dx = -.5;
                swapPoints = Math.sin(angle) > 0;
            }

            var pt1 = {
                x:0.5 + dx,
                y:0.5 + dy
            };
            var pt2 = {
                x:0.5 - dx,
                y:0.5 - dy
            };

            var res = swapPoints ? { start:pt2, end:pt1} :
            { start:pt1, end:pt2};

            element.setAttribute('x1', res.start.x);
            element.setAttribute('y1', res.start.y);
            element.setAttribute('x2', res.end.x);
            element.setAttribute('y2', res.end.y);
            element.setAttribute('gradientUnits', 'objectBoundingBox');
        }
        this.addStops_(element, gradient);
        return element;

    };

/**
 * Create radial gradient.
 * @private
 * @param {anychart.visual.gradient.Gradient} gradient Gradient settings.
 * @param {Boolean} opt_userSpaceOnUse userSpaceOnUse.
 * @return {SVGRadialGradientElement} Radial SVG gradient.
 */
anychart.visual.gradient.SVGGradientManager.prototype.createRadialGradient_ = function (gradient, opt_userSpaceOnUse) {
    if (!opt_userSpaceOnUse) opt_userSpaceOnUse = false;
    if ((opt_userSpaceOnUse) && gradient.getBounds().width != gradient.getBounds().height)
        opt_userSpaceOnUse = false;

    if (!opt_userSpaceOnUse) {
        var element = this.svgManager_.createSVGElement('radialGradient');
        var angle = anychart.utils.geom.common.degToRad(gradient.getAngle());
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        var centerPoint = new anychart.utils.geom.Point(0.5, 0.5);
        var focalPoint = new anychart.utils.geom.Point(
            /*centerPoint.x + */gradient.getFocalPoint() * cos,
            /*centerPoint.y + */gradient.getFocalPoint() * sin
        );

        element.setAttribute('cx', centerPoint.x);
        element.setAttribute('cy', centerPoint.y);
        element.setAttribute('fx', (focalPoint.x + 1) / 2);
        element.setAttribute('fy', (focalPoint.y + 1) / 2);
        element.setAttribute('gradientUnits', 'objectBoundingBox');
        element.setAttribute('spreadMethod', 'pad');
        this.addStops_(element, gradient);
        return element;
    } else {
        element = this.svgManager_.createSVGElement('radialGradient');
        angle = anychart.utils.geom.common.degToRad(gradient.getAngle());

        cos = Math.cos(angle);
        sin = Math.sin(angle);

        var bounds = gradient.getBounds();
        var cx = bounds.x + bounds.width / 2;
        var cy = bounds.y + bounds.height / 2;
        var r = Math.min(bounds.width, bounds.height) / 2;
        var fp = gradient.getFocalPoint();
        var fx = cx + fp * r * cos;
        var fy = cy + fp * r * sin;
        element.setAttribute('cx', cx);
        element.setAttribute('cy', cy);
        element.setAttribute('r', r);
        element.setAttribute('fx', fx);
        element.setAttribute('fy', fy);
        element.setAttribute('gradientUnits', 'userSpaceOnUse');
        element.setAttribute('spreadMethod', 'pad');
        this.addStops_(element, gradient);
        return element;

    }
};
/**
 * Gets gradient id.
 * @private
 * @param {String} strGradient Gradient string.
 */
anychart.visual.gradient.SVGGradientManager.prototype.getGradientId_ = function (strGradient) {
    var strStart = "fill:url(#";
    return strGradient.substr(strGradient.indexOf(strStart) + strStart.length, strGradient.length - strStart.length - 2);
};
/**
 * Deletes gradient node from defs.
 * @private
 * @param {String} id Gradient id.
 */
anychart.visual.gradient.SVGGradientManager.prototype.deleteGradientFromTree_ = function (id) {
    var defsNode = this.svgManager_.defs_;
    var gradients = defsNode.childNodes;
    for (var child in gradients) {
        // != undefined - это костыль лдя фаерфокса.
        if (gradients[child] != undefined && gradients[child]['id'] == id) {
            defsNode.removeChild(gradients[child]);
        }
    }
};

/**
 * Removes gradient from gradients map and from defs tree.
 * @param {String} strGradient Gradient string.
 */
anychart.visual.gradient.SVGGradientManager.prototype.removeGradient = function (strGradient) {
    var found = false;
    var id = this.getGradientId_(strGradient);
    var uid = 'url(#' + id + ')';
    for (var ids in this.gradientsMap_) {
        if (this.gradientsMap_[ids] == uid) {
            delete (this.gradientsMap_[ids]);
            found = true;
            if (found) {
                this.deleteGradientFromTree_(id);
                found = false;
            }
        }
    }
};


/**
 * Add stops to gradient element.
 * @private
 * @param {SVGGradientElement} element SVG gradient element.
 * @param {anychart.visual.gradient.Gradient} gradient Gradient setting.
 */
anychart.visual.gradient.SVGGradientManager.prototype.addStops_ = function (element, gradient) {
    var len = gradient.keyCount();
    for (var i = 0; i < len; i++) {
        var key = gradient.getKeyAt(i);
        var stop = this.svgManager_.createSVGElement('stop');
        stop.setAttribute('offset', key.getPercentPosition());
        stop.setAttribute('style', 'stop-color:' +
            key.getCalculatedColor() +
            ';stop-opacity:' + key.getOpacity());
        element.appendChild(stop);
    }
};
//------------------------------------------------------------------------------
//                  Clear.
//------------------------------------------------------------------------------
anychart.visual.gradient.SVGGradientManager.prototype.clear = function () {
    this.gradientsMap_ = {};
};
