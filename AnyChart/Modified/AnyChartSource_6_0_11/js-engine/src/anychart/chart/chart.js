/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.chart.ChartTitle};</li>
 *  <li>@class {anychart.chart.Chart}.</li>
 * <ul>
 */
goog.provide('anychart.chart');

goog.require('anychart.errors');
goog.require('anychart.styles');
goog.require('anychart.layout');
goog.require('anychart.plots');
goog.require('anychart.plots.gaugePlot');
goog.require('anychart.plots.axesPlot');
goog.require('anychart.plots.piePlot');
goog.require('anychart.plots.funnelPlot');
goog.require('anychart.plots.treeMapPlot');
goog.require('anychart.plots.radarPolarPlot');
goog.require('anychart.templates');
goog.require('anychart.utils');
goog.require('anychart.visual.background');
goog.require('anychart.visual.text');
goog.require('anychart.controls');
//------------------------------------------------------------------------------
//
//                           ChartTitle class.
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @extends {anychart.visual.text.BaseTitle}
 * @param {anychart.chartView.MainChartView} mainChartView Ссылка на MainChartView
 * @param {anychart.plots.BasePlot} plot Ссылка на плот.
 */
anychart.chart.ChartTitle = function (mainChartView, plot) {
  goog.base(this, mainChartView, plot);
};
goog.inherits(anychart.chart.ChartTitle, anychart.visual.text.InteractiveTitle);

/**
 * Позиция тайтла чарта.
 * @private
 * @type {anychart.layout.Position}
 */
anychart.chart.ChartTitle.prototype.position_ = anychart.layout.Position.TOP;

/**
 * Флаг, расчитывать позицию относительно размеров плота
 * или относительно размеров чарта.
 * @private
 * @type {Boolean}
 */
anychart.chart.ChartTitle.prototype.alignByDataPlot_ = false;

/**
 * Зажимает размеры плота на основе размеров тайтла.
 * @param {anychart.utils.geom.Rectangle} plotBounds Баунды плота.
 */
anychart.chart.ChartTitle.prototype.cropBounds = function (plotBounds) {
  var bounds = this.getBounds();
  var padding = this.getPadding();

  var spaceX = bounds.width + padding;
  var spaceY = bounds.height + padding;

  switch (this.position_) {
    case anychart.layout.Position.LEFT:
      this.sprite_.setX(plotBounds.x);
      plotBounds.x += spaceX;
      plotBounds.width -= spaceX;
      break;
    case anychart.layout.Position.RIGHT:
      this.sprite_.setX(plotBounds.getRight() - bounds.width);
      plotBounds.width -= spaceX;
      break;
    case anychart.layout.Position.TOP:
      this.sprite_.setY(plotBounds.y);
      plotBounds.y += spaceY;
      plotBounds.height -= spaceY;
      break;
    case anychart.layout.Position.BOTTOM:
      this.sprite_.setY(plotBounds.getBottom() - bounds.height);
      plotBounds.height -= spaceY;
      break;
  }
};

/**
 *  Устанока позиции тайтла в соотвтетствии с десериализоваными настройками.
 * @param {anychart.utils.geom.Rectangle} chartBounds Баунды чарта.
 * @param {anychart.utils.geom.Rectangle} plotBounds Баунды плота.
 */
anychart.chart.ChartTitle.prototype.setPosition = function (chartBounds, plotBounds) {
  var rect = this.alignByDataPlot_ ? plotBounds : chartBounds;
  switch (this.position_) {
    case anychart.layout.Position.TOP:
    case anychart.layout.Position.BOTTOM:
      var x = 0;
      switch (this.align_) {
        case anychart.layout.AbstractAlign.NEAR:
          x = rect.x;
          break;
        case anychart.layout.AbstractAlign.CENTER:
          x = rect.x + (rect.width - this.getBounds().width) / 2;
          break;
        case anychart.layout.AbstractAlign.FAR:
          x = rect.getRight() - this.getBounds().width;
          break;
      }

      if (x < rect.x) x = rect.x;
      this.sprite_.setX(x);
      break;

    case anychart.layout.Position.LEFT:
    case anychart.layout.Position.RIGHT:
      var y = 0;
      switch (this.align_) {
        case anychart.layout.AbstractAlign.NEAR:
          y = rect.y;
          break;
        case anychart.layout.AbstractAlign.CENTER:
          y = rect.y + (rect.height - this.getBounds().height) / 2;
          break;
        case anychart.layout.AbstractAlign.FAR:
          y = rect.getBottom() - this.getBounds().height;
          break;
      }

      if (y < rect.y) y = rect.y;
      this.sprite_.setY(y);
      break;
  }
};

/**
 * @param {Object} data Title configuration.
 */
anychart.chart.ChartTitle.prototype.deserialize = function (data) {
  goog.base(this, 'deserialize', data);
  var des = anychart.utils.deserialization;
  if (des.hasProp(data, 'position')) {
    switch (des.getLString(des.getProp(data, 'position'))) {
      case 'left':
        this.position_ = anychart.layout.Position.LEFT;
        this.setRotationAngle(this.getRotationAngle() + 90);
        break;
      case 'right':
        this.position_ = anychart.layout.Position.RIGHT;
        this.setRotationAngle(this.getRotationAngle() - 90);
        break;
      case 'top':
        this.position_ = anychart.layout.Position.TOP;
        break;
      case 'bottom':
        this.position_ = anychart.layout.Position.BOTTOM;
        break;
    }
  }
  if (des.hasProp(data, 'align_by'))
    this.alignByDataPlot_ =
      des.getLStringProp(data, 'align_by') == 'dataplot';
};

/**
 * @param {anychart.svg.SVGManager} svgManager Link to svg manager.
 * TODO: param descr
 * @return {anychart.svg.SVGSprite} SVG sprite with text or null.
 */

anychart.chart.ChartTitle.prototype.createSVGText = function (svgManager, formatter) {
  this.initSize(svgManager, this.textFormatter_.getValue(formatter));
  var sprite = goog.base(this, 'createSVGText', svgManager);

  if (IS_ANYCHART_DEBUG_MOD) {
    sprite.setId('ChartTitle');
  }
  return sprite;
};
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------
/**
 * @param {String} token
 * @return {String}
 */
anychart.chart.ChartTitle.prototype.getTokenValue = function (token) {
  return this.plot_ ? this.plot_.getTokenValue(token) : '';
};
/**
 * @param {String} token
 * @return {String}
 */
anychart.chart.ChartTitle.prototype.getTokenType = function (token) {
  return this.plot_ ? this.plot_.getTokenType(token) : anychart.formatting.TextFormatTokenType.TEXT;
};
//------------------------------------------------------------------------------
//
//                           Chart class.
//
//------------------------------------------------------------------------------
/**
 * В качестве chartView может передваться MainChartView при сингл чарте или
 * DashboardChartView в случае дашборда.
 * @constructor
 * @param {anychart.chartView.MainChartView} mainChartView
 * @param {anychart.chartView.ChartView} chartView
 */
anychart.chart.Chart = function (mainChartView, chartView) {
  this.mainChartView_ = mainChartView;
  this.chartView_ = chartView;
  this.isInitialized_ = false;
};
//------------------------------------------------------------------------------
//                     Chart base/view.
//------------------------------------------------------------------------------
/**
 * Chart base.
 * @private
 * @type {anychart.chartView.IAnyChart}
 */
anychart.chart.Chart.prototype.mainChartView_ = null;
/**
 * @private
 * @type {anychart.chartView.ChartView}
 */
anychart.chart.Chart.prototype.chartView_ = null;
//------------------------------------------------------------------------------
//                          Visual properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.chart.Chart.prototype.svgManager_ = null;
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.chart.Chart.prototype.initializeSVG = function (svgManager) {
  this.svgManager_ = svgManager;
};

/**
 * Chart title object.
 * @private
 * @type {anychart.chart.ChartTitle}
 */
anychart.chart.Chart.prototype.title_ = null;

/**
 * Chart subtitle object.
 * @private
 * @type {anychart.chart.ChartTitle}
 */
anychart.chart.Chart.prototype.subtitle_ = null;

/**
 * Chart footer.
 * @private
 * @type {anychart.chart.ChartTitle}
 */
anychart.chart.Chart.prototype.footer_ = null;

/**
 * Chart background.
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.chart.Chart.prototype.background_ = null;

/**
 * Background sprite
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.chart.Chart.prototype.backgroundSprite_ = null;
/**
 * Chart plot.
 * @private
 * @type {anychart.plots.IPlot}
 */
anychart.chart.Chart.prototype.plot_ = null;
/**
 * @param {anychart.plots.IPlot} value
 */
anychart.chart.Chart.prototype.setPlot = function (value) {
  this.plot_ = value;
};
/**
 * @return {anychart.plots.IPlot}
 */
anychart.chart.Chart.prototype.getPlot = function () {
  return this.plot_;
};
/**
 * Chart container sprite.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.chart.Chart.prototype.sprite_ = null;

/**
 * Chart bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.chart.Chart.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.chart.Chart.prototype.getBounds = function () {
  return this.bounds_;
};

/**
 * Chart inside margin.
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.chart.Chart.prototype.margin_ = null;
/**
 * @param {anychart.layout.Margin} value
 */
anychart.chart.Chart.prototype.setMargin = function (value) {
  this.margin_ = value;
};
/**
 * Chart controls list.
 * @private
 * @type {anychart.controls.ControlsList}
 */
anychart.chart.Chart.prototype.controlsList_ = null;
/**
 * Chart controls list.
 * @private
 * @type {anychart.styles.StylesList}
 */
anychart.chart.Chart.prototype.stylesList_ = null;
/**
 * Clear chart styles list.
 */
anychart.chart.Chart.prototype.clearStylesList = function () {
  if (this.stylesList_) this.stylesList_.clear();
};
/**
 * Indicates, is chart.
 * @return {Boolean}
 */
anychart.chart.Chart.prototype.isChart = function () {
  return true;
};
anychart.chart.Chart.prototype.isDashboard = function () {
  return false;
};
/**
 * Chart base.
 * @private
 * @type {Boolean}
 */
anychart.chart.Chart.prototype.hasData_ = false;
/**
 * @private
 * @type {Boolean}
 */
anychart.chart.Chart.prototype.deserializeSuccess_ = false;
/**
 * @return {Boolean}
 */
anychart.chart.Chart.prototype.isDeserializeSuccess = function () {
  return this.deserializeSuccess_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.chart.Chart.prototype.isInitialized_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.chart.Chart.prototype.isInitialized = function () {
  return this.isInitialized_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * Create title and deserialize title settings.
 * <p>
 *  Note: Apply to title, subtitle, footer.
 * </p>
 * @private
 * @param {Object} config Title json configuration.
 * @param {String} titleNodeName Name of title conif object.
 * @return {anychart.chart.ChartTitle} Chart title.
 */
anychart.chart.Chart.prototype.deserializeTitle_ = function (config, titleNodeName) {
  var des = anychart.utils.deserialization;
  if (des.isEnabled(des.getProp(config, titleNodeName))) {
    var title = new anychart.chart.ChartTitle(this.mainChartView_, this.plot_);
    title.deserialize(des.getProp(config, titleNodeName));
    return title;
  }
  return null;
};

/**
 * Deserialize chart settings.
 * @param {Object} data Chart configuration.
 * Templates container.
 */
anychart.chart.Chart.prototype.deserialize = function (data) {
  //check data
  this.hasData_ = false;
  if (!data) {
    this.chartView_.showNoDataMessage();
    this.deserializeSuccess_ = false;
    return;
  }

  if (!this.plot_ || this.plot_.checkNoData(data)) {
    this.chartView_.showNoDataMessage();
    this.deserializeChartBackground_(data);
    this.deserializeSuccess_ = false;
    return;
  }
  this.hasData_ = true;
  var des = anychart.utils.deserialization;
  //styles list
  this.stylesList_ = new anychart.styles.StylesList(des.getProp(data, 'styles'));

  //controls list
  this.controlsList_ = new anychart.controls.ControlsList(
    this.mainChartView_,
    this.plot_,
    this);
  //plot
  if (this.plot_) {
    this.plot_.initializeSVG(this.svgManager_);
    this.plot_.setControlsList(this.controlsList_);
    if (this.plot_.supportsCache())
      this.plot_.setCachedData(data);
    this.plot_.deserialize(data, this.stylesList_);
  }
  //chart setting
  if (des.hasProp(data, 'chart_settings')) {
    var chartSettings = des.getProp(data, 'chart_settings');
    //background
    this.deserializeChartBackground_(data);
    //titles
    this.title_ = this.deserializeTitle_(chartSettings, 'title');
    this.subtitle_ = this.deserializeTitle_(chartSettings, 'subtitle');
    this.footer_ = this.deserializeTitle_(chartSettings, 'footer');
    //controls list settings
    if (des.hasProp(chartSettings, 'controls'))
      this.controlsList_.deserialize(des.getProp(chartSettings, 'controls'));
    //legend
    if (des.isEnabledProp(chartSettings, 'legend'))
      this.controlsList_.deserializeControl(
        des.getProp(chartSettings, 'legend'),
        this.stylesList_,
        'legend');
    //other controls
    if (des.hasProp(chartSettings, 'controls')) {
      var controls = des.getProp(chartSettings, 'controls');
      for (var i in controls) {
        var controlData = controls[i];
        if (controlData instanceof Array) {
          var controlsCount = controlData.length;
          for (var j = 0; j < controlsCount; j++)
            this.deserializeControl(controlData[j], i);
        } else if (controlData instanceof Object)
          this.deserializeControl(controlData, i);
      }
    }
  }
  this.deserializeSuccess_ = true;
};
/**
 * @protected
 * @param {Object} controlData
 */
anychart.chart.Chart.prototype.deserializeControl = function (controlData, controlName) {
  if (anychart.utils.deserialization.isEnabled(controlData))
    this.controlsList_.deserializeControl(controlData, this.stylesList_, controlName);
};
/**
 * @private
 * @param {Object} data
 */
anychart.chart.Chart.prototype.deserializeChartBackground_ = function (data) {
  var des = anychart.utils.deserialization;
  if (!des.hasProp(data, 'chart_settings')) return;
  var chartSetting = des.getProp(data, 'chart_settings');
  if (des.isEnabled(des.getProp(chartSetting, 'chart_background'))) {
    this.background_ = new anychart.visual.background.RectBackground();
    this.background_.deserialize(des.getProp(chartSetting, 'chart_background'));
  }
};
//------------------------------------------------------------------------------
//                          VISUAL
//------------------------------------------------------------------------------

/**
 * @param {anychart.svg.SVGSprite} container chart container.
 * @param {anychart.utils.geom.Rectangle} chartBounds chart bounds.
 */
anychart.chart.Chart.prototype.initVisual = function (container, chartBounds) {
  this.isInitialized_ = true;

  this.sprite_ = container;

  if (this.margin_) this.margin_.applyInside(chartBounds);

  var plotBounds = chartBounds.clone(), //Полные баунды плота (данные, оси и все что есть на плоте).
    chartBoundsWithoutTitles, //Баунды чарта без учета title, subtitle, footer
    chartBoundsWithoutTitlesAndMargins = plotBounds.clone(); //Баунды чарта без учета title, subtitle, footer и background margins

  if (this.background_) {
    this.backgroundSprite_ = this.background_.initialize(this.svgManager_);
    if (IS_ANYCHART_DEBUG_MOD) this.backgroundSprite_.setId('ChartBackground');
    this.sprite_.appendSprite(this.backgroundSprite_);
    this.backgroundBounds_ = plotBounds.clone();
    this.background_.applyInsideMargin(plotBounds);
  }

  this.bounds_ = plotBounds.clone();
  if (!this.hasData_)  return this.sprite_;

  this.createTitleVisual_(this.title_, this.svgManager_);
  this.createTitleVisual_(this.subtitle_, this.svgManager_);
  this.createTitleVisual_(this.footer_, this.svgManager_);

  plotBounds = this.applySpaceForTitles_(plotBounds);
  chartBoundsWithoutTitles = plotBounds.clone();
  this.plot_.setBounds(plotBounds);

  this.controlsList_.initialize(this.svgManager_, this.sprite_);

  if (this.plot_) {
    this.controlsList_.setLayout(plotBounds, chartBoundsWithoutTitles);
    this.plot_.initialize(plotBounds, this.chartView_.getTooltipsSprite(), true);
    this.cropPlot_(chartBoundsWithoutTitles, chartBoundsWithoutTitlesAndMargins);
    this.sprite_.appendSprite(this.plot_.getSprite());
  }

  this.cropTitles(chartBoundsWithoutTitles, this.plot_.getInnerBounds());
  this.sprite_.appendSprite(this.controlsList_.getSprite());

  return this.sprite_;
};
anychart.chart.Chart.prototype.cropPlot_ = function (chartBoundsWithoutTitles, chartBoundsWithoutTitlesAndMargins) {
  var finalized = this.controlsList_.finalizeLayout(this.plot_.getInnerBounds(), chartBoundsWithoutTitles, chartBoundsWithoutTitlesAndMargins);
  var count = 0;
  while (!finalized) {
    count++;
    if (count == 10) return;

    var innerBounds = chartBoundsWithoutTitles.clone();
    this.controlsList_.cropPlot(innerBounds);
    this.plot_.calculateResize(innerBounds);
    finalized = this.controlsList_.finalizeLayout(this.plot_.getInnerBounds(), chartBoundsWithoutTitles, chartBoundsWithoutTitlesAndMargins);
  }
};
/**
 * Crop bounds for titles.
 * @protected
 * @param {anychart.utils.geom.Rectangle} chartBounds
 * @param {anychart.utils.geom.Rectangle} plotBounds
 */
anychart.chart.Chart.prototype.cropTitles = function (chartBounds, plotBounds) {
  if (this.footer_) this.footer_.setPosition(chartBounds, plotBounds);
  if (this.title_) this.title_.setPosition(chartBounds, plotBounds);
  if (this.subtitle_) this.subtitle_.setPosition(chartBounds, plotBounds);
};

/**
 * Draw chart.
 */
anychart.chart.Chart.prototype.draw = function () {
  //chart background
  if (this.backgroundSprite_)
    this.background_.draw(this.backgroundSprite_, this.backgroundBounds_);
  //chart plot
  if (this.plot_ && this.hasData_) {
    this.plot_.draw();
    this.controlsList_.draw();
    this.initializeWatermark_();
  } else if (!this.hasData_) {
    this.chartView_.showNoDataMessage();
  }
};
/**
 * Create title visualization.
 * @private
 * @param {anychart.chart.ChartTitle} title Title.
 * @param {anychart.svg.SVGManager} svgManager svg manager.
 */
anychart.chart.Chart.prototype.createTitleVisual_ = function (title, svgManager) {
  if (title) {
    var el = title.createSVGText(svgManager, this.plot_);
    this.sprite_.appendSprite(el);
  }
};
/**
 * @private
 * @param {anychart.utils.geom.Rectangle} plotBounds Chart bounds.
 * @return {anychart.utils.geom.Rectangle} cropped bounds.
 */
anychart.chart.Chart.prototype.applySpaceForTitles_ = function (plotBounds) {
  if (this.footer_) this.footer_.cropBounds(plotBounds);
  if (this.title_) this.title_.cropBounds(plotBounds);
  if (this.subtitle_) this.subtitle_.cropBounds(plotBounds);
  return plotBounds;
};

/**
 * @param {anychart.utils.geom.Rectangle} bounds new chart bounds.
 */
anychart.chart.Chart.prototype.resize = function (bounds) {
  var chartBounds = bounds.clone(),
    plotBounds,
    chartBoundsWithoutTitles,
    chartBoundsWithoutTitlesAndMargins;

  if (this.margin_) this.margin_.applyInside(chartBounds);
  if (this.background_) this.background_.resize(this.backgroundSprite_, chartBounds);

  if (!this.hasData_) return;

  if (this.plot_) {
    chartBoundsWithoutTitlesAndMargins = chartBounds.clone();
    if (this.background_) this.background_.applyInsideMargin(chartBounds);
    plotBounds = chartBounds.clone();
    plotBounds = this.applySpaceForTitles_(plotBounds);
    chartBoundsWithoutTitles = plotBounds.clone();
    this.plot_.setBounds(plotBounds);
    this.controlsList_.setLayout(plotBounds, chartBoundsWithoutTitles);
    this.plot_.calculateResize(plotBounds);
    this.cropPlot_(chartBoundsWithoutTitles, chartBoundsWithoutTitlesAndMargins);
    this.plot_.execResize(plotBounds);
    this.controlsList_.resize();
    this.cropTitles(chartBounds, this.plot_.getInnerBounds());
  }

  this.bounds_ = chartBounds.clone();
  this.resizeWatermark_();
};
anychart.chart.Chart.prototype.setNoData = function () {
  this.hasData_ = false;
};
//------------------------------------------------------------------------------
//                      Post initialize.
//------------------------------------------------------------------------------
anychart.chart.Chart.prototype.postInitialize = function () {
  if (this.hasData_ && this.plot_)
    this.plot_.postInitialize();
};
//------------------------------------------------------------------------------
//                               Trial.
//------------------------------------------------------------------------------
/**
 * @define {boolean}
 */
var IS_ANYCHART_TRIAL_VERSION = true;
/**
 * @define {string}
 */
var WATERMARK_TEXT = '65,110,121,67,104,97,114,116,32,84,114,105,97,108,32,86,101,114,115,105,111,110';
/**
 * @private
 * @param {Array.<Number>} array
 */
anychart.chart.Chart.prototype.decodeTrialMessage_ = function (array) {
  var res = '';
  var charCount = array.length;
  for (var i = 0; i < charCount; i++)
    res += String.fromCharCode(Number(array[i]));
  return res;
};
//------------------------------------------------------------------------------
//                               Watermark.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.chart.Chart.prototype.watermarkSprite_ = null;
/**
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.chart.Chart.prototype.watermarkTextElement_ = null;
/**
 * @private
 */
anychart.chart.Chart.prototype.initializeWatermark_ = function () {
  if (!IS_ANYCHART_TRIAL_VERSION && !this.mainChartView_.getWatermarkText()) return;
  this.watermarkSprite_ = new anychart.svg.SVGSprite(this.sprite_.getSVGManager());
  this.watermarkTextElement_ = new anychart.visual.text.TextElement();
  this.sprite_.appendSprite(this.watermarkSprite_);
  this.addWatermark_();

  if (IS_ANYCHART_DEBUG_MOD)
    this.watermarkSprite_.setId('Watermark');
};
/**
 * @private
 */
anychart.chart.Chart.prototype.addWatermark_ = function () {
  this.watermarkTextElement_.deserialize(
    {
      'font':{
        'family':'Verdana',
        'size':this.getTextSizeByBounds(this.bounds_)
      }
    });

  this.watermarkTextElement_.initSize(
    this.sprite_.getSVGManager(),
    this.getWatermarkText_());

  var sprite = this.watermarkTextElement_.createSVGText(
    this.sprite_.getSVGManager(),
    anychart.utils.deserialization.getColor('red'));

  this.setWatermarkPosition(this.watermarkTextElement_.getBounds(), sprite);
  sprite.getElement().setAttribute('opacity', 0.15);
  this.watermarkSprite_.appendSprite(sprite);
};
/**
 * @private
 * @return {String}
 */
anychart.chart.Chart.prototype.getWatermarkText_ = function () {
  if (IS_ANYCHART_TRIAL_VERSION) return this.decodeTrialMessage_(WATERMARK_TEXT.split(','));
  return this.mainChartView_.getWatermarkText();
};
/**
 * @private
 */
anychart.chart.Chart.prototype.resizeWatermark_ = function () {
  if (!IS_ANYCHART_TRIAL_VERSION && !this.mainChartView_.getWatermarkText()) return;
  this.watermarkSprite_.clear();
  this.addWatermark_();
};
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {Number}
 */
anychart.chart.Chart.prototype.getTextSizeByBounds = function (bounds) {
  return bounds.width / 12.4;
};
/**
 * @param {anychart.utils.geom.Rectangle} textBounds
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.chart.Chart.prototype.setWatermarkPosition = function (textBounds, sprite) {
  var posX = (this.bounds_.width / 2) - (textBounds.width / 2) + this.bounds_.x;
  var posY = (this.bounds_.height / 2) - (textBounds.height / 2) + this.bounds_.y;
  sprite.setPosition(posX, posY);
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
anychart.chart.Chart.prototype.serialize = function (opt_target) {
  if (opt_target == null || opt_target == undefined) opt_target = {};

  if (this.title_)
    opt_target['Title'] = this.title_.getTextFormatter().getValue(this.plot_);
  if (this.subtitle_)
    opt_target['SubTitle'] = this.subtitle_.getTextFormatter().getValue(this.plot_);
  if (this.footer_)
    opt_target['Footer'] = this.footer_.getTextFormatter().getValue(this.plot_);

  if (this.hasData_ && this.plot_) this.plot_.serialize(opt_target);

  return opt_target;
};


