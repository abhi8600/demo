goog.require('anychart.utils');

$(document).ready(function() {

    module('utils/stringUtils');
    test('stringUtils exists', function() {
        ok(anychart.utils.stringUtils != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('utils/deserialization');
    test('deserialization exists', function() {
        ok(anychart.utils.deserialization != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('utils/disposeUtils');
    test('disposeUtils exists', function() {
        ok(anychart.utils.disposeUtils != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('utils/JSONUtils');
    test('JSONUtils exists', function() {
        ok(anychart.utils.JSONUtils != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('utils/serialization');
    test('serialization exists', function() {
        ok(anychart.utils.serialization != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });
});