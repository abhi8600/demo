goog.provide('anychart.templates.DEFAULT_TEMPLATE');

/**
 * Contains base super-default template for everything. 
 *
 * @public
 * @const
 * @type {Object.<*>} - JSON
 */
anychart.templates.DEFAULT_TEMPLATE = {
	"chart": {
		"chart_settings": {
			"chart_background": {
				"border": {
					"thickness": "2",
					"color": "#2466B1"
				},
				"fill": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "#FFFFFF"
							},
							{
								"position": "0.5",
								"color": "#F3F3F3"
							},
							{
								"position": "1",
								"color": "#FFFFFF"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"inside_margin": {
					"all": "15"
				},
				"corners": {
					"type": "Rounded",
					"all": "10"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "True",
						"distance": "2",
						"opacity": "0.3"
					},
					"enabled": "False"
				}
			},
			"title": {
				"text": {
					"value": "Chart Title"
				},
				"background": {
					"border": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#DDDDDD"
								},
								{
									"position": "1",
									"color": "#D0D0D0"
								}
							],
							"angle": "90"
						},
						"enabled": "True",
						"type": "Gradient"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#FFFFFF"
								},
								{
									"position": "0.5",
									"color": "#F3F3F3"
								},
								{
									"position": "1",
									"color": "#FFFFFF"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"inside_margin": {
						"all": "5"
					},
					"effects": {
						"drop_shadow": {
							"enabled": "True",
							"distance": "1",
							"opacity": "0.1"
						},
						"enabled": "True"
					},
					"enabled": "false"
				},
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "#232323",
					"bold": "True"
				},
				"align_by": "DataPlot"
			},
			"subtitle": {
				"text": {
					"value": "Chart Sub-Title"
				},
				"background": {
					"border": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#DDDDDD"
								},
								{
									"position": "1",
									"color": "#D0D0D0"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#FFFFFF"
								},
								{
									"position": "0.5",
									"color": "#F3F3F3"
								},
								{
									"position": "1",
									"color": "#FFFFFF"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"inside_margin": {
						"all": "10"
					},
					"effects": {
						"drop_shadow": {
							"enabled": "True",
							"opacity": "0.1",
							"angle": "45",
							"blur_x": "1",
							"blur_y": "1",
							"color": "Black",
							"distance": "1",
							"strength": "1"
						},
						"enabled": "True"
					},
					"enabled": "True"
				},
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "#232323",
					"bold": "True"
				},
				"enabled": "False",
				"align_by": "dataplot"
			},
			"footer": {
				"text": {
					"value": "© 2008 AnyChart.Com Flash Charting Solutions"
				},
				"background": {
					"border": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#DDDDDD"
								},
								{
									"position": "1",
									"color": "#D0D0D0"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#FFFFFF"
								},
								{
									"position": "0.5",
									"color": "#F3F3F3"
								},
								{
									"position": "1",
									"color": "#FFFFFF"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"inside_margin": {
						"all": "10"
					},
					"effects": {
						"drop_shadow": {
							"enabled": "True",
							"opacity": "0.1",
							"angle": "45",
							"blur_x": "1",
							"blur_y": "1",
							"color": "#000000",
							"distance": "1",
							"strength": "1"
						},
						"enabled": "True"
					},
					"enabled": "True"
				},
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "#232323"
				},
				"enabled": "false",
				"position": "bottom",
				"align": "far"
			}
		}
	},
	"defaults": {
		"legend": {
			"font": {
				"family": "Verdana",
				"size": "10",
				"bold": "false",
				"color": "rgb(35,35,35)"
			},
			"format": {
				"value": "{%Icon} {%Name}"
			},
			"title": {
				"text": {
					"value": "Legend Title"
				},
				"font": {
					"family": "Verdana",
					"size": "10",
					"bold": "true",
					"color": "rgb(35,35,35)"
				},
				"background": {
					"inside_margin": {
						"all": "6"
					},
					"enabled": "false"
				},
				"padding": "2"
			},
			"title_separator": {
				"gradient": {
					"key": [
						{
							"position": "0",
							"color": "#333333",
							"opacity": "0"
						},
						{
							"position": "0.5",
							"color": "#333333",
							"opacity": "1"
						},
						{
							"position": "1",
							"color": "#333333",
							"opacity": "0"
						}
					]
				},
				"enabled": "true",
				"type": "Gradient",
				"padding": "7"
			},
			"background": {
				"border": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "rgb(221,221,221)"
							},
							{
								"position": "1",
								"color": "rgb(208,208,208)"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"fill": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "rgb(255,255,255)"
							},
							{
								"position": "0.5",
								"color": "rgb(243,243,243)"
							},
							{
								"position": "1",
								"color": "rgb(255,255,255)"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"inside_margin": {
					"all": "5"
				},
				"effects": {
					"drop_shadow": {
						"enabled": "True",
						"distance": "1",
						"opacity": "0.1"
					},
					"enabled": "True"
				},
				"enabled": "true"
			},
			"rows_separator": {
				"enabled": "true",
				"padding": "3",
				"type": "Solid",
				"color": "#333333",
				"opacity": "0.05"
			},
			"columns_separator": {
				"enabled": "true",
				"padding": "3",
				"type": "Solid",
				"color": "#333333",
				"opacity": "0.05"
			},
			"scroll_bar": {
				"fill": {
					"gradient": {
						"key": [
							{
								"color": "#94999B",
								"position": "0"
							},
							{
								"color": "White",
								"position": "1"
							}
						],
						"angle": "0"
					},
					"enabled": "true",
					"type": "Gradient"
				},
				"border": {
					"enabled": "true",
					"color": "#707173",
					"opacity": "1"
				},
				"buttons": {
					"background": {
						"corners": {
							"type": "Square",
							"all": "2"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"color": "#FDFDFD"
									},
									{
										"color": "#EBEBEB"
									}
								],
								"angle": "90"
							},
							"enabled": "true",
							"type": "Gradient"
						},
						"border": {
							"gradient": {
								"key": [
									{
										"color": "#898B8D"
									},
									{
										"color": "#5D5F60"
									}
								],
								"angle": "90"
							},
							"enabled": "true",
							"type": "Gradient",
							"opacity": "1"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "true",
								"distance": "5",
								"opacity": "0.2"
							},
							"enabled": "false"
						}
					},
					"arrow": {
						"fill": {
							"color": "#111111"
						},
						"border": {
							"enabled": "false"
						}
					},
					"states": {
						"hover": {
							"background": {
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FDFDFD"
											},
											{
												"color": "#F4F4F4"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#009DFF"
											},
											{
												"color": "#0056B8"
											}
										],
										"angle": "90"
									},
									"type": "Gradient"
								}
							}
						},
						"pushed": {
							"background": {
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#DFF2FF"
											},
											{
												"color": "#99D7FF"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#009DFF"
											},
											{
												"color": "#0056B8"
											}
										],
										"angle": "90"
									},
									"type": "Gradient"
								}
							}
						}
					}
				},
				"thumb": {
					"background": {
						"corners": {
							"type": "Rounded",
							"all": "0",
							"right_top": "3",
							"right_bottom": "3"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"color": "#FFFFFF"
									},
									{
										"color": "#EDEDED"
									}
								],
								"angle": "0"
							},
							"enabled": "true",
							"type": "Gradient"
						},
						"border": {
							"gradient": {
								"key": [
									{
										"color": "#ABAEB0"
									},
									{
										"color": "#6B6E6F"
									}
								],
								"angle": "0"
							},
							"enabled": "true",
							"type": "Gradient",
							"color": "#494949",
							"opacity": "1"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "false",
								"distance": "5",
								"opacity": ".2"
							},
							"enabled": "true"
						}
					},
					"states": {
						"hover": {
							"background": {
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FFFFFF"
											},
											{
												"color": "#F7F7F7"
											}
										],
										"angle": "0"
									},
									"enabled": "true",
									"type": "gradient",
									"color": "Cyan"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#009DFF"
											},
											{
												"color": "#0064C6"
											}
										],
										"angle": "0"
									},
									"type": "Gradient"
								}
							}
						},
						"pushed": {
							"background": {
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#D8F0FF",
												"position": "0"
											},
											{
												"color": "#ACDEFF",
												"position": "1"
											}
										],
										"angle": "0"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#009DFF"
											},
											{
												"color": "#008AEC"
											}
										],
										"angle": "0"
									},
									"type": "Gradient"
								}
							}
						}
					}
				},
				"size": "16",
				"enabled": "true"
			},
			"position": "right",
			"align": "near",
			"elements_align": "Left",
			"columns_padding": "10",
			"rows_padding": "10"
		},
		"panel": {
			"margin": {
				"all": "5"
			},
			"title": {
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "#232323",
					"bold": "True"
				},
				"align": "center"
			},
			"background": {
				"border": {
					"thickness": "2",
					"color": "#2466B1"
				},
				"fill": {
					"gradient": {
						"key": [
							{
								"position": "0",
								"color": "#FFFFFF"
							},
							{
								"position": "0.5",
								"color": "#F3F3F3"
							},
							{
								"position": "1",
								"color": "#FFFFFF"
							}
						],
						"angle": "90"
					},
					"type": "Gradient"
				},
				"inside_margin": {
					"all": "5"
				},
				"corners": {
					"type": "Rounded",
					"all": "10"
				}
			}
		},
		"dashboard": {
			"margin": {
				"all": "5"
			},
			"title": {
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "#232323",
					"bold": "True"
				},
				"text": {
					"value": "Dashboard Title"
				},
				"align": "center"
			}
		}
	}
};