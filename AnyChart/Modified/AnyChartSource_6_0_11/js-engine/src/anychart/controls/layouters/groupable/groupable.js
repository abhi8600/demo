/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes.
 * <ul>
 *  <li>
 *    @class {anychart.controls.layouters.groupable.ControlsGroup};
 *  </li>
 *  <li>
 *    @class {anychart.controls.layouters.groupable.ControlsLine};
 *  </li>
 *  <li>
 *    @class {anychart.controls.layouters.groupable.HorizontalControlsLine};
 *  </li>
 *  <li>
 *  @class {anychart.controls.layouters.groupable.HorizontalInsideControlsLine};
 *  </li>
 *  <li>
 *    @class {anychart.controls.layouters.groupable.VerticalControlsLine};
 *  </li>
 *  <li>
 *    @class {anychart.controls.layouters.groupable.VerticalInsideControlsLine};
 *  </li>
 *  <li>
 *  @class {anychart.controls.layouters.groupable.GroupableControlsCollection};
 *  </li>
 *  <li>
 *  @class {anychart.controls.layouters.groupable.InsidePlotControlsCollection};
 *  </li>
 *  <li>
 * @class {anychart.controls.layouters.groupable.OutsidePlotControlsCollection};
 *  </li>
 * <ul>.
 */
goog.provide('anychart.controls.layouters.groupable');

goog.require('anychart.controls.layouters');
//------------------------------------------------------------------------------
//
//                           ControlsGroup class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.controls.layouters.groupable.ControlsGroup = function () {
    this.controls = [];
    this.alignByDataPlot_ = true;
    this.isAlignSetted_ = false;
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @type {Array.<anychart.controls.controlsBase.Control>}
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.controls = null;

/**
 * Getter for controls.
 * @return {Array.<anychart.controls.controlsBase.Control>} Controls.
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.getControls =
    function () {
        return this.controls;
    };

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.
    alignByDataPlot_ = null;

/**
 * Getter for alignByDataPlot.
 * @return {Boolean} Aligned by data plot.
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.
    isAlignByDataPlot = function () {
    return this.alignByDataPlot_;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.
    isAlignSetted_ = null;
//------------------------------------------------------------------------------
//                          Adding controls
//------------------------------------------------------------------------------
/**
 * Adds a control.
 * @param {anychart.controls.controlsBase.Control} control Control.
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.addControl =
    function (control) {
        if (!control.getLayout().isAlignByDataPlot())
            this.alignByDataPlot_ = false;
        this.controls.push(control);
    };

/**
 * Sets align.
 */
anychart.controls.layouters.groupable.ControlsGroup.prototype.setAlign =
    function () {
        if (this.isAlignSetted_ || this.alignByDataPlot_) return;
        this.isAlignSetted_ = true;
        var controlCount = this.controls.length;
        for (var i = 0; i < controlCount; i++)
            this.controls[i].getLayout().setAlignByDataPlot(false);

    };
//------------------------------------------------------------------------------
//
//                           ControlsLine class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Boolean} isOpposite Is controls line opposite.
 */
anychart.controls.layouters.groupable.ControlsLine = function (isOpposite) {
    this.isOpposite_ = isOpposite;
    this.nearControls_ = new anychart.controls.layouters.groupable.
        ControlsGroup();
    this.centerControls_ = new anychart.controls.layouters.groupable.
        ControlsGroup();
    this.farControls_ = new anychart.controls.layouters.groupable.
        ControlsGroup();
};
//------------------------------------------------------------------------------
//                              Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.layouters.groupable.ControlsGroup}
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    nearControls_ = null;

/**
 * @private
 * @type {anychart.controls.layouters.groupable.ControlsGroup}
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    centerControls_ = null;

/**
 * @private
 * @type {anychart.controls.layouters.groupable.ControlsGroup}
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    farControls_ = null;

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    isOpposite_ = null;

/**
 * Getter for isOpposite.
 * @return {Boolean} Is controls line opposite.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.isOpposite =
    function () {
        return this.isOpposite_;
    };

/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    basePositionBounds_ = null;

/**
 * Setter for base position bounds.
 * @param {anychart.utils.geom.Rectangle} value Value.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    setBasePositionBounds = function (value) {
    this.basePositionBounds_ = value;
};
//------------------------------------------------------------------------------
//                         Adding controls
//------------------------------------------------------------------------------
/**
 * Adds a control.
 * @param {anychart.controls.controlsBase.Control} control Control.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.addControl =
    function (control) {
        switch (control.getLayout().getAlign()) {
            case anychart.controls.layout.ControlAlign.NEAR:
                this.nearControls_.addControl(control);
                break;

            case anychart.controls.layout.ControlAlign.CENTER:
                this.centerControls_.addControl(control);
                break;

            case anychart.controls.layout.ControlAlign.FAR:
                this.farControls_.addControl(control);
                break;

            case anychart.controls.layout.ControlAlign.SPREAD:
                this.stretchControl(control.getLayout());
                this.centerControls_.addControl(control);
        }
    };
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * Stretching control.
 * @param {anychart.controls.layout.ControlLayout} layout Layout.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.stretchControl =
    function (layout) {
        goog.abstractMethod();
    };

/**
 * Finalizes near or far layouts.
 * @param {anychart.controls.layouters.groupable.ControlsGroup} controls
 * Controls.
 * @param {int} space Space.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    finalizeNearOrFarLayout = function (controls, space) {
    goog.abstractMethod();
};

/**
 * Finalizes center layout.
 * @param {anychart.controls.layouters.groupable.ControlsGroup} controls
 * Controls.
 * @param {int} space Space.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.
    finalizeCenterLayout = function (controls, space) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                              Layout
//------------------------------------------------------------------------------
/**
 * Sets controls layout.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @return {int} Max from spaces of layout.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.setControlsLayout =
    function (chartBounds, plotBounds) {
        this.nearControls_.setAlign();
        this.centerControls_.setAlign();
        this.farControls_.setAlign();
        var nearSpace = this.setNearLayout(chartBounds, plotBounds);
        var centerSpace = this.setCenterLayout(chartBounds, plotBounds);
        var farSpace = this.setFarLayout(chartBounds, plotBounds);

        this.finalizeNearOrFarLayout(this.nearControls_.getControls(),
            nearSpace);
        this.finalizeNearOrFarLayout(this.farControls_.getControls(),
            farSpace);
        this.finalizeCenterLayout(this.centerControls_.getControls(),
            centerSpace);

        return Math.max(nearSpace, centerSpace, farSpace);
    };

/**
 * Sets near layout.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @return {int} 0.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.setNearLayout =
    function (chartBounds, plotBounds) {
        return 0;
    };

/**
 * Sets center layout.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @return {int} 0.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.setCenterLayout =
    function (chartBounds, plotBounds) {
        return 0;
    };

/**
 * Sets far layout.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @return {int} 0.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.setFarLayout =
    function (chartBounds, plotBounds) {
        return 0;
    };

/**
 * Sets max bound.
 * @param {anychart.controls.layout.ControlLayout} layout Layout.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 */
anychart.controls.layouters.groupable.ControlsLine.prototype.setMaxBounds =
    function (layout, chartBounds, plotBounds) {
        goog.abstractMethod();
    };
//------------------------------------------------------------------------------
//
//                           HorizontalControlsLine class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Boolean} isOpposite Is controls line opposite.
 * @extends {anychart.controls.layouters.groupable.ControlsLine}
 */
anychart.controls.layouters.groupable.HorizontalControlsLine =
    function (isOpposite) {
        anychart.controls.layouters.groupable.ControlsLine.call(this,
            isOpposite);
    };
goog.inherits(anychart.controls.layouters.groupable.HorizontalControlsLine,
    anychart.controls.layouters.groupable.ControlsLine);
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.finalizeNearOrFarLayout = function (controls, space) {
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite())
            control.getBounds().y = this.basePositionBounds_.getBottom() - space;
        else
            control.getBounds().y = this.basePositionBounds_.getTop() + space - control.getBounds().height;
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.finalizeCenterLayout = function (controls, space) {
    var y = this.isOpposite() ?
        (this.basePositionBounds_.getBottom() - space) :
        (this.basePositionBounds_.getTop() + space);


    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite()) {
            control.getBounds().y = y;
            y += control.getBounds().height;
        } else {
            control.getBounds().y = y - control.getBounds().height;
            y -= control.getBounds().height;
        }
    }
};
//------------------------------------------------------------------------------
//                              Layout
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.
    stretchControl = function (layout) {
    layout.setIsWidthSetted(true);
    layout.setIsPercentWidth(true);
    layout.setWidth(1);
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.
    setNearLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetBounds = this.nearControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var x = targetBounds.x;

    var controls = this.nearControls_.getControls();
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        control.getBounds().x = x;
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);
        x += control.getBounds().width;
        space = Math.max(space, control.getBounds().height);
    }
    return space;
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.
    setCenterLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetBounds = this.centerControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var x = targetBounds.x + targetBounds.width / 2;
    var controls = this.centerControls_.getControls();

    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);

        control.getBounds().x = x - control.getBounds().width / 2;

        space += control.getBounds().height;
    }
    return space;
};
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.
    setFarLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetBounds = this.farControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var x = targetBounds.getRight();

    var controls = this.farControls_.getControls();
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);

        control.getBounds().x = x - control.getBounds().width;
        x -= control.getBounds().width;

        space = Math.max(space, control.getBounds().height);
    }
    return space;
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalControlsLine.prototype.
    setMaxBounds = function (layout, chartBounds, plotBounds) {
    layout.setMaxWidth(layout.isAlignByDataPlot() ?
        plotBounds.width : chartBounds.width);
    layout.setMaxHeight(chartBounds.height * .5);
};
//------------------------------------------------------------------------------
//
//                           HorizontalInsideControlsLine class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Boolean} isOpposite Is controls line opposite.
 * @extends {anychart.controls.layouters.groupable.HorizontalControlsLine}
 */
anychart.controls.layouters.groupable.HorizontalInsideControlsLine =
    function (isOpposite) {
        anychart.controls.layouters.groupable.HorizontalControlsLine.call(this,
            isOpposite);
    };
goog.inherits(anychart.controls.layouters.groupable.
    HorizontalInsideControlsLine,
    anychart.controls.layouters.groupable.HorizontalControlsLine);
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalInsideControlsLine.prototype.
    finalizeNearOrFarLayout = function (controls, space) {
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite())
            control.getBounds().y = this.basePositionBounds_.getBottom() -
                control.getBounds().height;
        else
            control.getBounds().y = this.basePositionBounds_.getTop();
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalInsideControlsLine.prototype.
    finalizeCenterLayout = function (controls, space) {
    var y = this.isOpposite() ? (this.basePositionBounds_.getBottom()) :
        (this.basePositionBounds_.getTop());
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite()) {
            control.getBounds().y = y - control.getBounds().height;
            y -= control.getBounds().height;
        } else {
            control.getBounds().y = y;
            y += control.getBounds().height;
        }
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.HorizontalInsideControlsLine.prototype.
    setMaxBounds = function (layout, chartBounds, plotBounds) {
    layout.setMaxWidth(layout.isAlignByDataPlot() ?
        plotBounds.width : chartBounds.width);
    layout.setMaxHeight(chartBounds.height);
};
//------------------------------------------------------------------------------
//
//                           VerticalControlsLine class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Boolean} isOpposite Is controls line opposite.
 * @extends {anychart.controls.layouters.groupable.ControlsLine}
 */
anychart.controls.layouters.groupable.VerticalControlsLine =
    function (isOpposite) {
        anychart.controls.layouters.groupable.ControlsLine.call(this,
            isOpposite);
    };
goog.inherits(anychart.controls.layouters.groupable.VerticalControlsLine,
    anychart.controls.layouters.groupable.ControlsLine);
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    finalizeNearOrFarLayout = function (controls, space) {
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite())
            control.getBounds().x = this.basePositionBounds_.getRight() -
                space;
        else
            control.getBounds().x = this.basePositionBounds_.getLeft() +
                space - control.getBounds().width;
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    finalizeCenterLayout = function (controls, space) {
    var x = this.isOpposite() ?
        (this.basePositionBounds_.getRight() - space) :
        (this.basePositionBounds_.getLeft() + space);

    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite()) {
            control.getBounds().x = x;
            x += control.getBounds().width;
        } else {
            control.getBounds().x = x - control.getBounds().width;
            x -= control.getBounds().width;
        }
    }
};
//------------------------------------------------------------------------------
//                              Layout
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    stretchControl = function (layout) {
    layout.setIsHeightSetted(true);
    layout.setIsPercentHeight(true);
    layout.setHeight(1);
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    setNearLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetBounds = this.nearControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var y = targetBounds.getTop();

    var controls = this.nearControls_.getControls();
    var i, control;

    for (i = 0; i < controls.length; i++) {
        control = controls[i];
        control.getBounds().y = y;
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);
        y += control.getBounds().height;
        space = Math.max(space, control.getBounds().width);
    }

    return space;
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    setCenterLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetBounds = this.centerControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var x = this.isOpposite() ? this.basePositionBounds_.getRight() :
        this.basePositionBounds_.getLeft();
    var y = targetBounds.y + targetBounds.height / 2;
    var controls = this.centerControls_.getControls();

    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);

        control.getBounds().y = y - control.getBounds().height / 2;
        space += control.getBounds().width;
    }
    return space;
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    setFarLayout = function (chartBounds, plotBounds) {
    var space = 0;
    var targetRect = this.farControls_.isAlignByDataPlot() ?
        plotBounds : chartBounds;
    var y = targetRect.getBottom();

    var controls = this.farControls_.getControls();
    var i, control;
    for (i = 0; i < controls.length; i++) {
        control = controls[i];
        this.setMaxBounds(control.getLayout(), chartBounds, plotBounds);
        control.setBounds(chartBounds, plotBounds);

        control.getBounds().y = y - control.getBounds().height;
        y -= control.getBounds().height;

        space = Math.max(space, control.getBounds().width);
    }
    return space;
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalControlsLine.prototype.
    setMaxBounds = function (layout, chartBounds, plotBounds) {
    layout.setMaxHeight(layout.isAlignByDataPlot() ?
        plotBounds.height : chartBounds.height);
    layout.setMaxWidth(chartBounds.width * .5);
};
//------------------------------------------------------------------------------
//
//                           VerticalInsideControlsLine class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Boolean} isOpposite Is controls line opposite.
 * @extends {anychart.controls.layouters.groupable.VerticalControlsLine}
 */
anychart.controls.layouters.groupable.VerticalInsideControlsLine =
    function (isOpposite) {
        anychart.controls.layouters.groupable.VerticalControlsLine.call(this,
            isOpposite);
    };
goog.inherits(anychart.controls.layouters.groupable.VerticalInsideControlsLine,
    anychart.controls.layouters.groupable.VerticalControlsLine);
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalInsideControlsLine.prototype.
    finalizeNearOrFarLayout = function (controls, space) {
    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite())
            control.getBounds().x = this.basePositionBounds_.getRight() -
                control.getBounds().width;
        else
            control.getBounds().x = this.basePositionBounds_.getLeft();
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalInsideControlsLine.prototype.
    finalizeCenterLayout = function (controls, space) {
    var x = this.isOpposite ? (this.basePositionBounds_.getRight()) :
        (this.basePositionBounds_.getLeft());

    for (var i = 0; i < controls.length; i++) {
        var control = controls[i];
        if (this.isOpposite()) {
            control.getBounds().x = x - control.getBounds().width;
            x -= control.getBounds().width;
        } else {
            control.getBounds().x = x;
            x += control.getBounds().width;
        }
    }
};

/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.VerticalInsideControlsLine.prototype.
    setMaxBounds = function (layout, chartBounds, plotBounds) {
    layout.maxHeight = layout.isAlignByDataPlot() ?
        plotBounds.height : chartBounds.height;
    layout.maxWidth = chartBounds.width;
};
//------------------------------------------------------------------------------
//
//                           GroupableControlsCollection class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.layouters.ControlsCollection}
 */
anychart.controls.layouters.groupable.GroupableControlsCollection = function () {
    anychart.controls.layouters.ControlsCollection.apply(this);
};
goog.inherits(anychart.controls.layouters.groupable.GroupableControlsCollection,
    anychart.controls.layouters.ControlsCollection);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.layouters.groupable.VerticalControlsLine}
 */
anychart.controls.layouters.groupable.GroupableControlsCollection.prototype.
    leftControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.groupable.VerticalControlsLine}
 */
anychart.controls.layouters.groupable.GroupableControlsCollection.prototype.
    rightControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.groupable.HorizontalControlsLine}
 */
anychart.controls.layouters.groupable.GroupableControlsCollection.prototype.
    topControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.groupable.HorizontalControlsLine}
 */
anychart.controls.layouters.groupable.GroupableControlsCollection.prototype.
    bottomControls_ = null;
//------------------------------------------------------------------------------
//                          Adding controls
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.GroupableControlsCollection.prototype.
    addControl = function (control) {
    switch (control.getLayout().getPosition()) {
        case anychart.controls.layout.ControlPosition.LEFT:
            this.leftControls_.addControl(control);
            break;
        case anychart.controls.layout.ControlPosition.RIGHT:
            this.rightControls_.addControl(control);

            break;
        case anychart.controls.layout.ControlPosition.TOP:
            this.topControls_.addControl(control);
            break;
        case anychart.controls.layout.ControlPosition.BOTTOM:
            this.bottomControls_.addControl(control);
            break;
    }
};
//------------------------------------------------------------------------------
//
//                           InsidePlotControlsCollection class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.layouters.groupable.GroupableControlsCollection}
 */
anychart.controls.layouters.groupable.InsidePlotControlsCollection =
    function () {
        anychart.controls.layouters.groupable.
            GroupableControlsCollection.apply(this);
        this.leftControls_ = new anychart.controls.layouters.groupable.
            VerticalInsideControlsLine(false);
        this.rightControls_ = new anychart.controls.layouters.groupable.
            VerticalInsideControlsLine(true);
        this.topControls_ = new anychart.controls.layouters.groupable.
            HorizontalInsideControlsLine(false);
        this.bottomControls_ = new anychart.controls.layouters.groupable.
            HorizontalInsideControlsLine(true);
    };
goog.inherits(anychart.controls.layouters.groupable.
    InsidePlotControlsCollection,
    anychart.controls.layouters.groupable.GroupableControlsCollection);
//------------------------------------------------------------------------------
//                              Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.InsidePlotControlsCollection.prototype.
    finalizeLayout = function (plotBounds, chartBounds) {
    this.leftControls_.setBasePositionBounds(plotBounds);
    this.rightControls_.setBasePositionBounds(plotBounds);
    this.topControls_.setBasePositionBounds(plotBounds);
    this.bottomControls_.setBasePositionBounds(plotBounds);

    this.leftControls_.setControlsLayout(chartBounds, plotBounds);
    this.rightControls_.setControlsLayout(chartBounds, plotBounds);
    this.topControls_.setControlsLayout(chartBounds, plotBounds);
    this.bottomControls_.setControlsLayout(chartBounds, plotBounds);
    return true;
};
//------------------------------------------------------------------------------
//
//                           OutsidePlotControlsCollection class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.layouters.groupable.GroupableControlsCollection}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection =
    function () {
        anychart.controls.layouters.groupable.
            GroupableControlsCollection.apply(this);
        this.defaultAlignLeftByDataPlot_ = true;
        this.defaultAlignRightByDataPlot_ = true;
        this.defaultAlignTopByDataPlot_ = true;
        this.defaultAlignBottomByDataPlot_ = true;

        this.leftControls_ = new anychart.controls.layouters.groupable.
            VerticalControlsLine(false);
        this.rightControls_ = new anychart.controls.layouters.groupable.
            VerticalControlsLine(true);
        this.topControls_ = new anychart.controls.layouters.groupable.
            HorizontalControlsLine(false);
        this.bottomControls_ = new anychart.controls.layouters.groupable.
            HorizontalControlsLine(true);
    };
goog.inherits(anychart.controls.layouters.groupable.
    OutsidePlotControlsCollection,
    anychart.controls.layouters.groupable.GroupableControlsCollection);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    defaultAlignLeftByDataPlot_ = null;

/**
 * Setter for defaultAlignLeftByDataPlot.
 * @param {Boolean} value Value.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    setDefaultAlignLeftByDataPlot = function (value) {
    this.defaultAlignLeftByDataPlot_ = value;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    defaultAlignRightByDataPlot_ = null;

/**
 * Setter for defaultAlignRightByDataPlot.
 * @param {Boolean} value Value.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    setDefaultAlignRightByDataPlot = function (value) {
    this.defaultAlignRightByDataPlot_ = value;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    defaultAlignTopByDataPlot_ = null;

/**
 * Setter for defaultAlignTopByDataPlot.
 * @param {Boolean} value Value.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    setDefaultAlignTopByDataPlot = function (value) {
    this.defaultAlignTopByDataPlot_ = value;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    defaultAlignBottomByDataPlot_ = null;

/**
 * Setter for defaultAlignBottomByDataPlot.
 * @param {Boolean} value Value.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    setDefaultAlignBottomByDataPlot = function (value) {
    this.defaultAlignBottomByDataPlot_ = value;
};

/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    margin_ = null;
//------------------------------------------------------------------------------
//                          Adding controls
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    addControl = function (control) {
    if (!control.getLayout().isAlignBySet()) {
        switch (control.getLayout().getPosition()) {
            case anychart.controls.layout.ControlPosition.LEFT:
                control.getLayout().setAlignByDataPlot(
                    this.defaultAlignLeftByDataPlot_);
                break;
            case anychart.controls.layout.ControlPosition.RIGHT:
                control.getLayout().setAlignByDataPlot(
                    this.defaultAlignRightByDataPlot_);
                break;
            case anychart.controls.layout.ControlPosition.TOP:
                control.getLayout().setAlignByDataPlot(
                    this.defaultAlignTopByDataPlot_);
                break;
            case anychart.controls.layout.ControlPosition.BOTTOM:
                control.getLayout().setAlignByDataPlot(
                    this.defaultAlignBottomByDataPlot_);
                break;
        }
    }
    goog.base(this, 'addControl', control);
};
//------------------------------------------------------------------------------
//                          Finalize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.
    finalizeLayout = function (plotBounds, chartBounds) {
    var newMargin = new anychart.layout.Margin();
    this.setMargin(newMargin, plotBounds, chartBounds);
    var isValid = (newMargin.getLeft() == this.margin_.getLeft()) &&
        (newMargin.getRight() == this.margin_.getRight()) &&
        (newMargin.getTop() == this.margin_.getTop()) &&
        (newMargin.getBottom() == this.margin_.getBottom());
    this.margin_ = newMargin;
    return isValid;
};
//------------------------------------------------------------------------------
//                          Position
//------------------------------------------------------------------------------
/**
 * Sets layout.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.setLayout = function (plotBounds, chartBounds) {
    this.margin_ = new anychart.layout.Margin();
    this.setMargin(this.margin_, plotBounds, chartBounds);
    this.cropPlot(plotBounds);
};

/**
 * Crops plot.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.cropPlot = function (plotBounds) {
    this.margin_.applyInside(plotBounds)
};

/**
 * Sets margin.
 * @param {anychart.layout.Margin} margin Margin.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 */
anychart.controls.layouters.groupable.OutsidePlotControlsCollection.prototype.setMargin = function (margin, plotBounds, chartBounds) {
    this.leftControls_.setBasePositionBounds(chartBounds);
    this.rightControls_.setBasePositionBounds(chartBounds);
    this.topControls_.setBasePositionBounds(chartBounds);
    this.bottomControls_.setBasePositionBounds(chartBounds);

    margin.setTop(this.topControls_.setControlsLayout(chartBounds, plotBounds));
    margin.setBottom(this.bottomControls_.setControlsLayout(chartBounds, plotBounds));
    margin.setLeft(this.leftControls_.setControlsLayout(chartBounds, plotBounds));
    margin.setRight(this.rightControls_.setControlsLayout(chartBounds, plotBounds));
};
