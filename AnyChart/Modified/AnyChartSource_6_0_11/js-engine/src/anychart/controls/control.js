/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.ControlsFactory};</li>
 *  <li>@class {anychart.controls.ControlsList};</li>
 * <ul>
 */

goog.provide('anychart.controls');
goog.require('anychart.controls.legend');
goog.require('anychart.controls.colorSwatch');
goog.require('anychart.controls.label');
//------------------------------------------------------------------------------
//
//                           ControlsFactory class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.ControlsFactory = function() {
};
/**
 * @return {anychart.controls.controlsBase.Control}
 */
anychart.controls.ControlsFactory.prototype.create = function(data, controlName) {
    if (!controlName) return null;
    switch (controlName) {
        case 'legend':
            return this.createLegend_(data);
        case 'color_swatch':
            return this.createColorSwatch_(data);
        case 'image':
            return null;
        case 'label':
            return new anychart.controls.label.LabelControl();
    }
    return new anychart.controls.legend.ColumnBasedLegend();
};
/**
 * @private
 * @param {Object} data
 * @return {anychart.controls.controlsBase.Control}
 */
anychart.controls.ControlsFactory.prototype.createLegend_ = function(data) {
    var des = anychart.utils.deserialization;

    //create legend by layout
    if (des.hasProp(data, 'elements_layout') &&
            des.getLStringProp(data, 'elements_layout') != 'horizontal')
        return new anychart.controls.legend.ColumnBasedLegend();

    //create legend by position
    if (des.hasProp(data, 'position')) {
        var position = des.getLStringProp(data, 'position');
        if (position == 'float' ||
                position == 'fixed' ||
                position == 'left' ||
                position == 'right')
            return new anychart.controls.legend.ColumnBasedLegend();
    }

    return new anychart.controls.legend.RowBasedLegend();
};
/**
 * @private
 * @param {Object} data
 * @return {anychart.controls.controlsBase.Control}
 */
anychart.controls.ControlsFactory.prototype.createColorSwatch_ = function(data) {
    var des = anychart.utils.deserialization;
    switch (des.getLStringProp(data, 'orientation')) {
        default :
        case 'vertical' :
            return new anychart.controls.colorSwatch.VerticalColorSwatch();
        case 'horizontal' :
            return new anychart.controls.colorSwatch.HorizontalColorSwatch();
    }

};
//------------------------------------------------------------------------------
//
//                           ControlsList class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.ControlsList = function(mainChartView, plot, plotContainer) {
    this.controls_ = [];
    this.mainChartView_ = mainChartView;
    this.plot_ = plot;
    this.plotContainer_ = plotContainer;
    this.controlsFactory_ = new anychart.controls.ControlsFactory();

    this.floatControls_ = new anychart.controls.layouters.anchored.FloatControlsCollection();
    this.fixedControls_ = new anychart.controls.layouters.anchored.FixedControlsCollection();
    this.insideControls_ = new anychart.controls.layouters.groupable.InsidePlotControlsCollection();
    this.outsideControls_ = new anychart.controls.layouters.groupable.OutsidePlotControlsCollection();
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.ControlsList.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.controls.ControlsList.prototype.getSprite = function() {
    return this.sprite_;
};
/**
 * @private
 * @type {Array.<anychart.controls.controlsBase.Control>}
 */
anychart.controls.ControlsList.prototype.controls_ = null;
/**
 * @private
 * @type {anychart.plots.IPlot}
 */
anychart.controls.ControlsList.prototype.plot_ = null;
/**
 * @private
 * @type {anychart}
 */
anychart.controls.ControlsList.prototype.mainChartView_ = null;
/**
 * @private
 * @type {anychart.chart.Chart}
 */
anychart.controls.ControlsList.prototype.plotContainer_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.ControlsList.prototype.needReorderControls_ = null;
/**
 * @private
 * @type {anychart.controls.ControlsFactory}
 */
anychart.controls.ControlsList.prototype.controlsFactory_ = null;
//------------------------------------------------------------------------------
//                          Controls containers
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.layouters.groupable.InsidePlotControlsCollection}
 */
anychart.controls.ControlsList.prototype.insideControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.groupable.OutsidePlotControlsCollection}
 */
anychart.controls.ControlsList.prototype.outsideControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.anchored.FloatControlsCollection}
 */
anychart.controls.ControlsList.prototype.floatControls_ = null;
/**
 * @private
 * @type {anychart.controls.layouters.anchored.FixedControlsCollection}
 */
anychart.controls.ControlsList.prototype.fixedControls_ = null;
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.controls.ControlsList.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'align_left_by'))
        this.outsideControls_.setDefaultAlignLeftByDataPlot(
                !(des.getEnumProp(data, 'align_left_by') == 'chart'));

    if (des.hasProp(data, 'align_right_by'))
        this.outsideControls_.setDefaultAlignRightByDataPlot(
                !(des.getEnumProp(data, 'align_right_by') == 'chart'));

    if (des.hasProp(data, 'align_top_by'))
        this.outsideControls_.setDefaultAlignTopByDataPlot(
                !(des.getEnumProp(data, 'align_top_by') == 'chart'));

    if (des.hasProp(data, 'align_bottom_by'))
        this.outsideControls_.setDefaultAlignBottomByDataPlot(
                !(des.getEnumProp(data, 'align_bottom_by') == 'chart'));
};

anychart.controls.ControlsList.prototype.deserializeControl = function(data, stylesList, controlName) {
    var control = this.controlsFactory_.create(data, controlName);
    if (control == null) return;
    control.initializeDataContainer(this.mainChartView_, this.plot_, this.plotContainer_);
    control.deserialize(data, stylesList);

    //register control
    if (control.getLayout().getPosition() == anychart.controls.layout.ControlPosition.FLOAT)
        this.floatControls_.addControl(control);
    else if (control.getLayout().getPosition() == anychart.controls.layout.ControlPosition.FIXED)
        this.fixedControls_.addControl(control);
    else if (control.getLayout().isInsideDataPlot())
        this.insideControls_.addControl(control);
    else
        this.outsideControls_.addControl(control);

    this.controls_.push(control);
};
//------------------------------------------------------------------------------
//                          Initialization
//------------------------------------------------------------------------------
anychart.controls.ControlsList.prototype.initialize = function(svgManager) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    if(IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('ChartControls');

    var controlsCount = this.controls_.length;
    for (var i = 0; i < controlsCount; i++) {
        this.sprite_.appendSprite(this.controls_[i].initialize(svgManager));
    }
};

anychart.controls.ControlsList.prototype.draw = function() {
    var count = this.controls_.length;
    for (var i = 0; i < count; i++) {
        this.controls_[i].draw();
    }
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Resize all controls
 */
anychart.controls.ControlsList.prototype.resize = function() {
    var count = this.controls_.length;
    for (var i = 0; i < count; i++) {
        this.controls_[i].resize();
    }
};
//------------------------------------------------------------------------------
//                          Layout
//------------------------------------------------------------------------------
anychart.controls.ControlsList.prototype.setLayout = function(plotBounds, chartBounds) {
    this.outsideControls_.setLayout(plotBounds, chartBounds);
};

anychart.controls.ControlsList.prototype.cropPlot = function(plotBounds) {
    this.outsideControls_.cropPlot(plotBounds);
};
/**
 * @inheritDoc
 */
anychart.controls.ControlsList.prototype.finalizeLayout = function(plotBounds, chartBounds, totalChartBounds) {
    if (this.outsideControls_.finalizeLayout(plotBounds, chartBounds)) {
        this.insideControls_.finalizeLayout(plotBounds, chartBounds);
        this.floatControls_.finalizeLayout(plotBounds, totalChartBounds);
        this.fixedControls_.finalizeLayout(plotBounds, totalChartBounds);
        return true;
    }
    return false;
};