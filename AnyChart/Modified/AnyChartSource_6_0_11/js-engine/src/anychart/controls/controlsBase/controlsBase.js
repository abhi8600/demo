/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.controlsBase.Control};</li>
 *  <li>@class {anychart.controls.controlsBase.ControlTitle};</li>
 *  <li>@class {anychart.controls.controlsBase.ControlWithTitle};</li>
 * <ul>
 */
goog.provide('anychart.controls.controlsBase');
goog.require('anychart.layout');
goog.require('anychart.utils');
goog.require('anychart.controls.layout');
goog.require('anychart.visual.text');
goog.require('anychart.events');
//------------------------------------------------------------------------------
//
//                           Control class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.controlsBase.Control = function() {
    this.layout_ = new anychart.controls.layout.ControlLayout();
    this.bounds_ = new anychart.utils.geom.Rectangle();
    this.contentBounds_ = new anychart.utils.geom.Rectangle();
    this.eventTarget_ = new goog.events.EventTarget();
};
//------------------------------------------------------------------------------
//                           Control properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.controlsBase.Control.prototype.bounds_ = null;
anychart.controls.controlsBase.Control.prototype.getBounds = function() {
    return this.bounds_;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.controlsBase.Control.prototype.contentBounds_ = null;
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.controls.controlsBase.Control.prototype.background_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.controls.controlsBase.Control.prototype.backgroundSprite_ = null;
/**
 * @private
 * @type {anychart.controls.layout.ControlLayout}
 */
anychart.controls.controlsBase.Control.prototype.layout_ = null;
/**
 * @return {anychart.controls.layout.ControlLayout}
 */
anychart.controls.controlsBase.Control.prototype.getLayout = function() {
    return this.layout_;
};
/**
 * @private
 * @type {anychart.svg.SVGScrollableSprite}
 */
anychart.controls.controlsBase.Control.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGScrollableSprite}
 */
anychart.controls.controlsBase.Control.prototype.getSprite = function() {
    return this.sprite_;
};
/**
 * @return {anychart.svg.SVGManager}
 */
anychart.controls.controlsBase.Control.prototype.getSVGManager = function() {
    return this.plot_.getSVGManager();
};
//------------------------------------------------------------------------------
//                           External properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.controls.controlsBase.Control.prototype.mainChartView_ = null;
/**
 * @private
 */
anychart.controls.controlsBase.Control.prototype.plot_ = null;
/**
 * @private
 */
anychart.controls.controlsBase.Control.prototype.plotContainer_ = null;
anychart.controls.controlsBase.Control.prototype.getPlotContainer = function() {
    return this.plotContainer_;
};
//------------------------------------------------------------------------------
//                          Initialization
//------------------------------------------------------------------------------
/**
 * Initialize control.
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.controlsBase.Control.prototype.initialize = function(svgManager) {
    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.initializeBackground_(svgManager);
    goog.events.listen(this.sprite_.getElement(), goog.events.EventType.MOUSEDOWN, this.redirectEvent_, false, this);
    return this.sprite_;
};
/**
 * Initialize control data container.
 * @param {anychart.chartView.MainChartView} mainChartView
 * @param {anychart.plots.BasePlot} plot
 * @param {?} plotContainer
 */
anychart.controls.controlsBase.Control.prototype.initializeDataContainer = function(mainChartView, plot, plotContainer) {
    this.mainChartView_ = mainChartView;
    this.plot_ = plot;
    this.plotContainer_ = plotContainer;
    this.itemsSprite_ = new anychart.svg.SVGScrollableSprite(this.plot_.getSVGManager());
};
/**
 * Initialize control background
 * @private
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.controlsBase.Control.prototype.initializeBackground_ = function(svgManager) {
    if (this.background_ && this.background_.isEnabled()) {
        this.backgroundSprite_ = this.background_.initialize(svgManager);
        this.sprite_.appendSprite(this.backgroundSprite_);
    }
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.controls.controlsBase.Control.prototype.deserialize = function(data, stylesList) {
    this.layout_.deserialize(data);
    if (anychart.utils.deserialization.hasProp(data, 'background')) {
        var bgData = anychart.utils.deserialization.getProp(data, 'background');
        if (anychart.utils.deserialization.isEnabled(bgData)) {
            this.background_ = new anychart.visual.background.RectBackground();
            this.background_.deserialize(bgData);
        }
    }
};
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
anychart.controls.controlsBase.Control.prototype.setBounds = function(chartBounds, plotBounds) {
    this.layout_.setBounds(this.bounds_, chartBounds, plotBounds);
    this.checkControlBounds();
    this.setContentBounds();
};
/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.getContentWidth = function() {
    var width = this.bounds_.width;
    if (this.hasMargin()) width -= this.getHorizontalMargin();
    return width;
};
/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.getMaxContentWidth = function() {
    var width = this.layout_.getMaxWidth();
    if (this.hasMargin()) width -= this.getHorizontalMargin();
    return width;
};
/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.getContentHeight = function() {
    var height = this.bounds_.height;
    if (this.hasMargin()) height -= this.getVerticalMargin();
    return height;
};
/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.getMaxContentHeight = function() {
    var height = this.layout_.getMaxHeight();
    if (this.hasMargin()) height -= this.getVerticalMargin();
    return height;
};

/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.checkControlBounds = function() {
    this.bounds_.width += this.getHorizontalLayoutMargin();
    this.bounds_.height += this.getVerticalLayoutMargin();
    if (this.hasMargin()) {
        if (!this.layout_.isWidthSetted())
            this.bounds_.width += this.getHorizontalMargin();
        if (!this.layout_.isHeightSetted())
            this.bounds_.height += this.getVerticalMargin();
    }
};
/**
 * @protected
 */
anychart.controls.controlsBase.Control.prototype.setContentBounds = function() {
    this.contentBounds_.x = 0;
    this.contentBounds_.y = 0;

    this.contentBounds_.width =
        this.bounds_.width - this.getHorizontalLayoutMargin();

    this.contentBounds_.height =
        this.bounds_.height - this.getVerticalLayoutMargin();
    if (this.hasMargin()) this.background_.getMargin().applyInside(this.contentBounds_);
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
anychart.controls.controlsBase.Control.prototype.draw = function() {
    this.setSpritePosition();
    this.drawBackground();
};
anychart.controls.controlsBase.Control.prototype.setSpritePosition = function() {
    this.sprite_.setX(this.bounds_.x + this.layout_.getMargin().getLeft());
    this.sprite_.setY(this.bounds_.y + this.layout_.getMargin().getTop());
};
anychart.controls.controlsBase.Control.prototype.drawBackground = function() {
    if (this.backgroundSprite_) this.background_.draw(this.backgroundSprite_, this.getBackgroundBounds());
};
//------------------------------------------------------------------------------
//                          Events
//------------------------------------------------------------------------------

/**
 * @private
 * @type {goog.events.EventTarget}
 */
anychart.controls.controlsBase.Control.prototype.eventTarget_ = null;
/**
 * Add event listener fot control.
 * @param {String} eventType
 * @param {Function} handler
 * @param {Boolean} opt_capture
 * @param {Object} opt_scope
 */
anychart.controls.controlsBase.Control.prototype.addEventListener = function(eventType, handler, opt_capture, opt_scope) {
    this.eventTarget_.addEventListener(goog.events.EventType.MOUSEDOWN, handler, opt_capture, opt_scope);
};
/**
 * @private
 * @param {goog.events.Event} event
 */
anychart.controls.controlsBase.Control.prototype.redirectEvent_ = function(event) {
    event.target = this;
    this.eventTarget_.dispatchEvent(event)
};
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.controls.controlsBase.Control.prototype.hasMargin = function() {
    return this.background_ && this.background_.getMargin();
};
/**
 * @return {Number}
 */
anychart.controls.controlsBase.Control.prototype.getHorizontalMargin = function() {
    return this.background_.getMargin().getLeft() +
        this.background_.getMargin().getRight();
};
/**
 * @return {Number}
 */
anychart.controls.controlsBase.Control.prototype.getVerticalMargin = function() {
    return this.background_.getMargin().getTop() +
        this.background_.getMargin().getBottom();
};
/**
 * @return {Number}
 */
anychart.controls.controlsBase.Control.prototype.getHorizontalLayoutMargin = function() {
    return this.layout_.getMargin().getLeft() +
        this.layout_.getMargin().getRight();
};
/**
 * @return {Number}
 */
anychart.controls.controlsBase.Control.prototype.getVerticalLayoutMargin = function() {
    return this.layout_.getMargin().getTop() +
        this.layout_.getMargin().getBottom();
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.controlsBase.Control.prototype.getBackgroundBounds = function() {
    return new anychart.utils.geom.Rectangle(
        0,
        0,
        this.bounds_.width - this.getHorizontalLayoutMargin(),
        this.bounds_.height - this.getVerticalLayoutMargin());
};
//------------------------------------------------------------------------------
//                               Resize.
//------------------------------------------------------------------------------
/**
 * Resize control background.
 */
anychart.controls.controlsBase.Control.prototype.resize = function() {
    this.setSpritePosition();
    if (this.background_) this.background_.resize(
        this.backgroundSprite_,
        this.getBackgroundBounds());
};
//------------------------------------------------------------------------------
//
//                           ControlTitle class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.controlsBase.ControlTitle = function() {
    anychart.visual.text.BaseTitle.apply(this);
};
goog.inherits(anychart.controls.controlsBase.ControlTitle, anychart.visual.text.BaseTitle);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.controlsBase.ControlTitle.prototype.space_ = null;
/**
 * @return {Number}
 */
anychart.controls.controlsBase.ControlTitle.prototype.getSpace = function() {
    return this.space_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.controlsBase.ControlTitle.prototype.minWidth_ = null;
/**
 * @return {Number}
 */
anychart.controls.controlsBase.ControlTitle.prototype.getMinWidth = function() {
    return this.minWidth_;
};
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.controls.controlsBase.ControlTitle.prototype.separator_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.controlsBase.ControlTitle.prototype.separatorBounds_ = null;
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
anychart.controls.controlsBase.ControlTitle.prototype.deserialize = function(data) {
    if (!data) return;
    goog.base(this, 'deserialize', anychart.utils.deserialization.getProp(data, 'title'));
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'title_separator')) {
        this.separator_ = new anychart.visual.stroke.Stroke();
        this.separator_.deserialize(des.getProp(data, 'title_separator'));
    }
};
//------------------------------------------------------------------------------
//                          Size
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.controls.controlsBase.ControlTitle.prototype.initSize = function(svgManager, plot) {
    goog.base(this, 'initSize', svgManager, this.textFormatter_.getValue(plot));
    this.space_ = this.bounds_.height + this.padding_;
    this.minWidth_ = this.bounds_.width;
    if (this.separator_) this.space_ += this.separator_.getThickness();
};
/**
 * Apply title space to bounds.
 * @param {anychart.utils.geom.Rectangle} bounds Target bounds
 */
anychart.controls.controlsBase.ControlTitle.prototype.applySpace = function(bounds) {
    bounds.y += this.space_;
    bounds.height -= this.space_;
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
anychart.controls.controlsBase.ControlTitle.prototype.createSVGText = function(svgManager, contentBounds) {
    goog.base(this, 'createSVGText', svgManager);
    var controlTitleSprite = new anychart.svg.SVGSprite(svgManager);
    var y = this.padding_;
    var x;
    switch (this.align_) {
        case anychart.layout.AbstractAlign.NEAR:
            x = contentBounds.x;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            x = contentBounds.x + (contentBounds.width - this.bounds_.width) / 2;
            break;
        case anychart.layout.AbstractAlign.FAR:
            x = contentBounds.getRight() - this.bounds_.width;
            break;
    }
    this.sprite_.setX(x);
    this.sprite_.setY(y);
    controlTitleSprite.appendSprite(this.sprite_);

    if (this.separator_) {
        y += this.bounds_.height + this.separator_.getThickness() / 2 + 2;
        this.separatorBounds_ = new anychart.utils.geom.Rectangle(
            contentBounds.x,
            y,
            contentBounds.width,
            this.separator_.getThickness() / 2);
        this.drawSeparator_(svgManager, controlTitleSprite);
    }

    return controlTitleSprite;
};

anychart.controls.controlsBase.ControlTitle.prototype.drawSeparator_ = function(svgManager, sprite) {
    var strokeStyle = this.separator_.getSVGStroke(
        svgManager,
        this.separatorBounds_);
    var path = svgManager.createPath(true);
    var pathData = svgManager.pMove(this.separatorBounds_.getLeft(), this.separatorBounds_.y);
    pathData += svgManager.pLine(this.separatorBounds_.getRight(), this.separatorBounds_.y);

    path.setAttribute('d', pathData);
    path.setAttribute('style', strokeStyle);

    sprite.appendChild(path);
};
//------------------------------------------------------------------------------
//
//                           ControlWithTitle class.
//
//------------------------------------------------------------------------------
/**@constructor*/
anychart.controls.controlsBase.ControlWithTitle = function() {
    anychart.controls.controlsBase.Control.apply(this);
};
goog.inherits(anychart.controls.controlsBase.ControlWithTitle,
    anychart.controls.controlsBase.Control);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.controlsBase.ControlTitle}
 */
anychart.controls.controlsBase.ControlWithTitle.prototype.title_ = null;
/**
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.controls.controlsBase.ControlWithTitle.prototype.titleSprite_ = null;
/**
 * Control bounds without title.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.controlsBase.ControlWithTitle.prototype.noTitleBounds_ = null;
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
anychart.controls.controlsBase.ControlWithTitle.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;
    if (des.isEnabledProp(data, 'title')) {
        this.title_ = new anychart.controls.controlsBase.ControlTitle();
        this.title_.deserialize(data);
        this.title_.initSize(this.plot_.getSVGManager(), this.plot_);
    }
};
//------------------------------------------------------------------------------
//                          Bounds
//------------------------------------------------------------------------------
anychart.controls.controlsBase.ControlWithTitle.prototype.getMaxContentHeight = function() {
    var height = goog.base(this, 'getMaxContentHeight');
    if (this.title_) height -= this.title_.getSpace();
    return height;
};

anychart.controls.controlsBase.ControlWithTitle.prototype.getContentHeight = function() {
    var height = goog.base(this, 'getContentHeight');
    if (this.title_)
        height -= this.title_.getSpace();
    return height;
};

anychart.controls.controlsBase.ControlWithTitle.prototype.checkControlBounds = function() {
    if (this.title_) {
        if (!this.layout_.isWidthSetted())
            this.bounds_.width = Math.max(this.bounds_.width, this.title_.getMinWidth());
        if (!this.layout_.isHeightSetted())
            this.bounds_.height += this.title_.getSpace();
    }
    goog.base(this, 'checkControlBounds');
};

anychart.controls.controlsBase.ControlWithTitle.prototype.setContentBounds = function() {
    goog.base(this, 'setContentBounds');
    this.noTitleBounds_ = this.contentBounds_.clone();
    if (this.title_) this.title_.applySpace(this.contentBounds_);
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
anychart.controls.controlsBase.ControlWithTitle.prototype.draw = function() {
    goog.base(this, 'draw');
    if (this.title_) {
        this.titleSprite_ = this.title_.createSVGText(
            this.sprite_.getSVGManager(),
            this.noTitleBounds_);
        this.sprite_.appendSprite(this.titleSprite_);
    }
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * Resize title and background.
 */
anychart.controls.controlsBase.ControlWithTitle.prototype.resize = function() {
    goog.base(this, 'resize');
    if (this.title_) {
        this.sprite_.removeChild(this.titleSprite_);
        this.titleSprite_ = this.title_.createSVGText(
            this.sprite_.getSVGManager(),
            this.noTitleBounds_);
        this.sprite_.appendSprite(this.titleSprite_);
    }
};