/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.colorSwatch.ColorSwatchLabelsPosition}</li>
 *  <li>@class {anychart.controls.colorSwatch.ColorSwatchLabels}</li>
 *  <li>@class {anychart.controls.colorSwatch.RangeSprite}</li>
 *  <li>@class {anychart.controls.colorSwatch.ColorSwatchControl}</li>
 *  <li>@class {anychart.controls.colorSwatch.HorizontalColorSwatch}</li>
 *  <li>@class {anychart.controls.colorSwatch.VerticalColorSwatch}</li>
 * <ul>
 */
goog.provide('anychart.controls.colorSwatch');
goog.require('anychart.controls.controlsBase');
//------------------------------------------------------------------------------
//
//                  ColorSwatchLabelsPosition class.
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.controls.colorSwatch.ColorSwatchLabelsPosition = {
    NORMAL : 0,
    OPPOSITE : 1,
    COMBINED : 2
};
/**
 * @param {String} value
 * @return {int}
 */
anychart.controls.colorSwatch.ColorSwatchLabelsPosition.deserialize = function(value) {
    switch (value) {
        case 'normal' :
            return anychart.controls.colorSwatch.ColorSwatchLabelsPosition.NORMAL;
        default :
        case 'combined' :
            return anychart.controls.colorSwatch.ColorSwatchLabelsPosition.COMBINED;
        case 'opposite' :
            return anychart.controls.colorSwatch.ColorSwatchLabelsPosition.OPPOSITE;
    }
};
//------------------------------------------------------------------------------
//
//                          ColorSwatchLabels class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.text.TextElement}
 */
anychart.controls.colorSwatch.ColorSwatchLabels = function() {
    anychart.visual.text.TextElement.call(this);
    this.padding_ = 5;
    this.tokensHash_ = new anychart.formatting.TokensHash();
};
goog.inherits(anychart.controls.colorSwatch.ColorSwatchLabels,
    anychart.visual.text.TextElement);
//------------------------------------------------------------------------------
//                          Common  properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.colorSwatch.ColorSwatchControl}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.control_ = null;
/**
 * @param {anychart.controls.colorSwatch.ColorSwatchControl} value
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.setControl = function(value) {
    this.control_ = value;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.padding_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getPadding = function() {
    return this.padding_;
};
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.textFormatter_ = null;
/**
 * @private
 * @type {anychart.thresholds.AutomaticThreshold}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.threshold_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.labelSprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.svgManager_ = null;
//------------------------------------------------------------------------------
//                   Space calculation properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<anychart.utils.geom.Rectangle>}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.labelsBounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getFirstBounds = function() {
    return this.getLabelsBounds(0);
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getLastBounds = function() {
    return this.getLabelsBounds(this.labelsBounds_.length - 1);
};
/**
 * @param {int} index
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getLabelsBounds = function(index) {
    return this.labelsBounds_[index];
};
/**
 * @private
 * @type {Array.<String>}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.labelsText_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxEvenLabelWidth_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getMaxEvenLabelWidth = function() {
    return this.maxEvenLabelWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxEvenLabelHeight_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getMaxEvenLabelHeight = function() {
    return this.maxEvenLabelHeight_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.summEvenLabelWidth_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getSummEvenLabelWidth = function() {
    return this.summEvenLabelWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.summEvenLabelHeight_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getSummEvenLabelHeight = function() {
    return this.summEvenLabelHeight_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxOddLabelWidth_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getMaxOddLabelWidth = function() {
    return this.maxOddLabelWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxOddLabelHeight_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getMaxOddLabelHeight = function() {
    return this.maxOddLabelHeight_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.summOddLabelWidth_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getSummOddLabelWidth = function() {
    return this.summOddLabelWidth_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.summOddLabelHeight_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getSummOddLabelHeight = function() {
    return this.summOddLabelHeight_;
};
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxSummIAndIPlus1LabelWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxSummIAndIPlus2LabelWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxSummIAndIPlus1LabelHeight_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.maxSummIAndIPlus2LabelHeight_ = null;
/**
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getMaxSummIAndIPlus2LabelHeight = function() {
    return this.maxSummIAndIPlus2LabelHeight_;
};
//------------------------------------------------------------------------------
//                       Value properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.startValue_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.endValue_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.value_ = null;
//------------------------------------------------------------------------------
//                          Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'format'))
        this.textFormatter_ = new anychart.formatting.TextFormatter(des.getStringProp(data, 'format'));

    if (des.hasProp(data, 'padding'))
        this.padding_ = des.getNumProp(data, 'padding');
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize color swatch labels.
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.initialize = function(svgManager) {
    this.labelsBounds_ = [];
    this.labelsText_ = [];
    this.svgManager_ = svgManager;

    this.maxEvenLabelHeight_ = 0;
    this.maxEvenLabelWidth_ = 0;
    this.summEvenLabelHeight_ = 0;
    this.summEvenLabelWidth_ = 0;
    this.maxOddLabelHeight_ = 0;
    this.maxOddLabelWidth_ = 0;
    this.summOddLabelHeight_ = 0;
    this.summOddLabelWidth_ = 0;
    this.maxSummIAndIPlus1LabelWidth_ = 0;
    this.maxSummIAndIPlus2LabelWidth_ = 0;
    this.maxSummIAndIPlus1LabelHeight_ = 0;
    this.maxSummIAndIPlus2LabelHeight_ = 0;

    this.threshold_ = this.control_.getThreshold();

    if (this.control_.isTickmarkOnRangeCenter())
        this.initializeLabels(this.initCenterLabel_);
    else
        this.initializeLabels(this.initBoundLabel_);
};
/**
 * Инициализирует лейблы по переданному алгоритму.
 * @param {Function} initializeMethod
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.initializeLabels = function(initializeMethod) {
    var count = this.threshold_.getRangeCount();
    var i;

    if (this.control_.isInverted())
        for (i = count - 1; i >= 0; i--)
            initializeMethod.call(this, i);
    else
        for (i = 0; i <= count; i++)
            initializeMethod.call(this, i);

    this.calculateMaxSummIAndIPlusLabelSize_();
};
/**
 * @private
 * @param {int} index
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.initCenterLabel_ = function(index) {
    this.startValue_ = this.threshold_.getRangeStart(index);
    this.endValue_ = this.threshold_.getRangeEnd(index);
    this.value_ = this.startValue_ + (this.endValue_ - this.startValue_) / 2;
    this.setLabelsTokenValue_();
    this.createLabelsBoundsArray_(index % 2 == 0);
};
/**
 * @private
 * @param {int} index
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.initBoundLabel_ = function(index) {
    this.value_ = (index != this.threshold_.getRangeCount()) ? this.threshold_.getRangeStart(index) : this.threshold_.getRangeEnd(index - 1);
    this.startValue_ = this.endValue_ = this.value_;
    this.setLabelsTokenValue_();
    this.createLabelsBoundsArray_(index % 2 == 0);
};
/**
 * Initialize labels
 * @param {Boolean} isEven
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.createLabelsBoundsArray_ = function(isEven) {
    var text = this.textFormatter_.getValue(this);
    this.initSize(this.svgManager_, text);
    this.labelsBounds_.push(this.getBounds());
    this.labelsText_.push(text);

    if (isEven) {
        this.summEvenLabelWidth_ += this.getBounds().width;
        this.summEvenLabelHeight_ += this.getBounds().height;
        this.maxEvenLabelWidth_ = Math.max(this.maxEvenLabelWidth_, this.getBounds().width);
        this.maxEvenLabelHeight_ = Math.max(this.maxEvenLabelHeight_, this.getBounds().height);
    } else {
        this.summOddLabelWidth_ += this.getBounds().width;
        this.summOddLabelHeight_ += this.getBounds().height;
        this.maxOddLabelWidth_ = Math.max(this.maxOddLabelWidth_, this.getBounds().width);
        this.maxOddLabelHeight_ = Math.max(this.maxOddLabelHeight_, this.getBounds().height);
    }
};
/**
 * @private
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.calculateMaxSummIAndIPlusLabelSize_ = function() {
    var count = this.threshold_.getRangeCount();
    var i;

    for (i = 0; i < count; i++) {
        this.maxSummIAndIPlus1LabelWidth_ = Math.max(
            this.maxSummIAndIPlus1LabelWidth_,
            this.labelsBounds_[i].width + this.labelsBounds_[i + 1].width);

        this.maxSummIAndIPlus1LabelHeight_ = Math.max(
            this.maxSummIAndIPlus1LabelHeight_,
            this.labelsBounds_[i].height + this.labelsBounds_[i + 1].height);
    }

    if (count > 2) {
        for (i = 0; i <= count - 2; i++) {
            this.maxSummIAndIPlus2LabelWidth_ = Math.max(
                this.maxSummIAndIPlus2LabelWidth_,
                this.labelsBounds_[i].width + this.labelsBounds_[i + 1].width);

            this.maxSummIAndIPlus2LabelHeight_ = Math.max(
                this.maxSummIAndIPlus2LabelHeight_,
                this.labelsBounds_[i].height + this.labelsBounds_[i + 1].height);
        }
    }
};
//------------------------------------------------------------------------------
//                           Drawing.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGSprite} sprite
 * @param {anychart.utils.geom.Point} position
 * @param {int} index
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.drawLabel = function(sprite, position, index) {
    this.initSize(this.svgManager_, this.labelsText_[index]);
    this.labelSprite_ = this.createSVGText(this.svgManager_);
    this.labelSprite_.setPosition(position.x, position.y);
    sprite.appendSprite(this.labelSprite_);
};
//------------------------------------------------------------------------------
//                       Formatting.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.tokensHash_ = null;
/**
 * @private
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.setLabelsTokenValue_ = function () {
    this.tokensHash_.add('%RangeMin', this.startValue_);
    this.tokensHash_.add('%RangeMax', this.endValue_);
    this.tokensHash_.add('%Value', this.value_);
};
/**
 * @param {String} token
 * @return {*}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getTokenValue = function (token) {
    if (this.tokensHash_.isThresholdToken(token))
        return this.threshold_.getTokenValue(token);
    return this.tokensHash_.get(token);
};
/**
 * @param {String} token
 * @return {int}
 */
anychart.controls.colorSwatch.ColorSwatchLabels.prototype.getTokenType = function (token) {
    if (this.tokensHash_.isThresholdToken(token))
        return this.threshold_.getTokenType(token);

    switch (token) {
        case '%RangeMin':
        case '%RangeMax':
        case '%Value':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return anychart.formatting.TextFormatTokenType.UNKNOWN;
};
//------------------------------------------------------------------------------
//
//                          RangeSprite class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.svg.SVGSprite}
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.colorSwatch.RangeSprite = function(svgManager) {
    anychart.svg.SVGSprite.call(this, svgManager);
    svgManager.setCursorPointer(this.element_);
    this.bounds_ = new anychart.utils.geom.Rectangle();

    goog.events.listen(this.element_, goog.events.EventType.MOUSEOVER, this.onMouseOver_, false, this);
    goog.events.listen(this.element_, goog.events.EventType.MOUSEOUT, this.onMouseOut_, false, this);

    if (IS_ANYCHART_DEBUG_MOD)
        this.element_.setAttribute('id', 'RangeSprite');
};
goog.inherits(anychart.controls.colorSwatch.RangeSprite, anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.background.BackgroundStyle}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.style_ = null;
/**
 * @param {anychart.styles.background.BackgroundStyle} value
 */
anychart.controls.colorSwatch.RangeSprite.prototype.setStyle = function(value) {
    this.style_ = value;
};
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.getBounds = function() {
    return this.bounds_;
};
/**
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.colorContainer_ = null;
/**
 * @param {anychart.visual.color.ColorContainer} value
 */
anychart.controls.colorSwatch.RangeSprite.prototype.setColorContainer = function(value) {
    this.colorContainer_ = value;
};
/**
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.points_ = null;
/**
 * @param {Array.<anychart.plots.seriesPlot.data.BasePoint>} value
 */
anychart.controls.colorSwatch.RangeSprite.prototype.setPoints = function(value) {
    this.points_ = value;
};
/**
 * @private
 * @type {anychart.styles.StyleState}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.currentState_ = null;
//------------------------------------------------------------------------------
//                          Segments.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {SVGElement}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.fillSegment_ = null;
/**
 * @private
 * @type {SVGElement}
 */
anychart.controls.colorSwatch.RangeSprite.prototype.hatchFillSegment_ = null;
//------------------------------------------------------------------------------
//                       Event handlers.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.controls.colorSwatch.RangeSprite.prototype.onMouseOver_ = function() {
    this.currentState_ = this.style_.getHover();

    var pointsCount = this.points_.length;
    for (var i = 0; i < pointsCount; i++) this.points_[i].setHover();


    this.update();
};
/**
 * @private
 */
anychart.controls.colorSwatch.RangeSprite.prototype.onMouseOut_ = function() {
    this.currentState_ = this.style_.getNormal();

    var pointsCount = this.points_.length;
    for (var i = 0; i < pointsCount; i++) this.points_[i].setNormal();

    this.update();
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw range sprite
 */
anychart.controls.colorSwatch.RangeSprite.prototype.draw = function() {
    this.currentState_ = this.style_.getNormal();

    this.fillSegment_ = this.svgManager_.createRect();
    this.hatchFillSegment_ = this.svgManager_.createRect();

    this.element_.appendChild(this.fillSegment_);
    this.element_.appendChild(this.hatchFillSegment_);

    this.update();
};
//------------------------------------------------------------------------------
//                           Update.
//------------------------------------------------------------------------------
/**
 * Update range sprite
 */
anychart.controls.colorSwatch.RangeSprite.prototype.update = function() {
    var style;
    //fill
    if (this.currentState_.needUpdateFillShape()) {
        style = '';
        if (this.currentState_.needUpdateFill()) {
            style += this.currentState_.getFill().getSVGFill(
                this.svgManager_,
                this.bounds_,
                this.colorContainer_.getColor());
        } else style += this.svgManager_.getEmptySVGFill();
        if (this.currentState_.needUpdateBorder()) {
            style += this.currentState_.getBorder().getSVGStroke(
                this.svgManager_,
                this.bounds_,
                this.colorContainer_.getColor())
        } else style += this.svgManager_.getEmptySVGStroke();


    } else style = this.svgManager_.getEmptySVGStyle();

    this.fillSegment_.setAttribute('style', style);
    this.updatePath(this.fillSegment_);

    //hatch fill
    if (this.currentState_.needUpdateHatchFill()) {
        style = this.currentState_.getHatchFill().getSVGHatchFill(
            this.svgManager_,
            null,
            this.colorContainer_)
    } else style += this.svgManager_.getEmptySVGStroke();

    this.hatchFillSegment_.setAttribute('style', style);
    this.updatePath(this.hatchFillSegment_);
};
/**
 * @private
 * @param {SVGElement} segment
 */
anychart.controls.colorSwatch.RangeSprite.prototype.updatePath = function(segment) {
    segment.setAttribute('x', this.bounds_.x);
    segment.setAttribute('y', this.bounds_.y);
    segment.setAttribute('width', this.bounds_.width);
    segment.setAttribute('height', this.bounds_.height);
};
//------------------------------------------------------------------------------
//
//                      ColorSwatchControl class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.controlsBase.ControlWithTitle}
 */
anychart.controls.colorSwatch.ColorSwatchControl = function() {
    anychart.controls.controlsBase.ControlWithTitle.call(this);
    //tickmark defaults
    this.tickmarkOnRangeCenter_ = false;
    this.tickmarksSize_ = 5;
    this.tickmarksSpace_ = 0;
    this.normalTickmarksSpace_ = 0;

    //labels defaults
    this.labelsSpace_ = 0;
    this.normalLabelsSpace_ = 0;
    this.labelsPosition_ = anychart.controls.colorSwatch.ColorSwatchLabelsPosition.COMBINED;

    //placement defaults
    this.isRangeItemWidthSetted_ = false;
    this.isRangeItemHeightSetted_ = false;
    this.inverted_ = false;
    this.interval_ = 1;

    //initialization defaults
    this.isRangesInitialized_ = false;
};
goog.inherits(anychart.controls.colorSwatch.ColorSwatchControl,
    anychart.controls.controlsBase.ControlWithTitle);
//------------------------------------------------------------------------------
//                      Size properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.rangeItemWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.rangeItemHeight_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.settedRangeItemWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.settedRangeItemHeight_ = null;
/**
 * Сдвиги в случае, когда размер color swatch, указаный в XML больше чем надо
 * (Центрование)
 */
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.leftShift_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.topShift_ = null;
//------------------------------------------------------------------------------
//                       Visual properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.Style}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.rangeItemBackground_ = null;
/**
 * В него рисуется всё кроме ranges, для сохранения событий при ресайзе.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.contentSprite_ = null;
//------------------------------------------------------------------------------
//                  Labels properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.controls.colorSwatch.ColorSwatchLabels}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.labels_ = null;
/**
 * @private
 * @type {int}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.labelsPosition_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.normalLabelsSpace_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.labelsSpace_ = null;
//------------------------------------------------------------------------------
//                       Tickmark properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.tickmark_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.tickmarkOnRangeCenter_ = null;
/**
 * @return {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isTickmarkOnRangeCenter = function() {
    return this.tickmarkOnRangeCenter_;
};
/**
 * @private
 * @type {Number}
 */

anychart.controls.colorSwatch.ColorSwatchControl.prototype.tickmarksSize_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.normalTickmarksSpace_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.tickmarksSpace_ = null;
//------------------------------------------------------------------------------
//                    Ranges properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.inverted_ = null;
/**
 * @return {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isInverted = function() {
    return this.inverted_;
};
/**
 * @private
 * @type {int}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.interval_ = null;
/**
 * @private
 * @type {anychart.thresholds.AutomaticThreshold}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.threshold_ = null;
/**
 * @return {anychart.thresholds.AutomaticThreshold}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.getThreshold = function() {
    return this.threshold_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isRangeItemWidthSetted_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isRangeItemHeightSetted_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.defaultRangeItemWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.defaultRangeItemHeight_ = null;
//------------------------------------------------------------------------------
//                  Position properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<anychart.controls.colorSwatch.RangeSprite>}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.ranges_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.left_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.top_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.lastLabelRect_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.lastEvenLabelRect_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.lastOddLabelRect_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.deserialize = function(data, stylesList) {
    goog.base(this, 'deserialize', data, stylesList);
    var des = anychart.utils.deserialization;

    //item deserialization
    if (des.hasProp(data, 'range_item')) {
        var item = des.getProp(data, 'range_item');

        if (des.hasProp(item, 'width'))
            this.settedRangeItemWidth_ = des.getNumProp(item, 'width');

        if (des.hasProp(item, 'height'))
            this.settedRangeItemHeight_ = des.getNumProp(item, 'height');

        if (des.isEnabledProp(item, 'background')) {
            this.rangeItemBackground_ = new anychart.styles.background.BackgroundStyle();
            this.rangeItemBackground_.deserialize(des.getProp(item, 'background'));
        }
    }

    //labels deserialization
    if (des.hasProp(data, 'labels')) {
        var labels = des.getProp(data, 'labels');
        if (des.isEnabled(labels)) {
            this.labelsPosition_ = anychart.controls.colorSwatch.ColorSwatchLabelsPosition.deserialize(des.getEnumProp(labels, 'position'));
            this.labels_ = new anychart.controls.colorSwatch.ColorSwatchLabels();
            this.labels_.deserialize(labels);
            this.labels_.setControl(this);
        }
    }

    //tickmark deserialization
    if (des.isEnabledProp(data, 'tickmark')) {
        var tickmark = des.getProp(data, 'tickmark');
        this.tickmark_ = new anychart.visual.stroke.Stroke();
        this.tickmark_.deserialize(tickmark);
        if (des.hasProp(tickmark, 'size'))
            this.tickmarksSize_ = des.getNumProp(tickmark, 'size');
    }

    if (des.hasProp(data, 'tickmarks_placement'))
        this.tickmarkOnRangeCenter_ = des.getEnumProp(data, 'tickmarks_placement') == 'center';

    if (des.hasProp(data, 'inverted'))
        this.inverted_ = des.getBoolProp(data, 'inverted');

    if (des.getProp(data, 'interval'))
        this.interval_ = des.getNumProp(data, 'interval');

    if (des.hasProp(data, 'threshold'))
        this.threshold_ = this.plot_.getThresholdsList().getAutoThreshold(des.getLStringProp(data, 'threshold'));
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.controls.colorSwatch.ColorSwatchControl.prototype.initialize = function(svgManager) {
    goog.base(this, 'initialize', svgManager);
    this.contentSprite_ = new anychart.svg.SVGSprite(svgManager);

    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('ColorSwatch');

    return this.sprite_;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.setContentBounds = function() {
    if (!this.threshold_) {
        this.bounds_.width = 0;
        this.bounds_.height = 0;
        goog.base(this, 'setContentBounds', this);
        return;
    }

    this.initializeRanges(this.plot_.getSVGManager());

    /*
     Просчет размеров color swatch

     Допустим частный случай для ширины:
     Есть два параметра, влияющие на ширину:
     1. ширина контрола, руками заданая в XML
     2. ширина range item, руками заданая в XML

     Соостветственно получаем несколько случаев:
     1. Ничего не указано
     Подбираем ширину range item, исходя из label-ов и их позиций
     На основе этой ширины просчитываем ширину контрола

     2. Указана ширина контрола
     Подбираем ширину range item так, что бы все влезли (учитываем label)

     3. Указана ширина range item
     На основе этой ширины просчитываем ширину контрола

     4. Указана и ширина контрола и ширина range item
     Просчитываем максимальную ширину range item исходя из того, что бы все влезло
     Выставлем минимальное значение из подсчитаной и указаной ширины - тогда все гарантировано влезет
     И затем если указаная ширина range item меньше подсчитаной - центруем содержимое
     */


    this.leftShift_ = 0;
    this.topShift_ = 0;

    if (!this.layout_.isWidthSetted() && !this.isRangeItemWidthSetted_) {
        this.rangeItemWidth_ = this.calculateOptimalRangeItemWidth();
        this.bounds_.width = this.calculateWidthBasedOnRangeItemWidth();
    } else if (this.layout_.isWidthSetted() && !this.isRangeItemWidthSetted_) {
        this.rangeItemWidth_ = this.calculateRangeItemWidthBasedOnWidth();
        if (this.rangeItemWidth_ < 0) {
            this.rangeItemWidth_ = 0;
            this.bounds_.width = this.calculateWidthBasedOnRangeItemWidth();
        }
    } else if (!this.layout_.isWidthSetted() && this.isRangeItemWidthSetted_) {
        this.rangeItemWidth_ = this.settedRangeItemWidth_;
        this.bounds_.width = this.calculateWidthBasedOnRangeItemWidth();
    } else {
        var maxWidth = this.calculateRangeItemWidthBasedOnWidth();
        this.rangeItemWidth_ = Math.min(maxWidth, this.settedRangeItemWidth_);

        var contentWidth = this.calculateWidthBasedOnRangeItemWidth();
        var availableWidth = this.getContentWidth();
        if (contentWidth < availableWidth)
            this.leftShift_ = (availableWidth - contentWidth) / 2;
    }

    if (!this.layout_.isHeightSetted() && !this.isRangeItemHeightSetted_) {
        this.rangeItemHeight_ = this.calculateOptimalRangeItemHeight();
        this.bounds_.height = this.calculateHeightBasedOnRangeItemHeight();
    } else if (this.layout_.isHeightSetted() && !this.isRangeItemHeightSetted_) {
        this.rangeItemHeight_ = this.calculateRangeItemHeightBasedOnHeight();
        if (this.rangeItemHeight_ < 0) {
            this.rangeItemHeight_ = 0;
            this.bounds_.height = this.calculateHeightBasedOnRangeItemHeight();
        }
    } else if (!this.layout_.isHeightSetted() && this.isRangeItemHeightSetted_) {
        this.rangeItemHeight_ = this.settedRangeItemHeight_;
        this.bounds_.height = this.calculateHeightBasedOnRangeItemHeight();
    } else {
        var maxHeight = this.calculateRangeItemHeightBasedOnHeight();
        this.rangeItemHeight_ = Math.min(maxHeight, this.settedRangeItemHeight_);

        var contentHeight = this.calculateHeightBasedOnRangeItemHeight();
        var availableHeight = this.getContentHeight();
        if (contentHeight < availableHeight)
            this.topShift_ = (availableHeight - contentHeight) / 2;
    }

    goog.base(this, 'setContentBounds');
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isRangesInitialized_ = null;
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.initializeRanges = function(svgManager) {
    if (this.isRangesInitialized_) return;

    //init labels
    if (this.labels_) this.labels_.initialize(svgManager);

    this.ranges_ = [];

    var i;
    var count = this.threshold_.getRangeCount() - 1;

    if (this.inverted_)
        for (i = count; i >= 0; i--) this.addRange_(svgManager, i);
    else
        for (i = 0; i <= count; i++) this.addRange_(svgManager, i);

    this.calculateTickmarkSpace_();
    this.calculateLabelsSpace();

    this.isRangesInitialized_ = true;
};
anychart.controls.colorSwatch.ColorSwatchControl.prototype.addRange_ = function(svgManager, index) {
    var range = new anychart.controls.colorSwatch.RangeSprite(svgManager);
    range.setStyle(this.rangeItemBackground_);
    range.setColorContainer(this.threshold_.getRangeColor(index));
    range.setPoints(this.threshold_.getRangePoints(index));

    this.ranges_.push(range);
};
/**
 * @private
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateTickmarkSpace_ = function() {
    this.tickmarksSpace_ = 0;
    this.normalTickmarksSpace_ = 0;

    if (!this.tickmark_ || !this.tickmark_.isEnabled()) return;

    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    switch (this.labelsPosition_) {
        case positionType.COMBINED:
            this.tickmarksSpace_ = this.tickmarksSize_ * 2;
            this.normalTickmarksSpace_ = this.tickmarksSize_;
            break;
        case positionType.NORMAL:
            this.tickmarksSpace_ = this.tickmarksSize_;
            this.normalTickmarksSpace_ = this.tickmarksSize_;
            break;
        case positionType.OPPOSITE:
            this.tickmarksSpace_ = this.tickmarksSize_;
            break;
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.controls.colorSwatch.ColorSwatchControl.prototype.draw = function() {
    goog.base(this, 'draw');
    this.updateRanges_(true);
    this.sprite_.appendSprite(this.contentSprite_);
};
/**
 * Draw color swatch label.
 * @private
 * @param {anychart.utils.geom.Point} position
 * @param {anychart.utils.geom.Rectangle} rangeBounds
 * @param {int} currentIndex
 * @param {int} lastIndex
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.drawLabel_ = function(position, rangeBounds, currentIndex, lastIndex) {
    this.setLabelPosition(position, rangeBounds, currentIndex, lastIndex);
    if (!this.checkOverlap_(currentIndex, position, lastIndex))
        this.labels_.drawLabel(this.contentSprite_, position, currentIndex);

};
/**
 * Draw color swatch label.
 * @private
 * @param {anychart.utils.geom.Rectangle} rangeBounds
 * @param {int} currentIndex
 * @param {int} lastIndex
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.drawTickmark_ = function(rangeBounds, currentIndex, lastIndex) {
    var path = this.sprite_.getSVGManager().createPath();
    path.setAttribute('style', this.tickmark_.getSVGStroke(this.sprite_.getSVGManager(), rangeBounds));
    path.setAttribute('d', this.getTickmarkPathData(rangeBounds, currentIndex, lastIndex));
    this.contentSprite_.appendChild(path);
};
/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {int} index
 * @param {int} lastIndex
 * @return {String}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.getTickmarkPathData = goog.abstractMethod;
/**
 * @param {anychart.utils.geom.Point} position
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {int} index
 * @param {int} lastIndex
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.setLabelPosition = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
anychart.controls.colorSwatch.ColorSwatchControl.prototype.resize = function() {
    goog.base(this, 'resize');
    this.updateRanges_();
};
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**
 * @private
 * Resize ranges
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.updateRanges_ = function(opt_needDraw) {
    if (!this.threshold_) return;
    this.contentSprite_.clear();
    var position = new anychart.utils.geom.Point();
    var lastIndex = 0;

    this.onBeforeDrawRanges();

    var rangeCount = this.ranges_.length;
    for (var i = 0; i < rangeCount; i++) {
        this.setRangeBounds(this.ranges_[i].getBounds(), i);
        if (opt_needDraw) this.ranges_[i].draw();
        else this.ranges_[i].update();

        if (i % this.interval_ == 0) {
            if (this.tickmark_)
                this.drawTickmark_(this.ranges_[i].getBounds(), i, lastIndex);

            if (this.labels_ && this.labels_.isEnabled())
                this.drawLabel_(position, this.ranges_[i].getBounds(), i, lastIndex);

            this.sprite_.appendSprite(this.ranges_[i]);
        }

        lastIndex++;
    }

    if (!this.tickmarkOnRangeCenter_ && rangeCount % this.interval_ == 0) {
        if (this.tickmark_ && this.tickmark_.isEnabled())
            this.drawTickmark_(this.ranges_[rangeCount - 1].getBounds(), rangeCount, lastIndex);

        if (this.labels_ && this.labels_.isEnabled())
            this.drawLabel_(position, this.ranges_[rangeCount - 1].getBounds(), rangeCount, lastIndex);
    }
};
//------------------------------------------------------------------------------
//                  Labels overlap.
//------------------------------------------------------------------------------
/**
 * @private
 * @param {int} index
 * @param {anychart.utils.geom.Point} pos
 * @param {int} lastIndex
 * @return {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.checkOverlap_ = function(index, pos, lastIndex) {
    var labelRect = this.labels_.getLabelsBounds(index).clone();
    labelRect.x = pos.x;
    labelRect.y = pos.y;

    if (this.labelsPosition_ == anychart.controls.colorSwatch.ColorSwatchLabelsPosition.COMBINED) {
        if (lastIndex % 2 == 0) {
            if (!this.lastEvenLabelRect_) this.lastEvenLabelRect_ = labelRect;
            else if (this.lastEvenLabelRect_.intersects(labelRect)) return true;
            this.lastEvenLabelRect_ = labelRect;
        } else {
            if (!this.lastOddLabelRect_)
                this.lastOddLabelRect_ = labelRect;
            else if (this.lastOddLabelRect_.intersects(labelRect)) return true;
            this.lastOddLabelRect_ = labelRect;
        }
    } else {
        if (!this.lastLabelRect_)
            this.lastLabelRect_ = labelRect;
        else if (this.lastLabelRect_.intersects(labelRect)) return true;
        this.lastLabelRect_ = labelRect;
    }
    return false;
};
//------------------------------------------------------------------------------
//                      Ranges bounds.
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.onBeforeDrawRanges = function() {
    this.left_ = this.contentBounds_.getLeft() + this.leftShift_;
    this.top_ = this.contentBounds_.getTop() + this.topShift_;
};
/**
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {int} index
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.setRangeBounds = function(bounds, index) {
    bounds.width = this.rangeItemWidth_;
    bounds.height = this.rangeItemHeight_;
};
//------------------------------------------------------------------------------
//                       Position calculating.
//------------------------------------------------------------------------------
/**
 * Просчитать расстояния под label-ы
 * @protected
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateLabelsSpace = function() {
};

/**
 * Подситать оптимальную ширину range item
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateOptimalRangeItemWidth = function() {
    return NaN;
};

/**
 * Подсчтитать ширину контрола исходя из range item width
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateWidthBasedOnRangeItemWidth = function() {
    return NaN;
};

/**
 * Подсчитать ширину range item исходя из ширины контрола
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateRangeItemWidthBasedOnWidth = function() {
    return NaN;
};

/**
 * Подситать оптимальную высоту range item
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateOptimalRangeItemHeight = function() {
    return NaN;
};

/**
 * Подсчтитать высоту контрола исходя из range item height
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateHeightBasedOnRangeItemHeight = function() {
    return NaN;
};

/**
 * Подсчитать высоту range item исходя из высоты контрола
 * @protected
 * @return {Number}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.calculateRangeItemHeightBasedOnHeight = function() {
    return NaN;
};
/**
 * @private
 * @param {int} index
 * @return {Boolean}
 */
anychart.controls.colorSwatch.ColorSwatchControl.prototype.isEvent_ = function(index) {
    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    return (this.labelsPosition_ == positionType.NORMAL) || ((this.labelsPosition_ == positionType.COMBINED && (index % 2 == 0)));
};
//------------------------------------------------------------------------------
//
//                     HorizontalColorSwatch class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.colorSwatch.ColorSwatchControl}
 */
anychart.controls.colorSwatch.HorizontalColorSwatch = function() {
    anychart.controls.colorSwatch.ColorSwatchControl.call(this);
    this.defaultRangeItemWidth_ = 20;
    this.defaultRangeItemHeight_ = 10;
};
goog.inherits(anychart.controls.colorSwatch.HorizontalColorSwatch,
    anychart.controls.colorSwatch.ColorSwatchControl);
//------------------------------------------------------------------------------
//                          OnBeforeDraw.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.onBeforeDrawRanges = function() {
    goog.base(this, 'onBeforeDrawRanges');

    this.top_ += this.normalLabelsSpace_ + this.normalTickmarksSpace_;

    if (this.labels_) {
        if (this.tickmarkOnRangeCenter_) {
            var w = this.labels_.getFirstBounds().width;
            if (w > this.rangeItemWidth_)
                this.left_ += (w - this.rangeItemWidth_) / 2;
        } else this.left_ += this.labels_.getFirstBounds().width / 2;
    }
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.setRangeBounds = function(bounds, index) {
    goog.base(this, 'setRangeBounds', bounds, index);
    bounds.x = this.left_ + this.rangeItemWidth_ * index;
    bounds.y = this.top_;
};
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {int} index
 * @param {Number} labelWidth
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.getX_ = function(bounds, index, labelWidth) {
    if (this.tickmarkOnRangeCenter_) return bounds.x + (bounds.width - labelWidth) / 2;
    return ((index == this.threshold_.getRangeCount()) ? bounds.getRight() : bounds.getLeft()) - labelWidth / 2;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateOptimalRangeItemHeight = function() {
    return this.defaultRangeItemHeight_;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateRangeItemHeightBasedOnHeight = function() {
    return this.getContentHeight() - this.tickmarksSpace_ - this.labelsSpace_;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateHeightBasedOnRangeItemHeight = function() {
    return this.rangeItemHeight_ + this.tickmarksSpace_ + this.labelsSpace_;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateOptimalRangeItemWidth = function() {
    if (!this.labels_ || !this.labels_.isEnabled()) return this.defaultRangeItemWidth_;
    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    switch (this.labelsPosition_) {
        case positionType.COMBINED:
            if ((!this.tickmarkOnRangeCenter_ && this.threshold_.getRangeCount() > 1)
                ||
                (this.tickmarkOnRangeCenter_ && this.threshold_.getRangeCount() > 2))
                return .25 * this.labels_.maxSummIAndIPlus2LabelWidth_;
            return this.defaultRangeItemWidth_;

        case positionType.NORMAL:
        case positionType.OPPOSITE:
            return this.labels_.maxSummIAndIPlus1LabelWidth_ / 2;
    }
    return NaN;
};
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateWidthBasedOnRangeItemWidth = function() {
    var res = this.rangeItemWidth_ * this.threshold_.getRangeCount();
    if (this.labels_ && this.labels_.isEnabled()) {
        if (this.tickmarkOnRangeCenter_) {
            if (this.labels_.getFirstBounds().width > this.rangeItemWidth_)
                res += (this.labels_.getFirstBounds().width - this.rangeItemWidth_) / 2;
            if (this.labels_.getLastBounds().width > this.rangeItemWidth_)
                res += (this.labels_.getLastBounds().width - this.rangeItemWidth_) / 2;
        } else {
            res += this.labels_.getFirstBounds().width / 2 + this.labels_.getLastBounds().width / 2;
        }
    }
    return res;
};
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateRangeItemWidthBasedOnWidth = function() {
    var space = this.getContentWidth();
    if (this.labels_ && this.labels_.isEnabled()) {
        if (this.tickmarkOnRangeCenter_) {

            var canCalc = false;

            if (this.labelsPosition_ == anychart.controls.colorSwatch.ColorSwatchLabelsPosition.COMBINED) {
                canCalc = this.labels_.getSummEvenLabelWidth() <= space &&
                    this.labels_.getSummOddLabelWidth() <= space;
            } else {
                canCalc = (this.labels_.getSummEvenLabelWidth() + this.labels_.getSummOddLabelWidth()) <= space;
            }

            var l1 = this.labels_.getFirstBounds().width;
            var l2 = this.labels_.getLastBounds().width;
            var n = this.threshold_.getRangeCount();

            if (canCalc) {

                var w1 = space / this.threshold_.getRangeCount();
                if (w1 >= l1 && w1 >= l2) return w1;

                var w2 = (2 * space - l1) / (2 * n - 1);
                if (w2 <= l1 && w2 >= l2) return w2;

                var w3 = (2 * space - l2) / (2 * n - 1);
                if (w3 >= l1 && w3 <= l2) return w3;

                var w4 = (2 * space - l1 - l2) / (2 * (n - 1));
                if (w4 <= l1 && w4 <= l2) return w4;

            }

            return (2 * space - l1 - l2) / (2 * (n - 1));

        } else space -= this.labels_.getFirstBounds().width / 2 + this.labels_.getLastBounds().width / 2;

    }
    return space / this.threshold_.getRangeCount();
};
//------------------------------------------------------------------------------
//                        Labels position.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.setLabelPosition = function(position, bounds, index, lastIndex) {
    var labelBounds = this.labels_.getLabelsBounds(index);

    position.x = this.getX_(bounds, index, labelBounds.width);
    var space = this.labels_.getPadding() + this.normalTickmarksSpace_;

    if (this.isEvent_(lastIndex))
        position.y = this.top_ - space - labelBounds.height;
    else
        position.y = this.top_ + this.rangeItemHeight_ + space;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.calculateLabelsSpace = function() {
    this.labelsSpace_ = 0;
    this.normalLabelsSpace_ = 0;

    if (!this.labels_ || !this.labels_.isEnabled()) return;

    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    switch (this.labelsPosition_) {
        case positionType.COMBINED:
            this.normalLabelsSpace_ = this.labels_.getMaxEvenLabelHeight() + this.labels_.getPadding();
            this.labelsSpace_ = this.normalLabelsSpace_;
            if ((this.threshold_.getRangeCount() > 1 && this.tickmarkOnRangeCenter_) ||
                (this.threshold_.getRangeCount() > 0 && !this.tickmarkOnRangeCenter_))
                this.labelsSpace_ += this.labels_.getMaxOddLabelHeight() + this.labels_.getPadding();
            break;
        case positionType.NORMAL:
            this.labelsSpace_ = Math.max(this.labels_.getMaxEvenLabelHeight(), this.labels_.getMaxOddLabelHeight()) + this.labels_.getPadding();
            this.normalLabelsSpace_ = this.labelsSpace_;
            break;

        case positionType.OPPOSITE:
            this.labelsSpace_ = Math.max(this.labels_.getMaxEvenLabelHeight(), this.labels_.getMaxOddLabelHeight());
            break;
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.controls.colorSwatch.HorizontalColorSwatch.prototype.getTickmarkPathData = function(bounds, index, lastIndex) {
    var x = this.getX_(bounds, index, 0);
    var top;
    if (this.isEvent_(lastIndex))
        top = this.top_ - this.tickmarksSize_;
    else
        top = this.top_ + this.rangeItemHeight_;

    var pathData = this.sprite_.getSVGManager().pMove(x, top);
    pathData += this.sprite_.getSVGManager().pLine(x, top + this.tickmarksSize_);
    return pathData;
};
//------------------------------------------------------------------------------
//
//                     VerticalColorSwatch class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.colorSwatch.ColorSwatchControl}
 */
anychart.controls.colorSwatch.VerticalColorSwatch = function() {
    anychart.controls.colorSwatch.ColorSwatchControl.call(this);
    this.defaultRangeItemWidth_ = 10;
    this.defaultRangeItemHeight_ = 20;
};
goog.inherits(anychart.controls.colorSwatch.VerticalColorSwatch,
    anychart.controls.colorSwatch.ColorSwatchControl);
//------------------------------------------------------------------------------
//                          OnBeforeDraw.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.onBeforeDrawRanges = function() {
    goog.base(this, 'onBeforeDrawRanges');
    this.left_ += this.normalLabelsSpace_ + this.normalTickmarksSpace_;
    if (this.labels_) {
        if (this.tickmarkOnRangeCenter_) {
            var w = this.labels_.getFirstBounds().height;
            if (w > this.rangeItemHeight_)
                this.top_ += (w - this.rangeItemHeight_) / 2;
        } else this.top_ += this.labels_.getFirstBounds().height / 2;

    }

};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.setRangeBounds = function(bounds, index) {
    goog.base(this, 'setRangeBounds', bounds, index);
    bounds.x = this.left_;
    bounds.y = this.top_ + this.rangeItemHeight_ * index;
};
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @param {int} index
 * @param {Number} labelHeight
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.getY_ = function(bounds, index, labelHeight) {
    if (this.tickmarkOnRangeCenter_) return bounds.y + (bounds.height - labelHeight) / 2;
    return ((index == this.threshold_.getRangeCount()) ? bounds.getBottom() : bounds.getTop()) - labelHeight / 2;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateOptimalRangeItemHeight = function() {
    if (!this.labels_ || !this.labels_.isEnabled()) return this.defaultRangeItemHeight_;
    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    switch (this.labelsPosition_) {
        case positionType.COMBINED:
            if ((!this.tickmarkOnRangeCenter_ && this.threshold_.getRangeCount() > 1)
                ||
                (this.tickmarkOnRangeCenter_ && this.threshold_.getRangeCount() > 2))
                return .25 * this.labels_.getMaxSummIAndIPlus2LabelHeight();
            return this.defaultRangeItemHeight_;

        case positionType.NORMAL:
        case positionType.OPPOSITE:
            return this.labels_.getMaxSummIAndIPlus2LabelHeight() / 2;
    }

    return NaN;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateRangeItemHeightBasedOnHeight = function() {
    var res = this.rangeItemHeight_ * this.threshold_.getRangeCount();
    if (this.labels_ && this.labels_.isEnabled()) {
        if (this.tickmarkOnRangeCenter_) {
            if (this.labels_.getFirstBounds().height > this.rangeItemHeight_)
                res += (this.labels_.getFirstBounds().height - this.rangeItemHeight_) / 2;
            if (this.labels_.getLastBounds().height > this.rangeItemHeight_)
                res += (this.labels_.getLastBounds().height - this.rangeItemHeight_) / 2;
        } else {
            res += this.labels_.getFirstBounds().height / 2 + this.labels_.getLastBounds().height / 2;
        }
    }
    return res;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateHeightBasedOnRangeItemHeight = function() {
    var space = this.getContentHeight();
    if (this.labels_ && this.labels_.isEnabled()) {
        if (this.tickmarkOnRangeCenter_) {

            var canCalc = false;

            if (this.labelsPosition_ == anychart.controls.colorSwatch.ColorSwatchLabelsPosition.COMBINED) {
                canCalc = this.labels_.getSummEvenLabelHeight() <= space &&
                    this.labels_.getSummOddLabelHeight() <= space;
            } else {
                canCalc = (this.labels_.getSummEvenLabelHeight() + this.labels_.getSummOddLabelHeight()) <= space;
            }

            var l1 = this.labels_.getFirstBounds().height;
            var l2 = this.labels_.getLastBounds().height;
            var n = this.threshold_.getRangeCount();

            if (canCalc) {

                var w1 = space / this.threshold_.getRangeCount();
                if (w1 >= l1 && w1 >= l2) return w1;

                var w2 = (2 * space - l1) / (2 * n - 1);
                if (w2 <= l1 && w2 >= l2) return w2;

                var w3 = (2 * space - l2) / (2 * n - 1);
                if (w3 >= l1 && w3 <= l2) return w3;

                var w4 = (2 * space - l1 - l2) / (2 * (n - 1));
                if (w4 <= l1 && w4 <= l2) return w4;

            }

            return (2 * space - l1 - l2) / (2 * (n - 1));

        } else {
            space -= this.labels_.getFirstBounds().height / 2 + this.labels_.getLastBounds().height / 2;
        }
    }
    return space / this.threshold_.getRangeCount();
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateOptimalRangeItemWidth = function() {
    return this.defaultRangeItemWidth_;
};
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateWidthBasedOnRangeItemWidth = function() {
    return this.getContentWidth() - this.labelsSpace_ - this.tickmarksSpace_;
};
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateRangeItemWidthBasedOnWidth = function() {
    return this.rangeItemWidth_ + this.labelsSpace_ + this.tickmarksSpace_;

};
//------------------------------------------------------------------------------
//                        Labels position.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.setLabelPosition = function(position, bounds, index, lastIndex) {
    var labelBounds = this.labels_.getLabelBounds(index);
    position.y = this.getY_(bounds, index, labelBounds.height);
    var space = this.labels_.getPadding() + this.normalTickmarksSpace_;

    if (this.isEvent_(lastIndex))
        position.x = this.left_ - space - labelBounds.width;
    else
        position.x = this.left_ + this.rangeItemWidth_ + space;
};
/**
 * @inheritDoc
 */
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.calculateLabelsSpace = function() {
    this.labelsSpace_ = 0;
    this.normalLabelsSpace_ = 0;
    if (!this.labels_ || !this.labels_.isEnabled()) return;

    var positionType = anychart.controls.colorSwatch.ColorSwatchLabelsPosition;
    switch (this.labelsPosition_) {
        case positionType.COMBINED:
            this.normalLabelsSpace_ = this.labels_.getMaxEvenLabelWidth() + this.labels_.getPadding();
            this.labelsSpace_ = this.normalLabelsSpace_ + this.labels_.getMaxOddLabelWidth() + this.labels_.getPadding();
            break;

        case positionType.NORMAL:
            this.labelsSpace_ = Math.max(this.labels_.getMaxEvenLabelWidth(), this.labels_.getMaxOddLabelWidth()) + this.labels_.getPadding();
            this.normalLabelsSpace_ = this.labelsSpace_;
            break;
        case positionType.OPPOSITE:
            this.labelsSpace_ = Math.max(this.labels_.getMaxEvenLabelWidth(), this.labels_.getMaxOddLabelWidth()) + this.labels_.getPadding();
            break;
    }
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
anychart.controls.colorSwatch.VerticalColorSwatch.prototype.getTickmarkPathData = function(bounds, index, lastIndex) {
    var y = this.getY_(bounds, index, 0);
    var left;
    if (this.isEvent_(lastIndex))
        left = this.left_ - this.tickmarksSize_;
    else
        left = this.left_ + this.rangeItemWidth_;
    var pathData = this.sprite_.getSVGManager().pMove(left, y);
    pathData += this.sprite_.getSVGManager().pLine(left + this.tickmarksSize_, y);
    return pathData;
};
