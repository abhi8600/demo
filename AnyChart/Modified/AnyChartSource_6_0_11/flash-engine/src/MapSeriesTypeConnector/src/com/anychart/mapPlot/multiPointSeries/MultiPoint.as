package com.anychart.mapPlot.multiPointSeries {
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.data.GeoBasedPoint;
	
	import flash.geom.Point;
	
	public class MultiPoint extends GeoBasedPoint {
		
		protected var geoPoints:Array;
		protected var linearGeoPoints:Array;
		
		public function MultiPoint() {
			this.geoPoints = [];
			this.linearGeoPoints = [];
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			var ptsCnt:int = data.point.length();
			for (var i:int = 0;i<ptsCnt;i++) {
				if (data.point[i].@link != undefined) {
					this.geoPoints.push(String(data.point[i].@link));
				}else {
					var pt:Point = new Point();
					pt.x = this.deserializeLongitude(data.point[i]);
					pt.y = this.deserializeLatitude(data.point[i]);
					this.geoPoints.push(pt);
				}
			}
		}
		
		override public function applyProjection():void {
			var i:int;
			var currentPoint:Point;
			if (this.geoPoints.length > 10) {
				for (i = 0;i<this.geoPoints.length;i++) {
					var currentLinearPoint:Point = new Point();
					if (this.geoPoints[i] is String)
						this.geoPoints[i] = MapPlot(this.plot).getRegion(this.geoPoints[i]).geoCentroid.clone();
					
					currentPoint = this.geoPoints[i];
					
					space.linearize(currentPoint.x, currentPoint.y, currentLinearPoint);
					this.linearGeoPoints.push(currentLinearPoint);
				}
			}else {
				var nextPoint:Point;
				for (i = 0;i<(this.geoPoints.length - 1);i++) {
					if (this.geoPoints[i] is String)
						this.geoPoints[i] = MapPlot(this.plot).getRegion(this.geoPoints[i]).geoCentroid.clone();
					if (this.geoPoints[i+1] is String)
						this.geoPoints[i+1] = MapPlot(this.plot).getRegion(this.geoPoints[i+1]).geoCentroid.clone();
					currentPoint = this.geoPoints[i];
					nextPoint = this.geoPoints[i+1];

					space.linearizeLine(currentPoint, nextPoint, this.linearGeoPoints);
				}
			}
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.drawShape();
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawShape();
		}
		
		protected function drawShape():void {}

	}
}