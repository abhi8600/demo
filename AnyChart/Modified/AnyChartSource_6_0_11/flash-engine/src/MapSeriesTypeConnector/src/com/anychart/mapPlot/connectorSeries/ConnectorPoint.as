package com.anychart.mapPlot.connectorSeries {
	import com.anychart.mapPlot.multiPointSeries.MultiPoint;
	import com.anychart.styles.states.LineStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	internal final class ConnectorPoint extends MultiPoint {
		
		override protected function drawShape():void {
			var g:Graphics = this.container.graphics;
			var style:LineStyleState = LineStyleState(this.styleState);
			if (style.stroke == null) return;
			if (this.linearGeoPoints.length == 0) return;
			style.stroke.apply(g, null, this.color);
			this.drawConnectorLine(g);
			g.lineStyle();
		}
		
		private function drawConnectorLine(g:Graphics):void {
			var pixPt:Point = new Point();
			this.space.transformLinearized(this.linearGeoPoints[0], pixPt);
			g.moveTo(pixPt.x, pixPt.y);
			
			for (var i:int = 1;i<this.linearGeoPoints.length;i++) {
				this.space.transformLinearized(this.linearGeoPoints[i], pixPt);
				g.lineTo(pixPt.x, pixPt.y);
			}
			
			this.space.transformLinearized(this.linearGeoPoints[this.linearGeoPoints.length-1], pixPt);
			g.lineTo(pixPt.x, pixPt.y);
		}
	}
}