package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.axesPlot.data.RangeSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class RangeSplineAreaSeries extends RangeSeries {
		
		public var needCreateGradientRect:Boolean;
		public var gradientBounds:Rectangle;
		
		internal var container:Sprite;
		
		public function RangeSplineAreaSeries () {
			super();
			this.needCallingOnBeforeDraw = true;
			this.isSortable = false;
			this.isClusterizableOnCategorizedAxis = false;
			this.container = new Sprite();
		}
		
		override public function setAnimation(animation:Animation, manager:AnimationManager):void {
			manager.registerDisplayObject(this.container, this.plot.scrollableContainer, animation);
		}

		override public function onBeforeDraw():void {
			this.global.container.addChild(this.container);
			createControls();
			this.doGradientBounds();
		}
		
		override public function onBeforeResize():void {
			createControls();
			this.doGradientBounds();
		}
		
		private function getNextNonMissing(ptNum:int):uint {
			for (var i:uint = ptNum + 1; i < length; i++)
				if (!BasePoint(points[i]).isMissing) return i;
			return i;
		}

		private var length:int;
		private function createControls():void {
			length = this.points.length;
			var pt:RangeSplineAreaPoint; 
			var ptPrev:RangeSplineAreaPoint; 
			
			var numPoint:uint = getNextNonMissing(-1);
			var numPointPrev:uint = numPoint;
			pt = RangeSplineAreaPoint(this.points[numPoint]);
			pt.prev = null;
			pt.firstInit();
			ptPrev = pt;
			for (numPoint = getNextNonMissing(numPoint); numPoint < length; numPoint = getNextNonMissing(numPoint)) {
				pt = RangeSplineAreaPoint(this.points[numPoint]);

				if (numPoint - numPointPrev == 1) {
					ptPrev.next = pt;
					pt.prev = ptPrev;
				} else {
					ptPrev.next = null;
					pt.prev = null;
				}
				pt.firstInit();
					 
				ptPrev.initDelegates();
				ptPrev.createControlPoints();
				//ptPrev.initConrolPoints();
				numPointPrev = numPoint;
				ptPrev = pt;
			}
			ptPrev.initDelegates();
			ptPrev.createControlPoints();
			//ptPrev.initConrolPoints();
		}

		override public function createPoint():BasePoint {
			return new RangeSplineAreaPoint();
		}
		
		private var lt:Point;
		private var rb:Point;
		private var rect:Rectangle;
		
		private function doGradientBounds ():void {
			if (!this.needCreateGradientRect)
				return;
			this.lt = new Point (Number.MAX_VALUE,Number.MAX_VALUE);
			this.rb = new Point (-Number.MAX_VALUE,-Number.MAX_VALUE);	
			
			for (var j:int=0; j<this.points.length; j++){
				var point:RangeSplineAreaPoint = RangeSplineAreaPoint(this.points[j]);
				if (!point.isMissing){
					this.rect = point.getBound();
					this.lt.x = Math.min(lt.x,rect.x);
					this.lt.y = Math.min(lt.y,rect.y);
					this.rb.x = Math.max(rb.x,rect.x+rect.width);
					this.rb.y = Math.max(rb.y,rect.y+rect.height);
				}
			}
			this.gradientBounds = new Rectangle (lt.x,lt.y,rb.x-lt.x,rb.y-lt.y);
			//trace (this.gradientBounds.x,this.gradientBounds.y);
		} 
	}
}