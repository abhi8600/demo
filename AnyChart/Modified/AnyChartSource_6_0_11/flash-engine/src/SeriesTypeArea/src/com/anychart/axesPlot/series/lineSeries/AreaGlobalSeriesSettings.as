package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class AreaGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:SingleValueSeries;
			switch (seriesType) {
				case SeriesType.STEP_AREA_BACKWARD:
					series = new StepBackwardAreaSeries();
					break;
				case SeriesType.STEP_AREA_FORWARD:
					series = new StepForwardAreaSeries();
					break;
				case SeriesType.SPLINE_AREA:
					series = new SplineAreaSeries();
					break;
				case SeriesType.AREA:
				default:
					series = new AreaSeries();
					break;
			}
			series.type = seriesType;
			series.clusterKey = SeriesType.LINE;
			return series;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSettings():BaseSeriesSettings {
			return new LineSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return AreaStyleState;
		}
		
		override public function getSettingsNodeName():String {
			return "area_series";
		}
		
		override public function getStyleNodeName():String {
			return "area_style";
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { 
			return true;
		}
		
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			var color:uint = item.color;
			g.lineStyle(1, ColorUtils.getDarkColor(color), 1);
			g.beginFill(color, 1);
			this.doDrawIcon(seriesType, g, bounds);
			g.endFill();
			g.lineStyle();
		}
		
		protected function doDrawIcon(seriesType:uint, g:Graphics, bounds:Rectangle):void {
			switch (seriesType) {
				case SeriesType.AREA:
					this.drawAreaIcon(g, bounds);
					break;
				case SeriesType.STEP_AREA_FORWARD:
				case SeriesType.STEP_AREA_BACKWARD:
					this.drawStepAreaIcon(g, bounds);
					break;
				case SeriesType.SPLINE_AREA:
					this.drawSplineAreaIcon(g, bounds);
					break;
			}
			g.lineStyle();
			g.lineTo(bounds.right, bounds.bottom);
			g.lineTo(bounds.left, bounds.bottom);
		}
		
		private function drawAreaIcon(g:Graphics, bounds:Rectangle):void {
			var wStep:Number = bounds.width/4;
			var hStep:Number = bounds.height/4;
			g.moveTo(bounds.x, bounds.y + hStep*3);
			g.lineTo(bounds.x + wStep, bounds.y + hStep);
			g.lineTo(bounds.x + wStep*2, bounds.y + hStep*2);
			g.lineTo(bounds.right, bounds.y);
		}
		
		private function drawStepAreaIcon(g:Graphics, bounds:Rectangle):void {
			var hStep:Number = bounds.height/2;
			g.moveTo(bounds.x, bounds.y + hStep);
			g.lineTo(bounds.x + bounds.width/2, bounds.y + hStep);
			g.lineTo(bounds.x + bounds.width/2, bounds.y);
			g.lineTo(bounds.x + bounds.width, bounds.y);
		}
		
		private function drawSplineAreaIcon(g:Graphics, bounds:Rectangle):void {
			var hStep:Number = bounds.height/2;
			g.moveTo(bounds.x, bounds.y + hStep);
			g.curveTo(bounds.x + bounds.width*.3, bounds.y+hStep, bounds.x + bounds.width/2, bounds.y + hStep/2);    
			g.curveTo(bounds.x + bounds.width*.7, bounds.y, bounds.x + bounds.width, bounds.y);
		}
		
		override public function seriesCanBeAnimated():Boolean { return true; }
		override public function pointsCanBeAnimated():Boolean { return false; }
	}
}