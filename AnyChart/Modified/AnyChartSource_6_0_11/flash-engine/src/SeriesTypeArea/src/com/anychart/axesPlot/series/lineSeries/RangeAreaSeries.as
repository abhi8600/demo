package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.axesPlot.data.RangeSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class RangeAreaSeries extends RangeSeries {
		
		public var needCreateGradientRect:Boolean;
		public var gradientBounds:Rectangle;
		
		internal var container:Sprite;
		
		public function RangeAreaSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
			this.isSortable = false;
			this.isClusterizableOnCategorizedAxis = false;
			this.needCreateGradientRect = false;
			this.container = new Sprite();
		}
		
		override public function createPoint():BasePoint {
			return new RangeAreaPoint();
		}
		
		override public function onBeforeDraw():void {
			this.global.container.addChild(this.container);
			this.doGradientBounds();
		}
		
		override public function onBeforeResize():void {
			this.doGradientBounds();
		} 
		
		override public function setAnimation(animation:Animation, manager:AnimationManager):void {
			manager.registerDisplayObject(this.container, this.plot.scrollableContainer, animation);
		}
		
		private function doGradientBounds():void {
			if (this.needCreateGradientRect){
				var lt:Point = new Point (Number.MAX_VALUE,Number.MAX_VALUE);
				var rb:Point = new Point (-Number.MAX_VALUE,-Number.MAX_VALUE);
				for (var j:int=0; j<this.points.length; j++){
					var point:RangeAreaPoint = RangeAreaPoint(this.points[j]);
					if (!point.isMissing){
						point.needCreateDrawingPoints = true;
						point.needInitializePixValues = true;
						if (point.needCalcPixValue)
							point.doInitializePixValues();
						if (point.isDrawable)
							point.doCreateDrawingPoints();
						for (var i:int=0; i<point.startDrawingPoints.length;i++){
							lt.x = Math.min(point.startDrawingPoints[i].x,lt.x);
							lt.y = Math.min(point.startDrawingPoints[i].y,lt.y);
							
							rb.x = Math.max(point.startDrawingPoints[i].x,rb.x);
							rb.y = Math.max(point.startDrawingPoints[i].y,rb.y);
						}
						for (i=0; i<point.endDrawingPoints.length;i++){
							lt.x = Math.min(point.endDrawingPoints[i].x,lt.x);
							lt.y = Math.min(point.endDrawingPoints[i].y,lt.y);
							
							rb.x = Math.max(point.endDrawingPoints[i].x,rb.x);
							rb.y = Math.max(point.endDrawingPoints[i].y,rb.y);
						}
					}
				}
				this.gradientBounds = new Rectangle (lt.x,lt.y,rb.x-lt.x,rb.y-lt.y);
			}	
		}
	}
}