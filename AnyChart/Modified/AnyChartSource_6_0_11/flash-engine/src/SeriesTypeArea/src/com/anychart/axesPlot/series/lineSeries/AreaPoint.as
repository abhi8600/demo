package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class AreaPoint extends LinePoint {
		
		public var bottomStart:Point;
		public var bottomEnd:Point;
		public var needCreateDrawingPoints:Boolean;
		public var needInitializePixValues :Boolean;
		
		public function AreaPoint() {
			super();
			this.needCreateDrawingPoints = true;
			this.needInitializePixValues = true;
		}
		
		override public function initialize():void {
			super.initialize();
			var s:AreaSeries = AreaSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		private function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}
		
		public function doInitializePixValues():void {
			this.initializePixValues();
			this.needInitializePixValues = false;
		}
		
		override protected function initializePixValues():void {
			if (!this.needInitializePixValues) return;
			super.initializePixValues();
			if (this.isLineDrawable && this.previousStackPoint == null) {
				var series:AreaSeries = AreaSeries(this.series);
				this.bottomStart = new Point();
				this.bottomEnd = new Point();
				
				series.valueAxis.transform(this, series.valueAxisStart, this.bottomStart);
				series.valueAxis.transform(this, series.valueAxisStart, this.bottomEnd);
				
				if (this.z) {
					this.z.transform(this.bottomStart);
					this.z.transform(this.bottomEnd);
				}
			}
		}
		
		public function doCreateDrawingPoints ():void {
			this.createDrawingPoints();
			this.needCreateDrawingPoints = false;
		}
				
		
		override protected function createDrawingPoints():void {
			if (!this.needCreateDrawingPoints) return;
			
			this.drawingPoints = [];
			var tmp:Point;
			var tmp1:Point;
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].pixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.pixValuePt.x)/2;
				tmp1.y = (tmp.y + this.pixValuePt.y)/2;
				if (this.previousStackPoint == null) {
					if (this.isHorizontal)
						this.bottomStart.y = tmp1.y;
					else 
						this.bottomStart.x = tmp1.x;
				}
				this.drawingPoints.push(tmp1);
			}else if (this.previousStackPoint == null) {
				if (this.isHorizontal)
					this.bottomStart.y = this.pixValuePt.y;
				else 
					this.bottomStart.x = this.pixValuePt.x;
			}
			
			this.drawingPoints.push(this.pixValuePt);
			
			if (this.hasNext) {
				this.series.points[this.index+1].initializePixValues();
				this.series.points[this.index+1].needCalcPixValue = false;
				tmp = this.series.points[this.index+1].pixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.pixValuePt.x)/2;
				tmp1.y = (tmp.y + this.pixValuePt.y)/2;
				this.drawingPoints.push(tmp1);
				
				if (this.previousStackPoint == null) {
					if (this.isHorizontal)
						this.bottomEnd.y = tmp1.y;
					else 
						this.bottomEnd.x = tmp1.x;
				}
			}else if (this.previousStackPoint == null) {
				if (this.isHorizontal)
					this.bottomEnd.y = this.pixValuePt.y;
				else 
					this.bottomEnd.x = this.pixValuePt.x;
			}	
			
		}
		
		override final protected function drawSegment(g:Graphics):void {
			
			var i:uint;
			var s:AreaStyleState = AreaStyleState(this.styleState);
			
			var gradientBounds:Rectangle = new Rectangle;
			if (AreaSeries(this.series).needCreateGradientRect)
				gradientBounds = AreaSeries(this.series).gradientBounds;
				
			if (s.stroke != null)
				s.stroke.apply(g, gradientBounds, this.color);
			if (s.fill != null)
				s.fill.begin(g, gradientBounds, this.color);
			g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
			for (i = 1;i<this.drawingPoints.length;i++) 
				g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
			g.lineStyle();
			
			if (this.previousStackPoint == null) {
				g.lineTo(this.bottomEnd.x, this.bottomEnd.y);
				g.lineTo(this.bottomStart.x, this.bottomStart.y);
			}else {
				AreaPoint(this.previousStackPoint).drawSegmentBackward(g);
			}
			g.endFill();
			
			if (s.hatchFill != null) {
				s.hatchFill.beginFill(g, this.hatchType, this.color);
				g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
				for (i = 1;i<this.drawingPoints.length;i++) 
					g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
				if (this.previousStackPoint == null) {
					g.lineTo(this.bottomEnd.x, this.bottomEnd.y);
					g.lineTo(this.bottomStart.x, this.bottomStart.y);
				}else {
					AreaPoint(this.previousStackPoint).drawSegmentBackward(g);
				}
				g.endFill();
			}
		}
		
		private function drawSegmentBackward(g:Graphics):void {
			if (this.drawingPoints != null)
				for (var i:int = this.drawingPoints.length-1;i>=0;i--)
					g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
		}
	}
}
