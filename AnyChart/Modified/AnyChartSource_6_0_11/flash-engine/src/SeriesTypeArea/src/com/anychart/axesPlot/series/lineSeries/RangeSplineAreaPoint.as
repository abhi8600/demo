package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.RangePoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class RangeSplineAreaPoint extends RangePoint {
		
		//this.start
		//this.end
		//this.x
		
		protected var xAxis:Axis;
		protected var yAxis:Axis;
		
		override public function initialize():void {
			super.initialize();
			var s:RangeSplineAreaSeries = RangeSplineAreaSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		override protected function addContainerToSeriesContainer():void {
			RangeSplineAreaSeries(this.series).container.addChild(this.container);
		}
		
		private function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}

		public function set prev(value:RangeSplineAreaPoint):void {
			startPoint.prev = value != null ? value.startPoint : null;
			endPoint.prev = value != null ? value.endPoint : null;
		}
		public function set next(value:RangeSplineAreaPoint):void {
			startPoint.next = value != null ? value.startPoint : null;
			endPoint.next = value != null ? value.endPoint : null;
		}

		private var startPoint:StandaloneSplinePoint = new StandaloneSplinePoint();
		private var endPoint:StandaloneSplinePoint = new StandaloneSplinePoint();
		
		public function RangeSplineAreaPoint() {
			super();
		}
		
		public function firstInit():void {
			xAxis = AxesPlotSeries(this.series).argumentAxis;
			yAxis = AxesPlotSeries(this.series).valueAxis;
			initValuePoint();
		}

		public function initValuePoint():void {
			startPoint.x = xAxis.simpleTransform(this, this.x);
			startPoint.y = yAxis.simpleTransform(this, this.start);
			endPoint.x = startPoint.x;
			endPoint.y = yAxis.simpleTransform(this, this.end);
			
			var pt1:Point = startPoint.clone();
			var pt2:Point = endPoint.clone();
			
			this.xAxis.rotateValue(pt1.x, pt1);
			this.yAxis.rotateValue(pt1.y, pt1);
			
			this.xAxis.rotateValue(pt2.x, pt2);
			this.yAxis.rotateValue(pt2.y, pt2);
			
			this.bounds.x = Math.min(pt1.x,pt2.x);
			this.bounds.y = Math.min(pt1.y,pt2.y);
			this.bounds.width = Math.max(pt1.x,pt2.x)-this.bounds.x;
			this.bounds.height = Math.max(pt1.y,pt2.y)-this.bounds.y;
		}

		public function initDelegates():void {
			startPoint.initDelegates();
			endPoint.initDelegates();
		}
		
		public function createControlPoints():void {
			startPoint.createControlPoints();
			endPoint.createControlPoints();
		}

		override protected function execDrawing():void {
			super.execDrawing();
			this.drawLine(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawLine(this.container.graphics);
		}
		
		protected function drawLine(g:Graphics):void {
			var state:RangeAreaStyleState = RangeAreaStyleState(this.styleState);
			var s:RangeSplineAreaSeries = RangeSplineAreaSeries(this.series);
			var rect:Rectangle = s.gradientBounds;
			
			if (state.stroke == null && state.fill == null && state.endStroke == null) return;
			if (state.fill != null) state.fill.begin(g, rect, this.color);
			this.execLineDrawing(g);
			if (state.stroke != null) {
				state.stroke.apply(g, null, this.color);
				startPoint.draw(g, xAxis, yAxis);
				g.lineStyle();
			}
			if (state.endStroke != null) {
				state.endStroke.apply(g, null, this.color);
				endPoint.drawBackward(g, xAxis, yAxis, false);
			}
		}
		
		protected function execLineDrawing(g:Graphics):void {
			var p:Point = new Point();
			g.lineStyle();
			startPoint.draw(g, xAxis, yAxis);

			xAxis.rotateValue(endPoint.rightPoint.x, p);
			yAxis.rotateValue(endPoint.rightPoint.y, p);
			g.lineTo(p.x, p.y);

			endPoint.drawBackward(g, xAxis, yAxis, true);

			xAxis.rotateValue(startPoint.leftPoint.x, p);
			yAxis.rotateValue(startPoint.leftPoint.y, p);
			g.lineTo(p.x, p.y);
			g.lineStyle();
			g.endFill();
		}
		
		private var bound:Rectangle;
		
		public function getBound():Rectangle {
			if (bound == null) bound = new Rectangle();
			var lS:Point = startPoint.leftPoint;
			var rS:Point = startPoint.rightPoint;
			var lE:Point = endPoint.leftPoint;
			var rE:Point = endPoint.rightPoint;
			
			var topLeft:Point = new Point(
				Math.min(lS.x, rS.x, lE.x, rE.x),
				Math.min(lS.y, rS.y, lE.y, rE.y));
			var bottomRight:Point = new Point(
				Math.max(lS.x, rS.x, lE.x, rE.x),
				Math.max(lS.y, rS.y, lE.y, rE.y));
			bound.x = topLeft.x;
			bound.y = topLeft.y;
			bound.width = bottomRight.x - topLeft.x;
			bound.height = bottomRight.y - topLeft.y;
			return bound;
		}

	}
}