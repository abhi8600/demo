package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class SplineAreaPoint extends SplinePoint {
		
		protected var bottomStart:Point;
		protected var bottomEnd:Point;

		override public function initialize():void {
			super.initialize();
			var s:SplineAreaSeries = SplineAreaSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		override public function initDelegates():void {
			super.initDelegates();
			//SplinePoint(this)
			this.bottomStart = new Point();
			this.bottomEnd = new Point();
			
			if (this.previousStackPoint == null) {
				if (hasPrev || hasNext) {
					var series:SplineAreaSeries = SplineAreaSeries(this.series);
					var startY:Number = series.valueAxis.simpleTransform(this, series.valueAxisStart);
					var endY:Number = series.valueAxis.simpleTransform(this, series.valueAxisStart);
					
					var startX:Number = !hasPrev ? this.point.x : (this.point.x + this.point.prev.x) * 0.5;
					var endX:Number = !hasNext ? this.point.x : (this.point.x + this.point.next.x) * 0.5;
					series.argumentAxis.rotateValue(startX, this.bottomStart);
					series.argumentAxis.rotateValue(endX, this.bottomEnd);
					series.valueAxis.rotateValue(startY, this.bottomStart);
					series.valueAxis.rotateValue(endY, this.bottomEnd);
				}
			}
		}
		
		private function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}

		override protected function drawLine(g:Graphics):void {
			if (!hasPrev && !hasNext) return;
			var state:AreaStyleState = AreaStyleState(this.styleState);
			var s:SplineAreaSeries = SplineAreaSeries(this.series)
			var gradientBounds:Rectangle = s.gradientBounds;
			if (state.stroke != null)
				state.stroke.apply(g,gradientBounds,this.color);
			if (state.fill != null)
				state.fill.begin(g,gradientBounds,this.color);
			this.execLineDrawing(g);
			g.endFill();
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				this.execLineDrawing(g);
				g.endFill();
			}
		}
		
		override protected function execLineDrawing(g:Graphics):void {
			super.execLineDrawing(g);

			g.lineStyle();
			if (this.previousStackPoint == null) {
				g.lineTo(this.bottomEnd.x, this.bottomEnd.y);
				g.lineTo(this.bottomStart.x, this.bottomStart.y);
			} else {
				SplineAreaPoint(this.previousStackPoint).execLineDrawingBackward(g);
			}
		}
		
		public var bound:Rectangle;
		
		
		public function getBound():Rectangle {
			
			if (bound == null) bound = new Rectangle();
			var l:Point = point.leftPoint;
			var r:Point = point.rightPoint;
			
			var topLeft:Point = new Point(
				Math.min(l.x, r.x),
				Math.min(l.y, r.y));
			var bottomRight:Point = new Point(
				Math.max(l.x, r.x),
				Math.max(l.y, r.y));
			
			bound.x = topLeft.x;
			bound.y = topLeft.y;
			bound.width = bottomRight.x - topLeft.x;
			bound.height = bottomRight.y - topLeft.y;
			return bound;
		}

	}
}