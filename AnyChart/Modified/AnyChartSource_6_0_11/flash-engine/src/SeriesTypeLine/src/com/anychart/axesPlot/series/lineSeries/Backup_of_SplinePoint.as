package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.utils.LineEquation;
	import com.anychart.utils.MathUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	internal final class Backup_of_SplinePoint extends StackablePoint {
		
		private var xTransformer:Axis;
		private var yTransformer:Axis;

		public var pointPrev:SplinePoint;
		public var pointNext:SplinePoint;

		public var pointCoord:Point = new Point();
		public var pointValue:Point = new Point();

		public var controlPointFirst:ControlPoint = new ControlPoint();
		public var controlPointSecond:ControlPoint = new ControlPoint();
		
		public function Backup_of_SplinePoint() {
			super();
		}
		
		public function firstInit():void {
			xTransformer = AxesPlotSeries(this.series).argumentAxis;
			yTransformer = AxesPlotSeries(this.series).valueAxis;

			this.pointValue.x = this.x;
			this.pointValue.y = this.stackValue;
			initValuePoint();
		}

		public function initValuePoint():void {
			this.pointCoord.x = xTransformer.simpleTransform(this, this.x);
			this.pointCoord.y = yTransformer.simpleTransform(this, this.stackValue);
		}

		private var createConrolPointsDelegate:Function;
		private var initConrolPointsDelegate:Function;
		
		public function initDelegates():void {
			createConrolPointsDelegate = getCreateConrolPointsDelegate();
			initConrolPointsDelegate = getInitConrolPointsDelegate();
		}
		
		private function getCreateConrolPointsDelegate():Function {
			if (pointPrev == null && pointNext == null) return createControlPointsByNothing;
			if (pointPrev == null && pointNext != null) return createControlPointsByPointNext;
			bezierPrev = new BezierCurveCubic();
			bezierPrev.pointFirst = pointPrev;
			bezierPrev.pointSecond = this;
			bezierPrev.controlFirst = pointPrev.controlPointSecond;
			bezierPrev.controlSecond = controlPointFirst;
			if (pointNext == null) return createControlPointsByPointPrev;
			return createControlPointsNormal;
		}
		
		private function getInitConrolPointsDelegate():Function {
			if (pointPrev == null && pointNext == null) return initControlPointsByNothing;
			if (pointPrev == null && pointNext != null) return initControlPointsByPointNext;
			if (pointNext == null) return initControlPointsByPointPrev;
			return initControlPointsNormal;
		}
		
		private function initControlPointsNormal():void {
 			this.controlPointFirst.valuePointX = xTransformer.simpleTransform(this, controlPointFirst.x);
			this.controlPointFirst.valuePointY = yTransformer.simpleTransform(this, controlPointFirst.y);
			this.controlPointSecond.valuePointX = xTransformer.simpleTransform(this, controlPointSecond.x);
			this.controlPointSecond.valuePointY = yTransformer.simpleTransform(this, controlPointSecond.y);
			createCurves();
		}

		private function initControlPointsByPointNext():void {
			this.controlPointFirst.valuePointX = this.pointCoord.x;
			this.controlPointFirst.valuePointY = this.pointCoord.y;
			this.controlPointSecond.valuePointX = xTransformer.simpleTransform(this, controlPointSecond.x);
			this.controlPointSecond.valuePointY = yTransformer.simpleTransform(this, controlPointSecond.y);
		}

		private function initControlPointsByPointPrev():void {
			this.controlPointFirst.valuePointX = xTransformer.simpleTransform(this, controlPointFirst.x);
			this.controlPointFirst.valuePointY = yTransformer.simpleTransform(this, controlPointFirst.y);
			this.controlPointSecond.valuePointX = this.pointCoord.x;
			this.controlPointSecond.valuePointY = this.pointCoord.y;
			createCurves();
		}

		private function initControlPointsByNothing():void {
			this.controlPointFirst.valuePointX = this.pointCoord.x;
			this.controlPointFirst.valuePointY = this.pointCoord.y;
			this.controlPointSecond.valuePointX = this.pointCoord.x;
			this.controlPointSecond.valuePointY = this.pointCoord.y;
		}

		private var bezierPrev:BezierCurveCubic;
		[ArrayElementType("com.anychart.axesPlot.series.lineSeries.BezierCurveQuadratic")]
		public var curvesLeft:Array = [];
		[ArrayElementType("com.anychart.axesPlot.series.lineSeries.BezierCurveQuadratic")]
		public var curvesRight:Array = [];

		private function createCurves():void {
			//bezierPrev.buildQuadraticBezier((pointPrev.valuePointX + valuePointX) * 0.5);
			if (bezierPrev == null || pointPrev == null) return;
			bezierPrev.buildQuadraticBezier((pointPrev.pointCoord.x + pointCoord.x) * 0.5);
			curvesLeft = bezierPrev.quadraticCurvesRight;
			pointPrev.curvesRight = bezierPrev.quadraticCurvesLeft;
		}

		//private static const KR:Number = 0.27;
		//private static const KR:Number = 0.35;
		private static const KR:Number = 0.4;
		//private static const KR:Number = 1;

		private static const GRAD_TO_RAD:Number = Math.PI / 180;
		private static const RAD_TO_GRAD:Number = 180 / Math.PI;
		private static const RAD_90:Number = 90 * GRAD_TO_RAD;
		private static const RAD_180:Number = 180 * GRAD_TO_RAD;
		private static const RAD_270:Number = 270 * GRAD_TO_RAD;
		private static const RAD_360:Number = 360 * GRAD_TO_RAD;
		
		private function createControlPointsNormal():void {
			var l1:Number;
			var l2:Number;
			var angle:Number;
			
			/*
			var x:Number = pointCoord.x;
			var y:Number = pointCoord.y;
			
			var prevY:Number = pointPrev.pointCoord.y;
			var prevX:Number = pointPrev.pointCoord.x;
			var nextY:Number = pointNext.pointCoord.y;
			var nextX:Number = pointNext.pointCoord.x;
			//*/
			var x:Number = pointValue.x;
			var y:Number = pointValue.y;

			var prevY:Number = pointPrev.pointValue.y;
			var prevX:Number = pointPrev.pointValue.x;
			var nextY:Number = pointNext.pointValue.y;
			var nextX:Number = pointNext.pointValue.x;

			var minY:Number = Math.min(prevY, nextY);
			var maxY:Number = Math.max(prevY, nextY);
			var minX:Number = Math.min(prevX, nextX);
			var maxX:Number = Math.max(prevX, nextX);
			
			if ((y > maxY || y < minY) && x > minX && x < maxX) {
				l1 = Math.abs(x - prevX);
				l2 = Math.abs(x - nextX);
				angle = RAD_270;
			} else if ((x > maxX || x < minX) && y > minY && y < maxY) {
				l1 = Math.abs(y - prevY);
				l2 = Math.abs(y - nextY);
				angle = 0;
			} else if (y > minY && y < maxY && x > minX && x < maxX) {
				var line:LineEquation = LineEquation.getLineFrom2Points(pointPrev.pointValue, pointNext.pointValue);
				var lineParallel:LineEquation = line.getParallelLine(pointValue);
				angle = Math.atan2(prevX - nextX, prevY - nextY);
				if (line.isLeftPoint(pointValue) == (maxY == nextY)) {
					l1 = MathUtils.getDistance(pointValue, lineParallel.getPointByX(prevX));
					l2 = MathUtils.getDistance(pointValue, lineParallel.getPointByY(nextY));
				} else {
					l1 = MathUtils.getDistance(pointValue, lineParallel.getPointByY(prevY));
					l2 = MathUtils.getDistance(pointValue, lineParallel.getPointByX(nextX));
				}
			} else {
				angle = Math.atan2(prevX - nextX, prevY - nextY);
				var l:LineEquation = LineEquation.getLineFrom2Points(pointPrev.pointValue, pointNext.pointValue);
				var d:Number = l.getDistance(pointValue);
				d *= d;
				l1 = MathUtils.getDistance(pointPrev.pointValue, pointValue);
				l1 = Math.sqrt(l1 * l1 - d);

				l2 = MathUtils.getDistance(pointNext.pointValue, pointValue);
				l2 = Math.sqrt(l2 * l2 - d);
				
				/*/////////////////
				// TO DO !!
				if (MathUtils.getDistance(pointPrev.pointValue, pointNext.pointValue) < l1 + l2) {
					
				}
				//*///////////////
			}
			
			l1 *= KR;
			l2 *= KR;

			controlPointFirst.x = x + l1 * Math.sin(angle);
			controlPointFirst.y = y + l1 * Math.cos(angle);
			controlPointSecond.x = x + l2 * Math.sin(angle + RAD_180);
			controlPointSecond.y = y + l2 * Math.cos(angle + RAD_180);
		}

		private function createControlPointsByPointNext():void {
			var x:Number = pointValue.x;
			var y:Number = pointValue.y;
			controlPointFirst.x = x;
			controlPointFirst.y = y;
			controlPointSecond.x = x + (pointNext.pointValue.x - x) * KR;
			controlPointSecond.y = y;
		}

		private function createControlPointsByPointPrev():void {
			var x:Number = pointValue.x;
			var y:Number = pointValue.y;
			controlPointFirst.x = x - (x - pointPrev.pointValue.x) * KR;
			controlPointFirst.y = y;
			controlPointSecond.x = x;
			controlPointSecond.y = y;
		}

		private function createControlPointsByNothing():void {
			var x:Number = pointValue.x;
			var y:Number = pointValue.y;
			controlPointFirst.x = x;
			controlPointFirst.y = y;
			controlPointSecond = controlPointFirst;
		}
		
		public function createControlPoints():void {
			createConrolPointsDelegate();
			createCurves();
		}
		
		public function initConrolPoints():void {
			initConrolPointsDelegate();
		}

		//value: this.stackValue
		//argument: this.x
		//transofrmation: 
		/*
			var pt:Point = new Point();
			AxesPlotSeries(this.series).valueAxis.transform(this, this.stackValue, pt);
			AxesPlotSeries(this.series).argumentAxis.transform(this, this.y, pt);
		*/
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.drawLine(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawLine(this.container.graphics);
		}
		
		private function drawLine(g:Graphics):void {
			//this.pointCoord
			var p1:Point = new Point();
			var p2:Point = new Point();

			xTransformer.rotateValue(pointCoord.x, p1);
			yTransformer.rotateValue(pointCoord.y, p1);
			/*
			if (pointNext != null) {
				g.endFill();
				xTransformer.rotateValue(pointNext.pointCoord.x, p2);
				yTransformer.rotateValue(pointNext.pointCoord.y, p2);
				g.lineStyle(1, 0x008800);
				g.moveTo(p1.x, p1.y);
				g.lineTo(p2.x, p2.y);
			}
			//*/
			
			xTransformer.rotateValue(controlPointFirst.valuePointX, p2);
			yTransformer.rotateValue(controlPointFirst.valuePointY, p2);
			g.lineStyle(1, 0x880000);
			g.moveTo(p1.x, p1.y);
			g.lineTo(p2.x, p2.y);
			g.drawCircle(p2.x, p2.y, 2);

			xTransformer.rotateValue(controlPointSecond.valuePointX, p2);
			yTransformer.rotateValue(controlPointSecond.valuePointY, p2);
			g.lineStyle(1, 0xFF8800);
			g.moveTo(p1.x, p1.y);
			g.lineTo(p2.x, p2.y);
			g.drawCircle(p2.x, p2.y, 2);
			
			
			g.lineStyle(1, 0x000088);
			//g.beginFill(0x00FFFF);
			g.drawCircle(p1.x, p1.y, 3);
			g.endFill();
			//return;

			//trace(curvesLeft,"<-- LEFT | RIGHT -->", curvesRight);
			
			var pt:Point = new Point();
			var pt1:Point = new Point();
			//xTransformer.rotateValue(
			
			var state:LineStyleState = LineStyleState(this.styleState);
			state.stroke.apply(g,null,this.color);

			var i:uint;
			var curve:BezierCurveQuadratic;
			var r:Number = 3;
			var ln:uint = curvesLeft.length;
			
//g.lineStyle(1, 0xFF0000);
			if (ln > 1) {
				curve = BezierCurveQuadratic(curvesLeft[0]);
//g.drawCircle(curve.start.x, curve.start.y, r);
				xTransformer.rotateValue(curve.start.x, pt);
				yTransformer.rotateValue(curve.start.y, pt);
				
				g.moveTo(pt.x, pt.y);
				for (i = 0; i < ln; i++) {
					curve = BezierCurveQuadratic(curvesLeft[i]);

					xTransformer.rotateValue(curve.control.x, pt);
					yTransformer.rotateValue(curve.control.y, pt);
	
					xTransformer.rotateValue(curve.end.x, pt1);
					yTransformer.rotateValue(curve.end.y, pt1);

					g.curveTo(pt.x, pt.y, pt1.x, pt1.y);
//g.drawCircle(curve.end.x, curve.end.y, r);
//g.moveTo(curve.end.x, curve.end.y);
				}
			}
//g.lineStyle(1, 0x0000FF);
			ln = curvesRight.length;
			if (ln > 1) {
				curve = BezierCurveQuadratic(curvesRight[0]);
//g.drawCircle(curve.start.x, curve.start.y, r);
				xTransformer.rotateValue(curve.start.x, pt);
				yTransformer.rotateValue(curve.start.y, pt);

				g.moveTo(pt.x, pt.y);
				for (i = 0; i < ln; i++) {
					curve = BezierCurveQuadratic(curvesRight[i]);
					xTransformer.rotateValue(curve.control.x, pt);
					yTransformer.rotateValue(curve.control.y, pt);
	
					xTransformer.rotateValue(curve.end.x, pt1);
					yTransformer.rotateValue(curve.end.y, pt1);

					g.curveTo(pt.x, pt.y, pt1.x, pt1.y);
//g.drawCircle(curve.end.x, curve.end.y, r);
//g.moveTo(curve.end.x, curve.end.y);
				}
			}

			g.lineStyle(0,0);

			/*
			var r:Number = 4;

			g.lineStyle(1, 0xFF0000);
			g.drawCircle(controlPointFirst.valuePointX, controlPointFirst.valuePointY, r);
			g.moveTo(controlPointFirst.valuePointX, controlPointFirst.valuePointY);
			g.lineTo(valuePointX, valuePointY);
			
			g.lineStyle(1, 0x0000FF);
			g.drawCircle(controlPointSecond.valuePointX, controlPointSecond.valuePointY, r);
			g.moveTo(controlPointSecond.valuePointX, controlPointSecond.valuePointY);
			g.lineTo(valuePointX, valuePointY);
			//*/
		}
		
	}
}
