package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.selector.HitTestUtil;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.styles.states.LineStyleState;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class SplinePoint extends StackablePoint {
		
		private var xAxis:Axis;
		private var yAxis:Axis;
		
		override public function destroy():void {
			super.destroy();
			this.xAxis = null;
			this.yAxis = null;
			this.point = null;
		}

		public function set prev(value:SplinePoint):void { point.prev = value != null ? value.point : null; }
		public function set next(value:SplinePoint):void { point.next = value != null ? value.point : null; }
		public function get hasPrev():Boolean { return point.prev != null; }
		public function get hasNext():Boolean { return point.next != null; }

		protected var point:StandaloneSplinePoint = new StandaloneSplinePoint();
		
		override protected function addContainerToSeriesContainer():void {
			SplineSeries(this.series).container.addChild(this.container);
		}
		
		public function firstInit():void {
			xAxis = AxesPlotSeries(this.series).argumentAxis;
			yAxis = AxesPlotSeries(this.series).valueAxis;
			initValuePoint();
		}

		public function initValuePoint():void {
			point.x = xAxis.simpleTransform(this, this.x);
			point.y = yAxis.simpleTransform(this, this.stackValue);
		}

		public function initDelegates():void {
			point.initDelegates();
		}
		
		public function createControlPoints():void {
			point.createControlPoints();
		}

		override protected function execDrawing():void {
			super.execDrawing();
			
			var valuePt:Point = new Point();
			xAxis.rotateValue(this.point.x, valuePt);
			yAxis.rotateValue(this.point.y, valuePt);
			
			this.bounds.x = valuePt.x;
			this.bounds.y = valuePt.y;
			
			valuePt = null;
			
			this.drawLine(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawLine(this.container.graphics);
		}
		
		protected function drawLine(g:Graphics):void {
			//drawControlLines();
			var state:LineStyleState = LineStyleState(this.styleState);
			if (state.stroke == null) return;
			state.stroke.apply(g,null,this.color);
			this.execLineDrawing(g);
		}
		
		protected function execLineDrawing(g:Graphics):void {
			point.draw(g, xAxis, yAxis);
		}
		
		protected function execLineDrawingBackward(g:Graphics):void {
			point.drawBackward(g, xAxis, yAxis,true);
		}
		
		private function hitTestMarker(area:Rectangle, selectedArea:DisplayObject):Boolean {
			var tmp:Shape = new Shape();
			tmp.graphics.beginFill(0xFF0000, 1);
			tmp.graphics.drawRect(0,0, area.width, area.height);
			tmp.x = area.x;
			tmp.y = area.y;
			if (this.container != null && this.container.parent != null)
				this.container.parent.addChild(tmp);
			
			var res:Boolean = HitTestUtil.hitTest(this.markerSprite, tmp);
			
			if (this.container != null && this.container.parent != null)
				this.container.parent.removeChild(tmp);
			tmp = null;
			return res;
		}
		
		override public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			if (selector.selectLineOnlyMarkers)
				return this.hitTestMarker(area, selectedArea);
			return super.hitTestSelection(area, selectedArea, selector);
		}
	}
}
