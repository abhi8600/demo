package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.styles.states.LineStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class LineGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
	
		public var shiftStepLine:Boolean;
		
		public function LineGlobalSeriesSettings() {
			super();
			this.shiftStepLine = false;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@shift_step_line != undefined) this.shiftStepLine = SerializerBase.getBoolean(data.@shift_step_line);
		}
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:SingleValueSeries;
			switch (seriesType) {
				case SeriesType.STEP_LINE_BACKWARD:
					series = new StepBackwardLineSeries();
					break;
				case SeriesType.STEP_LINE_FORWARD:
					series = new StepForwardLineSeries();
					break;
				case SeriesType.SPLINE:
					series = new SplineSeries();
					break;
				case SeriesType.LINE:
				default:
					series = new LineSeries();
					break;
			}
			series.type = seriesType;
			series.clusterKey = SeriesType.LINE;
			return series;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSettings():BaseSeriesSettings {
			return new LineSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return LineStyleState;
		}
		
		override public function getSettingsNodeName():String {
			return "line_series";
		}
		
		override public function getStyleNodeName():String {
			return "line_style";
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { return true; }
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			g.lineStyle(1, item.color, 1);
			g.beginFill(0,0);
			switch (seriesType) {
				case SeriesType.LINE:
					g.moveTo(bounds.x, bounds.y + bounds.height/2);
					g.lineTo(bounds.right, bounds.y + bounds.height/2);
					break;
				case SeriesType.STEP_LINE_BACKWARD:
				case SeriesType.STEP_LINE_FORWARD:
					g.moveTo(bounds.x, bounds.bottom);
					g.lineTo(bounds.x + bounds.width/2, bounds.bottom);
					g.lineTo(bounds.x + bounds.width/2, bounds.y);
					g.lineTo(bounds.right, bounds.top);
					break;
				case SeriesType.SPLINE:
					g.moveTo(bounds.x, bounds.bottom);
					g.curveTo(bounds.x + bounds.width*.3, bounds.y + bounds.height, bounds.x + bounds.width/2, bounds.y + bounds.height/2);
					g.curveTo(bounds.x + bounds.width*.7, bounds.y, bounds.right, bounds.y);
					break;
			}
			g.lineStyle();
			g.endFill();
		} 
		
		override public function pointsCanBeAnimated():Boolean { return false; }
		override public function seriesCanBeAnimated():Boolean { return true; }
		
	}
}