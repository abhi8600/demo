package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	internal final class LineSeriesSettings extends BaseSeriesSettings {
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new LineSeriesSettings();
			return super.createCopy(settings);
		}
	}
}