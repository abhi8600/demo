package com.anychart.radarPolarPlot.series.lineSeries
{
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.geom.Point;

	internal class RadarLinePoint extends LinePoint
	{
		public var bottomStart:Point;
		public var bottomEnd:Point;
		public var needCreateDrawingPoints:Boolean;
		public var needInitializePixValues :Boolean;
		
		
		public function RadarLinePoint()
		{
			super();
			this.needCreateDrawingPoints = true;
			this.needInitializePixValues = true;
			this.bottomStart = new Point();
			this.bottomEnd = new Point();
		}
		
		public function doInitializePixValues():void {
			super.initializePixValues();
		}
		
		public function doCreateDrawingPoints():void {
			this.createDrawingPoints();
		}
		
		override public function initialize():void {
			super.initialize();
			var s:LineSeries = LineSeries(this.series);
			if (s.needCreateGradientRect) return;
			if (this.isMissing) {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.missing);
			}else {
				s.needCreateGradientRect = this.isGradientState(this.settings.style.normal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.hover);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.pushed);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedNormal);
				s.needCreateGradientRect = s.needCreateGradientRect || this.isGradientState(this.settings.style.selectedHover);
			}
		}
		
		override protected function createDrawingPoints():void{
			this.drawingPoints = [];
			var k:Number=0;
			if (this.index == 0) k=this.series.points.length;
			else k=this.index;
			if (this.hasPrev) {
				this.series.points[k-1].initializePixValues();
				this.drawingPoints.push(this.getPointBetween(this.series.points[k-1], this));
			} 
			
			this.drawingPoints.push(this.pixValuePt);
			
			if (this.index == this.series.points.length-1) k = -1;
			else k=this.index;
			if (this.hasNext) {
				this.series.points[k+1].initializePixValues();
				this.drawingPoints.push(this.getPointBetween(this, this.series.points[k+1]));
			}
		}
		
		protected function getPointBetween(pt1:LinePoint, pt2:LinePoint):*
		{
			var res:LinePoint = new LinePoint();
			var resTmp:Point = new Point();
			RadarPolarPlot(this.plot).transform((pt1.x + pt2.x)/2, Math.max(pt1.stackValue, pt2.stackValue), resTmp);
			res.x=resTmp.x;
			res.y=resTmp.y;
			var x3:Number = res.x;
			var y3:Number = res.y;
			
			var x1:Number = RadarLinePoint(pt1).pixValuePt.x;
			var y1:Number = RadarLinePoint(pt1).pixValuePt.y;
			
			var x2:Number = RadarLinePoint(pt2).pixValuePt.x;
			var y2:Number = RadarLinePoint(pt2).pixValuePt.y;
			
			var x0:Number = RadarPolarPlot(this.plot).getCenter().x;
			var y0:Number = RadarPolarPlot(this.plot).getCenter().y;
			
			if (x1==x2 && y1==y2) return res;
			
			if (x2==x1){
				res.x = x2;
				res.y = (( res.x - x0 ) / (x3 - x0))*(y3 - y0) + y0;
				return res;	
			}
			if (x0==x3) 
				res.x = x0;
			else
				res.x = ( (y1 - y0) + (x0*(y3 - y0))/(x3-x0) - (x1*(y2-y1))/(x2-x1) ) / ( (y3-y0)/(x3-x0) - (y2-y1)/(x2-x1) ); 
			res.y = (( res.x - x1 ) / (x2 - x1))*(y2 - y1) + y1;
			return res;	
		}
		
		override protected function drawSegment(g:Graphics):void{
			var s:Stroke = LineStyleState(this.styleState).stroke;
			if (s != null) {
				s.apply(g, LineSeries(this.series).gradientBounds, this.color);
				
				var i:int;
				if (s.dashed) {
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 0;i<(this.drawingPoints.length-1);i++) {
						DrawingUtils.drawDashedLine(g, s.spaceLength, s.dashLength, this.drawingPoints[i].x, this.drawingPoints[i].y, this.drawingPoints[i+1].x, this.drawingPoints[i+1].y);
					}
				}else {
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 1;i<this.drawingPoints.length;i++) 
						g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
				}
			}
		}
	}
}