package com.anychart.events
{
	import flash.events.Event;

	public class AnyChartMultiplePointsEvent extends Event {
		
		public static const MULTIPLE_POINTS_SELECT:String = "multiplePointsSelect";
		public static const MULTIPLE_POINTS_DESELECT:String = "multiplePointsDeselect";
		
		public var points:Array;
		
		public function AnyChartMultiplePointsEvent(type:String, points:Array) {
			this.points = points;
			super(type);
		}
		
		override public function clone():Event {
			return new AnyChartMultiplePointsEvent(this.type, this.points);
		}
		
	}
}