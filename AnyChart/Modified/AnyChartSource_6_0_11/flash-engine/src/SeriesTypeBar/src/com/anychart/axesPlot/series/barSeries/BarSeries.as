package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.scales.BaseScale;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal class BarSeries extends SingleValueSeries {
		
		internal var valueAxisStart:Number;
		
		public function BarSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
			this.isSortable = true;
		}
		
		private function setValueAxisStart():void {
			if (this.valueAxis.scale.minimum > 0)
				this.valueAxisStart = this.valueAxis.scale.minimum;
			else
				this.valueAxisStart = 0;
		}
		
		override public function onBeforeDraw():void {
			this.setValueAxisStart();
		}
		
		override public function onBeforeResize():void {
			this.setValueAxisStart();
		}
		
		override public function createPoint():BasePoint {
			return new BarPoint();
		}
		
		/* override public function configureValueScale(scale:BaseScale):void {
			if (!AxesPlot(this.plot).isScatter)
				scale.checkZero();
		} */
	}
}