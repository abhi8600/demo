package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.series.barSeries.BarShapeType;
	import com.anychart.errors.FeatureNotSupportedError;
	
	import flash.utils.getDefinitionByName;
	
	/**
	 * Класс описывает создание Drawer'ov 
	 * @author P@zin
	 * 
	 */
	public class BarDrawingFactory implements IBarDrawingFactory {
		
		private var conePyramidDrawerClass:Class;
		
		/**
		 * Метод Создает ссылку на класс Drawer'a в зависимости от ориентации графика 
		 *  
		 * @param isHorisontal
		 */
		public function  BarDrawingFactory (isHorisontal:Boolean) {
			conePyramidDrawerClass = isHorisontal ? ConePyramidDrawerHorizontal : ConePyramidDrawerVertical; 
		}
			
		/**
		 * Метод создает необходимый Drawer в зависимости shapeType
		 *  
		 * @param shapeType
		 * @return 
		 * 
		 */
		public function create(shapeType:uint):IBarDrawerBase {
			if (shapeType != BarShapeType.BOX && getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
				throw new FeatureNotSupportedError("Chart type not supported in this swf version");
				
			if (shapeType == BarShapeType.CONE || shapeType == BarShapeType.PYRAMID)
				return new this.conePyramidDrawerClass(); 
			return new BoxDrawer();
		}
	}
}