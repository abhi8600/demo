package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.scales.ScaleMode;
	
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывает рисование точки когда график ориентирован горизонтально
	 * @author P@zin
	 * 
	 */
	internal final class ConePyramidDrawerHorizontal extends ConePyramidDrawer{
		
		/**
		 * @inheritedDoc 
		 */
		override protected function getInverted():Boolean {
			return this.leftTop.x>this.rightBottom.x;
		}
		
		/**
		 * @inheritedDoc
		 */
		override protected function initializeNormal(inverted:Boolean,rect:Rectangle):void {
			this.rightTop.x = this.rightBottom.x = rect.right; 
			this.rightBottom.y = inverted ? rect.bottom  : rect.y + rect.height/2; 
			this.rightTop.y = inverted ? rect.top : rect.y + rect.height/2;
			
			this.leftTop.x = this.leftBottom.x = rect.left;
			this.leftBottom.y = !inverted ? rect.bottom  : rect.y + rect.height/2;
			this.leftTop.y = !inverted ? rect.top  : rect.y + rect.height/2;
		}
		
		/**
		 *@inheritedDoc
		 */
		override protected function initializeStacked(inverted:Boolean,rect:Rectangle, startValue:Number, endValue:Number):void {
			var stackSettings:StackSettings = this.point.category.getStackSettings(AxesPlotSeries(this.point.series).valueAxis.name, this.point.series.type,AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED); 
			var stackSumm:Number = stackSettings.getStackSumm(this.point.y);
			var bottomWidth:Number = rect.height*(stackSumm - startValue)/stackSumm;
			var topWidth :Number = rect.height*(stackSumm - endValue)/stackSumm;
			
			this.rightBottom.x = this.rightTop.x = rect.right; 
			this.leftBottom.x = this.leftTop.x = rect.left; 
			!inverted ? rect.right : rect.left;
			
			this.rightTop.y = rect.y + (!inverted ? (rect.height-topWidth)/2 : (rect.height-bottomWidth)/2);
			this.rightBottom.y = rect.y + (!inverted ? (rect.height+topWidth)/2 : (rect.height+bottomWidth)/2);
			this.leftTop.y = rect.y + (inverted ? (rect.height-topWidth)/2 : (rect.height-bottomWidth)/2);
			this.leftBottom.y = rect.y + (inverted ? (rect.height+topWidth)/2 : (rect.height+bottomWidth)/2);
		}
	}
}