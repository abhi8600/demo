package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.drawing.BarDrawingFactory;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawingFactory;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.ConeProgramaticStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.CylinderProgrammaticStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.DarkAquaBarStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.LightAquaBarStyle;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class BarGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings implements IBarCustomWidthSupport {
		
		public var drawingFactory:IBarDrawingFactory;
		public var isFixedBarWidth:Boolean;
		public var fixedBarWidth:Number;
		
		public function BarGlobalSeriesSettings() {
			super();
			this.isFixedBarWidth = false;
		}
		
		override public function initialize(plot:SeriesPlot):void {
			super.initialize(plot);
			this.drawingFactory = this.createDrawingFactory(AxesPlot(plot).isHorizontal);
		}
		
		protected function createDrawingFactory(isHorizontal:Boolean):IBarDrawingFactory {
			return new BarDrawingFactory(isHorizontal);
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@point_width != undefined) {
				this.isFixedBarWidth = true;
				this.fixedBarWidth = SerializerBase.getNumber(data.@point_width);
			}
		}
		
		public function getBarWidth(point:AxesPlotPoint): Number {
			if (AxesPlot(this.plot).isScatter)
				return (this.isPercentPointScetterWidth ? plot.getBounds().width : 1)*this.pointScetterWidth; 
			return this.isFixedBarWidth ? this.fixedBarWidth : point.clusterSettings.clusterWidth;
		}
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSettings():BaseSeriesSettings {
			return new BarSeriesSettings();
		}
		
		override public function getStyleStateClass():Class {
			return BarStateStyle;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "aqualight") {
				var pStyle:LightAquaBarStyle = new LightAquaBarStyle();
				pStyle.initialize(state);
				return pStyle;
			} else if (SerializerBase.getLString(styleNode.@name) == "aquadark") {
				var st:DarkAquaBarStyle = new DarkAquaBarStyle();
				st.initialize(state);
				return st;
			}else if (SerializerBase.getLString(styleNode.@name) == "cylinder" || SerializerBase.getLString(styleNode.@name) == "silver") {
				var cst:CylinderProgrammaticStyle = new CylinderProgrammaticStyle();
				cst.initialize(state);
				return cst;
			}else if (SerializerBase.getLString(styleNode.@name) == "cone"){
				var coneSt:ConeProgramaticStyle = new ConeProgramaticStyle();
				coneSt.initialize(state);
				
				if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
					throw new FeatureNotSupportedError("Cone chart type not supported in this swf version");
				
				return coneSt;
			}
			
			return null;
		}
		
		override public function isProgrammaticStyle(styleNode:XML):Boolean {
			if (styleNode.@name == undefined) return false;
			var name:String = SerializerBase.getLString(styleNode.@name);
			return name == "aqualight" || name == "aquadark" || name == "cylinder" || name == "silver";
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:BarSeries = new BarSeries();
			series.type = SeriesType.BAR;
			series.clusterKey = SeriesType.BAR;
			return series;
		}
		
		override public function getSettingsNodeName():String {
			return "bar_series";
		}
		
		override public function getStyleNodeName():String { return "bar_style"; }
		
		override public function hasIconDrawer(seriesType:uint):Boolean { return true; }
		
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			var dc:uint = ColorUtils.getDarkColor(item.color);
			g.lineStyle(1, dc, 1);
			g.beginFill(item.color, 1);
			g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
			g.endFill();
			g.lineStyle();
		}
	}
}