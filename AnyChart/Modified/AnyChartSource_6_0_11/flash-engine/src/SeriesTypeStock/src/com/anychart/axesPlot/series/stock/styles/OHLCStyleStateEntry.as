package com.anychart.axesPlot.series.stock.styles {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleStateWithEffects;
	import com.anychart.visual.stroke.Stroke;
	
	public class OHLCStyleStateEntry extends StyleStateWithEffects {
		public var line:Stroke;
		public var openLine:Stroke;
		public var closeLine:Stroke;
		
		public function OHLCStyleStateEntry(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (SerializerBase.isEnabled(data.line[0])) {
				this.line = new Stroke();
				this.line.deserialize(data.line[0], this.style);
				
				this.openLine = new Stroke();
				this.closeLine = new Stroke();
				
				this.openLine.deserialize(data.line[0], this.style);
				this.closeLine.deserialize(data.line[0], this.style);
			}
			if (SerializerBase.isEnabled(data.open_line[0])) {
				if (this.openLine == null)
					this.openLine = new Stroke();
				this.openLine.deserialize(data.open_line[0], this.style);
			}
			if (SerializerBase.isEnabled(data.close_line[0])) {
				if (this.closeLine == null)
					this.closeLine = new Stroke();
				this.closeLine.deserialize(data.close_line[0], this.style);
			}
			return data;
		}
	}
}