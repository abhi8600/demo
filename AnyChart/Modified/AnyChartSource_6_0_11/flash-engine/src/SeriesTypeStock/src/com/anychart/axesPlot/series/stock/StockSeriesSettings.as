package com.anychart.axesPlot.series.stock {
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	internal final class StockSeriesSettings extends BaseSeriesSettings {
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new StockSeriesSettings();
			super.createCopy(settings);
			return settings;
		}
	}
}