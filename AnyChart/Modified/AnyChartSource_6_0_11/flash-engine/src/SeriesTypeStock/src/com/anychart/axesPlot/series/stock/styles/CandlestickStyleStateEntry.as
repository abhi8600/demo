package com.anychart.axesPlot.series.stock.styles {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleStateWithEffects;
	import com.anychart.utils.StringUtils;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	public class CandlestickStyleStateEntry extends StyleStateWithEffects {
		
		public var fill:Fill;
		public var border:Stroke;
		public var hatchFill:HatchFill;
		public var line:Stroke;
		
		public function CandlestickStyleStateEntry(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			data = super.deserialize(data, resources);
			if (data.@hatch_type != undefined) {
				var hatchType:String = data.@hatch_type;
				if (data.hatch_fill[0] != null && data.hatch_fill[0].@type != undefined) {
					var childNode:XML = data.hatch_fill[0];
					childNode.@type = StringUtils.replace(String(childNode.@type).toLowerCase(),"%hatchtype",hatchType);
				}
			}
			
			if (SerializerBase.isEnabled(data.fill[0])) {
				this.fill = new Fill();
				this.fill.deserialize(data.fill[0],resources,this.style);
			}
			
			if (SerializerBase.isEnabled(data.border[0])) {
				this.border = new Stroke();
				this.border.deserialize(data.border[0], this.style);
			}
			
			if (SerializerBase.isEnabled(data.hatch_fill[0])) {
				this.hatchFill = new HatchFill();
				this.hatchFill.deserialize(data.hatch_fill[0],this.style);
			}
			
			if (SerializerBase.isEnabled(data.line[0])) {
				this.line = new Stroke();
				this.line.deserialize(data.line[0],this.style);
			}
			
			return data;
		}

	}
}