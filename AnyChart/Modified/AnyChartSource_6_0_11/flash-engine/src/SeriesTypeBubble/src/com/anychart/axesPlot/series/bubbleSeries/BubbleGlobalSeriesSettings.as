package com.anychart.axesPlot.series.bubbleSeries {
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.interpolation.IMissingInterpolator;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.programmaticStyles.CircleAquaStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class BubbleGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		public var isPercentMultiplierBubbleSize:Boolean = false;
		public var bubbleSizeMultiplier:Number = 0;
		
		public var minimumValueBubbleSize:Number = Number.MAX_VALUE;
		public var maximumValueBubbleSize:Number = -Number.MAX_VALUE;
		
		public var isPercentMaximumBubbleSize:Boolean = true;
		public var maximumBubbleSize:Number = .2;
		
		public var isPercentMinimumBubbleSize:Boolean = true;
		public var minimumBubbleSize:Number = .01;
		
		public var displayNegative:Boolean = true;
		
		public function getBubbleSize(size:Number):Number {
			var targetSize:Number = Math.min(this.plot.getBounds().width, this.plot.getBounds().height);
			
			if (this.bubbleSizeMultiplier > 0)
			{
				if (this.isPercentMultiplierBubbleSize) return size*this.bubbleSizeMultiplier*targetSize;
				else return size*this.bubbleSizeMultiplier;
			}
			
			var minPixSize:Number = this.isPercentMinimumBubbleSize ? (targetSize*this.minimumBubbleSize) : this.minimumBubbleSize;
			var maxPixSize:Number = this.isPercentMaximumBubbleSize ? (targetSize*this.maximumBubbleSize) : this.maximumBubbleSize;
			
			if (isNaN(size) || (this.minimumValueBubbleSize == this.maximumValueBubbleSize))
				return maxPixSize;
				
			var ratio:Number = (size - this.minimumValueBubbleSize)/(this.maximumValueBubbleSize - this.minimumValueBubbleSize);
			return (minPixSize + ratio * (maxPixSize - minPixSize));
		}
		
		override public function deserialize(data:XML):void {
			
			if (data == null) return;
			
			super.deserialize(data);
			if (data.@maximum_bubble_size != undefined) {
				if (SerializerBase.isPercent(data.@maximum_bubble_size)) {
					this.isPercentMaximumBubbleSize = true;
					this.maximumBubbleSize = SerializerBase.getPercent(data.@maximum_bubble_size);
				}else {
					this.isPercentMaximumBubbleSize = false;
					this.maximumBubbleSize = SerializerBase.getNumber(data.@maximum_bubble_size);
				}
			}
			if (data.@minimum_bubble_size != undefined) {
				if (SerializerBase.isPercent(data.@minimum_bubble_size)) {
					this.isPercentMinimumBubbleSize = true;
					this.minimumBubbleSize = SerializerBase.getPercent(data.@minimum_bubble_size);
				}else {
					this.isPercentMinimumBubbleSize = false;
					this.minimumBubbleSize = SerializerBase.getNumber(data.@minimum_bubble_size);
				}
			}
			
			if (data.@bubble_size_multiplier != undefined){
				if (SerializerBase.isPercent(data.@bubble_size_multiplier)){
					this.isPercentMultiplierBubbleSize = true;
					this.bubbleSizeMultiplier=SerializerBase.getPercent(data.@bubble_size_multiplier);
				}else{
					this.isPercentMultiplierBubbleSize = false;
					this.bubbleSizeMultiplier=SerializerBase.getNumber(data.@bubble_size_multiplier);
				}
			}
			
			if (data.@display_negative_bubbles != undefined)
					this.displayNegative = SerializerBase.getBoolean(data.@display_negative_bubbles);
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:BubbleSeries = new BubbleSeries();
			series.type = SeriesType.BUBBLE;
			series.clusterKey = SeriesType.BUBBLE;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BubbleSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "bubble_series";
		}
		
		override public function getStyleNodeName():String {
			return "bubble_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "aqua") {
				var pStyle:CircleAquaStyle = new CircleAquaStyle();
				pStyle.initialize(state);
				return pStyle;
			}
			return null;
		}
		
		override public function isProgrammaticStyle(styleNode:XML):Boolean {
			return styleNode.@name != undefined && SerializerBase.getLString(styleNode.@name) == "aqua";
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean { return true; }
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			var dc:uint = ColorUtils.getDarkColor(item.color);
			var r:Number = Math.min(bounds.width, bounds.height)/2;
			bounds = new Rectangle(bounds.x + bounds.width/2 - r, bounds.y + bounds.height/2 - r, r, r);
			g.beginFill(item.color, 1);
			g.lineStyle(1, dc, 1);
			g.drawCircle(bounds.x + r, bounds.y + r, r);
			g.lineStyle();
			g.endFill();
		}
		
		
		private var sizeInterpolator:IMissingInterpolator;
		
		override public function missingInitializeInterpolators():void {
			super.missingInitializeInterpolators();
			this.sizeInterpolator.initialize("bubbleSize");
		}
		
		override public function missingCheckPointDuringDeserialize(point:BasePoint):void {
			super.missingCheckPointDuringDeserialize(point);
			this.sizeInterpolator.checkDuringDeserialize(point, point.index);
		}
		
		override public function missingInterpolate(points:Array):void {
			var missingPoints:Array = this.yInterpolator.interpolate(points);
			var sizeMissingPoints:Array = this.sizeInterpolator.interpolate(points);
			var pt:BubblePoint;
			
			for each (pt in missingPoints) {
				this.missingInitializeAfterInterpolate(pt);
				if (sizeMissingPoints.indexOf(pt) != -1) {
					sizeMissingPoints.splice(sizeMissingPoints.indexOf(pt), 1);
				}
				this.missingInitializeAfterInterpolate(pt);
			}
			
			for each (pt in sizeMissingPoints) {
				this.missingInitializeAfterInterpolate(pt);
			}
			
			this.sizeInterpolator.clear();
			this.yInterpolator.clear();
		}
		
		override public function reset():void {
			this.minimumValueBubbleSize = Number.MAX_VALUE;
			this.maximumValueBubbleSize = -Number.MAX_VALUE;
		}
	}
}