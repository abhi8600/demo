package com.anychart.axesPlot.series.barSeries.pointsOrdering {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.categorization.ClusterSettings;
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.data.SeriesType;
	import com.anychart.scales.ScaleMode;
	
	internal class BasePointsOrderer implements IPointsOrderer {
		protected var plot:AxesPlot3D;
		
		protected var isArgumentAxisLeftToRight:Boolean;
		
		private var isLeftSideVisible:Boolean;
		private var isTopSideVisible:Boolean;
		
		private var isCategorizedBySeries:Boolean;
		
		public function order(plot:AxesPlot3D):void {
			this.plot = plot;
			if (this.plot.xCategories.length == 0) return;
			
			this.isCategorizedBySeries = this.plot.getPlotType().indexOf("categorizedbyseries") == 0;
			
			this.initialize();
			
			var min:int = this.isArgumentAxisLeftToRight ? 0 : (plot.xCategories.length - 1);
			var max:int = this.isArgumentAxisLeftToRight ? (plot.xCategories.length - 1) : 0;
			var iterator:int = min;
			var step:int = this.isArgumentAxisLeftToRight ? 1 : -1;
			
			this.distributeCategory(this.plot.xCategories[iterator]);
			while (iterator != max) {
				iterator += step;
				this.distributeCategory(this.plot.xCategories[iterator]);
			}
			
			this.plot = null;
		}
		
		protected function initialize():void {
			this.isTopSideVisible = this.plot.space3D.isZAxisAtLeftBottom() || this.plot.space3D.isZAxisAtRightBottom();
			this.isLeftSideVisible = this.plot.space3D.isZAxisAtRightBottom() || this.plot.space3D.isZAxisAtRightTop();
		}
		protected function isInvertedStackInit(axis:Axis):Boolean { return false; }
		
		private function getClusterIndex(point:AxesPlotPoint):int {
			return this.isCategorizedBySeries ? point.clusterIndex : AxesPlotSeries(point.series).clusterIndex;
		}
		
		private function distributeCategory(category:Category):void {
			
			var categoryPoints:Array = category.getPoints();
			if (categoryPoints.length == 0) return;
			
			var clusters:Array = this.getClusters(categoryPoints);
			
			var min:int = this.isArgumentAxisLeftToRight ? 0 : (clusters.length - 1);
			var max:int = this.isArgumentAxisLeftToRight ? (clusters.length - 1) : 0;
			var iterator:int = min;
			var step:int = this.isArgumentAxisLeftToRight ? 1 : -1;
			
			this.distributeCluster(clusters[iterator].data);
			
			while (iterator != max) {
				iterator += step;
				this.distributeCluster(clusters[iterator].data);
			}
		}
		
		private function distributeCluster(clusterPoints:Array):void {
			if (clusterPoints == null || clusterPoints.length == 0)
				return;
			
			if (clusterPoints.length == 1) {
				this.distributeSinglePointCluster(clusterPoints);
			}else {
				this.distributeCombloCluster(clusterPoints);
			}
		}
		
		private function getClusters(categoryPoints:Array):Array {
			var categoryPointsByCluster:Object = {};
			var clusters:Array = [];
			
			for each (var point:AxesPlotPoint in categoryPoints) {
				if (!(point is I3DObject)) continue;
				var clusterIndex:int = this.getClusterIndex(point);
				
				if (categoryPointsByCluster[clusterIndex] == null) {
					categoryPointsByCluster[clusterIndex] = [];
					clusters.push({index:clusterIndex, data:categoryPointsByCluster[clusterIndex]});
				}
				categoryPointsByCluster[clusterIndex].push(point);
			}
			
			clusters.sortOn("index",Array.NUMERIC);
			return clusters;
		}
		
		private function distributeSinglePointCluster(clusterPoints:Array):void {
			this.initPoint(clusterPoints[0]);
		}
		
		private function distributeCombloCluster(clusterPoints:Array):void {
			var pt:AxesPlotPoint = clusterPoints[0];
			var axis:Axis = AxesPlotSeries(pt.series).valueAxis;
			if (axis.scale.mode == ScaleMode.STACKED || axis.scale.mode == ScaleMode.PERCENT_STACKED) {
				this.distributeStackedCluster(axis, clusterPoints[0].category);
			}else if (axis.scale.mode == ScaleMode.OVERLAY) {
				this.distributeOverlayCluster(clusterPoints);
			}else {
				this.distributeSortedOverlayCluster(clusterPoints);
			}
		}
		
		private function distributeStackedCluster(axis:Axis, category:Category):void {
			var stackSettings:StackSettings = category.getStackSettings(axis.name, SeriesType.BAR,axis.scale.mode == ScaleMode.PERCENT_STACKED);
			var pts:Array;
			var i:int;
			
			if (this.isInvertedStackInit(axis)) {
				pts = stackSettings.getPositivePoints();
				for (i = (pts.length-1);i>=0;i--)
					this.initPoint(pts[i]);
				
				pts = stackSettings.getNegativePoints();
				
				for (i = 0;i<pts.length;i++) 
					this.initPoint(pts[i]);
			}else {
				pts = stackSettings.getNegativePoints();
				for (i = (pts.length-1);i>=0;i--)
					this.initPoint(pts[i]);
				
				pts = stackSettings.getPositivePoints();
				
				for (i = 0;i<pts.length;i++) 
					this.initPoint(pts[i]);
			}
		}
		
		private function distributeOverlayCluster(clusterPoints:Array):void {
			for (var i:int = (clusterPoints.length - 1);i>=0;i--)
				this.initPoint(clusterPoints[i]);
		}
		
		private function distributeSortedOverlayCluster(clusterPoints:Array):void {
			clusterPoints.sortOn("zIndex", Array.NUMERIC);
			for (var i:int = (clusterPoints.length - 1);i>=0;i--) {
				this.initPoint(clusterPoints[i]);
			}
		}
		
		private function initPoint(point:I3DObject):void {
			point.isLeftSideVisible = this.isLeftSideVisible;
			point.isTopSideVisible = this.isTopSideVisible;
			point.initialize();
			this.plot.drawingPoints.push(point);
		}
	}
}