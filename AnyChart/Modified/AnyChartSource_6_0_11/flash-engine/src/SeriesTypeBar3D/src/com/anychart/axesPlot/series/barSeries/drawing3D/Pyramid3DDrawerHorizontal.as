package com.anychart.axesPlot.series.barSeries.drawing3D{
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.BarPoint3D;
	import com.anychart.scales.ScaleMode;
	
	import flash.geom.Point;
	
	
	internal final class Pyramid3DDrawerHorizontal extends PolygonBased3DDrawer implements IBar3DDrawer{
		
		public function Pyramid3DDrawerHorizontal() {
			super();
		}
		
		override protected function initializeSides():void {
			var rightYRadius:Number = 0;
			var leftYRadius:Number = (this.rightBottom.y-this.leftTop.y)/2;//(this.leftTop.y - this.rightBottom.y)/2;
			var stackSumm:Number = this.endValue;
			
			if(this.isStacked()){
				var stackSettings:StackSettings = this.point.category.getStackSettings(
					AxesPlotSeries(this.point.series).valueAxis.name, 
					this.point.series.type,
					AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED);
				stackSumm = stackSettings.getStackSumm(BarPoint3D(this.point).y);				
				rightYRadius = leftYRadius = leftYRadius/stackSumm;
				rightYRadius*= (stackSumm - this.endValue);
				leftYRadius*= (stackSumm - this.startValue);
			}
			
			this.init(rightYRadius, leftYRadius, stackSumm);
		}
		
		private function init(rightYRadius:Number, leftYRadius:Number, stackSumm:Number):void {
			var frontLeftCenter:Point = new Point();
			var frontRightCenter:Point = new Point();
			var backLeftCenter:Point = new Point();
			var backRightCenter:Point = new Point();
			
			frontLeftCenter.y = frontRightCenter.y = (this.rightBottom.y+this.leftTop.y)/2;
			frontRightCenter.x = this.rightBottom.x;
			frontLeftCenter.x = this.leftTop.x;
			
			backLeftCenter.y = backRightCenter.y = (this.rightBottom.y+this.leftTop.y)/2;
			backRightCenter.x =  this.rightBottom.x;
			backLeftCenter.x = this.leftTop.x;
			
			var leftOffset:Number = this.startValue/stackSumm;
			var rightOffset:Number = this.endValue/stackSumm; 
			
			this.point.z.transform(frontLeftCenter,0,leftOffset/2);
			this.point.z.transform(frontRightCenter,0,rightOffset/2);  
			this.point.z.transform(backLeftCenter,0,1-leftOffset/2);
			this.point.z.transform(backRightCenter,0,1-rightOffset/2);     
			//
			//Бля мне хочется оторвать кому нибудь руки за порчу кода!
			//
			//
			this.points[0].x = frontLeftCenter.x;
			this.points[0].y = frontLeftCenter.y + leftYRadius;
			
			this.points[1].x = frontLeftCenter.x;
			this.points[1].y = frontLeftCenter.y - leftYRadius;
			
			this.points[2].x = frontRightCenter.x;
			this.points[2].y = frontRightCenter.y - rightYRadius;
			
			this.points[3].x = frontRightCenter.x;
			this.points[3].y = frontRightCenter.y + rightYRadius;
			
			this.points[4].x = backLeftCenter.x;
			this.points[4].y = backLeftCenter.y + leftYRadius;
			
			this.points[5].x = backLeftCenter.x;
			this.points[5].y = backLeftCenter.y - leftYRadius;
			
			this.points[6].x = backRightCenter.x;
			this.points[6].y = backRightCenter.y - rightYRadius;
			
			this.points[7].x = backRightCenter.x;
			this.points[7].y = backRightCenter.y + rightYRadius;
		}

		override public function initializePixels(startValue:Number, endValue:Number):void {
			this.startValue = startValue;
			this.endValue = endValue;
			super.initializePixels(startValue,endValue);
		}
	}
}