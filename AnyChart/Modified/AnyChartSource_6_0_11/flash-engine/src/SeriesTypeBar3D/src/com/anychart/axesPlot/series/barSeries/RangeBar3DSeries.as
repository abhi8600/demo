package com.anychart.axesPlot.series.barSeries {
	
	import com.anychart.seriesPlot.data.BasePoint;

	public class RangeBar3DSeries extends RangeBarSeries{
		
		override public function createPoint():BasePoint {
			return new RangeBarPoint3D();
		}
	}
}