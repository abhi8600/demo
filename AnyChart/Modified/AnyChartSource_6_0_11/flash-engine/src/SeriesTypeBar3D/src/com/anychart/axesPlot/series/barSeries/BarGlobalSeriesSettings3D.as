package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawingFactory;
	import com.anychart.axesPlot.series.barSeries.drawing3D.Bar3DDrawerFactory;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.Box3DProgramaticStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.Cone3DProgramaticStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.Cylinder3DProgramaticStyle;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.Pyramid3DProgramaticStyle;
	import com.anychart.data.SeriesType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	public final class BarGlobalSeriesSettings3D extends BarGlobalSeriesSettings {
		
		private static var initialized:Boolean = initialize();
		private static function initialize():Boolean {
			var tmp:Bar3DPointsDistributor;
			return true;
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:BarSeries3D = new BarSeries3D();
			series.type = SeriesType.BAR;
			series.clusterKey = SeriesType.BAR;
			return series;
		}
		
		override public function initializeIfSortedOVerlay():Boolean { return false; }
		
		override protected function createDrawingFactory(isHorizontal:Boolean):IBarDrawingFactory {
			return new Bar3DDrawerFactory(isHorizontal);
		}
		
		//-----------------------------------------------------------
		//			STYLES
		//-----------------------------------------------------------
		
		override public function getStyleNodeName():String { return "bar_style"; }
		override public function getStyleStateClass():Class { return BackgroundBaseStyleState; }
		override public function isProgrammaticStyle(styleNode:XML):Boolean { return false; }
		override public function createSettings():BaseSeriesSettings { return new BarSeriesSettings3D(); }
		
		override public function checkStyle(style:Style):void {
			super.checkStyle(style);
			/* if (AxesPlot3D(this.plot).isHorizontal) {
				this.applyToStates(style, this.rotateFrontToHorizontal);
				this.applyToStates(style, this.rotateLeftRightTopBottomToHorizontal);
			}
			
			this.applyToStates(style, this.clearState); */
		}
		/* 
		private function applyToStates(style:Style, applicator:Function):void {
			applicator(style.normal);
			applicator(style.hover);
			applicator(style.pushed);
			applicator(style.selectedNormal);
			applicator(style.selectedHover);
			applicator(style.missing);
		}
		
		private function clearState(state:BackgroundBaseStyleState3D):void {
			state.front.clear();
			state.back.clear();
			state.left.clear();
			state.right.clear();
			state.top.clear();
			state.bottom.clear();
		}
		
		private function rotateFrontToHorizontal(state:BackgroundBaseStyleState3D):void {
			if (state.front.fill == null) return;
			if (state.front.fill.type != FillType.GRADIENT) return;
			state.front.fill.gradient.angle += 90;
		}
		
		private function rotateLeftRightTopBottomToHorizontal(state:BackgroundBaseStyleState3D):void {
			var rightCopy:BackgroundBaseStyleState3DEntry = state.right.createCopy();
			state.left = state.top.createCopy();
			state.right = state.top.createCopy();
			state.top = rightCopy;
			state.bottom = rightCopy.createCopy();
		} */
		
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils3D;
			return true;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "box3dprogramaticstyle") {
				var boxSt:Box3DProgramaticStyle = new Box3DProgramaticStyle();
				boxSt.initialize(state);
				return boxSt;
			} else if (SerializerBase.getLString(styleNode.@name) == "pyramid3dprogramaticstyle") {
				var pyramidSt:Pyramid3DProgramaticStyle = new Pyramid3DProgramaticStyle();
				pyramidSt.initialize(state);
				return pyramidSt;
			}else if (SerializerBase.getLString(styleNode.@name) == "cylinder3dprogramaticstyle") {
				var cylinderSt:Cylinder3DProgramaticStyle = new Cylinder3DProgramaticStyle();
				cylinderSt.initialize(state);
				return cylinderSt;
			}else if (SerializerBase.getLString(styleNode.@name) == "cone3dprogramaticstyle") {
				var coneSt:Cone3DProgramaticStyle = new Cone3DProgramaticStyle();
				coneSt.initialize(state);
				return coneSt;
			}
			return null;
		} 
	}
}