package com.anychart.axesPlot.series.barSeries.pointsOrdering {
	import com.anychart.axesPlot.AxesPlot3D;
	
	public interface IPointsOrderer {
		function order(plot:AxesPlot3D):void;
	}
}