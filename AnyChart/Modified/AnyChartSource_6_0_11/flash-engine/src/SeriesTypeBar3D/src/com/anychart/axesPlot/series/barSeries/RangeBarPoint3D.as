package com.anychart.axesPlot.series.barSeries{
	
	import com.anychart.axesPlot.data.RangePoint;
	import com.anychart.axesPlot.data.data3D.ZPointSettings;
	import com.anychart.axesPlot.series.barSeries.data3D.ZBarPointSettings;
	import com.anychart.axesPlot.series.barSeries.drawing3D.IBar3DDrawer;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;

	public class RangeBarPoint3D extends RangePoint implements I3DObject {
		
		private var drawer:IBar3DDrawer;
		
		private var _isLeftSideVisible:Boolean;
		private var _isTopSideVisible:Boolean;
		
		public function get isLeftSideVisible():Boolean { return this._isLeftSideVisible; }
		public function set isLeftSideVisible(value:Boolean):void { this._isLeftSideVisible = value; }
		public function get isTopSideVisible():Boolean { return this._isTopSideVisible; }
		public function set isTopSideVisible(value:Boolean):void { this._isTopSideVisible = value; }
		
		public function getDrawer ():IBar3DDrawer {
			return this.drawer;
		}
		
		public function RangeBarPoint3D() {
			super();
			this._isLeftSideVisible = true;
			this._isTopSideVisible = true;
		}
		
		public function get zIndex():Number { return this.z.getZ(); }
		
		override protected function create3D():ZPointSettings {
			return new ZBarPointSettings();
		}
		override public function initialize():void {
			super.initialize();
			
			this.drawer = IBar3DDrawer(RangeBarGlobalSeriesSettings3D(this.series.global).drawingFactory.create(BarShapeType.BOX));
			this.drawer.initialize(this);
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			this.drawer.initializePixels(this.start,this.end);
			this.drawBar(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawBar(this.container.graphics);
		} 
		
		private function drawBar(g:Graphics):void {
			
			if (this.hasPersonalContainer) {
				if (BackgroundBaseStyleState(this.styleState).effects != null) {
					this.container.filters = BackgroundBaseStyleState(this.styleState).effects.list;
				}else {
					this.container.filters = null;
				}
			}
			this.styleState.programmaticStyle.draw(this);
			
		/* 	this.drawer.drawBackSide(this.container.graphics);
			
			if (this.isTopSideVisible) {
				this.drawer.drawBottomSide(this.container.graphics);
				if (this.isLeftSideVisible) {
					this.drawer.drawRightSide(this.container.graphics);
					this.drawer.drawLeftSide(this.container.graphics);
				}else {
					this.drawer.drawLeftSide(this.container.graphics);
					this.drawer.drawRightSide(this.container.graphics);
				}
				this.drawer.drawTopSide(this.container.graphics);
			}else {
				this.drawer.drawTopSide(this.container.graphics);
				if (this.isLeftSideVisible) {
					this.drawer.drawRightSide(this.container.graphics);
					this.drawer.drawLeftSide(this.container.graphics);
				}else {
					this.drawer.drawLeftSide(this.container.graphics);
					this.drawer.drawRightSide(this.container.graphics);
				}
				this.drawer.drawBottomSide(this.container.graphics);
			}
			
			this.drawer.drawFrontSide(this.container.graphics); */
		}
	}
}