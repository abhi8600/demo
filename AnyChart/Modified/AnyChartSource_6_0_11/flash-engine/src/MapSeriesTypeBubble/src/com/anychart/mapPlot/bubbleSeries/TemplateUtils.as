package com.anychart.mapPlot.bubbleSeries {
	import com.anychart.templates.GlobalTemplateItems;
	
	import flash.utils.ByteArray;
	
	public final class TemplateUtils {
		[Embed(source="../../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
	}
}