package com.anychart.mapPlot.bubbleSeries {
	import com.anychart.data.SeriesType;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.programmaticStyles.CircleAquaStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	
	public final class MapBubbleGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		public var minimumValueBubbleSize:Number = Number.MAX_VALUE;
		public var maximumValueBubbleSize:Number = -Number.MAX_VALUE;
		
		override public function reset():void {
			super.reset();
			this.minimumValueBubbleSize = Number.MAX_VALUE;
			this.maximumValueBubbleSize = -Number.MAX_VALUE;
		}
		
		public var isPercentMaximumBubbleSize:Boolean = true;
		public var maximumBubbleSize:Number = .2;
		
		public var isPercentMinimumBubbleSize:Boolean = true;
		public var minimumBubbleSize:Number = .01;
		
		public var displayNegative:Boolean = true;
		
		public function getBubbleSize(size:Number):Number {
			var targetSize:Number = Math.min(this.plot.getBounds().width, this.plot.getBounds().height);
			var minPixSize:Number = (this.isPercentMinimumBubbleSize ? (targetSize*this.minimumBubbleSize) : this.minimumBubbleSize)/2;
			if (isNaN(size) || (this.minimumValueBubbleSize == this.maximumValueBubbleSize))
				return minPixSize;
				
			var ratio:Number = (size - this.minimumValueBubbleSize)/(this.maximumValueBubbleSize - this.minimumValueBubbleSize);
			var maxPixSize:Number = (this.isPercentMaximumBubbleSize ? (targetSize*this.maximumBubbleSize) : this.maximumBubbleSize)/2;
			
			return minPixSize + ratio * (maxPixSize - minPixSize);
		}
		
		override public function deserialize(data:XML):void {
			
			if (data == null) return;
			
			super.deserialize(data);
			if (data.@maximum_bubble_size != undefined) {
				if (SerializerBase.isPercent(data.@maximum_bubble_size)) {
					this.isPercentMaximumBubbleSize = true;
					this.maximumBubbleSize = SerializerBase.getPercent(data.@maximum_bubble_size);
				}else {
					this.isPercentMaximumBubbleSize = false;
					this.maximumBubbleSize = SerializerBase.getNumber(data.@maximum_bubble_size);
				}
			}
			if (data.@minimum_bubble_size != undefined) {
				if (SerializerBase.isPercent(data.@minimum_bubble_size)) {
					this.isPercentMinimumBubbleSize = true;
					this.minimumBubbleSize = SerializerBase.getPercent(data.@minimum_bubble_size);
				}else {
					this.isPercentMinimumBubbleSize = false;
					this.minimumBubbleSize = SerializerBase.getNumber(data.@minimum_bubble_size);
				}
			}
			if (data.@display_negative_bubbles != undefined)
					this.displayNegative = SerializerBase.getBoolean(data.@display_negative_bubbles);
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:BubbleSeries = new BubbleSeries();
			series.type = SeriesType.BUBBLE;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "bubble_series";
		}
		
		override public function getStyleNodeName():String {
			return "bubble_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "aqua") {
				var pStyle:CircleAquaStyle = new CircleAquaStyle();
				pStyle.initialize(state);
				return pStyle;
			}
			return null;
		}
		
		override public function isProgrammaticStyle(styleNode:XML):Boolean {
			return styleNode.@name != undefined && SerializerBase.getLString(styleNode.@name) == "aqua";
		}
		
		override protected function getPointValueFieldName():String { return "bubbleSize"; }
	}
}