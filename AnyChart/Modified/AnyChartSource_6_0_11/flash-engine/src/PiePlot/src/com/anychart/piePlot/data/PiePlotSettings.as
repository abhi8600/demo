package com.anychart.piePlot.data {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	public class PiePlotSettings extends BaseSeriesSettings {
		
		public var exploded:Boolean;
		
		public function PiePlotSettings() {
			super();
			this.exploded = false;
		}
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new PiePlotSettings();
			PiePlotSettings(settings).exploded = exploded;
			super.createCopy(settings);
			return settings;
		}
		
		override public function pointContainsUniqueSettings(point:XML):Boolean {
			return true;
		}
		
		override public function seriesContainsUniqueSettings(point:XML):Boolean {
			return true;
		}
		
		override protected function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data,stylesList,resources);
			if (data.@exploded != undefined) this.exploded = SerializerBase.getBoolean(data.@exploded);
			if (this.exploded)
				PiePlotGlobalSeriesSettings(this.global).hasExplodedItems = true;
		}
	}
}