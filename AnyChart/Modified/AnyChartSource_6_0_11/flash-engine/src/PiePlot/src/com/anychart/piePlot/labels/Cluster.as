package com.anychart.piePlot.labels
{
	import com.anychart.piePlot.PiePlot;
	
	import flash.geom.Rectangle;

	public class Cluster
	{
		private var _left:Boolean;
		private var _criticalAngle:Number;
		
		private var _labels:Array;
		private var _labelStartAngles:Array;
		private var _labelBottomShifts:Array;
		
		private var _currTop:Number;
		private var _currBottom:Number;
		
		private var _maxTop:Number; // для левых кластеров макстоп, минтоп работают с точностью до наоборот
		private var _minTop:Number;
		private var _effectiveRestrictionIndex:int;
		
		private var _currMagicNumber:Number;
		private var _currH:Number;
		private var _totalHeight:Number;
		
		private var _plot:PiePlot;
		
		private var _initialAngle:Number;
		
		public function get initialAngle():Number { return this._initialAngle; }
		public function get totalHeight():Number { return this._totalHeight; }
		
		
		public function Cluster(label:LabelInfo, criticalAngle:Number, plot:PiePlot)
		{
			this._plot = plot;
			this._initialAngle = this._plot.normalizeAngle(label.angle, -90, 270, 90);
			label.angle = this._initialAngle;
			this._left = this._plot.between(this._initialAngle, 90, 270);
			
			this._labels = new Array();
			this._labels.push(label);
			
			this._labelStartAngles = new Array();
			this._labelStartAngles.push(label.angle);
			
			this._labelBottomShifts = new Array();
			this._labelBottomShifts.push(label.height);
			
			this._totalHeight = label.height;
			this._currH = this._totalHeight;
			if (this._left) {
				this._currMagicNumber = plot.getLabelPosition(label, false).y + this._totalHeight / 2;
				this._currBottom = this._currMagicNumber;
				this._currTop = this._currBottom - this._totalHeight;
			} else {
				this._currMagicNumber = plot.getLabelPosition(label, false).y - this._totalHeight / 2;
				this._currTop = this._currMagicNumber;
				this._currBottom = this._currTop + this._totalHeight;
			}			
			
			this._criticalAngle = criticalAngle;
			
			this._maxTop = Number.MAX_VALUE;
			this._minTop = -Number.MAX_VALUE;
			
			this.updateBounds(0);
		}
		
		private function updateBounds(index:int):void {
			var label:LabelInfo = this._labels[index];
			var currAngle:Number = label.angle;
			var y:Number;
			
			if (this._left){
				label.angle = Math.max(90.01, this._plot.normalizeAngle(this._labelStartAngles[index] - this._criticalAngle, -90, 270, 90));
				y = this._plot.getLabelPosition(label, false).y - label.height / 2 + this._labelBottomShifts[index];
				this._maxTop = Math.min(this._maxTop, y);
				
				label.angle = Math.min(270, this._plot.normalizeAngle(this._labelStartAngles[index] + this._criticalAngle, 90, 450, 270));
				y = this._plot.getLabelPosition(label, false).y - label.height / 2 + this._labelBottomShifts[index];
				if (this._minTop < y){
					this._minTop = y;
					this._effectiveRestrictionIndex = index;
				}
			} else {
				label.angle = Math.max(-90, this._plot.normalizeAngle(this._labelStartAngles[index] - this._criticalAngle, -270, 90, -90));
				y = this._plot.getLabelPosition(label, false).y + label.height / 2 - this._labelBottomShifts[index];
				this._minTop = Math.max(this._minTop, y);
				
				label.angle = Math.min(90, this._plot.normalizeAngle(this._labelStartAngles[index] + this._criticalAngle, -90, 270, 90));
				y = this._plot.getLabelPosition(label, false).y + label.height / 2 - this._labelBottomShifts[index];
				if (this._maxTop > y){
					this._maxTop = y;
					this._effectiveRestrictionIndex = index;
				}
			}
			
			label.angle = currAngle;
		}
		
		public function checkOverlap(cluster:Cluster):Boolean {
			if (cluster._left != this._left) return false;
			if (this._left)
				return this._currTop <= cluster._currBottom;
			else
				return this._currBottom >= cluster._currTop;
		}
		
		public function merge(cluster:Cluster):void {
			var len:int = cluster._labels.length;
			for (var i:int = 0; i < len; i++){
				var label:LabelInfo = cluster._labels[i];
				label.angle = cluster._labelStartAngles[i];
				this._labels.push(label);
				this._labelStartAngles.push(label.angle);
				this._totalHeight += label.height;
				this._labelBottomShifts.push(this._totalHeight);
				if (this._left)
					this._currMagicNumber += this._plot.getLabelPosition(label, false).y + label.height / 2 + this._currH;
				else
					this._currMagicNumber += this._plot.getLabelPosition(label, false).y - label.height / 2 - this._currH;
				this._currH += label.height;
				this.updateBounds(this._labels.length - 1);
			}
			
			if (this._left){
				this._currBottom = this._currMagicNumber / this._labels.length;
				if (this._minTop <= this._maxTop){
					if (this._currBottom > this._maxTop)
						this._currBottom = this._maxTop;
					if (this._currBottom < this._minTop)
						this._currBottom = this._minTop;
					this._currTop = this._currBottom - this._totalHeight;
				} else {
					this._currBottom = this._maxTop;
					this._currTop = this._minTop - this._totalHeight;
				}
			} else {
				this._currTop = this._currMagicNumber / this._labels.length;
				if (this._minTop <= this._maxTop){
					if (this._currTop > this._maxTop)
						this._currTop = this._maxTop;
					if (this._currTop < this._minTop)
						this._currTop = this._minTop;
					this._currBottom = this._currTop + this._totalHeight;
				} else {
					this._currTop = this._minTop;
					this._currBottom = this._maxTop + this._totalHeight;
				}
			}
		}
		
		public function applyPositioning():void {
			var i:int;
			var len:int = this._labels.length;
			
			var k:Number;
			if (this._minTop <= this._maxTop)
				k = 1;
			else {
				var h:Number = this._labelBottomShifts[this._effectiveRestrictionIndex];
				k = 1 - (this._minTop - this._maxTop) / h;
			}
			
			if (this._left){
				for (i = 0; i < len; i++)
					this._labels[i].angle = checkLabelAngle(180 - this._plot.getLabelAngleInRightHalf(this._labels[i], this._currBottom - this._labelBottomShifts[i]*k + this._labels[i].height / 2), this._labelStartAngles[i]);
			} else
				for (i = 0; i < len; i++)
					this._labels[i].angle = checkLabelAngle(this._plot.getLabelAngleInRightHalf(this._labels[i], this._currTop + this._labelBottomShifts[i]*k - this._labels[i].height / 2), this._labelStartAngles[i]);
		}
		
		private var _crit90270:Number=88*Math.PI/180;
		private function checkLabelAngle (angle:Number, startAngle:Number):Number
		{
			var old_angle:Number=angle;
			angle=angle*Math.PI/180;
			startAngle=startAngle*Math.PI/180;
			if (Math.cos(angle)*Math.cos(startAngle)<0 || Math.abs(Math.cos(angle))<Math.cos(_crit90270))
			{
				if (Math.sin(startAngle)>0)
					return 90-(Math.cos(startAngle)>0?1:-1)*2;
				else
					return 270+(Math.cos(startAngle)>0?1:-1)*2;
			}
			else return old_angle;
		}
	}
}