package com.anychart.piePlot.data {
	import com.anychart.piePlot.PiePlot;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	
	public class PiePlot2DSeries extends BaseSeries {
		
		public var startAngle:Number;
		
		public var innerRadius:Number;
		public var outerRadius:Number;
		
		public function PiePlot2DSeries() {
			super();
			this.startAngle = 0;
			
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = '';
			this.cashedTokens['MinYValuePointName'] = '';
			
			this.needCallingOnBeforeDraw = true;
		}
		
		override public function onBeforeDraw():void {
			this.setPixelRadius();
		}
		
		override public function onBeforeResize():void {
			this.setPixelRadius();
		}
		
		override public function createPoint():BasePoint {
			return new PiePlot2DPoint();
		}
		
		protected function setPixelRadius():void {
			this.innerRadius = PiePlot(this.plot).getSeriesRadius(this.index, true);
			this.outerRadius = PiePlot(this.plot).getSeriesRadius(this.index, false);
		}
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var pt:PiePlot2DPoint = PiePlot2DPoint(point);
			this.cashedTokens['YSum'] += pt.y;
			if (pt.y > this.cashedTokens['YMax']) {
				this.cashedTokens['YMax'] = pt.y;
				this.cashedTokens['MaxYValuePointName'] = pt.name;
			}
			if (pt.y < this.cashedTokens['YMin']) {
				this.cashedTokens['YMin'] = pt.y;
				this.cashedTokens['MinYValuePointName'] = pt.name;
			}
		}
		
		override public function resetTokens():void {
			this.cashedTokens['YSum'] = 0;
			this.cashedTokens['YMax'] = -Number.MAX_VALUE;
			this.cashedTokens['YMin'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYValuePointName'] = '';
			this.cashedTokens['MinYValuePointName'] = '';
			this.cashedTokens['MaxYSumSeries'] = -Number.MAX_VALUE;
			this.cashedTokens['MinYSumSeries'] = Number.MAX_VALUE;
			this.cashedTokens['MaxYSumSeriesName'] = '';
			this.cashedTokens['MinYSumSeriesName'] = '';
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			if (plotTokensCash['YSum'] == null)
				plotTokensCash['YSum'] = 0;
			if (plotTokensCash['YBasedPointsCount'] == null)
				plotTokensCash['YBasedPointsCount'] = 0;
				
			plotTokensCash['YSum'] += this.cashedTokens['YSum'];
			plotTokensCash['YBasedPointsCount'] += this.points.length;
			
			if (plotTokensCash['YMax'] == null || this.cashedTokens['YMax'] > plotTokensCash['YMax']) {
				plotTokensCash['YMax'] = this.cashedTokens['YMax'];
				plotTokensCash['MaxYValuePointName'] = this.cashedTokens['MaxYValuePointName'];
				plotTokensCash['MaxYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['YMin'] == null || this.cashedTokens['YMin'] < plotTokensCash['YMin']) {
				plotTokensCash['YMin'] = this.cashedTokens['YMin'];
				plotTokensCash['MinYValuePointName'] = this.cashedTokens['MinYValuePointName'];
				plotTokensCash['MinYValuePointSeriesName'] = this.name;
			}
			if (plotTokensCash['MaxYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MaxYSumSeries']) {
				plotTokensCash['MaxYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MaxYSumSeriesName'] = this.name;
			}
			if (plotTokensCash['MinYSumSeries'] == null || this.cashedTokens['YSum'] > plotTokensCash['MinYSumSeries']) {
				plotTokensCash['MinYSumSeries'] = this.cashedTokens['YSum'];
				plotTokensCash['MinYSumSeriesName'] = this.name;
			}
			
			super.checkPlot(plotTokensCash);
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesFirstYValue') return this.points.length > 0 ? this.points[0].y : '';
			if (token == '%SeriesLastYValue') return this.points.length > 0 ? this.points[this.points.length-1].y : '';
			var pt:PiePlot2DPoint;
			if (token == '%SeriesYSum') return this.cashedTokens['YSum'];
			if (token == '%SeriesYMax') return this.cashedTokens['YMax'];
			if (token == '%SeriesYMin') return this.cashedTokens['YMin'];
			if (token == '%SeriesYAverage') {
				if (this.cashedTokens['YAverage'] == null)
					this.cashedTokens['YAverage'] = this.cashedTokens['YSum']/this.points.length;
				return this.cashedTokens['YAverage'];
			}
			return super.getSeriesTokenValue(token);
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@start_angle != undefined) this.startAngle = SerializerBase.getNumber(data.@start_angle);
			this.startAngle += PiePlotGlobalSeriesSettings(this.global).startAngle;
		}
	}
}