package com.anychart.piePlot.styles {
	import com.anychart.piePlot.data.PiePlot3DPoint;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.filters.BlurFilter;
	
	public class Pie3DAquaProgrammaticStyle extends ProgrammaticStyle {
		
		private var topGrd:Gradient;
		private var frontGrd:Gradient;
		private var smileGrd:Gradient;
		private var sidesFill:Fill;
		private var sidesStroke:Stroke;
		private var backSideFill:Fill;
		
		private function execCreateGradient(grd:Gradient,state:BackgroundBaseStyleState,opacity:Number,color1:String,color2:String):void {
			if (!color1 && !color2) return;
			if (color1 && color2)
				grd.deserialize(<gradient>
						<key color={color1.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
						<key color={color2.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
					</gradient>);
		} 
		
		override public function initialize(state:StyleState):void {
			super.initialize(state);
			var pieState:BackgroundBaseStyleState = BackgroundBaseStyleState(state);
			var isDynamic:Boolean = pieState.fill.isDynamic;
			var keys:Array;
			
			var clr:String = isDynamic ? pieState.fill.strColor : "%Color";
			if (clr == null || clr.indexOf("%Color") == -1) clr = "%Color";
			
			var percentColorIndex:int = ColorParser.getInstance().parse(clr);
			var opacity:Number= pieState.fill ? pieState.fill.opacity : 1;
			
			{
				/*
				<gradient angle="135">
					<key position="0" color="%Color"/>
					<key position="1" color="Blend(%Color,DarkColor(%Color),0.7)" />
				</gradient>
				*/
				if (isDynamic){
					var blend1Index:int = ColorParser.getInstance().parse("Blend("+clr+",DarkColor("+clr+"),0.7)");
					this.topGrd = new Gradient(1);
					this.topGrd.type = GradientType.LINEAR;
					this.topGrd.angle = -50;
					keys = [];
					keys.push(createKey(0, percentColorIndex, opacity, true));
					keys.push(createKey(1, blend1Index, opacity, true));
					createDynamicGradient(state, this.topGrd, keys);	
				}else {
					this.topGrd = new Gradient(1);
					this.topGrd.angle = -50;
					this.execCreateGradient(this.topGrd,pieState,opacity,"Blend("+clr+",DarkColor("+clr+"),0.7)","Blend("+clr+",DarkColor("+clr+"),0.7)");
				}
				
			}
			
			{
				/*
				<gradient  angle="-90">
					<key position="0" color="%Color"/>
					<key position="0.19" color="Blend(DarkColor(%Color),LightColor(%Color),0.3)"/>
					<key position="1" color="Blend(%Color,DarkColor(%Color),0.3)"/>
				</gradient>
				*/
				if (isDynamic){
					var blend2:int = ColorParser.getInstance().parse("Blend(DarkColor("+clr+"),LightColor("+clr+"),0.3)");
					var blend3:int = ColorParser.getInstance().parse("Blend("+clr+",DarkColor("+clr+"),0.3)");
					this.frontGrd = new Gradient(1);
					this.frontGrd.angle = 0;
					keys = [];
					keys.push(createKey(0,percentColorIndex,opacity,true));
					keys.push(createKey(0.19, blend2, opacity, true));
					keys.push(createKey(1, blend3, opacity, true)); 
					this.createDynamicGradient(state, this.frontGrd, keys);	
				}else {
					this.frontGrd = new Gradient(1);
					this.frontGrd.angle = 0;
					this.execCreateGradient(this.frontGrd,pieState,opacity,"Blend(DarkColor("+clr+"),LightColor("+clr+"),0.3)","Blend("+clr+",DarkColor("+clr+"),0.3)");
				}
				
			}
			
			{
				this.smileGrd = new Gradient(1);
				
				keys = [];
				keys.push(createKey(0, 0xFFFFFF, .29));
				keys.push(createKey(0.28, 0xFFFFFF, .8));
				keys.push(createKey(0.72, 0xFFFFFF, .16));
				keys.push(createKey(1, 0xFFFFFF, 0)); 
				/* keys.push(createKey(0, 0xFFFFFF, 1));
				keys.push(createKey(1, 0xFFFFFF, 1)); */
				
				this.smileGrd.entries = keys;
			}
			
			var tmp:String;
			
			{
				//<fill type="Solid" color="Blend(%Color,DarkColor(%Color),0.6)" opacity="1"/>
				if (isDynamic){
					this.sidesFill = new Fill();
					tmp = "Blend("+clr+",DarkColor("+clr+"),0.35)";
					this.sidesFill.deserialize(<fill type="Solid" color={tmp} opacity={opacity.toString()}/>, null, state.style);					
				}else {
					this.sidesFill = new Fill();
					this.sidesFill.deserialize(<fill type="Solid" color={"#"+pieState.fill.color.toString(16)} opacity={opacity.toString()}/>,null,pieState.style);
				}
				
			}
			
			{	
				if (isDynamic){
					this.backSideFill = new Fill();
					tmp = "Blend("+clr+",DarkColor("+clr+"),0.35)";
					this.backSideFill.deserialize(<fill type="Solid" color={tmp} opacity={opacity.toString()}/>, null, state.style);					
				}else {
					this.backSideFill = new Fill();
					this.backSideFill.deserialize(<fill type="Solid" color={"#"+pieState.fill.color.toString(16)} opacity={opacity.toString()}/>, null, state.style);
				}
				
			}
		}
		
		override public function draw(data:Object):void {
	        var point:PiePlot3DPoint = PiePlot3DPoint(data);
	        var pointState:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
	        var g:Graphics;
	        
	        {
		        g = point.getTopGraphics();
		        g.clear();
		        this.topGrd.beginGradientFill(g, point.topBounds, point.color);
		        point.drawTopSide();
		        g.endFill();
	        }
	        
	        var i:uint;
	        
	        if (point.frontSideDrawer != null) {
	        	g = point.getSmileShape().graphics;
	        	g.clear();
		        this.smileGrd.beginGradientFill(g, point.bounds, point.color);
		        point.drawSmile();
		        g.endFill();
		        
		        point.getSmileShape().filters = [
		        	new BlurFilter(4,4,1)
		        ];   
	        
		        for (i = 0;i<point.frontSideDrawer.length;i++) {
		        	g = point.frontSideDrawer.getGraphics(i);
			        g.clear();
			        this.frontGrd.beginGradientFill(g, point.bounds, point.color);
			        point.frontSideDrawer.draw(i);
			        g.endFill();
			        if (pointState.hatchFill){
			        	pointState.hatchFill.beginFill(g,point.hatchType,point.color);
			        	point.frontSideDrawer.draw(i);
			        	g.endFill();
			        }
		        }
	        }	   
	        
	        if (point.backSideDrawer != null) {	        
		        for (i = 0;i<point.backSideDrawer.length;i++) {
		        	g = point.backSideDrawer.getGraphics(i);
			        g.clear();
			        this.backSideFill.begin(g, point.bounds, point.color);
			        point.backSideDrawer.draw(i);
			        g.endFill();
			        if (pointState.hatchFill){
			        	pointState.hatchFill.beginFill(g,point.hatchType,point.color);
			        	point.backSideDrawer.draw(i);
			        	g.endFill();
			        }
		        }
	        }	     
	        
	        g = point.getStartGraphics();
	        g.clear();
	        //g.lineStyle(1, 0,0.07);
	        this.sidesFill.begin(g, null, point.color);	        
	        point.drawStartSide();
	        g.endFill();
	        
	        g = point.getEndGraphics();
	        g.clear();
	        //g.lineStyle(1, 0,0.07);
	        this.sidesFill.begin(g, null, point.color);
	        point.drawEndSide();
	        g.endFill();
	        
	        
		}

	}
}