package com.anychart.piePlot.legend {
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.piePlot.PiePlot;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.categories.BaseCategoryInfo;
	import com.anychart.seriesPlot.controls.legend.LegendAdapter;
	import com.anychart.seriesPlot.controls.legend.LegendAdaptiveItem;

	public final class PiePlotLegendAdapter extends LegendAdapter {
		
		public function PiePlotLegendAdapter(plot:PiePlot) {
			super(plot);
		}
		
		override public function getLegendDataBySource(source:String, item:XML, container:ILegendItemsContainer):void {
			if (source == "categories") {
				var categoryName:String = item.@category != undefined ? SerializerBase.getLString(item.@category) : null;
				var xCategories:Array = PiePlot(plot).categoriesList;
				for (var i:uint = 0;i<xCategories.length;i++) {
					var category:BaseCategoryInfo = xCategories[i];
					if (categoryName == null || categoryName == category.name.toLowerCase()) {
						category.initColor();
						container.addItem(new LegendAdaptiveItem(category.color, 0, -1, true, category));
					} 
				}
			}else {
				super.getLegendDataBySource(source, item, container);
			}
		}
	}
}