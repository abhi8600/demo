package com.anychart.piePlot.data {
	import com.anychart.animation.Animation;
	import com.anychart.piePlot.PiePlot;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class PiePlot3DPoint extends PiePlot2DPoint {
		
		private var startSideShape:Sprite;
		private var endSideShape:Sprite;
		private var topSideShape:Sprite;
		private var topSideShapeSmile:Shape;
		
		public var frontSideDrawer:PiePlot3DPointFrontSideDrawer;
		public var backSideDrawer:PiePlot3DPointBackSideDrawer;
		
		public function getSmileShape():Shape {
			return this.topSideShapeSmile;
		}
		
		public function drawSmile():void {
			var g:Graphics = this.topSideShapeSmile.graphics;
			var center:Point = this.getCenter();
			
			var wRadius:Number = PiePlot2DSeries(this.series).outerRadius;
			var hRadius:Number = PiePlot3DSeries(this.series).outerYRadius;
			
			var dH:Number = PiePlot(this.plot).getRadiusByFactor(.03);
			
			var j:Number;
			var rX:Number;
			var rY:Number;
			
			for (var i:uint = 0;i<this.frontSideDrawer.length;i++) {
				var start:Number = this.frontSideDrawer.startAngles[i] - .275;
				var end:Number = this.frontSideDrawer.endAngles[i] + .275;
				if (end < start)
					end += 360;
				for (j = start;j<=end;j++) {
					rX = (dH/2)*Math.abs(Math.sin(j*Math.PI/180));
					rY = (dH/2)*Math.abs(Math.sin(j*Math.PI/180));
					if (j == start) 
						g.moveTo(center.x + DrawingUtils.getPointX(wRadius - rX, j),
						   	     center.y + DrawingUtils.getPointY(hRadius - rY, j));
					else
						g.lineTo(center.x + DrawingUtils.getPointX(wRadius - rX, j),
							     center.y + DrawingUtils.getPointY(hRadius - rY, j));
				}
				rX = (dH/2)*Math.abs(Math.sin(end*Math.PI/180));
				rY = (dH/2)*Math.abs(Math.sin(end*Math.PI/180));
				g.lineTo(center.x + DrawingUtils.getPointX(wRadius - rX, end),
					center.y + DrawingUtils.getPointY(hRadius - rY, end));
				for (j = end;j>=start;j--) {
					rX = (dH/2)*Math.abs(Math.sin(j*Math.PI/180));
					rY = (dH/2)*Math.abs(Math.sin(j*Math.PI/180));
					
					g.lineTo(center.x + DrawingUtils.getPointX(wRadius + rX, j),
						     center.y + DrawingUtils.getPointY(hRadius + rY, j));
				}				
				rX = (dH/2)*Math.abs(Math.sin(start*Math.PI/180));
				rY = (dH/2)*Math.abs(Math.sin(start*Math.PI/180));
				g.lineTo(center.x + DrawingUtils.getPointX(wRadius - rX, start),
					center.y + DrawingUtils.getPointY(hRadius - rY, start));
			}
		}
		
		private const ANGLE_DRIFT:Number = 0.001;
		
		override public function calculateAngles():void {
			super.calculateAngles();
			
			this.topSideShapeSmile = new Shape();
			this.startSideShape = new Sprite();
			this.endSideShape = new Sprite();
			this.topSideShape = new Sprite();
			this.topSideShape.addChild(this.topSideShapeSmile);
			
			PiePlot(this.plot).depthManager.addTopSide(this.topSideShape);
			PiePlot(this.plot).depthManager.addSide(this.startAngle + this.ANGLE_DRIFT, this.startSideShape, this.series.index);
			PiePlot(this.plot).depthManager.addSide(this.endAngle - this.ANGLE_DRIFT, this.endSideShape, this.series.index);
			
			this.setContainerProperties(this.startSideShape);
			this.setContainerProperties(this.endSideShape);
			this.setContainerProperties(this.topSideShape);
			
			var i:uint;
			
			if (this.hasFrontSide()) {
				this.frontSideDrawer = new PiePlot3DPointFrontSideDrawer();
				this.frontSideDrawer.initialize(this);
				PiePlot(this.plot).depthManager.addFrontSide(this.frontSideDrawer, this.series.index);
				
				for (i = 0;i<this.frontSideDrawer.length;i++)
					this.setContainerProperties(this.frontSideDrawer.getSprite(i));
			}
			
			if (this.hasBackSide()) {
				this.backSideDrawer = new PiePlot3DPointBackSideDrawer();
				this.backSideDrawer.initialize(this);
				PiePlot(this.plot).depthManager.addBackSide(this.backSideDrawer, this.series.index);
				
				for (i = 0;i<this.backSideDrawer.length;i++)
					this.setContainerProperties(this.backSideDrawer.getSprite(i));
			}
			
			this.initializeAnimation();
		}
		
		public function getTopGraphics():Graphics { return this.topSideShape.graphics; }
		public function getStartGraphics():Graphics { return this.startSideShape.graphics; }
		public function getEndGraphics():Graphics { return this.endSideShape.graphics; }
		
		override public function getCenter(pt:Point=null):Point {
			if (pt == null)
				pt = new Point();
			pt.x = PiePlot(this.plot).centerPoint.x;
			pt.y = PiePlot(this.plot).centerPoint.y;
			if (PiePlotSettings(this.settings).exploded) {
				var xR:Number = PiePlot(this.plot).getExplodeRadius();
				var yR:Number = PiePlot(this.plot).getExplodeYRadius();
				
				var centerAngle:Number = (this.startAngle+this.endAngle)/2;
				pt.x += DrawingUtils.getPointX(xR, centerAngle);
				pt.y += DrawingUtils.getPointY(yR, centerAngle);
			}
			return pt;
		}
		
		public var topBounds:Rectangle;
		
		override protected function calcBounds():void {
			var center:Point = this.getCenter();
			var series:PiePlot3DSeries = PiePlot3DSeries(this.series);
			var h:Number = PiePlot(this.plot).get3DHeight();
			
			this.topBounds = new Rectangle();
			this.topBounds.x = center.x - series.outerRadius;
			this.topBounds.y = center.y - series.outerYRadius;
			this.topBounds.width = series.outerRadius*2;
			this.topBounds.height = series.outerYRadius*2;
			
			this.bounds = this.topBounds.clone();
			this.bounds.height += h;
		}
		
		
		private function hasFrontSide():Boolean {
			return PiePlot3DPointFrontSideDrawer.enabled(this.startAngle, this.endAngle);
		}
		
		private function hasBackSide():Boolean {
			return PiePlot3DPointBackSideDrawer.enabled(this.startAngle, this.endAngle);
		}
		
		override public function drawShape():void {
			return;
		}
		
		public function drawTopSide():void {
			var g:Graphics = this.topSideShape.graphics;
			var outerXR:Number = PiePlot2DSeries(this.series).outerRadius;
			var outerYR:Number = PiePlot3DSeries(this.series).outerYRadius;
			var innerXR:Number = PiePlot2DSeries(this.series).innerRadius;
			var innerYR:Number = PiePlot3DSeries(this.series).innerYRadius;
			var center:Point = this.getCenter();
			var state: BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			
			DrawingUtils.drawArc(g, center.x, center.y,
								 this.startAngle, this.endAngle,
								 outerYR, outerXR,0, true);
			
			DrawingUtils.drawArc(g, center.x, center.y,
								 this.endAngle, this.startAngle,
								 innerYR, innerXR,0, false);
			g.lineTo(center.x+DrawingUtils.getPointX(outerXR, this.startAngle),
					 center.y+DrawingUtils.getPointY(outerYR, this.startAngle));
			
			if (state.hatchFill){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				DrawingUtils.drawArc(g, center.x, center.y,
								 this.startAngle, this.endAngle,
								 outerYR, outerXR,0, true);
			
				DrawingUtils.drawArc(g, center.x, center.y,
									 this.endAngle, this.startAngle,
									 innerYR, innerXR,0, false);
				g.lineTo(center.x+DrawingUtils.getPointX(outerXR, this.startAngle),
						 center.y+DrawingUtils.getPointY(outerYR, this.startAngle));
						 
				g.endFill();
			}
			
		}
		
		public function drawStartSide():void {
			var g:Graphics = this.startSideShape.graphics;
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState); 
			this.drawSide(g, this.startAngle);
			
			if (state.hatchFill){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				this.drawSide(g, this.startAngle);
				g.endFill();
			}
		}
		
		public function drawEndSide():void {
			var g:Graphics = this.endSideShape.graphics;
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			this.drawSide(this.endSideShape.graphics, this.endAngle);
			if (state.hatchFill){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				this.drawSide(g, this.endAngle);
				g.endFill();
			}
			
		}
		
		private function drawSide(g:Graphics,angle:Number):void {
			var center:Point = this.getCenter();
			
			var outerXR:Number = PiePlot2DSeries(this.series).outerRadius;
			var outerYR:Number = PiePlot3DSeries(this.series).outerYRadius;
			var innerXR:Number = PiePlot2DSeries(this.series).innerRadius;
			var innerYR:Number = PiePlot3DSeries(this.series).innerYRadius;
			
			var h:Number = PiePlot(this.plot).get3DHeight();
			
			var pt1:Point = new Point(center.x + DrawingUtils.getPointX(innerXR, angle),
									  center.y + DrawingUtils.getPointY(innerYR, angle));
			var pt2:Point = new Point(center.x + DrawingUtils.getPointX(outerXR, angle),
					 				  center.y + DrawingUtils.getPointY(outerYR, angle));
			var pt3:Point = new Point(pt2.x, pt2.y + h);
			var pt4:Point = new Point(pt1.x, pt1.y + h);
			
			g.moveTo(pt1.x, pt1.y);
			g.lineTo(pt2.x, pt2.y);
			g.lineTo(pt3.x, pt3.y);
			g.lineTo(pt4.x, pt4.y);
			g.lineTo(pt1.x, pt1.y);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			this.getCenter(pos);
			
			var outerXR:Number = PiePlot2DSeries(this.series).outerRadius;
			var outerYR:Number = PiePlot3DSeries(this.series).outerYRadius;
			var innerXR:Number = PiePlot2DSeries(this.series).innerRadius;
			var innerYR:Number = PiePlot3DSeries(this.series).innerYRadius;
			
			var angle:Number;
			var xRadius:Number;
			var yRadius:Number;
			switch (anchor) {
				case Anchor.CENTER:
					angle = this.startAngle+this.sweepAngle/2;
					xRadius = (innerXR+outerXR)/2;
					yRadius = (innerYR+outerYR)/2;
					break;
				case Anchor.CENTER_BOTTOM:
					angle = this.startAngle+this.sweepAngle/2;
					xRadius = innerXR;
					yRadius = innerYR;
					break;
				case Anchor.CENTER_LEFT:
					angle = this.startAngle;
					xRadius = (innerXR+outerXR)/2;
					yRadius = (innerYR+outerYR)/2;
					break;
				case Anchor.CENTER_RIGHT:
					angle = this.endAngle;
					xRadius = (innerXR+outerXR)/2;
					yRadius = (innerYR+outerYR)/2;
					break;
				case Anchor.CENTER_TOP:
					angle = this.startAngle + this.sweepAngle/2;
					xRadius = outerXR;
					yRadius = outerYR;
					break;
				case Anchor.LEFT_BOTTOM:
					angle = this.startAngle;
					xRadius = innerXR;
					yRadius = innerYR;
					break;
				case Anchor.LEFT_TOP:
					angle = this.startAngle;
					xRadius = outerXR;
					yRadius = outerYR;
					break;
				case Anchor.RIGHT_BOTTOM:
					angle = this.endAngle;
					xRadius = innerXR;
					yRadius = innerYR;
					break;
				case Anchor.RIGHT_TOP:
					angle = this.endAngle;
					xRadius = outerXR;
					yRadius = outerYR;
					break;
			}
			pos.x += DrawingUtils.getPointX(xRadius, angle);
			pos.y += DrawingUtils.getPointY(yRadius, angle);
		}
		
		private var connectorShapeAdded:Boolean = false;
		
		override protected function drawConnectors(firstPoint:Point, secondPoint:Point, thirdPoint:Point):void {
			var plotRadius:Number = (this.series as Object).outerRadius;
			var plotYRadius:Number = PiePlot(this.plot).getYRadius(plotRadius);
			var angle:Number = (this.startAngle+this.endAngle)/2;
			
			firstPoint.y += PiePlot(this.plot).get3DHeight()/2;
			firstPoint.x += DrawingUtils.getPointX(plotRadius,angle);
			firstPoint.y += DrawingUtils.getPointY(plotYRadius,angle);
			
			var g:Graphics = this.connectorShape.graphics;
			this.doConnectorDrawing(firstPoint, secondPoint, thirdPoint, g);
			if (!this.connectorShapeAdded){	
				if (Math.sin(angle*Math.PI/180)<0)
					PiePlot(this.plot).depthManager.addBackSideConnector(this.connectorShape);			
				else
					PiePlot(this.plot).depthManager.addFrontSideConnector(this.connectorShape);
				this.connectorShapeAdded = true;
			}
		}
		
		
		override public function reset():void {
			if (this.startSideShape) {
				while (this.startSideShape.numChildren > 0) this.startSideShape.removeChildAt(0);
				this.startSideShape.graphics.clear();
				if (this.startSideShape.parent) this.startSideShape.parent.removeChild(this.startSideShape);
				this.startSideShape = null;
			}
			
			if (this.endSideShape) {
				while (this.endSideShape.numChildren > 0) this.endSideShape.removeChildAt(0);
				this.endSideShape.graphics.clear();
				if (this.endSideShape.parent) this.endSideShape.parent.removeChild(this.endSideShape);
				this.endSideShape = null;
			}
			
			if (this.topSideShapeSmile) {
				this.topSideShapeSmile.graphics.clear();
				if (this.topSideShapeSmile.parent) this.topSideShapeSmile.parent.removeChild(this.topSideShapeSmile);
				this.topSideShapeSmile = null;
			}
			
			if (this.topSideShape) {
				while (this.topSideShape.numChildren > 0) this.topSideShape.removeChildAt(0);
				this.topSideShape.graphics.clear();
				if (this.topSideShape.parent) this.topSideShape.parent.removeChild(this.topSideShape);
				this.topSideShape = null;
			}
			
			if (this.frontSideDrawer) {
				this.frontSideDrawer.clear();
				this.frontSideDrawer = null;
			}
			if (this.backSideDrawer) {
				this.backSideDrawer.clear();
				this.backSideDrawer = null;
			}
		}
		
		//---------------------------------------------------------------------------
		//							ANIMATION
		//---------------------------------------------------------------------------
		
		public var animation:Animation;
		private function getSides():Array {
			var res:Array = [];
			if (startSideShape) res.push(startSideShape);
			if (endSideShape) res.push(endSideShape);
			if (topSideShape) res.push(topSideShape);
			var i:uint;
			if (frontSideDrawer) {
				for (i = 0;i<frontSideDrawer.length;i++)
					res.push(frontSideDrawer.getSprite(i));
			}
			if (backSideDrawer) {
				for (i = 0;i<backSideDrawer.length;i++)
					res.push(backSideDrawer.getSprite(i));
			}
			if (connectorShape)
				res.push(connectorShape);
			return res;
		}
		
		private function initializeAnimation():void {
			if (this.animation) {
				var sides:Array = this.getSides();
				for each (var side:DisplayObject in sides)
					PiePlot(this.plot).registerPiePointAnimation(side, this, this.animation);
				this.animation = null;
			}
		}
	}
}