package com.anychart.piePlot.styles {
	import com.anychart.piePlot.data.PiePlot2DPoint;
	import com.anychart.piePlot.data.PiePlot2DSeries;
	import com.anychart.styles.programmaticStyles.CircleAquaStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class PieAquaProgrammaticStyle extends CircleAquaStyle {
		
		override public function initialize(state:StyleState):void {
			super.initialize(state);
			
			// Init main fill gradient.
			{
				this.fillGrd = new Gradient(1);
   				this.fillGrd.type = GradientType.RADIAL;
   				this.fillGrd.focalPoint = 0.5;
            	this.fillGrd.angle = -145;
            	
            	var ents:Array = [];
            	var opacity:Number=1;

            	ents.push(createKey(0,darkIndex,opacity,true));
            	ents.push(createKey(0.95,lightIndex,opacity,true));
            	ents.push(createKey(1,lightIndex,opacity,true));
            	
            	createDynamicGradient(state,this.fillGrd,ents);
			}
			
			// Init dark border gradient.
			{
				this.borderGrd = new Gradient(1);
	        	this.borderGrd.type = GradientType.RADIAL;
    		    this.borderGrd.focalPoint=0;
	        	this.borderGrd.angle=0;

	        	ents = [];
	        	opacity=1;
	        	
	        	ents.push(createKey(0,0,0));
	        	ents.push(createKey(1-0.05,0,0));
	        	ents.push(createKey(1,0,100.0/255.0*opacity));
	        	
	        	this.borderGrd.entries=ents;
   			}
		}
		
		override public function draw(data:Object):void {
	        
	        super.draw(data);
	        var point:PiePlot2DPoint = PiePlot2DPoint(data);
	        var series:PiePlot2DSeries = PiePlot2DSeries(point.series);
	        
	        if (series.innerRadius > 0) {
	        	
	        	var state:BackgroundBaseStyleState = point.getCurrentState();
	        	var bounds:Rectangle = point.bounds;
	        	var g:Graphics = point.getGraphics();
	        	
	        	g.lineStyle();
	        	
	        	var n:Number = series.innerRadius/series.outerRadius;
	        	
        		var gr:Gradient = new Gradient(1);
        		gr.angle = 0;
        		gr.focalPoint = 0;
        		gr.type = GradientType.RADIAL;
        		var ents:Array = [];
        		
        		ents.push(createKey(n, 0, 100/255*state.fill.opacity));
        		if (series.index == 0)
        			ents.push(createKey(n+.1,0,0));
        		else
        			ents.push(createKey(n+.05,0,0));
        		ents.push(createKey(1,0,0));
	        		
        		gr.entries = ents;
        		
        		gr.beginGradientFill(g, bounds, point.color);
        		point.drawShape();
        		g.endFill();
	        } 
		}
	}
}