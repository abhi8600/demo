package com.anychart.piePlot.animation {
	import com.anychart.animation.AnimationDisplayObjectComplexTarget;
	import com.anychart.piePlot.data.PiePlot2DPoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class PieSliceAnimationTarget extends AnimationDisplayObjectComplexTarget {
		
		private var slice:PiePlot2DPoint;
		private var angle:Number;
		private var maxR:Number;
		
		public function PieSliceAnimationTarget(slice:PiePlot2DPoint) {
			this.slice = slice;
			super();
		}
		
		override public function initialize():void {
			var bounds:Rectangle = this.slice.plot.getBounds();
			this.maxR = new Point(bounds.width, bounds.height).length/2
			 
			this.angle = (this.slice.startAngle+this.slice.endAngle)/2;
			this.angle *= Math.PI/180;
			
			this.slice = null;
			this.setX(0);
			this.setY(0);
			super.initialize();
		}
		
		override protected function setInitialX():void {
			var startX:Number = this.container.x + this.maxR*Math.cos(this.angle);
			
			this.x.initialize(startX, this.container.x);
			if (!this.x.isAnimated())
				this.x = null;
			else
				this.container.x = startX;
		}
		
		override protected function setInitialY():void {
			var startY:Number = this.container.y + this.maxR*Math.sin(this.angle);
			
			this.y.initialize(startY, this.container.y);
			if (!this.y.isAnimated())
				this.y = null;
			else
				this.container.y = startY;
		}
	}
}