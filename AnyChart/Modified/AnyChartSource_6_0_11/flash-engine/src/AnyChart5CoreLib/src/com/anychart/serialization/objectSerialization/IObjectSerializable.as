package com.anychart.serialization.objectSerialization {
	public interface IObjectSerializable {
		function serialize(res:Object = null):Object;
	}
}