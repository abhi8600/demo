package com.anychart.errors {
	
	public class FeatureNotSupportedError extends Error {
		
		public function get failMessage():String {
			return 'This chart type is not supported by Oracle APEX version of AnyChart.<br />'
+'To learn more and update your version please visit AnyChart website:<br />'
+'<u><font color="#0000FF">http://www.anychart.com/products/anychart/docs/oracle_version_vs_regular.php</font></u>';
		}
		
		public function get url():String { return "http://www.anychart.com/products/anychart/docs/oracle_version_vs_regular.php"; }
		
		public function FeatureNotSupportedError(message:String) {
			super(message);
		}

	}
}