package com.anychart.events
{
	import flash.events.Event;
	
	public final class PointsEvent extends Event
	{
		public static const MULTIPLE_POINTS_SELECT:String = "multiplePointsSelect";
		public static const MULTIPLE_POINTS_DESELECT:String = "multiplePointsDeselect";
		
		public var points:Array;
		
		public function PointsEvent(type:String, points:Array) {
			this.points = points;
			super(type);
		}
		
		override public function clone():Event {
			return new PointsEvent(this.type, this.points);
		}

	}
}