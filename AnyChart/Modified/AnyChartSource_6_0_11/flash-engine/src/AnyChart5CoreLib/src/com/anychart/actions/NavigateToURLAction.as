package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	
	internal final class NavigateToURLAction extends Action {
		private var request:URLRequest;
		
		private var url:String;
		private var isDynamicUrl:Boolean;
		private var urlFormater:TextFormater;
		
		private var isDynamicTarget:Boolean;
		private var urlTargetFormater:TextFormater;
		private var urlTarget:String;
		
		public function NavigateToURLAction(chart:IAnyChart) {
			super(chart);
		}
		
		override public function execute(formater:IFormatable):Boolean {
			this.request.url = (this.isDynamicUrl) ? this.urlFormater.getValue(formater.getTokenValue, formater.isDateTimeToken) : this.url;
			navigateToURL(this.request, this.isDynamicTarget ? this.urlTargetFormater.getValue(formater.getTokenValue, formater.isDateTimeToken) : this.urlTarget);
			return false;
		}
		
		override public function deserialize(data:XML):void {
			this.isValidXML = data.@url != undefined;
			if (this.isValidXML) {
				this.url = SerializerBase.getString(data.@url);
				this.isDynamicUrl = FormatsParser.isDynamic(this.url);
				if (this.isDynamicUrl)
					this.urlFormater = FormatsParser.parse(this.url);
				
				this.request = new URLRequest();
				
				this.urlTarget = "_blank";
				if (data.@target != undefined) this.urlTarget = SerializerBase.getString(data.@target);
				if (data.@url_target != undefined) this.urlTarget = SerializerBase.getString(data.@url_target);
				
				this.isDynamicTarget = FormatsParser.isDynamic(this.urlTarget);
				if (this.isDynamicTarget)
					this.urlTargetFormater = FormatsParser.parse(this.urlTarget);
			} 
		}
	}
}