package com.anychart.serialization {
	import com.anychart.styles.IStyle;
	
	public interface IStyleSerializable {
		function deserialize(data:XML, style:IStyle = null):void;
	}
}