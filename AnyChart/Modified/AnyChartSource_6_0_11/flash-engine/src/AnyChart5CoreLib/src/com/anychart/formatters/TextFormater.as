package com.anychart.formatters
{
	import com.anychart.formatters.terms.ITerm;
	
	public class TextFormater
	{
		private var formater:ITerm;
		
		public function TextFormater(formater:ITerm) {
			this.formater = formater;
		}

		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return formater.getValue(getTokenFunction, isDateTimeToken);
		}
		
	}
}