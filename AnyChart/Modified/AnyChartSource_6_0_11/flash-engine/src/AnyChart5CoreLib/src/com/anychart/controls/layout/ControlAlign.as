package com.anychart.controls.layout {
	public final class ControlAlign {
		public static const NEAR:uint = 0;
		public static const CENTER:uint = 1;
		public static const FAR:uint = 2;
		public static const SPREAD:uint = 3;
	}
}