package com.anychart.visual.stroke
{
	import com.anychart.serialization.IStyleSerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.CapsStyle;
	import flash.display.Graphics;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.geom.Rectangle;
	
	public class Stroke implements IStyleSerializable {
		// Properties.
		public var enabled:Boolean;
		public var thickness:Number;
		public var color:uint;
		public var isDynamic:Boolean;
		public var opacity:Number;
		public var pixelHinting:Boolean;
		public var scaleMode:String;
		public var caps:String;
		public var joints:String;
		public var miterLimit:Number;
		
		public var type:uint;
		
		public var gradient:Gradient;
		
		public var dashed:Boolean;
		public var dashLength:Number;
		public var spaceLength:Number;
		
		public function Stroke() {
			this.type = StrokeType.SOLID;
			this.dashed = false;
			this.dashLength = 2;
			this.spaceLength = 2;
			this.miterLimit = 0;
			this.joints="round";
			this.caps="round";
			this.scaleMode=LineScaleMode.NONE;
			this.pixelHinting=false;
			this.opacity=1;
			this.isDynamic = false;
			this.color=0x000000;
			this.thickness=1;
			this.enabled=true;
		}
		
		// Methods.
		public function apply(target:Graphics,rc:Rectangle=null,styleColor:uint=0):void {
			if(!this.enabled) {
				this.reset(target);
				return;
			}
			
			switch(this.type) {
				case StrokeType.SOLID:
					target.lineStyle(this.thickness,
									 this.isDynamic ? (ColorParser.getInstance().getDynamicColor(this.color, styleColor)) : 
									 				   this.color,
									 this.opacity,this.pixelHinting,this.scaleMode,this.caps,this.joints,this.miterLimit);
					break;
				
				case StrokeType.GRADIENT:
					target.lineStyle(this.thickness,
									 this.isDynamic ? (ColorParser.getInstance().getDynamicColor(this.color, styleColor)) : 
									 				   this.color,
									 this.opacity,this.pixelHinting,this.scaleMode,this.caps,this.joints,this.miterLimit);
					this.gradient.lineGradientStyle(target,rc,styleColor);
					break;
			}
		}
		
		public function reset(target:Graphics):void {
			target.lineStyle(0,0,0);
		}
		
		public function deserialize(data:XML, style:IStyle = null):void {
			if(data.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(data.@enabled);
			if(!this.enabled) return;
			
			if(data.@type!=undefined) {
				switch (SerializerBase.getEnumItem(data.@type)) {
					case "solid": this.type = StrokeType.SOLID; break;
					case "gradient": this.type = StrokeType.GRADIENT; break;
				}
			}
			
			if(data.@color!=undefined) {
				this.color=SerializerBase.getColor(data.@color);
				if (style != null && SerializerBase.isDynamicColor(data.@color)) {
					this.isDynamic = true;
					style.addDynamicColor(this.color);
				}else {
					this.isDynamic = false;
				}
			}
			if(data.@opacity!=undefined) this.opacity=SerializerBase.getNumber(data.@opacity);
			if(data.@thickness!=undefined) this.thickness=SerializerBase.getNumber(data.@thickness);
			if (data.@caps != undefined) {
				switch (SerializerBase.getEnumItem(data.@caps)) {
					case "none": this.caps = CapsStyle.NONE; break;
					case "round": this.caps = CapsStyle.ROUND; break;
					case "square": this.caps = CapsStyle.SQUARE; break;
				}
			}
			if (data.@joints != undefined) {
				switch (SerializerBase.getEnumItem(data.@joints)) {
					case "bevel": this.joints = JointStyle.BEVEL; break;
					case "miter": this.joints = JointStyle.MITER; break;
					case "round": this.joints = JointStyle.ROUND; break;
				}
			}
			if (data.@pixel_hinting != undefined) this.pixelHinting = SerializerBase.getBoolean(data.@pixel_hinting);
			if(this.type == StrokeType.GRADIENT && data.gradient[0]!=null) {
				this.gradient = new Gradient(this.opacity);
				this.gradient.deserialize(data.gradient[0],style);
			}
			
			if (this.type == StrokeType.GRADIENT && (this.gradient == null || this.gradient.entries.length == 0)) {
				this.type = StrokeType.SOLID;
				this.gradient = null;
			}
			
			if (data.@dashed != undefined)
				this.dashed = SerializerBase.getBoolean(data.@dashed);
				
			if (this.dashed) {
				if (data.@dash_length != undefined) this.dashLength = SerializerBase.getNumber(data.@dash_length);
				if (data.@space_length != undefined) this.spaceLength = SerializerBase.getNumber(data.@space_length);
			}
		}
	}
}