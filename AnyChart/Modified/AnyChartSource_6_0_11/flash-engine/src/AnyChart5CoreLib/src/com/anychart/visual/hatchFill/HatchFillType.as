package com.anychart.visual.hatchFill
{
	public final class HatchFillType
	{
		public static const BACKWARD_DIAGONAL:uint			= 0;
		public static const FORWARD_DIAGONAL:uint			= 1;
		public static const HORIZONTAL:uint					= 2;
		public static const VERTICAL:uint					= 3;
		public static const DASHED_BACKWARD_DIAGONAL:uint 	= 4;
		public static const GRID:uint						= 5;
		public static const DASHED_FORWARD_DIAGONAL:uint    = 6;
		public static const DASHED_HORIZONTAL:uint 			= 7;
		public static const DASHED_VERTICAL:uint			= 8;
		public static const DIAGONAL_CROSS:uint 			= 9;
		public static const DIAGONAL_BRICK:uint 			= 10;
		public static const DIVOT:uint 						= 11;
		public static const HORIZONTAL_BRICK:uint 			= 12;
		public static const VERTICAL_BRICK:uint 			= 13;
		public static const CHECKER_BOARD:uint 				= 14;
		public static const CONFETTI:uint 					= 15;
		public static const PLAID:uint 						= 16;
		public static const SOLID_DIAMOND:uint 				= 17;
		public static const ZIG_ZAG:uint 					= 18;
		public static const WEAVE:uint 						= 19;
		public static const PERCENT_05:uint					= 20;
		public static const PERCENT_10:uint					= 21;
		public static const PERCENT_20:uint					= 22;
		public static const PERCENT_25:uint					= 23;
		public static const PERCENT_30:uint					= 24;
		public static const PERCENT_40:uint					= 25;
		public static const PERCENT_50:uint					= 26;
		public static const PERCENT_60:uint					= 27;
		public static const PERCENT_70:uint					= 28;
		public static const PERCENT_75:uint					= 29;
		public static const PERCENT_80:uint					= 30;
		public static const PERCENT_90:uint					= 31;
		
		public static const NONE:uint = 40;

	}
}