package com.anychart.controls {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.text.BaseTitle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	internal final class ControlTitle extends BaseTitle {
		
		public var space:Number;
		public var minWidth:Number;
		private var separator:Stroke;
		private var info:TextElementInformation;
		private var g:Graphics;
		
		private var separatorBounds:Rectangle;
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data.title[0], resources);
			this.info = this.createInformation();
			
			if (data.title_separator[0] != null) {
				this.separator = new Stroke();
				this.separator.deserialize(data.title_separator[0]);
				
				this.separatorBounds = new Rectangle();
			}
		}
		
		public function initialize(container:Sprite):void {
			this.g = container.graphics;
		}
		
		public function calculateSpace(getToken:Function, isDateTimeToken:Function):void {
			this.info.formattedText = this.isDynamicText ? this.dynamicText.getValue(getToken, isDateTimeToken) : this.text;
			this.getBounds(this.info);
			
			this.space = this.info.rotatedBounds.height + this.padding;
			if (this.separator != null)
				this.space += this.separator.thickness;
				
			this.minWidth = this.info.rotatedBounds.width;
		}
		
		public function applySpace(contentBounds:Rectangle):void {
			contentBounds.y += this.space;
			contentBounds.height -= this.space;
		}
		
		public function drawTitle(contentBounds:Rectangle):void {
			var y:Number = this.padding;
			var x:Number;
			switch (this.align) {
				case AbstractAlign.NEAR:
					x = contentBounds.x;
					break;
				case AbstractAlign.CENTER:
					x = contentBounds.x + (contentBounds.width - this.info.rotatedBounds.width)/2;
					break;;
				case AbstractAlign.FAR:
					x = contentBounds.right - this.info.rotatedBounds.width;
					break;
			}
			
			this.draw(this.g, x, y, this.info, 0, 0);
			
			if (this.separator != null) {
				
				y += this.info.rotatedBounds.height + this.separator.thickness/2;
				
				this.separatorBounds.x = contentBounds.left;
				this.separatorBounds.y = y;
				this.separatorBounds.width = contentBounds.width;
				this.separatorBounds.height = this.separator.thickness/2;
				
				this.separator.apply(g, separatorBounds);
				g.moveTo(contentBounds.left, y);
				g.lineTo(contentBounds.right, y);
				g.lineStyle();
			}
		}
	}
}