package com.anychart.formatters.terms
{
	public class TermTokenMultiScale extends TermToken implements ITerm
	{
		private var scale:Array;

		public function TermTokenMultiScale(tokenName:String, parentCall:ITerm, scale:Array) {
			super(tokenName, parentCall);
			this.scale = scale;
		}

		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return parentCall.getValue(getTokenFunction, isDateTimeToken) + execFormating(getTokenFunction(tokenName));
		}

		private function execFormating(value:String):String {
			var numValue:Number = Number(value);
			if (isNaN(numValue)) return String(value);
			var isNegative:Boolean = numValue < 0;
			if (numValue < 0) numValue = -numValue;
			
			var unitValue:Number;
			var newValue:Number;
			var res:String;

			var ost:Number = numValue;
			var signFound:Boolean = false;
			var summ:Number = 0;
			var tmp:Number = 0;
			
			var sVal:Number = 0;
			var sDiv:Number = 0;
			var uVal:String = "";
			
			for (var i:int = scale.length - 1;i>=0;i--) {
				var divider:Number = scale[i][2];
				newValue = Math.floor(ost/divider);
				tmp = newValue * divider;
				if (signFound) {
					ost -= tmp;
					summ += tmp;
				} else if (newValue != 0) {
					ost -= tmp;
					sVal = tmp;
					sDiv = divider;
					uVal = scale[i][1];
					signFound = true;
				}
			}
			
			if (!signFound) {
				ost = 0;
				sVal = 0;
				sDiv = scale[0][0];
				summ = numValue;
				uVal = scale[0][1];
			}
			
			newValue = (sVal + summ + ost)/sDiv;
			res = formatNumber(newValue, isNegative)
				  +uVal;
			return formatString(res);
		}

	}
}