package com.anychart.visual.stroke {
	
	public final class StrokeType {
		public static const SOLID:uint=0;
		public static const GRADIENT:uint=1;
	}
}