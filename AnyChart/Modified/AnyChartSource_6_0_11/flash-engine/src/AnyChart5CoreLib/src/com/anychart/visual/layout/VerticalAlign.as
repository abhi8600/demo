package com.anychart.visual.layout {
	
	public final class VerticalAlign {
		public static const TOP:uint = 0;
		public static const CENTER:uint = 1;
		public static const BOTTOM:uint = 2;
	}
}