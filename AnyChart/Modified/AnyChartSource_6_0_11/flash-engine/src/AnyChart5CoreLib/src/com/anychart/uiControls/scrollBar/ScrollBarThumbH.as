package com.anychart.uiControls.scrollBar
{
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class ScrollBarThumbH extends ScrollBarButton
	{
		public function ScrollBarThumbH() {
			super();
			this.dh = 2;
		}
		
		override public function draw():void {
			super.draw();
			var s:Rectangle = size;
			
			var cX:Number = s.width * 0.5;
			var cY:Number = s.height * 0.25;
		
			var g:Graphics = this.graphics;
			g.lineStyle(1, 0x494949,05);
			g.beginFill(0,0);
			g.moveTo(cX-4,cY);
			g.lineTo(cX-4,s.height-cY);
			g.moveTo(cX-2,cY);
			g.lineTo(cX-2,s.height-cY);
			g.moveTo(cX,cY);
			g.lineTo(cX,s.height-cY);
			g.moveTo(cX+2,cY);
			g.lineTo(cX+2,s.height-cY);
			g.moveTo(cX+4,cY);
			g.lineTo(cX+4,s.height-cY);
			g.lineStyle();
			g.endFill();
		}

	}
}