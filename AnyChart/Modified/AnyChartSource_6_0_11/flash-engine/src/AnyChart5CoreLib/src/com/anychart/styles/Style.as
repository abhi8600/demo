package com.anychart.styles {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.hatchFill.HatchFillMap;
	
	public class Style implements ISerializableRes,IStyle {
		
		private var dynamicColors:Object;
		private var dynamicColorsList:Array;
		
		private var dynamicGradients:Object;
		private var dynamicGradientsList:Array;
		
		private var dynamicHatchTypes:Object;
		private var dynamicHatchTypesList:Array;
		
		public function addDynamicColor(colorIndex:uint):void {
			if (this.dynamicColors[colorIndex] == null) {
				this.dynamicColors[colorIndex] = true;
				this.dynamicColorsList.push(colorIndex);
			}
		}
		
		public function addDynamicGradient(gradientIndex:uint):void {
			if (this.dynamicGradients[gradientIndex] == null) {
				this.dynamicGradients[gradientIndex] = true;
				this.dynamicGradientsList.push(gradientIndex);
			}
		}
		
		public function addDynamicHatchType(hatchIndex:uint):void {
			if (this.dynamicHatchTypes[hatchIndex] == null) {
				this.dynamicHatchTypes[hatchIndex] = true;
				this.dynamicHatchTypesList.push(hatchIndex);
			}
		}
		
		public function setDynamicColors(color:uint):void {
			for (var i:uint = 0;i<this.dynamicColorsList.length;i++) {
				ColorParser.getInstance().setDynamicColor(this.dynamicColorsList[i],color);
			}
		}
		
		public function setDynamicGradients(color:uint):void {
			for (var i:uint = 0;i<this.dynamicGradientsList.length;i++)
				ColorParser.getInstance().initDynamicGradient(this.dynamicGradientsList[i],color);
		}
		
		public function setDynamicHatchTypes(type:uint):void {
			for (var i:uint = 0;i<this.dynamicHatchTypesList.length;i++)
				HatchFillMap.initItem(this.dynamicHatchTypesList[i],type);
		}
		
		public function initializeRotations(manager:IRotationManager, baseScaleInverted:Boolean):void {
			this.normal.initializeRotations(manager,baseScaleInverted);
			this.hover.initializeRotations(manager,baseScaleInverted);
			this.pushed.initializeRotations(manager,baseScaleInverted);
			this.selectedNormal.initializeRotations(manager,baseScaleInverted);
			this.selectedHover.initializeRotations(manager,baseScaleInverted);
			this.missing.initializeRotations(manager,baseScaleInverted);
		}
		
		public var normal:StyleState;
		public var hover:StyleState;
		public var pushed:StyleState;
		public var selectedNormal:StyleState;
		public var selectedHover:StyleState;
		public var missing:StyleState;
		
		private var stateClass:Class;
		public var xmlData:XML;
		private var resources:ResourcesLoader;
		
		public function Style(stateClass:Class) {
			
			
			this.dynamicColors = {};
			this.dynamicColorsList = [];
		
			this.dynamicGradients = {};
			this.dynamicGradientsList = [];
		
			this.dynamicHatchTypes = {};
			this.dynamicHatchTypesList = [];
			
			if (stateClass) {
				this.stateClass = stateClass;
				
				this.normal = new stateClass(this);
				this.normal.stateIndex = 0;
				
				this.hover = new stateClass(this);
				this.hover.stateIndex = 1;
				
				this.pushed = new stateClass(this);
				this.pushed.stateIndex = 2;
				
				this.selectedNormal = new stateClass(this);
				this.selectedNormal.stateIndex = 3;
				
				this.selectedHover = new stateClass(this);
				this.selectedHover.stateIndex = 4;
				
				this.missing = new stateClass(this);
				this.missing.stateIndex = 5;
			}
			
			StylesList.stylesList.push(this);
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data == null) return;
			this.xmlData = data.copy();
			this.resources = resources;
			
			if (data.states[0] == null) {
				this.normal.deserialize(data, resources);
				this.hover.deserialize(data, resources);
				this.pushed.deserialize(data, resources);
				this.selectedNormal.deserialize(data, resources);
				this.selectedHover.deserialize(data, resources);
				this.missing.deserialize(data, resources);
			}else {
				this.normal.deserialize(this.mergeState(data,data.states[0].normal[0]), resources);
				this.hover.deserialize(this.mergeState(data,data.states[0].hover[0]), resources);
				this.pushed.deserialize(this.mergeState(data,data.states[0].pushed[0]), resources);
				this.selectedNormal.deserialize(this.mergeState(data,data.states[0].selected_normal[0]), resources);
				this.selectedHover.deserialize(this.mergeState(data,data.states[0].selected_hover[0]), resources);
				this.missing.deserialize(this.mergeState(data,data.states[0].missing[0]), resources);
			}
		}
		
		private function mergeState(style:XML, state:XML):XML {
			if (state == null) return style;
			return XMLUtils.merge(style, state, null,"states");
		}
		
		/**
		 * Иногда его все-таки надо клонировать :(
		 * 
		 */
		public function createCopy(res:Style = null):Style {
			if (res == null)
				res = new Style(this.stateClass);
			res.deserialize(this.xmlData, this.resources);
			return res;
		}

	}
}