package com.anychart.visual.effects
{
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public class EffectBase implements ISerializable
	{
		public function EffectBase()
		{
			super();
		}
		
		// Properties.
		public var enabled:Boolean=false;
		public var blurX:Number=4;
		public var blurY:Number=4;
		
		
		// Methods.
		public function deserialize(data:XML):void
		{
			if(data.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(data.@enabled);
			if(!this.enabled) return;
			
			if(data.@blur_x!=undefined) this.blurX=SerializerBase.getNumber(data.@blur_x);
			if(data.@blur_y!=undefined) this.blurY=SerializerBase.getNumber(data.@blur_y);
		}

	}
}