package com.anychart.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.effects.EffectsList;
	
	import flash.text.TextFormat;
	
	public class FontSettings implements IStyleSerializableRes {
		// Properties.
		public var family:String="Arial";
		public var size:Number=10;
		public var bold:Boolean=false;
		public var italic:Boolean=false;
		public var underline:Boolean=false;
		public var color:uint=0x000000;
		public var isDynamicTextColor:Boolean;
		public var renderAsHTML:Boolean=false;
		
		public var format:TextFormat;
		
		protected var _effects:EffectsList=null;
		public function get effects():EffectsList {
			if(_effects==null) _effects=new EffectsList();
			return _effects;
		}
		public function set effects(value:EffectsList):void { _effects=value; }
		
		public function hasEffects():Boolean {
			return this._effects != null;
		}
		
		// Methods.
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@render_as_html!=undefined) this.renderAsHTML=SerializerBase.getBoolean(data.@render_as_html);
			if (!this.renderAsHTML) {
				if(data.@family!=undefined) this.family=data.@family;
				if(data.@size!=undefined) this.size=SerializerBase.getNumber(data.@size);
				if(data.@bold!=undefined) this.bold=SerializerBase.getBoolean(data.@bold);
				if(data.@italic!=undefined) this.italic=SerializerBase.getBoolean(data.@italic);
				if(data.@underline!=undefined) this.underline=SerializerBase.getBoolean(data.@underline);
				if(data.@color!=undefined) {
					this.color=SerializerBase.getColor(data.@color);
					this.isDynamicTextColor = SerializerBase.isDynamicColor(data.@color);
					if (style != null && this.isDynamicTextColor)
						style.addDynamicColor(this.color);
				}
			}
			if(SerializerBase.isEnabled(data.effects[0])) this.effects.deserialize(data.effects[0]);
			
			if (this.renderAsHTML)
				return;
			
			this.createFormat();
		}
		
		public function createFormat(color:uint = 0):void {
			this.format = this.getFormat(color);
		}
		
		public function getFormat(color:uint):TextFormat {
			return new TextFormat(this.family, 
										 this.size, 
										 this.isDynamicTextColor ? ColorParser.getInstance().getDynamicColor(this.color, color) : this.color, 
										 this.bold, 
										 this.italic, 
										 this.underline);
		}
	}
}