package com.anychart.visual.background {
	
	import com.anychart.IResizable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public class RectBackgroundSprite implements ISerializableRes, IResizable {
		
		protected var sprite:Sprite;
		protected var bounds:Rectangle;
		
		public var background:RectBackground;
		
		//-------------------------------------------------------
		// IResizable
		//-------------------------------------------------------		
		
		protected function getDrawingTarget():Sprite {
			return this.sprite;
		}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.sprite = new Sprite();
			this.bounds = bounds;
			if (this.background != null && this.background.effects != null) {
				this.getDrawingTarget().filters=this.background.effects.list;
			}
			return this.sprite; 
		}
		
		public function draw():void {
			if (this.background != null)
				this.background.draw(this.getDrawingTarget().graphics, this.bounds);
		}
		
		public function calculateResize(newBounds:Rectangle):void {
			this.bounds = newBounds;
		}
		
		public function resize(newBounds:Rectangle):void {
			this.bounds = newBounds;
			this.execResize();
		}
		
		public function execResize():void {
			if (this.background != null)
				this.background.draw(this.getDrawingTarget().graphics, this.bounds);
		}
		
		//-------------------------------------------------------
		// ISerializable
		//-------------------------------------------------------
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data == null) return;
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new RectBackground();
				this.background.deserialize(data.background[0], resources);
			}
		}
	}
}