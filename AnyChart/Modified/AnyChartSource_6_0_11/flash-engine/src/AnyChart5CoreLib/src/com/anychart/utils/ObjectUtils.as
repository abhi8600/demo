package com.anychart.utils {
	
	public final class ObjectUtils {
		
		/*
		На 1 000 000 вызовов
		
		Быстрее на 0.3 секунды чем obj.hasOwnProperty
		Быстрее на 0.1 секунды чем obj[propName] == undefined
		 */
		public static function objectHasProperty(obj:Object, propName:String):Boolean {
			return obj[propName] != null;
		}
	}
}