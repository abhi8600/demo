package com.anychart.animation {
	public class AnimationTarget {
		public var animation:Animation;
		public function initialize():void {}
		public function setInitial():void {}
		public function animate(time:Number):void {}
		public function setFinal():void {}
		public function destroy():void {}
	}
}