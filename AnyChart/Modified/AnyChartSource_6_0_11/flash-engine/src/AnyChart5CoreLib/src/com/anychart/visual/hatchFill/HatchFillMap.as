package com.anychart.visual.hatchFill {
	import com.anychart.dispose.DisposableStaticList;
	
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	
	
	public final class HatchFillMap {
		
		private static var hatchList:Array = [];
		private static var parseMap:Object = {};
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(HatchFillMap);
			return true;
		}
		
		public static function clear():void {
			hatchList = [];
			parseMap = [];
		}
		
		public static function addItem(fill:HatchFill):uint {
			var key:String = fill.opacity.toString() + "_"+
							 fill.patternSize.toString() + "_"+
							 fill.thickness.toString() + "_";
			if (parseMap[key] != null)
				return parseMap[key];
			var obj:Object = {fill:fill};
			var index:uint = hatchList.length;
			hatchList.push(obj); 
			parseMap[key] = index;
			return index;
		}
		
		public static function initItem(index:uint, type:uint):void {
			if (hatchList[index][type] != null) 
				return;
			hatchList[index][type] = HatchFill(hatchList[index].fill).getPattern(type);
		}
		
		public static function getPattern(index:uint, type:uint):BitmapData {
			return hatchList[index][type][0];
		}
		
		public static function getMatrix(index:uint, type:uint):Matrix {
			return hatchList[index][type][1];
		}
	}
}