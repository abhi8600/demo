package com.anychart.elements.marker {
	import com.anychart.dispose.DisposableStaticList;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	public class MarkerDrawer {
		
		private static var instance:MarkerDrawer;
		
		public static function draw(g:Graphics, type:uint, size:Number, dx:Number, dy:Number, width:Number = NaN, height:Number = NaN):void {
			if (instance == null) instance = new MarkerDrawer();
			instance.drawMarker(g, type, size, dx, dy, width, height);
		}
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(MarkerDrawer);
			return true;
		}
		
		public static function clear():void {
			instance = null;
		}
		
		private function drawMarker(g:Graphics, type:uint, size:Number, dx:Number, dy:Number, width:Number = NaN, height:Number = NaN):void {
			switch (type) {
				case MarkerType.CIRCLE:
					this.drawCircle(g, size, dx, dy);
					break;
				case MarkerType.CROSS:
					this.drawCross(g, size, dx, dy);
					break;
				case MarkerType.DIAGONAL_CROSS:
					this.drawDiagonalCross(g, size, dx, dy);
					break;
				case MarkerType.DIAMOND:
					this.drawDiamond(g, size, dx, dy);
					break;
				case MarkerType.H_LINE:
					this.drawHLine(g, size, dx, dy);
					break;
				case MarkerType.V_LINE:
					this.drawVLine(g, size, dx, dy);
					break;
				case MarkerType.SQUARE:
					if (isNaN(width)) width = size;
					if (isNaN(height)) height = size;
					this.drawSquare(g, width, height, dx, dy);
					break;
				case MarkerType.STAR_10:
					this.drawStar(g, size, dx, dy, 10);
					break;
				case MarkerType.STAR_4:
					this.drawPrettyStar(g, size, dx, dy, 4);
					break;
				case MarkerType.STAR_5:
					this.drawPrettyStar(g, size, dx, dy, 5);
					break;
				case MarkerType.STAR_6:
					this.drawStar(g, size, dx, dy, 6);
					break;
				case MarkerType.STAR_7:
					this.drawPrettyStar(g, size, dx, dy, 7);
					break;
				case MarkerType.TRIANGLE_DOWN:
					this.drawTriangleDown(g, size, dx, dy);
					break;
				case MarkerType.TRIANGLE_UP:
					this.drawTriangleUp(g, size, dx, dy);
					break;
			}
		}
		
		private function drawCircle(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.drawCircle(dx + size/2, dy + size/2, size/2);
		}
		
		private function drawSquare(g:Graphics, w:Number, h:Number, dx:Number, dy:Number):void {
			g.drawRect(dx+1, dy+1, w-2, h-2);
		}
		
		private function drawDiamond(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.moveTo(dx + size/2,dy);
			g.lineTo(dx + size,dy + size/2);
			g.lineTo(dx + size/2,dy + size);
			g.lineTo(dx,dy + size/2);
			g.lineTo(dx + size/2,dy);
		}
		
		private function drawCross(g:Graphics, size:Number, dx:Number, dy:Number):void {
			var w:Number = size/4;
			g.moveTo(dx+size/2 - w/2,dy);
			g.lineTo(dx+size/2 - w/2,dy+size/2 - w/2);
			g.lineTo(dx,dy+size/2 - w/2);
			g.lineTo(dx,dy+size/2 + w/2);
			g.lineTo(dx+size/2 - w/2,dy+size/2 + w/2);
			g.lineTo(dx+size/2 - w/2,dy+size);
			g.lineTo(dx+size/2 + w/2,dy+size);
			g.lineTo(dx+size/2 + w/2,dy+size/2 + w/2);
			g.lineTo(dx+size,dy+size/2 + w/2);
			g.lineTo(dx+size,dy+size/2 - w/2);
			g.lineTo(dx+size/2 + w/2,dy+size/2 - w/2);
			g.lineTo(dx+size/2 + w/2,dy);
			g.lineTo(dx+size/2 - w/2,dy);
		}
		
		private function drawDiagonalCross(g:Graphics, size:Number, dx:Number, dy:Number):void {
			var dw:Number = size/4;
			var d:Number = dw*Math.SQRT2/2;
	
			g.moveTo(dx+d,dy);
			g.lineTo(dx+size/2,dy+size/2-d);
			g.lineTo(dx+size - d,dy);
			g.lineTo(dx+size, dy+d);
			g.lineTo(dx+size/2 + d, dy+size/2);
			g.lineTo(dx+size, dy+size - d);
			g.lineTo(dx+size-d, dy+size);
			g.lineTo(dx+size/2, dy+size/2+d);
			g.lineTo(dx+d, dy+size);
			g.lineTo(dx, dy+size - d);
			g.lineTo(dx+size/2 - d,dy+size/2);
			g.lineTo(dx, dy+d);
			g.lineTo(dx+d, dy);
		}
		
		private function drawHLine(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.drawRect(dx, dy + (size - size/4)/2, size, size/4);
		}
		
		private function drawVLine(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.drawRect(dx + (size - size/4)/2, dy, size/4, size);
		}
		
		private function drawPrettyStar(g:Graphics, size:Number, dx:Number, dy:Number, n:Number):void {
			
			var R:Number = size/2;
			var r:Number = R/2;
			
			var da:Number = (Math.PI*2)/n;
			
			var outA:Number = -Math.PI/2;
			var inA:Number = outA + da/2;
			
			for (var i:uint = 0;i<=n;i++) {
				var outX:Number = R*Math.cos(outA);
				var outY:Number = R*Math.sin(outA);
				var inX:Number = r*Math.cos(inA);
				var inY:Number = r*Math.sin(inA);
				
				outX += R;
				outY += R;
				inX += R;
				inY += R;
				
				if (i == 0) 
					g.moveTo(dx + outX, dy + outY);
				else 
					g.lineTo(dx + outX, dy + outY);
				g.lineTo(dx + inX, dy + inY);
				outA += da;
				inA += da;
			}
		}
		
		private function drawStar(g:Graphics, size:Number, dx:Number, dy:Number, n:Number):void {
			var R:Number = size/2;
			
			var alpha:Number = Math.PI*2/n;
			
			var pointsOut:Array = new Array();
			
			var i:uint;
			
			for (i = 0;i<=n;i++) {
				pointsOut.push(new Point(-R*Math.sin(i*alpha),
										 -R*Math.cos(i*alpha)));
			}
			
			//get inner radius
			var aP:Point = pointsOut[0];
			var bP:Point = pointsOut[1];
			var cP:Point = pointsOut[2];
			
			var dP:Point = new Point();
			dP.x = (aP.x - cP.x)*(bP.y - cP.y)/(aP.y - cP.y) + cP.x;
			dP.y = bP.y;
			
			var pOut:Point;
			var pIn:Point;
			
			var r:Number = Math.sqrt(Math.pow(dP.x,2) + Math.pow(dP.y,2));
			
			var pointsIn:Array = new Array();
			for (i = 0;i<=n;i++) {
				pointsIn.push(new Point(-r*Math.sin(i*alpha + alpha/2),
										-r*Math.cos(i*alpha + alpha/2)));
			}
			
			g.moveTo(dx + aP.x + R,dy + aP.y + R);
			var k:uint = 0;
			for (i = 0;i<=2*n;i+=2) {
				pOut = pointsOut[k];
				pIn = pointsIn[k];
				g.lineTo(dx + pOut.x + R,dy + pOut.y + R);
				g.lineTo(dx + pIn.x + R,dy + pIn.y + R);
				k++;
			}
		}
		
		private function drawTriangleDown(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.moveTo(dx + size/2,dy + size);
			g.lineTo(dx + size,dy);
			g.lineTo(dx,dy);
			g.lineTo(dx + size/2,dy + size);
		}
		
		private function drawTriangleUp(g:Graphics, size:Number, dx:Number, dy:Number):void {
			g.moveTo(dx + size/2,dy);
			g.lineTo(dx + size,dy+size);
			g.lineTo(dx,dy+size);
			g.lineTo(dx + size/2,dy);
		}
	}
}