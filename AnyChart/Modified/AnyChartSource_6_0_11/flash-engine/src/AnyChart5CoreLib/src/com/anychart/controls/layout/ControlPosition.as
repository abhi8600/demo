package com.anychart.controls.layout {
	public final class ControlPosition {
		public static const LEFT:uint = 0;
		public static const RIGHT:uint = 1;
		public static const TOP:uint = 2;
		public static const BOTTOM:uint = 3;
		public static const FLOAT:uint = 4;
		public static const FIXED:uint = 5;
	}
}