package com.anychart.palettes {
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public class BasePalette implements ISerializable {
		
		[ArrayElementType("uint")]
		protected var items:Array;
		
		public function getItemAt(index:uint):uint {
			if (index >= this.items.length)
				index = index%this.items.length;
			return this.items[index];
		}
		
		public function deserialize(data:XML):void {
			this.items = [];
		}
	}
}