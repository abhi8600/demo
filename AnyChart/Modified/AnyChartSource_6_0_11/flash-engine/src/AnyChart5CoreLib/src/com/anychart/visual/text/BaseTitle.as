package com.anychart.visual.text {
	import com.anychart.formatters.FormatsParser;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.layout.AbstractAlign;
	
	public class BaseTitle extends BaseTextElement {
		
		public var align:uint = AbstractAlign.CENTER;
		public var padding:Number = 5;
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			super.deserialize(data, resources, style);
			if (data.text[0] != null) { 
				this.text = this.getCDATAString(data.text[0]);
				this.isDynamicText = FormatsParser.isDynamic(this.text);
				if (this.isDynamicText)
					this.dynamicText = FormatsParser.parse(this.text);
			}
			if (data.@align != undefined) {
				switch (SerializerBase.getEnumItem(data.@align)) {
					case "near":
					case "left":
					case "top":
						this.align = AbstractAlign.NEAR;
						break;
					case "far":
					case "right":
					case "bottom":
						this.align = AbstractAlign.FAR;
						break;
					case "center":
						this.align = AbstractAlign.CENTER;
						break;
				}
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
		}
		
	}
}