package com.anychart.styles {
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.gradient.GradientEntry;
	
	public class ProgrammaticStyle {
		
		public function initialize(style:StyleState):void {}
		
		public function draw(data:Object):void {}
		
		// Aqua drawing UTILS
		protected var darkIndex:Number;
		protected var lightIndex:Number;
		
		// Methods.
		protected function createKey(ratio:Number,color:uint,opacity:Number,isDynamic:Boolean=false):GradientEntry {
			var key:GradientEntry=new GradientEntry(color,ratio,opacity);
			key.isDynamic=isDynamic;
			return key;
		}
		
		protected function initDynamicIndexes(state:StyleState,darkFn:Function,lightFn:Function):void {
			var st:BackgroundBaseStyleState = BackgroundBaseStyleState(state);
			
			if (st.fill.isDynamic) {
				var fillColorIndex:uint = st.fill.color;
				var fillColor:Array = ColorParser.getInstance().getNotation(fillColorIndex);
				darkIndex = ColorParser.getInstance().addNotation(fillColor.concat([darkFn]));
				lightIndex = ColorParser.getInstance().addNotation(fillColor.concat([lightFn]));
				st.fill.color = ColorParser.getInstance().parse('%color');
				state.style.addDynamicColor(st.fill.color);
			}else {
				darkIndex = ColorParser.getInstance().addNotation(["%color",darkFn]);
				lightIndex = ColorParser.getInstance().addNotation(["%color",lightFn]);
			}
		
			state.style.addDynamicColor(darkIndex);
			state.style.addDynamicColor(lightIndex);
		}
		
		protected function createDynamicGradient(state:StyleState,grd:Gradient,entries:Array):void {
			var st:BackgroundBaseStyleState = BackgroundBaseStyleState(state);
			
			grd.entries=entries;
            var grdIndex:int = ColorParser.getInstance().setUniqueDynamicGradient(entries);
        	grd.colorsIndex = grdIndex;
        	grd.isDynamic = true;
        	state.style.addDynamicGradient(grdIndex);
        	
        	if (!st.fill.isDynamic) {
   				state.style.setDynamicColors(st.fill.color);
   				state.style.setDynamicGradients(st.fill.color);
   			}
		}
	}
}