package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlLayout;
	
	import flash.geom.Rectangle;
	
	internal class VerticalControlsLine extends ControlsLine {
		
		public function VerticalControlsLine(isOpposite:Boolean) {
			super(isOpposite);
		}
		
		override protected function stretchControl(layout:ControlLayout):void {
			layout.isHeightSetted = true;
			layout.isPercentHeight = true;
			layout.height = 1;
		}
		
		override protected function setNearLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.nearControls.alignByDataPlot ? plotRect : chartRect; 
			var y:Number = targetRect.top;
			
			var controls:Array = this.nearControls.controls;
			var i:uint;
			var control:Control;
			
			for (i = 0;i<controls.length;i++) {
				control = controls[i];
				control.bounds.y = y;
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				y += control.bounds.height;
				space = Math.max(space, control.bounds.width);
			}
			
			return space;
		}
		
		override protected function setCenterLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.centerControls.alignByDataPlot ? plotRect : chartRect; 
			var x:Number = this.isOpposite ? basePositionRect.right : basePositionRect.left;
			var y:Number = targetRect.y + targetRect.height/2;
			var controls:Array = this.centerControls.controls;
			
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				
				control.bounds.y = y - control.bounds.height/2;
				space += control.bounds.width;
			}
			return space;
		}
		
		override protected function setFarLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.farControls.alignByDataPlot ? plotRect : chartRect; 
			var y:Number = targetRect.bottom;
			
			var controls:Array = this.farControls.controls;
			var i:uint;
			var control:Control;
			for (i = 0;i<controls.length;i++) {
				control = controls[i];
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				
				control.bounds.y = y - control.bounds.height;
				y -= control.bounds.height;
				
				space = Math.max(space, control.bounds.width);
			}			
			return space;
		}
		
		override protected function setMaxBounds(layout:ControlLayout, chartRect:Rectangle, plotRect:Rectangle):void {
			layout.maxHeight = layout.alignByDataPlot ? plotRect.height : chartRect.height;
			layout.maxWidth = chartRect.width*.5;
		}
		
		override protected function finalizeNearOrFarLayout(controls:Array, space:Number):void {
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite)
					control.bounds.x = basePositionRect.right - space;
				else
					control.bounds.x = basePositionRect.left + space - control.bounds.width;
			}
		}
		
		override protected function finalizeCenterLayout(controls:Array, space:Number):void {
			var x:Number = this.isOpposite ? (basePositionRect.right - space) : (basePositionRect.left + space);
			
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite) {
					control.bounds.x = x;
					x += control.bounds.width;
				}else {
					control.bounds.x = x - control.bounds.width;
					x -= control.bounds.width;
				}
			}
		}
	}
}