package com.anychart.visual.effects
{
	import com.anychart.serialization.SerializerBase;
	
	import flash.filters.GlowFilter;
	
	public class Glow extends EffectBase
	{
		public var opacity:Number=1;
		public var color:uint=0xFFFFFF;
		public var inner:Boolean=false;
		public var strength:Number=1;
		
		public function Glow()
		{
			super();
		}
		
		// Methods.
		override public function deserialize(data:XML):void
		{
			super.deserialize(data);
			if(data.@opacity!=undefined) this.opacity=SerializerBase.getRatio(data.@opacity);
			if(data.@color!=undefined) this.color=SerializerBase.getColor(data.@color);
			if(data.@strength!=undefined) this.strength=SerializerBase.getNumber(data.@strength);
		}
		
		public function createFilter():GlowFilter
		{
			return new GlowFilter(color,opacity,blurX,blurY,strength,1,inner);
		}
		
	}
}