package com.anychart.animation {
	import com.anychart.animation.interpolation.InterpolationFactory;
	import com.anychart.animation.interpolation.Interpolator;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	public class Animation {
		public var startTime:int;
		public var duration:int;
		public var interpolation:Interpolator;
		public var animateOpacity:Boolean;
		
		public var type:uint;
		
		private var smooth:Number;
		
		public var isMain:Boolean;
		
		public function Animation() {
			this.startTime = 0;
			this.duration = 500;
			this.animateOpacity = false;
			this.smooth = 0;
			this.isMain = false;
		}
		
		public function deserialize(data:XML, styles:XML = null):XML {
			var actualData:XML = (data.@style != undefined) ? StylesList.getStyleXMLByMerge(styles, "animation_style", data, data.@style)
														  : data;
			
			if (actualData.@start_time != undefined) this.startTime = SerializerBase.getNumber(actualData.@start_time)*1000;
			if (actualData.@duration != undefined) this.duration = SerializerBase.getNumber(actualData.@duration)*1000;
			
			this.interpolation = InterpolationFactory.create(actualData.@interpolation_type != undefined ? actualData.@interpolation_type : "");
			if (actualData.@animate_opacity != undefined) this.animateOpacity = SerializerBase.getBoolean(actualData.@animate_opacity);
			if (actualData.@type != undefined) {
				switch (SerializerBase.getEnumItem(actualData.@type)) {
					case "appear": this.type = AnimationType.APPEAR; break;
					case "sidefromleft": this.type = AnimationType.SIDE_FROM_LEFT; break;
					case "sidefromtop": this.type = AnimationType.SIDE_FROM_TOP; break;
					case "sidefromright": this.type = AnimationType.SIDE_FROM_RIGHT; break;
					case "sidefrombottom": this.type = AnimationType.SIDE_FROM_BOTTOM; break;
					case "sidefromlefttop": this.type = AnimationType.SIDE_FROM_LEFT_TOP; break;
					case "sidefromrighttop": this.type = AnimationType.SIDE_FROM_RIGHT_TOP; break;
					case "sidefromrightbottom": this.type = AnimationType.SIDE_FROM_RIGHT_BOTTOM; break;
					case "sidefromleftbottom": this.type = AnimationType.SIDE_FROM_LEFT_BOTTOM; break;
					case "sidefromleftcenter": this.type = AnimationType.SIDE_FROM_LEFT_CENTER; break;
					case "sidefromtopcenter": this.type = AnimationType.SIDE_FROM_TOP_CENTER; break;
					case "sidefromrightcenter": this.type = AnimationType.SIDE_FROM_RIGHT_CENTER; break;
					case "sidefrombottomcenter": this.type = AnimationType.SIDE_FROM_BOTTOM_CENTER; break;
					case "scalexcenter": this.type = AnimationType.SCALE_X_CENTER; break;
					case "scalexleft": this.type = AnimationType.SCALE_X_LEFT; break;
					case "scalexright": this.type = AnimationType.SCALE_X_RIGHT; break;
					case "scaleycenter": this.type = AnimationType.SCALE_Y_CENTER; break;
					case "scaleytop": this.type = AnimationType.SCALE_Y_TOP; break;
					case "scaleybottom": this.type = AnimationType.SCALE_Y_BOTTOM; break;
					case "scalexycenter": this.type = AnimationType.SCALE_XY_CENTER; break;
					case "scalexyleft": this.type = AnimationType.SCALE_XY_LEFT; break;
					case "scalexytop": this.type = AnimationType.SCALE_XY_TOP; break;
					case "scalexyright": this.type = AnimationType.SCALE_XY_RIGHT; break;
					case "scalexybottom": this.type = AnimationType.SCALE_XY_BOTTOM; break;
					case "scalexylefttop": this.type = AnimationType.SCALE_XY_LEFT_TOP; break;
					case "scalexyrighttop": this.type = AnimationType.SCALE_XY_RIGHT_TOP; break;
					case "scalexyrightbottom": this.type = AnimationType.SCALE_XY_RIGHT_BOTTOM; break;
					case "scalexyleftbottom": this.type = AnimationType.SCALE_XY_LEFT_BOTTOM; break;
					case "show": this.type = AnimationType.SHOW; break;
					case "outside": this.type = AnimationType.OUTSIDE; break;
				}
			}
			
			if (actualData.@show_mode != undefined) {
				switch (SerializerBase.getEnumItem(actualData.@show_mode)) {
					case "together": this.smooth = 0; break; 
					case "onebyone": this.smooth = 1; break; 
					case "smoothed": this.smooth = (actualData.@smoothed_delay != undefined) ? SerializerBase.getNumber(actualData.@smoothed_delay) : .5; break;
				}
			}
			
			this.initType();
			
			return actualData;
		}
		
		public function createCopy(res:Animation = null):Animation {
			if (res == null)
				res = new Animation();
			res.startTime = this.startTime;
			res.duration = this.duration;
			res.animateOpacity = this.animateOpacity;
			res.interpolation = this.interpolation;
			res.type = this.type;
			res.invertedType = this.invertedType;
			res.horizontalInvertedType = this.horizontalInvertedType;
			res.horizontalType = this.horizontalType;
			res.smooth = this.smooth;
			return res;
		}
		
		public function getAnimation(index:uint, totalElements:uint):Animation {
			if (this.smooth == 0) return this;
			
			var a:Animation = this.createCopy();
			a.duration = this.duration/(1 + this.smooth*(totalElements-1));
			a.startTime = this.startTime + index*a.duration*this.smooth;
			return a;
		}
		
		public function needGetAnimation():Boolean {
			return this.smooth != 0;
		}
		
		public function getActualType(isHorizontal:Boolean, isInverted:Boolean):uint {
			return isHorizontal ? 
						(isInverted ? this.horizontalInvertedType : this.horizontalType) :
						(isInverted ? this.invertedType : this.type);
		}
		
		private var horizontalType:uint;
		private var invertedType:uint;
		private var horizontalInvertedType:uint;
		
		private function initType():void {
			switch (this.type) {
				case AnimationType.SIDE_FROM_LEFT: 
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_TOP;
					this.invertedType = this.type;
					break;
					
				case AnimationType.SIDE_FROM_TOP:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_RIGHT;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_RIGHT:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_BOTTOM;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_BOTTOM:
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_LEFT;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_LEFT_TOP:
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_RIGHT_TOP;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_TOP:
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_RIGHT_BOTTOM;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_BOTTOM:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_LEFT_BOTTOM;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_LEFT_BOTTOM:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_LEFT_TOP;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_LEFT_CENTER:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_TOP_CENTER;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_TOP_CENTER:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_RIGHT_CENTER;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_RIGHT_CENTER:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_BOTTOM_CENTER;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SIDE_FROM_BOTTOM_CENTER:	
					this.horizontalInvertedType = this.horizontalType = AnimationType.SIDE_FROM_LEFT_CENTER;
					this.invertedType = this.type; 
					break;
				
				case AnimationType.SCALE_X_CENTER:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_Y_CENTER;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SCALE_Y_CENTER:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_X_CENTER;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SCALE_X_LEFT:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_Y_TOP;
					this.invertedType = this.type;
					break;
					
				case AnimationType.SCALE_X_RIGHT:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_Y_BOTTOM;
					this.invertedType = this.type; 
					break;
					
				case AnimationType.SCALE_Y_TOP:				
					this.horizontalType = AnimationType.SCALE_X_RIGHT;
					this.horizontalInvertedType = AnimationType.SCALE_X_LEFT;
					this.invertedType = AnimationType.SCALE_Y_BOTTOM; 
					break;
					
				case AnimationType.SCALE_Y_BOTTOM:			
					this.horizontalType = AnimationType.SCALE_X_LEFT;
					this.horizontalInvertedType = AnimationType.SCALE_X_RIGHT;
					this.invertedType = AnimationType.SCALE_Y_TOP; 
					break;
				
				case AnimationType.SCALE_XY_LEFT:		
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_XY_TOP;
					this.invertedType = this.type;
					break;
					
				case AnimationType.SCALE_XY_TOP:			
					this.horizontalType = AnimationType.SCALE_XY_RIGHT;
					this.horizontalInvertedType = AnimationType.SCALE_XY_LEFT;
					this.invertedType = AnimationType.SCALE_XY_BOTTOM; 
					break;
					
				case AnimationType.SCALE_XY_RIGHT:			
					this.horizontalInvertedType = this.horizontalType = AnimationType.SCALE_XY_BOTTOM;
					this.invertedType = this.type;
					break;
					
				case AnimationType.SCALE_XY_BOTTOM:			
					this.horizontalType = AnimationType.SCALE_XY_LEFT;
					this.horizontalInvertedType = AnimationType.SCALE_XY_RIGHT;
					this.invertedType = AnimationType.SCALE_XY_TOP; 
					break;
					
				case AnimationType.SCALE_XY_LEFT_TOP:		
					this.horizontalType = AnimationType.SCALE_XY_RIGHT_TOP;
					this.horizontalInvertedType = AnimationType.SCALE_XY_LEFT_TOP;
					this.invertedType = AnimationType.SCALE_XY_LEFT_BOTTOM; 
					break;
					
				case AnimationType.SCALE_XY_RIGHT_TOP:		
					this.horizontalType = AnimationType.SCALE_XY_RIGHT_BOTTOM;
					this.horizontalInvertedType = AnimationType.SCALE_XY_LEFT_BOTTOM;
					this.invertedType = AnimationType.SCALE_XY_RIGHT_BOTTOM;
					break;
					
				case AnimationType.SCALE_XY_RIGHT_BOTTOM:	
					this.horizontalType = AnimationType.SCALE_XY_LEFT_BOTTOM;
					this.horizontalInvertedType = AnimationType.SCALE_XY_RIGHT_BOTTOM;
					this.invertedType = AnimationType.SCALE_XY_RIGHT_TOP; 
					break;
					
				case AnimationType.SCALE_XY_LEFT_BOTTOM:	
					this.horizontalType = AnimationType.SCALE_XY_LEFT_TOP;
					this.horizontalInvertedType = AnimationType.SCALE_XY_RIGHT_TOP;
					this.invertedType = AnimationType.SCALE_XY_LEFT_TOP; 
					break;
				
				default: 
					this.horizontalInvertedType = this.horizontalType = this.invertedType = this.type;
			}
		}
	}
}