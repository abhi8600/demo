package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	import com.anychart.serialization.ISerializable;
	
	public final class ActionsList implements ISerializable {
		
		private var actions:Array;
		private var chart:IAnyChart;
		private var formatter:IFormatable;
		
		public function ActionsList(chart:IAnyChart,formatter:IFormatable = null) {
			this.chart = chart;
			this.formatter = formatter;
		}
		
		public function hasActions():Boolean {
			return this.actions.length > 0;
		}
		
		public function execute(formatter:IFormatable = null):Boolean {
			if (formatter != null)
				this.formatter = formatter;
			var res:Boolean = false;
			for (var i:uint = 0;i<this.actions.length;i++)
				res = res || this.execAction(this.actions[i], i, this.actions);
			return res; 
		}
		
		private function execAction(element:Action, index:int, arr:Array):Boolean {
			return element.execute(this.formatter);
        }
		
		public function deserialize(data:XML):void {
			this.actions = [];
			var cnt:int = data.action.length();
			for (var i:int = 0;i<cnt;i++) {
				var action:Action = Action.create(this.chart, data.action[i]);
				if (action != null)
					this.actions.push(action);
			}
		}

		private static var defaultsInitialized:Boolean = initializeDefaults();
		private static function initializeDefaults():Boolean {
			Action.register("navigatetourl", NavigateToURLAction);
			Action.register("call", CallAction);
			Action.register("updatechart", UpdateChartAction);
			Action.register("updateview", UpdateViewAction);
			Action.register("scroll", ScrollAction);
			Action.register("zoom", ZoomAction);
			Action.register("scrollview", DashboardScrolAction);
			Action.register("zoomView", DashboardZoomAction);
			return true;
		}
	}
}