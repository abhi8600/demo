package com.anychart.actions {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	internal class ChartAction extends Action {
		internal static const SOURCE_EXTERNAL:uint = 0;
		internal static const SOURCE_INTERNAL:uint = 1;
		
		private var source:String;
		private var isDynamicSource:Boolean;
		private var dynamicSource:TextFormater;
		
		private var isDynamicReplaceTokens:Boolean;
		private var replaceTokens:Array; 
		protected var sourceMode:uint;
		
		public function ChartAction(chart:IAnyChart) {
			super(chart);
		}
		
		protected final function getSource(formater:IFormatable):String {
			return this.isDynamicSource ? this.dynamicSource.getValue(formater.getTokenValue, formater.isDateTimeToken) : this.source;
		}
		
		protected final function getReplace(formater:IFormatable):Array {
			if (this.replaceTokens == null) return null;
			var tokens:Array;
			if (this.isDynamicReplaceTokens) {
				tokens = [];
				for (var i:uint = 0;i<this.replaceTokens.length;i++) {
					if (this.replaceTokens[i][1] is TextFormater) {
						tokens.push([this.replaceTokens[i][0], TextFormater(this.replaceTokens[i][1]).getValue(formater.getTokenValue, formater.isDateTimeToken)]);
					}else {
						tokens.push(this.replaceTokens[i]);
					}
				}
			}else {
				tokens = this.replaceTokens;
			}
			return tokens;
		}
		
		override public function deserialize(data:XML):void {
			this.isValidXML = data.@source != undefined && data.@source_mode != undefined;
			if (!this.isValidXML) return;
			
			this.source = SerializerBase.getString(data.@source);
			this.isDynamicSource = FormatsParser.isDynamic(this.source);
			if (this.isDynamicSource)
				this.dynamicSource = FormatsParser.parse(this.source);
			
			switch (SerializerBase.getEnumItem(data.@source_mode)) {
				case "externaldata": this.sourceMode = SOURCE_EXTERNAL; break;
				case "internaldata": this.sourceMode = SOURCE_INTERNAL; break;
				default: this.isValidXML = false;
			}
			
			if (!this.isValidXML) return;
			
			this.isDynamicReplaceTokens = false;
			var replaceCnt:int = data.replace.length();
			if (replaceCnt > 0) {
				this.replaceTokens = [];
				for (var i:int = 0;i<replaceCnt;i++) {
					var token:XML = data.replace[i];
					if (token.@token != undefined) {
						var tokenValue:String = SerializerBase.getCDATAString(token);
						var template:String = SerializerBase.getString(token.@token);
						if (FormatsParser.isDynamic(tokenValue)) {
							this.replaceTokens.push([template,FormatsParser.parse(tokenValue)]);
							this.isDynamicReplaceTokens = true;
						}else {
							this.replaceTokens.push([template,tokenValue]);
						}
					}
				}
			}
		}
	}
}