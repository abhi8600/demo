package com.anychart.uiControls.scrollBar
{
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class ScrollBarThumbV extends ScrollBarButton
	{
		public function ScrollBarThumbV() {
			super();
			this.dw = 2;
		}
		
		override public function draw():void {
			super.draw();
			var s:Rectangle = size;
			
			var cX:Number = s.width * 0.25;
			var cY:Number = s.height * 0.5;
		
			var g:Graphics = this.graphics;
			g.lineStyle(1, 0x494949,05);
			g.beginFill(0,0);
			g.moveTo(cX,cY-4);
			g.lineTo(s.width-cX,cY-4);
			g.moveTo(cX,cY-2);
			g.lineTo(s.width-cX,cY-2);
			g.moveTo(cX,cY);
			g.lineTo(s.width-cX,cY);
			g.moveTo(cX,cY+2);
			g.lineTo(s.width-cX,cY+2);
			g.moveTo(cX,cY+4);
			g.lineTo(s.width-cX,cY+4);
			g.lineStyle();
			g.endFill();
		}

	}
}