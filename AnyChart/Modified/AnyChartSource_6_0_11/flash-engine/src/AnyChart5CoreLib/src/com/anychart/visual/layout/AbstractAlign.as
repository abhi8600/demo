package com.anychart.visual.layout {
	public final class AbstractAlign {
		public static const NEAR:uint = 0;
		public static const CENTER:uint = 1;
		public static const FAR:uint = 2;
	}
}