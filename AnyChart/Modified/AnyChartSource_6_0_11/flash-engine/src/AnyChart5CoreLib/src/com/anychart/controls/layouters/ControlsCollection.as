package com.anychart.controls.layouters {
	import com.anychart.controls.Control;
	
	import flash.geom.Rectangle;
	
	public /*abstract*/ class ControlsCollection {
		public function addControl(control:Control):void {}
		public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle):Boolean { return true; }
	}
}