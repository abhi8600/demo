package com.anychart.events
{
	import flash.events.Event;
	
	public class AxesPlotScrollEvent extends Event	{
		
		public static const X_AXIS_SCROLL_CHANGE:String = "XAxisScrollChange";
		public static const Y_AXIS_SCROLL_CHANGE:String = "YAxisScrollChange";
		
		public var startValue:Number;
		public var endValue:Number;
		public var valueRange:Number;
		public var source:String;
		public var phase:String;
		
		public function AxesPlotScrollEvent(type:String, source:String, phase:String, startValue:Number, endValue:Number, valueRange:Number) {
			
			this.startValue = startValue;
			this.endValue = endValue;
			this.valueRange = valueRange;
			this.source = source;
			this.phase = phase;
						
			super(type);
		}
		
		override public function clone():Event {
			return new AxesPlotScrollEvent(this.type, this.source, this.phase, this.startValue, this.endValue, this.valueRange);
		}

	}
}