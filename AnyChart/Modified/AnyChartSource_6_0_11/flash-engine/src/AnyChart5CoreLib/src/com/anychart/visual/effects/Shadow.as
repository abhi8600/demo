package com.anychart.visual.effects
{
	import com.anychart.serialization.SerializerBase;
	
	import flash.filters.DropShadowFilter;
	
	public class Shadow extends EffectBase
	{
		public function Shadow()
		{
			super();
		}
		
		// Properties.
		public var angle:Number=45;
		public var opacity:Number=1;
		public var color:uint=0x000000;
		public var distance:Number=3;
		public var inner:Boolean=false;
		public var strength:Number=1;
		
		
		// Methods.
		override public function deserialize(data:XML):void
		{
			super.deserialize(data);
			if(data.@opacity!=undefined) this.opacity=SerializerBase.getRatio(data.@opacity);
			if(data.@color!=undefined) this.color=SerializerBase.getColor(data.@color);
			if(data.@distance!=undefined) this.distance=SerializerBase.getNumber(data.@distance);
			if(data.@strength!=undefined) this.strength=SerializerBase.getNumber(data.@strength);
			if(data.@angle!=undefined) this.angle=SerializerBase.getNumber(data.@angle);
		}
		
		public function createFilter():DropShadowFilter
		{
			var filter:DropShadowFilter=new DropShadowFilter(distance,angle,color,opacity,blurX,blurY,strength,
				1,inner,false,false);
			return filter;
		}
		
		
	}
}