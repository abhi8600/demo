package com.anychart.formatters.terms
{
	public interface ITerm
	{
		function getValue(getTokenFunction:Function, isDateTimeTokenFunction:Function):String;
	}
}