package com.anychart.controls.layout {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.Margin;
	
	import flash.geom.Rectangle;
	
	public final class ControlLayout {
		
		public var margin:Margin;
		
		//position
		public var position:uint;
		public var align:uint;
		public var insideDataPlot:Boolean;
		public var zIndex:int;
		public var anchor:uint;
		
		//size
		public var width:Number;
		public var isPercentWidth:Boolean;
		public var isWidthSetted:Boolean;
		
		public var height:Number;
		public var isPercentHeight:Boolean;
		public var isHeightSetted:Boolean;
		
		//padding
		public var horizontalPadding:Number;
		public var isPercentHorizontalPadding:Boolean;
		public var pixHorizontalPaddding:Number;
		
		public var verticalPadding:Number;
		public var isPercentVerticalPadding:Boolean;
		public var pixVerticalPadding:Number;
		
		//align
		public var hAlign:int;
		public var vAlign:int;
		
		public var floatX:Number;
		public var floatY:Number;
		
		public var alignByDataPlot:Boolean;
		public var isAlignBySet:Boolean;
		
		public var maxWidth:Number;
		public var maxHeight:Number;
		
		public function ControlLayout() {
			this.position = ControlPosition.RIGHT;
			this.align = ControlAlign.NEAR;
			this.insideDataPlot = false;
			this.anchor = Anchor.CENTER;
			this.isPercentHeight = false;
			this.isPercentWidth = false;
			this.isPercentHorizontalPadding = false;
			this.isPercentVerticalPadding = false;
			this.horizontalPadding = 0;
			this.verticalPadding = 0;
			this.hAlign = -1;
			this.vAlign = -1;
			this.margin = new Margin();
			this.margin.all = 0;
			this.alignByDataPlot = true;
			this.zIndex = -1;
			this.isWidthSetted = false;
			this.isHeightSetted = false;
			this.isAlignBySet = false;
		}
		
		public function setBounds(controlBounds:Rectangle, chartRect:Rectangle, plotRect:Rectangle):void {
			controlBounds.width = NaN;
			controlBounds.height = NaN;
			
			var targetRect:Rectangle = this.alignByDataPlot ? plotRect : chartRect;
			
			if (this.position != ControlPosition.FLOAT && 
				this.position != ControlPosition.FIXED && 
				!this.insideDataPlot) {
					
					switch (this.position) {
						case ControlPosition.LEFT:
						case ControlPosition.RIGHT:
							if (!isNaN(this.width))
								controlBounds.width = this.isPercentWidth ? (chartRect.width*this.width) : this.width;
								
							if (!isNaN(this.height))
								controlBounds.height = this.isPercentHeight ? (targetRect.height*this.height) : this.height;
							break;
						
						case ControlPosition.TOP:
						case ControlPosition.BOTTOM:
							if (!isNaN(this.width))
								controlBounds.width = this.isPercentWidth ? (targetRect.width*this.width) : this.width;
								
							if (!isNaN(this.height))
								controlBounds.height = this.isPercentHeight ? (chartRect.height*this.height) : this.height;
							break;
					}
			}else {
				if (!isNaN(this.width))
					controlBounds.width = this.isPercentWidth ? (targetRect.width*this.width) : this.width;
					
				if (!isNaN(this.height))
					controlBounds.height = this.isPercentHeight ? (targetRect.height*this.height) : this.height;
			}
				
			this.pixHorizontalPaddding = (this.isPercentHorizontalPadding) ? (targetRect.width*this.horizontalPadding) : this.horizontalPadding;
			this.pixVerticalPadding = (this.isPercentVerticalPadding) ? (targetRect.height*this.verticalPadding) : this.verticalPadding;
		}
		
		public function deserialize(data:XML):void {
			if (data.@position != undefined) {
				switch (SerializerBase.getEnumItem(data.@position)) {
					case "left": this.position = ControlPosition.LEFT; break;
					case "right": this.position = ControlPosition.RIGHT; break;
					case "top": this.position = ControlPosition.TOP; break;
					case "bottom": this.position = ControlPosition.BOTTOM; break;
					case "float": this.position = ControlPosition.FLOAT; break;
					case "fixed": this.position = ControlPosition.FIXED; break;
				}
			}
			
			if (data.@align != undefined) {
				switch (SerializerBase.getEnumItem(data.@align)) {
					case "near": this.align = ControlAlign.NEAR; break;
					case "center": this.align = ControlAlign.CENTER; break;
					case "far": this.align = ControlAlign.FAR; break;
					case "spread": this.align = ControlAlign.SPREAD; break;
				}
			}
			
			if (data.@inside_dataplot != undefined) this.insideDataPlot = SerializerBase.getBoolean(data.@inside_dataplot);
			if (data.@z_index != undefined) this.zIndex = SerializerBase.getNumber(data.@z_index);
			if (data.@anchor != undefined) {
				switch (SerializerBase.getEnumItem(data.@anchor)) {
					case "center": this.anchor = Anchor.CENTER; break;
					case "leftcenter":
					case "centerleft":
					case "left": 
						this.anchor = Anchor.CENTER_LEFT; break;
					case "lefttop": this.anchor = Anchor.LEFT_TOP; break;
					case "topcenter":
					case "centertop": 
					case "top":
						this.anchor = Anchor.CENTER_TOP; break;
					case "righttop": this.anchor = Anchor.RIGHT_TOP; break;
					case "rightcenter":
					case "centerright":
					case "right": 
						this.anchor = Anchor.CENTER_RIGHT; break;
					case "rightbottom": this.anchor = Anchor.RIGHT_BOTTOM; break;
					case "bottomcenter":
					case "centerbottom":
					case "bottom": 
						this.anchor = Anchor.CENTER_BOTTOM; break;
					case "leftbottom": this.anchor = Anchor.LEFT_BOTTOM; break;
				}
			}
			if (data.@width != undefined) {
				this.isPercentWidth = SerializerBase.isPercent(data.@width);
				this.width = (this.isPercentWidth) ? SerializerBase.getPercent(data.@width) : SerializerBase.getNumber(data.@width);
				this.isWidthSetted = true;
			}
			if (data.@height != undefined) {
				this.isPercentHeight = SerializerBase.isPercent(data.@height);
				this.height = (this.isPercentHeight) ? SerializerBase.getPercent(data.@height) : SerializerBase.getNumber(data.@height);
				this.isHeightSetted = true;
			}
			if (data.@horizontal_padding != undefined) {
				this.isPercentHorizontalPadding = SerializerBase.isPercent(data.@horizontal_padding);
				this.horizontalPadding = (this.isPercentHorizontalPadding) ? SerializerBase.getPercent(data.@horizontalPadding) : SerializerBase.getNumber(data.@horizontal_padding);
			}
			if (data.@vertical_padding != undefined) {
				this.isPercentVerticalPadding = SerializerBase.isPercent(data.@vertical_padding);
				this.verticalPadding = (this.isPercentVerticalPadding) ? SerializerBase.getPercent(data.@vertical_padding) : SerializerBase.getNumber(data.@vertical_padding);
			}
			if (data.@halign != undefined) this.hAlign = SerializerBase.getHorizontalAlign(data.@halign);
			if (data.@valign != undefined) this.vAlign = SerializerBase.getVerticalAlign(data.@valign);
			if (this.insideDataPlot) {
				this.alignByDataPlot = true;
				this.isAlignBySet = true;
			}else {
				if (data.@align_by != undefined) {
					this.alignByDataPlot = !(SerializerBase.getEnumItem(data.@align_by) == "chart");
					this.isAlignBySet = true;
				}else {
					if (this.position == ControlPosition.FLOAT || this.position == ControlPosition.FIXED)
						this.alignByDataPlot = false;
				}
				
			}
			if (this.position != ControlPosition.FLOAT && this.position != ControlPosition.FIXED) {
				if (data.@padding == undefined)
					data.@padding = 10;
				var padding:Number = SerializerBase.getNumber(data.@padding);
				this.margin.all = 0;
				if (this.insideDataPlot) {
					switch (this.position) {
						case ControlPosition.LEFT: this.margin.left = padding; break;
						case ControlPosition.RIGHT: this.margin.right = padding; break;
						case ControlPosition.TOP: this.margin.top = padding; break;
						case ControlPosition.BOTTOM: this.margin.bottom = padding; break;
					}
				}else {
					switch (this.position) {
						case ControlPosition.LEFT: this.margin.right = padding; break;
						case ControlPosition.RIGHT: this.margin.left = padding; break;
						case ControlPosition.TOP: this.margin.bottom = padding; break;
						case ControlPosition.BOTTOM: this.margin.top = padding; break;
					}
				}
				if (data.margin[0] != null)    this.margin.deserialize(data.margin[0]);
			}
		}
	}
}