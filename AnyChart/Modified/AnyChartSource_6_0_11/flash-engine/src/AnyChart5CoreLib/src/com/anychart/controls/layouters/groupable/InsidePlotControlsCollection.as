package com.anychart.controls.layouters.groupable {
	import flash.geom.Rectangle;
	
	public final class InsidePlotControlsCollection extends GroupableControlsCollection {
		
		public function InsidePlotControlsCollection() {
			super();
			this.leftControls = new VerticalInsideControlsLine(false);
			this.rightControls = new VerticalInsideControlsLine(true);
			this.topControls = new HorizontalInsideControlsLine(false);
			this.bottomControls = new HorizontalInsideControlsLine(true);
		}
		
		override public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle):Boolean {
			this.leftControls.basePositionRect = plotRect;
			this.rightControls.basePositionRect = plotRect;
			this.topControls.basePositionRect = plotRect;
			this.bottomControls.basePositionRect = plotRect;
			
			this.leftControls.setControlsLayout(chartRect, plotRect);
			this.rightControls.setControlsLayout(chartRect, plotRect);
			this.topControls.setControlsLayout(chartRect, plotRect);
			this.bottomControls.setControlsLayout(chartRect, plotRect);
			return true;
		}
	}
}