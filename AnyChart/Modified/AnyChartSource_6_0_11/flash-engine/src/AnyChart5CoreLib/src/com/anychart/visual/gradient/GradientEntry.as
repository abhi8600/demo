package com.anychart.visual.gradient {
	
	import com.anychart.serialization.IStyleSerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	
	public class GradientEntry implements IStyleSerializable {
		
		public var isDynamic:Boolean = false;
		public var alpha:Number;
		public var color:uint;
		public var ratio:Number;
		
		public function GradientEntry(clr:uint=0,rt:Number=Number.NaN,a:Number=1) {
			this.color=clr;
			this.ratio=rt;
			this.alpha=a;
		}
		
		public function deserialize(data:XML, style:IStyle = null):void {
			if(data.@position!=undefined) this.ratio=SerializerBase.getRatio(data.@position);
			if(data.@color!=undefined) {
				this.color=SerializerBase.getColor(data.@color);
				if (style != null && SerializerBase.isDynamicColor(data.@color)) {
					this.isDynamic = true;
					style.addDynamicColor(this.color);
				}
			}
			if(data.@opacity!=undefined) this.alpha=SerializerBase.getRatio(data.@opacity);
		}
	}
}