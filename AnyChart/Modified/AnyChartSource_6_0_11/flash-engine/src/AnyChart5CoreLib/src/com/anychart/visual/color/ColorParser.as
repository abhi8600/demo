package com.anychart.visual.color {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.visual.gradient.GradientEntry;
	
	
	public class ColorParser {
		private static var instance:ColorParser;
		
		public static function getInstance():ColorParser {
			if (instance == null)
				instance = new ColorParser();
			return instance;
		}
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(ColorParser);
			return true;
		}
		
		public static function clear():void {
			clearArray(instance.colorsList);
			clearArray(instance.gradientsList);
			clearObject(instance.colorsParseMap);
			clearObject(instance.gradientsParseMap);
			
			instance.colorsList = null;
			instance.colorsParseMap = null;
			instance.gradientsList = null;
			instance.gradientsParseMap = null;
			instance = null;
		}
		
		public function ColorParser() {
			this.colorsList = [];
			this.colorsParseMap = {};
			this.gradientsList = [];
			this.gradientsParseMap = {};
		}
		
		private static function clearArray(arr:Array):void {
			for (var i:uint = 0;i<arr.length;i++) {
				if (arr[i] is Array)
					clearArray(arr[i]);
				else if (arr[i] is Object)
					clearObject(arr[i]);
				else
					arr[i] = null;
				delete arr[i];
			}
		}
		
		private static function clearObject(obj:Object):void {
			for (var j:String in obj) {
				if (obj[j] is Array)
					clearArray(obj[j]);
				else if (obj[j] is Object)
					clearObject(obj[j]);
				else
					obj[j] = null;
				delete obj[j];
			}
		}
		
		private var colorsList:Array;
		private var colorsParseMap:Object;
		
		private var gradientsList:Array;
		private var gradientsParseMap:Object;
		
		public function isDynamic(color:String):Boolean {
			return (color.indexOf('%') != -1);
		}
		
		public function parse(color:String):int {
			if (color.indexOf(" ") != -1)
				color = color.split(" ").join("");
			if (color.indexOf("\t") != -1)
				color = color.split("\t").join("");
				
			color = color.toLowerCase();
			
			var isDynamic:Boolean = this.isDynamic(color);
			
			if (!isDynamic) {
				if (color.indexOf('(') == -1) {
					if (WebColors.isWebColor(color))
						return WebColors.getColor(color);
					else if (color.charAt(0) == '#')
						return Number('0x'+color.substr(1));
					
					return Number(color);
				}
			}else {
				if (this.colorsParseMap[color] != null)
					return this.colorsParseMap[color];
			}
			
			var stack:Array = [];
			var index:uint = 0;
			var notation:Array = [];
			
			while (index < color.length) {
				var fnct:String = this.getFunction(color, index);
				if (fnct != null) {
					stack.push(fnct);
					index += fnct.length;
					continue;
				}
				
				var variable:String = this.getVariable(color, index);
				if (variable != null) {
					stack.push(variable);
					index += variable.length;
					continue;
				}
				
				var colorName:String = this.getColorName(color, index);
				if (colorName != null) {
					stack.push(WebColors.getColor(colorName));
					index += colorName.length;
					continue;
				}
				
				var hexColor:String = this.getHexColor(color, index);
				if (hexColor != null && hexColor.length > 0) {
					stack.push(Number('0x'+hexColor.substr(1)));
					index += hexColor.length;
					continue;
				}
				
				hexColor = this.get0xHexColor(color, index);
				if (hexColor != null && hexColor.length > 0) {
					stack.push(Number(hexColor));
					index += hexColor.length;
					continue;
				}
				
				var char:String = color.charAt(index); 
				
				if (!this.isOperation(char)) {
					hexColor = this.getStringBeforeOp(color, index);
					if (hexColor == null || hexColor.length == 0)
						return 0xCCCCCC;
					notation.push(Number(hexColor));
					index += hexColor.length;
					continue;
				}
				
				var stackTop:String = stack[stack.length - 1];
				
				if (char == ',') {
					while (stackTop != '(' && stack.length > 0) {
						stackTop = stack.pop();
						notation.push(stackTop);
						stackTop = stack[stack.length - 1];
					}
					index++;
					continue;
				}
				
				if (stack.length == 0) {
					stack.push(char);
					index++;
					continue;
				}
				
				if (char == '(') {
					stack.push(char);
					index++;
					continue;
				}
				
				if (char == ')') {
					while (stackTop != '(') {
						stackTop = stack.pop();
						notation.push(stackTop);
						stackTop = stack[stack.length-1];
					}
					if (stackTop == '(') 
						stack.pop();
					index++;
					continue;
				}
				
				var charPrior:uint = this.getPrior(char);
					
				var stackPrior:uint = this.getPrior(stackTop);
				
				while (stack.length > 0 && stackPrior >= charPrior) {
					stackTop = stack.pop();
					notation.push(stackTop);
					stackTop = stack[stack.length - 1];
					
					stackPrior = this.getPrior(stackTop);
				}
				stack.push(char);
				
				index++;				
			}
			
			while (stack.length > 0)
				notation.push(stack.pop());
			
			if (!isDynamic) {
				return this.calc(notation,0xCCCCCC);
			}else {
				var colorIndex:uint = this.colorsList.length;
				this.colorsList.push(color);
				this.colorsList[colorIndex] = {};
				this.colorsList[colorIndex].notation = notation;
				this.colorsParseMap[color] = colorIndex;
				
				return colorIndex;
			}			
			
			return -1;
		}
		
		private function getPrior(character:String):uint {
			if (character == '(')
				return 0;
			if (character == ')')
				return 1;
			return 10;
		}
		
		public function getNotation(index:uint):Array {
			return this.colorsList[index].notation;
		}
		
		public function addNotation(notation:Array):uint {
			var index:uint = this.colorsList.length;
			this.colorsList.push({});
			this.colorsList[index].notation = notation;
			return index;
		}
		
		public function calc(notation:Array, color:uint):uint {
			
			var stack:Array = [];
			
			var c1:Number;
			var c2:Number;
			var c3:Number;
			
			for (var i:uint = 0;i<notation.length;i++) {
				if (notation[i] is Function) {
					var fn:Function = notation[i];
					c1 = stack.pop();
					stack.push(fn.call(null,c1));
				}else {
					var f:String = notation[i];
				
					switch (f) {
						case '%color':
							stack.push(color);
							break;
							
						case 'blend':
							c1 = stack.pop();
							c2 = stack.pop();
							c3 = stack.pop();
							stack.push(ColorUtils.blendColor(c3, c2, c1));
							break;
							
						case 'lightcolor':
							stack.push(ColorUtils.getLightColor(stack.pop()));
							break;
							
						case 'darkcolor':
							stack.push(ColorUtils.getDarkColor(stack.pop()));
							break;
							
						case 'hsb':
							c1 = stack.pop();
							c2 = stack.pop();
							c3 = stack.pop();
							stack.push(ColorUtils.fromHSB(c3, c2, c1));
							break;
							
						case 'rgb':
							c1 = stack.pop();
							c2 = stack.pop();
							c3 = stack.pop();
							stack.push(ColorUtils.fromRGB(c3, c2, c1));
							break;
							
						default:
							stack.push(f);
							break;
					}
				}
			}
			
			var res:uint = stack[0];
			if (res < 0) res = 0;
			else if (res > 0xFFFFFF) res = 0xFFFFFF;
			return res;
		}
		
		/**
		 * Set style color
		 */
		public function setDynamicColor(colorIndex:uint, color:uint):void {
			if (this.colorsList[colorIndex][color] == null) {
				var notation:Array = this.colorsList[colorIndex].notation;
				this.colorsList[colorIndex][color] = this.calc(notation, color);
			}
		}
		
		/**
		 * Get style color
		 */
		public function getDynamicColor(colorIndex:uint, color:uint):uint {
			return this.colorsList[colorIndex][color];
		}
		
		private function getFunction(string:String, index:uint):String {
			var s:String = string.charAt(index);
			if (s == 'b' || s == 'l' || s == 'd' || s == 'r' || s == 'h') { 
				var endIndex:int = string.indexOf('(',index);
				if (endIndex == -1) return null;
				var fnct:String = string.substring(index, endIndex);
				return (fnct == 'blend' ||
						fnct == 'lightcolor' ||
						fnct == 'darkcolor' ||
						fnct == 'rgb' ||
						fnct == 'hsb') ? fnct : null;
			}
			return null;
		}
		
		private function getVariable(string:String, index:uint):String {
			if (string.charAt(index) != '%') return null;
			var variable:String = this.getStringBeforeOp(string, index);
			return (variable == '%color') ? '%color' : null;
		}
		
		private function getColorName(string:String, index:uint):String {
			var variable:String = this.getStringBeforeOp(string, index);
			return WebColors.isWebColor(variable) ? variable : null;
		}
		
		private function getHexColor(string:String, index:uint):String {
			if (string.charAt(index) != '#') return null;
			return this.getStringBeforeOp(string, index);
		}
		
		private function get0xHexColor(string:String, index:uint):String {
			if (string.substr(index,2) != '0x') return null;
			return this.getStringBeforeOp(string, index);
		}
		
		private function getStringBeforeOp(string:String, index:uint):String {
			var endIndex:int = index;
			while ((endIndex < string.length) && !isOperation(string.charAt(endIndex)))
				endIndex++;
			return string.substring(index, endIndex);
		}
		
		private function isOperation(char:String):Boolean {
			return char == '(' ||
				   char == ')' ||
				   char == ',';
		}
		
		public function setDynamicGradient(keys:Array, keysXML:XMLList):uint {
			var keysStringXML:String = keysXML.toXMLString();
			if (this.gradientsParseMap[keysStringXML] != null) {
				return this.gradientsParseMap[keysStringXML];
			}else {
				var index:uint = this.setUniqueDynamicGradient(keys);
				this.gradientsParseMap[keysStringXML] = index;
				return index;
			}
		}
		
		public function setUniqueDynamicGradient(keys:Array):uint {
			var colors:Array = [];
			for (var i:uint = 0;i<keys.length;i++) {
				var key:GradientEntry = keys[i];
				colors.push([key.isDynamic,key.color]);
			}
			var index:uint = this.gradientsList.length;
			this.gradientsList.push({colors:colors});
			return index;
		}
		
		public function initDynamicGradient(gradientIndex:uint, color:uint):void {
			if (this.gradientsList[gradientIndex][color] != null) return;
			var gradient:Object = this.gradientsList[gradientIndex];
			var resColors:Array = [];
			var colors:Array = this.gradientsList[gradientIndex].colors;
			for (var i:uint = 0;i<colors.length;i++) {
				var colorExp:Array = colors[i];
				if (colorExp[0] == true) {
					this.setDynamicColor(colorExp[1],color);
					resColors.push(this.getDynamicColor(colorExp[1],color));
				}else {
					resColors.push(colorExp[1]);
				}
			}
			this.gradientsList[gradientIndex][color] = resColors;
		}
		
		public function getDynamicGradient(gradientIndex:uint, color:uint):Array {
			return this.gradientsList[gradientIndex][color];
		}
	}
}