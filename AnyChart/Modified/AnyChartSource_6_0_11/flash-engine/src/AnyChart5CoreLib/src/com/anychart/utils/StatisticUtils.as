package com.anychart.utils {
	import com.anychart.formatters.IFormatable;
	
	public final class StatisticUtils {
		
		private static var fieldName:String;
		
		public static function getMedian(points:Array, field:String):Number {
			
			var sortedPoints:Array = getSortedPoints(points, field);
			
			var point:IFormatable;
			var point1:IFormatable;
			var point2:IFormatable;
			
			if (sortedPoints.length % 2 == 0) {
				point1 = sortedPoints[Math.floor(sortedPoints.length/2)-1];
				point2 = sortedPoints[Math.floor(sortedPoints.length/2)];
				if (point1 == null || point2 == null) return NaN;
				
				var val1:Number = Number(point1.getTokenValue(fieldName));
				var val2:Number = Number(point2.getTokenValue(fieldName));
				
				return (val1 + val2)/2;
			}else {
				point = sortedPoints[Math.floor(sortedPoints.length/2)]; // cause 0-based indexes
				if (point == null) return NaN;
				return Number(point.getTokenValue(fieldName));
			}
		}
		
		public static function getMode(points:Array, field:String):Number {
			
			fieldName = field;
			
			var sortedPoints:Array = getSortedPoints(points, field);
			
			var cTable:Object = {};
			
			var i:uint;
			var mode:Number = 0;
			var maxCnt:int = 0;
			for (i = 0;i<sortedPoints.length;i++) {
				var contains:Boolean = false;
				var value:Number = sortedPoints[i].getTokenValue(fieldName);
				if (cTable[value] != null)
					cTable[value]++;
				else
					cTable[value] = 1;
					
				if (cTable[value] > maxCnt) {
					maxCnt = cTable[value];
					mode = value;
				}
			}
			
			return mode;
		}
		
		private static function getSortedPoints(points:Array, field:String):Array {
			
			fieldName = field;
			
			var newPoints:Array = new Array();
			var i:uint = 0;
			for (i = 0;i<points.length;i++)
				newPoints.push(points[i]);
			newPoints.sort(sortPointsByValue);
			
			return newPoints;
		}
		
		private static function sortPointsByValue(first:IFormatable, next:IFormatable):Number {
			var firstValue:Number = Number(first.getTokenValue(fieldName));
			var nextValue:Number = Number(next.getTokenValue(fieldName));
			if (firstValue > nextValue)
				return 1;
			else if (firstValue < nextValue)
				return -1;
			return 0;
		}		
	}
}