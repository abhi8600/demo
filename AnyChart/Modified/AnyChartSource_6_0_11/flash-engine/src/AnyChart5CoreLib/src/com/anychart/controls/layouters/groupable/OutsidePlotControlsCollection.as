package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlPosition;
	import com.anychart.visual.layout.Margin;
	
	import flash.geom.Rectangle;
	
	public final class OutsidePlotControlsCollection extends GroupableControlsCollection {
		
		public var defaultAlignLeftByDataPlot:Boolean;
		public var defaultAlignRightByDataPlot:Boolean;
		public var defaultAlignTopByDataPlot:Boolean;
		public var defaultAlignBottomByDataPlot:Boolean;
		
		public function OutsidePlotControlsCollection() {
			super();
			
			this.defaultAlignLeftByDataPlot = true;
			this.defaultAlignRightByDataPlot = true;
			this.defaultAlignTopByDataPlot = true;
			this.defaultAlignBottomByDataPlot = true;
			
			this.leftControls = new VerticalControlsLine(false);
			this.rightControls = new VerticalControlsLine(true);
			this.topControls = new HorizontalControlsLine(false);
			this.bottomControls = new HorizontalControlsLine(true);
		}
		
		override public function addControl(control:Control):void {
			if (!control.layout.isAlignBySet) {
				switch (control.layout.position) {
					case ControlPosition.LEFT: control.layout.alignByDataPlot = this.defaultAlignLeftByDataPlot; break;
					case ControlPosition.RIGHT: control.layout.alignByDataPlot = this.defaultAlignRightByDataPlot; break;
					case ControlPosition.TOP: control.layout.alignByDataPlot = this.defaultAlignTopByDataPlot; break;
					case ControlPosition.BOTTOM: control.layout.alignByDataPlot = this.defaultAlignBottomByDataPlot; break;
				}
			}
			super.addControl(control);
		}
		
		override public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle):Boolean {
			var newMargin:Margin = new Margin();
			this.setMargin(newMargin, plotRect, chartRect);
			var isValid:Boolean = (newMargin.left == this.margin.left) &&
								   (newMargin.right == this.margin.right) &&
								   (newMargin.top == this.margin.top) &&
								   (newMargin.bottom == this.margin.bottom);
			this.margin = newMargin;
			return isValid;
		}
		
		private var margin:Margin;
		
		public function setLayout(plotRect:Rectangle, chartRect:Rectangle):void {
			this.margin = new Margin();
			this.setMargin(this.margin, plotRect, chartRect);
			this.cropPlot(plotRect);
		}
		
		public function cropPlot(plotRect:Rectangle):void {
			plotRect.left += this.margin.left;
			plotRect.right -= this.margin.right;
			plotRect.top += this.margin.top;
			plotRect.bottom -= this.margin.bottom;
		}
		
		private function setMargin(margin:Margin, plotRect:Rectangle, chartRect:Rectangle):void {
			
			this.leftControls.basePositionRect = chartRect;
			this.rightControls.basePositionRect = chartRect;
			this.topControls.basePositionRect = chartRect;
			this.bottomControls.basePositionRect = chartRect;
			
			margin.top = this.topControls.setControlsLayout(chartRect, plotRect);
			margin.bottom = this.bottomControls.setControlsLayout(chartRect, plotRect);
			
			margin.left = this.leftControls.setControlsLayout(chartRect, plotRect);
			margin.right = this.rightControls.setControlsLayout(chartRect, plotRect);
		}
	}
}