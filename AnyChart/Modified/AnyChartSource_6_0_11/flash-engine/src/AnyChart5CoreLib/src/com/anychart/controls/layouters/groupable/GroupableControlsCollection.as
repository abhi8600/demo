package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlPosition;
	import com.anychart.controls.layouters.ControlsCollection;
	
	internal class GroupableControlsCollection extends ControlsCollection {
		protected var leftControls:VerticalControlsLine;
		protected var rightControls:VerticalControlsLine;
		protected var topControls:HorizontalControlsLine;
		protected var bottomControls:HorizontalControlsLine;
		
		public function GroupableControlsCollection() {
			super();
		}
		
		override public function addControl(control:Control):void {
			switch (control.layout.position) {
				case ControlPosition.LEFT:
					this.leftControls.addControl(control);
					break;
				case ControlPosition.RIGHT:
					this.rightControls.addControl(control);
					break;
				case ControlPosition.TOP:
					this.topControls.addControl(control);
					break;
				case ControlPosition.BOTTOM:
					this.bottomControls.addControl(control);
					break;
			}
		}
	}
}