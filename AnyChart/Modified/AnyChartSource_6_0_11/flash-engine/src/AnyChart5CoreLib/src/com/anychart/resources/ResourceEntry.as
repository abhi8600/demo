package com.anychart.resources {
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	
	/**
	 * Абстрактный класс для описания ресурса
	 * Ресурсом в AnyChart является любой загружаемый объект
	 * 
	 * @see ResourcesLoader
	 */
	internal /* abstract */ class ResourceEntry extends EventDispatcher {
		
		/**
		 * Идет-ли в данный момент загрузка ресурса
		 * Используется для прерывания загрузки
		 */
		protected var isLoading:Boolean;
		
		/**
		 * URLRequest для получения данных
		 */
		protected var request:URLRequest;
		
		/**
		 * Относительный путь к ресурсу
		 */
		internal var path:String;
		
		public function ResourceEntry() {
			this.isLoading = false;
		}
		
		private var initialPath:String;
		
		public function getPath():String { return this.initialPath; }
		
		public function initialize(path:String, enableCashe:Boolean = true):void {
			this.initialPath = path;
			this.path = path;
			if (!enableCashe) {
				if (path.lastIndexOf("?") == -1)
					path += "?";
				else if (path.lastIndexOf("&") != path.length - 1)
					path += "&";
				path += "XMLCallDate="+(new Date().getTime());
			}
			this.request = new URLRequest(path);
		}
		
		public final function load():void {
			this.startLoading();
		}
		
		private function startLoading():void {
			try {
				this.execLoading();
				this.isLoading = true;
			}catch(se:SecurityError) {
				this.securityErrorEventHandler(new SecurityErrorEvent(SecurityErrorEvent.SECURITY_ERROR));
			}catch(ioe:IOError) {
				this.ioErrorEventHandler(new IOErrorEvent(IOErrorEvent.IO_ERROR));
			}
		}
		
		protected function execLoading():void {}
		public function stopLoading():void {}
		protected function setData(event:ResourceEntryEvent):void {}
		
		protected function initListeners(target:IEventDispatcher):void {
			target.addEventListener(HTTPStatusEvent.HTTP_STATUS, this.httpStatusEventHandler);
			target.addEventListener(ProgressEvent.PROGRESS, this.progressEventHandler);
			target.addEventListener(IOErrorEvent.IO_ERROR, this.ioErrorEventHandler);
			target.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.securityErrorEventHandler);
			this.initCompleteListener(target);
		}
		
		protected function initCompleteListener(target:IEventDispatcher):void {
			target.addEventListener(Event.COMPLETE, this.completeEventHandler);
		}
		
		private function httpStatusEventHandler(event:HTTPStatusEvent):void {
			if (event.status == 404)
				this.ioErrorEventHandler(new IOErrorEvent(IOErrorEvent.IO_ERROR));
		}
		
		private function progressEventHandler(event:ProgressEvent):void {
			this.bubbleEvent(event);
		}
		
		protected function completeEventHandler(event:Event):void {
			var completeEvent:ResourceEntryEvent = new ResourceEntryEvent(ResourceEntryEvent.COMPLETE);
			this.setData(completeEvent);
			this.dispatchEvent(completeEvent);
		}
		
		private function ioErrorEventHandler(event:IOErrorEvent):void {
			trace ("Error loading "+this.request.url);
			this.stopLoading();
			this.bubbleEvent(event);
		}
		
		private function securityErrorEventHandler(event:SecurityErrorEvent):void {
			this.stopLoading();
			this.bubbleEvent(event);
		}
		
		private function bubbleEvent(event:Event):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			this.dispatchEvent(event.clone());
		}
	}
}