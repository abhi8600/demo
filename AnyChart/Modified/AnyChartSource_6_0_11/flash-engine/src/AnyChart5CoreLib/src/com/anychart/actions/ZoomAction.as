package com.anychart.actions{
	
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;

	public class ZoomAction extends Action{
		
		protected var start:String;
		protected var isDinamicStart:Boolean;
		protected var startFormater:TextFormater;
		
		protected var end:String;
		protected var isDinamicEnd:Boolean;
		protected var endFormater:TextFormater;
		
		protected var range:String;
		protected var isDinamicRange:Boolean;
		protected var rangeFormater:TextFormater;
		
		protected var rangeUnit:String;
		
		protected var xStart:String;
		protected var xIsDinamicStart:Boolean;
		protected var xStartFormater:TextFormater;
		
		protected var xEnd:String;
		protected var xIsDinamicEnd:Boolean;
		protected var xEndFormater:TextFormater;
		
		protected var xRange:String;
		protected var xIsDinamicRange:Boolean;
		protected var xRangeFormater:TextFormater;
		
		protected var xRangeUnit:String;
		
		protected var yZoomSettings:Object;
		protected var xZoomSettings:Object;
		
		public function ZoomAction(chart:IAnyChart){
			super(chart);
		}
		
		override public function execute(formatter:IFormatable):Boolean {
			this.doExecute(formatter);
			this.chart.setZoom(this.xZoomSettings,this.yZoomSettings);
			
			return false;	
		}
		
		override public function deserialize(data:XML):void {
			this.isValidXML = data.@x_start != undefined 
							|| data.@x_end != undefined 
							|| data.@x_range != undefined 
							|| data.@x_range_unit != undefined 
							|| data.@y_start != undefined 
							|| data.@y_end != undefined 
							|| data.@y_range != undefined 
							|| data.@y_range_unit != undefined ;
			if (!this.isValidXML) return;
			
			if (data.@y_start != undefined){
				this.start = SerializerBase.getString(data.@y_start);
				this.isDinamicStart = FormatsParser.isDynamic(this.start);
				if (this.isDinamicStart) this.startFormater = FormatsParser.parse(this.start);
			}
			
			if (data.@y_end != undefined){
				this.end = SerializerBase.getString(data.@y_end);
				this.isDinamicEnd = FormatsParser.isDynamic(this.end);
				if (this.isDinamicEnd) this.endFormater = FormatsParser.parse(this.end);
			}
			
			if (data.@y_range != undefined){
				this.range = SerializerBase.getString(data.@y_range);
				this.isDinamicRange = FormatsParser.isDynamic(this.range);
				if (this.isDinamicRange) this.rangeFormater = FormatsParser.parse(this.range);
			}
			
			if (data.@y_range_unit != undefined) {
				this.rangeUnit = SerializerBase.getString(data.@y_range_unit);
			}
			
			if (data.@x_start != undefined){
				this.xStart = SerializerBase.getString(data.@x_start);
				this.xIsDinamicStart = FormatsParser.isDynamic(this.xStart);
				if (this.xIsDinamicStart) this.xStartFormater = FormatsParser.parse(this.xStart);
			}
			
			if (data.@x_end != undefined){
				this.xEnd = SerializerBase.getString(data.@x_end);
				this.xIsDinamicEnd = FormatsParser.isDynamic(this.xEnd);
				if (this.xIsDinamicEnd) this.xEndFormater = FormatsParser.parse(this.xEnd);
			}
			
			if (data.@x_range != undefined){
				this.xRange = SerializerBase.getString(data.@x_range);
				this.xIsDinamicRange = FormatsParser.isDynamic(this.xRange);
				if (this.xIsDinamicRange) this.xRangeFormater = FormatsParser.parse(this.xRange);
			}
			
			if (data.@x_range_unit != undefined) {
				this.xRangeUnit = SerializerBase.getString(data.@x_range_unit);
			}
		}
		
		protected function doExecute(formatter:IFormatable):void{
			if (this.start || this.end || this.range || this.rangeUnit){
				this.yZoomSettings = new Object;
				this.yZoomSettings.start = this.isDinamicStart ? (this.startFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.start);
				this.yZoomSettings.end = this.isDinamicEnd ? (this.endFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.end);
				this.yZoomSettings.range = this.isDinamicRange ? (this.rangeFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.range);
				this.yZoomSettings.rangeUnit = this.rangeUnit;	
			}
			if (this.xEnd || this.xStart || this.xRange || this.xRangeUnit){
				this.xZoomSettings = new Object;	
				this.xZoomSettings.start = this.xIsDinamicStart ? (this.xStartFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.xStart);
				this.xZoomSettings.end = this.xIsDinamicEnd ? (this.xEndFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.xEnd);
				this.xZoomSettings.range = this.xIsDinamicRange ? (this.xRangeFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)) : (this.xRange);
				this.xZoomSettings.rangeUnit = this.xRangeUnit;
			}
		}
	}
}