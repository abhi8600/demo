package com.anychart.treeMapPlot.style {

	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public interface ITreeMapBranchTitle {

		function getBounds(info:TextElementInformation):void;
		function drawHeader(container:DisplayObject, bounds:Rectangle, info:TextElementInformation, color:uint, hatchType:uint):void;
		function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void;
		function getHeight():Number;
		function hasHeight():Boolean;
	}
}