package com.anychart.treeMapPlot.data {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.thresholds.IThreshold;
	
	
	public final class TreeMapDeserializationParams {
		
		public var xmlData:XML;
		public var xmlStyles:XML;
		public var xmlPalletes:XML;
		public var resources:ResourcesLoader; 
		public var index:int;
		public var threshold:IThreshold;
		
		public function createCopy():TreeMapDeserializationParams{
			var deserParams:TreeMapDeserializationParams = new TreeMapDeserializationParams();
			deserParams.xmlData = this.xmlData;
			deserParams.xmlStyles = this.xmlStyles;
			deserParams.xmlPalletes = this.xmlPalletes;
			deserParams.resources = this.resources;
			deserParams.index = this.index;
			deserParams.threshold = this.threshold;
			return deserParams;
		}
	}
}