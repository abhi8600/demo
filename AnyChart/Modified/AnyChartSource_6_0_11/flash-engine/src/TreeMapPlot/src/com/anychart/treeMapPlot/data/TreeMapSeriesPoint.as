package com.anychart.treeMapPlot.data {
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.treeMapPlot.TreeMapPlot;
	import com.anychart.treeMapPlot.style.ITreeMapBranchTitle;
	import com.anychart.treeMapPlot.style.TreeMapBaseStyleState;
	import com.anychart.visual.text.BaseTitle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	
	public final class TreeMapSeriesPoint extends TreeMapBasePoint {
		
		private var headerInfo:TextElementInformation;
		
		override protected function drawBox():void {
			var state:TreeMapBaseStyleState = TreeMapBaseStyleState(this.styleState ? this.styleState : this.settings.style.normal);
			var g:Graphics = this.container.graphics;
			if (this.hasPersonalContainer && state.background != null)
				this.container.filters = state.background.effects ? state.background.effects.list : null;
			if (state.background)
				state.background.draw(g, this.bounds, this._color, this._hatchType);
			if (state.header) {
				var header:BaseTitle = BaseTitle(state.header);
				var headerHeight:Number = this.getHeaderHeight();
				this.initHeaderInfo();
				var headerBackgroundEnable:Boolean = header.background.enabled;
				if (header.background != null && headerBackgroundEnable && headerHeight != 0){
					var headerBackgroundBounds:Rectangle = new Rectangle(this.bounds.x, this.bounds.y, this.bounds.width, Math.min(headerHeight, this.bounds.height));
					if (header.background.border != null && header.background.border.thickness != 0){
						var dFill:Number = header.background.border.thickness/2;
						headerBackgroundBounds.x -= dFill;
						headerBackgroundBounds.y -= dFill;
						headerBackgroundBounds.width += dFill*2;
						headerBackgroundBounds.height += dFill*2;
					}
					header.background.drawWithoutClearing(g, headerBackgroundBounds, this._color, this._hatchType);
					header.background.enabled = false;
					state.header.drawHeader(this.container, headerBackgroundBounds, this.headerInfo, this._color, this._hatchType);
					header.background.enabled = headerBackgroundEnable;
				} else 
					state.header.drawHeader(this.container, this.bounds, this.headerInfo, this._color, this._hatchType);
			}  
		}
		
		private function initHeaderInfo():void {
			if (!this.headerInfo) {
				var state:TreeMapBaseStyleState = TreeMapBaseStyleState(this.styleState ? this.styleState : this.settings.style.normal);
				var header:BaseTitle = BaseTitle(state.header);
				this.headerInfo = header.createInformation();
				this.headerInfo.formattedText = header.isDynamicText ? header.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : header.text;
				state.header.getBounds(this.headerInfo);
			}
		}
		
		public function getHeaderHeight():Number {
			var state:TreeMapBaseStyleState = TreeMapBaseStyleState(this.styleState ? this.styleState : this.settings.style.normal);
			var header:ITreeMapBranchTitle = ITreeMapBranchTitle(state.header);
			if (header == null)
				return 0;
			if (header.hasHeight())
				return header.getHeight();
			this.initHeaderInfo();
			return this.headerInfo ? this.headerInfo.rotatedBounds.height : 0;
		}
		
		override protected function execDrawing():void {
			this.container.x = bounds.x;
			this.container.y = bounds.y;
			bounds.x = 0;
			bounds.y = 0;
			super.execDrawing();
		} 
		
		override public function deserializeData(deserParams:TreeMapDeserializationParams):void {
			this.series = BaseSeries(this._parent);
		
			this.settings = this._parent.settings;
			this.index = this._parent.index;
			this.name = this._parent.index.toString();			
			this.color = this._parent.color;
			this.hatchType = this._parent.hatchType;			
			this.markerType = this._parent.markerType;
			this.threshold = this._parent.threshold;
			this.name = this._parent.name;
			
			this.createContainer();
			this.initializeStyle();
			this.initialize();		
		}
		
		override protected function mouseClickActionsExec():void {
			super.mouseClickActionsExec();
			if (TreeMapGlobalSeriesSettings(this.settings.global).enableDrilldown)
				TreeMapPlot(this.plot).drillDown(this.parent);			
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == "%Value" || token == "%YValue") return this._value;
			return super.getPointTokenValue(token); 
		}
	}
}