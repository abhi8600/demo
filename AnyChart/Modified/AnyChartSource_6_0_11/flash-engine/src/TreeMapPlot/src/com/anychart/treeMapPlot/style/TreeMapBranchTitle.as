package com.anychart.treeMapPlot.style {
	
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	internal final class TreeMapBranchTitle extends TreeMapBaseBranchTitle implements ITreeMapBranchTitle {
		
		public function drawHeader(container:DisplayObject, bounds:Rectangle, info:TextElementInformation, color:uint, hatchType:uint):void {
			var x:Number;
			switch (this.align){
				case AbstractAlign.NEAR:
					x = bounds.x;
					break;
				case AbstractAlign.CENTER:
					x = bounds.x + (bounds.width - info.rotatedBounds.width) / 2;
					break;
				case AbstractAlign.FAR:
					x = bounds.x + (bounds.width - info.rotatedBounds.width);
					break;	
			}
			this.draw(Sprite(container).graphics, x, bounds.y, info, color, hatchType);
		}
	}
}