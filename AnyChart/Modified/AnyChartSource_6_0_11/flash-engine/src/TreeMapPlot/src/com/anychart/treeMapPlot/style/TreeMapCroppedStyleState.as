package com.anychart.treeMapPlot.style {
	import com.anychart.styles.Style;

	public final class TreeMapCroppedStyleState extends TreeMapBaseStyleState {
		
		public function TreeMapCroppedStyleState(style:Style) {
			super(style);
		}
		
		override protected function createBranchTitle():ITreeMapBranchTitle {
			return new TreeMapCroppedBranchTitle();
		}
	}
}