package com.anychart.treeMapPlot.data {
	
	internal final class ByParentNode {
		
		public var xmlData:XML;
		public var children:Array;		
		
		public function ByParentNode(){
			this.xmlData = null;
			this.children = new Array();
		}
	}
}