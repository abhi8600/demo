package com.anychart.treeMapPlot.style {
	import com.anychart.styles.Style;

	public final class TreeMapStyleState extends TreeMapBaseStyleState {
		
		public function TreeMapStyleState(style:Style) {
			super(style);
		}
		
		override protected function createBranchTitle():ITreeMapBranchTitle {
			return new TreeMapBranchTitle();
		}
	}
}