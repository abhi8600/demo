package com.anychart.controls.legend {
	import com.anychart.elements.marker.MarkerDrawer;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.TextFormater;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	
	internal final class LegendItemsGroup implements ISerializableRes {
		
		private var leftTextInfo:TextElementInformation;
		private var rightTextInfo:TextElementInformation;
		
		public var plot:IPlot;
		
		private var text:BaseTextElement;
		private var hasIcon:Boolean;
		private var icon:IconSettings;
		
		private var hasLeftText:Boolean;
		private var leftTextFormat:String;
		private var isDynamicLeftText:Boolean;
		private var leftTextFormater:TextFormater;
		
		private var hasRightText:Boolean;
		private var rightTextFormat:String;
		private var isDynamicRightText:Boolean;
		private var rightTextFormater:TextFormater;
		
		private var fixedWidth:Number;
		private var fixedHeight:Number;
		
		public function LegendItemsGroup(plot:IPlot) {
			this.plot = plot;
			this.fixedWidth = NaN;
			this.fixedHeight = NaN;
		}
		
		public function deserializeDefault(data:XML, resources:ResourcesLoader):void {
			this.text = new BaseTextElement();
			this.text.deserialize(data, resources);
			this.text.background = null;
			
			this.leftTextInfo = this.text.createInformation();
			this.rightTextInfo = this.text.createInformation();
			
			this.deserialize(data, resources);
			if (this.plot == null) return;
			this.icon = this.plot.createIconSettings();
			if (data.icon[0] != null)
				this.icon.deserialize(data.icon[0], resources);
				
			if (data.@fixed_item_width != undefined) this.fixedWidth = SerializerBase.getNumber(data.@fixed_item_width);
			if (data.@fixed_item_height != undefined) this.fixedHeight = SerializerBase.getNumber(data.@fixed_item_height);
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.format[0] != null)
				this.processFormat(SerializerBase.getCDATAString(data.format[0]));
			else if (data.text[0] != null)
				this.processFormat(SerializerBase.getCDATAString(data.text[0]));
			if (this.icon != null) {
				this.icon = this.icon.createCopy();
				if (data.icon[0] != null)
					this.icon.deserialize(data.icon[0], resources);
			}
		}
		
		public function createCopy():LegendItemsGroup {
			var group:LegendItemsGroup = new LegendItemsGroup(this.plot);
			group.hasIcon = this.hasIcon;
			
			group.hasLeftText = this.hasLeftText;
			group.leftTextFormat = this.leftTextFormat;
			group.leftTextFormater = this.leftTextFormater;
			group.isDynamicLeftText = this.isDynamicLeftText;
			
			group.hasRightText = this.hasRightText;
			group.rightTextFormat = this.rightTextFormat;
			group.rightTextFormater = this.rightTextFormater;
			group.isDynamicRightText = this.isDynamicRightText;
			
			group.leftTextInfo = this.leftTextInfo;
			group.rightTextInfo = this.rightTextInfo;
			
			group.text = this.text;
			group.icon = this.icon;
			
			group.fixedWidth = this.fixedWidth;
			group.fixedHeight = this.fixedHeight;
			
			return group;
		}
		
		public function createItem(source:ILegendItem):LegendItem {
			var item:LegendItem = new LegendItem();
			//bounds calculation
			if (this.hasLeftText) {
				this.setLeftText(source, item);
				item.contentBounds.width += this.leftTextInfo.rotatedBounds.width;
				item.contentBounds.height = Math.max(this.leftTextInfo.rotatedBounds.height, item.contentBounds.height);
			}
			if (this.hasRightText) {
				this.setRightText(source, item);
				item.contentBounds.width += this.rightTextInfo.rotatedBounds.width;
				item.contentBounds.height = Math.max(this.rightTextInfo.rotatedBounds.height, item.contentBounds.height);
			}
			if (this.hasIcon) {
				item.contentBounds.width += this.icon.width;
				item.contentBounds.height = Math.max(this.icon.height, item.contentBounds.height);
			}
			
			//item drawing
			var g:Graphics = item.graphics;
			var x:Number = 0;
			var y:Number;
			if (this.hasLeftText) {
				y = (item.contentBounds.height - this.leftTextInfo.rotatedBounds.height)/2;
				this.text.draw(g, 0, y, this.leftTextInfo, source.color, source.hatchType);
				x += this.leftTextInfo.rotatedBounds.width;
			}
			if (this.hasIcon) {
				var iconShape:Shape = new Shape();
				y = (item.contentBounds.height - this.icon.height)/2;
				this.plot.drawDataIcon(this.icon, source, iconShape.graphics, 0, 0);
				
				item.addChild(iconShape);
				
				if (source.iconCanDrawMarker && this.icon.markerEnabled) {
					var type:int = (this.icon.markerType == -1) ? source.markerType : this.icon.markerType;
					if (type != -1) {
						var markerSize:Number = this.icon.getMarkerSize();
						var markerShape:Shape = new Shape();
						
						var color:int = this.icon.markerColor == -1 ? source.color : this.icon.markerColor;
						markerShape.graphics.lineStyle(1, ColorUtils.getDarkColor(color), 1);
						markerShape.graphics.beginFill(color, 1);
						MarkerDrawer.draw(markerShape.graphics, type, markerSize,0,0);
						markerShape.graphics.endFill();
						markerShape.graphics.lineStyle();
						
						markerShape.x = x + (this.icon.width - markerSize)/2;
						markerShape.y = y + (this.icon.height - markerSize)/2;
						
						item.addChild(markerShape);
					}
				}
				iconShape.x = x;
				iconShape.y = y;
				
				x += this.icon.width;
			}
			
			if (this.hasRightText) {
				y = (item.contentBounds.height - this.rightTextInfo.rotatedBounds.height)/2;
				this.text.draw(g, x, y, this.rightTextInfo, source.color, source.hatchType);
			}
			
			item.initTag(source.tag);
			
			if (!isNaN(this.fixedWidth)) item.contentBounds.width = this.fixedWidth;
			if (!isNaN(this.fixedHeight)) item.contentBounds.height = this.fixedHeight;
			
			return item;
		}
		
		private function setLeftText(source:ILegendItem, item:LegendItem):void {
			this.leftTextInfo.formattedText = this.isDynamicLeftText ? this.leftTextFormater.getValue(source.tag.getTokenValue, source.tag.isDateTimeToken) : this.leftTextFormat;
			this.text.getBounds(this.leftTextInfo);
		}
		
		private function setRightText(source:ILegendItem, item:LegendItem):void {
			this.rightTextInfo.formattedText = this.isDynamicRightText ? this.rightTextFormater.getValue(source.tag.getTokenValue, source.tag.isDateTimeToken) : this.rightTextFormat;
			this.text.getBounds(this.rightTextInfo);
		}
		
		//-------------------------------------------------------------------
		//					FORMAT
		//-------------------------------------------------------------------
		
		private function processFormat(format:String):void {
			this.hasLeftText = false;
			this.hasRightText = false;
			
			var iconIndex:int = format.indexOf("{%Icon}"); 
			this.hasIcon = iconIndex != -1;
			
			if (!this.hasIcon) {
				this.hasLeftText = true;
				this.leftTextFormat = format;
				this.isDynamicLeftText = FormatsParser.isDynamic(format);
				if (this.isDynamicLeftText)
					this.leftTextFormater = FormatsParser.parse(format);
			}else {
				if (iconIndex > 0) {
					this.hasLeftText = true;
					this.leftTextFormat = format.substring(0,iconIndex);
					this.isDynamicLeftText = FormatsParser.isDynamic(this.leftTextFormat);
					if (this.isDynamicLeftText)
						this.leftTextFormater = FormatsParser.parse(this.leftTextFormat);
				}
				if (iconIndex < format.length - 7) {
					this.hasRightText = true;
					this.rightTextFormat = format.substring(iconIndex+7);
					this.isDynamicRightText = FormatsParser.isDynamic(this.rightTextFormat);
					if (this.isDynamicRightText)
						this.rightTextFormater = FormatsParser.parse(this.rightTextFormat);
				}
			}
		}
		
		//-------------------------------------------------------------------
		//					ICON
		//-------------------------------------------------------------------
	}
}