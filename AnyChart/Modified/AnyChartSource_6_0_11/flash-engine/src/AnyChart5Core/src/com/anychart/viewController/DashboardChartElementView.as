package com.anychart.viewController {
	import com.anychart.AnyChart;
	import com.anychart.Chart;
	import com.anychart.IChart;
	import com.anychart.animation.event.AnimationEvent;
	import com.anychart.dashboard.ChartElement;
	import com.anychart.events.DashboardEvent;
	import com.anychart.plot.PlotFactory;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	
	public final class DashboardChartElementView extends ChartView implements IObjectSerializable {
		
		public var dashboardElement:ChartElement;
		
		public function DashboardChartElementView(base:AnyChart, dashboardElement:ChartElement) {
			this.dashboardElement = dashboardElement;
			super(base);
		}
		
		override protected function processXML(data:XML):XML {
			var newData:XML = super.processXML(data);
			if (newData != null)
				this.generateChart(newData);
			return newData;
		}
		
		override protected function createElement(data:XML):void {
			this.element = new Chart(this.base, this);
			PlotFactory.createSinglePlot(this, Chart(this.element),data);
		}
		
		override protected function dispatchRenderEvent():void {
			if (!this.isInRefresh)
				this.base.dispatchEvent(new DashboardEvent(DashboardEvent.DASHBOARD_VIEW_RENDER, this.dashboardElement.name));
		}
		
		override protected function dispatchDrawEvent():void {
			if (this.element && 
			    IChart(this.element).getAnimation() && 
				IChart(this.element).getAnimation().isAnimated()) {
				IChart(this.element).getAnimation().addEventListener(AnimationEvent.ANIMATION_FINISH, this.animationFinish);
			}else {
				this.execDispatchDrawEvent();
			}
		}
		
		override protected function execDispatchDrawEvent():void {
			this.base.dispatchViewDrawEvent(this, this.dashboardElement.name);
		}
		
		public function serialize(res:Object = null):Object {
			if (!res) res = {};
			res.ViewName = this.dashboardElement.name;
			this.element.serialize(res);
			return res;
		}
		
		override protected function showNoData():void {
			if (this.element)
				IChart(this.element).showNoData(null);
			else
				super.showNoData();
		}
	}
}