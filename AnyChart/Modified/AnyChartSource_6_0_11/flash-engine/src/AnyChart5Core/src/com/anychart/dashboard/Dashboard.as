package com.anychart.dashboard {
	import com.anychart.AnyChart;
	import com.anychart.Chart;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.utils.XMLUtils;
	import com.anychart.viewController.DashboardChartElementView;
	import com.anychart.viewController.IViewElement;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class Dashboard extends Panel implements IViewElement {
		
		public var base:AnyChart;
		
		public function Dashboard(base:AnyChart) {
			this.base = base;
			this.views = {};
			this.viewsList = [];
			super(this);
			
			if (getDefinitionByName("com.anychart.plot::PlotFactoryBase").IS_ORACLE)
				throw new FeatureNotSupportedError("Dashboard not supported in this swf version");
		}
		
		public function destroy():void {
			for each (var view:DashboardChartElementView in this.viewsList) {
				if (view.element != null)
					view.element.destroy();
			}
		}
		
		public function getTitle():String {
			return (this.title != null) ? this.title.info.formattedText : null;
		}
		
		public function getBounds():Rectangle {
			return this.bounds;
		}
		
		public function getContainer():Sprite {
			return this.container;
		}
		
		private var globalMargin:Margin;
		public function setMargin(margin:Margin):void { this.globalMargin = margin; }
		
		public function get isInitialized():Boolean { return true; }
		
		override public function initialize(newBounds:Rectangle):DisplayObject {
			var bounds:Rectangle = newBounds.clone();
			if (this.globalMargin != null)
				this.globalMargin.applyInside(bounds);
			var container:DisplayObject = super.initialize(bounds);
			this._isInitialized = true;
			return container;
		}
		
		private var _isInitialized:Boolean = false;
		
		public function postInitialize():void {
			this.checkPostInitialize();
		}
		
		public function checkPostInitialize():void {
			if (!this._isInitialized) return;
			var view:DashboardChartElementView;
			
			for each (view in this.viewsList) {
				if (view.element != null && !view.element.isInitialized)
					return;
			}
			
			for each (view in this.viewsList) {
				if (view.element != null && view.element.isInitialized)
					view.element.postInitialize();
			}
		}
		
		override public function resize(newBounds:Rectangle):void {
			var bounds:Rectangle = newBounds.clone();
			if (this.globalMargin != null)
				this.globalMargin.applyInside(bounds);
			super.resize(bounds);
		}
		
		internal var views:Object;
		internal var viewsList:Array;
		
		public function getView(viewId:String):DashboardChartElementView {
			return this.views[viewId.toLowerCase()];
		} 
		
		public function getViewsCount():uint {
			return this.viewsList.length;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			var dTemplate:XML = Chart.getDefaultTemplate().defaults[0].dashboard[0].copy();
			var pTemplate:XML = Chart.getDefaultTemplate().defaults[0].panel[0].copy();
			var template:XML = XMLUtils.merge(pTemplate, dTemplate, "dashboard");
			data = this.mergeDashboardTemplate(template, data);
			super.deresializeWithoutTemplate(data, resources);
		}
		
		public function serialize(res:Object = null):Object {
			if (!res) res = {};
			res.views = [];
			for (var i:uint = 0;i<this.viewsList.length;i++) {
				res.views.push(DashboardChartElementView(this.viewsList[i]).serialize());
			}
			return res;
		}
	}
}