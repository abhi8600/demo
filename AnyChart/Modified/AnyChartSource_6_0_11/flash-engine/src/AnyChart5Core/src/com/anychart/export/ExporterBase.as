package com.anychart.export {
	import com.anychart.IAnyChart;
	import com.anychart.context.ContextMenuElement;
	import com.anychart.serialization.SerializerBase;
	
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	internal class ExporterBase extends ContextMenuElement {
		
		protected var url:String;
		private var fileName:String;
		protected var extension:String;
		
		private var useTitleAsFileName:Boolean;
		private var width:Number;
		private var height:Number;
		
		public function ExporterBase(chart:IAnyChart) {
			super(chart);
			this.setDefaults();
		}
		
		override public function setDefaults():void {
			super.setDefaults();
			this.fileName = "anychart";
			this.useTitleAsFileName = false;
			this.width = NaN;
			this.height = NaN;
		}
		
		public final function deserializeSettings(data:XML):void {
			if (data == null) return;
			if (data.@url != undefined) this.url = SerializerBase.getString(data.@url);
			if (data.@file_name != undefined) {
				this.fileName = SerializerBase.getString(data.@file_name);
				this.useTitleAsFileName = false;
			}else if (data.@use_title_as_file_name != undefined) {
				this.useTitleAsFileName = SerializerBase.getBoolean(data.@use_title_as_file_name);
			}
			if (data.@width != undefined) this.width = SerializerBase.getNumber(data.@width);
			if (data.@height != undefined) this.height = SerializerBase.getNumber(data.@height);
		}
		
		public final function getData(width:Number = NaN, height:Number = NaN):String {
			var savedWidth:Number = this.width;
			var savedHeight:Number = this.height;
				
			if (!isNaN(width)) this.width = width;
			if (!isNaN(height)) this.height = height;
			
			var needResize:Boolean = !isNaN(this.width) && !isNaN(this.height);
			var currentBounds:Rectangle = this.chart.getBounds().clone();
			if (needResize)
				this.chart.resize(new Rectangle(currentBounds.x, currentBounds.y, this.width, this.height));
			
			var res:String = this.getBase64Data();
			
			if (needResize)
				this.chart.resize(currentBounds);
			
			this.width = savedWidth;
			this.height = savedHeight;
			
			return res;
		}
		
		protected function getBase64Data():String { return null; }
		
		override public final function execute():void {
			var needResize:Boolean = !isNaN(this.width) && !isNaN(this.height);
			var currentBounds:Rectangle = this.chart.getBounds().clone();
			if (needResize)
				this.chart.resize(new Rectangle(currentBounds.x, currentBounds.y, this.width, this.height));
			
			this.executeSaving();
			
			if (needResize)
				this.chart.resize(currentBounds);
		}
		
		protected final function getFileName():String {
			var name:String = this.fileName;
			if (this.useTitleAsFileName)
				name = this.chart.getTitle() + "." + this.extension;
			if (name == null)
				name = "anychart."+this.extension;
			if (name.lastIndexOf("."+this.extension) != (name.length-4))
				name += "."+this.extension;
				
			return name;
		}
		
		protected function executeSaving():void { }
		
		protected final function removePreloader(e:Event):void {
			this.chart.hideTopPreloader();
		}
		
		protected final function showPreloader():void {
			this.chart.showPreloaderOnTheTop("Saving...");
		}
	}
}