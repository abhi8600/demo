package com.anychart.dashboard {
	import flash.geom.Rectangle;
	
	internal final class DashboardVBox extends DashboardElementContainer {
		
		public function DashboardVBox(dashboard:Dashboard) {
			super(dashboard);
		}
		
		override protected function setLayout(newBounds:Rectangle):void {
			super.setLayout(newBounds);
			var tmpY:Number = this.bounds.y;
			for (var i:uint = 0;i<this.children.length;i++) {
				var child:DashboardElement = this.children[i];
				child.calculatedY = tmpY;
				child.calculatedHeight = NaN;
				child.calculatedHeight = child.getHeight(newBounds);
				tmpY += child.calculatedHeight;
			}
		}
		
	}
}