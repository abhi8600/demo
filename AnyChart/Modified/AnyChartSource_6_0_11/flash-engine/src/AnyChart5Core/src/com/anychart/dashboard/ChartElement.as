package com.anychart.dashboard {
	import com.anychart.plot.PlotFactory;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.viewController.DashboardChartElementView;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	public final class ChartElement extends DashboardElement {
		public var view:DashboardChartElementView;
		public var name:String;
		
		private var xmlSource:String;
		
		public function ChartElement(dashboard:Dashboard) {
			super(dashboard);
			this.view = new DashboardChartElementView(dashboard.base, this);
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			super.initialize(bounds);
			
			if (this.xmlSource != null) {
				this.view.xmlDataRaw = PlotFactory.getChartXML(this.xmlSource).toXMLString();
			}
			this.xmlSource = null;
			this.view.container = this.container;
			if (this.margin != null) {
				this.view.container.x += this.margin.left;
				this.view.container.y += this.margin.top;
			}
			this.view.initialize(new Rectangle(0,0,this.bounds.width,this.bounds.height));
			return this.container;
		}
		
		override public function resize(bounds:Rectangle):void {
			super.resize(bounds);
			if (this.margin != null) {
				this.view.container.x += this.margin.left;
				this.view.container.y += this.margin.top;
			}
			this.view.resize(new Rectangle(0,0,this.bounds.width,this.bounds.height));
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			this.dashboard.viewsList.push(this.view);
			if (data.@name != undefined) {
				this.dashboard.views[SerializerBase.getLString(data.@name)] = this.view;
				this.name = name;
			}
			if (data.@source != undefined) {
				var source:String = SerializerBase.getString(data.@source);
				var isExternal:Boolean = data.@source_mode != undefined && (SerializerBase.getEnumItem(data.@source_mode) == "external" || SerializerBase.getEnumItem(data.@source_mode) == "externaldata");
				if (isExternal)
					this.view.xmlFilePath = source;
				else
					this.xmlSource = source;
			}
		}
	}
}