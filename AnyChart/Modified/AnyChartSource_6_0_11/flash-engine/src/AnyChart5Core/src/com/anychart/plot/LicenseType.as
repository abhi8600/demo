package com.anychart.plot {
	import com.anychart.Chart;
	
	import flash.utils.getDefinitionByName;
	
	internal final class LicenseType {
		private static const BUNDLE:LicenseType = new LicenseType(null);
		private static const CHARTS:LicenseType = new LicenseType("Charts");
		private static const MAPS:LicenseType = new LicenseType("Maps");
		
		private static var currentType:LicenseType = BUNDLE;
		
		private var code:String;
		
		public function LicenseType(code:String) {
			this.code = code;
		}
		
		private static function getLicenseType(plotClass:Class):LicenseType {
			try {
			if (plotClass == getDefinitionByName("com.anychart.mapPlot::MapPlot"))
				return LicenseType.MAPS;
			}catch (e:Error) {}
			return LicenseType.CHARTS;
		}
		
		internal static function checkPlotLicense(plotClass:Class, chart:Chart):void {
			var requiredType:LicenseType = getLicenseType(plotClass);
			if (currentType == LicenseType.BUNDLE || requiredType == currentType)
				return;
			LicenseInfoDrawer.create(chart, getMessage(requiredType));
		}
		
		private static function getMessage(requiredType:LicenseType):String {
			return "Your license doesn't include "+requiredType.code+"\n"
					+"To purchase them please visit:\n" 
					+"http://www.anychart.com/products/anychart/buy/";
		}
	}
}

import com.anychart.Chart;
import flash.geom.Rectangle;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

class LicenseInfoDrawer {
	public static function create(chart:Chart, message:String):void {
		var drawer:LicenseInfoDrawer = new LicenseInfoDrawer(chart.afterPlotContainer, message);
		chart.drawAfterCaller = drawer.draw;
		chart.resizeAfterCaller = drawer.resize;
	}
	
	private var messageTf:TextField;
	
	public function LicenseInfoDrawer(sprite:Sprite, message:String):void {
		
		this.messageTf = new TextField();
		this.messageTf.autoSize = TextFieldAutoSize.CENTER;
		
		var fmt:TextFormat = new TextFormat();
		fmt.url = "http://www.anychart.com/products/anychart/buy/";
		fmt.font = "Tahoma";
		this.messageTf.defaultTextFormat = fmt;
		
		this.messageTf.border = true;
		this.messageTf.borderColor = 0;
		this.messageTf.background = true;
		this.messageTf.backgroundColor = 0xFFFFFF;
		this.messageTf.text = message;
		
		sprite.addChild(this.messageTf);
	}
	
	private function draw(bounds:Rectangle):void {
		this.setCenter(bounds);
	}
	
	private function resize(bounds:Rectangle):void {
		this.setCenter(bounds);
	}
	
	private function setCenter(bounds:Rectangle):void {
		this.messageTf.x = bounds.x + (bounds.width - this.messageTf.width)/2;
		this.messageTf.y = bounds.y + (bounds.height - this.messageTf.height)/2;
	}
}