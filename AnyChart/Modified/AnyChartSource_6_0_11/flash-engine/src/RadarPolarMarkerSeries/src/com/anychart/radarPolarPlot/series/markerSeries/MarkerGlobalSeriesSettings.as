package com.anychart.radarPolarPlot.series.markerSeries {
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.data.SeriesType;
	import com.anychart.elements.marker.MarkerDrawer;
	import com.anychart.elements.marker.MarkerElementStyleState;
	import com.anychart.radarPolarPlot.elements.markerElement.RadarPolarMarkerElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BaseDynamicElementsContainer;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.styles.Style;
	import com.anychart.visual.color.ColorUtils;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class MarkerGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		}
		
		//-----------------------------------------------------------
		//			Elements
		//-----------------------------------------------------------
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (marker == null) 
				marker = new RadarPolarMarkerElement();
			if (data != null) 
				marker.deserializeGlobalAsSeries(data, styles, resources);
			else 
				marker.style = new Style(marker.getStyleStateClass());
			marker.enabled = true;
			this.settings.style = marker.style;
			return marker;
		}
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function getSettingsNodeName():String {
			return "marker_series";
		}
		
		override public function getStyleNodeName():String {
			return null;
		}
		
		override public function getStyleStateClass():Class {
			return null;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:MarkerSeries = new MarkerSeries();
			series.type = SeriesType.MARKER;
			return series; 
		}
		
		override public function hasIconDrawer(seriesType:uint):Boolean {
			return true;
		}
		
		override public function iconCanDrawMarker():Boolean { return false; }
		
		override public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {
			/*
			var type:uint = markerState.markerType;
			if (markerState.isDynamicType) {
				if (container.elementsInfo != null && container.elementsInfo[elementIndex] != null)
					type = container.elementsInfo[elementIndex].type;
				else if (this.type != -1)
					type = this.type;
				else 
					type = container.markerType;
			}
			*/
			var ptType:int = -1;
			if (item.tag is MarkerPoint) {
				
				var state:MarkerElementStyleState = MarkerElementStyleState(MarkerPoint(item.tag).marker.style.normal);
				if (state.isDynamicType) {
					var info:Object = MarkerPoint(item.tag).elementsInfo;
					if (info != null && info[BasePoint.MARKER_INDEX] != null)
						ptType = info[BasePoint.MARKER_INDEX].type;
				}else {
					ptType = state.markerType;
				}
			}else if (item.tag is MarkerSeries) {
				ptType = MarkerSeries(item.tag).marker.type;
			}
			var type:int = -1;
			if (ptType != -1)
				type = ptType;
			else 
				type = BaseDynamicElementsContainer(item.tag).markerType;
			
			var size:Number = Math.min(bounds.width,bounds.height);
			g.beginFill(item.color, 1);
			g.lineStyle(1, ColorUtils.getDarkColor(item.color), 1);
			MarkerDrawer.draw(g, type, size, bounds.x + (bounds.width - size)/2, bounds.y+(bounds.height-size)/2);
			g.endFill(); 
			g.lineStyle();
		}
	}
}