package com.anychart.radarPolarPlot.series.markerSeries {
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.radarPolarPlot.elements.markerElement.RadarPolarMarkerElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class MarkerPoint extends RadarPolarPlotPoint {
		
		private var pixValuePt:Point;
		
		override protected function execDrawing():void {
			this.pixValuePt = new Point();
			RadarPolarPlot(this.plot).transform(this.x, this.y, this.pixValuePt);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			pos.x = pixValuePt.x;
			pos.y = pixValuePt.y;
		}
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, elementIndex:int, resources:ResourcesLoader):MarkerElement {
			if (data.@style != undefined)
				if (data.marker[0] == null)
					data.marker[0] = <marker style={data.@style} />;
				else 
					data.marker[0].@style = data.@style;
				
			if (data.marker[0] != null) {
				if (marker.needCreateElementForPoint(data.marker[0]))
					marker = MarkerElement(marker.createCopy());
				marker.deserializePoint(data.marker[0], this, styles, elementIndex,resources);
				
				if (data.marker[0].@style != undefined) {
					this.settings = this.settings.createCopy(this.settings);
					this.settings.style = marker.style;
				}
			}
			marker.enabled = true;
			this.checkPointElementPosition(marker, elementIndex);
			return marker;
		}
		
		override protected function createElementsSprites():void {
			if (this.marker) RadarPolarMarkerElement(this.marker).setContainer(this.container);
			super.createElementsSprites();
		}
		
	}
}