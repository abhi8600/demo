package com.anychart.seriesPlot.thresholds {
	import com.anychart.seriesPlot.data.BasePoint;
	
	public interface IThresholdItemFormatable {
		function hasToken(token:String):Boolean;
		function getFormatedTokenValue(point:BasePoint, token:String):*;
		function isCustomAttribute(token:String):Boolean;
		function getCustomAttribute(token:String):*;
	}
}