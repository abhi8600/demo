package com.anychart.seriesPlot.thresholds {
	
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.seriesPlot.controls.legend.LegendAdaptiveItem;
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class CustomThreshold extends Threshold {
		private var conditions:Array;
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.conditions = [];
			var cnt:int = data.condition.length();
			for (var i:int = 0;i<cnt;i++) {
				var c:CustomThresholdCondition = CustomThresholdCondition.create(data.condition[i], this);
				if (c == null) continue;
				this.conditions.push(c);
			}
		}
		
		override public function checkBeforeDraw(point:BasePoint):void {
			for (var i:uint = 0;i<this.conditions.length;i++) {
				if (this.conditions[i].check(point)) break;
			}
		}
		
		override public function reset():void {
			super.reset();
			for each (var condition:CustomThresholdCondition in this.conditions)
				condition.reset();
		}
		
		override public function getData(container:ILegendItemsContainer):void {
			for (var i:uint = 0;i<this.conditions.length;i++) {
				var condition:CustomThresholdCondition = this.conditions[i];
				container.addItem(new LegendAdaptiveItem(condition.color, 0, -1, true, condition));
			}
		}
	}
}