package com.anychart.seriesPlot.thresholds {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class PointInformation {
		public var pointValue:Number;
		public var point:BasePoint;
	}
}