package com.anychart.seriesPlot {
	import com.anychart.animation.Animation;
	import com.anychart.animation.interpolation.InterpolationFactory;
	import com.anychart.animation.interpolation.Interpolator;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.controls.legend.IconSettings;
	import com.anychart.controls.legend.RectangleIconDrawer;
	import com.anychart.events.PointsEvent;
	import com.anychart.interactivity.Interactivity;
	import com.anychart.palettes.BasePalette;
	import com.anychart.palettes.ColorPalette;
	import com.anychart.palettes.HatchTypePalette;
	import com.anychart.palettes.MarkerPalette;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.controls.legend.LegendAdapter;
	import com.anychart.seriesPlot.controls.legend.LegendAdaptiveItem;
	import com.anychart.seriesPlot.controls.legend.SeriesIconSettings;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.CSVParserHandler;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.thresholds.IThreshold;
	import com.anychart.seriesPlot.thresholds.ThresholdsList;
	import com.anychart.seriesPlot.updateAnimation.PointAnimatedUpdater;
	import com.anychart.seriesPlot.utils.CSVParser;
	import com.anychart.utils.XMLUtils;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	public class SeriesPlot extends BasePlot {
		
		// Containers extend
		private var seriesGroupsSprites:Array;
		
		public function SeriesPlot() {
			this.selectedPoints = [];
			this.settings = {};
			this.series = [];
			this.allowMultipleSelect = false;
			
			this.seriesGroupsSprites = [];
			this.drawingPoints = [];
			this.onBeforeDrawSeries = [];
			
			this.interpolateMissings = false;	
			this.initLegendAdapter();
			
			super();
		}
		
		override public function destroy():void {
			super.destroy();
			if (this.legendAdapter)
				this.legendAdapter.destroy();
			this.legendAdapter = null;
			
			for each (var settings:GlobalSeriesSettings in this.settings) {
				if (settings)
					settings.destroy();
			} 
			
			for each (var series:BaseSeries in this.series) {
				if (series)
					series.destroy();
			}
			
			this.settings = null;
		}
		
		//---------------------------------------------------
		/*
			Сей блок кода используется из извращенной необходимости обхода серий и поиска точек в них
			в MapPlot, дабы включить точки undefined регионов
		*/
		protected var seriesIndex:int;
		public function resetSeriesIterator():void {
			this.seriesIndex = 0;
		}
		public function hasSeries():Boolean {
			return this.seriesIndex < this.series.length;
		}
		public function getNextSeries():BaseSeries {
			var res:BaseSeries = this.series[seriesIndex];
			this.seriesIndex++;
			return res;
		}
		/* конец ебанутого блока */	
		
		//-----------------------------------------------------
		//				SERIES
		//-----------------------------------------------------
		
		protected var defaultSeriesType:uint;
		public var series:Array;
		public var drawingPoints:Array;
		protected var onBeforeDrawSeries:Array;
		
		public function getSeriesType(type:*):uint {
			return defaultSeriesType;
		} 
		
		protected function drawSeries():void {
			var i:uint;
			for (i = 0;i<this.onBeforeDrawSeries.length;i++)
				this.onBeforeDrawSeries[i].onBeforeDraw();
			for (i = 0;i<this.drawingPoints.length;i++) {
				this.drawingPoints[i].checkThreshold();
				this.drawingPoints[i].draw();
			}
		}
		
		protected function resizeSeries():void {
			for (i = 0;i<this.onBeforeDrawSeries.length;i++)
				this.onBeforeDrawSeries[i].onBeforeResize();
			for (var i:uint = 0;i<this.drawingPoints.length;i++) 
				this.drawingPoints[i].resize();
		}
		
		//-----------------------------------------------------
		//				IPlot
		//-----------------------------------------------------
		
		override public final function createIconSettings():IconSettings {
			return new SeriesIconSettings(this);
		}
		
		override public function drawDataIcon(iconSettings:IconSettings, src:ILegendItem, g:Graphics, dx:Number, dy:Number):void {
			var settings:SeriesIconSettings = SeriesIconSettings(iconSettings);
			var source:LegendAdaptiveItem = LegendAdaptiveItem(src);
			
			if (source.color == -1)
				source.color = iconSettings.color;
			
 			if (settings.isSimpleBox) {
				RectangleIconDrawer.draw(g, dx, dy, iconSettings.width, iconSettings.height, source.color);
				return;
			}
			
			var seriesType:uint = (settings.isDynamicSeriesType) ? source.seriesType : settings.seriesType;
			//draw series icon 
			var seriesSettings:GlobalSeriesSettings = (this.settings[seriesType] == null) ? this.createGlobalSeriesSettings(seriesType) : this.settings[seriesType];  
			if (seriesSettings.hasIconDrawer(seriesType)) {
				var bounds:Rectangle = new Rectangle(dx,dy,iconSettings.width,iconSettings.height);
				//if (seriesSettings==SeriesType.LINE) 
				seriesSettings.drawIcon(seriesType, source, g, bounds);
			}else {
				RectangleIconDrawer.draw(g, dx, dy, iconSettings.width, iconSettings.height, source.color);
			}
		}
		
		protected var legendAdapter:LegendAdapter;
		protected function initLegendAdapter():void {
			this.legendAdapter = new LegendAdapter(this);
		}
		
		override public final function getLegendData(item:XML, container:ILegendItemsContainer):void {
			if (item.@source == undefined) {
				container.addItem(new LegendAdaptiveItem(-1,0,-1,true,null));
			}else {
				legendAdapter.getLegendDataBySource(SerializerBase.getEnumItem(item.@source), item, container);
			}
		}
		
		//-----------------------------------------------------
		//				Deserialization
		//-----------------------------------------------------
		
		override protected function onAfterDeserializeData(data:XML, resources:ResourcesLoader):void {}
		override protected function deserializeSeriesArguments(series:BaseSeries, seriesNode:XML):void {}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data != null) {
				
				this.selectionManager = new MouseSelectionManager(this);
				
				this.thresholdsList = new ThresholdsList(data.thresholds[0] == null ? null : data.thresholds[0].threshold);
				
				if (data.chart_settings[0] != null)
					this.deserializeChartSettings(data.chart_settings[0], data.styles[0], resources);
				
				if (data.data_plot_settings[0] != null)
					this.deserializeDataPlotSettings(data.data_plot_settings[0], resources);
					
				this.deserializeData(data, resources);
				this.onAfterDeserializeData(data, resources);
			}
		}
		
		public var interpolateMissings:Boolean;
		
		//<data_plot_settings />
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {
			if (data.@default_series_type != undefined)
				this.defaultSeriesType = this.getSeriesType(data.@default_series_type);
			if (data.@ignore_missing != undefined)
				this.interpolateMissings = !SerializerBase.getBoolean(data.@ignore_missing);
			if (data.interactivity[0] != null) {
			    if (data.interactivity[0].@allow_multiple_select != undefined)
			    	this.allowMultipleSelect = SerializerBase.getBoolean(data.interactivity[0].@allow_multiple_select);
				
				if (data.interactivity[0].select_rectangle[0] != null && this.allowMultipleSelect) {
					this.selectionManager.deserializeSettings(data.interactivity[0].select_rectangle[0]);
				}
		    }
		}
		
		protected function needCheckPointNames(series:BaseSeries):Boolean { return false; }
		
		//<chart />
		private var xmlStyles:XML;
		override protected function deserializeData(data:XML, resources:ResourcesLoader):void {
			
			super.deserializeData(data, resources);
			
			var xmlDataNode:XML = data.data[0];
			
			if (xmlDataNode == null)
				return;
			
			this.xmlStyles = data.styles[0];
			var xmlPalletes:XML = data.palettes[0];
			var xmlDataPlotSettings:XML = data.data_plot_settings[0];
			
			var seriesColorPalette:ColorPalette;
			var seriesMarkerPalette:BasePalette;
			var seriesHatchPalette:BasePalette;
			
			var dataColorPalette:ColorPalette = new ColorPalette();
			var dataHatchPalette:HatchTypePalette = new HatchTypePalette();
			var dataMarkerPalette:MarkerPalette = new MarkerPalette();
			
			var dataThreshold:IThreshold = null;
			if (xmlDataNode.@threshold != undefined)
				dataThreshold = thresholdsList.getThreshold(xmlPalletes, xmlDataNode.@threshold);
				
			this.enableAnimation = 
				this.chart.enableAnimation && this.animationManager != null && animationManager.enabled;
				
			this.getDataPalette(dataColorPalette, xmlDataNode, xmlPalletes, 'palette');
			this.getDataPalette(dataHatchPalette, xmlDataNode, xmlPalletes, 'hatch_palette');
			this.getDataPalette(dataMarkerPalette, xmlDataNode, xmlPalletes, 'marker_palette');
			
			var seriesCnt:uint = xmlDataNode.series.length();
			
			dataColorPalette = dataColorPalette ? dataColorPalette.checkAuto(seriesCnt) : null;
			
			var useCSVParser:Boolean = false;
			
			for (var i:uint = 0;i<seriesCnt;i++) {
				
				// получаем ноду и ее тип
				var xmlSeriesNode:XML = xmlDataNode.series[i];
				
				if (xmlSeriesNode.@visible != undefined && !SerializerBase.getBoolean(xmlSeriesNode.@visible))
					continue;
				
				var seriesType:uint = this.defaultSeriesType;
				if (xmlSeriesNode.@type != undefined) 
					seriesType = this.getSeriesType(xmlSeriesNode.@type);
					
				// получаем глобалСеттингс, создаем по нему серию и присваиваем индекс и имя
				var globalSettings:GlobalSeriesSettings = this.getSeriesSettings(seriesType, xmlDataPlotSettings, xmlStyles, resources, xmlDataNode);
				var series:BaseSeries = globalSettings.createSeries(seriesType);
				series.index = i;
				series.name = "Series "+i.toString();
				
				// анимируется ли серия
				var animateSeries:Boolean = globalSettings.seriesCanBeAnimated();
				
				// фигачим настройки серии, юзая локальные переменные из за цикла
				series.color = (xmlSeriesNode.@color == undefined) ? dataColorPalette.getItemAt(i) : 
																  SerializerBase.getColor(xmlSeriesNode.@color);
				series.hatchType = (xmlSeriesNode.@hatch_type == undefined) ? dataHatchPalette.getItemAt(i) :
																  SerializerBase.getHatchType(xmlSeriesNode.@hatch_type);																  
				series.markerType = dataMarkerPalette.getItemAt(i);
				
				// фигачим сеттинги серии
				if (globalSettings.settings.seriesContainsUniqueSettings(xmlSeriesNode)) {
					series.settings = globalSettings.settings.createCopy(series.settings);
					series.settings.deserialiseSeries(xmlSeriesNode, xmlStyles, resources);
				}else {
					series.settings = globalSettings.settings;
				}
								
				series.threshold = dataThreshold;
				series.deserializeThreshold(this.thresholdsList, xmlSeriesNode, xmlPalletes);
				
				globalSettings.setElements(series);
				
				xmlSeriesNode.point = this.beforeSeriesPointsDeserialization(series, xmlSeriesNode.point);
				
				// 
				var xmlPointsList:XMLList = xmlSeriesNode.point;
				
				//points creating
				var pointsCount:uint = xmlPointsList.length();
				series.points = new Array(pointsCount);
				
				// десериаизуем серию
				series.deserializeElements(xmlSeriesNode, xmlStyles, resources);
				series.deserialize(xmlSeriesNode);
				
				pointsCount = xmlPointsList.length();
				
				this.deserializeSeriesArguments(series, xmlSeriesNode);
				
				// анимация серии
				var seriesAnimation:Animation = this.getSeriesAnimation(globalSettings, xmlSeriesNode, xmlStyles);
				if (animateSeries && seriesAnimation) {
					this.addSeriesAnimation(seriesAnimation, series);
				}
				
				// инит стилей
				series.initializeStyle();
				series.initializeElementsStyle();
				
				// определение палитр серии
				seriesColorPalette = ColorPalette(this.getSeriesPalette(ColorPalette, dataColorPalette, globalSettings, xmlSeriesNode, xmlPalletes, 'palette'));
				seriesMarkerPalette = this.getSeriesPalette(MarkerPalette, dataMarkerPalette, globalSettings, xmlSeriesNode, xmlPalletes, 'marker_palette');
				seriesHatchPalette = this.getSeriesPalette(HatchTypePalette, dataHatchPalette, globalSettings, xmlSeriesNode, xmlPalletes, 'hatch_palette');											 
				
				seriesColorPalette = seriesColorPalette ? seriesColorPalette.checkAuto(pointsCount) : null;
				
				if (xmlSeriesNode.@data_source != undefined && SerializerBase.getEnumItem(xmlSeriesNode.@data_source) == "csv"){
					useCSVParser = true;
					if (xmlSeriesNode.csv_data_mapping[0] != null){
						var parser:CSVParserHandler = new CSVParserHandler(); 
						var str:String = parser.deserialize(xmlSeriesNode.csv_data_mapping[0]);
						if (str != ""){
							parser.init(series, seriesColorPalette, seriesMarkerPalette, seriesHatchPalette, seriesAnimation);
							this.addCSVParserHandler(str, parser);
						}
					}
				} else {
					
					var check:Boolean = this.needCheckPointNames(series);
					if (check)
						var tmpPoints:Object = {};
				
					var addPointsToDrawing:Boolean = series.addPointsToDrawing();
					
					// анимировать ли точки
					var animatePoints:Boolean = globalSettings.pointsCanBeAnimated();
					
					// проход по всем точкам
					for (var j:uint = 0;j<pointsCount;j++) {
						var pointNode:XML = xmlPointsList[j];
						var dataPoint:BasePoint = series.createPoint();
						dataPoint.index = j;
						dataPoint.series = series;
						
						dataPoint.name = j.toString();
						dataPoint.deserializeName(pointNode);
						if (check) { 
							if (tmpPoints[dataPoint.name] == true)
								continue;
							tmpPoints[dataPoint.name] = true; 
						}
						
						
						if (series.settings.pointContainsUniqueSettings(pointNode)) {
							dataPoint.settings = series.settings.createCopy(dataPoint.settings);
							dataPoint.settings.deserializePoint(pointNode, xmlStyles, resources);
						}else {
							dataPoint.settings = series.settings;
						}
						dataPoint.deserialize(pointNode);
						
						
						if (!globalSettings.addMissingPoints()) {
							if (dataPoint.isMissing) continue;
							//dataPoint.index = series.points.length;
						}
						
						if (this.interpolateMissings)
							globalSettings.missingCheckPointDuringDeserialize(dataPoint);
						
						series.setElements(dataPoint);
						dataPoint.deserializeElements(pointNode, xmlStyles, resources);
						
						if (pointNode.@color == undefined) {
							dataPoint.color = (seriesColorPalette == null) ? series.color : seriesColorPalette.getItemAt(dataPoint.paletteIndex);
						}else {
							dataPoint.color = SerializerBase.getColor(pointNode.@color);
						}
						
						if (pointNode.@hatch_type == undefined) {
							dataPoint.hatchType = (seriesHatchPalette == null) ? series.hatchType : seriesHatchPalette.getItemAt(j);
						}else {
							dataPoint.hatchType = SerializerBase.getHatchType(pointNode.@hatch_type);
						}
						
						dataPoint.markerType = (seriesMarkerPalette != null) ? seriesMarkerPalette.getItemAt(j) : series.markerType;
						
						dataPoint.threshold = series.threshold;
						dataPoint.deserializeThreshold(thresholdsList, pointNode, xmlPalletes);
						
						series.points[dataPoint.index]=dataPoint;
						
						dataPoint.createContainer();
						
						if (!dataPoint.isMissing) {
							dataPoint.initializeStyle();
							dataPoint.initializeElementsStyle();
							series.checkPoint(dataPoint);
						}
						
						if (addPointsToDrawing && (!(dataPoint.isMissing && !interpolateMissings))) {
							if (!globalSettings.needSortPoints)
								dataPoint.initialize();
							if (dataPoint.threshold != null)
								dataPoint.threshold.checkAfterDeserialize(dataPoint);
							this.drawingPoints.push(dataPoint);
						}
						
						if (animatePoints)
							this.setPointAnimation(seriesAnimation, pointNode, xmlStyles, dataPoint, pointsCount);
						if (this.enableAnimation)
							dataPoint.initializeElementsAnimation(this.animationManager, dataPoint.index, pointsCount);
					}
					
					this.seriesAfterPointDeserializationInit(series);
				} // end else (not from CSV)
				
				if (series.needCallingOnBeforeDraw)
					this.onBeforeDrawSeries.push(series);
				this.series.push(series);
			}
			
			if (useCSVParser)
				this.makeCSVParse(xmlDataNode);
			
			this.checkTresholdsAfterDeserialize();
		}
		
		protected function beforeSeriesPointsDeserialization(series:BaseSeries, points:XMLList):XMLList{
			return points;
		}
		
		protected function seriesAfterPointDeserializationInit(series:BaseSeries):void{
			// интерполяция пропущенных точек
			if (this.interpolateMissings) {
				series.global.missingInterpolate(series.points);
			}
			
			// сортировка точек
			if (series.global.needSortPoints) {
				series.global.sortPoints(series.points);
				for (var t:uint = 0;t<series.points.length;t++) {
					var dataPoint:BasePoint = series.points[t];
					dataPoint.index = t;
					dataPoint.initialize();
				}
			}

			series.checkPlot(this.cashedTokens);
		} 
		
		private function addCSVParserHandler(datasetName:String, handler:CSVParserHandler):void{
			if (this.csvParserHandlers[datasetName] == null)
				this.csvParserHandlers[datasetName] = new Array();
			this.csvParserHandlers[datasetName].push(handler);
		}
		
		private var csvParserHandlers:Object = {};
		private function makeCSVParse(xmlData:XML):void{
			//this.chart.getDataSets()
			/*
				<data_sets>
					<csv_data_set ... />
				</data_sets>
			*/
			var xmlCSVSets:XML = this.chart.getDataSets();//xmlData.csv_data_sets[0];
			if (xmlCSVSets == null)
				return;
			var parser:CSVParser = new CSVParser();
			var len:uint = xmlCSVSets.csv_data_set.length();
			for (var i:uint = 0; i < len; i++){
				var xmlCSVSet:XML = xmlCSVSets.csv_data_set[i];
				if (xmlCSVSet.@name == undefined)
					continue;
				var name:String = SerializerBase.getLString(xmlCSVSet.@name);
				var handlers:Array = this.csvParserHandlers[name];
				if (handlers == null)
					continue;
				var handlersCount:uint = handlers.length;
				var strCSVSet:String = SerializerBase.getCDATAString(xmlCSVSet);
				var rowsSeparator:String = this.getSeparator(xmlCSVSet.@rows_separator, "\n");
				var colsSeparator:String = this.getSeparator(xmlCSVSet.@cols_separator, ",");
				// обратная совместимость. columns_separator приоритетнее
				colsSeparator = this.getSeparator(xmlCSVSet.@columns_separator, colsSeparator);
				
				var ignoreTrailingSpaces:Boolean = xmlCSVSet.@ignore_trailing_spaces == undefined ? true : SerializerBase.getBoolean(xmlCSVSet.@ignore_trailing_spaces);
				parser.init(rowsSeparator, colsSeparator, strCSVSet, ignoreTrailingSpaces);
				
				var series:BaseSeries;
				var seriesArr:Array = new Array(handlersCount);
				var addPointsToDrawingArr:Array = new Array(handlersCount);
				var animatePointsArr:Array = new Array(handlersCount);
				var seriesColorPaletteArr:Array = new Array(handlersCount);
				var seriesMarkerPaletteArr:Array = new Array(handlersCount);
				var seriesHatchPaletteArr:Array = new Array(handlersCount);
				var seriesAnimationArr:Array = new Array(handlersCount);
				var k:uint;
				for (k = 0; k < handlersCount; k++){
					var handler:CSVParserHandler = CSVParserHandler(handlers[k]);
					series = handler.series;
					seriesArr[k] = series;
					addPointsToDrawingArr[k] = series.addPointsToDrawing();
					animatePointsArr[k] = series.global.pointsCanBeAnimated();
					seriesColorPaletteArr[k] = handler.seriesColorPalette;
					seriesMarkerPaletteArr[k] = handler.seriesMarkerPalette;
					seriesHatchPaletteArr[k] = handler.seriesHatchPalette;
					seriesAnimationArr[k] = handler.seriesAnimation;
				}
				
				var arr:Array;
				k = 0;
				while ((arr = parser.next()) != null){
					for (var j:uint = 0; j < handlersCount; j++){
						var pointNode:XML = CSVParserHandler(handlers[j]).parse(arr);
						series = seriesArr[j];
						
						var dataPoint:BasePoint = series.createPoint();
						dataPoint.index = k;
						dataPoint.series = series;
						dataPoint.name = k.toString();
						
						dataPoint.settings = series.settings;
						
						dataPoint.deserialize(pointNode);
						
						if (!series.global.addMissingPoints()) {
							if (dataPoint.isMissing) continue;
							//dataPoint.index = series.points.length;
						}
						
						if (this.interpolateMissings)
							series.global.missingCheckPointDuringDeserialize(dataPoint);
						
						series.setElements(dataPoint);
						
						dataPoint.color = (seriesColorPaletteArr[j] == null) ? series.color : seriesColorPaletteArr[j].getItemAt(dataPoint.paletteIndex);
						dataPoint.hatchType = (seriesHatchPaletteArr[j] == null) ? series.hatchType : seriesHatchPaletteArr[j].getItemAt(j);
						dataPoint.markerType = (seriesMarkerPaletteArr[j] == null) ? series.markerType : seriesMarkerPaletteArr[j].getItemAt(j);
						
						dataPoint.threshold = series.threshold;
						
						series.points[dataPoint.index]=dataPoint;
						
						dataPoint.createContainer();
						
						if (!dataPoint.isMissing) {
							dataPoint.initializeStyle();
							dataPoint.initializeElementsStyle();
							series.checkPoint(dataPoint);
						}
						
						if (addPointsToDrawingArr[j] && (!(dataPoint.isMissing && !interpolateMissings))) {
							if (!series.global.needSortPoints)
								dataPoint.initialize();
							if (dataPoint.threshold != null)
								dataPoint.threshold.checkAfterDeserialize(dataPoint);
							this.drawingPoints.push(dataPoint);
						}
						
						if (animatePointsArr[j])
							this.setPointAnimation(seriesAnimationArr[j], pointNode, xmlStyles, dataPoint, 1);
						if (this.enableAnimation)
							dataPoint.initializeElementsAnimation(this.animationManager, dataPoint.index, 1);
					}
					k++;
				}
				
				for (k = 0; k < handlersCount; k++)
					this.seriesAfterPointDeserializationInit(seriesArr[k]);
			}
		}
		
		protected function getSeparator(separator:*, def:String):String {
			var sep:String = separator != undefined ? SerializerBase.getString(separator) : def;
			var len:uint = sep.length;
			var res:String = "";
			var esc:Boolean = false;
			var i:int = 0;
			var j:int;
			var e:Boolean;
			var s:String;
			var c:String;					
			var tmp:String;
			while (i < len){
				tmp = sep.charAt(i);
				if (esc){
					switch(tmp){
						case "b": res += "\b"; break;
						case "f": res += "\f"; break;
						case "n": res += "\n"; break;
						case "r": res += "\r"; break;
						case "t": res += "\t"; break;
						case "u":
							if (i + 4 < len){
								s = sep.substr(i + 1, 4);
								e = false;
								for (j = 0; j < 4; j++){
									c = s.charAt(j);
									if (c != '0' && uint("0x"+c) == 0){
										e = true;
										break;
									}
								}
								if (e)
									res += tmp + s;
								else
									res += String.fromCharCode(uint("0x"+s));
							} else {
								res += tmp;
							}
							i+=4;
							break;
						case "x":
							if (i + 2 < len){
								s = sep.substr(i + 1, 2);
								e = false;
								for (j = 0; j < 2; j++){
									c = s.charAt(j);
									if (c != '0' && uint("0x"+c) == 0){
										e = true;
										break;
									}
								}
								if (e)
									res += tmp + s;
								else
									res += String.fromCharCode(uint("0x"+s));
							} else {
								res += tmp;
							} 
							i+=2;
							break;
						case "'": res += "'"; break;
						case "\"": res += "\""; break;
						case "\\": res += "\\"; break;
						default: res += tmp; break;
					}
					esc = false;
				} else {
					if (tmp == "\\")
						esc = true;
					else
						res += tmp;
				}
				i++;
			}
			return res;
		}
		
		protected function checkTresholdsAfterDeserialize():void {
			if (this.thresholdsList != null)
				this.thresholdsList.initialize();
		}
		
		//-----------------------------------------------------
		//				ANIMATION
		//-----------------------------------------------------
		
		private function getSeriesAnimation(settings:GlobalSeriesSettings, seriesNode:XML, styles:XML):Animation {
			if (this.chart.enableAnimation) {
				var seriesAnimation:Animation;
				if (settings.animation != null)
					seriesAnimation = settings.animation;
				
				if (SerializerBase.isEnabled(seriesNode.animation[0])) {
					seriesAnimation = (!seriesAnimation) ? new Animation() : seriesAnimation.createCopy();
					seriesAnimation.deserialize(seriesNode.animation[0], styles);
				}
				
				return seriesAnimation;
			}
			return null;
		}
		
		private function setPointAnimation(seriesAnimation:Animation, pointNode:XML, styles:XML, dataPoint:BasePoint, pointsCount:uint):void {
			if (!this.chart.enableAnimation)
				return;
				
			var pointAnimation:Animation;
			
			if (seriesAnimation)
				pointAnimation = seriesAnimation.getAnimation(dataPoint.index, pointsCount);

			if (SerializerBase.isEnabled(pointNode.animation[0])) {
				if (pointAnimation == null)
					pointAnimation = new Animation();
				else if (pointAnimation == seriesAnimation)
					pointAnimation = pointAnimation.createCopy();
				
				pointAnimation.deserialize(pointNode.animation[0], styles);
			}
			if (pointAnimation) 
				this.addPointAnimation(pointAnimation, dataPoint);
		}
		
		protected function addPointAnimation(animation:Animation, dataPoint:BasePoint):void {
			this.animationManager.registerDisplayObject(dataPoint.container, this.scrollableContainer, animation);
		}
		
		protected function addSeriesAnimation(animation:Animation, series:BaseSeries):void {
			series.setAnimation(animation, this.animationManager);
		}
		
		//-----------------------------------------------------
		//				PALETTES
		//-----------------------------------------------------
		
		private function getDataPalette(palette:BasePalette, data:XML, palettes:XML, paletteType:String):void {
			var paletteName:String = "default";
			var paletteAttributeName:String = '@'+paletteType;
			if (data[paletteAttributeName] != undefined) {
				var dataPaletteName:String = String(data[paletteAttributeName]).toLowerCase();
				if (palettes[paletteType].(@name == dataPaletteName).length() > 0)
					paletteName = dataPaletteName;
			}
			
			palette.deserialize(palettes[paletteType].(@name == paletteName)[0]);
		}
		protected function getSeriesPalette(paletteClass:Class, dataPalette:BasePalette, globalSettings:GlobalSeriesSettings, seriesNode:XML, palettes:XML, paletteType:String):BasePalette {
			var applyDataPaletteToSeries:Boolean = globalSettings.applyDataPaletteToSeries;
			var palette:BasePalette;
			var paletteAttrName:String = '@'+paletteType;
			if (seriesNode[paletteAttrName] != undefined) {
				var seriesPaletteName:String = String(seriesNode[paletteAttrName]).toLowerCase();
				if (palettes[paletteType].(@name == seriesPaletteName).length() > 0) {
					palette = new paletteClass();
					palette.deserialize(palettes[paletteType].(@name == seriesPaletteName)[0]);
				}
			}else {
				palette = applyDataPaletteToSeries ? dataPalette : null;
			}
			return palette;
		}	
		
		//-----------------------------------------------------
		//				SERIES SETTINGS
		//-----------------------------------------------------
		
		protected var settings:Object;
		
		protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			return null;
		}
		
		protected function getSeriesSettings(seriesType:uint, dataPlotSettings:XML, styles:XML, resources:ResourcesLoader, dataNode:XML):GlobalSeriesSettings {
			if (this.settings[seriesType] != null)
				return this.settings[seriesType];
			
			var globalSettings:GlobalSeriesSettings = this.createGlobalSeriesSettings(seriesType);

			this.processSettings(globalSettings, dataPlotSettings, styles, resources, dataNode);
			
			this.registerSettings(seriesType, globalSettings);
			return globalSettings;
		}
		
		protected function registerSettings(type:uint, settings:GlobalSeriesSettings):void {
			this.settings[type] = settings;
		}
		
		protected function processSettings(globalSettings:GlobalSeriesSettings, dataPlotSettings:XML, styles:XML, resources:ResourcesLoader, dataNode:XML):void {
			globalSettings.plot = this;
			globalSettings.chartBase = this.chart;
			globalSettings.settings = globalSettings.createSettings();
			globalSettings.settings.global = globalSettings;
			globalSettings.settings.deserializeData(dataNode);
			globalSettings.deserializeGlobal(dataPlotSettings);
			
			globalSettings.settings.interactivity = new Interactivity();
			globalSettings.settings.interactivity.deserialize(dataPlotSettings.interactivity[0]);
			
			var settingsNodeName:String = globalSettings.getSettingsNodeName();
			var settingsNode:XML;
			if (dataPlotSettings != null && dataPlotSettings[settingsNodeName][0] != null) {
				settingsNode = dataPlotSettings[settingsNodeName][0];
				globalSettings.deserialize(settingsNode);
				globalSettings.deserializeElements(settingsNode, styles,resources);
				globalSettings.settings.deserializeGlobal(settingsNode, styles, resources);
			}
			if (this.enableAnimation)
				globalSettings.deserializeAnimation(dataPlotSettings, settingsNode, styles);
			
			globalSettings.initialize(this);
			
			globalSettings.setPlotSettings(this);
			
			if (this.interpolateMissings)
				globalSettings.missingInitializeInterpolators();
			
			this.seriesGroupsSprites.push(globalSettings.container.graphics);
		}
		
		override public function getTokenValue(token:String):* {
			if (token == '%DataPlotSeriesCount') return this.series.length;
			return super.getTokenValue(token);
		}
		
		//-----------------------------------------------------
		//				IResizable
		//-----------------------------------------------------
		
		protected var baseBounds:Rectangle;
		
		override public function draw():void {
			super.draw();
			this.drawSeries();
		}
		
		override public function execResize():void {
			super.execResize();
			
			for each (var d:Graphics in this.seriesGroupsSprites)
				d.clear();
			
			this.resizeSeries();
		}
		
		//-----------------------------------------------------
		//				Animation
		//-----------------------------------------------------
		
		public function hasPoint(seriesId:String, pointId:String):Boolean {
			return this.getPointById(seriesId, pointId) != null;
		}
		
		public function getPointById(seriesId:String, pointId:String):BasePoint {
			var series:BaseSeries;
			var i:int;
			for (i = 0;i<this.series.length;i++) {
				if (BaseSeries(this.series[i]).id == seriesId) {
					series = this.series[i];
					break;
				}
			}
			
			if (series == null) return null;
			
			var point:BasePoint;
			for (i = 0;i<series.points.length;i++) {
				if (BasePoint(series.points[i]).id == pointId) {
					point = series.points[i];
					break;
				}
			}
			
			return point;
		}
		
		protected function onAfterThresholdsReset():void {}
		
		public function recalculateAndRedraw():void {
			
			this.resetTokens();
			
			if (this.thresholdsList)
				this.thresholdsList.reset();
			
			this.onAfterThresholdsReset();
			
			for (var i:uint = 0;i<this.series.length;i++) {
				
				var series:BaseSeries = this.series[i];
				series.resetTokens();
				
				var pointsCount:int = series.points.length;
				
				for (var j:uint = 0;j<pointsCount;j++) {
					var dataPoint:BasePoint = BasePoint(series.points[j]);
					if (dataPoint != null) {
						dataPoint.reset();
						dataPoint.resetTokens();
						
						if (dataPoint.threshold != null) dataPoint.threshold.checkAfterDeserialize(dataPoint);
						
						series.checkPoint(dataPoint);
						series.checkPlot(this.cashedTokens);
					}
				}
			}
			
			if (this.thresholdsList != null)
				this.thresholdsList.initialize();
				
			this.onRecalculateFinished();
			
			for (i = 0;i<this.onBeforeDrawSeries.length;i++)
				this.onBeforeDrawSeries[i].onBeforeDraw();
			for (i = 0;i<this.drawingPoints.length;i++) {
				this.drawingPoints[i].checkThreshold();
			}
			
			if (this.controls) this.controls.recalculateAndRedraw();
			
			this.calculateResize(this.baseBounds.clone());
			this.execResize();
		}
		
		protected function onRecalculateFinished():void {}
		
		private var pointAnimatedUpdater_:PointAnimatedUpdater;
		
		override public function startPointsUpdate():void {
			if (this.pointAnimatedUpdater_) {
				this.pointAnimatedUpdater_.stop();
				this.pointAnimatedUpdater_.start();
			}
		}
		
		override public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void {
			if (this.pointAnimatedUpdater_ == null)
				this.pointAnimatedUpdater_ = new PointAnimatedUpdater(this);
			
			this.pointAnimatedUpdater_.updatePointValue(seriesId, pointId, newValues, settings);
		}
		
		
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
						
			res = super.serialize(res);
			
			res.SeriesCount = this.series.length;
			
			res.Series = new Array();
			var ptsCount:uint = 0;
			for (var i:uint = 0;i<this.series.length;i++) {
				res.Series.push(BaseSeries(this.series[i]).serialize());
				ptsCount += this.series[i].points.length;
			}
			res.PointCount = ptsCount;
			
			return res;
		}
		
		//---------------------------------------------------------------
		//				Value Change
		//---------------------------------------------------------------
		
		override public function addSeries(...seriesData):void {
			var seriesCnt:uint = seriesData.length;
			if (this.cashedData.data.length() == 0)
				this.cashedData.data += <data />;
				
			for (var i:uint = 0;i<seriesCnt;i++)
				this.cashedData.data[0].series += new XML(seriesData[i]);
		}
		
		override public function addSeriesAt(index:uint, seriesData:String):void {
			this.cashedData.data[0].series = XMLUtils.insert(this.cashedData.data[0].series, index, new XML(seriesData));
		}
		
		override public function removeSeries(seriesId:String):void {
			XMLUtils.removeById(this.cashedData.data[0].series, seriesId);
		}
		
		override public function updateSeries(seriesId:String, seriesData:String):void {
			var seriesNodeIndex:int = XMLUtils.findById(this.cashedData.data[0].series, seriesId);
			var seriesList:XMLList = this.cashedData.data[0].series;
			if (seriesNodeIndex != -1) {
				seriesList[seriesNodeIndex] = XMLUtils.mergeDataNode(seriesList[seriesNodeIndex], new XML(seriesData));
			}
		}
		
		override public function showSeries(seriesId:String, isVisible:Boolean):void {
			var seriesNode:XML =  XMLUtils.findXMLById(this.cashedData.data[0].series, seriesId);
			if (seriesNode != null)
				seriesNode.@visible = isVisible.toString();
		}
		
		override public function addPoint(seriesId:String, ...pointXML):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0].series, seriesId);
			if (seriesNode != null) {
				var pointsCnt:uint = pointXML.length;
				for (var i:uint = 0;i<pointsCnt;i++)
					seriesNode.point += new XML(pointXML[i]);
			}
		}
		
		override public function addPointAt(seriesId:String, pointIndex:uint, pointData:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0].series, seriesId);
			if (seriesNode != null) {
				seriesNode.point = XMLUtils.insert(seriesNode.point, pointIndex, new XML(pointData));
			}
		}
		
		override public function removePoint(seriesId:String, pointId:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0].series, seriesId);
			if (seriesNode != null)
				XMLUtils.removeById(seriesNode.point, pointId);
		}
		
		override public function updatePoint(seriesId:String, pointId:String, pointXML:String):void {
			var seriesNode:XML = XMLUtils.findXMLById(this.cashedData.data[0].series, seriesId);
			if (seriesNode != null) {
				var pointIndex:int = XMLUtils.findById(seriesNode.point, pointId);
				if (pointIndex != -1)
					seriesNode.point[pointIndex] = XMLUtils.mergeDataNode(seriesNode.point[pointIndex], new XML(pointXML));
			}
		}
		
		override public function clear():void {
			this.cashedData.data[0].series = new XMLList();
		}
		
		override public function refresh():void {
			this.view.setXMLData(this.cashedData.copy());
		}
		
		override public function highlightSeries(seriesId:String, highlighted:Boolean):void {
			for each (var series:BaseSeries in this.series) {
				if (series.id != null && series.id == seriesId) {
					if (highlighted)
						series.setHover();
					else
						series.setNormal();
					return;
				}
			}
		}
		
		override public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {
			for each (var series:BaseSeries in this.series) {
				if (series.id != null && series.id == seriesId) {
					for each (var point:BasePoint in series.points) {
						if (point.id != null && point.id == pointId) {
							if (highlighted)
								point.setHover();
							else
								point.setNormal();
							return;
						}
					}
				}
			}
		}
		
		override public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {
			for each (var series:BaseSeries in this.series) {
				if (series.id != null && series.id == seriesId) {
					for each (var point:BasePoint in series.points) {
						if (point.id != null && point.id == pointId) {
							point.select(selected);
							return;
						}
					}
				}
			}
		}
		
			    
	    override public function showAxisMarker(markerId:String, isVisible:Boolean):void {
			var seriesNode:XML=null;
			var xmlL:XMLList = this.cashedData..axis_markers;
			if (xmlL.ranges !=undefined) seriesNode =  XMLUtils.findXMLById(xmlL..range, markerId);
			if (xmlL.lines != undefined && seriesNode == null) seriesNode =  XMLUtils.findXMLById(xmlL..line, markerId);
			if (seriesNode != null)
				seriesNode.@visible = isVisible.toString();
				//seriesNode.label.format=isVisible.toString();
				
		}
		
		//---------------------------------------------------------------
		//				Selection
		//---------------------------------------------------------------
		
		override public function selectPointsInArea(area:Rectangle):void {
			
		}
		
		override public function deselectSelectedPoints():void {
			if (lastSelectedPoints != null && lastSelectedPoints.length > 0) {
				for each (var point:BasePoint in lastSelectedPoints) {
					point.setSelected(false);
				}
				
				this.chart.dispatchEvent(new PointsEvent(PointsEvent.MULTIPLE_POINTS_DESELECT, lastSelectedPoints));
			}
			lastSelectedPoints = null;
		}
		
		protected var lastSelectedPoints:Array;
		
		override public function finalizePointsSelection(area:Rectangle):void {
			
			if (Math.abs(area.width) < 1 && Math.abs(area.height) < 1) {
				return;
			}
			
			var pts:Array = [];
			for each (var series:BaseSeries in this.series) {
				for each (var point:BasePoint in series.points) {
					if (point.hitTestSelection(area, this.drawingContainer, this.selectionManager)) {
						point.setSelected(true);
						pts.push(point);
					}
				}
			}
			lastSelectedPoints = pts;
			this.chart.dispatchEvent(new PointsEvent(PointsEvent.MULTIPLE_POINTS_SELECT, pts));
		}
	}
}