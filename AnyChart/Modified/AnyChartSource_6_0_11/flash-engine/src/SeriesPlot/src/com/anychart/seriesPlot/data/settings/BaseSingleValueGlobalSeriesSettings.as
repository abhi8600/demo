package com.anychart.seriesPlot.data.settings {
	import com.anychart.interpolation.IMissingInterpolator;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class BaseSingleValueGlobalSeriesSettings extends GlobalSeriesSettings {
		protected var yInterpolator:IMissingInterpolator;
		
		protected function getPointValueFieldName():String { return "y"; }
		
		override public function missingInitializeInterpolators():void {
			this.yInterpolator = this.createMissingInterpolator();
			this.yInterpolator.initialize(this.getPointValueFieldName());
		}
		
		override public function missingCheckPointDuringDeserialize(point:BasePoint):void {
			this.yInterpolator.checkDuringDeserialize(point, point.index);
		}
		
		override public function missingInterpolate(points:Array):void {
			var missingPoints:Array = this.yInterpolator.interpolate(points);
			for each (var pt:BasePoint in missingPoints) {
				this.missingInitializeAfterInterpolate(pt);
			}
				
			this.yInterpolator.clear();
		}
	}
}