package com.anychart.seriesPlot.data.settings {
	import com.anychart.IAnyChart;
	import com.anychart.animation.Animation;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.elements.AnimatableElement;
	import com.anychart.interpolation.IMissingInterpolator;
	import com.anychart.interpolation.SimpleMissingInterpolator;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseDataElement;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.drawing.DrawingMode;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.XMLUtils;
	import com.anychart.visual.effects.EffectsList;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class GlobalSeriesSettings extends BaseDataElement implements ISerializable {
		
		public var chartBase:IAnyChart;
		
		public var groupPadding:Number = .1;
		public var pointPadding:Number = .1;
		
		public var applyDataPaletteToSeries:Boolean = false;
		
		public var isPercentPointScetterWidth:Boolean = true;
		public var pointScetterWidth:Number = .05;
		
		public function destroy():void {
			this._plot = null;
			this.chartBase = null;
		}
		
		//-------------------------------------------------------
		// 					Base settings
		//-------------------------------------------------------
		
		public function createSettings():BaseSeriesSettings {
			 return new BaseSeriesSettings(); 
		}
		
		//-------------------------------------------------------
		// 					Drawing container
		//-------------------------------------------------------
		
		private var _plot:SeriesPlot;
		override public function get plot():SeriesPlot { return this._plot; }
		public function set plot(value:SeriesPlot):void { this._plot = value; }
		
		override public function get global():GlobalSeriesSettings { return this; }
		
		public var container:Sprite;
		public var drawingMode:uint = DrawingMode.DRAW_TO_PERSONAL_CONTAINER;
		public var effects:EffectsList;
		
		public function initialize(plot:SeriesPlot):void {
			this.plot = plot;
			this.container = new Sprite();
			plot.dataContainer.addChild(this.container);
			if (this.effects != null && this.effects.enabled) this.container.filters = this.effects.list;
		}
		
		//-------------------------------------------------------
		// 					Animation
		//-------------------------------------------------------
		
		public var animation:Animation;
		
		//-------------------------------------------------------
		// 					Deserialization
		//-------------------------------------------------------
		
		//<data_plot_settings />
		public function deserializeGlobal(data:XML):void {
			if (data.@apply_palettes_to != undefined)
				this.applyDataPaletteToSeries = SerializerBase.getEnumItem(data.@apply_palettes_to) == "points";
			if (data.@drawing_mode != undefined) {
				switch (SerializerBase.getEnumItem(data.@drawing_mode)) {
					case 'fast': 
						this.drawingMode = DrawingMode.DRAW_TO_SERIES_GROUP_CONTAINER;
						break;
					case 'normal':
						this.drawingMode = DrawingMode.DRAW_TO_PERSONAL_CONTAINER;
						break;
				}
			}
			if (data.@group_padding != undefined) this.groupPadding = SerializerBase.getNumber(data.@group_padding);
			if (data.@point_padding != undefined) this.pointPadding = SerializerBase.getNumber(data.@point_padding);
		}
		
		public function deserializeAnimation(globalData:XML, data:XML, styles:XML):void {
			if (SerializerBase.isEnabled(globalData.animation[0]) || (data != null && SerializerBase.isEnabled(data.animation[0]))) {
				var animationData:XML = XMLUtils.merge(globalData.animation[0], data != null ? data.animation[0] : null, "animation");
				if (SerializerBase.isEnabled(animationData)) {
					this.animation = new Animation();
					this.animation.deserialize(animationData, styles);
				}
			}
		}
		
		//<data_plot_settings><*_series />
		override public function deserialize(data:XML):void {
			this.deserializeGlobal(data);
			if (data.effects[0] != undefined) {
				if (this.effects == null) 
					this.effects = new EffectsList();
				this.effects.deserialize(data.effects[0]);
			}
			if (this.canSortPoints || this.canPreSortPoints) {
				if (data.@sort != undefined) {
					switch (SerializerBase.getEnumItem(data.@sort)) {
						case "asc": this.sort = true; this.sortDescending = false; break;
						case "desc": this.sort = true; this.sortDescending = true; break;
						default:
						case "none": this.sort = false; break; 
					}
				}
			}
			if (data.@scatter_point_width != undefined){
				this.isPercentPointScetterWidth = SerializerBase.isPercent(data.@scatter_point_width);
				this.pointScetterWidth = this.isPercentPointScetterWidth ? 
					(SerializerBase.getPercent(data.@scatter_point_width)):(SerializerBase.getNumber(data.@scatter_point_width));
			}
		}
		
		override public function deserializeElements(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.marker = this.deserializeMarkerElement(data, stylesList, this.marker, resources);
			this.label = this.deserializeLabelElement(data, stylesList, this.label, resources);
			this.tooltip = this.deserializeTooltipElement(data, stylesList, this.tooltip, resources);
			this.deserializeExtraLabels(data, stylesList, resources);
			this.deserializeExtraMarkers(data, stylesList, resources);
			this.deserializeExtraTooltips(data, stylesList, resources);
		}
		
		private function deserializeExtraLabels(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_labels[0] != null) {
				this.extraLabels = [];
				var extraLabelsList:XMLList = data.extra_labels[0].label;
				var extraLabelsCnt:uint = extraLabelsList.length();
				for (var i:uint = 0;i<extraLabelsCnt;i++) {
					this.extraLabels.push(this.deserializeExtraLabel(extraLabelsList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraLabel(data:XML, styles:XML, resources:ResourcesLoader):LabelElement {
			var label:LabelElement = this.createLabelElement(true);
			this.checkElementAnimation(label, data, styles);			
			if (data != null) 
				label.deserializeGlobal(data, styles, resources);
			return label;
		}
		
		private function deserializeExtraMarkers(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_markers[0] != null) {
				this.extraMarkers = [];
				var extraMarkersList:XMLList = data.extra_markers[0].marker;
				var extraMarkersCnt:uint = extraMarkersList.length();
				for (var i:uint = 0;i<extraMarkersCnt;i++) {
					this.extraMarkers.push(this.deserializeExtraMarker(extraMarkersList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraMarker(data:XML, styles:XML, resources:ResourcesLoader):MarkerElement {
			var marker:MarkerElement = this.createMarkerElement();
			this.checkElementAnimation(marker, data, styles);			
			if (data != null) 
				marker.deserializeGlobal(data, styles, resources);
			return marker;
		}
		
		private function deserializeExtraTooltips(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_tooltips[0] != null) {
				this.extraTooltips = [];
				var extraTooltipsList:XMLList = data.extra_tooltips[0].tooltip;
				var extraTooltipsCnt:uint = extraTooltipsList.length();
				for (var i:uint = 0;i<extraTooltipsCnt;i++) {
					this.extraTooltips.push(this.deserializeExtraTooltip(extraTooltipsList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraTooltip(data:XML, styles:XML, resources:ResourcesLoader):TooltipElement {
			var tooltip:TooltipElement = new TooltipElement();
			if (data != null) 
				tooltip.deserializeGlobal(data, styles, resources);
			return tooltip;
		}
		
		public function createMarkerElement():MarkerElement
		{
			return new MarkerElement();
		}
		
		protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (marker == null) { 
				marker = createMarkerElement();
			}
			this.checkElementAnimation(marker, data != null ? data.marker_settings[0] : null, styles);
			
			if (data != null && data.marker_settings[0] != null) 
				marker.deserializeGlobal(data.marker_settings[0], styles, resources);
			else 
				marker.style = new Style(marker.getStyleStateClass());
			return marker;
		}
		
		protected final function deserializeLabelElement(data:XML, styles:XML, label:LabelElement, resources:ResourcesLoader):LabelElement {
			if (label == null) {
				label = this.createLabelElement();
			}
			this.checkElementAnimation(label, data != null ? data.label_settings[0] : null, styles);
			
			if (data != null && data.label_settings[0] != null) 
				label.deserializeGlobal(data.label_settings[0], styles, resources);
			else 
				label.style = new Style(label.getStyleStateClass());
			return label;
		}
		
		public function createLabelElement(isExtra:Boolean = false):LabelElement {
			return new LabelElement();
		}
		
		protected final function checkElementAnimation(element:AnimatableElement, data:XML, styles:XML):void {
			element.enableAnimation = this.plot.enableAnimation;
			if (data != null) {
				if (data.animation[0] != null && !SerializerBase.isEnabled(data.animation[0]))
					element.enableAnimation = false;
				else
					element.deserializeAnimation(data, styles);
			}
		}
		
		protected final function deserializeTooltipElement(data:XML, styles:XML, tooltip:TooltipElement, resources:ResourcesLoader):TooltipElement {
			if (tooltip == null) 
				tooltip = new TooltipElement();
			
			if (data != null && data.tooltip_settings[0] != null) 
				tooltip.deserializeGlobal(data.tooltip_settings[0], styles, resources);
			else 
				tooltip.style = new Style(tooltip.getStyleStateClass());
			return tooltip;
		}
		
		//-------------------------------------------------------
		// 					SORTING
		//-------------------------------------------------------
		
		protected function get canSortPoints():Boolean { return false; }
		protected function get canPreSortPoints():Boolean { return false; }
		protected var sort:Boolean = false;
		protected var sortDescending:Boolean = false;
		
		public final function get needSortPoints():Boolean {
			return this.canSortPoints && this.sort;
		}
		
		public final function sortPoints(points:Array):void {
			if (!this.canPreSortPoints && this.canSortPoints && this.sort)
				this.execSorting(points, this.sortDescending);
		}
		
		public final function sortXMLPoints(points:XMLList):XMLList {
			if (this.canPreSortPoints && this.sort)
				return this.execXMLSorting(points, this.sortDescending);
			return points;
		}
		
		protected function execSorting(points:Array, sortDescending:Boolean):void {}
		protected function execXMLSorting(points:XMLList, sortDescending:Boolean):XMLList { return points; }
		
		//-------------------------------------------------------
		// 					MISSING
		//-------------------------------------------------------
		
		protected function createMissingInterpolator():IMissingInterpolator {
			return new SimpleMissingInterpolator();
		}
		
		protected function missingInitializeAfterInterpolate(point:BasePoint):void {
			point.initializeAfterInterpolate();
		}
		
		public function missingInitializeInterpolators():void {}
		public function missingCheckPointDuringDeserialize(point:BasePoint):void {}
		//should call series.checkPoint(dataPoint) for each missing point
		public function missingInterpolate(points:Array):void {}
		
		//-------------------------------------------------------
		// 					ABSTRACT METHODS
		//-------------------------------------------------------
		
		public function hasIconDrawer(seriesType:uint):Boolean { return false; }
		public function drawIcon(seriesType:uint, item:ILegendItem, g:Graphics, bounds:Rectangle):void {}
		public function iconCanDrawMarker():Boolean { return true; }
		public function createSeries(seriesType:uint):BaseSeries { return null; }
		public function getSettingsNodeName():String { return null; }
		public function getStyleNodeName():String { return null; }
		public function getStyleStateClass():Class { return null; }
		public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle { return null; }
		public function isProgrammaticStyle(styleNode:XML):Boolean { return false; }
		public function addMissingPoints():Boolean { return true; }
		public function initializeIfSortedOVerlay():Boolean { return true; }
		public function checkStyle(style:Style):void {}
		public function pointsCanBeAnimated():Boolean { return true; }
		public function seriesCanBeAnimated():Boolean { return false; }
		public function setPlotSettings(plot:SeriesPlot):void {}
		
		public function reset():void {}
	}
}