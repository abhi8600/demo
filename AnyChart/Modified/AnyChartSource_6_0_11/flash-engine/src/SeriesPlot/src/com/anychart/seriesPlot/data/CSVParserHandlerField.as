package com.anychart.seriesPlot.data {
	
	internal final class CSVParserHandlerField {
		
		public var isCustom:Boolean;
		public var name:String;
		
		public function CSVParserHandlerField(name:String, custom:Boolean){
			this.isCustom = custom;
			this.name = name;					
		}
	}
}