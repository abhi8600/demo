package com.anychart.seriesPlot.controls.colorMap {
	import com.anychart.controls.ControlWithTitle;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.thresholds.AutomaticThreshold;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class ColorMapControl extends ControlWithTitle {
		protected var rangeItemWidth:Number;
		protected var rangeItemHeight:Number;
		protected var settedRangeItemWidth:Number;
		protected var settedRangeItemHeight:Number;
		private var rangeItemBackground:Style;
		
		protected var labels:ColorMapLabels; 
		protected var tickmark:Stroke;
		internal var tickmarkOnRangeCenter:Boolean;
		
		protected var tickmarksSize:Number;
		protected var normalTickmarksSpace:Number;
		protected var tickmarksSpace:Number;
		
		internal var inverted:Boolean;
		internal var threshold:AutomaticThreshold;
		
		protected var normalLabelsSpace:Number;
		protected var labelsSpace:Number;
		
		protected var isRangeItemWidthSetted:Boolean;
		protected var isRangeItemHeightSetted:Boolean;
		
		protected var defaultRangeItemWidth:Number;
		protected var defaultRangeItemHeight:Number;
		
		internal var labelsPosition:uint;
		
		private var interval:uint;
		
		/**
		 * Сдвиги в случае, когда размер color swatch, указаный в XML больше чем надо
		 * (Центрование)
		 */
		private var leftShift:Number;
		private var topShift:Number;
		
		public function ColorMapControl() {
			super();
			this.tickmarkOnRangeCenter = false;
			this.inverted = false;
			this.tickmarksSize = 5;
			this.normalTickmarksSpace = 0;
			this.tickmarksSpace = 0;
			this.normalLabelsSpace = 0;
			this.labelsSpace = 0;
			this.labelsPosition = ColorMapLabelsPosition.COMBINED;
			this.isRangeItemWidthSetted = false;
			this.isRangeItemHeightSetted = false;
			this.interval = 1;
		}
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			if (data.range_item[0] != null) {
				var item:XML = data.range_item[0];
				if (item.@width != undefined) {
					this.settedRangeItemWidth = SerializerBase.getNumber(item.@width);
					this.isRangeItemWidthSetted = true;
				}
				if (item.@height != undefined) {
					this.settedRangeItemHeight = SerializerBase.getNumber(item.@height);
					this.isRangeItemHeightSetted =  true;
				}
				if (SerializerBase.isEnabled(item.background[0])) {
					this.rangeItemBackground = new Style(BackgroundBaseStyleState);
					this.rangeItemBackground.deserialize(item.background[0], resources);
				}
			}
			if (SerializerBase.isEnabled(data.labels[0])) {
				this.labels = new ColorMapLabels();
				this.labels.deserialize(data.labels[0], resources);
				this.labels.control = this;
			}
			if (SerializerBase.isEnabled(data.tickmark[0])) {
				this.tickmark = new Stroke();
				this.tickmark.deserialize(data.tickmark[0]);
				if (data.tickmark[0].@size != undefined)
					this.tickmarksSize = SerializerBase.getNumber(data.tickmark[0].@size);
			}
			if (data.@tickmarks_placement != undefined) this.tickmarkOnRangeCenter = SerializerBase.getEnumItem(data.@tickmarks_placement) == "center";
			if (data.@inverted != undefined) this.inverted = SerializerBase.getBoolean(data.@inverted);
			if (data.@interval != undefined) this.interval = SerializerBase.getNumber(data.@interval);
			
			if (data.@threshold != undefined)
				this.threshold = SeriesPlot(this.plot).thresholdsList.getAutoThreshold(SerializerBase.getLString(data.@threshold));
				
			if (data.labels[0] != null && data.labels[0].@position != undefined) {
				switch (SerializerBase.getEnumItem(data.labels[0].@position)) {
					case "normal": this.labelsPosition = ColorMapLabelsPosition.NORMAL; break;
					case "combined": this.labelsPosition = ColorMapLabelsPosition.COMBINED; break;
					case "opposite": this.labelsPosition = ColorMapLabelsPosition.OPPOSITE; break;
				}
			}
		}
		
		override public function recalculateAndRedraw():void {
			this.isRangesInitialized = false;
			this.initializeRanges();
			super.recalculateAndRedraw();
		}
		
		private var isRangesInitialized:Boolean = false;
		private function initializeRanges():void {
			if (this.isRangesInitialized) 
				return;
				
			if (this.labels != null) 
				this.labels.initialize();
				
			this.ranges = [];
			
			var i:int;
			var cnt:int = this.threshold.getRangeCount()-1;
			if (this.inverted) {
				for (i = cnt;i>=0;i--)
					this.addRange(i);
			}else {
				for (i = 0;i<=cnt;i++)
					this.addRange(i);
			}
			
			this.calculateTickmarksSpace();
			this.calculateLabelsSpace();
				
			this.isRangesInitialized = true;
		}
		
		private function addRange(index:int):void {
			var color:uint = this.threshold.getRangeColor(index);
			this.rangeItemBackground.setDynamicColors(color);
			this.rangeItemBackground.setDynamicGradients(color);
			this.rangeItemBackground.setDynamicHatchTypes(color);
			
			var range:RangeShape = new RangeShape();
			range.style = this.rangeItemBackground;
			range.color = color;
			range.points = this.threshold.getRangePoints(index);
			this.container.addChild(range);
			this.ranges.push(range);
		}
		
		override protected function checkContentBounds():void {
			if (this.threshold == null) {
				this.bounds.width = 0;
				this.bounds.height = 0;
				super.checkContentBounds();
				return;
			}
			
			this.initializeRanges();
			/*
			Просчет размеров color swatch
			
			Допустим частный случай для ширины:
			Есть два параметра, вилающие на ширину:
			1. ширина контрола, руками заданая в XML
			2. ширина range item, руками заданая в XML
			
			Соостветственно получаем несколько случаев:
			1. Ничего не указано
			Подбираем ширину range item, исходя из label-ов и их позиций
			На основе этой ширины просчитываем ширину контрола 
			
			2. Указана ширина контрола
			Подбираем ширину range item так, что бы все влезли (учитываем label)
			
			3. Указана ширина range item
			На основе этой ширины просчитываем ширину контрола
			
			4. Указана и ширина контрола и ширина range item
			Просчитываем максимальную ширину range item исходя из того, что бы все влезло
			Выставлем минимальное значение из подсчитаной и указаной ширины - тогда все гарантировано влезет
			И затем если указаная ширина range item меньше подсчитаной - центруем содержимое
			*/
			
			this.leftShift = 0;
			this.topShift = 0;
			
			if (!this.layout.isWidthSetted && !this.isRangeItemWidthSetted) {
				this.rangeItemWidth = this.calculateOptimalRangeItemWidth();
				this.bounds.width = this.calculateWidthBasedOnRangeItemWidth();
			}else if (this.layout.isWidthSetted && !this.isRangeItemWidthSetted) {
				this.rangeItemWidth = this.calculateRangeItemWidthBasedOnWidth();
				if (this.rangeItemWidth < 0) {
					this.rangeItemWidth = 0;
					this.bounds.width = this.calculateWidthBasedOnRangeItemWidth();
				}
			}else if (!this.layout.isWidthSetted && this.isRangeItemWidthSetted) {
				this.rangeItemWidth = this.settedRangeItemWidth;
				this.bounds.width = this.calculateWidthBasedOnRangeItemWidth();
			}else {
				var maxWidth:Number = this.calculateRangeItemWidthBasedOnWidth();
				this.rangeItemWidth = Math.min(maxWidth, this.settedRangeItemWidth);
				
				var contentWidth:Number = this.calculateWidthBasedOnRangeItemWidth();
				var availableWidth:Number = this.getContentWidth();
				if (contentWidth < availableWidth)
					this.leftShift = (availableWidth - contentWidth)/2;
			}
			
			if (!this.layout.isHeightSetted && !this.isRangeItemHeightSetted) {
				this.rangeItemHeight = this.calculateOptimalRangeItemHeight();
				this.bounds.height = this.calculateHeightBasedOnRangeItemHeight();
			}else if (this.layout.isHeightSetted && !this.isRangeItemHeightSetted) {
				this.rangeItemHeight = this.calculateRangeItemHeightBasedOnHeight();
				if (this.rangeItemHeight < 0) {
					this.rangeItemHeight = 0;
					this.bounds.height = this.calculateHeightBasedOnRangeItemHeight();
				}
			}else if (!this.layout.isHeightSetted && this.isRangeItemHeightSetted) {
				this.rangeItemHeight = this.settedRangeItemHeight;
				this.bounds.height = this.calculateHeightBasedOnRangeItemHeight();
			}else {
				var maxHeight:Number = this.calculateRangeItemHeightBasedOnHeight();
				this.rangeItemHeight = Math.min(maxHeight, this.settedRangeItemHeight);
				
				var contentHeight:Number = this.calculateHeightBasedOnRangeItemHeight();
				var availableHeight:Number = this.getContentHeight();
				if (contentHeight < availableHeight)
					this.topShift = (availableHeight - contentHeight)/2;
			}
				
			super.checkContentBounds();
		}
		
		private function calculateTickmarksSpace():void {
			this.tickmarksSpace = 0;
			this.normalTickmarksSpace = 0;
			if (this.tickmark == null || !this.tickmark.enabled)
				return;
				
			switch (this.labelsPosition) {
				case ColorMapLabelsPosition.COMBINED:
					this.tickmarksSpace = this.tickmarksSize*2;
					this.normalTickmarksSpace = this.tickmarksSize;
					break;
				case ColorMapLabelsPosition.NORMAL:
					this.tickmarksSpace = this.tickmarksSize;
					this.normalTickmarksSpace = this.tickmarksSize;
					break;
				case ColorMapLabelsPosition.OPPOSITE:
					this.tickmarksSpace = this.tickmarksSize;
					break;
			}
		}
		
		/**
		 * Просчитать расстояния под label-ы
		 */
		protected function calculateLabelsSpace():void {}
		
		/**
		 * Подситать оптимальную ширину range item
		 */
		protected function calculateOptimalRangeItemWidth():Number { return NaN; }
		
		/**
		 * Подсчтитать ширину контрола исходя из range item width
		 */
		protected function calculateWidthBasedOnRangeItemWidth():Number { return NaN; }
		
		/**
		 * Подсчитать ширину range item исходя из ширины контрола
		 */
		protected function calculateRangeItemWidthBasedOnWidth():Number { return NaN; }
		
		/**
		 * Подситать оптимальную высоту range item
		 */
		protected function calculateOptimalRangeItemHeight():Number { return NaN; }
		
		/**
		 * Подсчтитать высоту контрола исходя из range item height
		 */
		protected function calculateHeightBasedOnRangeItemHeight():Number { return NaN; }
		
		/**
		 * Подсчитать высоту range item исходя из высоты контрола
		 */
		protected function calculateRangeItemHeightBasedOnHeight():Number { return NaN; }
		
		override public function draw():void {
			super.draw();
			this.drawRanges();
		}
		
		override public function resize():void {
			super.resize();
			this.clear();
			this.drawRanges();
		}
		
		private var ranges:Array;
		
		private function clear():void {
			for (var i:uint = 0;i<this.ranges.length;i++) {
				this.ranges[i].clear();
			}
		}
		
		protected var left:Number;
		protected var top:Number;
		
		protected function onBeforeDrawRanges():void {
			this.left = this.contentBounds.left + this.leftShift;
			this.top = this.contentBounds.top + this.topShift;
		}
		protected function setRangeBounds(rect:Rectangle, index:int):void {
			rect.width = this.rangeItemWidth;
			rect.height = this.rangeItemHeight;
		}
		
		protected function setLabelPosition(pos:Point, rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {}
		protected function drawTickmark(rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {}
		
		private function drawRanges():void {
			if (this.threshold == null) return;
			this.onBeforeDrawRanges();
			
			this.lastLabelRect = new Rectangle();
			this.lastEvenLabelRect = new Rectangle();
			this.lastOddLabelRect = new Rectangle();
			
			var pos:Point = new Point();
			
			var lastIndex:int = 0;
			
			for (var i:uint = 0;i<this.ranges.length;i++) {
				var range:RangeShape = this.ranges[i];
				this.setRangeBounds(range.bounds, i);
				range.draw();
				
				if (i % interval == 0) {
				
					if (this.tickmark != null) {
						this.tickmark.apply(this.container.graphics);
						this.drawTickmark(range.bounds,i, lastIndex);
						this.container.graphics.lineStyle();
					}
					
					if (this.labels != null) {
						
						this.setLabelPosition(pos, range.bounds, i, lastIndex);
						if (!this.checkLabelOverlap(i, pos, lastIndex))
							this.labels.drawLabel(this.container.graphics, pos, i);
					}
					
					lastIndex++;
				
				}
				
			}
			
			if (!this.tickmarkOnRangeCenter && (this.ranges.length % this.interval == 0)) {
				if (this.labels != null) {
					this.setLabelPosition(pos, range.bounds, this.ranges.length, lastIndex);
					if (!this.checkLabelOverlap(this.ranges.length, pos, lastIndex))
						this.labels.drawLabel(this.container.graphics, pos, this.ranges.length);
				}
				
				if (this.tickmark != null) {
					this.tickmark.apply(this.container.graphics);
					this.drawTickmark(range.bounds,this.ranges.length, lastIndex);
					this.container.graphics.lineStyle(); 
				}
			}
				
		}
		
		
		private var lastLabelRect:Rectangle;
		private var lastEvenLabelRect:Rectangle;
		private var lastOddLabelRect:Rectangle;

		private function checkLabelOverlap(index:int, pos:Point, lastIndex:int):Boolean {
			var labelRect:Rectangle = this.labels.getLabelBounds(index).clone();
			labelRect.x = pos.x;
			labelRect.y = pos.y;
			
			if (this.labelsPosition == ColorMapLabelsPosition.COMBINED) {
				if (lastIndex%2 == 0) {
					if (this.lastEvenLabelRect == null)
						this.lastEvenLabelRect = labelRect;
					else if (this.lastEvenLabelRect.intersects(labelRect))
						return true;
					this.lastEvenLabelRect = labelRect;
				}else {
					if (this.lastOddLabelRect == null)
						this.lastOddLabelRect = labelRect;
					else if (this.lastOddLabelRect.intersects(labelRect))
						return true;
					this.lastOddLabelRect = labelRect;
				}
			}else {
				if (this.lastLabelRect == null)
					this.lastLabelRect = labelRect;
				else if (this.lastLabelRect.intersects(labelRect))
					return true;
				this.lastLabelRect = labelRect;
			}
			return false;
		}
	}
}

import flash.display.Sprite;
import com.anychart.styles.Style;
import flash.geom.Rectangle;
import flash.events.MouseEvent;
import com.anychart.styles.states.StyleState;
import com.anychart.styles.states.BackgroundBaseStyleState;
import flash.display.Graphics;
import com.anychart.seriesPlot.data.BasePoint;

class RangeShape extends Sprite {
	public var style:Style;
	public var bounds:Rectangle;
	public var color:uint;
	
	public var points:Array;
	
	private var currentState:StyleState;
	
	public function RangeShape() {
		super();
		this.mouseChildren = false;
		this.buttonMode = true;
		this.useHandCursor = true;
		this.bounds = new Rectangle();
		this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
	}
	
	private function mouseOverHandler(event:MouseEvent):void {
		this.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
		this.currentState = this.style.hover;
		for (var i:uint = 0;i<this.points.length;i++) {
			var pt:BasePoint = this.points[i];
			if (pt != null)
				pt.setHover();
		}
		this.redraw()
		this.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHander);
	}
	
	private function mouseOutHander(event:MouseEvent):void {
		this.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHander);
		this.currentState = this.style.normal;
		for (var i:uint = 0;i<this.points.length;i++) {
			var pt:BasePoint = this.points[i];
			if (pt != null)
				pt.setNormal();
		}
		this.redraw()
		this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
	}
	
	public function draw():void {
		this.currentState = this.style.normal;
		this.redraw();
	}
	
	private function redraw():void {
		var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.currentState);
		var g:Graphics = this.graphics;
		
		g.clear();
		
		if (state.fill != null)
			state.fill.begin(g, this.bounds, this.color);
		if (state.stroke != null)
			state.stroke.apply(g, this.bounds, this.color);
			
		g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
		g.lineStyle();
		g.endFill();
		
		if (state.hatchFill != null) {
			state.hatchFill.beginFill(g, 0, this.color);
			g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			g.endFill();
		}
	}
	
	public function clear():void {
		this.graphics.clear();
	} 
}