package com.anychart.seriesPlot.data.settings {
	import com.anychart.actions.ActionsList;
	import com.anychart.interactivity.Interactivity;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	
	public class BaseSeriesSettings {
		public var global:GlobalSeriesSettings;
		public var style:Style;
		public var interactivity:Interactivity;
		public var actions:ActionsList;
		
		//-------------------------------------------------------------------
		//			BaseSeries/BasePoint.settings field creation
		//-------------------------------------------------------------------
		
		public function seriesContainsUniqueSettings(series:XML):Boolean {
			return this.checkElement(series);
		}
		
		public function pointContainsUniqueSettings(point:XML):Boolean {
			return this.checkElement(point);
		}
		
		protected function checkElement(data:XML):Boolean {
			if (data.@style != undefined) return true;
			if (this.interactivity.isNew(data)) return true;
			if (data.actions[0] != null && data.actions[0].action.length() > 0) return true;
			return false;
		}
		
		public function createCopy(settings:BaseSeriesSettings = null):BaseSeriesSettings {
			if (settings == null)
				settings = new BaseSeriesSettings();
			settings.global = this.global;
			settings.style = this.style;
			settings.interactivity = this.interactivity;
			settings.actions = this.actions;
			return settings;
		}
		
		//-------------------------------------------------------------------
		//			Deserialization
		//-------------------------------------------------------------------
		
		//<data_plot_settings><*_series />
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.deserializeGlobalStyle(data, stylesList,resources);
			if (this.interactivity.isNew(data.interactivity[0])) {
				this.interactivity = this.interactivity.createCopy();
				this.interactivity.deserialize(data.interactivity[0]);
			}
			if (data != null && data.actions[0] != null && data.actions[0].action.length() > 0) {
				this.actions = new ActionsList(this.global.chartBase);
				this.actions.deserialize(data.actions[0]);
			}
		}
		
		//<data />
		public function deserializeData(data:XML):void {
			if (data != null && data.actions[0] != null && data.actions[0].action.length() > 0) {
				this.actions = new ActionsList(this.global.chartBase);
				this.actions.deserialize(data.actions[0]);
			}
		}
		
		//<series />
		public function deserialiseSeries(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.deserializeElement(data, stylesList,resources);
		}
		
		//<point />
		public function deserializePoint(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.deserializeElement(data, stylesList, resources);
		}
		
		protected function deserializeElement(data:XML, stylesList:XML,resources:ResourcesLoader):void {
			if (data.@style != undefined) this.deserializeStyleApplicator(data.@style, stylesList,resources);
			if (this.interactivity.isNew(data)) {
				this.interactivity = this.interactivity.createCopy();
				this.interactivity.deserialize(data);
			}
			if (data.actions[0] != null && data.actions[0].action.length() > 0) {
				this.actions = new ActionsList(this.global.chartBase);
				this.actions.deserialize(data.actions[0]);
			}
		}
		
		//-------------------------------------------------------------------
		//			Styles deserialization
		//-------------------------------------------------------------------
		
		private function deserializeGlobalStyle(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			var styleNodeName:String = this.global.getStyleNodeName();
			if (styleNodeName == null) return;
			
			if (data.@style != undefined || data[styleNodeName][0] != null) {
				if (data.@style != undefined && data[styleNodeName][0] == null) {
					this.deserializeStyleApplicator(data.@style, stylesList,resources);
				}else {
					var parentStyleName:*;
					if (data.@style != undefined)
						parentStyleName = data.@style;
					if (data[styleNodeName][0] != null && data[styleNodeName][0].@parent != undefined) {
						parentStyleName = data[styleNodeName][0].@parent;
					}
					
					var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, styleNodeName, data[styleNodeName][0], parentStyleName);
					actualStyleXML.@name = parentStyleName;
					this.style = new Style(this.global.getStyleStateClass());
					this.style.deserialize(actualStyleXML, resources);
					this.style.normal.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.normal);
					this.style.hover.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.hover);
					this.style.pushed.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.pushed);
					this.style.missing.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.missing);
					this.style.selectedNormal.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.selectedNormal);
					this.style.selectedHover.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.selectedHover);
					this.global.checkStyle(this.style);
				}
			}else {
				var cashedStyle:Style = StylesList.getStyle(styleNodeName, "anychart_default");
				if (cashedStyle != null) {
					this.style = cashedStyle;
				}else {
					this.style = new Style(this.global.getStyleStateClass());
					this.style.deserialize(stylesList[styleNodeName].(@name == "anychart_default")[0],resources);
					StylesList.addStyle(styleNodeName, "anychart_default", this.style);
					this.global.checkStyle(this.style);
				}
			}
		}
		
		private function deserializeStyleApplicator(styleName:String, stylesList:XML,resources:ResourcesLoader):void {
			var styleNodeName:String = this.global.getStyleNodeName();
			var cashedStyle:Style = StylesList.getStyle(styleNodeName, styleName);
			if (cashedStyle != null) {
				this.style = cashedStyle;
			}else {
				var actualStyleXML:XML = StylesList.getStyleXMLByApplicator(stylesList, styleNodeName, styleName);
				if (actualStyleXML == null) {
					cashedStyle = StylesList.getStyle(styleNodeName, "anychart_default");
				}else {
					this.style = new Style(this.global.getStyleStateClass());
					this.style.deserialize(actualStyleXML,resources);
					
					this.style.normal.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.normal);
					this.style.hover.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.hover);
					this.style.pushed.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.pushed);
					this.style.missing.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.missing);
					this.style.selectedNormal.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.selectedNormal);
					this.style.selectedHover.programmaticStyle = this.global.getProgrammaticStyleClass(actualStyleXML, this.style.selectedHover);
					
					this.global.checkStyle(this.style);
					
					StylesList.addStyle(styleNodeName, styleName, style);
				}
			}
		}
	}
}