package com.anychart.seriesPlot.elements.tooltipElement {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.tooltip.ItemTooltipElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public final class TooltipElement extends ItemTooltipElement {
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new TooltipElement();
			super.createCopy(element);
			return element;
		}
		
		override protected function createSprite(container:IElementContainer, index:uint):Sprite {
			if (!(container is BasePoint)) return super.createSprite(container, index);
			return BasePlot(BasePoint(container).plot).getTooltipSprite(BasePoint.getTooltipContainerIndex(index));
		}
		
		public function needCreateElementForSeries(elementNode:XML):Boolean {
			if (elementNode == null) return false;
			if (elementNode.@style != undefined) return true;
			if (elementNode.@enabled != undefined && SerializerBase.getBoolean(elementNode.@enabled) != this.enabled) return true;
			if (elementNode.format[0] != null) return true;
			return false; 
		}
		
		public function needCreateElementForPoint(elementNode:XML):Boolean {
			return super.needCreateElementForContainer(elementNode);
		}
		
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			delete data.@enabled;
			this.deserializeStyleFromNode(stylesList, data, resources);
		}
		
		public function deserializeSeries(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
		}
		
		public function deserializePoint(data:XML, point:BasePoint, stylesList:XML, elementIndex:uint, resources:ResourcesLoader):void {
			super.deserializeElementForContainer(data, point, stylesList, elementIndex, resources);
		}
		
		override protected function setSpritePosition(container:IElementContainer, sprite:Sprite, pos:Point):void {
			if (container is BasePoint) BasePlot(BasePoint(container).plot).setTooltipPosition(pos);
			super.setSpritePosition(container, sprite, pos);
			
		}
	}
}