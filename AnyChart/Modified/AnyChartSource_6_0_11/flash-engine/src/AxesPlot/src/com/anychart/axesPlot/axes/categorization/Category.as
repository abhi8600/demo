package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.visual.text.TextElementInformation;
	
	public final class Category extends CategoryInfo {
		
		public var clusterSettings:ClusterSettings;
		
		private var sortedOverlays:Object;
		private var sortedOverlaysList:Array;
		
		private var stackSettings:Object;
		private var percentStackSettingsList:Array;
		
		private var overlaySettings:Object;
		
		public var info:TextElementInformation;
		
		public function Category(plot:SeriesPlot, index:int, name:String, isArgument:Boolean, axis:Axis) {
			this.axis = axis;
			this.index = index;
			this.name = name;
			super(plot);
		}
		
		override public function recalculate():void {
			for (var axisName:String in this.stackSettings) {
				for (var seriesType:* in this.stackSettings[axisName]) {
					var stackSettings:StackSettings = this.stackSettings[axisName][seriesType];
					stackSettings.recalculate();
				}
			} 
			
			super.recalculate();
		}
		
		//-------------------------------------------
		//				Points ordering
		//-------------------------------------------
		
		public function getSortedOverlay(axisName:String, seriesType:int):Array {
			if (this.sortedOverlays == null) {
				this.sortedOverlays = {};
				this.sortedOverlaysList = [];
			}
			if (this.sortedOverlays[axisName] == null)
				this.sortedOverlays[axisName] = {};
			if (this.sortedOverlays[axisName][seriesType] == null) {
				this.sortedOverlays[axisName][seriesType] = [];
				this.sortedOverlaysList.push(this.sortedOverlays[axisName][seriesType]);
			}
			return this.sortedOverlays[axisName][seriesType];
		}
		
		public function isStackSettingsExists(axisName:String, seriesType:int):Boolean {
			return this.stackSettings != null && this.stackSettings[axisName] != null && this.stackSettings[axisName][seriesType] != null;
		}
		
		public function getStackSettings(axisName:String, seriesType:int, isPercent:Boolean):StackSettings {
			if (this.stackSettings == null) {
				this.stackSettings = {};
				if (isPercent)
					this.percentStackSettingsList = [];
			}
			
			if (this.stackSettings[axisName] == null)
				this.stackSettings[axisName] = {};
			if (this.stackSettings[axisName][seriesType] == null) {
				this.stackSettings[axisName][seriesType] = new StackSettings(isPercent);
				if (isPercent)
					this.percentStackSettingsList.push(this.stackSettings[axisName][seriesType]);
			}
			
			return this.stackSettings[axisName][seriesType];
		}
		
		public function getOverlaysCount(axisName:String, seriesType:int):uint {
			this.checkOverlays(axisName, seriesType);
			return this.overlaySettings[axisName][seriesType];
		}
		
		public function setOverlaysCount(axisName:String, seriesType:int, value:uint):void {
			this.checkOverlays(axisName, seriesType);
			this.overlaySettings[axisName][seriesType] = value;
		}
		
		private function checkOverlays(axisName:String, seriesType:int):void {
			if (this.overlaySettings == null)
				this.overlaySettings = {};
			
			if (this.overlaySettings[axisName] == null)
				this.overlaySettings[axisName] = {};
				
			if (this.overlaySettings[axisName][seriesType] == undefined)
				this.overlaySettings[axisName][seriesType] = 0;
		}
		
		public function onAfterDeserializeData(plotDrawingPoints:Array):void {
			var i:uint;
			if (this.sortedOverlaysList != null) {
				for (i=0;i<this.sortedOverlaysList.length;i++) {
					var list:Array = this.sortedOverlaysList[i];
					list.sortOn("sortedOverlayValue",Array.NUMERIC | Array.DESCENDING);
					
					var initialize:Boolean = true;
					if (list.length > 0)
						 initialize = AxesPlotPoint(list[0]).global.initializeIfSortedOVerlay();
					
					for (var j:uint = 0;j<list.length;j++) {
						if (AxesPlotPoint(list[j]).z)
							AxesPlotPoint(list[j]).z.setZ(list.length - j - 1);
						if (initialize) {
							list[j].initialize();
							plotDrawingPoints.push(list[j]);
						}
					}
					list = null;
				}
				this.sortedOverlays = null;
				this.sortedOverlaysList = null;
			}
			if (this.percentStackSettingsList != null) {
				for (i<0;i<this.percentStackSettingsList.length;i++)
					this.percentStackSettingsList[i].setPercent();
				this.percentStackSettingsList = null;
			}
		}
	}
}