package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.CategorizedAxisLabels;
	import com.anychart.axesPlot.axes.text.StagerCategorizedAxisLabels;
	import com.anychart.axesPlot.scales.TextDateTimeScale;
	import com.anychart.axesPlot.scales.TextScale;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	
	import flash.geom.Rectangle;
	
	public class BaseCategorizedAxis extends Axis {
		
		protected var categoriesMap:Object;
		protected var categoriesList:Array;
		
		protected var clusterSettingsList:Array;
		
		public function BaseCategorizedAxis() {
			super();
			this.categoriesMap = {};
			this.categoriesList = [];
			this.clusterSettingsList = [];
		}
		
		override public function destroy():void {
			super.destroy();
			for each (var cat:CategoryInfo in this.categoriesList) {
				if (cat)
					cat.destroy();
			}
			this.categoriesList = null;
			this.categoriesMap = null;
			this.clusterSettingsList = null;
		}
		
		override public function drawZeroLine():void {
		}
		
		override public function onAfterDataDeserialize(plotDrawingPoints:Array):void {
			for (var i:uint = 0;i<this.categoriesList.length;i++) 
				Category(this.categoriesList[i]).onAfterDeserializeData(plotDrawingPoints);
		}
		
		override protected function onSetBounds(bounds:Rectangle):void {
			var categoryWidth:Number = this.scale.transform(this.scale.minimum+1)-this.scale.transform(this.scale.minimum);
			for each (var clusterSettings:ClusterSettings in this.clusterSettingsList)
				clusterSettings.calculate(categoryWidth);
		}
		
		override protected function createScale(scaleNode:XML):BaseScale {
			if (scaleNode == null || scaleNode.@type == undefined || SerializerBase.getEnumItem(scaleNode.@type) != "datetime")
				return new TextScale(this);
			return new TextDateTimeScale(this);
		}
		
		override protected function createLabels(labelsNode:XML):AxisLabels {
			return (labelsNode.@display_mode != undefined && SerializerBase.getEnumItem(labelsNode.@display_mode) == "stager") ?
						new StagerCategorizedAxisLabels(this, this.viewPort) :
						new CategorizedAxisLabels(this, this.viewPort);
		}
		
		override public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return token == '%Value' ? this.categoriesList[scaleValue].name : '';
		}
		
		override public function setLabelsSpace(bounds:Rectangle, scaleBounds:Rectangle, totalBounds:Rectangle):void {
			this.applyLabelsSpace(bounds, scaleBounds, totalBounds, this.scale.majorIntervalsCount - 1);
		}
		
		protected function createCategory(name:String):Category {
			var category:Category = new Category(SeriesPlot(this.plot),this.categoriesList.length, name,this.isArgument, this);
			if (this.isArgument) {
				this.plot.xCategories.push(category);
				this.registerDataCategory(category);
			}else {
				this.plot.yCategories.push(category);
			}
			if (this.labels != null)
				CategorizedAxisLabels(this.labels).addCategory(category);
			this.checkScaleValue(category.index);
			this.categoriesList.push(category);
			this.categoriesMap[name] = category;
			return category;
		}
		
		protected function registerDataCategory(category:Category):void {
			this.plot.dataCategories.push(category);
		}
		
		public function getCategoryByIndex(index:int):Category {
			return this.categoriesList[index];
		}
		
		override public function recalculateDataRange():void {
			for (var i:int = 0;i<this.categoriesList.length;i++) {
				this.categoriesList[i].recalculate();
			}
		}
		
		public function getCategory(name:String):Category {
			var category:Category = this.categoriesMap[name];
			if (category == null)
				category = this.createCategory(name);
			return category;
		}
		
		public function hasCategory(name:String):Boolean {
			return this.categoriesMap[name] != null;
		}
	}
}