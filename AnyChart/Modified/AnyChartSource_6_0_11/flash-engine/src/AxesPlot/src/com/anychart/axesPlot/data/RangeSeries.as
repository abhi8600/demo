package com.anychart.axesPlot.data {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BaseDataElement;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	public class RangeSeries extends AxesPlotSeries {
		
		public var additionalLabel:LabelElement;
		public var additionalMarker:MarkerElement;
		public var additionalTooltip:TooltipElement;
		
		public function RangeSeries() {
			super();
			this.cashedTokens['RangeMax'] = -Number.MAX_VALUE;
			this.cashedTokens['RangeMin'] = Number.MAX_VALUE;
			this.cashedTokens['RangeSum'] = 0;
		}
		
		//-----------------------------------------------------------------------
		//			ELEMENTS
		//-----------------------------------------------------------------------
		
		override public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.start_point[0] != null) {
				this.label = this.deserializeLabelElement(data.start_point[0], styles, this.label, resources);
				this.marker = this.deserializeMarkerElement(data.start_point[0], styles, this.marker, resources);
				this.tooltip = this.deserializeTooltipElement(data.start_point[0], styles, this.tooltip, resources);
			}
			if (data.end_point[0] != null) {
				this.additionalLabel = this.deserializeLabelElement(data.end_point[0], styles, this.additionalLabel, resources);
				this.additionalMarker = this.deserializeMarkerElement(data.end_point[0], styles, this.additionalMarker, resources);
				this.additionalTooltip = this.deserializeTooltipElement(data.end_point[0], styles, this.additionalTooltip, resources);
			}
		}
		
		override public function setElements(data:BaseDataElement):void {
			super.setElements(data);
			data['additionalMarker'] = this.additionalMarker;
			data['additionalLabel'] = this.additionalLabel;
			data['additionalTooltip'] = this.additionalTooltip;
		}
		
		override public function initializeElementsStyle():void {
			super.initializeElementsStyle();
			
			this.initializeElementStyleHatchType(this.additionalLabel, this.hatchType)
			this.initializeElementStyleHatchType(this.additionalMarker, this.hatchType)
			this.initializeElementStyleHatchType(this.additionalTooltip, this.hatchType)
		}
		
		override public function initializeElementsStyleColor(color:uint):void {
			this.initializeElementStyleColor(this.additionalLabel, color);
			this.initializeElementStyleColor(this.additionalMarker, color);
			this.initializeElementStyleColor(this.additionalTooltip, color);
		}
		
		//-----------------------------------------------------------------------
		//			TOKENTS
		//-----------------------------------------------------------------------
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var pt:RangePoint = RangePoint(point);
			this.cashedTokens['RangeSum'] += pt.range;
			if (pt.range > this.cashedTokens['RangeMax'])
				this.cashedTokens['RangeMax'] = pt.range;
			
			if (pt.range < this.cashedTokens['RangeMin']) 
				this.cashedTokens['RangeMin'] = pt.range;
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			if (plotTokensCash['RangeSum'] == null)
				plotTokensCash['RangeSum'] = 0;
				
			plotTokensCash['RangeSum'] += this.cashedTokens['RangeSum'];
			
			if (plotTokensCash['RangeMax'] == null || this.cashedTokens['RangeMax'] > plotTokensCash['RangeMax']) 
				plotTokensCash['RangeMax'] = this.cashedTokens['RangeMax'];

			if (plotTokensCash['RangeMin'] == null || this.cashedTokens['RangeMin'] < plotTokensCash['RangeMin']) 
				plotTokensCash['RangeMin'] = this.cashedTokens['RangeMin'];
			
			super.checkPlot(plotTokensCash);
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesYRangeMax') return this.cashedTokens['RangeMax'];
			if (token == '%SeriesYRangeMin') return this.cashedTokens['RangeMin'];
			if (token == '%SeriesYRangeSum') return this.cashedTokens['RangeSum'];
			return super.getSeriesTokenValue(token);
		}
		
		override public function isDateTimeSeriesToken(token:String):Boolean {
			if (this.valueAxis.isDateTimeAxis()) {
				if (token == '%SeriesYRangeMax') return true;
				if (token == '%SeriesYRangeMin') return true;
				if (token == '%SeriesYRangeSum') return true;
			}
			return super.isDateTimeSeriesToken(token);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.YRangeSum = this.cashedTokens['RangeSum'];
			res.YRangeMax = this.cashedTokens['RangeMax'];
			res.YRangeMin = this.cashedTokens['RangeMin'];
			return res;
		}
	}
}