package com.anychart.axesPlot.data {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.axesPlot.axes.categorization.ClusterSettings;
	import com.anychart.scales.BaseScale;
	import com.anychart.scales.ScaleMode;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	
	public class AxesPlotSeries extends BaseSeries {
		
		public var argumentAxis:Axis;
		public var valueAxis:Axis;
		
		public var category:Category;
		public var clusterSettings:ClusterSettings;
		public var clusterIndex:uint;
		public var isClusterizableOnCategorizedAxis:Boolean;
		public var clusterContainsOnlyOnePoint:Boolean;
		public var clusterKey:uint;
		
		public var isSingleCluster:Boolean;
		
		public var isSortable:Boolean;
		
		override public function addPointsToDrawing():Boolean {
			return !this.isSortable || this.valueAxis.scale.mode != ScaleMode.SORTED_OVERLAY;
		}
		
		public function AxesPlotSeries() {
			super();
			this.isClusterizableOnCategorizedAxis = true;
			this.clusterContainsOnlyOnePoint = false;
			this.isSortable = false;
			this.isSingleCluster = false;
			
			this.cashedTokens['XSum'] = 0;
			this.cashedTokens['XMax'] = -Number.MAX_VALUE;
			this.cashedTokens['XMin'] = Number.MAX_VALUE;
		}
		
		public function configureValueScale(scale:BaseScale):void {}
		
		//-------------------------------------------------
		//				Formatting
		//-------------------------------------------------
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var pt:AxesPlotPoint = AxesPlotPoint(point);
			this.cashedTokens['XSum'] += pt.x;
			if (pt.x > this.cashedTokens['XMax'])
				this.cashedTokens['XMax'] = pt.x;
			if (pt.x < this.cashedTokens['XMin'])
				this.cashedTokens['XMin'] = pt.x;
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			
			if (plotTokensCash['XSum'] == null)
				plotTokensCash['XSum'] = 0;
			
			plotTokensCash['XSum'] += this.cashedTokens['XSum'];
			if (plotTokensCash['XMax'] == null || this.cashedTokens['XMax'] > plotTokensCash['XMax'])
				plotTokensCash['XMax'] = this.cashedTokens['XMax'];
			if (plotTokensCash['XMin'] == null || this.cashedTokens['XMin'] < plotTokensCash['XMin'])
				plotTokensCash['XMin'] = this.cashedTokens['XMin'];
				
			this.checkAxesTokens();
		}
		
		protected function checkAxesTokens():void {
			var cashedTokens:Object = this.argumentAxis.cashedTokens;
			cashedTokens["PointCount"]+=this.points.length;
			cashedTokens["Sum"] += this.cashedTokens['XSum'];
			if (this.cashedTokens['XMax'] > cashedTokens["Max"])
				cashedTokens["Max"] = this.cashedTokens['XMax'];
			if (this.cashedTokens['XMin'] < cashedTokens["Min"])
				cashedTokens["Min"] = this.cashedTokens['XMin'];
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesFirstXValue') return (this.points.length > 0 && this.points[0]!=null) ? this.points[0].x : '';
			if (token == '%SeriesLastXValue') return (this.points.length > 0 && this.points[this.points.length-1]!=null) ? this.points[this.points.length-1].x : '';
			if (token == '%SeriesXSum') return this.cashedTokens['XSum'];
			if (token == '%SeriesXMax') return this.cashedTokens['XMax'];
			if (token == '%SeriesXMin') return this.cashedTokens['XMin'];
			if (token == '%SeriesXAverage') {
				if (this.cashedTokens['XAverage'] == null)
					this.cashedTokens['XAverage'] = this.cashedTokens['XSum']/this.points.length;
				return this.cashedTokens['XAverage'];
			}
			if (token == '%SeriesYAxisName') return this.valueAxis.getAxisTokenValue("%AxisName");
			if (token == '%SeriesXAxisName') return this.argumentAxis.getAxisTokenValue("%AxisName");
			return super.getSeriesTokenValue(token);
		}
		
		override public function getTokenValue(token:String):* {
			if (token.indexOf("%YAxis") == 0)
				return this.valueAxis.getAxisTokenValue("%"+token.substr(2));
			if (token.indexOf("%XAxis") == 0)
				return this.argumentAxis.getAxisTokenValue("%"+token.substr(2));
			return super.getTokenValue(token);
		}
		
		override public function isDateTimeToken(token:String):Boolean {
			if (token.indexOf("%YAxis") == 0)
				return this.valueAxis.isDateTimeToken(token);
			if (token.indexOf("%XAxis") == 0)
				return this.argumentAxis.isDateTimeToken(token);
			
			return super.isDateTimeToken(token);
		}
		
		override public function isDateTimeSeriesToken(token:String):Boolean {
			if (AxesPlotSeries(this).argumentAxis.isDateTimeAxis()) {
				if (token == "%SeriesFirstXValue") return true;
				if (token == "%SeriesLastXValue") return true; 
				if (token == "%SeriesXSum") return true;
				if (token == "%SeriesXMax") return true;
				if (token == "%SeriesXMin") return true;
				if (token == "%SeriesXAverage") return true;
			}
			return false;
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.StartMarkerType = res.EndMarkerType = this.markerType;
			res.FirstXValue = this.getSeriesTokenValue("%SeriesFirstXValue");
			res.LastXValue = this.getSeriesTokenValue("%SeriesLastXValue");
			res.XSum = this.cashedTokens['XSum'];
			res.XMax = this.cashedTokens['XMax'];
			res.XMin = this.cashedTokens['XMin'];
			res.XAverage = this.getSeriesTokenValue("%SeriesXAverage");
			res.XMedian = this.getSeriesTokenValue("%SeriesXMedian");
			res.XMode = this.getSeriesTokenValue("%SeriesXMode");
			
			if (this.valueAxis) res.YAxis = this.valueAxis.name;
			if (this.argumentAxis) res.XAxis = this.argumentAxis.name;
			return res;
		}
	}
}