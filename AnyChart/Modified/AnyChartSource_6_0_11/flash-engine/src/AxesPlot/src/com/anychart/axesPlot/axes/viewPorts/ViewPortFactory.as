package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.visual.layout.Position;
	
	public final class ViewPortFactory {
		public static function create(axis:Axis):IAxisViewPort {
			switch (axis.position) {
				case Position.LEFT: return new LeftAxisViewPort(axis);
				case Position.RIGHT: return new RightAxisViewPort(axis);
				case Position.TOP: return new TopAxisViewPort(axis);
				case Position.BOTTOM: return new BottomAxisViewPort(axis);
			}
			throw new Error("Unknown axis position");
		}
	}
}