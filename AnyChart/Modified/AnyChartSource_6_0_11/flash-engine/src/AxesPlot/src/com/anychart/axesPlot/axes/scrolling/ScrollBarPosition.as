package com.anychart.axesPlot.axes.scrolling {
	
	public final class ScrollBarPosition {
		public static const INSIDE:uint = 0;
		public static const OUTSIDE:uint = 1;
	}
}