package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	
	import flash.geom.Point;
	
	public class CategorizedAxis extends BaseCategorizedAxis {
		
		private var clusterSettingsMap:Object;
		
		public function CategorizedAxis() {
			super();
			this.clusterSettingsMap = {};
		}
		
		override public function checkSeries(series:AxesPlotSeries):void {
			if (!series.isClusterizableOnCategorizedAxis) return;
			var clusterSettings:ClusterSettings = this.clusterSettingsMap[series.clusterKey];
			if (clusterSettings == null) {
				clusterSettings = new ClusterSettings();
				clusterSettings.groupPadding = series.global.groupPadding;
				clusterSettings.pointPadding = series.global.pointPadding;
				this.clusterSettingsMap[series.clusterKey] = clusterSettings;
				this.clusterSettingsList.push(clusterSettings);
			}
			series.clusterSettings = clusterSettings;
			if (series.clusterContainsOnlyOnePoint) {
				series.clusterIndex = 0;
				clusterSettings.numClusters = 1;
			}else {
				series.clusterIndex = clusterSettings.checkAxis(series.valueAxis);
			}
		}
		
		override public function transform(dataPoint:AxesPlotPoint, value:Number, point:Point, pixOffset:Number = 0):void {
			if (!AxesPlotSeries(dataPoint.series).isClusterizableOnCategorizedAxis) {
				super.transform(dataPoint, value, point, pixOffset);
			}else {
				var pixValue:Number = this.scale.transform(value) + pixOffset;
				var series:AxesPlotSeries = AxesPlotSeries(dataPoint.series); 
				pixValue += series.clusterSettings.getOffset(series.clusterIndex);
				this.viewPort.setScaleValue(pixValue, point);
			}
		}
		
		override public function simpleTransform(dataPoint:AxesPlotPoint, value:Number):Number {
			if (!AxesPlotSeries(dataPoint.series).isClusterizableOnCategorizedAxis)
				return super.simpleTransform(dataPoint, value);
			
			var pixValue:Number = this.scale.transform(value);
			var series:AxesPlotSeries = AxesPlotSeries(dataPoint.series); 
			pixValue += series.clusterSettings.getOffset(series.clusterIndex);
			return pixValue;
		}
	}
}