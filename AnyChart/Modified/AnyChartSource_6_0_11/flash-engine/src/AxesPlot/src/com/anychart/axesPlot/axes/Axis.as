package com.anychart.axesPlot.axes {
	import com.anychart.axesPlot.IAxesPlot;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.axes.grid.AxisGrid;
	import com.anychart.axesPlot.axes.markers.AxisMarkersList;
	import com.anychart.axesPlot.axes.scrolling.AxisDataViewApplicator;
	import com.anychart.axesPlot.axes.scrolling.AxisScrollBarFactory;
	import com.anychart.axesPlot.axes.scrolling.IAxisScrollBar;
	import com.anychart.axesPlot.axes.scrolling.ScrollBarPosition;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.axesPlot.axes.tickmark.AxisTickmark;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.scales.LinearScale;
	import com.anychart.axesPlot.scales.TextDateTimeScale;
	import com.anychart.axesPlot.scales.ValueScale;
	import com.anychart.axesPlot.scales.dateTime.DateTimeScale;
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.formatters.IFormatable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.uiControls.scrollBar.ScrollBarEvent;
	import com.anychart.utils.StatisticUtils;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.fill.FillType;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	
	public class Axis implements IFormatable,IObjectSerializable {
		
		public var position:uint;
		
		public var name:String;
		public var enabled:Boolean;
		
		public var plot:IAxesPlot;
		
		public var fixedSpace:Number;
		public var titleSpace:Number;
		public var space:Number;
		
		private var tickmarksSpace:Number;
		private var insideTickmakrsSpace:Number;
		private var outsideTickmarkSpace:Number;
		
		private var markersFixedSpace:Number;
		private var markersLabelsSpace:Number;
		private var markersBeforeSpace:Number;
		
		private var scrollBarSpace:Number;
		
		private var markers:AxisMarkersList;
		
		public var isArgument:Boolean;
		
		public var points:Array;
		
		private var scrollBar:IAxisScrollBar;
		public var zoomManager:AxisDataViewApplicator;
		
		public function get isZoomEnabled():Boolean { return this.zoomManager != null; }
		
		public function execZoom():void {
			if (this.zoomManager) this.zoomManager.execZoom();
		}
		
		public function Axis() {
			this.enabled = true;
			this.gridBounds = new Rectangle();
			this.isCenterTickmarks = false;
			this.cashedTokens = {};
			this.cashedTokens['Sum'] = 0;
			this.cashedTokens['Max'] = -Number.MAX_VALUE;
			this.cashedTokens['Min'] = Number.MAX_VALUE;
			this.titleSpace = 0;
			this.scrollBarSpace = 0;
			this.isArgument = false;
			this.points = [];
		}
		
		public function destroy():void {
			this.points = null;
			this.plot = null;
		}
		
		public var crossAxis:Axis;
		
		//----------------------------------------------------------------
		//					   EXTRA
		//----------------------------------------------------------------
		
		public var isOpposite:Boolean;
		public var isFirstInList:Boolean;
		public var padding:Number;
		
		public var offset:Number;
		public var extraAdditionalOffset:Number;
		
		//----------------------------------------------------------------
		//					   VISUALIZATION
		//----------------------------------------------------------------
		
		private var majorGrid:AxisGrid;
		private var majorTickmark:AxisTickmark;
		
		private var minorGrid:AxisGrid;
		private var minorTickmark:AxisTickmark;
		
		private var line:Stroke;
		private var zeroLine:Stroke;
		
		private var title:AxisTitle;
		public var labels:AxisLabels;
		
		public var isCenterTickmarks:Boolean;
		
		//----------------------------------------------------------------
		//					   Containers
		//----------------------------------------------------------------
		
		private var fixedContainer:Sprite;
		private var outsideContainer:Sprite;
		
		public function setContainers(fixedContainer:Sprite, outsideContainer:Sprite):void {
			this.fixedContainer = fixedContainer;
			this.outsideContainer = outsideContainer;
		}
		
		//----------------------------------------------------------------
		//					   GEOMETRY
		//----------------------------------------------------------------
		
		public var viewPort:IAxisViewPort;
		
		public var scale:BaseScale;
		
		public final function createViewPort(viewPortFactory:Class):void {
			this.viewPort = viewPortFactory['create'](this);
		}
		
		//----------------------------------------------------------------
		//					   SIZE
		//----------------------------------------------------------------
		
		private function getOutsideLabelsSpace():Number {
			return (this.labels && !this.labels.isInsideLabels) ? this.labels.totalSpace : 0; 
		}
		
		public final function calculateWidthDefaultSize(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			if (!this.enabled) return;
			this.viewPort.setScaleBounds(totalBounds, visibleBounds);
			
			this.scale.calculate();
			if (this.enabled && this.labels != null)
				this.labels.calcSpace();
		}
		
		public final function recalculateWithDynamicSize(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			this.calculateWidthDefaultSize(totalBounds, visibleBounds);
			
			if (this.enabled && this.labels != null)
				this.space = this.markersFixedSpace + this.fixedSpace + this.titleSpace + Math.max(this.getOutsideLabelsSpace(), this.markersLabelsSpace);
			else
				this.space = this.markersFixedSpace + this.fixedSpace + this.titleSpace + this.markersLabelsSpace;
		}
		
		public final function applyFixedOffset(bounds:Rectangle):void {
			if (this.enabled && !ValueScale(this.scale).isCrossingEnabled)
				this.viewPort.applyOffset(bounds, this.fixedSpace+this.titleSpace+this.elementsSpace+this.markersFixedSpace);
		}
		
		public final function applyDynamicOffset(bounds:Rectangle):void {
			if (!this.enabled || ValueScale(this.scale).isCrossingEnabled) return;
			
			if (this.labels != null)
				this.viewPort.applyOffset(bounds, Math.max(this.getOutsideLabelsSpace(), this.markersLabelsSpace));
			else
				this.viewPort.applyOffset(bounds, this.markersLabelsSpace);
		}
		
		public final function setBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			this.viewPort.setScaleBounds(totalBounds, visibleBounds);
						
			this.scale.calculate();
			this.initTitle();
			this.onSetBounds(totalBounds);
		}
		
		protected final function applyLabelsSpace(bounds:Rectangle, scaleBounds:Rectangle, totalBounds:Rectangle, lastInterval:uint):void {
			if (!this.enabled || this.labels == null) return;
			
			this.viewPort.setScaleBounds(totalBounds, scaleBounds);
			
			var firstLabel:Rectangle = this.labels.getLabelBounds(0);
			var lastLabel:Rectangle = this.labels.getLabelBounds(lastInterval);
			
			if (firstLabel && lastLabel) {
				if (firstLabel.x > bounds.width)
					firstLabel = lastLabel;
				else if (lastLabel.x > bounds.width) 
					lastLabel = firstLabel;
				
				if (firstLabel.y > bounds.height)
					firstLabel = lastLabel;
				else if (lastLabel.y > bounds.height)
					lastLabel = firstLabel;
				
				this.viewPort.applyLabelsOffsets(bounds, firstLabel, lastLabel);
			}
		}
		
		public function setLabelsSpace(bounds:Rectangle, scaleBounds:Rectangle, totalBounds:Rectangle):void {
			this.applyLabelsSpace(bounds, scaleBounds, totalBounds, this.scale.majorIntervalsCount);
		}
		
		protected function onSetBounds(bounds:Rectangle):void {
		}
		
		//----------------------------------------------------------------
		//	Drawing axis line
		//----------------------------------------------------------------
		
		private var axisLineBounds:Rectangle;
		
		public function drawAxisLine():void {
			if (!this.enabled || this.line == null) return;
			
			var position:Number = this.getOffset(this.offset, 0);
			
			var pixStart:Number = this.viewPort.getVisibleStart();
			var pixEnd:Number = this.viewPort.getVisibleEnd();
			
			if (this.line.type == StrokeType.GRADIENT) 
				this.viewPort.setParallelLineBounds(this.line.thickness,pixStart,pixEnd,position,this.axisLineBounds);
			var g:Graphics = this.fixedContainer.graphics;
			this.line.apply(g, this.axisLineBounds);
			this.viewPort.drawParallelLine(g, pixStart, pixEnd, position);
			g.lineStyle();
		}
		
		private function getOffset(offset:Number, extraOffsetInCross:Number):Number {
			var position:Number = offset;
			if (ValueScale(this.scale).isCrossingEnabled) {
				position = this.crossAxis.scale.transform(ValueScale(this.scale).crossValue);
				var pt:Point = new Point();
				this.crossAxis.viewPort.setScaleCrossingValue(position, pt);
				position = pt.x != 0 ? pt.x : pt.y;
				position = -position+extraOffsetInCross;
			}else {
				position += this.scrollBarSpace;
			}
			return position;
		}
		
		//----------------------------------------------------------------
		//	Drawing axis markers
		//----------------------------------------------------------------
		
		public function drawAxisMarkers():void {
			if (!this.enabled || this.markers == null) return;
			this.markers.draw();
		}
		
		//----------------------------------------------------------------
		//	Drawing title
		//----------------------------------------------------------------
		
		public function initTitle():void {
			if (this.enabled && this.title != null) {
				this.title.initialize();
				this.titleSpace = this.title.space;
			}
		}
		
		public function drawTitle():void {
			if (!this.enabled || this.title == null) return;
			
			var alwaysOffset:Number = this.isFirstInList ? this.tickmarksSpace : this.outsideTickmarkSpace;
			
			this.title.offset = this.offset;
			if (this.labels != null)
				alwaysOffset += Math.max(this.markersLabelsSpace, this.getOutsideLabelsSpace());
			else
				this.title.offset += this.markersLabelsSpace;
			
			this.title.offset += this.elementsSpace + this.markersFixedSpace;
			
			this.title.offset += alwaysOffset;
			
			this.title.offset = this.getOffset(this.title.offset, alwaysOffset);
			this.title.drawAxisTitle(this.viewPort);
		}
		
		//----------------------------------------------------------------
		//	Drawing scrollBar
		//----------------------------------------------------------------
		
		public function initZoom():void {
			if (this.zoomManager) this.zoomManager.initialize();
		}
		
		public function initScrollBar(bounds:Rectangle):void {
			if (!this.enabled || !this.scrollBar  ) return;
			
			var container:DisplayObject = this.scrollBar.initialize(this.getScrollBarBounds(bounds));
				
			if (this.zoomManager.showScrollBar){
				this.fixedContainer.addChild(container);
			}
			this.zoomManager.setScrollBaseValue();
			this.scrollBar.addEventListener(ScrollBarEvent.CHANGE, this.scrollBarPositionChange);
		}
		
		public function drawScrollBar():void {
			if (!this.enabled || !this.scrollBar || !this.zoomManager.showScrollBar) return;
			this.scrollBar.draw();
		}
		
		public function resizeScrollBar(bounds:Rectangle):void {
			if (!this.enabled || !this.scrollBar) return;
			this.scrollBar.resize(this.getScrollBarBounds(bounds));
		}
		
		private function getScrollBarBounds(bounds:Rectangle):Rectangle {
			bounds = bounds.clone();
			var pos:Point = this.viewPort.getScrollBarPixPosition(this.scrollBar.position, this.offset, this.scrollBar.scrollBarSize);
			bounds.x = pos.x;
			bounds.y = pos.y;
			return bounds;
		}
		
		public var scrollSource:String="-";
		public var scrollPhase:String="-";
		
		public final function scrollBarPositionChange(event:ScrollBarEvent, callFromScrollBar:Boolean = true):void {
			if (!this.plot) return;
			
			var eScrollInfo:Object;
			if (this.isArgument) 
			{
				this.plot.moveDataView(event.value, NaN, callFromScrollBar);
				if (!this.plot.isExternalCall_ScrollBarChangeX)
				{
					var eX:Object= new Object();
					eX.type = "XAxisScrollChange";
					eScrollInfo=this.plot.getXScrollInfo();
					if (eScrollInfo == null) return;
					eX.startValue = eScrollInfo.start;
    				eX.valueRange = eScrollInfo.range;
    				eX.endValue = eX.startValue + eX.valueRange;
    				eX.phase = this.scrollPhase;
    				eX.source = this.scrollSource;
    				this.plot.dispatchScrollerEvent(eX.type, eX.source, eX.phase, eX.startValue, eX.endValue, eX.valueRange);
    			}	
				this.plot.isExternalCall_ScrollBarChangeX=false;	
			}		
			else
			{
				this.plot.moveDataView(NaN, event.value, callFromScrollBar);
				if (!this.plot.isExternalCall_ScrollBarChangeY)
				{
					var eY:Object= new Object();
					eY.type = "YAxisScrollChange";
					eScrollInfo=this.plot.getYScrollInfo();
					if (eScrollInfo == null) return;
					eY.startValue = eScrollInfo.start;
    				eY.valueRange = eScrollInfo.range;
    				eY.endValue = eY.startValue + eY.valueRange;
    				eY.phase = this.scrollPhase;
    				eY.source = this.scrollSource;
    				this.plot.dispatchScrollerEvent(eY.type, eY.source, eY.phase, eY.startValue, eY.endValue, eY.valueRange);
    			}	
				this.plot.isExternalCall_ScrollBarChangeY=false;	
			}
			
		}
		
		public function deserializeScrollBar(axesNode:XML, axisNode:XML, resources:ResourcesLoader):void {
			if (SerializerBase.isEnabled(axisNode.zoom[0])) {
				this.zoomManager = new AxisDataViewApplicator(this);
				this.zoomManager.deserialize(axisNode.zoom[0]);
				this.scrollBar = AxisScrollBarFactory.create(this, this.plot.root.getMainContainer(), this.position);
				if (this.scrollBar) {
					this.zoomManager.setScrollBar(this.scrollBar);
					this.scrollBar.deserialize(AxisScrollBarFactory.getSettings(axesNode.scroll_bar_settings[0], this.scrollBar), resources);
						
					if (this.zoomManager.showScrollBar) {
						if (this.scrollBar.position != ScrollBarPosition.INSIDE) {
							this.scrollBarSpace = this.scrollBar.scrollBarSize;
							this.fixedSpace += this.scrollBarSpace;
						}
					}//else
				}
			}
			this.space = this.fixedSpace;
		}
		
		//----------------------------------------------------------------
		//	Drawing labels
		//----------------------------------------------------------------
		
		public function drawLabels():void {
			if (!this.enabled || this.labels == null) return;
			
			if (this.labels.isInsideLabels)
				this.setInsideLabelsOffset();
			else
				this.setOutsideLabelsOffset();
							
			this.labels.drawLabels(this.outsideContainer.graphics);
		}
		
		private function setOutsideLabelsOffset():void {
			var alwaysOffset:Number = this.isFirstInList ? this.tickmarksSpace : this.outsideTickmarkSpace; 
			this.labels.offset = this.offset;
			alwaysOffset += this.labels.padding;
			this.labels.offset += this.elementsSpace;
			this.labels.offset += this.markersBeforeSpace;
			this.labels.offset += alwaysOffset;
			this.labels.offset = this.getOffset(this.labels.offset, alwaysOffset);
		}
		
		private function setInsideLabelsOffset():void {
			this.labels.offset = this.offset - this.insideTickmakrsSpace - this.labels.padding - this.labels.totalSpace - this.labels.padding;
		}
		
		//----------------------------------------------------------------
		//	Drawing zero line
		//----------------------------------------------------------------
		
		private var zeroLineBounds:Rectangle;
		
		public function drawZeroLine():void {
			if (!this.enabled || this.zeroLine == null || !this.scale.contains(0)) return;
			var pixValue:Number = this.scale.transform(0);
			if (this.zeroLine.type == StrokeType.GRADIENT)
				this.viewPort.setPerpendicularLineBounds(this.zeroLine.thickness,pixValue, this.zeroLineBounds);
			var g:Graphics = this.plot.getScrollableContainer().graphics;
			this.zeroLine.apply(g, this.zeroLineBounds);
			this.viewPort.drawPerpendicularLine(g, pixValue, this.zeroLine.dashed, this.zeroLine.dashLength, this.zeroLine.spaceLength);
			g.lineStyle();
		}
		
		//----------------------------------------------------------------
		//	Drawing tickmarks
		//----------------------------------------------------------------
		
		public function drawTickmarks():void {
			if (!this.enabled) return;
			if (this.minorTickmark != null && this.minorTickmark.stroke != null)
				this.drawMinorTickmarks();
			if (this.majorTickmark != null && this.majorTickmark.stroke != null)
				this.drawMajorTickmarks();
		}
		
		private function drawMajorTickmarks():void {
			var isGradientTickmarks:Boolean = this.majorTickmark.stroke.type == StrokeType.GRADIENT;
			var g:Graphics = this.outsideContainer.graphics;
			
			if (!isGradientTickmarks)
				this.majorTickmark.stroke.apply(g);
				
			var position:Number = this.getOffset(this.offset, 0);
			
			var cnt:int = this.scale.majorIntervalsCount;
			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				cnt--;
				valueOffset = .5;
			}
			
			for (var i:int = 0;i<=cnt;i++) {
				var value:Number = this.scale.getMajorIntervalValue(i) + valueOffset;
				if (value >= this.scale.minimum && value <= this.scale.maximum)
					this.majorTickmark.draw(g, isGradientTickmarks, this.scale.localTransform(value), position, this.viewPort);
			}
			
			if (!isGradientTickmarks)
				g.lineStyle();
		}
		
		private function drawMinorTickmarks():void {
			var isGradientTickmarks:Boolean = this.minorTickmark.stroke.type == StrokeType.GRADIENT;
			var g:Graphics = this.outsideContainer.graphics;
			
			if (!isGradientTickmarks)
				this.minorTickmark.stroke.apply(g);
				
			var position:Number = this.getOffset(this.offset, 0);

			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				valueOffset = .5;
			}
			
			var lastIndex:int = this.scale.majorIntervalsCount - 1;
			var chekMax:Boolean = false;
			if (this.scale.getMajorIntervalValue(lastIndex) < this.scale.maximum) {
				lastIndex++;
				chekMax = true;
			}
			for (var i:int = 0;i<=lastIndex;i++) {
				var majorValue:Number = this.scale.getMajorIntervalValue(i);
				for (var j:uint = 1;j<this.scale.minorIntervalsCount;j++) {
					if (this.checkCenterTickmarkLast(valueOffset, i, j)) 
						continue;
					var minorValue:Number = this.scale.getMinorIntervalValue(majorValue, j)+valueOffset;
					if (chekMax && this.notInAxisBounds(minorValue))
						break;
					this.minorTickmark.draw(g, isGradientTickmarks, this.scale.localTransform(minorValue), position, this.viewPort);
				}
			}
			
			if (!isGradientTickmarks)
				g.lineStyle();
		}
		
		//----------------------------------------------------------------
		//	Drawing grid lines
		//----------------------------------------------------------------
		
		private var gridBounds:Rectangle;
		
		public function drawGridLines():void {
			if (!this.enabled) return;
			var g:Graphics = this.plot.getScrollableContainer().graphics;
			
			if (this.minorGrid != null && this.minorGrid.line != null) {
				this.drawMinorGridLines(g);
			}
			
			if (this.majorGrid != null && this.majorGrid.line != null) {
				this.drawMajorGridLines(g);
			}
		}
		
		private function drawMajorGridLines(g:Graphics):void {
			var isGradientLines:Boolean = this.majorGrid.line.type == StrokeType.GRADIENT;
			
			if (!isGradientLines)
				this.majorGrid.line.apply(g);
				
			var cnt:int = this.scale.majorIntervalsCount;
			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				cnt--;
				valueOffset = .5;
			}
			
			for (var i:uint = 0;i<=cnt;i++) {
				var pixValue:Number = this.scale.localTransform(this.scale.getMajorIntervalValue(i)+valueOffset);
				if (isGradientLines) {
					this.viewPort.setPerpendicularLineBounds(this.majorGrid.line.thickness, pixValue, this.gridBounds);
					this.majorGrid.line.apply(g, this.gridBounds);
				}
				this.viewPort.drawPerpendicularLine(g, pixValue, this.majorGrid.line.dashed, this.majorGrid.line.dashLength, this.majorGrid.line.spaceLength);
				if (isGradientLines)
					g.lineStyle();
			}
			
			if (!isGradientLines)
				g.lineStyle();
		}
		
		private function drawMinorGridLines(g:Graphics):void {
			var isGradientLines:Boolean = this.minorGrid.line.type == StrokeType.GRADIENT;
			
			if (!isGradientLines)
				this.minorGrid.line.apply(g);
			
			var lastIndex:int = this.scale.majorIntervalsCount - 1;
			var chekMax:Boolean = false;
			if (this.scale.getMajorIntervalValue(lastIndex) < this.scale.maximum) {
				lastIndex++;
				chekMax = true;
			}
			
			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				valueOffset = .5;
			}
			
			for (var i:uint = 0;i<=lastIndex;i++) {
				var majorValue:Number = this.scale.getMajorIntervalValue(i);
				for (var j:uint = 1;j<this.scale.minorIntervalsCount;j++) {
					if (this.checkCenterTickmarkLast(valueOffset, i, j)) 
						continue;
					var minorValue:Number = this.scale.getMinorIntervalValue(majorValue, j) + valueOffset;
					if (chekMax && this.notInAxisBounds(minorValue))
						break;
						
					var pixValue:Number = this.scale.localTransform(minorValue);
					
					if (isGradientLines) {
						this.viewPort.setPerpendicularLineBounds(this.minorGrid.line.thickness, pixValue, this.gridBounds);
						this.minorGrid.line.apply(g, this.gridBounds);
					}
					this.viewPort.drawPerpendicularLine(g, pixValue, this.minorGrid.line.dashed, this.minorGrid.line.dashLength, this.minorGrid.line.spaceLength);
					if (isGradientLines)
						g.lineStyle();
				}
			}
			
			if (!isGradientLines)
				g.lineStyle();
		}
		
		private function checkCenterTickmarkLast(valueOffset:Number, i:int, j:int):Boolean {
			return valueOffset != 0 && (i + 1) == this.scale.majorIntervalsCount && j == this.scale.minorIntervalsCount;
		}
		
		private function notInAxisBounds(value:Number):Boolean {
			return !this.scale.linearizedContains(value);
		}
		
		public function drawGridInterlaces():void {
			if (!this.enabled) return;
			var drawEven:Boolean;
			var drawOdd:Boolean;
			
			var g:Graphics = this.plot.getScrollableContainer().graphics;
			
			if (this.minorGrid != null) {
				drawEven = this.minorGrid.evenFill != null || this.minorGrid.evenHatchFill != null;
				drawOdd = this.minorGrid.oddFill != null || this.minorGrid.oddHatchFill != null;
				if (drawEven || drawOdd)
					this.drawMinorGridInterlaces(g, drawEven, drawOdd); 
			}
			
			if (this.majorGrid != null) {
				drawEven = this.majorGrid.evenFill != null || this.majorGrid.evenHatchFill != null;
				drawOdd = this.majorGrid.oddFill != null || this.majorGrid.oddHatchFill != null;
				if (drawEven || drawOdd)
					this.drawMajorGridInterlaces(g, drawEven, drawOdd);
			}
									 
		}
		
		private function drawMajorGridInterlaces(g:Graphics, drawEven:Boolean, drawOdd:Boolean):void {
			var isMajorOdd:Boolean = false;
			
			var cnt:int = this.scale.majorIntervalsCount;
			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				cnt--;
				valueOffset = .5;
			}
			
			for (var i:uint = 0;i<cnt;i++) {
				if ((isMajorOdd && drawOdd) || (!isMajorOdd && drawEven)) {
					var start:Number = this.scale.getMajorIntervalValue(i)+valueOffset;
					var end:Number = this.scale.getMajorIntervalValue(i+1)+valueOffset;
					this.drawInterlace(g, start, end, this.majorGrid, isMajorOdd);
				}
				isMajorOdd = !isMajorOdd;
			}
		}
		
		private function drawMinorGridInterlaces(g:Graphics, drawEven:Boolean, drawOdd:Boolean):void {
			var isMinorOdd:Boolean = false;
			var valueOffset:Number = 0;
			if (this.isCenterTickmarks) {
				valueOffset = .5;
			}
			for (var i:uint = 0;i<this.scale.majorIntervalsCount;i++) {
				var majorValue:Number = this.scale.getMajorIntervalValue(i);
				for (var j:uint = 0;j<this.scale.minorIntervalsCount;j++) {
					if (this.checkCenterTickmarkLast(valueOffset, i, j)) 
						continue;
					if ((isMinorOdd && drawOdd) || (!isMinorOdd && drawEven)) {
						var start:Number = this.scale.getMinorIntervalValue(majorValue, j) + valueOffset;
						var end:Number = this.scale.getMinorIntervalValue(majorValue, j+1) + valueOffset;
						this.drawInterlace(g, start, end, this.minorGrid, isMinorOdd);
					} 
					
					isMinorOdd = !isMinorOdd;
				}
			}
		}
		
		private function drawInterlace(g:Graphics, start:Number, end:Number, grid:AxisGrid, isOdd:Boolean):void {
			start = this.scale.localTransform(start);
			end = this.scale.localTransform(end);
			
			this.viewPort.setPerpendicularRectangle(start, end, this.gridBounds);
			var fill:Fill;
			var hatchFill:HatchFill;
			if (isOdd) {
				fill = grid.oddFill;
				hatchFill = grid.oddHatchFill;
			}else {
				fill = grid.evenFill;
				hatchFill = grid.evenHatchFill;
			}
			if (fill != null) {
				fill.begin(g, this.gridBounds);
				this.viewPort.drawPerpendicularRectangle(g, this.gridBounds);
				g.endFill();
			}
			
			if (hatchFill != null) {
				hatchFill.beginFill(g);
				this.viewPort.drawPerpendicularRectangle(g, this.gridBounds);
				g.endFill();
			}
		}
		
		//----------------------------------------------------------------
		//	Deserialization
		//----------------------------------------------------------------
		
		public function onAfterDataDeserialize(plotDrawingPoints:Array):void {}
		
		public final function deserializeScale(data:XML):void {
			this.scale = this.createScale(data.scale[0]);
			if (data.scale[0] != null) 
				this.scale.deserialize(data.scale[0]);
		}
		
		public final function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			
			this.fixedSpace = 0;
			this.offset = 0;
			this.padding = 10;
			this.extraAdditionalOffset = this.padding;
			
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			
			if (!this.enabled)
				return;
				
			if (SerializerBase.isEnabled(data.major_grid[0])) {
				this.majorGrid = new AxisGrid();
				this.majorGrid.deserialize(data.major_grid[0], resources);
				this.setGridGradientsRotation(this.majorGrid);
			}
			if (SerializerBase.isEnabled(data.minor_grid[0])) {
				this.minorGrid = new AxisGrid();
				this.minorGrid.deserialize(data.minor_grid[0], resources);
			}
			
			this.tickmarksSpace = 0;
			this.insideTickmakrsSpace = 0;
			this.outsideTickmarkSpace = 0;
			if (SerializerBase.isEnabled(data.major_tickmark[0])) {
				this.majorTickmark = new AxisTickmark();
				this.majorTickmark.deserialize(data.major_tickmark[0]);
				if (this.majorTickmark.stroke != null) {
					if (this.majorTickmark.stroke.type == StrokeType.GRADIENT)
						this.viewPort.setGradientRotation(this.majorTickmark.stroke.gradient);
						
					if (this.majorTickmark.outside) {
						this.tickmarksSpace += this.majorTickmark.size;
						this.outsideTickmarkSpace = this.majorTickmark.size;
					}
					if (this.majorTickmark.inside && !this.isFirstInList) {
						this.tickmarksSpace += this.majorTickmark.size;
						this.insideTickmakrsSpace = this.majorTickmark.size;
					}
				}
			}
			if (SerializerBase.isEnabled(data.minor_tickmark[0])) {
				this.minorTickmark = new AxisTickmark();
				this.minorTickmark.deserialize(data.minor_tickmark[0]);
				if (this.minorTickmark.stroke != null) {
					if (this.minorTickmark.stroke.type == StrokeType.GRADIENT)
						this.viewPort.setGradientRotation(this.minorTickmark.stroke.gradient);
						
					var minorTickmarkSpace:Number = 0;
					if (this.minorTickmark.outside) {
						minorTickmarkSpace += this.minorTickmark.size;
						if (this.minorTickmark.size > outsideTickmarkSpace)
							outsideTickmarkSpace = this.minorTickmark.size;
					}
					if (this.minorTickmark.inside && !this.isFirstInList) {
						minorTickmarkSpace += this.minorTickmark.size;
						if (this.minorTickmark.size > insideTickmakrsSpace)
							insideTickmakrsSpace = this.minorTickmark.size;
					}
					
					if (minorTickmarkSpace > this.tickmarksSpace)
						this.tickmarksSpace = minorTickmarkSpace;
				}
			}

			if (SerializerBase.isEnabled(data.line[0])) {
				this.line = new Stroke();
				this.line.deserialize(data.line[0]);
				if (this.line.type == StrokeType.GRADIENT) { 
					this.axisLineBounds = new Rectangle();
					this.viewPort.setGradientRotation(this.line.gradient);
				}
			}
			
			if (SerializerBase.isEnabled(data.zero_line[0])) {
				this.zeroLine = new Stroke();
				this.zeroLine.deserialize(data.zero_line[0]);
				if (this.zeroLine.type == StrokeType.GRADIENT) {
					this.zeroLineBounds = new Rectangle();
					this.viewPort.setGradientRotation(this.zeroLine.gradient);
				}
			}
			
			this.fixedSpace = this.tickmarksSpace;
			
			this.extraAdditionalOffset += this.insideTickmakrsSpace;
			if (!this.isFirstInList) {
				this.fixedSpace += this.padding;
			}
				
			if (SerializerBase.isEnabled(data.title[0])) {
				this.title = new AxisTitle(this,this.fixedContainer, this.viewPort);
				this.title.deserialize(data.title[0], resources);
			}
			
			if (SerializerBase.isEnabled(data.labels[0])) {
				this.labels = this.createLabels(data.labels[0]);
				if (this.labels != null)
					this.labels.deserialize(data.labels[0], resources);
			}
			
			if (data.axis_markers[0] != null) {
				this.markers = new AxisMarkersList();
				this.markers.axis = this;
				this.markers.deserialize(data.axis_markers[0], styles, resources);
				this.markers.outsideLabelsContainerGraphics = this.outsideContainer.graphics;
			}
			
			if (this is BaseCategorizedAxis) {
				if (data.@tickmarks_placement != undefined)
					this.isCenterTickmarks = SerializerBase.getEnumItem(data.@tickmarks_placement) == "center";
			}
			
			this.space = this.fixedSpace;
			
			if (data.scale[0] != null && data.scale[0].@auto_calculation_mode != undefined &&
					SerializerBase.getLString(data.scale[0].@auto_calculation_mode) == "smart" &&
					this.scale is LinearScale && this.labels.numDecimals != -1) { 
				LinearScale(this.scale).enableSmartLabels(this.labels.numDecimals);
			}
		}
		
		private function setGridGradientsRotation(grid:AxisGrid):void {
			if (grid.evenFill != null && grid.evenFill.type == FillType.GRADIENT)
				this.viewPort.setGradientRotation(grid.evenFill.gradient);
			if (grid.oddFill != null && grid.oddFill.type == FillType.GRADIENT)
				this.viewPort.setGradientRotation(grid.oddFill.gradient);
			if (grid.line != null && grid.line.type == StrokeType.GRADIENT)
				this.viewPort.setGradientRotation(grid.line.gradient);
		}
		
		protected function createScale(scaleNode:XML):BaseScale {
			return null;
		}
		
		protected function createLabels(labelsNode:XML):AxisLabels {
			return null;
		}
		
		//----------------------------------------------------------------
		//	Markers
		//----------------------------------------------------------------
		
		public final function initMarkers():void {
			this.markersFixedSpace = 0;
			this.markersLabelsSpace = 0;
			this.markersBeforeSpace = 0;
			
			if (this.markers != null) {
				this.markers.initialize();
				this.markersBeforeSpace = this.markers.beforeAxisLabelsSpace;
				this.markersFixedSpace = this.markers.afterAxisLabelsSpace + this.markers.beforeAxisLabelsSpace;
				this.markersLabelsSpace = this.markers.axisLabelsSpace;
			}
		}
		
		public final function getMarkersAxisSpace():Number {
			var offset:Number = this.isFirstInList ? (this.offset + this.tickmarksSpace) : (this.offset + this.outsideTickmarkSpace);
			offset += this.elementsSpace;
			offset += this.markersBeforeSpace;
			offset += this.scrollBarSpace;
			return offset;
		}
		
		public final function getMarkersBeforeLabelsSpace():Number {
			var offset:Number = this.isFirstInList ? (this.offset + this.tickmarksSpace) : (this.offset + this.outsideTickmarkSpace);
			offset += this.scrollBarSpace;
			offset += this.elementsSpace;
			return offset;
		}
		
		public final function getMarkersAfterLabelsSpace():Number {
			var offset:Number = this.isFirstInList ? (this.offset + this.tickmarksSpace) : (this.offset + this.outsideTickmarkSpace);
			if (this.labels != null)
				offset += Math.max(this.markersLabelsSpace, this.getOutsideLabelsSpace());
			else
				offset += this.markersLabelsSpace;
			offset += this.markersBeforeSpace;
			offset += this.elementsSpace;
			offset += this.scrollBarSpace;
			return offset;
		}
		
		public function get hasData():Boolean {
			return !isNaN(this.scale.dataRangeMinimum) && !isNaN(this.scale.dataRangeMaximum); 
		}
		
		public function copyScale(primaryAxis:Axis):void {
			this.scale.dataRangeMinimum = primaryAxis.scale.dataRangeMinimum;
			this.scale.dataRangeMaximum = primaryAxis.scale.dataRangeMaximum;
		}
		
		//----------------------------------------------------------------
		//	Transformation
		//----------------------------------------------------------------
		
		public function recalculateDataRange():void {
			this.scale.dataRangeMaximum = this.scale.dataRangeMinimum = NaN;
			for (var i:int = 0;i<this.points.length;i++) {
				AxesPlotPoint(this.points[i]).checkScaleValue();
			}
		}
		
		public final function checkScaleValue(value:Number):void {
			if (isNaN(this.scale.dataRangeMaximum) || value > this.scale.dataRangeMaximum)
				this.scale.dataRangeMaximum = value;
			if (isNaN(this.scale.dataRangeMinimum) || value < this.scale.dataRangeMinimum)
				this.scale.dataRangeMinimum = value;
		}
		
		public function checkSeries(series:AxesPlotSeries):void {
		}
		
		public function transform(dataPoint:AxesPlotPoint, value:Number, point:Point, pixOffset:Number = 0):void {
			var pixValue:Number = this.scale.transform(value) + pixOffset;
			this.viewPort.setScaleValue(pixValue, point);
		}
		
		public function backTransform(pix:Point):Number {
			return this.scale.fromPix(this.viewPort.getScaleValue(pix));
		}
		
		public function simpleTransform(dataPoint:AxesPlotPoint, value:Number):Number {
			return this.scale.transform(value);
		}
		
		public final function rotateValue(pixValue:Number, point:Point):void {
			this.viewPort.setScaleValue(pixValue, point);
		}
		
		public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return token == '%Value' ? scaleValue : '';
		}
		
		//----------------------------------------------------------------
		//	Formatting
		//----------------------------------------------------------------
		
		public var cashedTokens:Object;
		
		public function getAxisTokenValue(token:String):* {
			if (token == "%AxisName")
				return (this.title != null && this.title.info != null) ? this.title.info.formattedText : this.name;
			if (token == "%AxisSum") return this.cashedTokens["Sum"];
			if (token == "%AxisMax") return this.cashedTokens["Max"];
			if (token == "%AxisMin") return this.cashedTokens["Min"];
			if (token == "%AxisRange") return (this.cashedTokens["Max"] - this.cashedTokens["Min"]);
			if (token == "%AxisScaleMax") return this.scale.maximum;
			if (token == "%AxisScaleMin") return this.scale.minimum;
			if (token == "%AxisAverage") return this.cashedTokens["Sum"]/this.points.length;
			if (token == "%AxisBubbleSizeSum") return this.cashedTokens["BubbleSum"];
			if (token == "%AxisBubbleSizeMax") return this.cashedTokens["BubbleMax"];
			if (token == "%AxisBubbleSizeMin") return this.cashedTokens["BubbleMin"];
			if (token == "%AxisMedian") {
				if (this.cashedTokens["Median"] != null)
					return this.cashedTokens["Median"];
				this.cashedTokens["Median"] = StatisticUtils.getMedian(this.points, this.isArgument ? "%XValue" : "%Value");
				return this.cashedTokens["Median"];
			}
			if (token == "%AxisMode") {
				if (this.cashedTokens["Mode"] != null)
					return this.cashedTokens["Mode"];
				this.cashedTokens["Mode"] = StatisticUtils.getMode(this.points, this.isArgument ? "%XValue" : "%Value");
				return this.cashedTokens["Mode"];
			}
			return "";
		}
		
		public function resetTokens():void {
			this.cashedTokens["Sum"] = 0;
			this.cashedTokens["Max"] = -Number.MAX_VALUE;
			this.cashedTokens["Min"] = Number.MAX_VALUE;
			this.cashedTokens["BubbleSum"] = 0;
			this.cashedTokens["BubbleMax"] = -Number.MAX_VALUE;
			this.cashedTokens["BubbleMin"] = Number.MAX_VALUE;
			this.cashedTokens["AxisMedian"] = null;
			this.cashedTokens["AxisMode"] = null;
		}
		
		public function getTokenValue(token:String):* {
			if (token.indexOf('%DataPlot') == 0)
				return this.plot.getTokenValue(token);
			return this.getAxisTokenValue(token);
		} 
		
		public function isDateTimeToken(token:String):Boolean {
			if (token.indexOf('%DataPlot') == 0)
				return false;
			if (!this.isDateTimeAxis()) return false;
			return (token == "%Value");
		}
		
		public function isDateTimeAxis():Boolean {
			return this.scale is DateTimeScale || this.scale is TextDateTimeScale;
		}
		
		public function isCategorizedDateTimeAxis():Boolean {
			return this.scale is TextDateTimeScale;
		}
		
		//----------------------------------------------------------------
		//	Point elements
		//----------------------------------------------------------------
		
		private var elementsSpace:Number;
		private var elements:Array;
		
		public final function initElementsBounds():void {
			this.elementsSpace = 0;
			if (this.elements == null) return;
			for each (var elementInfo:Array in this.elements) {
				var element:BaseElement = elementInfo[0];
				var container:IElementContainer = elementInfo[1];
				var elementIndex:uint = elementInfo[2];
				this.elementsSpace = Math.max(this.elementsSpace,
											  this.viewPort.getSpace(element.getBounds(container, element.style.normal, elementIndex))+BaseElementStyleState(element.style.normal).padding);
			}
		}
		
		public final function addPointAxisElement(container:IElementContainer, element:BaseElement, elementIndex:uint):void {
			if (this.elements == null)
				this.elements = [];
			this.elements.push([element, container, elementIndex]);
		}
		
		public final function setElementPosition(dataPoint:AxesPlotPoint, bounds:Rectangle, padding:Number, pos:Point):void {
			this.transform(dataPoint, dataPoint.x, pos);
			this.viewPort.setOffset(padding, bounds, pos);
		}
		
		public final function serialize(res:Object = null):Object {
			if (!res) res = {};
			res.Key = this.name;
			res.Name = this.getAxisTokenValue("%AxisName");
			res.Sum = this.cashedTokens["Sum"];
			res.Max = this.cashedTokens["Max"];
			res.Min = this.cashedTokens["Min"];
			res.BubbleSizeMax = this.cashedTokens["BubbleMax"];
			res.BubbleSizeMin = this.cashedTokens["BubbleMin"];
			res.BubbleSizeSum = this.cashedTokens["BubbleSum"];
			res.Average = this.getAxisTokenValue("%AxisAverage");
			res.Median = this.getAxisTokenValue("%AxisMedian");
			res.Mode = this.getAxisTokenValue("%AxisMode");
			return res;
		}
	}
}