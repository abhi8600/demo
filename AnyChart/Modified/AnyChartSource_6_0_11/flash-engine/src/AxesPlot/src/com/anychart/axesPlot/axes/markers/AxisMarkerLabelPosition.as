package com.anychart.axesPlot.axes.markers {
	
	internal final class AxisMarkerLabelPosition {
		public static const BEFORE_AXIS_LABELS:uint = 0;
		public static const AFTER_AXIS_LABELS:uint = 1;
		public static const AXIS:uint = 2;
		public static const NEAR:uint = 3;
		public static const CENTER:uint = 4;
		public static const FAR:uint = 5;
	}
}