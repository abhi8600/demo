package com.anychart.axesPlot.axes.text {
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.formatters.IFormatable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTitle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public final class AxisTitle extends BaseTitle {
		public var space:Number;
		
		public var info:TextElementInformation;
		private var container:Sprite;
		private var pos:Point;
		
		public var offset:Number;
		
		private var viewPort:IAxisViewPort;
		private var axis:IFormatable;
		
		public function AxisTitle(axis:IFormatable, titlesContainer:DisplayObjectContainer, viewPort:IAxisViewPort) {
			super();
			this.axis = axis;
			this.space = 0;
			this.offset = 0;
			this.pos = new Point(); 
			this.viewPort = viewPort;
			this.viewPort.setTitleRotation(this);
			this.container = new Sprite();
			titlesContainer.addChild(this.container);
		}
		
		public function initialize():void {
			var txt:String = (this.isDynamicText) ? this.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : 
												  	this.text;
			this.info = this.createInformation();
			this.info.formattedText = this.isDynamicText ? this.dynamicText.getValue(this.axis.getTokenValue, this.axis.isDateTimeToken) : this.text;
			this.getBounds(this.info);
			this.space = this.viewPort.getTextSpace(this.info.rotatedBounds) + this.padding;
		}
		
		public function drawAxisTitle(viewPort:IAxisViewPort):void {
			viewPort.setTitlePosition(this, this.pos);
			this.container.graphics.clear();
			this.draw(this.container.graphics, this.pos.x, this.pos.y, this.info, 0,0);
		}
		
		private function getTokenValue(token:String):* {
			return "";
		}
		
		private function isDateTimeToken(token:String):Boolean { return false; }
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			var rotation:Number = this.rotation;
			this.rotation = 0;
			super.deserialize(data, resources);
			this.rotation = -rotation - this.rotation;
		}
	}
}