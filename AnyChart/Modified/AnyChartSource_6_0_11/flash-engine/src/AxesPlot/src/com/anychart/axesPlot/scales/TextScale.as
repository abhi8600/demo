package com.anychart.axesPlot.scales {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.serialization.SerializerBase;
	
	public class TextScale extends ValueScale {
		
		public function TextScale(axis:Axis) {
			super(axis);
		}
		
		override public function calculate():void {
			
			this.minimum = this.dataRangeMinimum;
			this.maximum = this.dataRangeMaximum;
			
			if (!this.adjustCategorizedMargins) {
				this.minimum -= 0.5;
				this.maximum += 0.5;
			}
			
			this.minorInterval = 1;
			this.baseValue = this.minimum;
			
			var isLabelsCalculated:Boolean = false;
			
			if (this.isMajorIntervalAuto) {
				if (this.axis.labels != null && !this.axis.labels.allowOverlap) {
					this.majorInterval = 1;
					this.calculateMajorTicksCount();
					var maxLabels:int = this.axis.labels.calcMaxLabelsCount();
					this.majorInterval = Math.ceil((this.maximum - this.minimum)/maxLabels);
					isLabelsCalculated = true;
				}else {
					this.majorInterval = int((this.maximum - this.minimum - 1)/12) + 1;
				}
			}else {
				this.majorInterval = int(this.majorInterval);
				if (this.majorInterval <= 0)
					this.majorInterval = 1;
			}
			
			if (this.isMinorIntervalAuto)
				this.minorInterval = this.majorInterval;
			
			if (!isLabelsCalculated && this.axis.labels != null)
				this.axis.labels.initMaxLabelSize();
			
			this.calculateTicksCount();
			this.calculateMinorStart();
		}
		
		private var adjustCategorizedMargins:Boolean = false;
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@adjust_categorized_margins != undefined) {
				this.adjustCategorizedMargins = SerializerBase.getBoolean(data.@adjust_categorized_margins);
			}
		}
		
		override public function deserializeValue(value:*):* {
			return SerializerBase.getString(value);
		}
	}
}