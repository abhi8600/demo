package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class RightOppositeAxisViewPort3D extends RightAxisViewPort3D {
		
		override protected function get xOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixXAspect(); }
		override protected function get yOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixYAspect(); }
		
		public function RightOppositeAxisViewPort3D(axis:Axis) {
			super(axis);
		}

		override public function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {
			var y:Number = this.axis.scale.pixelRangeMaximum - axisValue;
			
			var frontLeftX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(0);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(y);
			
			if (!dashed) {
				g.moveTo(frontLeftX,frontY);
				g.lineTo(0, y);
				g.lineTo(this.bounds.width, y);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, frontLeftX, frontY, 0, y);
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, 0, y, this.bounds.width, y);
			}
		}
		
		override public function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void {
				
			var frontLeftX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(bounds.x);
			var frontLeftTopY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(bounds.top);
			var frontLeftBottomY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(bounds.bottom);
			
			g.moveTo(frontLeftX, frontLeftTopY);
			g.lineTo(bounds.x, bounds.y);
			g.lineTo(bounds.right, bounds.y);
			g.lineTo(bounds.right, bounds.bottom);
			g.lineTo(bounds.x, bounds.bottom);
			g.lineTo(frontLeftX, frontLeftBottomY);
			g.lineTo(frontLeftX, frontLeftTopY);
		}
		
		override public function drawMarkerLine(g:Graphics, startValue:Number, endValue:Number, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean = true, invertedDrawing:Boolean = false):void {
			
			var y:Number = this.axis.scale.pixelRangeMaximum - endValue;
			var frontLeftX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(0);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(y);
			
			var startPt:Point = new Point();
			var endPt:Point = new Point();
			
			this.setStartValue(startValue, startPt);
			this.setEndValue(endValue, endPt);
			
			var pt1:Point = startPt;
			var pt2:Point = endPt;
			var pt3:Point = new Point(frontLeftX, frontY);
			this.drawComplexLine(g, pt1, pt2, pt3, isDashed, dashOn, dashOff, moveTo, invertedDrawing);
		}
		
		override protected function offsetInsideMarkerLabel(pos:Point):void {}
		
		override public function applyLabelsOffsets(bounds:Rectangle, firstLabel:Rectangle, lastLabel:Rectangle):void {
			
			firstLabel.y = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(firstLabel.y);
			lastLabel.y = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(lastLabel.y);
			super.applyLabelsOffsets(bounds, firstLabel, lastLabel);
		}
	}
}