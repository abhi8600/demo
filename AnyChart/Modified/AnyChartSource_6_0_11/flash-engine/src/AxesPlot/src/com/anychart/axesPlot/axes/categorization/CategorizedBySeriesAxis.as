package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.scales.ScaleMode;
	
	import flash.geom.Point;
	
	public class CategorizedBySeriesAxis extends BaseCategorizedAxis {
		
		override public function checkSeries(series:AxesPlotSeries):void {
			series.category = this.createCategory(series.name);
			series.category.clusterSettings = new ClusterSettings();
			series.category.clusterSettings.groupPadding = series.global.groupPadding;
			series.category.clusterSettings.pointPadding = series.global.pointPadding;
			if (series.valueAxis is ValueAxis && series.valueAxis.scale.mode != ScaleMode.NORMAL) {
				series.isSingleCluster = true;
				series.category.clusterSettings.numClusters = 1;
			}
			this.clusterSettingsList.push(series.category.clusterSettings);
		}
		
		override public function transform(dataPoint:AxesPlotPoint, value:Number, point:Point, pixOffset:Number = 0):void {
			var pixValue:Number = this.scale.transform(value) + pixOffset;
			pixValue += dataPoint.category.clusterSettings.getOffset(dataPoint.clusterIndex);
			this.viewPort.setScaleValue(pixValue, point);
		}
		
		override public function simpleTransform(dataPoint:AxesPlotPoint, value:Number):Number {
			var pixValue:Number = this.scale.transform(value);
			pixValue += dataPoint.category.clusterSettings.getOffset(dataPoint.clusterIndex);
			return pixValue;
		}
		
		override protected function registerDataCategory(category:Category):void { }
	}
}