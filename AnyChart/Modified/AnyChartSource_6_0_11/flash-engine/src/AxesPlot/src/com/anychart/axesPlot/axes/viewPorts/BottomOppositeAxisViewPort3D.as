package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	internal final class BottomOppositeAxisViewPort3D extends BottomAxisViewPort3D {
		
		override protected function get xOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixXAspect(); }
		override protected function get yOffset():Number { return AxesPlot3D(this.axis.plot).space3D.getPixYAspect(); }
		
		public function BottomOppositeAxisViewPort3D(axis:Axis) {
			super(axis);
		}
		
		override public function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {
			
			var frontX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(axisValue);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(this.bounds.top);
			
			if (!dashed) {
				g.moveTo(frontX, frontY);
				g.lineTo(axisValue, 0);
				g.lineTo(axisValue, this.bounds.height);
			}else {
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, frontX, frontY, axisValue, 0);
				DrawingUtils.drawDashedLine(g, dashOff, dashOn, axisValue, 0, axisValue, this.bounds.height);
			}
		}
		
		override public function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void {
				
			var frontLeftX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(bounds.left);
			var frontRightX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(bounds.right);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(bounds.top);
			
			g.moveTo(frontLeftX, frontY);
			g.lineTo(frontRightX, frontY);
			g.lineTo(bounds.right, bounds.top);
			g.lineTo(bounds.right, bounds.bottom);
			g.lineTo(bounds.left, bounds.bottom);
			g.lineTo(bounds.left, bounds.top);
			g.lineTo(frontLeftX, frontY);
		}
		
		override public function drawMarkerLine(g:Graphics, startValue:Number, endValue:Number, isDashed:Boolean, dashOn:Number, dashOff:Number, moveTo:Boolean = true, invertedDrawing:Boolean = false):void {
			
			var x:Number = endValue;
			var frontX:Number = AxesPlot3D(this.axis.plot).offsetXToPlotFrontProjection(x);
			var frontY:Number = AxesPlot3D(this.axis.plot).offsetYToPlotFrontProjection(this.bounds.top);
			
			var startPt:Point = new Point();
			var endPt:Point = new Point();
			
			this.setStartValue(startValue, startPt);
			this.setEndValue(endValue, endPt);
			
			var pt1:Point = startPt;
			var pt2:Point = endPt;
			var pt3:Point = new Point(frontX, frontY);
			this.drawComplexLine(g, pt1, pt2, pt3, isDashed, dashOn, dashOff, moveTo, invertedDrawing);
		}
		
		override protected function offsetInsideMarkerLabel(pos:Point):void {}
	}
}