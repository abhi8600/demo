package com.anychart.axesPlot {
	import com.anychart.IAnyChart;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.CategoryInfo;
	import com.anychart.locale.DateTimeLocale;
	import com.anychart.plot.IPlot;
	
	import flash.display.Sprite;
	
	public interface IAxesPlot extends IPlot {
		function get xCategories():Array;
		function get yCategories():Array;
		
		function get dataCategories():Array;
		function getDataCategory(axis:Axis, name:String):CategoryInfo;
		
		function get dateTimeLocale():DateTimeLocale;
		function get root():IAnyChart;
		function getAfterDataContainer():Sprite;
		function getBeforeDataContainer():Sprite;
		function getScrollableContainer():Sprite;
		
		function moveDataView(startXValue:Number, startYValue:Number, callFromScrollBar:Boolean = false):void;
		
		function get isExternalCall_ScrollBarChangeX():Boolean;
		function set isExternalCall_ScrollBarChangeX(newValue:Boolean):void;
		
		function get isExternalCall_ScrollBarChangeY():Boolean;
		function set isExternalCall_ScrollBarChangeY(newValue:Boolean):void;
		
		function dispatchScrollerEvent(type:String, source:String, phase:String, startValue:Number, endValue:Number, valueRange:Number):void;
   			
	}
}