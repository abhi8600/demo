package com.anychart.axesPlot.scales.dateTime {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.scales.ValueScale;
	import com.anychart.serialization.SerializerBase;
	
	public final class DateTimeScale extends ValueScale {
		internal var majorIntervalUnit:uint;
		internal var minorIntervalUnit:uint;
		
		public function DateTimeScale(axis:Axis) {
			super(axis);
		}
		
		override public function calculate():void {
			super.calculate();
			
			var isLabelsCalculated:Boolean = false;
			
			if (this.isMajorIntervalAuto) {
				this.majorInterval = DateTimeRange.calculateIntervals(this, 7);
				if (this.axis.labels != null && !this.axis.labels.allowOverlap) {
					this.calculateBaseValue();
					this.calculateMajorTicksCount();
					var maxLabels:int = this.axis.labels.calcMaxLabelsCount();
					if (maxLabels <= (this.maximum - this.minimum)/this.majorInterval) {
						this.majorInterval =  DateTimeRange.calculateIntervals(this, maxLabels);
					}
					isLabelsCalculated = true;
				}
			}
			
			if (this.isMaximumAuto)
				this.maximum = this.getEvenStepDate(this.maximum, true);

			if (this.isMinimumAuto)
				this.minimum = this.getEvenStepDate(this.minimum, false);

			this.calculateBaseValue();
			this.calculateTicksCount();
			this.calculateMinorStart();
			
			if (!isLabelsCalculated && this.axis.labels != null)
	    		this.axis.labels.initMaxLabelSize();
		}
		
		private function getEvenStepDate(date:Number, increase:Boolean):Number {
			return DateTimeUnit.getEvenStepDate(this.majorIntervalUnit, date, increase);
		}
		
		internal function calcStepSize(range:Number, steps:int):Number {
			return this.calculateOptimalStepSize(range, steps, false);
		}
		
		override public function getMajorIntervalValue(index:Number):Number {
			return DateTimeUnit.increaseDate(this.baseValue, this.majorIntervalUnit, index * this.majorInterval);
		}
		
		override public function getMinorIntervalValue(baseValue:Number, index:int):Number {
			return DateTimeUnit.increaseDate(baseValue, this.minorIntervalUnit, index * this.minorInterval);
		}
		
		override protected function calculateMinorStart():void {
			
			var tmp:Number = this.minimum - this.baseValue;
			
			switch (this.minorIntervalUnit) {
				case DateTimeUnit.YEAR:
				default: 
					this._minorStart = tmp/(365*this.minorInterval);
					break;
					
				case DateTimeUnit.MONTH:
					this._minorStart = tmp/(28*this.minorInterval);
					
				case DateTimeUnit.DAY:
					this._minorStart = tmp/this.minorInterval;
					
				case DateTimeUnit.HOUR:
					this._minorStart = tmp*24/this.minorInterval;
					
				case DateTimeUnit.MINUTE:
					this._minorStart = tmp*(24*60)/this.minorInterval;
					
				case DateTimeUnit.SECOND:
					this._minorStart = tmp*(24*60*60)/this.minorInterval;
			}
		}
		
		override protected function calculateBaseValue():void {
			
			if (!this.isBaseValueAuto) return;
			
			var minDate:Date = new Date(this.minimum);
			var date:Date;
			
			switch (this.majorIntervalUnit) {
				case DateTimeUnit.YEAR:
				default:
					date = new Date(Date.UTC(minDate.getUTCFullYear(), 0));
					break;
					
				case DateTimeUnit.MONTH:
					date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth()));
					break;
				
				case DateTimeUnit.DAY:
					date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate()));
					break;
					
				case DateTimeUnit.HOUR:
					date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate(), minDate.getUTCHours()));
					break;
					
				case DateTimeUnit.MINUTE:
					date = new Date(Date.UTC(minDate.getUTCFullYear(), minDate.getUTCMonth(), minDate.getUTCDate(), minDate.getUTCHours(), minDate.getUTCMinutes()));
					break;
					
				case DateTimeUnit.SECOND:
					date = minDate;
					break;
			}
			
			if (date.getTime() < minDate.getTime())
				this.baseValue = DateTimeUnit.increaseDate(date.getTime(), this.majorIntervalUnit, 1);
			else
				this.baseValue = date.getTime();
		}
		
		override protected function calculateTicksCount():void {
			this.calculateMajorTicksCount();
			
			this._minorIntervalsCount = 0;
			var base:Number = this.getMajorIntervalValue(0);
			var end:Number = this.getMajorIntervalValue(1);
			var temp:Number = this.getMinorIntervalValue(base, 0);
			while (temp < end) {
				this._minorIntervalsCount++;
				temp = this.getMinorIntervalValue(base, this._minorIntervalsCount);
			}
			this._minorIntervalsCount = Math.max(0, this._minorIntervalsCount);
		}
		
		override protected function calculateMajorTicksCount():void {
			var minDate:Date = new Date(this.minimum);
			var maxDate:Date = new Date(this.maximum);
			
			var tmp:Number = this.majorInterval;
			var ticks:Number = 1;
			
			var dYear:Number = maxDate.getUTCFullYear() - minDate.getUTCFullYear();
			
			switch (this.majorIntervalUnit) {
				case DateTimeUnit.YEAR:
				default:
					ticks = dYear/tmp;
					break;
					
				case DateTimeUnit.MONTH:
					ticks =  (maxDate.getUTCMonth() - minDate.getUTCMonth() + 12*dYear)/tmp; 
					break;
					
				case DateTimeUnit.DAY:
					ticks = (this.maximum - this.minimum)/(tmp*DateTimeUnit.MILISECONDS_PER_DAY);
					break;
					
				case DateTimeUnit.HOUR:
					ticks = (this.maximum - this.minimum)/(tmp*DateTimeUnit.MILISECONDS_PER_HOUR);
					break;
					
				case DateTimeUnit.MINUTE:
					ticks = (this.maximum - this.minimum)/(tmp*DateTimeUnit.MILISECONDS_PER_MINUTE);
					break;
					
				case DateTimeUnit.SECOND:
					ticks = (this.maximum - this.minimum)/(tmp*DateTimeUnit.MILISECONDS_PER_SECOND);
					break;
			}
			
			if (ticks < 1)
				ticks = 1;
				
			this._majorIntervalsCount = int(ticks);
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data == null) return;
			var unit:int;
			
			if (data.@major_interval_unit != undefined) {
				unit = this.deserializeUnit(data.@major_interval_unit);
				if (unit != -1)
					this.majorIntervalUnit = unit;
			}
			
			if (data.@minor_interval_unit != undefined) {
				unit = this.deserializeUnit(data.@minor_interval_unit);
				if (unit != -1)
					this.minorIntervalUnit = unit;
			}
		}
		
		private function deserializeUnit(value:*):int {
			switch (SerializerBase.getEnumItem(value)) {
				case "year": return DateTimeUnit.YEAR;
				case "month": return DateTimeUnit.MONTH;
				case "day": return DateTimeUnit.DAY;
				case "hour": return DateTimeUnit.HOUR;
				case "minute": return DateTimeUnit.MINUTE;
				case "second": return DateTimeUnit.SECOND;
			}
			return -1;
		}
		
		override public function deserializeValue(value:*):* {
			if (String(value) == "") return NaN;
			return SerializerBase.getDateTime(value, AxesPlot(this.axis.plot).dateTimeLocale);
		}
	}
}