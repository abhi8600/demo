package com.anychart.mapPlot.controls.navigationPanel {
	import com.anychart.mapPlot.controls.MapControlButton;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	
	internal final class CenterNavigationButton extends MapControlButton {
		
		override protected function drawPointer():void {
			var g:Graphics = this.graphics;
			
			var opacity:Number=1;
			
			var rc:Rectangle = new Rectangle(this.bounds.width/2-(this.bounds.width/2)/2,this.bounds.height/2-(this.bounds.height/2)/2,this.bounds.width/2,this.bounds.height/2)
			var rcEl:Rectangle = new Rectangle((rc.left+rc.width/2)-(rc.width/2)/2.5,(rc.top+rc.height/2)-(rc.height/2)/2.5,rc.width/2.5,rc.height/2.5);
			g.beginFill(0,0);
			g.lineStyle(1,0,opacity,false);
			g.drawEllipse(rcEl.left,rcEl.top,rcEl.width,rcEl.height);
			g.lineStyle(0,0,0);
			g.endFill();
			
			// Left top.
			g.lineStyle(1,0,opacity,false);
			g.beginFill(0,0);
			g.moveTo(rc.left,rc.top+3);
			g.lineTo(rc.left,rc.top);
			g.lineTo(rc.left+3,rc.top);
			g.lineStyle(0,0,0);
			g.endFill();
			
			
			// Right top.
			g.lineStyle(1,0,opacity,false);
			g.beginFill(0,0);
			g.moveTo(rc.left+rc.width-3,rc.top);
			g.lineTo(rc.left+rc.width,rc.top);
			g.lineTo(rc.left+rc.width,rc.top+3);
			g.lineStyle(0,0,0);
			g.endFill();
			
			// Left bottom.
			g.lineStyle(1,0,opacity,false);
			g.beginFill(0,0);
			g.moveTo(rc.left,rc.top+rc.height-3);
			g.lineTo(rc.left,rc.top+rc.height);
			g.lineTo(rc.left+3,rc.top+rc.height);
			g.lineStyle(0,0,0);
			g.endFill();
			
			// Right bottom.
			g.lineStyle(1,0,opacity,false);
			g.beginFill(0,0);
			g.moveTo(rc.left+rc.width,rc.top+rc.height-3);
			g.lineTo(rc.left+rc.width,rc.top+rc.height);
			g.lineTo(rc.left+rc.width-3,rc.top+rc.height);
			g.lineStyle(0,0,0);
			g.endFill();
			
			
			g.lineStyle();

		}
	}
}