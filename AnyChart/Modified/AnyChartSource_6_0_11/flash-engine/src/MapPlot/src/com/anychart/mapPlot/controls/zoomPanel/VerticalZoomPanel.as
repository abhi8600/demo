package com.anychart.mapPlot.controls.zoomPanel {
	public final class VerticalZoomPanel extends ZoomPanel	{
		public static function check(data:XML):Boolean {
			return String(data.name()) == "zoom_panel" && !HorizontalZoomPanel.check(data);
		}
		
		public function VerticalZoomPanel() {
			super();
			this.layout.width = 40;
			this.layout.height = 190;
			this.slider = new VerticalSlider();
		}
		
		override protected function initializeLayout():void {
			super.initializeLayout();
			var totalW:Number = this.contentBounds.width;
			var btnW:Number = this.getButtonSize();
			this.zoomInBtn.x = (totalW - btnW)/2;
			this.zoomOutBtn.x = (totalW - btnW)/2;
			this.zoomInBtn.y = this.reversed ? (this.contentBounds.bottom - this.zoomInBtn.bounds.height - 10) : (this.contentBounds.top + 10);
			this.zoomOutBtn.y = this.reversed ? (this.contentBounds.top + 10) : (this.contentBounds.bottom - this.zoomOutBtn.bounds.height - 10);
			
			var w:Number = this.contentBounds.height - 20 - this.zoomInBtn.bounds.height - this.zoomOutBtn.bounds.height;
			this.slider.initialize(this.getButtonSize(), w-12);
			
			this.slider.x = (totalW - btnW)/2 - .5;
			this.slider.y = this.contentBounds.top + 16 +
								((this.reversed) ? (this.zoomOutBtn.bounds.width) : 
								  				   (this.zoomInBtn.bounds.width));
		}
	}
}