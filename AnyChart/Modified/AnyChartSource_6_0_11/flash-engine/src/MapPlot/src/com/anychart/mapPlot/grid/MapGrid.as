package com.anychart.mapPlot.grid {
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.mapPlot.grid.meridians.MeridiansAxis;
	import com.anychart.mapPlot.grid.parallels.ParallelsAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public final class MapGrid implements ISerializableRes {
		
		internal var space:GeoSpace;
		
		private var background:BackgroundBase;
		private var parallels:Axis;
		private var meridians:Axis;
		
		public var backgroundShape:Shape;
		public var beforeMapSprite:Sprite;
		public var underMapSprite:Sprite;
		public var zeroLinesContainer:Shape;
		
		internal var beforeMapLabels:Shape;
		internal var underMapLabels:Shape;
		
		public function MapGrid(space:GeoSpace) {
			this.space = space;
			this.backgroundShape = new Shape();
			this.beforeMapSprite = new Sprite();
			this.underMapSprite = new Sprite();
			
			this.beforeMapLabels = new Shape();
			this.beforeMapSprite.addChild(this.beforeMapLabels);
			
			this.underMapLabels = new Shape();
			this.underMapSprite.addChild(this.underMapLabels);
			
			this.beforeMapSprite.mouseEnabled = false;
			this.underMapSprite.mouseEnabled = false;
			
			this.zeroLinesContainer = new Shape();
			
			this.parallels = new ParallelsAxis();
			this.meridians = new MeridiansAxis();
		}
		
		public function get hasLabels():Boolean {
			return (this.parallels.enabled && this.parallels.labels != null &&
					this.meridians.enabled && this.meridians.labels != null);
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new BackgroundBase();
				this.background.deserialize(data.background[0], resources);
			}
			
			if (SerializerBase.isEnabled(data.parallels[0]))
				this.parallels.deserialize(data.parallels[0], resources);
			else
				this.parallels.enabled = false;
			
			if (SerializerBase.isEnabled(data.meridians[0]))
				this.meridians.deserialize(data.meridians[0], resources);
			else
				this.meridians.enabled = false;
		}
		
		public function initialize():void {
			this.parallels.initialize(this);
			this.meridians.initialize(this);
			
			if (this.parallels.beforeMap || this.meridians.beforeMap) {
				this.beforeMapSprite.addChild(this.zeroLinesContainer);
			}else {
				this.underMapSprite.addChild(this.zeroLinesContainer);
			}
		}
		
		public function setMargin(margin:Margin):void {
			this.parallels.calculateSpace();
			this.meridians.calculateSpace();
			margin.left += Math.max(this.parallels.nearSpace,this.meridians.startSpace);
			margin.right += Math.max(this.parallels.farSpace,this.meridians.endSpace);
			margin.top += Math.max(this.meridians.nearSpace,this.parallels.startSpace);
			margin.bottom += Math.max(this.meridians.farSpace,this.parallels.endSpace);
		}
		
		public function draw():void {
			if (this.background != null)
				this.drawBackground();
			if (this.parallels.enabled)
				this.parallels.draw();
			if (this.meridians.enabled)
				this.meridians.draw();
		}
		
		public function resize():void {
			this.beforeMapSprite.graphics.clear();
			this.underMapSprite.graphics.clear();
			this.backgroundShape.graphics.clear();
			this.beforeMapLabels.graphics.clear();
			this.underMapLabels.graphics.clear();
			this.zeroLinesContainer.graphics.clear();
			this.draw();
		}
		
		private function drawBackground():void {
			var bg:BackgroundBase = this.background;
			var g:Graphics = this.backgroundShape.graphics;
			
			var bounds:Rectangle = this.space.mapPixBounds;
			
			if (bg.border != null)
				bg.border.apply(g, bounds);
			if (bg.fill != null)
				bg.fill.begin(g, bounds);
			
			this.parallels.drawLine(g, this.space.geoBounds.top, true);
			this.meridians.drawLine(g, this.space.geoBounds.right, false);
			this.parallels.drawLine(g, this.space.geoBounds.bottom, false, true);
			this.meridians.drawLine(g, this.space.geoBounds.left, false, true);
			
			g.endFill();
			g.lineStyle();
			
			if (bg.hatchFill != null) {
				bg.hatchFill.beginFill(g);
				this.parallels.drawLine(g, this.space.geoBounds.top, true);
				this.meridians.drawLine(g, this.space.geoBounds.right, false);
				this.parallels.drawLine(g, this.space.geoBounds.bottom, false, true);
				this.meridians.drawLine(g, this.space.geoBounds.left, false, true);
				g.endFill();
			}
		}
	}
}