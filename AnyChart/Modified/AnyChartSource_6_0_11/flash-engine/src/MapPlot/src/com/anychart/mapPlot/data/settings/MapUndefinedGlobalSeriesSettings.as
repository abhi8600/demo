package com.anychart.mapPlot.data.settings {
	import com.anychart.palettes.ColorPalette;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	public class MapUndefinedGlobalSeriesSettings extends MapGroupGlobalSeriesSettings {
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.undefined_map_region[0] != null)
				super.deserialize(data.undefined_map_region[0]);
		}
		
		override public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserializeElements(data, styles, resources);
			if (data.undefined_map_region[0] != null)
				super.deserializeElements(data.undefined_map_region[0], styles, resources);
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new MapUndefinedSeriesSettings();
		}
	}
}