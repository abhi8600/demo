package com.anychart.mapPlot.controls.navigationPanel {
	import com.anychart.controls.Control;
	import com.anychart.mapPlot.MapPlot;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public final class NavigationPanel extends Control {
		
		public static function check(data:XML):Boolean {
			return String(data.name()) == "navigation_panel";
		}
		
		private var leftBtn:LeftNavigationButton;
		private var rightBtn:RightNavigationButton;
		private var topBtn:TopNavigationButton;
		private var bottomBtn:BottomNavigationButton;
		private var centerBtn:CenterNavigationButton;
		
		public function NavigationPanel() {
			super();
			this.leftBtn = new LeftNavigationButton();
			this.rightBtn = new RightNavigationButton();
			this.topBtn = new TopNavigationButton();
			this.bottomBtn = new BottomNavigationButton();
			this.centerBtn = new CenterNavigationButton();
		}
		
		override protected function checkContentBounds():void {
			super.checkContentBounds();
		}
		
		override public function initialize():DisplayObject {
			var container:Sprite = Sprite(super.initialize());
			container.addChild(this.leftBtn);
			container.addChild(this.rightBtn);
			container.addChild(this.topBtn);
			container.addChild(this.bottomBtn);
			container.addChild(this.centerBtn);
			
			this.leftBtn.addEventListener(MouseEvent.CLICK, leftBtnClick);
			this.rightBtn.addEventListener(MouseEvent.CLICK, rightBtnClick);
			this.topBtn.addEventListener(MouseEvent.CLICK, topBtnClick);
			this.bottomBtn.addEventListener(MouseEvent.CLICK, bottomBtnClick);
			this.centerBtn.addEventListener(MouseEvent.CLICK, centerBtnClick);
			
			return container;
		}
		
		private function leftBtnClick(e:MouseEvent):void {
			MapPlot(this.plot).navigationManager.moveMapLeft();
		}
		
		private function rightBtnClick(e:MouseEvent):void {
			MapPlot(this.plot).navigationManager.moveMapRight();
		}
		
		private function topBtnClick(e:MouseEvent):void {
			MapPlot(this.plot).navigationManager.moveMapTop();
		}
		
		private function bottomBtnClick(e:MouseEvent):void {
			MapPlot(this.plot).navigationManager.moveMapBottom();
		}
		
		private function centerBtnClick(e:MouseEvent):void {
			MapPlot(this.plot).navigationManager.centerMap();
		}
		
		override public function draw():void {
			super.draw();
			this.initializeLayout();
			this.drawButtons();
		}
		
		override public function resize():void {
			super.resize();
			this.initializeLayout();
			this.drawButtons();
		}
		
		private function initializeLayout():void {
			var px:Number = (this.contentBounds.width-10)*0.05;
			var py:Number = (this.contentBounds.height-10)*0.05;
			
			var sx:Number=((this.contentBounds.width-10)-px*2)/3;
			var sy:Number=((this.contentBounds.height-10)-py*2)/3;
			
			var dx:Number = 5;
			var dy:Number = 5;
			
			this.leftBtn.initialize(sx, sy);
			this.rightBtn.initialize(sx, sy);
			this.topBtn.initialize(sx, sy);
			this.bottomBtn.initialize(sx, sy);
			this.centerBtn.initialize(sx, sy);
			
			this.topBtn.x = sx+px+dx;
			this.topBtn.y = dy;
			
			this.bottomBtn.x = sx+px+dx;
			this.bottomBtn.y = sy*2+py*2+dy;
			
			this.leftBtn.x = dx;
			this.leftBtn.y = sy+py+dy;

			this.rightBtn.x = sx*2+px*2+dx;
			this.rightBtn.y = sy+py+dy;
			
			this.centerBtn.x = sx+px+dx;
			this.centerBtn.y = sy+py+dy;
		}
		
		private function drawButtons():void {
			this.leftBtn.draw();
			this.rightBtn.draw();
			this.topBtn.draw();
			this.bottomBtn.draw();
			this.centerBtn.draw();
		}
		
	}
}