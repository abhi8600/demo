package com.anychart.mapPlot.controls.label {
	import com.anychart.IAnyChart;
	import com.anychart.controls.Control;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public final class MapRegionLabelControl extends Control implements IElementContainer, IMainElementsContainer {
		
		public function get root():IAnyChart {
			return this.getLink().plot.root;
		}
		
		public static function check(data:XML):Boolean {
			return data.name().toString() == "map_region_label";
		}
		
		private var label:LabelElement;
		private var labelContainer:Sprite;
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			this.background = null;
			if (data.@link != undefined) this.link = SerializerBase.getString(data.@link);
		}
		
		override public function draw():void {
			super.draw();
			if (this.label) this.label.draw(this, this.label.style.normal, this.labelContainer, BasePoint.LABEL_INDEX);
		}
		
		override public function resize():void {
			super.resize();
			if (this.label) this.label.resize(this, this.label.style.normal, this.labelContainer, BasePoint.LABEL_INDEX);
		}
		
		override protected function execBackgroundDrawing(bounds:Rectangle):void {
			this.background.draw(this.container.graphics, bounds, this.color, this.hatchType);
		}
		
		override protected function checkContentBounds():void {
			this.setRegionProps();
			if (!this.layout.isWidthSetted || !this.layout.isHeightSetted) {
				if (this.label != null) {
					var bounds:Rectangle = this.label.getBounds(this, this.label.style.normal, BasePoint.LABEL_INDEX);
					if (!this.layout.isWidthSetted) this.bounds.width = bounds.width;
					if (!this.layout.isHeightSetted) this.bounds.height = bounds.height; 
				}else {
					if (!this.layout.isWidthSetted) this.bounds.width = 0;
					if (!this.layout.isHeightSetted) this.bounds.height = 0;
				}
			}
			
			
			super.checkContentBounds();
		}
		
		//--------------------------------------------------------
		//		Region properties
		//--------------------------------------------------------
		
		private var isPropsSetted:Boolean = false;
		private function setRegionProps():void {
			if (this.isPropsSetted) return;
			
			if (this.isLinkExists()) {
				this.setTooltip();
				this.setLabel();
			}
			
			this.isPropsSetted = true;
		}
		
		private function setTooltip():void {
			var region:BasePoint = this.getLink();
			if (region.tooltip.style == null) return;
			region.tooltip = TooltipElement(region.tooltip.createCopy());
			region.tooltip.style = region.tooltip.style.createCopy();
			this.setTooltipState(region.tooltip.style.normal);
			this.setTooltipState(region.tooltip.style.hover);
			this.setTooltipState(region.tooltip.style.pushed);
			this.setTooltipState(region.tooltip.style.selectedNormal);
			this.setTooltipState(region.tooltip.style.selectedHover);
			this.setTooltipState(region.tooltip.style.missing);
		}
		
		private function setTooltipState(state:StyleState):void {
			if (state != null)
				BaseElementStyleState(state).anchor = Anchor.CENTER;
		}
		
		private function setLabel():void {
			this.label = LabelElement(this.getLink().label.createCopy());
			var styleXML:XML = this.label.style.xmlData;
			if (styleXML.@enabled != undefined) styleXML.@enabled = true;
			if (styleXML.states[0]) {
				this.setLabelState(styleXML, "normal");
				this.setLabelState(styleXML, "hover");
				this.setLabelState(styleXML, "pushed");
				this.setLabelState(styleXML, "selected_normal");
				this.setLabelState(styleXML, "selected_hover");
				this.setLabelState(styleXML, "missing");
			}
			this.label.enabled = true;
			
			this.label.style = this.label.style.createCopy();
			if (this.label.style.normal != null) {
				if (LabelElementStyleState(this.label.style.normal).label.background != null) {
					this.background = LabelElementStyleState(this.label.style.normal).label.background;
					LabelElementStyleState(this.label.style.normal).label.background = null;
				}
			}
			this.label.style.setDynamicColors(this.getLink().color);
			this.label.style.setDynamicGradients(this.getLink().color);
			this.label.style.setDynamicHatchTypes(this.getLink().hatchType);
			
			this.labelContainer = this.label.initialize(this, BasePoint.LABEL_INDEX);
			this.container.mouseChildren = false;
			this.container.buttonMode = true;
			
			this.getLink().initListeners(this.container);
		}
		
		private function setLabelState(styleXML:XML, stateName:String):void {
			if (styleXML.states[0][stateName][0] && styleXML.states[0][stateName][0].@enabled != undefined)
				styleXML.states[0][stateName][0].@enabled = "true"; 
		}
		
		//--------------------------------------------------------
		//		Link
		//--------------------------------------------------------
		
		private var link:String;
		
		private function isLinkExists():Boolean {
			if (this.link == null) return false;
			if (!(this.plot is MapPlot)) return false;
			return MapPlot(this.plot).getRegion(this.link) != null;
		}
		
		private function getLink():BasePoint {
			return MapPlot(this.plot).getRegion(this.link);
		}
		
		//--------------------------------------------------------
		//		IElementContainer
		//--------------------------------------------------------
		
		public function get elementsInfo():Object { return this.getLink().elementsInfo; }
		public function set elementsInfo(value:Object):void { this.getLink().elementsInfo = value; }
		
		public function get color():int { return this.getLink().color; }
		public function get hatchType():int { return this.getLink().hatchType; }
		public function get markerType():int { return this.getLink().markerType; }
		public function getTokenValue(token:String):* { return this.getLink().getTokenValue(token); }
		public function isDateTimeToken(token:String):Boolean { return this.getLink().isDateTimeToken(token); }
		
		public function get hasPersonalContainer():Boolean { return true; }
		
		public function getElementPosition(state:BaseElementStyleState, sprite:Sprite, bounds:Rectangle):Point {
			return new Point(0,0);
		}
		
		public function getMainContainer():IMainElementsContainer { return this; }
		
		//--------------------------------------------------------
		//		IMainElementsContainer
		//--------------------------------------------------------
		
		public function getLabelsContainer(label:ItemLabelElement):Sprite {
			return this.container;
		}
		
		public function get tooltipsContainer():DisplayObjectContainer { return this.getLink().getMainContainer().tooltipsContainer; }
		public function get markersContainer():Sprite { return this.getLink().getMainContainer().markersContainer; }
	}
}