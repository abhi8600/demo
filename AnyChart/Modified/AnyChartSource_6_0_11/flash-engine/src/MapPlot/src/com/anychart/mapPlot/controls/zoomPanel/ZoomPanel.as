package com.anychart.mapPlot.controls.zoomPanel {
	import com.anychart.controls.Control;
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.navigation.MapZoomManager;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class ZoomPanel extends Control {
		
		protected var zoomInBtn:ZoomInButton;
		protected var zoomOutBtn:ZoomOutButton;
		protected var reversed:Boolean;
		protected var slider:Slider;
		
		public var zoomManager:MapZoomManager;
		
		public function ZoomPanel() {
			super();
			this.reversed = false;
			this.zoomInBtn = new ZoomInButton();
			this.zoomOutBtn = new ZoomOutButton();
		}
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.zoomManager = MapPlot(this.plot).zoomManager;
			this.zoomManager.controls.push(this);
			this.container.addChild(this.zoomInBtn);
			this.container.addChild(this.zoomOutBtn);
			this.container.addChild(this.slider);
			this.slider.addEventListener(Event.CHANGE, this.sliderValueChangeHandler);
			this.zoomInBtn.addEventListener(MouseEvent.CLICK, this.zoomInBtnClick);
			this.zoomOutBtn.addEventListener(MouseEvent.CLICK, this.zoomOutBtnClick);
			return this.container;
		}
		
		override public function draw():void {
			super.draw();
			this.initializeLayout();
			this.drawButtons();
			this.setSliderValue();
		}
		
		override public function resize():void {
			super.resize();
			this.initializeLayout();
			this.drawButtons();
		}
		
		protected function initializeLayout():void {
			var s:Number=this.getButtonSize();
			
			this.zoomInBtn.initialize(s, s);
			this.zoomOutBtn.initialize(s, s);
		}
		
		protected function getButtonSize():Number {
			return 19;
			//return (Math.min(this.contentBounds.width, this.contentBounds.height)-21);
		}
		
		private function drawButtons():void {
			this.zoomInBtn.draw();
			this.zoomOutBtn.draw();
			this.slider.draw();
		}
		
		private var prevValue:Number = NaN;
		private function sliderValueChangeHandler(e:Event):void {
			var value:Number = this.reversed ? (1 - this.slider.value) : this.slider.value;
			if (!isNaN(prevValue) && value == prevValue)
				return;
			this.zoomManager.factor = this.zoomManager.minimum + (this.zoomManager.maximum - this.zoomManager.minimum)*value;
			this.plot.calculateResize(MapPlot(this.plot).globalBounds);
			this.plot.execResize();
			prevValue = value;
		}
		
		public function setSliderValue():void {
			var value:Number = (this.zoomManager.factor - this.zoomManager.minimum)/(this.zoomManager.maximum - this.zoomManager.minimum)
			if (this.reversed)
				value = 1-value;
			this.slider.value = value;
		}
		
		private function zoomInBtnClick(e:MouseEvent):void {
			this.zoomManager.factor += this.zoomManager.step;
			this.setSliderValue();
			this.sliderValueChangeHandler(null);
		}
		
		private function zoomOutBtnClick(e:MouseEvent):void {
			this.zoomManager.factor -= this.zoomManager.step;
			this.setSliderValue();
			this.sliderValueChangeHandler(null);
		}
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			if (data.@inverted != undefined) this.reversed = SerializerBase.getBoolean(data.@inverted);
		}
	}
}