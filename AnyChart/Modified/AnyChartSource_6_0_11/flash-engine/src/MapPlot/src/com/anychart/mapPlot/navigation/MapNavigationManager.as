package com.anychart.mapPlot.navigation {
	import com.anychart.mapPlot.MapPlot;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class MapNavigationManager {
		
		private var step:Number = .1;
		private var plot:MapPlot;
		
		public function MapNavigationManager(plot:MapPlot) {
			this.plot = plot;
		}
		
		public function initialize():void {
			if (this.plot.zoomManager.needMoveMap)
				this.moveTo(this.plot.zoomManager.x, this.plot.zoomManager.y, this.plot.zoomManager.geoX, this.plot.zoomManager.geoY);
			if (this.plot.zoomManager.dragable)
				this.plot.draggableContainer.addEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
		}
		
		private function move(dx:Number, dy:Number):void {
			this.plot.zoomManager.x += dx;
			this.plot.zoomManager.y += dy;
			
			if (this.plot.zoomManager.x < 0) this.plot.zoomManager.x = 0;
			else if (this.plot.zoomManager.x> 1) this.plot.zoomManager.x = 1;
			
			if (this.plot.zoomManager.y < 0) this.plot.zoomManager.y = 0;
			else if (this.plot.zoomManager.y> 1) this.plot.zoomManager.y = 1; 
			
			this.moveTo(this.plot.zoomManager.x, this.plot.zoomManager.y, this.plot.zoomManager.geoX, this.plot.zoomManager.geoY);
		}
		
		public function moveMapLeft():void {
			this.move(-this.step / this.plot.zoomManager.factor, 0);
		}
		
		public function moveMapRight():void {
			this.move(this.step / this.plot.zoomManager.factor, 0);
		}
		
		public function moveMapTop():void {
			this.move(0, -this.step / this.plot.zoomManager.factor);
		}
		
		public function moveMapBottom():void {
			this.move(0, this.step / this.plot.zoomManager.factor);
		}
		
		public function centerMap():void {
			this.plot.zoomManager.x = 0.5;
			this.plot.zoomManager.y = 0.5;
			this.plot.zoomManager.factor = 1;
			this.plot.resize(this.plot.getBounds());
		}
		
		private function moveTo(fX:Number, fY:Number, geoX:Number = NaN, geoY:Number = NaN):void {
			var geoBounds:Rectangle = this.plot.space.geoBounds;
			var geoX:Number = isNaN(geoX) ? (geoBounds.x + geoBounds.width*fX) : geoX;
			var geoY:Number = isNaN(geoY) ? (geoBounds.y + geoBounds.height*fY) : geoY; 
			var pixCenter:Point = new Point();
			this.plot.space.transform(geoX, geoY, pixCenter);
			this.plot.setMapPixelCenterPosition(pixCenter.x,pixCenter.y);
		}
		
		private var dragPoint:Point;
		public var dragRect:Rectangle;				
		
		private function onMouseDown(e:MouseEvent):void {
			if (!this.dragPoint) dragPoint = new Point; 
			this.dragPoint.x = e.stageX;
			this.dragPoint.y = e.stageY;
			
			/*this.plot.draggableContainer.mouseChildren = false;
			this.plot.root.getMainContainer().addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.plot.root.getMainContainer().addEventListener(MouseEvent.MOUSE_UP,stopDragging);*/
		}
		
		private function onMouseMove(e:MouseEvent):void{
			var pix:Point = new Point (e.stageX,e.stageY);
			pix = this.plot.root.getMainContainer().localToGlobal(pix);
			 
			var dx:Number = (this.dragPoint.x-pix.x)/this.plot.space.mapPixBounds.width;
			var dy:Number = (this.dragPoint.y-pix.y)/this.plot.space.mapPixBounds.height;
			
			this.dragPoint.x = e.stageX;
			this.dragPoint.y = e.stageY;
			
			this.move(dx,dy); 
		}
		
		
		
		
		
		private function stopDragging(e:MouseEvent):void {
			/*this.plot.root.getMainContainer().removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.plot.root.getMainContainer().removeEventListener(MouseEvent.MOUSE_UP,stopDragging);
			this.plot.draggableContainer.mouseChildren = true;*/
		}

	}
}