package com.anychart.mapPlot.geoSpace {
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.data.MapRegion;
	import com.anychart.mapPlot.geoSpace.projection.Projection;
	import com.anychart.visual.layout.Margin;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class GeoSpace extends BaseGeoSpace {
		
		//non-linear geo coordinates scales
		//DON'T USE IT FOR TRANSFORMATIONS!!!
		//USED FOR AXES GRIDS AND LABELS ONLY!!
		public var geoXScale:MapScale;
		public var geoYScale:MapScale;
		
		public var isWorldMap:Boolean;
		
		public var transformedRegions:Array;
		
		public function GeoSpace() {
			super();
			this.geoXScale = new MapScale();
			this.geoYScale = new MapScale();
			
			this.isWorldMap = false;
			this.projection = new Projection();
			this.transformedRegions = [];
		}
		
		override protected function boundX(x:Number):Number {
			return Math.max(Math.min(x, geoBounds.right),geoBounds.left);
		}
		
		override protected function boundY(y:Number):Number {
			return Math.max(Math.min(y, geoBounds.bottom),geoBounds.top);
		}
		
		override public function calculateProjection(showLabels:Boolean = false):void {
			
			this.geoXScale.minimum = this.geoBounds.left;
			this.geoXScale.maximum = this.geoBounds.right;
			this.geoYScale.minimum = this.geoBounds.top;
			this.geoYScale.maximum = this.geoBounds.bottom;
			
			if (!this.isWorldMap) {
				
				if (!showLabels || this.projection.customCentroid) {
					this.geoXScale.isMaximumAuto = false;
					this.geoXScale.isMinimumAuto = false;
					this.geoYScale.isMaximumAuto = false;
					this.geoYScale.isMinimumAuto = false;
				}else {
					this.geoXScale.isMaximumAuto = true;
					this.geoYScale.isMaximumAuto = true;
					this.geoXScale.isMinimumAuto = true;
					this.geoYScale.isMinimumAuto = true;
				}
				this.geoXScale.calculate();
				this.geoYScale.calculate();
				
				if (!this.projection.customCentroid) {
					this.geoXScale.minimum = Math.max(this.geoXScale.minimum, -180);
					this.geoXScale.maximum = Math.min(this.geoXScale.maximum, 180);
					this.geoYScale.minimum = Math.max(this.geoYScale.minimum, -90);
					this.geoYScale.maximum = Math.min(this.geoYScale.maximum, 90);
				}
				
				this.geoBounds.x = this.geoXScale.minimum;
				this.geoBounds.width = this.geoXScale.maximum - this.geoXScale.minimum;
				this.geoBounds.y = this.geoYScale.minimum;
				this.geoBounds.height = this.geoYScale.maximum - this.geoYScale.minimum;
			}
			
			var resetGeoBounds:Boolean = false;
			
			if (this.geoBounds.width % this.geoXScale.majorInterval != 0) {
				if (this.geoXScale.contains(0) && (Math.abs(this.geoXScale.minimum) == Math.abs(this.geoXScale.maximum)))
					this.geoXScale.majorInterval = Math.abs(this.geoXScale.maximum/3);
				else
					this.geoXScale.majorInterval = this.geoBounds.width/5;
				this.geoXScale.isMajorIntervalAuto = false;
				this.geoXScale.calculate();
				resetGeoBounds = true;
			}
			if (this.geoBounds.height % this.geoYScale.majorInterval != 0) {
				if (this.geoYScale.contains(0) && (Math.abs(this.geoYScale.minimum) == Math.abs(this.geoYScale.maximum)))
					this.geoYScale.majorInterval = Math.abs(this.geoYScale.maximum/3);
				else
					this.geoYScale.majorInterval = this.geoBounds.height/5;
				this.geoYScale.isMajorIntervalAuto = false;
				this.geoYScale.calculate();
				resetGeoBounds = true;
			}
			
			if (resetGeoBounds) {
				this.geoBounds.x = this.geoXScale.minimum;
				this.geoBounds.width = this.geoXScale.maximum - this.geoXScale.minimum;
				this.geoBounds.y = this.geoYScale.minimum;
				this.geoBounds.height = this.geoYScale.maximum - this.geoYScale.minimum;
			}
			super.calculateProjection();
		}
		
		public function initBounds(plot:MapPlot, pixRect:Rectangle, margin:Margin):void {
			
			pixRect = pixRect.clone();
			pixRect.width -= margin.left + margin.right;
			pixRect.height -= margin.top + margin.bottom;
			
			//set proportions
			var geoScale:Number = Math.abs(this.linearGeoBounds.width/this.linearGeoBounds.height);
			var pixScale:Number = pixRect.width/pixRect.height;
			this.mapPixBounds = new Rectangle();
			if (geoScale > pixScale) {
				this.mapPixBounds.width = pixRect.width;
				this.mapPixBounds.height = pixRect.width/geoScale;
			}else {
				this.mapPixBounds.height = pixRect.height;
				this.mapPixBounds.width = pixRect.height*geoScale;
			}
			plot.zoomManager.applyZoom(pixRect,this.mapPixBounds);
			
			this.mapPixBounds.x += margin.left;
			this.mapPixBounds.y += margin.top;
			
			if (!plot.zoomManager.needMoveMap) {
				if (this.mapPixBounds.width < pixRect.width)
					this.mapPixBounds.x -= (this.mapPixBounds.width/2 - pixRect.width/2);
					
				if (this.mapPixBounds.height < pixRect.height)
					this.mapPixBounds.y -= (this.mapPixBounds.height/2 - pixRect.height/2);
			}
			
			this.setPixelRange(this.mapPixBounds);
			
			var leftTop:Point = new Point();
			var rightBottom:Point = new Point();
			for (var i:uint = 0;i<this.transformedRegions.length;i++) {
				var region:MapRegion = this.transformedRegions[i];
				var transformedSpace:TransformedGeoSpace = TransformedGeoSpace(region.space);
				
				this.transformLinearized(transformedSpace.linearLeftTop, leftTop);
				this.transformLinearized(transformedSpace.linearRightBottom, rightBottom);
				
				transformedSpace.initBounds(new Rectangle(leftTop.x,leftTop.y, rightBottom.x - leftTop.x, rightBottom.y - leftTop.y)); 
			}
		}
		
	}
}