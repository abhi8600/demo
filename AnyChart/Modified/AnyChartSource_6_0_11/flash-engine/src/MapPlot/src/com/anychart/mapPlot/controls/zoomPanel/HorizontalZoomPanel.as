package com.anychart.mapPlot.controls.zoomPanel {
	import flash.geom.Rectangle;
	
	public final class HorizontalZoomPanel extends ZoomPanel {
		public static function check(data:XML):Boolean {
			return String(data.name()) == "zoom_panel" &&
					( 
						(data.@orientation != undefined && 
							String(data.@orientation).toLowerCase() == "horizontal")
						||
						(data.@position != undefined && 
							(
								(String(data.@position).toLowerCase() == "top") 
								|| 
								(String(data.@position).toLowerCase() == "bottom")
							)
						)
					);
		}		
		
		public function HorizontalZoomPanel() {
			super();
			this.layout.width = 190;
			this.layout.height = 40;
			this.slider = new HorizontalSlider();
		}
		
		override protected function initializeLayout():void {
			super.initializeLayout();
			
			var maxH:Number = this.contentBounds.height;
			var btnH:Number = this.getButtonSize();
			
			this.zoomInBtn.y = (maxH - btnH)/2;
			this.zoomOutBtn.y = (maxH - btnH)/2;
			this.zoomInBtn.x = this.reversed ? (this.contentBounds.left + 10) : (this.contentBounds.right - this.zoomInBtn.bounds.width - 10);
			this.zoomOutBtn.x = this.reversed ? (this.contentBounds.right - this.zoomOutBtn.width - 10) : (this.contentBounds.left + 10);
			
			
			var w:Number = this.contentBounds.width - 20 - this.zoomInBtn.bounds.width - this.zoomOutBtn.bounds.width;
			this.slider.initialize(w-12, this.getButtonSize());
			
			this.slider.y = (maxH - btnH)/2;
			this.slider.x = this.contentBounds.left + 16 +
								((this.reversed) ? (this.zoomInBtn.bounds.width) : 
								  				   (this.zoomOutBtn.bounds.width));
		}
	}
}