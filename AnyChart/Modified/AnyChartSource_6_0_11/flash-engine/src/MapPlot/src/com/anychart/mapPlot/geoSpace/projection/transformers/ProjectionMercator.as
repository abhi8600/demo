package com.anychart.mapPlot.geoSpace.projection.transformers {
	 
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	internal final class ProjectionMercator extends ProjectionTransformer {
		override public function transform(geoX:Number, geoY:Number, pt:Point):void {
    		var num2:Number = geoY;
			var a:Number = (this.a(num2, -84, 84) * Math.PI) / 180;
			pt.x = geoX - centerX;
			pt.y = Math.log(Math.tan(a) + (1 / Math.cos(a))) * 57.295779513082323;
		}
		
		override public function setLinearBounds(geoBounds:Rectangle, linearGeoBounds:Rectangle):void {
			var minX:Number = Number.POSITIVE_INFINITY;
			var maxX:Number = Number.NEGATIVE_INFINITY;
			var minY:Number = Number.POSITIVE_INFINITY;
			var maxY:Number = Number.NEGATIVE_INFINITY;
			
			var pt:Point = new Point();
			this.transform(geoBounds.left, geoBounds.top, pt);
			minX = Math.min(pt.x, minX);
			maxX = Math.max(pt.x, maxX);
			minY = Math.min(pt.y, minY);
			maxY = Math.max(pt.y, maxY);
			
			this.transform(geoBounds.right, geoBounds.bottom, pt);
			minX = Math.min(pt.x, minX);
			maxX = Math.max(pt.x, maxX);
			minY = Math.min(pt.y, minY);
			maxY = Math.max(pt.y, maxY);
			
			linearGeoBounds.x = minX;
			linearGeoBounds.y = minY;
			linearGeoBounds.width = maxX - minX;
			linearGeoBounds.height = maxY - minY;
		}
		
		override public function drawLine(g:Graphics, space:BaseGeoSpace, start:Point, end:Point, moveTo:Boolean):void {
			var pixPt:Point = new Point();
			space.transform(start.x, start.y, pixPt);
			if (moveTo) g.moveTo(pixPt.x, pixPt.y);
			else g.lineTo(pixPt.x, pixPt.y);
			space.transform(end.x, end.y, pixPt);
			g.lineTo(pixPt.x, pixPt.y);
		}
		
		override public function interpolateLine(start:Point, end:Point, linearPts:Array):void {
			var pixPt:Point = new Point();
			this.transform(start.x, start.y, pixPt);
			linearPts.push(pixPt);
			this.transform(end.x, end.y, pixPt);
			linearPts.push(pixPt.clone());
		}
	}
}