package com.anychart.mapPlot {
	import com.anychart.controls.ControlsFactory;
	import com.anychart.data.SeriesType;
	import com.anychart.elements.marker.MarkerType;
	import com.anychart.mapPlot.data.GeoBasedPoint;
	import com.anychart.mapPlot.data.MapRegion;
	import com.anychart.mapPlot.data.MapSeries;
	import com.anychart.mapPlot.data.settings.MapDefinedGlobalSeriesSettings;
	import com.anychart.mapPlot.data.settings.MapUndefinedGlobalSeriesSettings;
	import com.anychart.mapPlot.geoSpace.GeoSpace;
	import com.anychart.mapPlot.geoSpace.TransformedGeoSpace;
	import com.anychart.mapPlot.labels.LabelsManager;
	import com.anychart.mapPlot.navigation.MapNavigationManager;
	import com.anychart.mapPlot.navigation.MapZoomManager;
	import com.anychart.mapPlot.reader.MapReader;
	import com.anychart.mapPlot.templates.MapPlotTemplatesManager;
	import com.anychart.palettes.ColorPalette;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.visual.hatchFill.HatchFillType;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	public class MapPlot extends SeriesPlot {
		
		private static const UNDEFINED_REGION_SERIES_NAME:String = "AnyChart_UndefinedRegions";
		
		//-----------------------------------------------------
		//				Controls
		//-----------------------------------------------------
		private static var initControls:Boolean = includeControls();
		private static function includeControls():Boolean {
			import com.anychart.mapPlot.controls.navigationPanel.NavigationPanel;
			ControlsFactory.register(NavigationPanel);
			
			import com.anychart.mapPlot.controls.zoomPanel.HorizontalZoomPanel;
			ControlsFactory.register(HorizontalZoomPanel);
			
			import com.anychart.mapPlot.controls.zoomPanel.VerticalZoomPanel;
			ControlsFactory.register(VerticalZoomPanel);
			
			import com.anychart.mapPlot.controls.label.MapRegionLabelControl;
			ControlsFactory.register(MapRegionLabelControl);
			return true;
		}
		
		//--------------------------------------------------------
		//					IPlot
		//--------------------------------------------------------
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		private static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (BasePlot.templatesManager == null)
				BasePlot.templatesManager = new MapPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate().copy(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		override public function getTemplatesManager():BaseTemplatesManager {
			if (templatesManager == null)
				templatesManager = new MapPlotTemplatesManager();
			return BasePlot.templatesManager;
		}
		
		override public function hasSeries():Boolean {
			return this.seriesIndex <= this.series.length;
		}
		
		override public function getNextSeries():BaseSeries {
			var res:BaseSeries = (this.seriesIndex == 0) ? this.undefinedRegionSeries : this.series[this.seriesIndex-1];
			this.seriesIndex++;
			return res;
		}
		
		override public function destroy() : void {
			if (this.zoomManager) this.zoomManager.destroy();
			if (this.undefinedRegionSettings) this.undefinedRegionSettings.destroy();
			super.destroy();
			if (this.undefinedRegionSeries) {
				this.undefinedRegionSeries.destroy();
				this.undefinedRegionSeries = null;
			}
		}
		
		//-----------------------------------------------------
		//				Map
		//-----------------------------------------------------
		private var mapSettings:MapSettings;
		private var undefinedRegionSettings:MapUndefinedGlobalSeriesSettings;
		private var undefinedRegionSeries:MapSeries;
		private var undefinedRegionPalette:ColorPalette;
		private var regionIndex:int;
		
		public var mapContainer:Sprite;
		private var mapBorderContainer:Sprite;
		private var mapStatesBorderContainer:Sprite;
		internal var mapLabelsContainer:Sprite;
		
		private var mapMargin:Margin;
		
		public var space:GeoSpace;
		
		public var regionLabelsManager:LabelsManager;
		public var zoomManager:MapZoomManager;
		public var navigationManager:MapNavigationManager;
		
		public var regionElementsContainer:RegionElementsContainer;
		
		public function MapPlot() {
			this.defaultSeriesType = SeriesType.MAP_REGION;
			this.mapSettings = new MapSettings(this);
			this.undefinedRegionSettings = new MapUndefinedGlobalSeriesSettings();
			this.definedRegionsMap = {};
			this.regionsMap = {};
			this.visibleRegionsList = [];
			this.space = new GeoSpace();
			this.mapContainer = new Sprite();
			this.mapLabelsContainer = new Sprite();
			this.mapBorderContainer = new Sprite();
			this.regionIndex = 0;
			this.mapMargin = new Margin();
			this.mapMargin.all = 5;
			
			this.mapStatesBorderContainer = new Sprite();
			this.mapBorderContainer.mouseEnabled = false;
			this.mapStatesBorderContainer.mouseEnabled = false;
			this.mapBorderContainer.addChild(this.mapStatesBorderContainer);
			
			this.zoomManager = new MapZoomManager();
			this.navigationManager = new MapNavigationManager(this);
			this.regionElementsContainer = new RegionElementsContainer(this);
			super();
		}
		
		
		private var borderContainers:Object;
		public function createBorderContainer(region:MapRegion):Graphics {
			this.borderContainers = this.borderContainers || {};
			if (this.borderContainers[region.name] == null) {
				this.borderContainers[region.name] = new Sprite();
				this.mapStatesBorderContainer.addChild(this.borderContainers[region.name]);
			}
			return Sprite(this.borderContainers[region.name]).graphics;
		}
		
		public function removeBorderContainer(region:MapRegion):void {
			if (!this.borderContainers) return;
			if (!this.borderContainers[region.name]) return;
			this.mapStatesBorderContainer.removeChild(this.borderContainers[region.name]);
			this.borderContainers[region.name] = null;
		}
		
		//--------------------------------------------------------
		//					Factory
		//--------------------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			var sClass:Class;
			switch (seriesType) {
				case SeriesType.BUBBLE:
					sClass = Class(getDefinitionByName("com.anychart.mapPlot.bubbleSeries::MapBubbleGlobalSeriesSettings"));
					break;
				case SeriesType.CONNECTOR:
					sClass = Class(getDefinitionByName("com.anychart.mapPlot.connectorSeries::ConnectorGlobalSeriesSettings"));
					break;
				case SeriesType.AREA:
					sClass = Class(getDefinitionByName("com.anychart.mapPlot.areaSeries::MapAreaGlobalSeriesSettings"));
					break;
				case SeriesType.LINE:
					sClass = Class(getDefinitionByName("com.anychart.mapPlot.lineSeries::MapLineGlobalSeriesSettings"));
					break;
				case SeriesType.MARKER:
					sClass = Class(getDefinitionByName("com.anychart.mapPlot.markerSeries::MapMarkerGlobalSeriesSettings"));
					break;
				default:
				case SeriesType.MAP_REGION:
					return new MapDefinedGlobalSeriesSettings();
			}
			return new sClass();
		}
		
		override public function getSeriesType(type:*):uint {
			if (type == null) return this.defaultSeriesType;
			switch (SerializerBase.getEnumItem(type)) {
				case "bubble": return SeriesType.BUBBLE;
				case "connector": return SeriesType.CONNECTOR;
				case "area": return SeriesType.AREA;
				case "line": return SeriesType.LINE;
				case "marker": return SeriesType.MARKER;
			}
			return SeriesType.MAP_REGION;
		}
		
		//--------------------------------------------------------
		//					Deserialization
		//--------------------------------------------------------
		
		override protected function checkTresholdsAfterDeserialize():void {}
		
		override protected function needCheckPointNames(series:BaseSeries):Boolean {
			return (series is MapSeries);
		}
		
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {
			super.deserializeDataPlotSettings(data, resources);
			this.mapSettings.deserialize(data, resources);
		}
		
		override protected function onAfterDeserializeData(data:XML, resources:ResourcesLoader):void {
			
			var labelsDisplayMode:String = null;
			var rSettings:XML;
			
			if (data.data_plot_settings[0] != null && data.data_plot_settings[0].map_series[0] != null) {
				var mapSettings:XML = data.data_plot_settings[0].map_series[0];
				
				if (mapSettings.@labels_display_mode != undefined)
					labelsDisplayMode = SerializerBase.getEnumItem(mapSettings.@labels_display_mode);
				
				if (mapSettings.undefined_map_region[0] != null) {
					
					rSettings = mapSettings.undefined_map_region[0]; 
						
					if (rSettings.@palette != undefined) {
						var paletteName:String = SerializerBase.getLString(rSettings.@palette);
						var paletteNode:XML = data.palettes[0].palette.(@name == paletteName)[0];
						if (!paletteNode)
							paletteNode = data.palettes[0].palette.(@name == "default")[0];
						if (paletteNode != null) {
							this.undefinedRegionPalette = new ColorPalette();
							this.undefinedRegionPalette.deserialize(paletteNode);
						}
						
						
						if (this.undefinedRegionPalette) {
							if (rSettings.@color != undefined)
								delete rSettings.@color;
							if (rSettings.map_region_style[0] != null && rSettings.map_region_style[0].@color != undefined)
								delete rSettings.map_region_style[0].@color;
						}
					}
				}
			}
			
			this.processSettings(this.undefinedRegionSettings, data.data_plot_settings[0], data.styles[0], resources, data.data[0]);
			this.undefinedRegionSeries = MapSeries(this.undefinedRegionSettings.createSeries(SeriesType.MAP_REGION));
			this.undefinedRegionSeries.points = [];
			this.undefinedRegionSeries.name = UNDEFINED_REGION_SERIES_NAME;
			this.undefinedRegionSeries.settings = this.undefinedRegionSettings.settings;  
			this.undefinedRegionSettings.setElements(this.undefinedRegionSeries);
			
			var undefinedRegionColor:uint = 0xCDCDCD;
			var undefinedRegionHatchType:uint = HatchFillType.DIAGONAL_CROSS;
			var undefinedRegionMarkerType:uint = MarkerType.CIRCLE;
			
			if (rSettings != null) {
				if (rSettings.@color != undefined) undefinedRegionColor = SerializerBase.getColor(rSettings.@color);
				if (rSettings.@hatch_type != undefined) undefinedRegionHatchType = SerializerBase.getHatchType(rSettings.@hatch_type);
				if (rSettings.@marker_type != undefined) undefinedRegionMarkerType = SerializerBase.getMarkerType(rSettings.@marker_type);
				if (rSettings.@threshold != undefined)
					this.undefinedRegionSeries.deserializeThreshold(this.thresholdsList, rSettings, data.palettes[0]);
			}
			
			this.regionLabelsManager = LabelsManager.create(labelsDisplayMode);
			
			this.undefinedRegionSeries.color = undefinedRegionColor; 
			this.undefinedRegionSeries.hatchType = undefinedRegionHatchType;
			this.undefinedRegionSeries.markerType = undefinedRegionMarkerType;
			this.undefinedRegionSeries.initializeStyle();
			this.undefinedRegionSeries.initializeElementsStyle();
		}
		
		//--------------------------------------------------------
		//					Initialization and parsing
		//--------------------------------------------------------
		
		override public function preInitialize():void {
			
			this.undefinedAnimationTargets = [];
			
			var reader:MapReader = new MapReader();
			reader.read(this.mapSettings);
			super.checkTresholdsAfterDeserialize();
			
			if (this.undefinedAnimationTargets) {
				var ptCount:int = this.undefinedAnimationTargets.length;
				for (var i:int = 0;i<ptCount;i++) {
					var pt:MapRegion = this.undefinedAnimationTargets[i];
					if (this.undefinedRegionSettings.animation)
						this.addPointAnimation(this.undefinedRegionSettings.animation.getAnimation(i, ptCount), pt);
					
					pt.initializeElementsAnimation(this.animationManager, i, ptCount);
				}
				
				this.undefinedAnimationTargets = null;
			}
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			
			this.baseBounds = bounds.clone();
			
			this.globalBounds = bounds.clone();
			
			if (this.mapSettings.grid != null) {
				this.beforeDataContainer.addChild(this.mapSettings.grid.backgroundShape);
				this.beforeDataContainer.addChild(this.mapSettings.grid.underMapSprite);
			}
			this.beforeDataContainer.addChild(this.mapContainer);
			this.beforeDataContainer.addChild(this.mapLabelsContainer);
			if (this.mapSettings.grid != null) {
				this.beforeDataContainer.addChild(this.mapSettings.grid.beforeMapSprite);
				this.mapSettings.grid.initialize();
			}
			super.initialize(bounds);
			
			this.calcMargin();
			this.space.initBounds(this, bounds, this.mapMargin);
			
			this.zoomManager.initZoom(this);
			this.navigationManager.initialize();
			
			return this.sprite;
		}
		
		private function calcMargin():void {
			if (this.mapSettings.grid != null)
				this.mapSettings.grid.setMargin(this.mapMargin);
		}
		
		//--------------------------------------------------------
		//					Regions
		//--------------------------------------------------------
		
		public var regionsMap:Object;
		public var definedRegionsMap:Object;
		private var visibleRegionsList:Array;
		
		public function registerDefinedRegion(region:MapRegion):void {
			this.definedRegionsMap[region.name] = region;
		}
		
		private var undefinedAnimationTargets:Array;
		
		public function createRegion(regionName:String, data:Object):MapRegion {
			var region:MapRegion;
			if (this.definedRegionsMap[regionName] != null) {
				
				region = this.definedRegionsMap[regionName];
				region.data = data;
				this.visibleRegionsList.push(region);
				if (region.threshold != null)
					region.threshold.checkAfterDeserialize(region);
				
				region.initializeSelection();
				this.regionsMap[regionName] = region;
				return region;
			}
			region = MapRegion(this.undefinedRegionSeries.createPoint());
			region.data = data;
			region.settings = this.undefinedRegionSeries.settings;
			this.undefinedRegionSeries.setElements(region);
			if (this.undefinedRegionPalette != null) {
				region.color = this.undefinedRegionPalette.getItemAt(this.regionIndex);
				this.regionIndex++;
			}else {
				region.color = this.undefinedRegionSeries.color;
			}
			region.hatchType = this.undefinedRegionSeries.hatchType;
			region.markerType = this.undefinedRegionSeries.markerType;
			region.series = this.undefinedRegionSeries;
			region.threshold = this.undefinedRegionSeries.threshold;
			if (region.threshold != null)
				region.threshold.checkAfterDeserialize(region);
			region.createContainer();
			region.initializeStyle();
			region.initializeElementsStyle();
			
			this.regionsMap[regionName] = region;
			this.undefinedRegionSeries.points.push(region);
			this.visibleRegionsList.push(region);
			
			region.initializeSelection();
			
			if (this.undefinedAnimationTargets)
				this.undefinedAnimationTargets.push(region);
			
			return region;
		}
		
		public function getRegion(regionName:String):MapRegion {
			return this.regionsMap[regionName];
		}
		
		//--------------------------------------------------------
		//					Space
		//--------------------------------------------------------
		
		private function setPointCoords(point:GeoBasedPoint):void {
			for (var i:int = 0;i<this.space.transformedRegions.length;i++) {
				var tr:TransformedGeoSpace = TransformedGeoSpace(this.space.transformedRegions[i].space);
				if (tr.contains(point.geoX, point.geoY)) {
					point.space = tr;
					return;
				}
			}
			point.space = this.space;
		}
		
		private function setPointRegion(point:GeoBasedPoint):void {
			var r:MapRegion = MapRegion(this.regionsMap[point.link]);
			point.geoX = r.geoCentroid.x;
			point.geoY = r.geoCentroid.y;
			point.space = r.space;
		}
		
		//--------------------------------------------------------
		//					Drawing
		//--------------------------------------------------------
		
		override public function draw():void {
			super.draw();
			var i:int;
			for (i = (this.visibleRegionsList.length-1);i>=0;i--) {
				var region:MapRegion = this.visibleRegionsList[i];
				region.initialize();
				region.checkThreshold();
				region.draw();
				if (this.mapSettings.regionsBorder != null)
					region.drawBorder(this.mapSettings.regionsBorder, this.mapBorderContainer.graphics);
			}
			this.mapContainer.addChild(this.mapBorderContainer);
			
			if (this.mapSettings.effects != null)
				this.mapContainer.filters = this.mapSettings.effects.list;
			
			this.regionLabelsManager.process();
			
			if (this.mapSettings.grid != null)
				this.mapSettings.grid.draw();
		}
		
		override protected function drawSeries():void {
			var i:int;
			for (i = 0;i<this.onBeforeDrawSeries.length;i++)
				this.onBeforeDrawSeries[i].onBeforeDraw();
			for (i = 0;i<this.drawingPoints.length;i++) {
				var pt:GeoBasedPoint = GeoBasedPoint(this.drawingPoints[i]);
				if (pt.link == null) {
					this.setPointCoords(pt);
				}else {
					this.setPointRegion(pt);
				}
				pt.checkThreshold();
				pt.applyProjection();
				pt.draw();
			}
		}
		
		public var globalBounds:Rectangle;
		
		override public function calculateResize(bounds:Rectangle):void {
			this.baseBounds = bounds.clone();
			super.calculateResize(bounds);
		}
		
		override public function resize(newBounds:Rectangle):void {
			this.calculateResize(newBounds);
			this.execResize();
		}
		
		override public function execResize():void {
			var newBounds:Rectangle = this.bounds;
			this.globalBounds = newBounds.clone();
			
			this.mapBorderContainer.graphics.clear();
			this.space.initBounds(this, newBounds, this.mapMargin);
			super.execResize();
			var i:uint;
			for (i = 0;i<this.visibleRegionsList.length;i++) {
				var region:MapRegion = this.visibleRegionsList[i];
				region.resize();
				if (this.mapSettings.regionsBorder != null)
					region.drawBorder(this.mapSettings.regionsBorder, this.mapBorderContainer.graphics);
			}
			this.regionLabelsManager.process();
			if (this.mapSettings.grid != null)
				this.mapSettings.grid.resize();
			this.navigationManager.initialize();
		}
		
		override protected function setContainersPositions(newBounds:Rectangle):void {
			super.setContainersPositions(newBounds);
		}
		
		public function setMapPixelCenterPosition(x:Number, y:Number):void {
			var rect:Rectangle = this.scrollableContainer.scrollRect.clone();
			rect.x = x - rect.width/2;
			rect.y = y - rect.height/2;
			this.scrollableContainer.scrollRect = rect;
			this.tooltipsContainer.x = -rect.x+this.bounds.x;
			this.tooltipsContainer.y = -rect.y+this.bounds.y;
		}
		
		override protected function onAfterThresholdsReset():void {
			for each (var global:GlobalSeriesSettings in this.settings) {
				global.reset();
			}
		}
		
		public var needRedrawMap:Boolean = false;
		
		override public function recalculateAndRedraw():void {
			super.recalculateAndRedraw();
			
			if (this.needRedrawMap) {
				for (var i:int = (this.visibleRegionsList.length-1);i>=0;i--) {
					var region:MapRegion = this.visibleRegionsList[i];
					region.checkThreshold();
					region.draw();
				}
				this.needRedrawMap = true;
			}
		}
		
	}
}