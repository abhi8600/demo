package com.anychart.gaugePlot.axis.labels {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.geom.Point;
	
	public final class CircularGaugeAxisLabels extends GaugeAxisLabels {
		private var rotateCircular:Boolean;
		private var autoOrientation:Boolean;
		
		public function CircularGaugeAxisLabels() {
			super();
			this.rotateCircular = true;
			this.autoOrientation = false;
		}
		
		override public function getPercentFontSize(size:Number):Number {
			return CircularGaugeAxis(this.axis).getPixelRadius(size);
		}
		
		override public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader):void {
			super.deserialize(data, axisWidth, resources);
			if (data.@rotate_circular != undefined) this.rotateCircular = SerializerBase.getBoolean(data.@rotate_circular);
			if (data.@auto_orientation != undefined) this.autoOrientation = SerializerBase.getBoolean(data.@auto_orientation);
		}
		
		private var isInverted:Boolean;
		override protected function setAngle(value:Number):void {
			if (this.rotateCircular) {
				var invert:Boolean = this.autoOrientation && Math.sin(value*Math.PI/180) > 0;
				this.text.rotation = (!invert) ? (this.text.rotation+value+90) : (this.text.rotation+value-90);
			}
		}
		
		override protected function clearAngle(value:Number):void {
			if (this.rotateCircular) {
				var invert:Boolean = this.autoOrientation && Math.sin(value*Math.PI/180) > 0;
				this.text.rotation = (!invert) ? (this.text.rotation-value-90) : (this.text.rotation-value+90);
			}
		}
		
		private function getRotateCircularPosition(info:TextElementInformation, 
												pixValue:Number,
												text:BaseTextElement,
												align:uint,
												padding:Number):Point {
			
			var pos:Point = new Point();
			pos.x = CircularGauge(this.axis.gauge).pixPivotPoint.x;
			pos.y = CircularGauge(this.axis.gauge).pixPivotPoint.y;
			
			this.clearAngle(pixValue);
			text.getBounds(info);
			var size:Number = info.rotatedBounds.height;
			this.setAngle(pixValue);
			text.getBounds(info);
			
			var r:Number = CircularGaugeAxis(this.axis).getRadius(align, padding, 0,true);
			
			switch (align) {
				case GaugeItemAlign.INSIDE:
					r -= size/2;
					break;
				case GaugeItemAlign.OUTSIDE:
					r += size/2;
					break;
			}
			
			var angle:Number = pixValue*Math.PI/180;
			var cos:Number = Math.round(Math.cos(angle)*1000)/1000;
			var sin:Number = Math.round(Math.sin(angle)*1000)/1000;
			pos.x += r * cos;
			pos.y += r * sin;
			
			pos.x -= info.rotatedBounds.width/2;
			pos.y -= info.rotatedBounds.height/2;
			
			return pos;
		}
		
		private function getFixedPosition(info:TextElementInformation, 
												pixValue:Number,
												text:BaseTextElement,
												align:uint,
												padding:Number):Point {
			var pos:Point = new Point();
			var r:Number = CircularGaugeAxis(this.axis).getRadius(align, padding, 0);
			
			var angle:Number = pixValue*Math.PI/180;
			var cos:Number = Math.round(Math.cos(angle)*1000)/1000;
			var sin:Number = Math.round(Math.sin(angle)*1000)/1000;
			
			var dx:Number = -info.rotatedBounds.width/2;
			var dy:Number = -info.rotatedBounds.height/2;
			
			switch (align) {
				case GaugeItemAlign.OUTSIDE:
					r += (info.rotatedBounds.width/2)*Math.abs(cos)+(info.rotatedBounds.height/2)*Math.abs(sin);
					break;
				case GaugeItemAlign.INSIDE:
					r -= (info.rotatedBounds.width/2)*Math.abs(cos)+(info.rotatedBounds.height/2)*Math.abs(sin);
					break;
			}
			
			pos.x = CircularGauge(this.axis.gauge).pixPivotPoint.x + r*cos + dx;
			pos.y = CircularGauge(this.axis.gauge).pixPivotPoint.y + r*sin + dy;
			
			return pos;
		}
		
		override public function getPosition(info:TextElementInformation, 
												pixValue:Number,
												text:BaseTextElement,
												align:uint,
												padding:Number):Point {
			if (text == this.text && this.rotateCircular)
				return this.getRotateCircularPosition(info, pixValue, text, align, padding);
			return this.getFixedPosition(info, pixValue, text, align, padding);
		}
		
		private function setCenter(text:BaseTextElement, info:TextElementInformation, sin:Number, cos:Number, pos:Point):void {
			var textAngle:Number = text.rotation*Math.PI/180;
			
			var txtSin:Number = Math.sin(textAngle);
			var txtCos:Number = Math.cos(textAngle);
			
			if (text != this.text) {
				
				if (align != GaugeItemAlign.CENTER) {
					var d:int = align == GaugeItemAlign.OUTSIDE ? 1 : -1;
					
					if (txtCos != 0) {
						pos.x += d*info.nonRotatedBounds.height/2 * cos/txtCos;
						pos.y += d*info.nonRotatedBounds.height/2 * sin/txtCos;
					}else {
						pos.x -= info.nonRotatedBounds.height/2;
						if (sin < 0)
							pos.y -= info.nonRotatedBounds.width;
					}
				}
			}
			
			//center
			if (txtSin > 0 && txtCos < 0)
				txtCos = -txtCos;
				
			if (txtSin < 0 && txtCos < 0) {
				txtCos = -txtCos;
				txtSin = -txtSin;
			}
			
			if (txtSin < 0 && txtCos > 0) {
				txtSin = -txtSin;
			}
			
			pos.x -= (info.nonRotatedBounds.width/2*txtSin + info.nonRotatedBounds.height/2*txtCos);
			pos.y -= (info.nonRotatedBounds.width/2*txtCos + info.nonRotatedBounds.height/2*txtSin);
		}
	}
}