package com.anychart.gaugePlot.axis {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	
	internal class ScaleBar {
		internal var shape:uint;
		internal var width:Number;
		internal var padding:Number;
		internal var align:uint;
		internal var background:BackgroundBase;
		
		internal var container:Shape;
		
		private var isExtra:Boolean;
		
		public function ScaleBar(isExtra:Boolean) {
			this.background = new BackgroundBase();
			this.width = .05;
			this.padding = 0;
			this.align = GaugeItemAlign.CENTER;
			this.shape = ScaleBarShapeType.BAR;	
			this.isExtra = isExtra;		
		}
		
		public function initialize():DisplayObject {
			this.container = new Shape();
			return this.container;
		}
		
		public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader):void {
			this.background.deserialize(data, resources);
			if (isExtra) {
				this.shape = ScaleBarShapeType.LINE;
				this.background = new BackgroundBase();
				this.background.border = new Stroke();
				this.background.border.deserialize(data);
			}else {
				if (data.@shape != undefined) {
					switch (SerializerBase.getEnumItem(data.@shape)) {
						case "line": this.shape = ScaleBarShapeType.LINE; break;
						case "bar": this.shape = ScaleBarShapeType.BAR; break;
					}
				}
			}
			if (data.@size != undefined) this.width = GaugePlotSerializerBase.getAxisSizeDependValue(data.@size, axisWidth);
			if (data.@padding != undefined) this.padding = GaugePlotSerializerBase.getSimplePercent(data.@padding);
			if (data.@align != undefined) this.align = GaugePlotSerializerBase.getAlign(data.@align);

			if (!this.background.enabled) return;
		}
	}
}