package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeType;
	import com.anychart.serialization.SerializerBase;
	
	public final class GaugePlotSerializerBase extends SerializerBase {
		
		public static function getAxisSizeDependValue(value:*, axisSize:Number):Number {
			var w:String = SerializerBase.getString(value);
			if (w.indexOf('%') == 0) {
				var res:Number = axisSize;
				if (w.indexOf('*') != -1)
					res *= Number(w.substr(w.indexOf('*')+1));
				return res;
			}
			return GaugePlotSerializerBase.getSimplePercent(w);
		}
		
		public static function isAxisSizeDepend(value:*):Boolean {
			return SerializerBase.getString(value).indexOf('%') == 0;
		}
		
		public static function getAxisSizeFactor(value:*):Number {
			var s:String = SerializerBase.getString(value);
			if (s.indexOf('*') == -1)
				return 1;
			return Number(s.substr(s.indexOf('*')+1));
		}
		
		public static function getSimplePercent(value:*):Number {
			return SerializerBase.getNumber(value)/100;
		}
		
		public static function getAlign(value:*):uint {
			switch (SerializerBase.getLString(value)) {
				case "inside": return GaugeItemAlign.INSIDE;
				case "outside": return GaugeItemAlign.OUTSIDE;
				default: return GaugeItemAlign.CENTER;
			}
		}
		
		public static function getAngle(value:*):Number {
			/* var angle:Number = SerializerBase.getNumber(value);
			if (angle > 360)
				angle -= Math.floor(angle/360)*360;
			if (angle < 360)
				angle += (Math.floor(Math.abs(angle)/360)+1)*360;
			return angle; */
			return SerializerBase.getNumber(value);
		}
		
		public static function shapeIsRectangle(type:uint):Boolean {
			return (type == ShapeType.RECTANGLE || 
					type == ShapeType.TRIANGLE ||
					type == ShapeType.TRAPEZOID ||
					type == ShapeType.LINE);
		}
		
		public static function getShapeType(value:*):uint {
			switch (SerializerBase.getEnumItem(value)) {
				case "rectangle": return ShapeType.RECTANGLE;
				case "triangle": return ShapeType.TRIANGLE;
				case "trapezoid": return ShapeType.TRAPEZOID;
				case "pentagon": return ShapeType.PENTAGON;
				case "line": return ShapeType.LINE;
			}
			return SerializerBase.getMarkerType(value);
		}
	}
}