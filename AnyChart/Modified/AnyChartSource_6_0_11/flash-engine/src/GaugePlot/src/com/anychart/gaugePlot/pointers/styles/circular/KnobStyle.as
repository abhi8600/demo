package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.circular.CircularGaugePointer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.styles.IStyle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.background.BackgroundBase;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class KnobStyle implements IStyleSerializableRes {
		private var radius:Number;
		private var isRadiusAxisSizeDepend:Boolean;
		
		private var isGearHeightAxisSizeDepend:Boolean;
		
		private var background:BackgroundBase;
		
		private var elementsCount:uint = 12;
		private var dW:Number = .03;
		
		public function KnobStyle() {
			this.background = new BackgroundBase();
			this.radius = .5;
			this.isRadiusAxisSizeDepend = false;
			this.isGearHeightAxisSizeDepend = false;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			this.background.deserialize(data, resources, style);
			if (data.@radius != undefined) {
				this.isRadiusAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@radius);
				if (this.isRadiusAxisSizeDepend)
					this.radius = GaugePlotSerializerBase.getAxisSizeFactor(data.@radius);
				else
					this.radius = GaugePlotSerializerBase.getSimplePercent(data.@radius);
			}
			if (data.@gear_height != undefined) {
				this.isGearHeightAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@gear_height);
				if (this.isRadiusAxisSizeDepend)
					this.dW = GaugePlotSerializerBase.getAxisSizeFactor(data.@gear_height);
				else
					this.dW = GaugePlotSerializerBase.getSimplePercent(data.@gear_height);
			}
		}
		
		public function draw(container:Shape, pointer:CircularGaugePointer, axis:CircularGaugeAxis):void {
			container.filters = this.background.effects == null ? null : this.background.effects.list;
			var g:Graphics = container.graphics;
			
			var percentR:Number = (this.isRadiusAxisSizeDepend ? axis.width : 1)*this.radius;
			var dW:Number = (this.isGearHeightAxisSizeDepend ? axis.width : 1)*this.dW; 
			
			var innerR:Number = axis.getPixelRadius(percentR);
			var outerR:Number = axis.getPixelRadius(percentR + dW);
			
			var center:Point = CircularGauge(axis.gauge).pixPivotPoint;
			container.x = center.x;
			container.y = center.y;
			var angleStep:Number = 360/elementsCount;
			
			var bounds:Rectangle = new Rectangle(- outerR,- outerR, outerR*2, outerR*2);
			
			if (this.background.fill != null)
				this.background.fill.begin(g, bounds, pointer.color);
			if (this.background.border != null)
				this.background.border.apply(g, bounds, pointer.color);
			this.drawKnob(g, new Point(0,0), angleStep, innerR, outerR);
			g.endFill();
			g.lineStyle();
			if (this.background.hatchFill != null) {
				this.background.hatchFill.beginFill(g, pointer.hatchType, pointer.color);
				this.drawKnob(g, new Point(0,0), angleStep, innerR, outerR);
				g.endFill();
			}
		}
		
		private function drawKnob(g:Graphics, center:Point, angleStep:Number, innerR:Number, outerR:Number):void {
			var startAngle:Number = 90-angleStep/2;
			var isInner:Boolean = false;
			
			var startPt:Point = new Point();
			startPt.x = center.x + outerR*Math.cos(startAngle*Math.PI/180);
			startPt.y = center.y + outerR*Math.sin(startAngle*Math.PI/180);
			
			g.moveTo(startPt.x, startPt.y);
			for (var i:uint = 0;i<elementsCount;i++) {
				var actualR:Number = isInner ? innerR : outerR;
				
				var start:Number = startAngle + angleStep*i;
				var end:Number = start+angleStep;
				
				DrawingUtils.drawArc(g, center.x, center.y, start, end,actualR, actualR, 0, false); 
				isInner = !isInner;
			}
			g.lineTo(startPt.x, startPt.y);
		}
	}
}