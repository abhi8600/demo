package com.anychart.gaugePlot.axis.scales {
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	
	public class LogGaugeScale extends BaseScale {
		private var transformedMin:Number;
		private var transformedMax:Number;
		
		public var logBase:Number = 10;
		
		private static var minorLogVals:Array = new Array(0,0.301029995663981, 0.477121254719662, 0.602059991327962,
										  				   0.698970004336019, 0.778151250383644, 0.845098040014257,
										  				   0.903089986991944, 0.954242509439325, 1);
		
		override public function calculate():void {
			
			if (isNaN(this.dataRangeMinimum) && isNaN(this.dataRangeMaximum) &&
				this.isMinimumAuto && this.isMaximumAuto) {
					
				this.isMinimumAuto = false;
				this.minimum = 1;
				
				this.isMaximumAuto = false;
				this.maximum = 10;
			}
			
			super.calculate();
			if (this.isMajorIntervalAuto)
				this.majorInterval = 1;
				
			if (this.minimum <= 0 && this.maximum <= 0) {
				this.minimum = 1;
				this.maximum = this.logBase;
			}else if (this.minimum <= 0) {
				this.minimum = this.maximum/this.logBase;
			}else if (this.maximum <= 0.0) {
				this.maximum = this.minimum*this.logBase;
			}

			if (this.maximum - this.minimum < 1.0e-20) {
				if ( this.isMaximumAuto )
					this.maximum *= 2;
				if ( this.isMinimumAuto )
					this.minimum /= 2;
			}

			if (this.isMinimumAuto)
				this.minimum = Math.pow(this.logBase,Math.floor(Math.log(this.minimum )/Math.log(this.logBase)));
			if ( this.isMaximumAuto )
				this.maximum = Math.pow(this.logBase,Math.ceil(Math.log(this.maximum)/Math.log(this.logBase)));
			
			this.transformedMin = this.safeLog(this.minimum);
			this.transformedMax = this.safeLog(this.maximum);
			
			this.calculateBaseValue();
			this.calculateMinorStart();
			this.calculateTicksCount();
			
			/* if (this.axis.labels != null)
				this.axis.labels.initMaxLabelSize(); */
		}
		
		override protected function calculateTicksCount():void {
			
			this._majorIntervalsCount = int(Math.floor( this.safeLog(this.maximum) + 1.0e-12))-
						  				int(Math.ceil( this.baseValue - 1.0e-12));
			if (this.getMajorIntervalValue(this._majorIntervalsCount) > Math.ceil(this.safeLog(this.maximum)))
				this._majorIntervalsCount--; 
			if (this._majorIntervalsCount < 1)
				this._majorIntervalsCount = 1;
				
			this._minorIntervalsCount = 9;
		}
		
		override protected function calculateBaseValue():void {
			if (!this.isBaseValueAuto) return;
			this.baseValue = Math.ceil(this.safeLog(this.minimum) - 0.00000001);
		}
		
		override public function transform(value:Number):Number {
			var ratio:Number = (safeLog(value) - this.transformedMin)/(this.transformedMax - this.transformedMin);
			var pixelOffset:Number = (this.pixelRangeMaximum - this.pixelRangeMinimum) * ratio;
			
			if (this.inverted)
				return this.pixelRangeMaximum -  pixelOffset;
			return this.pixelRangeMinimum + pixelOffset;
		}
		
		override public function transformToValue(pixValue:Number):Number {
			var ratio:Number = (pixValue - this.pixelRangeMinimum)/(this.pixelRangeMaximum - this.pixelRangeMinimum);
			var offset:Number = (this.transformedMax - this.transformedMin) * ratio;
			
			var pixMax:Number = this.transformedMax;
			var pixMin:Number = this.transformedMin;
			
			var val:Number = (this.inverted) ? (pixMax - offset) : (pixMin + offset);
			val = Math.round(val*1000)/1000;
			var max:Number = Math.round(this.transformedMax*1000)/1000;
			if (val < this.transformedMin)
				val = this.transformedMin;
			else if (val > max)
				val = max;
			return this.delinearize(val);
		}
		
		override public function localTransform(value:Number, isZeroAtAxisZero:Boolean = false):Number {
			var ratio:Number = (value - this.transformedMin)/( this.transformedMax - this.transformedMin);
			var pixelOffset:Number = (this.pixelRangeMaximum - this.pixelRangeMinimum) * ratio;
			
			var pixMax:Number = isZeroAtAxisZero ? (this.pixelRangeMaximum - this.pixelRangeMinimum) : this.pixelRangeMaximum;
			var pixMin:Number = isZeroAtAxisZero ? 0 : this.pixelRangeMinimum;
			
			return (this.inverted) ? (pixMax - pixelOffset) : (pixMin + pixelOffset);
		}
		
		override public function delinearize(value:Number):Number {
			return Math.pow(this.logBase, value);
		}
		
		override public function linearize(value:Number):Number {
			return this.safeLog(value);
		}
		
		override public function getMajorIntervalValue(index:Number):Number {
			return this.baseValue + index;
		}
		
		override public function getMinorIntervalValue(baseValue:Number, index:int):Number {
			return baseValue + Math.floor(Number(index)/9.0 ) + minorLogVals[( index + 9 ) % 9];
		}
		
		override protected function calculateMinorStart():void {
			this._minorStart = -9;
		}
		
		private function safeLog(value:Number):Number {
			return ( value > 1.0e-20) ? Math.log(value)/Math.log(this.logBase) : 0;
		}
		
		override public function deserialize(data:XML):void {
			if (data == null) return;
			super.deserialize(data);
			if (data.@log_base != undefined)
				if (data.@log_base.toLowerCase() == "e")
					this.logBase = Math.E;
				else
					this.logBase = SerializerBase.getNumber(data.@log_base);
		}
	}
}