package com.anychart.gaugePlot.frame {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class CircularGaugeFrameAuto extends CircularGaugeAutoFrameBase {
		
		override protected function execDrawing(g:Graphics, thickness:Number):void {
			
		 	var gauge:CircularGauge = CircularGauge(this.gauge);
		 	var r:Number = gauge.frameRadius;
		 	var pixPivotRadius:Number = gauge.maxCapRadius*r;
		 	var pixPadding:Number = this.padding*r;
		 	var pixThickness:Number = thickness*r;
            var startEndRadius:Number = pixPadding + pixThickness;
            
            var startAngle:Number = gauge.minAngle;
            var endAngle:Number = gauge.maxAngle;
            var startRadAngle:Number = startAngle*Math.PI/180;
            var endRadAngle:Number = endAngle*Math.PI/180;
            
            var startCenterPt:Point = new Point();
            startCenterPt.x = gauge.pixPivotPoint.x + (r * Math.cos(startRadAngle));
            startCenterPt.y = gauge.pixPivotPoint.y + (r * Math.sin(startRadAngle));
            var endCenterPt:Point = new Point();
            endCenterPt.x = gauge.pixPivotPoint.x + (r * Math.cos(endRadAngle));
            endCenterPt.y = gauge.pixPivotPoint.y + (r * Math.sin(endRadAngle));
            
            var startPtRect:Rectangle = new Rectangle(startCenterPt.x, startCenterPt.y,0, 0);
            startPtRect.inflate(startEndRadius, startEndRadius);
            
            var endPtRect:Rectangle = new Rectangle(endCenterPt.x, endCenterPt.y, 0, 0);
            endPtRect.inflate(startEndRadius, startEndRadius);
            
            var pivotRect:Rectangle = new Rectangle(gauge.pixPivotPoint.x, gauge.pixPivotPoint.y, 0, 0);
            pivotRect.inflate(startEndRadius + pixPivotRadius, startEndRadius + pixPivotRadius);
            
            var fullRect:Rectangle = new Rectangle(gauge.pixPivotPoint.x, gauge.pixPivotPoint.y, 0, 0);
            fullRect.inflate(r + startEndRadius, r + startEndRadius);
            
            var sweepAngle:Number = endAngle - startAngle;
            
            if (sweepAngle >= 320) {
            	g.drawCircle(gauge.pixPivotPoint.x, gauge.pixPivotPoint.y, r+startEndRadius);
            }else if (sweepAngle > 270) {
            	var correction:Number = 90 - ((360 - sweepAngle) / 2);
                this.renderRectArc(g,startPtRect, (startAngle + 270) + correction, 90 - correction,true);
                this.renderRectArc(g,fullRect, startAngle, sweepAngle);
                this.renderRectArc(g,endPtRect, endAngle, 90 - correction);
            }else {
	            this.renderRectArc(g,startPtRect, (startAngle + 270), 90,true);
	            this.renderRectArc(g,fullRect, startAngle, sweepAngle);
	            this.renderRectArc(g,endPtRect, endAngle, 90);
	            this.renderRectArc(g,pivotRect, endAngle+45, (360 - sweepAngle) - 90);
			}
		 }
		 
		 private function renderRectArc(g:Graphics,rect:Rectangle,startAngle:Number,sweepAngle:Number,needMoveTo:Boolean=false):void {
        	DrawingUtils.drawArc(g,rect.x+rect.width/2,rect.y+rect.height/2,startAngle,startAngle+sweepAngle, rect.height/2,rect.width/2,0,needMoveTo);
         }
	}
}