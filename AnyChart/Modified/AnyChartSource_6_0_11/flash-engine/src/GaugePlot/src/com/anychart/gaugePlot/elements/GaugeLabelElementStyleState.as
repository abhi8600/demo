package com.anychart.gaugePlot.elements {
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.styles.Style;
	import com.anychart.visual.text.BaseTextElement;
	
	public final class GaugeLabelElementStyleState extends LabelElementStyleState {
		
		public function GaugeLabelElementStyleState(style:Style) {
			super(style);
		}
		
		override protected function createLabel():BaseTextElement {
			return new GaugeBaseTextElement();
		}
		
	}
}