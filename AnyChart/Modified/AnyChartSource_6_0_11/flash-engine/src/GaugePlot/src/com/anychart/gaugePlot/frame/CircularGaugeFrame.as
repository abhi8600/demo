package com.anychart.gaugePlot.frame {
	import com.anychart.gaugePlot.CircularGauge;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class CircularGaugeFrame extends GaugeFrame {
		
		protected function getThickness():Number {
			var thickness:Number = 0;
            if (this.innerStroke != null && this.innerStroke.background.enabled)
            	thickness += this.innerStroke.thickness;
            if (this.outerStroke != null && this.outerStroke.background.enabled)
            	thickness += this.outerStroke.thickness;
            return thickness;
		}
		
		override public function autoFit(gaugeRect:Rectangle):void {
			
			var gauge:CircularGauge = CircularGauge(this.gauge);
			
			var cR:Number = 1+this.getThickness()+this.padding;
			
			var w:Number = cR*2;
			var h:Number = cR*2;
			
			var r:Number = Math.min(gauge.bounds.width/w,gauge.bounds.height/h);
            var newW:Number = w*r;
            var newH:Number = h*r;
			
			gauge.bounds.x = gauge.bounds.x + (gauge.bounds.width - newW)/2
            gauge.bounds.y = gauge.bounds.y + (gauge.bounds.height - newH)/2;
            gauge.bounds.width = newW;
            gauge.bounds.height =  newH;
            
            gauge.maxPixelRadius = r*2;
            gauge.frameRadius = r;
            
            gauge.pivotPoint.x = .5;
            gauge.pivotPoint.y = .5;
		}
		
		protected function execDrawing(g:Graphics, thickness:Number):void {
			var gauge:CircularGauge = CircularGauge(this.gauge);
			var r:Number = gauge.frameRadius;
			var pixPadding:Number = this.padding*r;
		 	var pixThickness:Number = thickness*r;
            var startEndRadius:Number = pixPadding + pixThickness;
			g.drawCircle(gauge.pixPivotPoint.x, gauge.pixPivotPoint.y, r+startEndRadius);
		}
		
		override public function getMask():Shape {
			var mask:Shape = this.mask;
			if (mask != null) {
				var g:Graphics = mask.graphics;
				g.clear();
				g.beginFill(0xFFFFFF,1);
				this.execDrawing(g, 0);
				g.endFill();
			}
			return mask;
		}
		
		override public function draw():void {
			super.draw();

            var innerThickness:Number=0;
            if(innerStroke!=null && innerStroke.background.enabled) innerThickness=innerStroke.thickness;
            
            var outerThickness:Number=0;
            if(outerStroke!=null && outerStroke.background.enabled) outerThickness=outerStroke.thickness;

			var g:Graphics = this.backgrondShape.graphics;

            if(this.background != null) {
            	if(this.background.fill!=null)
            		this.background.fill.begin(g,bounds);
            	else
            		g.beginFill(0,0);
            		
            	if(this.background.border!=null)
            		this.background.border.apply(g,bounds);
            		            	
            	this.execDrawing(g,0);
            	
            	g.endFill();
            	g.lineStyle();
            }
            
            g = this.outerStrokeShape.graphics;
            if (this.outerStroke != null) {
            	if(this.outerStroke.background.fill!=null)
            		this.outerStroke.background.fill.begin(g,bounds);
            	else
            		g.beginFill(0,0);
            		
            	if(this.outerStroke.background.border!=null)
            		this.outerStroke.background.border.apply(g,bounds);
            		        
				this.execDrawing(g,innerThickness);
				this.execDrawing(g,innerThickness+outerThickness);
				
            	g.endFill();
            	g.lineStyle();
            }
            
            g = this.innerStrokeShape.graphics;
            if (this.innerStroke!=null && this.innerStroke.background.enabled) {
            	if(this.innerStroke.background.fill!=null)
            		this.innerStroke.background.fill.begin(g,bounds);
            	else
            		g.beginFill(0,0);
            		
            	if(this.innerStroke.background.border!=null)
            		this.innerStroke.background.border.apply(g,bounds);
            		
            	this.execDrawing(g,0);
            	this.execDrawing(g,innerThickness);
            	
            	g.endFill();
            	g.lineStyle();
            }
		}
	}
}