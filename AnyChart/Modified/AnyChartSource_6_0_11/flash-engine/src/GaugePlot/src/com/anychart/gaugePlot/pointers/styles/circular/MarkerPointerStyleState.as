package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.pointers.styles.MarkerStyle;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	
	public final class MarkerPointerStyleState extends CircularGaugePointerStyleState {
		
		public var marker:MarkerStyle;
		
		public function MarkerPointerStyleState(style:Style) {
			super(style);
			this.marker = new MarkerStyle(false);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.marker.deserialize(data, resources, this.style);
			return data;
		}
	}
}