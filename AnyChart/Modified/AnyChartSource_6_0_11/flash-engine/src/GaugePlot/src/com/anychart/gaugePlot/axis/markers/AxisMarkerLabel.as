package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	
	internal final class AxisMarkerLabel implements IStyleSerializableRes {
		internal var align:uint;
		internal var padding:Number;
		
		internal var label:GaugeBaseTextElement;
		
		internal var fontSize:Number;
		internal var isDynamicFontSize:Boolean;
		internal var isFontSizeAxisDepend:Boolean;
		
		internal var value:Number;
		
		public function AxisMarkerLabel() {
			this.align = GaugeItemAlign.CENTER;
			this.padding = 0;
			this.label = new GaugeBaseTextElement(); 
			this.isDynamicFontSize = false;
			this.isFontSizeAxisDepend = false;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@align != undefined) this.align = GaugePlotSerializerBase.getAlign(data.@align);
			if (data.@padding != undefined) this.padding = GaugePlotSerializerBase.getSimplePercent(data.@padding);
			
			if (data.font[0] != null && data.font[0].@size != undefined && data.font[0].@family != undefined) {
				var family:String = SerializerBase.getString(data.font[0].@family);
				if (family.indexOf(".swf") == family.length - 4) {
					this.isDynamicFontSize = true;
					var size:String = SerializerBase.getLString(data.font[0].@size);
					this.isFontSizeAxisDepend = GaugePlotSerializerBase.isAxisSizeDepend(size);
					data.font[0].@size = undefined;
					this.fontSize = (this.isFontSizeAxisDepend) ? GaugePlotSerializerBase.getAxisSizeFactor(size) : GaugePlotSerializerBase.getSimplePercent(size);
				}
			}
			
			if (data.@value != undefined) this.value = SerializerBase.getNumber(data.@value);
			
			this.label = new GaugeBaseTextElement();
			this.label.deserialize(data, resources, style);
		}
	}
}