package com.anychart.gaugePlot.axis {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.LinearGauge;
	import com.anychart.gaugePlot.axis.labels.GaugeAxisLabels;
	import com.anychart.gaugePlot.axis.labels.LinearGaugeAxisLabels;
	import com.anychart.gaugePlot.axis.markers.AxisMarkerTickmark;
	import com.anychart.gaugePlot.axis.markers.AxisMarkersList;
	import com.anychart.gaugePlot.axis.markers.LinearGaugeAxisMarkersList;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeType;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.StylesList;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class LinearGaugeAxis extends GaugeAxis {
		private var position:Number;
		public var startMargin:Number;
		public var endMargin:Number;
		
		public var isHorizontal:Boolean;
		
		private var pixelPosition:Number;
		
		public function LinearGaugeAxis() {
			super();
			this.isHorizontal = false;
			this.position = .5;
			this.startMargin = .05;
			this.endMargin = .05;
		}
		
		override protected function createLabels():GaugeAxisLabels { return new LinearGaugeAxisLabels(); }
		override protected function createMarkers():AxisMarkersList { return new LinearGaugeAxisMarkersList(); }
		
		override public function deserialize(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, localStyles, styles, resources);
			if (!this.isHorizontal)
				this.scale.inverted = !this.scale.inverted;
				
			this.checkTickmark(this.majorTickmark);
			this.checkTickmark(this.minorTickmark);
			if (data.@position != undefined) this.position = GaugePlotSerializerBase.getSimplePercent(data.@position);
			if (data.@start_margin != undefined) this.startMargin = GaugePlotSerializerBase.getSimplePercent(data.@start_margin);
			if (data.@end_margin != undefined) this.endMargin = GaugePlotSerializerBase.getSimplePercent(data.@end_margin);
		}
		
		public function checkTickmark(tickmark:Tickmark):void {
			if (tickmark != null) {
				if (this.isHorizontal) {
					var tmp:Number = tickmark.width;
					tickmark.width = tickmark.height;
					tickmark.height = tmp;
					if (tickmark is AxisMarkerTickmark) {
						var tmp1:Boolean = AxisMarkerTickmark(tickmark).isWidthAxisSizeDepend;
						AxisMarkerTickmark(tickmark).isWidthAxisSizeDepend = AxisMarkerTickmark(tickmark).isHeightAxisSizeDepend;
						AxisMarkerTickmark(tickmark).isHeightAxisSizeDepend = tmp1;
					}  
				}else if (tickmark.shape == ShapeType.LINE)
					tickmark.shape = ShapeType.H_LINE;
			}
		}
		
		override public function initializeSize():void {
			
			super.initializeSize();
			
			this.pixelPosition = (this.isHorizontal ? this.gauge.bounds.y : this.gauge.bounds.x) 
								 + this.getLogicalTargetSize() * this.position;
			
			var w:Number = this.getLogicalTargetSize(false);
			var start:Number = this.startMargin * w;
			var end:Number = this.endMargin * w;
			
			var gauge:LinearGauge = LinearGauge(this.gauge);
			
			var w1:Number = this.getTargetSize();
			start = Math.max(start, gauge.startMargin*w1);
			end = Math.max(end, gauge.endMargin*w1);
			
			this.pixelWidth = this.width*w1;
			
			if (this.isHorizontal) {
				start += this.gauge.bounds.x;
				end = this.gauge.bounds.right - end;
			}else {
				start += this.gauge.bounds.y;
				end = this.gauge.bounds.bottom - end;
			}
			
			this.scale.setPixelRange(start, end);
			this.scale.calculate();
		}
		
		private function getLogicalTargetSize(perpendicular:Boolean = true):Number {
			return ((this.isHorizontal && !perpendicular) ||
					(!this.isHorizontal && perpendicular)) ?
						this.gauge.bounds.width : this.gauge.bounds.height;
		}
		
		public function getTargetSize():Number {
			//return this.isHorizontal ? this.gauge.bounds.height : this.gauge.bounds.width;
			return Math.min(this.gauge.bounds.width, this.gauge.bounds.height);
		}
		
		public function setPoint(position:Number, pixelValue:Number, point:Point):void {
			if (this.isHorizontal) {
				point.x = pixelValue;
				point.y = position;
			}else {
				point.x = position;
				point.y = pixelValue;
			}
		}
		
		public function getComplexPosition(align:uint, padding:Number, width:Number, isFirstPoint:Boolean):Number {
			var w:Number = this.getTargetSize();
			switch (align) {
				case GaugeItemAlign.INSIDE:
					return this.pixelPosition - w * (padding + (isFirstPoint ? 0 : width)) - this.pixelWidth/2;
				case GaugeItemAlign.OUTSIDE:
					return this.pixelPosition + w * (padding + (isFirstPoint ? 0 : width)) + this.pixelWidth/2;
				case GaugeItemAlign.CENTER:
				default:
					return this.pixelPosition + w * width * (isFirstPoint ? (-.5) : (.5));
			}
		}
		
		public function getPosition(align:uint, padding:Number, width:Number, isPixelWidth:Boolean = false):Number {
			var w:Number = this.getTargetSize();
			switch (align) {
				case GaugeItemAlign.INSIDE:
					return this.pixelPosition - w * (isPixelWidth ? padding : (padding + width)) - (isPixelWidth ? width : 0) - this.pixelWidth/2;
				case GaugeItemAlign.OUTSIDE:
					return this.pixelPosition + w * padding + this.pixelWidth/2;
				case GaugeItemAlign.CENTER:
				default:
					return this.pixelPosition - (isPixelWidth ? 1 : w) * width/2 + w*padding;
			}
		}
		
		override protected function drawScaleBar(scaleBar:ScaleBar):void {
			var g:Graphics = scaleBar.container.graphics;
			g.clear();
			var sRect:Rectangle = new Rectangle(this.gauge.bounds.x, this.gauge.bounds.y);
			var w:Number = this.getTargetSize();
			var start:Number;
			var h:Number;
			if (scaleBar.shape == ScaleBarShapeType.BAR) {
			 	start = this.getPosition(scaleBar.align, scaleBar.padding, scaleBar.width);
			 	h = w*scaleBar.width;
			}else {
				start = this.getPosition(scaleBar.align, scaleBar.padding, 0);
				h = 0;
			}
			
			var startMargin:Number = this.startMargin * this.getLogicalTargetSize(false);
			var endMargin:Number = this.endMargin * this.getLogicalTargetSize(false);
			
			var w1:Number = this.getTargetSize();
			
			var gauge:LinearGauge = LinearGauge(this.gauge);
			
			startMargin = Math.max(startMargin, gauge.startMargin*w1);
			endMargin = Math.max(endMargin, gauge.endMargin*w1);
			
			if (this.isHorizontal) {
				sRect.y = start;
				sRect.height = h;
				sRect.x += startMargin;
				sRect.width = this.gauge.bounds.width - startMargin - endMargin; 
			}else {
				sRect.x = start;
				sRect.width = h;
				sRect.y += startMargin;
				sRect.height = this.gauge.bounds.height - startMargin - endMargin;
			}
			
			if (scaleBar.shape == ScaleBarShapeType.BAR) {
				
				if (scaleBar.background.fill != null)
					scaleBar.background.fill.begin(g, sRect);
				
				if (scaleBar.background.border != null)
					scaleBar.background.border.apply(g, sRect);
				
				g.drawRect(sRect.x, sRect.y, sRect.width, sRect.height);
				
				g.endFill();
				
				if (scaleBar.background.hatchFill != null) {
					scaleBar.background.hatchFill.beginFill(g);
					g.drawRect(sRect.x, sRect.y, sRect.width, sRect.height);
					g.endFill();
				}
				
			}else if (scaleBar.background.border != null) {
				scaleBar.background.border.apply(g, sRect);
				g.moveTo(sRect.left, sRect.top);
				g.lineTo(sRect.right, sRect.bottom);
			}
			g.lineStyle();
			
			if (scaleBar.background.effects != null)
				scaleBar.container.filters = scaleBar.background.effects.list;
		}
		
		override public function drawTickmark(tickmark:Tickmark, value:Number, color:uint = 0):void {
			
			var r:Number = this.getTargetSize();
			
			var w:Number = tickmark.width * r;
			var h:Number = tickmark.height * r;
			
			var x:Number = 0;
			var y:Number = 0;
			
			if (this.isHorizontal) {
				y = this.getPosition(tickmark.align, tickmark.padding, h, true);
				x = value - w/2;
			}else {
				x = this.getPosition(tickmark.align, tickmark.padding, w, true);
				y = value - h/2;
			}
			var td:DisplayObject = tickmark.draw(x, y, w, h, color,false);
			td.rotation += tickmark.rotation;
		}
	}
}