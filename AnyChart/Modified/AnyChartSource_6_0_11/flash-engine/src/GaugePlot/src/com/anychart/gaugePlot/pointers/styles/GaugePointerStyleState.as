package com.anychart.gaugePlot.pointers.styles {
	import com.anychart.gaugePlot.AxisGaugeBase;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	public class GaugePointerStyleState extends BackgroundBaseStyleState {
		
		public var align:uint;
		public var padding:Number;
		public var underElements:Boolean;
		
		public function GaugePointerStyleState(style:Style) {
			super(style);
			this.align = GaugeItemAlign.CENTER;
			this.padding = 0;
			this.underElements = false;
		}
		
		public function initializeRotation(gauge:AxisGaugeBase):void {
			if (this.fill != null) gauge.setGradientRotation(this.fill.gradient);
			if (this.stroke != null) gauge.setGradientRotation(this.stroke.gradient);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.deserializePosition(data);
			return data;
		}
		
		protected function deserializePosition(data:XML):void {
			if (data.@align != undefined) {
				switch (SerializerBase.getEnumItem(data.@align)) {
					case "inside": this.align = GaugeItemAlign.INSIDE; break;
					case "outside": this.align = GaugeItemAlign.OUTSIDE; break;
					case "center": this.align = GaugeItemAlign.CENTER; break;
				}
			}
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding)/100;
			if (data.@under_elements != undefined) this.underElements = SerializerBase.getBoolean(data.@under_elements);
		}
		
		protected function isAxisSizeDepend(value:*):Boolean {
			return SerializerBase.getString(value).indexOf('%') == 0;
		}
		
		protected function getAxisSizeFactor(value:*):Number {
			var s:String = SerializerBase.getString(value);
			if (s.indexOf('*') == -1)
				return 1;
			return Number(s.substr(s.indexOf('*')+1));
		}
	}
}