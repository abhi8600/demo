package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.circular.BarPointerStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	 
	public class BarCircularGaugePointer extends CircularGaugePointer {
		
		override protected function getStyleNodeName():String {
			return "bar_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return BarPointerStyleState;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var state:BarPointerStyleState = BarPointerStyleState(this.currentState);
			this.drawBar(state.startFromZero ? 0 : this.axis.scale.minimum, this.fixValueScaleOut(this.value));
		}
		
		private var startAngle:Number;
		private var endAngle:Number;
		private var innerRadius:Number;
		private var outerRadius:Number;
		
		protected function drawBar(startValue:Number, endValue:Number):void {
			var g:Graphics = this.pointerShape.graphics;
			
			var state:BarPointerStyleState = BarPointerStyleState(this.currentState);
			
			this.startAngle = this.axis.scale.transform(startValue);
			this.endAngle = this.axis.scale.transform(endValue);
			
			var center:Point = CircularGauge(this.axis.gauge).pixPivotPoint;
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			
			var w:Number = (state.isAxisSizeWidth ? this.axis.width : 1)*state.width;
			
			this.innerRadius = axis.getComplexRadius(state.align, state.padding, w, true);
			this.outerRadius = axis.getComplexRadius(state.align, state.padding, w, false);
			
			var bounds:Rectangle = new Rectangle();
			
			if (state.fill != null)
				state.fill.begin(g, bounds, this._color);
				
			if (state.stroke != null)
				state.stroke.apply(g, bounds, this._color);
				
			this.drawBox(g, center, startAngle, endAngle, innerRadius, outerRadius);
			g.lineStyle();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, this._hatchType, this._color);
				this.drawBox(g, center, startAngle, endAngle, innerRadius, outerRadius);
			}
		}
		
		private function drawBox(g:Graphics, center:Point, startAngle:Number, endAngle:Number, innerR:Number, outerR:Number):void {
			DrawingUtils.drawArc(g, center.x, center.y, startAngle, endAngle, innerR, innerR, 0, true);
			DrawingUtils.drawArc(g, center.x, center.y, endAngle, startAngle, outerR, outerR, 0, false);
			g.endFill();
		}
		
		override public function getAnchorPoint(anchor:uint, pos:Point):void {
			var center:Point = CircularGauge(this.axis.gauge).pixPivotPoint;
			pos.x = center.x;
			pos.y = center.y;
			var angle:Number;
			var r:Number;
			switch (anchor) {
				case Anchor.CENTER:
					angle = (this.startAngle+this.endAngle)/2;
					r = (this.innerRadius+this.outerRadius)/2; 
					break;
				case Anchor.CENTER_BOTTOM:
					angle = (this.startAngle+this.endAngle)/2;
					r = this.innerRadius;
					break;
				case Anchor.CENTER_LEFT:
					angle = this.startAngle;
					r = (this.innerRadius+this.outerRadius)/2;
					break;
				case Anchor.CENTER_RIGHT:
					angle = this.endAngle;
					r = (this.innerRadius+this.outerRadius)/2;
					break;
				case Anchor.CENTER_TOP:
					angle = (this.startAngle+this.endAngle)/2;
					r = this.outerRadius;
					break;
				case Anchor.LEFT_BOTTOM:
					angle = this.startAngle;
					r = this.innerRadius;
					break;
				case Anchor.LEFT_TOP:
					angle = this.startAngle;
					r = this.outerRadius;
					break;
				case Anchor.RIGHT_BOTTOM:
					angle = this.endAngle;
					r = this.innerRadius;
					break;
				case Anchor.RIGHT_TOP:
					angle = this.endAngle;
					r = this.outerRadius;
					break;
			}
			pos.x += r*Math.cos(angle*Math.PI/180);
			pos.y += r*Math.sin(angle*Math.PI/180);
		}
		
	}
}