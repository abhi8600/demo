package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class GaugeAxisLineMarker extends GaugeAxisMarker {
		internal var value:Number;
		
		public function GaugeAxisLineMarker() {
			super();
		}
		
		override protected function getStyleNodeName():String { return "trendline_style"; }
		override protected function getStyleStateClass():Class { return TrendlineStyleState; }
		
		override public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader, stylesList:XML, localStyles:StylesList):void {
			super.deserialize(data, axisWidth, resources, stylesList, localStyles);
			if (data.@value != undefined) this.value = SerializerBase.getNumber(data.@value);
			this.gauge.setGradientRotation(this.getActualState().stroke.gradient);
		}
		
		public function draw(start:Point, end:Point):void {
			var g:Graphics = this.container.graphics;
			g.beginFill(0,0);
			var bounds:Rectangle = new Rectangle(Math.min(start.x, end.x), Math.min(start.y, end.y));
			bounds.width = Math.max(start.x, end.x) - bounds.x;
			bounds.height = Math.max(start.y, end.y) - bounds.y;
			this.getActualState().stroke.apply(g, bounds, this.color);
			g.moveTo(start.x, start.y);
			g.lineTo(end.x, end.y);
			g.lineStyle();
			g.endFill();
		}
		
		override public function checkScale(scale:BaseScale):void {
			this.checkScaleValue(this.value, scale);
		}
	}
}