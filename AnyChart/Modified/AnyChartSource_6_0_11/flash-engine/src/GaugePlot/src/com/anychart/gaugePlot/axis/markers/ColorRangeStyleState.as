package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	
	internal final class ColorRangeStyleState extends GaugeAxisMarkerStyleState {
		
		internal var startWidth:Number;
		internal var isStartSizeAxisSizeDepend:Boolean;
		internal var endWidth:Number;
		internal var isEndSizeAxisSizeDepend:Boolean;
		
		internal var label:AxisMarkerLabel;
		
		public function ColorRangeStyleState(style:Style) {
			super(style);
			this.startWidth = .05;
			this.endWidth = .2;
			this.isStartSizeAxisSizeDepend = false;
			this.isEndSizeAxisSizeDepend = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@start_size != undefined) {
				this.isStartSizeAxisSizeDepend = this.isAxisSizeDepend(data.@start_size);
				this.startWidth = this.isStartSizeAxisSizeDepend ? this.getAxisSizeFactor(data.@start_size) : (SerializerBase.getNumber(data.@start_size)/100); 
			}
			if (data.@end_size != undefined) {
				this.isEndSizeAxisSizeDepend = this.isAxisSizeDepend(data.@end_size);
				this.endWidth = this.isEndSizeAxisSizeDepend ? this.getAxisSizeFactor(data.@end_size) : (SerializerBase.getNumber(data.@end_size)/100);
			}
			if (SerializerBase.isEnabled(data.label[0])) {
				this.label = new AxisMarkerLabel();
				this.label.deserialize(data.label[0], resources, this.style);
			}
			return data;
		}

	}
}