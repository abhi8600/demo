package {
	import com.anychart.AnyChart;
	import com.anychart.encoders.Base64Encoder;
	import com.anychart.encoders.PNGEncoder;
	import com.anychart.events.AxesPlotScrollEvent;
	import com.anychart.events.DashboardEvent;
	import com.anychart.events.EngineEvent;
	import com.anychart.events.PointEvent;
	import com.anychart.events.PointsEvent;
	import com.anychart.events.ValueChangeEvent;
	import com.anychart.utils.IXMLPreprocessor;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;
	

	public class AnyChartRunnerBase extends Sprite {
		private var chart:AnyChart;
		public var externalObjId:String;
		
		private var externalEventsEnabled:Boolean;
		
		private var watermarkBitmap:Bitmap;
		private var mainContainer:DisplayObjectContainer;
		
		//DO NOT MODIFY!
		//Used by AnyChart51Builder
		public var xmlFile:String = null;
		//public var xmlFile:String = "./../anychart.xml";
		
		public var xmlData:String = null;
		
		private var chartScaleMode:int = CHART_SCALE_MODE_RECALCULATE;
		private static const CHART_SCALE_MODE_RECALCULATE:int = 0;
		private static const CHART_SCALE_MODE_SCALE:int = 1;
		
		public function AnyChartRunnerBase() {
			this.externalEventsEnabled = false;
			if (this.stage != null) {
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
				this.createEngine(this.stage, this.loaderInfo);
			}
		}
		
		private var enableChartMouseEvents:Boolean;
		
		private function fetchParams(loaderInfo:LoaderInfo):void {
			this.enableChartMouseEvents = false;
			var params:Object = loaderInfo.parameters;
			
			var xmlFileParams:String = "";
			
			var watermarkText:String = null;
			var scale:String = null;
			
			for (var j:String in params) {
				var propName:String = null;
				switch (j.toLowerCase()) {
					case "preloaderinittext":
					case "preloaderinit":
					case "preloaderloading":
					
					case "preloaderloadingtext":
					case "swffile":	
					case "dataxml":
					case "__jsresize":
					case "dispatchmouseevents":
						break; 
					case "resizemode": 
						scale = params[j]; 
						break;
					
					case "inittext":
					case "init":
						this.chart.messageOnInitialize = params[j]; 
						break;
					
					case "xmlloadingtext":			
					case "loadingconfig":			
						this.chart.messageOnLoadingXML = params[j]; 
						break;
					
					case "loadingresources":
					case "resourcesloadingtext":
						this.chart.messageOnLoadingResources = params[j]; 
						break;
					
					case "nodatatext":
					case "nodata":
						this.chart.messageNoData = params[j]; 
						break;
					
					case "waitingfordatatext":
					case "waitingfordata":
						this.chart.messageOnWaitingForData = params[j];
						break;
					
					case "templatesloadingtext":
					case "loadingtemplates":
						this.chart.messageOnLoadingTemplates = params[j];
						break;
					
					case "xmlfile":					this.xmlFile = params[j]; break;
					case "xmldata":					this.xmlData = params[j]; break;
					case "__externalobjid":			this.externalObjId = params[j]; break;
					case "__enableevents":			this.externalEventsEnabled = String(params[j]) == "1"; break;
					case "__enablechartmouseevents":this.enableChartMouseEvents = String(params[j]) == "1"; break;
					case "__watermarktext": watermarkText = String(params[j]); break;
					default:
						xmlFileParams += ((xmlFileParams.length == 0) ? j : ("&"+j)) + "=" + params[j];
						break; 
				}
			}
			
			if (scale == null) {
				this.chartScaleMode = CHART_SCALE_MODE_RECALCULATE;
			}else {
				switch (scale.toLowerCase()) {
					case "scale":
						this.chartScaleMode = CHART_SCALE_MODE_SCALE;
						break;
					default:
						this.chartScaleMode = CHART_SCALE_MODE_RECALCULATE;
				}
			}
			
			if (this.xmlFile != null) {
				if (xmlFileParams.length > 0) {
					if (this.xmlFile.indexOf("?") == -1)
						this.xmlFile += "?"+xmlFileParams;
					else if (this.xmlFile.indexOf("?") == this.xmlFile.length-1)
						this.xmlFile += xmlFileParams;
					else
						this.xmlFile += "&"+xmlFileParams;
				}
			}
			
			if (watermarkText != null && watermarkText.length > 0) {
				var fmt:TextFormat = new TextFormat();
				fmt.font = 'Verdana';
				fmt.size = 60;
				
				var watermarkTextField:TextField = new TextField();
				watermarkTextField.autoSize = TextFieldAutoSize.LEFT;
				watermarkTextField.defaultTextFormat = fmt;
				watermarkTextField.text = watermarkText;
				
				var d:BitmapData = new BitmapData(watermarkTextField.width, watermarkTextField.height, true, 0);
				d.draw(watermarkTextField);
				
				this.watermarkBitmap = new Bitmap(d);
				this.watermarkBitmap.alpha = 0.15;
				
				this.setWatermarkSize();
			}
		}
		
		public function set messageOnInitialize(value:String):void { this.chart.messageOnInitialize = value; }
		public function get messageOnInitialize():String { return this.chart.messageOnInitialize; }
		
		public function set messageOnLoadingXML(value:String):void { this.chart.messageOnLoadingXML = value; }
		public function get messageOnLoadingXML():String { return this.chart.messageOnLoadingXML; }
		
		public function set messageOnLoadingResources(value:String):void { this.chart.messageOnLoadingResources = value; }
		public function get messageOnLoadingResources():String { return this.chart.messageOnLoadingResources; }
		
		public function set messageNoData(value:String):void { this.chart.messageNoData = value; }
		public function get messageNoData():String { return this.chart.messageNoData; }
		
		public function set messageOnWaitingForData(value:String):void { this.chart.messageOnWaitingForData = value; }
		public function get messageOnWaitingForData():String { return this.chart.messageOnWaitingForData; }
		
		public function set messageOnLoadingTemplates(value:String):void { this.chart.messageOnLoadingTemplates = value; }
		public function get messageOnLoadingTemplates():String { return this.chart.messageOnLoadingTemplates; }
		
		public function enableEvents():void {
			this.externalEventsEnabled = true;
		}
		
		public function createEngine(mainContainer:DisplayObjectContainer, paramsContainer:LoaderInfo):void {
			this.mainContainer = mainContainer;
			mainContainer.addEventListener(Event.RESIZE, resizeHandler);
			mainContainer.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			mainContainer.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			mainContainer.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			
			var bounds:Rectangle = this.getChartBounds(mainContainer);
			if (!this.chart) this.initEngine();
			if (paramsContainer != null)
				this.fetchParams(paramsContainer);
			this.chart.addEventListener(EngineEvent.ANYCHART_CREATE, this.chartCreateHandler);
			this.chart.addEventListener(EngineEvent.ANYCHART_DRAW, this.chartDrawHandler);
			this.chart.addEventListener(EngineEvent.ANYCHART_RENDER, this.chartRenderHandler);
			this.chart.addEventListener(EngineEvent.ANYCHART_REFRESH, this.chartRefreshHandler);
			this.chart.addEventListener(DashboardEvent.DASHBOARD_VIEW_DRAW, this.dashboardViewDrawHandler);
			this.chart.addEventListener(DashboardEvent.DASHBOARD_VIEW_RENDER, this.dashboardViewRenderHandler);
			this.chart.addEventListener(DashboardEvent.DASHBOARD_VIEW_REFRESH, this.dashboardViewRefreshHandler);
			this.chart.addEventListener(ValueChangeEvent.VALUE_CHANGE,this.valueChangeHandler);
			this.chart.addEventListener(PointEvent.POINT_CLICK,this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_DESELECT, this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_DOWN, this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_OUT, this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_OVER, this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_UP, this.redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_SELECT, this.redirectPointEvent);
			this.chart.addEventListener(AxesPlotScrollEvent.X_AXIS_SCROLL_CHANGE, this.redirectScrollEvent);
			this.chart.addEventListener(AxesPlotScrollEvent.Y_AXIS_SCROLL_CHANGE, this.redirectScrollEvent);
			this.chart.addEventListener(PointsEvent.MULTIPLE_POINTS_SELECT, this.redirectPointsEvent);
			this.chart.addEventListener(PointsEvent.MULTIPLE_POINTS_DESELECT, this.redirectPointsEvent);
			try {
				this.initExternalInterface();
			}catch (e:Error) {
			}
			this.chart.xmlFilePath = xmlFile;
			this.chart.xmlDataRaw = xmlData;
			this.chart.initialize(bounds);
			this.chart.dispatchEvent(new EngineEvent(EngineEvent.ANYCHART_CREATE));
			
			if (watermarkBitmap != null) {
				this.setWatermarkSize();
				mainContainer.addChild(this.watermarkBitmap);
				this.setWatermarkSize();
			}
		}
		
		public function initEngine():void {
			var container:Sprite = new Sprite();
			this.addChild(container);
			this.chart = new AnyChart(container);
			if (this._preprocessor) this.chart.setXMLPreprocessor(this._preprocessor);
		}
		
		private var _fixedWidth:Number;
		private var _fixedHeight:Number;
		
		public function setFixedSize(width:Number, height:Number):void {
			this._fixedWidth = width;
			this._fixedHeight = height;
		}
		
		protected function getChartBounds(mainContainer:DisplayObject):Rectangle {
			var w:Number;
			var h:Number;
			var m:Boolean = mainContainer is Stage;
			
			if (!isNaN(this._fixedWidth))
				w = this._fixedWidth;
			else
				w = m ? Stage(mainContainer).stageWidth : mainContainer.stage.stageWidth;
				
			if (!isNaN(this._fixedHeight)) 
				h = this._fixedHeight;
			else
				h = m ? Stage(mainContainer).stageHeight : mainContainer.stage.stageHeight;
				
			return new Rectangle(0,0,w,h);
		}
		
		private function chartCreateHandler(event:EngineEvent):void {
			this.dispatchExternalInterfaceEvent({type:"create"});
			this.dispatchFlashEvent(event);
		}
		
		private function chartRenderHandler(event:EngineEvent):void {
			this.dispatchExternalInterfaceEvent({type:"render"});
			this.dispatchFlashEvent(event);
		}
		
		private function chartDrawHandler(event:EngineEvent):void {
			this.dispatchExternalInterfaceEvent({type:"draw"});
			this.dispatchFlashEvent(event);
		}
		
		private function chartRefreshHandler(event:EngineEvent):void {
			this.dispatchExternalInterfaceEvent({type:"refresh"});
			this.dispatchFlashEvent(event);
		}
		
		private function dashboardViewRenderHandler(event:DashboardEvent):void {
			this.dispatchExternalInterfaceEvent({type:"renderView", view:event.viewId});
			this.dispatchFlashEvent(event);
		}
		
		private function dashboardViewDrawHandler(event:DashboardEvent):void {
			this.dispatchExternalInterfaceEvent({type:"drawView", view:event.viewId});
			this.dispatchFlashEvent(event);
		}
		
		private function dashboardViewRefreshHandler(event:DashboardEvent):void {
			this.dispatchExternalInterfaceEvent({type:"refreshView", view:event.viewId});
			this.dispatchFlashEvent(event);
		}
		
		private function valueChangeHandler(event:ValueChangeEvent):void {
			this.dispatchExternalInterfaceEvent({type:"valueChange",params:event.params});
			this.dispatchFlashEvent(event);
		}
		
		private function redirectPointEvent(event:PointEvent):void {
			if (!this.externalEventsEnabled) {
				this.dispatchFlashEvent(event);
				return;
			}
			var data:Object = null;
			if (event.point != null) 
				data = event.point['serializeToEvent'] ? event.point['serializeToEvent']() : event.point.serialize();
			this.dispatchExternalInterfaceEvent({type:event.type, data:data, mouseX: event.mouseX, mouseY: event.mouseY});
			this.dispatchFlashEvent(event);
		}
		
		private function redirectPointsEvent(event:PointsEvent):void {
			if (event == null || event.points.length == 0) return;
			if (!this.externalEventsEnabled) {
				this.dispatchFlashEvent(event);
				return;
			}
			
			var data:Object = {};
			data.points = [];
			for (var i:int = 0;i<event.points.length;i++) {
				data.points.push(event.points[i]['serializeToEvent'] ? event.points[i]['serializeToEvent']() : event.points[i].serialize());
			}
			
			this.dispatchExternalInterfaceEvent({type:event.type, data:data});
			this.dispatchFlashEvent(event);
		}
		
		private function redirectScrollEvent(event:AxesPlotScrollEvent):void {
			if (!this.externalEventsEnabled) {
				this.dispatchFlashEvent(event);
				return;
			}
			this.dispatchExternalInterfaceEvent({type:event.type, source:event.source, phase:event.phase, startValue:event.startValue, endValue:event.endValue, range:event.valueRange});
			this.dispatchFlashEvent(event);
		}
		
		private function dispatchExternalInterfaceEvent(event:Object):void {
			if (ExternalInterface.available) {
				try {
					ExternalInterface.call("AnyChart.getChartById('"+this.externalObjId+"').dispatchEvent",event);
				}catch (e:Error) { }
			}
		}
		
		private function dispatchFlashEvent(event:Event):void {
			if (event) {
				event.stopImmediatePropagation();
				event.stopPropagation();
			}
			var newEvent:Event = event.clone();
			this.dispatchEvent(newEvent);
		}
		
		private var baseWidth:Number = NaN;
		private var baseHeight:Number = NaN;
		
		public function resizeHandler(event:Event):void {
			this.setWatermarkSize();
			if (stage.stageWidth > 0 && stage.stageHeight > 0) {
				if (isNaN(this.baseWidth)) this.baseWidth = stage.stageWidth;
				if (isNaN(this.baseHeight)) this.baseHeight = stage.stageHeight;
				
				if (this.chartScaleMode == CHART_SCALE_MODE_RECALCULATE) {
					if (this.chart != null)
						this.chart.resize(new Rectangle(0,0,stage.stageWidth, stage.stageHeight));
				}else {
					this.scaleX = stage.stageWidth/this.baseWidth;
					this.scaleY = stage.stageHeight/this.baseHeight;
				}
			}
			this.setWatermarkSize();
		}
		
		private function setWatermarkSize():void {
			if (mainContainer == null || watermarkBitmap == null) return;
			var s:Stage = (mainContainer is Stage) ? Stage(mainContainer) : mainContainer.stage;
			var w:Number = s == null ? mainContainer.width : s.stageWidth;
			var h:Number = s == null ? mainContainer.height : s.stageHeight;
			if (isNaN(w) || w == 0) return;
			if (isNaN(h) || h == 0) return;
			
			if (this.watermarkBitmap.width > w || this.watermarkBitmap.height > h)
				this.watermarkBitmap.scaleX = this.watermarkBitmap.scaleY = Math.min(w/this.watermarkBitmap.width, h/this.watermarkBitmap.height);
			
			this.watermarkBitmap.x = (w - this.watermarkBitmap.width)/2;
			this.watermarkBitmap.y = (h - this.watermarkBitmap.height)/2;
		}
		
		private function mouseMoveHandler(e:MouseEvent):void {
			if (this.enableChartMouseEvents)
				this.dispatchExternalInterfaceEvent({type:"chartMouseMove", mouseX: e.stageX, mouseY: e.stageY});
		}
		
		private function mouseDownHandler(e:MouseEvent):void {
			if (this.enableChartMouseEvents)
				this.dispatchExternalInterfaceEvent({type:"chartMouseDown", mouseX: e.stageX, mouseY: e.stageY});
		}
		
		private function mouseUpHandler(e:MouseEvent):void {
			if (this.enableChartMouseEvents)
				this.dispatchExternalInterfaceEvent({type:"chartMouseUp", mouseX: e.stageX, mouseY: e.stageY});
		}
		
		public var isXMLExternalInterfaceCallsEnabled:Boolean = true;
		
		private function initExternalInterface():void {
			if (ExternalInterface.available) {
				try {
					ExternalInterface.addCallback('SaveAsImage', this.chart.saveAsImage);
					ExternalInterface.addCallback('SaveAsPDF', this.chart.saveAsPDF);
					ExternalInterface.addCallback('PrintChart', this.chart.printChart);
					ExternalInterface.addCallback('SetLoading', this.chart.setLoading);
					if (this.isXMLExternalInterfaceCallsEnabled) {
						ExternalInterface.addCallback('SetXMLDataFromString',this.chart.setXMLData);
						ExternalInterface.addCallback('SetXMLDataFromURL',this.chart.setXMLFile);
						ExternalInterface.addCallback('SetXMLText',this.chart.setXMLData);
					}
					ExternalInterface.addCallback('GetPngScreen',this.chart.getBase64PNG);
					ExternalInterface.addCallback('GetPdfScreen',this.chart.getBase64PDF);
					
					ExternalInterface.addCallback('ResizeChart',this.resize);
					ExternalInterface.addCallback('UpdateViewFromString',this.chart.setViewXMLData);
					ExternalInterface.addCallback('UpdateViewFromURL',this.chart.setViewXMLFile);
					ExternalInterface.addCallback('getWidth',this.getWidth);
					ExternalInterface.addCallback('getHeight',this.getHeight);
					ExternalInterface.addCallback('UpdateData', this.chart.updateData);
					ExternalInterface.addCallback('UpdatePointData', this.chart.updatePointData);
					ExternalInterface.addCallback('UpdateViewPointData', this.chart.updateViewPointData);
					ExternalInterface.addCallback('GetInformation', this.chart.serialize);
					ExternalInterface.addCallback('ClearCashes', this.chart.clearCashes);
					ExternalInterface.addCallback('ScrollXTo', this.chart.scrollXTo);
					ExternalInterface.addCallback('ScrollYTo', this.chart.scrollYTo);
					ExternalInterface.addCallback('ScrollTo', this.chart.scrollTo);
					ExternalInterface.addCallback('ViewScrollXTo', this.chart.viewScrollXTo);
					ExternalInterface.addCallback('ViewScrollYTo', this.chart.viewScrollYTo);
					ExternalInterface.addCallback('ViewScrollTo', this.chart.viewScrollTo);
					ExternalInterface.addCallback('SetXZoom', this.chart.setXZoom);
					ExternalInterface.addCallback('SetYZoom', this.chart.setYZoom);
					ExternalInterface.addCallback('SetZoom', this.chart.setZoom);
					ExternalInterface.addCallback('SetViewXZoom', this.chart.setViewXZoom);
					ExternalInterface.addCallback('SetViewYZoom', this.chart.setViewYZoom);
					ExternalInterface.addCallback('SetViewZoom', this.chart.setViewZoom);
					ExternalInterface.addCallback('EnableEvents', this.enableEvents);
					ExternalInterface.addCallback('SetXMLFileCacheEnabled', this.setXMLFileCacheEnabled);
					ExternalInterface.addCallback('GetXScrollInfo', this.chart.getXScrollInfo);
					ExternalInterface.addCallback('GetYScrollInfo', this.chart.getYScrollInfo);
					ExternalInterface.addCallback('GetViewXScrollInfo', this.chart.getViewXScrollInfo);
					ExternalInterface.addCallback('GetViewYScrollInfo', this.chart.getViewYScrollInfo);
					
					ExternalInterface.addCallback('SetPlotCustomAttribute', this.chart.setPlotCustomAttribute);
					ExternalInterface.addCallback('AddSeries', this.chart.addSeries);
					ExternalInterface.addCallback('RemoveSeries', this.chart.removeSeries);
					ExternalInterface.addCallback('AddSeriesAt', this.chart.addSeriesAt);
					ExternalInterface.addCallback('UpdateSeries', this.chart.updateSeries);
					ExternalInterface.addCallback('ShowSeries', this.chart.showSeries);
		
					ExternalInterface.addCallback('AddPoint', this.chart.addPoint);
					ExternalInterface.addCallback('RemovePoint', this.chart.removePoint);
					ExternalInterface.addCallback('AddPointAt', this.chart.addPointAt);
					ExternalInterface.addCallback('UpdatePoint', this.chart.updatePoint);
					
					ExternalInterface.addCallback('Refresh', this.chart.refresh);
					ExternalInterface.addCallback('Clear', this.chart.clearChartData);
					
					ExternalInterface.addCallback('HighlightSeries', this.chart.highlightSeries);
					ExternalInterface.addCallback('HighlightPoint', this.chart.highlightPoint);
					ExternalInterface.addCallback('HighlightCategory', this.chart.highlightCategory);
					ExternalInterface.addCallback('SelectPoint', this.chart.selectPoint);
					
					ExternalInterface.addCallback('View_SetPlotCustomAttribute', this.chart.view_setPlotCustomAttribute);
					ExternalInterface.addCallback('View_AddSeries', this.chart.view_addSeries);
					ExternalInterface.addCallback('View_RemoveSeries', this.chart.view_removeSeries);
					ExternalInterface.addCallback('View_AddSeriesAt', this.chart.view_addSeriesAt);
					ExternalInterface.addCallback('View_UpdateSeries', this.chart.view_updateSeries);
					ExternalInterface.addCallback('View_ShowSeries', this.chart.view_showSeries);
		
					ExternalInterface.addCallback('View_AddPoint', this.chart.view_addPoint);
					ExternalInterface.addCallback('View_RemovePoint', this.chart.view_removePoint);
					ExternalInterface.addCallback('View_AddPointAt', this.chart.view_addPointAt);
					ExternalInterface.addCallback('View_UpdatePoint', this.chart.view_updatePoint);
					
					ExternalInterface.addCallback('View_Refresh', this.chart.view_refresh);
					ExternalInterface.addCallback('View_Clear', this.chart.view_clearChartData);
					
					ExternalInterface.addCallback('View_HighlightSeries', this.chart.view_highlightSeries);
					ExternalInterface.addCallback('View_HighlightPoint', this.chart.view_highlightPoint);
					ExternalInterface.addCallback('View_HighlightCategory', this.chart.view_highlightCategory);
					ExternalInterface.addCallback('View_SelectPoint', this.chart.view_selectPoint);
					
					ExternalInterface.addCallback('RepeatAnimation', this.chart.repeatAnimation);
					ExternalInterface.addCallback('ShowAxisMarker', this.chart.showAxisMarker);
					
					ExternalInterface.addCallback('updatePointWithAnimation', this.updatePointWithAnimation);
					ExternalInterface.addCallback('updateViewPointWithAnimation', this.updateViewPointWithAnimation);
					
					ExternalInterface.addCallback('startPointsUpdate', this.startPointsUpdate);
					ExternalInterface.addCallback('startViewPointsUpdate', this.startViewPointsUpdate);
					
					ExternalInterface.addCallback('getScrollableContent', this.getScrollableContent);
					
				}catch (e:Error) {}
			}
		}
		
		public function startPointsUpdate():void {
			if (this.chart) this.chart.startPointsUpdate();
		}
		
		public function startViewPointsUpdate(viewId:String):void {
			if (this.chart) this.chart.startViewPointsUpdate(viewId);
		}
		
		public function setPlotCustomAttribute(attributeName:String, attributeValue:String):void {
			this.chart.setPlotCustomAttribute(attributeName,attributeValue);
		}
		public function addSeries(...seriesData):void {
			this.chart.addSeries.apply(null, seriesData);
		}
		public function removeSeries (seriesId:String):void {
			this.chart.removeSeries(seriesId);
		}
		public function addSeriesAt(index:uint, seriesData:String):void {
			this.chart.addSeriesAt(index,seriesData);
		}
		public function updateSeries(seriesId:String, seriesData:String):void {
			this.chart.updateSeries(seriesId,seriesData);
		}
		public function  showSeries (seriesId:String, isVisible:Boolean):void {
			this.chart.showSeries(seriesId,isVisible);
		}
		public function addPoint(seriesId:String, ...pointsData):void {
			this.chart.addPoint.apply(null, [seriesId].concat(pointsData));
		}
		public function removePoint (seriesId:String, pointId:String):void {
			this.chart.removePoint(seriesId,pointId);
		}
		public function addPointAt (seriesId:String, index:uint, pointData:String):void {
			this.chart.addPointAt(seriesId,index,pointData);
		}
		public function updatePoint (seriesId:String, pointId:String, pointData:String):void {
			this.chart.updatePoint(seriesId,pointId,pointData);
		}		
		public function refresh ():void {
			this.chart.refresh();
		}
		public function clearChartData():void {
			this.chart.clearChartData();
		}
		public function highlightSeries(seriesId:String, highlighted:Boolean):void {
			this.chart.highlightSeries(seriesId,highlighted);
		}
		public function highlightPoint (seriesId:String, pointId:String, highlighted:Boolean):void {
			this.chart.highlightPoint(seriesId,pointId,highlighted);
		}
		public function highlightCategory(categoryName:String, highlighted:Boolean):void {
			this.chart.highlightCategory(categoryName,highlighted);
		}		
		public function selectPoint (seriesId:String, pointId:String, selected:Boolean):void {
			this.chart.selectPoint(seriesId,pointId,selected);
		}
		
		
		public function saveAsImage():void {
			this.chart.saveAsImage();
		}
		
		public function saveAsPDF():void {
			this.chart.saveAsPDF();
		}
		
		public function printChart():void {
			this.chart.printChart();
		}
		
		public function setLoading(text:String):void {
			this.chart.setLoading(text);
		}
		
		public function setXMLData(rawData:String, replaces:Array=null):void {
			this.chart.setXMLData(rawData,replaces);
		}
		
		public function setXMLFile(path:String, replaces:Array=null):void {
			this.chart.setXMLFile(path,replaces);
		}
		
		public function getBase64PNG(width:Number = NaN, height:Number = NaN):String {
			return this.chart.getBase64PNG(width, height);
		}
		
		
		public function setViewXMLData(viewId:String, data:String, replaces:Array = null):void {
			this.chart.setViewXMLData(viewId,data,replaces);
		}
		
		public function setViewXMLFile(viewId:String, path:String, replaces:Array = null):void {
			this.chart.setViewXMLFile(viewId,path,replaces);
		}
		
		
		public function updateData(path:String, data:Object):void {
			this.chart.updateData(path,data);
		}
		
		public function updatePointData(groupName:String, pointName:String, data:Object):void {
			this.chart.updatePointData(groupName,pointName,data);
		}
		
		public function updateViewPointData(viewName:String, groupName:String, pointName:String, data:Object):void {
			this.chart.updateViewPointData(viewName,groupName,pointName,data);
		}
		
		public function serialize():Object {
			return this.chart.serialize();
		}
		
		public function scrollXTo(xValue:String):void {
			this.chart.scrollXTo(xValue);
		}
		
		public function scrollYTo(yValue:String):void {
			this.chart.scrollYTo(yValue);
		}
		
		public function scrollTo(xValue:String,yValue:String):void {
			this.chart.scrollTo(xValue,yValue);
		}
		
		public function viewScrollXTo(viewName:String, xValue:String):void {
			this.chart.viewScrollXTo(viewName,xValue);
		}
		
		public function viewScrollYTo(viewName:String, yValue:String):void {
			this.chart.viewScrollYTo(viewName,yValue);
		}
		
		public function viewScrollTo(viewName:String, xValue:String,yValue:String):void {
			this.chart.viewScrollTo(viewName,xValue,yValue);
		}
		
		public function setXZoom(settings:Object):void {
			this.chart.setXZoom(settings);
		}
		
		public function setYZoom(settings:Object):void {
			this.chart.setYZoom(settings);
		}
		
		public function setZoom(xZoomSettings:Object, yZoomSettings:Object):void {
			this.chart.setZoom(xZoomSettings,yZoomSettings);
		}
		
		public function setViewXZoom(viewName:String, settings:Object):void {
			this.chart.setViewXZoom(viewName, settings);
		}
		
		public function setViewYZoom(viewName:String, settings:Object):void {
			this.chart.setViewYZoom(viewName, settings);
		}
		
		public function setViewZoom(viewName:String, xZoomSettings:Object, yZoomSettings:Object):void {
			this.chart.setViewZoom(viewName, xZoomSettings, yZoomSettings);
		}
		
		public function getXScrollInfo():Object { return this.chart.getXScrollInfo(); }
		public function getYScrollInfo():Object { return this.chart.getYScrollInfo(); }
		
		public function resize(newWidth:Number = NaN, newHeight:Number = NaN):void {
			if (this.chart != null && !isNaN(newWidth) && !isNaN(newHeight)) {
				this.chart.resize(new Rectangle(0,0,newWidth,newHeight));
			}
		}
		
		public function getWidth():Number {
			return (this.chart != null ? this.chart.getBounds().width : stage.stageWidth);
		}
		
		public function getHeight():Number {
			return (this.chart != null ? this.chart.getBounds().height : stage.stageHeight);
		}
		
		public function setXMLFileCacheEnabled(enabled:Boolean):void {
			if (this.chart) this.chart.setXMLCacheEnabled(enabled);
		}
		
		public function repeatAnimation():void {
			this.chart.repeatAnimation();
		}
		
		public function showAxisMarker(markerId:String, isVisible:Boolean):void {
			this.chart.showAxisMarker(markerId,isVisible);
		}
		
		private var _preprocessor:IXMLPreprocessor;
		
		public function setXMLPreprocessor(processor:IXMLPreprocessor):void {
			if (this.chart) this.chart.setXMLPreprocessor(processor);
			else this._preprocessor = processor;
		}
		
		public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object = null):void {
			if (this.chart) this.chart.updatePointWithAnimation(seriesId, pointId, newValues, settings);
		}
		
		public function updateViewPointWithAnimation(viewId:String, seriesId:String, pointId:String, newValues:Object, settings:Object = null):void {
			if (this.chart) this.chart.updateViewPointWithAnimation(viewId, seriesId, pointId, newValues, settings);
		}
		
		public function getScrollableContent():String {
			if (this.chart) {
				var data:BitmapData = this.chart.getScrollableContent();
				if (!data) return null;
				
				var img:ByteArray = PNGEncoder.PNGencode(data);
				return Base64Encoder.encode64(img, false);
			}
			
			return null;
		}
	}
}
