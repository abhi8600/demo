package com.anychart.radarPolarPlot.elements.markerElement
{
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	import flash.display.Sprite;

	public class RadarPolarMarkerElement extends MarkerElement
	{
		private var _container:Sprite;
		public function RadarPolarMarkerElement()
		{
		}
		public function setContainer (value:Sprite):void {
			this._container=value;
		}
		override protected function getContainer(container:IMainElementsContainer):Sprite{
			return _container;
		} 
		
		override public function createCopy(element:BaseElement=null):BaseElement
		{
			if (element == null)
				element = new RadarPolarMarkerElement();
			super.createCopy(element);
			return element;
		}
	}
}