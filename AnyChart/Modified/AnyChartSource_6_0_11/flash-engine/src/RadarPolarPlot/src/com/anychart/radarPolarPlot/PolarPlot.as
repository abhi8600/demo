package com.anychart.radarPolarPlot {
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
		
	public final class PolarPlot extends RadarPolarPlot {
		
		public function PolarPlot() {
			super();
			this.usePolarCoords = true;
		}
		
		public static function getDefaultTemplate():XML {
			return RadarPolarPlot.defaultTemplate;
		}
		
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {
			super.deserializeDataPlotSettings(data, resources);
			if (data.polar[0] != null){
				if (data.polar[0].background[0] != null) {
					polarBackground = new BackgroundBase();
					polarBackground.deserialize(data.polar[0].background[0], resources);
				}
				super.deserializeRadarPolarSettings(data.polar[0]);
			}
		}
	}
}