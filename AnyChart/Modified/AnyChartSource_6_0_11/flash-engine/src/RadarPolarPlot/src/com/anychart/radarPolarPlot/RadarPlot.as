package com.anychart.radarPolarPlot {
	import com.anychart.radarPolarPlot.axes.CategorizedAxis;
	import com.anychart.radarPolarPlot.axes.RotatedAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.utils.MathUtils;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class RadarPlot extends RadarPolarPlot {

		public function RadarPlot(){
			super();
			this.drawingModeCircle = false;
		}
		
		override protected function createXAxis():RotatedAxis {
			return new CategorizedAxis(this);
		}		
		
		public static function getDefaultTemplate():XML {
			return RadarPolarPlot.defaultTemplate;
		}
		
		override protected function deserializeRadarPolarSettings(data:XML):void {
			super.deserializeRadarPolarSettings(data);
			if (data.@drawing_style != undefined)
				this.drawingModeCircle = SerializerBase.getLString(data.@drawing_style) == 'circle'; //Polygon|Circle
		}
		
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void {
			super.deserializeDataPlotSettings(data, resources);
			if (data.radar[0] != null){
				if (data.radar[0].background[0] != null){
					polarBackground = new BackgroundBase();
					polarBackground.deserialize(data.radar[0].background[0], resources);
				}
				
				this.deserializeRadarPolarSettings(data.radar[0]);
			}
		}
		
		override protected function calcBoundsPaddings(bounds:Rectangle):Rectangle {
			if (this.drawingModeCircle)
				return super.calcBoundsPaddings(bounds);
				
			var axisBounds:Rectangle = bounds.clone();
			axisBounds.x = 0;
			axisBounds.y = 0;
			
			var cx:Number = axisBounds.width / 2;
			var cy:Number = axisBounds.height / 2;
			var radius:Number = Math.min(cx, cy);
			
			var minRadius:Number;
			
			if (this.isFixedRadius) {
				if (this.isPercentRadius) {
					minRadius = radius*this.radius;
				}else {
					minRadius = this.radius;
				}
				
				this.reduceRectBounds(axisBounds, radius - minRadius);
				
				return axisBounds;
			}
			
			if (this.xAxis == null)
				return axisBounds;
			
			minRadius = Number.MAX_VALUE;
			
			var tmpRadius:Number;
			var angle:Number;
			var crossing:Number = this.getYAxisCrossing();
			
			var i:int;
			var len:int = this.xAxis.scale.majorIntervalsCount;
			
			var tickmark:Number = 0;

			if (this.xAxis.majorTickmark != null && this.xAxis.majorTickmark.enabled && this.xAxis.majorTickmark.outside)
				tickmark = this.xAxis.majorTickmark.size;
			
			for (i = 0; i < len; i++){
				angle = this.xAxis.scale.localTransform(this.xAxis.scale.getMajorIntervalValue(i)) + crossing;
				
				tmpRadius = MathUtils.divide(cx, Math.abs(DrawingUtils.getPointX(1, angle)));
				minRadius = Math.min(minRadius, tmpRadius - tickmark);
				
				tmpRadius = MathUtils.divide(cy, Math.abs(DrawingUtils.getPointY(1, angle))); 
				minRadius = Math.min(minRadius, tmpRadius - tickmark);
			}
			
			minRadius = Math.max(minRadius, radius);
			
			var thickness:Number = this.getPlotOuterThickness();
			
			if (this.xAxis.labels != null && this.xAxis.labels.enabled && !this.xAxis.labels.isInsideLabels){
				var info:TextElementInformation;
				var startIndex:int = this.xAxis.labels.showFirst ? 0 : 1;
				var endIndex:int = this.xAxis.scale.majorIntervalsCount - (this.xAxis.labels.showLast ? 0 : 1);
				for (i = startIndex;i<endIndex;i++) {
					info = this.xAxis.labels.getTextInfo(i, true);
					angle = this.xAxis.scale.localTransform(this.xAxis.scale.getMajorIntervalValue(i)) + crossing;
					
					tmpRadius = MathUtils.divide(cx - info.rotatedBounds.width - this.xAxis.labels.padding - tickmark,
						Math.abs(DrawingUtils.getPointX(1, angle)));
					
					minRadius = Math.min(minRadius, tmpRadius);
					
					tmpRadius = MathUtils.divide(cy - info.rotatedBounds.height - this.xAxis.labels.padding - tickmark,
						Math.abs(DrawingUtils.getPointY(1, angle)));
					
					minRadius = Math.min(minRadius, tmpRadius);
				}
			}
			
			if (this.xAxis.minorTickmark != null && this.xAxis.minorTickmark.enabled && this.xAxis.minorTickmark.outside)
				minRadius = Math.min(minRadius, radius - this.xAxis.minorTickmark.size);
				
			minRadius = Math.min(minRadius, radius - tickmark);
			
			if (radius > minRadius)
				this.reduceRectBounds(axisBounds, radius - minRadius);
			
			return axisBounds;
		}
		
	}
}