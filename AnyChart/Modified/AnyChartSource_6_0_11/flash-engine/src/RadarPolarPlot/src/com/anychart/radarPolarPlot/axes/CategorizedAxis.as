package com.anychart.radarPolarPlot.axes {
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.axes.categorization.Category;
	import com.anychart.radarPolarPlot.scales.TextScale;
	import com.anychart.scales.BaseScale;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.utils.LineEquation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public final class CategorizedAxis extends RotatedAxis {
			
		public function CategorizedAxis(plot:RadarPolarPlot) {
			super(plot);
			this.categoriesList = [];
			this.categoriesMap = {};
		}
		
		override protected function createScale(scaleType:*):BaseScale {
			var scale:TextScale = new TextScale();
			scale.maximumOffset = 0;
			return scale;
		}
		
		private var categoriesList:Array;
		private var categoriesMap:Object;
		
		override public function onAfterDataDeserialize():void {
			for each (var category:Category in this.categoriesList) {
				category.onAfterDeserializeData(null);
			}
		}
		
		protected function createCategory(name:String):Category {
			var category:Category = new Category(this.plot,this.categoriesList.length, name, this);
			this.plot.xCategories.push(category);
			this.categoriesList.push(category);
			this.categoriesMap[name] = category;
			return category;
		}
		
		public function getCategoryByIndex(index:int):Category {
			return this.categoriesList[index];
		}
		
		public function getCategory(name:String):Category {
			var category:Category = this.categoriesMap[name];
			if (category == null)
				category = this.createCategory(name);
			return category;
		}
		
		public function hasCategory(name:String):Boolean {
			return this.categoriesMap[name] != null;
		}
		
		override public function formatAxisLabelToken(token:String, scaleValue:Number):* {
			return token == '%Value' ? this.categoriesList[scaleValue].name : '';
		}
		
		override protected function drawLine():void {
			if (this.line == null || !this.line.enabled) return;

			if (this.plot.drawingModeCircle){
				super.drawLine();
				return;
			}
			var cx:Number = this.center.x;
			var cy:Number = this.center.y;
			var dim:Number = Math.min(this.bounds.height, this.bounds.width) / 2;
			var bounds:Rectangle = new Rectangle(cx - dim, cy - dim, dim + dim, dim + dim);
			var thickness:Number = this.line.thickness / 2;
			bounds.x += thickness;
			bounds.y += thickness;
			bounds.width -= thickness + thickness;
			bounds.height -= thickness + thickness;
			var g:Graphics = this.axisLinesContainer.graphics;
			this.line.apply(g, bounds);
			var crossingAngle:Number = this.plot.getYAxisCrossing();
			var radius:Number = dim - thickness; 
			var majIntervals:uint = this.scale.majorIntervalsCount;
			g.moveTo(DrawingUtils.getPointX(radius, this.scale.localTransform(this.scale.getMajorIntervalValue(0))+ crossingAngle) + cx, DrawingUtils.getPointY(radius, this.scale.localTransform(this.scale.getMajorIntervalValue(0))+ crossingAngle) + cy);
			for (var i:uint = 0; i <= majIntervals; i++){
				var nextMajorInterval:Number = this.scale.getMajorIntervalValue(i);
				var tmpNext:Number = this.scale.localTransform(nextMajorInterval) + crossingAngle;
				g.lineTo(DrawingUtils.getPointX(radius, tmpNext) + cx, DrawingUtils.getPointY(radius, tmpNext) + cy);
			}
			g.lineStyle();
			
			//super.drawLine();
		}
		
		override protected function drawShape(g:Graphics, cx:Number, cy:Number, x1:Number, y1:Number, x2:Number, y2:Number, angle1:Number, angle2:Number, radius:Number):void {
			if (RadarPlot(this.plot).drawingModeCircle){
				super.drawShape(g, cx, cy, x1, y1, x2, y2, angle1, angle2, radius);
			} else {
				g.moveTo(cx, cy);
				g.lineTo(x1, y1);
				g.lineTo(x2, y2);
				g.lineTo(cx, cy);
			}			
		}
		
		private var majPixValue1:Number = NaN;
		private var majAngle1:Number = NaN;
		private var majX1:Number = NaN;
		private var majY1:Number = NaN;
			
		private var majPixValue2:Number = NaN;
		private var majAngle2:Number = NaN;
		private var majX2:Number = NaN;
		private var majY2:Number = NaN;
		
		private var equation:LineEquation = null;
		
		override protected function getGridSectorParams(pixValue:Number, nextValue:Number, radius:Number, cx:Number, cy:Number, 
			minorGrid:Boolean, majPixValue:Number, majPixValueNext:Number):void {
				
			if (!minorGrid){
				super.getGridSectorParams(pixValue, nextValue, radius, cx, cy, minorGrid, majPixValue, majPixValueNext);
				return;
			}
			
			var crossing:Number = this.plot.getYAxisCrossing();
			
			if (this.majPixValue1 != majPixValue){
				this.majPixValue1 = majPixValue;
				this.majPixValue2 = majPixValueNext;
				this.majAngle1 = -(majPixValue + crossing);
				this.majX1 = DrawingUtils.getPointX(radius, this.majAngle1) + cx;
				this.majY1 = DrawingUtils.getPointY(radius, this.majAngle1) + cy;
				this.majAngle2 = -(majPixValueNext + crossing);
				this.majX2 = DrawingUtils.getPointX(radius, this.majAngle2) + cx;
				this.majY2 = DrawingUtils.getPointY(radius, this.majAngle2) + cy;
				this.equation = LineEquation.getLineBy4Coords(this.majX1, this.majY1, this. majX2, this.majY2); 
			}
						
			this.angle1 = -(pixValue + crossing);
			var x:Number = DrawingUtils.getPointX(radius, this.angle1) + cx;
			var y:Number = DrawingUtils.getPointY(radius, this.angle1) + cy;
			var line:LineEquation = LineEquation.getLineBy4Coords(x, y, cx, cy); 
			var crossPoint:Point = LineEquation.getCrossPoint(line, this.equation);
			this.x1 = crossPoint.x;
			this.y1 = crossPoint.y;
			
			if (pixValue == nextValue){
				this.angle2 = this.angle1;
				this.x2 = this.x1;
				this.y2 = this.y1;
			} else {
				this.angle2 = (nextValue + this.plot.getYAxisCrossing());
				x = DrawingUtils.getPointX(radius, this.angle2) + cx;
				y = DrawingUtils.getPointY(radius, this.angle2) + cy;
				line = LineEquation.getLineBy4Coords(x, y, cx, cy); 
				crossPoint = LineEquation.getCrossPoint(line, this.equation);
				this.x2 = crossPoint.x;
				this.y2 = crossPoint.y;
			}
		}
	}
}