package com.anychart.radarPolarPlot.axes.categorization {
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	
	
	public final class StackSettings {
		
		private var positiveStackSumm:Number;
		private var negativeStackSumm:Number;
		
		private var isPercent:Boolean;
		
		public function StackSettings(isPercent:Boolean) {
			this.positiveStackSumm = 0;
			this.negativeStackSumm = 0;
			this.isPercent = isPercent;
			this.positivePoints = [];
			this.negativePoints = [];
		}
		
		public function getPrevious(value:Number):Number {
			return (value >= 0) ? this.positiveStackSumm : this.negativeStackSumm;
		}
		
		public function addValue(value:Number):Number {
			if (value >= 0) {
				this.positiveStackSumm += value;
				return this.positiveStackSumm;
			}
			this.negativeStackSumm += value;
			return this.negativeStackSumm;
		}
		
		private var positivePoints:Array;
		private var negativePoints:Array;
		
		public function getPositivePoints():Array {
			return this.positivePoints;
		}
		
		public function getNegativePoints():Array {
			return this.negativePoints;
		}
		
		public function addPoint(pt:RadarPolarPlotPoint, value:Number):Number {
			if (value >= 0) {
				if (this.positivePoints.length > 0)
					pt.previousStackPoint = this.positivePoints[this.positivePoints.length-1];
				this.positivePoints.push(pt);
				this.positiveStackSumm += value;
				return this.positiveStackSumm;
			}
			if (this.negativePoints.length > 0)
				pt.previousStackPoint = this.negativePoints[this.negativePoints.length-1];

			this.negativePoints.push(pt);
			this.negativeStackSumm += value;
			return this.negativeStackSumm;
		}
		
		public function setPercent():void {
			var pt:RadarPolarPlotPoint;
			for each (pt in this.positivePoints)
				pt.setStackMax(this.positiveStackSumm);
			for each (pt in this.negativePoints)
				pt.setStackMax(Math.abs(this.negativeStackSumm));
		}
		
		public function isLastInStack (point:RadarPolarPlotPoint):Boolean {
			if (this.negativePoints.length > 0 && this.negativePoints[this.negativePoints.length-1]==point) return true;
			if (this.positivePoints.length > 0 && this.positivePoints[this.positivePoints.length-1]==point) return true;
			return false
		}
		
		public function getPercentofStack(value:Number):Number{
			return  (value > 0) ? (value/this.positiveStackSumm) : (value/this.negativeStackSumm);
		}
		
		public function getStackSumm(value:Number):Number {
			return this.isPercent ? 100 : ((value > 0) ? this.positiveStackSumm : this.negativeStackSumm);
		}

	}
}