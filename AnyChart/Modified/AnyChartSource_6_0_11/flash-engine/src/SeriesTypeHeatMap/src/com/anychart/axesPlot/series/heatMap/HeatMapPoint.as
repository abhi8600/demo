package com.anychart.axesPlot.series.heatMap {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.CategorizedAxis;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.SingleValuePoint;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.layout.Position;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	
	public class HeatMapPoint extends SingleValuePoint {
		
		private var leftTop:Point;
		private var rightBottom:Point;
		
		private var row:Number;
		private var column:Number;
		
		public function HeatMapPoint() {
			super();
			this.leftTop = new Point();
			this.rightBottom = new Point();
		}
		
		override protected function deserializeArgument(data:XML):void {
			var argumentAxis:Axis = AxesPlotSeries(this.series).argumentAxis;
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			
			var columnName:String = SerializerBase.getString(data.@column);
			var rowName:String = SerializerBase.getString(data.@row);
			
			var axesSeries:AxesPlotSeries = AxesPlotSeries(this.series);
			this.isHorizontal = argumentAxis.position == Position.LEFT || argumentAxis.position == Position.RIGHT;
			
			this.category = CategorizedAxis(argumentAxis).getCategory(columnName);
			this.yCategory = CategorizedAxis(valueAxis).getCategory(rowName);
			
			this.column = this.category.index;
			this.row = this.yCategory.index;
			
			this.clusterSettings = axesSeries.clusterSettings;
			this.paletteIndex = this.category.index;
			
			if (!this.isMissing)
				this.category.checkPoint(this);
		}
		
		override protected function checkMissing(data:XML):void {
			this.isMissing = isNaN(this.deserializeValueFromXML(data.@y)) || (data.@column == undefined || data.@row == undefined);
		}
		
		override protected function deserializeValue(data:XML):void {
			if (data.@y != undefined)
				this.y = SerializerBase.getNumber(data.@y);
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			AxesPlotSeries(this.series).argumentAxis.transform(this, this.column-.5, this.leftTop);
			AxesPlotSeries(this.series).argumentAxis.transform(this, this.column+.5, this.rightBottom);
			AxesPlotSeries(this.series).valueAxis.transform(this, this.row - .5, this.leftTop);
			AxesPlotSeries(this.series).valueAxis.transform(this, this.row + .5, this.rightBottom);
			
			if (this.z) {
				this.z.transform(this.leftTop);
				this.z.transform(this.rightBottom);
			}
			
			this.bounds.x = Math.min(this.leftTop.x, this.rightBottom.x);
			this.bounds.y = Math.min(this.leftTop.y, this.rightBottom.y);
			this.bounds.width = Math.max(this.leftTop.x, this.rightBottom.x) - this.bounds.x;
			this.bounds.height = Math.max(this.leftTop.y, this.rightBottom.y) - this.bounds.y;
			
			this.drawItem(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawItem(this.container.graphics);
		}
		
		private function drawItem(g:Graphics):void {
			
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			
			if (state.fill != null && state.fill.enabled)
				state.fill.begin(g,this.bounds,this.color);
			
			if (state.stroke != null && state.stroke.enabled)
				state.stroke.apply(g, this.bounds, this.color);
			
			this.drawShape(g);
			g.endFill();
			g.lineStyle();
				
			if (state.hatchFill != null && state.hatchFill.enabled) {
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				this.drawShape(g);
				g.endFill();
			}
			
			if (this.hasPersonalContainer) {
				if (state.effects != null && state.effects.enabled) {
					this.container.filters = state.effects.list;
				}else {
					this.container.filters = null;
				}				
			}
		}
		
		private function drawShape(g:Graphics):void {
			g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%Column') return this.category.name;
			if (token == '%Row') return this.yCategory.name;
			return super.getPointTokenValue(token);
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "y";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				y: this.y
			}
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.y != undefined) this.y = newValue.y;
		}
	}
}