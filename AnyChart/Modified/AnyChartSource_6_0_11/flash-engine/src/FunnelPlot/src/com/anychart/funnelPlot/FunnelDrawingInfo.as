package com.anychart.funnelPlot{
	
	import com.anychart.percentBase.PercentBaseDrawingInfo;
	import com.anychart.percentBase.PercentBaseGlobalSeriesSettings;
	import com.anychart.percentBase.PercentBasePoint;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывает рисование для Funnel
	 * 
	 * @see com.anychart.percentBase.PercentBaseDrawingInfo
	 */
	public class FunnelDrawingInfo extends PercentBaseDrawingInfo{
		
		/**
		 * левая центральная точка
		 */
		public var leftCenter:Point;
		/**
		 * правая центральная точка
		 */
		public var rightCenter:Point;
		
		
		/**
		 * Центральные точки необходимые для правильного просчета 3D
		 */
		public var centerTopFront:Point;
		public var centerBottomFront:Point;
		public var centerTopBack:Point;
		public var centerBottomBack:Point;
		public var centerCenterFront:Point;
		public var centerCenterBack:Point; 
		
		/**
		 * Радиус центра фуннеля
		 */
		public var centerSize3D:Number;
		public var shiftNumber:Number;
		
		public var topBounds:Rectangle;
		public var bottomBounds:Rectangle;
		public var leftBounds:Rectangle;
		public var rightBounds:Rectangle;
		public var frontBounds:Rectangle;
		public var backBounds:Rectangle;
		
		public function FunnelDrawingInfo():void{
			this.leftCenter = new Point;
			this.rightCenter = new Point;
			
			this.centerTopFront = new Point;
			this.centerBottomFront = new Point;
			this.centerTopBack = new Point;
			this.centerBottomBack = new Point;
			this.centerCenterFront = new Point;
			this.centerCenterBack = new Point;
			
			this.shiftNumber = 0;
		}
		
		public function intialiseBounds():void {
			this.topBounds = this.getBound(this.createSides(Sides.TOP));
			this.backBounds = this.getBound(this.createSides(Sides.BACK));
			this.frontBounds = this.getBound(this.createSides(Sides.FRONT));
			this.leftBounds = this.getBound(this.createSides(Sides.LEFT));
			this.rightBounds = this.getBound(this.createSides(Sides.RIGHT));
			this.bottomBounds = this.getBound(this.createSides(Sides.BOTTOM));
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBaseDrawingInfo#setAnchorPoint
		 */
		override public function setAnchorPoint(anchor:uint, pos:Point, inverted:Boolean):void{
			
			switch (anchor){
				case Anchor.CENTER:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.centerTopFront.y + this.centerBottomFront.y)/2;
					break;
				}
				case Anchor.CENTER_LEFT:{
					pos.x = (this.leftTop.x + this.leftBottom.x)/2;
					pos.y = (this.leftTop.y + this.leftBottom.y)/2;
					if (this.leftTop.y != this.leftCenter.y){// учет neck'a
							if (pos.y<=this.leftCenter.y){
								pos.x = inverted ? (this.leftTop.x) :  (this.leftTop.x + ((this.leftCenter.x-this.leftTop.x)*(pos.y-this.leftTop.y))/(this.leftCenter.y-this.leftTop.y));
							}else pos.x = inverted ? (this.leftCenter.x-((this.leftCenter.x-this.leftBottom.x)/(this.leftBottom.y-this.leftCenter.y))*(pos.y-this.leftCenter.y))  : this.leftBottom.x ;
					}
					break;
				}
				case Anchor.CENTER_RIGHT:{
					pos.x = (this.rightTop.x + this.rightBottom.x)/2;
					pos.y = (this.rightTop.y + this.rightBottom.y)/2;
					if (this.rightTop.y != this.rightCenter.y){
							if (pos.y<=this.leftCenter.y){// учет neck'a
								pos.x = inverted ? (this.rightTop.x) :  (this.rightTop.x - (this.rightTop.x-this.rightCenter.x)/(this.rightCenter.y-this.rightTop.y)*(pos.y-this.rightTop.y));
							}else pos.x = inverted ? (this.rightCenter.x+(this.rightBottom.x-this.rightCenter.x)/(this.rightBottom.y-this.rightCenter.y)*(pos.y-this.rightCenter.y))  : this.rightBottom.x ;
					}
					break;
				}
				case Anchor.CENTER_BOTTOM:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.centerBottomFront.y);
					break;
				}
				case Anchor.CENTER_TOP:{
					pos.x = (this.rightTop.x + this.leftTop.x)/2;
					pos.y = (this.centerTopFront.y);
					break;
				}
				case Anchor.LEFT_TOP:{
					pos.x = (this.leftTop.x);
					pos.y = (this.leftTop.y);
					break;
				}
				case Anchor.RIGHT_TOP:{
					pos.x = (this.rightTop.x);
					pos.y = (this.rightTop.y);
					break;
				}
				case Anchor.LEFT_BOTTOM:{
					pos.x = (this.leftBottom.x);
					pos.y = (this.centerBottomFront.y);
					break;
				}
				case Anchor.RIGHT_BOTTOM:{
					pos.x = (this.rightBottom.x);
					pos.y = (this.rightBottom.y);
					break;
				}
			}
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBaseDrawingInfo#setSectorPoint
		 */
		override public function setSectorPoint(pt:PercentBasePoint, inverted:Boolean,w:Number):void{
			var h:Number = PercentBaseSeries(pt.series).getHeight();
			var x:Number = PercentBaseSeries(pt.series).seriesBound.x;
			var y:Number = PercentBaseSeries(pt.series).seriesBound.y;
			var shift:Number = PercentBaseSeries(pt.series).shiftNumber;
			var seriesWidth:Number = PercentBaseSeries(pt.series).seriesBound.width;
			var seriesHeight:Number = PercentBaseSeries(pt.series).seriesBound.height;
			var necHeight:Number = FunnelGlobalSeriesSettings(pt.global).necHeight;
			var baseWidth:Number = FunnelGlobalSeriesSettings(pt.global).baseWidth*w;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(pt.global).dataIsWidth;
			var feet:Number = PercentBaseGlobalSeriesSettings(pt.global).feet;
		 	if (shift == 0) {
				w=h*feet;
				if (w>seriesWidth ){
					w=seriesWidth;
					h = seriesWidth/feet;
					y = (seriesHeight-h)/2;	
				}
			}else {
				w=h*feet;
				if (w>shift){
					w=shift;
					h=shift/feet;
					y = (seriesHeight-h)/2;
				}
			} 
			
			this.shiftNumber  = w - seriesWidth;
			var size3D:Number = FunnelGlobalSeriesSettings(pt.global).size3D*h;
			
			if (!FunnelPoint(pt).is3D){
				size3D = 0;
			}
			
			this.bottomSize3D = pt.bottomWidth*size3D;
			this.topSize3D = pt.topWidth*size3D;
			this.centerSize3D = FunnelPoint(pt).centerWidth*size3D;
			
			this.centerBottomBack.x = this.centerBottomFront.x = this.centerCenterBack.x = this.centerCenterFront.x = this.centerTopBack.x = this.centerTopFront.x = x+seriesWidth/2;
			
			if (!inverted && necHeight == 0 && baseWidth == 0){
				this.centerBottomBack.y = (y + h - pt.topHeight*h);
				this.centerCenterBack.y = (y + h - FunnelPoint(pt).centerHeight*h);
				this.centerTopBack.y = (y + h - pt.bottomHeight*h);
				
				this.centerBottomFront.y = this.centerBottomBack.y + this.bottomSize3D*2;
				this.centerCenterFront.y = this.centerCenterBack.y + this.centerSize3D*2;
				this.centerTopFront.y = this.centerTopBack.y + this.topSize3D*2;
			}else {
				this.centerBottomFront.y =inverted ? (y + pt.bottomHeight*h) : (y + h - pt.topHeight*h);
				this.centerCenterFront.y = inverted ? (y + FunnelPoint(pt).centerHeight*h) :(y + h - FunnelPoint(pt).centerHeight*h);
				this.centerTopFront.y =inverted ? (y + pt.topHeight*h) : (y + h - pt.bottomHeight*h); 
			
				this.centerBottomBack.y = this.centerBottomFront.y - this.bottomSize3D*2;
				this.centerCenterBack.y = this.centerCenterFront.y - this.centerSize3D*2;
				this.centerTopBack.y = this.centerTopFront.y - this.topSize3D*2;
			}
			
			
			
			this.leftBottom.y = this.rightBottom.y = this.centerBottomFront.y - this.bottomSize3D;
			this.leftCenter.y = this.rightCenter.y = this.centerCenterFront.y - this.centerSize3D;
			this.leftTop.y = this.rightTop.y = this.centerTopFront.y - this.topSize3D;
			
			this.leftBottom.x = (x+seriesWidth/2-pt.bottomWidth*w/2);
			this.leftCenter.x = x+seriesWidth/2-FunnelPoint(pt).centerWidth*w/2;
			this.leftTop.x = (x+seriesWidth/2 - pt.topWidth*w/2);
			this.rightBottom.x =(x+seriesWidth/2+pt.bottomWidth*w/2);
			this.rightCenter.x = x+seriesWidth/2+FunnelPoint(pt).centerWidth*w/2;
			this.rightTop.x = (x+seriesWidth/2+pt.topWidth*w/2);
		}
		/**
		 * @see com.anychart.percentBase.PercentBaseDrawingInfo#updateSectorPoint
		 */
		override public function updateSectorPoint(x:Number):void{
		 	this.leftBottom.x -= x;
			this.leftCenter.x -= x;
			this.leftTop.x -= x;
			this.rightBottom.x -= x;
			this.rightCenter.x -= x;
			this.rightTop.x -= x;
			this.centerTopFront.x -= x;
			this.centerBottomFront.x -= x;
			this.centerTopBack.x -= x;
			this.centerBottomBack.x -= x;
			this.centerCenterBack.x -= x;
			this.centerCenterFront.x -= x;  
 		}
 		
		private function createSides(side:uint):Array{
			var res:Array = new Array();
			switch (side){
				case Sides.FRONT:
					res[0] = this.leftBottom;
					res[1] = this.leftCenter;
					res[2] = this.leftTop;
					res[3] = this.centerTopFront;
					res[4] = this.centerCenterFront;
					res[5] = this.centerBottomFront;
					break;
				case Sides.RIGHT:
					res[0] = this.centerBottomFront;
					res[1] = this.centerCenterFront;
					res[2] = this.centerTopFront;
					res[3] = this.rightTop;
					res[4] = this.rightCenter;
					res[5] = this.rightBottom;
					break;
				case Sides.BACK:
					res[0] = this.rightBottom;
					res[1] = this.rightCenter;
					res[2] = this.rightTop;
					res[3] = this.centerTopBack;
					res[4] = this.centerCenterBack;
					res[5] = this.centerBottomBack;
					break;
				case Sides.LEFT:
					res[0] = this.centerBottomBack;
					res[1] = this.centerCenterBack;
					res[2] = this.centerTopBack;
					res[3] = this.leftTop;
					res[4] = this.leftCenter;
					res[5] = this.leftBottom;
					break;
				case  Sides.TOP:
					res[0] = this.leftTop;
					res[1] = this.centerTopFront;
					res[2] = this.rightTop;
					res[3] = this.centerTopBack;
					break;
				case Sides.BOTTOM:
					res[0] = this.leftBottom;
					res[1] = this.centerBottomFront;
					res[2] = this.rightBottom;
					res[3] = this.centerBottomBack;
					break;
				}
			return res;
		}
		
		public function drawCircFrontSide(g:Graphics,state:BackgroundBaseStyleState):void{
			DrawingUtils.drawArc(g,(this.rightBottom.x + this.leftBottom.x)/2,this.leftBottom.y,0,180,this.bottomSize3D,(this.rightBottom.x - this.leftBottom.x)/2,0,true);
			g.lineTo(this.leftCenter.x,this.leftCenter.y);
			DrawingUtils.drawArc(g,(this.rightTop.x + this.leftTop.x)/2,this.leftTop.y,180,0,this.topSize3D,(this.rightTop.x - this.leftTop.x)/2,0,false);
			g.lineTo(this.rightCenter.x,this.rightCenter.y);
			g.endFill();
			g.lineStyle();
		}
		public function drawCircBackSide(g:Graphics,state:BackgroundBaseStyleState):void{
			DrawingUtils.drawArc(g,(this.rightBottom.x + this.leftBottom.x)/2,this.leftBottom.y,180,360,this.bottomSize3D,(this.rightBottom.x - this.leftBottom.x)/2,0,true);
			g.lineTo(this.rightCenter.x,this.rightCenter.y);
			DrawingUtils.drawArc(g,(this.rightTop.x + this.leftTop.x)/2,this.leftTop.y,360,180,this.topSize3D,(this.rightTop.x - this.leftTop.x)/2,0,false);
			g.lineTo(this.leftCenter.x,this.leftCenter.y);			
			g.endFill();
			g.lineStyle();
		}
		public function drawCircTopSide(g:Graphics,state:BackgroundBaseStyleState):void{
			g.drawEllipse(this.leftTop.x,this.leftTop.y-this.topSize3D,this.rightTop.x-this.leftTop.x,this.topSize3D*2);
		}
		public function drawCircBottomSide(g:Graphics,state:BackgroundBaseStyleState):void{
			g.drawEllipse(this.leftBottom.x,this.leftBottom.y-this.bottomSize3D,this.rightBottom.x-this.leftBottom.x,this.bottomSize3D*2);
		}
		public function drawFrontSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.FRONT));
		}
		
		public function drawRightSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.RIGHT));
		}
		
		public function drawBackSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.BACK));
		}
		
		public function drawLeftSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.LEFT));
		}
		
		public function drawTopSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.TOP));
		}
		
		public function drawBottomSide(g:Graphics,state:BackgroundBaseStyleState):void{
			this.drawSide(g,this.createSides(Sides.BOTTOM));
		}
	
		private function drawSide(g:Graphics,points:Array):void{
			var pt:Point = Point(points[0]);
			g.moveTo(pt.x,pt.y);
			
			for (var i:uint=1; i<points.length;i++){
				pt = Point(points[i]);
				g.lineTo(pt.x,pt.y);
			}
		}
		
		private function getBound(points:Array):Rectangle {
			var lt:Point = new Point();
			var rb:Point = new Point();
			lt.x = lt.y = Number.MAX_VALUE;
			rb.x = rb.y = -Number.MAX_VALUE;
			var rect:Rectangle = new Rectangle();
			
			for (var i:uint; i<points.length; i++){
				lt.x = Math.min(lt.x,Point(points[i]).x);
				lt.y = Math.min(lt.y,Point(points[i]).y);
				rb.x = Math.max(rb.x,Point(points[i]).x);
				rb.y = Math.max(rb.y,Point(points[i]).y);
			}
			rect.x = lt.x;
			rect.y = lt.y;
			rect.width = rb.x - lt.x;
			rect.height = rb.y - lt.y;
			
			return rect;
		} 
	}
}
final class Sides {
	public static const FRONT:uint = 0;
	public static const BACK:uint = 1;
	public static const TOP:uint = 2;
	public static const BOTTOM:uint = 3;
	public static const LEFT:uint = 4;
	public static const RIGHT:uint = 5;
}

