package com.anychart.multipleLayout{
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	import flash.geom.Rectangle;
	
	/**
	 *Класс предназначен для мульти серийных граффиков типа Funnel,Cone, Pyramid 
	 */
	public class MultipleLayouter implements ISerializable {
		
		/**
		 * Колличество колонок
		 */
		public var columns:int;
		
		/**
		 * Колличество строк
		 */
		public var rows:int;
		
		/**
		 * Расположение серий, если direction=VERTICAL то серии распологаются по порядку в колонках
		 * если direction=HORIZONTAL то по порядку в строках
		 */
		public var direction:uint;
		
		/**
		 * расстояние между столбцами
		 */
		public var horizontalPadding:Number;
		
		/**
		 * Расстояние между строками 
		 */
		public var verticalPadding:Number;
		
		public function MultipleLayouter() {
			this.columns = -1;
			this.rows = -1;
			this.direction = Direction.HORIZONTAL;
			this.horizontalPadding = 5;
			this.verticalPadding = 5;
		}
		
		/**
		 * Десериализуем данные о мультисерийности из XML
		 */
		public function deserialize(data:XML):void {
			if (data.@columns != undefined)this.columns = SerializerBase.getNumber(data.@columns);
			if (data.@rows != undefined)this.rows = SerializerBase.getNumber(data.@rows);
			if (data.@direction != undefined){
				switch (SerializerBase.getEnumItem(data.@direction)){
					case "horizontal": this.direction = Direction.HORIZONTAL;break;
					case "vertical": this.direction = Direction.VERTICAL;break;
				}
			}
			if (data.@vertical_padding !=undefined){
				if (SerializerBase.isPercent(data.@vertical_padding))this.verticalPadding = SerializerBase.getPercent(data.@vertical_padding);
				else this.verticalPadding = SerializerBase.getNumber(data.@vertical_padding);
			}
			if (data.@horizontal_padding != undefined){
				if (SerializerBase.isPercent(data.@horizontal_padding))this.horizontalPadding = SerializerBase.getPercent(data.@horizontal_padding);
				else this.horizontalPadding = SerializerBase.getNumber(data.@horizontal_padding);
			}
		}
		
		/**
		 * Поучаем колиичество строк и столбцов если пользователь явно не указал один из параметров или оба
		 */
		public function initialize(series:Array):void {
			if (this.columns == -1 && this.rows != -1){
				this.columns =Math.ceil(series.length/this.rows);
				return;
			}
			
			if (this.rows == -1 && this.columns == -1){
				this.columns = 4;
				if (series.length<=4){
					this.columns = series.length;
				}
			}
			this.rows = Math.ceil(series.length/this.columns);
		}
		
		/**
		 * Получаем значения ячеек в которых рисуется серия.
		 */
		public function setSeriesBounds(series:Array,bounds:Rectangle):void{
			var rowIndex:uint = 0;
			var columnIndex:uint = 0; 
			var x:Number = 0;
			var y:Number = 0;
			if (this.horizontalPadding<1)this.horizontalPadding*=bounds.width;
			if (this.verticalPadding<1)this.verticalPadding*=bounds.height;
			var realWidth:Number = bounds.width - this.horizontalPadding*(this.columns - 1);
			var realHeight:Number = bounds.height - this.verticalPadding*(this.rows - 1);
			for (var i:int = 0; i < series.length; i++){
				var seriesItem:PercentBaseSeries = series[i];
				seriesItem.seriesBound = new Rectangle();
				seriesItem.seriesBound.width = realWidth/this.columns;
				seriesItem.seriesBound.height = realHeight/this.rows;
				seriesItem.seriesBound.x = bounds.x + x;  
				seriesItem.seriesBound.y = bounds.y + y;
				
				if (this.direction == Direction.HORIZONTAL){
					columnIndex ++;
					x += seriesItem.seriesBound.width+this.horizontalPadding;
					if (columnIndex>this.columns-1){
						columnIndex = 0;
						x = 0;
						y +=seriesItem.seriesBound.height+this.verticalPadding; 
						rowIndex++;
					}	
				}
				if (this.direction == Direction.VERTICAL){
					rowIndex++;
					y +=seriesItem.seriesBound+this.verticalPadding; 
					if (rowIndex>this.rows-1){
						rowIndex = 0;
						y = 0;
						x += seriesItem.seriesBound.width+this.horizontalPadding;
						columnIndex++;
					}
				}
			}
		} 
	}
}