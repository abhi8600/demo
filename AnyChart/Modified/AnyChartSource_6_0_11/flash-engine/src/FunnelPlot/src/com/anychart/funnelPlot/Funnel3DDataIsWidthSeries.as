package com.anychart.funnelPlot{
	import com.anychart.seriesPlot.data.BasePoint;
	
	
	public class Funnel3DDataIsWidthSeries extends Funnel2DDataIsWidthSeries{
		
		override public function addPointsToDrawing():Boolean {return false}
		
		override public function onBeforeDraw():void {
			var point:Funnel3DDataIsWidthPoint;
			this.setRealSummHeight();
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var j:int = this.points.length;
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var minPointYValue:Number = Number.MAX_VALUE;
			this.currentY = 1;
			
			for (var i:int = 0; i<j ; i++){
				point = this.points[i];
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
				inverted ? (this.plot.drawingPoints.unshift(point)) : (this.plot.drawingPoints.push(point));
				if (FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y<minPointYValue) minPointYValue = FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y;
			}
			if(!inverted){// если  график инвертирован инициализируем точки в обратном порядке. 
				for (i = 0; i<j; i++){
					point = this.points[i];
					point.initialize();
				}	
			}else{
				for (i = (j-1 );i>=0; i--){
					point = this.points[i];
					point.initialize();
				} 
			}
			super.onBeforeDraw();
		}
		
		override public function onBeforeResize():void{
			var point:Funnel3DDataIsWidthPoint;
			this.setRealSummHeight();
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var minPointYValue:Number = Number.MAX_VALUE;
			var j:int = this.points.length;
			this.currentY = 1;
			for (var i:int = 0; i<j ; i++){
				point = this.points[i];
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
			}
			super.onBeforeDraw();
		}
		
		override public function createPoint():BasePoint{
			return new Funnel3DDataIsWidthPoint();
		} 
	}
}