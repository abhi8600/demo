package com.anychart.percentBase{
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	
	
	public class ConePyramidDIWPoint extends PercentBasePoint{
		
		public function ConePyramidDIWPoint():void {
			super();
			this.drawingInfo = new ConePyramidDIWDrawingInfo();
		}
		
		override public function setSectorYValue(dataIsWidth:Boolean, minValue:Number, padding:Number, realSummHeight:Number, size3D:Number):void{
			var firstPoint:PercentBasePoint = this.series.points[0];
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			var previousPoint:PercentBasePoint = this.series.points[this.index-1];
									
			this.percentHeight = 1/(PercentBaseSeries(this.series).points.length);
			this.percentHeight *= this.getRealPercentHeight(padding,size3D);
			this.bottomHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY-=this.percentHeight;
			this.topHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY -= padding; 
		}
		
		override protected function getRealPercentHeight(padding:Number, size3D:Number):Number{
			return (1-padding*(PercentBaseSeries(this.series).points.length-1));
		}
		
		override public function setSectorXValue():void {
			var index:Number = this.index;
			var nextPoint:PercentBasePoint = PercentBaseSeries(this.series).points[index+1];
			this.bottomWidth = this.calculationValue/this.series.cashedTokens['YMax'];
			if (nextPoint != null){
				this.topWidth = nextPoint.calculationValue/this.series.cashedTokens['YMax'];
			}else this.topWidth = 0;
		} 
		
		override protected function drawSector():void{
			this.setBounds();
			this.drawShape();
		}
		
		override protected function drawShape():void{
			var drawInfo: ConePyramidDIWDrawingInfo = ConePyramidDIWDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, this.bounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, this.bounds, this.color);
				
			g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
			g.lineTo(drawInfo.rightBottom.x,drawInfo.rightBottom.y);
			g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
			g.lineTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
			
			g.endFill();
			g.lineStyle();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(this.container.graphics, this.hatchType, this.color);
				g.moveTo(drawInfo.leftBottom.x,drawInfo.leftBottom.y);
				g.lineTo(drawInfo.rightBottom.x,drawInfo.rightBottom.y);
				g.lineTo(drawInfo.rightTop.x,drawInfo.rightTop.y);
				g.lineTo(drawInfo.leftTop.x,drawInfo.leftTop.y);
				g.endFill();
			 }
		}
	}
}