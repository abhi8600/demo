package com.anychart.funnelPlot{
	
	import com.anychart.data.SeriesType;
	import com.anychart.percentBase.PercentBaseGlobalSeriesSettings;
	import com.anychart.percentBase.PercentBasePlot;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.programaticStyle.CircularProgramaticStyle;
	import com.anychart.programaticStyle.SquareProgramaticStyle;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	/**
	 * Класс описывает глобальные настройки графика типа Funnel
	 */
	 
	public class FunnelGlobalSeriesSettings extends PercentBaseGlobalSeriesSettings{
		
		/**
		 * минимальная ширина в процентах
		 */
		public var baseWidth:Number;
		
		/**
		 * высота шейки воронки в процентах от общей высоты
		 */
		public var necHeight:Number;
		
		/**
		 * тип Funnel кгруглый или квадратный
		 */
		public var mode:uint;
		
				
		/**
		 * Создаем нужную нам серию в зависимости от того включено ли 3D или нет
		 * если да то 3D серию если нет 2Д
		 */
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:PercentBaseSeries;
			if (PercentBasePlot(this.plot).is3D){
				if (this.dataIsWidth){
					series = new Funnel3DDataIsWidthSeries();
				}else	series = new Funnel3DSeries();
			}else{
				if (this.dataIsWidth){
					series = new Funnel2DDataIsWidthSeries();
				}else	series = new Funnel2DSeries();
			}
			series.type = SeriesType.FUNNEL;
			return series;
		}
		
		override public function getSettingsNodeName():String {
			return "funnel_series";
		}
		
		override public function getStyleNodeName():String {
			return "funnel_style";
		}
		
		override public function getStyleStateClass():Class { return BackgroundBaseStyleState; }
		override public function createSettings():BaseSeriesSettings { return new FunnelSeriesSettings(); }
		override public function isProgrammaticStyle(styleNode:XML):Boolean { return false; }
		/**
		 * достаем настройки серии из XML
		 */
		override public function deserialize(data:XML):void{
			super.deserialize(data);
			if (data.@min_width != undefined) this.baseWidth = SerializerBase.getRatio(data.@min_width);
			if (data.@neck_height != undefined) this.necHeight = SerializerBase.getRatio(data.@neck_height);
			if (data.@mode != undefined){
				switch (SerializerBase.getEnumItem(data.@mode)){
					case "square": this.mode = FunnelMode.SQUARE;break;
					case "circular": this.mode = FunnelMode.CIRCULAR;break;
				}
			}
		}
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			if (SerializerBase.getLString(styleNode.@name) == "squareprogramaticstyle") {
				var square:SquareProgramaticStyle = new SquareProgramaticStyle();
				square.initialize(state);
				return square;
			}
			if (SerializerBase.getLString(styleNode.@name) == "circularprogramaticstyle") {
				var circular:CircularProgramaticStyle = new CircularProgramaticStyle();
				circular.initialize(state);
				return circular;
			}
			return null;
		}
	}
}