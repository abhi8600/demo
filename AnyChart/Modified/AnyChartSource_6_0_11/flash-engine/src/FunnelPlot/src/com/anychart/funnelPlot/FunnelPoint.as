﻿package com.anychart.funnelPlot{
	
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.percentBase.PercentBasePoint;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * класс описывает точкку типа Funnel
	 */
	public class FunnelPoint extends PercentBasePoint{
		
		public var centerHeight:Number;
		public var centerWidth:Number;
		public var is3D:Boolean;
		public var pointWidth:Number;
		
		public function FunnelPoint():void{
			this.drawingInfo = new FunnelDrawingInfo();
			super();
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#setBounds
		 */
		override protected function setBounds():void{
			var necHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight;
			var h:Number = PercentBaseSeries(this.series).getHeight();
			
			this.bounds.x = Math.min(FunnelDrawingInfo(this.drawingInfo).leftCenter.x, FunnelDrawingInfo(this.drawingInfo).leftTop.x, this.drawingInfo.leftBottom.x);
			if(this.bottomHeight<necHeight && this.topHeight<necHeight){
				this.bounds.width = this.drawingInfo.rightTop.x - this.drawingInfo.leftBottom.x;
			}else{
				this.bounds.width = Math.max(this.drawingInfo.rightTop.x-this.drawingInfo.leftTop.x,this.drawingInfo.rightBottom.x- this.drawingInfo.leftBottom.x);
			}
			this.bounds.y = Math.min(FunnelDrawingInfo(this.drawingInfo).leftBottom.y,FunnelDrawingInfo(this.drawingInfo).leftCenter.y,this.drawingInfo.leftTop.y);
			this.bounds.height = h* this.percentHeight;
		}
		
		/**
		 * Выставляем значения ширины верхней и нижней границы сектора в зависимости от режима
		 */
		override public function setSectorXValue():void{
				if (FunnelGlobalSeriesSettings(this.global).dataIsWidth){//получаем значения точек в секторах
					if (this.index != PercentBaseSeries(this.series).points.length-1){
						setSectorYWidth();
					}else return;
				}else {
					setSectorYHeight();
				}	
		}
		
		/**
		 * Выставляем значения ширины верхней и нижней границы сектора если dataIsWidth выключено
		 */
		private function setSectorYHeight():void{
			 var necHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight;
			var baseWidht:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var MYCONST:Number = (baseWidht*(1-necHeight))/(1-baseWidht);
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			
			this.bottomWidth = inverted ?  ((this.bottomHeight-necHeight+MYCONST)/(1-necHeight+MYCONST)) : ((this.topHeight-necHeight+MYCONST)/(1-necHeight+MYCONST));
			this.topWidth = inverted ? ((this.topHeight-necHeight+MYCONST)/(1-necHeight+MYCONST)) : ((this.bottomHeight-necHeight+MYCONST)/(1-necHeight+MYCONST));
			this.centerHeight = this.topHeight;
			this.centerWidth = inverted ? this.topWidth : this.bottomWidth;
			
			if (this.topHeight<necHeight && this.bottomHeight<necHeight){
				this.centerWidth = this.topWidth = this.bottomWidth = baseWidht;
			}else if (this.topHeight<necHeight){
				this.centerHeight = necHeight;
				this.centerWidth = baseWidht;
				if (inverted){
					this.topWidth = baseWidht;
				}else {
					this.bottomWidth = baseWidht;
				}
			}  
		}
		/**
		 * Выставляем значения ширины верхней и нижней границы сектора если dataIsWidth включено
		 */
		private function setSectorYWidth():void{
			var k:Number = this.index;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			
			this.bottomWidth = (this.calculationValue-this.series.cashedTokens['YMin'])/(this.series.cashedTokens['YMax']-this.series.cashedTokens['YMin'])*(1-baseWidth)+baseWidth;
			if (this.bottomWidth<baseWidth) this.bottomWidth = baseWidth;
			if (PercentBaseSeries(this.series).points[k+1] != null){
				this.topWidth = (PercentBaseSeries(this.series).points[k+1].y - this.series.cashedTokens['YMin'])/(this.series.cashedTokens['YMax']-this.series.cashedTokens['YMin'])*(1-baseWidth)+baseWidth;
				if (this.topWidth<baseWidth) this.topWidth = baseWidth;
			}else {
				 this.topWidth = (this.calculationValue-this.series.cashedTokens['YMin'])/(this.series.cashedTokens['YMax']-this.series.cashedTokens['YMin'])*(1-baseWidth)+baseWidth;;
			}
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#getRealPercentHeight
		 * 
		 * @param padding - расстояние между секторами
		 * @param size3D -  глубина 3D
		 */
		override protected function getRealPercentHeight(padding:Number, size3D:Number):Number{
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var maxWidth:Number = FunnelPoint(FunnelSeries(this.series).points[0]).calculationValue/this.series.cashedTokens['YSum'];
			
			
			if (FunnelGlobalSeriesSettings(this.global).baseWidth == 0 && !dataIsWidth){
				return (1 - (padding)*(PercentBaseSeries(this.series).visiblePoints-1));
			} 
			else{
				if (inverted){
					return (1 - padding*(PercentBaseSeries(this.series).visiblePoints-1)-baseWidth*size3D*2);
				}else return (1 - padding*(PercentBaseSeries(this.series).visiblePoints-1)-size3D*2);
			}
		}
		
		/**
		 * Поучаем позиции лейблов с учетом neck'a
		 */
		 override public function getLabelPosition(state: BaseElementStyleState, sprite: Sprite, bounds: Rectangle):Point{
			var placement:uint = FunnelGlobalSeriesSettings(this.global).labelsPlacementMode;
			var pos:Point = new Point();
			var neckHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight;
			var baseWidht:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var MYCONST:Number = (baseWidht*(1-neckHeight))/(1-baseWidht);
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
				
			if (placement == LabelsPlacementMode.INSIDE){return this.getElementPosition(state, sprite, bounds);}
			
			if (placement == LabelsPlacementMode.OUTSIDE_LEFT) {
				pos.x = (FunnelDrawingInfo(this.drawingInfo).leftTop.x + FunnelDrawingInfo(this.drawingInfo).leftBottom.x)/2;
				pos.y = (FunnelDrawingInfo(this.drawingInfo).leftTop.y + FunnelDrawingInfo(this.drawingInfo).leftBottom.y)/2;
				if (this.centerHeight != this.topHeight){
					if (pos.y>=FunnelDrawingInfo(this.drawingInfo).rightCenter.y){
						pos.x = inverted ? (FunnelDrawingInfo(this.drawingInfo).leftCenter.x+FunnelDrawingInfo(this.drawingInfo).leftBottom.x)/2:(FunnelDrawingInfo(this.drawingInfo).leftCenter.x);
					}
					else{
						pos.x = inverted ? (FunnelDrawingInfo(this.drawingInfo).leftCenter.x) : (FunnelDrawingInfo(this.drawingInfo).leftCenter.x+ FunnelDrawingInfo(this.drawingInfo).leftTop.x)/2;
					} 
				}
				this.applyHAlign(pos, HorizontalAlign.LEFT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}
			if (placement == LabelsPlacementMode.OUTSIDE_RIGHT) {
				pos.x = (FunnelDrawingInfo(this.drawingInfo).rightTop.x + FunnelDrawingInfo(this.drawingInfo).rightBottom.x)/2;
				pos.y = (FunnelDrawingInfo(this.drawingInfo).rightTop.y + FunnelDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				if (this.centerHeight != this.topHeight){
					if (pos.y>=FunnelDrawingInfo(this.drawingInfo).rightCenter.y){
						pos.x = inverted ? (FunnelDrawingInfo(this.drawingInfo).rightCenter.x+FunnelDrawingInfo(this.drawingInfo).rightBottom.x)/2:(FunnelDrawingInfo(this.drawingInfo).rightCenter.x);
					}
					else{
						pos.x = inverted ? (FunnelDrawingInfo(this.drawingInfo).rightCenter.x): (FunnelDrawingInfo(this.drawingInfo).rightCenter.x+ FunnelDrawingInfo(this.drawingInfo).rightTop.x)/2;
					} 
				} 
				this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}
			if (placement == LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN && this.label != null){
				pos.x = PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht() - this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width;
				pos.y = (FunnelDrawingInfo(this.drawingInfo).rightTop.y + FunnelDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}
			if (placement == LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN && this.label != null){
				pos.x = PercentBaseSeries(this.series).seriesBound.x;
				pos.y = (FunnelDrawingInfo(this.drawingInfo).rightTop.y + FunnelDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}	
			return pos;
		} 
		 
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void{
			this.drawingInfo.setAnchorPoint(anchor, pos, FunnelGlobalSeriesSettings(this.global).inverted);
		}
		
		
	}
}