package com.anychart.labels {
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.percentBase.PercentBasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class CustomLabelElement extends LabelElement {
		
		override protected function getPosition(container:IElementContainer, state:BaseElementStyleState, sprite:Sprite, elementIndex:uint):Point {
			return PercentBasePoint(container).getLabelPosition(state, sprite, this.getBounds(container, state, elementIndex));
		}
		
	} 
}