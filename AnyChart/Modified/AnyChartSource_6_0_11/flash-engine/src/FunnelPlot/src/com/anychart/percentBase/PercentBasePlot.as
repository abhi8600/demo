package com.anychart.percentBase{
	
	
	import com.anychart.data.SeriesType;
	import com.anychart.funnelPlot.FunnelGlobalSeriesSettings;
	import com.anychart.multipleLayout.MultipleLayouter;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	/**
	 * Базовый класс для Граффиков показывающих процентное соотношение
	 */
	public class PercentBasePlot extends SeriesPlot{
		
		/**
		 *включен 3D режим или нет
		 */
		public var is3D:Boolean;
		private var layouter:MultipleLayouter;
		
		protected static var templatesManager:BaseTemplatesManager;
		
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class; 
		protected static var defaultTemplate:XML = buildDefautlTemplate( new XMLContent());

		/**
		 * по умолчанию 3D режим выключен
		 */
		 
		public function PercentBasePlot():void{
			this.is3D = false;
			this.layouter = new MultipleLayouter();
		}
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate().copy(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		/**
		 * достаем настройки плота из XML
		 * @see com.anychart.seriesPlot.SeriesPlot#deserializeDataPlotSettings
		 */
		override protected function deserializeDataPlotSettings(data:XML, resources:ResourcesLoader):void{
			super.deserializeDataPlotSettings(data, resources);
			if (data.@enable_3d_mode != undefined) this.is3D = SerializerBase.getBoolean(data.@enable_3d_mode);
			if (data.multiple_series_layout[0] != null)	this.layouter.deserialize(data.multiple_series_layout[0]);
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			
			this.baseBounds = bounds.clone();
			
			var container:DisplayObject = super.initialize(bounds);
			bounds = bounds.clone();
			bounds.x = 0;
			bounds.y = 0;
			if (this.background != null && this.background.margins != null) {
				this.background.margins.applyInside(bounds);
			}
			this.layouter.initialize(this.series);
			this.layouter.setSeriesBounds(this.series,bounds);
			return container;
		}
		
		override public function recalculateAndRedraw():void {
			super.recalculateAndRedraw();
		}
		
		override public function calculateResize(bounds:Rectangle):void {
			
			this.baseBounds = bounds.clone();
			
			var innerBounds:Rectangle = bounds.clone();
			innerBounds.x = 0;
			innerBounds.y = 0;
			if (this.background != null && this.background.margins != null) {
				this.background.margins.applyInside(innerBounds);
			}
			this.layouter.setSeriesBounds(this.series, innerBounds);
			super.calculateResize(bounds);
		}
		
		override public function execResize():void {
			super.execResize();
		}
		
		/**
		 * Возвращает тип серии
		 * 
		 * @param type - тип полученный из XML
		 */
		override public function getSeriesType(type:*):uint{
			switch (SerializerBase.getEnumItem(type)) {
				case "funnel": return SeriesType.FUNNEL;
				case "pyramid": return SeriesType.PYRAMID;
				case "cone": return SeriesType.CONE;
				default: return this.defaultSeriesType;
			}
		}
		
		/**
		 * Возвращает класс с глобальными настройками
		 * 
		 * @param seriesType - тип серии
		 */
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings{
			switch (seriesType) {
				case SeriesType.FUNNEL: return new FunnelGlobalSeriesSettings();
			}
			return null;
		}
		
		override protected function seriesAfterPointDeserializationInit(series:BaseSeries):void {
			super.seriesAfterPointDeserializationInit(series);
			
			var pointsCount:uint = series.points.length;
			var allValuesAreZero:Boolean = false;
			var count:int = 0;
			var point:PercentBasePoint, value:Number;
			for (var i:int = 0; i < pointsCount; i++) {
				point = PercentBasePoint(series.points[i]);
				if (point == null)
					continue;
				value = point.y;
				point.calculationValue = value;
				if (value == 0) count++;
			}
			if (count == pointsCount) allValuesAreZero = true;
			if (allValuesAreZero) {
				for (i = 0; i < pointsCount; i++) {
		            series.points[i].calculationValue = 1;
				}
			}
		}
	
	}
}