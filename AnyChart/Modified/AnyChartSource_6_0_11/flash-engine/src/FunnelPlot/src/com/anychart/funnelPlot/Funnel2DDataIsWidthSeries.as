package com.anychart.funnelPlot{
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Funnel2DDataIsWidthSeries extends FunnelSeries{
		
		override public function onBeforeDraw():void{
			this.currentY = 1;
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			for (var i:uint = 0; i<this.points.length ; i++){
				var point:Funnel2DDataIsWidthPoint = this.points[i];
				if (point == null)
					continue;
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
			}
			super.onBeforeDraw();
		}
		
		override public function createPoint():BasePoint{
			return new Funnel2DDataIsWidthPoint();
		}
		
		public var targetWidht:Number;
		
		override public function setLabelsPosition():void {
			var labelPos:Point = new Point();
			var labelRect:Rectangle = new Rectangle();
			var labelPadding:Number;
			var pointWidth:Number;
			this.targetWidht = this.seriesBound.width;
			var pos:Number;
			for (var i:int = 0; i<this.points.length; i++){
				var point:FunnelPoint = FunnelPoint(this.points[i]);
				if (point == null)
					continue;
				if (point.label.enabled == true && !point.isMissing){
					pos = (FunnelDIWDrawingInfo(point.drawingInfo).rightBottom.x + FunnelDIWDrawingInfo(point.drawingInfo).rightTop.x)/2
					labelRect = point.label.getBounds(point,point.label.style.normal,BasePoint.LABEL_INDEX);
					labelPos = point.getLabelPosition(BaseElementStyleState(point.label.style.normal),null,labelRect); 	
					labelPadding = LabelElementStyleState(point.label.style.normal).padding;
					pointWidth = (point.bottomWidth + point.topWidth)/2*this.seriesBound.width;
					if ((labelPadding + labelRect.width+pointWidth/2+this.seriesBound.width/2)>this.seriesBound.width){
						this.targetWidht = Math.max(targetWidht,(labelPadding + labelRect.width+pointWidth/2+this.seriesBound.width/2));
					}
				}
			}
			this.shiftNumber =Math.ceil(this.seriesBound.width - (this.targetWidht - this.seriesBound.width));
		}
	}
}