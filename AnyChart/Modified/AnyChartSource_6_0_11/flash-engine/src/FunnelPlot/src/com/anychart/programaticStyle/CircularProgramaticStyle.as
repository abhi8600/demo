package com.anychart.programaticStyle{
	import com.anychart.funnelPlot.Funnel3DPoint;
	import com.anychart.funnelPlot.FunnelDrawingInfo;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.Graphics;
	
	
	public class CircularProgramaticStyle extends SquareProgramaticStyle{
		
		override protected function drawTopSide(point:Funnel3DPoint):void{
		  	var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			this.topGrd.angle = 45;
			this.topGrd.beginGradientFill(g, drawInfo.topBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.topBounds, point.color);

			drawInfo.drawCircTopSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawCircTopSide(g,state);
				g.endFill();
			}  
		}
		
		override protected function drawBottomSide(point:Funnel3DPoint):void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			this.bottomGrd.beginGradientFill(g, drawInfo.bottomBounds,point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.bottomBounds, point.color);

			drawInfo.drawCircBottomSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawCircBottomSide(g,state);
				g.endFill();
			}  
		}
		override protected function drawLeftSide(point:Funnel3DPoint):void{}
		
		override protected function drawRightSide(point:Funnel3DPoint):void{}
		
		override protected function drawFrontSide(point:Funnel3DPoint):void{
		 	var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			
			this.frontGrd.beginGradientFill(g, drawInfo.frontBounds, point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.frontBounds, point.color);

			drawInfo.drawCircFrontSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawCircFrontSide(g,state);
				g.endFill();
			} 
		}
		
		override protected function drawBackSide(point:Funnel3DPoint):void{
			var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			
			this.backGrd.beginGradientFill(g, drawInfo.backBounds, point.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.backBounds, point.color);

			drawInfo.drawCircBackSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,point.hatchType,point.color);
				drawInfo.drawCircBackSide(g,state);
				g.endFill();
			}
		
		}
		override protected function drawLine(point:Funnel3DPoint):void{
			var g:Graphics = point.container.graphics;
			var drawInfo: FunnelDrawingInfo = FunnelDrawingInfo(point.drawingInfo);
				
			if (BackgroundBaseStyleState(point.styleState).stroke != null)
				BackgroundBaseStyleState(point.styleState).stroke.apply(g, point.bounds, point.color);
			
			DrawingUtils.drawArc(g,(drawInfo.rightCenter.x + drawInfo.leftCenter.x)/2,drawInfo.leftCenter.y,0,180,drawInfo.centerSize3D,(drawInfo.rightCenter.x - drawInfo.leftCenter.x)/2,0,true);
			
			g.lineStyle(); 
		} 
	}
}