package com.anychart.funnelPlot{
	
	import com.anychart.elements.label.LabelElementStyleState;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class FunnelSeries extends PercentBaseSeries{
		
		/**
		 * Метод определяет ширину графика при которой все лейблы останутся в границах экрана
		 * 
		 * более подробно об этом можно почитать в wiki.
		 * http://192.168.1.55:8080/confluence/display/ac/How+labels+shift+plot
		 */
		override public function setLabelsPosition():void{
			var labelPos:Point = new Point;
			var labelRect:Rectangle = new Rectangle;
			var w:Number = this.getWidht();
			var h:Number = this.getHeight();
			var necHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight*h;
			var baseWidht:Number = FunnelGlobalSeriesSettings(this.global).baseWidth*w;
			var deltaH:Number =necHeight - (h-necHeight)*baseWidht/(w-baseWidht);
			var tangens:Number = w/(h-deltaH)/2;
			var labelH:Number;
			var pos:Number;
			var labelPadding:Number;
			for (var i:int = 0; i<this.points.length; i++){
				var point:FunnelPoint = this.points[i];
				if (point == null)
					continue;
				if (point.label.enabled == true && !point.isMissing){
					pos = (FunnelDrawingInfo(point.drawingInfo).rightBottom.x + FunnelDrawingInfo(point.drawingInfo).rightTop.x)/2;
					labelRect = point.label.getBounds(point,point.label.style.normal,BasePoint.LABEL_INDEX);
					labelPos = point.getLabelPosition(BaseElementStyleState(point.label.style.normal),null,labelRect);
					labelPadding = LabelElementStyleState(point.label.style.normal).padding;
					labelH = (point.bottomHeight+point.topHeight)*h/2-deltaH;
					
					if (!this is Funnel3DSeries) {
						if (labelPos.x-labelRect.width < this.seriesBound.x || labelPos.x+labelRect.width > this.getWidht() || pos+labelRect.width > this.getWidht()){
							tangens = Math.min(tangens,(w-labelRect.width-labelPadding)/((h-deltaH)+labelH));
						}
					} else {
						tangens = Math.min(tangens, (w - labelRect.width - labelPadding) / ((h - deltaH) + labelH));
					}
				}
			}
			this.shiftNumber = 2*(h-deltaH)*tangens;
		}
	}
}