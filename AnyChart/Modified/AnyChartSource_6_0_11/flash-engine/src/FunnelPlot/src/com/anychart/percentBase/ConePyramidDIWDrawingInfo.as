package com.anychart.percentBase{
	import flash.geom.Point;
	
	
	public class ConePyramidDIWDrawingInfo extends PercentBaseDrawingInfo{
		
		override public function setSectorPoint(point:PercentBasePoint, inverted:Boolean, w:Number):void{
			var h:Number = PercentBaseSeries(point.series).getHeight();
			var x:Number = PercentBaseSeries(point.series).seriesBound.x;
			var y:Number = PercentBaseSeries(point.series).seriesBound.y;
			
			this.leftBottom.x = x + w/2 - point.bottomWidth*w/2;
			this.rightBottom.x = x + w/2 + point.bottomWidth*w/2;
			this.leftBottom.y = this.rightBottom.y =inverted ? (y + h - point.bottomHeight*h) : (y +  point.bottomHeight*h);
			
			this.leftTop.x = x + w/2 - point.topWidth*w/2;
			this.rightTop.x = x + w/2 + point.topWidth*w/2;
			this.leftTop.y = this.rightTop.y =inverted ? (y + h - point.topHeight*h) : (y + point.topHeight*h);
		}
		
		override public function setAnchorPoint(anchor:uint, pos:Point, inverted:Boolean):void{
			
		}
	}
}