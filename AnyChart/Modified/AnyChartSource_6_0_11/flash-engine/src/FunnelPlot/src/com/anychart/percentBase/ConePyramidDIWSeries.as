package com.anychart.percentBase{
	import com.anychart.seriesPlot.data.BasePoint;
	
	
	public class ConePyramidDIWSeries extends PercentBaseSeries{
		
		override public function onBeforeDraw():void{
			this.currentY = 1;
			var padding:Number = PercentBaseGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = PercentBaseGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = PercentBaseGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = PercentBaseGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var size3D:Number = PercentBaseGlobalSeriesSettings(this.global).size3D;
			for (var i:uint = 0; i<this.points.length ; i++){
				var point:ConePyramidDIWPoint = this.points[i];
				if (point == null)
					continue;
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
			}
		} 
		
		
		override public function createPoint():BasePoint{
			return new ConePyramidDIWPoint();
		}
		
	}
}