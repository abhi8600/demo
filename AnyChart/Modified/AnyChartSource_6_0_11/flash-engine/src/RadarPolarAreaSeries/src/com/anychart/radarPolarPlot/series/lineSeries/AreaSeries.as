package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.radarPolarPlot.RadarPlot;
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class AreaSeries extends LineSeries {
		
		public function AreaSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
		}
		
		override public function createPoint():BasePoint {
			return (RadarPolarPlot(this.plot).usePolarCoords) ? new PolarAreaPoint() : new RadarAreaPoint();
		}
		
		override protected function deserializeClose(data:XML):void {}
		
		/* override public function configureValueScale(scale:BaseScale):void {
			if (!AxesPlot(this.plot).isScatter)
				scale.checkZero();
		} */
	}
}