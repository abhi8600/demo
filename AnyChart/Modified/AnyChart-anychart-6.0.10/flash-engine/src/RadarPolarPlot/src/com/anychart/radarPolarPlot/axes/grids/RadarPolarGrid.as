package com.anychart.radarPolarPlot.axes.grids {
	
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	public final class RadarPolarGrid implements ISerializableRes {
		
		public var enabled:Boolean;
		public var interlaced:Boolean;
		public var drawLastLine:Boolean;
		public var isMinor:Boolean;
		
		public var line:Stroke;
		
		public function RadarPolarGrid()	{
			this.enabled = true;
			this.interlaced = false;
			this.drawLastLine = true;
			this.isMinor = false;
		}
		
		public var evenFill:Fill;
		public var evenHatchFill:HatchFill;
		
		public function evenVisible():Boolean {
			return (this.evenFill != null || this.evenHatchFill != null);
		}
		
		public var oddFill:Fill;
		public var oddHatchFill:HatchFill;
		
		public function oddVisible():Boolean {
			return (this.oddFill != null || this.oddHatchFill != null);
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined) this.enabled = SerializerBase.getBoolean(data.@enabled);
			if (!this.enabled) return;
			
			if (data.@draw_last_line != undefined)
				this.drawLastLine = SerializerBase.getBoolean(data.@draw_last_line);
			
			if (SerializerBase.isEnabled(data.line[0])) {
				this.line = new Stroke();
				this.line.deserialize(data.line[0]);
			}
			
			if (data.@interlaced != undefined)
				this.interlaced = SerializerBase.getBoolean(data.@interlaced);
				
			if (this.interlaced && data.interlaced_fills[0] != null) {
				var even:XML = data.interlaced_fills[0].even[0];
				if (even != null) {
					if (SerializerBase.isEnabled(even.fill[0])) {
						this.evenFill = new Fill();
						this.evenFill.deserialize(even.fill[0],resources);
					}
					if (SerializerBase.isEnabled(even.hatch_fill[0])) {
						this.evenHatchFill = new HatchFill();
						this.evenHatchFill.deserialize(even.hatch_fill[0]);
					}
				}
				var odd:XML = data.interlaced_fills[0].odd[0];
				if (odd != null) {
					if (SerializerBase.isEnabled(odd.fill[0])) {
						this.oddFill = new Fill();
						this.oddFill.deserialize(odd.fill[0],resources);
					}
					if (SerializerBase.isEnabled(odd.hatch_fill[0])) {
						this.oddHatchFill = new HatchFill();
						this.oddHatchFill.deserialize(odd.hatch_fill[0]);
					}
				}
			} 
		}
	}
}