package com.anychart.radarPolarPlot.axes.categorization {
	import com.anychart.radarPolarPlot.axes.Axis;
	import com.anychart.radarPolarPlot.axes.ValueAxis;
	import com.anychart.scales.ScaleMode;
	
	public class ClusterSettings {
		
		public var groupPadding:Number;
		public var pointPadding:Number;
		
		public var numClusters:uint;
		
		private var axes:Object;
		
		public function ClusterSettings() {
			this.numClusters = 0;
			this.axes = {};
		}
		
		public function checkAxis(axis:Axis):uint {
			var index:uint = this.numClusters;
			if (!(axis is ValueAxis) || axis.scale.mode == ScaleMode.NORMAL) {
				this.numClusters++;
				return index;
			}
			var name:String = ValueAxis(axis).name;
			if (this.axes[name] != null)
				return this.axes[name];
			this.axes[name] = index; 
			this.numClusters++;
			return index;
		}
		
		public var clusterWidth:Number;
		private var categoryWidth:Number;
		
		public function calculate(categoryWidth:Number):void {
			//categoryWidth = groupPadding*w + w*count + w*pointPadding*(count-1)
			//w*(groupPadding + count + pointPadding*(count-1)) = categoryWidth 
			this.categoryWidth = categoryWidth;
			this.clusterWidth = categoryWidth/(this.groupPadding + this.numClusters + this.pointPadding*(this.numClusters-1));
		}
		
		public function getOffset(index:uint):Number {
			var offset:Number = this.clusterWidth*(index*(1 + this.pointPadding) + .5 + this.groupPadding/2);
			return offset - this.categoryWidth/2;
		}
			
	}
}