package com.anychart.radarPolarPlot {
	import com.anychart.data.SeriesType;
	import com.anychart.radarPolarPlot.axes.RotatedAxis;
	import com.anychart.radarPolarPlot.axes.ValueAxis;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotPoint;
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.radarPolarPlot.scales.LinearScale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.BasePlot;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.templates.BaseTemplatesManager;
	import com.anychart.templates.GlobalTemplateItems;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	
	import flash.external.ExternalInterface;
	
	public class RadarPolarPlot extends SeriesPlot {
		//--------------------------------
		// TEMPLATES
		//--------------------------------
		[Embed(source="../../../../presets/DefaultTemplate.xml",mimeType="application/octet-stream")]
		private static var XMLContent:Class;
		
		internal static var defaultTemplate:XML = buildDefautlTemplate(new XMLContent());
		
		private static function buildDefautlTemplate(content:*):XML {
			var b:ByteArray = content as ByteArray;			
			var data:XML = GlobalTemplateItems.createTemplateXML(b.readUTFBytes(b.length));
			if (templatesManager == null)
				templatesManager = new SeriesPlotTemplatesManager();
			data = templatesManager.mergeTemplates(BasePlot.getDefaultTemplate(), data);
			for (var i:uint = 0;i<BaseTemplatesManager.extraTemplates.length;i++)
				data = templatesManager.mergeTemplates(BaseTemplatesManager.extraTemplates[i], data);
			return data;
		}
		
		public static function getDefaultTemplate():XML {
			return defaultTemplate;
		}
		
		protected var xAxis:RotatedAxis;
		public var yAxis:ValueAxis;
		public var xCategories:Array;
		private var axisContainer:Sprite;
		private var displayUnderData:Boolean;
		public var drawingModeCircle:Boolean;
		public var usePolarCoords:Boolean;
		
		protected var startAngle:Number = -90;
		protected var isFixedRadius:Boolean = false;
		protected var isPercentRadius:Boolean = true;
		protected var radius:Number = .7;
		
		public function RadarPolarPlot() {
			super();
			this.xCategories = [];
			this.drawingModeCircle = true;
			this.usePolarCoords = false;
		}
		
		public function getXAxis():RotatedAxis {
			return this.xAxis;
		}
		
		public function getYScaleMinimum():Number {
			return this.yAxis.scale.minimum;
		}
		
		public function getXScale():BaseScale {
			return this.xAxis.scale;
		}
		
		public function getYScale():BaseScale {
			return this.yAxis.scale;
		}
		
		override public function draw():void {
			if (this.polarBackground != null)
				this.drawBackground();
			this.drawAxes();
			super.draw();
		}
		
		override public function execResize():void {
			if (this.polarBackground != null)
				this.drawBackground();
			this.drawAxes();
			super.draw();
		}
		
		protected var polarBackground:BackgroundBase;
		protected var polarBackgroundShape:Shape;
		
		private function drawBackground():void {
			var b:Rectangle = this.calcBoundsPaddings(this.bounds);
			var cx:Number = b.x + b.width / 2;
			var cy:Number = b.y + b.height / 2;
			var radius:Number = Math.min(b.height, b.width) / 2;
			var rc:Rectangle = new Rectangle(cx - radius, cy - radius, radius + radius, radius + radius);
			var g:Graphics = this.polarBackgroundShape.graphics;
			g.clear();
			
			var thickness:Number = polarBackground && polarBackground.border ? polarBackground.border.thickness : 0;
			
			if (polarBackground.fill != null)
				polarBackground.fill.begin(g, rc);
			if (polarBackground.border != null )
				polarBackground.border.apply(g, rc);
			
			if (!this.drawingModeCircle){
				var bounds:Rectangle = new Rectangle(cx - radius, cy - radius, radius + radius, radius + radius);
				thickness /= 2;
				bounds.x += thickness;
				bounds.y += thickness;
				bounds.width -= thickness + thickness;
				bounds.height -= thickness + thickness;
				
				if (polarBackground.border)
					polarBackground.border.apply(g, bounds);
				
				
				var crossingAngle:Number = this.getYAxisCrossing();
				var rad:Number = radius + thickness;
				var majIntervals:uint = this.getXScale().majorIntervalsCount;
				var tmp:Number =this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(0)) + crossingAngle;
				var tmpNext:Number =this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(0)) + crossingAngle;
				g.moveTo(DrawingUtils.getPointX(rad, tmp) + cx, DrawingUtils.getPointY(rad, tmp) + cy);
				for (var i:uint = 0; i <= majIntervals; i++){
					tmp = this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(i)) + crossingAngle;
					tmpNext = this.getXScale().localTransform(this.getXScale().getMajorIntervalValue(i+1)) + crossingAngle;
					g.lineTo(DrawingUtils.getPointX(rad, tmpNext) + cx, DrawingUtils.getPointY(rad, tmpNext) + cy);
				}
				
				g.endFill();
				g.lineStyle();
			} 
			else
			{	
				g.drawEllipse(rc.x, rc.y, rc.width, rc.height);
				g.endFill();
				g.lineStyle();
			}
			
			if (polarBackground.hatchFill != null){
				polarBackground.hatchFill.beginFill(g);
				g.drawEllipse(rc.x, rc.y, rc.width, rc.height);
				g.endFill();
			}
			this.polarBackgroundShape.filters = this.polarBackground.effects != null ? this.polarBackground.effects.list : null; 
		}
		
		private function drawAxes():void {
			var len:uint = this.axisContainer.numChildren;
			for (var i:int = 0; i < len; i++)
				this.axisContainer.getChildAt(i)['graphics'].clear();
			if (this.needDedicatedCleanForAxesLinesContainter){
				this.axisLinesContainer.graphics.clear();
				this.tickmarksContainer.graphics.clear();
				this.axisLabelsContainer.graphics.clear();
			}
			this.yAxis.draw();
			this.xAxis.draw();
		}
		
		public function getYAxisCrossing():Number {
			return this.startAngle;
		}
		
		//-----------------------------------------------
		// Bounds reduces calculating
		//-----------------------------------------------
		
		protected function calcBoundsPaddings(bounds:Rectangle):Rectangle {
			var axisBounds:Rectangle = bounds.clone();
			axisBounds.x = 0;
			axisBounds.y = 0;
			
			var cx:Number = axisBounds.width / 2;
			var cy:Number = axisBounds.height / 2;
			var radius:Number = Math.min(cx, cy);
			var minRadius:Number = radius;
			
			if (this.isFixedRadius) {
				if (this.isPercentRadius) {
					minRadius = radius*this.radius;
				}else {
					minRadius = this.radius;
				}
				
				this.reduceRectBounds(axisBounds, radius - minRadius);
				
				return axisBounds;
			}
			
			if (this.xAxis == null)
				return axisBounds;
			
			var tmpRadius:Number;
			var crossing:Number = this.getYAxisCrossing();
			var thickness:Number = this.getPlotOuterThickness();
			var tickmark:Number = 0;

			if (this.xAxis.majorTickmark != null && this.xAxis.majorTickmark.enabled && this.xAxis.majorTickmark.outside)
				tickmark = this.xAxis.majorTickmark.size;
			
			if (this.xAxis.labels != null && this.xAxis.labels.enabled && !this.xAxis.labels.isInsideLabels){
				var info:TextElementInformation;
				var pos:Point = new Point();
				var startIndex:int = this.xAxis.labels.showFirst ? 0 : 1;
				var endIndex:int = this.xAxis.scale.majorIntervalsCount - (this.xAxis.labels.showLast ? 0 : 1);
				if (this is RadarPlot) endIndex -= 1;
				for (var i:int = startIndex;i<=endIndex;i++) {
					info = this.xAxis.labels.getTextInfo(i, true);
					var angle:Number = this.xAxis.scale.localTransform(this.xAxis.scale.getMajorIntervalValue(i)) + crossing;
					tmpRadius = (cx - info.rotatedBounds.width - this.xAxis.labels.padding - tickmark) / 
						Math.abs(DrawingUtils.getPointX(1, angle));
					minRadius = Math.min(minRadius, tmpRadius);
					tmpRadius = (cy - info.rotatedBounds.height - this.xAxis.labels.padding - tickmark) /
						Math.abs(DrawingUtils.getPointY(1, angle));
					minRadius = Math.min(minRadius, tmpRadius);
				}
			}
			
			if (this.xAxis.minorTickmark != null && this.xAxis.minorTickmark.enabled && this.xAxis.minorTickmark.outside)
				minRadius = Math.min(minRadius, radius - this.xAxis.minorTickmark.size);
				
			minRadius = Math.min(minRadius, radius - tickmark);
			
			if (radius > minRadius)
				this.reduceRectBounds(axisBounds, radius - minRadius);
			
			return axisBounds;
		}
		
		protected function reduceRectBounds(rect:Rectangle, d:Number):void{
			rect.x += d;
			rect.width -= d + d;
			rect.y += d;
			rect.height -= d + d;
		}
		
		public function getPlotOuterThickness():Number {
			var res:Number = 0;
			
			if (this.yAxis.majorGrid != null && this.yAxis.majorGrid.enabled && this.yAxis.majorGrid.line != null && this.yAxis.majorGrid.line.enabled && this.yAxis.majorGrid.drawLastLine)
				res = Math.max(res, this.yAxis.majorGrid.line.thickness);
			if (this.yAxis.minorGrid != null && this.yAxis.minorGrid.enabled && this.yAxis.minorGrid.line != null && this.yAxis.minorGrid.line.enabled && this.yAxis.minorGrid.drawLastLine)
				res = Math.max(res, this.yAxis.minorGrid.line.thickness);
			res /= 2;

			if (this.xAxis.line != null && this.xAxis.line.enabled)
				res += this.xAxis.line.thickness;
			return res;
		}
		
		//-----------------------------------------------
		// Global Series Settings
		//-----------------------------------------------
		
		override protected function createGlobalSeriesSettings(seriesType:uint):GlobalSeriesSettings {
			var sClass:Class;
			switch (seriesType) {
//				case SeriesType.BUBBLE:
//					sClass = Class(getDefinitionByName("com.anychart.radarPolarPlot.series.bubbleSeries::BubbleGlobalSeriesSettings"));
//					break;
//				case SeriesType.BAR:
//					sClass = Class(getDefinitionByName("com.anychart.radarPolarPlot.series.barSeries::BarGlobalSeriesSettings"));
////					break;
				default:
				case SeriesType.LINE:
					sClass = Class(getDefinitionByName("com.anychart.radarPolarPlot.series.lineSeries::LineGlobalSeriesSettings"));
					break;
				case SeriesType.AREA:
					sClass = Class(getDefinitionByName("com.anychart.radarPolarPlot.series.lineSeries::AreaGlobalSeriesSettings"));
					break;
				case SeriesType.MARKER:
					sClass = Class(getDefinitionByName("com.anychart.radarPolarPlot.series.markerSeries::MarkerGlobalSeriesSettings"));
					break;
			}
			return new sClass();
		}
		
		override public function getSeriesType(type:*):uint {
			if (type == null) return this.defaultSeriesType;
			switch (SerializerBase.getEnumItem(type)) {
				case "bubble": 		return SeriesType.BUBBLE;
				case "bar":    		return SeriesType.BAR;
				case "line":		return SeriesType.LINE;
				case "area":		return SeriesType.AREA;
				case "marker":		return SeriesType.MARKER;
				default:			return this.defaultSeriesType;
			}
		} 
		
		//-----------------------------------------------
		// Deserialization
		//-----------------------------------------------
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.chart_settings[0] != null && data.chart_settings[0].axes[0] != null)
				this.deserializeAxes(data.chart_settings[0].axes[0], resources);
			super.deserialize(data, resources);
			
			this.xAxis.onAfterDataDeserialize();
			this.yAxis.initAngle();
			this.yAxis.onAfterDataDeserialize();
		}
		
		protected function deserializeRadarPolarSettings(data:XML):void {
			if (data.@start_angle != undefined) this.startAngle = -90+Number(data.@start_angle);
			if (data.@fixed_radius != undefined) this.isFixedRadius = SerializerBase.getBoolean(data.@fixed_radius);
			if (this.isFixedRadius) {
				if (data.@radius != undefined) this.isPercentRadius = SerializerBase.isPercent(data.@radius);
				this.radius = this.isPercentRadius ? SerializerBase.getPercent(data.@radius) : SerializerBase.getNumber(data.@radius);
			}
			
			if (data.@use_polar_coords != undefined) this.usePolarCoords = SerializerBase.getBoolean(data.@use_polar_coords);
			
			//UnderAxes|AboveAxes
			if (data.@display_data_mode != undefined)
				this.displayUnderData = SerializerBase.getLString(data.@display_data_mode) == "aboveaxes";
			else
				this.displayUnderData = true;
		}
		
		protected function createXAxis():RotatedAxis {
			return new RotatedAxis(this);
		}
		
		protected function deserializeXAxis(data:XML, resources:ResourcesLoader):void {
			this.xAxis = this.createXAxis();
			this.xAxis.deserialize(data, resources);
		}
		
		private function deserializeAxes(data:XML, resources:ResourcesLoader):void {
			
			this.yAxis = new ValueAxis(this);	
			this.yAxis.deserialize(data.y_axis[0], resources);
			
			this.deserializeXAxis(data.x_axis[0], resources);
		}
		
		public function checkPoint(point:RadarPolarPlotPoint):void {
			if (isNaN(this.xAxis.scale.dataRangeMinimum) || (point.x < this.xAxis.scale.dataRangeMinimum))
				this.xAxis.scale.dataRangeMinimum = point.x;
			if (isNaN(this.xAxis.scale.dataRangeMaximum) || (point.x > this.xAxis.scale.dataRangeMaximum))
				this.xAxis.scale.dataRangeMaximum = point.x;
		}
		
		public function checkYValue(val:Number):void {
			if (isNaN(this.yAxis.scale.dataRangeMinimum) || (val < this.yAxis.scale.dataRangeMinimum))
				this.yAxis.scale.dataRangeMinimum = val;
			if (isNaN(this.yAxis.scale.dataRangeMaximum) || (val > this.yAxis.scale.dataRangeMaximum))
				this.yAxis.scale.dataRangeMaximum = val;
		}
		
		//-----------------------------------
		// Initialization
		//-----------------------------------
		
		private var axisLinesContainer:Shape;
		private var axisLabelsContainer:Sprite;
		private var tickmarksContainer:Sprite;
		private var needDedicatedCleanForAxesLinesContainter:Boolean;
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			
			var sprite:DisplayObjectContainer = DisplayObjectContainer(super.initialize(bounds));
			if (polarBackground != null){
				this.beforeDataContainer.addChild(this.polarBackgroundShape = new Shape());
				this.polarBackgroundShape.x = 0;
				this.polarBackgroundShape.y = 0;
			}
			this.axisContainer = new Sprite();
			this.axisContainer.x = 0;
			this.axisContainer.y = 0;
			this.axisContainer.mouseChildren = false;
			this.axisContainer.mouseEnabled = false;
			this.beforeDataContainer.mouseEnabled = false;
			this.beforeDataContainer.mouseChildren = false;
			this.beforeDataContainer.addChild(this.axisContainer);
			
			this.afterDataContainer.mouseEnabled = false;
			this.afterDataContainer.mouseChildren = false;
			
			// To try to make this more effective 
			var gridInterlacesFillingContainer:Shape = new Shape();
			gridInterlacesFillingContainer.x = 0;
			gridInterlacesFillingContainer.y = 0;
			var gridLinesContainer:Shape = new Shape();
			gridLinesContainer.x = 0;
			gridLinesContainer.y = 0;
			this.tickmarksContainer = new Sprite();
			this.tickmarksContainer.x = 0;
			this.tickmarksContainer.y = 0;
			this.tickmarksContainer.mouseEnabled = false;
			this.tickmarksContainer.mouseChildren = false;
			
			this.axisLabelsContainer = new Sprite();
			this.axisLabelsContainer.x = 0;
			this.axisLabelsContainer.y = 0;
			this.axisLabelsContainer.mouseEnabled = false;
			this.axisLabelsContainer.mouseChildren = false;
			
			this.axisLinesContainer = new Shape();
			this.axisLinesContainer.x = 0;
			this.axisLinesContainer.y = 0;
			
			this.axisContainer.addChild(gridInterlacesFillingContainer);
			this.axisContainer.addChild(gridLinesContainer);
			if (this.displayUnderData){
				this.axisContainer.addChild(this.axisLinesContainer);
				this.axisContainer.addChild(this.tickmarksContainer);
				this.axisContainer.addChild(this.axisLabelsContainer);
				this.needDedicatedCleanForAxesLinesContainter = false;
			} else {
				this.afterDataContainer.addChild(this.axisLinesContainer);
				this.afterDataContainer.addChild(this.tickmarksContainer);
				this.afterDataContainer.addChild(this.axisLabelsContainer);
				this.needDedicatedCleanForAxesLinesContainter = true;
			}
			
			
			//this.axisContainer.x += axisBounds.x;
			//this.axisContainer.y += axisBounds.y;
			this.xAxis.setContainers(gridInterlacesFillingContainer, gridLinesContainer, tickmarksContainer, axisLabelsContainer, axisLinesContainer);
			this.xAxis.initialize();
			
			this.yAxis.setContainers(gridInterlacesFillingContainer, gridLinesContainer, tickmarksContainer, axisLabelsContainer, axisLinesContainer);
			this.yAxis.initialize();
			
			var axisBounds:Rectangle = this.calcBoundsPaddings(bounds);
			
			this.xAxis.calcBounds(axisBounds);
			this.yAxis.calcBounds(axisBounds);
			
			this.calcCenter();
			
			return sprite;
		}
		
		//---------------------------------------
		// Bounds sending
		//---------------------------------------
		
		override public function calculateResize(bounds:Rectangle):void {
			super.calculateResize(bounds);
			var axisBounds:Rectangle = this.calcBoundsPaddings(bounds);
			this.calcCenter();
			this.xAxis.calcBounds(axisBounds);
			this.yAxis.calcBounds(axisBounds);
		}
		
		public function transform(xValue:Number, yValue:Number, point:Point):void {
			var a:Number = (this.xAxis.scale.transform(xValue) + this.getYAxisCrossing());
			var r:Number = this.yAxis.scale.transform(yValue);
			point.x = DrawingUtils.getPointX(r, a) + this.center.x;
			point.y = DrawingUtils.getPointY(r, a) + this.center.y;
		}
		
		private var center:Point = new Point();
		public function getCenter():Point { return this.center; }
		private function calcCenter():void {
			this.center.x = this.bounds.width / 2;
			this.center.y = this.bounds.height / 2;
		}
		
/*		override public function get markersContainer():Sprite{
			return RadarPolarPlotSeries(this.series[this.series.length-1]).container;
		}*/
	}
}
