package com.anychart.tablePlot {
	
	internal final class BinaryTreeWork {
		
		public var root:BinaryTreeNode = null;

		public function parseArray(source:Array):void {
			var len:uint = source.length;
			for (var i:uint = 0; i < len; i++)
				this.addChild(new BinaryTreeNode(CellDimValue(source[i])));
		} 
		
		public function fillArray(asc:Boolean):Array {
			var i:uint = 0;
			if (this.root == null)
				return null;
			return this.deployNode(this.root, asc);
		} 
		
		private function deployNode(node:BinaryTreeNode, asc:Boolean):Array {
			if (node == null) return new Array();
			var tmpLeft:Array;
			var tmpRight:Array;
			var res:Array = new Array();
			var i:uint;
			var k:uint;
			var len:uint;
			tmpLeft = this.deployNode(node.left, asc);
			tmpRight = this.deployNode(node.right, asc);
			if (asc) {
				len = tmpLeft.length;
				for (i = 0; i < len; i++) 
					res[i] = tmpLeft[i];
				k = len;
				res[k++] = node.value;
				len = tmpRight.length;
				for (i = 0; i < len; i++)
					res[k++] = tmpRight[i];
			} else {
				len = tmpRight.length;
				for (i = 0; i < len; i++) 
					res[i] = tmpRight[i];
				k = len;
				res[k++] = node.value;
				len = tmpLeft.length;
				for (i = 0; i < len; i++)
					res[k++] = tmpLeft[i];
			}
			return res;
		}

		public function addChild(child:BinaryTreeNode):void {
			if (root == null)
				root = child;
			else
				root.addChild(child);
		}
		
		public function clear():void {
			this.clearNode(root);
			this.root = null;
		}
		
		private function clearNode(node:BinaryTreeNode):void {
			if (node == null) return;
			this.clearNode(node.left);
			this.clearNode(node.right);
			node.value = null;
			node.parent = null;
		}
	}
}