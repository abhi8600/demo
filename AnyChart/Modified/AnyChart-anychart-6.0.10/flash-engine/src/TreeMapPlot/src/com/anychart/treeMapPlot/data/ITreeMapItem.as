package com.anychart.treeMapPlot.data {
	import com.anychart.seriesPlot.data.BaseDataElement;
	
	public interface ITreeMapItem {
		
		function deserializeData(deserParams:TreeMapDeserializationParams):void;
		
		function get id():String;
		
		function get parent():TreeMapSeries;
		function set parent(value:TreeMapSeries):void;
		
		function get value():Number;
		function set value(val:Number):void;
				
		function get valueScale():Number;
		function set valueScale(value:Number):void;
		
		function get widthScale():Number;
		function set widthScale(value:Number):void;
		
		function get heightScale():Number;
		function set heightScale(value:Number):void;
		
		function get width():Number;
		function set width(value:Number):void;
		
		function get height():Number;
		function set height(value:Number):void;
		
		function get x():Number;
		function set x(value:Number):void;
		
		function get y():Number;
		function set y(value:Number):void;
		
		function get visible():Boolean;
		function set visible(value:Boolean):void;
		
		function setHover():void;
		function setNormal():void;
	}
}