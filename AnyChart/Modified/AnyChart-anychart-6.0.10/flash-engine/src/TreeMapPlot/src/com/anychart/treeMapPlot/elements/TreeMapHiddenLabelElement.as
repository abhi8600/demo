package com.anychart.treeMapPlot.elements {
	
	import com.anychart.elements.IElementContainer;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.styles.states.StyleState;
	import com.anychart.treeMapPlot.data.TreeMapGlobalSeriesSettings;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public final class TreeMapHiddenLabelElement extends LabelElement {
		
		override protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			var info:TextElementInformation = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			var bounds:Rectangle = new Rectangle(sprite.x, sprite.y, info.rotatedBounds.width, info.rotatedBounds.height);
			if (BasePoint(container).bounds.containsRect(bounds)) super.execDrawing(container, state, sprite, elementIndex);
		}
	}
}