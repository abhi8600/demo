package com.anychart.treeMapPlot.data {
	import com.anychart.seriesPlot.thresholds.IThreshold;

	public final class TreeMapFactory {
		
		public static const BY_TREE:int = 0;
		public static const BY_PARENT:int = 1;
		public static const BY_LEVEL:int = 2;
		
		private static var inputType:int;
		
		public static function initFactory(xmlData:XML):void {
			TreeMapFactory.inputType = TreeMapFactory.getNodesSetType(xmlData);
			if (TreeMapFactory.inputType == TreeMapFactory.BY_LEVEL)
				TreeMapFactory.initIterator(xmlData);
			if (TreeMapFactory.inputType == TreeMapFactory.BY_PARENT)
				TreeMapFactory.parseTree(xmlData);
		}		
		
		public static function createTree(deserParams:TreeMapDeserializationParams, global:TreeMapGlobalSeriesSettings = null):ITreeMapItem {
			switch (TreeMapFactory.inputType){
				case TreeMapFactory.BY_PARENT:
					return createByParent(root, deserParams, null, global, false);
					break;
				case TreeMapFactory.BY_LEVEL:
					return createByLevel(deserParams, global);
					break;
				default:
					return createByTree(deserParams, null, global, false);
					break;
			}
		}
		
		private static function createByParent(node:ByParentNode, deserParams:TreeMapDeserializationParams, parent:TreeMapSeries = null,
			global:TreeMapGlobalSeriesSettings = null, drawSelfHeader:Boolean = true):ITreeMapItem{
			if (node == null)
				return null;
			var res:ITreeMapItem;
			deserParams.xmlData = node.xmlData;
			if (node.children.length > 0 || global){
				res = new TreeMapSeries();
				res.parent = parent;
				var series:TreeMapSeries = TreeMapSeries(res);
				series.drawSelfHeader = drawSelfHeader;
				if (global)
					series.settings = global.settings;
				
				var threshold:IThreshold = deserParams.threshold;
				series.deserializeData(deserParams);
				
				var pointsList:Array = node.children;	
				var pointsCount:uint = pointsList.length;
				series.points = new Array();
				series.value = 0;
				for (var i:int = 0; i < pointsCount; i++) {
					var subNode:ByParentNode = ByParentNode(pointsList[i]);
					deserParams.index = i;
					deserParams.xmlData = subNode.xmlData;
					var point:ITreeMapItem = createByParent(subNode, deserParams, series);
					if (point.value != 0){
						series.value += point.value;
						series.points.push(point);
					}
				}
				
				deserParams.threshold = threshold;
				series.settings.global.sortPoints(series.points);
				
				series.sendValueToPoint();
			} else {
				res = new TreeMapPoint();
				res.parent = parent;
				res.deserializeData(deserParams);
			}
			return res;
		}
		
		private static var root:ByParentNode;
		private static var namedHash:Object;
		private static var summaryHash:Array;
		private static function parseTree(xmlData:XML):void {
			var xmlPoints:XMLList = xmlData.point; 
			var pointsCount:int = xmlPoints.length();
			namedHash = new Object();
			summaryHash = new Array(pointsCount);
			var node:ByParentNode;
			var i:int;
			for (i = 0; i < pointsCount; i++){
				var xmlPoint:XML = xmlPoints[i];
				node = new ByParentNode();
				node.xmlData = xmlPoint;
				summaryHash[i] = node;
				if (xmlPoint.@id != undefined)
					namedHash[String(xmlPoint.@id)] = node;
			}
			
			root = new ByParentNode();
			root.xmlData = xmlData;
			for (i = 0; i < pointsCount; i++){
				node = ByParentNode(summaryHash[i]);
				if (node.xmlData.@parent != undefined){
					var parentId:String = String(node.xmlData.@parent); 
					if (namedHash[parentId] != undefined){
						ByParentNode(namedHash[parentId]).children.push(node);
						continue;
					}
				}
				root.children.push(node);
			}
			
			clearArray(summaryHash);
			clearHash(namedHash);
		}
		
		// ToDo: заменить эти два метода на стандартные из коры
		private static function clearArray(array:Array):void {
			if (array == null) return;
   			var ln:uint = array.length;
   			for (var i:uint= 0; i < ln; i++) {
    			array[i] = null;
    			delete array[i];
   			}
  		}
  		
		private static function clearHash(hash:Object):void {
   			if (hash == null) return;
   			for (var s:String in hash) {
    			hash[s] = null;
    			delete hash[s];
   			}
  		}
		
		private static function createByLevel(deserParams:TreeMapDeserializationParams,	global:TreeMapGlobalSeriesSettings):ITreeMapItem{
			var series:TreeMapSeries;
			if (deserParams.xmlData.point.length() > 0 && global){
				series = new TreeMapSeries();
				series.drawSelfHeader = false;
				series.parent = null;
				series.settings = global.settings;
					
				var threshold:IThreshold = deserParams.threshold;
				series.deserializeData(deserParams);
				
				series.points = new Array();
				series.value = 0;
				var i:int = 0; 
				do {
					deserParams.index = i++;
					deserParams.xmlData = getNext(-Number.MAX_VALUE);
					if (deserParams.xmlData == null)
						break;
					var point:ITreeMapItem = createElement(deserParams, series, -Number.MAX_VALUE);
					series.value += point.value;
					series.points.push(point);
				} while (true);
				
				deserParams.threshold = threshold;
				series.settings.global.sortPoints(series.points);
				
				series.sendValueToPoint();
			} else
				series = null;
			return series;
		}
		
		private static var xmlPointsList:XMLList;
		private static var i:int;
		private static var length:int; 
		private static function initIterator(xmlData:XML):void{
			xmlPointsList = xmlData.point;
			i = 0;	
			length = xmlPointsList.length();
		}
		
		private static function getNext(parentLevel:Number):XML {
			if (i < length)
				if (getLevel(xmlPointsList[i]) <= parentLevel)
					return null;
				else
					return xmlPointsList[i++];
			else
				return null;
		}
		
		private static function lookupForChildren(currLevel:Number):Boolean{
			if (i >= length)
				return false;
			if (getLevel(xmlPointsList[i]) > currLevel)
				return true;
			return false;
		}
		
		private static function getLevel(xmlPoint:XML):Number {
			if (xmlPoint.@level == undefined)
				return 0;
			return Number(xmlPoint.@level);
		}
		
		private static function createElement(deserParams:TreeMapDeserializationParams, parent:TreeMapSeries, parentLevel:Number):ITreeMapItem {
			var res:ITreeMapItem;
			var currLevel:Number = getLevel(deserParams.xmlData);
			if (lookupForChildren(currLevel)){
				res = new TreeMapSeries();
				res.parent = parent;
				var series:TreeMapSeries = TreeMapSeries(res);
					
				series.deserializeData(deserParams);
					
				series.points = new Array();
				series.value = 0;
				var i:int = 0;
				do {
					deserParams.index = i++;
					deserParams.xmlData = getNext(currLevel);
					if (deserParams.xmlData == null)
						break;
					var point:ITreeMapItem = createElement(deserParams, series, currLevel);
					series.value += point.value;
					series.points.push(point);
				} while(true);
				
				series.settings.global.sortPoints(series.points);
				
				series.sendValueToPoint();
			} else {
				res = new TreeMapPoint();
				res.parent = parent;
				res.deserializeData(deserParams);
			}
			return res;
		}
		
		private static function createByTree(deserParams:TreeMapDeserializationParams, parent:TreeMapSeries = null,
			global:TreeMapGlobalSeriesSettings = null, drawSelfHeader:Boolean = true):ITreeMapItem{
			var res:ITreeMapItem;
			if (deserParams.xmlData.point.length() > 0 || global){
				res = new TreeMapSeries();
				res.parent = parent;
				var series:TreeMapSeries = TreeMapSeries(res);
				series.drawSelfHeader = drawSelfHeader;
				if (global)
					series.settings = global.settings;
				
				var threshold:IThreshold = deserParams.threshold;
				series.deserializeData(deserParams);
				
				var xmlPointsList:XMLList = deserParams.xmlData.point;	
				var pointsCount:uint = xmlPointsList.length();
				series.points = new Array(pointsCount);
				series.value = 0;
				for (var i:int = 0; i < pointsCount; i++) {
					deserParams.index = i;
					deserParams.xmlData = xmlPointsList[i];
					var point:ITreeMapItem = createByTree(deserParams, series);
					series.value += point.value;
					series.points[i] = point;
				}
				
				deserParams.threshold = threshold;
				series.settings.global.sortPoints(series.points);
				
				series.sendValueToPoint();
			} else {
				res = new TreeMapPoint();
				res.parent = parent;
				res.deserializeData(deserParams);
			}
			return res;
		}
	
		private static function getNodesSetType(xmlData:XML):int {
			if (xmlData.@input_mode != undefined){
				var inputMode:String = String(xmlData.@input_mode).toLowerCase();
				if (inputMode == "byparent")
					return BY_PARENT;
				if (inputMode == "bylevel")
					return BY_LEVEL;
			}
			// if "bytree"
			return BY_TREE;
		}
	}
}