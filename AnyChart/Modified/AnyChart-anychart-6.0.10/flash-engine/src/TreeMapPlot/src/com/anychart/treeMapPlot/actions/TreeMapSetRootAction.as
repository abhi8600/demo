package com.anychart.treeMapPlot.actions {
	import com.anychart.IAnyChart;
	import com.anychart.actions.Action;
	import com.anychart.formatters.IFormatable;
	import com.anychart.treeMapPlot.TreeMapPlot;
	import com.anychart.treeMapPlot.data.TreeMapSeriesPoint;

	public final class TreeMapSetRootAction extends Action {
		
		public function TreeMapSetRootAction(chart:IAnyChart)
		{
			super(chart);
			this.isValidXML = true;
		}
		
		override public function execute(formatter:IFormatable):Boolean {
			if (!(formatter is TreeMapSeriesPoint))	return false;
			var seriesPoint:TreeMapSeriesPoint = TreeMapSeriesPoint(formatter);
			TreeMapPlot(seriesPoint.plot).drillDown(seriesPoint.parent);		
			return false;		 
		}
		///override public function deserialize(data:XML):void {}
	}
}