package com.anychart.treeMapPlot.data {
	
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;

	internal class TreeMapPoint extends TreeMapBasePoint implements ITreeMapItem {
		
		//------------------------------------------
		// DESERIALIZATION
		//------------------------------------------
		override public function deserializeData(deserParams:TreeMapDeserializationParams):void{
			
			this.series = BaseSeries(this._parent);
			
			if (this._parent.settings.pointContainsUniqueSettings(deserParams.xmlData)) {
				this.settings = series.settings.createCopy(this.settings);
				this.settings.deserializePoint(deserParams.xmlData, deserParams.xmlStyles, deserParams.resources);
			}else {
				this.settings = this._parent.settings;
			}
			
			this.index = deserParams.index;
			this.name = deserParams.index.toString();
			
			this.deserialize(deserParams.xmlData);
			
			this._parent.setElements(this);
			this.deserializeElements(deserParams.xmlData, deserParams.xmlStyles, deserParams.resources);
			
			if (deserParams.xmlData.@color == undefined) {
				this.color = this._parent.color;
			}else {
				this.color = SerializerBase.getColor(deserParams.xmlData.@color);
			}
			
			if (deserParams.xmlData.@hatch_type == undefined) {
				this.hatchType = this._parent.hatchType;
			}else {
				this.hatchType = SerializerBase.getHatchType(deserParams.xmlData.@hatch_type);
			}
			
			this.markerType = this._parent.markerType;
			
			this.threshold = this._parent.threshold;
			this.deserializeThreshold(this.plot.thresholdsList, deserParams.xmlData, deserParams.xmlPalletes);
			
			this.createContainer();
			
			this.initializeStyle();
			this.initializeElementsStyle();
			
			this.initialize();
			
			if (this.threshold != null)
				this.threshold.checkAfterDeserialize(this);
				
			if (this._value != 0)
				this.plot.drawingPoints.push(this);
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@y != undefined)
				this._value = SerializerBase.getNumber(data.@y);
			else if (data.@value != undefined)
				this._value = SerializerBase.getNumber(data.@value);
			else
				this._value = 0;
			if (this._value < 0)
				this._value = 0;
		} 
		
		//-------------------------------------
		// DRAWING
		//-------------------------------------
		
		override protected function drawBox():void {
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			if (state.stroke != null && (state.stroke.thickness >= this.bounds.height || state.stroke.thickness >= this.bounds.width))
				return;
			var g:Graphics = this.container.graphics;
			if (this.hasPersonalContainer)
				this.container.filters = state.effects ? state.effects.list : null;
			if (state.stroke) 
				state.stroke.apply(g, this.bounds, this.color);
			if (state.fill)
				state.fill.begin(g, this.bounds, this.color);		
			g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			g.endFill();	
			g.lineStyle();
			
			if (state.hatchFill) {
				state.hatchFill.beginFill(g, this.hatchType, this.color);
				g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
				g.endFill();
			} 
		}
	}
}