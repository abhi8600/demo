package com.anychart.treeMapPlot.data {
	
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.treeMapPlot.elements.TreeMapCroppedLabelElements;
	import com.anychart.treeMapPlot.elements.TreeMapHiddenLabelElement;
	import com.anychart.treeMapPlot.elements.TreeMapLabelsDrawingMode;
	import com.anychart.treeMapPlot.style.TreeMapCroppedStyleState;
	import com.anychart.treeMapPlot.style.TreeMapHiddenStyleState;
	import com.anychart.treeMapPlot.style.TreeMapStyleState;

	public final class TreeMapGlobalSeriesSettings extends GlobalSeriesSettings {
		
		public function TreeMapGlobalSeriesSettings() {
			super();
			this.applyDataPaletteToSeries = true;
			this.sort = true;
			this.sortDescending = true;
			this.labelsDrawingMode = TreeMapLabelsDrawingMode.CROP;
		}
		public var labelsDrawingMode:uint;
		override protected function get canSortPoints():Boolean { return true; }
		override protected function execSorting(points:Array, sortDescending:Boolean):void{
			points.sortOn("value", sortDescending ? (Array.NUMERIC | Array.DESCENDING) : Array.NUMERIC);
		}
		override public function createSettings():BaseSeriesSettings { return new BaseSeriesSettings(); }
		override public function getSettingsNodeName():String { return "tree_map"; }
		override public function getStyleNodeName():String { return "tree_map_style"; }
		override public function getStyleStateClass():Class { 
			switch (this.labelsDrawingMode){
				case TreeMapLabelsDrawingMode.DISPLAY_ALL : return TreeMapStyleState;;
				case TreeMapLabelsDrawingMode.HIDE: return TreeMapHiddenStyleState;
				default: return TreeMapCroppedStyleState;
			} 
		}
		override public function createLabelElement(isExtra:Boolean=false):LabelElement { 
			switch (this.labelsDrawingMode){
				case TreeMapLabelsDrawingMode.DISPLAY_ALL : return new LabelElement();
				case TreeMapLabelsDrawingMode.HIDE: return new TreeMapHiddenLabelElement();
				default: return new TreeMapCroppedLabelElements();
			} 
		}
		public var enableDrilldown:Boolean = true;
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.@enable_drilldown != undefined){
				this.enableDrilldown = SerializerBase.getBoolean(data.@enable_drilldown);
			}
			if (data.label_settings[0] != null && data.label_settings[0].@drawing_mode != undefined){
				switch (SerializerBase.getEnumItem(data.label_settings[0].@drawing_mode)){
					case "displayall": this.labelsDrawingMode = TreeMapLabelsDrawingMode.DISPLAY_ALL; break;
					case "hide": this.labelsDrawingMode = TreeMapLabelsDrawingMode.HIDE; break;
					//default: this.labelsDrawingMode = TreeMapLabelsDrawingMode.CROP; break;
				}
			}
		}
	}
}