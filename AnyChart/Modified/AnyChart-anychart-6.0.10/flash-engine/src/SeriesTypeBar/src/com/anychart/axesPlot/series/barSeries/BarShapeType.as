package com.anychart.axesPlot.series.barSeries {
	public final class BarShapeType {
		
		public static const BOX:uint = 0;
		public static const CYLINDER:uint = 1;
		public static const PYRAMID:uint = 2;
		public static const CONE:uint = 3;
	}
}