package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.series.barSeries.BarStateStyle;
	import com.anychart.visual.corners.Corners;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывает рисование Bara типа Box
	 *  
	 * @author P@zin
	 * 
	 */
	internal class BoxDrawer extends BarDrawer{
		
		override public function isCornersVisible():Boolean {
			if (this.isStacked)
				return this.getStackSettings().isLastInStack(this.point);
			return true;
		}
		
		override public function isBottomGradientVisible():Boolean {
			return !this.isStacked;
		}
		
		/**
		 * Рисование точки
		 *  
		 * @param g
		 * @param state
		 * 
		 */
		override public function draw(g:Graphics, state:BarStateStyle):void {
			var bc:Corners = null;
			if (state && state.corners) {
				bc = state.corners.createCopy();
				var isLast:Boolean = this.isCornersVisible();
				if (isLast){
					if (this.point.isHorizontal) {
						if (this.point.isInverted)
							state.corners.rightTop = state.corners.rightBottom = 0;
						else 
							state.corners.leftTop = state.corners.leftBottom = 0;
					}else {
						if (this.point.isInverted)
							state.corners.leftTop = state.corners.rightTop = 0;
						else 
							state.corners.leftBottom = state.corners.rightBottom = 0;
					}	
				}else {
					state.corners = null;
				}
			}
			drawRect(g,this.point.bounds,state ? state.corners : null);
			if (bc != null)
				state.corners = bc;
		}
		
		/**
		 * РИсуем непосредственно прямоугольник или прямоугольник с корнерами
		 *  
		 * @param g
		 * @param rect
		 * @param corners
		 * 
		 */
		private static function drawRect(g:Graphics, rect:Rectangle, corners:Corners):void {
			if (corners == null)
				g.drawRect(rect.x, rect.y, rect.width, rect.height);
			else
				corners.drawRect(g, rect);
		}
		
		override public function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			if (this.leftTop.x>this.rightBottom.x ){
				var tmp:Number =this.leftTop.x;
				this.leftTop.x = this.rightBottom.x;
				this.rightBottom.x = tmp; 
			}
			if (this.leftTop.y<0) this.leftTop.y=0;
			if (this.leftTop.y>this.rightBottom.y ){
				var tmp1:Number =this.leftTop.y;
				this.leftTop.y = this.rightBottom.y;
				this.rightBottom.y = tmp1; 
			}
			switch (anchor){
				case Anchor.CENTER:{
					pos.x = (this.leftTop.x+this.rightBottom.x)/2;
					pos.y = (this.leftTop.y+this.rightBottom.y)/2;
					break;
				}
				case Anchor.CENTER_LEFT:{
					pos.x = this.leftTop.x;
					pos.y = (this.leftTop.y+this.rightBottom.y)/2;
					break;
				}
				case Anchor.CENTER_RIGHT:{
					pos.x = this.rightBottom.x;
					pos.y = (this.leftTop.y+this.rightBottom.y)/2;
					break;
				}
				case Anchor.CENTER_BOTTOM:{
					pos.x = (this.leftTop.x+this.rightBottom.x)/2;
					pos.y = this.rightBottom.y;
					break;
				}
				case Anchor.CENTER_TOP:{
					pos.x = (this.leftTop.x+this.rightBottom.x)/2;
					pos.y = this.leftTop.y;
					break;
				}
				case Anchor.LEFT_TOP:{
					pos.x = this.leftTop.x;
					pos.y = this.leftTop.y;
					break;
				}
				case Anchor.RIGHT_TOP:{
					pos.x = this.rightBottom.x;
					pos.y = this.leftTop.y;
					break;
				}
				case Anchor.LEFT_BOTTOM:{
					pos.x = this.leftTop.x;
					pos.y = this.rightBottom.y;
					break;
				}
				case Anchor.RIGHT_BOTTOM:{
					pos.x = this.rightBottom.x;
					pos.y = this.rightBottom.y;
					break;
				}
			}
		}
		
	}
}