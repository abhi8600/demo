package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.series.barSeries.BarPoint;
	import com.anychart.axesPlot.series.barSeries.BarStateStyle;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывает рисование Bar'a типа Cone или Pyramid
	 *  
	 * @author P@zin
	 * 
	 */
	internal class ConePyramidDrawer extends BarDrawer {
		
		/**
		 *Правая верхняя точка 
		 */
		protected var rightTop:Point;
		
		/**
		 *Левая нижняя точка 
		 */
		protected var leftBottom:Point;
				
		public function ConePyramidDrawer():void {
			super();
			this.leftBottom = new Point();
			this.rightTop = new Point();
		}
		
		/**
		 * Инициализирует Drawer
		 *  
		 * @param point
		 * @param startValue
		 * @param endValue
		 * 
		 */
		override public final function initialize(point:BarPoint, startValue:Number, endValue:Number):void {
			super.initialize(point,startValue,endValue);
			var inverted:Boolean = this.getInverted();
			var rect:Rectangle = this.point.bounds;
			if (this.isStacked)
				this.initializeStacked(inverted,rect,startValue,endValue);
			else
				this.initializeNormal(inverted,rect);
		}
		
		
		
		/**
		 * Рисует точку
		 *  
		 * @param g
		 * @param state
		 * 
		 */
		override public final function draw(g:Graphics, state:BarStateStyle):void {
		 	g.moveTo(this.leftBottom.x,this.leftBottom.y);
		 	g.lineTo(this.leftTop.x,this.leftTop.y);
			g.lineTo(this.rightTop.x,this.rightTop.y);
			g.lineTo(this.rightBottom.x,this.rightBottom.y); 
		}
		
		/**
		 * Метод определяет инвертирована ось значений или нет
		 *  
		 * @return 
		 * 
		 */
		protected function getInverted():Boolean { return false; }
		
		/**
		 * Инициализация Drawer'a когда график не стекирован
		 * @param inverted
		 * @param rect
		 * 
		 */
		protected function initializeNormal(inverted:Boolean,rect:Rectangle):void {}
		
		/**
		 * Инициализация Drawer'a когда график стекирован
		 * @param inverted
		 * @param rect
		 * @param startValue
		 * @param endValue
		 * 
		 */
		protected function initializeStacked(inverted:Boolean,rect:Rectangle, startValue:Number, endValue:Number):void {}
		
		override public function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			
			switch (anchor){
				case Anchor.CENTER:{
					pos.x = (this.leftTop.x+this.rightTop.x + this.rightBottom.x + this.leftBottom.x)/4;
					pos.y = (this.leftTop.y+this.rightTop.y + this.rightBottom.y + this.leftBottom.y)/4;
					break;
				}
				case Anchor.CENTER_LEFT:{
					pos.x = (this.leftTop.x+this.leftBottom.x)/2;
					pos.y = (this.leftTop.y+this.rightTop.y + this.rightBottom.y + this.leftBottom.y)/4;
					break;
				}
				case Anchor.CENTER_RIGHT:{
					pos.x = (this.rightTop.x+this.rightBottom.x)/2;
					pos.y = (this.leftTop.y+this.rightTop.y + this.rightBottom.y + this.leftBottom.y)/4;
					break;
				}
				case Anchor.CENTER_BOTTOM:{
					pos.x = (this.leftTop.x+this.rightTop.x + this.rightBottom.x + this.leftBottom.x)/4;
					pos.y = (this.rightBottom.y+this.leftBottom.y)/2;
					break;
				}
				case Anchor.CENTER_TOP:{
					pos.x = (this.leftTop.x+this.rightTop.x + this.rightBottom.x + this.leftBottom.x)/4;
					pos.y = (this.leftTop.y + rightTop.y)/2;
					break;
				}
				case Anchor.LEFT_TOP:{
					pos.x = this.leftTop.x;
					pos.y = this.leftTop.y;
					break;
				}
				case Anchor.RIGHT_TOP:{
					pos.x = this.rightTop.x;
					pos.y = this.rightTop.y;
					break;
				}
				case Anchor.LEFT_BOTTOM:{
					pos.x = this.leftBottom.x;
					pos.y = this.leftBottom.y;
					break;
				}
				case Anchor.RIGHT_BOTTOM:{
					pos.x = this.rightBottom.x;
					pos.y = this.rightBottom.y;
					break;
				}
			}
		}
	}
}