package com.anychart.axesPlot.series.barSeries {
	import com.anychart.axesPlot.data.RangeSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class RangeBarSeries extends RangeSeries {
		
		override public function createPoint():BasePoint {
			return new RangeBarPoint();
		}
	}
}