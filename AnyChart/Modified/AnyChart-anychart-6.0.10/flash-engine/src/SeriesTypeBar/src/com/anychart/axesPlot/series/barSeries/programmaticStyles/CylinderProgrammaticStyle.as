package com.anychart.axesPlot.series.barSeries.programmaticStyles {
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.IBarAquaContainer;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.Graphics;

	public class CylinderProgrammaticStyle extends ProgrammaticStyle {
		protected var fillGrd:Gradient;
		protected var border:Stroke;
		
		override public function initialize(state:StyleState):void {
			this.initDynamicIndexes(state,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			
			var opacity:Number=BackgroundBaseStyleState(state).fill ? BackgroundBaseStyleState(state).fill.opacity : 1;
			
			this.fillGrd = new Gradient(opacity);
			var entries:Array=[];
 			
			entries.push(createKey(0, darkIndex,opacity,true));
        	entries.push(createKey(0.3125,lightIndex,opacity,true));
        	entries.push(createKey(0.38,lightIndex,opacity,true));
        	entries.push(createKey(1,darkIndex,opacity,true));
            this.createDynamicGradient(state,this.fillGrd,entries);
            
            this.border = new Stroke();
            this.border.color = darkIndex;
            this.border.isDynamic = true; 
		}
		
		override public function draw(data:Object):void {
			var point:AxesPlotPoint = AxesPlotPoint(data);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			var color:uint = state.fill.color;
			if (state.fill.isDynamic)
				color = ColorParser.getInstance().getDynamicColor(color,point.color);
			
			this.fillGrd.angle = point.isHorizontal ? 90 : 0;
			g.lineStyle();
			
			// Draw main background.
			{
				this.fillGrd.beginGradientFill(g,point.bounds,color);
				IBarAquaContainer(data).drawShape(g, null);
				g.endFill();
			}
			
			
			// Draw hatch fill
            if (state.hatchFill != null && state.hatchFill.enabled) {
            	state.hatchFill.beginFill(g, point.hatchType, point.color);
            	IBarAquaContainer(data).drawShape(g, null);
            	g.endFill();
            }
            
            // Draw border
            this.border.apply(g, point.bounds, color);
            IBarAquaContainer(data).drawShape(g, null);
            g.lineStyle();
		}
	}
}