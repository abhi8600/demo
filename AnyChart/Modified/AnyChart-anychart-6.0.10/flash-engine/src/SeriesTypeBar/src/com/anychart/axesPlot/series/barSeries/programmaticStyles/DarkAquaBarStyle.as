package com.anychart.axesPlot.series.barSeries.programmaticStyles
{
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.IBarAquaContainer;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class DarkAquaBarStyle extends LightAquaBarStyle {
	
		public function DarkAquaBarStyle()
		{
		}
		
		private var colorA:uint;
		private var colorB:uint;
		private var colorC:uint;
		private var colorD:uint;
		
		protected function getDynamicBlendIndex(state:StyleState,darkFn:Function,lightFn:Function,blendAmount:Number,useColor:Boolean = false, color:uint = 0):uint
		{
			var st:BackgroundBaseStyleState = BackgroundBaseStyleState(state);
			var darkColor:Array;
			var lightColor:Array;
			if (!useColor) {
				var fillColorIndex:uint = st.fill.color;
				var fillColor:Array = ColorParser.getInstance().getNotation(fillColorIndex);
				
				darkColor = fillColor.concat([darkFn]);
			    lightColor = fillColor.concat([lightFn]);
			}else {
				darkColor = [color,darkFn];
				lightColor = [color,lightFn];
			}
		    var blend:Array = lightColor;
		    blend = blend.concat(darkColor);
		    blend.push(blendAmount);
		    blend.push("blend");
		   
		   	return ColorParser.getInstance().addNotation(blend);
		}
		
		protected function initDynamicIndexesEx(state:StyleState,darkFn:Function,lightFn:Function):void
		{
			var st:BackgroundBaseStyleState = BackgroundBaseStyleState(state);
			
			if (st.fill.isDynamic) {
				
				colorA=getDynamicBlendIndex(state,darkFn,lightFn,0);
				colorB=getDynamicBlendIndex(state,darkFn,lightFn,0.008);
				colorC=getDynamicBlendIndex(state,darkFn,lightFn,0.4);
				colorD=getDynamicBlendIndex(state,darkFn,lightFn,1);
				
				st.fill.color = ColorParser.getInstance().parse('%color');
				state.style.addDynamicColor(st.fill.color);
			}else {
				colorA=getDynamicBlendIndex(state,darkFn,lightFn,0, true,st.fill.color);
				colorB=getDynamicBlendIndex(state,darkFn,lightFn,0.008, true,st.fill.color);
				colorC=getDynamicBlendIndex(state,darkFn,lightFn,0.4, true,st.fill.color);
				colorD=getDynamicBlendIndex(state,darkFn,lightFn,1, true,st.fill.color);
				
				st.fill.color = ColorParser.getInstance().parse('%color');
				state.style.addDynamicColor(st.fill.color);
				state.style
				//st.fill.isDynamic = true;
				//lightIndex = ColorParser.getInstance().addNotation(["%color",lightFn]);
			}
			
			state.style.addDynamicColor(colorA);
			state.style.addDynamicColor(colorB);
			state.style.addDynamicColor(colorC);
			state.style.addDynamicColor(colorD);
		}
		
		override public function initialize(state:StyleState):void
		{
			initDynamicIndexes(state,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			initDynamicIndexesEx(state,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			
			
			// Initialize Fill.
			{
				this.fillGrd=new Gradient(1);
				var entries:Array=[];
				var opacity:Number=1;
				
				entries.push(createKey(0,colorA,1*opacity,true));
            	entries.push(createKey(0.5,colorB,1*opacity,true));
            	entries.push(createKey(0.7,colorC,1*opacity,true));
            	entries.push(createKey(1 ,colorD,1*opacity,true));
            	createDynamicGradient(state,this.fillGrd,entries);
			}
			
			// Initialize top shade gradient.
	  		{
	  			this.topShadeGrd=new Gradient(1);
	  			this.topShadeGrd.angle=90;
	  			entries=[];
	  			opacity=1;
	  			entries.push(createKey(0,darkIndex,1*opacity,true));
				entries.push(createKey(1,darkIndex,0,true));
				createDynamicGradient(state,this.topShadeGrd,entries);
				
	  		}
	  		
	  		// Initialize bottom shade gradient.
	  		{
	  			this.bottomShadeGrd=new Gradient(1);
	  			this.bottomShadeGrd.angle=90;
	  			entries=[];
	  			opacity=1;
	  			entries.push(createKey(0,darkIndex,0,true));
				entries.push(createKey(1,darkIndex,1*opacity,true));
				createDynamicGradient(state,this.bottomShadeGrd,entries);
	  		}
	  		
	  		// Initialize blik gradient.
	  		{
	  			this.blikGradient=new Gradient(1);
	  			entries=[];
	  			opacity=1;
	  			
	  			/*
	  			entries.push(createKey(0,0xFFFFFF,0));
	            entries.push(createKey(0.2,0xFFFFFF,Number(160.0/255.0)*opacity));
	            entries.push(createKey(0.25,0xFFFFFF,Number(140.0/255.0)*opacity));
	            entries.push(createKey(0.3,0xFFFFFF,Number(30.0/255.0)*opacity));
	            entries.push(createKey(0.35,0xFFFFFF,0));
	            entries.push(createKey(1,0xFFFFFF,0));
	            */
	            
	            entries.push(createKey(0,0xFFFFFF,Number(180.0/255.0)*opacity));
            	entries.push(createKey(0.25,0xFFFFFF,Number(100.0/255.0)*opacity));
            	entries.push(createKey(0.3,0xFFFFFF,Number(100.0/255.0)*opacity));
            	entries.push(createKey(0.5,0xFFFFFF,Number(110.0/255.0)*opacity));
            	entries.push(createKey(0.501,0xFFFFFF,0));
            	entries.push(createKey(1,0xFFFFFF,0));
	            
	            blikGradient.entries=entries;
	            
	  		}
			
		}
		

		override public function draw(data:Object):void {
			super.draw(data);
			
			var point:AxesPlotPoint = AxesPlotPoint(data);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(point.styleState);
			var g:Graphics = point.container.graphics;
			
			var color:uint = state.fill.color;
			if (state.fill.isDynamic)
				color = ColorParser.getInstance().getDynamicColor(color,point.color);
			
			var rad:Number = 0;
			
			var isAllCornersVisible:Boolean = IBarAquaContainer(point).isAllCornersVisible();
			var isLeftTopCornerVisible:Boolean = isAllCornersVisible;
			var isRightTopCornerVisible:Boolean = isAllCornersVisible;
			var isLeftBottomCornerVisible:Boolean = isAllCornersVisible;
			var isRightBottomCornerVisible:Boolean = isAllCornersVisible;
			
			var isCornersVisible:Boolean = IBarAquaContainer(point).isCornersCanBeVisible();
            
            if (point.isHorizontal) {
                if (isCornersVisible) {
                	rad = point.bounds.height * 0.12;
                	if (!isAllCornersVisible) {
                		if (point.isInverted) {
                		    isLeftTopCornerVisible = true;
	                		isLeftBottomCornerVisible = true;
	                	}else {
	                		isRightTopCornerVisible = true;
	                		isRightBottomCornerVisible = true;
	                	}
                	}
                }
            }else {
                if (isCornersVisible) {
                	rad = point.bounds.width * 0.12;
                	if (!isAllCornersVisible) {
                		if (point.isInverted) {
	                		isLeftBottomCornerVisible = true;
	                		isRightBottomCornerVisible = true;   
	                	}else {
	                		isLeftTopCornerVisible = true;
	                		isRightTopCornerVisible = true;
	                	}
                	}
                }
            }
            
            rad = Math.max(rad,1);  

			var rc:Rectangle = point.bounds;
			
			// Draw main border.
			{
				var borderColor:uint = ColorUtils.getDarkColor(color);
				g.lineStyle(1,borderColor,state.stroke != null ? state.stroke.opacity : state.fill.opacity);
				if (isCornersVisible) {
					g.drawRoundRectComplex(rc.x, rc.y, rc.width, rc.height, 
						isLeftTopCornerVisible ? rad : 0,
						isRightTopCornerVisible ? rad : 0,
						isLeftBottomCornerVisible ? rad : 0,
						isRightBottomCornerVisible ? rad : 0);
				}else {
					g.drawRect(rc.x, rc.y, rc.width, rc.height);
				}
				g.endFill();
				g.lineStyle();
				
			}
		}
	}
}