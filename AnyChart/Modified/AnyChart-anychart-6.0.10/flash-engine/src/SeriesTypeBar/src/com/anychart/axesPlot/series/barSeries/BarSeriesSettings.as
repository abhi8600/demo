package com.anychart.axesPlot.series.barSeries {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.styles.Style;
	
	internal class BarSeriesSettings extends BaseSeriesSettings {
		
		public var shapeType:uint;
		
		override public function createCopy(settings:BaseSeriesSettings=null):BaseSeriesSettings {
			if (settings == null)
				settings = new BarSeriesSettings();
			super.createCopy(settings);
			BarSeriesSettings(settings).shapeType = this.shapeType;
			return settings;
		}
		
		override protected function checkElement(data:XML):Boolean {
			if (super.checkElement(data)) return true;
			if (data.@shape_type != undefined && this.getShapeType(data.@shape_type) != this.shapeType) return true;
			return false;
		}
		
		private function getShapeType(value:*):uint { 
			switch (SerializerBase.getEnumItem(value)) {
				case 'box' : return BarShapeType.BOX;
				case 'cylinder' :
				case 'silver' : return BarShapeType.CYLINDER;
				case 'pyramid' : return BarShapeType.PYRAMID;
				case 'cone' : return BarShapeType.CONE;
			}
			return BarShapeType.BOX; 
		}
		
		override protected function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void{
			super.deserializeElement(data,stylesList,resources);
			this.deserializeShapeType(data);
		}
		
		override public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeGlobal(data, stylesList, resources);
			this.deserializeShapeType(data);
		}
		
		private function deserializeShapeType(data:XML):void {
			if (data.@shape_type !=undefined) this.shapeType = this.getShapeType(data.@shape_type);
			this.checkShapeTypeStyle();
		}
		
		protected function checkShapeTypeStyle():void {
			if (this.shapeType == BarShapeType.CONE) this.setProgrammaticStyle("cone");
			if (this.shapeType == BarShapeType.CYLINDER) this.setProgrammaticStyle("cylinder");
				 
		}
		
		private function setProgrammaticStyle(styleName:String):void {
			this.style = this.style.createCopy();
			var s:Style = this.style;
			s.normal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.normal); 
			s.hover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.hover);
			s.pushed.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.pushed);
			s.missing.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.missing);
			s.selectedNormal.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedNormal);
			s.selectedHover.programmaticStyle = this.global.getProgrammaticStyleClass(<style name={styleName} />, s.selectedHover);
		}
		
	}
}