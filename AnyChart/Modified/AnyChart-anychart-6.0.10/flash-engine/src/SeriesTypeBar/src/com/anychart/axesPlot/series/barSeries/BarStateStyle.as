package com.anychart.axesPlot.series.barSeries {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.corners.Corners;
	
	public final class BarStateStyle extends BackgroundBaseStyleState {
		
		public var corners:Corners;
		
		public function BarStateStyle(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data,resources);
			if (data.corners[0] != null) {
				this.corners = new Corners();
				this.corners.deserialize(data.corners[0]);
			}
			return data;
		}

	}
}