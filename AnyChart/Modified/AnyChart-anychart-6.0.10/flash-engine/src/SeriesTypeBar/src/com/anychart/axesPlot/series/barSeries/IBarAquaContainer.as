package com.anychart.axesPlot.series.barSeries {
	import flash.display.Graphics;
	
	public interface IBarAquaContainer {
		function drawShape(g:Graphics, state:BarStateStyle):void;
		function isCornersCanBeVisible():Boolean;
		function isBottomGradientVisible():Boolean;
		function isAllCornersVisible():Boolean;
	}
}