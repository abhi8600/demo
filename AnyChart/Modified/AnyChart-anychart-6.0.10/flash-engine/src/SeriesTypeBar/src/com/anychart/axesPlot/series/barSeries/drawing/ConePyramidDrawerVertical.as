package com.anychart.axesPlot.series.barSeries.drawing{
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.BarPoint;
	import com.anychart.scales.ScaleMode;
	
	import flash.geom.Rectangle;
	
	internal final class ConePyramidDrawerVertical extends ConePyramidDrawer{
		
		/**
		 * @inheritedDoc
		 */
		override protected function getInverted():Boolean {
			return this.leftTop.y<this.rightBottom.y;
		}
		
		/**
		 * @inheritedDoc
		 */
		override protected function initializeStacked(inverted:Boolean, rect:Rectangle, startValue:Number, endValue:Number):void {
			var stackSettings:StackSettings = this.point.category.getStackSettings(AxesPlotSeries(this.point.series).valueAxis.name, this.point.series.type,AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED);
			var stackSumm:Number = stackSettings.getStackSumm(this.point.y);
			var bottomWidth:Number = rect.width*(stackSumm - startValue)/stackSumm;
			var topWidth :Number  = rect.width*(stackSumm - endValue)/stackSumm;
			
			this.leftBottom.y = this.rightBottom.y = rect.bottom;
			this.leftTop.y = this.rightTop.y = rect.top;

			this.rightBottom.x = rect.x + (!inverted ? (rect.width + bottomWidth)/2 : (rect.width + topWidth)/2); 
			this.leftBottom.x = rect.x + (!inverted ? (rect.width - bottomWidth)/2 : (rect.width - topWidth)/2);
			this.rightTop.x = rect.x + (inverted ? (rect.width + bottomWidth)/2 : (rect.width + topWidth)/2);
			this.leftTop.x = rect.x + (inverted ? (rect.width - bottomWidth)/2 : (rect.width - topWidth)/2);
		}
		
		/**
		 * @inheritedDoc
		 */
		override protected function initializeNormal(inverted:Boolean, rect:Rectangle):void {
			this.rightTop.y = this.leftTop.y = rect.top;
			this.rightBottom.y = this.leftBottom.y = rect.bottom;

			this.rightTop.x = !inverted ? rect.x+rect.width/2 : rect.right;  
			this.leftTop.x =  !inverted ? rect.x+rect.width/2 : rect.left;
			this.leftBottom.x = inverted ? rect.x+rect.width/2 : rect.left;
			this.rightBottom.x = inverted ? rect.x+rect.width/2 : rect.right;
		}
	}
}