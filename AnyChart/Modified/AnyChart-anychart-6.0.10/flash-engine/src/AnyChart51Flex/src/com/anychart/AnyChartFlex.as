package com.anychart {
	import com.anychart.events.AnyChartMultiplePointsEvent;
	import com.anychart.events.AnyChartPointEvent;
	import com.anychart.events.AnyChartPointInfo;
	import com.anychart.events.DashboardEvent;
	import com.anychart.events.EngineEvent;
	import com.anychart.events.PointEvent;
	import com.anychart.events.PointsEvent;
	import com.anychart.events.ValueChangeEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import mx.core.BitmapAsset;
	import mx.core.UIComponent;
	import mx.core.UIComponentGlobals;
	import mx.events.FlexEvent;

	[IconFile("./assets/AnyChart.gif")]
	[Event(name="anychartCreate", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartRender", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartDraw", type="com.anychart.events.EngineEvent")]
	[Event(name="anychartRefresh", type="com.anychart.events.EngineEvent")]
	[Event(name="dashboardViewRender", type="com.anychart.events.DashboardEvent")]
	[Event(name="dashboardViewDraw", type="com.anychart.events.DashboardEvent")]
	[Event(name="pointClick", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointMouseOver", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointMouseOut", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointMouseDown", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointMouseUp", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointSelect", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="pointDeselect", type="com.anychart.events.AnyChartPointEvent")]
	[Event(name="multiplePointsSelect", type="com.anychart.events.AnyChartMultiplePointsEvent")]
	[Event(name="multiplePointsDeselect", type="com.anychart.events.AnyChartMultiplePointsEvent")]
	/*
	 For gauge:
	 event.params {
 		pointerName:String
		pointerValue:String
		gaugeName:String
	 } 
	 */
	[Event(name="valueChange", type="com.anychart.events.ValueChangeEvent")]
	public class AnyChartFlex extends UIComponent {
		
		[Embed(source="./assets/lg.png")]
		private static var EmbedLivePreviewImg:Class;
		
		private static var LOGO_WIDTH:Number = 267;
		private static var LOGO_HEIGHT:Number = 91;
		
		private var logo:DisplayObject;
		private var chart:AnyChart;
		private var chartContainer:Sprite;
		
		private static const IS_TRIAL:Boolean = false;
		
		private var isInitialized:Boolean = false;
		
		public static const VERSION:String = "Version 6.0.10 (build #36916)";
		
		public function AnyChartFlex() {
			super();
			this.width = 400;
			this.height = 300;
			if (!UIComponentGlobals.designMode) {
				this.chartContainer = new Sprite();
				this.addChild(this.chartContainer);
				this.chart = new AnyChart(this.chartContainer);
				this.addEventListener(FlexEvent.INITIALIZE, this.initHandler);
			}
		}
		
		private function initHandler(event:FlexEvent):void {
			var bounds:Rectangle = new Rectangle(0,0,this.width,this.height);
			this.chart.initialize(bounds.clone());
			this.initializeTrial();
			this.resizeTrial(bounds);
			this.initEvents();
			
			this.isInitialized = true;
			if (this._anychartXML != null) this.anychartXML = this._anychartXML;
			else if (this._anychartXMLFile != null) this.anychartXMLFile = this._anychartXMLFile;
		}
		
		private function clearTooltips():void {
			if (UIComponentGlobals.designMode) return;
			if (this.chart) this.chart.clearTooltips();
		}
		
		private var _anychartXML:XML;
		public function set anychartXML(data:XML):void {
			this._anychartXML = data;
			this.clearTooltips();
			if (!UIComponentGlobals.designMode && this.isInitialized) 
				this.chart.setXMLData(data.toXMLString(), null);
		}
		
		public function get anychartXML():XML { return this._anychartXML; }
		
		private var _anychartXMLFile:String;
		public function set anychartXMLFile(path:String):void {
			this._anychartXMLFile = path;
			this.clearTooltips();
			if (!UIComponentGlobals.designMode && this.isInitialized) 
				this.chart.setXMLFile(path);
		} 
		public function get anychartXMLFile():String { return this._anychartXMLFile; }
		
		public function updatePointerData(gaugeName:String, pointerName:String, pointerValue:Number):void {
			if (!UIComponentGlobals.designMode)
				this.chart.updatePointData(gaugeName, pointerName, {value: pointerValue});
		}
		
		public function updatePointerColor(gaugeName:String, pointerName:String, pointerColor:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.updatePointData(gaugeName, pointerName, {color: pointerColor});
		}
		
		public function scrollXTo(value:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.scrollXTo(value);
		}
		
		public function scrollYTo(value:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.scrollYTo(value);
		}
		
		public function scrollTo(xValue:String, yValue: String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.scrollTo(xValue, yValue);
		}
		
		public function setXZoom(settings:DataZoomSettings):void {
			if (!UIComponentGlobals.designMode)
				this.chart.setXZoom(settings);
		}
		
		public function setYZoom(settings:DataZoomSettings):void {
			if (!UIComponentGlobals.designMode)
				this.chart.setYZoom(settings);
		}
		
		public function setZoom(xZoom:DataZoomSettings, yZoom:DataZoomSettings):void {
			if (!UIComponentGlobals.designMode)
				this.chart.setZoom(xZoom, yZoom);
		}
		
		public function getXScrollInfo():ScrollInfo {
			if (!UIComponentGlobals.designMode)
				return this.convertScrollInfo(this.chart.getXScrollInfo());
			return null;
		}
		
		public function getYScrollInfo():ScrollInfo {
			if (!UIComponentGlobals.designMode)
				return this.convertScrollInfo(this.chart.getYScrollInfo());
			return null;
		}
/*		
		public function setPreLoaderText(Text:String):void{
			if (!UIComponentGlobals.designMode)
				this.chart.setLoading(Text);
		}
*/		
		private function convertScrollInfo(info:Object):ScrollInfo {
			if (info == null) return null;
			var res:ScrollInfo = new ScrollInfo();
			res._start = info.start;
			res._range = info.range;
			return res;
		}
		
		//---------------------------------------------------------
		//	Interactivity
		//---------------------------------------------------------
		
		public function setPlotCustomAttribute(attributeName:String, attributeValue:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.setPlotCustomAttribute(attributeName, attributeValue);
		}
		
		
		//-----------------------------------
		// Messages
		//-----------------------------------
		private var _messageLoadingXMLText:String;
		public function set messageLoadingXMLText(Text:String):void {
			this._messageLoadingXMLText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageOnLoadingXML=Text;
		} 
		public function get messageLoadingXMLText():String { return this._messageLoadingXMLText; }
		
		private var _messageWaitingForDataText:String;
		public function set messageWaitingForDataText(Text:String):void {
			this._messageWaitingForDataText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageOnWaitingForData=Text;
		} 
		public function get messageWaitingForDataText():String { return this._messageWaitingForDataText; }
		
		private var _messageInitializeText:String;
		public function set messageInitializeText(Text:String):void {
			this._messageInitializeText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageOnInitialize=Text;
		} 
		public function get messageInitializeText():String { return this._messageInitializeText; }
		
		private var _messageLoadingResourcesText:String;
		public function set messageLoadingResourcesText(Text:String):void {
			this._messageLoadingResourcesText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageOnLoadingResources=Text;
		} 
		public function get messageLoadingResourcesText():String { return this._messageLoadingResourcesText; }
		
		private var _messageLoadingTemplatesText:String;
		public function set messageLoadingTemplatesText(Text:String):void {
			this._messageLoadingTemplatesText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageOnLoadingTemplates=Text;
		} 
		public function get messageLoadingTemplatesText():String { return this._messageLoadingTemplatesText; }
		
		private var _messageNoDataText:String;
		public function set messageNoDataText(Text:String):void {
			this._messageNoDataText = Text;
			if (!UIComponentGlobals.designMode) 
				this.chart.messageNoData=Text;
		} 
		public function get messageNoDataText():String { return this._messageNoDataText; }
		
		//-----------
		//series
		//-----------
		
		public function addSeries(...seriesData):void {
			if (!UIComponentGlobals.designMode) {
				var seriesCnt:uint = seriesData.length;
				var stringSeries:Array = [];
				for (var i:uint = 0;i<seriesCnt;i++) {
					stringSeries.push(seriesData[i].toXMLString());
				}
				this.chart.addSeries.apply(null, stringSeries);
			}
		}
		
		public function removeSeries(seriesId:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.removeSeries(seriesId);
		}
		
		public function addSeriesAt(index:uint, seriesData:XML):void {
			if (!UIComponentGlobals.designMode)
				this.chart.addSeriesAt(index, seriesData.toXMLString());
		}
		
		public function updateSeries(seriesId:String, seriesData:XML):void {
			if (!UIComponentGlobals.designMode)
				this.chart.updateSeries(seriesId, seriesData.toXMLString());
		}
		
		public function showSeries(seriesId:String, isVisible:Boolean):void {
			if (!UIComponentGlobals.designMode)
				this.chart.showSeries(seriesId, isVisible);
		}
		
		//-----------
		//points
		//-----------
		
		public function addPoint(seriesId:String, ...pointsData):void {
			if (!UIComponentGlobals.designMode) {
				var pointsCnt:uint = pointsData.length;
				var stringPoints:Array = [];
				for (var i:uint = 0;i<pointsCnt;i++) {
					stringPoints.push(pointsData[i].toXMLString());
				}
				this.chart.addPoint.apply(null, [seriesId].concat(stringPoints));
			}
		}
		
		public function removePoint(seriesId:String, pointId:String):void {
			if (!UIComponentGlobals.designMode)
				this.chart.removePoint(seriesId, pointId);
		}
		
		public function addPointAt(seriesId:String, index:uint, pointData:XML):void {
			if (!UIComponentGlobals.designMode)
				this.chart.addPointAt(seriesId, index, pointData.toXMLString());
		}
		
		public function updatePoint(seriesId:String, pointId:String, pointData:XML):void {
			if (!UIComponentGlobals.designMode)
				this.chart.updatePoint(seriesId, pointId, pointData.toXMLString());
		}
		
		//-----------
		//refresh/clear
		//-----------
		
		public function refreshChart():void {
			if (!UIComponentGlobals.designMode)
				this.chart.refresh();
		}
		
		public function clearChartData():void {
			if (!UIComponentGlobals.designMode)
				this.chart.clearChartData();
		}
		
		public function animate():void {
			if (!UIComponentGlobals.designMode)
				this.chart.repeatAnimation();
		}
		
		public function saveAsImage(width:Number = NaN, height:Number = NaN):void {
			if (!UIComponentGlobals.designMode)
				this.chart.saveAsImage(width, height);
		}
		
		public function saveAsPDF(width:Number = NaN, height:Number = NaN):void {
			if (!UIComponentGlobals.designMode)
				this.chart.saveAsPDF(width, height);
		}
		
		public function printChart():void {
			if (!UIComponentGlobals.designMode)
				this.chart.printChart();
		}
		
		//-----------
		//states
		//-----------
		
		public function highlightSeries(seriesId:String, highlighted:Boolean):void {
			if (!UIComponentGlobals.designMode)
				this.chart.highlightSeries(seriesId, highlighted);
		}
		
		public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {
			if (!UIComponentGlobals.designMode)
				this.chart.highlightPoint(seriesId, pointId, highlighted);
		}
		
		public function highlightCategory(categoryName:String, highlighted:Boolean):void {
			if (!UIComponentGlobals.designMode)
				this.chart.highlightCategory(categoryName, highlighted);
		}
		
		public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {
			if (!UIComponentGlobals.designMode)
				this.chart.selectPoint(seriesId, pointId, selected);
		}
		
		public function getBase64PNG(width:Number = NaN, height:Number = NaN):String {
			return this.chart.getBase64PNG(width, height);
		}
		
		public function getBase64PDF(width:Number = NaN, height:Number = NaN):String {
			return this.chart.getBase64PDF(width, height);
		}
		
		public function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void {
			if (!UIComponentGlobals.designMode) 
				this.chart.updatePointWithAnimation(seriesId, pointId, newValues, settings);
		}
		
		public function startPointsUpdate():void {
			if (!UIComponentGlobals.designMode) 
				this.chart.startPointsUpdate();
		}
		
		public function updateViewPointWithAnimation(viewId:String, seriesId:String, pointId:String, newValues:Object, settings:Object):void {
			if (!UIComponentGlobals.designMode) 
				this.chart.updateViewPointWithAnimation(viewId, seriesId, pointId, newValues, settings);
		}
		
		public function startViewPointsUpdate(viewId:String):void {
			if (!UIComponentGlobals.designMode) 
				this.chart.startViewPointsUpdate(viewId);
		}
		
		//---------------------------------------------------------
		//	UIComponent override
		//---------------------------------------------------------
		
		override protected function measure():void {
			super.measure();
			measuredMinWidth = 50;
			measuredMinHeight = 50;
		}
		
		override protected function createChildren():void {
			super.createChildren();
			if (UIComponentGlobals.designMode) {
				var asset:BitmapAsset = new EmbedLivePreviewImg() as BitmapAsset;
				this.logo = asset;
				this.addChild(logo);
				/*
				try {
					logo = new getDefinitionByName("mx.controls::Image")();
				}catch (err:Error) {
				}
				
				if (logo == null) {
					try {
						logo = new getDefinitionByName("spark.components::Image")();
					}catch (err:Error) {
					}
				}
				
				if (logo != null) {
					logo["source"] = new EmbedLivePreviewImg();
					addChild(logo);
				}
				
				trace (logo);
				*/
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if (UIComponentGlobals.designMode) {
				graphics.clear();
				graphics.beginFill(0x4E4E4E,1);
				graphics.drawRect(0,0,unscaledWidth, unscaledHeight);
				
				var lWidth:Number = LOGO_WIDTH;
				var lHeight:Number = LOGO_HEIGHT;
				
				if (unscaledWidth < LOGO_WIDTH || unscaledHeight < LOGO_HEIGHT) {
					var scale:Number = Math.min(unscaledWidth/LOGO_WIDTH,unscaledHeight/LOGO_HEIGHT);
					lWidth *= scale;
					lHeight *= scale;
				}
				logo.width = lWidth;
				logo.height = lHeight;
				
				logo.x = (unscaledWidth - lWidth)/2;
				logo.y = (unscaledHeight - lHeight)/2;
			}else {
				var bounds:Rectangle = new Rectangle(0,0,unscaledWidth, unscaledHeight); 
				this.chart.resize(bounds.clone());
				this.resizeTrial(bounds);
			}
		}
		
		//---------------------------------------------------------
		//	Events
		//---------------------------------------------------------
		
		private function initEvents():void {
			this.chart.addEventListener(DashboardEvent.DASHBOARD_VIEW_DRAW, redirectEvent);
			this.chart.addEventListener(DashboardEvent.DASHBOARD_VIEW_RENDER, redirectEvent);
			
			this.chart.addEventListener(EngineEvent.ANYCHART_CREATE, redirectEvent);
			this.chart.addEventListener(EngineEvent.ANYCHART_DRAW, redirectEvent);
			this.chart.addEventListener(EngineEvent.ANYCHART_RENDER, redirectEvent);
			this.chart.addEventListener(EngineEvent.ANYCHART_REFRESH, redirectEvent);
			this.chart.addEventListener(ValueChangeEvent.VALUE_CHANGE, redirectEvent);
			this.chart.addEventListener(PointEvent.POINT_CLICK, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_DESELECT, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_DOWN, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_OUT, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_OVER, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_MOUSE_UP, redirectPointEvent);
			this.chart.addEventListener(PointEvent.POINT_SELECT, redirectPointEvent);
			
			this.chart.addEventListener(PointsEvent.MULTIPLE_POINTS_DESELECT, redirectPointsEvent);
			this.chart.addEventListener(PointsEvent.MULTIPLE_POINTS_SELECT, redirectPointsEvent);
		}
		
		private function redirectEvent(event:Event):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			var e:Event = event.clone();
			this.dispatchEvent(e);
		}
		
		private function redirectPointEvent(event:PointEvent):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			var pointt:Point=new Point(event.mouseX,event.mouseY);
        	var mouseXY:Point = this.globalToLocal(pointt); 
			this.dispatchEvent(new AnyChartPointEvent(event.type, event.point, mouseXY.x, mouseXY.y));
		}
		
		private function redirectPointsEvent(event:PointsEvent):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			if (event.points.length == 0) return;
			
			var pts:Array = [];
			for (var i:int = 0;i<event.points.length;i++)
				pts.push(new AnyChartPointInfo(event.points[i]));
			
			this.dispatchEvent(new AnyChartMultiplePointsEvent(event.type, pts));
		}
		
		public function getScrollableContent():BitmapData {
			return this.chart ? this.chart.getScrollableContent() : null;
		}
		
		//---------------------------------------------------------
		//	Trial
		//---------------------------------------------------------
		
		private var trialBitmap:Bitmap;
		private function initializeTrial():void {
			if (!IS_TRIAL) return;
			
			var fmt:TextFormat = new TextFormat();
			fmt.font = 'Verdana';
			fmt.size = 60;
			
			var trialTextField:TextField = new TextField();
			trialTextField.text = "AnyChart Trial Version";
			trialTextField.autoSize = TextFieldAutoSize.LEFT;
			trialTextField.setTextFormat(fmt);
			
			var d:BitmapData = new BitmapData(trialTextField.width, trialTextField.height, true, 0);
			d.draw(trialTextField);
			
			this.trialBitmap = new Bitmap(d);
			this.trialBitmap.alpha = 0.15;
			
			this.addChild(this.trialBitmap);
		}
		
		private function resizeTrial(bounds:Rectangle):void {
			if (!IS_TRIAL) return;
			this.setBitmapSize(bounds);
		}
		
		private function setBitmapSize(bounds:Rectangle):void {
			var w:Number = bounds.width;
			var h:Number = bounds.height;
			
			if (this.trialBitmap.width > w || this.trialBitmap.height > h)
				this.trialBitmap.scaleX = this.trialBitmap.scaleY = Math.min(w/this.trialBitmap.width, h/this.trialBitmap.height);
			
			this.trialBitmap.x = (w - this.trialBitmap.width)/2;
			this.trialBitmap.y = (h - this.trialBitmap.height)/2;
		}
	}
}