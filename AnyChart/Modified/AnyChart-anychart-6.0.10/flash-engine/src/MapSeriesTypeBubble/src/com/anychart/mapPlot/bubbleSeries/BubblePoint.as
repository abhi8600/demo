package com.anychart.mapPlot.bubbleSeries {
	import com.anychart.mapPlot.data.GeoBasedPoint;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.programmaticStyles.ICircleAquaStyleContainer;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class BubblePoint extends GeoBasedPoint implements ICircleAquaStyleContainer {
		
		public var bubbleSize:Number;
		private var pixelRadius:Number;
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
		}
		
		override public function canAnimate(field:String):Boolean {
			return super.canAnimate(field) || field == "size";
		}
		
		override public function getCurrentValuesAsObject():Object {
			var res:Object = super.getCurrentValuesAsObject();
			res.size = this.bubbleSize;
			return res;
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.size != undefined) this.bubbleSize = newValue.size;
			super.updateValue(newValue);
		}
		
		override protected function deserializeValue(data:XML):void {
			if (data.@size != undefined) this.bubbleSize = SerializerBase.getNumber(data.@size);
			var settings:MapBubbleGlobalSeriesSettings = MapBubbleGlobalSeriesSettings(this.global);
			settings.maximumValueBubbleSize = Math.max(settings.maximumValueBubbleSize, this.bubbleSize);
			settings.minimumValueBubbleSize = Math.min(settings.minimumValueBubbleSize, this.bubbleSize);
			this.isMissing = isNaN(data.@size);
			if (!MapBubbleGlobalSeriesSettings(this.global).displayNegative && SerializerBase.getNumber(data.@size) < 0)
				this.isMissing = true;
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%BubbleSize') return this.bubbleSize;
			if (token == '%BubbleSizePercentOfSeries') {
				if (this.cashedTokens['BubbleSizePercentOfSeries'] == null) {
					this.cashedTokens['BubbleSizePercentOfSeries'] = this.bubbleSize/this.series.getSeriesTokenValue("%SeriesBubbleSizeSum")*100;
				}
				return this.cashedTokens['BubbleSizePercentOfSeries'];
			}
			if (token == '%BubbleSizePercentOfTotal') {
				if (this.cashedTokens['BubbleSizePercentOfTotal'] == null) {
					this.cashedTokens['BubbleSizePercentOfTotal'] = this.bubbleSize/this.plot.getTokenValue('%DataPlotBubbleSizeSum')*100;
				}
				return this.cashedTokens['BubbleSizePercentOfTotal'];
			}
			return super.getPointTokenValue(token);
		}
		
		private function get stateStyle():BackgroundBaseStyleState { return BackgroundBaseStyleState(this.styleState); }
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			this.pixelRadius = MapBubbleGlobalSeriesSettings(this.global).getBubbleSize(this.bubbleSize);
			
			this.bounds.x = this.pixGeo.x - this.pixelRadius;
			this.bounds.y = this.pixGeo.y - this.pixelRadius;
			this.bounds.width = this.pixelRadius*2;
			this.bounds.height = this.pixelRadius*2;
			
			this.drawBubble(this.container.graphics);
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawBubble(this.container.graphics);
		} 
		
		private function drawBubble(g:Graphics):void {
			
			if (this.styleState.programmaticStyle != null) {
				this.styleState.programmaticStyle.draw(this);
			}else {
				if (this.stateStyle.fill != null && this.stateStyle.fill.enabled) {
					this.stateStyle.fill.begin(g,this.bounds,this.color);
					this.drawShape();
					g.endFill();
				}
				
				if (this.stateStyle.hatchFill != null && this.stateStyle.hatchFill.enabled) {
					this.drawShape();
					this.stateStyle.hatchFill.beginFill(g,this.hatchType,this.color);
				}
				
				if (this.stateStyle.stroke != null && this.stateStyle.stroke.enabled)
					this.stateStyle.stroke.apply(g, this.bounds,this.color);
					
				if (this.stateStyle.stroke != null)
					g.lineStyle();
			}
			if (this.hasPersonalContainer) {
				if (this.stateStyle.effects != null && this.stateStyle.effects.enabled) {
					this.container.filters = this.stateStyle.effects.list;
				}else {
					this.container.filters = null;
				}				
			}
		}
		
		public function drawShape():void {
			this.container.graphics.drawCircle(this.pixGeo.x,this.pixGeo.y,this.pixelRadius);
		}
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			this.getAnchorPointByRect(anchor, pos, bounds, padding);
		}
		
		public function getBounds():Rectangle { return this.bounds; }
		public function getActualColor():uint { return this.color; }
		public function getActualHatchType():uint { return this.hatchType; }
		public function getGraphics():Graphics { return this.container.graphics; }
		public function getCurrentState():BackgroundBaseStyleState { return BackgroundBaseStyleState(this.styleState); }
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.BubbleSize = this.bubbleSize;
			res.BubbleSizePercentOfSeries = this.getPointTokenValue("%BubbleSizePercentOfSeries");
			res.BubbleSizePercentOfTotal = this.getPointTokenValue("%BubbleSizePercentOfTotal");
			res.BubbleSizePercentOfCategory = this.getPointTokenValue("%BubbleSizePercentOfCategory");
			return res;
		}
	}
}