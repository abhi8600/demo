package com.anychart.piePlot.data {
	import com.anychart.data.SeriesType;
	import com.anychart.piePlot.PiePlot;
	import com.anychart.piePlot.labels.CustomLabelElement;
	import com.anychart.piePlot.styles.Pie3DAquaProgrammaticStyle;
	import com.anychart.piePlot.styles.PieAquaProgrammaticStyle;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.stroke.Stroke;
	
	public class PiePlotGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		public var radius:Number;
		public var innerRadius:Number;
		public var startAngle:Number;
		public var explodeRadius:Number;
		public var explodeOnClick:Boolean;
		public var isOutside:Boolean;
		public var isPercentConnectorPaddig:Boolean;
		public var connectorPadding:Number;
		public var connectorStyle:Style;
		public var labelEnabled:Boolean;
		public var multipleExplode:Boolean;
		
		public var connector:Stroke;
		
		public var hasExplodedItems:Boolean;
		public var connectorEnabled:Boolean;
		
		private var _labelsPadding:Number;
		public var allowLabelsOverlap:Boolean;
		public var useLogicMethodOfLabelsCalc:Boolean;
		
		public function getLabelsPadding():Number { return this._labelsPadding; }
		
		public function PiePlotGlobalSeriesSettings():void {
			super();
			this.radius = 1;
			this.innerRadius = .3;
			this.applyDataPaletteToSeries = true;
			this.startAngle = 0;
			this.explodeRadius = .1;
			this.explodeOnClick = true;
			this.hasExplodedItems = false;
			this.isOutside = false;
			this.isPercentConnectorPaddig = false;
			this.connectorPadding = 5;
			this.labelEnabled = false;
			this.connectorEnabled = false;
			this._labelsPadding = 20;
			this.multipleExplode = true;
			this.allowLabelsOverlap = false;
			this.useLogicMethodOfLabelsCalc = true;
		}
		
		override public function deserialize(data:XML):void {
			if (data == null) return;
			if (data.@radius != undefined) this.radius = SerializerBase.getNumber(data.@radius)/100;
			if (data.@inner_radius != undefined) this.innerRadius = SerializerBase.getNumber(data.@inner_radius)/100;
			if (data.@start_angle != undefined) this.startAngle = SerializerBase.getNumber(data.@start_angle);
			if (data.@explode != undefined) this.explodeRadius = SerializerBase.getNumber(data.@explode)/100;
			if (data.@explode_on_click != undefined) this.explodeOnClick = SerializerBase.getBoolean(data.@explode_on_click);
			if (SerializerBase.isEnabled(data.label_settings[0]) && data.label_settings[0].@mode != undefined){
				this.isOutside = SerializerBase.getEnumItem(data.label_settings[0].@mode) == "outside";
				this.labelEnabled = true;
				if (data.label_settings[0].@allow_overlap != undefined)
					this.allowLabelsOverlap = SerializerBase.getBoolean(data.label_settings[0].@allow_overlap);
				if (data.label_settings[0].@smart_labels != undefined)
					this.useLogicMethodOfLabelsCalc = SerializerBase.getBoolean(data.label_settings[0].@smart_labels);
			}
			if (data.@allow_multiple_expand != undefined) this.multipleExplode = SerializerBase.getBoolean(data.@allow_multiple_expand); 
				
			
			super.deserialize(data);
			
			if (this.isOutside) {
				if (SerializerBase.isEnabled(data.connector[0])) {
					this.connector = new Stroke();
					this.connectorStyle = new Style(null);
					this.connector.deserialize(data.connector[0],this.connectorStyle);
					this.connectorEnabled = true;
					if (data.connector[0].@padding != undefined)
						if (SerializerBase.isPercent(data.connector[0].@padding)){
							this.isPercentConnectorPaddig = true;
							this.connectorPadding = SerializerBase.getPercent(data.connector[0].@padding)
						}else {
							this.connectorPadding = SerializerBase.getNumber(data.connector[0].@padding);
						}
				}
				
				 if (SerializerBase.isEnabled(data.label_settings[0]) && 
					data.label_settings[0].position[0] != null &&
					data.label_settings[0].position[0].@padding != undefined) {
					this.isPercentLabelPadding = SerializerBase.isPercent(data.label_settings[0].position[0].@padding);
					if (isPercentLabelPadding)
						this._labelsPadding = SerializerBase.getPercent(data.label_settings[0].position[0].@padding);
					else
						this._labelsPadding = SerializerBase.getNumber(data.label_settings[0].position[0].@padding);
				} 
			}
		}
		
		public var isPercentLabelPadding:Boolean;
		public var explodedPoint:PiePlot2DPoint;
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:PiePlot2DSeries = (PiePlot(this.plot).is3D) ? new PiePlot3DSeries() : new PiePlot2DSeries();
			series.type = SeriesType.PIE;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new PiePlotSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "pie_series";
		}
		
		override public function getStyleNodeName():String {
			return "pie_style";
		}
		
		override public function getStyleStateClass():Class {
			return BackgroundBaseStyleState;
		}
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			var pStyle:ProgrammaticStyle = null;
			
			if (PiePlot(this.plot).is3D) {
				pStyle = new Pie3DAquaProgrammaticStyle();
				pStyle.initialize(state); 
			}else if (SerializerBase.getLString(styleNode.@name) == "aqua") {
				pStyle = new PieAquaProgrammaticStyle();
				pStyle.initialize(state);
			}
			return pStyle;
		}
		
		override public function isProgrammaticStyle(styleNode:XML):Boolean {
			return styleNode.@name != undefined && SerializerBase.getLString(styleNode.@name) == "aqua";
		}
		
		override public function createLabelElement(isExtra:Boolean=false):LabelElement {
			var el:CustomLabelElement = new CustomLabelElement();
			el.isExtra = isExtra;
			return el;
		}
		
		override protected function get canSortPoints():Boolean { return false; }
		override protected function get canPreSortPoints():Boolean { return true; }
		override protected function execSorting(points:Array, sortDescending:Boolean):void {
			var opts:Number = Array.NUMERIC;
			if (sortDescending) opts |= Array.DESCENDING;
			points.sortOn("y",opts);
		}
		override protected function execXMLSorting(points:XMLList, sortDescending:Boolean):XMLList {
			var opts:Number = Array.NUMERIC;
			if (sortDescending) opts |= Array.DESCENDING;
			
			var arr:Array = [];
			for each (var item:XML in points)
				arr.push(item);
			arr.sortOn("@y",opts);
			var res:XMLList = new XMLList();
			for (var i:int = 0;i<arr.length;i++)
				res += arr[i];
			return res;
		}
	}
}