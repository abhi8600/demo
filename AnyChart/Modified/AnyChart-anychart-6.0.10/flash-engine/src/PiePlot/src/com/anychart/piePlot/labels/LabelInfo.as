package com.anychart.piePlot.labels{
	import com.anychart.piePlot.data.PiePlot2DPoint;
	
	import flash.geom.Rectangle;
	
	public final class LabelInfo{
		
		public var angle:Number;
		public var height:Number;
		public var width:Number;
		public var point:PiePlot2DPoint;
		public var isShifted:Boolean;
		public var enabled:Boolean;
		public var index:Number;
		
		public function LabelInfo(angle:Number, bounds:Rectangle, point:PiePlot2DPoint, ind:Number){
			this.angle = angle;
			this.height = bounds.height;
			this.width = bounds.width;
			this.point = point;
			this.isShifted = false;
			this.enabled = true;
			this.index = ind;
		}
	}
}