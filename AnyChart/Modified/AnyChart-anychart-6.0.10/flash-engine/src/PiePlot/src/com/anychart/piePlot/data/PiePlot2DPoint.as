package com.anychart.piePlot.data {
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.piePlot.PiePlot;
	import com.anychart.selector.HitTestUtil;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.categories.BaseCategoryInfo;
	import com.anychart.seriesPlot.categories.ICategorizedPoint;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.styles.programmaticStyles.ICircleAquaStyleContainer;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class PiePlot2DPoint extends BasePoint implements ICircleAquaStyleContainer, ICategorizedPoint {
		public var y:Number;
		public var labelIndex:Number = NaN;
		public var connectorShape:Shape;
		
		public var categoryInfo:BaseCategoryInfo;
		
		public function PiePlot2DPoint() {
			super();
		}
		override public function initialize():void {
			super.initialize();
			if (PiePlotGlobalSeriesSettings(this.global).isOutside && PiePlotGlobalSeriesSettings(this.global).labelEnabled){
				this.connectorShape = new Shape();
				if (PiePlotGlobalSeriesSettings(this.global).connectorStyle) {
					PiePlotGlobalSeriesSettings(this.global).connectorStyle.setDynamicColors(this.color);
					PiePlotGlobalSeriesSettings(this.global).connectorStyle.setDynamicGradients(this.color);
				}
			}
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.deserializeYValue(data);
			
			if (this.name != null) {
				this.categoryInfo = PiePlot(this.plot).createCategory(this.name);
				this.categoryInfo.checkPoint(this);
			}
		}
		
		protected function deserializeYValue(data:XML):void {
			if (data.@y == undefined) {
				this.isMissing = true;
			}else {
				this.y = SerializerBase.getNumber(data.@y);
				if (this.y < 0)
					this.isMissing = true;
			}
		}
		
		
		override protected function createElementsSprites():void {
			super.createElementsSprites();
		}
		//-------------------------------------------------
		//				Angles
		//-------------------------------------------------
		
		public var startAngle:Number;
		public var endAngle:Number;
		protected var sweepAngle:Number;
		
		public function calculateAngles():void {
			var previousPt:PiePlot2DPoint;
			if (this.index == 0)
				this.startAngle = PiePlot2DSeries(this.series).startAngle;
			else
				this.startAngle = PiePlot2DPoint(this.series.points[this.index-1]).endAngle;
			var seriesTotal:Number = Number(this.series.getSeriesTokenValue("%SeriesYSum"));
			if (seriesTotal == 0){
				this.sweepAngle = 360/this.series.points.length;
			} else {
				this.sweepAngle = 360*this.y/seriesTotal;
			}			
			this.endAngle = this.startAngle + this.sweepAngle;
		}
		
		//-------------------------------------------------
		//				Formatting
		//-------------------------------------------------
		
		override public function getTokenValue(token:String):* {
			if (token.indexOf("%Category") == 0)
				return categoryInfo.getTokenValue(token);
			return super.getTokenValue(token);
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%Value') return this.y;
			if (token == '%YValue') return this.y;
			if (token == '%YPercentOfSeries') {
				if (this.cashedTokens['YPercentOfSeries'] == null) {
					this.cashedTokens['YPercentOfSeries'] = (this.series.getSeriesTokenValue("%SeriesYSum")!=0) ? this.y/this.series.getSeriesTokenValue("%SeriesYSum")*100 : 0;
				}
				return this.cashedTokens['YPercentOfSeries'];
			}
			if (token == '%YPercentOfTotal') {
				if (this.cashedTokens['YPercentOfTotal'] == null) {
					this.cashedTokens['YPercentOfTotal'] = (this.plot.getTokenValue('%DataPlotYSum')!=0) ? this.y/this.plot.getTokenValue('%DataPlotYSum')*100 : 0;
				}
				return this.cashedTokens['YPercentOfTotal'];
			}
			if (token == '%YPercentOfCategory') {
				if (this.cashedTokens['YPercentOfCategory'] == null) {
					this.cashedTokens['YPercentOfCategory'] = (this.categoryInfo.getCategoryTokenValue('%CategoryYSum')!=0) ? this.y/this.categoryInfo.getCategoryTokenValue('%CategoryYSum')*100 : 0;
				}
				return this.cashedTokens['YPercentOfCategory'];
			}
			return super.getPointTokenValue(token);
		}
		
		public function checkCategory(categoryTokens:Object):void {
			if (categoryTokens['YSum'] == null)
				categoryTokens['YSum'] = 0;
			if (categoryTokens['PointCount'] == null)
				categoryTokens['PointCount'] = 0;
				
			categoryTokens['YSum'] += this.y;
			if (categoryTokens['YMax'] == null || this.y > categoryTokens['YMax'])
				categoryTokens['YMax'] = this.y;
			if (categoryTokens['YMin'] == null || this.y < categoryTokens['YMin'])
				categoryTokens['YMin'] = this.y;
			categoryTokens['PointCount']++;
		}
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			
			res.Category = this.categoryInfo ? this.categoryInfo.name : null;

			res.YValue = this.y;						
			res.YPercentOfSeries = this.getPointTokenValue("%YPercentOfSeries");
			res.YPercentOfTotal = this.getPointTokenValue("%YPercentOfTotal");
			res.YPercentOfCategory = this.getPointTokenValue("%YPercentOfCategory");
			
			return res;
		}
		
		//-------------------------------------------------
		//				Drawing
		//-------------------------------------------------
		
		public function explodeCheck():Boolean {
			if (PiePlotGlobalSeriesSettings(this.global).explodeOnClick) {
				var exPoint:PiePlot2DPoint = PiePlotGlobalSeriesSettings(this.global).explodedPoint;
				if (!PiePlotGlobalSeriesSettings(this.global).multipleExplode && exPoint != null){
					PiePlotSettings(exPoint.settings).exploded = !PiePlotSettings(exPoint.settings).exploded;
					exPoint.resize();
				}
				PiePlotGlobalSeriesSettings(this.global).explodedPoint = this;
				PiePlotSettings(this.settings).exploded = !PiePlotSettings(this.settings).exploded;
				
				return true;
			}
			return false;
		}
		
		override protected function mouseClickActionsExec():void {
			if (this.explodeCheck()) 
				this.resize();
			/*
			if (PiePlotGlobalSeriesSettings(this.global).explodeOnClick) {
				var exPoint:PiePlot2DPoint = PiePlotGlobalSeriesSettings(this.global).explodedPoint;
				if (!PiePlotGlobalSeriesSettings(this.global).multipleExplode && exPoint != null){
					PiePlotSettings(exPoint.settings).exploded = !PiePlotSettings(exPoint.settings).exploded;
					exPoint.resize();
				}
				PiePlotGlobalSeriesSettings(this.global).explodedPoint = this;
				PiePlotSettings(this.settings).exploded = !PiePlotSettings(this.settings).exploded;
				this.resize();
			}*/
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.calcBounds();
			this.drawSector();
		}
		
		public function hasLabel():Boolean {
			return this.labelSprite != null;
		}
		
		protected function calcBounds():void {
			var center:Point = this.getCenter();
			var series:PiePlot2DSeries = PiePlot2DSeries(this.series);
			this.bounds.x = center.x - series.outerRadius;
			this.bounds.y = center.y - series.outerRadius;
			this.bounds.width =  this.bounds.height = series.outerRadius*2;
		}
		
		public function getCenter(pt:Point = null):Point {
			if (pt == null)
				pt = new Point();
			pt.x = PiePlot(this.plot).centerPoint.x;
			pt.y = PiePlot(this.plot).centerPoint.y;
			if (PiePlotSettings(this.settings).exploded) {
				var dR:Number = PiePlot(this.plot).getExplodeRadius();
				var centerAngle:Number = (this.startAngle+this.endAngle)/2;
				pt.x += DrawingUtils.getPointX(dR, centerAngle);
				pt.y += DrawingUtils.getPointY(dR, centerAngle);
			}
			return pt;
		}
		
		override protected function redraw():void {
			super.redraw();
			this.drawSector();
		} 
		
		protected function drawSector():void {
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);

			if (this.styleState.programmaticStyle != null) {
				this.styleState.programmaticStyle.draw(this);
			}else {
				var g:Graphics = this.container.graphics;
				if (state.fill)
					state.fill.begin(g,this.bounds,this.color);
				if (state.stroke)
					state.stroke.apply(g,this.bounds,this.color);	
				this.drawShape();
				g.lineStyle();
				g.endFill();
				if (state.hatchFill){
					state.hatchFill.beginFill(g,this.hatchType,this.color);
					this.drawShape();
					g.endFill();
				}
					
			}
			
			if (this.hasPersonalContainer) {
				if (state.effects != null && state.effects.enabled) {
					this.container.filters = state.effects.list;
				}else {
					this.container.filters = null;
				}				
			}
			this.drawConnector();
		}
		
			
		
		//-------------------------------------------------
		//				Aqia
		//-------------------------------------------------
		
		public function drawShape():void {
			var g:Graphics = this.container.graphics;
			var outerR:Number = PiePlot2DSeries(this.series).outerRadius;
			var innerR:Number = PiePlot2DSeries(this.series).innerRadius;
			var center:Point = this.getCenter();
			
			DrawingUtils.drawArc(g, center.x, center.y,
								 this.startAngle, this.endAngle,
								 outerR, outerR,0, true);
			
			DrawingUtils.drawArc(g, center.x, center.y,
								 this.endAngle, this.startAngle,
								 innerR, innerR,0, false);
			g.lineTo(center.x+DrawingUtils.getPointX(outerR, this.startAngle),
					 center.y+DrawingUtils.getPointY(outerR, this.startAngle));
		}
		
		public function getBounds():Rectangle { return this.bounds; }
		public function getActualColor():uint { return this.color; }
		public function getActualHatchType():uint { return this.hatchType; }
		public function getGraphics():Graphics { return this.container.graphics; }
		public function getCurrentState():BackgroundBaseStyleState { return BackgroundBaseStyleState(this.styleState); }

		
		//-------------------------------------------------
		//				Elements Layout
		//-------------------------------------------------
		
		override protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			this.getCenter(pos);
			
			var outerR:Number = PiePlot2DSeries(this.series).outerRadius;
			var innerR:Number = PiePlot2DSeries(this.series).innerRadius;
			
			var angle:Number;
			var radius:Number;
			switch (anchor) {
				case Anchor.CENTER:
					angle = this.startAngle+this.sweepAngle/2;
					radius = (innerR+outerR)/2;
					break;
				case Anchor.CENTER_BOTTOM:
					angle = this.startAngle+this.sweepAngle/2;
					radius = innerR;
					break;
				case Anchor.CENTER_LEFT:
					angle = this.startAngle;
					radius = (innerR+outerR)/2;
					break;
				case Anchor.CENTER_RIGHT:
					angle = this.endAngle;
					radius = (innerR+outerR)/2;
					break;
				case Anchor.CENTER_TOP:
					angle = this.startAngle + this.sweepAngle/2;
					radius = outerR;
					break;
				case Anchor.LEFT_BOTTOM:
					angle = this.startAngle;
					radius = innerR;
					break;
				case Anchor.LEFT_TOP:
					angle = this.startAngle;
					radius = outerR;
					break;
				case Anchor.RIGHT_BOTTOM:
					angle = this.endAngle;
					radius = innerR;
					break;
				case Anchor.RIGHT_TOP:
					angle = this.endAngle;
					radius = outerR;
					break;
			}
			pos.x += DrawingUtils.getPointX(radius, angle);
			pos.y += DrawingUtils.getPointY(radius, angle);
		}
		
		public function getLabelPosition (state:BaseElementStyleState, sprite:Sprite,bounds:Rectangle):Point {
			if (PiePlotGlobalSeriesSettings(this.global).isOutside && !isNaN(this.labelIndex)){
				var pos:Point = new Point;
				pos = PiePlot(this.plot).getLabelPos(this.labelIndex,true);
				return pos;
			}
			else 
				return this.getElementPosition(state,sprite,bounds);
		}
		
		public function enableLabel(enabled:Boolean = true):void {
 			if (!this.elementsInfo)
				this.elementsInfo = {};
			if (!this.elementsInfo[LABEL_INDEX])
				this.elementsInfo[LABEL_INDEX] = {};
			this.elementsInfo[LABEL_INDEX].enabled = enabled;
		} 
		
		
		private function drawConnector():void {
			if (this.connectorShape)
				this.connectorShape.graphics.clear();
				
			if (!this.labelSprite || !this.isLabelEnabled()){
				return;
			} 
			if (PiePlotGlobalSeriesSettings(this.global).isOutside){
				if (this.elementsInfo[LABEL_INDEX].enabled){
					var firsPoint:Point = new Point();
					var secondPoint:Point = new Point();
					var thirdPoint:Point = new Point();
					this.getCenter(firsPoint);
					
					var angle:Number = PiePlot(this.plot).getLabelAngle(this.labelIndex);
					var connectorPadding:Number = PiePlotGlobalSeriesSettings(this.global).isPercentConnectorPaddig ? 
							(PiePlot(this.plot).outerRadius * PiePlotGlobalSeriesSettings(this.global).connectorPadding) :
							(PiePlotGlobalSeriesSettings(this.global).connectorPadding);
					secondPoint = PiePlot(this.plot).getLabelPos(this.labelIndex,false);
					thirdPoint = PiePlot(this.plot).getLabelPos(this.labelIndex,false);
					
				 	 if ((angle % 360)>90 && (angle % 360)<270)
						secondPoint.x+=connectorPadding;
					else 
						secondPoint.x-=connectorPadding;  
						
					this.drawConnectors(firsPoint,secondPoint,thirdPoint);
				 }
			}
		}
		
		protected function drawConnectors(firstPoint:Point,secondPoint:Point,thirdPoint:Point):void {
			
			var plotRadius:Number = (this.series as Object).outerRadius;
			var angle:Number = (this.startAngle+this.endAngle)/2;
			firstPoint.x += DrawingUtils.getPointX(plotRadius,angle);
			firstPoint.y += DrawingUtils.getPointY(plotRadius,angle);	
			var g:Graphics = this.container.graphics;
			
			this.doConnectorDrawing(firstPoint, secondPoint, thirdPoint, g);
		} 
		
		protected function doConnectorDrawing(firstPoint:Point,secondPoint:Point,thirdPoint:Point, g:Graphics):void{
			var connector:Stroke = PiePlotGlobalSeriesSettings(this.global).connector;
				if (connector != null)		
				if (connector.type == StrokeType.GRADIENT){
					var bound:Rectangle = new Rectangle;
					bound.x = Math.min(firstPoint.x,secondPoint.x,thirdPoint.x);
					bound.y = Math.min(firstPoint.y,secondPoint.y,thirdPoint.y);
					bound.height = Math.max(firstPoint.y,secondPoint.y,thirdPoint.y) - Math.min(firstPoint.y,secondPoint.y,thirdPoint.y);
					bound.width =  Math.max(firstPoint.x,secondPoint.x,thirdPoint.x) - Math.min(firstPoint.x,secondPoint.x,thirdPoint.x);
					connector.apply(g,bound,this.color);
				}else 					
					connector.apply(g,null,this.color);
				
			g.moveTo(firstPoint.x,firstPoint.y);
			g.lineTo(secondPoint.x,secondPoint.y);
			g.lineTo(thirdPoint.x,thirdPoint.y);
			g.endFill();
			g.lineStyle();
		}
		
		override public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			
			var tmp:Shape = new Shape();
			tmp.graphics.beginFill(0xFF0000, 1);
			tmp.graphics.drawRect(0,0, area.width, area.height);
			tmp.x = area.x;
			tmp.y = area.y;
			if (this.container != null && this.container.parent != null)
				this.container.parent.addChild(tmp);
			
			var res:Boolean = HitTestUtil.hitTest(this.container, tmp);
			
			if (this.container != null && this.container.parent != null)
				this.container.parent.removeChild(tmp);
			tmp = null;
			return res;
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "y";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				y: this.y
			};
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.y != undefined) this.y = newValue.y;
			super.updateValue(newValue);
		}
	}
}