package com.anychart.piePlot {
	import com.anychart.piePlot.data.PiePlot3DPointBackSideDrawer;
	import com.anychart.piePlot.data.PiePlot3DPointFrontSideDrawer;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	internal final class PiePlot3DDepthManager {
		
		private var topSidesContainer:Sprite;
		private var sidesContainer:Sprite;
		private var frontSideConnectorsContainer:Sprite;
		private var backSideConnectorsContainers:Sprite;
		
		private var frontSides:Array;
		private var backSides:Array;
		private var sides:Array;
		
		public function PiePlot3DDepthManager(mainContainer:Sprite):void {
			this.sides = [];
			this.frontSides = [];
			this.backSides = [];
			
			this.topSidesContainer = new Sprite();
			this.sidesContainer = new Sprite();
			this.backSideConnectorsContainers = new Sprite();
			this.frontSideConnectorsContainer = new Sprite();
			
			mainContainer.addChild(this.backSideConnectorsContainers);
			mainContainer.addChild(this.sidesContainer);
			mainContainer.addChild(this.topSidesContainer);
			mainContainer.addChild(this.frontSideConnectorsContainer);
		}
		
		public function clear():void {
			this.sides = [];
			this.frontSides = [];
			this.backSides = [];
			
			while (this.backSideConnectorsContainers.numChildren > 0) this.backSideConnectorsContainers.removeChildAt(0);
			this.backSideConnectorsContainers.graphics.clear();
			
			if (this.sidesContainer.numChildren > 0) this.sidesContainer.removeChildAt(0);
			this.sidesContainer.graphics.clear();
			
			if (this.topSidesContainer.numChildren > 0) this.topSidesContainer.removeChildAt(0);
			this.topSidesContainer.graphics.clear();
			
			if (this.frontSideConnectorsContainer.numChildren > 0) this.frontSideConnectorsContainer.removeChildAt(0);
			this.frontSideConnectorsContainer.graphics.clear();
			
			this.sidesInitialized = false;
		}
		
		public function addTopSide(side:Sprite):void {
			this.topSidesContainer.addChild(side);
		}
		
		public function addFrontSide(side:PiePlot3DPointFrontSideDrawer, index: uint):void {
			this.frontSides.push({d:side, i:index});
		}
		
		public function addBackSide(side:PiePlot3DPointBackSideDrawer, index: uint):void {
			this.backSides.push({d:side, i:index});
		}
		
		public function addSide(angle:Number, side:Sprite, index:uint):void {
			this.sides.push({s: Math.sin(angle*Math.PI/180), c:side, i:index});
		}
		
		public function addFrontSideConnector(connector:DisplayObject):void {
			this.frontSideConnectorsContainer.addChild(connector);
		}
		
		public function addBackSideConnector(connector:DisplayObject):void {
			this.backSideConnectorsContainers.addChild(connector);
		}
		
		private var sidesInitialized:Boolean = false;
		
		public function initSides():void {
			if (this.sidesInitialized)
				return;
			var i:uint;		
			var index:uint;	
			var j:uint;
			var k:int;
			var length:int = this.frontSides.length;
			for (i = 0;i<length;i++) {
				if (this.frontSides[i] == null)
					continue;
				var fInfo:PiePlot3DPointFrontSideDrawer = this.frontSides[i].d;
				index = this.frontSides[i].i;
				k = fInfo.length;
				for (j = 0;j<k;j++)
					this.sides.push(
						{
							c: fInfo.getSprite(j), 
							s: (
								fInfo.isInFront() 
									? 1
									: Math.sin(fInfo.getCenter(j)*Math.PI/180)
								),
							i: index
						}
					);
			}
			
			length = this.backSides.length;
			for (i = 0;i<length;i++) {
				if (this.backSides[i] == null)
					continue;
				var bInfo:PiePlot3DPointBackSideDrawer = this.backSides[i].d;
				index = this.backSides[i].i;
				k = bInfo.length;
				for (j = 0;j<k;j++)
					this.sides.push(
						{
							c: bInfo.getSprite(j), 
							s: (
								bInfo.isInFront() 
									? -1
									: Math.sin(bInfo.getCenter(j)*Math.PI/180)
								),
							i: index
						}
					);
			}
			
			length = this.sides.length;
			for (i = 0;i<length;i++) {
				if (this.sides[i].s >= 0)
					this.sides[i].s += uint(this.sides[i].i*2);
				else 
					this.sides[i].s -= uint(this.sides[i].i*2);
			}
			this.sides.sortOn("s",Array.NUMERIC);
			for (i = 0;i<length;i++)
				this.sidesContainer.addChild(this.sides[i].c);
				
			this.sidesInitialized = true;
		}
	}
}