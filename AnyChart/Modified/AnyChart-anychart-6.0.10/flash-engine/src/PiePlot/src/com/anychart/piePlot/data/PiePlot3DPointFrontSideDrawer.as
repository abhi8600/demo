package com.anychart.piePlot.data {
	import com.anychart.piePlot.PiePlot;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public final class PiePlot3DPointFrontSideDrawer {
		
		internal var startAngles:Array;
		internal var endAngles:Array;
		private var point:PiePlot3DPoint;
		
		private var shapes:Array;
		
		public function PiePlot3DPointFrontSideDrawer() {
			this.startAngles = [];
			this.endAngles = [];
			this.shapes = [];
		}
		
		public function initialize(point:PiePlot3DPoint):void {
			
			this.point = point;
			
			var startAngle:Number = point.startAngle;
			var endAngle:Number = point.endAngle;
			
			var startSin:Number = Math.sin(startAngle*Math.PI/180);
			var endSin:Number = Math.sin(endAngle*Math.PI/180);
			
			var startCos:Number = Math.cos(startAngle*Math.PI/180);
			var endCos:Number = Math.cos(endAngle*Math.PI/180);
			
			var startF:uint = getAngleFourth(startCos, startSin);
			var endF:uint = getAngleFourth(endCos, endSin);;
			
			if (startF == 1) {
				switch (endF) {
					case 1:
						if (startCos >= endCos) {
							this.startAngles.push(startAngle);
							this.endAngles.push(endAngle);
						}else {
							this.startAngles.push(startAngle);
							this.endAngles.push(180);
							
							this.startAngles.push(360);
							this.endAngles.push(endAngle);
							
							this.isInFrontFlag = true;
						}
						break;
					case 2:
						this.startAngles.push(startAngle);
						this.endAngles.push(endAngle);
						this.isInFrontFlag = true;
						break;
					case 3:
					case 4:
						this.startAngles.push(startAngle);
						this.endAngles.push(180);
						this.isInFrontFlag = true;
						break;
				}
			}else if (startF == 2) {
				switch (endF) {
					case 1:
						this.startAngles.push(startAngle);
						this.endAngles.push(180);
						
						this.startAngles.push(360);
						this.endAngles.push(endAngle);
						break;
					case 2:
						if (startCos >= endCos) {
							this.startAngles.push(startAngle);
							this.endAngles.push(endAngle);
						}else {
							this.startAngles.push(startAngle);
							this.endAngles.push(180);
							
							this.startAngles.push(360);
							this.endAngles.push(endAngle);
							this.isInFrontFlag = true;
						}
					 	break;
					 case 3:
					 case 4:
					 	this.startAngles.push(startAngle);
					 	this.endAngles.push(180);
					 	break;
				}
			}else if (startF == 3) {
				switch (endF) {
					case 1:
						this.startAngles.push(360);
						this.endAngles.push(endAngle);
						break;
					case 2:
						this.startAngles.push(360);
						this.endAngles.push(endAngle);
						this.isInFrontFlag = true;
						break;
					case 3:
						if (startCos >= endCos) {
							this.startAngles.push(0);
							this.endAngles.push(180);
							this.isInFrontFlag = true;
						}
						break;
				}
			}else if (startF == 4) {
				switch (endF) {
					case 1:
						this.startAngles.push(360);
						this.endAngles.push(endAngle);
						break;
					case 2:
						this.startAngles.push(360);
						this.endAngles.push(endAngle);
						this.isInFrontFlag = true;
						break;
					case 3:
						this.startAngles.push(360);
						this.endAngles.push(180);
						this.isInFrontFlag = true;
						break;
					case 4:
						if (startCos >= endCos) {
							this.startAngles.push(0);
							this.endAngles.push(180);
							this.isInFrontFlag = true;
						}
						break;
				}
			}
			
			for (var i:uint = 0;i<this.startAngles.length;i++) {
				this.shapes.push(new Sprite());
			}
		}
		
		public function clear():void {
			while (this.shapes.length > 0) {
				var shape:Sprite = this.shapes[0];
				if (shape) {
					while (shape.numChildren > 0) shape.removeChildAt(0);
					if (shape.parent) shape.parent.removeChild(shape);
					this.shapes[0] = null;
					this.shapes.splice(0, 1);
				}
			}
		}
		
		public function get length():uint { return this.shapes.length; }
		public function getSprite(index:uint):Sprite { return this.shapes[index]; }
		public function getGraphics(index:uint):Graphics { return this.shapes[index].graphics; }
		public function getCenter(index:uint):Number { 
			var start:Number = this.startAngles[index];
			var end:Number = this.endAngles[index];
			
			if (end < start)
				end += 360;
			
			return (start + end)/2;
		}
		private var isInFrontFlag:Boolean = false;
		public function isInFront():Boolean {return this.isInFrontFlag;}
		
		public function draw(index:uint):void {
			var g:Graphics = this.getGraphics(index);
			
			var outerXR:Number = PiePlot2DSeries(point.series).outerRadius;
			var outerYR:Number = PiePlot3DSeries(point.series).outerYRadius;
			var center:Point = point.getCenter();
			
			var h:Number = PiePlot(point.plot).get3DHeight();
			
			var start:Number = this.startAngles[index];
			var end:Number = this.endAngles[index];
			
			if (end < start)
				end += 360;
				
			DrawingUtils.drawArc(g, center.x, center.y,
								 start, end,
								 outerYR, outerXR,0, true);
			
			DrawingUtils.drawArc(g, center.x, center.y+h,
								 end, start,
								 outerYR, outerXR,0, false);
			g.lineTo(center.x+DrawingUtils.getPointX(outerXR, start),
					 center.y+DrawingUtils.getPointY(outerYR, start));
		}
		
		private static function getAngleFourth(cos:Number, sin:Number):uint {
			if (cos >= 0 && sin >= 0)
				return 1;
			if (cos <= 0 && sin >= 0)
				return 2;
			if (cos <= 0 && sin < 0)
				return 3;
			return 4;
		}
		
		internal static function enabled(startAngle:Number, endAngle:Number):Boolean {
			if (startAngle==endAngle) return false;
			
			startAngle *= Math.PI/180;
			endAngle *= Math.PI/180;
			
			var startCos:Number = Math.cos(startAngle);
			var endCos:Number = Math.cos(endAngle);
			
			var startF:uint = getAngleFourth(startCos, Math.sin(startAngle));
			var endF:uint = getAngleFourth(endCos, Math.sin(endAngle));
			
			if (startF == 1 || startF == 2)
				return true;
			
			if (startF == 3) {
				if (endF == 1 || endF == 2)
					return true;
				if (endF == 3)
					return (startCos >= endCos);
				return false;
			}
			
			if (startF == 4) {
				if (endF == 4)
					return (startCos >= endCos);
				return true;
			}
			
			return false;
		}

	}
}