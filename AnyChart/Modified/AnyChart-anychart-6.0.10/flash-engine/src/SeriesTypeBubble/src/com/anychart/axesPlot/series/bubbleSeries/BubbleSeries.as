package com.anychart.axesPlot.series.bubbleSeries {
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	public class BubbleSeries extends SingleValueSeries {
		
		public function BubbleSeries() {
			super();
			this.isClusterizableOnCategorizedAxis = false;
			
			this.cashedTokens['BubbleSizeSum'] = 0;
			this.cashedTokens['BubbleSizeMax'] = -Number.MAX_VALUE;
			this.cashedTokens['BubbleSizeMin'] = Number.MAX_VALUE;
			this.isSortable = true;
		}
		
		override public function createPoint():BasePoint {
			return new BubblePoint();
		}
		
		override public function checkPoint(point:BasePoint):void {
			super.checkPoint(point);
			var bp:BubblePoint = BubblePoint(point);
			this.cashedTokens['BubbleSizeSum'] += bp.bubbleSize;
			if (bp.bubbleSize > this.cashedTokens['BubbleSizeMax'])
				this.cashedTokens['BubbleSizeMax'] = bp.bubbleSize;
			if (bp.bubbleSize < this.cashedTokens['BubbleSizeMin'])
				this.cashedTokens['BubbleSizeMin'] = bp.bubbleSize;
		}
		
		override public function checkPlot(plotTokensCash:Object):void {
			if (plotTokensCash['BubbleSizeSum'] == null)
				plotTokensCash['BubbleSizeSum'] = 0;
			if (plotTokensCash['BubblePointCount'] == null)
				plotTokensCash['BubblePointCount'] = 0;
			plotTokensCash['BubbleSizeSum'] += this.cashedTokens['BubbleSizeSum'];
			plotTokensCash['BubblePointCount'] += this.points.length;
			
			if (plotTokensCash['BubbleSizeMax'] == null || this.cashedTokens['BubbleSizeMax'] > plotTokensCash['BubbleSizeMax']) 
				plotTokensCash['BubbleSizeMax'] = this.cashedTokens['BubbleSizeMax'];
			if (plotTokensCash['BubbleSizeMin'] == null || this.cashedTokens['BubbleSizeMin'] < plotTokensCash['BubbleSizeMin'])
				plotTokensCash['BubbleSizeMin'] = this.cashedTokens['BubbleSizeMin'];
			super.checkPlot(plotTokensCash);
		}
		
		override protected function checkAxesTokens():void {
			super.checkAxesTokens();
			this.setBubblesTokensToAxis(this.argumentAxis.cashedTokens);
			this.setBubblesTokensToAxis(this.valueAxis.cashedTokens);
		}
		
		private function setBubblesTokensToAxis(tokens:Object):void {
			if (tokens["BubbleSum"] == null)
				tokens["BubbleSum"] = 0;
			
			tokens["BubbleSum"] += this.cashedTokens["BubbleSizeSum"];
			
			if (tokens["BubbleMax"] == null || this.cashedTokens["BubbleSizeMax"] > tokens["BubbleMax"])
				tokens["BubbleMax"] = this.cashedTokens["BubbleSizeMax"];
			if (tokens["BubbleMin"] == null || this.cashedTokens["BubbleSizeMin"] < tokens["BubbleMin"])
				tokens["BubbleMin"] = this.cashedTokens["BubbleSizeMin"];
		}
		
		override public function getSeriesTokenValue(token:String):* {
			if (token == '%SeriesBubbleSizeSum') return this.cashedTokens['BubbleSizeSum'];
			if (token == '%SeriesBubbleMaxSize') return this.cashedTokens['BubbleSizeMax'];
			if (token == '%SeriesBubbleMinSize') return this.cashedTokens['BubbleSizeMin'];
			if (token == '%SeriesBubbleSizeAverage') {
				if (this.cashedTokens['BubbleSizeAverage'] == null)
					this.cashedTokens['BubbleSizeAverage'] = this.cashedTokens['BubbleSizeSum']/this.points.length;
				return this.cashedTokens['BubbleSizeAverage'];
			}
			return super.getSeriesTokenValue(token);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.BubbleSizeSum = this.cashedTokens['BubbleSizeSum'];
			res.BubbleMaxSize = this.cashedTokens['BubbleSizeMax'];
			res.BubbleMinSize = this.cashedTokens['BubbleSizeMin'];
			res.BubbleSizeAverage = this.getSeriesTokenValue("%SeriesBubbleSizeAverage");
			res.BubbleSizeMedian = this.getSeriesTokenValue("%SeriesBubbleSizeMedian");
			res.BubbleSizeMode = this.getSeriesTokenValue("%SeriesBubbleSizeMode");;
			return res;
		}
	}
}