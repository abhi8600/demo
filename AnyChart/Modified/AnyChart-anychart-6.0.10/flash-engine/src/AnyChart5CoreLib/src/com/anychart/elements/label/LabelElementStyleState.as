package com.anychart.elements.label {
	
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.TextFormater;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.text.BaseTextElement;
	
	public class LabelElementStyleState extends BaseElementStyleState {
		
		public var label:BaseTextElement;
		public var isDynamicFormat:Boolean = false;
		public var format:String;
		public var formatRule:TextFormater;
		
		public function LabelElementStyleState(style:Style) {
			super(style);
		}
		
		protected function createLabel():BaseTextElement {
			return new BaseTextElement();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			
			if (!this.enabled) return data;
			 
			if (this.label == null)
				this.label = this.createLabel();
			this.label.deserialize(data, resources, this.style);
			if (data.format[0] != null) {
				this.format = SerializerBase.getCDATAString(data.format[0]);
				this.isDynamicFormat = FormatsParser.isDynamic(this.format);
				if (this.isDynamicFormat)
					this.formatRule = FormatsParser.parse(this.format);
			}
			if (!this.label.enabled)
				this.enabled = false;
				
			if (!this.enabled) return data;
			if (data.position[0] != null) {
				var pos:XML = data.position[0];
				if (pos.@anchor != undefined) this.anchor = SerializerBase.getAnchor(pos.@anchor);
				if (pos.@halign != undefined) this.hAlign = SerializerBase.getHorizontalAlign(pos.@halign);
				if (pos.@valign != undefined) this.vAlign = SerializerBase.getVerticalAlign(pos.@valign);
				if (pos.@padding != undefined) this.padding = SerializerBase.getNumber(pos.@padding); 
			}
			return data;
		}
	}
}