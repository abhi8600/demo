package com.anychart.visual.corners {
	
	public final class CornersType {
		
		public static const SQUARE:uint = 0;
		public static const ROUNDED:uint = 1;
		public static const ROUNDED_INNER:uint = 2;
		public static const CUT:uint = 3;
	}
}