package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.effects.EffectsList;
	
	public class StyleStateWithEffects extends StyleState {
		public var effects:EffectsList;
		
		public function StyleStateWithEffects(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			data = super.deserialize(data, resources);
			if (data.effects[0] != null) {
				if (SerializerBase.isEnabled(data.effects[0],true)) {
					if (this.effects == null) this.effects = new EffectsList();
					this.effects.deserialize(data.effects[0]);
				}else {
					this.effects = null;
				}
			}
			return data;
		}
	}
}