package com.anychart.uiControls.scrollBar {
	import com.anychart.IResizable;
	import com.anychart.visual.background.RectBackground;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	internal class ScrollBarButton extends Sprite implements IResizable {
		
		protected var state:uint;
		protected var size:Rectangle;
		
		internal var container:InteractiveObject;
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.size = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			
			this.container.addEventListener(MouseEvent.MOUSE_UP, containerStopMove);
			this.container.addEventListener(Event.MOUSE_LEAVE, containerStopMove);
			return this; 
		}
		
		public function destroy():void {
			this.size = null;
			this.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			
			if (this.container) {
				this.container.removeEventListener(MouseEvent.MOUSE_UP, containerStopMove);
				this.container.removeEventListener(Event.MOUSE_LEAVE, containerStopMove);
			}
			
			this.container = null;
			this.size = null;
			this.normalBackground = null;
			this.hoverBackground = null;
			this.pushedBackground = null;
		}
		
		protected static const NORMAL:uint = 0;
		protected static const HOVER:uint = 1;
		protected static const PUSHED:uint = 2;
		
		private var isOver:Boolean = false;
		private var isPushed:Boolean = false;
		
		private function onMouseOver(e:Event):void {
			isOver = true;
			if (!isPushed) {
				state = HOVER;
				draw();
			}
		}

		private function onMouseOut(e:Event):void {
			isOver = false;
			if (!isPushed) {
				state = NORMAL;
				draw();
			}
		}
		
		private function onMouseDown(e:Event):void {
			state = PUSHED;
			isPushed = true;
			draw();
		}
		
		private function onMouseUp(e:Event):void {
			isPushed = false;
			state = isOver ? HOVER : NORMAL;
			draw();
		}
		
		private function containerStopMove(e:Event):void {
			if (this.isPushed)
				this.onMouseUp(e);
		}
		
		public var normalBackground:RectBackground;
		public var hoverBackground:RectBackground;
		public var pushedBackground:RectBackground;
		
		public function draw():void {
			if (this.size == null || graphics == null || normalBackground == null || 
				hoverBackground == null || pushedBackground == null)
				return;
				
			var s:Rectangle = size;
			var g:Graphics = graphics;
			
			var bg:RectBackground;
			switch (state) {
				case NORMAL: bg = this.normalBackground; break;
				case HOVER: bg = this.hoverBackground; break;
				case PUSHED: bg = this.pushedBackground; break;
			} 
			
			bg.draw(g, new Rectangle(0,0,s.width, s.height));
			
			this.filters = bg.effects == null ? null : bg.effects.list;
		}
		
		public var dx:Number = 0;
		public var dy:Number = 0;
		public var dw:Number = 0;
		public var dh:Number = 0;
		
		public function resize(newBounds:Rectangle):void {
			if (this.size == null)
				this.size = new Rectangle();
			var s:Rectangle = size;
			this.x = newBounds.x + this.dx;
			this.y = newBounds.y + this.dy;
			s.width = newBounds.width - this.dw;
			s.height = newBounds.height - this.dh;
			
			draw();
		}
	}
}