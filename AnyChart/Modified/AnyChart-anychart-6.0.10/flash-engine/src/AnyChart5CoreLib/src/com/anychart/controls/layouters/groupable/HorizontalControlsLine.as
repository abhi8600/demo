package com.anychart.controls.layouters.groupable {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlLayout;
	
	import flash.geom.Rectangle;
	
	internal class HorizontalControlsLine extends ControlsLine {
		
		public function HorizontalControlsLine(isOpposite:Boolean) {
			super(isOpposite);
		}
		
		override protected function stretchControl(layout:ControlLayout):void {
			layout.isWidthSetted = true;
			layout.isPercentWidth = true;
			layout.width = 1;
		}
		
		override protected function setNearLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.nearControls.alignByDataPlot ? plotRect : chartRect; 
			var x:Number = targetRect.x;
			
			var controls:Array = this.nearControls.controls;
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				control.bounds.x = x;
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				x += control.bounds.width;
				space = Math.max(space, control.bounds.height);
			}
			return space;
		}
		
		override protected function setCenterLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.centerControls.alignByDataPlot ? plotRect : chartRect; 
			var x:Number = targetRect.x + targetRect.width/2;
			var controls:Array = this.centerControls.controls;
			
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				
				control.bounds.x = x - control.bounds.width/2;
				
				space += control.bounds.height;
			}
			return space;
		}
		
		override protected function setFarLayout(chartRect:Rectangle, plotRect:Rectangle):Number {
			var space:Number = 0;
			var targetRect:Rectangle = this.farControls.alignByDataPlot ? plotRect : chartRect; 
			var x:Number = targetRect.right;
			
			var controls:Array = this.farControls.controls;
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				this.setMaxBounds(control.layout, chartRect, plotRect);
				control.setBounds(chartRect, plotRect);
				
				control.bounds.x = x - control.bounds.width;
				x -= control.bounds.width;
				
				space = Math.max(space, control.bounds.height);				
			}
			return space;
		}
		
		override protected function finalizeNearOrFarLayout(controls:Array, space:Number):void {
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite)
					control.bounds.y = basePositionRect.bottom - space;
				else 
					control.bounds.y = basePositionRect.top + space - control.bounds.height;
			}
		}
		
		override protected function finalizeCenterLayout(controls:Array, space:Number):void {
			var y:Number = this.isOpposite ? (basePositionRect.bottom - space) : (basePositionRect.top + space);
			for (var i:uint = 0;i<controls.length;i++) {
				var control:Control = controls[i];
				if (this.isOpposite) {
					control.bounds.y = y;
					y += control.bounds.height;
				}else {
					control.bounds.y = y - control.bounds.height;
					y -= control.bounds.height;
				}
			}
		}
		
		override protected function setMaxBounds(layout:ControlLayout, chartRect:Rectangle, plotRect:Rectangle):void {
			layout.maxWidth = layout.alignByDataPlot ? plotRect.width : chartRect.width;
			layout.maxHeight = chartRect.height*.5;
		}
	}
}