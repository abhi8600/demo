package com.anychart.utils {
	import flash.display.Graphics;
	import flash.geom.Point;
	
	public final class DrawingUtils {
		public static function drawCircleSector(g:Graphics,x:Number, y:Number, r:Number, index:int):void {
			switch (index) {
				case 0:
					g.lineTo(x+r,y);
					g.curveTo(r+x, Math.tan(Math.PI/8)*r+y, Math.sin(Math.PI/4)*r+x,Math.sin(Math.PI/4)*r+y);
					g.curveTo(Math.tan(Math.PI/8)*r+x, r+y, x, r+y);
					break;
				case 1:
					g.lineTo(x,y+r);
					g.curveTo(-Math.tan(Math.PI/8)*r+x, r+y, -Math.sin(Math.PI/4)*r+x,Math.sin(Math.PI/4)*r+y);
					g.curveTo(-r+x, Math.tan(Math.PI/8)*r+y, -r+x, y);
					break;
				case 2:
					g.lineTo(x-r,y);
					g.curveTo(-r+x, -Math.tan(Math.PI/8)*r+y, -Math.sin(Math.PI/4)*r+x,-Math.sin(Math.PI/4)*r+y);
					g.curveTo(-Math.tan(Math.PI/8)*r+x, -r+y, x, -r+y);
					break;
				case 3:
					g.lineTo(x,y-r);
					g.curveTo(Math.tan(Math.PI/8)*r+x, -r+y, Math.sin(Math.PI/4)*r+x,-Math.sin(Math.PI/4)*r+y);
					g.curveTo(r+x, -Math.tan(Math.PI/8)*r+y, r+x, y);
					break;
			}
		}
		
		//*/
		public static function drawArc(graphics:Graphics, centerX:Number, centerY:Number, startAngle:Number,endAngle:Number,hRadius:Number,wRadius:Number,borderWidth:Number,needMoveTo:Boolean = true):void {
			
			var x:Number;
			var y:Number;			
			var angle:Number = startAngle;
			
			x = centerX + getPointX(wRadius,angle);
			y = centerY + getPointY(hRadius,angle);
			
			if (needMoveTo)
				graphics.moveTo(x,y);
			else
				graphics.lineTo(x,y);
			
			var step:Number = getAngleStep(Math.max(hRadius,wRadius));
			if (endAngle < startAngle)
				step = -step;
				
			var doDraw:Boolean = step > 0 ? angle < endAngle : angle > endAngle;
			while (doDraw) {
				
				x = centerX + getPointX(wRadius,angle);
				y = centerY + getPointY(hRadius,angle);
				graphics.lineTo(x,y);
				angle += step;
				doDraw = step > 0 ? angle < endAngle : angle > endAngle;
			}
			
			angle = endAngle;
			x = centerX + getPointX(wRadius,angle);
			y = centerY + getPointY(hRadius,angle);
			
			graphics.lineTo(x,y);
		}
		//*/
		public static function drawArc2(graphics:Graphics, centerX:Number, centerY:Number, startAngle:Number,endAngle:Number,hRadius:Number,
				wRadius:Number,borderWidth:Number,needMoveTo:Boolean = true):void {
			var angle:Number;
			var PIQuarter:Number = 45;
			var array:Array = [new Point(
					getPointX(wRadius,startAngle),
					getPointY(hRadius,startAngle)
				)];
				
			if (endAngle < startAngle)
				PIQuarter = -PIQuarter;
			for (
				angle = (Math.floor(startAngle / PIQuarter) + 1) * PIQuarter; 
				((angle < endAngle) && (angle > startAngle)) || ((angle < startAngle) && (angle > endAngle)); 
				angle += PIQuarter
			)
				array.push(new Point(
					getPointX(wRadius,angle),
					getPointY(hRadius,angle)
				));
			
			array.push(new Point(
					getPointX(wRadius,endAngle),
					getPointY(hRadius,endAngle)
				));
			if (needMoveTo)
				graphics.moveTo(array[0].x + centerX, array[0].y + centerY);
			else
				graphics.lineTo(array[0].x + centerX, array[0].y + centerY);
			var len:int = array.length - 1;
			for (var i:int = 0; i < len; i++){
				var x1:Number = array[i].x;
				var y1:Number = array[i].y;
				var x2:Number = array[i+1].x;
				var y2:Number = array[i+1].y;
				var a2:Number = wRadius * wRadius;
				var b2:Number = hRadius * hRadius;
				
				var vertical:Boolean = y1 == 0 || wRadius == 0;
				var k:Number = vertical ? 0 : -((x1 * b2) / (y1 * a2));
				var l1:LineEquation = new LineEquation(k, vertical ? x1 : b2 / y1, vertical);
				
				vertical = y2 == 0 || wRadius == 0;
				k = vertical ? 0 : -((x2 * b2) / (y2 * a2));
				var l2:LineEquation = new LineEquation(k, vertical ? x2 : b2 / y2, vertical);
				
				var p:Point = LineEquation.getCrossPoint(l1, l2);
				graphics.curveTo(p.x + centerX, p.y + centerY, x2 + centerX, y2 + centerY);
			}
		}//*/
		
		public static function getAngleStep(radius:Number):Number {
			//FIXME check circle length
			return 1;
		}
		
		public static function getPointX(wRadius:Number, angle:Number):Number {
			if (wRadius <= 0) return 0;
			return wRadius*Math.round(Math.cos(angle * Math.PI/180) * 1e15) / 1e15;
		}
		
		public static function getPointY(hRadius:Number, angle:Number):Number {
			if (hRadius <= 0) return 0;
			return hRadius*Math.round(Math.sin(angle * Math.PI/180) * 1e15) / 1e15;
		}
		
		public static function drawDashedLine(g:Graphics, gap:Number, len:Number, startX:Number, startY:Number, endX:Number, endY:Number):void {
			var segLength:Number = len + gap;
			var deltaX:Number = endX - startX;
			var deltaY:Number = endY - startY;
			
			var delta:Number = Math.sqrt(Math.pow(deltaX,2) + Math.pow(deltaY,2));
			var segs:Number = Math.floor(Math.abs(delta / segLength));
			
			var cx:Number = startX;
			var cy:Number = startY;

			var radians:Number = Math.atan2(deltaY,deltaX);
			
			deltaX = Math.cos(radians)*segLength;
			deltaY = Math.sin(radians)*segLength;
			
			for (var n:uint = 0; n < segs; n++) {
				g.moveTo(cx,cy);
				g.lineTo(cx+Math.cos(radians)*len,cy+Math.sin(radians)*len);
				cx += deltaX;
				cy += deltaY;
			}
			
			g.moveTo(cx,cy);
			delta = Math.sqrt(Math.pow(endX-cx,2)+Math.pow(endY-cy,2));
			if(delta>len)
				g.lineTo(cx+Math.cos(radians)*len,cy+Math.sin(radians)*len);
			else if(delta>0)
				g.lineTo(cx+Math.cos(radians)*delta,cy+Math.sin(radians)*delta);
			
			g.moveTo(endX,endY);
		}
	}
}