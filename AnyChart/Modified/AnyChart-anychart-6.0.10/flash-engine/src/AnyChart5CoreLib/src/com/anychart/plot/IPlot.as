package com.anychart.plot {
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.PlotControlsList;
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.controls.legend.ILegendItemsContainer;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.controls.legend.IconSettings;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.templates.BaseTemplatesManager;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public interface IPlot extends ISerializableRes, ILegendTag, IObjectSerializable {
		
		function updatePointWithAnimation(seriesId:String, pointId:String, newValues:Object, settings:Object):void;
		function startPointsUpdate():void;
		
		function get tooltipsContainer():DisplayObjectContainer;
		function preInitialize():void;
		function postInitialize():void;
		function setControlsList(controls:PlotControlsList):void;
		function setBase(chart:IAnyChart):void;
		function setView(view:Object):void;
		function setContainer(chart:IChart):void;
		function setPlotType(value:String):void;
		function getBounds():Rectangle;
		function getTemplatesManager():BaseTemplatesManager;
		function updateData(...params:*):void;
		function getLegendData(item:XML, container:ILegendItemsContainer):void;
		function drawDataIcon(iconSettings:IconSettings, source:ILegendItem, g:Graphics, dx:Number, dy:Number):void;
		function createIconSettings():IconSettings;
		function setAnimationManager(manager:AnimationManager):void;
		
		function initialize(bounds:Rectangle):DisplayObject;
		function draw():void;
		
		function calculateResize(bounds:Rectangle):void;
		function execResize():void;
		
		function checkNoData(data:XML):Boolean;
		
		function destroy():void;
		
		/*
			settings fields: start:Number, end:Number, range:Number, rangeUnit:String 
		*/
		function setXZoom(settings:Object):void;
		function setYZoom(settings:Object):void;
		function setZoom(xZoomSettings:Object, yZoomSettings:Object):void;
		function scrollXTo(xValue:String):void;
		function scrollYTo(yValue:String):void;
		function scrollTo(xValue:String, yValue:String):void;
		function getXScrollInfo():Object;
		function getYScrollInfo():Object;
		
		//------------ data update
		function setPlotCustomAttribute(attributeName:String, attributeValue:String):void;
		
		function addSeries(...seriesData):void;
		function removeSeries(sereisId:String):void;
		function addSeriesAt(index:uint, seriesData:String):void;
		function updateSeries(seriesId:String, seriesData:String):void;
		function showSeries(seriesId:String, isVisible:Boolean):void;
		
		function showAxisMarker(markerId:String, isVisible:Boolean):void;
		
		function addPoint(seriesId:String, ...pointXML):void;
		function removePoint(seriesId:String, pointId:String):void;
		function addPointAt(seriesId:String, pointIndex:uint, pointData:String):void;
		function updatePoint(seriesId:String, pointId:String, pointXML:String):void;
		
		function refresh():void;
		function clear():void;
		
		//------------ states manipulation
		
		function highlightSeries(seriesId:String, highlighted:Boolean):void;
		function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void;
		function highlightCategory(categoryName:String, highlighted:Boolean):void;
		function selectPoint(seriesId:String, pointId:String, selected:Boolean):void;
	}
}