package com.anychart.elements.label {
	import com.anychart.elements.AnimatableElement;
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.TextFormater;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class ItemLabelElement extends AnimatableElement {
		public var format:String = null;
		public var isDynamicFormat:Boolean = false;
		public var formatRule:TextFormater;
		
		override protected function getStyleNodeName():String { return 'label_style'; }
		override public function getStyleStateClass():Class { return LabelElementStyleState; }
		override protected function getContainer(container:IMainElementsContainer):Sprite { return container.getLabelsContainer(this); }
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new ItemLabelElement();
			var label:ItemLabelElement = ItemLabelElement(element);
			label.format = this.format;
			label.isDynamicFormat = this.isDynamicFormat;
			label.formatRule = this.formatRule;
			return super.createCopy(label);
		}
		
		override public function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
			if (data.format[0] != null) {
				this.format = SerializerBase.getCDATAString(data.format[0]);
				this.isDynamicFormat = FormatsParser.isDynamic(this.format);
				if (this.isDynamicFormat)
					this.formatRule = FormatsParser.parse(this.format);
			}
		}
		
		override public function deserializeElementForContainer(data:XML, container:IElementContainer, stylesList:XML, elementIndex:uint, resources:ResourcesLoader):void {
			if (container.elementsInfo == null)
				container.elementsInfo = {};
			if (container.elementsInfo[elementIndex] == null)
				container.elementsInfo[elementIndex] = {};
			super.deserializeElementForContainer(data, container, stylesList, elementIndex, resources);
			if (data.format[0] != null) {
				if (data.@style != undefined) {
					this.format = SerializerBase.getCDATAString(data.format[0]);
					this.isDynamicFormat = FormatsParser.isDynamic(this.format);
					if (this.isDynamicFormat)
						this.formatRule = FormatsParser.parse(this.format);
				}else {
					var ptInfo:Object = container.elementsInfo[elementIndex];
					ptInfo.format = SerializerBase.getCDATAString(data.format[0]);
					ptInfo.isDynamicFormat = FormatsParser.isDynamic(ptInfo.format);
					if (ptInfo.isDynamicFormat)
						ptInfo.formatRule = FormatsParser.parse(ptInfo.format);
				}
			}
		}
		
		override public function initialize(container:IElementContainer, elementIndex:uint):Sprite {
			var sp:Sprite = super.initialize(container, elementIndex);
			if (sp != null) {
				if (container.elementsInfo == null)
					container.elementsInfo = {};
				if (container.elementsInfo[elementIndex] == null)
					container.elementsInfo[elementIndex] = {};
			}
			return sp;
		}
		
		override protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			super.execDrawing(container, state, sprite, elementIndex);
			var labelState:LabelElementStyleState = LabelElementStyleState(state);
			
			var info:TextElementInformation = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			
			var bounds:Rectangle = info.rotatedBounds;
			
			var g:Graphics = sprite.graphics;
			g.lineStyle();
			
			if (!container.hasPersonalContainer) {
				labelState.label.draw(g, labelState.bounds.x, labelState.bounds.y, info, container.color, container.hatchType);
			}else {
				labelState.label.draw(g, 0,0, info, container.color, container.hatchType);
			}
		}
		
		override public function getBounds(container:IElementContainer, state:StyleState, elementIndex:uint):Rectangle {
			var labelState:LabelElementStyleState = LabelElementStyleState(state);
			
			var info:TextElementInformation;
			
			if (container.elementsInfo == null) return new Rectangle();
			if (container.elementsInfo[elementIndex] == null) return  new Rectangle();
			
			if (container.elementsInfo[elementIndex]['info'+state.stateIndex] == null) {
				info = labelState.label.createInformation();
				container.elementsInfo[elementIndex]['info'+state.stateIndex] = info;
				
				var format:String = this.format;
				var isDynamicFormat:Boolean = this.isDynamicFormat;
				var formatRule:TextFormater = this.formatRule;
				if (container.elementsInfo[elementIndex].format != null) {
					format = container.elementsInfo[elementIndex].format;
					isDynamicFormat = container.elementsInfo[elementIndex].isDynamicFormat;
					formatRule = container.elementsInfo[elementIndex].formatRule;
				}
				if (format == null) {
					format = labelState.format;
					isDynamicFormat = labelState.isDynamicFormat;
					formatRule = labelState.formatRule;
				}
				
				info.formattedText = (isDynamicFormat) ? formatRule.getValue(container.getTokenValue, container.isDateTimeToken) : 
														 format;
				labelState.label.getBounds(info);
				
			}else {
				info = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			}
			return info.rotatedBounds;
		}
	}
}