package com.anychart.uiControls.scrollBar
{
	import flash.display.InteractiveObject;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public class ScrollBarHorizontal extends ScrollBarControl
	{
		public function ScrollBarHorizontal(container:InteractiveObject) {
			buttonSub = new ScrollBarButtonLeft();
			buttonAdd = new ScrollBarButtonRight();
			thumb = new ScrollBarThumbH();
			buttonSizeRect = new Rectangle();
			dragRect = new Rectangle();
			super(container);
		}
		
		override protected function setScrollBarSize(bounds:Rectangle):void {
			bounds.height = this.scrollBarSize;
		}

		override protected function resizeButtons():void {
			var s:Rectangle = size;
			var sB:Rectangle = buttonSizeRect;
			sB.x = 0;
			sB.y = 0;
			sB.width = currentButtonSize;
			sB.height = s.height;
			buttonSub.resize(sB);
			sB.x = s.width - sB.width;
			buttonAdd.resize(sB);

			var w:Number = s.width - 2 * currentButtonSize - thumbBonus;
			var d:Number = maxValue - minValue;
			
			sB.x = currentButtonSize + (currentValue - minValue) * w / d;
			sB.width = thumbSize;
			thumb.resize(sB);
		}		
		
		override protected function getThumbDragRect():Rectangle {
			dragRect.x = buttonSize;
			dragRect.y = 0;
			dragRect.width = size.width - 2 * buttonSize - thumbSize;
			dragRect.height = 0;
			return dragRect;
		}
		
		override protected function getLength():Number {
			return size != null ? size.width : 0;
		}
		
		override protected function getMouseCoord(e:MouseEvent):Number {
			return e.localX;
		}
		
		override protected function setThumbPos(value:Number):void {
			thumb.x = value;
		}

		override protected function getThumbPos():Number {
			return thumb.x;
		}
		
	}
}