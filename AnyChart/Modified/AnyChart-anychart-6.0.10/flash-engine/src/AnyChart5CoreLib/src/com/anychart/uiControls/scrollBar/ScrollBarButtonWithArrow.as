package com.anychart.uiControls.scrollBar {
	import com.anychart.visual.background.BackgroundBase;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal class ScrollBarButtonWithArrow extends ScrollBarButton {
		
		public var arrowNormalBackground:BackgroundBase;
		public var arrowHoverBackground:BackgroundBase;
		public var arrowPushedBackground:BackgroundBase;
		
		override public function draw():void {
			if (arrowNormalBackground == null ||
				arrowHoverBackground == null ||
				arrowPushedBackground == null)
				return;
				
			super.draw();
			var g:Graphics = this.graphics;
			var s:Rectangle = this.getArrowBounds();
			
			var bg:BackgroundBase;
			switch (this.state) {
				case NORMAL: bg = this.arrowNormalBackground; break;
				case HOVER: bg = this.arrowHoverBackground; break;
				case PUSHED: bg = this.arrowPushedBackground; break;
			}
			
			
			if (bg.fill != null)
				bg.fill.begin(g, s);
			if (bg.border != null)
				bg.border.apply(g, s);
				
			this.drawArrow();
			g.endFill();
			g.lineStyle();
			
			if (bg.hatchFill != null) {
				bg.hatchFill.beginFill(g);
				this.drawArrow();
				g.endFill();
			}
		}
		
		override public function destroy():void {
			super.destroy();
			this.arrowNormalBackground = null;
			this.arrowHoverBackground = null;
			this.arrowPushedBackground = null;
		}
		
		protected function drawArrow():void {}
		protected function getArrowBounds():Rectangle { return null; }
	}
}