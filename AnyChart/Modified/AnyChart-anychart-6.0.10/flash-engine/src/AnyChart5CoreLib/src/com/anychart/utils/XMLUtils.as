package com.anychart.utils {
	import mx.utils.XMLUtil;
	
	
	public final class XMLUtils {
		
		public static function applyReplaces(data:XML, replaces:Array):XML {
			if (data == null) return null;
			if (replaces == null || replaces.length == 0) return data;
			var strData:String = data.toXMLString();
			for (var i:int = 0;i<replaces.length;i++) {
				strData = strData.split(replaces[i][0]).join(replaces[i][1]);
			}
			return new XML(strData);
		}
		
		public static function mergeAttributes(template:XML, actual:XML, res:XML):void {
			var templateAttributes:XMLList = template.attributes();
			var templateAttributesCount:uint = templateAttributes.length();
			
			var actualAttributes:XMLList = actual.attributes();
			var actualAttributesCount:uint = actualAttributes.length();
			
			var i:uint;
			for (i = 0;i<templateAttributesCount;i++) {
				res[templateAttributes[i].name()] = templateAttributes[i];
			}
			for (i = 0;i<actualAttributesCount;i++) {
				res[actualAttributes[i].name()] = actualAttributes[i];
			}
		}
		
		public static function mergeNamedChildren(template:XML, actual:XML):XML {
			if (template == null && actual == null) return null;
			if (template == null) return actual;
			if (actual == null) return template;
			var res:XML = template.copy();
			
			var children:XMLList = actual.children();
			var childrenCount:uint = children.length();
			for (var i:uint = 0;i<childrenCount;i++) {
				var child:XML = children[i];
				var childName:String = child.name().toString();
				
				if (res[childName].length() == 0) {
					res[childName][0] = child;
				}else {
					if (res[childName].(@name == child.@name).length() == 1)
						delete res[childName].(@name == child.@name)[0];
					res[childName] += child;
				}
			}
			
			return res;
		}
		
		public static function mergeNamedChildrenByName(template:XML, actual:XML, name:String, ignoreOther:Boolean = false, excludeNode:String = null):XML {
			if (template == null && actual == null) return null;
			if (template == null) return actual;
			if (actual == null) return template;
			var res:XML = template.copy();
			
			var children:XMLList = actual.children();
			var childrenCount:uint = children.length();
			for (var i:uint = 0;i<childrenCount;i++) {
				var child:XML = children[i];
				var childName:String = child.name().toString();
				
				if (res[childName].length() == 0) {
					res[childName][0] = child;
				}else {
					if (childName == name) {
						if (res[childName].@name != undefined && child.@name != undefined && res[childName].(@name == child.@name).length() == 1)
							delete res[childName].(@name == child.@name)[0];
						res[childName] += child;
					}else if (!ignoreOther) {
						res[childName][0] = merge(child, res[childName][0], childName, excludeNode);
					}
				}
			}
			
			return res;
		}
		
		public static function merge(template:XML, actual:XML, nodeName:String = null, excludeNode:String = null):XML {
			if (template == null && actual == null) return null;
			if (template == null) return actual;
			if (actual == null) return template;
			
			nodeName = (nodeName == null) ? template.name().toString() : nodeName;
			var res:XML = <{nodeName} />;
			
			mergeAttributes(template, actual, res);
			
			if (nodeName == 'gradient' && actual.key.length() != 0) {
				res.key = actual.key;
				return res;
			}else if (nodeName == 'format' || nodeName == 'text' || nodeName == 'save_as_image_item_text' || nodeName == 'save_as_pdf_item_text' || nodeName == 'print_chart_item_text') {
				return actual;
			}else if (nodeName == 'action' && actual.replace.length() != 0) {
				res.replace = actual.replace;
				return res;
			}else {
				var i:uint;
				
				var templateChild:XMLList = template.children();
				var templateChildCnt:uint = templateChild.length();
				
				var actualChild:XMLList = actual.children();
				var actualChildCnt:uint = actualChild.length();
				
				var childName:String;
				for (i = 0;i<templateChildCnt;i++) {
					if (templateChild[i].nodeKind() == "text") {
						res.appendChild(templateChild[i]);
					}else {
						childName = templateChild[i].name().toString();
						if (excludeNode == null || childName != excludeNode) {
							res[childName][0] = XMLUtils.merge(templateChild[i], actual[childName][0]);
						}
					}
				}
				
				for (i = 0;i<actualChildCnt;i++) {
					if (actualChild[i].nodeKind() == "text")  {
						res.appendChild(actualChild[i]);
					}else {
						childName = actualChild[i].name().toString();
						if (childName == excludeNode) {
							res[childName] += actualChild[i];
						}else if (res[childName][0] == null)
							res[childName][0] = actualChild[i];
					}
				}
			}
			return res;
		}
		
		public static function insert(list:XMLList, index:uint, data:XML):XMLList {
			if (list.length() < index) {
				list += data;
				return list;
			}
			var newList:XMLList = new XMLList();
			var i:uint;
			for (i = 0;i<index;i++)
				newList[i] = list[i];
			newList[index] = data;
			for (i = (index+1);i<=list.length();i++)
				newList[i] = list[i-1];
			
			return newList;
		}
		
		public static function removeById(list:XMLList, id:String):void {
			var listLen:uint = list.length();
			var deleteTargets:Array = [];
			var i:uint;
			for (i = 0;i<listLen;i++) {
				if (list[i].@id != undefined && list[i].@id == id)
					deleteTargets.push(i);
			}
			
			for (i = 0;i<deleteTargets.length;i++) {
				delete list[deleteTargets[i]];
			}
		}
		
		public static function findById(list:XMLList, id:String):int {
			var listLen:uint = list.length();
			for (var i:uint = 0;i<listLen;i++) {
				if (list[i].@id != undefined && list[i].@id == id)
					return i;
			}
			return -1;
		}
		
		public static function findXMLById(list:XMLList, id:String):XML {
			var listLen:uint = list.length();
			for (var i:uint = 0;i<listLen;i++) {
				if (list[i].@id != undefined && list[i].@id == id)
					return list[i];
			}
			return null;
		}
		
		public static function mergeDataNode(currentNode:XML, newNode:XML):XML {
			var res:XML = currentNode.copy();
			if (newNode.@id != undefined)
				delete newNode.@id;
			mergeAttributes(currentNode, newNode, res);
			for each (var child:XML in newNode.children()) {
				var childName:String = child.name();
				if (childName == null) continue;
				if (childName == "actions" || childName == "extra_labels" || childName == "extra_markers" || childName == "extra_tooltips") {
					res[childName][0] = child;
				}else {
					res[childName][0] = merge(currentNode[childName][0], child, childName);
				}
			}
			return res;
		}
	}
}