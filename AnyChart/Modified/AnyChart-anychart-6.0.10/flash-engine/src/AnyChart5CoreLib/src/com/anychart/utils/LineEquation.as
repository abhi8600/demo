package com.anychart.utils
{
	import flash.geom.Point;
	
	public final class LineEquation
	{
		public var k:Number = 0;
		public var b:Number = 0;
		public var isVertical:Boolean = false;
		
		public function LineEquation(k:Number = 0, b:Number = 0, isVertical:Boolean = false) {
			this.b = b;
			this.k = k;
			this.isVertical = isVertical;
		}
		
		//public function toString():String { return "[LineEquation] k="+k+"; b="+b+"; isVertical="+isVertical+";"; }
		
		public static function getLineBy4Coords(x1:Number, y1:Number, x2:Number, y2:Number):LineEquation {
			return x1 == x2 ? new LineEquation(0, x1, true) : 
				new LineEquation((y1 - y2) / (x1 - x2), y1 - (y1 - y2) / (x1 - x2) * x1, false);
		}

		public static function getLineFrom2Points(a:Point, b:Point):LineEquation {
			//return getLineBy4Coords(a.x, a.y, b.x, b.y);
			return a.x == b.x ? new LineEquation(0, a.x, true) : 
				new LineEquation((a.y - b.y) / (a.x - b.x), a.y - (a.y - b.y) / (a.x - b.x) * a.x, false);
		}

		public static function getLineFrom3Points(a:Point, b:Point, o:Point):LineEquation {
			return a.x == b.x ? new LineEquation(0, a.x, true) :
				new LineEquation((a.y - b.y) / (a.x - b.x), o.y - (a.y - b.y) / (a.x - b.x) * o.x, true);
		}
		
		public static function getLineFromPointAndAngle(point:Point, angle:Number):LineEquation {
			return getLineFrom2Points(point, new Point(
				point.x + point.x * Math.cos(angle), 
				point.y + point.y * Math.sin(angle)));
		}

		public function isVerticalLine():Boolean {
			return isVertical;
		}

		public static function isEqualsLines(l1:LineEquation, l2:LineEquation):Boolean {
			return (l1.k == l2.k || l1.k == -l2.k)&& l1.b == l2.b;
		}

		public static function isParallelLines(l1:LineEquation, l2:LineEquation):Boolean {
			return l1.k == l2.k || l1.k == -l2.k;
		}
		
		public function containsPoint(p:Point, tolerance:Number = 0):Boolean {
			if (isVertical) if (Math.abs(p.x - this.b) < tolerance) return true; else return false;
			if (Math.abs(p.y - (this.k * p.x + this.b)) < tolerance) return true;
			return false;
		}

		public static function getCrossPoint(l1:LineEquation, l2:LineEquation):Point {
			if ((l1.k == l2.k || l1.k == -l2.k)&& l1.b != l2.b) return null;
			if (l1.isVerticalLine()) return new Point(l1.b, l2.k * l1.b + l2.b);
			if (l2.isVerticalLine()) return new Point(l2.b, l1.k * l2.b + l1.b);
			var x:Number = (l2.b - l1.b) / (l1.k - l2.k);
			return new Point(x, l1.k * x + l1. b);
		}

		public function isLeftPointCoords(x:Number, y:Number):Boolean {
			if (isVertical) return !(b < x);
			if (k == 0) return b < y
			return k * x + b < y;
		}

		public function isLeftPoint(point:Point):Boolean {
			return isLeftPointCoords(point.x, point.y);
		}
		
		public function y(x:Number):Number {
			return !isVertical ? k * x + b : 0 ;
		}

		public function x(y:Number):Number {
			return !isVertical ? k != 0 ? (y - b) / k : b : b ;
		}
		
		public function getDistance(p:Point):Number {
			if (isVertical) return p.x - b;
			if (k == 0) return p.y - b * (p.y < b ? -1 : 1);
			var c1:Number = Math.abs(p.x - x(p.y));
			var c2:Number = Math.abs(p.y - y(p.x));
			var c3:Number = Math.sqrt(c1 * c1 + c2 * c2);
			return c3 == 0 ? 0 : c1 * c2 / c3 * (isLeftPoint(p) ? 1 : -1 );
		}
		
		public function clone():LineEquation {
			return new LineEquation(k, b, isVertical);
		}
		
		public function getParallelLineByCoords(xCoord:Number, yCoord:Number):LineEquation {
			return isVertical ? new LineEquation(k, xCoord, true) :
				new LineEquation(k, yCoord - y(xCoord) + b, false);
		}
		
		public function getParallelLine(p:Point):LineEquation {
			return getParallelLineByCoords(p.x, p.y);
		}

		public function getPointByX(x:Number):Point {
			return !isVertical ? new Point(x, k * x + b) : new Point(x, 0);
		}

		public function getPointByY(y:Number):Point {
			return !isVertical ? new Point(k != 0 ? (y - b) / k : b, y) : new Point(0, y);
		}
		
		public function getPerpendicularLine(p:Point):LineEquation {
			return getPerpendicularLineByCoords(p.x, p.y);
		}
		
		public function getPerpendicularLineByCoords(xCoord:Number, yCoord:Number):LineEquation {
			return isVertical 
				? new LineEquation(0, yCoord, false) 
				: k == 0 
					? new LineEquation(0, xCoord, true) 
					: new LineEquation(-1 / k, yCoord + xCoord / k , false);
		}
	}
}