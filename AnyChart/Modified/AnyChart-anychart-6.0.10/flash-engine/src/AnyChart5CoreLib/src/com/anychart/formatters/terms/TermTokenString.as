package com.anychart.formatters.terms
{
	public class TermTokenString implements ITerm
	{
		private var tokenName:String;
		private var parentCall:ITerm; 
		
		public function TermTokenString(tokenName:String, parentCall:ITerm) {
			this.tokenName = tokenName;
			this.parentCall = (parentCall != null ? parentCall : new TermEmpty());
		}
		
		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return parentCall.getValue(getTokenFunction, isDateTimeToken) + getTokenFunction(tokenName);
		}

	}
}