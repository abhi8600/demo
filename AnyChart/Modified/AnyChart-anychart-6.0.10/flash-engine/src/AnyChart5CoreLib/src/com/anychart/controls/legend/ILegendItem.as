package com.anychart.controls.legend {
	import com.anychart.formatters.IFormatable;
	
	public interface ILegendItem {
		function get color():int;
		function get hatchType():uint;
		function get markerType():int;
		function get iconCanDrawMarker():Boolean;
		
		function get tag():ILegendTag;
		function set tag(value:ILegendTag):void;
	}
}