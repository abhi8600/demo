package com.anychart.controls.image {
	import com.anychart.controls.Control;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.visual.fill.Fill;
	
	import flash.display.BitmapData;
	
	public final class ImageControl extends Control {
		
		public static function check(data:XML):Boolean {
			return String(data.name()) == "image";
		}
		 
		private var fill:Fill;
		
		public function ImageControl() {
			super();
			this.fill = new Fill();			
		}
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
			this.fill.deserialize(<fill type="image" image_mode="stretch" image_url={data.@url} />, resources);
		}
		
		override protected function checkContentBounds():void { 
			var bitmap:BitmapData = ResourcesHashMap.getImage(this.fill.imageUrl);
			if (isNaN(this.bounds.width))
				this.bounds.width = bitmap.width;
			if (isNaN(this.bounds.height))
				this.bounds.height = bitmap.height;
			super.checkContentBounds();
		}
		
		override public function draw():void {
			super.draw();
			this.drawImg();
		}
		
		override public function resize():void {
			super.resize();
			this.drawImg();
		}
		
		private function drawImg():void {
			this.fill.begin(this.container.graphics, this.contentBounds);
			this.container.graphics.drawRect(this.contentBounds.x, this.contentBounds.y, this.contentBounds.width, this.contentBounds.height);
		}
	}
}