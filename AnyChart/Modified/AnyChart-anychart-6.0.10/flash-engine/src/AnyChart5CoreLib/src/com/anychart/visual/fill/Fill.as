package com.anychart.visual.fill
{
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class Fill implements IStyleSerializableRes {
		
		// Properties.
		public var enabled:Boolean;
		public var type:uint;
		
		public var color:uint;
		public var isDynamic:Boolean;
		
		public var opacity:Number;
		
		public var gradient:Gradient;
		
		public var imageUrl:String;
		private var imageMode:uint;
		
		public function Fill() {
			this.enabled = true;
			this.type = FillType.SOLID;
			this.color = 0xEEEEEE;
			this.isDynamic = false;
			this.opacity = 1;
		}
		
		// Methods.
		public function begin(target:Graphics,rc:Rectangle=null,styleColor:uint=0):void {
			if(!this.enabled) {
				target.beginFill(0,0);
				return;
			}
			
			if (this.type == FillType.IMAGE)
				this.type = ResourcesHashMap.getImage(this.imageUrl) == null ? FillType.SOLID : FillType.IMAGE;
			
			switch(this.type) {
				case FillType.SOLID:
					target.beginFill(this.isDynamic ? ColorParser.getInstance().getDynamicColor(this.color,styleColor) :
													  this.color,
									 this.opacity); 
				break;
				
				case FillType.GRADIENT:
					gradient.beginGradientFill(target,rc,styleColor);
				break;
				
				case FillType.IMAGE:
					var data:BitmapData = ResourcesHashMap.getImage(this.imageUrl);
					if (this.opacity < 1)
						data.colorTransform(data.rect, new ColorTransform(1,1,1,this.opacity)); 
					
					var m:Matrix;
					switch (this.imageMode) {
						case FillImageMode.TILE:
							m = new Matrix();
							m.createBox(1,1,0,rc.x,rc.y);
							target.beginBitmapFill(data,m,true,false);
							break;
						case FillImageMode.FIT_BY_PROPORTIONS:
							var xScale:Number = rc.width/data.width;
							var yScale:Number = rc.height/data.height;
							
							var scale:Number = Math.min(xScale,yScale);
							m = new Matrix();
							m.createBox(scale,scale,0,rc.x,rc.y);
							target.beginBitmapFill(data,m,false,true);
							break;
						case FillImageMode.STRETCH:
							m = new Matrix();
							m.createBox(rc.width/data.width,rc.height/data.height,0,rc.x,rc.y);
							target.beginBitmapFill(data,m,false,true);
							break;
					}
					data = null;
					break;	
			}
		}
		
		public function end(target:Graphics):void {
			target.endFill();
		}
		
		public var strColor:String;
		
		public function deserialize(data:XML,resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@enabled!=undefined) this.enabled=SerializerBase.getBoolean(data.@enabled);
			if (!this.enabled) return;
			
			if(data.@type!=undefined) {
				switch (SerializerBase.getEnumItem(data.@type)) {
					case "solid":
						this.type = FillType.SOLID;
						break
					case "gradient":
						this.type = FillType.GRADIENT;
						break;
					case "image":
						this.type = data.@image_url != undefined ? FillType.IMAGE : FillType.SOLID;
						break;
				}
			}
			if(data.@color!=undefined) {
				this.strColor = String(data.@color);
				this.color=SerializerBase.getColor(data.@color);
				if (style != null && SerializerBase.isDynamicColor(data.@color)) {
					this.isDynamic = true;
					style.addDynamicColor(this.color);
				}else {
					this.isDynamic = false;
				}
			}
			if(data.@opacity!=undefined) this.opacity=SerializerBase.getRatio(data.@opacity);
			if (this.type == FillType.GRADIENT) {
				if(data.gradient[0]!=null) {
					this.gradient = new Gradient(this.opacity);
					this.gradient.deserialize(data.gradient[0],style);
				}
			}
			
			if (this.type == FillType.GRADIENT && (this.gradient == null || this.gradient.entries == null))
				this.type = FillType.SOLID;
				
			if (this.type == FillType.IMAGE) {
				this.imageUrl = SerializerBase.getString(data.@image_url);
				this.imageUrl = resources.addImage(this.imageUrl);
				this.imageMode = FillImageMode.STRETCH;
				if (data.@image_mode != undefined) {
					switch (SerializerBase.getEnumItem(data.@image_mode)) {
						case "tile": this.imageMode = FillImageMode.TILE; break;
						case "stretch": this.imageMode = FillImageMode.STRETCH; break;
						case "fitbyproportions": this.imageMode = FillImageMode.FIT_BY_PROPORTIONS; break;
					}
				}
			}
		}
	}
}