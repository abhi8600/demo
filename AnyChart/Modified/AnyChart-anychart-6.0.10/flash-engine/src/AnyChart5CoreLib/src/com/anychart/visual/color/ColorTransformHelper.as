package com.anychart.visual.color
{
	public class ColorTransformHelper
    {
    	// Fields
        public var aValue:Number;
        public var bValue:Number;
        public var cValue:Number;
        
        private var numArray:Array=[0.0,0.0,0.0];
        private var numArray2:Array=[0.0,0.0,0.0];

        // Methods
        public function a():Number
        {
            return this.bValue;
        }

        public function a_ex(A_0:Number):void
        {
            this.aValue = A_0;
            this.aValue = (this.aValue > 1) ? 1 : ((this.aValue < 0) ? 0 : this.aValue);
        }

        public function b():Number
        {
            return this.cValue;
        }

        public function b_ex(A_0:Number):void
        {
            this.cValue = A_0;
            this.cValue = (this.cValue > 1) ? 1 : ((this.cValue < 0) ? 0 : this.cValue);
        }

        public function c():Number
        {
            return this.aValue;
        }

        public function c_ex(A_0:Number):void
        {
            this.bValue = A_0;
            this.bValue = (this.bValue > 1) ? 1 : ((this.bValue < 0) ? 0 : this.bValue);
        }
        
        
        /// Methods.
        public function aColor():uint
        {
        	var num:Number = 0;
            var num2:Number = 0;
            var num3:Number = 0;
            if (this.cValue == 0)
            {
                num = num2 = num3 = 0;
            }
            else if (this.bValue == 0)
            {
                num = num2 = num3 = this.cValue;
            }
            else
            {
                var num5:Number = (this.cValue <= 0.5) ? (this.cValue * (1 + this.bValue)) : ((this.cValue + this.bValue) - (this.cValue * this.bValue));
                var num4:Number = (2 * this.cValue) - num5;
                
                numArray[0]=this.aValue + 0.33333333333333331;
                numArray[1]=this.aValue;
                numArray[2]=this.aValue - 0.33333333333333331
				                
                for (var i:int = 0; i < 3; i++)
                {
                    if (Number(numArray[i]) < 0)
                    {
                    	numArray[i]=Number(numArray[i])+1;
                    }
                    if (Number(numArray[i]) > 1)
                    {
                    	numArray[i]=Number(numArray[i])-1;
                    }
                    if ((6 * Number(numArray[i])) < 1)
                    {
                        numArray2[i] = num4 + (((num5 - num4) * numArray[i]) * 6);
                    }
                    else if ((2 * Number(numArray[i])) < 1)
                    {
                        numArray2[i] = num5;
                    }
                    else if ((3 * Number(numArray[i])) < 2)
                    {
                        numArray2[i] = num4 + (((num5 - num4) * (0.66666666666666663 - numArray[i])) * 6);
                    }
                    else
                    {
                        numArray2[i] = num4;
                    }
                }
                
                num = Number(numArray2[0]);
                num2 = Number(numArray2[1]);
                num3 = Number(numArray2[2]);
            }
            
            return ColorUtils.fromRGB(uint(255.0 * num),uint(255.0 * num2),uint(255.0 * num3));
        }
       
    }
}