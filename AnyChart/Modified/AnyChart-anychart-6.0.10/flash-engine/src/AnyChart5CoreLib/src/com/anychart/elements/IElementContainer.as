package com.anychart.elements {
	import com.anychart.IAnyChart;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.formatters.IFormatable;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	public interface IElementContainer extends IFormatable {
		function get elementsInfo():Object;
		function set elementsInfo(value:Object):void;
		function get hasPersonalContainer():Boolean;
		
		function get color():int;
		function get hatchType():int;
		function get markerType():int;
		
		function getElementPosition(state:BaseElementStyleState, sprite:Sprite, bounds:Rectangle):Point;
		function getMainContainer():IMainElementsContainer;
	}
}