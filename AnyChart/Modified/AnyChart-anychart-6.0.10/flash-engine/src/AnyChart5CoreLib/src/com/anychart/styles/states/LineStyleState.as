package com.anychart.styles.states {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IRotationManager;
	import com.anychart.styles.Style;
	import com.anychart.visual.stroke.Stroke;
	
	public class LineStyleState extends StyleStateWithEffects {
		public var stroke:Stroke;
		
		public function LineStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			if (data == null) return data;
			data = super.deserialize(data, resources);
			if (data.line[0] != null) {
				if (SerializerBase.isEnabled(data.line[0])) {
					if (this.stroke == null) this.stroke = new Stroke();
					this.stroke.deserialize(data.line[0],this.style);
				}else {
					this.stroke = null;
				}
			}
			return data;
		}
		
		override public function initializeRotations(manager:IRotationManager, baseScaleInvert:Boolean):void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.setGradientAngles(this.stroke.gradient, baseScaleInvert, manager);
		}
		
		override public function setNormalRotation():void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.stroke.gradient.angle = this.stroke.gradient.normalAngle;
		}
		
		override public function setInvertedRotation():void {
			if (this.stroke != null && this.stroke.gradient != null)
				this.stroke.gradient.angle = this.stroke.gradient.invertedAngle;
		}
	}
}