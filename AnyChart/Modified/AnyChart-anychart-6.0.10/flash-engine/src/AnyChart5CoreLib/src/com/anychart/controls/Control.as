package com.anychart.controls {
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.controls.layout.ControlLayout;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.background.RectBackground;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public /* abstract */ class Control implements IEventDispatcher {
		
		public var bounds:Rectangle;
		public var contentBounds:Rectangle;
		
		protected var background:RectBackground;
		public var layout:ControlLayout;
		
		public var container:Sprite;
		
		public function Control() {
			this.layout = new ControlLayout();
			this.bounds = new Rectangle();
			this.contentBounds = new Rectangle();
			this.container = new Sprite();
			this.container.addEventListener(MouseEvent.MOUSE_DOWN, this.redirectEvent);
			this.eventDispatcher = new EventDispatcher(this);
		}
		
		public function recalculateAndRedraw():void {
			this.draw();
		}
		
		public function destroy():void {
			if (this.container)
				this.container.removeEventListener(MouseEvent.MOUSE_DOWN, this.redirectEvent);
			this.container = null;
			this.eventDispatcher = null;
			this.layout = null;
			this.plot = null;
			this.chart = null;
			this.plotContainer = null;
		}
		
		public function get zIndex():int { return this.layout.zIndex; }
		
		protected var chart:IAnyChart;
		protected var plot:IPlot;
		public var plotContainer:IChart;
		public function initializeDataContainer(chart:IAnyChart, plot:IPlot, plotContainer:IChart):void {
			this.chart = chart;
			this.plot = plot;
			this.plotContainer = plotContainer;
		}
		
		public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			this.layout.deserialize(data);
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new RectBackground();
				this.background.deserialize(data.background[0], resources);
			}
		}
		
		//-------------------------------------------------------------------------------
		//	Bounds 
		//-------------------------------------------------------------------------------
		
		public function setBounds(chartRect:Rectangle, plotRect:Rectangle):void {
			this.layout.setBounds(this.bounds, chartRect, plotRect);
			this.checkContentBounds();
			this.setContentBounds();
		}
		
		protected function getContentWidth():Number {
			var w:Number = this.bounds.width;
			if (this.background != null && this.background.margins != null)
				w -= this.background.margins.left + this.background.margins.right;
			return w;
		}
		
		protected function getMaxContentWidth():Number {
			var w:Number = this.layout.maxWidth;
			if (this.background != null && this.background.margins != null)
				w -= this.background.margins.left + this.background.margins.right;
			return w;
		}
		
		protected function getContentHeight():Number {
			var h:Number = this.bounds.height;
			if (this.background != null && this.background.margins != null)
				h -= this.background.margins.top + this.background.margins.bottom;
			return h;
		}
		
		protected function getMaxContentHeight():Number {
			var h:Number = this.layout.maxHeight;
			if (this.background != null && this.background.margins != null)
				h -= this.background.margins.top + this.background.margins.bottom;
			return h;
		}
		
		protected function checkContentBounds():void {
			this.bounds.width += this.layout.margin.left + this.layout.margin.right;
			this.bounds.height += this.layout.margin.top + this.layout.margin.bottom;
			if (this.background != null && this.background.margins != null) {
				if (!this.layout.isWidthSetted)
					this.bounds.width += this.background.margins.left + this.background.margins.right;
				if (!this.layout.isHeightSetted)
					this.bounds.height += this.background.margins.top + this.background.margins.bottom;
			}
		}
		
		protected function setContentBounds():void {
			this.contentBounds.x = 0;
			this.contentBounds.y = 0;
			this.contentBounds.width = this.bounds.width - this.layout.margin.left - this.layout.margin.right;
			this.contentBounds.height = this.bounds.height - this.layout.margin.top - this.layout.margin.bottom;
			if (this.background != null && this.background.margins != null)
				this.background.margins.applyInside(this.contentBounds);
		}
		
		//-------------------------------------------------------------------------------
		//	Drawing 
		//-------------------------------------------------------------------------------
		
		public function initialize():DisplayObject {
			return this.container;
		}
		
		public function draw():void {
			this.drawBackground();
		}
		
		public function resize():void {
			this.drawBackground();
		}
		
		private function drawBackground():void {
			this.container.graphics.clear();
			this.container.x = this.bounds.x + this.layout.margin.left;
			this.container.y = this.bounds.y + this.layout.margin.top;
			if (this.background != null) {
				var rc:Rectangle = new Rectangle(0,0,this.bounds.width,this.bounds.height);
				rc.width -= this.layout.margin.left + this.layout.margin.right;
				rc.height -= this.layout.margin.top + this.layout.margin.bottom;
				this.execBackgroundDrawing(rc);
				this.container.filters = this.background.effects == null ? null : this.background.effects.list;
			}else {
				this.container.filters = null;
			}
		}
		
		protected function execBackgroundDrawing(bounds:Rectangle):void {
			this.background.draw(this.container.graphics, bounds);
		}
		
		//-------------------------------------------------------------------------------
		//	Events 
		//-------------------------------------------------------------------------------
		
		private var eventDispatcher:EventDispatcher = new EventDispatcher();
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			this.eventDispatcher.addEventListener(type, listener, useCapture);
		}
		
		public function dispatchEvent(event:Event):Boolean {
			return this.eventDispatcher.dispatchEvent(event);
		}
		
		public function hasEventListener(type:String):Boolean {
			return this.eventDispatcher.hasEventListener(type);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
			return this.eventDispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function willTrigger(type:String):Boolean {
			return this.eventDispatcher.willTrigger(type);
		}
		
		private function redirectEvent(e:MouseEvent):void {
			e.stopImmediatePropagation();
			e.stopPropagation();
			this.dispatchEvent(e.clone());
		}
	}
}