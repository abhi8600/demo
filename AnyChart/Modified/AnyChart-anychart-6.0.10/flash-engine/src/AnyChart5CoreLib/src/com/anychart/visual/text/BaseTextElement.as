package com.anychart.visual.text {
	
	import com.anychart.formatters.TextFormater;
	
	public class BaseTextElement extends BaseNonFormatableTextElement {
		public var isDynamicText:Boolean = false;
		public var dynamicText:TextFormater;
	}
}