package com.anychart.animation.interpolation {
	public class Interpolator {
		public function interpolate(time:Number, 
									initialValue:Number, 
									totalChange:Number, 
									duration:Number,
									extraObj1:Number = 0, extraObj2:Number = 1):Number { return NaN; }
	}
}