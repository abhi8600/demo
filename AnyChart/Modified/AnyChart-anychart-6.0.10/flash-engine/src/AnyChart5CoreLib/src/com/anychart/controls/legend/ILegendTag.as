package com.anychart.controls.legend {
	import com.anychart.formatters.IFormatable;
	
	public interface ILegendTag extends IFormatable {
		function setHover():void;
		function setNormal():void;
	}
}