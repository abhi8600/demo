package com.anychart.controls {
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.controls.layout.ControlPosition;
	import com.anychart.controls.layouters.anchored.FixedControlsCollection;
	import com.anychart.controls.layouters.anchored.FloatControlsCollection;
	import com.anychart.controls.layouters.groupable.InsidePlotControlsCollection;
	import com.anychart.controls.layouters.groupable.OutsidePlotControlsCollection;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public final class PlotControlsList {
		public var container:DisplayObjectContainer;
		
		private var insideControls:InsidePlotControlsCollection;
		private var outsideControls:OutsidePlotControlsCollection;
		private var floatControls:FloatControlsCollection;
		private var fixedControls:FixedControlsCollection;
		
		private var controls:Array;
		private var chart:IAnyChart;
		private var plot:IPlot;
		private var plotContainer:IChart;
		
		public function PlotControlsList(chart:IAnyChart, plot:IPlot, plotContainer:IChart) {
			this.controls = [];
			this.container = new Sprite();
			this.chart = chart;
			this.plot = plot;
			this.plotContainer = plotContainer;
			
			this.insideControls = new InsidePlotControlsCollection();
			this.outsideControls = new OutsidePlotControlsCollection();
			this.floatControls = new FloatControlsCollection();
			this.fixedControls = new FixedControlsCollection();
			
			this.lastZIndex = 0;
			this.needReorderControls = false;
		}
		
		public function destroy():void {
			this.chart = null;
			this.plot = null;
			this.plotContainer = null;
			
			for each (var control:Control in this.controls) {
				control.destroy();
			}
			this.controls = null;
			this.insideControls = null;
			this.outsideControls = null;
			this.floatControls = null;
			this.fixedControls = null;
		}
		
		public function deserialize(data:XML):void {
			//deserialize default align_by values from <controls /> node
			if (data.@align_left_by != undefined) this.outsideControls.defaultAlignLeftByDataPlot = !(SerializerBase.getEnumItem(data.@align_left_by) == "chart"); 
			if (data.@align_right_by != undefined) this.outsideControls.defaultAlignRightByDataPlot = !(SerializerBase.getEnumItem(data.@align_right_by) == "chart");
			if (data.@align_top_by != undefined) this.outsideControls.defaultAlignTopByDataPlot = !(SerializerBase.getEnumItem(data.@align_top_by) == "chart");
			if (data.@align_bottom_by != undefined) this.outsideControls.defaultAlignBottomByDataPlot = !(SerializerBase.getEnumItem(data.@align_bottom_by) == "chart");
		}
		
		public function deserializeControl(data:XML, styles:XML, resources:ResourcesLoader):void {
			var control:Control = ControlsFactory.create(data);
			if (control == null) return;
			control.initializeDataContainer(this.chart, this.plot, this.plotContainer);
			control.deserialize(data, styles, resources);
			
			//register control
			if (control.layout.position == ControlPosition.FLOAT)
				this.floatControls.addControl(control);
			else if (control.layout.position == ControlPosition.FIXED)
				this.fixedControls.addControl(control);
			else if (control.layout.insideDataPlot)
				this.insideControls.addControl(control);
			else
				this.outsideControls.addControl(control);
			
			if (control.layout.zIndex == -1) {
				control.layout.zIndex = this.lastZIndex;
				this.lastZIndex++;
			}else {
				this.needReorderControls = true;
			}
			
			this.controls.push(control);
		}
		
		private var lastZIndex:int;
		private var needReorderControls:Boolean;
		
		public function initialize():void {
			if (this.needReorderControls) {
				this.controls.sortOn("zIndex", Array.NUMERIC);
			}
			
			if (this.controls) {
				for (var i:int = 0;i<this.controls.length;i++)
					this.container.addChild(Control(this.controls[i]).initialize());
			}
		}
		
		public function recalculateAndRedraw():void {
			for each (var control:Control in this.controls) {
				control.recalculateAndRedraw();
			}
		}
		
		public function draw():void {
			for each (var control:Control in this.controls) {
				control.draw();
			}
		}
		
		public function resize():void {
			for each (var control:Control in this.controls) {
				control.resize();
			}
		}
		
		public function setLayout(plotRect:Rectangle, chartRect:Rectangle):void {
			this.outsideControls.setLayout(plotRect, chartRect);
		}
		
		public function cropPlot(plotRect:Rectangle):void {
			this.outsideControls.cropPlot(plotRect);
		}
		
		public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle, totalChartRect:Rectangle):Boolean {
			if (this.outsideControls.finalizeLayout(plotRect, chartRect)) {
				this.insideControls.finalizeLayout(plotRect, chartRect);
				this.floatControls.finalizeLayout(plotRect, totalChartRect);
				this.fixedControls.finalizeLayout(plotRect, totalChartRect);
				return true;
			}
			return false;
		}
	}
}