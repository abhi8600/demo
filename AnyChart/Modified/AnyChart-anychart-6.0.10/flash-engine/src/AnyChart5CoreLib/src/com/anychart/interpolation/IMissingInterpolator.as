package com.anychart.interpolation {
	public interface IMissingInterpolator {
		function initialize(fieldName:String):void;
		function clear():void;
		
		function checkDuringDeserialize(point:Object, index:int):void;
		function interpolate(points:Array):Array;
	}
}