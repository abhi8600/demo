package com.anychart.dateTime {
	import com.anychart.dispose.DisposableStaticList;
	import com.anychart.locale.DateTimeLocale;
	
	
	public final class DateTimeFormatter {
		private static var formatsCache:Object = {};
		private static var locale:DateTimeLocale;
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(DateTimeFormatter);
			return true;
		}
		
		public static function clear():void {
			formatsCache = {};
			locale = null;
		}
		
		public static function initialize(locale:DateTimeLocale):void {
			DateTimeFormatter.locale = locale;
		}
		
		public static function format(timeStamp:Number, mask:String):String {
			var date:Date = new Date(timeStamp);
			if (mask == null) mask = DateTimeParser.DEFAULT_FORMAT;
			var format:Format = formatsCache[mask];
			if (format == null){
				format = parseMask(mask);
				formatsCache[mask] = format;
			}
			
			var res:String = format.consts[0];
			var len:Number = format.aspects.length;
			for (var i:Number = 0; i < len; i++)
				res += format.aspects[i](date) + format.consts[i+1];
			return res;
		}
		
		private static function parseMask(mask:String):Format {
			var lexems:Array = mask.split('%');
			var aspects:Array = [];
			var consts:Array = [];
			consts.push(lexems[0]);
			var lexemsLn:uint = lexems.length;
			for (var i:uint = 1; i < lexemsLn; i++) {
				var leter:String = String(lexems[i]).charAt(0);
				var newPattern:String = leter;
				var j:uint;
				var ln:uint = String(lexems[i]).length;
				for (j = 1; j < ln; j++) {
					if (String(lexems[i]).charAt(j) != leter) break;
					else newPattern += leter;
				}
				if (addAspect(newPattern, aspects)) {
					consts.push(String(lexems[i]).substring(j));
				} else {
					consts[consts.length - 1] += String(lexems[i]);
				}
			}
			
			var format:Format = new Format();
			format.aspects = aspects;
			format.consts = consts;
			return format;
		}
		
		private static function addAspect(pattern:String, aspects:Array):Boolean {
			switch (pattern) {
				/*
				u - unix timestamp
				 */
				case "u": aspects.push(aspect_u); return true;
				
				/*
				d - day of the month without leading zero
				dd - day of the month with leading zero
				ddd - short day name (day of the week)
				dddd - long day name (day of the week)
				 */
				case "d": aspects.push(aspect_d); return true;
				case "dd": aspects.push(aspect_dd); return true;
				case "ddd": aspects.push(aspect_ddd); return true;
				case "dddd": aspects.push(aspect_dddd); return true;
				
				/* 
				M - month without leading zero
				MM - month with leading zero
				MMM - month as its abbr
				MMMM - month as its name
				*/
				case "M": aspects.push(aspect_M); return true;
				case "MM": aspects.push(aspect_MM); return true;
				case "MMM": aspects.push(aspect_MMM); return true;
				case "MMMM": aspects.push(aspect_MMMM); return true;
				
				/*
				y - 2-digits without leading zero
				yy - 2-digits with leading zero
				yyyy - 4-digits year
				*/
				case "y": aspects.push(aspect_y); return true;
				case "yy": aspects.push(aspect_yy); return true;
				case "yyyy": aspects.push(aspect_yyyy); return true;
				
				/*
				h - hours without leading zero (12 hour clock)
				hh - hours with leading zero (12 hour clock)
				H - hours without leading zero (24 hour clock)
				HH - hours with leading zero (24 hour clock)
				*/
				case "h": aspects.push(aspect_h); return true;
				case "hh": aspects.push(aspect_hh); return true;
				case "H": aspects.push(aspect_H); return true;
				case "HH": aspects.push(aspect_HH); return true;
				
				/*
				m - minutes without leading zero
				mm - minutes with leading zero
				*/
				case "m": aspects.push(aspect_m); return true;
				case "mm": aspects.push(aspect_mm); return true;
				
				/*		
				s - seconds without leading zero
				ss - seconds with leading zero
				*/
				case "s": aspects.push(aspect_s); return true;
				case "ss": aspects.push(aspect_ss); return true;
				
				/*
				t - one character time-marker string (A,P)
				tt - multicharacter time-marker string (AM,PM)
				*/
				case "t": aspects.push(aspect_t); return true;
				case "tt": aspects.push(aspect_tt); return true;
				
				/* 
				z - time zone offset without leading zero
				zz - time zone offset with leading zero
				zzz - time zone offset with minutes and leading zero
				*/
				case "z": aspects.push(aspect_z); return true;
				case "zz": aspects.push(aspect_zz); return true;
				case "zzz": aspects.push(aspect_zzz); return true;
			}
			return false;
		}
		
		//-------------------------------------------------------------------------
		//		ASPECTS
		//-------------------------------------------------------------------------
		
		private static function aspect_u(date:Date):String {
			return date.getTime().toString();
		}
		
		private static function aspect_d(date:Date):String {
			return date.getUTCDate().toString();
		}
		
		private static function aspect_dd(date:Date):String {
			var tmp:String = date.getUTCDate().toString();
			return tmp.length == 1 ? "0" + tmp : tmp;
		}
		
		private static function aspect_ddd(date:Date):String {
			return locale.shortWeekDayNames[date.getUTCDay()];
		}		
		
		private static function aspect_dddd(date:Date):String {
			return locale.weekDayNames[date.getUTCDay()];
		}

		private static function aspect_M(date:Date):String {
			return (date.getUTCMonth() + 1).toString();
		}

		private static function aspect_MM(date:Date):String {
			var tmp:String = (date.getUTCMonth() + 1).toString();
			return tmp.length == 1 ? "0" + tmp : tmp;
		}

		private static function aspect_MMM(date:Date):String {
			return locale.shortMonthNames[date.getUTCMonth()];
		}

		private static function aspect_MMMM(date:Date):String {
			return locale.monthNames[date.getUTCMonth()];
		}
	
		private static function aspect_y(date:Date):String {
			return date.getUTCFullYear().toString().substr(3,1);
		}

		private static function aspect_yy(date:Date):String {
			return date.getUTCFullYear().toString().substr(2,2);
		}
		
		private static function aspect_yyyy(date:Date):String {
			return date.getUTCFullYear().toString();
		}

		private static function aspect_h(date:Date):String {
			var tmp:Number = date.getUTCHours();
			if (tmp == 0)
				return "12";
			if (tmp > 12)
				return (tmp - 12).toString();
			return tmp.toString();
		}

		private static function aspect_hh(date:Date):String {
			var tmp:Number = date.getUTCHours();
			if (tmp == 0)			
				return "12";
			var s:String;
			if (tmp > 12){
				s = (tmp - 12).toString();
				return (s.length == 1) ? "0" + s : s;
			}
			s = tmp.toString();
			return (s.length == 1) ? "0" + s : s;
		}

		private static function aspect_H(date:Date):String {
			return date.getUTCHours().toString();
		}

		private static function aspect_HH(date:Date):String {
			var tmp:String = date.getUTCHours().toString();
			return (tmp.length == 1) ? "0" + tmp : tmp;
		}

		private static function aspect_m(date:Date):String {
			return date.getUTCMinutes().toString();
		}

		private static function aspect_mm(date:Date):String {
			var tmp:String = date.getUTCMinutes().toString();
			return (tmp.length == 1) ? "0" + tmp : tmp;
		}

		private static function aspect_s(date:Date):String {
			return date.getUTCSeconds().toString();
		}
		
		private static function aspect_ss(date:Date):String {
			var tmp:String = date.getUTCSeconds().toString();
			return (tmp.length == 1) ? "0" + tmp : tmp;
		}

		private static function aspect_t(date:Date):String {
			var tmp:Number = date.getUTCHours();
			if (tmp >= 12)
				return locale.shortPmString;
			return locale.shortAmString;
		}

		private static function aspect_tt(date:Date):String {
			var tmp:Number = date.getUTCHours();
			if (tmp >= 12)
				return locale.pmString;
			return locale.amString;
		}
		
		private static function aspect_z(date:Date):String {
			var tmp:Number = -date.getTimezoneOffset() / 60;
			return tmp >= 0 ? "+" + tmp.toString() : tmp.toString();
		}

		private static function aspect_zz(date:Date):String {
			var tmp:Number = -date.getTimezoneOffset() / 60;
			var s:String = Math.abs(tmp).toString();
			s = (s.length == 1 ? "0" + s : s);
			return (tmp < 0 ? "-" + s : "+" + s);
		}

		private static function aspect_zzz(date:Date):String {
			var s:String = (-date.getTimezoneOffset() % 60).toString();
			var tmp:Number = -date.getTimezoneOffset() / 60;
			s = Math.abs(tmp).toString() + ":" + ((s.length == 1) ? "0" + s : s);
			s = (s.length == 4 ? "0" + s : s);
			return tmp < 0 ? "-" + s : "+" + s;
		}
	}
}

class Format {
	public var consts:Array;
	public var aspects:Array;
	public function Format(){
		this.aspects = [];
		this.consts = [];
	}
} 