package com.anychart.controls.legend {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	public class IconSettings {
		public var width:Number;
		public var height:Number;
		public var color:int;
		
		public var markerEnabled:Boolean;
		public var markerType:int;
		public var markerColor:int;
		private var markerSize:Number;
		
		public function IconSettings() {
			this.width = 20;
			this.height = 10;
			this.markerEnabled = false;
			this.markerType = -1;
			this.markerColor = -1;
			this.color = -1;
		}
		
		public function createCopy(settings:IconSettings = null):IconSettings {
			if (settings == null)
				settings = new IconSettings();
			settings.width = this.width;
			settings.height = this.height;
			settings.markerEnabled = this.markerEnabled;
			settings.markerType = this.markerType;
			settings.markerSize = this.markerSize;
			settings.markerColor = this.markerColor;
			settings.color = this.color;
			return settings;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.@width != undefined) this.width = SerializerBase.getNumber(data.@width);
			if (data.@height != undefined) this.height = SerializerBase.getNumber(data.@height);
			if (data.@color != undefined) {
				if (SerializerBase.getEnumItem(data.@color) == '%color') this.color = -1;
				else this.color = SerializerBase.getColor(data.@color);
			}
			if (data.marker[0] != null) {
				var marker:XML = data.marker[0];
				if (marker.@enabled != undefined) this.markerEnabled = SerializerBase.getBoolean(marker.@enabled);
				if (marker.@type != undefined) {
					if (SerializerBase.getEnumItem(marker.@type) == '%markertype')
						this.markerType = -1;
					else
						this.markerType = SerializerBase.getMarkerType(marker.@type);
				}
				if (marker.@color != undefined) this.markerColor = SerializerBase.getColor(marker.@color);
				if (marker.@size != undefined) this.markerSize = SerializerBase.getNumber(marker.@size); 
			}
		}
		
		public function getMarkerSize():Number {
			return (isNaN(this.markerSize)) ? (Math.min(this.width, this.height)*.5) : this.markerSize;
		} 
	}
}