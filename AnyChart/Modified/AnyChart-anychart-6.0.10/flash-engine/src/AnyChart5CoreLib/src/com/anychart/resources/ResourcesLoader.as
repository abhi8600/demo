package com.anychart.resources {
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoaderDataFormat;
	
	/**
	 * Событие ошибки безопасности при загрузке одного из ресурсов
	 * @eventType flash.events.SecurityErrorEvent
	 */
	[Event(name="securityError",type="flash.events.SecurityErrorEvent")]
	
	/**
	 * Событие ошибки ввода-вывода при загрузке одного из ресурсов
	 * Обычно происходит когда невозможно найти файл
	 * @eventType flash.events.IOErrorEvent
	 */
	[Event(name="ioError",type="flash.events.IOErrorEvent")]
	
	/**
	 * Событие завершения загрузки всех ресурсов
	 * @eventType flash.events.IOErrorEvent
	 */
	[Event(name="complete",type="flash.events.Event")]
	
	/**
	 * Событие прогресса загрузки всех ресурсов
	 * Использовать и слушать не рекомендую, один фиг часто информация о размерах не реальная
	 * @eventType flash.events.ProgressEvent
	 */
	[Event(name="progress",type="flash.events.ProgressEvent")]
	
	/**
	 * Класс для загрузки одного и более ресурсов с возможностью кэширования и указания типа путей<br />
	 * т.е. класс позволяет указывать путь к ресурсам относительно swf-ки или же html-ки<br />
	 * 
	 * Так же с помощью этого класса можно остановить загрузку всех ресурсов<br />
	 * Получить все загруженые ресурсы в последствии можно используя ResourcesHashMap
	 * 
	 * @see ResourcesPathSettings
	 * @see ResourcesHashMap
	 * 
	 * @example Пример загрузки двух текстовых ресурсов
	 * <listing version="3.0"> package {
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class ResourcesLoaderExample extends Sprite {
		
		private var loader:ResourcesLoader;
		private var data1RealPath:String;
		private var data2RealPath:String;
		
		public function ResourcesLoaderExample() {
			this.loader = new ResourcesLoader();
			this.loader.deserializeSettings(&lt;empty /&gt;);
			this.loader.initialize(this.loaderInfo.url);
			this.data1RealPath = this.loader.addText("data-1.txt",false);
			this.data2RealPath = this.loader.addText("data-2.txt",false);
			this.loader.addEventListener(Event.COMPLETE, this.completeHandler);
		}
		
		private function completeHandler(e:Event):void {
			trace (ResourcesHashMap.getText(this.data1RealPath));
			trace (ResourcesHashMap.getText(this.data2RealPath));
		}
	}
}
	 * </listing>
	 * 
	 * Относительным путем в данной документации называется путь без учета настроек в ResourcesPathSettings<br />
	 * Абсолютным путем соотвественно называется путь с учетом настроек в ResourcesPathSettings
	 */
	public final class ResourcesLoader extends EventDispatcher {
		
		/**
		 * @private
		 * Настройки пути к любым ресурсам, кроме карт, xml-я и шрифтов
		 */
		private var resourcesPth:ResourcesPathSettings;
		
		/**
		 * @private
		 * Настройки пути к XML ресурсам
		 */
		private var xmlResourcesPth:ResourcesPathSettings;
		
		/**
		 * @private
		 * Настройки пути к embed шрифтам
		 * @see com.anychart.visual.text.EmbedableFontSettings
		 * @see com.anychart.visual.text.BaseTextElementWithEmbedFonts
		 */
		private var fontResourcesPth:ResourcesPathSettings;
		
		/**
		 * @private
		 * Настройки пути к картам
		 * @see com.anychart.mapPlot.MapPlot
		 */
		private var mapsPth:ResourcesPathSettings;
		
		/**
		 * Конструктор
		 */
		public function ResourcesLoader() {
			this.nonLoadedEntriesList = [];
			this.nonLoadedEntriesMap = {};
		}
		
		/**
		 * Метод для клонирования ResourcesLoader в другой ResourcesLoader или же в новый ResourcesLoader
		 * @param loader ResourceLoader, в который клонировать текущий ResourceLoader
		 * @return новый ResourceLoader если loader == null
		 */
		public function createCopy(loader:ResourcesLoader = null):ResourcesLoader {
			if (loader == null)
				loader = new ResourcesLoader();
			loader.resourcesPth = this.resourcesPth;
			loader.xmlResourcesPth = this.xmlResourcesPth;
			loader.fontResourcesPth = this.fontResourcesPth;
			loader.mapsPth = this.mapsPth;
			return loader;
		}
		
		/**
		 * Десериализация настроек базовых путей ресурсов
		 * @param data настройки в виде XML
		 * @see com.anychart.resources.ResourcesPathSettings
		 * @see #initialize()
		 * @example <listing version="3.0">
loader.deserialize(&lt;data&gt;
	&lt;settings&gt;
		&lt;resources path="./assets/" path_type="RelativeToSWF" /&gt;
	&lt;/settings&gt;
&lt;/data&gt;);</listing>		 
		 * 
		 */
		public function deserializeSettings(data:XML):void {
			
			this.resourcesPth = new ResourcesPathSettings(ResourcesPathSettings.PATH_RELATIVE_TO_HTML);
			this.xmlResourcesPth = new ResourcesPathSettings(ResourcesPathSettings.PATH_RELATIVE_TO_HTML);
			this.fontResourcesPth = new ResourcesPathSettings(ResourcesPathSettings.PATH_RELATIVE_TO_SWF,"fonts/");
			this.mapsPth = new ResourcesPathSettings(ResourcesPathSettings.PATH_RELATIVE_TO_SWF,"maps/");
			
			if (data.settings[0] != null) {
				var settings:XML = data.settings[0];
				if (settings.resources[0] != null)
					this.resourcesPth.deserialize(settings.resources[0]);
				if (settings.xml_resources[0] != null)
					this.xmlResourcesPth.deserialize(settings.xml_resources[0]);
				if (settings.fonts[0] != null)
					this.fontResourcesPth.deserialize(settings.fonts[0]);
				if (settings.maps[0] != null)
					this.mapsPth.deserialize(settings.maps[0]);
			}
		}
		
		/**
		 * Инициализация настроек базовых путей к ресурсам
		 * @param url путь к swf. Получается как правило из loaderInfo.url у основного класса
		 * @see com.anychart.resources.ResourcesPathSettings
		 * @see #deserializeSettings()
		 */
		public function initialize(url:String = null):void {
			this.resourcesPth.initialize(url);
			this.xmlResourcesPth.initialize(url);
			this.fontResourcesPth.initialize(url);
			this.mapsPth.initialize(url);
		}
		
		//---------------------------------------------------------------
		//		ENTRIES ADDING
		//---------------------------------------------------------------
		
		/**
		 * Добавить картинку для загрузки (всегда кэшируется)
		 * @param path путь к картинке
		 * @return реальный путь к картинке с учетом ResourcesPathSettings
		 * @see ResourcesHashMap#getImage()
		 */
		public function addImage(path:String):String {
			path = this.resourcesPth.apply(path);
			if (ResourcesHashMap.data[path] != null || this.nonLoadedEntriesMap[path] != null) return path;
			var entry:ImageEntry = new ImageEntry();
			entry.initialize(path, true);
			return this.addEntry(entry);
		}
		
		/**
		 * Добавить бинарные данные для загрузки (всегда кэшируется)
		 * @param path путь к файлу
		 * @return реальный путь к файлу с учетом ResourcesPathSettings
		 * @see ResourcesHashMap#getBinary()
		 */
		public function addBinary(path:String):String {
			if (ResourcesHashMap.data[path] != null || this.nonLoadedEntriesMap[path] != null) return path;
			var entry:DataEntry = new DataEntry(URLLoaderDataFormat.BINARY);
			entry.initialize(path, true);
			return this.addEntry(entry);
		}
		
		/**
		 * Добавить текстовые данные для загрузки (всегда кэшируется)
		 * @param path путь к файлу
		 * @param cashe добавить заруженые данные в кэш для предотвращения повторной загрузки
		 * @return реальный путь к файлу с учетом ResourcesPathSettings
		 * @see ResourcesHashMap#getText()
		 */
		public function addText(path:String, cashe:Boolean):String {
			if (this.nonLoadedEntriesMap[path] != null || (cashe && ResourcesHashMap.data[path] != null)) return path;
			var entry:DataEntry = new DataEntry(URLLoaderDataFormat.TEXT);
			entry.initialize(path, cashe);
			return this.addEntry(entry);
		}
		
		/**
		 * Добавить embed font для загрузки (всегда кэшируется)
		 * @param path путь к файлу
		 * @return реальный путь к файлу с учетом ResourcesPathSettings
		 */
		public function addFont(path:String):String {
			var basePath:String = path;
			path = this.fontResourcesPth.apply(path);
			if (ResourcesHashMap.data[path] != null || this.nonLoadedEntriesMap[path] != null) return path;
			var entry:FontEntry = new FontEntry();
			entry.initialize(path, true);
			this.addEntry(entry);
			return basePath;
		}
		
		/**
		 * Добавить кастомную запись для загрузки
		 * @param entry экземпляр класса, наследуемого от ResourceEntry
		 * @return реальный путь к файлу с учетом ResourcesPathSettings
		 * @see ResourceEntry
		 */
		public function addCustomEntry(entry:ResourceEntry):String {
			if (ResourcesHashMap.data[entry.path] != null || this.nonLoadedEntriesMap[entry.path] != null) return entry.path;
			return this.addEntry(entry);
		}
		
		/**
		 * Получить реальный путь к карте
		 * @see com.anychart.mapPlot.MapPlot
		 * @return String c реальным путем к amap файлу
		 */
		public function getMapPath(path:String):String {
			return this.mapsPth.apply(path);
		}
		
		/**
		 * Добавление ResourceEntry для загрузки
		 * добавляет слушаетелей для событий, возвращает реальный путь, добавляет запись в незагруженые
		 * @param entry ResourceEntry
		 * @return real path 
		 */
		private function addEntry(entry:ResourceEntry):String {
			entry.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.securityErrorEventHandler);
			entry.addEventListener(IOErrorEvent.IO_ERROR, this.ioErrorEventHandler);
			entry.addEventListener(ResourceEntryEvent.COMPLETE, this.loadEntryEventHandler);
			entry.addEventListener(ProgressEvent.PROGRESS, this.entryProgessEventHandler);
			
			this.nonLoadedEntriesMap[entry.path] = this.nonLoadedEntriesList.length;
			this.nonLoadedEntriesList.push(entry);
			return entry.path;
		}
		
		//---------------------------------------------------------------
		//		ENTRIES LOADING
		//---------------------------------------------------------------
		
		/**
		 * Массив со всеми ресурсами, которые еще не загружены
		 * @private
		 */
		private var nonLoadedEntriesList:Array;
		
		/**
		 * Hash Map со всеми ресурсами, которые еще не загружены
		 * ключ - реальный путь к ресурсу
		 * значение - индекс ресурса в nonLoadedEntriesList
		 * 
		 * Используется для удаления из nonLoadedEntriesList уже загруженых ресурсов
		 * @see #deleteEntry()
		 */
		private var nonLoadedEntriesMap:Object;
		
		/**
		 * Метод для проверки состояния загрузки
		 * @return true если все ресурсы загружены, false если нет
		 */
		public function isLoaded():Boolean {
			return this.nonLoadedEntriesList.length == 0;
		}
		
		/**
		 * Метод для запуска загрузки всех ресурсов
		 */
		public function load():void {
			if (this.nonLoadedEntriesList.length > 0)
				ResourceEntry(this.nonLoadedEntriesList[0]).load();
		}
		
		//---------------------------------------------------------------
		//		STOP LOADING
		//---------------------------------------------------------------
		
		/**
		 * Метод для остановки загрузки всех ресурсов
		 */
		public function stop():void {
			for each (var entry:ResourceEntry in this.nonLoadedEntriesList) {
				entry.stopLoading();
			}
		}
		
		//---------------------------------------------------------------
		//		ENTRIES EVENTS
		//---------------------------------------------------------------
		
		private function loadEntryEventHandler(event:ResourceEntryEvent):void {
			var entry:ResourceEntry = ResourceEntry(event.target);
			ResourcesHashMap.data[entry.path] = event.data;
			this.deleteEntry(entry);
			if (this.isLoaded())
				this.dispatchEvent(new Event(Event.COMPLETE));
			else
				ResourceEntry(this.nonLoadedEntriesList[0]).load();
		}
		
		private function entryProgessEventHandler(event:ProgressEvent):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			this.dispatchEvent(event.clone());
		}
		
		private function deleteEntry(entry:ResourceEntry):void {
			for (var i:int = 0;i<this.nonLoadedEntriesList.length;i++) {
				if (this.entriesEqual(this.nonLoadedEntriesList[i],entry)) {
					this.nonLoadedEntriesList.splice(i,1);
					delete this.nonLoadedEntriesMap[entry.path];
					break;
				}
			}
		}
		
		private function entriesEqual(entry1:ResourceEntry, entry2:ResourceEntry):Boolean {
			return entry1.path == entry2.path;
		}
		
		private function securityErrorEventHandler(event:SecurityErrorEvent):void {
			this.deleteEntry(ResourceEntry(event.target));
			this.bubbleEvent(event);
		}
		
		private var _lastTarget:ResourceEntry;
		public function getLastTarget():ResourceEntry { return this._lastTarget; } 
		
		private function ioErrorEventHandler(event:IOErrorEvent):void {
			this._lastTarget = ResourceEntry(event.target);
			this.deleteEntry(ResourceEntry(event.target));
			this.bubbleEvent(event);
		}
		
		private function bubbleEvent(event:Event):void {
			event.stopImmediatePropagation();
			event.stopPropagation();
			this.dispatchEvent(event.clone());
		}
	}
}