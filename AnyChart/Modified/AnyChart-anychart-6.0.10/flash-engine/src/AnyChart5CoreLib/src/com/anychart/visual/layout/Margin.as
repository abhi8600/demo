package com.anychart.visual.layout {
	
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	import flash.geom.Rectangle;

	public class Margin implements ISerializable {
		
		// Properties.
		public var left:Number=10;
		public var top:Number=10;
		public var right:Number=10;
		public var bottom:Number=10;
		
		public function set all(value:Number):void {
			this.left=this.top=this.right=this.bottom=value;
		}
		
		public function applyOutside(rect:Rectangle):void {
			rect.width += this.left + this.right;
			rect.height += this.top + this.bottom;
		}
		
		public function applyInside(rect:Rectangle):void {
			rect.x += this.left;
			rect.y += this.top;
			rect.width -= this.left + this.right;
			rect.height -= this.top + this.bottom;
		}

		// Methods.
		public function deserialize(data:XML):void {

			if(data.@all!=undefined) this.all=SerializerBase.getNumber(data.@all);
			
			if(data.@left!=undefined) this.left=SerializerBase.getNumber(data.@left);
			if(data.@top!=undefined) this.top=SerializerBase.getNumber(data.@top);
			if(data.@right!=undefined) this.right=SerializerBase.getNumber(data.@right);
			if(data.@bottom!=undefined) this.bottom=SerializerBase.getNumber(data.@bottom);
		}
		
	}
}