package com.anychart.elements.marker {
	import com.anychart.elements.AnimatableElement;
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class ItemMarkerElement extends AnimatableElement {
		
		public var type:int = -1;
		
		override public function getStyleStateClass():Class { return MarkerElementStyleState; }
		override protected function getStyleNodeName():String { return 'marker_style'; }
		override protected function getContainer(container:IMainElementsContainer):Sprite { 
			return container.markersContainer; }
		
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new ItemMarkerElement();
			var m:ItemMarkerElement = ItemMarkerElement(element);
			m.type = this.type;
			return super.createCopy(m);
		}
		
		override public function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
			if (data.@type != undefined) {
				this.type = SerializerBase.getMarkerType(data.@type);
			}else {
				this.type = -1;
			}
		}
		
		override public function deserializeElementForContainer(data:XML, 
															   container:IElementContainer, 
															   stylesList:XML, 
															   elementIndex:uint, 
															   resources:ResourcesLoader):void {
			super.deserializeElementForContainer(data, container, stylesList, elementIndex, resources);
			if (data.@type != undefined) {
				if (data.@style != undefined) {
					this.type = SerializerBase.getMarkerType(data.@type);
				}else {
					if (container.elementsInfo == null)
						container.elementsInfo = {};
					if (container.elementsInfo[elementIndex] == null)
						container.elementsInfo[elementIndex] = {};
					container.elementsInfo[elementIndex].type = SerializerBase.getMarkerType(data.@type);
				}
			}
		}
		
		override protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			super.execDrawing(container, state, sprite, elementIndex);
			var markerState:MarkerElementStyleState = MarkerElementStyleState(state);
			
			var type:uint = markerState.markerType;
			if (markerState.isDynamicType) {
				if (container.elementsInfo != null && container.elementsInfo[elementIndex] != null)
					type = container.elementsInfo[elementIndex].type;
				else if (this.type != -1)
					type = this.type;
				else 
					type = container.markerType;
			}
			
			this.drawMarkerByType(type, container.color, container.hatchType, markerState, sprite.graphics, markerState.bounds);

			if (container.hasPersonalContainer && markerState.background.effects != null)
				sprite.filters = markerState.background.effects.list;
		}
		
		private function drawMarker(g:Graphics, type:uint, bounds:Rectangle, size:Number, width:Number = NaN, height:Number = NaN):void {
			MarkerDrawer.draw(g, type, size, bounds.x, bounds.y,width,height);
		}
		
		public function drawSimple(g:Graphics, elementMarkerType:int, 
											   pointMarkerType:int, 
											   state:StyleState, 
											   bounds:Rectangle,
											   color:uint, hatchType:uint):void {
			var markerState:MarkerElementStyleState = MarkerElementStyleState(state);
			
			var type:uint = markerState.markerType;
			if (markerState.isDynamicType) {
				if (pointMarkerType != -1)
					type = pointMarkerType;
				else if (this.type != -1)
					type = this.type;
				else 
					type = elementMarkerType;
			}
			var newBounds:Rectangle = new Rectangle();
			newBounds.width = bounds.width;
			newBounds.height = bounds.height;
			newBounds.x = bounds.x + newBounds.width/2;
			newBounds.y = bounds.y + newBounds.height/2 - markerState.size/2;
			this.drawMarkerByType(type, color, hatchType, markerState, g, newBounds);
		}
		
		private function drawMarkerByType(type:uint, color:uint, hatchType:uint, markerState:MarkerElementStyleState, g:Graphics, bounds:Rectangle):void {
			if (type == MarkerType.NONE) return;
			if (type == MarkerType.IMAGE) {
				var img:BitmapData = ResourcesHashMap.getImage(markerState.imageUrl);
				var m:Matrix = new Matrix();
				m.translate(bounds.x, bounds.y);
				m.scale(markerState.size/img.width,markerState.size/img.height);
				g.beginBitmapFill(img, m, false, false);
				g.drawRect(bounds.x, bounds.y, bounds.width,bounds.height);
				img = null;
				m = null;
			}else {
			
				var hasHatchFill:Boolean = markerState.background.hatchFill != null;
				var hasFill:Boolean = markerState.background.fill != null;
				var hasBorder:Boolean = markerState.background.border != null;
				
				if (hasHatchFill) {
					if (hasFill)
						markerState.background.fill.begin(g, bounds, color);
					this.drawMarker(g, type, bounds, markerState.size, markerState.size_width, markerState.size_height);
					if (hasFill)
						markerState.background.fill.end(g);
						
					if (hasBorder)
						markerState.background.border.apply(g, bounds, color);
					markerState.background.hatchFill.beginFill(g, hatchType, color);
					this.drawMarker(g, type, bounds, markerState.size, markerState.size_width, markerState.size_height);
					g.endFill();
					g.lineStyle();
				}else {
					if (hasFill)
						markerState.background.fill.begin(g, bounds, color); 
					if (hasBorder)
						markerState.background.border.apply(g, bounds, color);
					this.drawMarker(g, type, bounds, markerState.size, markerState.size_width, markerState.size_height);
					if (hasFill)
						g.endFill();
					g.lineStyle();
				}
			}
		}
	}
}