package com.anychart.utils {
	public interface IXMLPreprocessor {
		function process(data:XML):XML;
	}
}