package com.anychart.controls.label {
	import com.anychart.IAnyChart;
	import com.anychart.IChart;
	import com.anychart.controls.Control;
	import com.anychart.plot.IPlot;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.text.InteractiveTitle;
	import com.anychart.visual.text.MultiLineAlign;
	
	import flash.display.DisplayObject;
	
	public final class LabelControl extends Control {
		
		public static function check(data:XML):Boolean {
			return String(data.name()) == "label";
		}
		
		private var title:InteractiveTitle;
		private var titleContainer:DisplayObject;
		
		public function LabelControl() {
			super();
		}
		
		override public function initializeDataContainer(chart:IAnyChart, plot:IPlot, plotContainer:IChart):void {
			super.initializeDataContainer(chart, plot, plotContainer);
			this.title = new InteractiveTitle(chart, plot);
		}
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			/*
			Get settings from styles
			 */
			var parentStyleName:String = data.@style != undefined ? data.@style : null;
			var styleData:XML = StylesList.getStyleXMLByMerge(styles, "label_style", data, parentStyleName, false);
			 
			super.deserialize(styleData, styles, resources);
			this.title.deserialize(styleData, resources);
			this.title.background = null;
		}
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.titleContainer = this.title.initialize(null);
			this.container.addChild(this.titleContainer);
			this.title.initListener(this.container);
			this.container.mouseChildren = false;
			return this.container;
		}
		
		protected function getTokenValue(token:String):* {
			return (this.plot != null) ? this.plot.getTokenValue(token) : "";
		}
		
		private function isDateTimeToken(token:String):Boolean { return false; }
		
		override protected function checkContentBounds():void {
			this.title.info.formattedText = this.title.isDynamicText ? this.title.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : this.title.text;
			this.title.getBounds(this.title.info);
			if (isNaN(this.bounds.width)) {
				this.bounds.width = this.title.info.rotatedBounds.width;
				if (this.title.multiLineAlign != MultiLineAlign.CENTER)
					this.bounds.width += this.title.padding;
			}
			if (isNaN(this.bounds.height)) {
				this.bounds.height = this.title.info.rotatedBounds.height;
			}
			super.checkContentBounds();
		}
		
		override public function draw():void {
			super.draw();
			this.drawTitle();
		}
		
		override public function resize():void {
			super.resize();
			this.drawTitle();
		}
		
		private function drawTitle():void {
			this.titleContainer.x = this.contentBounds.x;
			this.titleContainer.y = this.contentBounds.y; 
			switch (this.title.multiLineAlign) {
				case MultiLineAlign.LEFT:
					this.titleContainer.x += this.title.padding;
					break;
				case MultiLineAlign.CENTER:
					this.titleContainer.x += (this.contentBounds.width - this.title.info.rotatedBounds.width)/2;
					break;
				case MultiLineAlign.RIGHT:
					this.titleContainer.x += this.contentBounds.width - this.title.info.rotatedBounds.width - this.title.padding;
					break;
			}
			this.title.drawTitle();
		}
	}
}