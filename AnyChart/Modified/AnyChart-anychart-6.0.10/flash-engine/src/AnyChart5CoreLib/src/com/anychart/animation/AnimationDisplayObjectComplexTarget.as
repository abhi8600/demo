package com.anychart.animation {
	import com.anychart.animation.interpolation.Interpolator;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Rectangle;
	
	public class AnimationDisplayObjectComplexTarget extends AnimationTarget {
	
		protected var x:AnimatableProperty;
		protected var y:AnimatableProperty;
		private var width:AnimatableProperty;
		private var height:AnimatableProperty;
		private var opacity:AnimatableProperty;
		
		public var container:DisplayObject;
		public var mainContainer:DisplayObject;
		
		protected var space:Number = 10;
		
		protected var containerRect:Rectangle;
		
		public var mainContainerWidth:Number;
		public var mainContainerHeight:Number;
		
		public var useRect:Boolean = false;
		
		override public function destroy():void {
			this.x = null;
			this.y = null;
			this.width = null;
			this.height = null;
			this.opacity = null;
			this.container = null;
			this.mainContainer = null;
		}
		
		override public function initialize():void {
			this.mainContainerWidth = this.mainContainer.width;
			this.mainContainerHeight = this.mainContainer.height;
			
			this.containerRect = this.container.getBounds(this.mainContainer).clone();
		}
		
		override public function setInitial():void {
			if (this.x) this.setInitialX();
			if (this.y) this.setInitialY();
			if (this.width) this.setInitialWidth();
			if (this.height) this.setInitialHeight();
			if (this.opacity) this.setInitialOpacity();
		}
		
		override public function setFinal():void {
			super.setFinal();
			if (this.x) this.container.x = this.x.end;
			if (this.y) this.container.y = this.y.end;
			if (this.opacity) this.container.alpha = this.opacity.end;
			if (this.width) this.setContainerWidth(1);
			if (this.height) this.setContainerHeight(1);
		}
		
		override public function animate(currentTime:Number):void {
			var duration:Number = this.animation.duration;
			var i:Interpolator = this.animation.interpolation;
			
			if (this.width)
				this.setContainerWidth(this.width.getValue(i, currentTime, duration));
			else if (this.x) 
				this.container.x = this.x.getValue(i, currentTime, duration);
			
			if (this.height)
				this.setContainerHeight(this.height.getValue(i, currentTime, duration));
			else if (this.y)
				this.container.y = this.y.getValue(i, currentTime, duration);
				
			if (this.opacity) this.container.alpha = this.opacity.getValue(i, currentTime, duration);
		}
		
		protected function setInitialX():void {
			var startX:Number;
			switch (this.x.tempValue) {
				case HorizontalAlign.LEFT:
					startX = this.container.x-this.containerRect.right;
					break;
				case HorizontalAlign.CENTER:
					startX = this.container.x-this.containerRect.left + (this.mainContainerWidth - this.containerRect.width)/2;
					break;
				case HorizontalAlign.RIGHT:
					startX = this.container.x+this.mainContainerWidth - this.containerRect.left;
					break;
			}
			
			this.x.initialize(startX, this.container.x);
			if (!this.x.isAnimated())
				this.x = null;
			else
				this.container.x = startX;
		}
		
		protected function setInitialY():void {
			var startY:Number;
			switch (this.y.tempValue) {
				case VerticalAlign.TOP:
					startY = this.container.y - this.containerRect.bottom;
					break;
				case VerticalAlign.CENTER:
					startY = this.container.y - this.containerRect.top + (this.mainContainerHeight - this.containerRect.height)/2;
					break;
				case VerticalAlign.BOTTOM:
					startY = this.container.y + this.mainContainerHeight - this.containerRect.top;
					break;
			}
			this.y.initialize(startY, this.container.y);
			if (!this.y.isAnimated())
				this.y = null;
			else
				this.container.y = startY;
		}
		
		private function setInitialWidth():void {
			this.width.initialize(0, 1);
	
			switch (this.width.tempValue) {
				case HorizontalAlign.LEFT:
					this.width.tempValue = this.containerRect.left - this.container.x;
					break;
				case HorizontalAlign.RIGHT:
					this.width.tempValue = this.containerRect.right - this.container.x;
					break;
				case HorizontalAlign.CENTER:
					this.width.tempValue = (this.containerRect.left + this.containerRect.right)/2  - this.container.x;
					break;
			}
			this.width.tempValue1 = this.container.x;
	
			if (!this.width.isAnimated())
				this.width = null;
			else
				this.container.scaleX = 0;
		}
		
		private function setInitialHeight():void {
			this.height.initialize(0, 1);
			
			switch (this.height.tempValue) {
				case VerticalAlign.TOP:
					this.height.tempValue = this.containerRect.top - this.container.y;
					break;
				case VerticalAlign.BOTTOM:
					this.height.tempValue = this.containerRect.bottom - this.container.y;
					break;
				case VerticalAlign.CENTER:
					this.height.tempValue = (this.containerRect.top + this.containerRect.bottom)/2;
					break;
			}
			this.height.tempValue1 = this.container.y;
			
			if (!this.height.isAnimated())
				this.height = null;
			else
				this.container.scaleY = 0;
		}
		
		private function setInitialOpacity():void {
			this.opacity.initialize(0, this.container.alpha);
			this.container.alpha = 0;
		}
		
		private function setContainerWidth(scale:Number):void {
			this.container.scaleX = scale;
			this.container.x = this.width.tempValue1 + this.width.tempValue * (1 - scale);
		}
		
		private function setContainerHeight(scale:Number):void {
			this.container.scaleY = scale;
			this.container.y = this.height.tempValue1 + this.height.tempValue * (1 - scale);
		}
		
		//---------------------------------------------------
		//	Animation
		//---------------------------------------------------
		
		public function setX(initialPosition:uint):void {
			this.x = new AnimatableProperty(initialPosition);
		}
		
		public function setY(initialPosition:uint):void {
			this.y = new AnimatableProperty(initialPosition);
		}
		
		public function setScaleX(initialPosition:uint):void {
			this.width = new AnimatableProperty(initialPosition);
		}
		
		public function setScaleY(initialPosition:uint):void {
			this.height = new AnimatableProperty(initialPosition);
		}
		
		public function setOpacity():void {
			this.opacity = new AnimatableProperty(0);
		}
	}
}