package com.anychart.elements {
	import com.anychart.IAnyChart;
	import com.anychart.elements.label.ItemLabelElement;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	public interface IMainElementsContainer {
		function getLabelsContainer(label:ItemLabelElement):Sprite;
		function get tooltipsContainer():DisplayObjectContainer;
		function get markersContainer():Sprite;
		
		function get root():IAnyChart;
	}
}