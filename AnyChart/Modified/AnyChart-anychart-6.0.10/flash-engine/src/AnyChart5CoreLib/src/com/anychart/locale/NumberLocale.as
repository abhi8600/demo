package com.anychart.locale {
	import com.anychart.dispose.DisposableStaticList;
	
	public final class NumberLocale {
		private static var decimalSeparator:String = ".";
		private static var thousandsSeparator:String = "";
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(NumberLocale);
			return true;
		}
		
		public static function deserialize(data:XML):void {
			if (data.@decimal_separator != undefined) 
				decimalSeparator = String(data.@decimal_separator);
			if (data.@thousands_separator != undefined)
				thousandsSeparator = String(data.@thousands_separator);
				
			isDefault = decimalSeparator == "." && thousandsSeparator == "";
		}
		
		public static function clear():void {
			decimalSeparator = ".";
			thousandsSeparator = "";
			isDefault = true;
		}
		
		public static function getValue(value:*):Number {
			return isDefault ? Number(value) : parseValue(value);
		}
		
		private static function parseValue(value:*):Number {
			var res:String = String(value);
			res = res.split(thousandsSeparator).join("");
			res = res.split(decimalSeparator).join(".");
			return Number(res);
		}
		
		private static var isDefault:Boolean;
	}
}