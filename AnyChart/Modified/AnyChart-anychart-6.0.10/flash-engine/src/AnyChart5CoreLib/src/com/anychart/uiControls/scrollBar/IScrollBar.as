package com.anychart.uiControls.scrollBar {
	import com.anychart.IResizable;
	import com.anychart.serialization.ISerializableRes;
	
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	public interface IScrollBar extends ISerializableRes, IResizable, IEventDispatcher {
		function get scrollBarSize():Number;
		function set scrollBarSize(value:Number):void;
		function set min(value:Number):void;
		function get min():Number;
		function set max(value:Number):void;
		function get max():Number;
		function set visibleRange(value:Number):void;
		function get visibleRange():Number;
		function set value(value:Number):void;
		function get value():Number;
		
		function thumb_onMouseDown(e:MouseEvent):void;
	}
}