package com.anychart.resources {
	import com.anychart.dispose.DisposableStaticList;
	
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	/**
	 * Глобальное хранилище всех загруженых ресурсов 
	 * 
	 * Цели:
	 * 1. Легко достучаться до загруженого ресурса<br />
	 * 2. Предотвратить повторную загрузку уже загруженых ресурсов
	 * 
	 * @see ResourcesLoader 
	 */
	public final class ResourcesHashMap {
		
		/**
		 * Hash map с загружеными ресурсами
		 * ключ - полный путь к ресурсу
		 * значение - данные
		 * @see ResourceEntry#setData()
		 */
		internal static var data:Object = {}; 
		
		/**
		 * Получить BitmapData загруженого изображения
		 * @param path абсолютный путь к ресурсу
		 * @return BitmapData
		 * @see ResourcesLoader#addImage()
		 */
		public static function getImage(path:String):BitmapData {
			return BitmapData(data[path]).clone();
		}
		
		/**
		 * Получить ByteArray загруженых данных
		 * @param path абсолютный путь к ресурсу
		 * @return BitmapData
		 * @see ResourcesLoader#addBinary()
		 */
		public static function getBinary(path:String):ByteArray {
			return ByteArray(data[path]);
		}
		
		/**
		 * Получить String загруженых данных
		 * @param path абсолютный путь к ресурсу
		 * @return String
		 * @see ResourcesLoader#addText()
		 */
		public static function getText(path:String):String {
			return String(data[path]);
		}
		
		/**
		 * Получить Object загруженых данных
		 * @param path абсолютный путь к ресурсу
		 * @return String
		 * @see ResourcesLoader#addEntry()
		 */
		public static function getObject(path:String):Object {
			return data[path];
		}
		
		/**
		 * Удалить загруженый ресурс из кэша
		 * @param path абсолютный путь к ресурсу
		 */
		public static function removeEntry(path:String):void {
			delete data[path];
		}
		
		private static var _disposeInit:Boolean = initDispose();
		private static function initDispose():Boolean {
			DisposableStaticList.register(ResourcesHashMap);
			return true;
		}
		
		public static function clear():void {
			data = {};
		}
	}
}