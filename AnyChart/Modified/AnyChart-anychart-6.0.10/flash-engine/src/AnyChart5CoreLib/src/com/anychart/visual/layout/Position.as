package com.anychart.visual.layout {
	public final class Position {
		public static const LEFT:uint = 0;
		public static const RIGHT:uint = 1;
		public static const TOP:uint = 2;
		public static const BOTTOM:uint = 3;
	}
}