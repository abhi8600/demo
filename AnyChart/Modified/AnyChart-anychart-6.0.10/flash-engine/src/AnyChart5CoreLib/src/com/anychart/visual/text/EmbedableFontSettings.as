package com.anychart.visual.text {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	
	public final class EmbedableFontSettings extends FontSettings {
		public var isEmbed:Boolean;
		
		public function EmbedableFontSettings() {
			super();
			this.isEmbed = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			super.deserialize(data, resources, style);
			if (!this.renderAsHTML) {
				if (this.family.indexOf(".swf") == this.family.length - 4) {
					this.isEmbed = true;
					this.family = resources.addFont(this.family);
				}
			}
		}
		
		public function updateFormat():void {
			this.format.font = this.family; 
			this.format.size = this.size; 
			this.format.color = this.color; 
			this.format.bold = this.bold;
			this.format.italic = this.italic; 
			this.format.underline = this.underline;
		}
	}
}