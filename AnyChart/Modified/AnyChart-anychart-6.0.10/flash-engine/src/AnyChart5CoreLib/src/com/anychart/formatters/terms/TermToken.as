package com.anychart.formatters.terms
{
	import com.anychart.dateTime.DateTimeFormatter;
	
	public class TermToken 
	{
		protected var tokenName:String;
		protected var parentCall:ITerm; 
		
		public function TermToken(tokenName:String, parentCall:ITerm) {
			this.tokenName = tokenName;
			this.parentCall = (parentCall != null ? parentCall : new TermEmpty());
		}
		
		private var enabled:Boolean = true;
		public function setEnabled(enabled:Boolean):void {
			this.enabled = enabled;
		}
		
		private var useNegativeSign:Boolean;
		private var trailingZeros:Boolean;
		private var leadingZeros:int;
		private var numDecimals:int;
		private var thousandsSeparator:String;
		private var decimalSeparator:String;
		private var dateTimeFormat:String;
		
		private var maxChar:int;
		private var maxCharFinalChars:String;

		public function setFormatters(useNegativeSign:Boolean, trailingZeros:Boolean,
				leadingZeros:int, numDecimals:int, thousandsSeparator:String, decimalSeparator:String, dateTimeFormat:String,
				maxChar:int, maxCharFinalChars:String):void {
			this.useNegativeSign = useNegativeSign;
			this.trailingZeros = trailingZeros;
			this.leadingZeros = leadingZeros;
			this.numDecimals = numDecimals;
			this.thousandsSeparator = thousandsSeparator;
			this.decimalSeparator = decimalSeparator;
			this.dateTimeFormat = dateTimeFormat;
			this.maxChar = maxChar;
			this.maxCharFinalChars = maxCharFinalChars;
		}
		
		protected function formatDateTime(numValue:Number):String {
			return DateTimeFormatter.format(numValue, this.dateTimeFormat);
		}
		
		protected function formatString(value:String):String {
			if (this.maxChar > 0) { 
				if (value.length > this.maxChar) {
					value = value.substr(0, this.maxChar);
					if (this.maxCharFinalChars != null)
						value += this.maxCharFinalChars;
				}
			}
			
			return value;
		}
		
		protected function formatNumber(numValue:Number, isNegative:Boolean):String {
			var intgr:Number = Math.round(numValue*Math.pow(10,numDecimals));
			
			var res:String= Number(intgr/Math.pow(10,numDecimals)).toString();
			var resInt:String= (res.indexOf(".")==-1) ? res : res.substr(0,res.indexOf("."));
			var resFrac:String= (res.indexOf(".")==-1) ? "": res.substr(res.indexOf(".")+1,res.length-1);
			if (trailingZeros && resFrac.length < numDecimals)
				while(resFrac.length<numDecimals)
					resFrac+="0";
			if (leadingZeros > 1)
				while (resInt.length < leadingZeros)
					resInt="0"+resInt;
			if (thousandsSeparator && resInt.length > 3)
				for(var i:Number=resInt.length;i>3;i-=3)
					resInt=resInt.substr(0,i-3)+thousandsSeparator+resInt.substr(i-3,resInt.length-1);
			res= (resFrac!="") ? resInt+decimalSeparator+resFrac : resInt;
			res = (isNegative)? ( (useNegativeSign) ? ('-'+res) : '('+res+')') : res;
			
			return res;
			
			/*
			var c:Number = Math.floor(numValue);
			var d:Number = 0;
			
			var stringD:String = '';
			
			if (numDecimals > 0) {
				d = numValue - c;
				var pow:Number = Math.pow(10,numDecimals);
				d = Math.round(d*pow)/pow;
				if (d >= 1) c += Math.floor(d);
				stringD = d.toString().substr(2);
				if (trailingZeros) {
					while (stringD.length < numDecimals)
						stringD += '0';
				}
			}
			
			var stringC:String = c.toString();
			
			while (stringC.length < leadingZeros) stringC = '0'+stringC;
				
			if (thousandsSeparator != '' && stringC.length > 3) {
				var newStringC:String = '';
				for (var i:Number = 0; i < stringC.length; i++) {
					if (i != 0 && i%3 == 0)
						newStringC = thousandsSeparator + newStringC;
					newStringC = stringC.charAt(stringC.length - i - 1) + newStringC;
				}
				stringC = newStringC;
			}
			
			var res:String = (stringD == '') ? stringC : (stringC + decimalSeparator + stringD);
			if (isNegative) return useNegativeSign ? ('-'+res) : '('+res+')';

			return res;*/
		}
	}
}