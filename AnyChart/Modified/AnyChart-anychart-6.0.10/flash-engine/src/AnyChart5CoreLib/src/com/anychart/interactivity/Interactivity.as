package com.anychart.interactivity {
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	public class Interactivity implements ISerializable {
		
		public var allowSelect:Boolean = true;
		public var hoverable:Boolean   = true;
		public var useHandCursor:Boolean = true;
		
		public function deserialize(data:XML):void {
			if (data == null) return;
			if (data.@selectable != undefined) this.allowSelect = SerializerBase.getBoolean(data.@selectable);
			if (data.@allow_select != undefined) this.allowSelect = SerializerBase.getBoolean(data.@allow_select);
			if (data.@hoverable != undefined) this.hoverable = SerializerBase.getBoolean(data.@hoverable);
			if (data.@use_hand_cursor != undefined) this.useHandCursor = SerializerBase.getBoolean(data.@use_hand_cursor);
		}
		
		public function isNew(data:XML):Boolean {
			if (data == null) return false;
			if (data.@allow_select != undefined && 
				SerializerBase.getBoolean(data.@allow_select) != this.allowSelect) return true;
				
			if (data.@hoverable != undefined && 
				SerializerBase.getBoolean(data.@hoverable) != this.hoverable) return true;
				
			if (data.@use_hand_cursor != undefined && 
				SerializerBase.getBoolean(data.@use_hand_cursor) != this.useHandCursor) return true;
				
			return false;
		}
		
		public function createCopy(interactivity:Interactivity = null):Interactivity {
			if (interactivity == null)
				interactivity = new Interactivity();
			interactivity.allowSelect = this.allowSelect;
			interactivity.hoverable = this.hoverable;
			interactivity.useHandCursor = this.useHandCursor;
			return interactivity;
		}
	}
}