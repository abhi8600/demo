package com.anychart.visual.background {
	
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.styles.IStyle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.corners.Corners;
	import com.anychart.visual.corners.CornersType;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public class RectBackground extends BackgroundBase implements IStyleSerializableRes {

		//-------------------------------------------------------
		// 
		//						Properties
		//
		//-------------------------------------------------------		

		public var corners:Corners=null;
		public var margins:Margin = null;

		public function applyInsideMargins(rect:Rectangle):void {
			if (this.margins == null) return;
			this.margins.applyInside(rect);
		}
		
		public function applyMargins(rect:Rectangle):void {
			if (this.margins == null) return;
			this.margins.applyOutside(rect);
		}
		
		public function moveContent(sp:DisplayObject, isText:Boolean = false):void {
			if (this.margins == null) return;
			sp.x += this.margins.left + (isText ? 1 : 0);
			sp.y += this.margins.top + (isText ? 1 : 0);
		}
		
		//-------------------------------------------------------
		// 
		//						Methods
		//
		//-------------------------------------------------------
		
		public function drawWithoutClearing(g:Graphics, rc:Rectangle, styleColor:uint = 0, styleHatchType:uint = 0):void { 
			if(!this.enabled) return;
			
			var isFillVisible:Boolean = this.fill != null;
			var isBorderVisible:Boolean = this.border != null;
			
			var dFill:Number = 0;
			
			if (isBorderVisible && this.border.thickness != 0) {
				dFill = this.border.thickness/2;
			}
			
			if (this.hatchFill!=null && this.hatchFill.enabled) {
				
				if (isBorderVisible) {
					rc.x += dFill;
					rc.y += dFill;
					rc.width -= dFill*2;
					rc.height -= dFill*2;
				}

				// Fill background.
				if (isFillVisible) {
					this.fill.begin(g,rc,styleColor);
					this.drawShape(g,rc);
					this.fill.end(g);
				}
				
				if (isBorderVisible) {
					rc.x -= dFill;
					rc.y -= dFill;
					rc.width += dFill*2;
					rc.height += dFill*2;
					this.border.apply(g,rc,styleColor);
					rc.x += dFill;
					rc.y += dFill;
					rc.width -= dFill*2;
					rc.height -= dFill*2;
				}

				this.hatchFill.beginFill(g,styleHatchType,styleColor);
				this.drawShape(g,rc);
				g.endFill();
				
				if (isBorderVisible) {
					this.border.reset(g);
					
					rc.x -= dFill;
					rc.y -= dFill;
					rc.height += dFill*2;
					rc.width += dFill*2;
				}

			}else if (isFillVisible || isBorderVisible) {
				if (isBorderVisible) {
					this.border.apply(g,rc,styleColor);
					rc.x += dFill;
					rc.y += dFill;
					rc.width -= dFill*2;
					rc.height -= dFill*2;
				}
				
				if (isFillVisible)
					this.fill.begin(g,rc,styleColor);
				
				this.drawShape(g,rc);
				
				if (isFillVisible)
					this.fill.end(g);
					
				if (isBorderVisible) { 
					this.border.reset(g);
					rc.x -= dFill;
					rc.y -= dFill;
					rc.width += dFill*2;
					rc.height += dFill*2;
				}
				
			}
		}
		
		public function draw(g:Graphics, rc:Rectangle, styleColor:uint = 0, styleHatchType:uint = 0):void { 
			if(!this.enabled) return;
			g.clear();
			this.drawWithoutClearing(g, rc, styleColor, styleHatchType);
		}
		
		private function drawShape(g:Graphics, rc:Rectangle):void {
			if (this.corners == null) {
				g.drawRect(rc.x,rc.y,rc.width,rc.height);
			}else {
				this.corners.drawRect(g, rc);
			}
		}
		
		//------------------------------------
		// ISerializable
		//------------------------------------
		
		override public function deserialize(data:XML,resources:ResourcesLoader,style:IStyle = null):void {
			super.deserialize(data,resources,style);
			if (!this.enabled) return;
			if (data.corners[0]!=null) {
				this.corners = new Corners();
				this.corners.deserialize(data.corners[0]);
			}
			if (data.inside_margin[0]!=null) {
				this.margins = new Margin();
				this.margins.deserialize(data.inside_margin[0])
			}
		}
	}
}