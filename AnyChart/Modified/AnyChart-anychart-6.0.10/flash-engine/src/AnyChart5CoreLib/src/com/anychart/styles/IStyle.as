package com.anychart.styles {
	public interface IStyle {
		function addDynamicColor(colorIndex:uint):void;
		function addDynamicGradient(gradientIndex:uint):void;
		function addDynamicHatchType(hatchIndex:uint):void
	}
}