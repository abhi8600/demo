package com.anychart.formatters.terms
{
	public class TermTokenNoScale extends TermToken implements ITerm
	{
		public function TermTokenNoScale(tokenName:String, parentCall:ITerm) {
			super(tokenName, parentCall);
		}
		
		public function getValue(getTokenFunction:Function, isDateTimeToken:Function):String {
			return parentCall.getValue(getTokenFunction, isDateTimeToken) + execFormating(getTokenFunction(tokenName), isDateTimeToken(tokenName));
		}

		private function execFormating(value:String, isDateTime:Boolean):String {
			if (value == "") return "";
			var numValue:Number = Number(value);
			if (isDateTime) {
				return formatString(formatDateTime(numValue));
			}
			
			if (isNaN(numValue)) return this.formatString(value);
			var isNegative:Boolean = numValue < 0;
			if (numValue < 0) numValue = -numValue;
			var res:String = formatNumber(numValue, isNegative);
			return formatString(res);
		}

	}
}