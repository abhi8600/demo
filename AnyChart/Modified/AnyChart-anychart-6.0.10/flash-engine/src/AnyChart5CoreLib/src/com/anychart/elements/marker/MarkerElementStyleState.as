package com.anychart.elements.marker {
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.utils.StringUtils;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.geom.Rectangle;
	
	public final class MarkerElementStyleState extends BaseElementStyleState {
		
		public var markerType:uint = MarkerType.CIRCLE;
		public var isDynamicType:Boolean = false;
		public var size:Number = 10;
		public var size_width:Number = 0;
		public var size_height:Number = 0;
		public var imageUrl:String = null;
		
		public var background:BackgroundBase;
		
		public function MarkerElementStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@marker_type != undefined) {
				if (data.marker[0] != null && data.marker[0].@type != undefined)
					data.marker[0].@type = StringUtils.replace(String(data.marker[0].@type).toLowerCase(),"%markertype",data.@marker_type);
			}
			
			if (data.marker[0] != null) {
				var marker:XML = data.marker[0];
				if (marker.@anchor != undefined) this.anchor = SerializerBase.getAnchor(marker.@anchor);
				if (marker.@size != undefined) this.size = SerializerBase.getNumber(marker.@size);
				if (marker.@size_width != undefined) this.size_width = SerializerBase.getNumber(marker.@size_width);
				if (marker.@size_height != undefined) this.size_height = SerializerBase.getNumber(marker.@size_height);
				if (marker.@padding != undefined) this.padding = SerializerBase.getNumber(marker.@padding);
				if (marker.@h_align != undefined) this.hAlign = SerializerBase.getHorizontalAlign(marker.@h_align);
				if (marker.@v_align != undefined) this.vAlign = SerializerBase.getVerticalAlign(marker.@v_align);
				if (marker.@type != undefined) {
					if (SerializerBase.isDynamicMarkerType(marker.@type)) {
						this.isDynamicType = true;
					}else {
						this.isDynamicType = false;
						this.markerType = SerializerBase.getMarkerType(marker.@type);
					}
				}
				if (marker.@image_url != undefined) this.imageUrl = SerializerBase.getString(marker.@image_url);
				if (this.imageUrl != null && (this.isDynamicType || this.markerType == MarkerType.IMAGE))
					this.imageUrl = resources.addImage(this.imageUrl);
			}
			if (this.anchor == Anchor.X_AXIS) {
				this.hAlign = HorizontalAlign.CENTER;
				this.vAlign = VerticalAlign.BOTTOM;
			}
			this.background = new BackgroundBase();
			this.background.deserialize(data,resources,this.style);
			if (this.size_height==0 && this.size_width==0) this.size_width=this.size_height=this.size;
			this.bounds = new Rectangle(0,0,this.size_width, this.size_height);
			return data;
		}

	}
}