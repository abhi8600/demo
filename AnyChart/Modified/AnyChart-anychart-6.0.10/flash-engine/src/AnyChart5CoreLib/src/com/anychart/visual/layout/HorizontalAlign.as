package com.anychart.visual.layout {
	
	public final class HorizontalAlign {
		public static const LEFT:uint = 0;
		public static const CENTER:uint = 1;
		public static const RIGHT:uint = 2;
	}
}