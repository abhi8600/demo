package com.anychart.elements {
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.controls.ToolTip;
	
	public class BaseElement {
		public var enabled:Boolean = false;
		public var style:Style;
		
		public function getStyleStateClass():Class { throw new Error("Abstract"); }
		protected function getStyleNodeName():String { throw new Error("Abstract"); }
		protected function getContainer(plot:IMainElementsContainer):Sprite { throw new Error("Abstract"); }
		
		public function createCopy(element:BaseElement = null):BaseElement {
			if (element == null)
				element = new BaseElement();
			element.enabled = this.enabled;
			element.style = this.style;
			return element;
		}
		
		//-------------------------------------------------------
		//	Deserialization
		//-------------------------------------------------------
		
		public function needCreateElementForContainer(elementNode:XML):Boolean {
			if (elementNode == null) return false;
			if (elementNode.@style != undefined) return true;
			return false;
		}
		
		public function deserializeElementForContainer(data:XML, 
													   container:IElementContainer, 
													   stylesList:XML, 
													   elementIndex:uint, 
													   resources:ResourcesLoader):void {
			if (this.needCreateElementForContainer(data)) {
				//unique: can deserialize to itself
				this.deserializeElement(data, stylesList, resources);
			}else {
				if (data.@enabled != undefined) {
					if (container.elementsInfo == null)
						container.elementsInfo = {};
					if (container.elementsInfo[elementIndex] == null)
						container.elementsInfo[elementIndex] = {};
					container.elementsInfo[elementIndex].enabled = SerializerBase.getBoolean(data.@enabled);
				}
			}
		}
		
		public function deserializeElement(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@style != undefined)
				this.deserializeStyleFromApplicator(stylesList, data.@style, resources);
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
		}
		
		//-------------------------------------------------------
		//	Style
		//-------------------------------------------------------
		
		protected final function deserializeStyleFromNode(stylesList:XML,
														  data:XML,
														  resources:ResourcesLoader):void {
			var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, this.getStyleNodeName(), data, data.@style);
			this.style = new Style(this.getStyleStateClass());
			this.style.deserialize(actualStyleXML,resources);
	    }
		
		protected final function deserializeStyleFromApplicator(stylesList:XML, 
												  	   			styleName:String,
												  	   			resources:ResourcesLoader,
												  	   			localStylesList:StylesList = null):void {
			if (localStylesList == null)
				localStylesList = StylesList.getInstance();

			var styleNodeName:String = this.getStyleNodeName();
			var cashedStyle:Style = localStylesList.getStyle(styleNodeName, styleName);
			if (cashedStyle != null) {
				this.style = cashedStyle;
			}else {
				var actualStyleXML:XML = StylesList.getStyleXMLByApplicator(stylesList, styleNodeName, styleName, localStylesList);
				if (actualStyleXML == null) {
					this.style = localStylesList.getStyle(styleNodeName, "anychart_default"); 
				}else {
					this.style = new Style(this.getStyleStateClass());
					this.style.deserialize(actualStyleXML, resources);
					localStylesList.addStyle(styleNodeName, styleName, style);
				}
			}
		}
		
		//-------------------------------------------------------
		//	Drawing
		//-------------------------------------------------------
		
		protected function createSprite(container:IElementContainer, index:uint):Sprite {
			var sp:Sprite = new Sprite();
			this.getContainer(container.getMainContainer()).addChild(sp);
			return sp;
		}
		
		public function initialize(container:IElementContainer, elementIndex:uint):Sprite {
			if (container.elementsInfo != null &&
				container.elementsInfo[elementIndex] != null &&
				container.elementsInfo[elementIndex].enabled != null) {
				if (!container.elementsInfo[elementIndex].enabled) 
					return null;
			}else if (!this.enabled) 
				return null;
			if (container.hasPersonalContainer) {
				return this.createSprite(container, elementIndex);
			}
			return this.getContainer(container.getMainContainer());
		}
		
		public final function draw(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			if (container.hasPersonalContainer) 
				sprite.graphics.clear();
			if (container.elementsInfo != null &&
				container.elementsInfo[elementIndex] != null &&
				container.elementsInfo[elementIndex].enabled != null) {
				if (!container.elementsInfo[elementIndex].enabled)
					return;
			}else if (!this.enabled) {
				return;
			}
			if (!BaseElementStyleState(state).enabled)
				return;
			var pos:Point = this.getPosition(container, BaseElementStyleState(state), sprite, elementIndex);
			if (pos == null) return;
			
			if (container.hasPersonalContainer) {
				sprite.graphics.clear();
				this.setSpritePosition(container, sprite, pos);
			}else {
				if (BaseElementStyleState(state).bounds == null)
					BaseElementStyleState(state).bounds = new Rectangle();
				BaseElementStyleState(state).bounds.x = pos.x;
				BaseElementStyleState(state).bounds.y = pos.y;
			}
			this.execDrawing(container, state, sprite, elementIndex);
		}
		
		protected function setSpritePosition(container:IElementContainer, sprite:Sprite, pos:Point):void {
			sprite.x = pos.x;
			sprite.y = pos.y;
		}
		
		public function resize(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			if (container.elementsInfo != null &&
				container.elementsInfo[elementIndex] != null &&
				container.elementsInfo[elementIndex].enabled != null) {
				if (!container.elementsInfo[elementIndex].enabled)
					return;
			}else if (!this.enabled) {
				return;
			}
			if (!BaseElementStyleState(state).enabled)
				return;
			var pos:Point = this.getPosition(container, BaseElementStyleState(state), sprite, elementIndex);
			if (container.hasPersonalContainer) {
				sprite.x = pos.x;
				sprite.y = pos.y;
			}else {
				BaseElementStyleState(state).bounds.x = pos.x;
				BaseElementStyleState(state).bounds.y = pos.y;
				this.execDrawing(container, state, sprite, elementIndex);
			}
		}
		
		protected function getPosition(container:IElementContainer, state:BaseElementStyleState, sprite:Sprite, elementIndex:uint):Point {
			return container.getElementPosition(state, sprite, this.getBounds(container, state, elementIndex));			
		}
		
		protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
		}
		
		public function getBounds(container:IElementContainer, state:StyleState, elementIndex:uint):Rectangle {
			return BaseElementStyleState(state).bounds;
		}
	}
}