package com.anychart.dateTime {
	import com.anychart.locale.DateTimeLocale;
	
	public final class DateTimeParser {
		
		public static var DEFAULT_FORMAT:String = "%yyyy/%MM/%dd %hh:%mm:%ss";
		
		private var aspectsList:Array;
		private var consts:Array;
		private var locale:DateTimeLocale;
		
		public function initialize(mask:String, locale:DateTimeLocale):void {
			this.aspectsList = [];
			this.consts = [];
			this.locale = locale;
			this.parseMask(mask);
		}
		
		//-------------------------------------------------------------------------
		//		DATETIME FORMAT PARSING
		//-------------------------------------------------------------------------
		
		private function parseMask(mask:String):void {
			DEFAULT_FORMAT = mask;
			var lexems:Array = mask.split('%');
			this.consts = [];
			//if (lexems[0] != "")
			this.consts.push(lexems[0].length);
			var lexemsLn:uint = lexems.length;
			for (var i:uint = 1; i < lexemsLn; i++) {
				var leter:String = String(lexems[i]).charAt(0);
				var newPattern:String = leter;
				var j:uint;
				var ln:uint = String(lexems[i]).length;
				for (j = 1; j < ln; j++) {
					if (String(lexems[i]).charAt(j) != leter) break;
					else newPattern += leter;
				}
				if (addAspect(newPattern)) {
					this.consts.push(String(lexems[i]).substring(j).length);
				} else {
					this.consts[this.consts.length - 1] += String(lexems[i]).length;
				}
			}
		}
		
		private function addAspect(pattern:String):Boolean {
			switch (pattern) {
				/*
				u - unix timestamp
				 */
				case "u": this.aspectsList.push(this.aspectu); return true;
				
				/*
				d - day of the month without leading zero
				dd - day of the month with leading zero
				ddd - short day name (day of the week)
				dddd - long day name (day of the week)
				 */
				case "d": this.aspectsList.push(this.aspectd); return true;
				case "dd": this.aspectsList.push(this.aspectdd); return true;
				case "ddd": this.aspectsList.push(this.aspectddd); return true;
				case "dddd": this.aspectsList.push(this.aspectdddd); return true;
				
				/* 
				M - month without leading zero
				MM - month with leading zero
				MMM - month as its abbr
				MMMM - month as its name
				*/
				case "M": this.aspectsList.push(this.aspectM); return true;
				case "MM": this.aspectsList.push(this.aspectMM); return true;
				case "MMM": this.aspectsList.push(this.aspectMMM); return true;
				case "MMMM": this.aspectsList.push(this.aspectMMMM); return true;
				
				/*
				y - 2-digits without leading zero
				yy - 2-digits with leading zero
				yyyy - 4-digits year
				*/
				case "y": this.aspectsList.push(this.aspecty); return true;
				case "yy": this.aspectsList.push(this.aspectyy); return true;
				case "yyyy": this.aspectsList.push(this.aspectyyyy); return true;
				
				/*
				h - hours without leading zero (12 hour clock)
				hh - hours with leading zero (12 hour clock)
				H - hours without leading zero (24 hour clock)
				HH - hours with leading zero (24 hour clock)
				*/
				case "h": this.aspectsList.push(this.aspecth); return true;
				case "hh": this.aspectsList.push(this.aspecthh); return true;
				case "H": this.aspectsList.push(this.aspectH); return true;
				case "HH": this.aspectsList.push(this.aspectHH); return true;
				
				/*
				m - minutes without leading zero
				mm - minutes with leading zero
				*/
				case "m": this.aspectsList.push(this.aspectm); return true;
				case "mm": this.aspectsList.push(this.aspectmm); return true;
				
				/*		
				s - seconds without leading zero
				ss - seconds with leading zero
				*/
				case "s": this.aspectsList.push(this.aspects); return true;
				case "ss": this.aspectsList.push(this.aspectss); return true;
				
				/*
				t - one character time-marker string (A,P)
				tt - multicharacter time-marker string (AM,PM)
				*/
				case "t": this.aspectsList.push(this.aspectt); return true;
				case "tt": this.aspectsList.push(this.aspecttt); return true;
			}
			return false;
		}
		
		//-------------------------------------------------------------------------
		//		DATE PARSING
		//-------------------------------------------------------------------------
		
		private var year:int;
		/* 0 - 11 */
		private var month:int;
		/* 1 - 31 */
		private var day:int;
		/* 0 - 23 */
		private var hour:int;
		/* 0 - 59 */
		private var minute:int;
		/* 0 - 59 */
		private var second:Number;
		
		private var unixTime:Number;
		private var dayOfWeek:int;
		private var hour12:int;
		
		private var position:int;
		private var stringDate:String;
		private var isAm:Boolean;
		
		public function getDate(stringDate:String):Date {
			this.clear();
			
			this.stringDate = stringDate;
			
			this.parseDate();
			return this.createDate();
		}
		
		public function getDateAsNumber(stringDate:String):Number {
			return this.getDate(stringDate).getTime();
		}
		
		private function parseDate():void {
			var length:uint = stringDate.length;
			this.position = this.consts[0];
			var ln:uint = this.aspectsList.length;
			for (var i:uint = 0; i < ln; i++) {
				this.aspectsList[i]();
				position += consts[i+1];
				if (position >= length) break;
			}
		}
		
		private function createDate():Date {
			if (!isNaN(this.unixTime))
				return new Date(this.unixTime*1000);
			
			if (this.hour == -1 && this.hour12 != -1){
				this.hour12 = int(this.hour12 % 12);
				this.hour = this.isAm ? this.hour12 : (12+this.hour12);
			}
			var res:Date = new Date(Date.UTC(
				year == -1 ? 1970 : year,
				month == -1 ? 0 : month,
				day == -1 ? 1 : day,
				hour == -1 ? 0 : hour,
				minute == -1 ? 0 : minute,
				isNaN(second) ? 0 : second));
			if (this.dayOfWeek != -1 && this.day == -1){
			 	var tmp:Number = this.dayOfWeek - res.day;
				res.setUTCDate(res.date + tmp + (tmp < 0 ? 7 : 0));
			}
			return res;
		}
		
		private function clear():void {
			this.year = -1;
			this.month = -1;
			this.day = -1;
			this.dayOfWeek = -1;
			this.hour = -1;
			this.hour12 = -1;
			this.minute = -1;
			this.second = NaN;
			this.unixTime = NaN;
			this.isAm = true;
		}
		
		//-------------------------------------------------------------------------
		//		ASPECTS
		//-------------------------------------------------------------------------
		
		/**
		 * %u
		 * Parse unix time 
		 */
		private function aspectu():void {
			var parsed:String = "";
			while (this.position < this.stringDate.length && 
				  !isNaN(Number(this.stringDate.charAt(this.position)))) {
				parsed += this.stringDate.charAt(position);
				this.position++;
			} 
			this.unixTime = Number(parsed);
		}
		
		/**
		 * %d
		 * Parse day of the month without leading zero
		 */
		private function aspectd():void {
			this.day = this.getLongDigitalItem(2);
		}
		
		/**
		 * %dd
		 * Parse day of the month with leading zero
		 */
		private function aspectdd():void {
			this.day = this.getDigitalItem(2);
		}
		
		/**
		 * %ddd
		 * Parse short day name (day of the week)
		 */
		private function aspectddd():void {
			var index:int = this.getArrayItem(this.locale.shortWeekDayNames);
			this.dayOfWeek = index;
			this.position += this.locale.shortWeekDayNames[index].length;
		}
		
		/**
		 * %dddd
		 * Parse long day name (day of the week)
		 */
		private function aspectdddd():void {
			var index:int = this.getArrayItem(this.locale.weekDayNames);
			this.dayOfWeek = index;
			this.position += this.locale.weekDayNames[index].length;
		}
		
		/**
		 * %M 
		 * Parse month without leading zero
		 */
		private function aspectM():void {
			this.month = this.getLongDigitalItem(2)-1;
		}
		
		/**
		 * %MM
		 * Parse month with leading zero
		 */
		private function aspectMM():void {
			this.month = this.getDigitalItem(2)-1;
		}
		
		/**
		 * %MMM 
		 * Parse month as its abbr
		 */
		private function aspectMMM():void {
			var index:int = this.getArrayItem(this.locale.shortMonthNames);
			this.month = index;
			this.position += this.locale.shortMonthNames[index].length;
		}
		
		/**
		 * %MMMM 
		 * Parse month as its name
		 */
		private function aspectMMMM():void {
			var index:int = this.getArrayItem(this.locale.monthNames);
			this.month = index;
			this.position += this.locale.monthNames[index].length;
		}
		
		/**
		 * %y 
		 * Parse 2-digits year without leading zero
		 */
		private function aspecty():void {
			this.year = 2000 + this.getLongDigitalItem(2);
		}
		
		/**
		 * %yy 
		 * Parse 2-digits year with leading zero
		 */
		private function aspectyy():void {
			this.year = 2000 + this.getDigitalItem(2);
		}
		
		/**
		 * %yyyy 
		 * Parse 4-digits year
		 */
		private function aspectyyyy():void {
			this.year = this.getDigitalItem(4);
		}
		
		/**
		 * %h 
		 * Parse hours without leading zero (12 hour clock)
		 */
		private function aspecth():void {
			this.hour12 = this.getLongDigitalItem(2);
		}
		
		/**
		 * %hh 
		 * Parse hours with leading zero (12 hour clock)
		 */
		private function aspecthh():void {
			this.hour12 = this.getDigitalItem(2);
		}
		
		/**
		 * %H
		 * Parse hours without leading zero (24 hour clock)
		 */
		private function aspectH():void {
			this.hour = this.getLongDigitalItem(2);
		}
		
		/**
		 * %HH 
		 * Parse hours with leading zero (24 hour clock)
		 */
		private function aspectHH():void {
			this.hour = this.getDigitalItem(2);
		}
		
		/**
		 * %m 
		 * Parse minutes without leading zero
		 */
		private function aspectm():void {
			this.minute = this.getLongDigitalItem(2);
		}
		
		/**
		 * %mm 
		 * Parse minutes with leading zero
		 */
		private function aspectmm():void {
			this.minute = this.getDigitalItem(2);
		}
		
		/**
		 * %s 
		 * Parse seconds without leading zero
		 */
		private function aspects():void {
			this.second = this.getLongDigitalItem(2);
		}
		
		/**
		 * %ss 
		 * Parse seconds with leading zero
		 */
		private function aspectss():void {
			this.second = this.getDigitalItem(2);
		}
		
		/**
		 * %t 
		 * Parse one character time-marker string (A,P)
		 */
		private function aspectt():void {
			this.checkAMPM(this.locale.shortAmString, this.locale.shortPmString);
		}
		
		/**
		 * %tt 
		 * Parse multicharacter time-marker string (AM,PM)
		 */
		private function aspecttt():void {
			this.checkAMPM(this.locale.amString, this.locale.pmString);
		}
		
		//-------------------------------------------------------------------------
		//		UTILITES
		//-------------------------------------------------------------------------
		
		private function getArrayItem(arr:Array):int {
			for (var i:uint = 0;i<arr.length;i++) {
				var item:String = String(arr[i]).toLowerCase();
				if ((this.position + item.length) <= this.stringDate.length && 
					this.stringDate.substr(this.position, item.length).toLowerCase() == item)
					return i;
			}
			return -1;
		}
		
		private function getLongDigitalItem(maxLen:uint):int {
			var res:String = this.stringDate.charAt(this.position);
			this.position++;
			while (res.length < maxLen && 
				   this.position < this.stringDate.length &&
				   !isNaN(Number(this.stringDate.charAt(this.position)))) {
				res += this.stringDate.charAt(this.position);
				this.position++;
		    }
			return int(Number(res));	   
		}
		
		private function getDigitalItem(len:uint):int {
			var res:int = int(Number(this.stringDate.substr(this.position,len)));
			this.position += len;
			return res;
		}
		
		private function checkAMPM(amString:String, pmString:String):void {
			var amString:String = amString.toLowerCase();
			var pmString:String = pmString.toLowerCase();
			
			if (this.stringDate.substr(this.position, amString.length).toLowerCase() == amString) {
				this.isAm = true;
				this.position += amString.length;
			}else if (this.stringDate.substr(this.position, pmString.length).toLowerCase() == pmString) {
				this.isAm = false;
				this.position += pmString.length;
			}
		}
	}
}