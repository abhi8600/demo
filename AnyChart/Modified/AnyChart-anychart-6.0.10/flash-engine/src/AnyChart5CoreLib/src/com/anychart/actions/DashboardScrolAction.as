package com.anychart.actions{
	
	import com.anychart.IAnyChart;
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.formatters.TextFormater;
	import com.anychart.serialization.SerializerBase;
	
	import flash.geom.Point;

	public final class DashboardScrolAction extends ScrollAction{
		
		protected var id:String;
		protected var isDinamicId:Boolean;
		protected var iDFormater:TextFormater;
		
		public function DashboardScrolAction(chart:IAnyChart){
			super(chart);
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.isValidXML = data.@view_id != undefined;
			
			if (!this.isValidXML) return;
			
			if (data.@view_id != undefined) {
				this.id = SerializerBase.getString(data.@view_id);
				this.isDinamicId = FormatsParser.isDynamic(this.id);
				if (this.isDinamicId) this.iDFormater = FormatsParser.parse(this.id);
			}
		}
		override public function execute(formatter:IFormatable):Boolean {
			var pt:Point = this.doExecute(formatter);
			var viewId:String = String(isDinamicId ? (this.iDFormater.getValue(formatter.getTokenValue,formatter.isDateTimeToken)):(this.id))
			this.chart.viewScrollTo(viewId,pt.x.toString(),pt.y.toString());
			return false;
		}
		
	}
}