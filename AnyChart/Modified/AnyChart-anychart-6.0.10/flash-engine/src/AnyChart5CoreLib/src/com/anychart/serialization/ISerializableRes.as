package com.anychart.serialization {
	import com.anychart.resources.ResourcesLoader;
	
	public interface ISerializableRes {
		function deserialize(data:XML, resources:ResourcesLoader):void;
	}
}