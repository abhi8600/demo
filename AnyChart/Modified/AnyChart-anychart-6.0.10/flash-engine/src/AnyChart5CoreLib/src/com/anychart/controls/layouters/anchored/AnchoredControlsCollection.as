package com.anychart.controls.layouters.anchored {
	import com.anychart.controls.Control;
	import com.anychart.controls.layout.ControlLayout;
	import com.anychart.controls.layouters.ControlsCollection;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	internal class AnchoredControlsCollection extends ControlsCollection {
		protected var controls:Array;
		
		public function AnchoredControlsCollection() {
			super();
			this.controls = [];
		}
		
		override public function addControl(control:Control):void {
			this.controls.push(control);
		}
		
		override public function finalizeLayout(plotRect:Rectangle, chartRect:Rectangle):Boolean {
			for each (var control:Control in this.controls) {
				control.layout.maxWidth = chartRect.width;
				control.layout.maxHeight = chartRect.height;
				control.setBounds(chartRect, plotRect);
				this.setControlPosition(control, control.layout.alignByDataPlot ? plotRect : chartRect);
			}
			return true;
		}
		
		protected function setControlPosition(control:Control, targetRect:Rectangle):void {
			
			var layout:ControlLayout = control.layout;
			var hAlign:int;
			var vAlign:int;
			var pos:Point = new Point(targetRect.x, targetRect.y);
			switch (layout.anchor) {
				case Anchor.CENTER:
					hAlign = HorizontalAlign.CENTER;
					vAlign = VerticalAlign.CENTER;
					pos.x += targetRect.width/2;
					pos.y += targetRect.height/2;
					break;
				case Anchor.CENTER_BOTTOM:
					hAlign = HorizontalAlign.CENTER;
					vAlign = VerticalAlign.TOP;
					pos.x += targetRect.width/2;
					pos.y += targetRect.height;
					break;
				case Anchor.CENTER_LEFT:
					hAlign = HorizontalAlign.RIGHT;
					vAlign = VerticalAlign.CENTER;
					pos.y += targetRect.height/2;
					break;
				case Anchor.CENTER_RIGHT:
					hAlign = HorizontalAlign.LEFT;
					vAlign = VerticalAlign.CENTER;
					pos.x += targetRect.width;
					pos.y += targetRect.height/2;
					break;
				case Anchor.CENTER_TOP:
					hAlign = HorizontalAlign.CENTER;
					vAlign = VerticalAlign.BOTTOM;
					pos.x += targetRect.width/2;
					break;
				case Anchor.LEFT_BOTTOM:
					hAlign = HorizontalAlign.RIGHT;
					vAlign = VerticalAlign.TOP;
					pos.y += targetRect.height;
					break;
				case Anchor.LEFT_TOP:
					hAlign = HorizontalAlign.RIGHT;
					vAlign = VerticalAlign.BOTTOM;
					break;
				case Anchor.RIGHT_BOTTOM:
					hAlign = HorizontalAlign.LEFT;
					vAlign = VerticalAlign.TOP;
					pos.x += targetRect.width;
					pos.y += targetRect.height;
					break;
				case Anchor.RIGHT_TOP:
					hAlign = HorizontalAlign.LEFT;
					vAlign = VerticalAlign.BOTTOM;
					pos.x += targetRect.width;
					break;
			}
			
			hAlign = (layout.hAlign == -1) ? hAlign : layout.hAlign;
			vAlign = (layout.vAlign == -1) ? vAlign : layout.vAlign;
			
			var w:Number = control.bounds.width;
			var h:Number = control.bounds.height;
			
			switch (hAlign) {
				case HorizontalAlign.LEFT:
					pos.x -= w + layout.pixHorizontalPaddding;
					break;
				case HorizontalAlign.CENTER:
					pos.x -= w/2;
					break;
				case HorizontalAlign.RIGHT:
					pos.x += layout.pixHorizontalPaddding;
					break;
			}
			
			switch (vAlign) {
				case VerticalAlign.TOP:
					pos.y -= h + layout.pixVerticalPadding;
					break;
				case VerticalAlign.CENTER:
					pos.y -= h/2;
					break;
				case VerticalAlign.BOTTOM:
					pos.y += layout.pixVerticalPadding;
					break;
			}
			
			control.bounds.x = pos.x;
			control.bounds.y = pos.y;
		}
	}
}