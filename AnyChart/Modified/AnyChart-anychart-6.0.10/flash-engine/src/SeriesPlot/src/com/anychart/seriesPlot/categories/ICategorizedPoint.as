package com.anychart.seriesPlot.categories {
	public interface ICategorizedPoint {
		function checkCategory(tokensMap:Object):void;
	}
}