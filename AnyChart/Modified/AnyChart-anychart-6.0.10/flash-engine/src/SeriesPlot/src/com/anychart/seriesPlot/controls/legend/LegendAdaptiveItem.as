package com.anychart.seriesPlot.controls.legend {
	import com.anychart.controls.legend.ILegendItem;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.styles.states.StyleState;
	
	public final class LegendAdaptiveItem implements ILegendItem {
		private var _color:int;
		public function get color():int { return this._color; }
		public function set color(value:int):void { this._color = value; }
		
		private var _hatchType:uint;
		public function get hatchType():uint { return this._hatchType; }
		
		private var _tag:ILegendTag;
		public function set tag(value:ILegendTag):void { this._tag = value; }
		public function get tag():ILegendTag { return this._tag; }
		
		private var _markerType:int;
		public function get markerType():int { return this._markerType; }
		
		private var _iconCanDrawMarker:Boolean;
		public function get iconCanDrawMarker():Boolean { return this._iconCanDrawMarker; }
		
		public var seriesType:int;
		public var styleState:StyleState;
		
		public function LegendAdaptiveItem(color:int, hatchType:uint, markerType:int, iconCanDrawMarker:Boolean, tag:ILegendTag) {
			this._color = color;
			this._hatchType = hatchType;
			this._markerType = markerType;
			this._iconCanDrawMarker = iconCanDrawMarker;
			this._tag = tag;
		}
	}
}