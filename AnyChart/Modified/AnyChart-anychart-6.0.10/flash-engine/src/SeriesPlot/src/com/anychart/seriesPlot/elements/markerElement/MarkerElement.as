package com.anychart.seriesPlot.elements.markerElement {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.marker.ItemMarkerElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	
	public class MarkerElement extends ItemMarkerElement {
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new MarkerElement();
			super.createCopy(element);
			return element;
		}
		
		public function needCreateElementForSeries(elementNode:XML):Boolean {
			if (elementNode == null) return false;
			if (elementNode.@style != undefined) return true;
			if (elementNode.@enabled != undefined && SerializerBase.getBoolean(elementNode.@enabled) != this.enabled) return true;
			if (elementNode.@type != undefined) return true;
			return super.needCreateElementForContainer(elementNode); 
		}
		
		public function needCreateElementForPoint(elementNode:XML):Boolean {
			return super.needCreateElementForContainer(elementNode);
		}
		
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			delete data.@enabled;
			this.deserializeStyleFromNode(stylesList, data, resources);
		}
		
		public function deserializeGlobalAsSeries(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			this.enabled = true;
			this.deserializeGlobalStyle(data, stylesList, resources);
		}
		
		public function deserializeSeries(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			super.deserializeElement(data, stylesList, resources);
		}
		
		public function deserializePoint(data:XML, point:BasePoint, stylesList:XML, elementIndex:uint, resources:ResourcesLoader):void {
			super.deserializeElementForContainer(data, point, stylesList, elementIndex, resources);
		}
		
		private function deserializeGlobalStyle(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			var styleNodeName:String = this.getStyleNodeName();
			
			if (data.@style != undefined || data[styleNodeName][0] != null) {
				if (data.@style != undefined && data[styleNodeName][0] == null) {
					this.deserializeStyleFromApplicator(stylesList, data.@style, resources);
				}else {
					var parentStyleName:*;
					if (data.@style != undefined)
						parentStyleName = data.@style;
					if (data[styleNodeName][0] != null && data[styleNodeName][0].@parent != undefined) {
						parentStyleName = data[styleNodeName][0].@parent;
					}
					
					var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, styleNodeName, data[styleNodeName][0], parentStyleName);
					actualStyleXML.@name = parentStyleName;
					this.style = new Style(this.getStyleStateClass());
					this.style.deserialize(actualStyleXML, resources);
				}
			}else {
				var cashedStyle:Style = StylesList.getStyle(styleNodeName, "anychart_default");
				if (cashedStyle != null) {
					this.style = cashedStyle;
				}else {
					this.style = new Style(this.getStyleStateClass());
					this.style.deserialize(stylesList[styleNodeName].(@name == "anychart_default")[0],resources);
					StylesList.addStyle(styleNodeName, "anychart_default", this.style);
				}
			}
		}
	}
}