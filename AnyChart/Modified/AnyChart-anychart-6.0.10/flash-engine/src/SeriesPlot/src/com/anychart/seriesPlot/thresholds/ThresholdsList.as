package com.anychart.seriesPlot.thresholds {
	import com.anychart.palettes.ColorPalette;
	import com.anychart.serialization.SerializerBase;
	
	public final class ThresholdsList {
		
		private var parsed:Object;
		private var list:XMLList;
		private var autoThresholds:Array;
		private var autoThresholdsMap:Object;
		private var parsedList:Array;
		
		public function ThresholdsList(list:XMLList) {
			this.parsed = {};
			this.autoThresholds = [];
			this.parsedList = [];
			this.autoThresholdsMap = {};
			this.list = list;
		}
		
		public function getParsedList():Array {
			return this.parsedList;
		}
		
		public function reset():void {
			for each (var tr:IThreshold in this.parsedList) {
				tr.reset();
			}
		}
		
		public function destroy():void {
			this.parsed = null;
			this.list = null;
			this.autoThresholds = null;
		}
		
		public function getAutoThreshold(name:String):AutomaticThreshold {
			return this.autoThresholdsMap[name];
		}
		
		public function getThreshold(palettes:XML,name:String):IThreshold {
			name = name.toLowerCase();
			if (this.list == null) return null;
			if (this.parsed[name] != null)
				return this.parsed[name];
			var data:XML = this.list.(@name == name)[0];
			if (data == null) return null;
			var tr:IThreshold = this.create(data);
			if (tr == null) return null;
			tr.deserialize(data);
			if (tr.isAutomatic()) {
				var palette:ColorPalette = new ColorPalette();
				var paletteName:String = (data.@palette != undefined && palettes.palette.(@name == String(data.@palette).toLowerCase()).length() > 0) ? String(data.@palette).toLowerCase() : "default";
				palette.deserialize(palettes.palette.(@name == paletteName)[0]);
				IAutomaticThreshold(tr).palette = palette.checkAuto(IAutomaticThreshold(tr).getRangeCount(), true);
				this.autoThresholds.push(tr);
				this.autoThresholdsMap[name] = tr;
			}
			this.parsedList.push(tr);
			this.parsed[name] = tr;
			return tr;
		}
		
		public function initialize():void {
			for each (var autoTr:IAutomaticThreshold in this.autoThresholds) {
				autoTr.initialize();
			}
		}
		
		private function create(data:XML):IThreshold {
			var threshold:Threshold;
			if (data.@type == undefined) {
				threshold = new CustomThreshold();
			}else {
				switch (String(data.@type).toLowerCase()) {
					default:
					case "custom":
						threshold = new CustomThreshold();
						break;
					case "equalinterval":
					case "equalsteps":
						threshold = new EqualIntervalThreshold();
						break;
					case "quantiles":
					case "equaldistribution":
						threshold = new EqualDistributionThreshold();
						break;
					case "optimal":
					case "absolutedeviation":
						threshold = new OptimalThreshold();
						break;
				}
			}
			return threshold;
		} 
	}
}