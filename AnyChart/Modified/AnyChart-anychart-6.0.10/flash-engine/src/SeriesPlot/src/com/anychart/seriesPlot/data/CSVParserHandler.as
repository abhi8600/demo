package com.anychart.seriesPlot.data {
	import com.anychart.animation.Animation;
	import com.anychart.palettes.BasePalette;
	import com.anychart.palettes.ColorPalette;
	import com.anychart.serialization.SerializerBase;
	
	public final class CSVParserHandler{
		
		public var series:BaseSeries;
		public var seriesColorPalette:ColorPalette;
		public var seriesMarkerPalette:BasePalette;
		public var seriesHatchPalette:BasePalette;
		public var seriesAnimation:Animation;
		private var datasetName:String;
		private var datasetMapping:Array = [];
		
		public function init(series:BaseSeries, seriesColorPalette:ColorPalette,
				seriesMarkerPalette:BasePalette, seriesHatchPalette:BasePalette,
				seriesAnimation:Animation):void {
			this.series = series;
			this.seriesColorPalette = seriesColorPalette;
			this.seriesHatchPalette = seriesHatchPalette;
			this.seriesMarkerPalette = seriesMarkerPalette;
			this.seriesAnimation = seriesAnimation;
		}
		
		public function deserialize(xmlData:XML):String {
			if (xmlData == null)
				return "";
			if (xmlData.@data_set != undefined){
				this.datasetName = SerializerBase.getLString(xmlData.@data_set);
				if (this.datasetName == "")
					return this.datasetName;
			} else
				return "";
			var len:uint = xmlData.field.length();
			for (var i:uint = 0; i < len; i++){
				var xmlField:XML = xmlData.field[i];
				if (xmlField.@column == undefined || xmlField.@name == undefined)
					continue;
				var column:int = int(SerializerBase.getNumber(xmlField.@column));
				if (column < 0)
					continue;
				this.datasetMapping[column] = new CSVParserHandlerField(SerializerBase.getString(xmlField.@name), 
					xmlField.@is_custom_attribute != undefined ? SerializerBase.getBoolean(xmlField.@is_custom_attribute) : false);
			}
			return this.datasetName;
		}
		
		public function parse(row:Array):XML{
			var res:XML = <point/>;
			var mapLen:uint = this.datasetMapping.length;
			var rowLen:uint = row.length;
			var len:uint = Math.max(mapLen, rowLen);
			for (var i:uint = 0; i < len; i++){
				if (this.datasetMapping[i] == null || row[i] == null)
					continue;
				if (CSVParserHandlerField(this.datasetMapping[i]).isCustom) {
					if (res.attributes[0] == null) res.attributes[0] = <attributes />;
					res.attributes[0].attribute += <attribute name={CSVParserHandlerField(this.datasetMapping[i]).name}>{row[i]}</attribute>;
				}else
					res.@[CSVParserHandlerField(this.datasetMapping[i]).name] = row[i];
			}
			return res;
		}
	}
}