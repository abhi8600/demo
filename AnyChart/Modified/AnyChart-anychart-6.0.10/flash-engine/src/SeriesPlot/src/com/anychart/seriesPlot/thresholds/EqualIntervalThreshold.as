package com.anychart.seriesPlot.thresholds {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class EqualIntervalThreshold extends AutomaticThreshold {
		
		override public function initialize():void {
			
			super.initialize();
			
			if (this.minValue != this.maxValue) {
		        var tmp:Number = Math.pow(10, int(Math.log(Math.max(Math.abs(this.minValue), Math.abs(this.maxValue)))/Math.log(10)) - 1);
		        this.minValue = Math.floor(this.minValue/tmp);
		        var tmp1:int = int(this.minValue) * 10;
		        this.minValue *= tmp;
		        this.maxValue = Math.ceil(this.maxValue/tmp);
		        this.maxValue += Number((this.rangeCount - (((int(this.maxValue) * 10) - tmp1) % this.rangeCount))) / 10;
		        this.maxValue *= tmp;
			}
			
			this.range = (this.maxValue - this.minValue)/this.rangeCount;
			this.buildBoundsArray();
		}
		
		
		override public function checkBeforeDraw(point:BasePoint):void {
			var rangeIndex:int = this.findBounds(point);
			if (rangeIndex != -1) {
				this.intervals[rangeIndex].points.push(point);
				point.thresholdItem = this.intervals[rangeIndex];
				var val:Number = Number(point.getPointTokenValue(this.autoValue));
				var colorIndex:int = Math.floor((val-this.minValue)/this.range);
				this.setPointColor(point,  this.colorPalette.getItemAt(colorIndex));
			}
		}
		
		override public function checkAfterDeserialize(point:BasePoint):void {
			super.checkAfterDeserialize(point);
		}
	}
}