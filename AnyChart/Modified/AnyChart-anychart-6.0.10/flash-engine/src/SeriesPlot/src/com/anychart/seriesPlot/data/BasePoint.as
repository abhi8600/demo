package com.anychart.seriesPlot.data {
	import com.anychart.animation.AnimationManager;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.elements.AnimatableElement;
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.IMainElementsContainer;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.events.PointEvent;
	import com.anychart.formatters.IFormatableSerializableObject;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.drawing.DrawingMode;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	import com.anychart.seriesPlot.thresholds.IThresholdItemFormatable;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.layout.Anchor;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class BasePoint extends BaseDynamicElementsContainer implements IElementContainer,ILegendTag,IFormatableSerializableObject {
		
		public static const MARKER_INDEX:uint = 0;
		public static const LABEL_INDEX:uint = 1;
		protected static const TOOLTIP_INDEX:uint = 2;
		
		protected static const EXTRA_LABELS_FIRST_INDEX:uint = 10;
		protected static const EXTRA_MARKERS_FIRST_INDEX:uint = 500;
		protected static const EXTRA_TOOLTIPS_FIRST_INDEX:uint = 1000;
		
		public static function getTooltipContainerIndex(itemIndex:uint):uint {
			if (itemIndex == TOOLTIP_INDEX) return 0;
			if (itemIndex == 5) return 1;
			return itemIndex - EXTRA_TOOLTIPS_FIRST_INDEX+2;
		}
		
		public var index:uint;
		public var isSelected:Boolean;
		public var isMissing:Boolean;
		public var paletteIndex:uint;
		
		public var series:BaseSeries;
		public var thresholdItem:IThresholdItemFormatable;
		
		private var isOver:Boolean;
		
		public function get sortedOverlayValue():Number { return NaN; }
		
		public function BasePoint() {
			super();
			this.isSelected = false;
			this.isMissing = false;
			this.isOver = false;
			this.cashedTokens = {};
			this.canDrawToolip = true;
			this.stopPropagation = true;
		}
		
		public function destroy():void {
			if (this.hasPersonalContainer && this.container) {
				this.removeListeners(this.container);
			}
			if (this.listenersList) {
				for each (var obj:InteractiveObject in this.listenersList) {
					this.removeListeners(obj);
					if (obj.parent != null) obj.parent.removeChild(obj);
				}
				this.listenersList = null;
			}
			this.series = null;
		}
		
		public function getMainContainer():IMainElementsContainer { return this.plot; }
		
		//-----------------------------------------------------
		//				Deserialization
		//-----------------------------------------------------
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			this.paletteIndex = this.index;
			if (data.@selected != undefined) this.isSelected = SerializerBase.getBoolean(data.@selected);
		}
		
		//-----------------------------------------------------
		//					Formatting
		//-----------------------------------------------------
		
		protected var cashedTokens:Object;
		
		public function getPointTokenValue(token:String):* {
			if (token == "%Index") return this.index;
			if (token == "%Name") return this.getNameToken();
			if (token == "%Color") return "#"+this.color.toString(16);
			var attrToken:String = token.substr(1);
			if (this.customAttributes != null && this.customAttributes[attrToken] != null)
				return this.customAttributes[attrToken];
			return "";
		}
		
		protected function getNameToken():* { return this.name; }
		
		public function reset():void {}
		
		public function resetTokens():void {
			for (var elementInfoIndex:* in this.elementsInfo) {
				for (var key:String in this.elementsInfo[elementInfoIndex]) {
					if (key.indexOf("info") == 0) {
						this.elementsInfo[elementInfoIndex][key] = null;
						delete this.elementsInfo[elementInfoIndex][key]; 
					}
				}
			} 
		}
		
		public function getTokenValue(token:String):* {
			var attrToken:String = token.substr(1);
			if (this.customAttributes != null && this.customAttributes[attrToken] != null)
				return this.customAttributes[attrToken];
			var seriesCustomAttributes:Object = this.series.customAttributes;
			if (seriesCustomAttributes != null && seriesCustomAttributes[attrToken] != null)
				return seriesCustomAttributes[attrToken];
			var ptToken:String = this.getPointTokenValue(token);
			if (ptToken != "") return ptToken;
			if (token.indexOf('%Series') == 0)
				return this.series.getSeriesTokenValue(token);
			if (token.indexOf('%DataPlot') == 0)
				return this.plot.getTokenValue(token);
			if (this.threshold != null && token.indexOf('%Threshold') == 0)
				return this.threshold.getTokenValue(token);
			if (this.thresholdItem != null && this.thresholdItem.isCustomAttribute(token))
				return this.thresholdItem.getCustomAttribute(token);
			if (this.thresholdItem != null && this.thresholdItem.hasToken(token))
				return this.thresholdItem.getFormatedTokenValue(this,token);
			return "";
		}
		
		public function isDateTimeToken(token:String):Boolean { 
			var attrToken:String = token.substr(1);
			if (this.customAttributes != null && this.customAttributes[attrToken] != null)
				return false;
				
			var seriesCustomAttributes:Object = this.series.customAttributes;
			if (seriesCustomAttributes != null && seriesCustomAttributes[attrToken] != null)
				return false;
				
			if (token.indexOf('%Series') == 0)
				return this.series.isDateTimeSeriesToken(token);
				
			if (token.indexOf('%DataPlot') == 0)
				return this.plot.isDateTimeToken(token);
				
			if (this.threshold != null && token.indexOf('%Threshold') == 0)
				return this.threshold.isDateTimeToken(token);
				
			if (this.thresholdItem != null && this.thresholdItem.isCustomAttribute(token))
				return false;
				
			if (this.thresholdItem != null && this.thresholdItem.hasToken(token))
				return false;
				
			return this.isDateTimePointToken(token);
		}
		
		public function isDateTimePointToken(token:String):Boolean {
			return false;
		}
		
		//-----------------------------------------------------
		//					Elements
		//-----------------------------------------------------
		
		public function initializeElementsAnimation(manager:AnimationManager, index:uint, total:uint):void {
			this.initializeElementAnimation(this.label, this.labelSprite, manager, index, total);
			this.initializeElementAnimation(this.marker, this.markerSprite, manager, index, total);
			var i:uint;
			if (this.extraLabels) {
				for (i = 0;i<this.extraLabels.length;i++)
					this.initializeElementAnimation(this.extraLabels[i], this.extraLabelsSprites[i], manager, index, total);
			}
			if (this.extraMarkers) {
				for (i = 0;i<this.extraMarkers.length;i++)
					this.initializeElementAnimation(this.extraMarkers[i], this.extraMarkersSprites[i], manager, index, total);
			}
		}
		
		protected final function initializeElementAnimation(element:AnimatableElement, elementSprite:Sprite, manager:AnimationManager, index:uint, total:uint):void {
			if (element.animation && elementSprite) {
				if (element.animation.needGetAnimation())
					manager.registerDisplayObject(elementSprite, this.plot.scrollableContainer, element.animation.getAnimation(index, total));
				else
					manager.registerDisplayObject(elementSprite, this.plot.scrollableContainer, element.animation);
			}
		}
		
		protected function createElementsSprites():void {
			if (this.marker != null) this.markerSprite = this.marker.initialize(this, MARKER_INDEX);
			if (this.label != null) this.labelSprite = this.label.initialize(this, LABEL_INDEX);
			
			var i:uint;
			if (this.extraLabels) {
				var extraLabelsCnt:uint = this.extraLabels.length;
				this.extraLabelsSprites = [];
				for (i = 0;i<extraLabelsCnt;i++) {
					this.extraLabelsSprites.push(LabelElement(this.extraLabels[i]).initialize(this, EXTRA_LABELS_FIRST_INDEX+i));
				}
			}
			
			if (this.extraMarkers) {
				var extraMarkersCnt:uint = this.extraMarkers.length;
				this.extraMarkersSprites = [];
				for (i = 0;i<extraMarkersCnt;i++) {
					this.extraMarkersSprites.push(MarkerElement(this.extraMarkers[i]).initialize(this, EXTRA_MARKERS_FIRST_INDEX+i));
				}
			}
			
			if (this.extraTooltips)
				this.extraTooltipsSprites = [];
		}
		
		private var _elementsInfo:Object;
		public function get elementsInfo():Object { return this._elementsInfo; }
		public function set elementsInfo(value:Object):void { this._elementsInfo = value; }
		
		protected final function deserializeLabelElement(data:XML, styles:XML, label:LabelElement, elementIndex:int, resources:ResourcesLoader):LabelElement {
			if (data.label[0] != null) {
				if (label.needCreateElementForPoint(data.label[0]))
					label = LabelElement(label.createCopy());
				label.deserializePoint(data.label[0], this, styles, elementIndex,resources);
			}
			this.checkPointElementPosition(label, elementIndex);
			return label;
		}
		
		protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, elementIndex:int, resources:ResourcesLoader):MarkerElement {
			if (data.marker[0] != null) {
				if (marker.needCreateElementForPoint(data.marker[0]))
					marker = MarkerElement(marker.createCopy());
				marker.deserializePoint(data.marker[0], this, styles, elementIndex,resources);
			}
			this.checkPointElementPosition(marker, elementIndex);
			return marker;
		}
		
		protected function deserializeTooltipElement(data:XML, styles:XML, tooltip:TooltipElement, elementIndex:int, resources:ResourcesLoader):TooltipElement {
			if (data.tooltip[0] != null) {
				if (tooltip.needCreateElementForPoint(data.tooltip[0]))
					tooltip = TooltipElement(tooltip.createCopy());
				tooltip.deserializePoint(data.tooltip[0], this, styles, elementIndex,resources);
			}
			return tooltip;
		}
		
		protected function checkPointElementPosition(element:BaseElement, elementIndex:uint):void {}
		
		override public function deserializeElements(data:XML, styles:XML,resources:ResourcesLoader):void {
			this.marker = this.deserializeMarkerElement(data, styles, this.marker, MARKER_INDEX, resources);
			this.label = this.deserializeLabelElement(data, styles, this.label, LABEL_INDEX, resources);
			this.tooltip = this.deserializeTooltipElement(data, styles, this.tooltip, TOOLTIP_INDEX, resources);
			this.deserializeExtraLabels(data, styles, resources);
			this.deserializeExtraMarkers(data, styles, resources);
			this.deserializeExtraTooltips(data, styles, resources);
		}
		
		private function deserializeExtraLabels(data:XML, styles:XML, resources:ResourcesLoader):void {
			var i:uint = 0;
			var cnt:uint = 0;
			if (data.extra_labels[0] != null) {
				this.extraLabels = [];
				var extraLabelsList:XMLList = data.extra_labels[0].label;
				cnt = extraLabelsList.length();
				for (i=0;i<cnt;i++) {
					this.extraLabels.push(this.deserializeExtraLabel(extraLabelsList[i], styles, resources));
				}
			}else if (this.extraLabels) {
				cnt = this.extraLabels.length;
			}
			
			for (i = 0;i<cnt;i++)
				this.checkPointElementPosition(this.extraLabels[i], EXTRA_LABELS_FIRST_INDEX+i);
		}
		
		private function deserializeExtraLabel(data:XML, styles:XML, resources:ResourcesLoader):LabelElement {
			var label:LabelElement = this.global.createLabelElement(true);			
			if (data != null) 
				label.deserializeGlobal(data, styles, resources);
			return label;
		}
		
		private function deserializeExtraMarkers(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_markers[0] != null) {
				this.extraMarkers = [];
				var extraMarkersList:XMLList = data.extra_markers[0].marker;
				var extraMarkersCnt:uint = extraMarkersList.length();
				for (var i:uint = 0;i<extraMarkersCnt;i++) {
					this.extraMarkers.push(this.deserializeExtraMarker(extraMarkersList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraMarker(data:XML, styles:XML, resources:ResourcesLoader):MarkerElement {
			var marker:MarkerElement = this.global.createMarkerElement();			
			if (data != null) 
				marker.deserializeGlobal(data, styles, resources);
			return marker;
		}
		
		private function deserializeExtraTooltips(data:XML, styles:XML, resources:ResourcesLoader):void {
			if (data.extra_tooltips[0] != null) {
				this.extraTooltips = [];
				var extraTooltipsList:XMLList = data.extra_tooltips[0].tooltip;
				var extraTooltipsCnt:uint = extraTooltipsList.length();
				for (var i:uint = 0;i<extraTooltipsCnt;i++) {
					this.extraTooltips.push(this.deserializeExtraTooltip(extraTooltipsList[i], styles, resources));
				}
			}
		}
		
		private function deserializeExtraTooltip(data:XML, styles:XML, resources:ResourcesLoader):TooltipElement {
			var tooltip:TooltipElement = new TooltipElement();			
			if (data != null) 
				tooltip.deserializeGlobal(data, styles, resources);
			return tooltip;
		}
		
		protected final function applyHandCursor(sprite:Sprite):void {
			if (sprite != null) {
				sprite.buttonMode = true;
				sprite.useHandCursor = true;
			}
		}
		
		protected final function applyListeners(sprite:Sprite):void {
			if (sprite != null)
				this.initListeners(sprite);
		}
		
		protected function initializeElements():void {
			var i:uint = 0;
			var extraLabelsCnt:uint = this.extraLabels ? this.extraLabels.length : 0;
			var extraMarkersCnt:uint = this.extraMarkers ? this.extraMarkers.length : 0;
			var extraTooltipsCnt:uint = this.extraTooltips ? this.extraTooltips.length : 0;
			
			if (this.global.drawingMode == DrawingMode.DRAW_TO_PERSONAL_CONTAINER) {
				if (this.tooltip != null) this.tooltipSprite = this.tooltip.initialize(this, TOOLTIP_INDEX);
				for (i = 0; i < extraTooltipsCnt; i++)
					this.extraTooltipsSprites.push(TooltipElement(this.extraTooltips[i]).initialize(this, EXTRA_TOOLTIPS_FIRST_INDEX + i));
				if (this.markerSprite != null || this.labelSprite != null || extraLabelsCnt > 0 || extraMarkersCnt > 0) {
					if (this.settings.interactivity.useHandCursor) {
						this.applyHandCursor(this.markerSprite);
						this.applyHandCursor(this.labelSprite);
						for (i = 0;i<extraLabelsCnt;i++)
							this.applyHandCursor(this.extraLabelsSprites[i]);
						for (i = 0;i<extraMarkersCnt;i++)
							this.applyHandCursor(this.extraMarkersSprites[i]);
					}
					
					if (!this.isMissing) {
						this.applyListeners(this.markerSprite);
						this.applyListeners(this.labelSprite);
						for (i = 0;i<extraLabelsCnt;i++)
							this.applyListeners(this.extraLabelsSprites[i]);
						for (i = 0;i<extraMarkersCnt;i++)
							this.applyListeners(this.extraMarkersSprites[i]);
					}
				}
			}
		}
		
		public function isLabelEnabled():Boolean {
			if (!this.label) return false;
			if (this.elementsInfo != null && this.elementsInfo[LABEL_INDEX] != null && this.elementsInfo[LABEL_INDEX].enabled != null) {
				if (!this.elementsInfo[LABEL_INDEX].enabled) 
					return false;
			}
			if (!this.label.enabled) return false;
			return true;
		}
		
		//-----------------------------------------------------
		//					Elements Drawing
		//-----------------------------------------------------
		
		protected var markerSprite:Sprite;
		protected var labelSprite:Sprite;
		protected var tooltipSprite:Sprite;
		
		private var extraLabelsSprites:Array;
		private var extraMarkersSprites:Array;
		private var extraTooltipsSprites:Array;
		
		protected function drawLabelElement(element:BaseElement, styleState:StyleState, sprite:Sprite, index:uint):void {
			this.drawElement(element, styleState, sprite, index);
		}
		
		protected function resizeLabelElement(element:BaseElement, styleState:StyleState, sprite:Sprite, index:uint):void {
			this.resizeElement(element, styleState, sprite, index);
		}
		
		protected function drawElement(element:BaseElement, styleState:StyleState, sprite:Sprite, index:uint):void {
			if (sprite == null) return;
			element.draw(this, styleState, sprite, index);
		}
		
		protected function resizeElement(element:BaseElement, styleState:StyleState, sprite:Sprite, index:uint):void {
			if (sprite == null) return;
			element.resize(this, styleState, sprite, index);
		}
		
		protected final function drawElements():void {
			if (this.isMissing) {
				this.drawMissingElements();
			}else if (this.settings.interactivity.allowSelect && this.isSelected) {
				this.drawSelectedElements();
			}else {
				this.drawNormalElements();
			}
		}
		
		protected final function resizeElements():void {
			if (this.isMissing) {
				this.resizeMissingElements();
			}else if (this.settings.interactivity.allowSelect && this.isSelected) {
				this.resizeSelectedElements();
			}else {
				this.resizeNormalElements();
			}
		}
		
		protected function resizeMissingElements():void {
			if (this.marker != null) this.resizeElement(this.marker, this.marker.style.missing, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.missing, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null)this.tooltip.hide(this.tooltipSprite);
			
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++)
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.missing, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
			}
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.resizeElement(this.extraMarkers[i], this.extraMarkers[i].style.mising, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		protected function resizeSelectedElements():void {
			if (this.marker != null) this.resizeElement(this.marker, this.marker.style.selectedNormal, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.selectedNormal, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null) this.tooltip.hide(this.tooltipSprite);
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.selectedNormal, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.resizeElement(this.extraMarkers[i], this.extraMarkers[i].style.selectedNormal, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		protected function resizeNormalElements():void {
			if (this.marker != null) this.resizeElement(this.marker, this.marker.style.normal, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.normal, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null) this.tooltip.hide(this.tooltipSprite);
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.normal, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.resizeElement(this.extraMarkers[i], this.extraMarkers[i].style.normal, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		protected function drawNormalElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.normal, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.normal, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null) this.tooltip.hide(this.tooltipSprite);
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.normal, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.normal, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		private function drawTooltip(drawStateName:String, moveStateName:String):void {
			if (!this.canDrawToolip)
				return;
			this.execDrawTooltip(this.tooltip, this.tooltipSprite, TOOLTIP_INDEX, drawStateName, moveStateName);
			if (this.extraTooltips){
				var len:uint = this.extraTooltips.length;
				for (var i:uint = 0; i < len; i++)
					this.execDrawTooltip(this.extraTooltips[i], this.extraTooltipsSprites[i], EXTRA_TOOLTIPS_FIRST_INDEX + i, drawStateName, moveStateName);
			}
		}
		
		private function execDrawTooltip(tooltip:TooltipElement, tooltipSprite:Sprite, index:uint, drawStateName:String,
			moveStateName:String):void {
			if (tooltip != null) {
				this.drawElement(tooltip, tooltip.style[drawStateName], tooltipSprite, index);
				
				if (this.elementsInfo != null && this.elementsInfo[index] != null && this.elementsInfo[index].enabled != null && this.elementsInfo[index].enabled)
					tooltip.move(tooltip.style[moveStateName], this, index, tooltipSprite);
				else if (tooltip.enabled)
					tooltip.move(tooltip.style[moveStateName], this, index, tooltipSprite);
			}
		}
		
		protected function drawHoverElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.hover, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.hover, this.labelSprite, LABEL_INDEX);
			
			this.drawTooltip("normal", "normal");
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.hover, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.hover, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
		}
		
		protected function drawPushedElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.pushed, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.pushed, this.labelSprite, LABEL_INDEX);
			this.drawTooltip("pushed", "normal");
			/* if (this.tooltip != null) {
				this.drawElement(this.tooltip, this.tooltip.style.pushed, this.tooltipSprite, TOOLTIP_INDEX);
				this.tooltip.move(this.tooltip.style.normal, this, TOOLTIP_INDEX, this.tooltipSprite);
			} */
			
			var i:uint;
			if (this.extraLabels) {
				for (i = 0;i<this.extraLabels.length;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.pushed, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				for (i = 0;i<this.extraMarkers.length;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.pushed, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
		}

		protected function drawMissingElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.missing, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.missing, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null) this.tooltip.hide(this.tooltipSprite);
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.missing, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<this.extraMarkers.length;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.missing, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		protected function drawSelectedElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.selectedNormal, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.selectedNormal, this.labelSprite, LABEL_INDEX);
			if (this.tooltip != null) this.tooltip.hide(this.tooltipSprite);
			
			var i:uint;
			var len:uint;
			if (this.extraLabels) {
				len = this.extraLabels.length;
				for (i = 0;i<len;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.selectedNormal, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				len = this.extraMarkers.length;
				for (i = 0;i<len;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.selectedNormal, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
			
			if (this.extraTooltips) {
				len = this.extraTooltips.length;
				for (i = 0;i<len;i++)
					this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
			}
		}
		
		protected function drawSelectedHoverElements():void {
			if (this.marker != null) this.drawElement(this.marker, this.marker.style.selectedHover, this.markerSprite, MARKER_INDEX);
			if (this.label != null) this.drawLabelElement(this.label, this.label.style.selectedHover, this.labelSprite, LABEL_INDEX);
			this.drawTooltip("selectedNormal", "selectedNormal");
			var i:uint;
			if (this.extraLabels) {
				for (i = 0;i<this.extraLabels.length;i++) {
					this.drawLabelElement(this.extraLabels[i], this.extraLabels[i].style.selectedHover, this.extraLabelsSprites[i], EXTRA_LABELS_FIRST_INDEX+i);
				}
			}
			
			if (this.extraMarkers) {
				for (i = 0;i<this.extraMarkers.length;i++) 
					this.drawElement(this.extraMarkers[i], this.extraMarkers[i].style.selectedHover, this.extraMarkersSprites[i], EXTRA_MARKERS_FIRST_INDEX+i);
			}
		}
		
		protected function moveElements():void {
			if (this.tooltipSprite != null)
				this.tooltip.move(this.isSelected ? this.tooltip.style.selectedNormal : this.tooltip.style.normal,
								  this, TOOLTIP_INDEX, this.tooltipSprite);
			if (this.extraTooltips != null){
				var len:uint;
				for (var i:uint; i < len; i++){
					var tooltip:TooltipElement = this.extraTooltips[i]; 
					tooltip.move(this.isSelected ? tooltip.style.selectedNormal : tooltip.style.normal,
								  this, EXTRA_TOOLTIPS_FIRST_INDEX + i, this.extraTooltipsSprites[i]);
				}
			}
		}
		
		//-----------------------------------------------------
		//					Elements Placement
		//-----------------------------------------------------
		
		public function getElementPosition(state:BaseElementStyleState, 
				 						   sprite:Sprite,
									   	   elementSize:Rectangle):Point {
			if (state == null) return null;
			if (this.isMissing && !this.plot.interpolateMissings) return null;
			var pos:Point = new Point();
			this.getAnchorPoint(state.anchor, pos, elementSize, state.padding);
			if (state.anchor != Anchor.X_AXIS) {
				this.applyHAlign(pos, state.hAlign,elementSize,state.padding);
				this.applyVAlign(pos, state.vAlign,elementSize,state.padding);
			}
			return pos;
		} 
		
		protected function applyHAlign(pos:Point, hAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (hAlign) {
				case HorizontalAlign.LEFT:
					pos.x -= elementSize.width + padding;
					break;
				case HorizontalAlign.RIGHT:
					pos.x += padding;
					break;
				case HorizontalAlign.CENTER:
					pos.x -= elementSize.width/2;
					break;
			}
		}
		
		protected function applyVAlign(pos:Point, vAlign:uint, elementSize:Rectangle, padding:Number):void {
			switch (vAlign) {
				case VerticalAlign.TOP:
					pos.y -= elementSize.height + padding;
					break;
				case VerticalAlign.BOTTOM:
					pos.y += padding;
					break;
				case VerticalAlign.CENTER:
					pos.y -= elementSize.height/2;
					break;
			}
		}
		
		protected function getAnchorPoint(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			this.getAnchorPointByRect(anchor, pos, bounds, padding);
		}
		
		protected final function getAnchorPointByRect(anchor:uint, pos:Point, bounds:Rectangle, padding:Number):void {
			if (this.bounds == null) return;
			switch (anchor) {
				case Anchor.CENTER:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_BOTTOM:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.CENTER_LEFT:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_RIGHT:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y + this.bounds.height/2;
					break;
				case Anchor.CENTER_TOP:
					pos.x = this.bounds.x + this.bounds.width/2;
					pos.y = this.bounds.y;
					break;
				case Anchor.LEFT_BOTTOM:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.LEFT_TOP:
					pos.x = this.bounds.x;
					pos.y = this.bounds.y;
					break;
				case Anchor.RIGHT_BOTTOM:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y + this.bounds.height;
					break;
				case Anchor.RIGHT_TOP:
					pos.x = this.bounds.x + this.bounds.width;
					pos.y = this.bounds.y;
					break;
			}
		}
		
		//-----------------------------------------------------
		//				Drawing
		//-----------------------------------------------------
		
		public var container:Sprite;
		public var styleState:StyleState;
		private var _hasPersonalContainer:Boolean = false;
		public function get hasPersonalContainer():Boolean { return this._hasPersonalContainer; }
		public function set hasPersonalContainer(value:Boolean):void { this._hasPersonalContainer = value; }
		
		public var bounds:Rectangle;
		
		protected function initializeStyleState():void {
			var plot:SeriesPlot = this.plot;
			var allowSelect:Boolean = this.settings.interactivity.allowSelect;
			var prevSelectedPoint:BasePoint;
			
			if (this.settings.style != null) {
				if (this.isMissing) {
					this.styleState = this.settings.style.missing;
				}else if (allowSelect && this.isSelected) {
					this.styleState = this.settings.style.selectedNormal;
					if (!plot.allowMultipleSelect) {
						prevSelectedPoint = plot.getSelectedPoint();
						if (prevSelectedPoint != null) {
							prevSelectedPoint.isSelected = false;
							prevSelectedPoint.styleState = prevSelectedPoint.settings.style.normal;
						}
					}
					plot.setSelectedPoint(this);
				}else {
					this.styleState = this.settings.style.normal;
				}
			}else if (allowSelect && this.isSelected) {
				if (!plot.allowMultipleSelect) {
					prevSelectedPoint = plot.getSelectedPoint();
					if (prevSelectedPoint != null) {
						prevSelectedPoint.isSelected = false;
						if (prevSelectedPoint.settings.style != null)
							prevSelectedPoint.styleState = prevSelectedPoint.settings.style.normal;
					}
				}
				plot.setSelectedPoint(this);
			}
		}
		
		public function setSelected(isSelected:Boolean):void {
			this.isSelected = isSelected;
			var plot:SeriesPlot = this.plot;
			var prevSelectedPoint:BasePoint;
			
			if (this.settings.interactivity.allowSelect) {
				if (!this.plot.allowMultipleSelect) {
					if (this.isSelected) {
						if (plot.getSelectedPoint() != null)
							plot.getSelectedPoint().deselect();
						plot.setSelectedPoint(this);
					}else {
						this.deselect();
					}
				}
				
				if (this.isSelected) {
					if (this.settings.style != null)
						this.styleState = this.settings.style.selectedNormal;
					this.redraw();
					this.drawSelectedElements();
				}else {
					if (this.settings.style != null)
						this.styleState = this.settings.style.normal;
					this.redraw();
					this.drawNormalElements();
				}
			}
		}
		
		public function select(isSelected:Boolean):void {
			this.isSelected = isSelected;
			var plot:SeriesPlot = this.plot;
			var prevSelectedPoint:BasePoint;

			if (this.settings.interactivity.allowSelect) {
				if (!this.plot.allowMultipleSelect) {
					if (this.isSelected) {
						if (plot.getSelectedPoint() != null)
							plot.getSelectedPoint().deselect();
						if (plot.getSelectedPoint() != null)
							plot.getSelectedPoint().dispatchAnyChartPointEvent(null,PointEvent.POINT_DESELECT);
						plot.setSelectedPoint(this);
					}else {
						this.deselect();
					}
				}
				
				if (this.isSelected) {
					if (this.settings.style != null)
						this.styleState = this.settings.style.selectedNormal;
					this.redraw();
					this.drawSelectedElements();
					this.dispatchAnyChartPointEvent(null, PointEvent.POINT_DESELECT);
				}else {
					if (this.settings.style != null)
						this.styleState = this.settings.style.normal;
					this.redraw();
					this.drawNormalElements();
					this.dispatchAnyChartPointEvent(null,PointEvent.POINT_SELECT);
				}
			}
		}
		
		public final function createContainer():void {
			if (this.global.drawingMode == DrawingMode.DRAW_TO_PERSONAL_CONTAINER) {
				this.container = new Sprite();
				this.hasPersonalContainer = true;
			}else {
				this.hasPersonalContainer = false;
			}
			
			this.createElementsSprites();
		}
		
		protected function initializeContainer():void {
			
			if (this.hasPersonalContainer) {
				this.addContainerToSeriesContainer();
				this.setContainerProperties(this.container);
			}else {
				this.container = this.global.container;
			}
			
			this.initializeElements();
			this.bounds = new Rectangle();
		}
		
		protected function addContainerToSeriesContainer():void {
			this.global.container.addChild(this.container);
		}
		
		public function initialize():void {
			this.initializeStyleState();
			this.initializeContainer();
		}
		
		protected final function setContainerProperties(container:Sprite):void {
			if (this.settings.interactivity.useHandCursor) {
				container.buttonMode = true;
				container.useHandCursor = true;
			}
			if (!this.isMissing) {
				this.initListeners(container);
			}
		}
		
		private var listenersList:Array;
		
		public final function initListeners(element:InteractiveObject):void {
			if (this.listenersList == null) this.listenersList = [];
			this.listenersList.push(element);
			element.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			element.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			element.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			element.addEventListener(MouseEvent.CLICK, this.mouseClickHandler);
			element.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveEventHandler);
		}
		
		public final function removeListeners(element:InteractiveObject):void {
			element.removeEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			element.removeEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			element.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			element.removeEventListener(MouseEvent.CLICK, this.mouseClickHandler);
			element.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMoveEventHandler);
		}
		
		public final function checkThreshold():void {
			if (this.threshold != null)
				this.threshold.checkBeforeDraw(this);
		}
		
		public final function draw():void {
			this.execDrawing();
			this.drawElements();
		}
		
		public final function resize():void {
			this.execDrawing();
			this.resizeElements();
		}
		
		protected function execDrawing():void {
			if (this.hasPersonalContainer) {
				this.container.graphics.clear();
			}
		}
		
		//-----------------------------------------------------------------------
		//					Interactivity
		//-----------------------------------------------------------------------
		
		protected var canDrawToolip:Boolean;
		
		public function setHover():void {
			if (this.isMissing) return;
			this.canDrawToolip = false;
			this.mouseOverHandler(null);
			this.canDrawToolip = true;
		}
		
		public function setNormal():void {
			if (this.isMissing) return;
			this.mouseOutHandler(null);
		}
		
		public function deselect():void {
			this.isSelected = false;
			this.plot.deselectPoint();
			if (this.settings.style != null)
				this.styleState = this.settings.style.normal;
			this.redraw();
			this.drawElements();
		}
		
		public var stopPropagation:Boolean; 
		
		protected function redraw():void {
			if (this.hasPersonalContainer)
				this.container.graphics.clear();
		}
		
		private function mouseOverHandler(event:MouseEvent):void {
			if (event != null) {
				this.isOver = true;
				if (this.stopPropagation) event.stopImmediatePropagation();
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OVER);
			}
			this.execMouseOver();
		}
		
		private function execMouseOver():void {
			if (this.settings.interactivity.hoverable) {
				if (this.isSelected) {
					if (this.settings.style != null)
						this.styleState = this.settings.style.selectedHover;
					this.drawSelectedHoverElements();
				}else {
					if (this.settings.style != null)
						this.styleState = this.settings.style.hover;
					this.drawHoverElements();
				}
				this.redraw();
			}else {
				if (this.isSelected)
					this.drawTooltip("selectedNormal", "selectedNormal");
				else
					this.drawTooltip("normal", "normal");
			}
		}
		
		private function mouseOutHandler(event:MouseEvent):void {
			if (event != null) {
				if (this.stopPropagation) event.stopImmediatePropagation();
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_OUT);
			}
			this.execMouseOut();
		}
		
		private function execMouseOut():void {
			this.isOver = false;
			if (this.settings.interactivity.hoverable) {
				if (this.isSelected) {
					if (this.settings.style != null)
						this.styleState = this.settings.style.selectedNormal;
					this.drawSelectedElements();
				}else {
					if (this.settings.style != null)
						this.styleState = this.settings.style.normal;
					this.drawNormalElements();
				}
				this.redraw();
			}else {
				if (this.tooltip != null)
					this.tooltip.hide(this.tooltipSprite);
				if (this.extraTooltips != null){
					var len:uint;
					for (var i:uint; i < len; i++)
						this.extraTooltips[i].hide(this.extraTooltipsSprites[i]);
				}
			}
		}
		
		private function mouseDownHandler(event:MouseEvent):void {
			if (this.stopPropagation) event.stopImmediatePropagation();
			
			var isChangeChartAction:Boolean = false;
			if (this.settings.actions != null)
				isChangeChartAction = this.settings.actions.execute(this);
			if (!isChangeChartAction) {
				if (this.settings.interactivity.allowSelect && !this.isSelected) {
					if (this.settings.style != null)
						this.styleState = this.settings.style.pushed;
					this.drawPushedElements();
					this.redraw();
				}
			}
			
			this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_DOWN); 
		}
		
		private function mouseMoveEventHandler(event:MouseEvent):void {
			//if (this.stopPropagation) event.stopImmediatePropagation();
			if (this.isOver)
				this.moveElements();
		}
		
		private function mouseClickHandler(event:MouseEvent):void {
			
			try {
			
				if (this.stopPropagation) event.stopImmediatePropagation();
				
				if (this.settings.interactivity.allowSelect) {
					if (this.plot.allowMultipleSelect) {
						this.isSelected = !this.isSelected;
						if (this.settings.interactivity.hoverable) {
							this.execMouseOver();
						}else {
							if (this.isSelected) {
								if (this.settings.style != null)
									this.styleState = this.settings.style.selectedNormal;
								this.drawSelectedElements();
							}else {
								if (this.settings.style != null)
									this.styleState = this.settings.style.normal;
								this.drawNormalElements();
							}
						}
						if (this.isSelected)
							this.dispatchAnyChartPointEvent(event, PointEvent.POINT_SELECT);
						else
							this.dispatchAnyChartPointEvent(event, PointEvent.POINT_DESELECT);
					}else {
						if (!this.isSelected) {
							var plot:SeriesPlot = this.plot;
							if (plot.getSelectedPoint() != null)
								plot.getSelectedPoint().deselect();
							this.isSelected = true;
							if (plot.getSelectedPoint() != null)
								plot.getSelectedPoint().dispatchAnyChartPointEvent(event, PointEvent.POINT_DESELECT);
							plot.setSelectedPoint(this);
							this.dispatchAnyChartPointEvent(event, PointEvent.POINT_SELECT);
							
							if (this.settings.interactivity.hoverable) {
								this.execMouseOver();
							}else {
								if (this.settings.style != null)
									this.styleState = this.settings.style.selectedNormal;
								this.drawSelectedElements();
								this.redraw();
							}
							
						}
					}
				}
				
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_MOUSE_UP);
				this.dispatchAnyChartPointEvent(event, PointEvent.POINT_CLICK);
				this.mouseClickActionsExec();
			
			}catch (e:Error) { trace(e); }
		}
		
		protected function mouseClickActionsExec():void {}
		
		private function dispatchAnyChartPointEvent(e:MouseEvent, type:String):void {
			if (this.global && this.global.chartBase) {
				var x:Number = NaN;
				var y:Number = NaN;
				if (e != null) {
					x = e.stageX;
					y = e.stageY;
				}
				this.global.chartBase.dispatchEvent(new PointEvent(type, this, x, y));
			}
		}
		
		//---------------------------------------------------------------
		//				Hit test
		//---------------------------------------------------------------
		
		public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			return false;
		}
		
		//---------------------------------------------------------------
		//				Animated update
		//---------------------------------------------------------------
		
		public function updateValue(newValue:Object):void {
		}
		
		public function getCurrentValuesAsObject():Object {
			return {};
		}
		
		public function canAnimate(field:String):Boolean {
			return false;
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);
			res.SeriesPointCount = this.series.points.length;
			res.Index = this.index;
			return res;
		}
		
		public function serializeToEvent():Object {
			var res:Object = this.serialize();
			res.Series = this.series.serialize();		
			return res;
		}
		
		public function initializeAfterInterpolate():void {
			this.initializeStyle();
			this.initializeElementsStyle();
			this.series.checkPoint(this);
		}
	}
}