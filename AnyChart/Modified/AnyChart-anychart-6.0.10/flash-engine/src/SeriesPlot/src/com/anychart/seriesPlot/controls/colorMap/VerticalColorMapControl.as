package com.anychart.seriesPlot.controls.colorMap {
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class VerticalColorMapControl extends ColorMapControl {
		
		public static function check(data:XML):Boolean {
			return String(data.name()) == "color_swatch" 
				   && (data.@orientation != undefined) 
				   && (String(data.@orientation).toLowerCase() == "vertical");
		}
		
		public function VerticalColorMapControl() {
			super();
			this.defaultRangeItemWidth = 10;
			this.defaultRangeItemHeight = 20;
		}
		
		override protected function onBeforeDrawRanges():void {
			super.onBeforeDrawRanges();
			//this.left += (this.contentBounds.width - this.rangeItemWidth - this.labelsSpace - this.tickmarksSpace)/2 + this.normalLabelsSpace + this.normalTickmarksSpace;
			this.left += this.normalLabelsSpace + this.normalTickmarksSpace;
			if (this.labels != null) {
				if (this.tickmarkOnRangeCenter) {
					var w:Number = this.labels.getFirstBounds().height;
					if (w > this.rangeItemHeight)
						this.top += (w - this.rangeItemHeight)/2;
				}else {
					this.top += this.labels.getFirstBounds().height/2;
				}
			}
		}
		
		override protected function setRangeBounds(rect:Rectangle, index:int):void {
			super.setRangeBounds(rect, index);
			rect.x = this.left;
			rect.y = this.top + this.rangeItemHeight*index;
		}
		
		override protected function setLabelPosition(pos:Point, rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {
			var labelBounds:Rectangle = this.labels.getLabelBounds(index);
			pos.y = this.getY(rangeBounds, index, labelBounds.height);
			var space:Number = this.labels.padding + this.normalTickmarksSpace;
			
			if (this.labelsPosition == ColorMapLabelsPosition.NORMAL || (this.labelsPosition == ColorMapLabelsPosition.COMBINED && (labelDrawIndex%2==0)))
				pos.x = this.left - space - labelBounds.width;
			else
				pos.x = this.left + this.rangeItemWidth + space;
		}
		
		override protected function drawTickmark(rangeBounds:Rectangle, index:int, labelDrawIndex:int):void {
			var y:Number = this.getY(rangeBounds, index, 0);
			var left:Number;
			if (this.labelsPosition == ColorMapLabelsPosition.NORMAL || (this.labelsPosition == ColorMapLabelsPosition.COMBINED && (labelDrawIndex%2==0)))
				left = this.left - this.tickmarksSize;
			else
				left = this.left + this.rangeItemWidth;
			this.container.graphics.moveTo(left, y);
			this.container.graphics.lineTo(left+this.tickmarksSize,y);
		}
		
		private function getY(rangeBounds:Rectangle, index:int, h:Number):Number {
			if (this.tickmarkOnRangeCenter)
				return rangeBounds.y + (rangeBounds.height-h)/2;
			return ((index == this.threshold.getRangeCount()) ? rangeBounds.bottom : rangeBounds.top) - h/2;
		}
		
		override protected function calculateLabelsSpace():void {
			this.labelsSpace = 0;
			this.normalLabelsSpace = 0;
			if (this.labels == null || !this.labels.enabled)
				return;
				
			switch (this.labelsPosition) {
				case ColorMapLabelsPosition.COMBINED:
					this.normalLabelsSpace = this.labels.maxEvenLabelWidth + this.labels.padding;
					this.labelsSpace = this.normalLabelsSpace + this.labels.maxOddLabelWidth + this.labels.padding;
					break;
					
				case ColorMapLabelsPosition.NORMAL:
					this.labelsSpace = Math.max(this.labels.maxEvenLabelWidth, this.labels.maxOddLabelWidth) + this.labels.padding;
					this.normalLabelsSpace = this.labelsSpace;
					break;
				case ColorMapLabelsPosition.OPPOSITE:
					this.labelsSpace = Math.max(this.labels.maxEvenLabelWidth, this.labels.maxOddLabelWidth) + this.labels.padding;
					break;
			}
		}
		
		override protected function calculateOptimalRangeItemWidth():Number {
			return this.defaultRangeItemWidth;
		}
		
		override protected function calculateRangeItemWidthBasedOnWidth():Number {
			return this.getContentWidth() - this.labelsSpace - this.tickmarksSpace;
		}
		
		override protected function calculateWidthBasedOnRangeItemWidth():Number {
			return this.rangeItemWidth + this.labelsSpace + this.tickmarksSpace;
		}
		
		override protected function calculateOptimalRangeItemHeight():Number {
			if (this.labels == null || !this.labels.enabled)
				return this.defaultRangeItemHeight;
			
			switch (this.labelsPosition) {
				case ColorMapLabelsPosition.COMBINED:
					if ((!this.tickmarkOnRangeCenter && this.threshold.getRangeCount()>1)
						|| 
						(this.tickmarkOnRangeCenter && this.threshold.getRangeCount() > 2))
						return .25*this.labels.maxSummIAndIPlus2LabelHeight;
					return this.defaultRangeItemHeight; 
					
				case ColorMapLabelsPosition.NORMAL:
				case ColorMapLabelsPosition.OPPOSITE:
					return this.labels.maxSummIAndIPlus2LabelHeight/2;
			}
			
			return NaN;
		}
		
		override protected function calculateHeightBasedOnRangeItemHeight():Number {
			var res:Number = this.rangeItemHeight*this.threshold.getRangeCount();
			if (this.labels != null && this.labels.enabled) {
				if (this.tickmarkOnRangeCenter) {
					if (this.labels.getFirstBounds().height > this.rangeItemHeight)
						res += (this.labels.getFirstBounds().height - this.rangeItemHeight)/2;
					if (this.labels.getLastBounds().height > this.rangeItemHeight)
						res += (this.labels.getLastBounds().height - this.rangeItemHeight)/2;
				}else {
					res += this.labels.getFirstBounds().height/2 + this.labels.getLastBounds().height/2;
				}
			}
			return res;
		}
		
		override protected function calculateRangeItemHeightBasedOnHeight():Number {
			var space:Number = this.getContentHeight();
			if (this.labels != null && this.labels.enabled) {
				if (this.tickmarkOnRangeCenter) {
					
					var canCalc:Boolean = false;
					
					if (this.labelsPosition == ColorMapLabelsPosition.COMBINED) {
						canCalc = this.labels.summEvenLabelHeight <= space && 
								  this.labels.summOddLabelHeight <= space;
					}else {
						canCalc = (this.labels.summEvenLabelHeight + this.labels.summOddLabelHeight) <= space;
					}
					
					var l1:Number = this.labels.getFirstBounds().height;
					var l2:Number = this.labels.getLastBounds().height;
					var n:int = this.threshold.getRangeCount();
					
					if (canCalc) {
						
						var w1:Number = space/this.threshold.getRangeCount();
						if (w1 >= l1 && w1 >= l2)
							return w1;
							
						var w2:Number = (2*space - l1)/(2*n - 1);
						if (w2 <= l1 && w2 >= l2)
							return w2;
							
						var w3:Number = (2*space - l2)/(2*n - 1);
						if (w3 >= l1 && w3 <= l2)
							return w3;
							
						var w4:Number = (2*space - l1 - l2)/(2*(n - 1));
						if (w4 <= l1 && w4 <= l2)
							return w4;
						
					}
					
					return (2*space - l1 - l2)/(2*(n - 1));
					
				}else {
					space -= this.labels.getFirstBounds().height/2 + this.labels.getLastBounds().height/2; 
				}
			}
			return space/this.threshold.getRangeCount();
		}
	}
}