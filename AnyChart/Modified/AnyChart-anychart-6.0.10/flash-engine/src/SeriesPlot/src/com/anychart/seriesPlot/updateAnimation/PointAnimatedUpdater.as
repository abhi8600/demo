package com.anychart.seriesPlot.updateAnimation {
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.utils.clearInterval;
	import flash.utils.setInterval;

	public class PointAnimatedUpdater {
		
		private var plot_:SeriesPlot;
		private var animations_:Array;
		
		public function PointAnimatedUpdater(plot:SeriesPlot) {
			this.plot_ = plot;
			this.animations_ = new Array();
		}
		
		public function updatePointValue(seriesId:String, pointId:String, newValue:Object, animationSettings:Object):void {
			if (!this.plot_.hasPoint(seriesId, pointId)) return;
			var point:BasePoint = this.plot_.getPointById(seriesId, pointId);
			
			var animator:TargetPointAnimatedUpdater = new TargetPointAnimatedUpdater(point, newValue, animationSettings);
			if (animator.canAnimate()) {
				if (animationSettings.enabled != undefined && animationSettings.enabled == false) {
					animator.setFinal();
				}else {
					this.animations_.push(animator);
				}
			}
		}
		
		private var _animationInterval:int;
		private var _startTime:Number;
		
		public function start():void {
			clearInterval(this._animationInterval);
			if (this.animations_.length > 0) {
				this._startTime = new Date().getTime();
				this._animationInterval = setInterval(this.execAnimations, 1); 
			}else {
				this.plot_.recalculateAndRedraw();
			}
		}
		
		private function execAnimations():void {
			var currentTime:Number = new Date().getTime() - this._startTime;
			
			var newAnimations:Array = [];
			for (var i:int = 0;i<this.animations_.length;i++) {
				var animation:TargetPointAnimatedUpdater = this.animations_[i];
				animation.animate(currentTime);
				if (!animation.isFinalized())
					newAnimations.push(animation);
			}
			
			this.animations_ = newAnimations;
			if (this.animations_.length == 0) 
				this.stop();
			
			this.plot_.recalculateAndRedraw();
		}
		
		public function stop():void {
			clearInterval(this._animationInterval);
		}
	}
}