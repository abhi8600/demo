package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.scales.BaseScale;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class AreaSeries extends LineSeries {
		
		public var needCreateGradientRect:Boolean;
		public var gradientBounds:Rectangle;
		
		public function AreaSeries() {
			super();
			this.needCreateGradientRect = false;
			this.needCallingOnBeforeDraw = true;
		}
		
		internal var valueAxisStart:Number;
		internal var minimum:Number;
		internal var maximum:Number;
		
		override public function onBeforeDraw():void {
			super.onBeforeDraw();
			if (this.valueAxis.scale.minimum > 0)
				this.valueAxisStart = this.valueAxis.scale.minimum;
			else
				this.valueAxisStart = 0;
			this.doGradientBounds();
		}
		
		override public function onBeforeResize():void {
			this.doGradientBounds();
		}
		 
		private var lt:Point;
		private var rb:Point;
		
		private function doGradientBounds():void {
			if (!this.needCreateGradientRect)
				return;
			
			this.lt = new Point (Number.MAX_VALUE,Number.MAX_VALUE);
			this.rb = new Point (-Number.MAX_VALUE,-Number.MAX_VALUE);
			for (var j:int=0; j<this.points.length; j++){
				var point:AreaPoint = AreaPoint(this.points[j]);
				if (!point.isMissing){
					point.needCreateDrawingPoints = true;
					point.needInitializePixValues = true;
					
					if (point.needCalcPixValue)
						point.doInitializePixValues();
					
					if (point.isLineDrawable)
						point.doCreateDrawingPoints();

					this.getRectPoints(point);
					if (point.previousStackPoint == null){
						this.lt.x = Math.min(point.bottomEnd.x,point.bottomStart.x,lt.x);
						this.lt.y = Math.min(point.bottomEnd.y,point.bottomStart.y,lt.y);
						
						rb.x = Math.max(point.bottomEnd.x,point.bottomStart.x,rb.x);
						rb.y = Math.max(point.bottomEnd.y,point.bottomStart.y,rb.y);
					}else{
						this.getRectPoints(point.previousStackPoint);
					} 
				}
			}
			this.gradientBounds = new Rectangle (lt.x,lt.y,rb.x-lt.x,rb.y-lt.y);
		}	
		
		
		private function getRectPoints (point:StackablePoint):void {
			var drawPoints:Array = AreaPoint(point).drawingPoints;
			for (var i:int=0; i<drawPoints.length;i++){
				this.lt.x = Math.min(drawPoints[i].x,lt.x);
				this.lt.y = Math.min(drawPoints[i].y,lt.y);
				
				this.rb.x = Math.max(drawPoints[i].x,rb.x);
				this.rb.y = Math.max(drawPoints[i].y,rb.y);
			}
		}
		
		
		
		override public function createPoint():BasePoint {
			return new AreaPoint();
		}
		
		/* override public function configureValueScale(scale:BaseScale):void {
			if (!AxesPlot(this.plot).isScatter)
				scale.checkZero();
		} */
	}
}