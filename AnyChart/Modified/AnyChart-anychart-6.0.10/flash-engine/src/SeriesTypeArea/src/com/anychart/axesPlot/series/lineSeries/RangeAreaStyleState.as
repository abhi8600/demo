package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.stroke.Stroke;
	
	internal final class RangeAreaStyleState extends BackgroundBaseStyleState {
		
		public var endStroke:Stroke;
		
		public function RangeAreaStyleState(style:Style) {
			super(style);
		}
		
		override protected function deserializeStroke(data:XML):void {
			if (SerializerBase.isEnabled(data.start_line[0])) {
				this.stroke = new Stroke();
				this.stroke.deserialize(data.start_line[0],this.style); 
			}
			if (SerializerBase.isEnabled(data.end_line[0])) {
				this.endStroke = new Stroke();
				this.endStroke.deserialize(data.end_line[0],this.style);
			}
		}

	}
}