package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.seriesPlot.data.BasePoint;
	
	internal final class StepForwardAreaSeries extends AreaSeries {
		override public function createPoint():BasePoint {
			return new StepForwardAreaPoint();
		}
	}
}