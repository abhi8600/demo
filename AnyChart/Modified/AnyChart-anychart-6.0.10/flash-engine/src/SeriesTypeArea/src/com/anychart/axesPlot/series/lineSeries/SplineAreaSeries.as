package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	internal final class SplineAreaSeries extends SplineSeries {
		
		internal var valueAxisStart:Number;
		public var needCreateGradientRect:Boolean;
		public var gradientBounds:Rectangle;
		
		public function SplineAreaSeries() {
			super();
			this.needCreateGradientRect = false;
		}
		
		override public function onBeforeDraw():void {
			if (this.valueAxis.scale.minimum > 0)
				this.valueAxisStart = this.valueAxis.scale.minimum;
			else
				this.valueAxisStart = 0;
			super.onBeforeDraw();
			this.doGradientBounds();
		}
		
		override public function onBeforeResize():void {
			super.onBeforeResize();
			this.doGradientBounds();
		}
		
		override public function createPoint():BasePoint {
			return new SplineAreaPoint();
		}
		
		private var lt:Point;
		private var rb:Point;
		private var rect:Rectangle;
		
		private function doGradientBounds ():void {
			if (!this.needCreateGradientRect)
				return;
			this.lt = new Point (Number.MAX_VALUE,Number.MAX_VALUE);
			this.rb = new Point (-Number.MAX_VALUE,-Number.MAX_VALUE);	
			
			for (var j:int=0; j<this.points.length; j++){
				var point:SplineAreaPoint = SplineAreaPoint(this.points[j]);
				if (!point.isMissing){
					this.rect = point.getBound();
					this.lt.x = Math.min(lt.x,rect.x);
					this.lt.y = Math.min(lt.y,rect.y);
					this.rb.x = Math.max(rb.x,rect.x+rect.width);
					this.rb.y = Math.max(rb.y,rect.y+rect.height);
				}
			}
			this.gradientBounds = new Rectangle (lt.x,lt.y,rb.x-lt.x,rb.y-lt.y);
			//trace (this.gradientBounds.x,this.gradientBounds.y);
		}
	}
}