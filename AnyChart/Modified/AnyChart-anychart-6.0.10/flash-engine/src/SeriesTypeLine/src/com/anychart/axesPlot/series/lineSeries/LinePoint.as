package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.data.StackablePoint;
	import com.anychart.selector.HitTestUtil;
	import com.anychart.selector.MouseSelectionManager;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.stroke.Stroke;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class LinePoint extends StackablePoint {
		
		protected var hasPrev:Boolean;
		protected var hasNext:Boolean;
		
		protected var isValuePointsInitialized:Boolean;
		public var isLineDrawable:Boolean;
		public var needCalcPixValue:Boolean;
		
		public var drawingPoints:Array;
		protected var pixValuePt:Point;
		
		public function LinePoint() {
			super();
			this.pixValuePt = new Point();
			this.isValuePointsInitialized = false;
			this.isLineDrawable = false;
			this.hasPrev = false;
			this.hasNext = false;
			this.needCalcPixValue = true;
		}
		
		override protected function addContainerToSeriesContainer():void {
			LineSeries(this.series).container.addChild(this.container);
		}
		
		protected function initializePixValues():void {
			var series:AxesPlotSeries = AxesPlotSeries(this.series);
			
			if (!this.isValuePointsInitialized) {
				this.hasPrev = (this.index > 0) && 
							  (this.series.points[this.index - 1] != null) &&
							  (!this.series.points[this.index - 1].isMissing || this.plot.interpolateMissings);
				this.hasNext = (this.index < (this.series.points.length-1)) &&
							  (this.series.points[this.index + 1] != null) &&
							  (!this.series.points[this.index + 1].isMissing || this.plot.interpolateMissings);
				this.isLineDrawable = this.hasPrev || this.hasNext;
				this.isValuePointsInitialized = true;
			}
			
			this.transformArgument();
			series.valueAxis.transform(this, this.stackValue, this.pixValuePt);
			
			if (this.z) this.z.transform(this.pixValuePt);
		}
		
		protected function transformArgument():void {
			AxesPlotSeries(this.series).argumentAxis.transform(this, this.x, this.pixValuePt);
		}
		
		protected function createDrawingPoints():void {
			this.drawingPoints = [];
			
			var tmp:Point;
			var tmp1:Point;
			if (this.hasPrev) {
				tmp = this.series.points[this.index-1].pixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.pixValuePt.x)/2;
				tmp1.y = (tmp.y + this.pixValuePt.y)/2;
				this.drawingPoints.push(tmp1);
			}
			
			this.drawingPoints.push(this.pixValuePt);
			
			if (this.hasNext) {
				this.series.points[this.index+1].initializePixValues();
				this.series.points[this.index+1].needCalcPixValue = false;
				tmp = this.series.points[this.index+1].pixValuePt;
				tmp1 = new Point();
				tmp1.x = (tmp.x + this.pixValuePt.x)/2;
				tmp1.y = (tmp.y + this.pixValuePt.y)/2;
				this.drawingPoints.push(tmp1);
			}
		}
		
		override public function reset():void {
			this.needCalcPixValue = true;
			this.isValuePointsInitialized = false;
			super.reset();
		}
		
		override protected final function execDrawing():void {
			
			if (this.needCalcPixValue)
				this.initializePixValues();
				
			this.bounds.x = this.pixValuePt.x;
			this.bounds.y = this.pixValuePt.y;
			this.bounds.width = 0;
			this.bounds.height = 0;
			
			if (this.isLineDrawable) {
				this.createDrawingPoints();
				
				super.execDrawing();
				this.drawSegment(this.container.graphics);
			}
		}
		
		override protected function redraw():void {
			if (this.isLineDrawable) {
				super.redraw();
				this.drawSegment(this.container.graphics);
			}
		} 
		
		protected function drawSegment(g:Graphics):void {
			var s:Stroke = LineStyleState(this.styleState).stroke;
			if (s != null) {
				s.apply(g, null, this.color);
				
				var i:int;
				if (s.dashed) {
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 0;i<(this.drawingPoints.length-1);i++) {
						DrawingUtils.drawDashedLine(g, s.spaceLength, s.dashLength, this.drawingPoints[i].x, this.drawingPoints[i].y, this.drawingPoints[i+1].x, this.drawingPoints[i+1].y);
					}
				}else {
					g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
					for (i = 1;i<this.drawingPoints.length;i++) 
						g.lineTo(this.drawingPoints[i].x, this.drawingPoints[i].y);
				}
			}
			
			if (this.hasPersonalContainer) {
				if (LineStyleState(this.styleState).effects != null && LineStyleState(this.styleState).effects.enabled) {
					this.container.filters = LineStyleState(this.styleState).effects.list;
				}else {
					this.container.filters = null;
				}				
			}
		}
		
		private function hitTestMarker(area:Rectangle, selectedArea:DisplayObject):Boolean {
			var tmp:Shape = new Shape();
			tmp.graphics.beginFill(0xFF0000, 1);
			tmp.graphics.drawRect(0,0, area.width, area.height);
			tmp.x = area.x;
			tmp.y = area.y;
			if (this.container != null && this.container.parent != null)
				this.container.parent.addChild(tmp);
			
			var res:Boolean = HitTestUtil.hitTest(this.markerSprite, tmp);
			
			if (this.container != null && this.container.parent != null)
				this.container.parent.removeChild(tmp);
			tmp = null;
			return res;
		}
		
		override public function hitTestSelection(area:Rectangle, selectedArea:DisplayObject, selector:MouseSelectionManager):Boolean {
			if (selector.selectLineOnlyMarkers)
				return this.hitTestMarker(area, selectedArea);
			return super.hitTestSelection(area, selectedArea, selector);
		}
	}
}