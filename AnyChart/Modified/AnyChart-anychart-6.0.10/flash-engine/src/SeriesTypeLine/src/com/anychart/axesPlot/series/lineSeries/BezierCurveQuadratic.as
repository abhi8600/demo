package com.anychart.axesPlot.series.lineSeries
{
	import flash.geom.Point;
	
	internal final class BezierCurveQuadratic
	{
		public var start:Point;
		public var end:Point;
		public var control:Point;
		
		//*
		public function toString():String {
			return "[" + start.x + " ; " + end.x + "]";
		}
		//*/

		public function containsX(x:Number):Boolean {
			return (x > Math.min(start.x, end.x) && x < Math.max(start.x, end.x));
		}

		public function containsY(y:Number):Boolean {
			return (y > Math.min(start.y, end.y) && y < Math.max(start.y, end.y));
		}
		
		public function divide(x:Number):BezierCurveQuadratic {

			if (!containsX(x)) return null;
			var t:Number = getBezierValue(x);
			var y:Number = (1 - 2 * t + t * t) * start.y + 2 * t * (1 - t) * control.y + t * t * end.y;

			/*
				(1 - 2 * t + t * t) * start.y 
				+ 2 * t * (1 - t) * control.y + t * t * end.y;
			//*/

			var res:BezierCurveQuadratic = new BezierCurveQuadratic();
			res.start = new Point(x,y);
			res.control = new Point((1-t) * control.x + end.x * t, (1-t) * control.y + end.y * t);
			res.end = new Point(end.x, end.y);

			end.x = res.start.x;
			end.y = res.start.y;
			control.x = (1-t) * start.x + control.x * t; 
			control.y = (1-t) * start.y + control.y * t; 

			return res;
		}		

		public function getBezierValue(distance:Number):Number {
			var x1:Number = 0;
			var x2:Number = (control.x - start.x)/(end.x - start.x);
			var x3:Number = 1;
			var x4:Number = (distance - start.x)/(end.x - start.x);
			
			if ((x1 - 2*x2 + x3) == 0) return (x4-x1)/(2*(x2-x1));

			var d:Number = 4 * (x2 - x1) * (x2 - x1) - 4 * (x1 - x4) * (x1 - 2 * x2 + x3);
			var t1:Number = (-2 * (x2 - x1) + Math.sqrt(d)) / (2 * (x1 - 2 * x2 + x3));
			var t2:Number = (-2 * (x2 - x1) - Math.sqrt(d)) / (2 * (x1 - 2 * x2 + x3));
			return (t1 >= 0 && t1 <= 1 ? t1 : t2);
		}
	}
}