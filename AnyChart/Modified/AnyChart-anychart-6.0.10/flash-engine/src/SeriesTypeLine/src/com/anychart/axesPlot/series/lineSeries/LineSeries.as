package com.anychart.axesPlot.series.lineSeries {
	import com.anychart.animation.Animation;
	import com.anychart.animation.AnimationManager;
	import com.anychart.axesPlot.data.SingleValueSeries;
	import com.anychart.seriesPlot.data.BasePoint;
	
	import flash.display.Sprite;
	
	internal class LineSeries extends SingleValueSeries {
		
		internal var container:Sprite;
		
		public function LineSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
			this.isSortable = false;
			this.isClusterizableOnCategorizedAxis = false;
			this.container = new Sprite();
		}
		
		override public function onBeforeDraw():void {
			this.global.container.addChild(this.container);
		}
		
		override public function setAnimation(animation:Animation, manager:AnimationManager):void {
			manager.registerDisplayObject(this.container, this.plot.scrollableContainer, animation);
		}
		
		override public function createPoint():BasePoint {
			return new LinePoint();
		}
	}
}