package com.anychart.mapPlot.markerSeries {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	internal final class MarkerSeries extends BaseSeries {
		override public function createPoint():BasePoint {
			return new MarkerPoint();
		}
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (data.@style != undefined)
				if (data.marker[0] == null)
					data.marker[0] = <marker style={data.@style} />;
				else 
					data.marker[0].@style = data.@style;
					
			if (marker.needCreateElementForSeries(data.marker[0])) {
				marker = MarkerElement(marker.createCopy());
				marker.deserializeSeries(data.marker[0], styles, resources);
			}
			marker.enabled = true;
			
			return marker;
		}
	}
}