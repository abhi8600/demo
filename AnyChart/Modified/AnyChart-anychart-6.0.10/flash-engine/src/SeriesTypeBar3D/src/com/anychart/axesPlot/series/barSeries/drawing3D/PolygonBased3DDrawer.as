package com.anychart.axesPlot.series.barSeries.drawing3D {
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.BackgroundBaseStyleState3D;
	import com.anychart.styles.states.BackgroundBaseStyleState3DEntry;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	
	internal class PolygonBased3DDrawer extends Bar3DDrawerBase {
		
		protected var points:Array;
		
		
		
		public function PolygonBased3DDrawer() {
			super();
			
			this.points = new Array(8);
			this.fillPoints(this.points);
			
			this.leftBounds = new Rectangle();
			this.rightBounds = new Rectangle();
		}
		
		override protected function initializeBounds():void {
			this.setBoundsFromPoints(this.leftBounds, this.createSide(Sides.LEFT));
			this.setBoundsFromPoints(this.rightBounds, this.createSide(Sides.RIGHT));
			this.setBoundsFromPoints(this.topBounds, this.createSide(Sides.TOP));
			this.setBoundsFromPoints(this.bottomBounds, this.createSide(Sides.BOTTOM));
			this.setBoundsFromPoints(this.frontBounds, this.createSide(Sides.FRONT));
			this.setBoundsFromPoints(this.backBounds, this.createSide(Sides.BACK));
		}
		
		protected function fillPoints(points:Array):void {
			for (var i:uint = 0;i<points.length;i++) {
				points[i] = new Point(0,0);
			}
		} 
		
		private function setBoundsFromPoints(bounds:Rectangle, pts:Array):void {
			
			var leftTop:Point = new Point(Number.MAX_VALUE,Number.MAX_VALUE);
			var rightBottom:Point = new Point(-Number.MAX_VALUE,-Number.MAX_VALUE);
			
			for each (var point:Point in pts) {
				leftTop.x = Math.min(point.x, leftTop.x);
				leftTop.y = Math.min(point.y, leftTop.y);
				rightBottom.x = Math.max(point.x, rightBottom.x);
				rightBottom.y = Math.max(point.y, rightBottom.y);
			}
			
			bounds.x = leftTop.x;
			bounds.y = leftTop.y;
			bounds.width = rightBottom.x - leftTop.x;
			bounds.height = rightBottom.y - leftTop.y;
		}
		
		private function createSide(side:uint):Array{
			var res:Array = new Array(4);
			switch (side){
				case Sides.FRONT:
					res[0] = this.points[0];
					res[1] = this.points[1];
					res[2] = this.points[2];
					res[3] = this.points[3];
					break;
				case Sides.BACK:
					res[0] = this.points[4];
					res[1] = this.points[5];
					res[2] = this.points[6];
					res[3] = this.points[7];
					break;
				case Sides.TOP:
					res[0] = this.points[1];
					res[1] = this.points[5];
					res[2] = this.points[6];
					res[3] = this.points[2];
					break;
				case Sides.BOTTOM:
					res[0] = this.points[0];
					res[1] = this.points[4];
					res[2] = this.points[7];
					res[3] = this.points[3];
					break;
				case Sides.LEFT:
					res[0] = this.points[0];
					res[1] = this.points[1];
					res[2] = this.points[5];
					res[3] = this.points[4];
					break;
				case Sides.RIGHT:
					res[0] = this.points[3];
					res[1] = this.points[2];
					res[2] = this.points[6];
					res[3] = this.points[7];
					break;
			}
			return res;
		}
		
		public function drawFrontSide(g:Graphics):void {
			this.drawSide(this.createSide(Sides.FRONT), this.frontBounds, g);
		}
		
		public  function drawLeftSide(g:Graphics):void {
			var isLeftAtLeft:Boolean = this.leftBounds.x < this.rightBounds.x;
			
			this.drawSide(this.createSide(isLeftAtLeft ? Sides.LEFT : Sides.RIGHT), isLeftAtLeft ? this.leftBounds : this.rightBounds, g);
		}
		
		public function drawRightSide(g:Graphics):void {
			var isRightAtRight:Boolean = this.leftBounds.x < this.rightBounds.x;
			this.drawSide(this.createSide(isRightAtRight ? Sides.RIGHT : Sides.LEFT), isRightAtRight ? this.rightBounds : this.leftBounds, g);
		}
		
		public function drawTopSide(g:Graphics):void {
			var isTopAtTop:Boolean = this.topBounds.y <= this.bottomBounds.y;
			this.drawSide(this.createSide(isTopAtTop ? Sides.TOP : Sides.BOTTOM), isTopAtTop ? this.topBounds : this.bottomBounds, g);
		}
		
		public function drawBottomSide(g:Graphics):void {
			var isBottomAtBottom:Boolean = this.topBounds.y <= this.bottomBounds.y;
			this.drawSide(this.createSide(isBottomAtBottom ? Sides.BOTTOM : Sides.TOP), isBottomAtBottom ? this.bottomBounds : this.topBounds, g);
		}
		
		public function drawBackSide(g:Graphics):void {
			this.drawSide(this.createSide(Sides.BACK), this.backBounds, g);
		}
		
		private function drawSide(pts:Array, bounds:Rectangle, g:Graphics):void {
			g.moveTo(pts[0].x, pts[0].y);
			g.lineTo(pts[1].x, pts[1].y);
			g.lineTo(pts[2].x, pts[2].y);
			g.lineTo(pts[3].x, pts[3].y);
			g.lineTo(pts[0].x, pts[0].y);
		}
		
		protected function checkGeom(leftTop:Point, rightBottom:Point):void {
			var tmp:Number;
			if (leftTop.x > rightBottom.x) {
				tmp = leftTop.x;
				leftTop.x = rightBottom.x;
				rightBottom.x = tmp;
			}
			 if (leftTop.y > rightBottom.y) {
				tmp = leftTop.y;
				leftTop.y = rightBottom.y;
				rightBottom.y = tmp;
			} 
		} 
	}
}

final class Sides {
	public static const FRONT:uint = 0;
	public static const BACK:uint = 1;
	public static const TOP:uint = 2;
	public static const BOTTOM:uint = 3;
	public static const LEFT:uint = 4;
	public static const RIGHT:uint = 5;
}