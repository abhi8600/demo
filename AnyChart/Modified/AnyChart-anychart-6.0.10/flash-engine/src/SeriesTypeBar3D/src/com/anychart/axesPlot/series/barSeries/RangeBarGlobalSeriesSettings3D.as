package com.anychart.axesPlot.series.barSeries{
	

	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.series.barSeries.drawing.IBarDrawingFactory;
	import com.anychart.axesPlot.series.barSeries.drawing3D.Bar3DDrawerFactory;
	import com.anychart.axesPlot.series.barSeries.programmaticStyles.Box3DProgramaticStyle;
	import com.anychart.data.SeriesType;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	
	public class RangeBarGlobalSeriesSettings3D extends RangeBarGlobalSeriesSettings{
		
		internal var drawingFactory:IBarDrawingFactory;
		
		override public function getProgrammaticStyleClass(styleNode:XML, state:StyleState):ProgrammaticStyle {
			var boxSt:Box3DProgramaticStyle = new Box3DProgramaticStyle();
			boxSt.initialize(state);
			return boxSt;
		}
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:RangeBar3DSeries = new RangeBar3DSeries();
			series.type = SeriesType.RANGE_BAR;
			series.clusterKey = SeriesType.RANGE_BAR;
			return series;
		}
		
		override public function initialize(plot:SeriesPlot):void {
			super.initialize(plot);
			this.drawingFactory = new Bar3DDrawerFactory(AxesPlot3D(plot).isHorizontal);
		}
		
		override public function getStyleNodeName():String { return "bar_style"; }
		override public function getStyleStateClass():Class { return BackgroundBaseStyleState; }
		override public function isProgrammaticStyle(styleNode:XML):Boolean { return false; }
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils3D;
			return true;
		} 
	}
}