package com.anychart.axesPlot.series.barSeries.pointsOrdering {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.visual.layout.Position;
	
	internal final class VerticalPointsOrderer extends BasePointsOrderer {
		
		override protected function initialize():void {
			this.isArgumentAxisLeftToRight = true;
			
			var valueAxisAtRight:Boolean = this.plot.getPrimaryValueAxis().position == Position.RIGHT;
			var argumentAxisInverted:Boolean = this.plot.getPrimaryArgumentAxis().scale.inverted;
			
			if ((valueAxisAtRight && !argumentAxisInverted) || (!valueAxisAtRight && argumentAxisInverted)) 
				this.isArgumentAxisLeftToRight = false;
				
			super.initialize();
		}		
		
		override protected function isInvertedStackInit(axis:Axis):Boolean {
			var isArgumentAxisAtTop:Boolean = plot.getPrimaryArgumentAxis().position == Position.TOP;
			var isScaleInverted:Boolean = axis.scale.inverted;
			return (!isArgumentAxisAtTop && isScaleInverted) || (isArgumentAxisAtTop && !axis.scale.inverted);
		}
	}
}