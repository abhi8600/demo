package com.anychart.axesPlot.series.barSeries.programmaticStyles{
	
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.BarPoint3D;
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.axesPlot.series.barSeries.drawing3D.IBar3DDrawer;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;

	public class Base3DProgramaticStyle extends ProgrammaticStyle{
		
		override public final function draw(data:Object):void{
			var point:I3DObject = I3DObject(data);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(AxesPlotPoint(point).styleState);
			var g:Graphics = AxesPlotPoint(point).container.graphics;
			var drawer:IBar3DDrawer = IBar3DDrawer(I3DObject(point).getDrawer());
			
			this.drawBackSide(AxesPlotPoint(point));
			
			if (point.isTopSideVisible) {
				this.drawBottomSide(AxesPlotPoint(point));
				if (point.isLeftSideVisible) {
					this.drawRightSide(AxesPlotPoint(point));
					this.drawLeftSide(AxesPlotPoint(point));
				}else {
					this.drawLeftSide(AxesPlotPoint(point));
					this.drawRightSide(AxesPlotPoint(point));
				}
				this.drawTopSide(AxesPlotPoint(point));
			}else {
				this.drawTopSide(AxesPlotPoint(point));
				if (point.isLeftSideVisible) {
					this.drawRightSide(AxesPlotPoint(point));
					this.drawLeftSide(AxesPlotPoint(point));
				}else {
					this.drawLeftSide(AxesPlotPoint(point));
					this.drawRightSide(AxesPlotPoint(point));
				}
				this.drawBottomSide(AxesPlotPoint(point));
			}
			
			this.drawFrontSide(AxesPlotPoint(point));
	 
		}
		
		private function drawHachFill(drawSide:Function,point:AxesPlotPoint):void {
			if (BackgroundBaseStyleState(point.styleState).hatchFill == null) return;
			
			BackgroundBaseStyleState(point.styleState).hatchFill.beginFill(point.container.graphics,point.hatchType,point.color);
			drawSide(point);
			point.container.graphics.endFill();
		}
		
		protected function drawFrontSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawFrontSide, point);
		}
		
		protected function execDrawFrontSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawFrontSide(point.container.graphics);
		}
		
		
		protected function drawBackSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawBackSide, point);
		}
		
		protected function execDrawBackSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawBackSide(point.container.graphics);
		} 
		
		protected function drawLeftSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawLeftSide,point);
		}
		
		protected function execDrawLeftSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawLeftSide(point.container.graphics);
		}
		
		protected function drawRightSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawRightSide,point);
		}
		protected function execDrawRightSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawRightSide(point.container.graphics);
		}
		 
		
		protected function drawTopSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawTopSide,point);
		}
		protected function execDrawTopSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawTopSide(point.container.graphics);
		}
		
		protected function drawBottomSide(point:AxesPlotPoint):void {
			this.drawHachFill(this.execDrawBottomSide,point);
		} 
		protected function execDrawBottomSide(point:AxesPlotPoint):void {
			IBar3DDrawer(I3DObject(point).getDrawer()).drawBottomSide(point.container.graphics);
		} 
	}
}