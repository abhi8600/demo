package com.anychart.axesPlot.series.barSeries.programmaticStyles{
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.axesPlot.series.barSeries.I3DObject;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.color.ColorParser;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.stroke.Stroke;

	public final class Box3DProgramaticStyle extends Base3DProgramaticStyle{
		
		private var frontGrd:Gradient;
		private var backGrd:Gradient;
		
		private var border:Stroke;
		
		private var leftGrd:Gradient;
		private var leftHorGrd:Gradient;
		private var leftGrdInv:Gradient;
		private var leftHorGrdInv:Gradient;  
		
		private var rightGrd:Gradient;
		private var rightHorGrd:Gradient;
		private var rightGrdInv:Gradient;
		private var rightHorGrdInv:Gradient;
		
		private var topGrd:Gradient;
		private var topHorGrd:Gradient;
		private var topGrdInv:Gradient;
		private var topHorGrdInv:Gradient;
		
		private var bottomGrd:Gradient;
		private var bottomHorGrd:Gradient;
		private var bottomGrdInv:Gradient;
		private var bottomHorGrdInv:Gradient;
		
		private var blend1Index:uint;
		private var blend2Index:uint;
		private var blend1IndexHor:uint;
		private var blend2IndexHor:uint;
		private var entries:Array;    
		
		override public function initialize(stateBase:StyleState):void {
			this.initDynamicIndexes(stateBase,ColorUtils.getDarkColor,ColorUtils.getLightColor);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(stateBase);
			var opacity:Number= state.fill ? state.fill.opacity : 1;

			var isDynamic:Boolean = state.fill.isDynamic;
			this.entries = [];
			if (isDynamic) {//front
				this.getBlendIndexes("Blend(%Color, LightColor(%Color), 0.6)","Blend(%Color, DarkColor(%Color), 0)");
			
				this.frontGrd = new Gradient(1);
				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.frontGrd,state,opacity);
			}else {
				this.frontGrd = new Gradient(1);
				this.execCreateGradient(this.frontGrd,state,opacity,"Blend(%Color, LightColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0)");
			}
			
			{//back
				if (isDynamic){
					this.getBlendIndexes(
									"Blend(%Color, DarkColor(%Color), 0)",
									"Blend(%Color, DarkColor(%Color), 0)");
					this.backGrd = new Gradient(1);
       				this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.backGrd,state,opacity * .3);	
				}else {
					this.backGrd = new Gradient(1);
					this.execCreateGradient(this.backGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0)","Blend(%Color, DarkColor(%Color), 0)");
				}
				
			}
			
			{//left
				if (isDynamic){
					this.getBlendIndexes("Blend(%Color, #0x333333, 0.7)",
										 "Blend(DarkColor(%Color), 0, 0.7)",
										 "Blend(%Color, DarkColor(%Color), 0.6)",
										 "Blend(%Color, DarkColor(%Color), 0.6)");
				
					this.leftGrd = new Gradient(1);
					this.leftHorGrd = new Gradient(1);
					this.leftGrdInv = new Gradient (1);
					this.leftHorGrdInv = new Gradient (1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.leftGrd,state,opacity);
	        		this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.leftHorGrd,state,opacity);
					this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.leftHorGrdInv,state,opacity*0.3);
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.leftGrdInv,state,opacity*0.3);
				}else {
					this.leftGrd = new Gradient(1);
					this.leftHorGrd = new Gradient(1);
					this.leftGrdInv = new Gradient (1);
					this.leftHorGrdInv = new Gradient (1);
					
					this.execCreateGradient(this.leftGrd,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.leftHorGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
					this.execCreateGradient(this.leftGrdInv,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.leftHorGrdInv,state,opacity,"Blend(%Color, DarkColor(%Color), 0.5)","Blend(%Color, DarkColor(%Color), 0.5)");
				}
			}
			
			{//right
				if (isDynamic){
					this.getBlendIndexes("Blend( %Color, #0x333333, 0.7)",
										 "Blend( DarkColor(%Color), 0, 0.7)",
										 "Blend(%Color, DarkColor(%Color), 0.7)",
										 "Blend(%Color, DarkColor(%Color), 0.7)");
					
					this.rightGrd = new Gradient(1);
					this.rightHorGrd = new Gradient(1);
					this.rightGrdInv = new Gradient (1);
					this.rightHorGrdInv = new Gradient (1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.rightGrd,state,opacity);
	        		this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.rightHorGrd,state,opacity);
					this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.rightHorGrdInv,state,opacity*0.3);
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.rightGrdInv,state,opacity*0.3);
				}else {
					this.rightGrd = new Gradient(1);
					this.rightHorGrd = new Gradient(1);
					this.rightGrdInv = new Gradient (1);
					this.rightHorGrdInv = new Gradient (1);
					
					this.execCreateGradient(this.rightGrd,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.rightGrdInv,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.rightHorGrd,state,opacity,"Blend(%Color, DarkColor(%Color), 0.6)","Blend(%Color, DarkColor(%Color), 0.6)");
					this.execCreateGradient(this.rightHorGrdInv,state,opacity,"Blend(%Color, DarkColor(%Color), 0.6)","Blend(%Color, DarkColor(%Color), 0.6)");
				}
			}
			
			{//top
				if (isDynamic){
					this.getBlendIndexes("Blend( %Color, DarkColor(%Color), 0.7)",
										 "Blend( %Color, DarkColor(%Color), 0.6)",
										 "Blend(%Color, #0x333333, 0.7)",
										 "Blend(DarkColor(%Color), 0, 0.7)");
					
					this.topGrd = new Gradient(1);
					this.topHorGrd = new Gradient(1);
					this.topGrdInv = new Gradient (1);
					this.topHorGrdInv = new Gradient (1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.topGrd,state,opacity);
	        		this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.topHorGrd,state,opacity);
					this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.topHorGrdInv,state,opacity*0.3);
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.topGrdInv,state,opacity*0.3);
				}else {
					this.topGrd = new Gradient(1);
					this.topHorGrd = new Gradient(1);
					this.topGrdInv = new Gradient (1);
					this.topHorGrdInv = new Gradient (1);
					
					this.execCreateGradient(this.topGrd,state,opacity,"Blend( %Color, DarkColor(%Color), 0.6)","Blend( %Color, DarkColor(%Color), 0.5)");
					this.execCreateGradient(this.topGrdInv,state,opacity,"Blend( %Color, DarkColor(%Color), 0.6)","Blend( %Color, DarkColor(%Color), 0.5)");
					this.execCreateGradient(this.topHorGrd,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.topHorGrdInv,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
				}
			}
			
			{//bottom
				if (isDynamic){
					this.getBlendIndexes("Blend( %Color, DarkColor(%Color), 0.6)",
										 "Blend( %Color, DarkColor(%Color), 0.6)",
										 "Blend(%Color, #0x333333, 0.7)",
										 "Blend(DarkColor(%Color), 0, 0.7)");
	
					this.bottomGrd = new Gradient(1);
					this.bottomHorGrd = new Gradient(1);
					this.bottomGrdInv = new Gradient (1);
					this.bottomHorGrdInv = new Gradient (1);
					entries=[];
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.bottomGrd,state,opacity*0.3);
	        		this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.bottomHorGrd,state,opacity*0.3);
					this.execCreateDinamicGradient(this.blend1IndexHor,this.blend2IndexHor,this.bottomHorGrdInv,state,opacity);
					this.execCreateDinamicGradient(this.blend1Index,this.blend2Index,this.bottomGrdInv,state,opacity);
				}else{
					this.bottomGrd = new Gradient(1);
					this.bottomHorGrd = new Gradient(1);
					this.bottomGrdInv = new Gradient (1);
					this.bottomHorGrdInv = new Gradient (1);
					
					this.execCreateGradient(this.bottomGrd,state,opacity,"Blend( %Color, DarkColor(%Color), 0.5)","Blend( %Color, DarkColor(%Color), 0.5)");
					this.execCreateGradient(this.bottomGrdInv,state,opacity,"Blend( %Color, DarkColor(%Color), 0.5)","Blend( %Color, DarkColor(%Color), 0.5)");
					this.execCreateGradient(this.bottomHorGrd,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
					this.execCreateGradient(this.bottomHorGrdInv,state,opacity,"Blend(%Color, #0x333333, 0.6)","Blend(DarkColor(%Color), 0, 0.6)");
				}
			}
		}
		
		private function getBlendIndexes(a1:String = null,a2:String = null,a3:String = null,a4:String = null):void{
			if (!a1 && !a2 && !a3 && !a4) return;
			
			if (a1)this.blend1Index = ColorParser.getInstance().parse(a1);
			if (a2)this.blend2Index = ColorParser.getInstance().parse(a2);
			if (a3)this.blend1IndexHor = ColorParser.getInstance().parse(a3);
			if (a4)this.blend2IndexHor = ColorParser.getInstance().parse(a4);
		}
		
		private function execCreateDinamicGradient(index1:uint,index2:uint,grd:Gradient,state:BackgroundBaseStyleState,opacity:Number):void{
				this.entries.push(createKey(0,index1,opacity,true));
        		this.entries.push(createKey(1,index2,opacity,true));
       			this.createDynamicGradient(state,grd,entries);
				this.entries=[];
		}
		
		private function execCreateGradient(grd:Gradient,state:BackgroundBaseStyleState,opacity:Number,color1:String,color2:String):void {
			if (!color1 && !color2) return;
			if (color1 && color2)
				grd.deserialize(<gradient>
						<key color={color1.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
						<key color={color2.split("%Color").join("#"+state.fill.color.toString(16))} opacity={opacity} />
					</gradient>);
		} 
		
		override protected function drawFrontSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
				this.frontGrd.angle = point.isInverted ? 0 : 180;
			}else{
				this.frontGrd.angle = 90;	
			}
			
			this.frontGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
			this.execDrawFrontSide(point);
			point.container.graphics.endFill();
			
			super.drawFrontSide(point);
		}
		
		override protected function drawBackSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
				this.backGrd.angle = point.isInverted ? 0 : 180;
			}else{
				this.backGrd.angle = 90;	
			}
			
			this.backGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
			this.execDrawBackSide(point);
			point.container.graphics.endFill();
			
			super.drawBackSide(point);
		}
		
		override protected function drawLeftSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
				this.leftHorGrd.angle = -45;
				if (!I3DObject(point).isLeftSideVisible)
					this.leftHorGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.leftHorGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawLeftSide(point);
				point.container.graphics.endFill();
			}else{
				this.leftGrd.angle = 90;
				if (!I3DObject(point).isLeftSideVisible)
					this.leftGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.leftGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawLeftSide(point);
				point.container.graphics.endFill();
			}
			super.drawLeftSide(point);
		}
		
		override protected function drawRightSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
					this.rightHorGrd.angle = 45;
				if (I3DObject(point).isLeftSideVisible)
					this.rightHorGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.rightHorGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawRightSide(point);
				point.container.graphics.endFill();
			}else{
					this.rightGrd.angle = 90;
				if (I3DObject(point).isLeftSideVisible)
					this.rightGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.rightGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawRightSide(point);
				point.container.graphics.endFill();
			}
			super.drawRightSide(point);
		}
		
		override protected function drawTopSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
					this.topHorGrd.angle = point.isInverted ? 0:180;
				if (!I3DObject(point).isTopSideVisible)
					this.topHorGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.topHorGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawTopSide(point);
				point.container.graphics.endFill();
			}else{
				this.topGrd.angle = -45;
				if (!I3DObject(point).isTopSideVisible)
					this.topGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.topGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawTopSide(point);
				point.container.graphics.endFill();
			}
			super.drawTopSide(point);
		}
		override protected function drawBottomSide(point:AxesPlotPoint):void {
			if (point.isHorizontal){
					this.bottomHorGrdInv.angle = point.isInverted ? 0:180;
				if (!I3DObject(point).isTopSideVisible)
					this.bottomHorGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.bottomHorGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawBottomSide(point);
				point.container.graphics.endFill();
			}else{
				this.bottomGrd.angle = -45;
				if (!I3DObject(point).isTopSideVisible)
					this.bottomGrdInv.beginGradientFill(point.container.graphics, point.bounds, point.color);
				else
					this.bottomGrd.beginGradientFill(point.container.graphics, point.bounds, point.color);
				this.execDrawBottomSide(point);
				point.container.graphics.endFill();
			}
			super.drawBottomSide(point);
		}
		
		private function drawHachFill(drawSide:Function,point:AxesPlotPoint):void {
			if (BackgroundBaseStyleState(point.styleState).hatchFill == null) return;
			
			BackgroundBaseStyleState(point.styleState).hatchFill.beginFill(point.container.graphics,point.hatchType,point.color);
			drawSide(point);
			point.container.graphics.endFill();
		}
	}
}