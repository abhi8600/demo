package com.anychart.axesPlot.series.barSeries.drawing3D{
	import com.anychart.axesPlot.axes.categorization.StackSettings;
	import com.anychart.axesPlot.data.AxesPlotSeries;
	import com.anychart.axesPlot.series.barSeries.BarPoint3D;
	import com.anychart.scales.ScaleMode;
	
	internal final class Cone3DDrawerHorizontal extends ElipseBased3DDrawerHorizontal implements IBar3DDrawer{
		
		public function Cone3DDrawerHorizontal() {
			super();
		}
		
		override protected function initializeSides():void {
			super.initializeSides();
			
			if (this.isStacked())
				this.initializeStacked();
			else 
				this.initializeNormal();
		}
		
		private function initializeNormal():void {
			this.bottomXRadius = 0;
			this.bottomYRadius = 0; 
		}
		
		private function initializeStacked ():void {
			var stackSettings:StackSettings = this.point.category.getStackSettings(AxesPlotSeries(this.point.series).valueAxis.name, this.point.series.type,AxesPlotSeries(this.point.series).valueAxis.scale.mode == ScaleMode.PERCENT_STACKED);
			var stackSumm:Number = stackSettings.getStackSumm(BarPoint3D(this.point).y);
											
			this.topXRadius*= (stackSumm - this.startValue)/stackSumm;
			this.bottomXRadius*= (stackSumm - this.endValue)/stackSumm;
			
			this.topYRadius*=(stackSumm - this.startValue)/stackSumm;
			this.bottomYRadius*=(stackSumm - this.endValue)/stackSumm; 
			
		}
		
		override public function initializePixels(startValue:Number, endValue:Number):void {
			this.startValue = startValue;
			this.endValue = endValue;
			super.initializePixels(startValue,endValue);
		}
	}
}