package com.anychart.export {
	import com.anychart.IAnyChart;
	import com.anychart.encoders.Base64Encoder;
	import com.anychart.encoders.PNGEncoder;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	public final class ImageExporter extends ExporterBase {
		
		public function ImageExporter(chart:IAnyChart) {
			super(chart);
		}
		
		override public function setDefaults():void {
			super.setDefaults();
			this.contextMenuItemText = "Save as image...";
			this.extension = "png";
			this.url = "http://anychart.com/products/anychart/saveas/PNGSaver.php";
		}
		
		override protected function getBase64Data():String {
			var currentBounds:Rectangle = this.chart.getBounds();
			var bitmapData:BitmapData = new BitmapData(currentBounds.width, currentBounds.height);
			bitmapData.draw(chart.getContainer());
						
	        return Base64Encoder.encode64(PNGEncoder.PNGencode(bitmapData), true);
		}
		
		public function executeExternalSaving(width:Number = NaN, height:Number = NaN):void {
			var fileName:String = this.getFileName();
			
			var urlRequest:URLRequest = new URLRequest(this.url);
			urlRequest.method = URLRequestMethod.POST;
			
			var variables:URLVariables = new URLVariables();
			variables.file = this.getData(width, height);
			variables.name = fileName;
			urlRequest.data = variables;
			
			navigateToURL(urlRequest,"_self");			
		}
		
		override protected function executeSaving():void {
			
			var fileName:String = this.getFileName();
			
			var urlRequest:URLRequest = new URLRequest(this.url);
	        urlRequest.method = URLRequestMethod.POST;
			
	        var variables:URLVariables = new URLVariables();
	        variables.file = this.getData();
	        
    	    urlRequest.data = variables;
    	    
    	    var ref:FileReference = new FileReference();
    	    
    	    function downloadFile():void {
    	    	ref.download(urlRequest, fileName);
    	    }
    	    ref.addEventListener(Event.SELECT, this.startShowingPreloader);
    	    downloadFile();
		}
		
		private function startShowingPreloader(e:Event):void {
			var ref:FileReference = FileReference(e.target);
    	    ref.addEventListener(Event.CANCEL, this.removePreloader);
    	    ref.addEventListener(Event.COMPLETE, this.removePreloader);
    	    this.showPreloader();
		}
		
		override public function deserializeContextMenu(data:XML):void {
			if (data.@save_as_image != undefined)
				this.enabled = SerializerBase.getBoolean(data.@save_as_image);
			if (data.save_as_image_item_text[0] != null)
				this.contextMenuItemText = SerializerBase.getCDATAString(data.save_as_image_item_text[0]);
		}
	}
}