package com.anychart.printing {
	import com.anychart.IAnyChart;
	import com.anychart.context.ContextMenuElement;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.printing.PrintJob;
	
	
	public final class ChartPrinter extends ContextMenuElement {
		
		public function ChartPrinter(chart:IAnyChart) {
			super(chart);
		}
		
		override public function setDefaults():void {
			super.setDefaults();
			this.contextMenuItemText = "Print chart...";
		}
		
		override public function execute():void {
			var job:PrintJob = new PrintJob();
			if (job.start()) {
				
				var bounds:Rectangle = this.chart.getBounds().clone();
				
				var printWidth:Number = bounds.width;
				var printHeight:Number = bounds.height;
				
				var scale:Number=1;
				
				var xScale:Number = (job.paperWidth-20)/printWidth;
				var yScale:Number = (job.paperHeight-20)/printHeight;
				
				scale=Math.min(xScale,yScale);
				
				printWidth*=scale;
				printHeight*=scale;
				
				var printBounds:Rectangle = new Rectangle(bounds.x, bounds.y, printWidth, printHeight); 
				
				this.chart.resize(printBounds);
				
				var data:BitmapData = new BitmapData(printWidth, printHeight, false, 0xFFFFFFFF);
				data.draw(this.chart.getContainer());
				
				var bmp:Bitmap = new Bitmap(data);
				var sp:Sprite = new Sprite();
				sp.addChild(bmp);
				job.addPage(sp, printBounds);
				job.send();
				
				this.chart.resize(bounds);
			}
		}
		
		override public function deserializeContextMenu(data:XML):void {
			if (data.@print_chart != undefined)
				this.enabled = SerializerBase.getBoolean(data.@print_chart);
			if (data.print_chart_item_text[0] != null)
				this.contextMenuItemText = SerializerBase.getCDATAString(data.print_chart_item_text[0]);
		}
	}
}