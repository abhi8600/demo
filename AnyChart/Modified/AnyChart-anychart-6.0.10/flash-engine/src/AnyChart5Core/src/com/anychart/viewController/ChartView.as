package com.anychart.viewController {
	import com.anychart.AnyChart;
	import com.anychart.Chart;
	import com.anychart.animation.event.AnimationEvent;
	import com.anychart.dashboard.Dashboard;
	import com.anychart.errors.ErrorMessage;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.plot.PlotFactoryBase;
	import com.anychart.preloader.Preloader;
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.utils.IXMLPreprocessor;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.geom.Rectangle;
	
	public class ChartView extends EventDispatcher {
		
		public var isInRefresh:Boolean;
		
		public var container:Sprite;
		private var elementContainer:DisplayObject;
		protected var preloaderContainer:DisplayObject;
		
		public var base:AnyChart;
		protected var bounds:Rectangle;
		
		public var resources:ResourcesLoader;
		
		public var element:IViewElement;
		protected var preloader:Preloader;
		
		public var xmlFilePath:String;
		private var cacheXML:Boolean;
		public function setXMLCacheEnabled(value:Boolean):void { this.cacheXML = value;	}
		
		private var xmlPreprocessor:IXMLPreprocessor;
		public function setXMLPreprocessor(value:IXMLPreprocessor):void { this.xmlPreprocessor = value; }
		
		public var xmlDataRaw:String;
		
		protected var status:uint;
		protected var anychartXMLData:XML;
		
		public function ChartView(base:AnyChart) {
			this.isInRefresh = false;
			this.cacheXML = false;
			this.bounds = bounds;
			this.base = base;
			
			if (this is AnyChart) {
				this.resources = new ResourcesLoader();
			}else {
				this.resources = base.resources.createCopy();
				this.xmlPreprocessor = ChartView(base).xmlPreprocessor;
			}
			this.resources.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.showSecurityError);
			this.resources.addEventListener(IOErrorEvent.IO_ERROR, this.showIOError);
			this.resources.addEventListener(ProgressEvent.PROGRESS, this.progressHandler);
			this.preloader = new Preloader();
		}
		
		public function initialize(bounds:Rectangle):void {
			this.bounds = bounds;
			this.preloaderContainer = this.preloader.initialize(bounds);
			if (this.xmlFilePath != null)
				this.setXMLFile(this.xmlFilePath, null);
			else if (this.xmlDataRaw != null)
				this.setXMLData(this.xmlDataRaw, null);
			else 
				this.showWaitingForData();
		}
		
		//-----------------------------------------------------
		//XML File loading
		//----------------------------------------------------- 
		
		private var replaces:Array;
		
		public function setXMLFile(path:String, replaces:Array = null, needClear:Boolean = true):void {
			this.setDefaults();
			this.clear();
			this.resources.stop();
			
			this.xmlFilePath = path;
			this.resources.addText(path, this.cacheXML);
			if (this.resources.isLoaded()) {
				this.setXMLData(ResourcesHashMap.getText(path), replaces);
			}else {
				this.replaces = replaces;
				this.resources.addEventListener(Event.COMPLETE, this.xmlFileLoadHandler);
				this.showPreloader(this.base.messageOnLoadingXML, false);
			}
		}
		
		private function xmlFileLoadHandler(event:Event):void {
			this.preloader.setProgress(1);
			this.resources.removeEventListener(Event.COMPLETE, this.xmlFileLoadHandler);
			this.setXMLData(ResourcesHashMap.getText(this.xmlFilePath), this.replaces);
			this.replaces = null;
		}
		
		//-----------------------------------------------------
		//XML Data converting
		//-----------------------------------------------------
		
		protected final function getXML(rawData:String):XML {
			var data:XML = null;
			try {
				data = new XML(rawData);
			}catch (e:Error) {
				this.showXMLError(e);
			}
			return data;
		}
		
		public function setXMLData(rawData:String, replaces:Array = null, needClear:Boolean = true):void {
		
			this.setDefaults();
			this.clear();
			this.resources.stop();
			
			this.showInitializing();
			
			if (replaces != null) {
				for (var i:int = 0;i<replaces.length;i++) {
					rawData = rawData.split(replaces[i][0]).join(replaces[i][1]);
				}
			}
			
			var data:XML = this.getXML(rawData);
			if (data != null) {
				this.processXML(data);
			}
		}
		
		public final function setXML(data:XML):void {
			this.setDefaults();
			this.clear();
			this.showInitializing();
			this.processXML(data);
		}
		
		protected function processXML(data:XML):XML {
			
			if (data == null || data.name() == null) {
				this.showNoData();
				return null;
			}
			
			if (this.xmlPreprocessor)
				data = this.xmlPreprocessor.process(data);
			
			var nodeName:String = data.name().toString();
			var collectionName:String = nodeName+"s";
			
			var newData:XML;
			if (nodeName != "anychart" && data[collectionName][0] == null) {
				newData = <anychart>
					<{collectionName}>{data}</{collectionName}>
				</anychart>;
			}else {
				newData = data;
			}
			this.anychartXMLData = newData;
			return newData;
			//can load additional information
			//should call generateChart()
		}
		
		protected final function generateChart(data:XML):void {
			if (AnyChart.ENABLE_ERROR_REPORTING) {
				try {
					this.doGenerateChart(data);
				}catch (e:Error) {
					//FATAL ERROR 
					this.showErrorMessage(ErrorMessage.getFatalError(e));
					this.dispatchDrawEvent();
				}
			}else {
				this.doGenerateChart(data);
			}
		}
		
		private function doGenerateChart(data:XML):void {
			try {
				this.createElement(data);
				if (this.resources.isLoaded()) {
					this.showElement();
				}else {
					this.resources.addEventListener(Event.COMPLETE, this.resourcesLoadHandler);
					this.showPreloader(this.base.messageOnLoadingResources, true);
				}
			}catch (e:FeatureNotSupportedError) {
				if (PlotFactoryBase.IS_ORACLE)
					this.showErrorMessage(e.failMessage, e.url);
				else
					throw e;
			}
		}
		
		//--------------------------------------------------------
		//	Resources
		//--------------------------------------------------------
		
		private function resourcesLoadHandler(event:Event):void {
			this.resources.removeEventListener(Event.COMPLETE, this.resourcesLoadHandler);
			this.preloader.setProgress(1);
			this.removePreloader();
			this.showElement();
		}
		
		//-----------------------------------------------------
		//Resize
		//-----------------------------------------------------
		
		public function resize(newBounds:Rectangle):void {
			this.bounds = newBounds;
			switch (this.status) {
				case ChartViewStatus.ELEMENT:
					this.element.resize(newBounds);
					break;
				case ChartViewStatus.PRELOADER:
					this.preloader.resize(newBounds);
					break;
				case ChartViewStatus.ERROR:
					this.showErrorMessage(this.errorMessage);
					break;
			}
		}
		
		//-----------------------------------------------------
		//CLEAR ALL
		//-----------------------------------------------------
		
		private function clear():void {
			if (this.container != null)
				this.container.graphics.clear();
			this.removeElement();
			this.removePreloader();
			if (this.errorMessageContainer != null) {
				this.errorMessageContainer.parent.removeChild(this.errorMessageContainer);
				this.errorMessageContainer = null;
			}
		}
		
		protected function setDefaults():void {
		}
		
		//-----------------------------------------------------
		//Element
		//-----------------------------------------------------
		
		protected function createElement(data:XML):void {
		}
		
		private function showElement():void {
			if (AnyChart.ENABLE_ERROR_REPORTING) {
				try {
					this.doShowElement();
				}catch (e:Error) {
					this.dispatchRenderEvent();
					this.showErrorMessage(ErrorMessage.getFatalError(e));
				}
			}else {
				this.doShowElement();
			}
			this.dispatchDrawEvent();
		}
		
		private function doShowElement():void {
			if (this.element == null) return;
			this.showInitializing();
			this.status = ChartViewStatus.ELEMENT;
			this.elementContainer = this.element.initialize(this.bounds);
			if (this.element is Dashboard)
				this.element.postInitialize();
			else if (this is DashboardChartElementView)
				Dashboard(DashboardChartElementView(this).base.element).checkPostInitialize();

			this.dispatchRenderEvent();
			this.element.draw();
			this.removePreloader();
			this.container.addChild(this.elementContainer);
		}
		
		protected function dispatchRenderEvent():void {
		}
		
		protected function dispatchDrawEvent():void {
		}
		
		protected function execDispatchDrawEvent():void {
		}
		
		protected function animationFinish(e:AnimationEvent):void {
			this.execDispatchDrawEvent();
		}
		
		private function removeElement():void {
			if (this.element != null) {
				this.element.destroy();
				this.element = null;
			}
			if (this.elementContainer != null) {
				if (this.elementContainer is DisplayObjectContainer) {
					while (DisplayObjectContainer	(this.elementContainer).numChildren > 0) {
						DisplayObjectContainer(this.elementContainer).removeChildAt(0);
					}
				}
				if (this.elementContainer.parent != null)
					this.elementContainer.parent.removeChild(this.elementContainer);
				this.elementContainer = null;
			}
		}
		
		//-----------------------------------------------------
		//Errors
		//-----------------------------------------------------
		
		private function showIOError(event:IOErrorEvent):void {
			this.showErrorMessage(ErrorMessage.getIOErrorMessage((this.resources.getLastTarget() != null) ? this.resources.getLastTarget().getPath() : null));
			this.dispatchDrawEvent();
		}
		
		public function showSecurityError(event:SecurityErrorEvent):void {
			this.showErrorMessage(ErrorMessage.SECURITY_ERROR, ErrorMessage.SECURITY_ERROR_URL);
		}
		
		public function showFatalError(e:Error):void {
			this.showErrorMessage(ErrorMessage.FATAL_ERROR);
		}
		
		private function showXMLError(error:Error):void {
			this.showErrorMessage(ErrorMessage.getXMLErrorMessage(error));
		}
		
		private var errorMessage:String;
		private var errorMessageContainer:DisplayObject;
		
		public function showErrorMessage(message:String, url:String = null):void {
			this.resources.stop();
			this.clear();
			this.status = ChartViewStatus.ERROR;
			this.errorMessage = message;
			
			if (this.elementContainer != null)
				this.elementContainer.parent.removeChild(this.elementContainer);
			
			this.errorMessageContainer = ErrorMessage.showErrorMessage(this.bounds, message, url);
			this.container.addChild(this.errorMessageContainer);
		}
		
		//-----------------------------------------------------
		//Static messages
		//-----------------------------------------------------
		
		public function setLoading(text:String):void {
			this.clear();
			this.showAnimatedPreloader(text);
		}
		
		private function showWaitingForData():void {
			this.showAnimatedPreloader(this.base.messageOnWaitingForData);
		}
		
		protected function showNoData():void {
			this.showAnimatedPreloader(this.base.messageNoData);
			this.dispatchDrawEvent();
		} 
		
		protected final function showInitializing():void {
			this.showAnimatedPreloader(this.base.messageOnInitialize);
		}
		
		//-----------------------------------------------------
		//Preloader
		//-----------------------------------------------------
		
		protected function showAnimatedPreloader(text:String):void {
			this.status = ChartViewStatus.PRELOADER;
			if (this.preloaderContainer && this.preloaderContainer.parent == null)
				this.container.addChild(this.preloaderContainer);
			this.preloader.showAnimated(text);
		}
		
		protected final function showPreloader(prefix:String, showProgress:Boolean):void {
			if (showProgress) {
				this.preloader.initProgress(prefix);
			}else {
				this.preloader.showAnimated(prefix);
			}
			if (this.preloaderContainer.parent == null)
				this.container.addChild(this.preloaderContainer);
			this.status = ChartViewStatus.PRELOADER;
			this.resources.load();
		}
		
		private function progressHandler(event:ProgressEvent):void {
			this.preloader.setProgress(event.bytesLoaded/event.bytesTotal);
		}
		
		protected final function removePreloader():void {
			if (this.preloader) this.preloader.stopAnimation();
			if (this.preloaderContainer != null && this.preloaderContainer.parent != null)
				this.preloaderContainer.parent.removeChild(this.preloaderContainer);
		}
		
		//---------------------------------------------------------------------------------
		//					External Interface Data Manipulation
		//---------------------------------------------------------------------------------
		 
		//-----------
		//plot
		//-----------
		 
		public function setPlotCustomAttribute(attributeName:String, attributeValue:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.setPlotCustomAttribute(attributeName, attributeValue);
		}
		
		public function updatePointData(groupName:String, pointName:String, data:Object):void {
			if (this.element is Chart)
				Chart(this.element).plot.updateData.apply(null,[groupName, pointName,data]);
		}
		
		//-----------
		//series
		//-----------
		
		public function addSeries(...seriesData):void {
			if (this.element is Chart)
				Chart(this.element).plot.addSeries(seriesData);
		}
		
		public function removeSeries(seriesId:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.removeSeries(seriesId);
		}
		
		public function addSeriesAt(index:uint, seriesData:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.addSeriesAt(index, seriesData);
		}
		
		public function updateSeries(seriesId:String, seriesData:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.updateSeries(seriesId, seriesData);
		}
		
		public function showSeries(seriesId:String, isVisible:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.showSeries(seriesId, isVisible);
		}
		
		//-----------
		//points
		//-----------
		
		public function addPoint(seriesId:String, ...pointsData):void {
			if (this.element is Chart)
				Chart(this.element).plot.addPoint(seriesId, pointsData);
		}
		
		public function removePoint(seriesId:String, pointId:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.removePoint(seriesId, pointId);
		}
		
		public function addPointAt(seriesId:String, index:uint, pointData:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.addPointAt(seriesId, index, pointData);
		}
		
		public function updatePoint(seriesId:String, pointId:String, pointData:String):void {
			if (this.element is Chart)
				Chart(this.element).plot.updatePoint(seriesId, pointId, pointData);
		}
		
		//-----------
		//refresh/clear
		//-----------
		
		public function refresh():void {
			if (this.element is Chart) {
				this.isInRefresh = true;
				Chart(this.element).plot.refresh();
			}
		}
		
		public function clearChartData():void {
			if (this.element is Chart)
				Chart(this.element).plot.clear();
		}
		
		//-----------
		//states
		//-----------
		
		public function highlightSeries(seriesId:String, highlighted:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.highlightSeries(seriesId, highlighted);
		}
		
		public function highlightPoint(seriesId:String, pointId:String, highlighted:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.highlightPoint(seriesId, pointId, highlighted);
		}
		
		public function highlightCategory(categoryName:String, highlighted:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.highlightCategory(categoryName, highlighted);
		}
		
		public function selectPoint(seriesId:String, pointId:String, selected:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.selectPoint(seriesId, pointId, selected);
		}
		
		//--------------
		// AxisMarkers
		//--------------
		public function showAxisMarker(markerId:String, isVisible:Boolean):void {
			if (this.element is Chart)
				Chart(this.element).plot.showAxisMarker(markerId, isVisible);
		}
	}
}