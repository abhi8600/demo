package com.anychart.context {
	import com.anychart.IAnyChart;
	import com.anychart.serialization.SerializerBase;
	
	import flash.events.ContextMenuEvent;
	import flash.ui.ContextMenuItem;
	
	public class ContextMenuElement {
		
		protected var enabled:Boolean;
		protected var chart:IAnyChart;
		protected var contextMenuItemText:String;
		
		public function ContextMenuElement(chart:IAnyChart) {
			this.chart = chart;
		}
		
		public function setDefaults():void {
			this.enabled = true;
		}
		
		public function deserializeContextMenu(data:XML):void {}
		
		public function execute():void {}
		
		
		public final function initializeContextMenu():void {
			if (!this.enabled) return;
			var item:ContextMenuItem = new ContextMenuItem(contextMenuItemText);
			item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, this.contextMenuHandler);
			if (this.chart.contextMenu && this.chart.contextMenu.customItems && this.chart.contextMenu.customItems != null)
				this.chart.contextMenu.customItems.push(item);
		}
		
		private function contextMenuHandler(event:ContextMenuEvent):void {
			this.execute();
		}
	}
}