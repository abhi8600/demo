package com.anychart.controls.legend {
	import com.anychart.controls.layout.ControlAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public final class RowBasedLegend extends LegendControl {
		public static function check(data:XML):Boolean {
			return data.name().toString() == "legend" && !ColumnBasedLegend.check(data);
		}
		
		//------------------------------------------------------------------
		//		Constructor
		//------------------------------------------------------------------
		
		public function RowBasedLegend() {
			super();
		}
		
		//------------------------------------------------------------------
		//		XML
		//------------------------------------------------------------------
		
		override public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(data, styles, resources);
		}
		
		//------------------------------------------------------------------
		//		Bounds
		//------------------------------------------------------------------
		
		private var maxRowWidth:Number;
		private var rows:Array;
		
		override protected function checkContentBounds():void {
			this.initializeItems();
			
			this.elementsAlign = this.realElementsAlign;
			this.removeScrollBar();
			
			this.fitToLayout();
			
			super.checkContentBounds();
		}
		
		private function fitToLayout():void {
			
			var maxWidth:Number = this.layout.isWidthSetted ? this.getContentWidth() : this.getMaxContentWidth();
			var maxHeight:Number = this.layout.isHeightSetted ? this.getContentHeight() : this.getMaxContentHeight();
			
			this.calcLayout(maxWidth, maxHeight);
			
			if (!this.layout.isWidthSetted)
				this.bounds.width = this.maxRowWidth + this.scrollBarSpace;
				
			if (!this.layout.isHeightSetted)
				this.bounds.height = Math.min(this.realContentHeight, maxHeight);
			
			if (this.realContentHeight > maxHeight) {
				this.createScrollBar();
				this.calcLayout(maxWidth - this.scrollBarSpace, maxHeight);
				
				if (!this.layout.isWidthSetted)
					this.bounds.width = this.maxRowWidth + this.scrollBarSpace;
					
				if (!this.layout.isHeightSetted)
					this.bounds.height = Math.min(this.realContentHeight, maxHeight);
			}
		}
		
		override protected function updateLayout():void {
			var maxWidth:Number = this.layout.isWidthSetted ? this.getContentWidth() : this.getMaxContentWidth();
			var maxHeight:Number = this.layout.isHeightSetted ? this.getContentHeight() : this.getMaxContentHeight();
			
			this.calcLayout(maxWidth, maxHeight);
		}
		
		private function calcLayout(maxWidth:Number, maxHeight:Number):void {
			
			this.maxRowWidth = 0;
			this.rows = [];
			
			var cnt:uint = this.visualItems.length;
			
			this.realContentHeight = 0;
			
			var x:Number = 0;
			var y:Number = 0;

			var isInNextRow:Boolean = false;
			var row:LegendRow = new LegendRow();
			row.y = y;
			
			for (var i:uint = 0;i<cnt;i++) {
				
				var item:LegendItem = this.visualItems[i];
				
				isInNextRow = this.isNotInCurrentRow(item, x, maxWidth);
				if (isInNextRow) {
					row.width = x - this.columnSpace;
					this.maxRowWidth = Math.max(row.width, this.maxRowWidth);
					this.rows.push(row);
					
					y += row.height + this.rowSpace;
					x = 0;
					
					row = new LegendRow();
					row.y = y;
				}
				
				item.x = x;
				item.y = y;
				
				row.height = Math.max(row.height, item.contentBounds.height);
				x += item.contentBounds.width + this.columnSpace;
				row.items.push(item);
			}
			
			//add last row
			row.width = x - this.columnSpace;
			this.rows.push(row);
			this.maxRowWidth = Math.max(row.width, this.maxRowWidth);
			y += row.height;
			
			this.realContentHeight = y;
		}
		
		private function isNotInCurrentRow(item:LegendItem, x:Number, maxWidth:Number):Boolean {
			return x + item.contentBounds.width > maxWidth;
		}
		
		//------------------------------------------------------------------
		//		Drawing
		//------------------------------------------------------------------
		
		override protected function distributeItems():void {
			
			var g:Graphics = this.itemsContainer.graphics;
			var columnSeparatorBounds:Rectangle;
			if (this.columnsSeparator != null && this.columnsSeparator.type == StrokeType.GRADIENT) {
				columnSeparatorBounds = new Rectangle(0,0,this.columnsSeparator.thickness, 0);
			}
			var rowsSeparatorBounds:Rectangle;
			if (this.rowsSeparator != null && this.rowsSeparator.type == StrokeType.GRADIENT) {
				rowsSeparatorBounds = new Rectangle(0,0,this.maxRowWidth,this.rowsSeparator.thickness);
			}
			
			var rowsCount:uint = this.rows.length;
			for (var i:uint = 0;i<rowsCount;i++) {
				var row:LegendRow = this.rows[i];
				var rowItemsCount:uint = row.items.length;
				
				var offset:Number = 0;
				
				if (this.scrollBarSpace == 0) {
					
					if (this.layout.align == ControlAlign.SPREAD) {
						row.spreadItems(this.getContentWidth());
					}else {
						switch (this.elementsAlign) {
							case HorizontalAlign.CENTER:
								offset = (this.getContentWidth() - this.maxRowWidth)/2;
								row.offsetItems((this.getContentWidth() - row.width)/2);
								break;
							case HorizontalAlign.RIGHT:
								offset = this.getContentWidth() - this.maxRowWidth;
								row.offsetItems(this.getContentWidth() - row.width);
								break;
						}
					}
				}
				
				if (this.columnsSeparator != null) {
					
					var columnsSeparatorY:Number = row.y;
					var columnsSeparatorHeight:Number = row.height;
					
					if (i == 0 && rowsCount > 0) {
						columnsSeparatorHeight += this.rowSpace/2;
						if (this.rowsSeparator != null)
							columnsSeparatorHeight -= this.rowsSeparator.padding/2;
					}else if (i > 0 && (i == rowsCount - 1)) {
						var topSpace:Number = this.rowSpace/2;
						if (this.rowsSeparator != null)
							topSpace -= this.rowsSeparator.padding/2;
						columnsSeparatorHeight += topSpace;
						columnsSeparatorY -= topSpace;
					}else {
						var space:Number = this.rowSpace;
						if (this.rowsSeparator != null)
							space -= this.rowsSeparator.padding;
						columnsSeparatorHeight += space;
						columnsSeparatorY -= space/2;
					}
					
					for (var j:int = 0;j<rowItemsCount;j++) {
						if (j < (rowItemsCount-1)) {
							var item:LegendItem = row.items[j];
							var separatorX:Number = item.x + item.contentBounds.width + this.columnSpace/2;
							if (columnSeparatorBounds != null) {
								columnSeparatorBounds.x = separatorX - this.columnsSeparator.thickness/2;
								columnSeparatorBounds.y = columnsSeparatorY;
								columnSeparatorBounds.height = columnsSeparatorHeight;
							}
							this.columnsSeparator.apply(g, columnSeparatorBounds);
							g.moveTo(separatorX, columnsSeparatorY);
							g.lineTo(separatorX, columnsSeparatorY + columnsSeparatorHeight);
							g.lineStyle();
						}
					}
				}
				
				if (this.rowsSeparator != null && i < (rowsCount - 1)) {
					var rowsSeparatorY:Number = row.y + row.height + this.rowSpace/2;
					if (rowsSeparatorBounds != null) {
						rowsSeparatorBounds.y = rowsSeparatorY - this.rowsSeparator.thickness/2;
					}
					this.rowsSeparator.apply(g, rowsSeparatorBounds);
					g.moveTo(offset, rowsSeparatorY);
					g.lineTo(offset + this.maxRowWidth, rowsSeparatorY);
					g.lineStyle();
				}
			}
		}
	}
}

import com.anychart.controls.legend.LegendItem;
import flash.display.DisplayObject;

class LegendRow {
	public var items:Array;
	public var width:Number;
	public var height:Number;
	public var y:Number;
	
	public function LegendRow() {
		this.items = [];
		this.width = 0;
		this.height = 0;
		this.y = 0;
	}
	
	public function offsetItems(xOffset:Number):void {
		for each (var item:DisplayObject in this.items) {
			item.x += xOffset;
		}
	}
	
	public function spreadItems(maxWidth:Number):void {
		if (this.width >= maxWidth) return;
		var itemSpace:Number = maxWidth/this.items.length;
		var itemsCnt:uint = this.items.length;
		for (var i:int = 0;i<itemsCnt;i++) {
			var item:Object = this.items[i];
			item.x = itemSpace*i + (itemSpace - item.contentBounds.width)/2;
		}
	}
}