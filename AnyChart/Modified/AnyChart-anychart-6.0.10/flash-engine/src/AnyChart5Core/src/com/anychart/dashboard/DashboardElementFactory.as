package com.anychart.dashboard {
	
	internal final class DashboardElementFactory {
		
		public static function createElement(node:XML, dashboard:Dashboard):DashboardElement {
			var nodeName:String = node.name().toString();
			if (nodeName == "hbox") return new DashboardHBox(dashboard);
			if (nodeName == "vbox") return new DashboardVBox(dashboard);
			if (nodeName == "view" && node.@type != undefined) {
				var viewType:String = String(node.@type).toLowerCase();
				if (viewType == "panel") return new Panel(dashboard);
				return new ChartElement(dashboard);
			}
			return null;
		}
	}
}