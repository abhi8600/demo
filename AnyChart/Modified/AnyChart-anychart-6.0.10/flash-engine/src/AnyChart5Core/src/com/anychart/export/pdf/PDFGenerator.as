package com.anychart.export.pdf
{
	
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	/*
	 Objects: 
	 	1 - Catalog
	 	2 - Pages
	 	3 - Page
	 	4 - Page content
	 	5 - Page Resources
	 	6 - Image
	 */
	public final class PDFGenerator extends PdfGeneratorBase {
		
		public function generate(image:BitmapData, w:Number, h:Number):ByteArray {
			
			this.initGeneration();
			
			this.write("%PDF-1.4");
			
			this.startObj(1);
			this.write("<</Type /Catalog /Pages 2 0 R>>");
			this.endObj();
			
			this.startObj(2);
			this.write("<</Type /Pages /Kids [3 0 R]/Count 1>>");
			this.endObj();
			
			w = Math.round(w*100)/100;
			h = Math.round(h*100)/100;
			
			this.startObj(3);
			this.write("<</Type /Page /Parent 2 0 R /MediaBox [0 0 "+w+" "+h+"] /Resources 5 0 R /Contents 4 0 R>>");
			this.endObj();
			
			//page content
			var imgW:Number = w;//Math.round(image.width*100)/100;
			var imgH:Number = h;//Math.round(image.height*100)/100;
			var imgX:Number = 0;
			var imgY:Number = 0;
			
			var pageContent:String = "2 J\n";						//caps: none:0 J, round: 1 J, square: 2 J
			pageContent += "0.57 w\n";		 						//line width
			pageContent += "q "+imgW+" 0 0 "+imgH+" "+imgX+" "+imgY+" cm\n";//image bounds: width, 0, 0, height, x, spaceBetweenPageEndAndImageBottom
			pageContent += "/I1 Do Q\n";							//image index
			
			this.startObj(4);
			this.write("<</Length "+pageContent.length+">>stream\n"+pageContent+"endstream");
			this.endObj();
			
			//resources
			this.startObj(5);
			this.write("<</ProcSet [/ImageB /ImageC /ImageI] /XObject <</I1 6 0 R>>>>");
			this.endObj();
			
			//image
			this.generateImage(image);
			
			this.startObj(7);
			this.write ("<</Producer (AnyChart.com)/Creator (AnyChart.com)>>");
			this.endObj();
			
			this.finalize(7);
			this.bytes.position=0;
			return this.bytes;
		}
		
		private function generateImage(data:BitmapData):void {
			var pngBytes:ByteArray = PNGEncoder.encode(data, 1);
			pngBytes.position = 16;
			
			var width:int = pngBytes.readInt();
			var height:int = pngBytes.readInt();

			var bitsPerComponent:int = pngBytes.readByte();
			var colorTable:int = pngBytes.readByte();
			var colors:int;
			
			var colorScheme:String;
			
			switch (colorTable) {
				case 0: 
					colorScheme = "DeviceGray"; 
					colors = 1;
					break;
				case 2: 
					colorScheme = "DeviceRGB";
					colors = 3; 
					break;
				case 3: 
					colorScheme = "Indexed"; 
					colors = 1;
					break;
			}
			
			if ( pngBytes.readByte() != 0 ) throw new Error ("Unknown compression method");
			if ( pngBytes.readByte() != 0 ) throw new Error ("Unknown filter method");
			if ( pngBytes.readByte() != 0 ) throw new Error ("Interlacing not supported");
			
			pngBytes.position += 4;
			
			//encoding image
			var palBytes:ByteArray = new ByteArray();
			var idataBytes:ByteArray = new ByteArray();
			var pal:String = "";
			
			do {
				var n:int = pngBytes.readInt();
				var type:String = pngBytes.readUTFBytes(4);
				
				if (type == "PLTE") {
					pngBytes.readBytes(palBytes, pngBytes.position, n);
					pngBytes.readUnsignedInt();
					palBytes.position = 0;
					pal = palBytes.readUTFBytes(palBytes.bytesAvailable);
				}else if (type == "tRNS") {
				}else if (type == "IDAT") {
					pngBytes.readBytes(idataBytes, idataBytes.length, n);
					pngBytes.readUnsignedInt(); 
				}else if (type == "") {
					break;
				}else { pngBytes.position += (n+4); }
			}while (n > 0);
			
			var decodeParams:String = "/DecodeParms <</Predictor 15 /Colors "+colors+" /BitsPerComponent "+bitsPerComponent+" /Columns "+width+">>";
			
			this.startObj(6);
			this.write("<</Type/XObject/Subtype/Image/Width "+width+"/Height "+height);
			if (colorScheme == "Indexed")
				this.write("/ColorSpace [/Indexed /DeviceRGB "+(pal.length/3-1)+" 1 0 R");
			else 
				this.write("/ColorSpace /"+colorScheme);
			this.write("/BitsPerComponent "+bitsPerComponent); 
			this.write("/Filter /FlateDecode");
					
			idataBytes.position = 0;
					
			this.write(decodeParams);
		 	this.write("/Length "+idataBytes.length+">>"); 
			this.write("stream");
			this.bytes.writeBytes(idataBytes);
			this.bytes.writeUTFBytes("\n");		
			this.write("endstream");
			this.endObj();			
		}
	}
}

import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

class PNGEncoder {

    public static function encode(img:BitmapData, type:uint = 0):ByteArray
    {
    	
        // Create output byte array
        var png:ByteArray = new ByteArray();
        // Write PNG signature
        png.writeUnsignedInt(0x89504e47);
        png.writeUnsignedInt(0x0D0A1A0A);
        // Build IHDR chunk
        var IHDR:ByteArray = new ByteArray();
        IHDR.writeInt(img.width);
        IHDR.writeInt(img.height);
        if(img.transparent || type == 0)
        {
        	IHDR.writeUnsignedInt(0x08060000); // 32bit RGBA
        }
        else
        {
        	IHDR.writeUnsignedInt(0x08020000); //24bit RGB
        }
        IHDR.writeByte(0);
        writeChunk(png,0x49484452,IHDR);
        // Build IDAT chunk
        var IDAT:ByteArray= new ByteArray();
        
        switch(type)
        {
        	case 0:
        		writeRaw(img, IDAT);
        		break;
        	case 1:
        		writeSub(img, IDAT);
        		break;
        }
        
        IDAT.compress();
        writeChunk(png,0x49444154,IDAT);
        // Build IEND chunk
        writeChunk(png,0x49454E44,null);
        // return PNG
        return png;
        
    }
    
    private static function writeRaw(img:BitmapData, IDAT:ByteArray):void
    {
    	
        var h:int = img.height;
        var w:int = img.width;
        var transparent:Boolean = img.transparent;
        var subImage:ByteArray;
        
        for(var i:int=0;i < h;i++) {
            // no filter
            if ( !transparent ) {
            	subImage = img.getPixels(
            		new Rectangle(0, i, w, 1));
            	//Here we overwrite the alpha value of the first pixel
            	//to be the filter 0 flag
            	subImage[0] = 0;
				IDAT.writeBytes(subImage);
				//And we add a byte at the end to wrap the alpha values
				IDAT.writeByte(0xff);
            } else {
            	IDAT.writeByte(0);
            	var p:uint;
                for(var j:int=0;j < w;j++) {
                    p = img.getPixel32(j,i);
                    IDAT.writeUnsignedInt(
                        uint(((p&0xFFFFFF) << 8)|
                        (p>>>24)));
                }
            }
        }
    }
    
    private static function writeSub(img:BitmapData, IDAT:ByteArray):void
    {
        var r1:uint;
        var g1:uint;
        var b1:uint;
        var a1:uint;
        
        var r2:uint;
        var g2:uint;
        var b2:uint;
        var a2:uint;
        
        var r3:uint;
        var g3:uint;
        var b3:uint;
        var a3:uint;
        
        var p:uint;
        var h:int = img.height;
        var w:int = img.width;
        
        for(var i:int=0;i < h;i++) {
            // no filter
            IDAT.writeByte(1);
            if ( !img.transparent ) {
				r1 = 0;
				g1 = 0;
				b1 = 0;
				a1 = 0xff;
                for(var j:int=0;j < w;j++) {
                    p = img.getPixel(j,i);
                    
                    r2 = p >> 16 & 0xff;
                    g2 = p >> 8  & 0xff;
                    b2 = p & 0xff;
                    
                    r3 = (r2 - r1 + 256) & 0xff;
                    g3 = (g2 - g1 + 256) & 0xff;
                    b3 = (b2 - b1 + 256) & 0xff;
                    
                    IDAT.writeByte(r3);
                    IDAT.writeByte(g3);
                    IDAT.writeByte(b3);
                    
                    r1 = r2;
                    g1 = g2;
                    b1 = b2;
                    a1 = 0;
                }
            } else {
				r1 = 0;
				g1 = 0;
				b1 = 0;
				a1 = 0;
                for(var k:int=0;k < w;k++) {
                    p = img.getPixel32(k,i);
                    
                    a2 = p >> 24 & 0xff;
                    r2 = p >> 16 & 0xff;
                    g2 = p >> 8  & 0xff;
                    b2 = p & 0xff;
                    
                    r3 = (r2 - r1 + 256) & 0xff;
                    g3 = (g2 - g1 + 256) & 0xff;
                    b3 = (b2 - b1 + 256) & 0xff;
                    a3 = (a2 - a1 + 256) & 0xff;
                    
                    IDAT.writeByte(r3);
                    IDAT.writeByte(g3);
                    IDAT.writeByte(b3);
                    IDAT.writeByte(a3);
                    
                    r1 = r2;
                    g1 = g2;
                    b1 = b2;
                    a1 = a2;
                }
            }
        }
    }

    private static var crcTable:Array;
    private static var crcTableComputed:Boolean = false;

    private static function writeChunk(png:ByteArray, 
            type:uint, data:ByteArray):void {
        if (!crcTableComputed) {
            crcTableComputed = true;
            crcTable = [];
            for (var n:uint = 0; n < 256; n++) {
                var c:uint = n;
                for (var k:uint = 0; k < 8; k++) {
                    if (n & 1) {
                        c = uint(uint(0xedb88320) ^ 
                            uint(c >>> 1));
                    } else {
                        c = uint(c >>> 1);
                    }
                }
                crcTable[n] = c;
            }
        }
        var len:uint = 0;
        if (data != null) {
            len = data.length;
        }
        png.writeUnsignedInt(len);
        var p:uint = png.position;
        png.writeUnsignedInt(type);
        if ( data != null ) {
            png.writeBytes(data);
        }
        var e:uint = png.position;
        png.position = p;
        c = 0xffffffff;
        for (var i:int = 0; i < (e-p); i++) {
            c = uint(crcTable[
                (c ^ png.readUnsignedByte()) & 
                0xff] ^ (c >>> 8));
        }
        c = uint(c^uint(0xffffffff));
        png.position = e;
        png.writeUnsignedInt(c);
    }
}