private static var chartsInitialized:Boolean = initializeCharts();
private static function initializeCharts():Boolean {
	
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.piePlot.PiePlot;
	import com.anychart.axesPlot.AxesPlot3D;
	var p1:AxesPlot;
	var p2:PiePlot;
	var p3:AxesPlot3D;
	
	/* bar template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* 3d bar template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.barSeries::TemplateUtils3D")).getDefaultTemplate().copy());
	/* stock template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.stock::TemplateUtils")).getDefaultTemplate().copy());
	/* bubble template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.bubbleSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* line template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.lineSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* marker template */  BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.axesPlot.series.markerSeries::TemplateUtils")).getDefaultTemplate().copy());
	
	import com.anychart.axesPlot.series.bubbleSeries.BubbleGlobalSeriesSettings;
	var tmp1:BubbleGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.barSeries.BarGlobalSeriesSettings;
	import com.anychart.axesPlot.series.barSeries.BarGlobalSeriesSettings3D;
	var tmp2:BarGlobalSeriesSettings;
	var tmp2_1:BarGlobalSeriesSettings3D;
	
	import com.anychart.axesPlot.series.stock.CandlestickGlobalSeriesSettings;
	var tmp3:CandlestickGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.stock.OHLCGlobalSeriesSettings;
	var tmp4:OHLCGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.barSeries.RangeBarGlobalSeriesSettings;
	var tmp5:RangeBarGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.lineSeries.LineGlobalSeriesSettings;
	var tmp6:LineGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.lineSeries.AreaGlobalSeriesSettings;
	var tmp7:AreaGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.lineSeries.RangeAreaGlobalSeriesSettings;
	var tmp8:RangeAreaGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.markerSeries.MarkerGlobalSeriesSettings;
	var tmp9:MarkerGlobalSeriesSettings;
	
	import com.anychart.axesPlot.series.barSeries.RangeBarGlobalSeriesSettings3D;
	var tmp11:RangeBarGlobalSeriesSettings3D;
	
	return true;
}