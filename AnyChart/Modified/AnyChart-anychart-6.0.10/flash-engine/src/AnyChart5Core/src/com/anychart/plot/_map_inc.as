private static var mapsInitialized:Boolean = initializeMaps();
private static function initializeMaps():Boolean {
	import com.anychart.mapPlot.MapPlot;
	var plot:MapPlot;
	
	/* bubble template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.mapPlot.bubbleSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* connector and area template */ BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.mapPlot.multiPointSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* line template */  BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.mapPlot.lineSeries::TemplateUtils")).getDefaultTemplate().copy());
	/* marker template */  BaseTemplatesManager.addBuildinTemplate(Class(getDefinitionByName("com.anychart.mapPlot.markerSeries::TemplateUtils")).getDefaultTemplate().copy());
	
	import com.anychart.mapPlot.bubbleSeries.MapBubbleGlobalSeriesSettings;
	var tmp1:MapBubbleGlobalSeriesSettings;
	
	import com.anychart.mapPlot.connectorSeries.ConnectorGlobalSeriesSettings;
	var tmp2:ConnectorGlobalSeriesSettings;
	
	import com.anychart.mapPlot.areaSeries.MapAreaGlobalSeriesSettings;
	var tmp3:MapAreaGlobalSeriesSettings;
	
	import com.anychart.mapPlot.lineSeries.MapLineGlobalSeriesSettings;
	var tmp4:MapLineGlobalSeriesSettings;
	
	import com.anychart.mapPlot.markerSeries.MapMarkerGlobalSeriesSettings;
	var tmp5:MapMarkerGlobalSeriesSettings;
	
	return true;
}