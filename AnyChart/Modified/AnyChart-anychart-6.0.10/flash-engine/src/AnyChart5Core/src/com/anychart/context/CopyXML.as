package com.anychart.context {
	import com.anychart.IAnyChart;
	import com.anychart.serialization.SerializerBase;
	
	import flash.system.System;
	
	public class CopyXML extends ContextMenuElement {
		
		public function CopyXML(chart:IAnyChart) {
			this.contextMenuItemText = "Copy XML to clipboard";
			super(chart);
		}
		
		override public function setDefaults():void {
			this.enabled = false;
		}
		
		override public function deserializeContextMenu(data:XML):void {
			if (data.@copy_xml != undefined)
				this.enabled = SerializerBase.getBoolean(data.@copy_xml);
		} 
		
		override public function execute():void {
			var data:XML = this.chart.getXMLData();
			if (data != null)
				System.setClipboard(data.toXMLString());
			else
				System.setClipboard("Empty XML");
		}

	}
}