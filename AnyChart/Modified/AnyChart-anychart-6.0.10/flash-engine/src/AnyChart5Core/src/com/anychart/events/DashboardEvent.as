package com.anychart.events {
	import flash.events.Event;
	
	public final class DashboardEvent extends Event {
		
		public static const DASHBOARD_VIEW_RENDER:String = "dashboardViewRender";
		public static const DASHBOARD_VIEW_DRAW:String = "dashboardViewDraw";
		public static const DASHBOARD_VIEW_REFRESH:String = "dashboardViewRefresh";
		
		private var _viewId:String;
		public function get viewId():String { return this._viewId; }
		
		public function DashboardEvent(type:String, viewId:String) {
			this._viewId = viewId;
			super(type);
		}
		
		override public function clone():Event {
			var e:Event = new DashboardEvent(super.type, this._viewId);
			return e;
		}

	}
}