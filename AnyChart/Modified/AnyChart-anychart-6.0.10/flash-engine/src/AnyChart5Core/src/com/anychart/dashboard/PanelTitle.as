package com.anychart.dashboard {
	import com.anychart.IAnyChart;
	import com.anychart.formatters.IFormatable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.text.InteractiveTitle;
	
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	
	internal final class PanelTitle extends InteractiveTitle implements IFormatable {
		
		public var space:Number;
		
		public function PanelTitle(chart:IAnyChart) {
			super(chart,this);
		}
		
		public function getTokenValue(token:String):* { return ""; }
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		override public function initialize(panelBounds:Rectangle):DisplayObject {
			this.setPosition(panelBounds);
			return super.initialize(panelBounds);
		}
		
		override public function resize(panelBounds:Rectangle):void {
			this.setPosition(panelBounds);
		}
		
		private function setPosition(panelBounds:Rectangle):void {
			this.container.y = panelBounds.y;
			switch (this.align) {
				case AbstractAlign.NEAR:
					this.container.x = panelBounds.x;
					break;
				case AbstractAlign.FAR:
					this.container.x = panelBounds.x + panelBounds.width - info.rotatedBounds.width;
					break;
				case AbstractAlign.CENTER:
					this.container.x = panelBounds.x + (panelBounds.width - info.rotatedBounds.width)/2;
					break;
			}
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data,resources);
			
			this.info = this.createInformation();
			this.info.formattedText = this.text;
			this.getBounds(this.info);
			this.space = this.padding + this.info.rotatedBounds.height;
		}
	}
}