package com.anychart.plot {
	import com.anychart.AnyChart;
	import com.anychart.Chart;
	import com.anychart.controls.ControlsFactory;
	import com.anychart.errors.FeatureNotSupportedError;
	import com.anychart.viewController.ChartView;
	import com.anychart.xmlStore.XMLGlobalItems;
	
	import flash.utils.getDefinitionByName;
	
	public class PlotFactoryBase {
		
		public static const IS_ORACLE:Boolean = false;
		
		private static var _initialized:Boolean = initialize();
		private static function initialize():Boolean {
			import com.anychart.controls.label.LabelControl;
			import com.anychart.controls.image.ImageControl;
			import com.anychart.controls.legend.ColumnBasedLegend;
			import com.anychart.controls.legend.RowBasedLegend;
			ControlsFactory.register(LabelControl);
			ControlsFactory.register(ImageControl);
			ControlsFactory.register(ColumnBasedLegend);
			ControlsFactory.register(RowBasedLegend);
			
			return true;
		}
		
		internal static function getChartXML(source:String):XML {
			return XMLGlobalItems.getInstance().getChart(source);
		}
		
		internal static function createSinglePlot(view:ChartView, chart:Chart, xmlData:XML):void {
			var plotNode:XML;
			if (xmlData.charts[0] != null && xmlData.charts[0].chart[0] != null)
				plotNode = xmlData.charts[0].chart[0];
			else if (xmlData.gauges[0] != null && xmlData.gauges[0].gauge[0] != null)
				plotNode = xmlData.gauges[0].gauge[0];
			if (plotNode != null) {
				createPlot(view, chart, plotNode);
			}
		}
		
		internal static function createPlot(view:ChartView, chart:Chart, plotNode:XML):void {
			var nodeName:String = plotNode.name().toString();
			var plotClass:Class;
			var plotType:String;
			
			var templatesList:Array = null;
			var isGauge:Boolean = nodeName == "gauge";
			var i:int;
			
			var anychart:AnyChart = view.base;
			
			if (plotNode.@template != undefined) {
				templatesList = [];
				var userTemplate:XML = XMLGlobalItems.getInstance().getTemplate(String(plotNode.@template));
				while (userTemplate != null) {
					templatesList.push(userTemplate);
					userTemplate = (userTemplate.@parent == undefined || userTemplate.@parent == userTemplate.@name) ? null : XMLGlobalItems.getInstance().getTemplate(String(userTemplate.@parent));
				}
				if (!isGauge) {
					for (i = templatesList.length-1;i>=0;i--) {
						if (templatesList[i].chart[0] != null && templatesList[i].chart[0].@plot_type != undefined) {
							plotType = String(templatesList[i].chart[0].@plot_type).toLowerCase();
						}
					}
				} 
			}
			
			try {
				if (isGauge)
					plotClass = Class(getDefinitionByName("com.anychart.gaugePlot::GaugePlot"));
				else if (nodeName == "chart") {
					if (plotNode.@plot_type != undefined)
						plotType = String(plotNode.@plot_type).toLowerCase();
						
					switch (plotType) {
						case "map":
							plotClass = Class(getDefinitionByName("com.anychart.mapPlot::MapPlot"));
							break;
						case "pie":
						case "doughnut":
							plotClass = Class(getDefinitionByName("com.anychart.piePlot::PiePlot"));
							break;
						case "funnel":
							plotClass = Class(getDefinitionByName("com.anychart.funnelPlot::FunnelPlot"));
							break;
						case "table":
							plotClass = Class(getDefinitionByName("com.anychart.tablePlot::TablePlot"));
							break;
						case "treemap":
							plotClass = Class(getDefinitionByName("com.anychart.treeMapPlot::TreeMapPlot"));
							break;
						case "heatmap":
							plotClass = Class(getDefinitionByName("com.anychart.axesPlot::AxesPlot"));
							break;
						case "polar":
							plotClass = Class(getDefinitionByName("com.anychart.radarPolarPlot::PolarPlot"));
							break;
						case "radar":
							plotClass = Class(getDefinitionByName("com.anychart.radarPolarPlot::RadarPlot"));
							break;
						case "categorizedvertical":
						case "categorizedbyseriesvertical":
						case "categorizedhorizontal":
						case "categorizedbyserieshorizontal":
						case "scatter":
							plotClass = is3DPlot(plotNode) ? Class(getDefinitionByName("com.anychart.axesPlot::AxesPlot3D")) : Class(getDefinitionByName("com.anychart.axesPlot::AxesPlot"));
							break;
						default:
							plotType = "categorizedvertical";
							plotClass = is3DPlot(plotNode) ? Class(getDefinitionByName("com.anychart.axesPlot::AxesPlot3D")) : Class(getDefinitionByName("com.anychart.axesPlot::AxesPlot"));
							break;
					} 
				}
			}catch (e:Error) {
				throw new FeatureNotSupportedError("Plot type not supported");
			}
			if (plotClass != null) {
				var plot:IPlot = IPlot(new plotClass());
				plot.setPlotType(plotType);
				plot.setView(view);
				plot.setBase(view.base);
				
				var chartDefaultTemplate:XML = Chart.getDefaultTemplate().copy();
				var plotDefaultTemplate:XML = Object(plotClass).getDefaultTemplate().copy();
				
				XML(chartDefaultTemplate.chart[0]).setName(plot.getTemplatesManager().getBaseNodeName());
				var template:XML = plot.getTemplatesManager().mergeTemplates(chartDefaultTemplate, plotDefaultTemplate);
				
				if (plotNode.@template != undefined) {
					for (i = templatesList.length-1;i>=0;i--)
						template = plot.getTemplatesManager().mergeTemplates(template, templatesList[i]);
					templatesList = null;
				}
				
				
				plotNode = plot.getTemplatesManager().mergeWithChart(template, plotNode);
				
				if (Object(plot).hasOwnProperty("setCashedData") && plot["setCashedData"] != undefined) {
					plot["setCashedData"](plotNode);
				}
				
				chart.plot = plot; 
				chart.deserialize(plotNode, view.resources);
				
				LicenseType.checkPlotLicense(plotClass, chart);
			}else {
				chart.deserialize(plotNode, view.resources);
			}
		}
		
		private static function is3DPlot(data:XML):Boolean {
			if (data == null) return false;
			if (data.data_plot_settings[0] == null) return false;
			if (data.data_plot_settings[0].@enable_3d_mode == undefined) return false;
			if (String(data.data_plot_settings[0].@enable_3d_mode).toLowerCase() != "true") return false;
			return true;
		}
	}
}