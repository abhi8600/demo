package com.anychart.radarPolarPlot.series.markerSeries {
	import com.anychart.radarPolarPlot.data.RadarPolarPlotSeries;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	
	internal final class MarkerSeries extends RadarPolarPlotSeries {
		
		public function MarkerSeries() {
			super();
		}
		
		override public function createPoint():BasePoint {
			return new MarkerPoint();
		}
		
		override protected function deserializeMarkerElement(data:XML, styles:XML, marker:MarkerElement, resources:ResourcesLoader):MarkerElement {
			if (data.@style != undefined) {
				if (data.marker[0] == null)
					data.marker[0] = <marker style={data.@style} />;
				else 
					data.marker[0].@style = data.@style;
			}
			
			if (data.marker[0] != null && data.marker[0].@style != undefined) {
				this.settings = this.settings.createCopy(this.settings);
				this.settings.style = marker.style;
			}
					
			if (marker.needCreateElementForSeries(data.marker[0])) {
				marker = MarkerElement(marker.createCopy());
				marker.deserializeSeries(data.marker[0], styles, resources);
			}
			marker.enabled = true;
			
			return marker;
		}
	}
}