package com.anychart.gaugePlot.elements {
	import com.anychart.elements.BaseElement;
	import com.anychart.elements.IElementContainer;
	import com.anychart.elements.label.ItemLabelElement;
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.gaugePlot.GaugeBase;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.gaugePlot.visual.text.GaugeTextPlacementMode;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.states.StyleState;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class LabelElement extends ItemLabelElement {
		
		public var gauge:GaugeBase;
		public var underPointers:Boolean;
		
		public function LabelElement() {
			super();
			this.enabled = true;
			this.underPointers = false;
		}
		
		override public function getStyleStateClass():Class { return GaugeLabelElementStyleState; }
		
		override public function createCopy(element:BaseElement=null):BaseElement {
			if (element == null)
				element = new LabelElement();
			LabelElement(element).gauge = this.gauge;
			LabelElement(element).underPointers = this.underPointers;
			super.createCopy(element);
			return element;
		}
		
		public function deserializeGlobal(data:XML, stylesList:XML, resources:ResourcesLoader):void {
			if (data.@enabled != undefined)
				this.enabled = SerializerBase.getBoolean(data.@enabled);
			if (data.@under_pointers != undefined)
				this.underPointers = SerializerBase.getBoolean(data.@under_pointers);
			this.deserializeStyleFromNode(stylesList, data, resources);
		}
		
		override protected function getPosition(container:IElementContainer, state:BaseElementStyleState, sprite:Sprite, elementIndex:uint):Point {
			var labelState:GaugeLabelElementStyleState = GaugeLabelElementStyleState(state);
			var label:GaugeBaseTextElement = GaugeBaseTextElement(labelState.label);
			var pos:Point = new Point();
			
			this.getBounds(container, state, elementIndex);
			
			switch (label.placemenetMode) {
				case GaugeTextPlacementMode.BY_ANCHOR:
					GaugePointer(container).getAnchorPoint(labelState.anchor, pos);
					break;
				case GaugeTextPlacementMode.BY_POINT:
				case GaugeTextPlacementMode.BY_RECTANGLE:
					pos.x = this.gauge.bounds.x;
					pos.y = this.gauge.bounds.y;
					break;
			}
			return pos;			
		}
		
		override public function resize(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			super.draw(container, state, sprite, elementIndex);
		}
		
		override protected function execDrawing(container:IElementContainer, state:StyleState, sprite:Sprite, elementIndex:uint):void {
			var labelState:GaugeLabelElementStyleState = GaugeLabelElementStyleState(state);
			
			var info:TextElementInformation = container.elementsInfo[elementIndex]['info'+state.stateIndex];
			
			var bounds:Rectangle = info.rotatedBounds;
			
			var g:Graphics = sprite.graphics;
			g.lineStyle();
			
			GaugeBaseTextElement(labelState.label).drawGaugeText(sprite, 0,0, this.gauge.bounds.width, this.gauge.bounds.height, info, container.color, container.hatchType);
		}
	}
}