package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.frame.GaugeFrame;
	import com.anychart.gaugePlot.frame.LinearGaugeFrame;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.linear.BarLinearGaugePointer;
	import com.anychart.gaugePlot.pointers.linear.MarkerLinearGaugePointer;
	import com.anychart.gaugePlot.pointers.linear.RangeBarLinearGaugePointer;
	import com.anychart.gaugePlot.pointers.linear.TankPointer;
	import com.anychart.gaugePlot.pointers.linear.ThermometerPointer;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.gradient.Gradient;
	
	import flash.geom.Rectangle;
	
	public final class LinearGauge extends AxisGaugeBase {
		private var isHorizontal:Boolean;
		
		public var startMargin:Number;
		public var endMargin:Number;
		
		public function LinearGauge() {
			this.isHorizontal = false;
			this.startMargin = 0;
			this.endMargin = 0;
		}
		
		override protected function createPointerByType(type:*):GaugePointer {
			if (type == undefined) return new BarLinearGaugePointer();
			switch (SerializerBase.getEnumItem(type)) {
				case "bar": return new BarLinearGaugePointer();
				case "marker": return new MarkerLinearGaugePointer();
				case "tank": return new TankPointer();
				case "thermometer": return new ThermometerPointer();
				case "rangebar": return new RangeBarLinearGaugePointer();
				default: return new BarLinearGaugePointer();
			}
		}
		
		override protected function createFrame(data:XML):GaugeFrame {
			return new LinearGaugeFrame();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			if (data.@orientation != undefined)
				this.isHorizontal = SerializerBase.getEnumItem(data.@orientation) == "horizontal";
			super.deserialize(data, resources);
		}
		
		override protected function createAxis(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):GaugeAxis {
			var axis:LinearGaugeAxis = new LinearGaugeAxis();
			axis.gauge = this;
			axis.isHorizontal = this.isHorizontal;
			axis.deserialize(data, localStyles, styles, resources);
			return axis;
		}
		
		override public function setGradientRotation(gradient:Gradient):void {
			if (gradient != null && this.isHorizontal) {
				gradient.angle += 90;
			}
		}
		
		override protected function setBounds(newBounds:Rectangle):void {
			super.setBounds(newBounds);
			this.frame.autoFit(this.bounds);
		}
	}
}