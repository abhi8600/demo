package com.anychart.gaugePlot.pointers.styles {
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.gaugePlot.visual.shape.ShapeType;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.background.BackgroundBase;
	
	public final class MarkerStyle implements IStyleSerializableRes {
		
		public var align:uint;
		public var padding:Number;
		public var width:Number;
		public var isWidthAxisSizeDepend:Boolean;
		public var height:Number;
		public var isHeightAxisSizeDepend:Boolean;
		public var shape:uint;
		public var rotation:Number;
		public var autoRotate:Boolean;
		
		public var background:BackgroundBase;
		
		public function MarkerStyle(useBackground:Boolean) {
			this.width = .1;
			this.height = .1;
			this.shape = ShapeType.LINE;
			this.align = GaugeItemAlign.CENTER;
			this.padding = 0;
			if (useBackground)
				this.background = new BackgroundBase();
			this.isWidthAxisSizeDepend = false;
			this.isHeightAxisSizeDepend = false;
			this.rotation = 0;
			this.autoRotate = true;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@shape != undefined) this.shape = GaugePlotSerializerBase.getShapeType(data.@shape);
			if (data.@width != undefined) {
				this.isWidthAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@width);
				this.width = (this.isWidthAxisSizeDepend) ? GaugePlotSerializerBase.getAxisSizeFactor(data.@width) : GaugePlotSerializerBase.getSimplePercent(data.@width);
			}
			if (data.@height != undefined) {
				this.isHeightAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@height);
				this.height = this.isHeightAxisSizeDepend ? GaugePlotSerializerBase.getAxisSizeFactor(data.@height) : GaugePlotSerializerBase.getSimplePercent(data.@height);
			}
			if (!GaugePlotSerializerBase.shapeIsRectangle(this.shape))
				this.width = this.height = Math.min(this.width, this.height);
			if (this.background != null)
				this.background.deserialize(data, resources, style);
			if (data.@align != undefined) this.align = GaugePlotSerializerBase.getAlign(data.@align);
			if (data.@padding != undefined) this.padding = GaugePlotSerializerBase.getSimplePercent(data.@padding);
			if (data.@rotation != undefined) this.rotation = SerializerBase.getNumber(data.@rotation);
			if (data.@auto_rotate != undefined) this.autoRotate = SerializerBase.getBoolean(data.@auto_rotate);
		}
	}
}