package com.anychart.gaugePlot.pointers.linear {
	import com.anychart.IAnyChart;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	
	
	public class RangeBarLinearGaugePointer extends BarLinearGaugePointer {
		public var endValue:Number;
		
		override protected function getStyleNodeName():String {
			return 'range_bar_pointer_style';
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, localStyles:StylesList, stylesList:XML, chart:IAnyChart):void {
			data.@value = data.@start;
			super.deserialize(data, resources, localStyles, stylesList, chart);
			this.editable = false;
			this.endValue = SerializerBase.getNumber(data.@end);
		}
		
		override public function getTokenValue(token:String):* {
			if (token == '%Value') return (this.endValue-this.value);
			if (token == '%StartValue') return this.value;
			if (token == '%EndValue') return this.endValue;
			return '';
		}
		
		override protected function execDrawing():void {
			this.setFiltersAndClear();
			this.drawBar(this.fixValueScaleOut(this.value), this.fixValueScaleOut(this.endValue));
		}
	}
}