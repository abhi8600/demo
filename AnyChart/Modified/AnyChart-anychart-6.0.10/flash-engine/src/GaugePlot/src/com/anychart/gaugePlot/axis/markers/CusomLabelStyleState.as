package com.anychart.gaugePlot.axis.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	
	internal final class CusomLabelStyleState extends GaugeAxisMarkerStyleState {
		
		internal var label:AxisMarkerLabel;
		internal var tickmark:AxisMarkerTickmark;
		
		public function CusomLabelStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (SerializerBase.isEnabled(data.label[0])) {
				this.label = new AxisMarkerLabel();
				this.label.deserialize(data.label[0],resources,this.style);
			}
			if (SerializerBase.isEnabled(data.tickmark[0])) {
				this.tickmark = new AxisMarkerTickmark();
				this.tickmark.deserializeMarker(data.tickmark[0],resources,this.style);
			}
			return data;
		}

	}
}