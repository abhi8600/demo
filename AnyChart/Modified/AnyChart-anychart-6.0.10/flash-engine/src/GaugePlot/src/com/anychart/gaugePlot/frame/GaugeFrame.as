package com.anychart.gaugePlot.frame {
	import com.anychart.IResizable;
	import com.anychart.gaugePlot.GaugeBase;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.visual.BackgroundBasedStroke;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.ISerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.corners.Corners;
	import com.anychart.visual.effects.EffectsList;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	public class GaugeFrame implements ISerializableRes, IResizable {
		
		public static const CIRCULAR:uint = 0;
		public static const AUTO:uint = 1;
		public static const RECT:uint = 2;
		public static const ROUNDED_RECT:uint = 3;
		
		protected var innerStroke:BackgroundBasedStroke;
		protected var outerStroke:BackgroundBasedStroke;
		protected var background:BackgroundBase;
		protected var corners:Corners;
		protected var effects:EffectsList;
		protected var type:uint=RECT;
		
		protected var bounds:Rectangle;
		public var gauge:GaugeBase;
		
		protected var padding:Number=0.15;
		
		private var container:Sprite;
		protected var backgrondShape:Shape;
		protected var innerStrokeShape:Shape;
		protected var outerStrokeShape:Shape; 
		
		protected var mask:Shape;
		
		public var allowCrop:Boolean = false;
		public var allowAutoFit:Boolean = true;
		
		public function autoFit(gaugeRect:Rectangle):void {}
		
		public function deserialize(data:XML, resources:ResourcesLoader):void {
			if(data.@type!=undefined) 
			{
				switch(SerializerBase.getEnumItem(data.@type))
				{
					case "circular": this.type=CIRCULAR; break;
					case "auto": this.type=AUTO; break;
					case "rectangular": this.type=RECT; break;
					case "roundedrectangular": this.type=ROUNDED_RECT; break;
				}
			}
			
			if (data.@auto_fit != undefined) this.allowAutoFit = SerializerBase.getBoolean(data.@auto_fit);
			if (data.@crop_content != undefined) this.allowCrop = SerializerBase.getBoolean(data.@crop_content);
			
			this.padding = 0;
			
			if (data.@enabled != undefined && !SerializerBase.getBoolean(data.@enabled)) return;
			
			if (data.@padding!=undefined) this.padding=GaugePlotSerializerBase.getSimplePercent(data.@padding);
			
			if (SerializerBase.isEnabled(data.inner_stroke[0])) {
				this.innerStroke = new BackgroundBasedStroke();
				this.innerStroke.deserialize(data.inner_stroke[0], resources);
			}
			if (SerializerBase.isEnabled(data.outer_stroke[0])) {
				this.outerStroke = new BackgroundBasedStroke();
				this.outerStroke.deserialize(data.outer_stroke[0], resources);
			}
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new BackgroundBase();
				this.background.deserialize(data.background[0], resources);
			}
			if (SerializerBase.isEnabled(data.effects[0])) {
				this.effects = new EffectsList();
				this.effects.deserialize(data.effects[0]);
			}
			if (SerializerBase.isEnabled(data.corners[0])) {
				this.corners = new Corners();
				this.corners.deserialize(data.corners[0]);
			}
		}
		
		public function initialize(bounds:Rectangle):DisplayObject {
			this.container = new Sprite();
			if (this.effects != null)
				this.container.filters = this.effects.list;
			this.bounds = bounds;
			this.backgrondShape = new Shape();
			this.innerStrokeShape = new Shape();
			if (this.innerStroke != null && this.innerStroke.background != null && this.innerStroke.background.effects != null)
				this.innerStrokeShape.filters = this.innerStroke.background.effects.list;
			this.outerStrokeShape = new Shape();
			if (this.outerStroke != null && this.outerStroke.background != null && this.outerStroke.background.effects != null)
				this.outerStrokeShape.filters = this.outerStroke.background.effects.list;
			if (this.background != null && this.background.effects != null)
				this.backgrondShape.filters = this.background.effects.list;
			this.container.addChild(this.backgrondShape);
			this.container.addChild(this.innerStrokeShape);
			this.container.addChild(this.outerStrokeShape);
			if (this.allowCrop) this.mask = new Shape(); 
			return this.container;
		}
		
		public function getMask():Shape { return null; }
		
		public function draw():void {
		}
		
		public function resize(newBounds:Rectangle):void {
			this.bounds = bounds;
			this.container.graphics.clear();
			this.innerStrokeShape.graphics.clear();
			this.backgrondShape.graphics.clear();
			this.outerStrokeShape.graphics.clear();
			this.draw();
		}
		
	}
}