package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.circular.MarkerPointerStyleState;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	public final class MarkerCircularGaugePointer extends CircularGaugePointer {
		
		private var markerDrawer:MarkerDrawer;
		
		public function MarkerCircularGaugePointer() {
			super();
			this.markerDrawer = new MarkerDrawer();
		}
		
		override protected function getStyleNodeName():String {
			return "marker_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return MarkerPointerStyleState;
		}
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.markerDrawer.initialize(this.pointerShape);
			return this.container;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var state:MarkerPointerStyleState = MarkerPointerStyleState(this.currentState);
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			
			this.markerDrawer.draw(axis, state.marker, this, state.align, state.padding, state.fill, state.hatchFill, state.stroke, state.effects);
		}
		
		override public function getAnchorPoint(anchor:uint, anchorPos:Point):void {
			var state:MarkerPointerStyleState = MarkerPointerStyleState(this.currentState);
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			
			var percentW:Number = (state.marker.isWidthAxisSizeDepend ? axis.width : 1)*state.marker.width;
			var percentH:Number = (state.marker.isHeightAxisSizeDepend ? axis.width : 1)*state.marker.height;
			
			var w:Number = axis.getPixelRadius(percentW);
			var h:Number = axis.getPixelRadius(percentH);
			
			var r:Number = axis.getRadius(state.align, state.padding, 0, false);
			switch (state.align) {
				case GaugeItemAlign.INSIDE: r -= w/2; break;
				case GaugeItemAlign.OUTSIDE: r += w/2; break;
			}
				
			var pos:Point = new Point();
			var angle:Number = axis.scale.transform(this.fixValueScaleOut(this.value));
			axis.setPixelPoint(r, angle, pos);
			 
			this.getRectBasedAnchorPoint(anchor, anchorPos, w, h, r, angle);
		}
	}
}