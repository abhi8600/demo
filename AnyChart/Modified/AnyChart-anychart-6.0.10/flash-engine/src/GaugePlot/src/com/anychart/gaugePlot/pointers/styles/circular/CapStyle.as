package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.visual.BackgroundBasedStroke;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.background.BackgroundBase;
	import com.anychart.visual.effects.EffectsList;
	
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class CapStyle implements IStyleSerializableRes {
		
		private var radius:Number;
		private var isRadiusAxisSizeDepend:Boolean;
		
		private var innerStroke:BackgroundBasedStroke;
		private var outerStroke:BackgroundBasedStroke;
		private var background:BackgroundBase;
		private var effects:EffectsList;
		
		public function CapStyle() {
			this.radius = 0.1;
			this.isRadiusAxisSizeDepend = false;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			if (data.@radius != undefined) {
				this.isRadiusAxisSizeDepend = GaugePlotSerializerBase.isAxisSizeDepend(data.@radius);
				if (this.isRadiusAxisSizeDepend)
					this.radius = GaugePlotSerializerBase.getAxisSizeFactor(data.@radius);
				else
					this.radius = GaugePlotSerializerBase.getSimplePercent(data.@radius);
			}
			if (SerializerBase.isEnabled(data.inner_stroke[0])) {
				this.innerStroke = new BackgroundBasedStroke();
				this.innerStroke.deserialize(data.inner_stroke[0], resources, style);
			}
			if (SerializerBase.isEnabled(data.outer_stroke[0])) {
				this.outerStroke = new BackgroundBasedStroke();
				this.outerStroke.deserialize(data.outer_stroke[0], resources, style);
			}
			if (SerializerBase.isEnabled(data.background[0])) {
				this.background = new BackgroundBase();
				this.background.deserialize(data.background[0], resources, style);
			}
			if (SerializerBase.isEnabled(data.effects[0])) {
				this.effects = new EffectsList();
				this.effects.deserialize(data.effects[0]);
			}
		}
		
		public function getPercentRadius(axis:CircularGaugeAxis):Number {
			return (this.isRadiusAxisSizeDepend ? axis.width : 1)*this.radius;
		}
		
		public function draw(container:Shape, axis:CircularGaugeAxis, color:uint, hatchType:uint):void {
			
			var g:Graphics = container.graphics;
			container.filters = (this.effects == null) ? null : this.effects.list;
			
			var r:Number = (this.isRadiusAxisSizeDepend ? axis.width : 1)*this.radius;
			
			var pivotPoint:Point = CircularGauge(axis.gauge).pixPivotPoint;
			var pixelRadius:Number = axis.getPixelRadius(r);
			
			var bounds:Rectangle = new Rectangle();
			bounds.x = pivotPoint.x - pixelRadius;
			bounds.y = pivotPoint.y - pixelRadius;
			bounds.width = pixelRadius*2;
			bounds.height = pixelRadius*2;
			
			if (this.background != null) {
				if (this.background.hatchFill != null) {
					this.background.hatchFill.beginFill(g, hatchType, color);
					g.drawCircle(pivotPoint.x, pivotPoint.y, pixelRadius);
					g.endFill();
				}
				
				if (this.background.fill != null) 
					this.background.fill.begin(g, bounds, color);
				if (this.background.border != null)
					this.background.border.apply(g, bounds, color);
					
				g.drawCircle(pivotPoint.x, pivotPoint.y, pixelRadius);
				g.lineStyle();
				g.endFill();
			}
			
			var outerR:Number;
			if (this.innerStroke != null && this.innerStroke.background != null) {
				outerR = axis.getPixelRadius(r + this.innerStroke.thickness);
				this.drawCircularStroke(this.innerStroke, g, pivotPoint, pixelRadius, outerR, color, hatchType);
			}
			
			if (this.outerStroke != null && this.outerStroke.background != null) {
				var innerR:Number = axis.getPixelRadius(r + this.innerStroke.thickness);
				outerR = axis.getPixelRadius(r + this.innerStroke.thickness + this.outerStroke.thickness);
				this.drawCircularStroke(this.outerStroke, g, pivotPoint, innerR, outerR, color, hatchType);
			}

		}
		
		private function drawCircularStroke(stroke:BackgroundBasedStroke, g:Graphics, pivotPoint:Point, innerR:Number, outerR:Number, color:uint, hatchType:uint):void {
			var bounds:Rectangle = new Rectangle();
			bounds.x = pivotPoint.x - outerR;
			bounds.y = pivotPoint.y - outerR;
			bounds.width = bounds.height = outerR*2;
			
			if (stroke.background.fill != null)
				stroke.background.fill.begin(g, bounds);
				
			if (stroke.background.border != null)
				stroke.background.border.apply(g, bounds);
			
			DrawingUtils.drawArc(g, pivotPoint.x, pivotPoint.y, 0, 360, innerR, innerR, 0, true);
			DrawingUtils.drawArc(g, pivotPoint.x, pivotPoint.y, 360, 0, outerR, outerR, 0, false);
			g.endFill();
			g.lineStyle();
			
			if (stroke.background.hatchFill != null) {
				stroke.background.hatchFill.beginFill(g, hatchType, color);
				DrawingUtils.drawArc(g, pivotPoint.x, pivotPoint.y, 0, 360, innerR, innerR, 0, true);
				DrawingUtils.drawArc(g, pivotPoint.x, pivotPoint.y, 360, 0, outerR, outerR, 0, false);
				g.endFill();
			}
			
			bounds = null;
		}
		
	}
}