package com.anychart.gaugePlot.pointers.linear {
	import com.anychart.gaugePlot.LinearGauge;
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.linear.TankPointerStyleState;
	import com.anychart.utils.DrawingUtils;
	import com.anychart.visual.color.ColorUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.gradient.GradientEntry;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	public final class TankPointer extends LinearGaugePointer {
		override protected function getStyleNodeName():String {
			return "tank_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return TankPointerStyleState;
		}
		
		override protected function checkMargin():void {
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			var state:TankPointerStyleState = TankPointerStyleState(this.currentState.style.normal);
			var padding:Number = (state.isWidthAxisSizeDepend ? axis.width : 1)*state.width*0.14880952380;
			LinearGauge(this.gauge).endMargin = Math.max(LinearGauge(this.gauge).endMargin, padding);
			LinearGauge(this.gauge).startMargin = Math.max(LinearGauge(this.gauge).startMargin, padding);
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			
			var g:Graphics = this.container.graphics;
			var state:TankPointerStyleState = TankPointerStyleState(this.currentState);
			
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			
			var percentW:Number = (state.isWidthAxisSizeDepend ? axis.width : 1)*state.width;
			
			var start:Number = axis.getComplexPosition(state.align, state.padding, percentW, true);
			var end:Number = axis.getComplexPosition(state.align, state.padding, percentW, false);
			
			var totalMin:Number = axis.scale.transform(axis.scale.minimum);
			var totalMax:Number = axis.scale.transform(axis.scale.maximum);
			var val:Number = axis.scale.transform(this.value);
			
			var targetSize:Number = axis.getTargetSize();
			
			var color:uint = state.getTankColor(this.color);
			
			var w:Number = percentW*targetSize;
			
			if (axis.isHorizontal) {
				if (axis.scale.inverted) {
					this.bounds = new Rectangle(val,start,totalMin-val,w);
					this.drawHorizontalTank(g,new Rectangle(totalMax,start,val-totalMax,w),0xFFFFFF,0.3,true,false);
					this.drawHorizontalTank(g,new Rectangle(val,start,totalMin-val,w),color,1,true);
				}else {
					this.bounds = new Rectangle(totalMin,start,val-totalMin,w);
					this.drawHorizontalTank(g,new Rectangle(totalMin,start,val-totalMin,w),color,1,true);
					this.drawHorizontalTank(g,new Rectangle(val,start,totalMax-val,w),0xFFFFFF,0.3,true,false);
				}
			}else {
				if (axis.scale.inverted) {
					this.bounds = new Rectangle(start,val,w,totalMin-val);
					this.drawVerticalTank(g,new Rectangle(start,val,w,totalMin-val),color,1,false);
					this.drawVerticalTank(g,new Rectangle(start,totalMax,w,val-totalMax),0xFFFFFF,0.3,false,false);
				}else {
					this.bounds = new Rectangle(start,totalMin,w,val-totalMin);
					this.drawVerticalTank(g,new Rectangle(start,val,w,totalMax-val),0xFFFFFF,0.3,true,false);
					this.drawVerticalTank(g,new Rectangle(start,totalMin,w,val-totalMin),color,1,true);
				}
			}
		}
		
		private function drawHorizontalTank(g:Graphics,rc:Rectangle,colorSrc:uint,opacity:Number,inverted:Boolean,drawBottomBlik:Boolean=true):void {
			var color:uint = ColorUtils.getDarkColor(colorSrc);
			var color2:uint = ColorUtils.getLightColor(colorSrc);
			
			var xR:Number = rc.height*0.14880952380;
			var yR:Number = rc.height/2;
			
			var grd:Gradient = new Gradient(1);
			{
				grd.entries = [new GradientEntry(color, 0, opacity),
							   new GradientEntry(color, 0.05, opacity),
							   new GradientEntry(color2,0.85,opacity),
							   new GradientEntry(color2,0.85,opacity),
							   new GradientEntry(color,1,opacity)
							  ];
				grd.angle = -90;
				grd.beginGradientFill(g, rc);
				g.lineStyle();
				DrawingUtils.drawArc(g,rc.x,rc.y+rc.height/2,90,270,yR,xR,1,true);
				g.lineTo(rc.x+rc.width,rc.y);
				DrawingUtils.drawArc(g,rc.x+rc.width,rc.y+rc.height/2,180+90,360+90,yR,xR,1,false);
				g.lineStyle();
				g.endFill();
			}
			
			var tmpX1:Number = rc.x+rc.width-xR;
			var tmpX2:Number = rc.x+1;
			
			// Draw top blik and shade
			g.lineStyle(3,0xFFFFFF,0.3);
			g.beginFill(0,0);
			g.drawEllipse(tmpX1,rc.y+1,xR*2,rc.height);
			g.lineStyle();
		
			// Draw bottom blik
			if(drawBottomBlik) {
				g.lineStyle(3,0xFFFFFF,0.3);
				g.beginFill(0,0);
				DrawingUtils.drawArc(g,tmpX2,rc.y+rc.height/2-1,90,270,yR,xR,1,true);
				g.lineStyle();
			}
			
			// Draw blick.
			grd = new Gradient(1);
			grd.entries = [new GradientEntry(0xFFFFFF,0,0),
						   new GradientEntry(0xFFFFFF,0.2,Number(160.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.25,Number(140.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.3,Number(30.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.35,0),
						   new GradientEntry(0xFFFFFF,1,0)
						  ];
			grd.angle = -90;
			grd.beginGradientFill(g, rc);
			DrawingUtils.drawArc(g,rc.x,rc.y+rc.height/2,90,270,yR,xR,1,true);
			g.lineTo(rc.x+rc.width,rc.y);
			// g.lineTo(rc.x,rc.y+rc.height);
			DrawingUtils.drawArc(g,rc.x+rc.width,rc.y+rc.height/2,180+90,360+90,yR,xR,1,false);
			g.lineStyle();
			g.endFill();
			
			// Draw top blik and shade
			grd = new Gradient(1);
			grd.angle=50+90;
			grd.entries = [new GradientEntry(0xFFFFFF,0,0.1*opacity),
						   new GradientEntry(color, 1, opacity)
						  ];
			g.lineStyle(0,0,0);
			// g.beginFill(color,0.05);
			var rcc:Rectangle=new Rectangle(tmpX1,rc.y+1,xR*2,rc.height);
			grd.beginGradientFill(g,rcc);
			g.drawEllipse(tmpX1,rc.y+1,xR*2,rc.height-3);
			g.lineStyle();
			
		}
		
		private function drawVerticalTank(g:Graphics,rc:Rectangle,colorSrc:uint,opacity:Number,inverted:Boolean,drawBottomBlik:Boolean=true):void {
			var color:uint = ColorUtils.getDarkColor(colorSrc);
			var color2:uint = ColorUtils.getLightColor(colorSrc);
			
			var grd:Gradient = new Gradient(1);
			{
				grd.entries = [new GradientEntry(color, 0, opacity),
							   new GradientEntry(color, 0.05, opacity),
							   new GradientEntry(color2,0.85,opacity),
							   new GradientEntry(color2,0.85,opacity),
							   new GradientEntry(color,1,opacity)
							  ];
				
				grd.beginGradientFill(g, rc);
				g.lineStyle();
				DrawingUtils.drawArc(g,rc.x+rc.width/2,rc.y,180,360,rc.width*0.14880952380,rc.width/2,1,true);
				g.lineTo(rc.x+rc.width,rc.y+rc.height);
				DrawingUtils.drawArc(g,rc.x+rc.width/2,rc.y+rc.height,0,180,rc.width*0.14880952380,rc.width/2,1,false);
				g.lineStyle();
				g.endFill();
			}
			
			var tmpY1:Number = rc.y-rc.width*0.14880952380+1;
							
			var tmpY2:Number = rc.y+rc.height;
			
			// Draw top blik and shade
			g.lineStyle(3,0xFFFFFF,0.3);
			g.beginFill(0,0);
			g.drawEllipse(rc.x+1,tmpY1,rc.width-3,(rc.width*0.14880952380)*2);
			g.lineStyle();
		
			// Draw bottom blik
			if(drawBottomBlik) {
				g.lineStyle(3,0xFFFFFF,0.3);
				g.beginFill(0,0);
				DrawingUtils.drawArc(g,rc.x+rc.width/2,tmpY2-1,0,180,rc.width*0.14880952380,rc.width/2,1,true);
				g.lineStyle();
			}
			
			// Draw blick.
			grd = new Gradient(1);
			grd.entries = [new GradientEntry(0xFFFFFF,0,0),
						   new GradientEntry(0xFFFFFF,0.2,Number(160.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.25,Number(140.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.3,Number(30.0/255.0)*opacity),
						   new GradientEntry(0xFFFFFF,0.35,0),
						   new GradientEntry(0xFFFFFF,1,0)
						  ];
			grd.beginGradientFill(g, rc);
			DrawingUtils.drawArc(g,rc.x+rc.width/2,rc.y,180,360,rc.width*0.14880952380,rc.width/2,1,true);
			g.lineTo(rc.x+rc.width,rc.y+rc.height);
			// g.lineTo(rc.x,rc.y+rc.height);
			DrawingUtils.drawArc(g,rc.x+rc.width/2,rc.y+rc.height,0,180,rc.width*0.14880952380,rc.width/2,1,false);
			g.lineStyle();
			g.endFill();
			
			// Draw top blik and shade
			grd = new Gradient(1);
			grd.angle=50;
			grd.entries = [new GradientEntry(0xFFFFFF,0,0.1*opacity),
						   new GradientEntry(color, 1, opacity)
						  ];
			g.lineStyle(0,0,0);
			// g.beginFill(color,0.05);
			var rcc:Rectangle=new Rectangle(rc.x+1,tmpY1,rc.width-3,(rc.width*0.14880952380)*2);
			grd.beginGradientFill(g,rcc);
			g.drawEllipse(rc.x+1,tmpY1,rc.width-3,(rc.width*0.14880952380)*2);
			g.lineStyle();
			
		}
	}
}