package com.anychart.gaugePlot.axis.labels {
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.geom.Point;
	
	public final class LinearGaugeAxisLabels extends GaugeAxisLabels {
		
		override public function getPercentFontSize(size:Number):Number {
			return LinearGaugeAxis(this.axis).getTargetSize()*size;
		}
		
		override public function getPosition(info:TextElementInformation, 
										pixValue:Number,
										text:BaseTextElement,
										align:uint,
										padding:Number):Point {
			var pos:Point = new Point();
			if (LinearGaugeAxis(this.axis).isHorizontal) {
				pos.x = pixValue - info.rotatedBounds.width/2;
				pos.y = LinearGaugeAxis(this.axis).getPosition(align, padding, info.rotatedBounds.height, true);
			}else {
				pos.y = pixValue - info.rotatedBounds.height/2;
				pos.x = LinearGaugeAxis(this.axis).getPosition(align, padding, info.rotatedBounds.width, true);
			}
			return pos;
		}
	}
}