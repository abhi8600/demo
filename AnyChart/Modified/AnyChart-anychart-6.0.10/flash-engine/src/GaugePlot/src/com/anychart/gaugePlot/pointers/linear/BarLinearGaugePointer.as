package com.anychart.gaugePlot.pointers.linear {
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.linear.BarPointerStyleState;
	
	import flash.display.Graphics;
	
	public class BarLinearGaugePointer extends LinearGaugePointer {
		override protected function getStyleNodeName():String {
			return "bar_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return BarPointerStyleState;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			var state:BarPointerStyleState = BarPointerStyleState(this.currentState);
			this.drawBar(state.startFromZero ? 0 : axis.scale.minimum, this.fixValueScaleOut(this.value));
		}
		
		protected function drawBar(startValue:Number, endValue:Number):void {
			
			var g:Graphics = this.container.graphics;
			var state:BarPointerStyleState = BarPointerStyleState(this.currentState);
			
			var axis:LinearGaugeAxis = LinearGaugeAxis(this.axis);
			
			var percentW:Number = (state.isWidthAxisSizeDepend ? axis.width : 1)*state.width;
			
			var start:Number = axis.getComplexPosition(state.align, state.padding, percentW, true);
			var end:Number = axis.getComplexPosition(state.align, state.padding, percentW, false);
			
			var valStart:Number = axis.scale.transform(startValue);
			var valEnd:Number = axis.scale.transform(endValue);
			
			if (axis.isHorizontal) {
				bounds.y = Math.min(start,end);
				bounds.height = Math.max(start, end) - bounds.y;
				bounds.x = Math.min(valStart, valEnd);
				bounds.width = Math.max(valStart, valEnd) - bounds.x;
			}else {
				bounds.x = Math.min(start,end);
				bounds.width = Math.max(start, end) - bounds.x;
				bounds.y = Math.min(valStart, valEnd);
				bounds.height = Math.max(valStart, valEnd) - bounds.y;
			}
			
			if (state.fill != null)
				state.fill.begin(g, bounds, this._color);
				
			if (state.stroke != null)
				state.stroke.apply(g, bounds, this._color);
				
			g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
			g.endFill();
			g.lineStyle();
			
			if (state.hatchFill != null) {
				state.hatchFill.beginFill(g, this._hatchType, this._color);
				g.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
			}
		}
	}
}