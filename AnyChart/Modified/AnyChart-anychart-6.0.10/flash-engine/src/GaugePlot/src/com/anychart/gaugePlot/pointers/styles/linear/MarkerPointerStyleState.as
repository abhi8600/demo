package com.anychart.gaugePlot.pointers.styles.linear {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.gaugePlot.pointers.styles.MarkerStyle;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;
	
	public class MarkerPointerStyleState extends GaugePointerStyleState {
		
		public function get width():Number { return this.marker.width; }
		public function get height():Number { return this.marker.height; }
		public function get shape():uint { return this.marker.shape; }
		
		public function get markerStyle():MarkerStyle { return this.marker; }
		
		private var marker:MarkerStyle;
		
		public function MarkerPointerStyleState(style:Style) {
			super(style);
			this.marker = new MarkerStyle(false);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.marker.deserialize(data, resources, this.style);
			return data;
		}

	}
}