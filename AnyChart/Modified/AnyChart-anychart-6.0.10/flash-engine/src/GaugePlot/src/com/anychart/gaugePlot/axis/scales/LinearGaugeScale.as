package com.anychart.gaugePlot.axis.scales {
	import com.anychart.scales.BaseScale;
	
	public class LinearGaugeScale extends BaseScale {
		
		override public function calculate():void {
			
			var isLabelsCalculated:Boolean = false;
			
			if (this.isMinimumAuto ||
			    this.isMaximumAuto ||
			    this.isMajorIntervalAuto ||
			    this.isMinorIntervalAuto) {
		
				//применение разброса значений и minimumOffset с maximumOffset
				super.calculate();
				
				//различные проверки минимума и максимума
				if (this.isMinimumAuto || this.isMaximumAuto) {
					
					if ((this.dataRangeMaximum - this.dataRangeMinimum) == 0 && this.isMinimumAuto && this.isMaximumAuto) {
						this.minimum = this.dataRangeMinimum - .5;
						this.maximum = this.dataRangeMaximum + .5;
					}
					
					//проверка range на ноль
					if ((this.maximum - this.minimum) < Number.MIN_VALUE) {
						if (this.isMaximumAuto)
							this.maximum = this.maximum + .2 * (this.maximum == 0 ? 1 : Math.abs(this.maximum));
						if (this.isMinimumAuto)
							this.minimum = this.minimum - .2 * (this.minimum == 0 ? 1 : Math.abs(this.minimum));
					}
					
					//проверка минимума на ноль
					if (this.isMinimumAuto && this.minimum > 0 && this.minimum / (this.maximum - this.minimum) < .25)
						this.minimum = 0;
						
					//проверка максимума на нол
					if (this.isMaximumAuto && this.maximum < 0 && Math.abs(this.maximum/(this.maximum - this.minimum)) < .25)
						this.maximum = 0;
				}
				
				//подсчет мажорного и минорного интервала
				if (this.isMajorIntervalAuto) {
					
					this.majorInterval = this.calculateOptimalStepSize(this.maximum - this.minimum, this.optimalMajorSteps, false);
					/* if (this.axis.labels != null && !this.axis.labels.allowOverlap) {
						this.calculateBaseValue();
						this.calculateMajorTicksCount();
						var maxLabels:int = this.axis.labels.calcMaxLabelsCount();
						if (maxLabels <= (this.maximum - this.minimum)/this.majorInterval)
							this.majorInterval = this.calculateOptimalStepSize(this.maximum - this.minimum, maxLabels, true);
						isLabelsCalculated = true;
					} */
				}
	
				if (this.isMinorIntervalAuto)
					this.minorInterval = this.calculateOptimalStepSize(this.majorInterval, 5, true);
				
				//применение мажорного интервала к минимуму и максимуму
				if (this.isMinimumAuto)
					this.minimum -= this.safeMod(this.minimum, this.majorInterval);
					
				if (this.isMaximumAuto) {
					var d:Number = safeMod(this.maximum, this.majorInterval);
					if (d != 0)
						this.maximum += this.majorInterval - d;
				}
		    }
		    
		    /* if (!isLabelsCalculated && this.axis.labels != null)
	    		this.axis.labels.initMaxLabelSize(); */
			
			this.calculateBaseValue();
			this.calculateTicksCount();
			this.calculateMinorStart();
		}
		
		override protected function calculateBaseValue():void {
			if (!this.isBaseValueAuto) return;
			this.baseValue = Math.ceil(Number(this.minimum)/Number(this.majorInterval) - 0.00000001 )* Number(this.majorInterval);
		}
	}
}