package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.CircularGauge;
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.styles.circular.CircularGaugePointerStyleState;
	import com.anychart.styles.states.StyleStateWithEffects;
	import com.anychart.visual.layout.Anchor;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class CircularGaugePointer extends GaugePointer {
		
		private var capShape:Shape;
		protected var pointerShape:Sprite;
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.capShape = new Shape();
			this.pointerShape = new Sprite();
			
			this.container.addChild(this.pointerShape);
			this.container.addChild(this.capShape);
			return this.container;
		}
		
		override protected function execDrawing():void {
			this.setFiltersAndClear();
			this.drawCap();
		}
		
		override protected function setFiltersAndClear():void {
			this.pointerShape.filters = StyleStateWithEffects(this.currentState).effects != null ? StyleStateWithEffects(this.currentState).effects.list : null;
			this.capShape.graphics.clear();
			this.pointerShape.graphics.clear();
		}
		
		protected function drawCap():void {
			if (this.currentState == null) return;
			var state:CircularGaugePointerStyleState = CircularGaugePointerStyleState(this.currentState);
			if (state.cap != null)
				state.cap.draw(this.capShape, CircularGaugeAxis(this.axis), this._color, this._hatchType);
		}
		
		override protected function changeValue(event:MouseEvent,stageMouseX:Number, stageMouseY:Number):void {
			var pt:Point = new Point(stageMouseX, stageMouseY);
			pt = this.gauge.container.globalToLocal(pt);
			pt.x -= CircularGauge(this.gauge).pixPivotPoint.x;
			pt.y -= CircularGauge(this.gauge).pixPivotPoint.y;
			var angle:Number = Math.atan2(pt.y, pt.x)*180/Math.PI;
			while (angle < this.axis.scale.pixelRangeMinimum) {
				angle += 360;
			}
			while (angle > this.axis.scale.pixelRangeMaximum) {
				angle -= 360;
			}
			this.value = this.axis.scale.transformToValue(angle);
			this.updateFixedValue(this.value);
		}
		
		protected function getRectBasedAnchorPoint(anchor:uint, 
												   anchorPos:Point, 
												   w:Number, h:Number, r:Number,
												   angle:Number):void {
			
			var leftTop:Point = new Point();
			var rightTop:Point = new Point();
			var leftBottom:Point = new Point();
			var rightBottom:Point = new Point();
			
			var sin:Number = Math.sin(angle*Math.PI/180);
			var cos:Number = Math.cos(angle*Math.PI/180);
			
			var center:Point = CircularGauge(axis.gauge).pixPivotPoint;
			var c1x:Number = center.x + (w/2)*sin;
			var c1y:Number = center.y - (w/2)*cos;
			var c2x:Number = center.x - (w/2)*sin;
			var c2y:Number = center.y + (w/2)*cos;
			
			leftBottom.x = c1x+(r-h/2)*cos;
			leftBottom.y = c1y+(r-h/2)*sin;
			
			rightTop.x = c2x+(r+h/2)*cos;
			rightTop.y = c2y+(r+h/2)*sin;
			
			leftTop.x = c1x+(r+h/2)*cos;
			leftTop.y = c1y+(r+h/2)*sin;
			
			rightBottom.x = c2x+(r-h/2)*cos;
			rightBottom.y = c2y+(r-h/2)*sin;
			
			switch (anchor) {
				case Anchor.CENTER:
					anchorPos.x = (leftTop.x + rightBottom.x)/2;
					anchorPos.y = (leftTop.y + rightBottom.y)/2;
					break;
				case Anchor.CENTER_BOTTOM:
					anchorPos.x = (leftBottom.x + rightBottom.x)/2;
					anchorPos.y = (leftBottom.y + rightBottom.y)/2;
					break;
				case Anchor.CENTER_LEFT:
					anchorPos.x = (leftTop.x + leftBottom.x)/2;
					anchorPos.y = (leftTop.y + leftBottom.y)/2;
					break;
				case Anchor.CENTER_RIGHT:
					anchorPos.x = (rightBottom.x + rightTop.x)/2;
					anchorPos.y = (rightBottom.y + rightTop.y)/2;
					break;
				case Anchor.CENTER_TOP:
					anchorPos.x = (leftTop.x + rightTop.x)/2;
					anchorPos.y = (leftTop.y + rightTop.y)/2;
					break;
				case Anchor.LEFT_BOTTOM:
					anchorPos.x = leftBottom.x;
					anchorPos.y = leftBottom.y;
					break;
				case Anchor.LEFT_TOP:
					anchorPos.x = leftTop.x;
					anchorPos.y = leftTop.y;
					break;
				case Anchor.RIGHT_BOTTOM:
					anchorPos.x = rightBottom.x;
					anchorPos.y = rightBottom.y;
					break;
				case Anchor.RIGHT_TOP:
					anchorPos.x = rightTop.x;
					anchorPos.y = rightTop.y;
					break;
			}
		}
		
	}
}