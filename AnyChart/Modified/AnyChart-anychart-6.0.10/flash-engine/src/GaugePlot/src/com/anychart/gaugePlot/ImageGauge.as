package com.anychart.gaugePlot {
	import com.anychart.resources.ResourcesHashMap;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.fill.Fill;
	
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class ImageGauge extends GaugeBase {
		
		private var fill:Fill;
		private var isFitByProportions:Boolean;
		private var imageSrc:String;
		
		private var sizeInPixels:Boolean;
		
		public function ImageGauge() {
			super();
			this.fill = new Fill();
			this.isFitByProportions = true;
			this.sizeInPixels = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			var fillNode:XML = <fill type="image" />;
			if (data.@src != undefined) { 
				fillNode.@image_url = data.@src;
				this.imageSrc = data.@src;
			}
			if (data.@mode != undefined) { 
				fillNode.@image_mode = data.@mode;
				this.isFitByProportions = SerializerBase.getEnumItem(data.@mode) == "fitbyproportions";
			}else {
				fillNode.@image_mode = "fitbyproportions";
			}
			if (data.@size_in_pixels != undefined) this.sizeInPixels = SerializerBase.getBoolean(data.@size_in_pixels);
			this.fill.deserialize(fillNode, resources);
			this.imageSrc = this.fill.imageUrl; 
		}
		
		override protected function setBounds(newBounds:Rectangle):void {
			super.setBounds(newBounds);
			if (this.sizeInPixels) {
				this.bounds.width = this.width*100;
				this.bounds.height = this.height*100;
			}
			if (this.imageSrc == null) return;
			if (this.isFitByProportions) {
				var bitmap:BitmapData = ResourcesHashMap.getImage(this.imageSrc);
				
				var xScale:Number = this.bounds.width/bitmap.width;
				var yScale:Number = this.bounds.height/bitmap.height;
				var scale:Number = Math.min(xScale,yScale);
				
				this.bounds.x = this.bounds.x + (this.bounds.width - bitmap.width*scale)/2;
				this.bounds.y = this.bounds.y + (this.bounds.height - bitmap.height*scale)/2;
				this.bounds.width = bitmap.width*scale;
				this.bounds.height = bitmap.height*scale;
			}
		}
		
		override public function draw():void {
			var g:Graphics = this.container.graphics;
			if (this.imageSrc == null)
				return;
			this.fill.begin(g, this.bounds);
			g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			g.endFill();
		}
		
		override public function resize(newBounds:Rectangle, setBounds:Boolean=true):void {
			super.resize(newBounds, setBounds);
			this.container.graphics.clear();
			this.draw();
		}
	}
}