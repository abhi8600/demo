package com.anychart.gaugePlot.visual.layout {
	public final class GaugeItemAlign {
		public static const INSIDE:uint = 0;
		public static const OUTSIDE:uint = 1;
		public static const CENTER:uint = 2;
	}
}