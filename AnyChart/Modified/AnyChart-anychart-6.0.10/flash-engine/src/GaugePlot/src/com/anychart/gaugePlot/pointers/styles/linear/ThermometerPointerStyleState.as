package com.anychart.gaugePlot.pointers.styles.linear {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.background.BackgroundBase;
	
	public final class ThermometerPointerStyleState extends GaugePointerStyleState {
		
		public var width:Number;
		public var isWidthAxisSizeDepend:Boolean;
		public var capRadius:Number;
		public var isCapRadiusAxisSideDepend:Boolean;
		public var capPadding:Number;
		
		public var bulb:BackgroundBase;
		
		public function ThermometerPointerStyleState(style:Style) {
			super(style);
			this.capRadius = .15;
			this.capPadding = 0;
			this.isWidthAxisSizeDepend = true;
			this.isCapRadiusAxisSideDepend = true;
			this.width = 1;
			this.capRadius = 1;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@width != undefined) {
				this.isWidthAxisSizeDepend = this.isAxisSizeDepend(data.@width);
				this.width = this.isWidthAxisSizeDepend ? this.getAxisSizeFactor(data.@width) : (SerializerBase.getNumber(data.@width)/100);
			}
			if (data.@bulb_radius != undefined) {
				this.isCapRadiusAxisSideDepend = this.isAxisSizeDepend(data.@bulb_radius);
				this.capRadius = this.isCapRadiusAxisSideDepend ? this.getAxisSizeFactor(data.@bulb_radius) : SerializerBase.getNumber(data.@bulb_radius)/100;
			}
			if (data.@bulb_padding != undefined) this.capPadding = SerializerBase.getNumber(data.@bulb_padding)/100;
			if (SerializerBase.isEnabled(data.bulb[0])) {
				this.bulb = new BackgroundBase();
				this.bulb.deserialize(data.bulb[0], resources, this.style);
			}
			return data;
		}
	}
}