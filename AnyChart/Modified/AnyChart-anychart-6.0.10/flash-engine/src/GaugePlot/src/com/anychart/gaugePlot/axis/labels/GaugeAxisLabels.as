package com.anychart.gaugePlot.axis.labels {
	import com.anychart.formatters.FormatsParser;
	import com.anychart.formatters.IFormatable;
	import com.anychart.gaugePlot.GaugePlotSerializerBase;
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.visual.layout.GaugeItemAlign;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.BaseTextElementWithEmbedFonts;
	import com.anychart.visual.text.EmbedableFontSettings;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class GaugeAxisLabels implements IFormatable {
		
		protected var text:BaseTextElementWithEmbedFonts;
		protected var align:uint;
		protected var padding:Number;
		private var showFirst:Boolean;
		private var showLast:Boolean;
		
		public var axis:GaugeAxis;
		
		private var isDynamicFontSize:Boolean;
		private var percentFontSize:Number;
		
		public function get enabled():Boolean {
			return this.text.enabled;
		}
		
		public function GaugeAxisLabels() {
			this.text = new BaseTextElementWithEmbedFonts();
			this.text.enabled = false;
			this.align = GaugeItemAlign.INSIDE;
			this.padding = .02;
			this.showFirst = true;
			this.showLast = true;
			this.isDynamicFontSize = false;
		}
		
		public function deserialize(data:XML, axisWidth:Number, resources:ResourcesLoader):void {
			if (data.font[0] != null) {
				if (data.font[0].@size != undefined && data.font[0].@family != undefined) {
					var family:String = SerializerBase.getString(data.font[0].@family);
					if (family.indexOf(".swf") == family.length - 4) { 
						if (String(data.font[0].@size).indexOf("%") == 0)
							data.font[0].@size = GaugePlotSerializerBase.getAxisSizeDependValue(data.font[0].@size, axisWidth)*100;
					}
				}
			}
			this.text.deserialize(data, resources);
			if (data.format[0] != null) {
				this.text.text = SerializerBase.getCDATAString(data.format[0]);
				this.text.isDynamicText = FormatsParser.isDynamic(this.text.text);
				if (this.text.isDynamicText)
					this.text.dynamicText = FormatsParser.parse(this.text.text);
			}
			if (data.@align != undefined) this.align = GaugePlotSerializerBase.getAlign(data.@align);
			if (data.@padding != undefined) this.padding = GaugePlotSerializerBase.getSimplePercent(data.@padding);
			if (data.@show_first != undefined) this.showFirst = SerializerBase.getBoolean(data.@show_first);
			if (data.@show_last != undefined) this.showLast = SerializerBase.getBoolean(data.@show_last);
			if (EmbedableFontSettings(this.text.font).isEmbed) {
				this.isDynamicFontSize = true;
				this.percentFontSize = this.text.font.size/100;
			}
		}
		
		public function getPercentFontSize(size:Number):Number { return 0; }
		
		private var container:Sprite;
		public function initialize(axis:GaugeAxis):DisplayObject {
			this.axis = axis; 
			this.container = new Sprite();
			return this.container;
		}
		
		public function draw(scale:BaseScale):void {
			var cnt:int = scale.majorIntervalsCount;
			var start:int = this.showFirst ? 0 : 1;
			var end:int = this.showLast ? cnt : cnt-1;
			for (var i:int = start;i<=end;i++) {
				this.tmpValue = scale.getMajorIntervalValue(i);
				this.drawLabel(scale.localTransform(tmpValue)); 
			}
		}
		
		public function clear():void {
			this.container.graphics.clear();
			while (this.container.numChildren > 0)
				this.container.removeChildAt(0);
		}
		
		private var tmpValue:Number;
		public function getTokenValue(token:String):* {
			if (token == '%Value') return this.axis.scale.delinearize(tmpValue); 
			return '';
		}
		
		public function isDateTimeToken(token:String):Boolean { return false; }
		
		private function drawLabel(pixValue:Number):void {
			
			if (this.isDynamicFontSize) {
				this.text.font.size = this.getPercentFontSize(this.percentFontSize);
				this.text.updateField();
			}
			
			var info:TextElementInformation = this.text.createInformation();
			info.formattedText = this.text.isDynamicText ? this.text.dynamicText.getValue(this.getTokenValue, this.isDateTimeToken) : this.text.text;
			
			this.setAngle(pixValue);
			this.text.getBounds(info);
			var pos:Point = this.getPosition(info, pixValue, this.text, this.align, this.padding);
			this.text.drawEmbed(this.container, pos.x, pos.y, info, 0, 0);
			this.clearAngle(pixValue);
			info = null;
		}
		
		protected function setAngle(value:Number):void {}
		protected function clearAngle(value:Number):void {}
		
		public function getPosition(info:TextElementInformation, 
										pixValue:Number,
										text:BaseTextElement,
										align:uint,
										padding:Number):Point { return null; }
	}
}