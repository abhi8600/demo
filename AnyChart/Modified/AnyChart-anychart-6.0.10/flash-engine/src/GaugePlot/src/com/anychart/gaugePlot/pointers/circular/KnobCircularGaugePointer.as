package com.anychart.gaugePlot.pointers.circular {
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.pointers.styles.circular.KnobPointerStyleState;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.geom.Point;
	
	public class KnobCircularGaugePointer extends CircularGaugePointer {
		
		private var markerDrawer:MarkerDrawer;
		private var needleDrawer:NeedleDrawer;
		private var knobContainer:Shape;
		
		public function KnobCircularGaugePointer() {
			super();
			this.markerDrawer = new MarkerDrawer();
			this.needleDrawer = new NeedleDrawer();
		}
		
		override protected function getStyleNodeName():String {
			return "knob_pointer_style";
		}
		
		override protected function getStyleStateClass():Class {
			return KnobPointerStyleState;
		}
		
		override public function initialize():DisplayObject {
			this.knobContainer = new Shape();
			this.container.addChild(this.knobContainer);
			super.initialize();
			this.markerDrawer.initialize(this.pointerShape);
			return this.container;
		}
		
		override protected function execDrawing():void {
			super.execDrawing();
			this.markerDrawer.clear();
			this.knobContainer.graphics.clear();
			
			var axis:CircularGaugeAxis = CircularGaugeAxis(this.axis);
			var state:KnobPointerStyleState = KnobPointerStyleState(this.currentState);
			
			if (state.marker != null) {
				this.markerDrawer.draw(axis, state.marker, this, state.marker.align, state.marker.padding, state.marker.background.fill, state.marker.background.hatchFill, state.marker.background.border, state.marker.background.effects);
			}  
			
			if (state.knob != null) {
				state.knob.draw(this.knobContainer, this, axis);
				this.knobContainer.rotation = axis.scale.transform(this.fixValueScaleOut(this.value));
			}
			
			if (state.needle != null) {
				this.needleDrawer.draw(this.pointerShape, this, state.needle, axis, state.needle.background.fill, state.needle.background.hatchFill, state.needle.background.border, state.needle.background.effects);
			}
		}
		
		override public function getAnchorPoint(anchor:uint, anchorPos:Point):void {
			
			var angle:Number = axis.scale.transform(this.fixValueScaleOut(this.value));
			
			var outerR:Number = Math.max(this.needleDrawer.pixelBaseRadius, this.needleDrawer.pixelPointRadius, this.needleDrawer.pixelRadius);
			var innerR:Number = Math.min(this.needleDrawer.pixelBaseRadius, this.needleDrawer.pixelPointRadius, this.needleDrawer.pixelRadius);
			var w:Number = Math.max(this.needleDrawer.pixelPointThickness, this.needleDrawer.pixelThickness); 
			
			this.getRectBasedAnchorPoint(anchor, anchorPos, w, (outerR - innerR), (innerR + outerR)/2, angle);
		}
	}
}