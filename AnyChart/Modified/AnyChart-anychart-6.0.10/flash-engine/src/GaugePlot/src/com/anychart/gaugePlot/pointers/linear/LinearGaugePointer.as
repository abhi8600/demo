package com.anychart.gaugePlot.pointers.linear {
	import com.anychart.gaugePlot.axis.LinearGaugeAxis;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class LinearGaugePointer extends GaugePointer {
		protected function checkMargin():void {}
		
		override public function initialize():DisplayObject {
			super.initialize();
			this.checkMargin();
			return this.container;
		}
		
		override protected function changeValue(event:MouseEvent, stageMouseX:Number, stageMouseY:Number):void {
			var pt:Point = new Point(stageMouseX, stageMouseY);
			pt = this.gauge.container.globalToLocal(pt);
			var newValue:Number = LinearGaugeAxis(this.axis).isHorizontal ? pt.x : pt.y;
			newValue = this.axis.scale.transformToValue(newValue);			
			this.value = newValue;
			this.updateFixedValue(this.value);
		}
	}
}