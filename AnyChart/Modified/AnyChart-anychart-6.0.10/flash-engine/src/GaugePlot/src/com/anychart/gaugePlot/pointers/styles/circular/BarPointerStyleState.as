package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	
	public final class BarPointerStyleState extends CircularGaugePointerStyleState {
		
		public var startFromZero:Boolean;
		public var width:Number;
		public var isAxisSizeWidth:Boolean;
		
		public function BarPointerStyleState(style:Style) {
			super(style);
			this.startFromZero = false;
			this.width = .1;
			this.isAxisSizeWidth = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@width != undefined) {
				this.isAxisSizeWidth = this.isAxisSizeDepend(data.@width);
				if (this.isAxisSizeWidth)
					this.width = this.getAxisSizeFactor(data.@width);
				else
					this.width = SerializerBase.getNumber(data.@width)/100;
			}
			if (data.@start_from_zero != undefined) this.startFromZero = SerializerBase.getBoolean(data.@start_from_zero);
			return data;
		}
	}
}