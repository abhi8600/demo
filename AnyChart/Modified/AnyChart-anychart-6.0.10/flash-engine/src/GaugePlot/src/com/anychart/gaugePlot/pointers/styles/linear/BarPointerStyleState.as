package com.anychart.gaugePlot.pointers.styles.linear {
	import com.anychart.gaugePlot.pointers.styles.GaugePointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	
	public final class BarPointerStyleState extends GaugePointerStyleState {
		
		public var width:Number;
		public var isWidthAxisSizeDepend:Boolean;
		public var startFromZero:Boolean;
		
		public function BarPointerStyleState(style:Style) {
			super(style);
			this.width = .1;
			this.isWidthAxisSizeDepend = false;
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			if (data.@width != undefined) {
				this.isWidthAxisSizeDepend = this.isAxisSizeDepend(data.@width);
				this.width = this.isWidthAxisSizeDepend ? this.getAxisSizeFactor(data.@width) : (SerializerBase.getNumber(data.@width)/100);
			}
			if (data.@start_from_zero != undefined) this.startFromZero = SerializerBase.getBoolean(data.@start_from_zero);
			return data;
		}
	}
}