package com.anychart.gaugePlot.axis.markers {
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.visual.text.GaugeBaseTextElement;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class AxisMarkersList {
		protected var markers:Array;
		protected var container:Sprite;
		
		public var axis:GaugeAxis;
		public var scale:BaseScale;
		
		public function AxisMarkersList() {
			this.markers = [];
		} 
		
		public function deserialize(data:XML, localStyles:StylesList, styles:XML, axisWidth:Number, resources:ResourcesLoader):void {
			var i:int;
			var child:XMLList;
			var childCnt:int;
			
			if (data.color_ranges[0] != null) {
				child = data.color_ranges[0].color_range;
				childCnt = child.length();
				for (i=0;i<childCnt;i++) {
					if (SerializerBase.isEnabled(child[i]))
						this.addMarker(new GaugeAxisRangeMarker(), child[i], resources, axisWidth, styles, localStyles);
				}
			}
			
			if (data.trendlines[0] != null) {
				child = data.trendlines[0].trendline;
				childCnt = child.length();
				for (i=0;i<childCnt;i++){
					if (SerializerBase.isEnabled(child[i]))
						this.addMarker(new GaugeAxisLineMarker(), child[i], resources, axisWidth, styles, localStyles);
				}
			}
			
			if (data.custom_labels[0] != null) {
				child = data.custom_labels[0].custom_label;
				childCnt = child.length();
				for (i=0;i<childCnt;i++){
					if (SerializerBase.isEnabled(child[i]))
						this.addMarker(new GaugeAxisLabelMarker(), child[i], resources, axisWidth, styles, localStyles);
				}
			}
		}
		
		private function addMarker(marker:GaugeAxisMarker, data:XML, resources:ResourcesLoader, axisWidth:Number, stylesList:XML, localStyles:StylesList):void {
			marker.gauge = this.axis.gauge;
			marker.deserialize(data, axisWidth, resources, stylesList, localStyles);
			marker.checkScale(this.scale);
			this.markers.push(marker);
		}
		
		public function initialize():DisplayObject {
			this.container = new Sprite();
			for (var i:uint = 0;i<this.markers.length;i++)
				this.container.addChild(this.markers[i].initialize());
			return this.container;
		}
		
		public function draw():void {
			for (var i:uint = 0;i<this.markers.length;i++) {
				if (this.markers[i] is GaugeAxisRangeMarker)
					this.drawRangeMarker(this.markers[i]);
				else if (this.markers[i] is GaugeAxisLabelMarker)
					this.drawLabelMarker(this.markers[i]);
				else if (this.markers[i] is GaugeAxisLineMarker)
					this.drawLineMarker(this.markers[i]);
			}
		}
		
		public final function clear():void {
			for (var i:uint = 0;i<this.markers.length;i++)
				this.markers[i].clear();
		}
		
		protected function drawRangeMarker(marker:GaugeAxisRangeMarker):void {
		}
		
		protected function drawLineMarker(marker:GaugeAxisLineMarker):void {
		}
		
		protected function drawLabelMarker(marker:GaugeAxisLabelMarker):void {
			var state:CusomLabelStyleState = CusomLabelStyleState(marker.getActualState());
			if (state.tickmark != null) {
				this.axis.drawTickmark(state.tickmark, this.scale.transform(marker.value), marker.color);
			}
			if (state.label != null)
				this.drawMarkerLabel(marker, state.label, marker.value, marker.color);
		}
		
		protected function drawMarkerLabel(marker:GaugeAxisMarker, label:AxisMarkerLabel, defaultValue:Number, color:uint = 0):void {
			var text:GaugeBaseTextElement = label.label;
			var info:TextElementInformation = text.createInformation();
			info.formattedText = text.text;
			if (label.isDynamicFontSize) {
				var size:Number = (label.isFontSizeAxisDepend ? axis.width : 1)*label.fontSize;
				text.font.size = size;
				text.updateField();
			}
			text.getBounds(info);
			var val:Number = this.scale.transform(isNaN(label.value) ? defaultValue : label.value);
			//var pos:Point = this.axis.labels.getPosition(info, val, text, label.align, label.padding);
			var pos:Point = new Point();
			this.getLabelPosition(pos, val, label.align, label.padding);
			
			text.x = pos.x;
			text.y = pos.y;
			text.isPixXY = true;
			text.drawGaugeText(marker.container, 0, 0, this.axis.gauge.bounds.width, this.axis.gauge.bounds.height, info,color,0,true);
		}
		
		protected function getLabelPosition(pos:Point, pixVal:Number, align:uint, padding:Number):void {}
	}
}