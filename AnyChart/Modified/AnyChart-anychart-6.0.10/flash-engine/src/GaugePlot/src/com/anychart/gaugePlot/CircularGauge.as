package com.anychart.gaugePlot {
	import com.anychart.gaugePlot.axis.CircularGaugeAxis;
	import com.anychart.gaugePlot.axis.GaugeAxis;
	import com.anychart.gaugePlot.frame.CircularGaugeFrame;
	import com.anychart.gaugePlot.frame.CircularGaugeFrameAuto;
	import com.anychart.gaugePlot.frame.CircularGaugeFrameAutoRect;
	import com.anychart.gaugePlot.frame.CircularGaugeFrameRect;
	import com.anychart.gaugePlot.frame.GaugeFrame;
	import com.anychart.gaugePlot.pointers.GaugePointer;
	import com.anychart.gaugePlot.pointers.circular.BarCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.circular.KnobCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.circular.MarkerCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.circular.NeedleCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.circular.RangeBarCircularGaugePointer;
	import com.anychart.gaugePlot.pointers.styles.circular.CircularGaugePointerStyleState;
	import com.anychart.gaugePlot.pointers.styles.circular.KnobPointerStyleState;
	import com.anychart.gaugePlot.pointers.styles.circular.NeedlePointerStyleState;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.layout.Margin;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class CircularGauge extends AxisGaugeBase {
		
		public var pivotPoint:Point;
		public var maxRadius:Number;
		public var maxPixelRadius:Number;
		public var frameRadius:Number;
		public var minAngle:Number;
		public var maxAngle:Number;
		
		public var pixPivotPoint:Point;
		
		public var maxCapRadius:Number;
		
		public function CircularGauge() {
			super();
			this.pivotPoint = new Point(.5, .5);
			this.pixPivotPoint = new Point();
			this.maxRadius = 0;
			this.minAngle = 72000;
			this.maxAngle = -72000;
			this.maxCapRadius = 0;
		}
		
		override protected function createPointerByType(type:*):GaugePointer {
			if (type == undefined) return new NeedleCircularGaugePointer();
			switch (SerializerBase.getEnumItem(type)) {
				case "bar": return new BarCircularGaugePointer();
				case "knob": return new KnobCircularGaugePointer();
				case "marker": return new MarkerCircularGaugePointer();
				case "rangebar": return new RangeBarCircularGaugePointer();
				default: return new NeedleCircularGaugePointer();
			}
		}
		
		override protected function checkPointer(pointer:GaugePointer):void {
			this.checkStateCap(pointer, CircularGaugePointerStyleState(pointer.style.normal));
			this.checkStateCap(pointer, CircularGaugePointerStyleState(pointer.style.hover));
			this.checkStateCap(pointer, CircularGaugePointerStyleState(pointer.style.pushed));
			this.checkStateCap(pointer, CircularGaugePointerStyleState(pointer.style.selectedNormal));
			this.checkStateCap(pointer, CircularGaugePointerStyleState(pointer.style.selectedHover));
		}
		
		private function checkStateCap(pointer:GaugePointer, state:CircularGaugePointerStyleState):void {
			if (state.cap != null)
				this.maxCapRadius = Math.max(this.maxCapRadius, state.cap.getPercentRadius(CircularGaugeAxis(pointer.axis)));
			var r:Number;
			if (state is NeedlePointerStyleState) {
				this.maxCapRadius = Math.max(this.maxCapRadius, NeedlePointerStyleState(state).needle.getPercentThickness(CircularGaugeAxis(pointer.axis))/2);
				r = NeedlePointerStyleState(state).needle.getPercentBaseRadius(CircularGaugeAxis(pointer.axis));
				if (r < 0)
					this.maxCapRadius = Math.max(-r,this.maxCapRadius);
			}else if (state is KnobPointerStyleState) {
				this.maxCapRadius = Math.max(this.maxCapRadius, KnobPointerStyleState(state).needle.getPercentThickness(CircularGaugeAxis(pointer.axis))/2);
				r = KnobPointerStyleState(state).needle.getPercentBaseRadius(CircularGaugeAxis(pointer.axis));
				if (r < 0)
					this.maxCapRadius = Math.max(-r,this.maxCapRadius);
			}
		}
		
		override protected function createFrame(data:XML):GaugeFrame {
			if (data != null && data.@type != undefined) {
				switch (SerializerBase.getEnumItem(data.@type)) {
					case "auto": return new CircularGaugeFrameAuto();
					case "rectangular": return new CircularGaugeFrameRect();
					case "autorectangular": return new CircularGaugeFrameAutoRect(); 
				}
			}
			return new CircularGaugeFrame();
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):void {
			super.deserialize(data, resources);
			if (data.pivot_point[0] != null) {
				var p:XML = data.pivot_point[0];
				if (p.@x != undefined) this.pivotPoint.x = GaugePlotSerializerBase.getSimplePercent(p.@x);
				if (p.@y != undefined) this.pivotPoint.y = GaugePlotSerializerBase.getSimplePercent(p.@y);
			}
		}
		
		override protected function setBounds(newBounds:Rectangle):void {
			super.setBounds(newBounds);
			
			if (this.frame.allowAutoFit) {
				CircularGaugeFrame(this.frame).autoFit(this.bounds);
			}else {
				var w:Number = Math.min(this.bounds.width, this.bounds.height);
				this.maxPixelRadius = w;
				this.frameRadius = this.maxRadius*w;
				var m:Margin = new Margin();
			}
			this.pixPivotPoint.x = this.bounds.x + this.pivotPoint.x * this.bounds.width;
			this.pixPivotPoint.y = this.bounds.y + this.pivotPoint.y * this.bounds.height;
		}
		
		override protected function createAxis(data:XML, localStyles:StylesList, styles:XML, resources:ResourcesLoader):GaugeAxis {
			var axis:CircularGaugeAxis = new CircularGaugeAxis();
			axis.gauge = this;
			axis.deserialize(data, localStyles, styles, resources);
			this.maxRadius = Math.max(axis.radius, this.maxRadius);
			this.minAngle = Math.min(axis.startAngle, this.minAngle);
			this.maxAngle = Math.max(GaugePlotSerializerBase.getAngle(axis.startAngle+axis.sweepAngle), this.maxAngle);
			return axis;
		}
		
		override public function initialize(bounds:Rectangle):DisplayObject {
			var c:DisplayObject = super.initialize(bounds);
			var mask:Shape = this.frame.getMask();
			if (mask != null)
				this.contentContainer.mask = mask;
			return c;
		}
		
		override public function resize(newBounds:Rectangle, setBounds:Boolean = true):void {
			this.setBounds(newBounds);
			super.resize(newBounds, false);
		}
		
	}
}