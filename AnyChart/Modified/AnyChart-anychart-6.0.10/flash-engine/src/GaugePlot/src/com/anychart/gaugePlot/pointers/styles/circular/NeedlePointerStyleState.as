package com.anychart.gaugePlot.pointers.styles.circular {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.styles.Style;

	public class NeedlePointerStyleState extends CircularGaugePointerStyleState {
		
		public var needle:NeedleStyle; 
		
		public function NeedlePointerStyleState(style:Style) {
			super(style);
			this.needle = new NeedleStyle(false);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			this.needle.deserialize(data, resources, this.style);
			return data;
		}
	}
}