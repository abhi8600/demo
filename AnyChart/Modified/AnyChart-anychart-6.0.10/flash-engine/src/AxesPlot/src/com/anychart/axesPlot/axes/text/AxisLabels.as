package com.anychart.axesPlot.axes.text {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.axesPlot.scales.TextScale;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class AxisLabels extends BaseAxisLabel {
		
		public var allowOverlap:Boolean;
		
		protected var axis:Axis;
		
		public function AxisLabels(axis:Axis, viewPort:IAxisViewPort) {
			this.allowOverlap = false;
			this.axis = axis;
			this.space = 0;
			super(viewPort, axis.scale is TextScale);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle=null):void {
			super.deserialize(data, resources);
			if (!this.enabled) return;
			if (data.@allow_overlap != undefined) this.allowOverlap = SerializerBase.getBoolean(data.@allow_overlap);
		}
		
		//----------------------------------------------------------
		//		Formattings
		//----------------------------------------------------------
		
		protected var tmpValue:Number;
		
		protected function formatAxisLabel(token:String):* {
			return this.axis.formatAxisLabelToken(token, this.tmpValue);
		}
		
		protected function isDateTimeToken(token:String):Boolean {
			return this.axis.isDateTimeToken(token);
		}
		
		//----------------------------------------------------------
		//		Max size calculation
		//----------------------------------------------------------
		
		protected var maxLabelSize:Rectangle;
		protected var maxNonRotatedLabelSize:Rectangle;
		
		public function initMaxLabelSize():void {
			this.maxNonRotatedLabelSize = new Rectangle();
			this.maxLabelSize = new Rectangle();
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
				this.maxLabelSize.width = info.rotatedBounds.width;
				this.maxLabelSize.height = info.rotatedBounds.height;
				this.maxNonRotatedLabelSize.width = info.nonRotatedBounds.width;
				this.maxNonRotatedLabelSize.height = info.nonRotatedBounds.height;
			}else {
				var majorIntervalsCount:uint = this.axis.scale.majorIntervalsCount; 
				for (var i:uint = 0;i<=majorIntervalsCount;i++) {
					info = this.createInformation();
					this.tmpValue = this.axis.scale.getMajorIntervalValue(i);
					info.formattedText = this.dynamicText.getValue(this.formatAxisLabel, this.isDateTimeToken);
					this.getBounds(info);
					if (info.rotatedBounds.width > this.maxLabelSize.width) {
						this.maxLabelSize.width = info.rotatedBounds.width;
						this.maxNonRotatedLabelSize.width = info.nonRotatedBounds.width;
					}if (info.rotatedBounds.height > this.maxLabelSize.height) {
						this.maxLabelSize.height = info.rotatedBounds.height;
						this.maxNonRotatedLabelSize.height = info.nonRotatedBounds.height;
					}
				}
			}
		}
		
		//----------------------------------------------------------
		//		Max labels calculation
		//----------------------------------------------------------
		
		public function calcMaxLabelsCount():uint {
			this.initMaxLabelSize();
			var maxLabels:int = viewPort.getMaxRotatedLabelsCount(this.maxNonRotatedLabelSize, 
																  this.maxLabelSize, 
																  this.rotation, 
																  this.absSin, 
																  this.absCos, 
																  this.absTan);
			return (maxLabels < 1) ? 1 : maxLabels;
		}
		
		//----------------------------------------------------------
		//		Labels space
		//----------------------------------------------------------
		
		public var space:Number;
		public var totalSpace:Number;
		
		public function calcSpace():void {
			this.space = this.viewPort.getTextSpace(this.maxLabelSize);
			this.totalSpace = this.space + this.padding;
		}
		
		public function getLabelBounds(majorIntervalIndex:int):Rectangle {
			var info:TextElementInformation = this.createInformation();
			var pos:Point = new Point();
			this.tmpValue = this.axis.scale.getMajorIntervalValue(majorIntervalIndex);
			if (this.isDynamicText) {
				info.formattedText = this.dynamicText.getValue(this.formatAxisLabel, this.isDateTimeToken);
				this.getBounds(info);
			}else {
				info.formattedText = this.text;
				this.getBounds(info);
			}
			this.viewPort.setLabelPosition(this, info, pos, this.axis.scale.localTransform(this.tmpValue));
			this.moveLabel(pos, info);
			
			info.rotatedBounds.x = pos.x;
			info.rotatedBounds.y = pos.y;
			return info.rotatedBounds;
		}
		
		//----------------------------------------------------------
		//		Labels drawing
		//----------------------------------------------------------
		
		public var offset:Number;
		
		public function drawLabels(g:Graphics):void {
			var info:TextElementInformation;
			
			if (!this.isDynamicText) {
				info = this.createInformation();
				info.formattedText = this.text;
				this.getBounds(info);
			}
			
			var pos:Point = new Point();
			
			var checkLabels:Boolean = !this.allowOverlap;
			var isInvertedLabelsCheck:Boolean = checkLabels && this.viewPort.isInvertedLabelsCheck(this.axis.scale.inverted);
			var lastX:Number = isInvertedLabelsCheck ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;
			
			var isT1Overlap:Boolean; 
			if (!this.is90C)
				isT1Overlap = this.viewPort.isFirstTypeLabelsOverlap(this.maxNonRotatedLabelSize, this.absTan);
			
			var startIndex:int = this.showFirst ? 0 : 1;
			var endIndex:int = this.axis.scale.majorIntervalsCount;
			if (!this.showLast)
				endIndex--; 
			
			for (var i:int = startIndex;i<=endIndex;i++) {
				this.tmpValue = this.axis.scale.getMajorIntervalValue(i);
				
				//if (tmpValue < this.axis.scale.minimum || tmpValue > this.axis.scale.maximum) continue;
				if (!this.axis.scale.linearizedContains(this.tmpValue)) return;
				
				if (this.isDynamicText) {
					info = this.createInformation();
					info.formattedText = this.dynamicText.getValue(this.formatAxisLabel, this.isDateTimeToken);
					this.getBounds(info);
				}
				this.viewPort.setLabelPosition(this, info, pos, this.axis.scale.localTransform(this.tmpValue));
				
				if (checkLabels) {
					if (this.viewPort.isLabelsOverlaped(isInvertedLabelsCheck, is90C, pos, info, lastX, isT1Overlap, this.absSin, this.absCos))
						continue;
					
					lastX = this.viewPort.getLabelLastPosition(isInvertedLabelsCheck, is90C, pos, info, isT1Overlap, this.absSin, this.absCos);
				}
				this.moveLabel(pos, info);
				
				this.draw(g, pos.x, pos.y, info, 0, 0);
			}
		}
	}
}