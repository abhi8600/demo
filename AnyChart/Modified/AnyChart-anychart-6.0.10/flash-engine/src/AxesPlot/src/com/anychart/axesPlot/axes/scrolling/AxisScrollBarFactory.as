package com.anychart.axesPlot.axes.scrolling {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.visual.layout.Position;
	
	import flash.display.InteractiveObject;
	
	public final class AxisScrollBarFactory {
		public static function create(axis:Axis, container:InteractiveObject, position:uint):IAxisScrollBar {
			switch (position) {
				case Position.LEFT: 
				case Position.RIGHT:
					var vert:VerticalAxisScrollBar = new VerticalAxisScrollBar(axis, container);
					vert.isPercentStep = true; 
					return vert;
				case Position.TOP:
				case Position.BOTTOM:
					var horiz:HorizontalAxisScrollBar = new HorizontalAxisScrollBar(axis, container);
					horiz.isPercentStep = true; 
					return horiz;
				default:
					return null;
			}
		}
		
		public static function getSettings(node:XML, scroll:IAxisScrollBar):XML {
			return (scroll is HorizontalAxisScrollBar) ? node.horz_scroll_bar[0] : node.vert_scroll_bar[0];
		}
	}
}