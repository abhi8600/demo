package com.anychart.axesPlot.axes.markers {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.formatters.IFormatable;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.styles.StylesList;
	import com.anychart.visual.layout.AbstractAlign;
	
	import flash.display.Shape;
	
	internal class AxisMarker implements IFormatable {
		
		public var markersList:AxisMarkersList;
		
		protected var style:Style;
		private var displayUnderData:Boolean;
		
		protected var container:Shape;
		public var visible:Boolean;
		
		protected var axis:Axis;
		protected function get viewPort():IAxisViewPort { return this.axis.viewPort; }
		protected function get scale():BaseScale { return this.axis.scale; }
		
		protected var affectScaleRange:Boolean;
		
		public function AxisMarker(axis:Axis) {
			this.axis = axis;
			this.displayUnderData = true;
			this.affectScaleRange = true;
		}
		
		public function initialize():void {
			this.container = new Shape();
			if (this.displayUnderData)
				axis.plot.getBeforeDataContainer().addChild(this.container);
			else
				axis.plot.getAfterDataContainer().addChild(this.container);
				
			this.initializeLabel();
		}
		
		public function getLabel():AxisMarkerLabel {
			return AxisMarkerStyleState(this.style.normal).label;
		}
		
		private function initializeLabel():void {
	    	var label:AxisMarkerLabel = this.getLabel();
	    	if (label != null)
	    		label.initialize(this);
	    }
		
		public function deserialize(collectionData:XML, data:XML, styles:XML, resources:ResourcesLoader):void {
			if(data.@visible!=undefined) this.visible=SerializerBase.getBoolean(data.@visible);
			else this.visible=true;
			this.deserializeStyleFromNode(styles, data, resources);
			if (collectionData.@display_under_data != undefined) this.displayUnderData = SerializerBase.getBoolean(collectionData.@display_under_data);
			if (data.@display_under_data != undefined) this.displayUnderData = SerializerBase.getBoolean(data.@display_under_data);
			if (data.@affects_scale_range != undefined) this.affectScaleRange = SerializerBase.getBoolean(data.@affects_scale_range);
		}
		
		protected function getValue(strValue:String):Number {
			if (strValue.charAt(0) == "{") strValue = strValue.substr(1, strValue.length-2);
			var value:Number;
			if (this.axis is BaseCategorizedAxis) {
				value = Number(strValue);
				if (!isNaN(value)) return value-1;
				
				if (BaseCategorizedAxis(this.axis).hasCategory(strValue))
					return BaseCategorizedAxis(this.axis).getCategory(strValue).index;
			}
			
			value = this.scale.deserializeValue(strValue);
			if (!isNaN(value)) return value;
			return this.axis.getTokenValue(strValue);
		}
		
		protected function getStyleNodeName():String { return null; }
		protected function getStyleStateClass():Class { return null; }
		
		private function deserializeStyleFromNode(stylesList:XML,
												  data:XML,
												  resources:ResourcesLoader):void {
			var actualStyleXML:XML = StylesList.getStyleXMLByMerge(stylesList, this.getStyleNodeName(), data, data.@style);
			this.style = new Style(this.getStyleStateClass());
			this.style.deserialize(actualStyleXML,resources);
	    }
	    
	    public function draw():void {
	    	this.execDrawing();
	    }
	    
	    protected function execDrawing():void {
	    	this.container.graphics.clear();
	    }
	    
	    protected final function checkScale(value:Number):void {
	    	if (isNaN(this.scale.dataRangeMinimum) || value < this.scale.dataRangeMinimum)
	    		this.scale.dataRangeMinimum = value;
	    	if (isNaN(this.scale.dataRangeMaximum) || value > this.scale.dataRangeMaximum)
	    		this.scale.dataRangeMaximum = value;
	    }
	    
	    public function getTokenValue(token:String):* {
	    	if (token.indexOf("%DataPlot") == 0) return this.axis.plot.getTokenValue(token);
			if (token.indexOf("%Axis") == 0) return this.axis.getTokenValue(token);
			return "";
	    }
	    
	    public function isDateTimeToken(token:String):Boolean { return false; }
	    
	    //----------------------------------------------------------------------
	    //					LABEL DRAWING
	    //----------------------------------------------------------------------
	    
	    protected function drawLabel(label:AxisMarkerLabel, startValue:Number, endValue:Number):void {
	    	if (label.isOutside) {
	    		this.setOutsideLabelPosition(label, startValue);
	    		label.draw(this.markersList.outsideLabelsContainerGraphics);
	    	}else {
	    		this.setInsideLabelPosition(label, startValue, endValue);
	    		label.draw(this.container.graphics);
	    	}
	    }
	    
	    private function setOutsideLabelPosition(label:AxisMarkerLabel, value:Number):void {
	    	var pixPos:Number = this.scale.transform(value);
			var offset:Number = 0;
			switch (label.position) {
				case AxisMarkerLabelPosition.AFTER_AXIS_LABELS:
					offset = this.axis.getMarkersBeforeLabelsSpace();
					break;
				case AxisMarkerLabelPosition.AXIS:
					offset = this.axis.getMarkersAxisSpace();
					break;
				case AxisMarkerLabelPosition.BEFORE_AXIS_LABELS:
					offset = this.axis.getMarkersAfterLabelsSpace();
					break;
			}
			this.viewPort.setMarkerLabelPosition(pixPos, label.padding, offset, label.bounds, label.pos);
	    }
	    
	    private function setInsideLabelPosition(label:AxisMarkerLabel, startValue:Number, endValue:Number):void {
	    	
	    	var startPixPos:Number = this.scale.transform(startValue);
	    	var endPixPos:Number = this.scale.transform(endValue);
	    	
	    	var align:uint;
	    	switch (label.position) {
	    		case AxisMarkerLabelPosition.NEAR:
	    			align = AbstractAlign.NEAR;
	    			break;
	    		case AxisMarkerLabelPosition.CENTER:
	    			align = AbstractAlign.CENTER;
	    			break;
	    		case AxisMarkerLabelPosition.FAR:
	    			align = AbstractAlign.FAR;
	    			break;
	    	}
	    	
	    	this.viewPort.setInsideMarkerLabelPosition(startPixPos, endPixPos, align, label.padding, label.bounds, label.pos);
	    }

	}
}