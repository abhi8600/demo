package com.anychart.axesPlot.scales {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.serialization.SerializerBase;
	
	public final class TextDateTimeScale extends TextScale {
		
		public function TextDateTimeScale(axis:Axis) {
			super(axis);
		}
		
		override public function deserializeValue(value:*):* {
			if (String(value) == "") return NaN;
			return SerializerBase.getDateTime(value, this.axis.plot.dateTimeLocale);
		}
		
		override public function getCategoryName(category:Category):* {
			return SerializerBase.getDateTime(category.name, this.axis.plot.dateTimeLocale);
		}
		
		override protected function calculateMajorTicksCount():void {
			super.calculateMajorTicksCount();
			
			var r:Number = this.maximum - this.baseValue;
			if (r % this.majorInterval != 0) {
				var lastVal:Number = this.getMajorIntervalValue(this._majorIntervalsCount);
				if (lastVal >= this.minimum && lastVal <= this.maximum)
					this._majorIntervalsCount++;
			}
		}
		
	}
}