package com.anychart.axesPlot.axes.categorization {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	import com.anychart.controls.legend.ILegendTag;
	import com.anychart.formatters.IFormatable;
	import com.anychart.serialization.objectSerialization.IObjectSerializable;
	import com.anychart.seriesPlot.SeriesPlot;
	import com.anychart.seriesPlot.categories.BaseCategoryInfo;
	
	
	public class CategoryInfo extends BaseCategoryInfo implements IFormatable,ILegendTag,IObjectSerializable {
		public var axis:Axis;
		
		public function CategoryInfo(plot:SeriesPlot) {
			super(plot);
		}
		
		override public function destroy():void {
			this.axis = null;
			super.destroy();
		}
		
		//-------------------------------------------
		//				Formatting
		//-------------------------------------------
		
		override public function isDateTimeToken(token:String):Boolean {
			if (!this.axis.isDateTimeAxis()) return false;
			return token == "%Name" ||
				   token == "%CategoryName";
		}
	}
}