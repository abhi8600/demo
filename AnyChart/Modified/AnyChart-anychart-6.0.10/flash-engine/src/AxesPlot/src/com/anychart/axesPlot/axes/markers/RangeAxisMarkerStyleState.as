package com.anychart.axesPlot.axes.markers {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.Style;
	import com.anychart.visual.fill.Fill;
	import com.anychart.visual.hatchFill.HatchFill;
	import com.anychart.visual.stroke.Stroke;
	
	internal final class RangeAxisMarkerStyleState extends AxisMarkerStyleState {
		
		public var minimumLine:Stroke;
		public var maximumLine:Stroke;
		public var fill:Fill;
		public var hatchFill:HatchFill;
		
		public function RangeAxisMarkerStyleState(style:Style) {
			super(style);
		}
		
		override public function deserialize(data:XML, resources:ResourcesLoader):XML {
			data = super.deserialize(data, resources);
			
			if (SerializerBase.isEnabled(data.minimum_line[0])) {
				this.minimumLine = new Stroke();
				this.minimumLine.deserialize(data.minimum_line[0],this.style);
			}
			if (SerializerBase.isEnabled(data.maximum_line[0])) {
				this.maximumLine = new Stroke();
				this.maximumLine.deserialize(data.maximum_line[0],this.style);
			}
			if (SerializerBase.isEnabled(data.fill[0])) {
				this.fill = new Fill();
				this.fill.deserialize(data.fill[0], resources, this.style);
			}
			if (SerializerBase.isEnabled(data.hatch_fill[0])) {
				this.hatchFill = new HatchFill();
				this.hatchFill.deserialize(data.hatch_fill[0], this.style);
			}
			return data;
		}
	}
}