package com.anychart.axesPlot.axes.scrolling {
	import com.anychart.uiControls.scrollBar.IScrollBar;
	
	public interface IAxisScrollBar extends IScrollBar {
		function get position():uint;
		function set position(value:uint):void;
	}
}