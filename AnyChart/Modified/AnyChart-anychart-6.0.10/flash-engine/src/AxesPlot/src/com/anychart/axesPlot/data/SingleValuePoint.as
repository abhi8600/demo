package com.anychart.axesPlot.data {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.CategorizedAxis;
	import com.anychart.axesPlot.axes.categorization.Category;
	import com.anychart.serialization.SerializerBase;
	
	public class SingleValuePoint extends AxesPlotPoint {
		public var y:Number;
		public var yCategory:Category;
		
		override protected function checkMissing(data:XML):void {
			if (data.@y == undefined) {
				this.isMissing = true;
				return;
			}
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				this.isMissing = isNaN(this.deserializeValueFromXML(data.@y));
			}
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				y: this.y
			}
		}
		
		override public function checkScaleValue():void {
			
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis && !this.isMissing) {
				valueAxis.checkScaleValue(this.y);
			}
			
			super.checkScaleValue();
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.y != undefined) this.y = this.deserializeValueFromXML(newValue.y);
			super.updateValue(newValue);
		}
		
		override public function canAnimate(field:String):Boolean {
			return field == "y";
		}
		
		override protected function deserializeValue(data:XML):void {
			var valueAxis:Axis = AxesPlotSeries(this.series).valueAxis;
			if (valueAxis is ValueAxis) {
				this.y = this.deserializeValueFromXML(data.@y);
				this.isMissing = isNaN(this.y);
				if (!this.isMissing) {
					valueAxis.checkScaleValue(this.y);
				}
			}else {
				var categoryName:String = SerializerBase.getString(data.@y);
				this.yCategory = CategorizedAxis(valueAxis).getCategory(categoryName);
				this.y = this.yCategory.index;
				if (!this.isMissing)
					this.yCategory.checkPoint(this);
			}
			
			super.deserializeValue(data);
		}
		
		override public function initializeAfterInterpolate():void {
			AxesPlotSeries(this.series).valueAxis.checkScaleValue(this.y);
			super.initializeAfterInterpolate();
		}
		
		override public function get sortedOverlayValue():Number { return this.y; }
		
		//-------------------------------------------------
		//				Formatting
		//-------------------------------------------------
		
		override public function checkCategory(categoryTokens:Object):void {
			if (categoryTokens['YSum'] == null)
				categoryTokens['YSum'] = 0;
			if (categoryTokens['PointCount'] == null)
				categoryTokens['PointCount'] = 0;
				
			categoryTokens['YSum'] += this.y;
			if (categoryTokens['YMax'] == null || this.y > categoryTokens['YMax'])
				categoryTokens['YMax'] = this.y;
			if (categoryTokens['YMin'] == null || this.y < categoryTokens['YMin'])
				categoryTokens['YMin'] = this.y;
			categoryTokens['PointCount']++;
		}
		
		override public function getPointTokenValue(token:String):* {
			if (token == '%Value') return this.y;
			if (token == '%YValue') return this.y;
			if (token == '%YPercentOfSeries') {
				if (this.cashedTokens['YPercentOfSeries'] == null) {
					this.cashedTokens['YPercentOfSeries'] =  (this.series.getSeriesTokenValue("%SeriesYSum")!=0) ? this.y/this.series.getSeriesTokenValue("%SeriesYSum")*100 : 0;
				}
				return this.cashedTokens['YPercentOfSeries'];
			}
			if (token == '%YPercentOfTotal') {
				if (this.cashedTokens['YPercentOfTotal'] == null) {
					this.cashedTokens['YPercentOfTotal'] = (this.plot.getTokenValue('%DataPlotYSum')!=0) ? this.y/this.plot.getTokenValue('%DataPlotYSum')*100 : 0;
				}
				return this.cashedTokens['YPercentOfTotal'];
			}
			if (token == '%YPercentOfCategory') {
				if (this.dataCategory == null) return '';
				if (this.cashedTokens['YPercentOfCategory'] == null) {
					this.cashedTokens['YPercentOfCategory'] = (this.dataCategory.getCategoryTokenValue("%CategoryYSum")!=0) ? this.y/this.dataCategory.getCategoryTokenValue('%CategoryYSum')*100 : 0;
				}
				return this.cashedTokens['YPercentOfCategory'];
			}
			return super.getPointTokenValue(token);
		}
		
		override public function isDateTimePointToken(token:String):Boolean {
			if (AxesPlotSeries(this.series).valueAxis.isDateTimeAxis()) {
				if (token == "%Value") return true;
				if (token == "%YValue") return true; 
			}
			return super.isDateTimePointToken(token);
		}
		
		//---------------------------------------------------------------
		//				IObjectSerializable
		//---------------------------------------------------------------
		
		override public function serialize(res:Object = null):Object {
			res = super.serialize(res);

			res.YValue = this.y;						
			res.YPercentOfSeries = this.getPointTokenValue("%YPercentOfSeries");
			res.YPercentOfTotal = this.getPointTokenValue("%YPercentOfTotal");
			res.YPercentOfCategory = this.getPointTokenValue("%YPercentOfCategory");
			
			return res;
		}
	}
}