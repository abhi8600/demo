package com.anychart.axesPlot.data.data3D {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.data.AxesPlotPoint;
	
	import flash.geom.Point;
	
	public class ZPointSettings {
		protected var z:Number;
		
		protected var zAxis:Axis;
		protected var point:AxesPlotPoint;
		
		public final function deserialize(point:AxesPlotPoint, data:XML):void {
			this.point = point;
			this.zAxis = AxesPlot3D(point.plot).zAxis;
			this.setZValue(data);
		}
		
		public final function setZ(value:Number):void { this.z = value; }
		public final function getZ():Number { return this.z; }
		
		protected function setZValue(data:XML):void {
			this.z = (data.@z != undefined) ? Number(data.@z) : 0;
			this.zAxis.checkScaleValue(this.z);
		}
		
		public final function transform(pixPoint:Point, pixOffset:Number = 0, valueOffset:Number = 0):void {
			var plot:AxesPlot3D = AxesPlot3D(point.plot);
			if (valueOffset == 0)
				valueOffset += plot.space3D.padding/2;
			else
				valueOffset -= plot.space3D.padding/2;
			this.zAxis.transform(this.point, this.z + valueOffset, pixPoint, pixOffset);
		}
	}
}