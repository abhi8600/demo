package com.anychart.axesPlot.axes.markers {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.viewPorts.IAxisViewPort;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.scales.BaseScale;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Graphics;
	
	public final class AxisMarkersList {
		
		public var axis:Axis;
		public var outsideLabelsContainerGraphics:Graphics;
		
		private var markers:Array;
		
		private var _beforeAxisLabelsSpace:Number;
		private var _axisLabelsSpace:Number;
		private var _afterAxisLabelsSpace:Number;
		
		public function get beforeAxisLabelsSpace():Number { return this._beforeAxisLabelsSpace; }
		public function get axisLabelsSpace():Number { return this._axisLabelsSpace; }
		public function get afterAxisLabelsSpace():Number { return this._afterAxisLabelsSpace; }
		
		public function AxisMarkersList() {
			this._beforeAxisLabelsSpace = 0;
			this._axisLabelsSpace = 0;
			this._afterAxisLabelsSpace = 0;
		}
		
		public function deserialize(data:XML, styles:XML, resources:ResourcesLoader):void {
			this.markers = [];
			var i:int;
			var cnt:int;
			var marker:AxisMarker;
			
			if (data.lines[0] != null) {
				cnt = data.lines[0].line.length();
				for (i = 0;i<cnt;i++) {
					marker = new LineAxisMarker(this.axis);
					marker.deserialize(data.lines[0],data.lines[0].line[i], styles, resources);
					this.addMarker(marker);
				}
			}
			if (data.ranges[0] != null) {
				cnt = data.ranges[0].range.length();
				for (i = 0;i<cnt;i++) {
					marker = new RangeAxisMarker(this.axis);
					marker.deserialize(data.ranges[0], data.ranges[0].range[i], styles, resources);
					this.addMarker(marker);
				}
			}
			if (data.labels[0] != null) {
				cnt = data.labels[0].label.length();
				for (i = 0;i<cnt;i++) {
					marker = new LabelAxisMarker(this.axis);
					marker.deserialize(data.labels[0], data.labels[0].label[0], styles, resources);
					this.addMarker(marker);
				}
			}
		}
		
		private function addMarker(marker:AxisMarker):void {
			marker.markersList = this;
			this.markers.push(marker);
		}
		
		public function initialize():void {
			for (var i:uint = 0;i<this.markers.length;i++) {
				var marker:AxisMarker = this.markers[i];
				marker.initialize();
				
				var label:AxisMarkerLabel = marker.getLabel();
				if (label != null && label.isOutside) {
					var labelSpace:Number;
					
					switch (label.position) {
						case AxisMarkerLabelPosition.AFTER_AXIS_LABELS:
							labelSpace = this.axis.viewPort.getTextSpace(label.bounds) + label.padding;
							this._beforeAxisLabelsSpace = Math.max(labelSpace, this._beforeAxisLabelsSpace);
							break;
							
						case AxisMarkerLabelPosition.BEFORE_AXIS_LABELS:
							labelSpace = this.axis.viewPort.getTextSpace(label.bounds) + label.padding;
							this._afterAxisLabelsSpace = Math.max(labelSpace, this._afterAxisLabelsSpace);
							break;
							
						case AxisMarkerLabelPosition.AXIS:
							labelSpace = this.axis.viewPort.getTextSpace(label.bounds) + label.padding;
							this._axisLabelsSpace = Math.max(labelSpace, this._axisLabelsSpace);
							break;
					}
				}
			}
		}
		
		public function draw():void {
			for (var i:uint = 0;i<this.markers.length;i++)
				this.markers[i].draw();
		}

	}
}