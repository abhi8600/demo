package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.visual.layout.Position;
	
	public final class ViewPortFactory3D {
		public static function create(axis:Axis):IAxisViewPort {
			switch (axis.position) {
				case Position.LEFT: return axis.isOpposite ? new LeftOppositeAxisViewPort3D(axis) : new LeftAxisViewPort3D(axis);
				case Position.RIGHT: return axis.isOpposite ? new RightOppositeAxisViewPort3D(axis) : new RightAxisViewPort3D(axis);
				case Position.TOP: return axis.isOpposite ? new TopOppositeAxisViewPort3D(axis) : new TopAxisViewPort3D(axis);
				case Position.BOTTOM: return axis.isOpposite ? new BottomOppositeAxisViewPort3D(axis) : new BottomAxisViewPort3D(axis);
				default: return new ZAxisViewPort(axis);
			}
		}
	}
}