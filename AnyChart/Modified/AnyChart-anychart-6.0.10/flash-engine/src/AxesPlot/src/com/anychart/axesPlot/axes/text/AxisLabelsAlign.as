package com.anychart.axesPlot.axes.text {
	
	public final class AxisLabelsAlign {
		public static const INSIDE:uint = 0;
		public static const OUTSIDE:uint = 1;
		public static const CENTER:uint = 2;
	}
}