package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.scrolling.ScrollBarPosition;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisLabelsAlign;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.utils.MathUtils;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.layout.AbstractAlign;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class RightAxisViewPort extends VerticalAxisViewPort implements IAxisViewPort {
		public function RightAxisViewPort(axis:Axis) {
			super(axis);
		}
		
		public function setGradientRotation(g:Gradient):void {
			g.angle = MathUtils.reflectAngleBySin(g.angle + 90);
		}
		
		override public function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void {
			super.setParallelLineBounds(lineThickness, start, end, offset, bounds);
			bounds.x += this.visibleBounds.right + offset - lineThickness/2;
		}
		
		public function drawParallelLine(g:Graphics, start:Number, end:Number, offset:Number):void {
			var x:Number = this.visibleBounds.right + offset;
			g.moveTo(x + this.xOffset, start + this.yOffset);
			g.lineTo(x + this.xOffset, end + this.yOffset);
		}
		
		public function setInsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void {
			axisValue = this.bounds.bottom - axisValue;
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.x = this.visibleBounds.right + offset - size + this.xOffset;
			bounds.y += this.yOffset;
		}
		
		public function setOutsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void {
			axisValue = this.bounds.bottom - axisValue;
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.x = this.visibleBounds.right + offset + this.xOffset;
			bounds.y += this.yOffset;
		}
		
		public function setOppositeAxisTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void {
			axisValue = this.bounds.bottom - axisValue;
			this.setTickmarkBounds(lineThickness, axisValue, size, bounds);
			bounds.x = this.visibleBounds.left;
		}
		
		public function drawInsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void {
			axisValue = this.bounds.bottom - axisValue;
			g.moveTo(this.visibleBounds.right + offset - size + this.xOffset, axisValue + this.yOffset);
			g.lineTo(this.visibleBounds.right + offset + this.xOffset, axisValue+this.yOffset);
		}
		
		public function drawOutsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void {
			axisValue = this.bounds.bottom - axisValue;
			g.moveTo(this.visibleBounds.right + offset + this.xOffset, axisValue + this.yOffset);
			g.lineTo(this.visibleBounds.right + offset + size + this.xOffset, axisValue + this.yOffset);
		}
		
		public function drawOppositeAxisTickmark(g:Graphics, axisValue:Number, size:Number):void {
			axisValue = this.bounds.bottom - axisValue;
			g.moveTo(this.visibleBounds.left, axisValue);
			g.lineTo(this.visibleBounds.left + size, axisValue);
		}
		
		override public function setTitlePosition(title:AxisTitle, pos:Point):void {
			super.setTitlePosition(title, pos);
			pos.x += this.visibleBounds.right + title.offset + title.padding;
		}
		
		public function applyOffset(bounds:Rectangle, offset:Number):void {
			bounds.width -= offset;
		}
		
		public function getLabelYOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			if (rotation % 180 == 0) return 0;
			if (rotation % 90 == 0) return -.5;
			if (qIndex == 1 || qIndex == 3)
				return -absSin;
			return 0;
		}
		
		public function getLabelYOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number {
			if (rotation % 180 == 0) return -.5;
			if (rotation % 90 == 0) return 0;
			if (qIndex == 0 || qIndex == 2)
				return -absCos;
			return 0;
		}
		
		override public function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void {
			super.setLabelPosition(labels, info, pos, pixPos);
			pos.x += this.visibleBounds.right + labels.offset;
			switch (labels.align) {
				case AxisLabelsAlign.CENTER:
					pos.x += (labels.space - info.rotatedBounds.width)/2;
					break;
				case AxisLabelsAlign.OUTSIDE:
					pos.x += labels.space - info.rotatedBounds.width;
					break;
			}
		}
		
		override public function setMarkerLabelPosition(pixPos:Number, 
													   padding:Number, 
													   offset:Number,
													   bounds:Rectangle,
													   pos:Point):void {
			super.setMarkerLabelPosition(pixPos, padding, offset, bounds, pos);
			pos.x += this.visibleBounds.right + offset + padding;
		}
		
		public function setInsideMarkerLabelPosition(startPixPos:Number, 
	    										     endPixPos:Number, 
	    										     align:uint, 
	    										     padding:Number, 
	    										     bounds:Rectangle, 
	    										     pos:Point):void {
			var near:Point = new Point(this.bounds.width, this.bounds.bottom - startPixPos);
			var far:Point = new Point(0, this.bounds.bottom - endPixPos);
			
			var angle:Number = Math.atan2(far.y - near.y, far.x - near.x);
			var xOffset:Number = padding*Math.cos(angle);
			var yOffset:Number = padding*Math.sin(angle);
	    	switch (align) {
	    		case AbstractAlign.NEAR:
	    			pos.x = near.x + xOffset - bounds.width;
	    			pos.y = near.y + yOffset - bounds.height/2;
	    			break;
	    		case AbstractAlign.CENTER:
	    			pos.y = (near.y + far.y)/2 - bounds.height/2;
	    			pos.x = (near.x + far.x)/2 - bounds.width/2;
	    			break;
	    		case AbstractAlign.FAR:
	    			pos.x = far.x - xOffset;
	    			pos.y = far.y - bounds.height/2 - yOffset;
	    			break;
	    	}
	    	this.offsetInsideMarkerLabel(pos);
	    }
		
		override public function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void {
			super.setStagerLabelPosition(labels, info, pos, pixPos, offset, space);
			pos.x += this.visibleBounds.right + labels.offset + offset;
			switch (labels.align) {
				case AxisLabelsAlign.CENTER:
					pos.x += (space - info.rotatedBounds.width)/2;
					break;
				case AxisLabelsAlign.OUTSIDE:
					pos.x += space - info.rotatedBounds.width;
					break;
			}
		}
		
		override public function setStartValue(value:Number, pt:Point):void {
			super.setEndValue(value, pt);
		}
		
		override public function setEndValue(value:Number, pt:Point):void {
			super.setStartValue(value, pt);
		}
		
		public function setOffset(offset:Number, bounds:Rectangle, pos:Point):void {
			pos.x = this.visibleBounds.right + offset + this.xOffset;
			pos.y -= bounds.height/2 - this.yOffset;
		}
		
		override public function getScrollBarPixPosition(scrollBarPosition:uint, offset:Number, size:Number):Point {
			var pos:Point = super.getScrollBarPixPosition(scrollBarPosition, offset, size);
			pos.x = this.visibleBounds.right + offset;
			if (scrollBarPosition == ScrollBarPosition.INSIDE)
				pos.x -= size; 
			return pos;
		}
	}
}