package com.anychart.axesPlot.axes.markers {
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Graphics;
	
	internal final class LineAxisMarker extends AxisMarker {
		
		private var value:Number;
		private var startValue:Number;
		private var endValue:Number;
		
		private var strStartValue:String;
		private var strEndValue:String;
		private var strValue:String;
		
		public function LineAxisMarker(axis:Axis) {
			super(axis);
		}
		
		override protected function getStyleNodeName():String {
			return "line_axis_marker_style";
		}
		
		override protected function getStyleStateClass():Class {
			return LineAxisMarkerStyleState;
		}
		
		override public function initialize():void {
			if (this.strStartValue != null) 
				this.startValue = this.getValue(this.strStartValue);
			
			if (this.strEndValue != null) 
				this.endValue = this.getValue(this.strEndValue);
			
			if (this.strValue != null) 
				this.value = this.getValue(this.strValue);
			
			if (this.affectScaleRange && !(this.axis is BaseCategorizedAxis)) { 
				if (!isNaN(this.startValue)) this.checkScale(this.startValue);
				if (!isNaN(this.endValue)) this.checkScale(this.endValue);
			}
			
			super.initialize();
		}
		
		override public function deserialize(collectionData:XML, data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserialize(collectionData, data, styles, resources);
			if (data.@value != undefined)
				this.strStartValue = this.strEndValue = this.strValue = SerializerBase.getString(data.@value);

			if (data.@start_value != undefined)
				this.strStartValue = SerializerBase.getString(data.@start_value);

			if (data.@end_value != undefined)
				this.strEndValue = SerializerBase.getString(data.@end_value);
		}
		
		override protected function execDrawing():void {
		  if (this.visible) 
		  {	
			super.execDrawing();
			var s:LineAxisMarkerStyleState = LineAxisMarkerStyleState(this.style.normal);
			var g:Graphics = this.container.graphics;
			
			s.stroke.apply(g);
			g.beginFill(0,0);
			
			this.viewPort.drawMarkerLine(g, this.axis.scale.transform(this.startValue), 
											this.axis.scale.transform(this.endValue), 
											s.stroke.dashed, s.stroke.dashLength, s.stroke.spaceLength);
			
			g.lineStyle();
			g.endFill();
			
			if (s.label)
				this.drawLabel(s.label, this.startValue, this.endValue);
		  }		
		}
		
		override public function getTokenValue(token:String):* {
			if (token == "%Value") return (this.startValue + this.endValue)/2;
			if (token == "%StartValue" || token == "%Start") return this.startValue;
			if (token == "%EndValue" || token == "%End") return this.endValue;
	    	return super.getTokenValue(token);
	    }
	}
}