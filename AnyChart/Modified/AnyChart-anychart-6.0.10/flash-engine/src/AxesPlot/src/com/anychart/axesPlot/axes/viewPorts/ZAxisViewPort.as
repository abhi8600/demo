package com.anychart.axesPlot.axes.viewPorts {
	import com.anychart.axesPlot.AxesPlot3D;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.text.AxisLabels;
	import com.anychart.axesPlot.axes.text.AxisTitle;
	import com.anychart.visual.gradient.Gradient;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class ZAxisViewPort extends AxisViewPort implements IAxisViewPort {
		
		public function ZAxisViewPort(axis:Axis) {
			super(axis);
		}
		
		override public final function setScaleBounds(totalBounds:Rectangle, visibleBounds:Rectangle):void {
			super.setScaleBounds(totalBounds, visibleBounds);
			this.axis.scale.setPixelRange(0, AxesPlot3D(this.axis.plot).space3D.pixAspect);
		}
		
		public function setScaleValue(pixValue:Number, point:Point):void {
			point.x += AxesPlot3D(this.axis.plot).space3D.getPointXOffset(pixValue);
			point.y += AxesPlot3D(this.axis.plot).space3D.getPointYOffset(pixValue);
		}
		
		public final function getNormalAngle(angle:Number, isBaseInverted:Boolean):Number { return 0;	}
		public final function getInvertedAngle(angle:Number, isBaseInverted:Boolean):Number {	return 0;}
		public final function getNormalAnchor(anchor:uint):uint{return 0;}
		public final function getNormalHAlign(hAlign:uint, vAlign:uint):uint{return 0;}
		public final function getNormalVAlign(hAlign:uint, vAlign:uint):uint{return 0;}
		public final function getInvertedAnchor(anchor:uint):uint{return 0;}
		public final function getInvertedHAlign(hAlign:uint, vAlign:uint):uint{return 0;}
		public final function getInvertedVAlign(hAlign:uint, vAlign:uint):uint{return 0;}
		public final function setGradientRotation(gradient:Gradient):void	{}
		public final function setOffset(offset:Number, bounds:Rectangle, point:Point):void{}
		public final function drawPerpendicularLine(g:Graphics, axisValue:Number, dashed:Boolean, dashOn:Number, dashOff:Number):void {}
		public final function setPerpendicularLineBounds(lineThickness:Number, axisValue:Number, bounds:Rectangle):void{	}
		public final function setPerpendicularRectangle(axisStartValue:Number, axisEndValue:Number, bounds:Rectangle):void{}
		public final function drawPerpendicularRectangle(g:Graphics, bounds:Rectangle):void{}
		public final function drawParallelLine(g:Graphics, start:Number, end:Number, offset:Number):void{	}
		public final function setParallelLineBounds(lineThickness:Number, start:Number, end:Number, offset:Number, bounds:Rectangle):void	{}
		public final function setStartValue(value:Number, pt:Point):void{}
		public final function setEndValue(value:Number, pt:Point):void{}
		public final function setInsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void{}
		public final function setOutsideAxisTickmarkBounds(lineThickness:Number, axisValue:Number, offset:Number, size:Number, bounds:Rectangle):void	{}
		public final function setOppositeAxisTickmarkBounds(lineThickness:Number, axisValue:Number, size:Number, bounds:Rectangle):void{}
		public final function drawInsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void{}
		public final function drawOutsideAxisTickmark(g:Graphics, axisValue:Number, offset:Number, size:Number):void{}
		public final function drawOppositeAxisTickmark(g:Graphics, axisValue:Number, size:Number):void{}
		public final function getTextSpace(bounds:Rectangle):Number{return 0;}
		public final function setTitlePosition(title:AxisTitle, pos:Point):void{}
		public final function setTitleRotation(title:AxisTitle):void{}
		public final function applyOffset(bounds:Rectangle, offset:Number):void{}
		public final function getMaxLabelsCount(maxLabelSize:Rectangle):int{return 0;}
		public final function getMaxRotatedLabelsCount(maxNonRotatedLabelSize:Rectangle, maxLabelSize:Rectangle, rotation:Number, absSin:Number, absCos:Number, absTan:Number):int{return 0;}
		public final function isInvertedLabelsCheck(isScaleInverted:Boolean):Boolean{return false;}
		public final function isFirstTypeLabelsOverlap(size:Rectangle, absTan:Number):Boolean	{return false;}
		public final function getLabelXOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number	{return 0;}
		public final function getLabelXOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number{return 0;}
		public final function getLabelYOffsetWidthMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number{return 0;}
		public final function getLabelYOffsetHeightMult(qIndex:uint, rotation:Number, absSin:Number, absCos:Number):Number{return 0;}
		public final function setLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number):void{}
		public final function setMarkerLabelPosition(pixPos:Number, padding:Number, offset:Number, bounds:Rectangle, pos:Point):void {}
		public final function setInsideMarkerLabelPosition(startPixPos:Number, endPixPos:Number, align:uint, padding:Number, bounds:Rectangle, pos:Point):void {}
		public final function isLabelsOverlaped(isInvertedCheck:Boolean, is90C:Boolean, pos:Point, info:TextElementInformation, lastPosition:Number, isT1Overlap:Boolean, absSin:Number, absCos:Number):Boolean{return false;}
		public final function setStagerLabelPosition(labels:AxisLabels, info:TextElementInformation, pos:Point, pixPos:Number, offset:Number, space:Number):void{}
		public final function isStagerLabelsOverlaped(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation, lastPosition:Number):Boolean{return false;}
		public final function getStagerLabelLastPosition(isInvertedCheck:Boolean, pos:Point, info:TextElementInformation):Number{return 0;}
		public final function getLabelLastPosition(isInvertedCheck:Boolean, is90C:Boolean, pos:Point, info:TextElementInformation, isT1Overlap:Boolean, absSin:Number, absCos:Number):Number{return 0;}
		public final function applyLabelsOffsets(bounds:Rectangle, firstLabel:Rectangle, lastLabel:Rectangle):void{}
		public final function getSpace(bounds:Rectangle):Number{return 0;}
		public final function applyZoomFactor(bounds:Rectangle, factor:Number):void {}
		
		public final function getVisibleStart():Number { return NaN; }
		public final function getVisibleEnd():Number { return NaN; }
		public final function getScaleValue(pixPt:Point):Number { return NaN; }
		
		public function getMouseCoord(e:MouseEvent):Number { return NaN; }
		public function getScrollRectStart(rect:Rectangle):Number { return NaN; }
	}
}