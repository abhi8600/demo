package com.anychart.axesPlot.axes.markers {
	import com.anychart.formatters.FormatsParser;
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.serialization.IStyleSerializableRes;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.styles.IStyle;
	import com.anychart.visual.text.BaseTextElement;
	import com.anychart.visual.text.TextElementInformation;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal final class AxisMarkerLabel implements IStyleSerializableRes {
		
		public var position:uint = AxisMarkerLabelPosition.BEFORE_AXIS_LABELS;
		public var padding:Number = 5;
		public var pos:Point;
		public var isOutside:Boolean;
		
		private var text:BaseTextElement;
		private var info:TextElementInformation;
		
		public function AxisMarkerLabel() {
			this.text = new BaseTextElement();
			this.info = this.text.createInformation();
			this.pos = new Point();
			this.isOutside = true;
		}
		
		public function deserialize(data:XML, resources:ResourcesLoader, style:IStyle = null):void {
			this.text.deserialize(data, resources, style);
			if (data.@position != undefined) {
				switch (SerializerBase.getEnumItem(data.@position)) {
					case "beforeaxislabels":
						this.position = AxisMarkerLabelPosition.BEFORE_AXIS_LABELS;
						this.isOutside = true;
						break;
					case "afteraxislabels":
						this.position = AxisMarkerLabelPosition.AFTER_AXIS_LABELS;
						this.isOutside = true;
						break;
					case "axis":
						this.position = AxisMarkerLabelPosition.AXIS;
						this.isOutside = true;
						break;
					case "near":
						this.position = AxisMarkerLabelPosition.NEAR;
						this.isOutside = false;
						break;
					case "center":
						this.position = AxisMarkerLabelPosition.CENTER;
						this.isOutside = false;
						break;
					case "far":
						this.position = AxisMarkerLabelPosition.FAR;
						this.isOutside = false;
						break; 
				}
			}
			
			if (data.@padding != undefined) this.padding = SerializerBase.getNumber(data.@padding);
			if (data.format[0] != null) {
				this.text.text = SerializerBase.getCDATAString(data.format[0]);
				this.text.isDynamicText = FormatsParser.isDynamic(this.text.text);
				if (this.text.isDynamicText)
					this.text.dynamicText = FormatsParser.parse(this.text.text);
			}
		}
		
		public function initialize(marker:AxisMarker):void {
			this.info.formattedText = this.text.isDynamicText ? this.text.dynamicText.getValue(marker.getTokenValue, marker.isDateTimeToken) : this.text.text;
			this.text.getBounds(this.info);
		}
		
		public function draw(g:Graphics):void {
			this.text.draw(g, this.pos.x ,this.pos.y, this.info, 0, 0);
		}
		
		public function get bounds():Rectangle {
			return this.info.rotatedBounds;
		}
	}
}