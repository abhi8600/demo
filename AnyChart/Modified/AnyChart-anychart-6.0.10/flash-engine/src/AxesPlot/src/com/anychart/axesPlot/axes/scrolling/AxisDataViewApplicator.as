package com.anychart.axesPlot.axes.scrolling {
	import com.anychart.axesPlot.AxesPlot;
	import com.anychart.axesPlot.axes.Axis;
	import com.anychart.axesPlot.axes.ValueAxis;
	import com.anychart.axesPlot.axes.categorization.BaseCategorizedAxis;
	import com.anychart.axesPlot.scales.TextDateTimeScale;
	import com.anychart.axesPlot.scales.dateTime.DateTimeScale;
	import com.anychart.axesPlot.scales.dateTime.DateUtils;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.uiControls.scrollBar.ScrollBarEvent;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public final class AxisDataViewApplicator {
		
		private var axis:Axis;
		private var scroll:IAxisScrollBar;
		public var valueMult:Number;
		
		public function AxisDataViewApplicator(axis:Axis) {
			this.axis = axis;
			this.valueMult = 1;
			this._showScrollBar = true;
			this._scrollBarPosition = ScrollBarPosition.OUTSIDE;
			this._allowDrag = true;
			this._offsetToCategoryStart = false;
		} 
		
		private var _showScrollBar:Boolean;
		private var _scrollBarPosition:uint;
		private var _start:Number;
		private var _end:Number;
		private var _range:Number;
		private var _offsetToCategoryStart:Boolean;
		
		private var _allowDrag:Boolean;
		
		public function get showScrollBar():Boolean { return this._showScrollBar; }
		public function setScrollBar(value:IAxisScrollBar):void {
			this.scroll = value;
			this.scroll.position = this._scrollBarPosition;
		}
		
		private var _strStart:String="";
		private var _strEnd:String="";
		
		public function deserialize(data:XML):void {
			if (data.@show_scroll_bar != undefined) this._showScrollBar = SerializerBase.getBoolean(data.@show_scroll_bar);
			if (data.@scroll_bar_position != undefined) {
				switch (SerializerBase.getEnumItem(data.@scroll_bar_position)) {
					case "inside": this._scrollBarPosition = ScrollBarPosition.INSIDE; break;
					case "outside": this._scrollBarPosition = ScrollBarPosition.OUTSIDE; break;
				}
			}
			if (this.axis is BaseCategorizedAxis && this.axis.scale is TextDateTimeScale){
				if (data.@start != undefined)
					this._strStart = SerializerBase.getString(data.@start);
				if (data.@end != undefined)
					this._strEnd = SerializerBase.getString(data.@end);
			}else {
				if (data.@start != undefined)
					this._start = this.axis.scale.deserializeValue(data.@start);
				if (data.@end != undefined)
					this._end = this.axis.scale.deserializeValue(data.@end);
			}
			
			if (data.@visible_range != undefined) this._range = SerializerBase.getNumber(data.@visible_range);
			if (this.axis.scale is DateTimeScale && !isNaN(this._range) && data.@visible_range_unit != undefined) {
				var d:Date = new Date(0);
				switch (SerializerBase.getEnumItem(data.@visible_range_unit)) {
					case "year":	DateUtils.addFloatYear(d, this._range); break;
					case "month":	DateUtils.addFloatMonths(d, this._range); break;
					case "day":		DateUtils.addFloatDays(d, this._range); break;
					case "hour":	DateUtils.addFloatHours(d, this._range); break;
					case "minute":	DateUtils.addFloatMinutes(d, this._range); break;
					case "second":	DateUtils.addFloatSeconds(d, this._range); break;
				}
				this._range = d.valueOf();
			}
			if (data.@allow_drag != undefined) this._allowDrag = SerializerBase.getBoolean(data.@allow_drag);
			if (data.@offset_to_category_start != undefined) this._offsetToCategoryStart = SerializerBase.getBoolean(data.@offset_to_category_start);
		}
		
		public function execZoom():void {
			if (this.scroll == null) {
				var value:Number = this._start;
				value = Math.max(this.axis.scale.minimum, value);
				value = Math.min(this.axis.scale.maximum-Math.abs(this._realRange), value);
				this.axis.scrollBarPositionChange(new ScrollBarEvent(ScrollBarEvent.CHANGE, value),false);
			}
		}
		
		public function deserializeAxisValue(xmlValue:*):Number {
			var strValue:String = SerializerBase.getString(xmlValue);
			var value:Number;
			if (this.axis is BaseCategorizedAxis) {
				value = Number(strValue);
				if (!isNaN(value)) return value-1;
				
				if (BaseCategorizedAxis(this.axis).hasCategory(strValue)) {
					value = BaseCategorizedAxis(this.axis).getCategory(strValue).index;
				
					if (this._offsetToCategoryStart) value = Math.floor(value) - .5;
					return value;
				}
			}
			return this.axis.scale.deserializeValue(strValue);
		}
		
		public function deserializeFromObject(data:Object):void {
			this._start = NaN;
			this._end = NaN;
			this._range = NaN;
			
			if (data.start != undefined) this._start = this.deserializeAxisValue(data.start);
			if (data.end != undefined) this._end = this.deserializeAxisValue(data.end);
			if (data.range != undefined) this._range = SerializerBase.getNumber(data.range);
			if (this.axis.scale is DateTimeScale && !isNaN(this._range) && data.rangeUnit != undefined) {
				var d:Date = new Date(0);
				switch (SerializerBase.getEnumItem(data.rangeUnit)) {
					case "year":	DateUtils.addFloatYear(d, this._range); break;
					case "month":	DateUtils.addFloatMonths(d, this._range); break;
					case "day":		DateUtils.addFloatDays(d, this._range); break;
					case "hour":	DateUtils.addFloatHours(d, this._range); break;
					case "minute":	DateUtils.addFloatMinutes(d, this._range); break;
					case "second":	DateUtils.addFloatSeconds(d, this._range); break;
				}
				this._range = d.valueOf();
			}
			if (data.offsetToCategoryStart != undefined) this._offsetToCategoryStart = SerializerBase.getBoolean(data.offsetToCategoryStart);
		}
		
		public function resetSettings(start:Number = NaN, endOrRange:Number = NaN):void {
			
		}
		
		public function initialize():void {
			var minValue:Number = this.axis.scale.isMinimumAuto ? this.axis.scale.dataRangeMinimum : this.axis.scale.minimum;
			var maxValue:Number = this.axis.scale.isMaximumAuto ? this.axis.scale.dataRangeMaximum : this.axis.scale.maximum;
			//get values 
			if (this._strStart != "") this._start = this.deserializeAxisValue(this._strStart);
			if (this._strEnd != "") this._end = this.deserializeAxisValue(this._strEnd);
			
			this.calculate(minValue, maxValue);
			
			if (this._allowDrag) {
				AxesPlot(this.axis.plot).interactiveScrollContainer.addEventListener(MouseEvent.MOUSE_DOWN, onAxesPlotMouseDown);
				AxesPlot(this.axis.plot).scrollableContainer.addEventListener(MouseEvent.MOUSE_DOWN, onAxesPlotMouseDown);
			}

		}
		
		private var mouseValue:Number;
		private var scrollStart:Number;
		
		public function isDragable():Boolean {
			return this._allowDrag;
		}
		
		private function onAxesPlotMouseDown(e:MouseEvent):void {
			this.axis.scrollSource="plotDrag";
			var eScrollInfo:Object;
			if (this.axis as ValueAxis)
			{
				eScrollInfo=this.axis.plot.getYScrollInfo();
				if (eScrollInfo != null) this.axis.plot.dispatchScrollerEvent("YAxisScrollChange","plotDrag","start",eScrollInfo.start, eScrollInfo.start + eScrollInfo.range, eScrollInfo.range);
			}
			else
			{
				eScrollInfo=this.axis.plot.getXScrollInfo();
				if (eScrollInfo != null) this.axis.plot.dispatchScrollerEvent("XAxisScrollChange","plotDrag","start",eScrollInfo.start, eScrollInfo.start + eScrollInfo.range, eScrollInfo.range);
			}
			this.mouseValue = this.axis.viewPort.getMouseCoord(e);
			this.scrollStart = this.axis.viewPort.getScrollRectStart(AxesPlot(this.axis.plot).scrollableContainer.scrollRect);
			
			var container:IEventDispatcher = this.axis.plot.root.getMainContainer();
			AxesPlot(this.axis.plot).disablePointEvents();
			AxesPlot(this.axis.plot).drawClickedCursor();

			container.addEventListener(MouseEvent.MOUSE_MOVE, onAxesPlotMouseMove, false, 1000);
			container.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
			container.addEventListener(Event.MOUSE_LEAVE, stopDragging);
		}
		
		private function onAxesPlotMouseMove(e:MouseEvent):void {
			this.axis.scrollPhase="progress";
			var newValue:Number = this.axis.viewPort.getMouseCoord(e);
			var r:Number = newValue - this.mouseValue;
			
			var rc:Rectangle = AxesPlot(this.axis.plot).scrollableContainer.scrollRect.clone();
			rc.y = this.scrollStart - r;
			this.setValue(this.axis.scale.fromPix(this.axis.viewPort.checkPix(this.scrollStart - r)));
		}
		
		private function stopDragging(e:Event):void {
			var eScrollInfo:Object;
			if (this.axis as ValueAxis)
			{
				eScrollInfo=this.axis.plot.getYScrollInfo();
				if (eScrollInfo != null) this.axis.plot.dispatchScrollerEvent("YAxisScrollChange","plotDrag","finish",eScrollInfo.start, eScrollInfo.start + eScrollInfo.range, eScrollInfo.range);
			}
			else
			{
				eScrollInfo=this.axis.plot.getXScrollInfo();
				if (eScrollInfo != null) this.axis.plot.dispatchScrollerEvent("XAxisScrollChange","plotDrag","finish",eScrollInfo.start, eScrollInfo.start + eScrollInfo.range, eScrollInfo.range);
			}
			
			var container:IEventDispatcher = this.axis.plot.root.getMainContainer();
			AxesPlot(this.axis.plot).drawNormalCursor();
			AxesPlot(this.axis.plot).enablePointEvents();

			container.removeEventListener(MouseEvent.MOUSE_MOVE, onAxesPlotMouseMove, false);
			container.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);
			container.removeEventListener(Event.MOUSE_LEAVE, stopDragging);
		}
		
		public function update():void {
			this.calculate(this.axis.scale.minimum, this.axis.scale.maximum);
		}
		
		public function setScrollBaseValue():void {
			var value:Number = this.checkScrollDataRange(this.axis.scale.minimum, this.axis.scale.maximum).start;
			this.scroll.value = value;
		}
		
		public function changeScrollInfo(value:Number):void {
			if ((this.scroll as VerticalAxisScrollBar && !this.axis.scale.inverted) || (this.scroll as HorizontalAxisScrollBar && this.axis.scale.inverted)) 
				this.scroll.value= (value+this.realRange > this.axis.scale.maximum)? this.axis.scale.maximum : value+this.realRange;
			else
				this.scroll.value = value;
		}
		
		private var _realRange:Number;
		
		private function calculate(minValue:Number, maxValue:Number):void {
			var info:ScrollTempInfo = this.checkScrollDataRange(minValue, maxValue);
			
			var scaleRange:Number = maxValue - minValue;
			this.valueMult = info.range/scaleRange;
			if (this.axis.viewPort.isInverted(this.axis.scale.inverted)) {
				this.scroll.min = maxValue;
				this.scroll.max = minValue;
				this.scroll.visibleRange = -info.range;
			}else {
				this.scroll.min = minValue;
				this.scroll.max = maxValue;
				this.scroll.visibleRange = info.range;
			}
		}
		
		private function checkScrollDataRange(minValue:Number, maxValue:Number):ScrollTempInfo {
			
			var info:ScrollTempInfo = new ScrollTempInfo();
			
			var isRangeSet:Boolean = !isNaN(this._range);
			var isStartSet:Boolean = !isNaN(this._start);
			var isEndSet:Boolean = !isNaN(this._end);
			
			var realMinValue:Number = Math.min(minValue, maxValue);
			var realMaxValue:Number = Math.max(minValue, maxValue);
			
			minValue = realMinValue;
			maxValue = realMaxValue;
			
			if (isStartSet && isEndSet) {
				var realStart:Number = Math.min(this._start, this._end);
				var realEnd:Number = Math.max(this._start, this._end);
				this._start = realStart;
				this._end = realEnd;
			}
			
			if (!isRangeSet && !isStartSet && !isEndSet) {
				info.range = (maxValue - minValue)/2;
				info.start = minValue;
			}else if (isRangeSet && !isStartSet && !isEndSet) {
				info.range = this._range;
				info.start = minValue;
			}else if (!isRangeSet && isStartSet && !isEndSet) {
				info.range = maxValue - this._start;
				info.start = this._start;
			}else if (!isRangeSet && !isStartSet && isEndSet) {
				info.range = this._end - minValue;
				info.start = minValue;
			}else if (isRangeSet && isStartSet && !isEndSet) {
				info.range = this._range;
				info.start = this._start;
			}else if (isRangeSet && !isStartSet && isEndSet) {
				info.range = this._range;
				info.start = this._end - info.range;
			}else if (!isRangeSet && isStartSet && isEndSet) {
				info.range = this._end - this._start;
				info.start = this._start;
			}else {
				info.range = this._range;
				info.start = this._start;
			}
			
			info.range = Math.min(Math.abs(info.range), Math.abs(maxValue - minValue));
			if ((this.scroll as VerticalAxisScrollBar && !this.axis.scale.inverted) || (this.scroll as HorizontalAxisScrollBar && this.axis.scale.inverted)) 
				info.start=realEnd;
				
			this._realRange = info.range;
			
			return info;
		}
		
		public function get realRange():Number { return this._realRange; }
		
		public function setValue(value:Number):void {
			if (this.scroll)
				this.scroll.value = value;	
		}

		
		public function getScrolllInfo():Object {
			var res:ScrollTempInfo = new ScrollTempInfo();
			if (this.scroll) 
			{
				res.range = this.scroll.visibleRange;
				res.start = this.scroll.value;
				// inverted
				if (res.range <0) 
				{
					res.start += res.range;
					res.range = -res.range;
				}
			}
			return res;
		}
		
		public function apply(bounds:Rectangle):void {
			this.axis.viewPort.applyZoomFactor(bounds, 1/this.valueMult);
		}
		
		public function reset(bounds:Rectangle):void {
			this.axis.viewPort.applyZoomFactor(bounds, this.valueMult);
		}
		
		public function isScrollVisible(minValue:Number, maxValue:Number):Boolean {
			if (!this._showScrollBar) return false;
			
			var info:ScrollTempInfo = this.checkScrollDataRange(minValue, maxValue);
			return info.range < Math.abs(maxValue-minValue);
		}
	}
}

class ScrollTempInfo {
	public var start:Number;
	public var range:Number;
}