package com.anychart.radarPolarPlot.series.lineSeries {
	import com.anychart.utils.LineEquation;
	
	import flash.geom.Point;
	
	//import flash.geom.Point;
	
	internal final class Tangent
	{
		public var point:Point;
		public var line:LineEquation;
	}
}