package com.anychart.mapPlot.connectorSeries {
	import com.anychart.data.SeriesType;
	import com.anychart.mapPlot.multiPointSeries.TemplateUtils;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	import com.anychart.seriesPlot.data.settings.GlobalSeriesSettings;
	import com.anychart.styles.ProgrammaticStyle;
	import com.anychart.styles.states.LineStyleState;
	import com.anychart.styles.states.StyleState;
	
	public final class ConnectorGlobalSeriesSettings extends GlobalSeriesSettings {
		//-----------------------------------------------------------
		//			TEMPLATES
		//-----------------------------------------------------------
		
		private static var initTemplate:Boolean = checkTemplateClass();
		private static function checkTemplateClass():Boolean {
			var u:TemplateUtils;
			return true;
		} 
		
		//-----------------------------------------------------------
		//			SETTINGS
		//-----------------------------------------------------------
		
		override public function createSeries(seriesType:uint):BaseSeries {
			var series:ConnectorSeries = new ConnectorSeries();
			series.type = SeriesType.CONNECTOR;
			return series;
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new BaseSeriesSettings();
		}
		
		override public function getSettingsNodeName():String {
			return "connector_series";
		}
		
		override public function getStyleNodeName():String {
			return "connector_style";
		}
		
		override public function getStyleStateClass():Class {
			return LineStyleState;
		}
		
	}
}