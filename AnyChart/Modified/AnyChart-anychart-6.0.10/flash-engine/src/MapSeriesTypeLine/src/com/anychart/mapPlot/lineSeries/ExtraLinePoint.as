package com.anychart.mapPlot.lineSeries {
	import com.anychart.mapPlot.data.GeoBasedPoint;
	import com.anychart.styles.states.LineStyleState;
	
	import flash.display.Graphics;
	
	internal final class ExtraLinePoint extends GeoBasedPoint {
		
		override protected function execDrawing():void {
			super.execDrawing();
			if (this.series.points.length == 0) return;
			this.drawLine(this.container.graphics, this.series.points);
		}
		
		override protected function redraw():void {
			super.redraw();
			if (this.series.points.length == 0) return;
			this.drawLine(this.container.graphics, this.series.points);
		}
		
		private function drawLine(g:Graphics, points:Array):void {
			var pt:GeoBasedPoint = points[0];
			var state:LineStyleState = LineStyleState(this.styleState);
			if (state.stroke == null) return;
			state.stroke.apply(g,null,this.color);
			g.moveTo(pt.pixGeo.x, pt.pixGeo.y);
			for (var i:int = 1;i<points.length;i++){
				pt = points[i];
				g.lineTo(pt.pixGeo.x, pt.pixGeo.y);
			}
		}
	}
}