package com.anychart.mapPlot.lineSeries {
	import com.anychart.mapPlot.data.GeoBasedPoint;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSeries;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.seriesPlot.elements.markerElement.MarkerElement;
	import com.anychart.seriesPlot.elements.tooltipElement.TooltipElement;
	
	internal final class MapLineSeries extends BaseSeries {
		
		private var extraPt:ExtraLinePoint;
		
		public function MapLineSeries() {
			super();
			this.needCallingOnBeforeDraw = true;
		}
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			extraPt = new ExtraLinePoint();
			extraPt.series = this;
			extraPt.color = this.color;
			extraPt.settings = this.settings.createCopy();
			extraPt.marker = MarkerElement(this.marker.createCopy());
			extraPt.label = LabelElement(this.label.createCopy());
			extraPt.tooltip = TooltipElement(this.tooltip.createCopy());
			extraPt.marker.enabled = false;
			extraPt.label.enabled = false;
			extraPt.tooltip.enabled = false;
			extraPt.initialize();
		}
		
		override public function onBeforeDraw():void {
			this.plot.drawingPoints.push(extraPt);
		}
		
		override public function createPoint():BasePoint {
			return new GeoBasedPoint();
		}
	}
}