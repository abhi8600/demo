package com.anychart.mapPlot.grid {
	public final class AxisLabelsPosition {
		public static const NEAR:uint = 0;
		public static const ONE_QUARTER:uint = 1;
		public static const CENTER:uint = 2;
		public static const THREE_QUARTERS:uint = 3;
		public static const FAR:uint = 4; 
	}
}