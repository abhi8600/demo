package com.anychart.mapPlot.controls.zoomPanel {
	import com.anychart.visual.fill.Fill;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	[Event(name="change", type="flash.events.Event")]
	internal class Slider extends Sprite {
		public var bounds:Rectangle;
		protected var dragBounds:Rectangle;
		protected var sliderBounds:Rectangle;
		protected var thumb:SliderThumb;
		
		private var fill:Fill;
		
		public function Slider() {
			super();
			this.thumb = new SliderThumb();
			this.bounds = new Rectangle();
			this.dragBounds = new Rectangle();
			this.sliderBounds = new Rectangle();
			this.fill = new Fill();
			this.fill.deserialize(<fill type="Gradient" opacity=".9">
				<gradient angle={this.getGradientAngle()}>
					<key color="#DDDDDD" />
					<key color="#FEFEFE" />
				</gradient>
			</fill>, null);
			this.addChild(this.thumb);
			this.thumb.addEventListener(MouseEvent.MOUSE_DOWN, this.thumbMouseDownHandler);
		}
		
		public function initialize(w:Number, h:Number):void {
			this.bounds.width = w;
			this.bounds.height = h;
			this.setSliderBounds();
		}
		
		protected function setSliderBounds():void {}
		protected function getGradientAngle():Number { return 0; }
		
		public function draw():void {
			var g:Graphics = this.graphics;
			this.fill.begin(g, this.sliderBounds);
			g.lineStyle(1,0xAAAAAA);
			g.drawRect(this.sliderBounds.x, this.sliderBounds.y, this.sliderBounds.width, this.sliderBounds.height);
			g.endFill();
			g.lineStyle();
			this.thumb.draw();
		}
		
		private function thumbMouseDownHandler(e:MouseEvent):void {
			this.root.addEventListener(MouseEvent.MOUSE_UP, this.thumbMouseUpHandler);
			this.thumb.startDrag(false, this.dragBounds);
		}
		
		private function thumbMouseUpHandler(e:MouseEvent):void {
			this.root.removeEventListener(MouseEvent.MOUSE_UP, this.thumbMouseUpHandler);
			this.thumb.stopDrag();
			this.dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get value():Number { return NaN; }
		public function set value(newValue:Number):void {}
	}
}