package com.anychart.resources {
	import deng.fzip.FZip;
	import deng.fzip.FZipErrorEvent;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	public final class MapResourceEntry extends ResourceEntry {

		private var loader:FZip;
		
		public function MapResourceEntry() {
			this.loader = new FZip();
			this.initListeners(this.loader);
			super();
		}
		
		override protected function initListeners(target:IEventDispatcher):void {
			super.initListeners(target);
			FZip(target).addEventListener(FZipErrorEvent.PARSE_ERROR, this.parseErrorHandler);
		}
		
		override protected function initCompleteListener(target:IEventDispatcher):void {
			FZip(target).addEventListener(Event.COMPLETE, this.completeEventHandler);
		}
		
		private function parseErrorHandler(event:FZipErrorEvent):void {
			//TODO
		}
		
		override public function stopLoading():void {
			if (this.isLoading) {
				this.loader.close();
				this.isLoading = false;
			}
		}
		
		override protected function execLoading():void {
			this.loader.load(this.request);
		}
		
		override protected function setData(event:ResourceEntryEvent):void {
			event.data = this.loader;
		}
	}
}