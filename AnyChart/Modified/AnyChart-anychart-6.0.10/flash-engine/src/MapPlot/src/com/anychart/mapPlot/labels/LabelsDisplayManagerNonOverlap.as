package com.anychart.mapPlot.labels
{
	import flash.display.Sprite;

	internal final class LabelsDisplayManagerNonOverlap extends LabelsDisplayManagerNonOverlapBase {
		
		override public function process():void {
			squareCoordinator.clear();

	        var ln:uint = lables.length;
	        var labelNum:uint;
	        for (labelNum = 0; labelNum < ln; labelNum++) {
/*
if (LabelInfo(lables[labelNum]).labelContainer is Sprite) {
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.lineStyle(1, 0xFF0000);
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.drawRect(
		LabelInfo(lables[labelNum]).labelBounds.x,
		LabelInfo(lables[labelNum]).labelBounds.y,
		LabelInfo(lables[labelNum]).labelBounds.width,
		LabelInfo(lables[labelNum]).labelBounds.height)

	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.lineStyle(1, 0x00FF00);
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.drawRect(
		LabelInfo(lables[labelNum]).regionPixBounds.x,
		LabelInfo(lables[labelNum]).regionPixBounds.y,
		LabelInfo(lables[labelNum]).regionPixBounds.width,
		LabelInfo(lables[labelNum]).regionPixBounds.height)
}
//*/

	            if (isCuttingWithPlaced(labelNum)) {
	            	LabelInfo(lables[labelNum]).labelContainer.visible = false;
	        	} else {
	                squareCoordinator.addBox(LabelInfo(lables[labelNum]).labelBounds, labelNum);
	                LabelInfo(lables[labelNum]).labelContainer.visible = true;
				}
	        }
	 	}
		
	}
}