package com.anychart.mapPlot.grid {
	internal final class AxisLabelsFormat {
		public static const DECIMAL:uint = 0;
		public static const SEXAGESIMAL:uint =1;
	}
}