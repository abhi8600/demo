package com.anychart.mapPlot.geoSpace.projection {
	public final class ProjectionType {
		public static const EQUIRECTANGULAR:uint = 0;
		public static const MERCATOR:uint = 1;
		public static const WAGNER_3:uint = 2;
		public static const FAHEY:uint = 3;
		public static const ECKERT_1:uint = 4;
		public static const ECKERT_3:uint = 5;
		public static const HAMMER_AITOFF:uint = 6;
		public static const ROBINSON:uint = 7;
		public static const ORTHOGRAPHIC:uint = 8;
		public static const BONNE:uint = 9;
	}
}