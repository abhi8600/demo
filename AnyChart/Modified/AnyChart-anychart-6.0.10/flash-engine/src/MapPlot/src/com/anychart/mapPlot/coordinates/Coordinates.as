package com.anychart.mapPlot.coordinates {
	
	public final class Coordinates {
		
		private static const G:String = "°" ;//°
		private static const M:String = "′" ;//′
		private static const S:String = "″" ;//″
		
		public static function getStrLatitude(lat:Number):String {
			var c:Number = int(Math.abs(lat));
			var result:String = c.toString() + G;
			c = (Math.abs(lat) - c) * 60;
			var tmp:int = int(c);
			if (tmp != 0) 
				result += tmp.toString() + M;
			tmp = int((c - tmp) * 60);
			if (tmp != 0) 
				result += tmp.toString() + S;
			if (lat > 0) result += "S"; else result += "N";
			return result;
		}

		public static function getStrLongitude(lon:Number):String {
			var c:Number = int(Math.abs(lon));
			var result:String = c.toString() + G;
			c = (Math.abs(lon) - c) * 60;
			var tmp:int = int(c);
			if (tmp != 0) 
				result += tmp.toString() + M;
			tmp = int((c - tmp) * 60);
			if (tmp != 0) 
				result += tmp.toString() + S;
			if (lon < 0) result += "W"; else result += "E";
			return result;
		}

		public static function getIntLatitude(lat:String):Number {
			var pos:int = lat.search(G);
			var str:String = lat.substr(0, pos);
			var result:Number = int(str);
			var posLast:int = pos + 1;
			var tmp:Number = 0;
			
			pos = lat.search(M);
			if (pos > 0)
			{
				str = lat.substr(posLast, pos - posLast);
				tmp = int(str) * 60;
				posLast = pos + 1;
			}
			pos = lat.search(S);
			if (pos > 0)
			{
				str = lat.substr(posLast, pos - posLast);
				tmp += int(str);
			}
			result += tmp / 3600;
			if (lat.substr(lat.length - 1, 1) == "N") return result;
			else return -result;
		}

		public static function getIntLongitude(lon:String):Number {
			var pos:int = lon.search(G);
			var str:String = lon.substr(0, pos);
			var result:Number = int(str);
			var posLast:int = pos + 1;
			var tmp:Number = 0;
			
			pos = lon.search(M);
			if (pos > 0)
			{
				str = lon.substr(posLast, pos - posLast);
				tmp = int(str) * 60;
				posLast = pos + 1;
			}
			pos = lon.search(S);
			if (pos > 0)
			{
				str = lon.substr(posLast, pos - posLast);
				tmp += int(str);
			}
			result += tmp / 3600;
			if (lon.substr(lon.length - 1, 1) == "E") return result;
			else return -result;
		}

	}
}