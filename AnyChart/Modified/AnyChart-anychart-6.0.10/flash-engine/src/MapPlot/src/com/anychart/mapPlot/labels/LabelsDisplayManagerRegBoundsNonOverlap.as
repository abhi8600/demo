package com.anychart.mapPlot.labels {

	import flash.display.Sprite;

	internal final class LabelsDisplayManagerRegBoundsNonOverlap extends LabelsDisplayManagerNonOverlapBase {

		override public function process():void {
			squareCoordinator.clear();
            var nonePlaced:Array = new Array();
            var ln:uint = lables.length;
            var curentLabel:LabelInfo;
            var labelNum:uint;
            for (labelNum = 0; labelNum < ln; labelNum++) {

/*
if (LabelInfo(lables[labelNum]).labelContainer is Sprite) {
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.lineStyle(1, 0xFF0000);
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.drawRect(
		LabelInfo(lables[labelNum]).labelBounds.x,
		LabelInfo(lables[labelNum]).labelBounds.y,
		LabelInfo(lables[labelNum]).labelBounds.width,
		LabelInfo(lables[labelNum]).labelBounds.height)

	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.lineStyle(1, 0x00FF00);
	Sprite(LabelInfo(lables[labelNum]).labelContainer.parent).graphics.drawRect(
		LabelInfo(lables[labelNum]).regionPixBounds.x,
		LabelInfo(lables[labelNum]).regionPixBounds.y,
		LabelInfo(lables[labelNum]).regionPixBounds.width,
		LabelInfo(lables[labelNum]).regionPixBounds.height)
}
//*/

            	curentLabel = LabelInfo(lables[labelNum]);
            	if (curentLabel.regionPixBounds.containsRect(curentLabel.labelBounds)) {
                	curentLabel.labelContainer.visible = true;
	                squareCoordinator.addBox(curentLabel.labelBounds, labelNum);
		   		} else {
	                curentLabel.labelContainer.visible = false;
		   			nonePlaced.push(labelNum);
		   		}
		   	}
            for each(labelNum in nonePlaced) {

                if (isCuttingWithPlaced(labelNum)) {
	                LabelInfo(lables[labelNum]).labelContainer.visible = false;
            	} else {
	                LabelInfo(lables[labelNum]).labelContainer.visible = true;
            		squareCoordinator.addBox(LabelInfo(lables[labelNum]).labelBounds, labelNum);
				}
            }
		}
	}
}