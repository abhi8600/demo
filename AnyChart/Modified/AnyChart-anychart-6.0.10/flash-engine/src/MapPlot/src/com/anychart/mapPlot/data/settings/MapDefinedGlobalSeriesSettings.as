package com.anychart.mapPlot.data.settings {
	import com.anychart.resources.ResourcesLoader;
	import com.anychart.seriesPlot.data.settings.BaseSeriesSettings;
	
	
	public class MapDefinedGlobalSeriesSettings extends MapGroupGlobalSeriesSettings {
		
		override public function deserialize(data:XML):void {
			super.deserialize(data);
			if (data.defined_map_region[0] != null)
				super.deserialize(data.defined_map_region[0]);
		}
		
		override public function deserializeElements(data:XML, styles:XML, resources:ResourcesLoader):void {
			super.deserializeElements(data, styles, resources);
			if (data.defined_map_region[0] != null)
				super.deserializeElements(data.defined_map_region[0], styles, resources);
		}
		
		override public function createSettings():BaseSeriesSettings {
			return new MapDefinedSeriesSettings();
		}
	}
}