package com.anychart.mapPlot.controls.zoomPanel {
	import com.anychart.mapPlot.controls.MapControlButton;
	
	import flash.geom.Rectangle;
	
	internal class ZoomButton extends MapControlButton {
		override public function initialize(w:Number, h:Number):void {
			super.initialize(w, h);
			
			this.pointerBounds = new Rectangle(w/2-(w/3)/2,h/2-(h/3)/2,w/3,h/3);
		}
	}
}