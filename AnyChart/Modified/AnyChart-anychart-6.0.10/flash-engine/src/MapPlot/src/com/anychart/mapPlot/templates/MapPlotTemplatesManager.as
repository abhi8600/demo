package com.anychart.mapPlot.templates {
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.utils.XMLUtils;
	
	public final class MapPlotTemplatesManager extends SeriesPlotTemplatesManager {
		
		override protected function applyDefaults(res:XML, defaults:XML):void {
			super.applyDefaults(res, defaults);
			if (res == null || defaults == null) return;
			
			var defaultNavigationPanel:XML = defaults.navigation_panel[0];
			var defaultZoomPanel:XML = defaults.zoom_panel[0];
			var cnt:int;
			var i:int;
			
			if (defaultNavigationPanel != null) { 
				if (res.chart_settings[0] != null && res.chart_settings[0].controls[0] != null) {
					
					var navPanels:XMLList = res.chart_settings[0].controls[0].navigation_panel;
					cnt = navPanels.length();
					
					for (i = 0;i<cnt;i++)
						navPanels[i] = XMLUtils.merge(defaultNavigationPanel, navPanels[i], "navigation_panel");
				}
			}
			
			if (defaultZoomPanel != null) { 
				if (res.chart_settings[0] != null && res.chart_settings[0].controls[0] != null) {
					
					var zoomPanels:XMLList = res.chart_settings[0].controls[0].zoom_panel;
					cnt = navPanels.length();
					
					for (i = 0;i<cnt;i++)
						zoomPanels[i] = XMLUtils.merge(defaultZoomPanel, zoomPanels[i], "zoom_panel");
				}
			}
		}
	}
}