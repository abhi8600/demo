package com.anychart.mapPlot.controls.navigationPanel {
	import com.anychart.mapPlot.controls.MapControlButton;
	
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	
	internal final class LeftNavigationButton extends MapControlButton {
		
		override protected function drawPointer():void {
			var g:Graphics = this.graphics;
			var rc:Rectangle = this.pointerBounds;
			
			var opacity:Number=1;
			
			g.beginFill(0,0);
			g.lineStyle(1,0,opacity);
			g.moveTo(rc.left,rc.top+rc.height/2);
			g.lineTo(rc.left+rc.width,rc.top);
			g.lineStyle(0,0,0);
			g.endFill();
			
			g.lineStyle(1,0,opacity);
			g.moveTo(rc.left,rc.top+rc.height/2);
			g.lineTo(rc.left+rc.width,rc.top+rc.height);
			g.lineStyle(0,0,0);
			g.endFill();
		}
		
	}
}