package com.anychart.mapPlot.controls.zoomPanel {
	import com.anychart.mapPlot.controls.MapControlButton;
	import com.anychart.styles.Style;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	internal class SliderThumb extends Sprite {
		
		private var styleState:BackgroundBaseStyleState;
		public var bounds:Rectangle;
		
		public function SliderThumb() {
			super();
			this.mouseChildren = false;
			this.buttonMode = true;
			this.useHandCursor = true;
			
			this.bounds = new Rectangle();
			this.styleState = BackgroundBaseStyleState(style.normal);
			this.addEventListener(MouseEvent.MOUSE_OVER, this.mouseOverHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT, this.mouseOutHandler);
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDownHandler);
			this.addEventListener(MouseEvent.MOUSE_UP, this.mouseUpHandler);
		}
		
		private function mouseOverHandler(e:MouseEvent):void {
			this.styleState = BackgroundBaseStyleState(style.hover);
			this.draw();
		}
		
		private function mouseOutHandler(e:MouseEvent):void {
			this.styleState = BackgroundBaseStyleState(style.normal);
			this.draw();
		}
		
		private function mouseDownHandler(e:MouseEvent):void {
			this.styleState = BackgroundBaseStyleState(style.pushed);
			this.draw();
		}
		
		private function mouseUpHandler(e:MouseEvent):void {
			this.styleState = BackgroundBaseStyleState(style.hover);
			this.draw();
		}
		
		public final function draw():void {
			var g:Graphics = this.graphics;
			g.clear();
			this.styleState.fill.begin(g, this.bounds);
			this.styleState.stroke.apply(g, this.bounds);
			g.drawRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			g.lineStyle();
			g.endFill();
			this.filters = (this.styleState.effects != null) ? this.styleState.effects.list : null;
		}
		
		private static var style:Style = createThumbStyle();
		private static function createThumbStyle():Style {
			return MapControlButton.style;
		}
		
	}
}