package com.anychart.mapPlot.geoSpace {
	import com.anychart.scales.BaseScale;
	
	public final class MapScale extends BaseScale {
		
		public function MapScale() {
			this.isMinimumAuto = false;
			this.isMaximumAuto = false;
		}
		
		override public function calculate():void {
			
			//var isLabelsCalculated:Boolean = false;
			
			if (this.isMinimumAuto ||
			    this.isMaximumAuto ||
			    this.isMajorIntervalAuto ||
			    this.isMinorIntervalAuto) {
		
				//подсчет мажорного и минорного интервала
				if (this.isMajorIntervalAuto) {
					this.majorInterval = this.calculateOptimalStepSize(this.maximum - this.minimum, 7, false);
					/* if (this.axis.labels != null && !this.axis.labels.allowOverlap) {
						this.calculateBaseValue();
						this.calculateMajorTicksCount();
						var maxLabels:int = this.axis.labels.calcMaxLabelsCount();
						if (maxLabels <= (this.maximum - this.minimum)/this.majorInterval)
							this.majorInterval = this.calculateOptimalStepSize(this.maximum - this.minimum, maxLabels, true);
						isLabelsCalculated = true;
					} */
				}
	
				if (this.isMinorIntervalAuto) {
					//this.minorInterval = this.calculateOptimalStepSize(this.majorInterval, 4, true);
					this.minorInterval = this.majorInterval/3;
				}
					
				//применение мажорного интервала к минимуму и максимуму
				if (this.isMinimumAuto) {
					this.minimum -= this.safeMod(this.minimum, this.majorInterval);
				}
					
				if (this.isMaximumAuto) {
					var d:Number = safeMod(this.maximum, this.majorInterval);
					if (d != 0) {
						this.maximum += this.majorInterval - d;
					}
				}
		    }
		    /*
		    if (!isLabelsCalculated && this.axis.labels != null)
	    		this.axis.labels.initMaxLabelSize(); */
			
			this.calculateBaseValue();
			this.calculateTicksCount();
			this.calculateMinorStart();
		}
		
		override protected function calculateBaseValue():void {
			this.baseValue = Math.ceil(Number(this.minimum)/Number(this.majorInterval) - 0.00000001 )* Number(this.majorInterval);
		}		

	}
}