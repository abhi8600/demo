package com.anychart.mapPlot.geoSpace.projection {
	import com.anychart.mapPlot.geoSpace.BaseGeoSpace;
	import com.anychart.mapPlot.geoSpace.projection.transformers.ProjectionTransformer;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public final class Projection implements ISerializable {
		public var type:int;
		public var centerX:Number;
		public var centerY:Number;
		public var customCentroid:Boolean;
		
		public function Projection() {
			this.type = -1;
			this.centerX = NaN;
			this.centerY = NaN;
			this.customCentroid = false;
		}
		
		public function deserialize(data:XML):void {
			if (data.@type != undefined) this.type = this.deserializeType(data.@type);
			if (data.@centroid_x != undefined) this.centerX = SerializerBase.getNumber(data.@centroid_x);
			if (data.@centroid_y != undefined) this.centerY = -SerializerBase.getNumber(data.@centroid_y);
			this.customCentroid = !isNaN(this.centerX) || !isNaN(this.centerY);
		}
		
		public function deserializeType(value:*):int {
			switch (SerializerBase.getEnumItem(value)) {
				case "equirectangular": return ProjectionType.EQUIRECTANGULAR;
				case "mercator": return ProjectionType.MERCATOR;
				case "wagner3": return ProjectionType.WAGNER_3;
				case "fahey": return ProjectionType.FAHEY;
				case "eckert1": return ProjectionType.ECKERT_1;
				case "eckert3": return ProjectionType.ECKERT_3;
				case "hammeraitoff": return ProjectionType.HAMMER_AITOFF;
				case "robinson": return ProjectionType.ROBINSON;
				case "orthographic": return ProjectionType.ORTHOGRAPHIC;
				case "bonne": return ProjectionType.BONNE;
				default:
					return -1;
			}
		}
		
		public function createCopy():Projection {
			var p:Projection = new Projection();
			p.type = this.type;
			p.centerX = this.centerX;
			p.centerY = this.centerY;
			p.transformer = ProjectionTransformer.create(p);
			p.customCentroid = this.customCentroid;
			return p;
		}
		
		//--------------------------------------------------------------------------
		//						TRANSFORMATION
		//--------------------------------------------------------------------------
		
		public function get needExtraPoints():Boolean { return this.transformer.needExtraPoints; }
		
		private var transformer:ProjectionTransformer;
		
		public function createTransformer():void {
			this.transformer = ProjectionTransformer.create(this);
		}
    	
		public function transform(geoX:Number, geoY:Number, pt:Point):void {
			this.transformer.transform(geoX, geoY, pt);
		}
		
		public function interpolateLine(start:Point, end:Point, linearPoints:Array):void {
			this.transformer.interpolateLine(start, end, linearPoints);
		}
		
		public function setLinearGeoBounds(geoBounds:Rectangle, linearGeoBounds:Rectangle):void {
			this.transformer.setLinearBounds(geoBounds, linearGeoBounds);
		}
		
		public function drawLine(space:BaseGeoSpace, g:Graphics, start:Point, end:Point, moveTo:Boolean):void {
			this.transformer.drawLine(g, space, start, end, moveTo);
		}
		
	}
}