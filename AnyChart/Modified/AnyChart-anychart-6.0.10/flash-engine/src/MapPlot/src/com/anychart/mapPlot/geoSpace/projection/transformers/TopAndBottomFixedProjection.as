package com.anychart.mapPlot.geoSpace.projection.transformers {
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	internal class TopAndBottomFixedProjection extends ProjectionTransformer {
		
		override public function setLinearBounds(geoBounds:Rectangle, linearGeoBounds:Rectangle):void {
			var minX:Number = Number.POSITIVE_INFINITY;
			var maxX:Number = Number.NEGATIVE_INFINITY;
			var minY:Number = Number.POSITIVE_INFINITY;
			var maxY:Number = Number.NEGATIVE_INFINITY;
			
			var pt:Point = new Point();
			this.transform(geoBounds.left, geoBounds.top, pt);
			minY = Math.min(pt.y, minY);
			maxY = Math.max(pt.y, maxY);
			
			this.transform(geoBounds.right, geoBounds.bottom, pt);
			minY = Math.min(pt.y, minY);
			maxY = Math.max(pt.y, maxY);
			
			var geoX:Number;
			var geoY:Number;
			for (var i:uint = 0;i<300;i++) {
				geoY = geoBounds.top + geoBounds.height*i/300;
				geoX = geoBounds.left;
				this.transform(geoX, geoY, pt);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
				
				geoX = geoBounds.right;
				this.transform(geoX, geoY, pt);
				minX = Math.min(pt.x, minX);
				maxX = Math.max(pt.x, maxX);
			}
			
			linearGeoBounds.x = minX;
			linearGeoBounds.y = minY;
			linearGeoBounds.width = maxX - minX;
			linearGeoBounds.height = maxY - minY;
		}
	}
}