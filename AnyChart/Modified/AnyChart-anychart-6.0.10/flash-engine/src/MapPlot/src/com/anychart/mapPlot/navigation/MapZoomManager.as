package com.anychart.mapPlot.navigation {
	import com.anychart.mapPlot.MapPlot;
	import com.anychart.mapPlot.controls.zoomPanel.ZoomPanel;
	import com.anychart.serialization.ISerializable;
	import com.anychart.serialization.SerializerBase;
	
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	public final class MapZoomManager implements ISerializable {
		private var _factor:Number;
		
		public function set factor(value:Number):void {
			this._factor = Math.min(Math.max(value, this.minimum), this.maximum);
			for each (var control:ZoomPanel in this.controls) {
				control.setSliderValue();
			}
		}
		
		public function get factor():Number { return this._factor; }
		
		public var controls:Array;
		
		internal var x:Number;
		internal var y:Number;
		internal var geoX:Number;
		internal var geoY:Number;
		
		private var mouseWheel:Boolean;
		public var minimum:Number;
		public var maximum:Number;
		public var step:Number;
		
		public function MapZoomManager() {
			this.x = .5;
			this.y = .5;
			this.mouseWheel = false;
			
			this.minimum = 1;
			this.maximum = 10;
			this.factor = 1;
			this.step = 2;
			
			this.canZoom = false;
			this.controls = [];
			this.dragable = false;
		}
		
		public function get needMoveMap():Boolean {
			return this.factor != 1 || this.x != .5 || this.y != .5 || !isNaN(this.geoX) || !isNaN(this.geoY);
		}
		
		private var plot:MapPlot;
		private var canZoom:Boolean;
		internal var dragable:Boolean;
		
		public function deserialize(data:XML):void {
			if (data.@x != undefined) this.x = SerializerBase.getRatio(data.@x);
			if (data.@y != undefined) this.y = SerializerBase.getRatio(data.@y);
			if (data.@geo_x != undefined) this.geoX = SerializerBase.getNumber(data.@geo_x);
			if (data.@geo_y != undefined) this.geoY = -SerializerBase.getNumber(data.@geo_y);
			if (data.@mouse != undefined) this.mouseWheel = SerializerBase.getBoolean(data.@mouse);
			if (data.@step != undefined) this.step = SerializerBase.getNumber(data.@step);
			if (data.@minimum != undefined) this.minimum = SerializerBase.getNumber(data.@minimum);
			if (data.@maximum != undefined) this.maximum = SerializerBase.getNumber(data.@maximum);
			if (data.@factor != undefined) this.factor = SerializerBase.getNumber(data.@factor);
			if (data.@draggable != undefined) this.dragable = SerializerBase.getBoolean(data.@draggable);
		}
		
		public function initZoom(plot:MapPlot):void {
			this.plot = plot;
			if (this.mouseWheel) {
				this.plot.scrollableContainer.addEventListener(MouseEvent.MOUSE_WHEEL, this.mouseWheelHandler);
			}
			this.canZoom = true;
		}
		
		public function destroy():void {
			if (this.plot && this.plot.scrollableContainer) {
				this.plot.scrollableContainer.removeEventListener(MouseEvent.MOUSE_WHEEL, this.mouseWheelHandler);
				this.plot.scrollableContainer = null;
			}
			this.plot = null;
		}
		
		public function applyZoom(totalRect:Rectangle, mapRect:Rectangle):void {
			mapRect.width *= this.factor;
			mapRect.height *= this.factor;
			
			this.mapRect = mapRect;
		}
		
		public var dragContainer:Rectangle;
		
		private var mapRect:Rectangle;
		
		private function mouseWheelHandler(event:MouseEvent):void {
			if (!this.canZoom) 
				return;
				
			var d:Number = event.delta;
			this.factor += d>0 ? .5 : -.5; 
			
			var dx:Number = this.plot.scrollableContainer.mouseX - this.mapRect.x;
			var dy:Number = this.plot.scrollableContainer.mouseY - this.mapRect.y;
			
			this.x = dx/mapRect.width;
			this.y = dy/mapRect.height;
			this.canZoom = false;
			this.plot.resize(this.plot.globalBounds);
			this.canZoom = true;
		}
	}
}