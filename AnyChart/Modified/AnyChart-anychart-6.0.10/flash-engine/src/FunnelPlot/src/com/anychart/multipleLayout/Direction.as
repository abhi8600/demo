package com.anychart.multipleLayout{
	
	public final class Direction{
		public static const VERTICAL:uint = 0;
		public static const HORIZONTAL:uint = 1;	
	}
}