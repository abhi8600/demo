package com.anychart.labels{
	
	public final class LabelsPlacementMode{
		public static const	OUTSIDE_LEFT_IN_COLUMN:uint = 0;		
		public static const	OUTSIDE_LEFT:uint = 1;
		public static const	OUTSIDE_RIGHT_IN_COLUMN:uint = 2;
		public static const	OUTSIDE_RIGHT:uint = 3;
		public static const	INSIDE:uint = 4;
	}
}