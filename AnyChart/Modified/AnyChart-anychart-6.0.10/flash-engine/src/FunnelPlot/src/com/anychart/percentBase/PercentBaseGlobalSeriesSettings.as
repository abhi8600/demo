package com.anychart.percentBase{
	import com.anychart.labels.CustomLabelElement;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.serialization.SerializerBase;
	import com.anychart.seriesPlot.data.settings.BaseSingleValueGlobalSeriesSettings;
	import com.anychart.seriesPlot.elements.labelElement.LabelElement;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.stroke.Stroke;
	
	/**
	 * Класс получающий настройки Cone, Funel, Pyramid серий из XML
	 */
	public class PercentBaseGlobalSeriesSettings extends BaseSingleValueGlobalSeriesSettings {
		
		/**
		 * параметр задающий глубину трехмерности
		 */
		public var size3D:Number;
		
		/** 
		 * расстояние между секторами
		 */
		public var padding:Number;
		
		/**
		 * минимальный размер точки
		 */
		public var minPointSize:Number;
		
		public var connector:Stroke;
		public var connnectorPadding:Number;
		
		/**
		 * положение Лейблов 
		 * 
		 * @see com.anychart.labels.LabelsPlacementMode
		 * @see com.anychart.percentBase.PercentBaseSeries#setLabelsPosition
		 * @see com.anychart.percentBase.PercentBaseSeries#shiftPlotPoint
		 */
		public var labelsPlacementMode:uint;
		
		/**
		 * инвертирован график или нет
		 */
		public var inverted:Boolean;
		
		/**
		 * режим отбражения данных если да то ширина точки есть ее значение, если нет то ее высота естьее значение
		 */
		public var dataIsWidth:Boolean;
		
		
		/**
		 *соотношение между шириной и высотой 
		 */
		public var feet:Number;
		
		public function PercentBaseGlobalSeriesSettings():void {
			this.size3D = 0.09;
			this.padding = 0.02;
			this.minPointSize = 0.01;
			this.labelsPlacementMode = LabelsPlacementMode.OUTSIDE_LEFT;
			this.inverted = false;
			this.dataIsWidth = false;
			this.connnectorPadding = 5;
			this.feet = 0.6;
			
		}
		
		/**
		 * @see com.anychart.seriesPlot.data.settings.GlobalSeriesSettings
		 * missing точки добавлять в массив не нужно для того что бы в режиме dataIsWidht не пробегать
		 * массив до следующей не missing точки а просто брать значение из следующей 
		 */
		override public function addMissingPoints():Boolean{
			return false;
		}
		
		/**
		 * достаем настройки Cone, Funel, Pyramid сериЙ из XML
		 * 
		 * @param data  - XML c настрайками 
		 */
		override public function deserialize(data:XML):void{
			super.deserialize(data);
			if (data.@depth_3d != undefined) this.size3D = SerializerBase.getRatio(data.@depth_3d);
			if (data.@padding != undefined) this.padding = SerializerBase.getRatio(data.@padding);
			if (data.@min_point_size != undefined) this.minPointSize  = SerializerBase.getRatio(data.@min_point_size);
			if (SerializerBase.isEnabled(data.label_settings[0]) && data.label_settings[0].@placement_mode != undefined){
				switch (SerializerBase.getEnumItem(data.label_settings[0].@placement_mode)){
					case "outsideleftincolumn": this.labelsPlacementMode = LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN;;break;
					case "outsideleft": this.labelsPlacementMode = LabelsPlacementMode.OUTSIDE_LEFT;break;
					case "outsiderightincolumn": this.labelsPlacementMode = LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN;break;
					case "outsideright": this.labelsPlacementMode = LabelsPlacementMode.OUTSIDE_RIGHT;break;
					case "inside": this.labelsPlacementMode = LabelsPlacementMode.INSIDE;break;
				}
			}
			if (data.@fit_aspect != undefined) this.feet = SerializerBase.getNumber(data.@fit_aspect);
			if (data.@inverted != undefined) this.inverted = SerializerBase.getBoolean(data.@inverted);
			if (data.@data_is_width != undefined) this.dataIsWidth = SerializerBase.getBoolean(data.@data_is_width);
			if (SerializerBase.isEnabled(data.connector[0])) {
				this.connector = new Stroke();
				this.connector.deserialize(data.connector[0]);
				if (data.connector[0].@padding != undefined)
					this.connnectorPadding = SerializerBase.getNumber(data.connector[0].@padding);					
			}
		}
		
		/**
		 * @see com.anychart.seriesPlot.data.settings.GlobalSeriesSettings#getStyleStateClass
		 */
		override public function getStyleStateClass():Class{
			return BackgroundBaseStyleState;
		}
		
		/**
		 * @see com.anychart.seriesPlot.data.settings.GlobalSeriesSettings#createLabelElement
		 */
		override public function createLabelElement(isExtra:Boolean=false):LabelElement {
			return new CustomLabelElement();
		}
		
		override protected function get canSortPoints():Boolean { return true; }
		override protected function execSorting(points:Array, sortDescending:Boolean):void {
			var opts:Number = Array.NUMERIC;
			if (sortDescending) opts |= Array.DESCENDING;
				points.sortOn("y",opts);
		}
	}
}