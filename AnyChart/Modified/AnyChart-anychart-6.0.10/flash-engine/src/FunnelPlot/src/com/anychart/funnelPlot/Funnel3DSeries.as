package com.anychart.funnelPlot{
	
	import com.anychart.seriesPlot.data.BasePoint;
	
	/**
	 * Класс описывающий трехмерную серию
	 */
	public class Funnel3DSeries extends FunnelSeries{
		
		/**
		 * Обрабатываем точки перед рисованием
		 */
		override public function onBeforeDraw():void{
			var point:Funnel3DPoint;
			this.setRealSummHeight();
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var j:int = this.visiblePoints;
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var neckHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight;
			var minPointYValue:Number = Number.MAX_VALUE;
			  if (!inverted){
				this.currentY = 1-size3D*2;
			}else {
				this.currentY = 1;
			} 
			 if (neckHeight == 0 && baseWidth == 0)  this.currentY = 1; 
			
			for (var i:int = 0; i<j ; i++){
				point = this.points[i];
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
				inverted ? (this.plot.drawingPoints.unshift(point)) : (this.plot.drawingPoints.push(point));
				if (FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y<minPointYValue) minPointYValue = FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y;
			}
			
		 	 if(inverted){// если  график инвертирован инициализируем точки в обратном порядке.  
				for (i = 0; i<j; i++){
					point = this.points[i];
					point.initialize();
				}	
		 	}else{
				for (i = (j-1 );i>=0; i--){
					point = this.points[i];
					point.initialize();
				} 
			} 
			this.seriesBound.y -=minPointYValue/2; 
			  
			super.onBeforeDraw();
			
		} 
			
				
		
		/**
		 * Создаем точку
		 */
		override public function createPoint():BasePoint{
			return new Funnel3DPoint();
		}
		
		override public function addPointsToDrawing():Boolean{return false}
		
		/**
		 * Обрабатываем точки перед ресайзом
		 */
		override public function onBeforeResize():void{
			var point:Funnel3DPoint;
			this.setRealSummHeight();
			var padding:Number = FunnelGlobalSeriesSettings(this.global).padding;
			var dataIsWidth:Boolean = FunnelGlobalSeriesSettings(this.global).dataIsWidth;
			var inverted:Boolean = FunnelGlobalSeriesSettings(this.global).inverted;
			var minValue:Number = FunnelGlobalSeriesSettings(this.global).minPointSize * this.cashedTokens['YSum'];
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D;
			var baseWidth:Number = FunnelGlobalSeriesSettings(this.global).baseWidth;
			var neckHeight:Number = FunnelGlobalSeriesSettings(this.global).necHeight;
			var minPointYValue:Number = Number.MAX_VALUE;
			if (!inverted){
				this.currentY = 1-size3D*2;
			}else {
				this.currentY = 1;
			} 
			 if (neckHeight == 0 && baseWidth == 0)  this.currentY = 1; 
			var j:int = this.points.length;
			for (var i:int = 0; i<j ; i++){
				point = this.points[i];
				point.setSectorYValue(dataIsWidth,minValue,padding,this.realSummHeight,size3D);
				point.setSectorXValue();
				point.drawingInfo.setSectorPoint(point,inverted,this.getWidht());
				if (FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y<minPointYValue) minPointYValue = FunnelDrawingInfo(point.drawingInfo).centerTopBack.y-this.seriesBound.y;
			}
			
			this.seriesBound.y -=minPointYValue/2; 
			
			
			super.onBeforeDraw();
		}
	}
}