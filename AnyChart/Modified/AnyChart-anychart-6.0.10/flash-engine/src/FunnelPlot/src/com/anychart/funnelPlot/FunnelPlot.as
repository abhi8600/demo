package com.anychart.funnelPlot {
	
	import com.anychart.data.SeriesType;
	import com.anychart.percentBase.PercentBasePlot;
	import com.anychart.seriesPlot.templates.SeriesPlotTemplatesManager;
	import com.anychart.templates.BaseTemplatesManager;

	/**
	 * Класс описывает граффик типа Funnel
	 */
	public class FunnelPlot extends PercentBasePlot {
		
		//--------------------------------------------------------------------------
		//		Templates
		//--------------------------------------------------------------------------
		
		public static function getDefaultTemplate():XML {
			return PercentBasePlot.defaultTemplate;
		}
		
		
		override public function getTemplatesManager():BaseTemplatesManager {
			if (PercentBasePlot.templatesManager == null)
				PercentBasePlot.templatesManager = new SeriesPlotTemplatesManager();
			return PercentBasePlot.templatesManager;
		}
		
		public function FunnelPlot() {
			super();
			this.defaultSeriesType = SeriesType.FUNNEL;
		}
		
		override public function checkNoData(data:XML):Boolean {
			if (data.data[0] == null) return true;
			if (data.data[0].series[0] == null) return true;
			return false;
		}
		
	}
}