﻿package com.anychart.percentBase{
	import com.anychart.elements.styleStates.BaseElementStyleState;
	import com.anychart.funnelPlot.FunnelGlobalSeriesSettings;
	import com.anychart.labels.LabelsPlacementMode;
	import com.anychart.seriesPlot.data.BasePoint;
	import com.anychart.seriesPlot.data.BaseSingleValuePoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.visual.layout.HorizontalAlign;
	import com.anychart.visual.layout.VerticalAlign;
	import com.anychart.visual.stroke.Stroke;
	import com.anychart.visual.stroke.StrokeType;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * Класс описывающий базовую точку для Funnel Pyramid и Cone
	 * 
	 * @see  com.anychart.PercentBaseDrawingInfo
	 * @see  com.anychart.FunnelPlot
	 * @see  com.anychart.PyramidPlot
	 * @see  com.anychart.ConePlot
	 */
	public class PercentBasePoint extends BaseSingleValuePoint {
		
		/**
		 * нижняя ширина сектора в процентах
		 */
		public var bottomWidth:Number;
		
		/**
		 * верхняя ширина сектора в процентах
		 */
		public var topWidth:Number;
		
		/**
		 * высота нижней границы сектора в процентах
		 */
		public var bottomHeight:Number;
		
		/**
		 * высота верхней границы сектора в процентах
		 */
		public var topHeight:Number;
		
		/**
		 * экземпляр класа PercentBaseDrawingInfo, содержит в себе точки рисования и умеет их выставлять
		 */
		public var drawingInfo:PercentBaseDrawingInfo;
		
		/**
		 * высота данного сектора в прцентах от общей высоты.
		 */
		protected var percentHeight:Number; 
		
		//---------------------------------------
		//DRAWING
		//---------------------------------------
		
		/**
		 * @see com.anychart.seriesPlot.data.BasePoint#execDrawing
		 */
		override protected function execDrawing():void{
			super.execDrawing();
			this.container.filters = BackgroundBaseStyleState(this.styleState).effects !=null ? BackgroundBaseStyleState(this.styleState).effects.list : null; 
			if (!this.isMissing)
			this.drawSector();	
		}
		
		/**
		 * @see com.anychart.seriesPlot.data.BasePoint#redraw
		 */
		override protected function redraw():void{
			super.redraw();
			if (!this.isMissing)
				this.drawSector();
			this.container.filters = BackgroundBaseStyleState(this.styleState).effects !=null ? BackgroundBaseStyleState(this.styleState).effects.list : null; 
		}
		
		//---------------------------------------
		//ABSTRACT FUNCTIONS
		//---------------------------------------
		
		/**
		 * метод для рисования точки с заливкой, границами и т.д.
		 */
		protected function drawSector():void{}
		
		/**
		 * Рисуем коннекторы от лейбла до точки
		 */
		protected function drawConnectors():void {
			if (this.label != null && PercentBaseGlobalSeriesSettings(this.global).connector != null){
				var labelPlacement:uint = PercentBaseGlobalSeriesSettings(this.global).labelsPlacementMode;
				var g:Graphics = this.container.graphics;
				var moveToPosition:Point = new Point();
				var lineToPosition:Point = new Point();
				var centerPosition:Point = new Point();
				var labelRect:Rectangle = new Rectangle();
				var labelPos:Point = new Point();
				var connectorPadding:Number = PercentBaseGlobalSeriesSettings(this.global).connnectorPadding;
				labelRect = this.label.getBounds(this,this.label.style.normal,BasePoint.LABEL_INDEX);
				labelPos = this.getLabelPosition(BaseElementStyleState(this.label.style.normal),null,labelRect);
				if (labelPlacement != LabelsPlacementMode.INSIDE){
				 	switch  (labelPlacement){
				 		case LabelsPlacementMode.OUTSIDE_LEFT:
				 		case LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN:
				 			moveToPosition.x = (this.drawingInfo.leftTop.x + this.drawingInfo.leftBottom.x)/2;
				 			moveToPosition.y = (this.drawingInfo.leftTop.y + this.drawingInfo.leftBottom.y)/2;
				 			lineToPosition.x = labelPos.x + labelRect.width; 
				 			lineToPosition.y = labelPos.y + labelRect.height/2; 
				 			centerPosition.y = lineToPosition.y;
				 			centerPosition.x = lineToPosition.x+connectorPadding;
				 			break;
				 		case LabelsPlacementMode.OUTSIDE_RIGHT:
				 		case LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN:
				 			moveToPosition.x = (this.drawingInfo.rightTop.x + this.drawingInfo.rightBottom.x)/2;
				 			moveToPosition.y = (this.drawingInfo.rightTop.y + this.drawingInfo.rightBottom.y)/2;
				 			lineToPosition.x = labelPos.x;
				 			lineToPosition.y = labelPos.y + labelRect.height/2;

							centerPosition.y = lineToPosition.y;
				 			centerPosition.x = lineToPosition.x-connectorPadding;
				 			break;
				 	}
					
					var connector:Stroke = PercentBaseGlobalSeriesSettings(this.global).connector;
					
					if (connector.type == StrokeType.GRADIENT){
						var bounds:Rectangle = new Rectangle();
						bounds.x = Math.min(moveToPosition.x,lineToPosition.x);
						bounds.y = Math.min(moveToPosition.y,lineToPosition.y);
						bounds.width = Math.max(moveToPosition.x,lineToPosition.x) - Math.min(moveToPosition.x,lineToPosition.x);
						bounds.height = Math.max(moveToPosition.y,lineToPosition.y) - Math.min(moveToPosition.y,lineToPosition.y);
						connector.apply(g,bounds);
					}else connector.apply(g,null);
					
					g.moveTo(moveToPosition.x,moveToPosition.y);
					g.lineTo(centerPosition.x,centerPosition.y);
					g.lineTo(lineToPosition.x,lineToPosition.y);
					g.endFill();
					g.lineStyle();
				}
			}
		}
		
		/**
		 * метод для рисования линий точки
		 */
		protected function drawShape():void{}
		
		/**
		 * выставляем прямоугольник в котором рисутся точка
		 */
		protected function setBounds():void{
			var h:Number = PercentBaseSeries(this.series).getHeight();
			this.bounds.x = Math.min(this.drawingInfo.leftTop.x, this.drawingInfo.leftBottom.x);
			this.bounds.y = Math.min(this.drawingInfo.leftBottom.y, this.drawingInfo.leftBottom.y);
			this.bounds.width = Math.max(this.drawingInfo.rightTop.x - this.drawingInfo.leftTop.x,this.drawingInfo.rightBottom.x - this.drawingInfo.leftBottom.x);
			this.bounds.height = this.percentHeight*h;
		}
		
		/**
		 * выстваление X координат сектора.
		 */
		public function setSectorXValue (): void{
			this.bottomWidth = this.bottomHeight;
			this.topWidth = this.topHeight;	
		}
		
		//---------------------------------------
		//FILL
		//---------------------------------------
		
		/**
		 * Применяет Fill и LineStyle для сектора
		 */
		protected function getFill():void{
			if (BackgroundBaseStyleState(this.styleState).fill != null)
				BackgroundBaseStyleState(this.styleState).fill.begin(this.container.graphics, this.bounds, this.color);
			if (BackgroundBaseStyleState(this.styleState).stroke != null)
				BackgroundBaseStyleState(this.styleState).stroke.apply(this.container.graphics, this.bounds, this.color);
		} 
		
		/**
		 * Сбрасывает Fill и LineStyle для сектора
		 */
		protected function getEndFill():void{
			var g:Graphics = this.container.graphics;
			g.endFill();
			g.lineStyle();
		}
		//---------------------------------------
		//ANCHOR AND LABLES
		//---------------------------------------
		
		/**
		 * Получает позиции Лейбла у сектора 
		 */
		public function getLabelPosition(state: BaseElementStyleState, sprite: Sprite, bounds: Rectangle):Point{
			var placement:uint = PercentBaseGlobalSeriesSettings(this.global).labelsPlacementMode;
			var pos:Point = new Point();

			if (placement == LabelsPlacementMode.INSIDE){ return this.getElementPosition(state, sprite, bounds);}
			
			if (placement == LabelsPlacementMode.OUTSIDE_LEFT) {
				pos.x = (PercentBaseDrawingInfo(this.drawingInfo).leftTop.x + PercentBaseDrawingInfo(this.drawingInfo).leftBottom.x)/2;
				pos.y = (PercentBaseDrawingInfo(this.drawingInfo).leftTop.y + PercentBaseDrawingInfo(this.drawingInfo).leftBottom.y)/2;
				this.applyHAlign(pos, HorizontalAlign.LEFT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}else if (placement == LabelsPlacementMode.OUTSIDE_RIGHT) {
				pos.x = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.x + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.x)/2;
				pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}else if (placement == LabelsPlacementMode.OUTSIDE_RIGHT_IN_COLUMN && this.label != null){
				pos.x = PercentBaseSeries(this.series).seriesBound.x + PercentBaseSeries(this.series).getWidht() - this.label.getBounds(this, this.label.style.normal, LABEL_INDEX).width;
				pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				//this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}else if (placement == LabelsPlacementMode.OUTSIDE_LEFT_IN_COLUMN && this.label != null){
				pos.x = PercentBaseSeries(this.series).seriesBound.x;
				pos.y = (PercentBaseDrawingInfo(this.drawingInfo).rightTop.y + PercentBaseDrawingInfo(this.drawingInfo).rightBottom.y)/2;
				//this.applyHAlign(pos, HorizontalAlign.RIGHT, bounds, state.padding);
				this.applyVAlign(pos, VerticalAlign.CENTER, bounds, state.padding);
			}	
			return pos;
		}
		
		//---------------------------------------
		//SET Y VALUE
		//---------------------------------------
		
		public var calculationValue:Number;
		
		/**
		 * Выставляет У координаты верхней и нижней границы сектора в процентах
		 * 
		 * @param dataIsWidth режим отображения данных если true то данные отображаются как ширина, если false то как высоту
		 * @param minValue минимально возможное значение точки
		 * @param padding расстояние между точками
		 * @param realSummHeight сумма значений всех точек с учетом значений меньше минимального
		 * @param size3D глубина 3Д
		 */
		public function setSectorYValue(dataIsWidth:Boolean,minValue:Number,padding:Number,realSummHeight:Number,size3D:Number):void{
			
			/*если dataIsWidth выключен то проверяем значение точки 
			 если меньше минимального то высота точки равна отношению минимального значения к сумме
			 если нет то выота равна отношеню значения точки к сумме значений
			 если dataIsWidth включен высоты точек равны между собой и равны отношеню единицы к колличеству точек
			 
			 затем приводим высоту точки к реальному расстоянию оставшемуся для рисования точек
			 выставляем координату верхней границы по текушему значению currentY
			 уменьшаем текушее значение currentY на высоту точки
			 выставляем координату нижней границы по текушему значению currentY
			 уменьшаем текушее значение на padidng
			*/
			
			 if (!dataIsWidth){ 
				if (this.calculationValue<minValue) this.percentHeight = minValue/realSummHeight;
				else this.percentHeight = this.calculationValue/realSummHeight;
			}else this.percentHeight = 1/(PercentBaseSeries(this.series).points.length-1); 
			
			this.percentHeight*=this.getRealPercentHeight(padding,size3D);
			
			this.bottomHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY-=this.percentHeight;
			
			this.topHeight = PercentBaseSeries(this.series).currentY;
			PercentBaseSeries(this.series).currentY -= padding; 
		}
		
		/**
		 * Возвращает общую высоту в процентах, оставшуюся для рисования секторов.
		 * 
		 * @param padding расстояние между точками
		 * @param size3D глубина 3Д 
		 */
		protected function getRealPercentHeight(padding:Number,size3D:Number):Number{
			return (1 - (padding)*(PercentBaseSeries(this.series).points.length-1));
			// реальное расстояние оставшееся для рисования точек 
			//равно общая высота минус колличество отступов между точками 
			//тогда верхняя и нижняя точки не вылазят за пределы плота 
		}
		
		//---------------------------------------
		//Update
		//---------------------------------------
		
		override public function canAnimate(field:String):Boolean {
			return field == "y";
		}
		
		override public function getCurrentValuesAsObject():Object {
			return {
				y: this.y
			}
		}
		
		override public function updateValue(newValue:Object):void {
			if (newValue.y != undefined) this.y = newValue.y;
		}
	}
}