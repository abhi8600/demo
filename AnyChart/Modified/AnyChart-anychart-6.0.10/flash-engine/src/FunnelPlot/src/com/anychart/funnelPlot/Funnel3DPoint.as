package com.anychart.funnelPlot{
	import com.anychart.percentBase.PercentBaseSeries;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.utils.DrawingUtils;
	
	import flash.display.Graphics;
	
	
	
	/**
	 * Трехмерная точка типа Funnel
	 */
	public class Funnel3DPoint extends FunnelPoint{
		
		public function Funnel3DPoint():void {
			this.is3D = true;
			super();
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#drawSector
		 */
		override protected function drawSector():void{
			this.setBounds();
			this.drawShape();
			this.drawConnectors();
		}
		
		/**
		 * @see com.anychart.percentBase.PercentBasePoint#drawShape
		 */
		override protected function drawShape():void{
			FunnelDrawingInfo(this.drawingInfo).intialiseBounds();
			this.styleState.programmaticStyle.draw(this);
		}
		
		private function drawLine():void{
			var g:Graphics = this.container.graphics;
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			
			if (BackgroundBaseStyleState(this.styleState).stroke != null)
				BackgroundBaseStyleState(this.styleState).stroke.apply(this.container.graphics, this.bounds, this.color);
			
			g.moveTo(drawInfo.leftCenter.x,drawInfo.leftCenter.y);
			g.lineTo(drawInfo.centerCenterFront.x,drawInfo.centerCenterFront.y);
			g.lineTo(drawInfo.rightCenter.x,drawInfo.rightCenter.y);
			
			g.lineStyle();
		}
		
		/**
		 * рисуем переднюю сторону
		 */
		private function drawFrontSide():void{
			var g:Graphics = this.container.graphics;
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D*PercentBaseSeries(this.series).getHeight();
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			
			if (BackgroundBaseStyleState(this.styleState).fill != null)
				BackgroundBaseStyleState(this.styleState).fill.begin(this.container.graphics, this.bounds, this.color);
			if (BackgroundBaseStyleState(this.styleState).stroke != null)
				BackgroundBaseStyleState(this.styleState).stroke.apply(this.container.graphics, this.bounds, this.color);
			
			DrawingUtils.drawArc(g,(drawInfo.rightBottom.x + drawInfo.leftBottom.x)/2,drawInfo.leftBottom.y,0,180,drawInfo.bottomSize3D,(drawInfo.rightBottom.x - drawInfo.leftBottom.x)/2,0,true);
			g.lineTo(drawInfo.leftCenter.x,drawInfo.leftCenter.y);
			DrawingUtils.drawArc(g,(drawInfo.rightTop.x + drawInfo.leftTop.x)/2,drawInfo.leftTop.y,180,0,drawInfo.topSize3D,(drawInfo.rightTop.x - drawInfo.leftTop.x)/2,0,false);
			g.lineTo(drawInfo.rightCenter.x,FunnelDrawingInfo(this.drawingInfo).rightCenter.y);
			g.endFill();
			g.lineStyle();
			
			
			if (BackgroundBaseStyleState(this.styleState).hatchFill != null) {
				BackgroundBaseStyleState(this.styleState).hatchFill.beginFill(g,this.hatchType,this.color);
			
				DrawingUtils.drawArc(g,(drawInfo.rightBottom.x + drawInfo.leftBottom.x)/2,drawInfo.leftBottom.y,0,180,drawInfo.bottomSize3D,(drawInfo.rightBottom.x - drawInfo.leftBottom.x)/2,0,true);
				g.lineTo(drawInfo.leftCenter.x,drawInfo.leftCenter.y);
				DrawingUtils.drawArc(g,(drawInfo.rightTop.x + drawInfo.leftTop.x)/2,drawInfo.leftTop.y,180,0,drawInfo.topSize3D,(drawInfo.rightTop.x - drawInfo.leftTop.x)/2,0,false);
				g.lineTo(drawInfo.rightCenter.x,FunnelDrawingInfo(this.drawingInfo).rightCenter.y);
				
				g.endFill();
			}
		}
		
		/**
		 * Рисуем фронтальную сторону квадратного
		 */
		private function drawSquareFrontSide():void{
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.frontBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.frontBounds, this.color);

			drawInfo.drawFrontSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
			drawInfo.drawFrontSide(g,state);
			g.endFill();
			}
		}
		
		/**
		 * Рисуем верх 
		 */
		private function drawTopSide():void {
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D*PercentBaseSeries(this.series).getHeight();
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			
			if (BackgroundBaseStyleState(this.styleState).fill != null)
				BackgroundBaseStyleState(this.styleState).fill.begin(this.container.graphics, this.bounds, this.color);
			if (BackgroundBaseStyleState(this.styleState).stroke != null)
				BackgroundBaseStyleState(this.styleState).stroke.apply(this.container.graphics, this.bounds, this.color);

			this.container.graphics.drawEllipse(drawInfo.leftTop.x,drawInfo.leftTop.y-drawInfo.topSize3D,drawInfo.rightTop.x-drawInfo.leftTop.x,drawInfo.topSize3D*2);
			
			this.container.graphics.endFill();
			this.container.graphics.lineStyle();
		}
		
		/**
		 * Рисуем верх квадратного фуннеля
		 */
		private function drawSquareTopSide():void {
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.topBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.topBounds, this.color);
			drawInfo.drawTopSide(g,state);	
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				drawInfo.drawTopSide(g,state);
				g.endFill();
			}
		}
		
		private function drawSquareLeftSide():void {
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.leftBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g,drawInfo.leftBounds, this.color);

			drawInfo.drawLeftSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
			drawInfo.drawLeftSide(g,state);
			g.endFill();
			}
		}
		
		private function drawSquareRightSide():void {
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.rightBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.rightBounds, this.color);

			drawInfo.drawRightSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
			drawInfo.drawRightSide(g,state);
			g.endFill();
			}
		}
		
		private function drawSquareBackSide():void {
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.backBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.backBounds, this.color);

			drawInfo.drawBackSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
			drawInfo.drawBackSide(g,state);
			g.endFill();
			}
		}
		
		/**
		 * рисуем низ
		 */
		private function drawBottomSide():void{
			var size3D:Number = FunnelGlobalSeriesSettings(this.global).size3D*PercentBaseSeries(this.series).getHeight();
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			
			if (BackgroundBaseStyleState(this.styleState).fill != null)
				BackgroundBaseStyleState(this.styleState).fill.begin(this.container.graphics, this.bounds, this.color);
			if (BackgroundBaseStyleState(this.styleState).stroke != null)
				BackgroundBaseStyleState(this.styleState).stroke.apply(this.container.graphics, this.bounds, this.color);

			this.container.graphics.drawEllipse(drawInfo.leftBottom.x,drawInfo.leftBottom.y-drawInfo.bottomSize3D,drawInfo.rightBottom.x-drawInfo.leftBottom.x,drawInfo.bottomSize3D*2);
			
			this.container.graphics.endFill();
			this.container.graphics.lineStyle();
		}
		
		/**
		 * Рисуем низ квадратного фуннеля
		 */
		private function drawSquareBottomSide():void {
			var drawInfo:FunnelDrawingInfo = FunnelDrawingInfo(this.drawingInfo);
			var state:BackgroundBaseStyleState = BackgroundBaseStyleState(this.styleState);
			var g:Graphics = this.container.graphics;
			
			if (state.fill != null)
				state.fill.begin(g, drawInfo.bottomBounds, this.color);
			if (state.stroke != null)
				state.stroke.apply(g, drawInfo.bottomBounds, this.color);
				drawInfo.drawBottomSide(g,state);
			g.endFill();
			g.lineStyle();
			
			if(state.hatchFill != null){
				state.hatchFill.beginFill(g,this.hatchType,this.color);
				drawInfo.drawBottomSide(g,state);
				g.endFill();
			}
		}
		
	}
}