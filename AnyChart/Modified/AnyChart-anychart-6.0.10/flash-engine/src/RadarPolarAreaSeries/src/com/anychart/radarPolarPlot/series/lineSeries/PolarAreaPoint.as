package com.anychart.radarPolarPlot.series.lineSeries
{
	import com.anychart.radarPolarPlot.RadarPolarPlot;
	import com.anychart.radarPolarPlot.utils.PolarPoint;
	import com.anychart.styles.states.BackgroundBaseStyleState;
	import com.anychart.styles.states.StyleState;
	
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	internal class PolarAreaPoint extends PolarLinePoint
	{
		private var polarBottomEnd : PolarPoint;
		private var polarBottomStart : PolarPoint;
		
		public function PolarAreaPoint()
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
		}
		
		override protected function createDrawingPoints():void {
			//if (!this.needCreateDrawingPoints) return;
			var plot:RadarPolarPlot = RadarPolarPlot(this.plot);
			this.polarBottomEnd = new PolarPoint(plot.getCenter());
			this.polarBottomStart = new PolarPoint(plot.getCenter());
			var pt1:PolarPoint = new PolarPoint(plot.getCenter());
			var pt2:PolarPoint = new PolarPoint(pt1.getCenter());
			var t1:Point = new Point();
			var bottomValue:Number = LineSeries(series).valueAxisStart;
			this.drawingPoints = [];
			
			var k:Number=0;
			if (this.index == 0) k=this.series.points.length;
			else k=this.index;
			if (this.hasPrev) {
				this.series.points[k-1].initializePixValues();
				plot.transform(this.series.points[k-1].x, bottomValue, this.bottomStart)
				plot.transform(this.series.points[k-1].x,this.series.points[k-1].y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt1.r =Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt1.phi = plot.getXAxis().scale.transform(this.series.points[k-1].x) + plot.getYAxisCrossing();
				plot.transform(this.x,this.y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt2.r = Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt2.phi = plot.getXAxis().scale.transform(this.x) + plot.getYAxisCrossing();
				this.drawingPoints.push(this.getPointBetween(pt1, pt2));
			}
			else
				plot.transform(this.x, bottomValue, this.bottomStart)
			
			this.polarBottomStart.r = Math.sqrt( Math.pow(this.bottomStart.x - pt1.getCenter().x,2)+ Math.pow(this.bottomStart.y - pt1.getCenter().y,2));
			this.polarBottomStart.phi = plot.getXAxis().scale.transform(this.bottomStart.x) + plot.getYAxisCrossing();
			
			this.drawingPoints.push(pt2);
			
			if (this.index == this.series.points.length-1) k = -1;
			else k=this.index;
			if (this.hasNext) {
				this.series.points[k+1].initializePixValues();
				plot.transform(this.series.points[k+1].x, bottomValue, this.bottomEnd);
				plot.transform(this.series.points[k+1].x,this.series.points[k+1].y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt1.r =Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt1.phi = plot.getXAxis().scale.transform(this.series.points[k+1].x) + plot.getYAxisCrossing();
				plot.transform(this.x,this.y,t1);
				t1.x -= pt1.getCenter().x;
				t1.y -= pt1.getCenter().y;
				pt2.r = Math.sqrt( t1.x*t1.x + t1.y*t1.y);
				pt2.phi = plot.getXAxis().scale.transform(this.x) + plot.getYAxisCrossing();
				this.drawingPoints.push(this.getPointBetween(pt2, pt1));
			}
			else
				plot.transform(this.x, bottomValue, this.bottomEnd);
			
			this.polarBottomEnd.r = Math.sqrt( Math.pow(this.bottomEnd.x - pt1.getCenter().x,2)+ Math.pow(this.bottomEnd.y - pt1.getCenter().y,2));
			this.polarBottomEnd.phi = plot.getXAxis().scale.transform(this.bottomEnd.x) + plot.getYAxisCrossing();
			
			//this.needCreateDrawingPoints = false;
		}
		
		override protected function isGradientState(state:StyleState):Boolean {
			return BackgroundBaseStyleState(state).hasGradient();
		}
		
		override final protected function drawSegment(g:Graphics):void {
			
			var i:uint;
			var s:AreaStyleState = AreaStyleState(this.styleState);
			
			var gradientBounds:Rectangle = new Rectangle();
			if (LineSeries(this.series).needCreateGradientRect)
				gradientBounds = LineSeries(this.series).gradientBounds;
			
			if (s.stroke != null)
				s.stroke.apply(g, gradientBounds, this.color);
			if (s.fill != null)
				s.fill.begin(g, gradientBounds, this.color);
			g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
			for (i = 1;i<this.drawingPoints.length;i++) 
				lineTo(g,this.drawingPoints[i-1], this.drawingPoints[i]);
			g.lineStyle();
			
			if (this.previousStackPoint == null) {
				g.lineTo(this.polarBottomEnd.x,this.polarBottomEnd.y);
				g.lineTo(this.polarBottomStart.x,this.polarBottomStart.y);	
			}else {
				PolarAreaPoint(this.previousStackPoint).drawSegmentBackward(g);
			}
			g.endFill();
			
			if (s.hatchFill != null) {
				s.hatchFill.beginFill(g, this.hatchType, this.color);
				g.moveTo(this.drawingPoints[0].x, this.drawingPoints[0].y);
				for (i = 1;i<this.drawingPoints.length;i++) 
					lineTo(g,this.drawingPoints[i-1], this.drawingPoints[i]);
				if (this.previousStackPoint == null) {
					g.lineTo(this.polarBottomEnd.x,this.polarBottomEnd.y);
					g.lineTo(this.polarBottomStart.x,this.polarBottomStart.y);	
				}else {
					PolarAreaPoint(this.previousStackPoint).drawSegmentBackward(g);
				}
				g.endFill();
			}
		}
		
		private function drawSegmentBackward(g:Graphics):void {
			if (this.drawingPoints != null)
				for (var i:int = this.drawingPoints.length-1;i>=1;i--)
					lineTo(g, this.drawingPoints[i], this.drawingPoints[i-1]);
		}
	}
}