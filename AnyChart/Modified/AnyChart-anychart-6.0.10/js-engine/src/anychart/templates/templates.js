/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.templates.GlobalTemplatesContainer}</li>
 *  <li>@class {anychart.templates.BaseTemplatesMerger}.</li>
 * <ul>
 */

// Linking namespace
goog.provide('anychart.templates');

goog.require('anychart.templates.DEFAULT_TEMPLATE');

/**
 * Represents base type for all chart template mergers. Used in Chart
 * deserialization by its descendants.
 * @constructor
 */
anychart.templates.BaseTemplatesMerger = function() {

};

/**
 * Returns chart type-dependent chart node name. {String}
 */
anychart.templates.BaseTemplatesMerger.prototype.getBaseNodeName = goog.abstractMethod;

/**
 * Returns plot default template.
 *
 * @return {Object} Plot default template.
 */
anychart.templates.BaseTemplatesMerger.prototype.getPlotDefaultTemplate =
    function() {
        return null;
    };

/**
 * Merges passed templates and returns the result. Processes only common
 * subnodes. Should be overridden in descendants for more complex merging.
 *
 * @param {Object.<*>} source Source.
 * @param {Object.<*>} override Override template.
 * @return {Object.<*>} Merge result.
 *
 * // TODO: убрать положенный на детей хуй.
 */
anychart.templates.BaseTemplatesMerger.prototype.mergeTemplates = function(source, override) {
        var res = {'#name#': 'template'};
        anychart.utils.JSONUtils.mergeSimpleProperty(source, override, res);

        var nodeName = this.getBaseNodeName();
        var des = anychart.utils.deserialization;
        var chartBaseTemplate = des.getProp(source, nodeName);
        var chartOverrideTemplate = des.getProp(override, nodeName);

        res[nodeName] = {'#name#': nodeName, '#children#': []};
        this.mergeChartNode(chartBaseTemplate, chartOverrideTemplate,
                res[nodeName]);

        var defaults = anychart.utils.JSONUtils.merge(
                des.getProp(source, 'defaults'),
                des.getProp(override, 'defaults')
                );

        if (defaults != null)
            res['defaults'] = defaults;

        return res;
    };

/**
 * Merges passed template with passed chart. Processes only common subnodes.
 * Should be overridden in descendants for more complex merging.
 *
 * @param {Object.<*>} template Template.
 * @param {Object.<*>} chart Chart.
 * @return {Object.<*>} Merge result.
 *
 * // TODO: убрать положенный на детей хуй.
 */
anychart.templates.BaseTemplatesMerger.prototype.mergeWithChart = function(template, chart) {
        var nodeName = this.getBaseNodeName();
        var res = {'#name#': nodeName};
        var chartTemplate = anychart.utils.deserialization.getProp(template,
                nodeName);

        this.mergeChartNode(chartTemplate, chart, res);
        this.applyDefaults(res,
                anychart.utils.deserialization.getProp(template, 'defaults'));

        return res;
    };

/**
 * Processes base merging for chart node. Should be overridden in
 * descendants if they need any other <chart> subnodes to be merged.
 *
 * @protected
 * @param {Object.<*>} template Template.
 * @param {Object.<*>} chart Chart.
 * @param {Object.<*>} res Merge result.
 */
anychart.templates.BaseTemplatesMerger.prototype.mergeChartNode = function(template, chart, res) {
        anychart.utils.JSONUtils.mergeSimpleProperty(template, chart, res);

        var des = anychart.utils.deserialization;
        var templateChartSetting = des.getProp(template, 'chart_settings');
        var chartChartSettings = des.getProp(chart, 'chart_settings');

        var chartSettings = anychart.utils.JSONUtils.merge(
                templateChartSetting,
                chartChartSettings);

        if (chartSettings) res['chart_settings'] = chartSettings;

        var styles = anychart.utils.JSONUtils.mergeWithOverride(
                des.getProp(template, 'styles'),
                des.getProp(chart, 'styles'));

        if (styles != null) {
            res['styles'] = styles;
            for (var child in res['styles']) {
                if (des.hasProp(res['styles'][child], 'name'))
                    res['styles'][child]['name'] = des.getLString(
                            des.getProp(res['styles'][child], 'name'));
                if (des.hasProp(res['styles'][child], 'parent'))
                    res['styles'][child]['name'] = des.getLString(
                            des.getProp(res['styles'][child], 'parent'));
            }
        }

        if (templateChartSetting && des.hasProp(templateChartSetting,
                'controls'))
            var templateControls = des.getProp(templateChartSetting,
                    'controls');

        if (chartChartSettings && des.hasProp(templateChartSetting, 'controls'))
            var chartControls = des.getProp(chartChartSettings, 'controls');

        var controls = anychart.utils.JSONUtils.mergeWithOverride(
                templateControls, chartControls);
        if (controls && chartSettings) des.setProp(chartSettings, 'controls',
                controls);
    };

/**
 * Processes chart defaults applying. Protected method. Should be overridden in
 * descendants if they need any other defaults to be applied to a chart.
 *
 * @protected
 * @param {Object.<*>} res - output result.
 * @param {Object.<*>} defaults
 *
 * //TODO: раскомментировать мерджинг, когда он станет актуальным.
 */
anychart.templates.BaseTemplatesMerger.prototype.applyDefaults = function(res, defaults) {
        if (!res || !defaults) return;
        var des = anychart.utils.deserialization;

        var defaultLegend = des.getProp(defaults, 'legend');
        var settings = des.getProp(res, 'chart_settings');

        if (defaultLegend && settings) {
            //legend
            if (des.hasProp(settings, 'legend'))
                des.setProp(settings,
                        'legend',
                        anychart.utils.JSONUtils.merge(
                                defaultLegend,
                                des.getProp(settings, 'legend'),
                                'legend'));

            //old xml support
            if (des.hasProp(settings, 'controls')) {
                var controls = des.getProp(settings, 'controls');
                var legendsData = des.getPropArray(controls, 'legend');
                var legendsCount = legendsData.length;
                for (var i = 0; i < legendsCount; i++) {
                    legendsData[i] = anychart.utils.JSONUtils.merge(
                            defaultLegend,
                            legendsData[i],
                            'legend'
                            );
                }
                des.setProp(controls, 'legend', legendsData);
            }
        }
    };

/**
 * Global templates manager class.
 *
 * @constructor
 */
anychart.templates.GlobalTemplatesContainer = function() {
    this.templates_ = {};
};

/**
 * Templates hash-mapped by name
 *
 * @private
 * @type {Object.<object>} - JSON objects hashed by name
 */
anychart.templates.GlobalTemplatesContainer.prototype.templates_ = null;

/**
 * Adds passed templates to templates storage.
 *
 * @param {Array.<object>} templatesArray Array of templates.
 */
anychart.templates.GlobalTemplatesContainer.prototype.addTemplates =
    function(templatesArray) {
        for (var i = 0, len = templatesArray.length; i < len; i++)
            this.addTemplate_(templatesArray[i]);
    };

/**
 * Adds specified template to templates storage
 *
 * @private
 * @param {Object} template - JSON object.
 */
anychart.templates.GlobalTemplatesContainer.prototype.addTemplate_ =
    function(template) {
        var name = anychart.utils.deserialization.getLString(
                anychart.utils.deserialization.getProp(template, 'name'));
        if (name != '')
            this.templates_[name] = template;
    };

/**
 * Gets template by name.
 *
 * @param {String} name Name.
 * @return {Object} Template.
 */
anychart.templates.GlobalTemplatesContainer.prototype.getTemplate =
    function(name) {
        return (this.templates_[name]) ?
                anychart.utils.deserialization.copy(this.templates_[name]) :
                null;
    };

/**
 * Gets template by name, also retrieves all inheritance chain. (i+1)-th element
 * is parent for i-th element.
 *
 * @param {String} name Name.
 * @return {Array.<Object>} - array of templates with inheritance chain.
 */
anychart.templates.GlobalTemplatesContainer.prototype.getTemplatesChain =
    function(name) {
        var res = [];
        var usedTemplates = {};
        var currTemplate = this.getTemplate(name);
        var currName = name;
        while (currTemplate != null) {
            res.push(currTemplate);
            usedTemplates[currName] = true;
            currName = anychart.utils.deserialization.getLString(
                    anychart.utils.deserialization.getProp(currTemplate,
                            'parent'));
            if (usedTemplates[currName] != undefined)
                break;
            currTemplate = this.getTemplate(currName);
        }
        return res;
    };
//------------------------------------------------------------------------------
//                  Clear.
//------------------------------------------------------------------------------
/**
 * Clear templates container.
 */
anychart.templates.GlobalTemplatesContainer.prototype.clear = function() {
    this.templates_ = {};
};
