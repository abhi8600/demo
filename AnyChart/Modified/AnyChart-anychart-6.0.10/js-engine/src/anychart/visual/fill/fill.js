/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.fill.Fill}</li>
 *     <li>@class {anychart.visual.fill.Fill.FillType}.</li>
 * </ul>
 */

goog.provide('anychart.visual.fill');

goog.require('anychart.utils');
goog.require('anychart.visual.color');
goog.require('anychart.visual.gradient');
goog.require('anychart.visual.image');

/**
 * Fill class instance constructor.
 * The class should be used in the way like this:
 * <code>
 *  var fill = new anychart.visual.fill.Fill();
 *  fill.deserialize(fillSettingsJSON);
 *  var ctx = canvas.getContext('2d');
 *  ctx.beginPath();
 *  // do some path drawing
 *  ctx.closePath();
 *  var bounds = new anychart.utils.geom.Rectangle(...); // bounds of the path,
 *  required for gradient filling.
 *  fill.apply(ctx, bounds);
 * </code>
 *
 * @constructor
 */
anychart.visual.fill.Fill = function() {};

/**
 * Describes possible fill types.
 *
 * @enum {int}
 */
anychart.visual.fill.Fill.FillType = {
    SOLID: 0,
    GRADIENT: 1,
    IMAGE: 2
};
/**
 * Indicates whether the fill should be applied.
 *
 * @private
 * @type {boolean}
 */
anychart.visual.fill.Fill.prototype.enabled_ = true;

/**
 * Gets state of fill.
 * @return {Boolean} Is fill should be applied.
 */
anychart.visual.fill.Fill.prototype.isEnabled = function() {
    return this.enabled_;
};

/**
 * Sets the state for fill.
 * @param {Boolean} value Value.
 */
anychart.visual.fill.Fill.prototype.setEnabled = function(value) {
    this.enabled_ = value;
};

/**
 * Defines filling method (possible values are members of
 * anychart.visual.fill.Fill.FillType enumeration)
 *
 * @private
 * @type {int}
 */
anychart.visual.fill.Fill.prototype.type_ =
        anychart.visual.fill.Fill.FillType.SOLID;

/**
 * Is the type of fill = gradient.
 * @return {Boolean} Boolean.
 */
anychart.visual.fill.Fill.prototype.isGradient = function() {
    return this.type_ == anychart.visual.fill.Fill.FillType.GRADIENT;
};

/**
 * Defines filling color for solid fill only.
 *
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.fill.Fill.prototype.color_ =
        anychart.utils.deserialization.getColor('#EEEEEE', 1);

/**
 * Gets the color.
 * @return {anychart.visual.color.ColorContainer} Color container instance.
 */
anychart.visual.fill.Fill.prototype.getColor = function() {
    return this.color_;
};

/**
 * Defines color opacity.
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.fill.Fill.prototype.opacity_ = 1;

/**
 * Gets the opacity.
 * @return {Number} Opacity.
 */
anychart.visual.fill.Fill.prototype.getOpacity = function() {
    return this.opacity_;
};


/**
 * Defines the image 
 *
 * @private
 * @type {anychart.visual.image.Image}
 */
anychart.visual.fill.Fill.prototype.image_ = null;

/**
 * Describes gradient settings if there.
 *
 * @private
 * @type {anychart.visual.gradient.Gradient}
 */
anychart.visual.fill.Fill.prototype.gradient_ = null;

/**
 * Gets the gradient.
 * @return {anychart.visual.gradient.Gradient} Gradient.
 */
anychart.visual.fill.Fill.prototype.getGradient = function() {
    return this.gradient_;
};

/**
 * Deserializes fill settings from JSON object.
 * Expects the next JSON structure:
 * <code>
 *  {
 *      enabled: boolean,
 *      type: string, // "solid"|"gradient", "solid" by def
 *      opacity: number, // [0..1], values that are not in these bounds are set
 *      to the closest bound
 *      color: string, // color string, parsed by colorParser
 *      gradient: object // gradient settings object
 *  }
 * </code>
 *
 * @param {object} data JSON object.
 */
anychart.visual.fill.Fill.prototype.deserialize = function(data) {
    // aliases
    var deserialization = anychart.utils.deserialization;

    // @enabled deserialization
    this.enabled_ = deserialization.isEnabled(data);
    if (!this.enabled_) return;

    // deserializing fill type
    if (deserialization.hasProp(data, 'type')) {
        switch (deserialization.getEnumItem(
                deserialization.getProp(data, 'type'))) {
            case 'solid':
                this.type_ = anychart.visual.fill.Fill.FillType.SOLID;
                break;
            case 'gradient':
                this.type_ = anychart.visual.fill.Fill.FillType.GRADIENT;
                break;
            case 'image':
                this.type_ = anychart.visual.fill.Fill.FillType.IMAGE;
                break;
        }
    }

    // deserializing fill opacity
    if (deserialization.hasProp(data, 'opacity')) {
        this.opacity_ = deserialization.getRatio(
                deserialization.getProp(data, 'opacity'));
    }

    // deserializing fill solid color (opacity is counted on implicitly)
    if (deserialization.hasProp(data, 'color')) {
        this.color_ = deserialization.getColor(
                deserialization.getProp(data, 'color'), this.opacity_);
    }

    // deserializing image if needed
    if (this.type_ == anychart.visual.fill.Fill.FillType.IMAGE) {
        var imageUrl, imageMode;
        if (deserialization.hasProp(data, 'image_url')) {
            imageUrl = deserialization.getProp(data, 'image_url');
        }
        if (deserialization.hasProp(data, 'image_mode')) {
            switch (deserialization.getEnumItem(
                deserialization.getProp(data, 'image_mode'))) {
                case 'stretch':
                    imageMode = anychart.visual.image.ImageModeType.STRETCH;
                    break;
                case 'tile':
                    imageMode = anychart.visual.image.ImageModeType.TILE;
                    break;
                case 'fitbyproportions':
                    imageMode = anychart.visual.image.ImageModeType.FIT_BY_PROPORTIONS;
                    break;
            }
        }
        this.image_ = new anychart.visual.image.Image(imageUrl, imageMode);
    }
    // deserializing gradient_ if needed
    if (this.type_ == anychart.visual.fill.Fill.FillType.GRADIENT &&
            deserialization.hasProp(data, 'gradient')) {
        this.gradient_ = new anychart.visual.gradient.Gradient(this.opacity_);
        this.gradient_.deserialize(deserialization.getProp(data, 'gradient'));
    }

    // fixing fill type if no image_ settings supplied
    if (this.type_ == anychart.visual.fill.Fill.FillType.IMAGE && (!this.image_ || !this.image_.getImageUrl())) {
        this.type_ = anychart.visual.fill.Fill.FillType.SOLID;
    }
    
    // fixing fill type if no gradient_ settings supplied
    if (this.type_ == anychart.visual.fill.Fill.FillType.GRADIENT &&
            (this.gradient_ == null || this.gradient_.keys_ == null))
        this.type_ = anychart.visual.fill.Fill.FillType.SOLID;
};

/**
 * Gets fill attribute string.
 * @param {anychart.svg.SVGManager} svgManager svg manager.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {anychart.visual.color.Color} opt_color Color.
 * @param {Boolean} opt_userSpaceOnUse userSpaceOnUse.
 * @return {String} Fill attribute string.
 */
anychart.visual.fill.Fill.prototype.getSVGFill = function(svgManager,
                                                          bounds, opt_color, opt_userSpaceOnUse) {
    if (!this.enabled_) return '';
    
    switch (this.type_) {
        case anychart.visual.fill.Fill.FillType.SOLID:
            return 'fill:' + this.color_.getColor(opt_color).toRGB() +
                    ';fill-opacity:' + this.opacity_ + ';';
        case anychart.visual.fill.Fill.FillType.GRADIENT:
            this.gradient_.prepare(bounds, opt_color);
            return 'fill:' + svgManager.getGradientManager().
                getSVGGradient(this.gradient_, opt_userSpaceOnUse) + ';';
        case anychart.visual.fill.Fill.FillType.IMAGE:
            return 'fill:' + svgManager.getImageManager().getSVGImage(this.image_, bounds) + ';';
    }
};

/**
 * Identify whether fill-type is solid or gradient and returns color or gradient
 * id respectively.
 * @param {anychart.svg.SVGManager} svgManager svg manager.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {anychart.visual.color.Color} opt_color Color.
 * @return {String} Color or gradient id.
 */
anychart.visual.fill.Fill.prototype.getSVGFillColorOrGradinetId =
        function(svgManager, bounds, opt_color) {
            switch (this.type_) {
                case anychart.visual.fill.Fill.FillType.SOLID:
                    return this.color_.getColor(opt_color).toRGB() + ';';
                case anychart.visual.fill.Fill.FillType.GRADIENT:
                    this.gradient_.prepare(bounds, opt_color);
                    return svgManager.getGradientManager().
                            getSVGGradient(this.gradient_) + ';';
            }
        };
