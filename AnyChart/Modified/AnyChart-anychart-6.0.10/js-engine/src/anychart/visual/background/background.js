/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.background.SVGBackgroundSprite}</li>
 *     <li>@class {anychart.visual.background.Background}</li>
 *     <li>@class {anychart.visual.background.ShapeBackground}</li>
 *     <li>@class {anychart.visual.background.RectBackground}</li>
 * </ul>.
 */

goog.provide('anychart.visual.background');

goog.require('anychart.layout');
goog.require('anychart.svg');
goog.require('anychart.utils');
goog.require('anychart.visual.corners');
goog.require('anychart.visual.fill');
goog.require('anychart.visual.hatchFill');
goog.require('anychart.visual.stroke');
//------------------------------------------------------------------------------
//
//                          SVGBackgroundSprite class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager Anychart svg manager.
 * @param {SVGElement} fillSegment Fill segment.
 * @param {SVGElement} hatchFillSegment HatchFill segment.
 */
anychart.visual.background.SVGBackgroundSprite = function(svgManager, fillSegment, hatchFillSegment) {
    anychart.svg.SVGSprite.call(this, svgManager);

    this.fillSegment_ = fillSegment;
    this.hatchFillSegment_ = hatchFillSegment;

    this.appendChild(fillSegment);
    this.appendChild(hatchFillSegment);
};
goog.inherits(anychart.visual.background.SVGBackgroundSprite, anychart.svg.SVGSprite);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Fill segment.
 * @private
 * @type {SVGElement}
 */
anychart.visual.background.SVGBackgroundSprite.prototype.fillSegment_ = null;
/**
 * Getter for fill segment.
 * @return {SVGElement} Fill segment SVGElement.
 */
anychart.visual.background.SVGBackgroundSprite.prototype.getFillSegment = function() {
    return this.fillSegment_;
};
/**
 * Hatchfill segment.
 * @private
 * @type {SVGElement}
 */
anychart.visual.background.SVGBackgroundSprite.prototype.hatchFillSegment_ = null;

/**
 * Getter for hatchfill segment.
 * @return {SVGElement} Fill segment SVGElement.
 */
anychart.visual.background.SVGBackgroundSprite.prototype.getHatchFillSegment = function() {
    return this.hatchFillSegment_;
};
//------------------------------------------------------------------------------
//
//                          Background class
//
//------------------------------------------------------------------------------
/**
 * Class represent base background settings.
 * @constructor
 */
anychart.visual.background.Background = function() {
};

/**
 * Border settings.
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.visual.background.Background.prototype.border_ = null;

/**
 * Return background border settings.
 * @return {anychart.visual.stroke.Stroke} Border settings.
 */
anychart.visual.background.Background.prototype.getBorder = function() {
    return this.border_;
};

/**
 * Return have the background border or not.
 * @return {boolean}
 */
anychart.visual.background.Background.prototype.hasBorder = function() {
    return this.border_ && this.border_.isEnabled();
};

/**
 * Fill settings.
 * @private
 * @type {anychart.visual.fill.Fill}
 */
anychart.visual.background.Background.prototype.fill_ = null;

/**
 * Return background fill settings.
 * @return {anychart.visual.fill.Fill} Background fill settings.
 */
anychart.visual.background.Background.prototype.getFill = function() {
    return this.fill_;
};

/**
 * @param {anychart.visual.fill.Fill} fill
 */
anychart.visual.background.Background.prototype.setFill = function(fill) {
    this.fill_ = fill;
};

/**
 * Return has background fill or not.
 * @return {boolean}
 */
anychart.visual.background.Background.prototype.hasFill = function() {
    return this.fill_ && this.fill_.isEnabled();
};

/**
 * Hatch fill settings.
 * @private
 * @type {anychart.visual.hatchFill.HatchFill}
 */
anychart.visual.background.Background.prototype.hatchFill_ = null;

/**
 * Return background hatch fill settings.
 * @return {anychart.visual.hatchFill.HatchFill} Background hatchfill settings.
 */
anychart.visual.background.Background.prototype.getHatchFill = function() {
    return this.hatchFill_;
};

/**
 * @param {anychart.visual.hatchFill.HatchFill} hatchFill
 */
anychart.visual.background.Background.prototype.setHatchFill = function(hatchFill) {
    this.hatchFill_ = hatchFill;
};

/**
 * Return has background hatch fill or not.
 * @return {boolean}
 */
anychart.visual.background.Background.prototype.hasHatchFill = function() {
    return this.hatchFill_ && this.hatchFill_.isEnabled();
};

/**
 * Indicates whether the background settings should be applied.
 * @private
 * @type {boolean}
 */
anychart.visual.background.Background.prototype.enabled_ = true;

/**
 * Indicates whether the background settings should be applied.
 * @return {boolean} Is background settings should be applied.
 */
anychart.visual.background.Background.prototype.isEnabled = function() {
    return this.enabled_;
};

/**
 * Sets whether the background settings should be applied.
 * @param {boolean} value Boolean.
 */
anychart.visual.background.Background.prototype.setEnabled = function(value) {
    this.enabled_ = value;
};
/**
 * @private
 * @type {anychart.visual.effects.EffectsList}
 */
anychart.visual.background.Background.prototype.effects_ = null;
/**
 * @return {anychart.visual.effects.EffectsList}
 */
anychart.visual.background.Background.prototype.getEffects = function() {
    return this.effects_;
};
/**
 * @return {Boolean}
 */
anychart.visual.background.Background.prototype.hasEffects = function() {
    return Boolean(this.effects_ && this.effects_.isEnabled());
};
/**
 * Deserialize background settings from JSON object.
 * @param {Object} data JSON object with base background
 * settings.
 * <p>
 * Sample:
 * <code>
 *  var bg = new anychart.visual.background.Background();
 *  bg.deserialize();
 * </code>
 * </p>
 * <p>
 * Expected JSON structure:
 * <code>
 * object{
 *  fill : object //Deserialize by {anychart.visual.fill.Fill}
 *  border : object //Deserialize by {anychart.visual.stroke.Stroke}
 *  hatch_fill : object //Deserialize by {anychart.visual.hatchFill.HatchFill}
 * }.
 * </code>
 * </p>
 */
anychart.visual.background.Background.prototype.deserialize = function(data) {
    //aliases
    var des = anychart.utils.deserialization;

    /**enabled deserialization*/
    this.enabled_ = des.isEnabled(data);
    if (!this.enabled_) return;

    //deserializing fill
    if (des.isEnabledProp(data, 'fill')) {
        this.fill_ = new anychart.visual.fill.Fill();
        this.fill_.deserialize(des.getProp(data, 'fill'));
        if (!this.fill_.isEnabled()) this.fill_ = false;
    }

    //deserializing border
    if (des.isEnabledProp(data, 'border')) {
        this.border_ = new anychart.visual.stroke.Stroke();
        this.border_.deserialize(des.getProp(data, 'border'));
        if (!this.border_.isEnabled()) this.border_ = false;
    }

    //deserializing hatch fill
    if (des.isEnabledProp(data, 'hatch_fill')) {
        this.hatchFill_ = new anychart.visual.hatchFill.HatchFill();
        this.hatchFill_.deserialize(des.getProp(data, 'hatch_fill'));
        if (!this.hatchFill_.isEnabled()) this.hatchFill_ = false;
    }

    //deserializing effects
    if (des.isEnabledProp(data, 'effects')) {
        this.effects_ = new anychart.visual.effects.EffectsList();
        this.effects_.deserialize(des.getProp(data, 'effects'));
        if (!this.effects_.isEnabled()) this.effects_ = false;
    }
};
//------------------------------------------------------------------------------
//
//                          ShapeBackground class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.visual.background.Background}
 */
anychart.visual.background.ShapeBackground = function() {
    anychart.visual.background.Background.call(this);
};
goog.inherits(anychart.visual.background.ShapeBackground,
    anychart.visual.background.Background);
/**
 * Sprite represents background.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.visual.background.ShapeBackground.prototype.sprite_ = null;
anychart.visual.background.ShapeBackground.prototype.getSprite = function() {
    return this.sprite_;
};

//------------------------------------------------------------------------------
//                       Background creation.
//------------------------------------------------------------------------------
/**
 * Create background sprite.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @return {anychart.visual.background.SVGBackgroundSprite} Sprite element.
 */
anychart.visual.background.ShapeBackground.prototype.initialize = function(svgManager) {
    if (!this.enabled_) return null;

    var fillSegment = this.createElement(svgManager);
    var hatchFillSegment = this.createElement(svgManager);

    this.sprite_ = new anychart.visual.background.SVGBackgroundSprite(
        svgManager,
        fillSegment,
        hatchFillSegment);
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Background drawing.
//------------------------------------------------------------------------------
/**
 * Draw background into passed sprite.
 * @param {anychart.visual.background.SVGBackgroundSprite} sprite Sprite.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds settings.
 * @param {anychart.visual.color.Color} opt_color Color.
 */
anychart.visual.background.ShapeBackground.prototype.draw = function(sprite, bounds, opt_color) {
    var svgManager = sprite.getSVGManager();

    //effects
    var filter;
    if (this.effects_ && this.effects_.isEnabled())
        filter = this.effects_.createEffects(svgManager);

    var segmentData = this.getElementPath(svgManager, bounds);
    this.drawFill_(sprite.getFillSegment(), segmentData, sprite, bounds, opt_color, filter);
    this.drawHatchFill_(sprite.getHatchFillSegment(), segmentData, sprite, filter);
};

/**
 * Updates fill of segment.
 * @private
 * @param {SVGElement} segment Segment.
 * @param {String} segmentData Data of segment.
 * @param {anychart.visual.background.SVGBackgroundSprite} sprite Sprite.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds settings.
 * @param {anychart.visual.color.Color} opt_color Color.
 * @param {String} opt_filter Filter
 */
anychart.visual.background.ShapeBackground.prototype.drawFill_ = function(segment, segmentData, sprite, bounds, opt_color, opt_filter) {
    var svgManager = sprite.getSVGManager();
    var style = '';

    //update fill
    if (this.fill_ && this.fill_.isEnabled())
        style += this.fill_.getSVGFill(svgManager, bounds, opt_color);
    else
        style += svgManager.getEmptySVGFill();


    if (this.border_ && this.border_.isEnabled())
        style += this.border_.getSVGStroke(svgManager, bounds, opt_color);
    else
        style += svgManager.getEmptySVGStroke();

    this.execDraw_(segment, segmentData, style, opt_filter);
};

/**
 * Updates hatchfill of segment.
 * @private
 * @param {SVGElement} segment Segment.
 * @param {String} segmentData Data of segment.
 * @param {anychart.visual.background.SVGBackgroundSprite} sprite Sprite.
 * @param {String} opt_filter Filter.
 */
anychart.visual.background.ShapeBackground.prototype.drawHatchFill_ = function(segment, segmentData, sprite, opt_filter) {
    var style = '';
    if (this.hatchFill_ && this.hatchFill_.isEnabled())
        style = this.hatchFill_.getSVGHatchFill(sprite.getSVGManager());
    else style = sprite.getSVGManager().getEmptySVGStyle();

    this.execDraw_(segment, segmentData, style, opt_filter);
};

/**
 * Sets attributes to SVGElement
 * @private
 * @param {SVGElement} segment Segment.
 * @param {String} segmentData Data of segment.
 * @param {String} style Style of segment.
 * @param {String} opt_filter Filter.
 */
anychart.visual.background.ShapeBackground.prototype.execDraw_ = function(segment, segmentData, style, opt_filter) {
    if (opt_filter) segment.setAttribute('filter', opt_filter);
    else segment.removeAttribute('filter');

    segment.setAttribute('d', segmentData);
    segment.setAttribute('style', style);
};
/**
 * Resize background.
 * @param {anychart.visual.background.SVGBackgroundSprite} sprite Background
 * sprite.
 * @param {anychart.utils.geom.Rectangle} bounds New background bounds.
 */
anychart.visual.background.ShapeBackground.prototype.resize = function(sprite, bounds) {
    var path = this.getElementPath(sprite.getSVGManager(), bounds);
    sprite.getFillSegment().setAttribute('d', path);
    sprite.getHatchFillSegment().setAttribute('d', path);
};
//------------------------------------------------------------------------------
//                         Background element
//------------------------------------------------------------------------------
/**
 * Create element for background, it should be element with changeable
 * property 'd' (SVGPathElement for example).
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @return {SVGElement}
 */
anychart.visual.background.ShapeBackground.prototype.createElement = goog.abstractMethod;
/**
 * Return background element path data.
 * @protected
 * @param {anychart.svg.SVGManager} svgManager
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {String}
 */
anychart.visual.background.ShapeBackground.prototype.getElementPath = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                          RectBackground class
//
//------------------------------------------------------------------------------
/**
 * RectBackground settings.
 * @extends {anychart.visual.background.Background}
 * @constructor
 * <p>
 * Sample:
 *  <code>
 *      var background = new anychart.visual.background.RectBackground();
 *      background.deserialize(data);
 *      var backgroundDrawer = background.createDrawer(bounds);
 *  </code>
 * </p>
 */
anychart.visual.background.RectBackground = function() {
    anychart.visual.background.ShapeBackground.apply(this);
};
goog.inherits(anychart.visual.background.RectBackground, anychart.visual.background.ShapeBackground);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * Corners settings.
 * @private
 * @type {anychart.visual.corners.Corners}
 */
anychart.visual.background.RectBackground.prototype.corners_ = null;

/**
 * Getter for corners settings.
 * @return {anychart.visual.corners.Corners} Corners settings.
 */
anychart.visual.background.RectBackground.prototype.getCorners = function() {
    return this.corners_;
};

/**
 * Sets corners settings.
 * @param {anychart.visual.corners.Corners} value Corners settings.
 */
anychart.visual.background.RectBackground.prototype.setCorners = function(value) {
    this.corners_ = value;
};

/**
 * Margin settings.
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.visual.background.RectBackground.prototype.margin_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.visual.background.RectBackground.prototype.applyInsideMargin = function(bounds) {
    if (this.margin_) this.margin_.applyInside(bounds);
};

/**
 * Return background margin settings.
 * @return {anychart.layout.Margin} Margin settings.
 */
anychart.visual.background.RectBackground.prototype.getMargin = function() {
    return this.margin_;
};
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize rectBackground settings.
 * @override
 * @param {Object} data JSON object.
 * <p>
 * Sample:
 *  <code>
 *      var background = new anychart.visual.background.RectBackground();
 *      background.deserialize(data);
 *  </code>
 * </p>
 * Expected JSON structure:
 * <code>
 * object{
 *  fill : object //Deserialize by {anychart.visual.fill>Fill}
 *  border : object //Deserialize by {anychart.visual.stroke.Stroke}
 *  hatch_fill : object //Deserialize by {anychart.visual.hatchFill.HatchFill}
 *  corners : object //Deserialize by {anychart.visual.corners.Corners}
 * }.
 * </code>
 * <p>
 */
anychart.visual.background.RectBackground.prototype.deserialize =
    function(data) {
        goog.base(this, 'deserialize', data);
        var deserialization = anychart.utils.deserialization;

        if (!this.isEnabled()) return;
        if (deserialization.hasProp(data, 'corners')) {
            this.corners_ = new anychart.visual.corners.Corners();
            this.corners_.deserialize(deserialization.getProp(data, 'corners'));
        }
        if (deserialization.hasProp(data, 'inside_margin')) {
            this.margin_ = new anychart.layout.Margin();
            this.margin_.deserialize(deserialization.getProp(data, 'inside_margin'));
        }
    };
//------------------------------------------------------------------------------
//                          Background element.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.visual.background.RectBackground.prototype.createElement = function(svgManager) {
    return svgManager.createPath();
};
/**@inheritDoc*/
anychart.visual.background.RectBackground.prototype.getElementPath = function(svgManager, bounds) {
    if (this.corners_) return this.corners_.createSVGPolygonData(svgManager, bounds);
    var points = svgManager.pMove(bounds.x, bounds.y);
    points += svgManager.pLine(bounds.x + bounds.width, bounds.y);
    points += svgManager.pLine(bounds.x + bounds.width, bounds.y + bounds.height);
    points += svgManager.pLine(bounds.x, bounds.y + bounds.height);
    points += svgManager.pLine(bounds.x, bounds.y);
    points += svgManager.pFinalize();
    return points;
};
