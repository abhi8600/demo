/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.stroke.Stroke}</li>
 *     <li>@class {anychart.visual.stroke.Stroke.StrokeType}</li>
 *     <li>@class {anychart.visual.stroke.Stroke.CapsType}</li>
 *     <li>@class {anychart.visual.stroke.Stroke.JoinType}</li>
 *     <li>@class {anychart.visual.hatchFill.SVGHatchFillManager}</li>
 * </ul>.
 */

goog.provide('anychart.visual.stroke');

goog.require('anychart.utils');
goog.require('anychart.visual.color');
goog.require('anychart.visual.gradient');
//------------------------------------------------------------------------------
//
//                          Constructor
//
//------------------------------------------------------------------------------
/**
 * Defines Stroke class.
 * The class should be used in the next way:
 * <code>
 *  var stroke = new anychart.visual.stroke.Stroke();
 *  stroke.deserialize(strokeSettingsJSON);
 *  var ctx = canvas.getContext('2d');
 *  ctx.beginPath();
 *  // do some path drawing
 *  ctx.closePath();
 *  var bounds = new anychart.utils.geom.Rectangle(...); // bounds of the path,
 *  required for gradient filling
 *  stroke.applySettings(ctx, bounds);
 * </code>
 *
 * @constructor
 */
anychart.visual.stroke.Stroke = function () {
};
//------------------------------------------------------------------------------
//
//                          StrokeType
//
//------------------------------------------------------------------------------
/**
 * Describes possible stroke types.
 *
 * @enum {int}
 */
anychart.visual.stroke.Stroke.StrokeType = {
    SOLID:0,
    GRADIENT:1
};
//------------------------------------------------------------------------------
//
//                          CapsType
//
//------------------------------------------------------------------------------
/**
 * Describes possible caps type. Contains string representation getter.
 *
 * @enum {int}
 * @return {String} String representation of caps type.
 */
anychart.visual.stroke.Stroke.CapsType = {
    NONE:0,
    ROUND:1,
    SQUARE:2,
    getCupsString:function (type) {
        switch (type) {
            case 0:
                return 'butt';
            case 1:
                return 'round';
            case 2:
                return 'square';
        }
    }
};
//------------------------------------------------------------------------------
//
//                          JointType
//
//------------------------------------------------------------------------------
/**
 * Describes possible join type. Contains string representation getter.
 *
 * @enum {int}
 * @return {String} String representation of join type.
 */
anychart.visual.stroke.Stroke.JoinType = {
    MITER:0,
    ROUND:1,
    BEVEL:2,
    getJoinString:function (type) {
        switch (type) {
            case 0:
                return 'miter';
            case 1:
                return 'round';
            case 2:
                return 'bevel';
        }
    }
};
//------------------------------------------------------------------------------
//
//                          Common properties
//
//------------------------------------------------------------------------------
/**
 * Indicates whether the stroke should be applied.
 *
 * @private
 * @type {Boolean}
 */
anychart.visual.stroke.Stroke.prototype.enabled_ = true;

/**
 * Gets the stroke enabled state.
 * @return {Boolean} Stroke enabled state.
 */
anychart.visual.stroke.Stroke.prototype.isEnabled = function () {
    return this.enabled_;
};

/**
 * Defines filling method.
 *
 * @private
 * @type {int}
 */
anychart.visual.stroke.Stroke.prototype.type_ =
    anychart.visual.stroke.Stroke.StrokeType.SOLID;

/**
 * Is type is gradient.
 * @return {Boolean} Is type is gradient.
 */
anychart.visual.stroke.Stroke.prototype.isGradient = function () {
    return this.type_ == anychart.visual.stroke.Stroke.StrokeType.GRADIENT;
};

/**
 * Return stroke type.
 * @return {anychart.visual.stroke.Stroke.StrokeType} Stroke type.
 */
anychart.visual.stroke.Stroke.prototype.getType = function () {
    return this.type_;
};

/**
 * Defines filling color for solid stroke.
 *
 * @private
 * @type {anychart.visual.color.ColorContainer} (html color string).
 */
anychart.visual.stroke.Stroke.prototype.color_ =
    anychart.utils.deserialization.getColor(null, 1);

/**
 * Defines color opacity.
 *
 * @private
 * @type {Number} (0..1)
 */
anychart.visual.stroke.Stroke.prototype.opacity_ = 1;

/**
 * Defines stroke thickness.
 *
 * @private
 * @type {Number}
 */
anychart.visual.stroke.Stroke.prototype.thickness_ = 1;

/**
 * Return stroke thickness.
 * @return {Number} Thickness.
 */
anychart.visual.stroke.Stroke.prototype.getThickness = function () {
    return this.thickness_;
};

/**
 * Defines line caps style.
 *
 * @private
 * @type {int} (anychart.visual.stroke.Stroke.CapsType)
 */
anychart.visual.stroke.Stroke.prototype.caps_ =
    anychart.visual.stroke.Stroke.CapsType.ROUND;

/**
 * Setter for Stroke caps type.
 * @param {anychart.visual.stroke.Stroke.CapsType} value Value.
 */
anychart.visual.stroke.Stroke.prototype.setCaps = function (value) {
    this.caps_ = value;
};

/**
 * Defines lines join style.
 *
 * @private
 * @type {int} (anychart.visual.stroke.Stroke.JoinType)
 */
anychart.visual.stroke.Stroke.prototype.joints_ =
    anychart.visual.stroke.Stroke.JoinType.ROUND;

/**
 * Describes gradient settings
 *
 * @private
 * @type {anychart.visual.gradient.Gradient}
 */
anychart.visual.stroke.Stroke.prototype.gradient_ = null;

/**
 * Getter for gradient settings.
 * @return {anychart.visual.gradient.Gradient} Gradient settings.
 */
anychart.visual.stroke.Stroke.prototype.getGradient = function () {
    return this.gradient_;
};
//------------------------------------------------------------------------------
//
//                          Dash properties
//
//------------------------------------------------------------------------------
/**
 * Describe is stroke dashed.
 * @private
 * @type {boolean}
 */
anychart.visual.stroke.Stroke.prototype.dashed_ = false;

/**
 * Describe is stroke dashed.
 * @return {boolean} Is stroke dashed.
 */
anychart.visual.stroke.Stroke.prototype.isDashed = function () {
    return this.dashed_;
};

/**
 * Dash length.
 * @private
 * @type {Number}
 */
anychart.visual.stroke.Stroke.prototype.dashLength_ = 2;

/**
 * Return dash length.
 * @return {Number} Length.
 */
anychart.visual.stroke.Stroke.prototype.getDashLength = function () {
    return this.dashLength_;
};

/**
 * Space length.
 * @private
 * @type {Number}
 */
anychart.visual.stroke.Stroke.prototype.spaceLenght_ = 2;

/**
 * Return space length.
 * @return {Number} Length.
 */
anychart.visual.stroke.Stroke.prototype.getSpaceLength = function () {
    return this.spaceLenght_;
};
//------------------------------------------------------------------------------
//
//                          Deserialization
//
//------------------------------------------------------------------------------
/**
 * Deserializes stroke settings from JSON object
 * Expects the next JSON structure:
 * <code>
 *  {
 *      enabled: boolean,
 *      type: string, // "solid"|"gradient", "solid" by def
 *      opacity: number, // [0..1], values that are not in these bounds are set
 *      to the closest bound
 *      color: string, // color string, parsed by colorParser
 *      gradient: object, // gradient settings object
 *      thickness: number, // line thickness
 *      caps: string, // "none"|"round"|"square", "none" by default
 *      joints: string // "miter"|"round"|"bevel", "miter" by default
 *  }
 * </code>
 *
 * @param {object} data JSON object.
 */
anychart.visual.stroke.Stroke.prototype.deserialize = function (data) {
    // aliases
    var des = anychart.utils.deserialization;

    // @enabled deserialization
    this.enabled_ = des.isEnabled(data);
    if (!this.enabled_) return;

    // deserializing stroke type
    if (des.hasProp(data, 'type')) {
        switch (des.getEnumItem(des.getProp(data, 'type'))) {
            case 'solid':
                this.type_ = anychart.visual.stroke.Stroke.StrokeType.SOLID;
                break;
            case 'gradient':
                this.type_ = anychart.visual.stroke.Stroke.StrokeType.GRADIENT;
                break;
        }
    }

    // deserializing stroke opacity
    if (des.hasProp(data, 'opacity')) {
        this.opacity_ = des.getRatio(des.getProp(data, 'opacity'));
    }

    // deserializing stroke solid color (opacity is counted on implicitly)
    if (des.hasProp(data, 'color')) {
        this.color_ = des.getColor(des.getProp(data, 'color'), this.opacity_);
    }

    // deserializing gradient_ if needed
    if (this.type_ == anychart.visual.stroke.Stroke.StrokeType.GRADIENT &&
        des.hasProp(data, 'gradient')) {
        this.gradient_ = new anychart.visual.gradient.Gradient(this.opacity_);
        this.gradient_.deserialize(des.getProp(data, 'gradient'));
    }

    // fixing stroke type if no gradient_ settings supplied
    if (this.type_ == anychart.visual.stroke.Stroke.StrokeType.GRADIENT &&
        (this.gradient_ == null || this.gradient_.keys_ == null))
        this.type_ = anychart.visual.stroke.Stroke.StrokeType.SOLID;

    // deserializing thickness
    if (des.hasProp(data, 'thickness')) this.thickness_ =
        des.getNum(des.getProp(data, 'thickness'));

    // deserializing caps style
    if (des.hasProp(data, 'caps')) {
        var capsString = des.getEnumItem(des.getProp(data, 'caps'));
        switch (capsString) {
            case 'none':
                this.caps_ = anychart.visual.stroke.Stroke.CapsType.NONE;
                break;
            case 'square':
                this.caps_ = anychart.visual.stroke.Stroke.CapsType.SQUARE;
                break;
            default:
                this.caps_ = anychart.visual.stroke.Stroke.CapsType.ROUND;
        }
    }

    // deserializing joints style
    if (des.hasProp(data, 'joints')) {
        var jointsString = des.getEnumItem(des.getProp(data, 'joints'));
        switch (jointsString) {
            default:
            case 'round':
                this.joints_ = anychart.visual.stroke.Stroke.JoinType.ROUND;
                break;
            case 'miter':
                this.joints_ = anychart.visual.stroke.Stroke.JoinType.MITER;
                break;
            case 'bevel':
                this.joints_ = anychart.visual.stroke.Stroke.JoinType.BEVEL;
                break;
        }
    }
    //dash disabled
    if (des.hasProp(data, 'dashed'))
        this.dashed_ = des.getBool(des.getProp(data, 'dashed'));
    if (des.hasProp(data, 'dash_length'))
        this.dashLength_ = des.getNum(des.getProp(data, 'dash_length'));
    if (des.hasProp(data, 'space_length'))
        this.spaceLenght_ = des.getNum(des.getProp(data, 'space_length'));
};
//------------------------------------------------------------------------------
//
//                          Visual
//
//------------------------------------------------------------------------------

/**
 * Gets the stroke settings.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {anychart.visual.color.Color} opt_color Color.
 * @return {String} Stroke settings.
 */
anychart.visual.stroke.Stroke.prototype.getSVGStroke = function (svgManager, bounds, opt_color, opt_userSpaceOnUse) {
    if (!this.enabled_) return '';
    var style = 'stroke-width:' + this.thickness_ + ';';
    style += 'stroke-linecap:' + anychart.visual.stroke.Stroke.CapsType.
        getCupsString(this.caps_) + ';';
    style += 'stroke-linejoin:' + anychart.visual.stroke.Stroke.JoinType.
        getJoinString(this.joints_) + ';';
    style += 'stroke-opacity:' + this.opacity_ + ';';
    switch (this.type_) {
        case anychart.visual.stroke.Stroke.StrokeType.SOLID:
            style += 'stroke:' + this.color_.getColor(opt_color).toRGB() + ';';
            break;
        case anychart.visual.stroke.Stroke.StrokeType.GRADIENT:
            this.gradient_.prepare(bounds, opt_color);
            style += 'stroke:' + svgManager.getGradientManager().
                getSVGGradient(this.gradient_, opt_userSpaceOnUse) + ';';
            break;
    }
    if (this.dashed_) {
        style += 'stroke-dasharray: ' + this.dashLength_ + ', ' +
            this.spaceLenght_ + ';';
    }
    return style;
};
