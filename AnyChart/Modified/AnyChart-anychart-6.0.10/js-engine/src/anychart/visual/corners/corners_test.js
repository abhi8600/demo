goog.require('anychart.visual.corners');

$(document).ready(function() {
    module('visual/corners/Corners');

    test('Empty corners', function() {
        var corners = new anychart.visual.corners.Corners();
        ok(corners != null, 'object exists');
    });

    test('Advanced tests exists', function() {
        ok(false, 'no tests implemented');
    });
});

