/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.image.Image}.</li>
 *     <li>@class {anychart.visual.image.SVGImageManager}.</li>
 * </ul>
 */
goog.provide('anychart.visual.image');

//------------------------------------------------------------------------------
//
//                          Image class
//
//------------------------------------------------------------------------------

/**
 * @param {string} imageUrl
 * @param imageMode
 * @constructor
 */
anychart.visual.image.Image = function (imageUrl, imageMode) {
    this.imageUrl_ = imageUrl;
    this.imageMode_ = imageMode || anychart.visual.image.ImageModeType.TILE;
};
/**
 * Describes possible image mode.
 *
 * @enum {int}
 */
anychart.visual.image.ImageModeType = {
    TILE: 0,
    STRETCH: 1,
    FIT_BY_PROPORTIONS: 2
};
/**
 * Defines the image mode enumeration
 *
 * @private
 * @type {int} (0, 1, or 2)
 */
anychart.visual.image.Image.prototype.imageMode_ =
    anychart.visual.image.ImageModeType.TILE;
/**
 * Defines the image url
 *
 * @private
 * @type {string}
 */
anychart.visual.image.Image.prototype.imageUrl_ = null;

anychart.visual.image.Image.prototype.getImageMode = function () {
    return this.imageMode_;
};
anychart.visual.image.Image.prototype.getImageUrl = function () {
    return this.imageUrl_;
};
anychart.visual.image.Image.prototype.setImageMode = function (imageMode) {
    this.imageMode_ = imageMode;
};
anychart.visual.image.Image.prototype.setImageUrl = function (imageUrl) {
    this.imageUrl_ = imageUrl;
};

//------------------------------------------------------------------------------
//
//                          SVGImageManager class
//
//------------------------------------------------------------------------------
/**
 * Image manager.
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager SVG manager.
 */
anychart.visual.image.SVGImageManager = function (svgManager) {
    this.svgManager_ = svgManager;
    this.imageMap_ = {};
};
//------------------------------------------------------------------------------
//
//                          Properties
//
//------------------------------------------------------------------------------
/**
 * Last image element id.
 * @private
 * @type {int}
 */
anychart.visual.image.SVGImageManager.lastId_ = 0;

/**
 * Link to SVG manager.
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.visual.image.SVGImageManager.prototype.svgManager_ = null;

/**
 * Image uid map.
 * @private
 * @type {Object}
 */
anychart.visual.image.SVGImageManager.prototype.imageMap_ = null;
//------------------------------------------------------------------------------
//
//                          Image generation
//
//------------------------------------------------------------------------------
/**
 * Return image id.
 * @param {anychart.visual.image.Image} image Image settings
 * @return {String} Image id.
 */
anychart.visual.image.SVGImageManager.prototype.getSVGImage = function (image, bounds) {
    var id = image.getImageUrl() + '-' + image.getImageMode();
    if (this.imageMap_[id]) return this.imageMap_[id];

    var pattern = this.svgManager_.createSVGElement('pattern');
    var imageElement = this.svgManager_.createSVGElement('image');
    // now calculate image size
    
    var uid = 'img' + (anychart.visual.image.SVGImageManager.lastId_++) + '-' + image.getImageUrl() + '-' + image.getImageMode();
    
    pattern.setAttribute('id', uid);
    imageElement.setAttribute('xlink:href', image.getImageUrl());
    imageElement.setAttribute('x', 0);
    imageElement.setAttribute('y', 0);
    imageElement.href['baseVal'] = image.getImageUrl();
    imageElement.href['animVal'] = image.getImageUrl();

    pattern.setAttribute('x', bounds.x);
    pattern.setAttribute('y', bounds.y);
    pattern.setAttribute('patternUnits', 'userSpaceOnUse');
    if (image.getImageMode() == anychart.visual.image.ImageModeType.STRETCH) {
        pattern.setAttribute('width', bounds.width);
        pattern.setAttribute('height', bounds.height);
        imageElement.setAttribute('width', bounds.width);
        imageElement.setAttribute('height', bounds.height);
        imageElement.setAttribute('preserveAspectRatio', "none");
        imageElement.setAttribute('image-rendering', 'optimize-speed');
    } else if (image.getImageMode() == anychart.visual.image.ImageModeType.FIT_BY_PROPORTIONS) {
        imageElement.setAttribute('preserveAspectRatio', "xMinYMin meet");
        pattern.setAttribute('width', bounds.width);
        pattern.setAttribute('height', bounds.height);
        imageElement.setAttribute('width', bounds.width);
        imageElement.setAttribute('height', bounds.height);
    } else { // TILE
        pattern.setAttribute('width', 1);
        pattern.setAttribute('height', 1);
        imageElement.setAttribute('width', 1);
        imageElement.setAttribute('height', 1);
    }
    pattern.appendChild(imageElement);
    uid = 'url(#' + uid + ')';

    this.svgManager_.addDef(pattern);
    this.imageMap_[id] = uid;
    
    var jsImage = new Image();
    jsImage.src = image.getImageUrl();
    if (image.getImageMode() == anychart.visual.image.ImageModeType.TILE) {
        jsImage.onload = function() {
            var w = jsImage.width;
            var h = jsImage.height;
            pattern.setAttribute('width', w);
            pattern.setAttribute('height', h);
            imageElement.setAttribute('width', w);
            imageElement.setAttribute('height', h);
        }; 
    }
    return uid;
};

//------------------------------------------------------------------------------
//                  Clear.
//------------------------------------------------------------------------------
anychart.visual.image.SVGImageManager.prototype.clear = function () {
    anychart.visual.image.SVGImageManager.lastId_ = 0;
    this.imageMap_ = {};
};
