goog.require('anychart.visual.text');

$(document).ready(function() {

    module('visual/text/SVGTextMeasurement');

    test('SVGTextMeasurement exists', function() {
        ok(anychart.visual.text.SVGTextMeasurement != null, 'object exists');
    });

    test('Advanced tests exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/text/TextElement');

    test('TextElement exists', function() {
        var text = new anychart.visual.text.TextElement();
        ok(text != null, 'object exists');
    });

    test('Advanced tests exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/text/BaseTitle');

    test('BaseTitle exists', function() {
        var title = new anychart.visual.text.BaseTitle();
        ok(title != null, 'object exists');
    });

    test('Advanced tests exists', function() {
        ok(false, 'no tests implemented');
    });
});

