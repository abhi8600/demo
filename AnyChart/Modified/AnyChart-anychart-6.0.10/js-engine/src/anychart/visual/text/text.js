/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *     <li>@class {anychart.visual.text.SVGTextMeasurement}</li>
 *     <li>@class {anychart.visual.text.SVGRotatedTextSprite}</li>
 *     <li>@class {anychart.visual.text.SVGTextSprite}</li>
 *     <li>@class {anychart.visual.text.TextElement}</li>
 *     <li>@class {anychart.visual.text.TextElement.TextLine_}</li>
 *     <li>@class {anychart.visual.text.BaseTitle}</li>
 *     <li>@class {anychart.visual.text.InteractiveTitle}</li>
 * </ul>.
 */

goog.provide('anychart.visual.text');

goog.require('anychart.formatting');
goog.require('anychart.layout');
goog.require('anychart.utils');
goog.require('anychart.utils.geom');
goog.require('anychart.visual.background');

//------------------------------------------------------------------------------
//
//                          SVGTextMeasurement class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 */
anychart.visual.text.SVGTextMeasurement = function (svgManager) {
    this.svgManager_ = svgManager;
    this.textElement_ = this.svgManager_.createSVGElement('text');
    this.textElement_.setAttribute('visibility', 'hidden');
    this.textElement_.setAttribute('x', '0');
    this.textElement_.setAttribute('y', '0');
    this.textElementText_ = this.svgManager_.createText('');
    this.textElement_.appendChild(this.textElementText_);
    svgManager.getMeasureSVG().appendChild(this.textElement_);
};

/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.visual.text.SVGTextMeasurement.prototype.svgManager_ = null;

/**
 * @private
 * @type {SVGElement}
 */
anychart.visual.text.SVGTextMeasurement.prototype.textElement_ = null;

/**
 * @private
 * @type {Text}
 */
anychart.visual.text.SVGTextMeasurement.prototype.textElementText_ = null;

/**
 * Set the style of text element.
 * @param {String} fontStyle Font style.
 */
anychart.visual.text.SVGTextMeasurement.prototype.configureText = function (fontStyle) {
    this.textElement_.setAttribute('style', fontStyle);
};

/**
 * Sets the style of text element and value, and return bounds of the text
 * element.
 * @param {String} fontStyle Font style.
 * @param {String} text Text.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.visual.text.SVGTextMeasurement.prototype.measureText = function (text, textElement) {
    textElement.setSVGFontStyle(this.textElement_);
    this.textElementText_.nodeValue = text;
    var bounds = this.textElement_['getBBox']();
    this.textElementText_.nodeValue = '';
    textElement.clearSVGFontStyle(this.textElement_);

    return new anychart.utils.geom.Rectangle(
        bounds.x,
        bounds.y,
        bounds.width,
        bounds.height);
};
//------------------------------------------------------------------------------
//
//                          SVGTextSprite class
//
//------------------------------------------------------------------------------

/**
 * Text sprite class. Define 2 elements: background and text.
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {Text=} opt_textSegment Text segment.
 * @param {anychart.svg.SVGSprite} opt_backgroundSprite Background sprite.
 */
anychart.visual.text.SVGTextSprite = function (svgManager, opt_textSegment, opt_backgroundSprite) {
    anychart.svg.SVGSprite.call(this, svgManager);

    if (opt_backgroundSprite) {
        this.backgroundSprite_ = opt_backgroundSprite;
        this.appendSprite(opt_backgroundSprite);
    }

    if (opt_textSegment) {
        this.textSegment_ = opt_textSegment;
        this.element_.appendChild(opt_textSegment);
    }
};
goog.inherits(anychart.visual.text.SVGTextSprite, anychart.svg.SVGSprite);

//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------

/**
 * Text segment.
 * @private
 * @type {Text}
 */
anychart.visual.text.SVGTextSprite.prototype.textSegment_ = null;

/**
 * Returns text segment.
 * @return {Text} Text segment.
 */
anychart.visual.text.SVGTextSprite.prototype.getTextSegment = function () {
    return this.textSegment_;
};

/**
 * Sets text segment.
 * @param {Text} value Text segment.
 */
anychart.visual.text.SVGTextSprite.prototype.setTextSegment = function (value) {
    this.textSegment_ = value;
};

/**
 * Background sprite.
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.visual.text.SVGTextSprite.prototype.backgroundSprite_ = null;

/**
 * Returns background sprite.
 * @return {anychart.svg.SVGSprite} Background sprite.
 */
anychart.visual.text.SVGTextSprite.prototype.getBackgroundSprite = function () {
    return this.backgroundSprite_;
};

/**
 * Sets background sprite.
 * @param {anychart.svg.SVGSprite} value Background sprite.
 */
anychart.visual.text.SVGTextSprite.prototype.setBackgroundSprite =
    function (value) {
        this.backgroundSprite_ = value;
    };
//------------------------------------------------------------------------------
//
//                          SVGRotatedTextSprite class
//
//------------------------------------------------------------------------------

/**
 * Rotated text sprite. Define sprite which contains text sprite.
 * @constructor
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.visual.text.SVGTextSprite} textSprite Text sprite.
 */
anychart.visual.text.SVGRotatedTextSprite = function (svgManager, textSprite) {
    anychart.svg.SVGSprite.call(this, svgManager);
    this.textSprite_ = textSprite;
    this.appendSprite(textSprite);
};
goog.inherits(anychart.visual.text.SVGRotatedTextSprite,
    anychart.svg.SVGSprite);

/**
 * Returns text segment from text sprite.
 * @return {Text} Text segment.
 */
anychart.visual.text.SVGRotatedTextSprite.prototype.getTextSegment =
    function () {
        return this.textSprite_.getTextSegment();
    };

/**
 * Sets text segment to text sprite.
 * @param {Text} value Text segment.
 */
anychart.visual.text.SVGRotatedTextSprite.prototype.setTextSegment =
    function (value) {
        this.textSprite_.setTextSegment(value);
    };

/**
 * Returns background sprite from text sprite.
 * @return {anychart.svg.SVGSprite} Background sprite.
 */
anychart.visual.text.SVGRotatedTextSprite.prototype.getBackgroundSprite =
    function () {
        return this.textSprite_.getBackgroundSprite();
    };

/**
 * Set background sprite to text sprite.
 * @param {anychart.svg.SVGSprite} value Background sprite.
 */
anychart.visual.text.SVGRotatedTextSprite.prototype.setBackgroundSprite =
    function (value) {
        this.textSprite_.setBackgroundSprite(value);
    };
//------------------------------------------------------------------------------
//
//                          TextElement class
//
//------------------------------------------------------------------------------
/**
 * Class represent base text element, class contains methods to deserialize text
 * element settings and create TextDrawer.
 * @constructor
 * <p>
 *  <code>
 *      var textElement = new anychart.visual.text.TextElement();
 *      textElement.deserialize(result);
 *      var sprite = textElement.createVisual(bounds, true, null, null);
 *  </code>
 * </p>
 */
anychart.visual.text.TextElement = function () {
    this.setDefaultSettings();
};

/**
 * Creates new rotation utils and returns it.
 * @private
 * @return {anychart.utils.geom.RotationUtils} Rotation utils.
 */
anychart.visual.text.TextElement.prototype.createRotationUtils_ = function () {
    return new anychart.utils.geom.RotationUtils();
};
//------------------------------------------------------------------------------
//                  Events.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.disableEvents_ = true;
//------------------------------------------------------------------------------
//                          Text settings
//------------------------------------------------------------------------------
/**
 * The name of the font for text in this text format, as a string.
 * @private
 * @type {String}
 */
anychart.visual.text.TextElement.prototype.family_ = null;

/**
 * The text size in pixels.
 * @private
 * @type {Number}
 */
anychart.visual.text.TextElement.prototype.size_ = null;
/**
 * @param {Number} value
 */
anychart.visual.text.TextElement.prototype.setSize = function (value) {
    this.size_ = value;
};

/**
 * Specifies whether the text is boldface.
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.bold_ = null;

/**
 * Indicates whether text in this text format is italic.
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.italic_ = null;

/**
 * Indicates whether the text is underlined (true) or not (false).
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.underline_ = null;

/**
 * Identify line spacing of the text.
 * @private
 * @type {Number}
 */
anychart.visual.text.TextElement.prototype.spacing_ = null;

/**
 * Indicates the color of the text.
 * @private
 * @type {anychart.visual.color.ColorContainer}
 */
anychart.visual.text.TextElement.prototype.color_ = null;

/**
 * Multiline text align.
 * @private
 * @type {anychart.layout.HorizontalAlign}
 */
anychart.visual.text.TextElement.prototype.textAlign_ = null;

/**
 * @return {int}
 */
anychart.visual.text.TextElement.prototype.getTextAlign = function () {
    return this.textAlign_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.renderAsHTTML_ = null;
/**
 * Устанавливает для елемента дефолтные настроки.
 */
anychart.visual.text.TextElement.prototype.setDefaultSettings = function () {
    this.renderAsHTTML_ = false;
    this.textAlign_ = anychart.layout.HorizontalAlign.LEFT;
    this.color_ = anychart.utils.deserialization.getColor(null, 1);
    this.spacing_ = 3;
    this.underline_ = false;
    this.italic_ = false;
    this.bold_ = false;
    this.size_ = 10;
    this.family_ = 'Arial';
    this.rotation_ = this.createRotationUtils_();
    this.setRotationAngle(0);
    this.background_ = null;
};
//------------------------------------------------------------------------------
//
//                          Size settings
//
//------------------------------------------------------------------------------

/**
 * Svg text element bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.visual.text.TextElement.prototype.textBounds_ = null;

/**
 * Rotated text element bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.visual.text.TextElement.prototype.bounds_ = null;

/**
 * Returns bounds of rotated text element.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.visual.text.TextElement.prototype.getBounds = function () {
    return this.bounds_;
};

/**
 * Non rotated text element bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.visual.text.TextElement.prototype.nonRotatedBounds_ = null;

/**
 * Returns bounds of text sprite (text element with background sprite)
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.visual.text.TextElement.prototype.getNonRotatedBounds = function () {
    return this.nonRotatedBounds_;
};

/**
 * Returns bounds of background sprite.
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.visual.text.TextElement.prototype.getBackgroundBounds = function () {
    var bounds = this.nonRotatedBounds_.clone();
    bounds.x = 1;
    bounds.y = 1;
    bounds.width -= 2;
    bounds.height -= 2;
    return bounds;
};
//------------------------------------------------------------------------------
//
//                          Rotation settings
//
//------------------------------------------------------------------------------

/**
 * Instance of rotation controller.
 * @private
 * @type {anychart.utils.geom.RotationUtils}
 */
anychart.visual.text.TextElement.prototype.rotation_ = null;

/**
 * Return rotation angle value in deg.
 * @return {Number} Angle.
 */
anychart.visual.text.TextElement.prototype.getRotationAngle = function () {
    return this.rotation_.degRotation;
};

/**
 * Return rotation angle value in radian.
 * @return {Number} Angle.
 */
anychart.visual.text.TextElement.prototype.getRadianRotationAngle = function () {
    return this.rotation_.radRotation;
};

/**
 * Set new rotation angle value in deg.
 * @param {Number} value Angle.
 */
anychart.visual.text.TextElement.prototype.setRotationAngle = function (value) {
    this.rotation_.degRotation = -value;
    this.rotation_.calculate();
};
//------------------------------------------------------------------------------
//
//                          Other settings
//
//------------------------------------------------------------------------------

/**
 * Indicates, is text element enabled to draw.
 * @private
 * @type {Boolean}
 */
anychart.visual.text.TextElement.prototype.enabled_ = true;

/**
 * Returns the enabled state of text element.
 * @return {Boolean} Is text element enabled to draw.
 */
anychart.visual.text.TextElement.prototype.isEnabled = function () {
    return this.enabled_;
};

/**
 * Turns text element on/off
 * @param {boolean} value
 */
anychart.visual.text.TextElement.prototype.setEnabled = function (value) {
    this.enabled_ = value;
};

/**
 * Text split by lines.
 * @private
 * @type {Array.<String>}
 */
anychart.visual.text.TextElement.prototype.textArray_ = null;

/**
 * Returns array of lines of the text.
 * @return {Array.<String>} Lines.
 */
anychart.visual.text.TextElement.prototype.getTextArray = function () {
    return this.textArray_;
};

/**
 * Background settings.
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.visual.text.TextElement.prototype.background_ = null;

/**
 * Returns background settings.
 * @return {anychart.visual.background.RectBackground} Background settings.
 */
anychart.visual.text.TextElement.prototype.getBackground = function () {
    return this.background_;
};

/**
 * Sets background settings.
 * @param {anychart.visual.background.RectBackground} value Background settings.
 */
anychart.visual.text.TextElement.prototype.setBackground = function (value) {
    this.background_ = value;
};

//------------------------------------------------------------------------------
//
//                          Deserialization
//
//------------------------------------------------------------------------------
/**
 * Deserialize font settings.
 * @param {Object} data JSON object with font settings.
 * <p>
 *   Node: Underline text not supported in canvas API.
 * </p>
 * <p>
 *  Usage sample:
 *  <code>
 *      var textElement = new anychart.visual.text.TextElement();
 *      textElement.deserialize(data);
 *  </code>
 * </p>
 * <p>
 * Expected JSON structure:
 * <code>
 *  object {
 *      background : object // deserialize by anychart.visual.
 *                          //                background.RectBackground,
 *      rotation : number // Sets text element rotation,
 *      text_align : string // Sets text element align mode,
 *      font : object // Described bellow
 *  }
 *  </code>
 * </p>
 * <p>
 * Expected JSON structure of font object:
 * <code>
 *  object{
 *      family : string // Sets a font family, it can be a system font name, or
 *      // the name embeded fonts, placed in the embedded fonts folder.
 *      size : number // Sets a font size.
 *      bold : boolean // Sets whether font is bold.
 *      italic : boolean // Sets whether font is italic.
 *      underline : boolean // Sets whether font is underlined.
 *      color : string // Sets font color.
 *  }.
 * </code>
 * <p>
 */
anychart.visual.text.TextElement.prototype.deserialize = function (data) {
    //aliases
    var des = anychart.utils.deserialization;

    if (!des.isEnabled(data)) {
        this.enabled_ = false;
        return;
    }

    if (des.hasProp(data, 'background')) {
        this.background_ = new anychart.visual.background.RectBackground();
        this.background_.deserialize(des.getProp(data, 'background'));
    }

    this.setRotationAngle(des.hasProp(data, 'rotation') ?
        des.getNumProp(data, 'rotation') : 0);

    //Backward compatibility support with old version of attribute name
    var alignAttributeName = (des.hasProp(data, 'text_align')) ?
        'text_align' : 'multi_line_align';
    if (des.hasProp(data, alignAttributeName)) {
        switch (des.getLString(des.getProp(data, alignAttributeName))) {
            case 'left':
                this.textAlign_ = anychart.layout.HorizontalAlign.LEFT;
                break;
            case 'center':
                this.textAlign_ = anychart.layout.HorizontalAlign.CENTER;
                break;
            case 'right':
                this.textAlign_ = anychart.layout.HorizontalAlign.RIGHT;
                break;
        }
    }

    if (des.hasProp(data, 'font')) {
        var font = des.getProp(data, 'font');

        if (des.hasProp(font, 'family'))
            this.family_ = des.getLString(des.getProp(font, 'family'));

        if (des.hasProp(font, 'size'))
            this.size_ = des.getNum(des.getProp(font, 'size'));

        if (des.hasProp(font, 'bold'))
            this.bold_ = des.getBool(des.getProp(font, 'bold'));

        if (des.hasProp(font, 'italic'))
            this.italic_ = des.getBool(des.getProp(font, 'italic'));

        if (des.hasProp(font, 'underline'))
            this.underline_ = des.getBool(des.getProp(font, 'underline'));

        if (des.hasProp(font, 'color'))
            this.color_ = des.getColor(des.getProp(font, 'color'));

        if (des.hasProp(font, 'render_as_html'))
            this.renderAsHTTML_ = des.getBoolProp(font, 'render_as_html');
    }
};
//------------------------------------------------------------------------------
//
//                          Size
//
//------------------------------------------------------------------------------
/**
 * Данный метод удалет из переданной строки любые HTML теги и их аттрибуты.
 * Пример:
 *  <b>Hello world</b><br><br/> => Hello world
 *  <a href='#'>Hello world</a><br => Hello world
 *
 * @param {String} text Текст из которого будут удалены все тэги.
 * @return {String} Чистый текст, с удаленными тэгами.
 */
anychart.visual.text.TextElement.prototype.removeHTMLTags = function (svgManager, text) {
    //remove html tags
    return text.replace(/<.*?>/gi, '');

    // 01.06.12
    //Использование такого метода убирает управляющие символы (перенос строк, табуляция)
    //из строки. Нужно либо рендерить в диве каждую строку по отдельности либо искать другой способ.
    //Связаный с этим код есть в SVGManager но он lazy поэтому остался как есть.


    //29.05.12
    //convert ascii and unicode symbols
    // http://dev.anychart.com/jira/browse/AFJ-170
    //var div = svgManager.getSymbolsConverterDiv();
    //div.innerHTML = text;
    //text = div.innerText;
    //div.innerHTML = '';
};

/**
 * Split text to Array.<String>, separator symbols is '\n' or '\r' symbols.
 * @private
 * @param {String} text Split text.
 * @return {Array.<String>} Splited lines of the text.
 */
anychart.visual.text.TextElement.prototype.splitText_ = function (text) {
    var res = [];
    var charCount = text.length;
    var str = '';
    for (var i = 0; i < charCount; i++) {
        var charCode = text.charCodeAt(i);
        if (charCode == 13 || charCode == 10) {
            res.push(anychart.utils.stringUtils.trim(str));
            str = '';
        } else if (i == charCount - 1) {
            str += text.charAt(i);
            res.push(anychart.utils.stringUtils.trim(str));
            str = '';
        } else str += text.charAt(i);
    }
    return res;
};

/**
 * Applying font style to text, and returns bounds of resulting text element.
 * @private
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {String} text Text.
 * @return {anychart.utils.geom.Rectangle} Bounds of measured text.
 */
anychart.visual.text.TextElement.prototype.measureText_ = function (svgManager, text) {
    return svgManager.getTextMeasurement().
        measureText(text, this);
};

/**
 * Returns bounds of multiline text.
 * @private
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {Array.<String>} lines Lines of text.
 * @return {anychart.urils.geom.Rectangle} Bounds.
 */
anychart.visual.text.TextElement.prototype.measureMultilineText_ =
    function (svgManager, lines) {
        var res = new anychart.utils.geom.Rectangle(0, 0, 0, 0);
        for (var i = 0; i < lines.length; i++) {
            var lineBounds = this.measureText_(svgManager, lines[i]);
            res.width = Math.max(res.width, lineBounds.width);
            res.height += lineBounds.height;
        }
        return res;
    };

/**
 * Initialize text element size.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {String} text Text to draw.
 */
anychart.visual.text.TextElement.prototype.initSize = function (svgManager, text) {
    if (this.renderAsHTTML_)
        text = this.removeHTMLTags(svgManager, text);

    this.textArray_ = this.splitText_(text);
    this.textBounds_ = this.measureMultilineText_(svgManager,
        this.textArray_);

    this.textBounds_.x += 1;
    this.textBounds_.y += 1;

    var textElementBounds = this.textBounds_.clone();
    if (this.background_ && this.background_.isEnabled() &&
        this.background_.getMargin()) {
        var temp = this.textBounds_.clone();
        textElementBounds =
            this.applyBackgroundMargin_(this.textBounds_, temp);
    }
    this.nonRotatedBounds_ = textElementBounds;
    this.bounds_ =
        this.rotation_.getRotatedBounds(this.nonRotatedBounds_);
};

/**
 * Apply margins to text and background bounds, return textElementBounds.
 * @private
 * @param {anychart.utils.geom.Rectangle} textBounds Text bounds.
 * @param {anychart.utils.geom.Rectangle} backgroundBounds Background bounds.
 * @return {anychart.utils.geom.Rectangle} Bounds of text element.
 */
anychart.visual.text.TextElement.prototype.applyBackgroundMargin_ =
    function (textBounds, backgroundBounds) {
        //+4  moved from flash
        textBounds.x += this.background_.getMargin().getLeft() + 1;
        textBounds.y += this.background_.getMargin().getTop() + 1;

        backgroundBounds.x = textBounds.x;
        backgroundBounds.y = textBounds.y;
        backgroundBounds.width += this.background_.getMargin().getLeft() +
            this.background_.getMargin().getRight() + 4;

        backgroundBounds.height += this.background_.getMargin().getTop() +
            this.background_.getMargin().getBottom() + 4;

        return backgroundBounds;
    };

//------------------------------------------------------------------------------
//
//                          Visual
//
//------------------------------------------------------------------------------

/**
 * Set font setting to passed element.
 * @param {anychart.visual.color.Color=} opt_color Color.
 */
anychart.visual.text.TextElement.prototype.setSVGFontStyle = function (element, opt_color) {
    element.setAttribute('font-family', this.family_);
    element.setAttribute('font-size', this.size_);
    if (this.bold_) element.setAttribute('font-weight', 'bold');

    if (this.italic_) element.setAttribute('font-style', 'italic');

    if (this.underline_) element.setAttribute('font-decoration', 'underline');

    element.setAttribute('fill', this.color_.getColor(opt_color).toRGB());
    element.setAttribute('stroke', 'none');
};
/**
 * Clear element style
 * @param {SVGElement} element
 */
anychart.visual.text.TextElement.prototype.clearSVGFontStyle = function (element) {
    element.removeAttribute('font-family');
    element.removeAttribute('font-size');
    element.removeAttribute('font-weight');
    element.removeAttribute('font-style');
    element.removeAttribute('font-decoration');
    element.removeAttribute('fill');
    element.removeAttribute('stroke');
};

/**
 * Creates SVGTextSprite element.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.visual.color.Color=} opt_color Color.
 * @param {Boolean=} opt_updateText Identifies whether text is need to update.
 * @return {anychart.visual.text.SVGTextSprite} Svg text sprite.
 */
anychart.visual.text.TextElement.prototype.createSVGText = function (svgManager, opt_color, opt_updateText) {
    var textSegment = svgManager.createSVGElement('text');
    var bgSprite = this.createSVGTextBackground_(svgManager, opt_color);
    var sprite = new anychart.visual.text.SVGTextSprite(svgManager,
        textSegment, bgSprite);

    if (this.disableEvents_)
        textSegment.setAttribute('pointer-events', 'none');

    if (opt_updateText || opt_updateText == null ||
        opt_updateText == undefined)
        this.updateText_(svgManager, textSegment, opt_color);

    this.execRotation(sprite);

    return new anychart.visual.text.SVGRotatedTextSprite(
        svgManager, sprite);
};

/**
 * ND: Needs doc!
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.visual.text.TextElement.prototype.execRotation = function (sprite) {
    sprite.setRotation(this.getRotationAngle());
    var pos = new anychart.utils.geom.Point();
    this.rotation_.setRotationOffset(pos, this.nonRotatedBounds_);
    sprite.setPosition(pos.x, pos.y);
};
/**
 * Updates text and background settings.
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.visual.text.SVGRotatedTextSprite} sprite Sprite to update.
 * @param {anychart.visual.color.Color=} opt_color Color.
 * @param {int} opt_align Text alignment.
 */
anychart.visual.text.TextElement.prototype.update = function (svgManager, sprite, opt_color, opt_noUpdateText, opt_align) {
    this.updateText_(svgManager, sprite.getTextSegment(), opt_color, opt_noUpdateText, opt_align);

    this.updateBackground_(sprite.getBackgroundSprite(), this.getBackgroundBounds(), opt_color);
};


/**
 * Updates text.
 * @private
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {Text} segment Text segment.
 * @param {anychart.visual.color.Color=} opt_color Color.
 * @param {int} opt_align Text alignment.
 */
anychart.visual.text.TextElement.prototype.updateText_ = function (svgManager, segment, opt_color, opt_noUpdateText, opt_align) {
    this.setSVGFontStyle(segment, opt_color);
    segment.setAttribute('x', this.textBounds_.x);
    segment.setAttribute('y', this.textBounds_.y);
    var halign = anychart.layout.HorizontalAlign;
    if (opt_align == undefined || opt_align == null) opt_align = -1;
    if (opt_align) {
        switch (opt_align) {
            case halign.CENTER:
                segment.setAttribute('text-anchor', 'middle');
                break;
            case halign.RIGHT:
                segment.setAttribute('text-anchor', 'end');
                break;
        }
    }
    if (opt_noUpdateText) return;
    this.clearText_(segment);
    for (var i = 0; i < this.textArray_.length; i++) {
        var tSpan = svgManager.createSVGElement('tspan');
        tSpan.appendChild(svgManager.createText(this.textArray_[i]));
        switch (opt_align) {
            case halign.CENTER:
                tSpan.setAttribute('x', this.textBounds_.width / 2);
                break;
            case halign.RIGHT:
                tSpan.setAttribute('x', this.textBounds_.width);
                break;
            default:
                tSpan.setAttribute('x', this.textBounds_.x);
        }
        tSpan.setAttribute('dy', '1em');
        segment.appendChild(tSpan);
    }
};

/**
 * Fully clears text segment.
 * @private
 * @param {Text} segment Text segment.
 */
anychart.visual.text.TextElement.prototype.clearText_ = function (segment) {
    while (segment.childNodes.length > 0)
        segment.removeChild(segment.childNodes[0]);
};

/**
 * Updates background settings.
 * @private
 * @param {anychart.visual.background.SVGBackgroundSprite} sprite Background
 * sprite.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 * @param {anychart.visual.color.Color=} opt_color Color.
 */
anychart.visual.text.TextElement.prototype.updateBackground_ = function (sprite, bounds, opt_color) {
    if (this.background_ && this.background_.isEnabled())
        this.background_.draw(sprite, bounds, opt_color);
};

/**
 * Returns background sprite with settings or creates a new one.
 * @private
 * @param {anychart.svg.SVGManager} svgManager Svg manager.
 * @param {anychart.visual.color.Color=} opt_color Color.
 * @return {anychart.visual.background.SVGBackgroundSprite} Background sprite.
 */
anychart.visual.text.TextElement.prototype.createSVGTextBackground_ = function (svgManager, opt_color) {
    var res;

    if (this.background_ && this.background_.isEnabled()) {
        res = this.background_.initialize(svgManager);
        this.background_.draw(res, this.getBackgroundBounds(), opt_color);
    }


    if (!res) res = new anychart.visual.background.SVGBackgroundSprite(svgManager, svgManager.createPath(), svgManager.createPath());

    return res;
};

//------------------------------------------------------------------------------
//
//                          Text line
//
//------------------------------------------------------------------------------
/**
 * Class represent auxiliary object for text drawing.
 * <p>
 * Description: TextLine is an object with contains information about one line
 * of multiline text such as line value and line metrics.
 * Later it can be expanded to add own color to each text line or something
 * else.
 * </p>
 * @private
 * @constructor
 * @param {String} lineValue Line value.
 * @param {anychart.utils.geom.Rectangle} lineMetrics Line metrics.
 * <p>
 *  Usage sample:
 *  <code>
 *      var line = new anychart.visual.text.TextElement.TextLine();.
 *  </code>
 * <p>
 */
anychart.visual.text.TextElement.TextLine_ = function (lineValue, lineMetrics) {
    if (lineValue != undefined && lineMetrics != undefined) {
        this.lineValue_ = lineValue;
        this.lineMetrics_ = lineMetrics;
    }
};

/**
 * Line value.
 * @private
 * @type {String}
 */
anychart.visual.text.TextElement.TextLine_.prototype.lineValue_ = null;

/**
 * Return line value.
 * @return {String} Line value.
 */
anychart.visual.text.TextElement.TextLine_.prototype.getLineValue = function () {
    return this.lineValue_;
};

/**
 * Line metrics.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.visual.text.TextElement.TextLine_.prototype.lineMetrics_ = null;

/**
 * Return line metrics.
 * @return {anychart.utils.geom.Rectangle} Line metrics.
 */
anychart.visual.text.TextElement.TextLine_.prototype.getLineMetrics =
    function () {
        return this.lineMetrics_;
    };

//------------------------------------------------------------------------------
//
//                          BaseTitle class
//
//------------------------------------------------------------------------------

/**
 * Class represent base title object.
 * <p>
 *  Note: BaseTitle is a base class for all title object, you should not create
 *  instance of this class, it's just for inheritance.
 * </p>
 * @constructor
 * @extends {anychart.visual.text.TextElement}
 * <p>
 *  Usage sample:
 *  <code>
 *      var newClass = function() {
 *          anychart.visual.text.BaseTitle.apply(this);
 *      };
 *      goog.inherits(newClass, anychart.visual.text.BaseTitle);
 *  </code>
 * </p>
 */
anychart.visual.text.BaseTitle = function () {
    anychart.visual.text.TextElement.apply(this);
};
goog.inherits(anychart.visual.text.BaseTitle, anychart.visual.text.TextElement);

//-------------------------------------------------------------------------
//      PROPERTIES
//-------------------------------------------------------------------------

/**
 * Title sprite.
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.visual.text.BaseTitle.prototype.sprite_ = null;

/**
 * Returns title sprite.
 * @return {anychart.visual.text.SVGRotatedTextSprite} Title sprite.
 */
anychart.visual.text.BaseTitle.prototype.getSprite = function () {
    return this.sprite_;
};

/**
 * Title text formatter.
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.visual.text.BaseTitle.prototype.textFormatter_ = null;

/**
 * @return {anychart.formatting.TextFormatter} Text formatter.
 */
anychart.visual.text.BaseTitle.prototype.getTextFormatter = function () {
    return this.textFormatter_;
};

/**
 * Title padding.
 * @private
 * @type {Number}
 */
anychart.visual.text.BaseTitle.prototype.padding_ = 5;

/**
 * Return title padding.
 * @return {Number} Padding value.
 */
anychart.visual.text.BaseTitle.prototype.getPadding = function () {
    return this.padding_;
};

/**
 * Title align.
 * @private
 * @type {anychart.layout.AbstractAlign}
 */
anychart.visual.text.BaseTitle.prototype.align_ =
    anychart.layout.AbstractAlign.CENTER;

/**
 * Return title align.
 * @return {anychart.layout.AbstractAlign} Title align.
 */
anychart.visual.text.BaseTitle.prototype.getAlign = function () {
    return this.align_;
};
//-------------------------------------------------------------------------
//      METHODS
//-------------------------------------------------------------------------
/**
 * Deserialize base title settings.
 * @param {Object} data JSON object.
 * @override
 * <p>
 *  Usage sample:
 *  <code>
 *      anychart.visual.text.BaseTitle.deserialize.call(this, data);
 *  </code>
 * </p>
 * <p>
 *  Expected JSON structure:
 *  <code>
 *      enabled : boolean // Indicates whether chart title is visible.
 *      text : string // Node contains chart title text, it can be plain text or
 *      // CDATA block.
 *      align : string // Sets chart title align mode. If "align_by" attribute
 *      // is set to "DataPlot" title will be aligned by data plot area.
 *      padding : number // Sets padding between chart title and chart area, or
 *      //between chart title and chart subtitle (is subtitle is enabled)
 *      background : object // Deserialize by anychart.visual.text.TextElement
 *      rotation : number // Deserialize by anychart.visual.text.TextElement
 *      text_align : string // Deserialize by anychart.visual.text.TextElement
 *      font : object // Deserialize by anychart.visual.text.TextElement
 *  </code>
 * </p>
 */
anychart.visual.text.BaseTitle.prototype.deserialize = function (data) {
    //deserialize by TextElement
    goog.base(this, 'deserialize', data);
    //aliases
    var deserialization = anychart.utils.deserialization;
    //deserialize text
    var textParamName = (deserialization.hasProp(data, 'text')) ?
        'text' : 'format';
    if (deserialization.hasProp(data, textParamName))
        this.textFormatter_ = new anychart.formatting.
            TextFormatter(deserialization.
            getStringProp(data, textParamName));

    //deserialize align
    if (deserialization.hasProp(data, 'align')) {
        switch (deserialization.getLStringProp(data, 'align')) {
            case 'near':
            case 'left':
            case 'top':
                this.align_ = anychart.layout.AbstractAlign.NEAR;
                break;
            case 'far':
            case 'right':
            case 'bottom':
                this.align_ = anychart.layout.AbstractAlign.FAR;
                break;
            case 'center':
                this.align_ = anychart.layout.AbstractAlign.CENTER;
                break;
        }
    }
    //deserialize padding
    if (deserialization.hasProp(data, 'padding'))
        this.padding_ = deserialization.getNumProp(data, 'padding');
};
/** @inheritDoc */
anychart.visual.text.BaseTitle.prototype.createSVGText = function (svgManager) {
    this.sprite_ = goog.base(this, 'createSVGText', svgManager);
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * Must be empty.
 */
anychart.visual.text.BaseTitle.prototype.resize = function () {
};
//------------------------------------------------------------------------------
//
//                          InteractiveTitle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.ChartView} mainChartView
 * @param {anychart.formatting.ITextFormatInfoProvider} formatter
 * @extends {anychart.visual.text.BaseTitle}
 */
anychart.visual.text.InteractiveTitle = function (mainChartView, formatter) {
    anychart.visual.text.BaseTitle.call(this);
    this.mainChartView = mainChartView;
    this.formatter_ = formatter;
};
goog.inherits(anychart.visual.text.InteractiveTitle,
    anychart.visual.text.BaseTitle);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.actions.ActionsList}
 */
anychart.visual.text.InteractiveTitle.prototype.actions_ = null;
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.visual.text.InteractiveTitle.prototype.mainChartView = null;
/**
 * @private
 * @type {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.visual.text.InteractiveTitle.prototype.formatter_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.visual.text.InteractiveTitle.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    if (!this.enabled_) return;
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'actions')) {
        this.actions_ = new anychart.actions.ActionsList(this.mainChartView, this.formatter_);
        this.actions_.deserialize(des.getProp(data, 'actions'));
    }
};
//------------------------------------------------------------------------------
//                           Visual.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.visual.text.InteractiveTitle.prototype.createSVGText = function (svgManager) {
    if (this.actions_) this.disableEvents_ = false;
    goog.base(this, 'createSVGText', svgManager);
    if (this.actions_) {
        this.setVisualSettings(this.sprite_);
        this.initListeners(this.sprite_);
    }
    return this.sprite_;
};
/**
 * @protected
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.visual.text.InteractiveTitle.prototype.setVisualSettings = function (sprite) {
    sprite.getElement().setAttribute('cursor', 'pointer');
};
/**
 * @protected
 * @param {anychart.svg.SVGSprite} sprite
 */
anychart.visual.text.InteractiveTitle.prototype.initListeners = function (sprite) {
    goog.events.listen(
        sprite.getElement(),
        goog.events.EventType.CLICK,
        this.onMouseClick_,
        false,
        this);
};
/**
 * @private
 */
anychart.visual.text.InteractiveTitle.prototype.onMouseClick_ = function () {
    if (this.actions_) this.actions_.execute();
};
