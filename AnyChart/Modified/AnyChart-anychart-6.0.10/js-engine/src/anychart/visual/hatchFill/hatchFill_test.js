goog.require('anychart.visual.hatchFill');

$(document).ready(function() {

    module('visual/hatchFill/HatchFill');

    test('Empty hatch fill', function() {
        var hatchFill = new anychart.visual.hatchFill.HatchFill();
        ok(hatchFill != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/hatchFill/SVGHatchFillManager');

    test('Empty hatch fill manager', function() {
        var manager = new anychart.visual.hatchFill.SVGHatchFillManager();
        ok(manager != null, 'object exists');
    });

    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });
});

