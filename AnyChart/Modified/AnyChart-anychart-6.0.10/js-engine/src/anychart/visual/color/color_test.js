goog.require('anychart.visual.color');

$(document).ready(function() {

    module('visual/color/Color');
    test('Object exists', function() {
        var color = new anychart.visual.color.Color();
        ok(color != null, 'object exists');
    });
    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/color/ColorContainer');
    test('Object exists', function() {
        var color = new anychart.visual.color.ColorContainer(new anychart.visual.color.Color());
        ok(color != null, 'object exists');
    });
    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

    module('visual/color/colorParser');
    test('Object exists', function() {
        ok(anychart.visual.color.colorParser != null, 'object exists');
    });
    test('Test exists', function() {
        ok(false, 'no tests implemented');
    });

});