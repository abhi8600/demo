/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.styles.aqua.CircleAquaStyleState};.</li>
 * <ul>
 */
goog.provide('anychart.styles.aqua');
goog.require('anychart.styles.background');
goog.require('anychart.visual.gradient');
//------------------------------------------------------------------------------
//
//                          CircleAquaStyle class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleState}
 */
anychart.styles.aqua.CircleAquaStyleState = function (style, opt_stateType) {
    goog.base(this, style, opt_stateType);
};
goog.inherits(anychart.styles.aqua.CircleAquaStyleState,
    anychart.styles.background.BackgroundStyleState);
/**
 * ND: Needs doc!
 * @private
 * @type {*}
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.stroke_ = null;
/**
 * ND: Needs doc!
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.getStroke = function() {
    return this.stroke_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {*}
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.outerBorder_ = null;
/**
 * ND: Needs doc!
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.getOuterBorder = function() {
    return this.outerBorder_;
};
/**
 * ND: Needs doc!
 * @private
 * @type {*}
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.innerBorder_ = null;
/**
 * ND: Needs doc!
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.getInnerBorder = function() {
    return this.innerBorder_;
};

//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * ND: Needs doc!
 * @param {*} color
 * @param {*} opacity
 * @private
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.deserializeFill_ = function (color, opacity) {
    var fillData = {};
    var gradientData = {};
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};

    key1['position'] = '0';
    key1['opacity'] = opacity;
    key1['color'] = 'LightColor(' + color + ')';
    keys.push(key1);

    key2['position'] = 0.95;
    key2['opacity'] = opacity;
    key2['color'] = 'DarkColor(' + color + ')';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = opacity;
    key3['color'] = 'DarkColor(' + color + ')';
    keys.push(key3);

    gradientData['type'] = 'radial';
    gradientData['angle'] = -145;
    gradientData['focal_point'] = '0.5';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.fill_ = new anychart.visual.fill.Fill();
    this.fill_.deserialize(fillData);

    this.stroke_ = new anychart.visual.stroke.Stroke();
    this.stroke_.deserialize(fillData);
    this.stroke_.opacity_ = 0;
};
/**
 * ND: Needs doc!
 * @private
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.deserializeInnerBorder_ = function () {
    this.innerBorder_ = new anychart.visual.fill.Fill();
};

/**
 * ND: Needs doc!
 * @private
 */
anychart.styles.aqua.CircleAquaStyleState.prototype.deserializeOuterBorder_ = function (color, opacity) {
    var fillData = {};
    var gradientData = {};
    var keys = [];
    var key1 = {};
    var key2 = {};
    var key3 = {};

    key1['position'] = '0';
    key1['opacity'] = '0';
    key1['color'] = 'Black';
    keys.push(key1);

    key2['position'] = 0.95;
    key2['opacity'] = '0';
    key2['color'] = 'Black';
    keys.push(key2);

    key3['position'] = 1;
    key3['opacity'] = 100.0 / 255.0 * opacity;
    key3['color'] = 'Black';
    keys.push(key3);

    gradientData['type'] = 'radial';
    gradientData['angle'] = '0';
    gradientData['focal_point'] = '0';
    gradientData['key'] = keys;

    fillData['enabled'] = true;
    fillData['type'] = 'gradient';
    fillData['gradient'] = gradientData;

    this.outerBorder_ = new anychart.visual.fill.Fill();
    this.outerBorder_.deserialize(fillData);
};


//deserializeBorder
/** @inheritDoc */
anychart.styles.aqua.CircleAquaStyleState.prototype.deserialize = function (config) {
    config = goog.base(this, 'deserialize', config);
    var des = anychart.utils.deserialization;
    var color = des.getProp(des.getProp(config, 'fill'), 'color');
    var fillNode = des.getProp(config, 'fill');
    var opacity = des.hasProp(fillNode, 'opacity') ? des.getProp(fillNode, 'opacity') : 1;
    this.deserializeFill_(color, opacity);
    this.deserializeOuterBorder_(color, opacity);
    this.deserializeInnerBorder_();
    return config;
};

/**
 * ND: Needs doc!
 * @constructor
 *
 */
anychart.styles.aqua.CircleStyle = function() {};
/**
 * ND: Needs doc!
 * @constructor
 *
 */
anychart.styles.aqua.CircleShapes = function() {};
/**
 * ND: Needs doc!
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.aqua.CircleFillShape = function() {};
goog.inherits(anychart.styles.aqua.CircleFillShape,
    anychart.styles.StyleShape);
/**
 * Current style.
 * @private
 * @type {String}
 */
anychart.styles.aqua.CircleFillShape.prototype.currentStyle_ = null;

anychart.styles.aqua.CircleFillShape.prototype.updatePath = function() {
    var path = this.point_.getPathData();
    this.element_.setAttribute(
        'd',
        path);
};
anychart.styles.aqua.CircleFillShape.prototype.createElement = function() {
    return this.styleShapes_.createElement();
};
/**
 * ND: Needs doc!
 * @param {*} styleState
 */
anychart.styles.aqua.CircleFillShape.prototype.updateGradients = function(styleState) {
    var svgManager = this.point_.getSVGManager();

    if (this.currentStyle_ && this.currentStyle_.indexOf('fill:none') == -1) {
        var gm = svgManager.getGradientManager();
        gm.removeGradient(this.currentStyle_);
    }

    this.currentStyle_ = '';

    this.updateStyle(styleState);
};


/**@inheritDoc*/
anychart.styles.aqua.CircleFillShape.prototype.updateStyle = function(styleState) {
    var svgManager = this.point_.getSVGManager();
    var style = '';
    var color = this.point_.getColor().getColor();
    var bounds = this.point_.getBounds();

    style += styleState.getFill().getSVGFill(svgManager, bounds, color, true);
    this.currentStyle_ = style;

    this.element_.setAttribute('style', style);
};

/** @inheritDoc */
anychart.styles.aqua.CircleFillShape.prototype.updateEffects = function() {
    return;
};
