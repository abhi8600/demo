/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.styles.line.LineStyleShape};</li>
 *  <li>@class {anychart.styles.line.LineStyleShapes};</li>
 *  <li>@class {anychart.styles.line.LineStyleState};</li>
 *  <li>@class {anychart.styles.line.LineStyle};</li>
 * <ul>
 */
goog.provide('anychart.styles.line');
goog.require('anychart.styles');
//------------------------------------------------------------------------------
//
//                            LineStyleShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.StyleShape}
 */
anychart.styles.line.LineStyleShape = function() {
    anychart.styles.StyleShape.call(this);
};
goog.inherits(anychart.styles.line.LineStyleShape,
        anychart.styles.StyleShape);
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.line.LineStyleShape.prototype.createElement = function() {
    return this.styleShapes_.createElement();
};
/**@inheritDoc*/
anychart.styles.line.LineStyleShape.prototype.updateStyle = function(opt_styleState) {
    var svgManager = this.point_.getSVGManager();
    var style;

    if (opt_styleState.needUpdateStrokeShape()) {
        style = opt_styleState.getStroke().getSVGStroke(
                svgManager,
                this.point_.getBounds(),
                this.point_.getColor().getColor());
        style += svgManager.getEmptySVGFill();
    } else style = svgManager.getEmptySVGStyle();

    this.element_.setAttribute('style', style);
};
/**
 * @inheritDoc
 */
anychart.styles.line.LineStyleShape.prototype.updateEffects = function(styleState) {
    this.styleShapes_.updateEffects(this.element_, styleState);
};
//------------------------------------------------------------------------------
//
//                       LineStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.styles.line.LineStyleShapes = function() {
    anychart.styles.StyleShapes.call(this);
};
goog.inherits(anychart.styles.line.LineStyleShapes, anychart.styles.StyleShapes);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.styles.line.LineStyleShape}
 */
anychart.styles.line.LineStyleShapes.prototype.lineShape_ = null;
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.line.LineStyleShapes.prototype.createShapes = function() {
    if (this.point_.getStyle().needCreateStrokeShape()) {
        this.lineShape_ = new anychart.styles.line.LineStyleShape();
        this.lineShape_.initialize(this.point_, this);
    }
};
/**@inheritDoc*/
anychart.styles.line.LineStyleShapes.prototype.placeShapes = function() {
  if(this.lineShape_)
      this.point_.getSprite().appendChild(this.lineShape_.getElement());
};
//------------------------------------------------------------------------------
//                          Create element.
//------------------------------------------------------------------------------
/**
 * Create line element.
 * @return {SVGElement}
 */
anychart.styles.line.LineStyleShapes.prototype.createElement = goog.abstractMethod;
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.styles.line.LineStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if (this.lineShape_) this.lineShape_.update(opt_styleState, opt_updateData);
};
/**
 * Update element effects
 * @param {SVGElement} element
 * @param {anychart.styles.StyleStateWithEffects} styleState
 */
anychart.styles.line.LineStyleShapes.prototype.updateEffects = function(element, styleState) {
    if (styleState.needUpdateEffects())
        element.setAttribute('filter', styleState.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};
//------------------------------------------------------------------------------
//
//                          LineStyleState class
//
//------------------------------------------------------------------------------
/**
 * @public
 * @constructor
 * @param {anychart.styles.Style} style Style for this state.
 * @param {?anychart.styles.StateType} opt_stateType State type, default is NORMAL.
 * @extends {anychart.styles.StyleState}
 */
anychart.styles.line.LineStyleState = function(style, opt_stateType) {
    anychart.styles.StyleStateWithEffects.call(this, style, opt_stateType);
};
//inheritance
goog.inherits(anychart.styles.line.LineStyleState, anychart.styles.StyleStateWithEffects);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.stroke.Stroke}
 */
anychart.styles.line.LineStyleState.prototype.stroke_ = null;
/**
 * @return {anychart.visual.stroke.Stroke}
 */
anychart.styles.line.LineStyleState.prototype.getStroke = function() {
    return this.stroke_;
};
//------------------------------------------------------------------------------
//                          Definers.
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.styles.line.LineStyleState.prototype.needUpdateStrokeShape = function() {
    return this.stroke_ && this.stroke_.isEnabled();
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @public
 * @override
 * @param {object} data
 * @return {object}
 */
anychart.styles.line.LineStyleState.prototype.deserialize = function(data) {
    if (!data) return data;
    data = goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'line') && des.isEnabled(des.getProp(data, 'line'))) {
        this.stroke_ = new anychart.visual.stroke.Stroke();
        this.stroke_.deserialize(des.getProp(data, 'line'));
    }
    return data;
};
//------------------------------------------------------------------------------
//
//                              LineStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.styles.line.LineStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.styles.line.LineStyle, anychart.styles.Style);
//------------------------------------------------------------------------------
//                          Style state.
//------------------------------------------------------------------------------
anychart.styles.line.LineStyle.prototype.getStyleStateClass = function() {
    return anychart.styles.line.LineStyleState;
};
//------------------------------------------------------------------------------
//                          Shapes.
//------------------------------------------------------------------------------
anychart.styles.line.LineStyle.prototype.createStyleShapes = function() {
    return anychart.styles.line.LineStyleShapes();
};
//------------------------------------------------------------------------------
//                          Definers.
//------------------------------------------------------------------------------
/**
 * @return {Boolean}
 */
anychart.styles.line.LineStyle.prototype.needCreateStrokeShape = function() {
    return  this.normal_.needUpdateStrokeShape() ||
            this.hover_.needUpdateStrokeShape() ||
            this.pushed_.needUpdateStrokeShape() ||
            this.selectedNormal_.needUpdateStrokeShape() ||
            this.selectedHover_.needUpdateStrokeShape() ||
            this.missing_.needUpdateStrokeShape();
};
