/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.utils.geom.common}</li>
 *  <li>@class {anychart.utils.geom.DrawingUtils}</li>
 *  <li>@class {anychart.utils.geom.Point}</li>
 *  <li>@class {anychart.utils.geom.Rectangle}</li>
 *  <li>@class {anychart.utils.geom.RotationUtils}</li>
 *  <li>@class {anychart.utils.geom.MathUtils}</li>
 * <ul>
 */
goog.provide('anychart.utils.geom');
//----------------------------------------------------------------------------------------------------------------------
//
//                          Common class.
//
//----------------------------------------------------------------------------------------------------------------------

anychart.utils.geom.common = {};
/**
 * Converts angle from degrees to radians.
 * Usage sample:
 * <code>
 *   alert(anychart.utils.geom.common.degToRad(180) == Math.PI); // alerts true
 * </code>
 *
 * @param {!number} deg - angle in degrees
 * @return {Number} - angle in radians
 */
anychart.utils.geom.common.degToRad = function (deg) {
    return deg * Math.PI / 180;
};

/**
 * Converts angle from radians to degrees.
 * Usage sample:
 * <code>
 *   alert(anychart.utils.geom.common.radToDeg(Math.PI) == 180); // alerts true
 * </code>
 *
 * @param {!number} rad - angle in radians
 * @return {Number} - angle in degrees
 */
anychart.utils.geom.common.radToDeg = function (rad) {
    return Number(rad) * 180 / Math.PI;
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          DrawingUtils class.
//
//----------------------------------------------------------------------------------------------------------------------
/**
 *
 * @public
 * @static
 * @constructor
 */
anychart.utils.geom.DrawingUtils = function () {
};
/**
 * @param {anychart.svg.SVGManager} svgManager
 * @param {Number} centerX
 * @param {Number} centerY
 * @param {Number} startAngle
 * @param {Number} endAngle
 * @param {Number} hRadius
 * @param {Number} wRadius
 * @param {Boolean} needMoveTo Default: true
 */
anychart.utils.geom.DrawingUtils.drawArc = function(svgManager, centerX, centerY, startAngle, endAngle, hRadius, wRadius, needMoveTo) {
    if (needMoveTo == undefined) needMoveTo = true;
    var du = anychart.utils.geom.DrawingUtils;
    var path = '',
        x, y, angle = startAngle;
    x = centerX + du.getPointX(wRadius, angle);
    y = centerY + du.getPointY(hRadius, angle);

    if (needMoveTo)
        path += svgManager.pMove(x, y);
    else
        path += svgManager.pLine(x, y);

    var step = 1;
    if (endAngle < startAngle) step = -step;

    var doDraw = step > 0 ? angle < endAngle : angle > endAngle;
    while(doDraw) {
        x = centerX + du.getPointX(wRadius,angle);
        y = centerY + du.getPointY(hRadius,angle);
        path += svgManager.pLine(x,y);
        angle += step;
        doDraw = step > 0 ? angle < endAngle : angle > endAngle;
    }
    angle = endAngle;
    x = centerX + du.getPointX(wRadius,angle);
    y = centerY + du.getPointY(hRadius,angle);

    path += svgManager.pLine(x,y);
    return path;
};
/**
 * Draw dashed lien.
 * @public
 * @param {CanvasContext} context
 * @param {Number} gap
 * @param {Number} length
 * @param {anychart.utils.geom.Point} startPoint
 * @param {anychart.utils.geom.Point} endPoint
 */
anychart.utils.geom.DrawingUtils.drawDashedLine = function (context, gap, length, startPoint, endPoint) {
    var segLength = length + gap;
    var deltaX = endPoint.x - startPoint.x;
    var deltaY = endPoint.y - startPoint.y;
    var delta = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
    var seg = Math.floor(Math.abs(delta / segLength));
    var radians = Math.atan2(deltaY, deltaX);
    deltaX = Math.cos(radians) * segLength;
    deltaY = Math.sin(radians) * segLength;

    var dx = startPoint.x;
    var dy = startPoint.y;
    context.beginPath();
    for (var i = 0; i < seg; i++) {
        context.moveTo(dx, dy);
        context.lineTo(dx + Math.cos(radians) * length, dy + Math.sin(radians) * length);
        dx += deltaX;
        dy += deltaY;
    }
    context.moveTo(dx, dy);
    delta = Math.sqrt(Math.pow(endPoint.x - dx, 2) + Math.pow(endPoint.y - dy, 2));
    if (delta > length) context.lineTo(dx + Math.cos(radians) * length, dy + Math.sin(radians) * length);
    else if (delta > 0) context.lineTo(dx + Math.cos(radians) * delta, dy + Math.sin(radians) * delta);
    context.moveTo(endPoint.x, endPoint.y);
    context.closePath();
};
anychart.utils.geom.DrawingUtils.getPointX = function (wRadius, angle) {
    if (wRadius <= 0) return 0;
    return wRadius * Math.round(Math.cos(angle * Math.PI / 180) * 1e15) / 1e15;
};
anychart.utils.geom.DrawingUtils.getPointY = function (hRadius, angle) {
    if (hRadius <= 0) return 0;
    return hRadius * Math.round(Math.sin(angle * Math.PI / 180) * 1e15) / 1e15;
};
//------------------------------------------------------------------------------
//
//                          Point class.
//
//------------------------------------------------------------------------------
/**
 * 2D point class instances constructor.
 * Can also initialize point with a pair of coordinates.
 * Usage sample:
 * <code>
 *   var point = new anychart.utils.geom.Point(10, 10);
 *   alert(point.x); // 10
 *   alert(point.y); // 10
 * </code>
 *
 * @constructor
 * @public
 * @param {number=} opt_x (default 0)
 * @param {number=} opt_y (default 0)
 */
anychart.utils.geom.Point = function (opt_x, opt_y) {
    if (opt_x != undefined) this.x = Number(opt_x);
    if (opt_y != undefined) this.y = Number(opt_y);
};

/**
 * 2D point x coordinate
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Point.prototype.x = 0;

/**
 * 2D point y coordinate
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Point.prototype.y = 0;

/**
 * Clone point.
 * @public
 * @return {anychart.utils.geom.Point}
 */
anychart.utils.geom.Point.prototype.clone = function () {
    return new anychart.utils.geom.Point(this.x, this.y);
};

/**
 * Translates the point by given offsets.
 * Just an optimization to provide inline moving by both coordinates
 * at the same time.
 *
 * Usage sample:
 * <code>
 *   var point = new anychart.utils.geom.Point(10, 10);
 *   alert(point.x); // 10
 *   alert(point.y); // 10
 *   point.translate(10, -15);
 *   alert(point.x); // 20
 *   alert(point.y); // -5
 * </code>
 *
 * @public
 * @param {Number} dx
 * @param {Number} dy
 */
anychart.utils.geom.Point.prototype.translate = function (dx, dy) {
    this.x += dx;
    this.y += dy;
};
//------------------------------------------------------------------------------
//
//                          Rectangle class.
//
//------------------------------------------------------------------------------
/**
 * 2D rectangle class instances constructor. The default values for initial position and size are
 * left-top x: 0, left-top y: 0, width: 0, height: 0;
 * Usage sample:
 * <code>
 *      var rect = new anychart.utils.geom.Rectangle(10, 15, 25, 30);
 *      alert(rect.x); // 10
 *      alert(rect.y); // 15
 *      alert(rect.width); // 25
 *      alert(rect.height); // 30
 * </code>
 *
 * @constructor
 * @public
 * @param {number=} opt_x
 * @param {number=} opt_y
 * @param {number=} opt_w
 * @param {number=} opt_h
 */
anychart.utils.geom.Rectangle = function (opt_x, opt_y, opt_w, opt_h) {
    if (opt_x != undefined)this.x = Number(opt_x);
    if (opt_y != undefined)this.y = Number(opt_y);
    if (opt_w != undefined)this.width = Number(opt_w);
    if (opt_h != undefined)this.height = Number(opt_h);
};

/**
 * LeftTop corner x
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Rectangle.prototype.x = 0;

/**
 * LeftTop corner y
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Rectangle.prototype.y = 0;

/**
 * Rectangle width
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Rectangle.prototype.width = 0;

/**
 * Rectangle height
 *
 * @public
 * @type {Number}
 */
anychart.utils.geom.Rectangle.prototype.height = 0;

/**
 * Left rectangle coordinate getter (added for consistency)
 *
 * @public
 * @return {Number}
 */
anychart.utils.geom.Rectangle.prototype.getLeft = function () {
    return this.x;
};

/**
 * Top rectangle coordinate getter (added for consistency)
 *
 * @public
 * @return {Number}
 */
anychart.utils.geom.Rectangle.prototype.getTop = function () {
    return this.y;
};

/**
 * Right rectangle coordinate getter (added for consistency)
 *
 * @public
 * @return {Number}
 */
anychart.utils.geom.Rectangle.prototype.getRight = function () {
    return this.x + this.width;
};

/**
 * Bottom rectangle coordinate getter (added for consistency)
 *
 * @public
 * @return {Number}
 */
anychart.utils.geom.Rectangle.prototype.getBottom = function () {
    return this.y + this.height;
};
/**
 * Sets new left coordinate for rectangle without changing right coordinate
 * @param {Number} left New left coordinate.
 */
anychart.utils.geom.Rectangle.prototype.setLeft = function (left) {
    var dx = left - this.getLeft();
    this.x += dx;
    this.width -= dx;
};

/**
 * Sets new right coordinate for rectangle without changing left coordinate
 * @param {Number} right New right coordinate.
 */
anychart.utils.geom.Rectangle.prototype.setRight = function (right) {
    var dx = this.getRight() - right;
    this.width -= dx;
};

/**
 * Sets new top coordinate for rectangle without changing bottom coordinate
 * @param {Number} top New top coordinate.
 */
anychart.utils.geom.Rectangle.prototype.setTop = function (top) {
    var dy = top - this.getTop();
    this.y += dy;
    this.height -= dy;
};

/**
 * Sets new bottom coordinate for rectangle without changing top coordinate
 * @param {Number} bottom New bottom coordinate.
 */
anychart.utils.geom.Rectangle.prototype.setBottom = function (bottom) {
    var dy = this.getBottom() - bottom;
    this.height -= dy;
};

/**
 * Gets rectangle center as a 2D point
 *
 * @public
 * @return {anychart.utils.geom.Point}
 */
anychart.utils.geom.Rectangle.prototype.getCenterPoint = function () {
    return new anychart.utils.geom.Point(this.x + this.width / 2, this.y + this.height / 2);
};

/**
 * Checks rectangles intersection and returns the result of checking
 * Usage sample:
 * <code>
 *   var rect1 = new anychart.utils.geom.Rectangle(10, 10, 20, 20);
 *   var rect2 = new anychart.utils.geom.Rectangle(20, 20, 20, 20);
 *   alert(rect1.intersects(rect2)); // true
 *   rect2.translate(20, 20);
 *   alert(rect1.intersects(rect2)); // false
 * </code>
 *
 * @public
 * @param {anychart.utils.geom.Rectangle} otherRect
 * @return {boolean}
 */
anychart.utils.geom.Rectangle.prototype.intersects = function (otherRect) {
    return !(this.getRight() < otherRect.getLeft() ||
        this.getBottom() < otherRect.getTop() ||
        this.getLeft() > otherRect.getRight() ||
        this.getTop() > otherRect.getBottom());
};
/**
 * @param {anychart.utils.geom.Rectangle} innerRect
 * @return {Boolean}
 */
anychart.utils.geom.Rectangle.prototype.containsRect = function (innerRect) {
    return this.x <= innerRect.x && this.getRight() >= innerRect.getRight() &&
        this.y <= innerRect.y && this.getBottom() >= innerRect.getBottom();
};

/**
 * Test the point to be inside the rectangle or on it's border
 * Usage sample:
 * <code>
 *   var rect = new anychart.utils.geom.Rectangle(10, 10, 20, 20);
 *   var point = new anychart.utils.geom.Point(20, 20);
 *   alert(rect.hitTest(point)); //true
 * </code>
 *
 * @public
 * @param {anychart.utils.geom.Point} point
 * @return {boolean}
 */
anychart.utils.geom.Rectangle.prototype.hitTestPoint = function (point) {
    return this.hitTest(point.x, point.y);
};

anychart.utils.geom.Rectangle.prototype.hitTest = function (x, y) {
    return (this.getRight() >= x &&
        this.getBottom() >= y &&
        this.getLeft() <= x &&
        this.getTop() <= y);
};

/**
 * Translates the rectangle by given offsets
 * Usage sample:
 * <code>
 *   var rect1 = new anychart.utils.geom.Rectangle(10, 10, 20, 20);
 *   alert(rect1.left()); // 10
 *   alert(rect1.top()); // 10
 *   alert(rect1.right()); // 30
 *   alert(rect1.bottom()); // 30
 *   rect1.translate(20, 20);
 *   alert(rect1.left()); // 30
 *   alert(rect1.top()); // 30
 *   alert(rect1.right()); // 50
 *   alert(rect1.bottom()); // 50
 * </code>
 *
 * @public
 * @param {Number} dx
 * @param {Number} dy
 */
anychart.utils.geom.Rectangle.prototype.translate = function (dx, dy) {
    this.x += dx;
    this.y += dy;
};
/**
 * Increases the size of the Rectangle object by the specified amounts, in pixels.
 * The center point of the Rectangle object stays the same, and its size increases to the left and right by the
 * dx value, and to the top and the bottom by the dy value.
 * @param {Number} dx
 * @param {Number} dy
 */
anychart.utils.geom.Rectangle.prototype.inflate = function(dx, dy) {
    this.x -= dx;
    this.width += 2 * dx;

    this.y -= dy;
    this.height += 2 * dy;
};

/**
 * Clones the rectangle.
 * Usage sample:
 * <code>
 *   var rect1 = new anychart.utils.geom.Rectangle(10, 10, 20, 20);
 *   var rect2 = rect1.clone();
 *   rect1.x = 20;
 *   alert(rect1.x); // 10
 * </code>
 *
 * @public
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.utils.geom.Rectangle.prototype.clone = function () {
    return new anychart.utils.geom.Rectangle(this.x, this.y, this.width, this.height);
};

anychart.utils.geom.Rectangle.prototype.toString = function () {
    return  this.x.toFixed(1) + ' ' +
        this.y.toFixed(1) + ' ' +
        this.width.toFixed(1) + ' ' +
        this.height.toFixed(1);
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          RotationUtils class.
//
//----------------------------------------------------------------------------------------------------------------------
anychart.utils.geom.Quarter = {
    FIRST:0,
    SECOND:1,
    THIRD:2,
    FOURTH:3,
    getQuarter:function (rotation) {
        var sinPositive = rotation.sin >= 0;
        var cosPositive = rotation.cos >= 0;
        if (sinPositive && cosPositive)
            return anychart.utils.geom.Quarter.FIRST;
        if (sinPositive && !cosPositive)
            return anychart.utils.geom.Quarter.SECOND;
        if (!cosPositive) return anychart.utils.geom.Quarter.THIRD;
        return anychart.utils.geom.Quarter.FOURTH;
    }
};
//----------------------------------------------------------------------------------------------------------------------
//
//                          RotationUtils class.
//
//----------------------------------------------------------------------------------------------------------------------
/**
 * Class provide methods for calculating rotation params.
 * @public
 * @constructor
 * <p>
 * Usage sample:
 * <code>
 *  var rotate = anychart.utils.geom.RotationUtils();
 *  rotate.calculateRotationParam(20);
 *  alert(rotate.radRotation);
 *  alert(rotate.sin);
 *  alert(rotate.cos);
 *  alert(rotate.absSin);
 *  alert(rotate.absCos);
 *  var rotatedBounds = rotate.getRotatedBounds(bounds);
 * </code>
 * </p>
 */
anychart.utils.geom.RotationUtils = function () {
};
//-----------------------------------------------------------------------------------
//
//                          Settings
//
//-----------------------------------------------------------------------------------
/**
 * Rotation angle.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.degRotation = 0;

/**
 * Rotation angle in radian.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.radRotation = 0;

/**
 * Rotation sine.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.sin = 0;

/**
 * Rotation cosine.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.cos = 1;

/**
 * Rotation sine module.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.absSin = 0;


/**
 * Rotation cosine module.
 * @public
 * @type {Number}
 */
anychart.utils.geom.RotationUtils.prototype.absCos = 1;

/**
 * Calculate rotation param: radRotation, sine, cosine, sine module, cosine module.
 * @public
 */
anychart.utils.geom.RotationUtils.prototype.calculate = function () {
    if (Math.abs(this.degRotation) >= 360)
        this.degRotation %= 360;

    if (this.degRotation < 0)
        this.degRotation += 360;

    this.radRotation = anychart.utils.geom.common.degToRad(this.degRotation);
    this.sin = Math.sin(this.radRotation);
    this.cos = Math.cos(this.radRotation);
    this.absSin = this.sin < 0 ? -this.sin : this.sin;
    this.absCos = this.cos < 0 ? -this.cos : this.cos;
};

/**
 * Return rotated bounds by non rotated.
 * @public
 * @param {anychart.utils.geom.Rectangle} bounds Non rotated bounds.
 * @return {anychart.utils.geom.Rectangle}
 * <p>
 * Usage sample:
 * <code>
 *  var rotate = anychart.utils.geom.RotationUtils();
 *  rotate.calculateRotationParam(20);
 *  var rotatedBounds = rotate.getRotatedBounds(bounds);
 * </code>
 * </p>
 */
anychart.utils.geom.RotationUtils.prototype.getRotatedBounds = function (bounds) {
    var res = bounds.clone();
    if (!isNaN(this.degRotation) && this.degRotation != 0) {
        var w = res.width;
        var h = res.height;
        res.width = w * this.absCos + h * this.absSin;
        res.height = w * this.absSin + h * this.absCos;
    }
    return res;
};

anychart.utils.geom.RotationUtils.prototype.setRotationOffset = function (position, nonRotatedBounds) {
    var dx = 0;
    var dy = 0;
    switch (anychart.utils.geom.Quarter.getQuarter(this)) {
        case anychart.utils.geom.Quarter.FIRST :
            dx = nonRotatedBounds.height * this.sin;
            break;
        case anychart.utils.geom.Quarter.SECOND :
            dx = nonRotatedBounds.width * this.absCos + nonRotatedBounds.height * this.absSin;
            dy = nonRotatedBounds.height * this.absCos;
            break;
        case anychart.utils.geom.Quarter.THIRD :
            dx = nonRotatedBounds.width * this.absCos;
            dy = nonRotatedBounds.width * this.absSin + nonRotatedBounds.height * this.absCos;
            break;
        case anychart.utils.geom.Quarter.FOURTH :
            dx = 0;
            dy = nonRotatedBounds.width * this.absSin;
            break;
    }
    position.x += dx;
    position.y += dy;
};

/**
 * Содержит неадекватные математические операции, но тем не мение прекрасно работающие.
 * В основном это обход косяков ECMAScript в работе с float значениями.
 * @constructor
 */
anychart.utils.geom.MathUtils = function () {
};

/**
 * Define, contains passed value in passed range.
 * <p>
 *     Note: Ввиду того что ECMA скрипт криво оперирует с дробными числами с любым количеством знаков после запятой (0,95 - 0,5 = 0.44999999999999996),
 *     вводим операцию сравнения бесконечно малых чисел.
 *     expValue - Степень переданного числа value. Смысл в отсечении милионной доли числа.
 *     Если порядок числа больше тысячи то делем на порядкок.
 *     val Ни в коем случае не должен быть равен нулю, т.к сравнение нуля с бесконечно мылым числом приведет к фейлу.
 * </p>
 * @param {Number} startValue Start range value.
 * @param {Number} endValue End range value.
 * @param {Number} searchValue Target value.
 * @return {boolean}
 */

anychart.utils.geom.MathUtils.contains = function (startValue, endValue, searchValue) {
    var val = searchValue;
    if (val == 0) val = startValue;
    if (val == 0) val = endValue;
    var expValue = Math.pow(10,
        (Math.max(6,
            Math.floor(Math.log(Math.abs(val)) / Math.log(10)) + 3) ));
    return (searchValue > startValue ||
        Math.abs(searchValue - startValue) <= Math.abs(val) / expValue) &&
        (searchValue < endValue ||
            Math.abs(searchValue - endValue) <=
                Math.abs(val) / expValue);
};
