/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.actions.Action}</li>
 *  <li>@class {anychart.actions.NavigateToURLAction}</li>
 *  <li>@class {anychart.actions.CallAction}</li>
 *  <li>@class {anychart.actions.ChartAction}</li>
 *  <li>@class {anychart.actions.UpdateChartAction}</li>
 *  <li>@class {anychart.actions.UpdateDashboardAction}</li>
 *  <li>@class {anychart.actions.ActionsList}</li>
 * <ul>
 */
goog.provide('anychart.actions');
goog.require('anychart.formatting');
//------------------------------------------------------------------------------
//
//                          Action class.
//
//------------------------------------------------------------------------------
/**
 * Base class for all actions.
 * @constructor
 * @param {anychart.chartView.MainChartView}
    */
anychart.actions.Action = function (mainChartView) {
    this.mainChartView_ = mainChartView;
    this.isValidData_ = false;
};
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.actions.Action.prototype.mainChartView_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.actions.Action.prototype.isValidData_ = null;
/**
 * @return {Boolean}
 */
anychart.actions.Action.prototype.isValidData = function () {
    return this.isValidData_;
};
//------------------------------------------------------------------------------
//                        Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize action settings.
 * @param {Object} data JSON object with action settings.
 */
anychart.actions.Action.prototype.deserialize = goog.abstractMethod;
//------------------------------------------------------------------------------
//                       Execute.
//------------------------------------------------------------------------------
/**
 * Execute action.
 * @param {anychart.formatting.ITextFormatInfoProvider} formatterProvider Action formatting provider.
 * @return {Boolean}
 */
anychart.actions.Action.prototype.execute = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                    NavigateToURLAction class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.MainChartView}
    * @extends {anychart.actions.Action}
 */
anychart.actions.NavigateToURLAction = function (mainChartView) {
    anychart.actions.Action.call(this, mainChartView);
};
goog.inherits(anychart.actions.NavigateToURLAction, anychart.actions.Action);
//------------------------------------------------------------------------------
//                      Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.actions.NavigateToURLAction.prototype.url_ = null;
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.actions.NavigateToURLAction.prototype.urlFormatter_ = null;
/**
 * @private
 * @type {String}
 */
anychart.actions.NavigateToURLAction.prototype.urlTarget_ = null;
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.actions.NavigateToURLAction.prototype.urlTargetFormatter_ = null;
//------------------------------------------------------------------------------
//                      Deserialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.NavigateToURLAction.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    this.isValidData_ = des.hasProp(data, 'url');

    if (!this.isValidData_) return;
    //url
    this.urlFormatter_ = new anychart.formatting.TextFormatter(des.getStringProp(data, 'url'));

    //url target
    var urlTarget = '_blank';
    if (des.hasProp(data, 'target'))
        urlTarget = des.getStringProp(data, 'target');

    if (des.hasProp(data, 'url_target'))
        urlTarget = des.getStringProp(data, 'url_target');

    this.urlTargetFormatter_ = new anychart.formatting.TextFormatter(urlTarget);
};
//------------------------------------------------------------------------------
//                      Execute.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.NavigateToURLAction.prototype.execute = function (formatProvider) {
    switch (this.urlTargetFormatter_.getValue(formatProvider)) {
        default:
        case '_blank' :
            window.open(this.urlFormatter_.getValue(formatProvider));
            break;
        case '_self' :
            window.location.href = this.urlFormatter_.getValue(formatProvider);
            break;
    }
    return false;
};
//------------------------------------------------------------------------------
//
//                          CallAction class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.actions.Action}
 */
anychart.actions.CallAction = function (mainChartView) {
    anychart.actions.Action.call(this, mainChartView);
};
goog.inherits(anychart.actions.CallAction, anychart.actions.Action);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.actions.CallAction.prototype.functionNameFormatter_ = null;
/**
 * @private
 * @type {Array.<anychart.formatting.TextFormatter>}
 */
anychart.actions.CallAction.prototype.args_ = null;
//------------------------------------------------------------------------------
//                       Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.CallAction.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'function')) {
        this.isValidData_ = true;
        this.functionNameFormatter_ = new anychart.formatting.TextFormatter(des.getProp(data, 'function'));
    }

    if (!this.isValidData_) return;

    if (des.hasProp(data, 'arg')) {
        this.args_ = [];
        var args = des.getPropArray(data, 'arg');

        var argCount = args.length;
        for (var i = 0; i < argCount; i++) {
            var prop;
            if (anychart.utils.JSONUtils.isSimpleProperty(args[i])) prop = args[i];
            else prop = des.getProp(args[i], 'value');

            this.args_.push(new anychart.formatting.TextFormatter(prop));
        }
    }
};
//------------------------------------------------------------------------------
//                          Execute.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.CallAction.prototype.execute = function (formatProvider) {
    var functionName = this.functionNameFormatter_.getValue(formatProvider);

    var i;
    var argsCount = this.args_.length;
    var args = '';

    for (i = 0; i < argsCount; i++) {
        args += '"' + this.args_[i].getValue(formatProvider) + '"';
        if (argsCount > 1 && i != argsCount - 1) {
            args += ', ';
        }

    }

    eval(functionName + '.call(null,' + args + ');');

    return false;
};
//------------------------------------------------------------------------------
//
//                          ChartAction class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.actions.Action}
 */
anychart.actions.ChartAction = function (mainChartView) {
    anychart.actions.Action.call(this, mainChartView);
};
goog.inherits(anychart.actions.ChartAction, anychart.actions.Action);
//------------------------------------------------------------------------------
//                          Source mod.
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.actions.ChartAction.SourceMod = {
    SOURCE_EXTERNAL:0,
    SOURCE_INTERNAL:1
};
/**
 * @param {Object} data
 */
anychart.actions.ChartAction.SourceMod.deserialize = function (data) {
    switch (data) {
        case 'externaldata' :
            return anychart.actions.ChartAction.SourceMod.SOURCE_EXTERNAL;
        case 'internaldata' :
            return anychart.actions.ChartAction.SourceMod.SOURCE_INTERNAL;
        default :
            return null;
    }
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.actions.ChartAction.prototype.sourceTextFormatter_ = null;
/**
 * @private
 * @type {Array.<anychart.formatting.TextFormatter>}
 */
anychart.actions.ChartAction.prototype.replaces_ = null;
/**
 * @private
 * @type {String}
 */
anychart.actions.ChartAction.prototype.sourceMode_ = null;
//------------------------------------------------------------------------------
//                        Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.ChartAction.prototype.deserialize = function (data) {
    var des = anychart.utils.deserialization;
    this.isValidData_ = des.hasProp(data, 'source') && des.hasProp(data, 'source_mode');

    if (!this.isValidData_) return;

    this.sourceMode_ = anychart.actions.ChartAction.SourceMod.deserialize(des.getEnumProp(data, 'source_mode'));
    this.sourceTextFormatter_ = new anychart.formatting.TextFormatter(des.getStringProp(data, 'source'));

    if (des.hasProp(data, 'replace')) {
        var replace = des.getPropArray(data, 'replace');
        var replacesCount = replace.length;
        this.replaces_ = [];

        for (var i = 0; i < replacesCount; i++) {
            var token = replace[i];
            var tokenValue = des.getStringProp(token, 'value');
            var template = des.getStringProp(token, 'token');

            this.replaces_.push([template, new anychart.formatting.TextFormatter(tokenValue)]);
        }
    }
};
//------------------------------------------------------------------------------
//                              Execute.
//------------------------------------------------------------------------------
/**
 * @param {anychart.formatting.TextFormatter} formatProvider
 * @return {String}
 */
anychart.actions.ChartAction.prototype.getSource = function (formatProvider) {
    return this.sourceTextFormatter_.getValue(formatProvider);
};
/**
 * @param {anychart.formatting.TextFormatter} formatProvider
 * @return {Array.<String>}
 */
anychart.actions.ChartAction.prototype.getReplaces = function (formatProvider) {
    if (!this.replaces_) return null;
    var tokens = [];
    var replaceCount = this.replaces_.length;

    for (var i = 0; i < replaceCount; i++) {
        tokens.push([this.replaces_[i][0], this.replaces_[i][1].getValue(formatProvider)]);
    }

    return tokens;
};
//------------------------------------------------------------------------------
//
//                     UpdateChartAction class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.actions.ChartAction}
 */
anychart.actions.UpdateChartAction = function (mainChartView) {
    anychart.actions.ChartAction.call(this, mainChartView);
};
goog.inherits(anychart.actions.UpdateChartAction, anychart.actions.ChartAction);
/**
 * @inheritDoc
 */
anychart.actions.UpdateChartAction.prototype.execute = function (formatProvider) {
    if (this.sourceMode_ == anychart.actions.ChartAction.SourceMod.SOURCE_EXTERNAL)
        this.mainChartView_.setXMLFile(this.getSource(formatProvider), this.getReplaces(formatProvider), false);
    else
        this.mainChartView_.setSource(this.getSource(formatProvider), this.getReplaces(formatProvider), false);

    return false;
};
//------------------------------------------------------------------------------
//
//                     UpdateDashboardAction class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.actions.ChartAction}
 */
anychart.actions.UpdateDashboardAction = function (mainChartView) {
    anychart.actions.ChartAction.call(this, mainChartView);
};
goog.inherits(anychart.actions.UpdateDashboardAction, anychart.actions.ChartAction);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.formatting.TextFormatter}
 */
anychart.actions.UpdateDashboardAction.prototype.viewIdFormatter_ = null;
//------------------------------------------------------------------------------
//                          Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.UpdateDashboardAction.prototype.deserialize = function (data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    this.isValidData_ = this.isValidData_ && (des.hasProp(data, 'view') || des.hasProp(data, 'view_id'));

    if (!this.isValidData_) return;

    var viewId = des.hasProp(data, 'view_id') ? des.getStringProp(data, 'view_id') : des.getStringProp(data, 'view');
    this.viewIdFormatter_ = new anychart.formatting.TextFormatter(viewId);
};
//------------------------------------------------------------------------------
//                            Execute.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.actions.UpdateDashboardAction.prototype.execute = function (formatProvider) {
    var view = this.viewIdFormatter_.getValue(formatProvider);

    if (this.sourceMode_ == anychart.actions.ChartAction.SourceMod.SOURCE_EXTERNAL)
        this.mainChartView_.setViewXMLFile(view, this.getSource(formatProvider), this.getReplaces(formatProvider));
    else
        this.mainChartView_.setViewSource(view, this.getSource(formatProvider), this.getReplaces(formatProvider));

    return false;
};
//------------------------------------------------------------------------------
//
//                        ActionsList class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.MainChartView} mainChartView
 * @param {anychart.formatting.ITextFormatInfoProvider} formatProvider
 */
anychart.actions.ActionsList = function (mainChartView, formatProvider) {
    this.mainChartView_ = mainChartView;
    this.formatProvider_ = formatProvider;
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.MainChartView}
 */
anychart.actions.ActionsList.prototype.mainChartView_ = null;
/**
 * @private
 * @type {anychart.formatting.ITextFormatInfoProvider}
 */
anychart.actions.ActionsList.prototype.formatProvider_ = null;
/**
 * @private
 * @type {Array.<anychart.actions.Action>}
 */
anychart.actions.ActionsList.prototype.actions_ = null;
/**
 * Define, has actions in actions list.
 * @return {Boolean}
 */
anychart.actions.ActionsList.prototype.hasActions = function () {
    return this.actions_ && this.actions_.length > 0;
};
//------------------------------------------------------------------------------
//                  Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize actions.
 * @param {Object} data JSON object with actions settings.
 */
anychart.actions.ActionsList.prototype.deserialize = function (data) {
    this.actions_ = [];
    var des = anychart.utils.deserialization;

    var actionsData = des.getPropArray(data, 'action');
    var actionsCount = actionsData.length;

    for (var i = 0; i < actionsCount; i++) {
        var action = this.createAction_(actionsData[i]);
        if (action) this.actions_.push(action);
    }

};
/**
 * Create action by name.
 * @private
 * @param {Object} data
 * @return {anychart.actions.Action}
 */
anychart.actions.ActionsList.prototype.createAction_ = function (data) {
    var action = null;
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'type')) {
        switch (des.getEnumProp(data, 'type')) {
            case 'navigatetourl' :
                action = new anychart.actions.NavigateToURLAction(this.mainChartView_);
                break;
            case 'call' :
                action = new anychart.actions.CallAction(this.mainChartView_);
                break;

            case 'updatechart' :
                action = new anychart.actions.UpdateChartAction(this.mainChartView_);
                break;

            case 'updateview' :
                action = new anychart.actions.UpdateDashboardAction(this.mainChartView_);
                break;

            case 'scroll' :
                anychart.errors.featureNotSupported('Action scroll');
                break;

            case 'zoom' :
                anychart.errors.featureNotSupported('Action zoom');
                break;

            case 'scrollview' :
                anychart.errors.featureNotSupported('Action scrollView');
                break;

            case 'zoomView' :
                anychart.errors.featureNotSupported('Action zoomView');
                break;

            default:
                return null;
        }
        action.deserialize(data);
        if (!action.isValidData()) action = null;
    }

    return action;
};
//------------------------------------------------------------------------------
//                  Execute actions.
//------------------------------------------------------------------------------
/**
 * Execute all action in actions list.
 * @param {anychart.formatting.ITextFormatInfoProvider} opt_formatProvider
 */
anychart.actions.ActionsList.prototype.execute = function (opt_formatProvider) {
    if (opt_formatProvider) this.formatProvider_ = opt_formatProvider;

    var actionsCount = this.actions_.length;
    var res = false;

    for (var i = 0; i < actionsCount; i++)
        res = res || this.actions_[i].execute(this.formatProvider_);

    return res;
};










