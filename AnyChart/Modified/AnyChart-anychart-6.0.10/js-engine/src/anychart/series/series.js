goog.provide('anychart.series');

/**
 * Possible series types
 * @enum {int}
 * @return {Boolean} Is series type are line, spline, step_line_forward or
 * step_line_backward.
 */
anychart.series.SeriesType = {
    BAR: 0,
    RANGE_BAR: 1,

    LINE: 2,
    SPLINE: 3,
    STEP_LINE_FORWARD: 4,
    STEP_LINE_BACKWARD: 5,

    AREA: 6,
    SPLINE_AREA: 7,
    STEP_AREA_FORWARD: 8,
    STEP_AREA_BACKWARD: 9,
    RANGE_AREA: 10,
    RANGE_SPLINE_AREA: 11,

    BUBBLE: 12,

    OHLC: 13,
    CANDLESTICK: 14,

    MAP_REGION: 15,
    PIE: 16,

    CONNECTOR: 17,
    MARKER: 18,

    FUNNEL: 19,
    TABLE: 20,
    CONE: 21,
    PYRAMID: 22,

    HEAT_MAP: 23,

    TREE_MAP: 24,

    isLine: function(type) {
        return type == anychart.series.SeriesType.LINE ||
                type == anychart.series.SeriesType.SPLINE ||
                type == anychart.series.SeriesType.STEP_LINE_FORWARD ||
                type == anychart.series.SeriesType.STEP_LINE_BACKWARD;
    }
};
