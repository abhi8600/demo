/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains interfaces:
 * <ul>
 *  <li>@class {anychart.thresholds.IThreshold}</li>
 *  <li>@class {anychart.thresholds.IAutomaticThreshold}</li>
 *  <li>@class {anychart.thresholds.IFormattableThresholdItem}</li>
 * <ul>
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.thresholds.Threshold}</li>
 *  <li>@class {anychart.thresholds.CustomThresholdCondition}</li>
 *  <li>@class {anychart.thresholds.BetweenCondition}</li>
 *  <li>@class {anychart.thresholds.NotBetweenCondition}</li>
 *  <li>@class {anychart.thresholds.EqualCondition}</li>
 *  <li>@class {anychart.thresholds.NotEqualToCondition}</li>
 *  <li>@class {anychart.thresholds.GreaterThanCondition}</li>
 *  <li>@class {anychart.thresholds.GreaterThanOrEqualToCondition}</li>
 *  <li>@class {anychart.thresholds.LessThanCondition}</li>
 *  <li>@class {anychart.thresholds.LessThanOrEqualToCondition}</li>
 *  <li>@class {anychart.thresholds.CustomThresholdConditionFactory}</li>
 *  <li>@class {anychart.thresholds.CustomThreshold}</li>
 *  <li>@class {anychart.thresholds.AutomaticThresholdRange}</li>
 *  <li>@class {anychart.thresholds.AutomaticThreshold}</li>
 *  <li>@class {anychart.thresholds.CollectionPointInfo}</li>
 *  <li>@class {anychart.thresholds.CollectionBasedThreshold}</li>
 *  <li>@class {anychart.thresholds.EqualIntervalThreshold}</li>
 *  <li>@class {anychart.thresholds.EqualDistributionThreshold}</li>
 *  <li>@class {anychart.thresholds.OptimalThreshold}</li>
 *  <li>@class {anychart.thresholds.ThresholdsList}</li>
 * <ul>
 */
//namespace
goog.provide('anychart.thresholds');
goog.require('anychart.utils.geom');
//------------------------------------------------------------------------------
//
//                          IThresholds interface
//
//------------------------------------------------------------------------------
/**
 * @interface
 */
anychart.thresholds.IThreshold = function() {
};

/**
 * Return threshold name.
 * @return {String} Threshold name.
 */
anychart.thresholds.IThreshold.prototype.getName = function() {
};

/**
 * Check threshold correctly deserialization fot point.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point instance.
 */
anychart.thresholds.IThreshold.prototype.checkAfterDeserialize = function(point) {

};

/**
 * Check relevance point settings relatively threshold settings.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point instance.
 */
anychart.thresholds.IThreshold.prototype.checkBeforeDraw = function(point) {

};

/**
 * Define, is threshold automatic.
 * @return {Boolean} Is automatic value.
 */
anychart.thresholds.IThreshold.prototype.isAutomatic = function() {
};
//todo:write description
anychart.thresholds.IThreshold.prototype.getData = function() {
};
//------------------------------------------------------------------------------
//
//                          IAutomaticThresholds interface
//
//------------------------------------------------------------------------------
/**
 * @interface
 * @extends {anychart.thresholds.IThreshold}
 */
anychart.thresholds.IAutomaticThreshold = function() {
};
goog.inherits(anychart.thresholds.IAutomaticThreshold, anychart.thresholds.IThreshold);
/**
 * Initialize automatic threshold.
 */
anychart.thresholds.IAutomaticThreshold.prototype.initialize = function() {
};

/**
 * Set color palette to threshold
 */
anychart.thresholds.IAutomaticThreshold.prototype.setPalette = function(value) {
};
/**
 * Return threshold color range count.
 * @return {int}
 */
anychart.thresholds.IAutomaticThreshold.prototype.getRangeCount = function() {
};
//------------------------------------------------------------------------------
//
//                          IFormattableThresholdItem interface
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.thresholds.IFormattableThresholdItem = function() {
};
anychart.thresholds.IFormattableThresholdItem.prototype.hasToken = function(token) {
};
anychart.thresholds.IFormattableThresholdItem.prototype.getTokenValue = function(token) {
};
anychart.thresholds.IFormattableThresholdItem.prototype.isCustomAttribute = function(token) {
};
anychart.thresholds.IFormattableThresholdItem.prototype.getCustomAttribute = function(token) {
};
//------------------------------------------------------------------------------
//
//                          Threshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @implements {anychart.thresholds.IThreshold}
 */
anychart.thresholds.Threshold = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Threshold name.
 * @private
 * @type {String}
 */
anychart.thresholds.Threshold.prototype.name_ = null;

/**
 * @inheritDoc
 */
anychart.thresholds.Threshold.prototype.getName = function() {
    return this.name_;
};

/**
 * Threshold tokens hash.
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.thresholds.Threshold.prototype.tokensHash_ = null;
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * Deserialize threshold settings.
 * @param {Object} data JSON data with settings.
 */
anychart.thresholds.Threshold.prototype.deserialize = function(data) {
    if (anychart.utils.deserialization.hasProp(data, 'name')) {
        this.name_ = anychart.utils.deserialization.getStringProp(data, 'name');
        this.tokensHash_.add('%ThresholdName', this.name_);
    }
};
//------------------------------------------------------------------------------
//                     IThreshold implementation
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.thresholds.Threshold.prototype.checkBeforeDraw = function(point) {
    goog.abstractMethod();
};
/**
 * @inheritDoc
 */
anychart.thresholds.Threshold.prototype.checkAfterDeserialize = function(point) {
    goog.abstractMethod();
};

/**
 * @inheritDoc
 */
anychart.thresholds.Threshold.prototype.isAutomatic = function() {
    return false;
};

/**
 * Set point color settings.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point instance
 * @param {anychart.visual.color.Color} color Target color.
 */
anychart.thresholds.Threshold.prototype.setPointColor = function(point, color) {
    point.setColor(color);
};
anychart.thresholds.Threshold.prototype.getData = function(container) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
anychart.thresholds.Threshold.prototype.initFormatting = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
anychart.thresholds.Threshold.prototype.setDefaultTokenValues = function() {
    goog.abstractMethod();
};
anychart.thresholds.Threshold.prototype.setTokensValue = function() {
    goog.abstractMethod();
};
anychart.thresholds.Threshold.prototype.getTokenValue = function(token) {
    return this.tokensHash_.get(token);
};
/**
 * ND: Needs doc!
 * @param {String} token ND: Needs doc!
 */
anychart.thresholds.Threshold.prototype.getTokenType = goog.abstractMethod;
/**
 * ND: Needs doc!
 * @param {String} token ND: Needs doc!
 */
anychart.thresholds.Threshold.prototype.isCustomAttribute = function(token) {return false};

anychart.thresholds.Threshold.prototype.checkDynamicValue = function(value) {
    value = value.split('{').join('');
    return value.split('}').join('');
};
//------------------------------------------------------------------------------
//
//              CustomThresholdCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @implements {anychart.thresholds.IFormattableThresholdItem}
 */
anychart.thresholds.CustomThresholdCondition = function() {
    this.color_ = anychart.utils.deserialization.getColor('0#FF0000', 1);
    this.name_ = 'condition';
    this.points_ = [];
    this.initFormatting();
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * First threshold value.
 * @private
 * @type {String}
 */
anychart.thresholds.CustomThresholdCondition.prototype.value1_ = null;
/**
 * Define, is first threshold value dynamic.
 * @private
 * @type {Boolean}
 */
anychart.thresholds.CustomThresholdCondition.prototype.isDynamicValue1_ = false;
/**
 * Second threshold value.
 * @private
 * @type {String}
 */
anychart.thresholds.CustomThresholdCondition.prototype.value2_ = null;
/**
 * Define, is second threshold value dynamic.
 * @private
 * @type {Boolean}
 */
anychart.thresholds.CustomThresholdCondition.prototype.isDynamicValue2_ = false;
/**
 * Third threshold value.
 * @private
 * @type {String}
 */
anychart.thresholds.CustomThresholdCondition.prototype.value3_ = null;
/**
 * Define, is third threshold value dynamic.
 * @private
 * @type {Boolean}
 */
anychart.thresholds.CustomThresholdCondition.prototype.isDynamicValue3_ = false;

/**
 * Condition name.
 * @private
 * @type {String}
 */
anychart.thresholds.CustomThresholdCondition.prototype.name_ = null;

/**
 * Condition color
 * @private
 * @type {anychart.visual.color.Color}
 */
anychart.thresholds.CustomThresholdCondition.prototype.color_ = null;
/**
 * @return {anychart.visual.color.Color}
 */
anychart.thresholds.CustomThresholdCondition.prototype.getColor = function() {
    return this.color_;
};

/**
 * Link to custom threshold instance.
 * @private
 * @type {anychart.thresholds.CustomThreshold}
 */
anychart.thresholds.CustomThresholdCondition.prototype.threshold_ = null;
/**
 * Set link to custom threshold instance.
 * @private
 * @param {anychart.thresholds.CustomThreshold} value Link to threshold.
 */
anychart.thresholds.CustomThresholdCondition.prototype.setThreshold = function(value) {
    this.threshold_ = value;
};
/**
 * Condition points.
 * @private
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.thresholds.CustomThresholdCondition.prototype.points_ = null;

/**
 * Custom attributes map, key is attribute name.
 * @private
 * @type {Array.<String>}
 */
anychart.thresholds.CustomThresholdCondition.prototype.attributes_ = null;
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * Deserialize condition settings
 * @param {Object} data JSON object with condition settings.
 */
anychart.thresholds.CustomThresholdCondition.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'color')) this.color_ = des.getColorProp(data, 'color');
    if (des.hasProp(data, 'name')) this.name_ = des.getStringProp(data, 'name');

    var tempVal;
    if (des.hasProp(data, 'value_1')) {
        tempVal = des.getStringProp(data, 'value_1');
        this.isDynamicValue1_ = this.tokensHash_.isToken(tempVal);
        this.value1_ = tempVal;
    }

    if (des.hasProp(data, 'value_2')) {
        tempVal = des.getStringProp(data, 'value_2');
        this.isDynamicValue2_ = this.tokensHash_.isToken(tempVal);
        this.value2_ = tempVal;
    }

    if (des.hasProp(data, 'value_3')) {
        tempVal = des.getStringProp(data, 'value_3');
        this.isDynamicValue3_ = this.tokensHash_.isToken(tempVal);
        this.value3_ = tempVal;
    }

    if (des.hasProp(data, 'attributes')) {
        var attributeData = des.getPropArray(des.getProp(data, 'attributes'), 'attribute');
        this.attributes_ = {};

        for (var i = 0; i < attributeData.length; i++) {
            var attribute = attributeData[i];
            if (des.hasProp(attribute, 'name')) {
                this.attributes_['%Threshold' + des.getStringProp(attribute, 'name')] = des.getStringProp(attribute, 'value');
                this.attributes_['%Condition' + des.getStringProp(attribute, 'name')] = des.getStringProp(attribute, 'value');
            }
        }
    }
    this.setTokensValue();
};

/**
 * Check compliance on custom condition.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Data point
 */
anychart.thresholds.CustomThresholdCondition.prototype.checkPoint = function(point) {
    var v1 = this.isDynamicValue1_ ?
        point.getTokenValue(this.checkTokenFormat(this.value1_)) :
        this.value1_;
    var v2 = this.isDynamicValue2_ ?
        point.getTokenValue(this.checkTokenFormat(this.value2_)) :
        this.value2_;
    var v3 = this.isDynamicValue3_ ?
        point.getTokenValue(this.checkTokenFormat(this.value3_)) :
        this.value3_;

    if (this.checkCondition_(
        isNaN(Number(v1)) ? v1 : Number(v1),
        isNaN(Number(v2)) ? v2 : Number(v2),
        isNaN(Number(v3)) ? v3 : Number(v3))) {

        this.threshold_.setPointColor(point, this.color_);
        point.setThresholdItem(this);
        this.points_.push(point);
    }
};
anychart.thresholds.CustomThresholdCondition.prototype.checkTokenFormat = function(value) {
    value = value.split('{').join('');
    return value.split('}').join('');
};
/**
 * Check values on compliance with condition.
 * @protected
 * @param {Number} value1 First value.
 * @param {Number} value2 Second value.
 * @param {Number} value3 Third value.
 * @return {Boolean}
 */
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
anychart.thresholds.CustomThresholdCondition.prototype.checkCondition_ = function (value1, value2, value3) {
    goog.abstractMethod();
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
anychart.thresholds.CustomThresholdCondition.prototype.initFormatting = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
/**
 * inheritDoc
 */
anychart.thresholds.CustomThresholdCondition.prototype.setDefaultTokenValues = function() {
    this.tokensHash_.add('%ThresholdConditionName', '');
    this.tokensHash_.add('%ThresholdName', '');
};
anychart.thresholds.CustomThresholdCondition.prototype.getTokenType = function(token) {
    return anychart.formatting.TextFormatTokenType.TEXT;
};
/**
 * inheritDoc
 */
anychart.thresholds.CustomThresholdCondition.prototype.setTokensValue = function() {
    this.tokensHash_.add('%Name', this.name_);
    this.tokensHash_.add('%ThresholdConditionName', this.name_);
    this.tokensHash_.add('%ThresholdName', this.name_);
};
/**
 * inheritDoc
 */
anychart.thresholds.CustomThresholdCondition.prototype.getTokenValue = function(token) {
    if (this.attributes_ && this.attributes_[token])
        return this.attributes_[token];
    return this.tokensHash_.get(token);
};
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point
 * @param {String} token
 */
anychart.thresholds.CustomThresholdCondition.prototype.getFormattedTokenValue = function(point, token) {
    if (token == '%ConditionValue1')
        return this.isDynamicValue1_ ? point.getTokenValue(this.checkTokenFormat(this.value1_)) : this.value1_;
    if (token == '%ConditionValue2')
        return this.isDynamicValue2_ ? point.getTokenValue(this.checkTokenFormat(this.value2_)) : this.value2_;
    if (token == '%ConditionValue3')
        return this.isDynamicValue3_ ? point.getTokenValue(this.checkTokenFormat(this.value3_)) : this.value3_;

    return this.getTokenValue(token);
};
/**
 * @param {String} token Token name
 * @return {Boolean} 
 */
anychart.thresholds.CustomThresholdCondition.prototype.isCustomAttribute = function(token) {
   return Boolean(this.attributes_ && this.attributes_[token]);
};
//------------------------------------------------------------------------------
//                          Legend
//------------------------------------------------------------------------------
anychart.thresholds.CustomThresholdCondition.prototype.setHover = function() {
    //todo : implement legend
};

anychart.thresholds.CustomThresholdCondition.prototype.setNormal = function() {
    //todo : implement legend
};
//------------------------------------------------------------------------------
//
//              BetweenCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.BetweenCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.BetweenCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.BetweenCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 >= value2 && value1 <= value3;
};
//------------------------------------------------------------------------------
//
//              NotBetweenCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.NotBetweenCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.NotBetweenCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.NotBetweenCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 < value2 || value1 > value3;
};
//------------------------------------------------------------------------------
//
//              EqualCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustromThresholdCondition}
 */
anychart.thresholds.EqualCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.EqualCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.EqualCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 == value2;
};
//------------------------------------------------------------------------------
//
//              NotEqualToCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.NotEqualToCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.NotEqualToCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.NotEqualToCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 != value2;
};
//------------------------------------------------------------------------------
//
//              GreaterThanCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.GreaterThanCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.GreaterThanCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.GreaterThanCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 > value2;
};
//------------------------------------------------------------------------------
//
//              GreaterThanOrEqualToCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.GreaterThanOrEqualToCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.GreaterThanOrEqualToCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.GreaterThanOrEqualToCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 >= value2;
};
//------------------------------------------------------------------------------
//
//              LessThanCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.LessThanCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.LessThanCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.LessThanCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 < value2;
};
//------------------------------------------------------------------------------
//
//              LessThanOrEqualToCondition class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.LessThanOrEqualToCondition = function() {
    anychart.thresholds.CustomThresholdCondition.apply(this);
};
goog.inherits(anychart.thresholds.LessThanOrEqualToCondition,
    anychart.thresholds.CustomThresholdCondition);
//------------------------------------------------------------------------------
//                          Check condition
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 * @override
 */
anychart.thresholds.LessThanOrEqualToCondition.prototype.checkCondition_ = function(value1, value2, value3) {
    return value1 <= value2;
};
//------------------------------------------------------------------------------
//
//              CustomThresholdConditionFactory class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.thresholds.CustomThresholdConditionFactory = function() {
};
/**
 * Create condition by data.
 * @param {Object} data Condition settings.
 * @param {anychart.thresholds.CustomThreshold} threshold Condition threshold
 * @return {anychart.thresholds.CustomThresholdCondition}
 */
anychart.thresholds.CustomThresholdConditionFactory.prototype.createCondition = function(data, threshold) {
    if (!anychart.utils.deserialization.hasProp(data, 'type')) return null;
    var condition;
    switch (anychart.utils.deserialization.getLStringProp(data, 'type')) {
        default:
        case 'lessthan':
            condition = new anychart.thresholds.LessThanCondition();
            break;
        case 'between':
            condition = new anychart.thresholds.BetweenCondition();
            break;
        case 'notbetween':
            condition = new anychart.thresholds.NotBetweenCondition();
            break;
        case 'equalto':
            condition = new anychart.thresholds.EqualCondition();
            break;
        case 'notequalto':
            condition = new anychart.thresholds.NotEqualToCondition();
            break;
        case 'greaterthan':
            condition = new anychart.thresholds.GreaterThanCondition();
            break;
        case 'greaterthanorequalto':
            condition = new anychart.thresholds.GreaterThanOrEqualToCondition();
            break;
        case 'lessthanorequalto':
            condition = new anychart.thresholds.LessThanOrEqualToCondition();
            break;
    }
    condition.setThreshold(threshold);
    condition.deserialize(data);
    return condition;
};
//------------------------------------------------------------------------------
//
//                          CustomThreshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.Threshold}
 * @implements {anychart.thresholds.IAutomaticThreshold}
 */
anychart.thresholds.CustomThreshold = function() {
    anychart.thresholds.Threshold.apply(this);
};
goog.inherits(anychart.thresholds.CustomThreshold,
    anychart.thresholds.Threshold);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Custom conditions list.
 * @private
 * @type {Array.<anychart.thresholds.CustomThresholdCondition>}
 */
anychart.thresholds.CustomThreshold.prototype.conditions_ = null;

/**
 * Custom conditions factory.
 * @private
 * @type {Array.<anychart.thresholds.CustomThresholdConditionFactory>}
 */
anychart.thresholds.CustomThreshold.prototype.conditionsFactory_ = null;
//------------------------------------------------------------------------------
//                          IThreshold
//------------------------------------------------------------------------------
/**
 * @override
 * @inheritDoc
 */
anychart.thresholds.CustomThreshold.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    this.conditions_ = [];
    this.conditionsFactory_ = new anychart.thresholds.CustomThresholdConditionFactory();
    var conditionsData = anychart.utils.deserialization.getPropArray(data, 'condition');
    for (var i = 0; i < conditionsData.length; i++) {
        var condition = this.conditionsFactory_.createCondition(conditionsData[i], this);
        if (!condition) continue;
        this.conditions_.push(condition);
    }
};
anychart.thresholds.CustomThreshold.prototype.checkBeforeDraw = function(point) {
    for (var i = 0; i < this.conditions_.length; i++) {
        if (this.conditions_[i].checkPoint(point)) break;
    }
};
anychart.thresholds.CustomThreshold.prototype.checkAfterDeserialize = function(point) {

};
anychart.thresholds.CustomThreshold.prototype.getData = function(container) {
    var conditionsCount = this.conditions_.length;
    for (var i = 0; i < conditionsCount; i++) {
        var condition = this.conditions_[i];
        container.addItem(new anychart.controls.legend.LegendAdaptiveItem(
            condition.getColor(),
            0,
            -1,
            true,
            condition));
    }
};

/**
 * @inheritDoc
 */
anychart.thresholds.CustomThreshold.prototype.getTokenType = function(token) {
    return anychart.formatting.TextFormatTokenType.TEXT;
};

/**
 * @inheritDoc
 */
anychart.thresholds.CustomThreshold.prototype.isCustomAttribute = function(token) {
    var conditionsCount = this.conditions_.length;
    for (var i = 0; i < conditionsCount; i++) {
        var condition = this.conditions_[i];
        if (condition.isCustomAttribute(token)) return true;
    }
    return false;
};

//------------------------------------------------------------------------------
//
//                          AutomaticThresholdRange class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.thresholds.AutomaticThresholdRange = function() {
    this.points_ = [];
    this.initFormatting();
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Range start value.
 * @private
 * @type {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.start_ = null;
/**
 * Return range start value.
 * @return {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.getStart = function() {
    return this.start_;
};
/**
 * Set new range start value.
 * @param {Number} value New start value.
 */
anychart.thresholds.AutomaticThresholdRange.prototype.setStart = function(value) {
    this.start_ = value;
};
/**
 * Range end value.
 * @type {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.end_ = null;
/**
 * Return range end value.
 * @return {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.getEnd = function() {
    return this.end_;
};
/**
 * Set new range end value.
 * @param {Number} value New range end value.
 */
anychart.thresholds.AutomaticThresholdRange.prototype.setEnd = function(value) {
    this.end_ = value;
};
/**
 * Range points.
 * @type {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.points_ = null;
/**
 * Return range points.
 * @return {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.getPoints = function() {
    return this.points_;
};
/**
 * Range auto value.
 * @type {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.autoValue_ = null;
/**
 * Return  range auto value.
 * @return {Number}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.getAutoValue = function() {
    return this.autoValue_;
};
/**
 * Set new range auto value.
 * @param {Number} value New range auto value.
 */
anychart.thresholds.AutomaticThresholdRange.prototype.setAutoValue = function(value) {
    this.autoValue_ = value;
};

/**
 * Define, contains passed value in threshold range.
 * @param value
 */
anychart.thresholds.AutomaticThresholdRange.prototype.contains = function(value) {
    return value >= this.start_ && value < this.end_;
};
/**
 * Threshold range tokens hash.
 * @private
 * @type {anychart.formatting.TokensHash}
 */
anychart.thresholds.AutomaticThresholdRange.prototype.tokensHash_ = null;
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
anychart.thresholds.AutomaticThresholdRange.prototype.initFormatting = function() {
    this.tokensHash_ = new anychart.formatting.TokensHash();
    this.setDefaultTokenValues();
};
anychart.thresholds.AutomaticThresholdRange.prototype.setDefaultTokenValues = function() {
    this.tokensHash_.add('%ThresholdRangeMin', 0);
    this.tokensHash_.add('%ThresholdRangeMax', 0);
    this.tokensHash_.add('%ThresholdValue', 0);
};
anychart.thresholds.AutomaticThresholdRange.prototype.setTokensValue = function() {
    this.tokensHash_.add('%ThresholdRangeMin', this.start_);
    this.tokensHash_.add('%ThresholdRangeMax', this.end_);
};
anychart.thresholds.AutomaticThresholdRange.prototype.getTokenValue = function(token) {
    if (token == '%RangeMin') return this.start_;
    if (token == '%RangeMax') return this.end_;
    return this.tokensHash_.get(token);
};
anychart.thresholds.AutomaticThresholdRange.prototype.getTokenType = function(token) {
    return anychart.formatting.TextFormatTokenType.NUMBER;
};
/**
 * Defines is token is custom attribute.
 * @param {String} token Token.
 * @return {Boolean} is token is custom attribute.
 */
anychart.thresholds.AutomaticThresholdRange.prototype.isCustomAttribute = function(token) {
    return false;
};

//------------------------------------------------------------------------------
//                          Legend
//------------------------------------------------------------------------------
anychart.thresholds.AutomaticThresholdRange.prototype.setHover = function() {
    //todo : implement
};

anychart.thresholds.AutomaticThresholdRange.prototype.setNormal = function() {
    //todo : implement
};
//------------------------------------------------------------------------------
//
//                          AutomaticThreshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.Threshold}
 * @implements {anychart.thresholds.IAutomaticThreshold}
 */
anychart.thresholds.AutomaticThreshold = function() {
    anychart.thresholds.Threshold.apply(this);
};
goog.inherits(anychart.thresholds.AutomaticThreshold,
    anychart.thresholds.Threshold);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.thresholds.AutomaticThreshold.prototype.autoValue_ = null;
/**
 * @private
 */
anychart.thresholds.AutomaticThreshold.prototype.minValue_ = null;
/**
 * @private
 */
anychart.thresholds.AutomaticThreshold.prototype.maxValue_ = null;
/**
 * @private
 */
anychart.thresholds.AutomaticThreshold.prototype.range_ = null;
/**
 * @private
 */
anychart.thresholds.AutomaticThreshold.prototype.intervals_ = null;
/**
 * Color palette.
 * @private
 * @type {anychart.palettes.ColorPalette}
 */
anychart.thresholds.AutomaticThreshold.prototype.colorPalette_ = null;
/**
 * Set new color palette value
 * @param {anychart.palettes.ColorPalette} value New color palette value.
 */
anychart.thresholds.AutomaticThreshold.prototype.setColorPalette = function(value) {
    this.colorPalette_ = value;
};
/**
 * Threshold color range count.
 * @private
 * @type {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.rangeCount_ = null;
/**
 * Return threshold color range count.
 * @return {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangeCount = function() {
    return this.rangeCount_;
};
/**
 * Return range color by index.
 * @param {int} index Range index.
 * @return {anychart.visual.color.Color}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangeColor = function(index) {
    return this.colorPalette_.getItemAt(index);
};
/**
 * Return range start value by index.
 * @param {int} index Range index.
 * @return {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangeStart = function(index) {
    return this.intervals_[index].getStart();
};
/**
 * Return range end value by index.
 * @param {int} index Range index.
 * @return {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangeEnd = function(index) {
    return this.intervals_[index].getEnd();
};
/**
 * Return range value by index.
 * @param {int} index Range index.
 * @return {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangeValue = function(index) {
    return this.getRangeStart(index) + this.getRangeEnd(index);
};
/**
 * Return range point by index.
 * @param {int} index Range index.
 * @return {Array.<anychart.plots.seriesPlot.data.BasePoint>}
 */
anychart.thresholds.AutomaticThreshold.prototype.getRangePoints = function(index) {
    return this.intervals_[index].getPoints();
};
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.thresholds.AutomaticThreshold.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    this.minValue_ = Number.MAX_VALUE;
    this.maxValue_ = -Number.MAX_VALUE;

    var des = anychart.utils.deserialization;

    this.rangeCount_ = (des.hasProp(data, 'range_count')) ?
        des.getNumProp(data, 'range_count') : 5;

    this.autoValue_ = (des.hasProp(data, 'auto_value')) ?
        des.getStringProp(data, 'auto_value') : '%Value';
};
//------------------------------------------------------------------------------
//              IAutomaticThreshold implementation
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.thresholds.AutomaticThreshold.prototype.initialize = function () {
    if (this.minValue_ == Number.MAX_VALUE) this.minValue_ = 0;
    if (this.maxValue_ == -Number.MAX_VALUE) this.maxValue_ = 10;
    this.intervals_ = [];
};

/**
 * @inheritDoc
 */
anychart.thresholds.AutomaticThreshold.prototype.checkAfterDeserialize = function(point) {
    var val = this.getCheckedPointTokenValue_(point, this.autoValue_);

    if (val < this.minValue_)
        this.minValue_ = val;
    if (val > this.maxValue_)
        this.maxValue_ = val;
};

/**
 * @protected
 */
anychart.thresholds.AutomaticThreshold.prototype.buildBoundsArray = function() {
    var ratio = (this.maxValue_ - this.minValue_) / this.rangeCount_;
    for (var i = 0; i < this.rangeCount_; i++) {
        var range = new anychart.thresholds.AutomaticThresholdRange();
        range.setStart(i == 0 ? (this.minValue_ + ratio * i) : (this.intervals_[i - 1].getEnd()));
        range.setEnd(range.getStart() + ratio);
        range.setAutoValue(this.autoValue_);
        this.intervals_[i] = range;
    }
};

/**
 * Return bounds index by point token value.
 * @protected
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point instance.
 */
anychart.thresholds.AutomaticThreshold.prototype.fineRangeIndexByPoint = function(point) {
    var val = this.getCheckedPointTokenValue_(point, this.autoValue_);

    if (val < this.minValue_ || val > this.maxValue_) return -1;

    for (var i = 0; i < this.rangeCount_; i++)
        if (this.intervals_[i] && this.intervals_[i].contains(val)) return i;

    if (val == this.intervals_[this.rangeCount_ - 1].getEnd())
        return this.rangeCount_ - 1;

    return -1;
};

/**
 * Return not empty point token value.
 * @private
 * @return {Number}
 */
anychart.thresholds.AutomaticThreshold.prototype.getCheckedPointTokenValue_ = function(point, token) {
    var valString = point.getTokenValue(this.checkDynamicValue(token));
    if (valString == '') return null;

    var val = anychart.utils.deserialization.getNum(valString);
    if (isNaN(val)) return null;

    return val;
};
/**
 * @override
 */
anychart.thresholds.AutomaticThreshold.prototype.isAutomatic = function() {
    return true;
};
anychart.thresholds.AutomaticThreshold.prototype.getData = function(container) {
    var intervalsCount = this.intervals_.length;
    for (var i = 0; i < intervalsCount; i++) {
        container.addItem(new anychart.controls.legend.LegendAdaptiveItem(
            this.getRangeColor(i),
            0,
            -1,
            true,
            this.intervals_[i]));
    }
};
//------------------------------------------------------------------------------
//
//                   CollectionPointInfo class
//
//------------------------------------------------------------------------------
/**
 * Информационный класс, содержит инфу необходимую CollectionBasedThreshold
 * для выбора цвета у точки. По сути нафиг не нужен. CollectionBasedThreshold
 * нужна сама точка и её отформатированое значение, именно эту пару и хранит CollectionPointInfo.
 * @constructor
 * @param {anychart.plots.seriesPlot.data.BasePoint} opt_point
 * @param {Number} opt_checkedPointValue
 */
anychart.thresholds.CollectionPointInfo = function(opt_point, opt_checkedPointValue) {
    if (opt_point)this.point_ = opt_point;
    if (opt_checkedPointValue) this.checkedPointValue_ = opt_checkedPointValue;
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Link to point.
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.thresholds.CollectionPointInfo.prototype.point_ = null;
/**
 * Return link to point.
 * @return {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.thresholds.CollectionPointInfo.prototype.getPoint = function() {
    return  this.point_;
};
/**
 * Set new link to point.
 * @param {anychart.plots.seriesPlot.data.BasePoint} value New link to point
 */
anychart.thresholds.CollectionPointInfo.prototype.setPoint = function(value) {
    this.point_ = value;
};
/**
 * Point formatted value.
 * @private
 * @type {Number}
 */
anychart.thresholds.CollectionPointInfo.prototype.checkedPointValue_ = null;
/**
 * Return point formatted value.
 * @return {Number}
 */
anychart.thresholds.CollectionPointInfo.prototype.getCheckedPointValue = function() {
    return this.checkedPointValue_;
};
/**
 * Set new point formatted value.
 * @param {Number} value New formatted point value
 */
anychart.thresholds.CollectionPointInfo.prototype.setCheckedPointValue = function (value) {
    this.checkedPointValue_ = value;
};
//------------------------------------------------------------------------------
//
//                   CollectionBasedThreshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.AutomaticThreshold}
 */
anychart.thresholds.CollectionBasedThreshold = function() {
    anychart.thresholds.AutomaticThreshold.apply(this);
    this.pointsCollection_ = [];
};
goog.inherits(anychart.thresholds.CollectionBasedThreshold,
    anychart.thresholds.AutomaticThreshold);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Points collection.
 * @private
 * @type {Array.<anychart.thresholds.CollectionPointInfo>}
 */
anychart.thresholds.CollectionBasedThreshold.prototype.pointsCollection_ = null;
//------------------------------------------------------------------------------
//                          IThreshold
//------------------------------------------------------------------------------
anychart.thresholds.CollectionBasedThreshold.prototype.checkBeforeDraw = function(point) {
    var index = this.fineRangeIndexByPoint(point);
    if (index != -1) {
        point.setThresholdItem(this.intervals_[index]);
        this.intervals_[index].getPoints().push(point);
        this.setPointColor(point, this.colorPalette_.getItemAt(index));
    }
};

anychart.thresholds.CollectionBasedThreshold.prototype.checkAfterDeserialize = function(point) {
    var val = this.getCheckedPointTokenValue_(point, this.autoValue_);

    if (val < this.minValue_)
        this.minValue_ = val;
    if (val > this.maxValue_)
        this.maxValue_ = val;
    if (!isNaN(val)) {
        this.pointsCollection_.push(
            new anychart.thresholds.CollectionPointInfo(point, val));
    }
};
//------------------------------------------------------------------------------
//                          Magic
//------------------------------------------------------------------------------
/**
 * Летопись не сохранила иторию о том кто, когда и почему написал этот алгоритм, неизвестно зачем и почему, что то делающий.
 * Всё довольно не плохо работает, поэтому разобраться - задача минорная. Надеюсь когда ни будь руки дойдут.
 * @protected
 * @param {Number} val1
 * @param {Number} val2
 * @param {Boolean} toLower
 * @return {Number}
 */
anychart.thresholds.CollectionBasedThreshold.prototype.doUnknownAction = function(val1, val2, toLower) {
    var tmp = val1 + (0.5 * (val2 - val1));
    if (tmp == 0) return 0;

    var tmp1 = Math.floor(Math.log(Math.abs(tmp)) / Math.log(10));
    var tmp2 = Math.pow(10, Number(tmp1 - 1));

    tmp /= tmp2;
    tmp = (toLower) ? Math.floor(tmp) : Math.ceil(tmp);

    return tmp * tmp2;
};
//------------------------------------------------------------------------------
//
//                   EqualIntervalThreshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.AutomaticThreshold}
 */
anychart.thresholds.EqualIntervalThreshold = function() {
    anychart.thresholds.AutomaticThreshold.apply(this);
};
goog.inherits(anychart.thresholds.EqualIntervalThreshold,
    anychart.thresholds.AutomaticThreshold);
//------------------------------------------------------------------------------
//                          IAutomaticThreshold
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.thresholds.EqualIntervalThreshold.prototype.initialize = function() {
    goog.base(this, 'initialize');
    var des = anychart.utils.deserialization;

    if (this.minValue_ != this.maxValue_) {
        var tmp = Math.pow(10, Math.log(Math.max(Math.abs(this.minValue_), Math.abs(this.maxValue_))) / Math.log(10) - 1);
        this.minValue_ = Math.floor(this.minValue_ / tmp);

        var tmp1 = this.minValue_ * 10;
        this.minValue_ *= tmp;
        this.maxValue_ = Math.ceil(this.maxValue_ / tmp);
        this.maxValue_ += Number((this.rangeCount_ - (((this.maxValue_ * 10) - tmp1) % this.rangeCount_))) / 10;
        this.maxValue_ *= tmp;
    }
    this.range_ = (this.maxValue_ - this.minValue_) / this.rangeCount_;
    this.buildBoundsArray();
};

/**
 * @inheritDoc
 */
anychart.thresholds.EqualIntervalThreshold.prototype.checkBeforeDraw = function(point) {
    var rangeIndex = this.fineRangeIndexByPoint(point);
    if (rangeIndex != -1) {
        this.intervals_[rangeIndex].getPoints().push(point);
        point.setThresholdItem(this.intervals_[rangeIndex]);
        var val = point.getPointTokenValue(this.checkDynamicValue(this.autoValue_));
        var colorIndex = Math.floor((val - this.minValue_) / this.range_);
        this.setPointColor(point, this.colorPalette_.getItemAt(colorIndex));
    }
};
//------------------------------------------------------------------------------
//
//                          EqualDistributionThreshold class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.thresholds.CollectionBasedThreshold}
 */
anychart.thresholds.EqualDistributionThreshold = function() {
    anychart.thresholds.CollectionBasedThreshold.apply(this);
};
goog.inherits(anychart.thresholds.EqualDistributionThreshold,
    anychart.thresholds.CollectionBasedThreshold);
//------------------------------------------------------------------------------
//                          IAutomaticThreshold
//------------------------------------------------------------------------------
anychart.thresholds.EqualDistributionThreshold.prototype.initialize = function() {
    goog.base(this, 'initialize');
    this.pointsCollection_.sort(function(a, b) {
        return a.getCheckedPointValue() - b.getCheckedPointValue()
    });

    var rangeCount = this.rangeCount_;

    if (this.pointsCollection_.length == 0) {
        this.buildBoundsArray();
    } else {
        if (rangeCount > this.pointsCollection_.length) {
            this.rangeCount_ = rangeCount = this.pointsCollection_.length;
        }
        if (rangeCount < 2) {
            this.rangeCount_ = rangeCount;
            this.minValue_ = this.doUnknownAction(this.minValue_, this.minValue_, true);
            this.maxValue_ = this.doUnknownAction(this.maxValue_, this.maxValue_, false);
            this.buildBoundsArray();
        } else {
            var num = this.pointsCollection_.length / rangeCount;
            var a = num;

            var val = this.pointsCollection_[0].getCheckedPointValue();
            var valExt1 = this.pointsCollection_[Math.round(a - 1)].getCheckedPointValue();
            var valExt2 = this.pointsCollection_[Math.round(a)].getCheckedPointValue();

            var range = new anychart.thresholds.AutomaticThresholdRange();
            range.setStart(this.doUnknownAction(val, val, true));
            var tmp = this.doUnknownAction(valExt1, valExt2, true);
            range.setEnd(tmp);
            range.setAutoValue(this.autoValue_);
            this.intervals_[0] = range;

            for (var i = 1; i < (rangeCount - 1); i++) {
                a += num;
                this.intervals_[i] = new anychart.thresholds.AutomaticThresholdRange();
                this.intervals_[i].setAutoValue(this.autoValue_);
                this.intervals_[i].setStart(tmp);
                tmp = this.doUnknownAction(
                    this.pointsCollection_[Math.round(a - 1)].getCheckedPointValue(),
                    this.pointsCollection_[Math.round(a)].getCheckedPointValue(),
                    true);
                this.intervals_[i].setEnd(tmp);
            }
            this.intervals_[this.rangeCount_ - 1] = new anychart.thresholds.AutomaticThresholdRange();
            this.intervals_[this.rangeCount_ - 1].setStart(tmp);
            this.intervals_[this.rangeCount_ - 1].setAutoValue(this.autoValue_);
            this.intervals_[this.rangeCount_ - 1].setEnd(
                this.doUnknownAction(
                    this.pointsCollection_[this.pointsCollection_.length - 1].getCheckedPointValue(),
                    this.pointsCollection_[this.pointsCollection_.length - 1].getCheckedPointValue(),
                    false));
        }
    }
};
//------------------------------------------------------------------------------
//
//                          OptimalThreshold class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.thresholds.OptimalThreshold = function() {
    anychart.thresholds.CollectionBasedThreshold.apply(this);
};
goog.inherits(anychart.thresholds.OptimalThreshold,
    anychart.thresholds.CollectionBasedThreshold);
//------------------------------------------------------------------------------
//                          IAutomaticThreshold
//------------------------------------------------------------------------------

anychart.thresholds.OptimalThreshold.prototype.initialize = function() {
    goog.base(this, 'initialize');
    this.pointsCollection_.sort(function(a, b) {
        return a.getCheckedPointValue() - b.getCheckedPointValue()
    });

    if (this.pointsCollection_.length == 0) {
        this.buildBoundsArray();
    } else {
        var rangeCount = Math.min(this.rangeCount_, this.pointsCollection_.length);
        if (rangeCount <= 4) {
            this.rangeCount_ = Math.max(this.rangeCount_, 1);
            this.buildBoundsArray();
        } else {
            if (rangeCount > (this.pointsCollection_.length - 3)) {
                this.rangeCount_ = this.pointsCollection_.length;
                //08.04.2010 Комент перенесен из флеша вместе с кодом.
                /*
                 Эксперимент в трешхолде...
                 пока активна старая версия...
                 это не удалять!!!
                 if (this.intervals.length < this.rangeCount)
                 for (var ij:int = this.intervals.length ; ij < this.rangeCount ; ij ++)
                 this.intervals[ij]=0;
                 */
            }
            if (rangeCount < 4) {
                this.rangeCount_ = Math.max(this.rangeCount_, 1);
                this.buildBoundsArray();
            } else {
                var list = [];
                for (var i = 0; i < this.pointsCollection_.length; i++)
                    list.push(this.pointsCollection_[i].getCheckedPointValue());

                var numArray = this.helper1(list, this.rangeCount_);
                var range = new anychart.thresholds.AutomaticThresholdRange();
                range.setAutoValue(this.autoValue_);
                var tmp1 = this.pointsCollection_[0].getCheckedPointValue();
                range.setStart(this.doUnknownAction(tmp1, tmp1, true));

                tmp1 = this.pointsCollection_[numArray[0] - 1].getCheckedPointValue();
                var tmp2 = this.pointsCollection_[numArray[0]].getCheckedPointValue();

                var obj2 = this.doUnknownAction(tmp1, tmp2, true);
                range.setEnd(obj2);

                this.intervals_[0] = range;

                for (var j = 1; j < Math.min(this.rangeCount_ - 1, numArray.length); j++) {
                    range = new anychart.thresholds.AutomaticThresholdRange();
                    range.setAutoValue(this.autoValue_);
                    range.setStart(obj2);
                    tmp1 = this.pointsCollection_[numArray[j] - 1].getCheckedPointValue();
                    tmp2 = this.pointsCollection_[numArray[j]].getCheckedPointValue();
                    obj2 = this.doUnknownAction(tmp1, tmp2, true);
                    range.setEnd(obj2);
                    this.intervals_[j] = range;
                }

                range = new anychart.thresholds.AutomaticThresholdRange();
                range.setAutoValue(this.autoValue_);
                range.setStart(obj2);
                range.setEnd(this.doUnknownAction(
                    this.pointsCollection_[this.pointsCollection_.length - 1].getCheckedPointValue(),
                    this.pointsCollection_[this.pointsCollection_.length - 1].getCheckedPointValue(),
                    false));
                this.intervals_[this.rangeCount_ - 1] = range;

                //08.04.2010 Комент перенесен из флеша вместе с кодом.
                /*
                 Эксперимент в трешхолде...
                 пока активна старая версия...
                 это не удалять!!!

                 if (numArray.length < this.rangeCount-1)
                 {
                 this.intervals[numArray.length] = r;
                 for (var il:int = numArray.length; il < this.rangeCount-1 ; il ++)
                 this.intervals.pop();
                 this.rangeCount = this.intervals.length;
                 }
                 */
            }
        }
    }
};

anychart.thresholds.OptimalThreshold.prototype.helper1 = function(listPoints, rangeCountVar) {
    var numArray = [];
    var numArray2 = [];
    for (var i = 0; i < listPoints.length + 1; i++) {
        numArray.push(new Array(rangeCountVar + 1));
        numArray2.push(new Array(rangeCountVar + 1));
    }
    for (i = 1; i <= rangeCountVar; i++) {
        numArray[1][i] = 1;
        numArray2[1][i] = 0;
        for (var m = 2; m <= listPoints.length; m++)
            numArray2[m][i] = Number.POSITIVE_INFINITY;
    }

    var expression = 0;
    for (var j = 2; j <= listPoints.length; j++) {
        var Summ = 0;
        var SummSqrt = 0;
        var count = 0;
        for (var n = 1; n <= j; n++) {
            var indxR = j - n;
            var point = Number(listPoints[indxR]);
            SummSqrt += point * point;
            Summ += point;
            count++;
            expression = SummSqrt - ((Summ * Summ) / count);
            var indx = indxR;
            if (indx != 0) {
                for (var ii = 2; ii <= rangeCountVar; ii++) {
                    if (numArray2[j][ii] >= (expression + numArray2[indx][ii - 1])) {
                        numArray[j][ii] = indxR + 1;
                        numArray2[j][ii] = expression + numArray2[indx][ii - 1];
                    }
                }
            }
        }
        numArray[j][1] = 1;
        numArray2[j][1] = expression;
    }

    var list = [];

    indx = listPoints.length;
    for (var k = rangeCountVar; k >= 2; k--) {
        indxR = (numArray[indx][k]) - 2;
        if (indxR > 0) list.splice(0, 0, indxR);
        indx = Math.max((numArray[indx][k]) - 1, 0);
    }

    list.push(listPoints.length - 1);

    return list;
};
//------------------------------------------------------------------------------
//
//                          ThresholdsList class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {Object} thresholdsNode Chart thresholds node.
 */
anychart.thresholds.ThresholdsList = function(thresholdsNode) {
    this.thresholdsNode_ = thresholdsNode;
    this.parsedThresholdsMap_ = {};
    this.parsedThresholdList_ = [];
    this.autoThresholdsMap_ = {};
    this.autoThresholdsList_ = [];
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Chart threshold node.
 * @private
 * @type {Object}
 */
anychart.thresholds.ThresholdsList.prototype.thresholdsNode_ = null;
anychart.thresholds.ThresholdsList.prototype.parsedThresholdsMap_ = null;
/**
 * Custom threshold list.
 * @private
 * @type {Array.<anychart.thresholds.IThreshold>}
 */
anychart.thresholds.ThresholdsList.prototype.parsedThresholdList_ = null;
anychart.thresholds.ThresholdsList.prototype.getParsedThresholdList = function() {
    return this.parsedThresholdList_;
};
/**
 * Automatic threshold list.
 * @private
 * @type {Array.<anychart.thresholds.IAutomaticThreshold>}
 */
anychart.thresholds.ThresholdsList.prototype.autoThresholdsList_ = null;
anychart.thresholds.ThresholdsList.prototype.autoThresholdsMap_ = null;
//------------------------------------------------------------------------------
//                          Initialize
//------------------------------------------------------------------------------
anychart.thresholds.ThresholdsList.prototype.initializeAutomaticThresholds = function() {
    for (var i = 0; i < this.autoThresholdsList_.length; i++)
        this.autoThresholdsList_[i].initialize();
};
//------------------------------------------------------------------------------
//                          Getting threshold
//------------------------------------------------------------------------------
/**
 * Return threshold by threshold name.
 * @private
 * @param {String} name Threshold name.
 * @param {anychart.palettes.PalettesCollection} opt_palettesList Palettes data, necessary only for automatic thresholds.
 * @return {anychart.thresholds.IThreshold} Threshold instance
 */
anychart.thresholds.ThresholdsList.prototype.getThreshold = function(name, opt_palettesList) {
    if (!this.thresholdsNode_) return null;
    name = anychart.utils.deserialization.getLString(name);
    if (this.parsedThresholdsMap_[name]) return this.parsedThresholdsMap_[name];

    var data = anychart.utils.JSONUtils.getNodeByName(this.thresholdsNode_, name);
    if (!data) return null;

    var threshold = this.createThreshold_(data);
    if (!threshold) return null;
    this.deserializeThreshold_(threshold, data, name, opt_palettesList);

    this.parsedThresholdList_.push(threshold);
    this.parsedThresholdsMap_[name] = threshold;
    return threshold;
};

/**
 * Return automatic threshold.
 * @param {String} name Threshold name.
 * @return {anychart.thresholds.AutomaticThreshold}
 */
anychart.thresholds.ThresholdsList.prototype.getAutoThreshold = function(name) {
    return this.autoThresholdsMap_[name];
};
//------------------------------------------------------------------------------
//                          Creation
//------------------------------------------------------------------------------
/**
 * Create threshold instance by data.
 * @private
 * @param {Object} data Threshold data.
 * @return {anychart.thresholds.IThreshold} Threshold instance
 */
anychart.thresholds.ThresholdsList.prototype.createThreshold_ = function(data) {
    var threshold;
    switch (anychart.utils.deserialization.getLStringProp(data, 'type')) {
        default:
        case 'custom':
            threshold = new anychart.thresholds.CustomThreshold();
            break;
        case 'equalinterval':
        case 'equalsteps':
            threshold = new anychart.thresholds.EqualIntervalThreshold();
            break;
        case 'quantiles':
        case 'equaldistribution':
            threshold = new anychart.thresholds.EqualDistributionThreshold();
            break;
        case 'optimal':
        case 'absolutedeviation':
            threshold = new anychart.thresholds.OptimalThreshold();
            break;
    }
    return threshold;
};

/**
 * Deserialize threshold.
 * @private
 * @param {anychart.thresholds.IThreshold} threshold Threshold instance to deserialize.
 * @param {Object} data Threshold data.
 * @param {anychart.palettes.PalettesCollection} opt_palettesList Palettes data, necessary only for automatic thresholds.
 */
anychart.thresholds.ThresholdsList.prototype.deserializeThreshold_ = function(threshold, data, name, opt_palettesList) {
    threshold.deserialize(data);
    var des = anychart.utils.deserialization;

    if (threshold.isAutomatic()) {
        var palette = new anychart.palettes.ColorPalette();
        var paletteData;

        if (des.hasProp(data, 'palette')) {
            var paletteName = des.getProp(data, 'palette');
            paletteData = (opt_palettesList) ? opt_palettesList.getPalette('palette', paletteName) : null;
        }
        if (!paletteData) paletteData = opt_palettesList.getPalette('palette', 'default');

        palette.deserialize(paletteData);
        threshold.setColorPalette(palette.checkAuto(threshold.getRangeCount(), true));
        this.autoThresholdsList_.push(threshold);
        this.autoThresholdsMap_[name] = threshold;
    }
};
