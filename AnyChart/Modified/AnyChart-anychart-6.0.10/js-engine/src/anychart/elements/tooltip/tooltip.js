/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.tooltip.TooltipElementStyleState}</li>
 *  <li>@class {anychart.elements.tooltip.BaseTooltipElement}.</li>
 * <ul>
 */
goog.provide('anychart.elements.tooltip');
goog.require('anychart.elements.tooltip.styles');
goog.require('anychart.layout');
goog.require('goog.userAgent');

//------------------------------------------------------------------------------
//
//                           BaseTooltipElement class.
//
//------------------------------------------------------------------------------
/**
 * Class represent base tooltip element.
 * @constructor
 * @extends {anychart.elements.label.BaseLabelElement}
 */
anychart.elements.tooltip.BaseTooltipElement = function () {
    anychart.elements.label.BaseLabelElement.apply(this);

    if (goog.userAgent.WEBKIT)
        this.getAbsolutePosition_ = this.getAbsolutePositionWebKit_;
    else this.getAbsolutePosition_ = this.getAbsolutePositionFF_;


};
//inheritance
goog.inherits(anychart.elements.tooltip.BaseTooltipElement,
    anychart.elements.label.BaseLabelElement);
//------------------------------------------------------------------------------
//
//                          Override
//
//------------------------------------------------------------------------------
/**
 * Return tooltip style node name.
 * @override
 * @return {String} Style node name.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getStyleNodeName =
    function () {
        return 'tooltip_style';
    };

/**
 * Creates a style.
 * @return {anychart.elements.tooltip.styles.TooltipElementStyle} Tooltip
 * element style.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.createStyle =
    function () {
        return new anychart.elements.tooltip.styles.TooltipElementStyle();
    };

/**
 * @inheritDoc
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getElementContainer =
    function (container) {
        return container.getPlot().getTooltipsSprite();
    };


//------------------------------------------------------------------------------
//
//                          Drawing
//
//------------------------------------------------------------------------------
/**
 *
 * @param {Event} event Event.
 * @param {anychart.plots.seriesPlot.data.BasePoint} container Container.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.execMoveTooltip = function (event, container, state, sprite) {
    if (state.getAnchor() == anychart.layout.Anchor.FLOAT) {        
        var bounds = this.getBounds(container, state, sprite);
        // we use the event above to get the position and size
        // we then create a div that mimics the sprite in terms of position and size
        // we add the sprite into that div
        // we only add it if is a different one than the one in there for performance 
        // The anychart svg is still there and showing but it is also a child of the
        // div we have put in here
        // we also reset the actual svg position to 0,0 relative to the div (of course) after
        // remember, we're still showing the original sprite, but we are positioning it with another element
        // the svg sprite must be 0,0 so it is completely shown within the div
        
        //we check for the existence of an anychart tooltip div
        // this is the master container which is ultimately moved around
        // since svg root elements can't be moved
        var div = document.getElementById('anychart-tooltip-div');
        // container to put anychart svg elements inside of
        var tooltip = document.getElementById('anychart-tooltip-svg');
        var svgm = container.getSVGManager();
        var svg = svgm.getSVG();
        if (!div) {
            div = document.createElement('div');
            div.id = 'anychart-tooltip-div';
            div.style.position = 'absolute';
            div.style.left = '0px';
            div.style.top = '0px';
            div.style['pointerEvents'] = 'none';
	    div.style.zIndex = "99999";
            document.body.appendChild(div);

            tooltip = document.createElementNS(svgm.svgNS_, 'svg');
            tooltip.setAttribute('xmlns', svgm.svgNS_);
            tooltip.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
            tooltip.setAttribute("width", "100%");
            tooltip.setAttribute("height", "100%");
            tooltip.setAttribute("id", "anychart-tooltip-svg");
            div.appendChild(tooltip);
        }
        var sx = 0;
        var sy = 0;
        if (goog.userAgent.WEBKIT) {
            //chrome/safari
            sx = event.clientX + svg.offsetLeft;
            sy = event.clientY + svg.offsetTop - bounds.height;
        } else {
            // FF & IE
            var el = sprite.getSVGManager().getSVG()['parentNode'];
            var lx = 0;
            var ly = 0;
            while (el != null) {
                lx += el.offsetLeft;
                ly += el.offsetTop;
                el = el.offsetParent;
            }
            sx = event.clientX + (window.pageXOffset || document.body.scrollLeft);
            sy = event.clientY + (window.pageYOffset || document.body.scrollTop) - bounds.height;
        }
        div.style.left = sx + 'px';
        div.style.top = sy + 'px';
        div.style.x = sx + 'px';
        div.style.y = sy + 'px';
        div.style.width = bounds.width + 'px';
        div.style.height = bounds.height + 'px';

        if (window['lastSprite']) {
            if (window['lastSprite'] !== sprite.element_) {
                tooltip.removeChild(window['lastSprite']);
                tooltip.appendChild(sprite.element_);
            }
        } else {
            tooltip.appendChild(sprite.element_);
        }
        window['lastSprite'] = sprite.element_;
        this.setPosition({x:0,y:0}, sprite);
    }
};

/**
 * Creates text sprite.
 * @private
 * @param {anychart.plots.seriesPlot.data.BasePoint} container Container.
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.createTextSprite_ =
    function (container, state, sprite) {
        goog.base(this, 'createTextSprite_', container, state, sprite);
        sprite.getElement().setAttribute('pointer-events', 'none');
    };
//------------------------------------------------------------------------------
//                          Utils
//------------------------------------------------------------------------------
/**
 * Специфичная по браузеру определение положения мыши.
 * @private
 * @type {Function)}
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePosition_ = null;

anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePositionFF_ = function (event, sprite, pos) {
    var el = sprite.getSVGManager().getSVG()['parentNode'];
    var clientPosition = sprite.getClientPosition();
    var lx = 0;
    var ly = 0;
    while (el != null) {
        lx += el.offsetLeft;
        ly += el.offsetTop;
        el = el.offsetParent;
    }

    pos.x = event.clientX - (clientPosition.x + lx);
    pos.y = event.clientY - (clientPosition.y + ly);

    return pos
};
anychart.elements.tooltip.BaseTooltipElement.prototype.getAbsolutePositionWebKit_ = function (event, sprite, pos) {
    var clientPosition = sprite.getClientPosition();
    pos.x = event.offsetX - clientPosition.x;
    pos.y = event.offsetY - clientPosition.y;
    return  pos;
};

/**
 * Return copy of base marker element.
 * @override
 * @param {*} opt_target Target object to copy.
 * @return {anychart.elements.tooltip.BaseTooltipElement} Base tooltip element.
 */
anychart.elements.tooltip.BaseTooltipElement.prototype.copy =
    function (opt_target) {
        if (!opt_target) opt_target =
            new anychart.elements.tooltip.BaseTooltipElement();
        return goog.base(this, 'copy', opt_target);
    };
