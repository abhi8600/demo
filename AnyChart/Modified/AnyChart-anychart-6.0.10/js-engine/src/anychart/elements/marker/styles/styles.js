/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes.
 * <ul>
 *  <li>@class {anychart.elements.marker.styles.MarkerElementFillStyleShape}</li>
 *  <li>@class {anychart.elements.marker.styles.MarkerElementHatchFillStyleShape}</li>
 *  <li>@class {anychart.elements.marker.styles.MarkerElementStyleState}</li>
 *  <li>@class {anychart.elements.marker.styles.MarkerElementStyleShapes}</li>
 *  <li>@class {anychart.elements.marker.styles.MarkerElementStyle}.</li>
 * <ul>
 */
goog.provide('anychart.elements.marker.styles');
goog.require('anychart.elements.styles');
goog.require('anychart.styles.background');
//------------------------------------------------------------------------------
//
//                          MarkerElementFillStyleShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillWithStrokeStyleShape}
 */
anychart.elements.marker.styles.MarkerElementFillStyleShape = function() {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.elements.marker.styles.MarkerElementFillStyleShape,
        anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**
 * Update shape.
 * @param {anychart.styles.StyleState} state Current style state.
 */
anychart.elements.marker.styles.MarkerElementFillStyleShape.prototype.update = function(state) {
    goog.base(this, 'update', state);
    this.styleShapes_.updateMarkerPath(this.element_, state);
};
//------------------------------------------------------------------------------
//
//                          MarkerElementHatchFillStyleShape.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.FillWithStrokeStyleShape}
 */
anychart.elements.marker.styles.MarkerElementHatchFillStyleShape = function() {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.elements.marker.styles.MarkerElementHatchFillStyleShape,
        anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                          Update.
//------------------------------------------------------------------------------
/**
 * Update shape.
 * @param {anychart.styles.StyleState} state Current style state.
 */
anychart.elements.marker.styles.MarkerElementHatchFillStyleShape.prototype.update = function(state) {
    goog.base(this, 'update', state);
    this.styleShapes_.updateMarkerPath(this.element_, state);
};
//------------------------------------------------------------------------------
//
//                          MarkerElementStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.elements.styles.BaseElementsStyleShapes}
 */
anychart.elements.marker.styles.MarkerElementStyleShapes = function() {
    anychart.elements.styles.BaseElementsStyleShapes.call(this);
};
goog.inherits(anychart.elements.marker.styles.MarkerElementStyleShapes,
        anychart.elements.styles.BaseElementsStyleShapes);
//------------------------------------------------------------------------------
//                  .
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.elements.marker.styles.MarkerElementFillStyleShape}
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.
        fillShape_ = null;
/**
 * @private
 * @type {anychart.elements.marker.styles.MarkerElementHatchFillStyleShape}
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.
        hatchFillShape_ = null;
//------------------------------------------------------------------------------
//                              Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.
        createShapes = function(point, element) {
    var style = this.point_.getMarker().getStyle();
    if (style.needCreateFillShape()) {
        this.fillShape_ = new anychart.elements.marker.styles.
                MarkerElementFillStyleShape();
        this.fillShape_.initialize(
                point,
                this,
                this.createMarkerElement);
    }
    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.elements.marker.styles.
                MarkerElementHatchFillStyleShape();
        this.hatchFillShape_.initialize(
                point,
                this,
                this.createMarkerElement);
    }
};

/**
 * Place shapes.
 * @param {anychart.svg.SVGSprite} sprite Sprite.
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.placeShapes =
        function(sprite) {
            if (this.fillShape_) sprite.appendChild(this.fillShape_.getElement());
            if (this.hatchFillShape_)
                sprite.appendChild(this.hatchFillShape_.getElement());
        };
//------------------------------------------------------------------------------
//                                  Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.update = function(state) {
    goog.base(this, 'update', state);
    if (this.fillShape_) this.fillShape_.update(state);
    if (this.hatchFillShape_) this.hatchFillShape_.update(state);
};

/**
 * Creates marker element.
 * @return {SVGElement} Marker element.
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.createMarkerElement = function() {
    return this.point_.getSVGManager().createPath();
};
/**
 * @param {SVGElement} element Element.
 * @param {anychart.elements.marker.styles.MarkerElementStyleState} state State.
 */
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.updateMarkerPath = function(element, state) {
    var markerType = state.getMarkerType(this.point_);
    if (markerType === anychart.elements.marker.MarkerType.NONE) {
        element.setAttribute(
                'style',
                this.point_.getSVGManager().getEmptySVGStyle());
        return;
    }
    element.setAttribute(
            'd',
            this.element_.getMarkerPath(
                    markerType,
                    this.point_.getSVGManager(),
                    state));
};
anychart.elements.marker.styles.MarkerElementStyleShapes.prototype.updateEffects = function(element, state) {
    if (state.needUpdateEffects())
        element.setAttribute('filter', state.getEffects().createEffects(this.point_.getSVGManager()));
    else
        element.removeAttribute('filter');
};
//------------------------------------------------------------------------------
//
//                           MarkerElementStyleState class.
//
//------------------------------------------------------------------------------
/**
 * Class represent marker style state
 * @constructor
 * @param {anychart.styles.Style} style Style for this state.
 * @param {anychart.styles.StateType} opt_stateType State type.
 * @extends {anychart.elements.styles.BaseElementStyleState}
 */
anychart.elements.marker.styles.MarkerElementStyleState =
        function(style, opt_stateType) {
            anychart.elements.styles.BaseElementStyleState.call(this, style,
                    opt_stateType);
        };
//inheritance
goog.inherits(anychart.elements.marker.styles.MarkerElementStyleState,
        anychart.elements.styles.BaseElementStyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.type_ = -1;
/**
 * @return {int} Type.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getType = function() {
    return this.type_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.dynamicType_ = false;
/**
 * @return {Boolean} Is type dynamic.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.isDynamicType = function() {
    return this.dynamicType_;
};
/**
 * @private
 * @type {Number}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.size_ = 10;
/**
 * @return {Number} Size.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getSize = function() {
    return this.size_;
};
/**
 * Marker size width (old config).
 * @private
 * @type {Number}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.sizeWidth_ = 0;
/**
 * @return {Number} Size width.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getWidth = function() {
    return this.width_;
};
/**
 * Marker size height (old config).
 * @private
 * @type {Number}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.sizeHeigth_ = 0;
/**
 * @return {Number} Height.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getHeight = function() {
    return this.height_;
};
/**
 * Marker background.
 * @private
 * @type {anychart.visual.background.Background}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.background_ = null;
/**
 * @return {anychart.visual.background.Background.} Background.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getBackground = function() {
    return this.background_;
};
/**
 * @return {anychart.visual.effects.EffectsList}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getEffects = function() {
    return this.background_.getEffects();
};
/**
 * Marker bounds.
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle} Bounds.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getBounds = function() {
    return this.bounds_;
};
//------------------------------------------------------------------------------
//                              Definers.
//------------------------------------------------------------------------------
/**
 * @return {Boolean} Has background or not.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.hasBackground = function() {
    return this.background_ && this.background_.isEnabled();
};
/**
 * @return {Boolean}
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.needUpdateEffects = function() {
    return this.hasBackground() && this.background_.hasEffects();
};
/**
 * @return {Boolean} Need update fill or not.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.needUpdateFill = function() {
    return this.hasBackground() &&
            this.background_.getFill() &&
            this.background_.getFill().isEnabled();
};
/**
 * @return {Boolean} Need update border or not.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.needUpdateBorder = function() {
    return this.hasBackground() &&
            this.background_.getBorder() &&
            this.background_.getBorder().isEnabled();
};
/**
 * @return {Boolean} Need update fill shape or not.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.
        needUpdateFillShape = function() {
    return this.needUpdateFill() || this.needUpdateBorder();
};
/**
 * @return {Boolean} Need update hatchfill or not.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.
        needUpdateHatchFill = function() {
    return this.hasBackground() &&
            this.background_.getHatchFill() &&
            this.background_.getFill().isEnabled();
};
//------------------------------------------------------------------------------
//                              Getters.
//------------------------------------------------------------------------------
/**
 * @return {anychart.visual.fill.Fill} Fill.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.
        getFill = function() {
    return this.background_.getFill();
};
/**
 * @return {anychart.visual.hatchFill.HatchFill} Hatchfill.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getHatchFill =
        function() {
            return this.background_.getHatchFill();
        };
/**
 * @return {anychart.visual.stroke.Stroke} Background border.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.getBorder =
        function() {
            return this.background_.getBorder();
        };
/**
 * @param {anychart.plots.seriesPlot.data.BaseDynamicElementsContainer} container
 * Container.
 * @return {anychart.elements.marker.styles.MarkerElementStyleState} Marker
 * type.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.
        getMarkerType = function(container) {
    return this.dynamicType_ ? container.getMarkerType() : this.type_;
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize marker style state settings.
 * @override
 * @param {Object} data JSON object with marker settings.
 * @return {Object} JSON object.
 */
anychart.elements.marker.styles.MarkerElementStyleState.prototype.deserialize =
        function(data) {
            data = goog.base(this, 'deserialize', data);
            var des = anychart.utils.deserialization;

            var marker = des.getProp(data, 'marker');
            //type
            if (des.hasProp(data, 'marker_type')) {
                if (marker && des.hasProp(marker, 'type')) {
                    var type = des.getLStringProp(marker, 'type');
                    des.setProp(marker, 'type',
                            type.split('%markertype').
                                    join(des.getStringProp(data, 'marker_type')));
                }
            }

            if (marker) {
                if (des.hasProp(marker, 'anchor'))
                    this.anchor = anychart.layout.Anchor.deserialize(
                            des.getProp(marker, 'anchor'));
                if (des.hasProp(marker, 'size'))
                    this.size_ = des.getNumProp(marker, 'size');
                if (des.hasProp(marker, 'size_width'))
                    this.sizeWidth_ = des.getNumProp(marker, 'size_width');
                if (des.hasProp(marker, 'size_height'))
                    this.sizeHeigth_ = des.getNumProp(marker, 'size_height');
                if (des.hasProp(marker, 'padding'))
                    this.padding = des.getNumProp(marker, 'padding');
                if (des.hasProp(marker, 'h_align'))
                    this.hAlign = anychart.layout.HorizontalAlign.deserialize(
                            des.getProp(marker, 'h_align'));
                if (des.hasProp(marker, 'v_align'))
                    this.vAlign = anychart.layout.VerticalAlign.deserialize(
                            des.getProp(marker, 'v_align'));

                if (des.hasProp(marker, 'type')) {
                    var strType = anychart.utils.deserialization.getLStringProp(
                            marker, 'type');
                    this.dynamicType_ = strType == '%markertype';
                    if (!this.dynamicType_)
                        this.type_ = anychart.elements.marker.MarkerType.
                                deserialize(strType);
                }
            }

            if (this.sizeHeigth_ == 0 && this.sizeWidth_ == 0)
                this.sizeHeigth_ = this.sizeWidth_ = this.size_;

            if (this.anchor == anychart.layout.Anchor.X_AXIS) {
                this.hAlign = anychart.layout.HorizontalAlign.CENTER;
                this.vAlign = anychart.layout.VerticalAlign.BOTTOM;
            }

            this.background_ = new anychart.visual.background.Background();
            this.background_.deserialize(data);
            this.bounds_ = new anychart.utils.geom.Rectangle(0, 0, this.sizeWidth_,
                    this.sizeHeigth_);

            return data;
        };
//------------------------------------------------------------------------------
//
//                          MarkerElementStyle class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.elements.marker.styles.MarkerElementStyle = function() {
    anychart.elements.styles.BaseElementsStyle.call(this);
};
goog.inherits(anychart.elements.marker.styles.MarkerElementStyle,
        anychart.elements.styles.BaseElementsStyle);
//------------------------------------------------------------------------------
//                              StyleState.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.
        getStyleStateClass = function() {
    return anychart.elements.marker.styles.MarkerElementStyleState;
};
/**
 * @inheritDoc
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.createStyleShapes =
        function() {
            return new anychart.elements.marker.styles.MarkerElementStyleShapes();
        };
//------------------------------------------------------------------------------
//                         Definers
//------------------------------------------------------------------------------
/**
 * @return {Boolean} Need create fill shape or not.
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.
        needCreateFillShape = function() {
    return this.normal_.needUpdateFillShape() ||
        this.hover_.needUpdateFillShape() ||
        this.pushed_.needUpdateFillShape() ||
        this.selectedNormal_.needUpdateFillShape() ||
        this.selectedHover_.needUpdateFillShape() ||
        this.missing_.needUpdateFillShape();
};
/**
 * @return {Boolean} Need create stroke shape or not.
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.
        needCreateStrokeShape = function() {
    return this.normal_.needUpdateBorder() ||
        this.hover_.needUpdateBorder() ||
        this.pushed_.needUpdateBorder() ||
        this.selectedNormal_.needUpdateBorder() ||
        this.selectedHover_.needUpdateBorder() ||
        this.missing_.needUpdateBorder();
};
/**
 * @return {Boolean} Need create fill and stroke shape or not.
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.
        needCreateFillAndStrokeShape = function() {
    return this.normal_.needUpdateFillShape() ||
            this.hover_.needUpdateFillShape() ||
            this.pushed_.needUpdateFillShape() ||
            this.selectedNormal_.needUpdateFillShape() ||
            this.selectedHover_.needUpdateFillShape() ||
            this.missing_.needUpdateFillShape();
};
/**
 * @return {Boolean} Need create hatchfill shape.
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.
        needCreateHatchFillShape = function() {
    return this.normal_.needUpdateHatchFill() ||
            this.hover_.needUpdateHatchFill() ||
            this.pushed_.needUpdateHatchFill() ||
            this.selectedNormal_.needUpdateHatchFill() ||
            this.selectedHover_.needUpdateHatchFill() ||
            this.missing_.needUpdateHatchFill();
};
/**
 * @return {Boolean} Has gradient or not.
 */
anychart.elements.marker.styles.MarkerElementStyle.prototype.hasGradient =
        function() {
            return this.normal_.hasGradient() ||
                    this.hover_.hasGradient() ||
                    this.pushed_.hasGradient() ||
                    this.selectedNormal_.hasGradient() ||
                    this.selectedHover_.hasGradient() ||
                    this.missing_.hasGradient();
        };

