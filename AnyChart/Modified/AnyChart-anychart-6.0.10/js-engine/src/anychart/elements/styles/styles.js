/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.elements.styles.BaseElementStyleState}</li>
 *  <li>@class {anychart.elements.styles.BaseElementsStyleShapes}</li>
 *  <li>@class {anychart.elements.styles.BaseElementsStyle}.</li>
 * <ul>
 */
goog.provide('anychart.elements.styles');
goog.require('anychart.styles');
//------------------------------------------------------------------------------
//
//                           BaseElementStyleState class.
//
//------------------------------------------------------------------------------
/**
 * Class BaseElementStyleState is a base class for all elements style state
 * classes.
 * @constructor
 * @param {anychart.styles.Style} style Style for this state.
 * @param {anychart.styles.StateType} opt_stateType State type.
 * @extends {anychart.styles.StyleState}
 */
anychart.elements.styles.BaseElementStyleState = function(style,
                                                          opt_stateType) {
    anychart.styles.StyleState.call(this, style, opt_stateType);
};
//inheritance
goog.inherits(anychart.elements.styles.BaseElementStyleState,
        anychart.styles.StyleState);
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * Enabled style.
 * @protected
 * @type {Boolean}
 */
anychart.elements.styles.BaseElementStyleState.prototype.enabled = true;
/**
 * @return {Boolean} is element enabled.
 */
anychart.elements.styles.BaseElementStyleState.prototype.isEnabled =
    function() {
        return this.enabled;
    };
/**
 * Base element padding.
 * @protected
 * @type {Number}
 */
anychart.elements.styles.BaseElementStyleState.prototype.padding = 5;
/**
 * @return {Number} element padding.
 */
anychart.elements.styles.BaseElementStyleState.prototype.getPadding =
    function() {
        return this.padding;
    };
/**
 * @param {Number} value New element padding.
 */
anychart.elements.styles.BaseElementStyleState.prototype.setPadding =
    function(value) {
        this.padding = value;
    };
/**
 * Base element anchor value.
 * @protected
 * @type {anychart.layout.Anchor}
 */
anychart.elements.styles.BaseElementStyleState.prototype.anchor =
        anychart.layout.Anchor.CENTER;
/**
 * @return {anychart.layout.Anchor} Get anchor.
 */
anychart.elements.styles.BaseElementStyleState.prototype.getAnchor =
    function() {
        return this.anchor;
    };
/**
 * @param {anychart.layout.Anchor} value New element anchor.
 */
anychart.elements.styles.BaseElementStyleState.prototype.setAnchor =
    function(value) {
        this.anchor = value;
    };
/**
 * Base element horizontal align value.
 * @protected
 * @type {anychart.layout.HorizontalAlign}
 */
anychart.elements.styles.BaseElementStyleState.prototype.hAlign =
        anychart.layout.HorizontalAlign.CENTER;
/**
 * @return {anychart.layout.HorizontalAlign} Horiztonal element aligment.
 */
anychart.elements.styles.BaseElementStyleState.prototype.getHAlign =
    function() {
        return this.hAlign;
    };
/**
 * Base element vertical align value.
 * @protected
 * @type {anychart.layout.VerticalAlign}
 */
anychart.elements.styles.BaseElementStyleState.prototype.vAlign =
        anychart.layout.VerticalAlign.CENTER;
/**
 * @return {anychart.layout.VerticalAlign} Vertical element aligment.
 */
anychart.elements.styles.BaseElementStyleState.prototype.getVAlign =
    function() {
        return this.vAlign;
    };
//------------------------------------------------------------------------------
//                          Deserialize
//------------------------------------------------------------------------------
/**
 * Deserialize base element style state settings.
 * @param {object} data JSON object with base element style state settings.
 * @return {object} Modified JSON object with base element style state settings.
 */
anychart.elements.styles.BaseElementStyleState.prototype.deserialize =
    function(data) {
        data = goog.base(this, 'deserialize', data);
        var des = anychart.utils.deserialization;
        this.enabled = des.isEnabled(data);
        if (des.hasProp(data, 'hatch_type')) {
            var hatchType = des.getStringProp(data, 'hatch_type');
            if (des.hasProp(data, 'hatch_fill')) {
                var hatchFill = des.getProp(data, 'hatch_fill');
                if (des.hasProp(hatchFill, 'type')) {
                    var type = des.getLStringProp(hatchFill, 'type');
                    des.setProp(hatchFill,
                            'type',
                            type.split('%hatchtype').join(hatchType));
                }
            }
        }
        return data;
    };
//------------------------------------------------------------------------------
//
//                          BaseBaseElementsStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.elements.styles.BaseElementsStyleShapes = function() {
};
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.seriesPlot.data.BasePoint}
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.point_ = null;
/**
 * @private
 * @type {anychart.elements.BaseElement}
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.element_ = null;
/**
 * @private
 * @type {anychart.elements.ElementsSprite}
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.sprite_ = null;
//------------------------------------------------------------------------------
//                          Initialize.
//------------------------------------------------------------------------------
/**
 * Initialize element style shapes
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point.
 * @param {anychart.elements.BaseElement} element Element.
 * @param {anychart.elements.ElementsSprite} sprite Sprite.
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.initialize =
    function(point, element, sprite) {
        this.point_ = point;
        this.element_ = element;
        this.sprite_ = sprite;

        this.createShapes(point, element);
        this.placeShapes(sprite);
    };

/**
 * Creates shapes.
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point.
 * @param {anychart.elements.BaseElement} element Element.
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.createShapes = goog.abstractMethod;
/**
 * Place shapes elements at passed sprite.
 * @param {anychart.elements.ElementsSprite} sprite Sprite.
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.placeShapes = goog.abstractMethod;
//------------------------------------------------------------------------------
//                              Update.
//------------------------------------------------------------------------------
/**
 * @param {anychart.elements.styles.BaseElementStyleState} state State.
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.update =
    function(state) {
        //position
        this.element_.setPosition(
                this.element_.getPosition(
                        this.point_,
                        state,
                        this.sprite_),
                this.sprite_);
    };
/**
 * Sets visibility.
 * @param {Boolean} value Value.
 */
anychart.elements.styles.BaseElementsStyleShapes.prototype.setVisible = function (value) {
    if (value) this.sprite_.getElement().setAttribute('visibility', 'visible');
    else this.sprite_.getElement().setAttribute('visibility', 'hidden');
};
//------------------------------------------------------------------------------
//
//                          BaseElementsStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.Style}
 */
anychart.elements.styles.BaseElementsStyle = function() {
    anychart.styles.Style.call(this);
};
goog.inherits(anychart.elements.styles.BaseElementsStyle,
        anychart.styles.Style);
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**
 * @param {anychart.plots.seriesPlot.data.BasePoint} point Point.
 * @param {anychart.elements.BaseElement} element Element.
 * @return {anychart.elements.ElementsSprite} Sprite.
 */
anychart.elements.styles.BaseElementsStyle.prototype.initialize =
    function(point, element) {
        if (!this.needCreateElementSprite(element)) return null;
        var sprite = element.createSprite(point.getSVGManager());
        var shapes = this.createStyleShapes();
        shapes.initialize(point, element, sprite);
        sprite.setStyleShapes(shapes);
        element.getElementContainer(point).appendSprite(sprite);
        return sprite;
    };
//------------------------------------------------------------------------------
//                          StyleShapes.
//------------------------------------------------------------------------------
/**
 * Creates style shapes.
 */
anychart.elements.styles.BaseElementsStyle.prototype.createStyleShapes = goog.abstractMethod;
//------------------------------------------------------------------------------
//                            Definers.
//------------------------------------------------------------------------------
/**
 * Define, need create sprite for passed element.
 * @param {anychart.elements.BaseElement} element Element.
 * @return {Boolean} Whether need to create element sprite.
 */
anychart.elements.styles.BaseElementsStyle.prototype.needCreateElementSprite =
    function(element) {
        return element.isEnabled() &&
                (this.normal_.isEnabled() ||
                        this.hover_.isEnabled() ||
                        this.pushed_.isEnabled() ||
                        this.selectedHover_.isEnabled() ||
                        this.selectedNormal_.isEnabled() ||
                        this.missing_.isEnabled());
    };
