/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.dashboard.DashboardElement}.</li>
 *  <li>@class {anychart.dashboard.ChartElement}.</li>
 *  <li>@class {anychart.dashboard.DashboardElementContainer}.</li>
 *  <li>@class {anychart.dashboard.PanelTitle}.</li>
 *  <li>@class {anychart.dashboard.Panel}.</li>
 *  <li>@class {anychart.dashboard.DashboardHBox}.</li>
 *  <li>@class {anychart.dashboard.DashboardVBox}.</li>
 *  <li>@class {anychart.dashboard.Dashboard}.</li>
 *  <li>@class {anychart.dashboard.DashboardElementFactory}.</li>
 * <ul>
 */
goog.provide('anychart.dashboard');
goog.require('anychart.layout');
goog.require('anychart.svg');
goog.require('anychart.utils');
//------------------------------------------------------------------------------
//
//                          DashboardElement class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.dashboard.DashboardElement = function(dashboard) {
    this.dashboard_ = dashboard;

    this.x_ = 0;
    this.calculatedX_ = NaN;
    this.isPercentX_ = true;

    this.y_ = 0;
    this.calculatedY_ = NaN;
    this.isPercentY_ = true;

    this.width_ = 1;
    this.isPercentWidth_ = true;
    this.calculatedWidth_ = NaN;

    this.height_ = 1;
    this.isPercentHeight_ = true;
    this.calculatedHeight_ = NaN;

    this.margin_ = new anychart.layout.Margin(5);
};
//------------------------------------------------------------------------------
//                           Bounds properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.dashboard.DashboardElement.prototype.margin_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.dashboard.DashboardElement.prototype.bounds_ = null;
//------------------------------------------------------------------------------
//                          Visual properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.dashboard.DashboardElement.prototype.sprite_ = null;
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.dashboard.DashboardElement.prototype.svgManager_ = null;
/**
 * @param {anychart.svg.SVGManager} svgManager
 */
anychart.dashboard.DashboardElement.prototype.initializeSVG = function(svgManager) {
    this.svgManager_ = svgManager;
};
//------------------------------------------------------------------------------
//                           X coordinate.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.x_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getX = function(bounds) {
    if (isNaN(this.calculatedX_))
        return bounds.x + (this.isPercentX_ ? bounds.width * this.x_ : this.x_);
    return this.calculatedX_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.DashboardElement.prototype.isPercentX_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.calculatedX_ = null;
/**
 * @param {Number} value
 */
anychart.dashboard.DashboardElement.prototype.setCalculatedX = function(value) {
    this.calculatedX_ = value;
};
//------------------------------------------------------------------------------
//                           Y coordinate.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.y_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getY = function(bounds) {
    if (isNaN(this.calculatedY_))
        return bounds.y + (this.isPercentY_ ? bounds.height * this.y_ : this.y_);
    return this.calculatedY_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.DashboardElement.prototype.isPercentY_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.calculatedY_ = null;
/**
 * @param {Number} value
 */
anychart.dashboard.DashboardElement.prototype.setCalculatedY = function(value) {
    this.calculatedY_ = value;
};
//------------------------------------------------------------------------------
//                           Width.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.width_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getWidth = function(bounds) {
    if (isNaN(this.calculatedWidth_))
        return this.isPercentWidth_ ?
            bounds.width * this.width_ : this.width_;
    return this.calculatedWidth_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.DashboardElement.prototype.isPercentWidth_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.calculatedWidth_ = null;
/**
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getCalculatedWidth = function() {
    return this.calculatedWidth_;
};
/**
 * @param {Number} value
 */
anychart.dashboard.DashboardElement.prototype.setCalculatedWidth = function(value) {
    this.calculatedWidth_ = value;
};
//------------------------------------------------------------------------------
//                           Height.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.height_ = null;
/**
 * @param {anychart.utils.geom.Rectangle} bounds
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getHeight = function(bounds) {
    if (isNaN(this.calculatedHeight_))
        return this.isPercentHeight_ ?
            bounds.height * this.height_ : this.height_;
    return this.calculatedHeight_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.DashboardElement.prototype.isPercentHeight_ = null;
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.DashboardElement.prototype.calculatedHeight_ = null;
/**
 * @return {Number}
 */
anychart.dashboard.DashboardElement.prototype.getCalculatedHeight = function() {
    return this.calculatedHeight_;
};
/**
 * @param {Number} value
 */
anychart.dashboard.DashboardElement.prototype.setCalculatedHeight = function(value) {
    this.calculatedHeight_ = value;
};
//------------------------------------------------------------------------------
//                           Dashboard.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.dashboard.Dashboard}
 */
anychart.dashboard.DashboardElement.prototype.dashboard_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**
 * Deserialize dashboard element settings.
 * @param {Object} data
 */
anychart.dashboard.DashboardElement.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    var strValue;

    if (des.hasProp(data, 'margin')) {
        this.margin_ = new anychart.layout.Margin();
        this.margin_.deserialize(des.getProp(data, 'margin'));
    }

    if (des.hasProp(data, 'x')) {
        strValue = des.getProp(data, 'x');
        this.x_ = des.isPercent(strValue) ?
            des.getPercent(strValue) :
            des.getNum(strValue);
    }

    if (des.hasProp(data, 'y')) {
        strValue = des.getProp(data, 'y');
        this.y_ = des.isPercent(strValue) ?
            des.getPercent(strValue) :
            des.getNum(strValue);
    }

    if (des.hasProp(data, 'width')) {
        strValue = des.getProp(data, 'width');
        this.width_ = des.isPercent(strValue) ?
            des.getPercent(strValue) :
            des.getNum(strValue);
    }

    if (des.hasProp(data, 'height')) {
        strValue = des.getProp(data, 'height');
        this.height_ = des.isPercent(strValue) ?
            des.getPercent(strValue) :
            des.getNum(strValue);
    }
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * Initialize dashboard element.
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.dashboard.DashboardElement.prototype.initVisual = function(svgManager, bounds) {
    if (!this.sprite_)
        this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.setLayout(bounds);
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw dashboard element
 */
anychart.dashboard.DashboardElement.prototype.draw = function() {
};
//------------------------------------------------------------------------------
//                             Resize.
//------------------------------------------------------------------------------
/**
 * Resize dashboard element.
 * @param {anychart.utils.geom.Rectangle} bounds New element bounds.
 */
anychart.dashboard.DashboardElement.prototype.resize = function(bounds) {
    this.setLayout(bounds);
};
//------------------------------------------------------------------------------
//                              Layout.
//------------------------------------------------------------------------------
/**
 * Calculate dashboard element layout by passed bounds.
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.dashboard.DashboardElement.prototype.setLayout = function(bounds) {
    this.setPosition(bounds);
};
/**
 * Calculate dashboard element position by passed bounds.
 * @protected
 * @param {anychart.utils.geom.Rectangle} bounds
 */
anychart.dashboard.DashboardElement.prototype.setPosition = function(bounds) {
    this.bounds_ = bounds.clone();

    this.bounds_.x = this.getX(bounds);
    this.bounds_.y = this.getY(bounds);
    this.bounds_.width = this.getWidth(bounds);
    this.bounds_.height = this.getHeight(bounds);

    this.sprite_.setPosition(this.bounds_.x, this.bounds_.y);

    this.bounds_.x = 0;
    this.bounds_.y = 0;

    if (this.margin_) this.margin_.applyInside(this.bounds_);
};
//------------------------------------------------------------------------------
//
//                          ChartElement class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.DashboardElement}
 */
anychart.dashboard.ChartElement = function(dashboard) {
    anychart.dashboard.DashboardElement.call(this, dashboard);
    this.view_ = new anychart.chartView.DashboardChartView(dashboard.getMainChartView(), this);
};
goog.inherits(anychart.dashboard.ChartElement, anychart.dashboard.DashboardElement);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.DashboardChartView}
 */
anychart.dashboard.ChartElement.prototype.view_ = null;
/**
 * @private
 * @type {String}
 */
anychart.dashboard.ChartElement.prototype.name_ = null;
/**
 * @return {String}
 */
anychart.dashboard.ChartElement.prototype.getName = function() {
    return this.name_;
};
/**
 * @private
 * @type {String}
 */
anychart.dashboard.ChartElement.prototype.chartSource_ = null;
//------------------------------------------------------------------------------
//                          Deserialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.ChartElement.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);

    var des = anychart.utils.deserialization;
    this.dashboard_.getViewList().push(this.view_);

    if (des.hasProp(data, 'name')) {
        this.name_ = des.getStringProp(data, 'name');
        this.dashboard_.addView(this.name_, this.view_);
    }
    
    if (des.hasProp(data, 'source')) {
        var source = des.getStringProp(data, 'source');
        var isExternal = false;

        if (des.hasProp(data, 'source_mode')) {
            var tempVal = des.getEnumProp(data, 'source_mode');
            isExternal = tempVal === 'external' || tempVal === 'externaldata';
        }

        if (isExternal) this.view_.setXmlFilePath(source);
        else this.chartSource_ = source;
    }
};

/**
 * @inheritDoc
 */
anychart.dashboard.ChartElement.prototype.initVisual = function(svgManager, bounds) {
    goog.base(this, 'initVisual', svgManager, bounds);

    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('DashboardChartElement');

    //chart config source
    if (this.chartSource_) {
        this.view_.setConfig(this.dashboard_.getMainChartView().getChartConfig(
            this.chartSource_));

        this.view_.setChartType(this.dashboard_.getMainChartView().getChartType(
            this.chartSource_));
    }
    this.chartSource_ = null;

    this.view_.initVisual(
        svgManager,
        this.sprite_,
        new anychart.utils.geom.Rectangle(
            0,
            0,
            this.bounds_.width,
            this.bounds_.height));

    if (this.margin_) {
        this.sprite_.incrementX(this.margin_.getLeft());
        this.sprite_.incrementY(this.margin_.getTop());
    }

    this.view_.applyDashboardConfig();


    return this.sprite_;
};
//------------------------------------------------------------------------------
//                        Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.ChartElement.prototype.resize = function(bounds) {
    goog.base(this, 'resize', bounds);

    this.view_.resize(new anychart.utils.geom.Rectangle(
        0,
        0,
        this.bounds_.width,
        this.bounds_.height));
};
//------------------------------------------------------------------------------
//
//                          DashboardElementContainer.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.DashboardElement}
 */
anychart.dashboard.DashboardElementContainer = function(dashboard) {
    anychart.dashboard.DashboardElement.call(this, dashboard);
    this.children_ = [];
};
goog.inherits(anychart.dashboard.DashboardElementContainer,
    anychart.dashboard.DashboardElement);
//------------------------------------------------------------------------------
//                              Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<anychart.dashboard.DashboardElement>}
 */
anychart.dashboard.DashboardElementContainer.prototype.children_ = null;

/**
 * TODO: Write docs.
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.dashboard.DashboardElementContainer.prototype.childrenContainer_ = null;

/**
 * TODO: Write docs.
 * @protected
 * @type {anychart.svg.SVGSprite}
 */
anychart.dashboard.DashboardElementContainer.prototype.contentContainer_ = null;


//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.DashboardElementContainer.prototype.deserialize = function(data) {
    goog.base(this, 'deserialize', data);
    var des = anychart.utils.deserialization;
    for (var i in data) {
        var node = data[i];
        if (anychart.utils.JSONUtils.isSimpleProperty(node)) continue;
        if (anychart.utils.JSONUtils.isObject(node))
            this.createDashboardElement(node, i);
        if (goog.isArray(node)) {
            var length = node.length;
            for (var j = 0; j < length; j++)
                this.createDashboardElement(node[j], i);
        }
    }
};
anychart.dashboard.DashboardElementContainer.prototype.createDashboardElement = function(data, nodeName) {
    var element = anychart.dashboard.DashboardElementFactory.createElement(
        data,
        nodeName,
        this.dashboard_);
    if (element) {
        element.deserialize(data);
        this.children_.push(element);
    }
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.DashboardElementContainer.prototype.initVisual = function(svgManager, bounds) {
    this.childrenContainer_ = new anychart.svg.SVGSprite(svgManager);
    this.contentContainer_ = new anychart.svg.SVGSprite(svgManager);
    goog.base(this, 'initVisual', svgManager, bounds);
    this.sprite_.appendSprite(this.contentContainer_);
    this.sprite_.appendSprite(this.childrenContainer_);
    this.initializeChildren(svgManager);

    if (IS_ANYCHART_DEBUG_MOD) {
        this.childrenContainer_.setId('DashboardChildren');
        this.contentContainer_.setId('DashboardContent');
        this.sprite_.setId('DashboardElementContainer');
    }

    return this.sprite_;
};
/**
 * Initialize dashboard container children.
 * @protected
 */
anychart.dashboard.DashboardElementContainer.prototype.initializeChildren = function(svgManager) {
    var childrenCount = this.children_.length;
    for (var i = 0; i < childrenCount; i++)
        this.childrenContainer_.appendSprite(
            this.children_[i].initVisual(svgManager, this.bounds_));
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.DashboardElementContainer.prototype.draw = function() {
    var childrenCount = this.children_.length;
    for (var i = 0; i < childrenCount; i++)
        this.children_[i].draw();
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.DashboardElementContainer.prototype.resize = function(bounds) {
    goog.base(this, 'resize', bounds);
    this.resizeChildren();
};
/**
 * Resize dashboard container children.
 * @protected
 */
anychart.dashboard.DashboardElementContainer.prototype.resizeChildren = function() {
    var childrenCount = this.children_.length;
    for (var i = 0; i < childrenCount; i++)
        this.children_[i].resize(this.bounds_);
};
//------------------------------------------------------------------------------
//
//                          PanelTitle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.MainChartView} mainChartView
 * @param {anychart.formatting.ITextFormatInfoProvider} formatter
 * @extends {anychart.visual.text.InteractiveTitle}
 */
anychart.dashboard.PanelTitle = function(mainChartView) {
    anychart.visual.text.InteractiveTitle.call(this, mainChartView, this);
};
goog.inherits(anychart.dashboard.PanelTitle,
    anychart.visual.text.InteractiveTitle);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.dashboard.PanelTitle.prototype.space_ = null;
/**
 * @return {Number}
 */
anychart.dashboard.PanelTitle.prototype.getSpace = function() {
    return this.space_;
};
//------------------------------------------------------------------------------
//                              Size.
//------------------------------------------------------------------------------
anychart.dashboard.PanelTitle.prototype.initSize = function(svgManager) {
    goog.base(this, 'initSize', svgManager, this.textFormatter_.getValue());
    this.space_ = this.padding_ + this.bounds_.height;
};
//------------------------------------------------------------------------------
//                             Initialize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.PanelTitle.prototype.createSVGText = function(svgManager, panelBounds) {
    var sprite = goog.base(this, 'createSVGText', svgManager);
    this.setPosition(sprite, panelBounds);
    return sprite;
};
//------------------------------------------------------------------------------
//                          Position.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGSprite} sprite
 * @param {anychart.utils.geom.Rectangle} panelBounds
 */
anychart.dashboard.PanelTitle.prototype.setPosition = function(sprite, panelBounds) {
    var x;
    switch (this.align_) {
        case anychart.layout.AbstractAlign.NEAR:
            x = panelBounds.x;
            break;
        case anychart.layout.AbstractAlign.FAR:
            x = panelBounds.x + panelBounds.width - this.bounds_.width;
            break;
        case anychart.layout.AbstractAlign.CENTER:
            x = panelBounds.x + (panelBounds.width - this.bounds_.width) / 2;
            break;
    }
    sprite.setPosition(x, panelBounds.y);
};
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------
/**
 * @param {String} token
 * @return {String}
 */
anychart.dashboard.PanelTitle.prototype.getTokenValue = function(token) {
    return '';
};
/**
 * @param {String} token
 * @return {String}
 */
anychart.dashboard.PanelTitle.prototype.getTokenType = function(token) {
    return anychart.formatting.TextFormatTokenType.TEXT;
};
//------------------------------------------------------------------------------
//
//                              Panel.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.DashboardElementContainer}
 */
anychart.dashboard.Panel = function(dashboard) {
    anychart.dashboard.DashboardElementContainer.call(this, dashboard);
};
goog.inherits(anychart.dashboard.Panel,
    anychart.dashboard.DashboardElementContainer);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.dashboard.PanelTitle}
 */
anychart.dashboard.Panel.prototype.title_ = null;
/**
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.dashboard.Panel.prototype.titleSprite_ = null;
/**
 * @private
 * @type {anychart.visual.background.RectBackground}
 */
anychart.dashboard.Panel.prototype.background_ = null;
/**
 * @private
 * @type {anychart.visual.background.SVGBackgroundSprite}
 */
anychart.dashboard.Panel.prototype.backgroundSprite_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.dashboard.Panel.prototype.panelTotalBounds_ = null;
/**
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.dashboard.Panel.prototype.contentTotalBounds_ = null;
//------------------------------------------------------------------------------
//                          Deserialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.Panel.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    data = this.mergeDashboardTemplate(des.copy(anychart.templates.DEFAULT_TEMPLATE['defaults']['panel']), data);
    this.deserializeWithoutTemplate(data);
};
/**
 * @param {Object} data
 */
anychart.dashboard.Panel.prototype.deserializeWithoutTemplate = function(data) {
    //из за того что closure не позволяет вызывать override методы суперкласса
    // из медода с другим названием, вызов идет по старинке напрямую.
    anychart.dashboard.Panel.superClass_.deserialize.call(this, data);
    var des = anychart.utils.deserialization;
    if (des.isEnabledProp(data, 'title')) {
        var titleData = des.getProp(data, 'title');
        if (des.hasProp(titleData, 'text')) {
            this.title_ = new anychart.dashboard.PanelTitle(
                this.dashboard_.getMainChartView());
            this.title_.deserialize(titleData);
        }
    }

    if (des.hasProp(data, 'background')) {
        this.background_ = new anychart.visual.background.RectBackground();
        this.background_.deserialize(des.getProp(data, 'background'));
    }

};
/**
 * @protected
 * @param {Object} data
 */
anychart.dashboard.Panel.prototype.mergeDashboardTemplate = function(template, data) {
    var des = anychart.utils.deserialization;
    var res = des.copy(data);

    anychart.utils.JSONUtils.mergeSimpleProperty(template, data, res);
    for (var i in template) {
        var node = template[i];
        if (anychart.utils.JSONUtils.isSimpleProperty(node)) continue;

        if (res[i]) res[i] = anychart.utils.JSONUtils.merge(
            node,
            res[i],
            i);
        else res[i] = node;
    }

    return res;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.Panel.prototype.initVisual = function(svgManager, bounds) {
    if (this.title_) this.title_.initSize(svgManager);

    goog.base(this, 'initVisual', svgManager, bounds);

    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('DashboardPanel');

    if (this.background_ && this.background_.isEnabled()) {
        this.backgroundSprite_ = this.background_.initialize(svgManager);
        this.contentContainer_.appendSprite(this.backgroundSprite_);
    }


    if (this.title_) {
        this.titleSprite_ = this.title_.createSVGText(
            svgManager,
            this.contentTotalBounds_);
        this.contentContainer_.appendSprite(this.titleSprite_);
    }

    if (IS_ANYCHART_DEBUG_MOD) {
        this.sprite_.setId('DashboardPanel');
        if (this.backgroundSprite_) this.backgroundSprite_.setId('DashboardBackground');
        if (this.titleSprite_) this.titleSprite_.setId('DashboardTitle');
    }

    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * Draw panel.
 */
anychart.dashboard.Panel.prototype.draw = function() {
    goog.base(this, 'draw');
    if (this.backgroundSprite_)
        this.background_.draw(this.backgroundSprite_, this.panelTotalBounds_);
};
//------------------------------------------------------------------------------
//                            Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.Panel.prototype.resize = function(bounds) {
    goog.base(this, 'resize', bounds);

    if (this.titleSprite_)
        this.title_.setPosition(this.titleSprite_, this.panelTotalBounds_);

    if (this.backgroundSprite_)
        this.background_.resize(this.backgroundSprite_, this.panelTotalBounds_);
};
//------------------------------------------------------------------------------
//                          Layout.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.Panel.prototype.setLayout = function(bounds) {
    goog.base(this, 'setLayout', bounds);
    this.panelTotalBounds_ = this.bounds_.clone();

    if (this.background_ && this.background_.isEnabled())
        this.background_.applyInsideMargin(this.bounds_);
    this.contentTotalBounds_ = this.bounds_.clone();

    if (this.title_) {
        this.bounds_.y += this.title_.getSpace();
        this.bounds_.height -= this.title_.getSpace();
    }
};
//------------------------------------------------------------------------------
//
//                          DashboardHBox class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.DashboardElementContainer}
 */
anychart.dashboard.DashboardHBox = function(dashboard) {
    anychart.dashboard.DashboardElementContainer.call(this, dashboard);
};
goog.inherits(anychart.dashboard.DashboardHBox,
    anychart.dashboard.DashboardElementContainer);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.DashboardHBox.prototype.initVisual = function(svgManager, bounds) {
    goog.base(this, 'initVisual', svgManager, bounds);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('HBox');
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                              Layout.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.DashboardHBox.prototype.setLayout = function(bounds) {
    goog.base(this, 'setLayout', bounds);
    var tmpX = this.bounds_.x;
    var childrenCount = this.children_.length;
    for (var i = 0; i < childrenCount; i++) {
        var child = this.children_[i];
        child.setCalculatedX(tmpX);
        child.setCalculatedWidth(NaN);
        child.setCalculatedWidth(child.getWidth(bounds));
        tmpX += child.getCalculatedWidth();
    }
};
//------------------------------------------------------------------------------
//
//                          DashboardVBox class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.DashboardElementContainer}
 */
anychart.dashboard.DashboardVBox = function(dashboard) {
    anychart.dashboard.DashboardElementContainer.call(this, dashboard);
};
goog.inherits(anychart.dashboard.DashboardVBox,
    anychart.dashboard.DashboardElementContainer);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.DashboardVBox.prototype.initVisual = function(svgManager, bounds) {
    goog.base(this, 'initVisual', svgManager, bounds);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('VBox');
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                              Layout.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.DashboardVBox.prototype.setLayout = function(bounds) {
    goog.base(this, 'setLayout', bounds);
    var tmpY = this.bounds_.y;
    var childrenCount = this.children_.length;
    for (var i = 0; i < childrenCount; i++) {
        var child = this.children_[i];
        child.setCalculatedY(tmpY);
        child.setCalculatedHeight(NaN);
        child.setCalculatedHeight(child.getHeight(bounds));
        tmpY += child.getCalculatedHeight();
    }
};
//------------------------------------------------------------------------------
//
//                          Dashboard class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.dashboard.Panel}
 * @param {anychart.chartView.MainChartView} mainChartView
 * @implements {anychart.chartView.IChartViewElement}
 */
anychart.dashboard.Dashboard = function(mainChartView) {
    anychart.dashboard.Panel.call(this, this);
    this.mainChartView_ = mainChartView;
    this.viewsMap_ = {};
    this.viewList_ = [];
};
goog.inherits(anychart.dashboard.Dashboard, anychart.dashboard.Panel);
//------------------------------------------------------------------------------
//                          View properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.chartView.ChartView}
 */
anychart.dashboard.Dashboard.prototype.mainChartView_ = null;
/**
 * @return {anychart.chartView.ChartView}
 */
anychart.dashboard.Dashboard.prototype.getMainChartView = function() {
    return this.mainChartView_;
};
/**
 * @private
 * @type {Object.<String>}
 */
anychart.dashboard.Dashboard.prototype.viewsMap_ = null;
/**
 * Add view to views map.
 * @param {String} key
 * @param {anychart.chartView.DashboardChartView} value
 */
anychart.dashboard.Dashboard.prototype.addView = function(key, value) {
    if (!key || !value) return;
    this.viewsMap_[key] = value;
};
/**
 * @param {String} key
 * @return {anychart.chartView.DashboardChartView}
 */
anychart.dashboard.Dashboard.prototype.getView = function(key) {
    return this.viewsMap_[key];
};
/**
 * @private
 * @type {Array.<anychart.chartView.DashboardChartView>}
 */
anychart.dashboard.Dashboard.prototype.viewList_ = null;
/**
 * @return {Array.<anychart.chartView.DashboardChartView>}
 */
anychart.dashboard.Dashboard.prototype.getViewList = function() {
    return this.viewList_;
};
/**
 * @return {Number}
 */
anychart.dashboard.Dashboard.prototype.getViewCount = function() {
    return this.viewList_.length;
};
//------------------------------------------------------------------------------
//                    Initialization properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.Dashboard.prototype.isInitialized_ = false;
/**
 * @return {Boolean}
 */
anychart.dashboard.Dashboard.prototype.isInitialized = function() {
    return true;
};
/**
 * @return {Boolean}
 */
anychart.dashboard.Dashboard.prototype.isDashboard = function() {
    return true;
};
//------------------------------------------------------------------------------
//                              Margin.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.dashboard.Dashboard.prototype.globalMargin_ = null;
/**
 * @param {anychart.layout.Margin} value
 */
anychart.dashboard.Dashboard.prototype.setMargin = function(value) {
    this.globalMargin_ = value;
};
//------------------------------------------------------------------------------
//                              Getters.
//------------------------------------------------------------------------------
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.dashboard.Dashboard.prototype.getSprite = function() {
    return this.sprite_;
};
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.dashboard.Dashboard.prototype.getBounds = function() {
    return this.bounds_;
};
/**
 * @return {anychart.dashboard.PanelTitle}
 */
anychart.dashboard.Dashboard.prototype.getTitle = function() {
    return this.title_;
};
//------------------------------------------------------------------------------
//                        Deserialization.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.dashboard.Dashboard.prototype.deserializeSuccess_ = false;
/**
 * @return {Boolean}
 */
anychart.dashboard.Dashboard.prototype.isDeserializeSuccess = function() {
    return this.deserializeSuccess_;
};
/**@inheritDoc*/
anychart.dashboard.Dashboard.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;

    var dashboardTemplate = des.copy(
        des.getProp(
            des.getProp(
                anychart.templates.DEFAULT_TEMPLATE,
                'defaults'),
            'dashboard'));
    var panelTemplate = des.copy(
        des.getProp(
            des.getProp(
                anychart.templates.DEFAULT_TEMPLATE,
                'defaults'),
            'panel'));
    var template = anychart.utils.JSONUtils.merge(
        panelTemplate,
        dashboardTemplate);

    data = this.mergeDashboardTemplate(template, data);

    this.deserializeWithoutTemplate(data);

    this.deserializeSuccess_ = true;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.dashboard.Dashboard.prototype.initVisual = function(container, bounds) {
    this.sprite_ = container;
    var tempBounds = bounds.clone();
    if (this.globalMargin_)
        this.globalMargin_.applyInside(tempBounds);

    goog.base(this, 'initVisual', this.svgManager_, tempBounds);
    if (IS_ANYCHART_DEBUG_MOD) this.sprite_.setId('Dashboard');


    this.isInitialized_ = true;
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                       Post initialize.
//------------------------------------------------------------------------------
anychart.dashboard.Dashboard.prototype.postInitialize = function() {
    this.checkPostInitialize();
};
anychart.dashboard.Dashboard.prototype.checkPostInitialize = function() {
    if (!this.isInitialized_) return;
    var i;
    var viewCount = this.viewList_.length;

    for (i = 0; i < viewCount; i++) {
        var view = this.viewList_[i];
        if (view.getChartElement() && !view.getChartElement().isInitialized())
            return;
    }


    for (i = 0; i < viewCount; i++)
        if (this.viewList_[i].getChartElement() && this.viewList_[i].getChartElement().isInitialized())
            this.viewList_[i].getChartElement().postInitialize();
};

//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.dashboard.Dashboard.prototype.resize = function(bounds) {
    var newBounds = bounds.clone();
    if (this.globalMargin_) this.globalMargin_.applyInside(newBounds);

    goog.base(this, 'resize', newBounds);
};
//------------------------------------------------------------------------------
//
//                          DashboardElementFactory.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.dashboard.DashboardElementFactory = function() {
};
/**
 * @static
 * @param {Object} node
 * @param {anychart.dashboard.Dashboard} dashboard
 */
anychart.dashboard.DashboardElementFactory.createElement = function(node, nodeName, dashboard) {
    var des = anychart.utils.deserialization;
    if (nodeName === 'hbox') return new anychart.dashboard.DashboardHBox(dashboard);
    if (nodeName === 'vbox') return new anychart.dashboard.DashboardVBox(dashboard);
    if (nodeName === 'view' && des.hasProp(node, 'type')) {
        var viewType = des.getLStringProp(node, 'type');
        if (viewType == 'panel') return new anychart.dashboard.Panel(dashboard);
        return new anychart.dashboard.ChartElement(dashboard);
    }
    return null;
};
