/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.controls.layout.ControlAlign};</li>
 *  <li>@class {anychart.controls.layout.ControlPosition};</li>
 *  <li>@class {anychart.controls.layout.ControlLayout}.</li>
 * <ul>
 */
goog.provide('anychart.controls.layout');

goog.require('anychart.layout');
//------------------------------------------------------------------------------
//
//                           ControlAlign class.
//
//------------------------------------------------------------------------------
/**
 * @enum {Integer}
 * @return {int} Control align.
 */
anychart.controls.layout.ControlAlign = {
    NEAR: 0,
    CENTER: 1,
    FAR: 2,
    SPREAD: 3,
    deserialize: function(align) {
        switch (align) {
            default:
            case 'near':
                return anychart.controls.layout.ControlAlign.NEAR;
                break;
            case 'center':
                return anychart.controls.layout.ControlAlign.CENTER;
                break;
            case 'far':
                return anychart.controls.layout.ControlAlign.FAR;
                break;
            case 'spread':
                return anychart.controls.layout.ControlAlign.SPREAD;
                break;
        }
    }
};
//------------------------------------------------------------------------------
//
//                           ControlPosition class.
//
//------------------------------------------------------------------------------
/**
 * @enum {Integer}
 * @return {int} Control position.
 */
anychart.controls.layout.ControlPosition = {
    LEFT: 0,
    RIGHT: 1,
    TOP: 2,
    BOTTOM: 3,
    FLOAT: 4,
    FIXED: 5
};
/**
 * @param {String} value
 * @return {int}
 */
anychart.controls.layout.ControlPosition.deserialize = function(value) {
    switch (value) {
        default:
        case 'right':
            return anychart.controls.layout.ControlPosition.RIGHT;
            break;
        case 'left':
            return anychart.controls.layout.ControlPosition.LEFT;
            break;
        case 'top':
            return anychart.controls.layout.ControlPosition.TOP;
            break;
        case 'bottom':
            return anychart.controls.layout.ControlPosition.BOTTOM;
            break;
        case 'float':
            return anychart.controls.layout.ControlPosition.FLOAT;
            break;
        case 'fixed':
            return anychart.controls.layout.ControlPosition.FIXED;
            break;
    }
};
//------------------------------------------------------------------------------
//
//                           ControlLayout class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.controls.layout.ControlLayout = function() {
    this.margin_ = new anychart.layout.Margin(0);
};
//------------------------------------------------------------------------------
//                           Position properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.controls.layout.ControlLayout.prototype.margin_ = null;

/**
 * Getter for margin.
 * @return {anychart.layout.Margin} Margin.
 */
anychart.controls.layout.ControlLayout.prototype.getMargin = function() {
    return this.margin_;
};

/**
 * @private
 * @type {int}
 */
anychart.controls.layout.ControlLayout.prototype.position_ = anychart.controls.layout.ControlPosition.RIGHT;

/**
 * Getter for position.
 * @return {int} Position.
 */
anychart.controls.layout.ControlLayout.prototype.getPosition = function() {
    return this.position_;
};

/**
 * @private
 * @type {int}
 */
anychart.controls.layout.ControlLayout.prototype.align_ =
    anychart.controls.layout.ControlAlign.NEAR;

/**
 * Getter for align.
 * @return {int} Align.
 */
anychart.controls.layout.ControlLayout.prototype.getAlign = function() {
    return this.align_;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.insideDataPlot_ = false;

/**
 * Getter for insideDataPlot.
 * @return {Boolean} Is inside data plot.
 */
anychart.controls.layout.ControlLayout.prototype.isInsideDataPlot = function() {
    return this.insideDataPlot_;
};

/**
 * @private
 * @type {anychart.layout.Anchor}
 */
anychart.controls.layout.ControlLayout.prototype.anchor_ =
    anychart.layout.Anchor.CENTER;

/**
 * Getter for anchor.
 * @return {anychart.layout.Anchor} Anchor.
 */
anychart.controls.layout.ControlLayout.prototype.getAnchor = function() {
    return this.anchor_;
};
//------------------------------------------------------------------------------
//                           Size properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.width_ = null;

/**
 * Setter for width.
 * @param {Number} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setWidth = function(value) {
    this.width_ = value;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isPercentWidth_ = false;

/**
 * Setter for isPercentWidth.
 * @param {Boolean} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setIsPercentWidth =
    function(value) {
        this.isPercentWidth_ = value;
    };

/**
 * Getter for isPercentWidth.
 * @return {Boolean} Is width in percent.
 */
anychart.controls.layout.ControlLayout.prototype.isPercentWidth = function() {
    return this.isPercentWidth_;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isWidthSetted_ = false;

/**
 * Getter for isWidthSetted.
 * @return {Boolean} Is width setted.
 */
anychart.controls.layout.ControlLayout.prototype.isWidthSetted = function() {
    return this.isWidthSetted_;
};

/**
 * Setter for isWidthSetted.
 * @param {Boolean} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setIsWidthSetted =
    function(value) {
        this.isWidthSetted_ = value;
    };

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.height_ = null;

/**
 * Setter for height.
 * @param {Number} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setHeight = function(value) {
    this.height_ = value;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isPercentHeight_ = false;

/**
 * Getter for isPercentHeight.
 * @return {Boolean} Is height in percent.
 */
anychart.controls.layout.ControlLayout.prototype.isPercentHeight = function() {
    return this.isPercentHeight_;
};

/**
 * Setter for isPercentHeight.
 * @param {Boolean} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setIsPercentHeight =
    function(value) {
        this.isPercentHeight_ = value;
    };

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isHeightSetted_ = false;

/**
 * Getter for isHeightSetted.
 * @return {Boolean} Is height setted.
 */
anychart.controls.layout.ControlLayout.prototype.isHeightSetted = function() {
    return this.isHeightSetted_;
};

/**
 * Setter for isHeightSetted.
 * @param {Boolean} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setIsHeightSetted =
    function(value) {
        this.isHeightSetted_ = value;
    };
//------------------------------------------------------------------------------
//                           Size properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.horizontalPadding_ = 0;

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.
    isPercentHorizontalPadding_ = false;

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.pixHorizontalPadding_ = null;

/**
 * Getter for pixHorizontalPadding.
 * @return {Number} Pix horizontal padding.
 */
anychart.controls.layout.ControlLayout.prototype.getPixHorizontalPadding =
    function() {
        return this.pixHorizontalPadding_;
    };

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.verticalPadding_ = 0;

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isPercentVerticalPadding_ =
    false;

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.pixVerticalPadding_ = null;

/**
 * Getter for pixVerticalPadding.
 * @return {Number} Pix vertical padding.
 */
anychart.controls.layout.ControlLayout.prototype.getPixVerticalPadding =
    function() {
        return this.pixVerticalPadding_;
    };

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.maxWidth_ = null;

/**
 * Getter for maxWidth.
 * @return {Number} Max width.
 */
anychart.controls.layout.ControlLayout.prototype.getMaxWidth = function() {
    return this.maxWidth_;
};

/**
 * Setter for maxWidth.
 * @param {Number} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setMaxWidth = function(value) {
    this.maxWidth_ = value;
};

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.maxHeight_ = null;

/**
 * Getter for maxHeight.
 * @return {Number} Max height.
 */
anychart.controls.layout.ControlLayout.prototype.getMaxHeight = function() {
    return this.maxHeight_;

};

/**
 * Setter for maxHeight.
 * @param {Number} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setMaxHeight =
    function(value) {
        this.maxHeight_ = value;
    };
//------------------------------------------------------------------------------
//                           Align properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.HorizontalAlign}
 */
anychart.controls.layout.ControlLayout.prototype.hAlign_ =
    anychart.layout.HorizontalAlign.UNDEFINED;

/**
 * Getter for hAlign.
 * @return {anychart.layout.HorizontalAlign} Horizontal align.
 */
anychart.controls.layout.ControlLayout.prototype.getHAlign = function() {
    return this.hAlign_;
};

/**
 * Setter for hAlign.
 * @param {anychart.layout.HorizontalAlign} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setHAlign = function(value) {
    this.hAlign_ = value;
};

/**
 * @private
 * @type {anychart.layout.VerticalAlign}
 */
anychart.controls.layout.ControlLayout.prototype.vAlign_ =
    anychart.layout.VerticalAlign.UNDEFINED;

/**
 * Getter for vAlign.
 * @return {anychart.layout.VerticalAlign} Vertical align.
 */
anychart.controls.layout.ControlLayout.prototype.getVAlign = function() {
    return this.vAlign_;

};
/**
 * Setter for vAlign.
 * @param {anychart.layout.VerticalAlign} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setVAlign = function(value) {
    this.vAlign_ = value;
};

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.floatX_ = NaN;

/**
 * Getter for floatX.
 * @return {Number} Float x.
 */
anychart.controls.layout.ControlLayout.prototype.getFloatX = function() {
    return this.floatX_;
};

/**
 * Setter for floatX.
 * @param {Number} value Float x.
 */
anychart.controls.layout.ControlLayout.prototype.setFloatX = function(value) {
    this.floatX_ = value;
};

/**
 * @private
 * @type {Number}
 */
anychart.controls.layout.ControlLayout.prototype.floatY_ = NaN;

/**
 * Getter for floatY.
 * @return {Number} Float y.
 */
anychart.controls.layout.ControlLayout.prototype.getFloatY = function() {
    return this.floatY_;
};
/**
 * Setter for floatY.
 * @param {Number} value Float y.
 */
anychart.controls.layout.ControlLayout.prototype.setFloatY = function(value) {
    this.floatY_ = value;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.alignByDataPlot_ = true;

/**
 * Getter for alignByDataPlot.
 * @return {Boolean} Is aligned by data plot.
 */
anychart.controls.layout.ControlLayout.prototype.isAlignByDataPlot =
    function() {
        return this.alignByDataPlot_;
    };

/**
 * Setter for alignByDataPlot.
 * @param {Boolean} value Value.
 */
anychart.controls.layout.ControlLayout.prototype.setAlignByDataPlot =
    function(value) {
        this.alignByDataPlot_ = value;
    };

/**
 * @private
 * @type {Boolean}
 */
anychart.controls.layout.ControlLayout.prototype.isAlignBySet_ = false;

/**
 * Getter for isAlignBySet.
 * @return {Boolean} Is aligned by set.
 */
anychart.controls.layout.ControlLayout.prototype.isAlignBySet = function() {
    return this.isAlignBySet_;
};
//------------------------------------------------------------------------------
//                           Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialization.
 * @param {Object} data JSON object.
 */
anychart.controls.layout.ControlLayout.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;

    if (des.hasProp(data, 'position'))
        this.position_ = anychart.controls.layout.ControlPosition.deserialize(
            des.getEnumProp(data, 'position'));

    if (des.hasProp(data, 'align'))
        this.align_ = anychart.controls.layout.ControlAlign.deserialize(
            des.getEnumProp(data, 'align'));

    if (des.hasProp(data, 'inside_dataplot'))
        this.insideDataPlot_ = des.getBoolProp(data, 'inside_dataplot');

    if (des.hasProp(data, 'anchor'))
        this.anchor_ = anychart.layout.Anchor.deserialize(
            des.getEnumProp(data, 'anchor'));

    var temp;
    if (des.hasProp(data, 'width')) {
        temp = des.getProp(data, 'width');
        this.isPercentWidth_ = des.isPercent(temp);
        this.width_ = this.isPercentWidth_ ?
            des.getPercent(temp) : des.getNum(temp);
        this.isWidthSetted_ = true;
    }

    if (des.hasProp(data, 'height')) {
        temp = des.getProp(data, 'height');
        this.isPercentHeight_ = des.isPercent(temp);
        this.height_ = this.isPercentHeight_ ?
            des.getPercent(temp) : des.getNum(temp);
        this.isHeightSetted_ = true;
    }

    if (des.hasProp(data, 'horizontal_padding')) {
        temp = des.getProp(data, 'horizontal_padding');
        this.isPercentHorizontalPadding_ = des.isPercent(temp);
        this.horizontalPadding_ = this.isPercentHorizontalPadding_ ?
            des.getPercent(temp) : des.getNum(temp);
    }

    if (des.hasProp(data, 'vertical_padding')) {
        temp = des.getProp(data, 'vertical_padding');
        this.isPercentVerticalPadding_ = des.isPercent(temp);
        this.verticalPadding_ = this.isPercentVerticalPadding_ ?
            des.getPercent(temp) : des.getNum(temp);
    }

    if (des.hasProp(data, 'halign'))
        this.hAlign_ = anychart.layout.HorizontalAlign.deserialize(
            des.getEnumProp(data, 'halign'));

    if (des.hasProp(data, 'valign'))
        this.vAlign_ = anychart.layout.VerticalAlign.deserialize(
            des.getEnumProp(data, 'valign'));

    if (this.insideDataPlot_) {
        this.alignByDataPlot_ = true;
        this.isAlignBySet_ = true;
    } else {
        if (des.hasProp(data, 'align_by')) {
            this.alignByDataPlot_ = !(des.getEnumProp(data,
                'align_by') == 'chart');
            this.isAlignBySet_ = true;
        } else if (this.position_ ==
            anychart.controls.layout.ControlPosition.FLOAT ||
            this.position_ ==
                anychart.controls.layout.ControlPosition.FIXED)
            this.alignByDataPlot_ = false;
    }

    if (this.position_ != anychart.controls.layout.ControlPosition.FLOAT && this.position_ != anychart.controls.layout.ControlPosition.FIXED) {

        if (!des.hasProp(data, 'padding')) des.setProp(data, 'padding', 10);
        var padding = des.getNumProp(data, 'padding');

        if (this.insideDataPlot_) {
            switch (this.position_) {
                case anychart.controls.layout.ControlPosition.LEFT:
                    this.margin_.setLeft(padding);
                    break;
                case anychart.controls.layout.ControlPosition.RIGHT:
                    this.margin_.setRight(padding);
                    break;
                case anychart.controls.layout.ControlPosition.TOP:
                    this.margin_.setTop(padding);
                    break;
                case anychart.controls.layout.ControlPosition.BOTTOM:
                    this.margin.setBottom(padding);
                    break;
            }
        } else {
            switch (this.position_) {
                case anychart.controls.layout.ControlPosition.LEFT:
                    this.margin_.setRight(padding);
                    break;
                case anychart.controls.layout.ControlPosition.RIGHT:
                    this.margin_.setLeft(padding);
                    break;
                case anychart.controls.layout.ControlPosition.TOP:
                    this.margin_.setBottom(padding);
                    break;
                case anychart.controls.layout.ControlPosition.BOTTOM:
                    this.margin_.setTop(padding);
                    break;
            }
        }
        if (des.hasProp(data, 'margin'))
            this.margin_.deserialize(des.hasProp(data, 'margin'));
    }
};
//------------------------------------------------------------------------------
//                           Bounds
//------------------------------------------------------------------------------
/**
 * Set bounds.
 * @param {anychart.utils.geom.Rectangle} controlBounds Control bounds.
 * @param {anychart.utils.geom.Rectangle} chartBounds Chart bounds.
 * @param {anychart.utils.geom.Rectangle} plotBounds Plot bounds.
 */
anychart.controls.layout.ControlLayout.prototype.setBounds =
    function(controlBounds, chartBounds, plotBounds) {
        controlBounds.width = null;
        controlBounds.height = null;

        var targetBounds = (this.alignByDataPlot_ ? plotBounds : chartBounds);

        if (this.position_ != anychart.controls.layout.ControlPosition.FLOAT &&
            this.position_ !=
                anychart.controls.layout.ControlPosition.FIXED &&
            !this.insideDataPlot_) {

            switch (this.position_) {
                case anychart.controls.layout.ControlPosition.LEFT:
                case anychart.controls.layout.ControlPosition.RIGHT:
                    if (this.width_)
                        controlBounds.width = this.isPercentWidth_ ?
                            (chartBounds.width * this.width_) :
                            this.width_;

                    if (this.height_)
                        controlBounds.height = this.isPercentHeight_ ?
                            (targetBounds.height * this.height_) :
                            this.height_;
                    break;

                case anychart.controls.layout.ControlPosition.TOP:
                case anychart.controls.layout.ControlPosition.BOTTOM:
                    if (this.width_)
                        controlBounds.width = this.isPercentWidth_ ?
                            (targetBounds.width * this.width_) :
                            this.width_;

                    if (this.height_)
                        controlBounds.height = this.isPercentHeight_ ?
                            (chartBounds.height * this.height_) :
                            this.height_;
                    break;
            }
        } else {
            if (this.width_)
                controlBounds.width = this.isPercentWidth_ ?
                    (targetBounds.width * this.width_) : this.width_;

            if (this.height_)
                controlBounds.height = this.isPercentHeight_ ?
                    (targetBounds.height * this.height_) : this.height_;
        }

        this.pixHorizontalPadding_ = (this.isPercentHorizontalPadding_) ?
            (targetBounds.width * this.horizontalPadding_) :
            this.horizontalPadding_;
        this.pixVerticalPadding_ = (this.isPercentVerticalPadding_) ?
            (targetBounds.height * this.verticalPadding_) :
            this.verticalPadding_;
    };
