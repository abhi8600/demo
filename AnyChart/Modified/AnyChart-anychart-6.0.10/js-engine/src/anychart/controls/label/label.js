/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {}</li>
 * <ul>
 */
goog.provide('anychart.controls.label');
//------------------------------------------------------------------------------
//
//                        LabelControl class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.controls.controlsBase.Control}
 */
anychart.controls.label.LabelControl = function() {
    anychart.controls.controlsBase.Control.call(this);
};
goog.inherits(anychart.controls.label.LabelControl,
    anychart.controls.controlsBase.Control);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.visual.text.InteractiveTitle}
 */
anychart.controls.label.LabelControl.prototype.title_ = null;
/**
 * @private
 * @type {anychart.visual.text.SVGRotatedTextSprite}
 */
anychart.controls.label.LabelControl.prototype.titleSprite_ = null;
//------------------------------------------------------------------------------
//                          Data container.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.label.LabelControl.prototype.initializeDataContainer = function(mainChartView, plot, plotContainer) {
    goog.base(this, 'initializeDataContainer', mainChartView, plot, plotContainer);
    this.title_ = new anychart.visual.text.InteractiveTitle(mainChartView, plot);
};
//------------------------------------------------------------------------------
//                          Deserialize.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.label.LabelControl.prototype.deserialize = function(data, stylesList) {
    var des = anychart.utils.deserialization;
    var parentStyle = des.hasProp(data, 'style') ? des.getProp(data, 'style') : null;
    var styleData = stylesList.getStyleByMerge('label_style', data, parentStyle, false);

    goog.base(this, 'deserialize', styleData, stylesList);

    this.title_.deserialize(styleData);
    this.title_.setBackground(null);
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.controls.label.LabelControl.prototype.initialize = function(svgManager) {
    goog.base(this, 'initialize', svgManager);

    this.title_.initSize(svgManager, this.title_.getTextFormatter().getValue(this.plot_));
    this.titleSprite_ = this.title_.createSVGText(svgManager);
    this.title_.initListeners(this.sprite_);

    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Bounds.
//------------------------------------------------------------------------------
anychart.controls.label.LabelControl.prototype.checkControlBounds = function() {
    this.title_.initSize(this.plot_.getSVGManager(), this.title_.getTextFormatter().getValue(this.plot_));
    var titleBounds = this.title_.getBounds();
    if(!this.bounds_.width) {
        this.bounds_.width = titleBounds.width;
        if(this.title_.getTextAlign() == anychart.layout.HorizontalAlign.CENTER)
            this.bounds_.width += this.title_.getPadding();
    }

    if(!this.bounds_.height)
        this.bounds_.height = titleBounds.height;

    goog.base(this, 'checkControlBounds');
};
//------------------------------------------------------------------------------
//                            Drawing.
//------------------------------------------------------------------------------
anychart.controls.label.LabelControl.prototype.draw = function() {
    goog.base(this, 'draw');
    this.sprite_.appendSprite(this.titleSprite_);
    this.drawTitle();
};
anychart.controls.label.LabelControl.prototype.drawTitle = function() {
    var x = this.contentBounds_.x;
    var y = this.contentBounds_.y;

    switch(this.title_.getTextAlign()) {
        case anychart.layout.HorizontalAlign.LEFT :
            x += this.title_.getPadding();
            break;
        case anychart.layout.HorizontalAlign.CENTER :
            x += (this.contentBounds_.width - this.title_.getBounds().width)/2;
            break;
        case anychart.layout.HorizontalAlign.RIGHT :
            x += this.contentBounds_.width - this.title_.getBounds().width - this.title_.getPadding();
            break;
    }
    this.titleSprite_.setPosition(x, y);
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
anychart.controls.label.LabelControl.prototype.resize = function() {
    goog.base(this, 'resize');
    this.drawTitle();
};
//------------------------------------------------------------------------------
//                          Formatting.
//------------------------------------------------------------------------------