/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.chartView.ChartType}</li>
 *  <li>@class {anychart.chartView.BaseChartView}</li>
 *  <li>@class {anychart.chartView.ChartView}</li>
 *  <li>@class {anychart.chartView.DashboardChartView}</li>
 *  <li>@class {anychart.chartView.MainChartView}</li>
 * <ul>
 * Interfaces:
 * <ul>
 *  <li>@class {anychart.chartView.IChartViewElement}.</li>
 *  <li>@class {anychart.chartView.IAnyChart}.</li>
 * <ul>
 */
goog.provide('anychart.chartView');

goog.require('anychart.events');
goog.require('goog.events.EventTarget');
goog.require('anychart.chart');
goog.require('anychart.dashboard');
goog.require('anychart.resources');
goog.require('anychart.utils');
goog.require('anychart.layout');
goog.require('anychart.visual.text');
goog.require('anychart.templates');
goog.require('anychart.locale');
//------------------------------------------------------------------------------
//
//                          ChartType class.
//
//------------------------------------------------------------------------------
/**
 * Describe possible chart type.
 * @enum {int}
 */
anychart.chartView.ChartType = {
    UNDEFINED:0,
    CHART:1,
    GAUGE:2
};
//------------------------------------------------------------------------------
//
//                          ChartType class.
//
//------------------------------------------------------------------------------
/**
 * Describe possible chart type.
 * @enum {int}
 */
anychart.chartView.ViewType = {
    MAIN_CHART_VIEW:0,
    DASHBOARD_CHART_VIEW:1,
    CHART_VIEW:2
};
//------------------------------------------------------------------------------
//
//                          BaseChartView.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.IAnyChart}
    * @extends {goog.events.EventTarget}
 */
anychart.chartView.BaseChartView = function (mainChartView) {
    goog.events.EventTarget.call(this);
    this.mainChartView_ = mainChartView;
    this.setViewType();
};
goog.inherits(anychart.chartView.BaseChartView, goog.events.EventTarget);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * Instance of a main view witch contains chart element and
 * provide external methods end events.
 * In current realization is anychart.chartView.MainChartView instance.
 * @private
 * @type {anychart.chartView.IAnyChart}
 */
anychart.chartView.BaseChartView.prototype.mainChartView_ = null;
/**
 * @return {anychart.chartView.IAnyChart}
 */
anychart.chartView.BaseChartView.prototype.getMainChartView = function () {
    return this.mainChartView_;
};
/**
 * @private
 * @type {int}
 */
anychart.chartView.BaseChartView.prototype.viewType_ = null;
/**
 * Set view type.
 */
anychart.chartView.BaseChartView.prototype.setViewType = goog.abstractMethod;
//------------------------------------------------------------------------------
//
//                          ChartView class
//
//------------------------------------------------------------------------------
/**
 * This is class for displaying single chart.
 * <p>
 * Main goals:
 * <ul>
 * <li>Show states messages at chart area. E.g. waiting for data, loading data,
 * loading resources, initializing etc</li>
 * <li>Show errors information at chart area.</li>
 * <li>Load external resource for chart</li>
 * <li>Process chart config</li>
 * <li>Show chart</li>
 * <li>Provide bounds to chart during resize</li>
 * </ul>
 *</p>
 * @constructor
 * @extends {anychart.chartView.BaseChartView}
 * @implements {anychart.chartView.IChartView}
 */
anychart.chartView.ChartView = function (mainChartView) {
    anychart.chartView.BaseChartView.call(this, mainChartView);
    this.initResourcesManager();
};
goog.inherits(anychart.chartView.ChartView, anychart.chartView.BaseChartView);
//------------------------------------------------------------------------------
//                      View type.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.chartView.ChartView.prototype.setViewType = function () {
    this.viewType_ = anychart.chartView.ViewType.CHART_VIEW;
};
//------------------------------------------------------------------------------
//                          Visual
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGManager}
 */
anychart.chartView.ChartView.prototype.svgManager_ = null;
/**
 * @private
 * @type {anychart.svg.SVGSprite} ChartView sprite/
 */
anychart.chartView.ChartView.prototype.sprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.chartView.ChartView.prototype.getSprite = function () {
    return this.sprite_;
};
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.chartView.ChartView.prototype.chartElementSprite_ = null;
//------------------------------------------------------------------------------
//                       Bounds.
//------------------------------------------------------------------------------
/**
 * ChartView actual bounds for drawing states and chart
 * @see #initVisual()
 * @private
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.chartView.ChartView.prototype.bounds_ = null;
/**
 * @return {anychart.utils.geom.Rectangle}
 */
anychart.chartView.ChartView.prototype.getBounds = function () {
    return this.bounds_;
};
anychart.chartView.ChartView.prototype.tooltipsSprite_ = null;
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.chartView.ChartView.prototype.getTooltipsSprite = function () {
    return this.tooltipsSprite_;
};
//------------------------------------------------------------------------------
//                        Initialization.
//------------------------------------------------------------------------------
/**
 * @param {anychart.svg.SVGManager} svgManager svg manager.
 * @param {anychart.svg.SVGSprite} container ChartView container.
 * @param {anychart.utils.geom.Rectangle} bounds ChartView bounds.
 */
anychart.chartView.ChartView.prototype.initVisual = function (svgManager, container, bounds) {
    this.svgManager_ = svgManager;
    this.bounds_ = bounds.clone();

    this.sprite_ = new anychart.svg.SVGSprite(svgManager);
    this.chartElementSprite_ = new anychart.svg.SVGSprite(svgManager);
    this.tooltipsSprite_ = new anychart.svg.SVGSprite(svgManager);

    if (IS_ANYCHART_DEBUG_MOD) {
        this.sprite_.setId('ChartView');
        this.chartElementSprite_.setId('ChartElement');
        this.tooltipsSprite_.setId('Tooltips');
    }

    this.sprite_.appendSprite(this.chartElementSprite_);
    this.sprite_.appendSprite(this.tooltipsSprite_);
    this.initMessagesSprite(svgManager);

    //В случае сингл чарта контейнером является корневой svg елемент
    //В случае дашборда спрайт дашборд контейнера.
    if (container.appendSprite) container.appendSprite(this.sprite_);
    else container.appendChild(this.sprite_.getElement());

    this.showWaitingForDataMessage();
};
//------------------------------------------------------------------------------
//                           Resources
//------------------------------------------------------------------------------
/**
 * Resources manager.
 * Used for loading config files and additional required external files
 * @protected
 * @type {anychart.resources.ResourcesManager}
 */
anychart.chartView.ChartView.prototype.resources = null;
/**
 * Initialize chart view resource manager.
 * @protected
 */
anychart.chartView.ChartView.prototype.initResourcesManager = function () {
    //todo:fix this gag,  use google closure events instead.
    this.resources = new anychart.resources.ResourcesManager();
    var ths = this;
    this.resources.onError = function (entry, error) {
        ths.showErrorMessage.call(ths, error.message);
    };
};
//------------------------------------------------------------------------------
//                    Single chart Refresh/Clear
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.chartView.ChartView.prototype.isInRefresh_ = false;
/**@return {Boolean}*/
anychart.chartView.ChartView.prototype.isInRefresh = function () {
    return this.isInRefresh_;
};
/**@param {Boolean} value*/
anychart.chartView.ChartView.prototype.setIsInRefresh = function (value) {
    this.isInRefresh_ = value;
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.refresh = function () {
    if (!this.isDashboard_) {
        this.isInRefresh_ = true;
        this.chartElement_.getPlot().refresh();
    }

};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.clearChartData = function () {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().clear();
};
/**
 *
 */
anychart.chartView.ChartView.prototype.clearStylesList = function () {
    if (!this.isDashboard_ && this.chartElement_)
        this.chartElement_.clearStylesList();
};
//------------------------------------------------------------------------------
//                 Single chart points external methods
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.addPoint = function (seriesId, pointData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().addPoint(seriesId, pointData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.addPointAt = function (seriesId, index, pointData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().addPointAt(seriesId, index, pointData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.removePoint = function (seriesId, pointId) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().removePoint(seriesId, pointId);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.updatePoint = function (seriesId, pointId, pointData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().updatePoint(seriesId, pointId, pointData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.updatePointData = function (groupName, pointName, data) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().updatePointData(groupName, pointName, data);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.highlightPoint = function (seriesId, pointId, highlight) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().highlightPoint(seriesId, pointId, highlight);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.selectPoint = function (seriesId, pointId, select) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().selectPointById(seriesId, pointId, select);
};
//------------------------------------------------------------------------------
//                 Single chart series external methods
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.addSeries = function (seriesData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().addSeries(seriesData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.addSeriesAt = function (index, seriesData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().addSeriesAt(index, seriesData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.removeSeries = function (seriesId) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().removeSeries(seriesId);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.updateSeries = function (seriesId, seriesData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().updateSeries(seriesId, seriesData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.showSeries = function (seriesId, seriesData) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().showSeries(seriesId, seriesData);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.highlightSeries = function (seriesId, highlight) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().highlightSeries(seriesId, highlight);
};
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.highlightCategory = function (categoryName, highlight) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().highlightCategory(categoryName, highlight);
};
//------------------------------------------------------------------------------
//        Single chart custom attributes external methods
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.ChartView.prototype.setPlotCustomAttribute = function (attributeName, attributeValue) {
    if (!this.isDashboard_)
        this.chartElement_.getPlot().setPlotCustomAttribute(attributeName, attributeValue);
};
//------------------------------------------------------------------------------
//                          Config replaces.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Array.<Array.<String>>}
 */
anychart.chartView.ChartView.prototype.replaces_ = null;
/**
 * @private
 * @type {Boolean}
 */
anychart.chartView.ChartView.prototype.needClear_ = true;
//------------------------------------------------------------------------------
//                          XML File loading
//------------------------------------------------------------------------------
/**
 * XML configuration file path
 * @private
 * @type {string}
 */
anychart.chartView.ChartView.prototype.xmlFilePath_ = null;
/**
 * @param {String} path
 */
anychart.chartView.ChartView.prototype.setXmlFilePath = function (path) {
    this.xmlFilePath_ = path;
};
/**
 * Set new configuration (XML file).
 * All existing chart elements will be destroyed.
 *
 * @param {string} path xml config file path (relative to html).
 */
anychart.chartView.ChartView.prototype.setXMLFile = function (path, opt_replaces, opt_needClear) {
    this.replaces_ = opt_replaces;
    this.needClear_ = (opt_needClear != null && opt_needClear != undefined) ? opt_needClear : true;

    this.xmlFilePath_ = path;

    this.resources.stopLoading();
    this.resources.addTextEntry(path, false);

    if (this.resources.isLoaded()) {
        this.setXMLData(anychart.resources.get(path));
    } else {
        var ths = this;
        this.showLoadingConfigMessage();
        this.resources.onLoad = function () {
            ths.onXMLFileLoad_.call(ths);
        };
        this.resources.load();
    }
};
/**
 * XML config load handler.
 *
 * @private
 */
anychart.chartView.ChartView.prototype.onXMLFileLoad_ = function () {
    this.setXMLData(anychart.resources.get(this.xmlFilePath_));
};
//------------------------------------------------------------------------------
//                        JSON File loading
//------------------------------------------------------------------------------
/**
 * JSON configuration file path
 * @private
 * @param {string} path json file path.
 */
anychart.chartView.ChartView.prototype.jsonFilePath_ = null;
/**
 * @param {String} path
 */
anychart.chartView.ChartView.prototype.setJsonFilePath = function (path) {
    this.jsonFilePath_ = path;
};

/**
 * Set new configuration (json file).
 * All existing chart elements will be destroyed.
 *
 * @param {string} path json config file path (relative to html).
 */
anychart.chartView.ChartView.prototype.setJSONFile = function (path) {
    this.jsonFilePath_ = path;

    this.resources.stopLoading();
    this.resources.addTextEntry(path, false);
    if (this.resources.isLoaded()) {
        this.setJSONData(anychart.resources.get(path));
    } else {
        var ths = this;
        this.showLoadingConfigMessage();
        this.resources.onLoad = function () {
            ths.onJSONFileLoad_.call(ths);
        };
        this.resources.load();
    }
};
/**
 * JSON config load handler
 * @private
 */
anychart.chartView.ChartView.prototype.onJSONFileLoad_ = function () {
    this.setJSONData(anychart.resources.get(this.jsonFilePath_));
};
//------------------------------------------------------------------------------
//                          View config
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.chartView.ChartView.prototype.config_ = null;
/**@param {Object} value */
anychart.chartView.ChartView.prototype.setConfig = function (value) {
    this.config_ = value;
};
/**@return {Object}*/
anychart.chartView.ChartView.prototype.getConfig = function () {
    return this.config_;
};
//------------------------------------------------------------------------------
//                  Set xml document with replaces.
//------------------------------------------------------------------------------
/**
 * Set chart config with replaces.
 * @param {String} source
 * @param {Array.<Array.<String>>} opt_replaces
 * @param {Boolean} opt_needClear
 */
anychart.chartView.ChartView.prototype.setSource = function (source, opt_replaces, opt_needClear) {
    this.replaces_ = opt_replaces ? opt_replaces : null;
    this.needClear_ = opt_needClear != undefined ? opt_needClear : true;
    this.setJSON(this.getChartConfig(source));
};
//------------------------------------------------------------------------------
//                          Set config
//------------------------------------------------------------------------------
/**
 * Sets config as XML document ajax reult.
 * @param {Document} document
 */
anychart.chartView.ChartView.prototype.setXMLDocument = function (document) {
    this.setJSON(anychart.utils.deserialization.parseXML(document));
};
/**
 * Sets config as XML string (ajax result)
 * @param {Object} xmlString XML String.
 */
anychart.chartView.ChartView.prototype.setXMLData = function (xmlString) {
    this.setJSON(anychart.utils.deserialization.parseXMLString(xmlString));
};
/**
 * Sets config as JSON string
 * @param {string} data String with JSON.
 */
anychart.chartView.ChartView.prototype.setJSONData = function (data) {
    this.setJSON(eval('(' + data + ')'));
};
/**
 * Apply chart view config, called from dashboard.
 */
anychart.chartView.ChartView.prototype.applyDashboardConfig = function () {
    if (this.config_) this.setJSON(this.config_);
    else if (this.xmlFilePath_) this.setXMLFile(this.xmlFilePath_);
    else if (this.jsonFilePath_) this.setJsonFilePath(this.jsonFilePath_);
    else this.showWaitingForDataMessage();
};
/**
 * Задание конфига для этого chart view
 * @param {Object} data Object with chart json config.
 */
anychart.chartView.ChartView.prototype.setJSON = function (data) {
    if (this.replaces_)
        data = anychart.utils.JSONUtils.applyReplaces(data, this.replaces_);

    this.config_ = data;
    this.showInitializeMessage();

    if (IS_ANYCHART_DEBUG_MOD) {
        this.processConfig();
    } else {
        try {
            this.processConfig();
        } catch (e) {
            this.mainChartView_.dispatchAnychartEvent(e);
            this.showMessage(e.toString());
        }
    }
};
//------------------------------------------------------------------------------
//                    Config processing.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.chartView.ChartView.prototype.chartType_ = null;
/**
 * @param {int} value
 */
anychart.chartView.ChartView.prototype.setChartType = function (value) {
    this.chartType_ = value;
};
/**
 * Process config file
 * @protected
 */
anychart.chartView.ChartView.prototype.processConfig = function () {
    if (!this.config_) {
        this.showNoDataMessage();
        return false;
    }
    this.checkConfig();
    return true;
};
/**
 * Check config on exist charts or gauges nodes.
 * @private
 */
anychart.chartView.ChartView.prototype.checkConfig = function () {
    var des = anychart.utils.deserialization;
    if (!this.chartType_) return;
    var wrapNodeName = this.chartType_ == anychart.chartView.ChartType.CHART ?
        'chart' : 'gauge';
    if (!des.hasProp(this.config_, (wrapNodeName + 's')))
        this.config_ = this.wrapChartConfig_(wrapNodeName, this.config_);
};
/**
 * @private
 * @param {String} wrapNodeName
 */
anychart.chartView.ChartView.prototype.wrapChartConfig_ = function (wrapNodeName, chartConfig) {
    var res = {};
    var charts = {};
    var des = anychart.utils.deserialization;

    des.setProp(res, wrapNodeName + 's', charts);
    des.setProp(charts, wrapNodeName, chartConfig);
    return res;
};
/**
 * Generate chart element
 * @protected
 */
anychart.chartView.ChartView.prototype.generateChart = function () {
    this.createChartElement(this.config_);

    if (this.resources.isLoaded()) {
        this.onExtraResourcesLoaded_();
    } else {
        this.showLoadingResourcesMessage();
        var ths = this;
        this.resources.onLoad = function () {
            ths.onExtraResourcesLoaded_.call(ths);
        };
        this.resources.load();
    }
};
//------------------------------------------------------------------------------
//                          Extra resources
//------------------------------------------------------------------------------
/**
 * Extra resources load handler
 * @private
 */
anychart.chartView.ChartView.prototype.onExtraResourcesLoaded_ = function () {
    this.showChart_();
};
//------------------------------------------------------------------------------
//                          Chart
//------------------------------------------------------------------------------
/**
 * Templates manager
 * @protected
 * @type {anychart.templates.GlobalTemplatesContainer}
 */
anychart.chartView.ChartView.prototype.templatesContainer = null;
/**
 * @return {anychart.templates.GlobalTemplatesContainer}
 */
anychart.chartView.ChartView.prototype.getTemplatesContainer = function () {
    return this.templatesContainer;
};
//------------------------------------------------------------------------------
//                          Messages core.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.chartView.ChartView.prototype.messagesSprite_ = null;
/**
 * @private
 * @type {anychart.visual.text.TextElement}
 */
anychart.chartView.ChartView.prototype.messagesText_ = null;
/**
 * @private
 * @type {String}
 */
anychart.chartView.ChartView.prototype.currentMessageText_ = null;
/**
 * Initialize chart view messages sprite.
 * @protected
 */
anychart.chartView.ChartView.prototype.initMessagesSprite = function (svgManager) {
    if (!this.messagesSprite_) {
        this.messagesSprite_ = new anychart.svg.SVGSprite(svgManager);
        this.messagesText_ = new anychart.visual.text.TextElement();
        this.sprite_.appendSprite(this.messagesSprite_);

        if (IS_ANYCHART_DEBUG_MOD)
            this.messagesSprite_.setId('Messages');
    }
};
/**
 * Show preloader or error message.
 * This method is util method for displaying text.
 * @param {string} text Message text.
 */
anychart.chartView.ChartView.prototype.showMessage = function (text, opt_saveCurrentSettings, opt_rotation) {
    if (opt_saveCurrentSettings !== true) {
        this.messagesText_.setDefaultSettings();
    }
    this.clearMessages();
    this.currentMessageText_ = text;
    this.messagesText_.deserialize({'format':text, 'rotation':opt_rotation});
    this.messagesText_.initSize(this.svgManager_, text);
    this.messagesSprite_.appendSprite(this.messagesText_.createSVGText(this.svgManager_));
    this.setMessagesPosition_(this.messagesText_.getBounds());
};
anychart.chartView.ChartView.prototype.resizeMessages_ = function () {
    if (this.currentMessageText_)
        this.showMessage(this.currentMessageText_, true);
};
/**
 * Remove all messages from chart view.
 * @protected
 */
anychart.chartView.ChartView.prototype.clearMessages = function () {
    if (this.messagesSprite_) this.messagesSprite_.clear();
    this.currentMessageText_ = null;
};
/**
 * @private
 * @param {anychart.utils.geom.Rectangle}
    */
anychart.chartView.ChartView.prototype.setMessagesPosition_ = function (textElementBounds) {
    this.messagesSprite_.setPosition(
        this.bounds_.x + (this.bounds_.width - textElementBounds.width) / 2,
        this.bounds_.y + (this.bounds_.height - textElementBounds.height) / 2);
};
//------------------------------------------------------------------------------
//                      Messages output.
//------------------------------------------------------------------------------
/**
 * Show initializing message.
 */
anychart.chartView.ChartView.prototype.showInitializeMessage = function () {
    this.showMessage(this.mainChartView_.getInitializing());
};
/**
 * Show waiting for data message.
 */
anychart.chartView.ChartView.prototype.showWaitingForDataMessage = function () {
    this.showMessage(this.mainChartView_.getWaitingForData());
};
/**
 * Show no data message.
 */
anychart.chartView.ChartView.prototype.showNoDataMessage = function () {
    var des = anychart.utils.deserialization;
    var noDataSettings = this.mainChartView_.getNoDataSettings();
    var text = (noDataSettings && des.hasProp(noDataSettings, 'text')) ? des.getStringProp(noDataSettings, 'text') : this.mainChartView_.getNoData();
    this.messagesText_.deserialize(this.mainChartView_.getNoDataSettings());
    this.showMessage(text, true, des.getProp(noDataSettings, 'rotation'));
};
/**
 * Show loading resources message.
 */
anychart.chartView.ChartView.prototype.showLoadingResourcesMessage = function () {
    this.showMessage(this.mainChartView_.getLoadingResources());
};
/**
 * Show loading resources message.
 */
anychart.chartView.ChartView.prototype.showLoadingTemplatesMessage = function () {
    this.showMessage(this.mainChartView_.getLoadingTemplates());
};
/**
 * Show loading message.
 * @param {String} opt_message
 */
anychart.chartView.ChartView.prototype.showLoadingConfigMessage = function (opt_message) {
    if (!opt_message) opt_message = this.mainChartView_.getLoadingConfig();
    this.showMessage(opt_message);
};
/**
 * Show error
 * @protected
 * @param {String} message error message.
 */
anychart.chartView.ChartView.prototype.showErrorMessage = function (message) {
    this.showMessage('Error: ' + message);
};
//------------------------------------------------------------------------------
//                  NOT SORTED.
//------------------------------------------------------------------------------
/**
 * @private
 */
anychart.chartView.ChartView.prototype.chartElement_ = null;
/**
 * todo:interaface for chart element
 */
anychart.chartView.ChartView.prototype.getChartElement = function () {
    return this.chartElement_;
};
/**
 * Show chart
 *
 * @private
 */
anychart.chartView.ChartView.prototype.showChart_ = function () {
    //chart view position
    this.clearMessages();
    var chartBounds = this.bounds_.clone();
    this.sprite_.setPosition(chartBounds.x, chartBounds.y);
    //init visual
    this.chartElement_.initVisual(this.chartElementSprite_, chartBounds);

    if (this.chartElement_.isDashboard())
        this.chartElement_.postInitialize();
    else if (this.viewType_ == anychart.chartView.ViewType.DASHBOARD_CHART_VIEW) {
        this.mainChartView_.getChartElement().checkPostInitialize();
    }
    this.dispatchRenderEvent();

    //draw
    this.chartElement_.draw();

    this.dispatchDrawEvent();
};
/**
 * Remove all visual content of ChartView
 */
anychart.chartView.ChartView.prototype.clear = function () {
    this.chartElement_ = null;
    if (this.sprite_ == null) return;
    this.sprite_.clear();
};
//------------------------------------------------------------------------------
//                              Resize.
//------------------------------------------------------------------------------
/**
 * Resize chart view.
 * @param {anychart.utils.geom.Rectangle} bounds New chart view bounds.
 */
anychart.chartView.ChartView.prototype.resize = function (bounds) {
    this.bounds_ = bounds;
    this.resizeMessages_();
    if (!this.chartElement_) return;
    this.chartElement_.resize(bounds);
};
//------------------------------------------------------------------------------
//
//                    DashboardChartView class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @param {anychart.chartView.MainChartView} mainChartView
 * @param {anychart.dashboard.ChartElement} dashboardElement
 * @extends {anychart.chartView.ChartView}
 * @implements {anychart.chartView.IChartViewElement}
 */
anychart.chartView.DashboardChartView = function (mainChartView, dashboardElement) {
    this.dashboardElement_ = dashboardElement;
    this.templatesContainer = mainChartView.getTemplatesContainer();
    anychart.chartView.ChartView.call(this, mainChartView);
};
goog.inherits(anychart.chartView.DashboardChartView, anychart.chartView.ChartView);
//------------------------------------------------------------------------------
//                          Properties.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.chartView.DashboardChartView.prototype.setViewType = function () {
    this.viewType_ = anychart.chartView.ViewType.DASHBOARD_CHART_VIEW;
};
/**
 * @private
 * @type {anychart.dashboard.ChartElement}
 */
anychart.chartView.DashboardChartView.prototype.dashboardElement_ = null;
/**
 * @param {anychart.svg.SVGManager} value
 */
anychart.chartView.DashboardChartView.prototype.setSprite = function (value) {
    this.sprite_ = value;
};
/**
 * @return {anychart.svg.SVGSprite}
 */
anychart.chartView.DashboardChartView.prototype.getSprite = function () {
    return this.sprite_;
};
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.chartView.DashboardChartView.prototype.initVisual = function (svgManager, container, bounds) {
    goog.base(this, 'initVisual', svgManager, container, bounds);
    if (IS_ANYCHART_DEBUG_MOD)
        this.sprite_.setId('DashboardChartView');
};
//------------------------------------------------------------------------------
//                          Config processing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.DashboardChartView.prototype.processConfig = function () {
    if (!goog.base(this, 'processConfig')) return;
    this.generateChart();
};
//------------------------------------------------------------------------------
//                          Create element.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.DashboardChartView.prototype.createChartElement = function () {
    this.isDashboard_ = false;
    this.chartElement_ = new anychart.chart.Chart(this.mainChartView_, this);
    this.chartElement_.initializeSVG(this.svgManager_);
    this.mainChartView_.getPlotFactory().createSinglePLot(this, this.chartElement_, this.config_);
};
/**
 * @inheritDoc
 */
anychart.chartView.DashboardChartView.prototype.showNoDataMessage = function () {
    this.chartElement_.setNoData();
    goog.base(this, 'showNoDataMessage');
};
//------------------------------------------------------------------------------
//                          Events.
//------------------------------------------------------------------------------
/** @inheritDoc */
anychart.chartView.DashboardChartView.prototype.dispatchRenderEvent = function () {
    if (!this.isInRefresh_)
        this.mainChartView_.dispatchAnychartEvent(new anychart.events.DashboardEvent(
            anychart.events.DashboardEvent.DASHBOARD_VIEW_REFRESH,
            this.dashboardElement_.getName()));
};

/** @inheritDoc */
anychart.chartView.DashboardChartView.prototype.dispatchDrawEvent = function () {
    this.execDispatchDrawEvent();
};
/** @inheritDoc */
anychart.chartView.DashboardChartView.prototype.execDispatchDrawEvent = function () {
    this.mainChartView_.dispatchDashboardDrawEvent(this, this.dashboardElement_.getName());
};
//------------------------------------------------------------------------------
//
//                          MainChartView class
//
//------------------------------------------------------------------------------
/**
 * Main chart view. This view is unique for each AnyChart instance.
 *
 * * Current workflow for MainChartView:
 * 1. Show no data
 * 2. Waiting for set config...
 * 3. Load config (if external)
 * 4. Load templates
 * 5. Create chart
 * 6. Deserialize chart configuration
 * 7. Load additional resources specified in configuration
 * 8. Draw chart
 *
 *
 * @constructor
 * @extends {anychart.chartView.ChartView}
 */
anychart.chartView.MainChartView = function () {
    anychart.chartView.ChartView.call(this, this);
    this.templatesContainer = new anychart.templates.GlobalTemplatesContainer();
    this.margin_ = new anychart.layout.Margin(20);
    this.plotFactory_ = new anychart.plots.PlotFactory();
    this.clearChartConfigStorage();
    this.needClear_ = false;
};
goog.inherits(anychart.chartView.MainChartView, anychart.chartView.ChartView);
//------------------------------------------------------------------------------
//                           Initialization.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.chartView.MainChartView.prototype.setViewType = function () {
    this.viewType_ = anychart.chartView.ViewType.MAIN_CHART_VIEW;
};
/**
 * @inheritDoc
 */
anychart.chartView.MainChartView.prototype.initVisual = function (svgManager, container, bounds) {
    goog.base(this, 'initVisual', svgManager, container, bounds);
    if (IS_ANYCHART_DEBUG_MOD)
        this.sprite_.setId('MainChartView');
};
//------------------------------------------------------------------------------
//                        Templates loading.
//------------------------------------------------------------------------------
/**
 * External templates path.
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.templatesPath_ = null;
/**
 * Load external templates
 */
anychart.chartView.MainChartView.prototype.loadTemplates_ = function () {
    var des = anychart.utils.deserialization;
    this.templatesPath_ = null;
    if (des.hasProp(this.config_, 'templates')) {
        var templates = des.getProp(this.config_, 'templates');
        if (des.hasProp(templates, 'path')) {
            this.templatesPath_ = des.getStringProp(templates, 'path');
            this.resources.addXMLEntry(this.templatesPath_, true);
        }
        this.templatesContainer.addTemplates(des.getPropArray(templates, 'template'));
    }

    if (this.resources.isLoaded()) {
        this.generateChart();
    } else {
        this.showLoadingTemplatesMessage();
        var ths = this;
        this.resources.onLoad = function () {
            ths.onTemplatesLoaded_.call(ths);
        };
        this.resources.load();
    }
};
/**
 * External templates loaded handler.
 * @private
 */
anychart.chartView.MainChartView.prototype.onTemplatesLoaded_ = function () {
    var templatesData = anychart.utils.deserialization.parseXML(anychart.resources.get(this.templatesPath_));
    this.templatesContainer.addTemplates(anychart.utils.deserialization.getPropArray(templatesData, 'template'));
    this.generateChart();
};
//------------------------------------------------------------------------------
//                         Set config
//------------------------------------------------------------------------------
/**
 * Все методы по заданию конфига в конечно счете сводятся к setJSON,
 * поэтому отчистка чарта и svg елемента актуально только тут.
 * @param {Object} data
 */
anychart.chartView.MainChartView.prototype.setJSON = function (data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'anychart'))
        data = des.getProp(data, 'anychart');

    if (this.needClear_) this.clearChart_();
    goog.base(this, 'setJSON', data);
    this.needClear_ = true;
};
/**
 * Clear visual and support instance.
 * @private
 */
anychart.chartView.MainChartView.prototype.clearChart_ = function () {
    this.svgManager_.clearDefs();
    this.clearChartConfigStorage();
    this.templatesContainer.clear();
    this.chartElementSprite_.clear();
    this.tooltipsSprite_.clear();
    this.clearMessages();
    if (this.isDashboard_) {
        for (var i in this.viewsMap_) {
            this.viewsMap_[i].clearStylesList();
        }
    } else this.clearStylesList();
};
//------------------------------------------------------------------------------
//                        Plot factory.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.plots.PlotFactory}
 */
anychart.chartView.MainChartView.prototype.plotFactory_ = null;
/**
 * @return {anychart.plots.PlotFactory}
 */
anychart.chartView.MainChartView.prototype.getPlotFactory = function () {
    return this.plotFactory_;
};
//------------------------------------------------------------------------------
//                          Margin.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.layout.Margin}
 */
anychart.chartView.MainChartView.prototype.margin_ = null;
/**
 * @private
 * @type {Object}
 */
anychart.chartView.MainChartView.prototype.cachedMargin_ = null;
//------------------------------------------------------------------------------
//                  Cacheed no data settings.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object}
 */
anychart.chartView.MainChartView.prototype.noDataSettings_ = null;
/**
 * @return {Object}
 */
anychart.chartView.MainChartView.prototype.getNoDataSettings = function () {
    return this.noDataSettings_;
};
//------------------------------------------------------------------------------
//                    Config processing.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.chartView.MainChartView.prototype.processConfig = function () {
    if (!goog.base(this, 'processConfig')) return;
    this.deserializeMargin_();
    this.deserializeSettings_();
    this.cacheConfig_();
    this.loadTemplates_();
};
/**
 * Cache chart config.
 * @private
 */
anychart.chartView.MainChartView.prototype.cacheConfig_ = function () {
    var des = anychart.utils.deserialization;

    if (des.hasProp(this.config_, 'gauges')) {
        this.addChartsConfig(des.getPropArray(
            des.getProp(
                this.config_,
                'gauges'),
            'gauge'),
            anychart.chartView.ChartType.GAUGE);
        this.chartType_ = anychart.chartView.ChartType.GAUGE;
    }


    if (des.hasProp(this.config_, 'charts')) {
        this.addChartsConfig(des.getPropArray(
            des.getProp(
                this.config_,
                'charts'),
            'chart'),
            anychart.chartView.ChartType.CHART);
        this.chartType_ = anychart.chartView.ChartType.CHART;
    }

};
/**
 * Deserialize global margins.
 * @private
 */
anychart.chartView.MainChartView.prototype.deserializeMargin_ = function () {
    var des = anychart.utils.deserialization;
    if (des.hasProp(this.config_, 'margin') && !this.cachedMargin_)
        this.cachedMargin_ = des.getProp(this.config_, 'margin');
    else
        this.cachedMargin_ = anychart.utils.JSONUtils.merge(
            this.cachedMargin_,
            des.getProp(this.config_, 'margin'),
            'margin');

    if (this.cachedMargin_) {
        des.setProp(this.config_, 'margin', this.cachedMargin_);
        this.margin_.deserialize(this.cachedMargin_);
    }
};
/**
 * Deserialize charts locale.
 */
anychart.chartView.MainChartView.prototype.deserializeSettings_ = function () {
    var des = anychart.utils.deserialization;
    this.dateTimeLocale_ = new anychart.locale.DateTimeLocale();
    this.numberLocale_ = new anychart.locale.NumberLocale();

    if (des.hasProp(this.config_, 'settings')) {
        var settings = des.getProp(this.config_, 'settings');

        if (des.hasProp(settings, 'locale')) {
            var locale = des.getProp(settings, 'locale');
            if (des.hasProp(locale, 'date_time_format')) {
                this.dateTimeLocale_.deserialize(des.getProp(locale, 'date_time_format'));
            }
            if (des.hasProp(locale, 'number_format')) {
                this.numberLocale_.deserialize(des.getProp(locale, 'number_format'));
            }
        }
        if (des.hasProp(settings, 'no_data')) {
            var noDataNode = des.getProp(settings, 'no_data');
            if (des.hasProp(noDataNode, 'label')) {
                this.noDataSettings_ = des.getProp(noDataNode, 'label');
            }
        }
    }

    this.dateTimeLocale_.initialize();
};
//------------------------------------------------------------------------------
//                      Chart element
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Boolean}
 */
anychart.chartView.MainChartView.prototype.isDashboard_ = null;
/**@inheritDoc*/
anychart.chartView.MainChartView.prototype.createChartElement = function () {
    if (this.isDashboardConfig_(this.config_)) {
        this.isDashboard_ = true;
        this.drawedViewsCount_ = 0;
        var des = anychart.utils.deserialization;
        this.chartElement_ = new anychart.dashboard.Dashboard(this);
        this.chartElement_.setMargin(this.margin_);
        this.chartElement_.initializeSVG(this.svgManager_);
        this.chartElement_.deserialize(
            des.getProp(
                des.getProp(
                    this.config_,
                    'dashboard'),
                'view'));
    } else {
        this.isDashboard_ = false;
        this.chartElement_ = new anychart.chart.Chart(this, this);
        this.chartElement_.setMargin(this.margin_);
        this.chartElement_.initializeSVG(this.svgManager_);
        this.plotFactory_.createSinglePLot(this, this.chartElement_, this.config_);
    }
};
/**
 * @private
 * @param {Object} data
 * @return {Boolean}
 */
anychart.chartView.MainChartView.prototype.isDashboardConfig_ = function (data) {
    var des = anychart.utils.deserialization;
    return  des.hasProp(data, 'dashboard') &&
        des.hasProp(des.getProp(data, 'dashboard'), 'view');
};
//------------------------------------------------------------------------------
//                  Charts config storage.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {Object.<String>}
 */
anychart.chartView.MainChartView.prototype.chartsConfigStorage_ = null;
/**
 * @private
 * @type {Object.<int>}
 */
anychart.chartView.MainChartView.prototype.chartsTypeStorage_ = null;
/**
 * @private
 */
anychart.chartView.MainChartView.prototype.clearChartConfigStorage = function () {
    this.chartsConfigStorage_ = {};
    this.chartsTypeStorage_ = {};
};
/**
 * @param {Array.<String>} charts
 */
anychart.chartView.MainChartView.prototype.addChartsConfig = function (charts, chartType) {
    var chartsCount = charts.length;
    var des = anychart.utils.deserialization;
    for (var i = 0; i < chartsCount; i++) {
        var chart = charts[i];
        if (chart && des.hasProp(chart, 'name'))
            this.addChartConfig_(des.getProp(chart, 'name'), chart, chartType);
    }
};
/**
 * @private
 * @param {String} chartName
 * @param {Object} chart
 */
anychart.chartView.MainChartView.prototype.addChartConfig_ = function (chartName, chart, chartType) {
    this.chartsConfigStorage_[chartName] = chart;
    this.chartsTypeStorage_[chartName] = chartType;
};
/**
 * @param {String} chartName
 */
anychart.chartView.MainChartView.prototype.getChartConfig = function (chartName) {
    return this.chartsConfigStorage_[chartName];
};
/**
 * @param {String} chartName
 * @return {int}
 */
anychart.chartView.MainChartView.prototype.getChartType = function (chartName) {
    return this.chartsTypeStorage_[chartName];
};
//------------------------------------------------------------------------------
//                          Events
//------------------------------------------------------------------------------
/**
 * @protected
 */
anychart.chartView.MainChartView.prototype.dispatchRenderEvent = function () {
    if (!this.isInRefresh_)
        this.dispatchAnychartEvent(new anychart.events.EngineEvent(
            anychart.events.EngineEvent.ANYCHART_RENDER));
};
/**
 * @protected
 */
anychart.chartView.MainChartView.prototype.dispatchDrawEvent = function () {
    if (!this.chartElement_) return;
    if (this.isDashboard_) {
        if (this.chartElement_.getViewCount() == 0 || this.chartElement_.getViewCount() <= this.drawedViewsCount_)
            this.execDispatchDrawEvent();
    } else {
        this.execDispatchDrawEvent();
    }
};
/**
 * @protected
 */
anychart.chartView.MainChartView.prototype.execDispatchDrawEvent = function () {
    if (this.isInRefresh_) {
        this.dispatchAnychartEvent(new anychart.events.EngineEvent(
            anychart.events.EngineEvent.ANYCHART_REFRESH));
    } else {
        this.dispatchAnychartEvent(new anychart.events.EngineEvent(
            anychart.events.EngineEvent.ANYCHART_DRAW));
    }
    this.isInRefresh_ = false;
};
/**
 * @private
 */
anychart.chartView.MainChartView.prototype.drawedViewsCount_ = null;
/**
 * @param {anychart.chartView.DashboardChartView} view View instance.
 * @param {String} viewName View name.
 */
anychart.chartView.MainChartView.prototype.dispatchDashboardDrawEvent = function (view, viewName) {
    if (view.isInRefresh()) {
        this.dispatchAnychartEvent(new anychart.events.DashboardEvent(
            anychart.events.DashboardEvent.DASHBOARD_VIEW_REFRESH, viewName));
    } else {
        this.dispatchAnychartEvent(new anychart.events.DashboardEvent(
            anychart.events.DashboardEvent.DASHBOARD_VIEW_DRAW, viewName));
    }
    view.setIsInRefresh(false);
    this.drawedViewsCount_++;
};
/**
 * Dispatch event as anychart event.
 * @param {goog.events.Event} event
 */
anychart.chartView.MainChartView.prototype.dispatchAnychartEvent = function (event) {
    goog.events.dispatchEvent(this, event);
};
//------------------------------------------------------------------------------
//                        Locale.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.locale.DateTimeLocale}
 */
anychart.chartView.MainChartView.prototype.dateTimeLocale_ = null;
/**
 * @private
 * @type {anychart.locale.DateTimeLocale}
 */
anychart.chartView.MainChartView.prototype.getDateTimeLocale = function () {
    return this.dateTimeLocale_
};
/**
 * @private
 * @type {anychart.locale.NumberLocale}
 */
anychart.chartView.MainChartView.prototype.numberLocale_ = null;
/**
 * @private
 * @type {anychart.locale.NumberLocale}
 */
anychart.chartView.MainChartView.prototype.getNumberLocale = function () {
    return this.numberLocale_
};
//------------------------------------------------------------------------------
//                          Serialization
//------------------------------------------------------------------------------
/**
 * @return {Object}
 */
anychart.chartView.MainChartView.prototype.serialize = function () {
    return this.chartElement_.serialize();
};
//------------------------------------------------------------------------------
//                  Dashboard data setters.
//------------------------------------------------------------------------------
anychart.chartView.MainChartView.prototype.setViewSource = function (viewId, source, opt_replaces) {
    var data = this.getChartConfig(source);
    var view = this.getDashboardChartView(viewId);

    if (!data || !view) return;

    if (opt_replaces)
        data = anychart.utils.JSONUtils.applyReplaces(data, opt_replaces);

    view.chartElement_.sprite_.clear();
    view.setJSON(data);
};
anychart.chartView.MainChartView.prototype.setViewXMLFile = function (viewId, url, opt_replaces) {
    if (!this.isDashboard_) return;
    var view = this.getDashboardChartView(viewId);
    if (view) view.setXMLFile(url);
};
anychart.chartView.MainChartView.prototype.setViewXMLData = function (viewId, data, opt_replaces) {
    if (!this.isDashboard_) return;
    var view = this.getDashboardChartView(viewId);
    if (view) view.setXMLData(data);

};
//------------------------------------------------------------------------------
//                  Dashboard external methods.
//------------------------------------------------------------------------------
/**
 * Return dashboard chart view from dashboard by viewId
 * @param {String} viewId
 * @return {anychart.chartView.DashboardChartView}
 */
anychart.chartView.MainChartView.prototype.getDashboardChartView = function (viewId) {
    if (!this.isDashboard_) return null;
    var view = this.chartElement_.getView(viewId);
    if (!view || !view.getChartElement() || !view.getChartElement().getPlot()) return null;
    return view;
};
/**
 *
 * @param viewId
 * @param attributeName
 * @param attributeValue
 */
anychart.chartView.MainChartView.prototype.view_setPlotCustomAttribute = function (viewId, attributeName, attributeValue) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.setPlotCustomAttribute(attributeName, attributeValue);
};
/**
 *
 * @param viewId
 * @param seriesData
 */
anychart.chartView.MainChartView.prototype.view_addSeries = function (viewId, seriesData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.addSeries(seriesData);
};
/**
 *
 * @param viewId
 * @param seriesId
 */
anychart.chartView.MainChartView.prototype.view_removeSeries = function (viewId, seriesId) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.removeSeries(seriesId);
};
/**
 *
 * @param viewId
 * @param index
 * @param seriesData
 */
anychart.chartView.MainChartView.prototype.view_addSeriesAt = function (viewId, index, seriesData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.addSeriesAt(index, seriesData);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param seriesData
 */
anychart.chartView.MainChartView.prototype.view_updateSeries = function (viewId, seriesId, seriesData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.updateSeries(seriesId, seriesData);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param seriesData
 */
anychart.chartView.MainChartView.prototype.view_showSeries = function (viewId, seriesId, seriesData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.showSeries(seriesId, seriesData);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointData
 */
anychart.chartView.MainChartView.prototype.view_addPoint = function (viewId, seriesId, pointData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.addPoint(seriesId, pointData);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointIndex
 * @param pointData
 */
anychart.chartView.MainChartView.prototype.view_addPointAt = function (viewId, seriesId, pointIndex, pointData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.addPointAt(seriesId, pointIndex, pointData);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointId
 */
anychart.chartView.MainChartView.prototype.view_removePoint = function (viewId, seriesId, pointId) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.removePoint(seriesId, pointId);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointId
 * @param pointData
 */
anychart.chartView.MainChartView.prototype.view_updatePoint = function (viewId, seriesId, pointId, pointData) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.updatePoint(seriesId, pointId, pointData);
};
/**
 *
 */
anychart.chartView.MainChartView.prototype.view_clearChartData = function (viewId) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.clearChartData();
};
/**
 *
 */
anychart.chartView.MainChartView.prototype.view_refresh = function (viewId) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.refresh();
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param highlight
 */
anychart.chartView.MainChartView.prototype.view_highlightSeries = function (viewId, seriesId, highlight) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.highlightSeries(seriesId, highlight);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointId
 * @param highlight
 */
anychart.chartView.MainChartView.prototype.view_highlightPoint = function (viewId, seriesId, pointId, highlight) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.highlightPoint(viewId, seriesId, pointId, highlight);
};
/**
 *
 * @param viewId
 * @param categoryName
 * @param highlight
 */
anychart.chartView.MainChartView.prototype.view_highlightCategory = function (viewId, categoryName, highlight) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.highlightCategory(categoryName, highlight);
};
/**
 *
 * @param viewId
 * @param seriesId
 * @param pointId
 * @param select
 */
anychart.chartView.MainChartView.prototype.view_selectPoint = function (viewId, seriesId, pointId, select) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.selectPoint(seriesId, pointId, select);
};
/**
 *
 * @param viewId
 * @param groupName
 * @param pointName
 * @param data
 */
anychart.chartView.MainChartView.prototype.updateViewPointData = function (viewId, groupName, pointName, data) {
    var view = this.getDashboardChartView(viewId);
    if (view) view.updatePointData(groupName, pointName, data);
};
//------------------------------------------------------------------------------
//                          Update data
//------------------------------------------------------------------------------
/**
 * @param {String} path XPath
 * @param {Object|String|Document} data
 */
anychart.chartView.MainChartView.prototype.updateData = function (path, data) {
    if (data instanceof Document) {
        data = anychart.utils.deserialization.parseXML(data);
    } else if ((typeof(data) == 'string') && data.charAt(0) == '<') {
        data = anychart.utils.deserialization.parseXMLString(data)
    } else if (!(data instanceof Object)) return;

    this.updateJSONData_(path, data);
    this.setJSON(this.config_);
};
/**
 * @private
 * @param {String} path XPath
 * @param {Object} data
 */
anychart.chartView.MainChartView.prototype.updateJSONData_ = function (path, data) {
    var i = 0;
    var actualData = this.config_;
    var pathLength = path.length;
    var lastNodeName;
    while (i < pathLength) {
        var c = path.charAt(i);
        var tmp, s, j, newData;
        switch (c) {
            case '/':
                i++;
                break;
            case '[':
                s = path.substring(i, path.indexOf(']', i));
                i += s.length + 1;
                if (!goog.isArray(actualData)) continue;
                s = s.substring(1);
                actualData = actualData[s];
                break;
            case '(':
                s = path.substring(i, path.indexOf(')', i));
                i += s.length + 1;
                s = s.substring(1);
                tmp = s.split('=');
                var paramName = tmp[0];
                var paramValue = tmp[1];
                newData = null;
                for (j in actualData) {
                    if (actualData[j][paramName] != undefined && actualData[j][paramName] == paramValue) {
                        if (newData == null) {
                            newData = actualData[j];
                        } else {
                            if (!goog.isArray(newData)) {
                                newData = [newData];
                            }
                            newData.push(actualData[j]);
                        }
                    }
                }
                actualData = newData;
                break;
            default:
                if (c == '.' && i < (path.length - 1) && path.charAt(i + 1) == '.') {
                    tmp = this.readString_(path, i);
                    i = tmp[1];
                    newData = actualData.descendants(tmp[0].substr(2));
                    actualData = newData;
                    break;
                }

                tmp = this.readString_(path, i);
                i = tmp[1];
                lastNodeName = tmp[0];
                if (goog.isArray(actualData)) {
                    actualData = actualData[0][lastNodeName];
                } else actualData = actualData[lastNodeName];
                break;
        }
    }
    if (actualData == null) return;
    data = this.chartElement_.getPlot().checkData(data);
    //merge attributes
    anychart.utils.JSONUtils.mergeSimpleProperty(actualData, data, actualData);
    //merge text and format object (usually not simple)
    if (data['text']) actualData['text'] = data['text'];
    if (data['format']) actualData['text'] = data['format'];
};
/**
 * Reads string from path
 * @private
 * @param {String} data String path string
 * @param {int} startIndex node name start index
 * @return {Array} Array 0 is node name, 1 is the index of the end of node name
 */
anychart.chartView.MainChartView.prototype.readString_ = function (data, startIndex) {
    var c = data.charAt(startIndex);
    var nodeName = "";
    var isNodeName = true;
    var dataLength = data.length;
    while (startIndex < dataLength && isNodeName) {
        nodeName += c;
        startIndex++;
        c = data.charAt(startIndex);
        isNodeName = this.isSymbol_(data, startIndex);
    }
    return [nodeName, startIndex];
};
/**
 * Define, is passed strData a symbol.
 * @private
 * @param data
 * @param index
 */
anychart.chartView.MainChartView.prototype.isSymbol_ = function (data, index) {
    var c = data.charAt(index);
    if (c == '[' || c == ']' || c == '(' || c == ')' || c == '/') return false;
    return !(c == '.' &&
        (index < (data.length - 1)) &&
        data.charAt(index + 1) == '.');
};
//------------------------------------------------------------------------------
//                         Messages.
//------------------------------------------------------------------------------
/**
 * @private
 * @type {String}
 */
anychart.chartView.MainChartView.prototype.watermarkText_ = null;
/**
 * @return {String}
 */
anychart.chartView.MainChartView.prototype.getWatermarkText = function () {
    return this.watermarkText_;
};
/**
 * @param {String} value
 */
anychart.chartView.MainChartView.prototype.setWatermarkText = function (value) {
    this.watermarkText_ = value;
};
/**
 * Custom waiting for data message
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.waitingForData_ = null;
/**
 * @return {string} Waiting for data message.
 */
anychart.chartView.MainChartView.prototype.getWaitingForData = function () {
    return this.waitingForData_;
};
/**
 * @param {string} value Custom waiting for data message.
 */
anychart.chartView.MainChartView.prototype.setWaitingForData = function (value) {
    this.waitingForData_ = value;
};
/**
 * Custom Initializing message
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.initializing_ = null;
/**
 * @return {string} initializing message.
 */
anychart.chartView.MainChartView.prototype.getInitializing = function () {
    return this.initializing_;
};
/**
 * Enabled Mouse Chart Events
 * @private
 * @type {Boolean}
 */
anychart.chartView.MainChartView.prototype.enabledChartMouseEvents_ = false;
/**
 * @param {Boolean} value
 */
anychart.chartView.MainChartView.prototype.setEnabledChartMouseEvents = function (value) {
    this.enabledChartMouseEvents_ = value;
};
/**
 * @return {Boolean} value
 */
anychart.chartView.MainChartView.prototype.isEnabledChartMouseEvents = function () {
    return this.enabledChartMouseEvents_;
};
/**
 * @param {string} value custom initializing message.
 */
anychart.chartView.MainChartView.prototype.setInitializing = function (value) {
    this.initializing_ = value;
};
/**
 * Custom Loading config message
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.loadingConfig_ = null;
/**
 * @return {string} Loading config message.
 */
anychart.chartView.MainChartView.prototype.getLoadingConfig = function () {
    return this.loadingConfig_;
};
/**
 * @param {string} value Custom loading config message.
 */
anychart.chartView.MainChartView.prototype.setLoadingConfig = function (value) {
    this.loadingConfig_ = value;
};
/**
 * Custom loading resources message
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.loadingResources_ = null;
/**
 * @return {string} Loading resources message.
 */
anychart.chartView.MainChartView.prototype.getLoadingResources = function () {
    return this.loadingResources_;
};
/**
 * @param {string} value Custom loading resources message.
 */
anychart.chartView.MainChartView.prototype.setLoadingResources = function (value) {
    this.loadingResources_ = value;
};
/**
 * Custom loading templates message
 *
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.loadingTemplates_ = null;
/**
 * @return {string} Loading templates message.
 */
anychart.chartView.MainChartView.prototype.getLoadingTemplates = function () {
    return this.loadingTemplates_;
};
/**
 * @param {string} value Custom loading templates message.
 */
anychart.chartView.MainChartView.prototype.setLoadingTemplates = function (value) {
    this.loadingTemplates_ = value;
};
/**
 * Custom No Data message
 *
 * @private
 * @type {string}
 */
anychart.chartView.MainChartView.prototype.noData_ = null;
/**
 * @return {string} No Data message.
 */
anychart.chartView.MainChartView.prototype.getNoData = function () {
    return this.noData_;
};
/**
 * @param {string} value Custom No Data Message.
 */
anychart.chartView.MainChartView.prototype.setNoData = function (value) {
    this.noData_ = value;
};
//------------------------------------------------------------------------------
//
//                          IChartViewElement interface
//
//------------------------------------------------------------------------------
/**
 * Can be instance of anychart.chart.Chart or anychart.dashboard.Dashboard.
 * @interface
 */
anychart.chartView.IChartViewElement = function () {
};
anychart.chartView.IChartViewElement.prototype.deserialize = function (data) {
};
anychart.chartView.IChartViewElement.prototype.initVisual = function (container) {
};
anychart.chartView.IChartViewElement.prototype.initSize = function (bounds) {
};
anychart.chartView.IChartViewElement.prototype.getTitle = function () {
};
anychart.chartView.IChartViewElement.prototype.getBounds = function () {
};
anychart.chartView.IChartViewElement.prototype.getSprite = function () {
};
anychart.chartView.IChartViewElement.prototype.setMargin = function (value) {
};
anychart.chartView.IChartViewElement.prototype.isInitialized = function () {
};
//------------------------------------------------------------------------------
//
//                         IAnyChart interface.
//
//------------------------------------------------------------------------------
/**
 * @interface
 * @extends {anychart.chartView.IChartViewElement}
 */
anychart.chartView.IAnyChart = function () {
};
