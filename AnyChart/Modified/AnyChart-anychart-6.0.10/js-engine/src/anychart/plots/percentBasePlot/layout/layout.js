/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.percentBasePlot.layout.Direction}</li>.
 *  <li>@class {anychart.plots.percentBasePlot.layout.MultipleLayouter}</li>.
 * <ul>
 */
goog.provide('anychart.plots.percentBasePlot.layout');
//------------------------------------------------------------------------------
//
//                              Direction enum
//
//------------------------------------------------------------------------------
/**
 * @enum {int}
 */
anychart.plots.percentBasePlot.layout.Direction = {
    VERTICAL: 0,
    HORIZONTAL: 1,
    deserialize : function(value) {
        switch (value) {
            default:
            case 'vertical': return anychart.plots.percentBasePlot.layout.Direction.VERTICAL;
            case 'horizontal': return anychart.plots.percentBasePlot.layout.Direction.HORIZONTAL;
        }
    }
};
//------------------------------------------------------------------------------
//
//                              MultipleLayouter class
//
//------------------------------------------------------------------------------

/**
 * @constructor
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter = function() {
    this.columns_ = -1;
    this.rows_ = -1;
    this.direction_ = anychart.plots.percentBasePlot.layout.Direction.HORIZONTAL;
    this.horizontalPadding_ = 5;
    this.verticalPadding_ = 5;
};
//------------------------------------------------------------------------------
//                          Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {int}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.columns_ = null;
/**
 * Getter for columns.
 * @return {int} Columns.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.getColumns = function() {
    return this.columns_;
};
/**
 * Setter for columns.
 * @param {int} columns Columns.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setColumns = function(columns) {
    this.columns_ = columns;
};
/**
 * @private
 * @type {int}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.rows_ = null;
/**
 * Getter for rows.
 * @return {int} rows.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.getRows = function() {
    return this.rows_;
};
/**
 * Setter for rows.
 * @param {int} rows Columns.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setRows = function(rows) {
    this.rows_ = rows;
};
/**
 * @private
 * @type {anychart.plots.percentBasePlot.layout.Direction}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.direction_ = null;
/**
 * Getter for direction.
 * @return {anychart.plots.percentBasePlot.layout.Direction} direction.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.getDirection = function() {
    return this.direction_;
};
/**
 * Setter for direction.
 * @param {anychart.plots.percentBasePlot.layout.Direction} direction Columns.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setDirection = function(direction) {
    this.direction_ = direction;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.horizontalPadding_ = null;

/**
 * Getter for horizontal padding.
 * @return {int} Horizontal padding.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.getHorizontalPadding = function() {
    return this.horizontalPadding_;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.isPercentHorizontalPadding_ = false;
/**
 * Setter for horizontal padding.
 * @param {int} horizontalPadding Horizontal padding.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setHorizontalPadding = function(horizontalPadding) {
    this.horizontalPadding_ = horizontalPadding;
};
/**
 * @private
 * @type {Number}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.verticalPadding_ = null;
/**
 * Getter for vertical padding.
 * @return {int} Vertical padding.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.getVerticalPadding = function() {
    return this.verticalPadding_;
};
/**
 * Setter for vertical padding.
 * @param {int} verticalPadding Vertical padding.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setVerticalPadding = function(verticalPadding) {
    this.verticalPadding_ = verticalPadding;
};
/**
 * @private
 * @type {Boolean}
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.isPercentVerticalPadding_ = false;
//------------------------------------------------------------------------------
//                      Deserialization
//------------------------------------------------------------------------------
/**
 * Deserialize data about whether series is multiple.
 * @param {Object} data JSON object.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.deserialize = function(data) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'columns'))
        this.columns_ = des.getNumProp(data, 'columns');

    if (des.hasProp(data, 'rows'))
        this.rows_ = des.getNumProp(data, 'rows');

    if (des.hasProp(data, 'direction')) {
        this.direction_ = anychart.plots.percentBasePlot.layout.Direction.deserialize(
                des.getLStringProp(
                        data,
                        'direction'));
    }

    var tempVal;
    if (des.hasProp(data, 'vertical_padding')) {
        tempVal = des.getProp(data, 'vertical_padding');
        if (des.isPercent(tempVal)) {
            this.verticalPadding_ = des.getPercent(tempVal);
            this.isPercentVerticalPadding_ = true;
        } else this.verticalPadding_ = des.getNum(tempVal);
    }

    if (des.hasProp(data, 'horizontal_padding')) {
        tempVal = des.getProp(data, 'horizontal_padding');
        if (des.isPercent(tempVal)) {
            this.horizontalPadding_ = des.getPercent(tempVal);
            this.isPercentHorizontalPadding_ = true;
        } else this.horizontalPadding_ = des.getNum(tempVal);
    }
};
//------------------------------------------------------------------------------
//                           Initialize.
//------------------------------------------------------------------------------
/**
 * Gets the count of rows and columns.
 * @param {Array.<anychart.plots.percentBasePlot.data.PercentBaseSeries>} series
 * Series.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.initialize = function(series) {
    if ((this.columns_ == -1) && (this.rows_ != - 1)) {
        this.columns_ = Math.ceil(series.length / this.rows_);
    }

    if ((this.columns_ == -1) && (this.rows_ == - 1)) {
        this.columns_ = 4;
        if (series.length <= 4) this.columns_ = series.length;
    }

    this.rows_ = Math.ceil(series.length / this.columns_);
};
/**
 * Value of cells in which series draw.
 * @param {Array.<anychart.plots.percentBasePlot.data.PercentBaseSeries>} series
 * Series.
 * @param {anychart.utils.geom.Rectangle} bounds Bounds.
 */
anychart.plots.percentBasePlot.layout.MultipleLayouter.prototype.setSeriesBounds = function(series, bounds) {
    var rowIndex = 0;
    var columnIndex = 0;
    var x = 0;
    var y = 0;

    if (this.isPercentHorizontalPadding_)
        this.horizontalPadding_ *= bounds.width;
    if (this.isPercentVerticalPadding_)
        this.verticalPadding_ *= bounds.height;

    var realWidth = bounds.width - this.horizontalPadding_ * (this.columns_ - 1);
    var realHeight = bounds.height - this.verticalPadding_ * (this.rows_ - 1);

    var seriesItem;
    for (var i = 0; i < series.length; i++) {
        seriesItem = series[i];
        seriesItem.setSeriesBounds(new anychart.utils.geom.Rectangle(
                bounds.x + x,
                bounds.y + y,
                realWidth / this.columns_,
                realHeight / this.rows_));

        if (this.direction_ == anychart.plots.percentBasePlot.layout.Direction.HORIZONTAL) {
            columnIndex++;
            x += seriesItem.getSeriesBounds().width + this.horizontalPadding_;
            if (columnIndex > this.columns_ - 1) {
                columnIndex = 0;
                x = 0;
                y += seriesItem.getSeriesBounds().height + this.verticalPadding_;
                rowIndex++;
            }
        }

        if (this.direction_ == anychart.plots.percentBasePlot.layout.Direction.VERTICAL) {
            rowIndex++;
            y += seriesItem.getSeriesBounds().height + this.verticalPadding_;
            if (rowIndex > this.rows_ - 1) {
                rowIndex = 0;
                y = 0;
                x += seriesItem.getSeriesBounds().width + this.horizontalPadding_;
                columnIndex++;
            }
        }
    }
};