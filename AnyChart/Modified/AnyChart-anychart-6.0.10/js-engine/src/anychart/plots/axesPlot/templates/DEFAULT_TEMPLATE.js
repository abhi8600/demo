//Copyright 2010 AnyChart. All rights reserved.

/**
 * @fileoverview Contains default settings template for AxesPlot.
 * Converted from "$AnyChartTrunk$/src/component/AxesPlot/presets/DefaultTemplate.xml".
 *
 * @author saukham@gmail.com (Anton Saukh)
 */

goog.provide('anychart.plots.axesPlot.templates.DEFAULT_TEMPLATE');

/**
 * Contains default template for all axes plots.
 *
 * @public
 * @const
 * @type {Object.<*>} - JSON
 */
anychart.plots.axesPlot.templates.DEFAULT_TEMPLATE = {
	"chart": {
		"styles": {
			"line_axis_marker_style": {
				"label": {
					"font": {
						"family": "Tahoma",
						"size": "11",
						"bold": "True"
					},
					"format": {
						"value": "{%Value}"
					},
					"background": {
						"border": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(221,221,221)"
									},
									{
										"position": "1",
										"color": "rgb(208,208,208)"
									}
								],
								"angle": "90"
							},
							"enabled": "True",
							"type": "Gradient"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(255,255,255)"
									},
									{
										"position": "0.5",
										"color": "rgb(243,243,243)"
									},
									{
										"position": "1",
										"color": "rgb(255,255,255)"
									}
								],
								"angle": "90"
							},
							"type": "Gradient"
						},
						"inside_margin": {
							"all": "5"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "True",
								"distance": "1",
								"opacity": "0.1"
							},
							"enabled": "True"
						},
						"enabled": "false"
					},
					"enabled": "False",
					"position": "BeforeAxisLabels",
					"align": "Near",
					"text_align": "Left"
				},
				"name": "anychart_default",
				"thickness": "1",
				"color": "#DC0A0A",
				"caps": "Square",
				"joints": "miter",
				"dash_length": "10",
				"space_length": "10"
			},
			"range_axis_marker_style": {
				"label": {
					"format": {
						"value": "{%Minimum} - {%Maximum}"
					},
					"font": {
						"family": "Tahoma",
						"size": "11",
						"color": "rgb(34,34,34)"
					},
					"background": {
						"border": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(221,221,221)"
									},
									{
										"position": "1",
										"color": "rgb(208,208,208)"
									}
								],
								"angle": "90"
							},
							"enabled": "True",
							"type": "Gradient"
						},
						"fill": {
							"gradient": {
								"key": [
									{
										"position": "0",
										"color": "rgb(255,255,255)"
									},
									{
										"position": "0.5",
										"color": "rgb(243,243,243)"
									},
									{
										"position": "1",
										"color": "rgb(255,255,255)"
									}
								],
								"angle": "90"
							},
							"type": "Gradient"
						},
						"inside_margin": {
							"all": "5"
						},
						"effects": {
							"drop_shadow": {
								"enabled": "True",
								"distance": "1",
								"opacity": "0.1"
							},
							"enabled": "True"
						},
						"enabled": "false"
					},
					"enabled": "False",
					"position": "BeforeAxisLabels",
					"text_align": "Left"
				},
				"fill": {
					"color": "%Color",
					"opacity": ".4"
				},
				"minimum_line": {
					"color": "DarkColor(%Color)"
				},
				"maximum_line": {
					"color": "DarkColor(%Color)"
				},
				"name": "anychart_default",
				"color": "Green"
			}
		},
		"chart_settings": {
			"axes": {
				"scroll_bar_settings": {
					"vert_scroll_bar": {
						"fill": {
							"gradient": {
								"key": [
									{
										"color": "#94999B",
										"position": "1"
									},
									{
										"color": "#E7E7E7",
										"position": "0.7"
									},
									{
										"color": "#E7E7E7",
										"position": "0"
									}
								],
								"angle": "0"
							},
							"enabled": "true",
							"type": "Solid",
							"color": "#FEFEFE"
						},
						"border": {
							"enabled": "true",
							"color": "#707173",
							"opacity": "1"
						},
						"effects": {
							"inner_shadow": {
								"enabled": "true",
								"angle": "180",
								"blur_x": "15",
								"blur_y": "0",
								"distance": "7",
								"color": "#777777"
							},
							"enabled": "true"
						},
						"buttons": {
							"background": {
								"corners": {
									"type": "Square",
									"all": "2"
								},
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FDFDFD"
											},
											{
												"color": "#EBEBEB"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#898B8D"
											},
											{
												"color": "#5D5F60"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient",
									"opacity": "1"
								},
								"effects": {
									"drop_shadow": {
										"enabled": "true",
										"distance": "5",
										"opacity": "0.2"
									},
									"enabled": "false"
								}
							},
							"arrow": {
								"fill": {
									"color": "#111111"
								},
								"border": {
									"enabled": "false"
								}
							},
							"states": {
								"hover": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#FDFDFD"
													},
													{
														"color": "#F4F4F4"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0056B8"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								},
								"pushed": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#DFF2FF"
													},
													{
														"color": "#99D7FF"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0056B8"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								}
							}
						},
						"thumb": {
							"background": {
								"corners": {
									"type": "Rounded",
									"all": "0",
									"left_top": "3",
									"left_bottom": "3"
								},
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FFFFFF"
											},
											{
												"color": "#EDEDED"
											}
										],
										"angle": "0"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#ABAEB0"
											},
											{
												"color": "#6B6E6F"
											}
										],
										"angle": "0"
									},
									"enabled": "true",
									"type": "Gradient",
									"color": "#494949",
									"opacity": "1"
								},
								"effects": {
									"drop_shadow": {
										"enabled": "false",
										"distance": "5",
										"opacity": ".2"
									},
									"enabled": "true"
								}
							},
							"states": {
								"hover": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#FFFFFF"
													},
													{
														"color": "#F7F7F7"
													}
												],
												"angle": "0"
											},
											"enabled": "true",
											"type": "gradient",
											"color": "Cyan"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0064C6"
													}
												],
												"angle": "0"
											},
											"type": "Gradient"
										}
									}
								},
								"pushed": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#D8F0FF",
														"position": "0"
													},
													{
														"color": "#ACDEFF",
														"position": "1"
													}
												],
												"angle": "0"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#008AEC"
													}
												],
												"angle": "0"
											},
											"type": "Gradient"
										}
									}
								}
							}
						},
						"size": "16"
					},
					"horz_scroll_bar": {
						"fill": {
							"gradient": {
								"key": [
									{
										"color": "#94999B",
										"position": "0"
									},
									{
										"color": "#E7E7E7",
										"position": "0.7"
									},
									{
										"color": "#E7E7E7",
										"position": "1"
									}
								],
								"angle": "90"
							},
							"enabled": "true",
							"type": "Solid",
							"color": "#FEFEFE"
						},
						"border": {
							"enabled": "true",
							"color": "#707173",
							"opacity": "1"
						},
						"effects": {
							"inner_shadow": {
								"enabled": "true",
								"blur_x": "0",
								"blur_y": "15",
								"distance": "7",
								"color": "#555555"
							},
							"enabled": "true"
						},
						"buttons": {
							"background": {
								"corners": {
									"type": "Square",
									"all": "2"
								},
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FDFDFD"
											},
											{
												"color": "#EBEBEB"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#898B8D"
											},
											{
												"color": "#5D5F60"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient",
									"opacity": "1"
								},
								"effects": {
									"drop_shadow": {
										"enabled": "true",
										"distance": "5",
										"opacity": "0.2"
									},
									"enabled": "false"
								}
							},
							"arrow": {
								"fill": {
									"color": "#111111"
								},
								"border": {
									"enabled": "false"
								}
							},
							"states": {
								"hover": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#FDFDFD"
													},
													{
														"color": "#F4F4F4"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0056B8"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								},
								"pushed": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#DFF2FF"
													},
													{
														"color": "#99D7FF"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0056B8"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								}
							}
						},
						"thumb": {
							"background": {
								"corners": {
									"type": "Rounded",
									"all": "0",
									"left_bottom": "3",
									"right_bottom": "3"
								},
								"fill": {
									"gradient": {
										"key": [
											{
												"color": "#FFFFFF"
											},
											{
												"color": "#EDEDED"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient"
								},
								"border": {
									"gradient": {
										"key": [
											{
												"color": "#ABAEB0"
											},
											{
												"color": "#6B6E6F"
											}
										],
										"angle": "90"
									},
									"enabled": "true",
									"type": "Gradient",
									"color": "#494949",
									"opacity": "1"
								},
								"effects": {
									"drop_shadow": {
										"enabled": "false",
										"distance": "5",
										"opacity": ".2"
									},
									"enabled": "true"
								}
							},
							"states": {
								"hover": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#FFFFFF"
													},
													{
														"color": "#F7F7F7"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "gradient",
											"color": "Cyan"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#0064C6"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								},
								"pushed": {
									"background": {
										"fill": {
											"gradient": {
												"key": [
													{
														"color": "#D8F0FF",
														"position": "0"
													},
													{
														"color": "#ACDEFF",
														"position": "1"
													}
												],
												"angle": "90"
											},
											"enabled": "true",
											"type": "Gradient"
										},
										"border": {
											"gradient": {
												"key": [
													{
														"color": "#009DFF"
													},
													{
														"color": "#008AEC"
													}
												],
												"angle": "90"
											},
											"type": "Gradient"
										}
									}
								}
							}
						},
						"size": "16"
					}
				},
				"x_axis": {
					"major_grid": {
						"enabled": "true",
						"interlaced": "false"
					},
					"minor_grid": {
						"enabled": "true",
						"interlaced": "false"
					},
					"title": {
						"text": {
							"value": " "
						}
					}
				},
				"y_axis": {
					"major_grid": {
						"enabled": "true"
					},
					"minor_grid": {
						"enabled": "true"
					},
					"title": {
						"text": {
							"value": " "
						}
					}
				},
				"z_axis": {
					"enabled": "false"
				}
			},
			"data_plot_background": {
				"effects": {
					"enabled": "False"
				},
				"x_axis_plane": {
					"fill": {
					},
					"border": {
					}
				},
				"y_axis_plane": {
					"fill": {
					},
					"border": {
					}
				}
			}
		}
	},
	"defaults": {
		"axis": {
			"line": {
				"enabled": "True",
				"color": "#474747"
			},
			"zero_line": {
				"enabled": "True",
				"color": "#FF0000",
				"opacity": "0.3"
			},
			"major_grid": {
				"line": {
					"enabled": "true",
					"color": "#C1C1C1",
					"dash_length": "1",
					"space_length": "5"
				},
				"interlaced_fills": {
					"even": {
						"fill": {
							"enabled": "true",
							"color": "#F5F5F5",
							"opacity": "0.5"
						}
					},
					"odd": {
						"fill": {
							"enabled": "true",
							"color": "#FFFFFF",
							"opacity": "0.5"
						}
					}
				},
				"enabled": "false",
				"interlaced": "true"
			},
			"minor_grid": {
				"line": {
					"enabled": "true",
					"color": "#EEEEEE",
					"dash_length": "1",
					"space_length": "10"
				},
				"interlaced_fills": {
					"even": {
						"fill": {
							"enabled": "True",
							"color": "#F5F5F5",
							"opacity": "0.5"
						}
					},
					"odd": {
						"fill": {
							"enabled": "True",
							"color": "#FFFFFF",
							"opacity": "0.5"
						}
					}
				},
				"enabled": "false",
				"interlaced": "false"
			},
			"major_tickmark": {
				"enabled": "true",
				"size": "5",
				"color": "#313131"
			},
			"minor_tickmark": {
				"enabled": "true",
				"size": "2",
				"color": "#3C3C3C"
			},
			"title": {
				"text": {
					"value": "Axis title"
				},
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "rgb(34,34,34)",
					"bold": "True"
				},
				"background": {
					"border": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#DDDDDD"
								},
								{
									"position": "1",
									"color": "#D0D0D0"
								}
							],
							"angle": "90"
						},
						"enabled": "True",
						"type": "Gradient"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#FFFFFF"
								},
								{
									"position": "0.5",
									"color": "#F3F3F3"
								},
								{
									"position": "1",
									"color": "#FFFFFF"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"inside_margin": {
						"all": "5"
					},
					"effects": {
						"drop_shadow": {
							"enabled": "True",
							"distance": "1",
							"opacity": "0.1"
						},
						"enabled": "True"
					},
					"enabled": "false"
				},
				"enabled": "True",
				"text_align": "Center",
				"align": "Center"
			},
			"labels": {
				"format": {
					"value": "{%Value}{numDecimals:2}"
				},
				"background": {
					"border": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#DDDDDD"
								},
								{
									"position": "1",
									"color": "#D0D0D0"
								}
							],
							"angle": "90"
						},
						"enabled": "True",
						"type": "Gradient"
					},
					"fill": {
						"gradient": {
							"key": [
								{
									"position": "0",
									"color": "#FFFFFF"
								},
								{
									"position": "0.5",
									"color": "#F3F3F3"
								},
								{
									"position": "1",
									"color": "#FFFFFF"
								}
							],
							"angle": "90"
						},
						"type": "Gradient"
					},
					"inside_margin": {
						"all": "5"
					},
					"effects": {
						"drop_shadow": {
							"enabled": "True",
							"distance": "1",
							"opacity": "0.1"
						},
						"enabled": "True"
					},
					"enabled": "false"
				},
				"font": {
					"family": "Tahoma",
					"size": "11",
					"color": "rgb(34,34,34)"
				},
				"enabled": "true",
				"align": "inside",
				"position": "outside",
				"display_mode": "normal",
				"multi_line_align": "center",
				"rotation": "0",
				"allow_overlap": "false",
				"show_first_label": "true",
				"show_last_label": "true",
				"show_cross_label": "true"
			},
			"zoom": {
				"enabled": "false"
			},
			"enabled": "true"
		}
	}
};