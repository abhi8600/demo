/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.marker.Marker}</li>
 *  <li>@class {anychart.plots.axesPlot.series.marker.MarkerPoint}</li>
 *  <li>@class {anychart.plots.axesPlot.series.marker.MarkerSeries}</li>
 *  <li>@class {anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.marker');

goog.require('anychart.plots.axesPlot.data');
//------------------------------------------------------------------------------
//
//                          Marker class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.marker.Marker = function() {
    anychart.plots.seriesPlot.elements.MarkerElement.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.marker.Marker,
        anychart.plots.seriesPlot.elements.MarkerElement);
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.marker.Marker.prototype.deserializeGlobalSettings = function(data, stylesList) {
    var styleNodeName = this.getStyleNodeName();
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'style') || des.hasProp(data, styleNodeName)) {
        if (des.hasProp(data, 'style') && !des.hasProp(data, styleNodeName))
            this.deserializeStyleFromApplicator_(stylesList, des.getProp(data, 'style'));
        else {
            var parentStyleName;
            var styleNode;

            if (des.hasProp(data, 'style'))
                parentStyleName = des.getProp(data, 'style');
            if (des.hasProp(data, styleNodeName)) {
                styleNode = des.getProp(data, styleNodeName);
                if (des.hasProp(styleNode, 'parent'))
                    parentStyleName = des.getProp(styleNode, 'parent');
            }

            var actualStyleXML = stylesList.getStyleByMerge(styleNodeName, styleNode, parentStyleName);
            des.setProp(actualStyleXML, 'name', parentStyleName);
            this.style_ = new anychart.elements.marker.styles.MarkerElementStyle();
            this.style_.deserialize(actualStyleXML);
        }
    } else {
        //var cashedStyle = stylesList.getStyle(styleNodeName, 'anychart_default');
        //if (cashedStyle)  this.style_ = cashedStyle;
        //else {
            this.style_ = new anychart.elements.marker.styles.MarkerElementStyle();
            var defaults = des.getPropArray(stylesList.getStylesList(), styleNodeName);
            this.style_.deserialize(anychart.utils.JSONUtils.getNodeByName(defaults, 'anychart_default'));
            stylesList.addStyle(styleNodeName, 'anychart_default', this.style_);
        //}
    }
};
//------------------------------------------------------------------------------
//
//                          MarkerPoint class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.marker.MarkerPoint = function() {
    anychart.plots.axesPlot.data.StackablePoint.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.marker.MarkerPoint,
        anychart.plots.axesPlot.data.StackablePoint);
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
/**
 * @private
 * @type {anychart.utils.geom.Point}
 */
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.pixValuePoint_ = null;
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.deserializeMarkerElement_ = function(data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'style')) {
        if (des.hasProp(data, 'marker'))
            des.setProp(data, 'marker', des.getProp(data, 'style'));
        else {
            var res = {};
            des.setProp(res, 'style', des.getProp(data, 'style'));
            des.setProp(res, '#name#', 'marker');
            des.setProp(data, 'marker', res);
        }
    }

    if (this.marker_.canDeserializeForPoint(des.getProp(data, 'marker'))) {
        this.marker_ = this.marker_.copy();
        this.marker_.deserializePointSettings(des.getProp(data, 'marker'), stylesList);
        this.style_ = this.marker_.getStyle();
    }
    this.marker_.setEnabled(true);
};
//------------------------------------------------------------------------------
//                  Initialization.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.initialize = function(svgManager) {
    goog.base(this, 'initialize', svgManager);
    return this.sprite_;
};
/**@inheritDoc*/
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.initializeStyle = function() {
};
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.calcPixValue = function() {
    this.pixValuePoint_ = new anychart.utils.geom.Point();
    this.series_.getArgumentAxis().transform(this, this.x_, this.pixValuePoint_);
    this.series_.getValueAxis().transform(this, this.stackValue_, this.pixValuePoint_);
};
//------------------------------------------------------------------------------
//                           Resize.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.update = function() {
    this.calcPixValue();
    this.updateElements();
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.resize = function() {
    this.calcPixValue();
    this.resizeElements();
};
//------------------------------------------------------------------------------
//                         Element position
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.setAnchorPoint = function(position, anchor, bounds, padding) {
    position.x = this.pixValuePoint_.x;
    position.y = this.pixValuePoint_.y;
};
//------------------------------------------------------------------------------
//                         Hit test
//------------------------------------------------------------------------------
/**
 * Determine if a rectangle should select this point
 * @public
 * @param {anychart.utils.geom.Rectangle} rect The rectangle defining the selection
 * @return boolean if the rectangle should select the point
 */
anychart.plots.axesPlot.series.marker.MarkerPoint.prototype.isRectSelecting = function (rect) {
    // check for containment of points in rect
    var p = this.pixValuePoint_;
    var pBounds = new anychart.utils.geom.Rectangle(p.x, p.y, 1, 1);
    return rect.containsRect(pBounds);
};
//------------------------------------------------------------------------------
//
//                          MarkerSeries class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.marker.MarkerSeries = function() {
    anychart.plots.axesPlot.data.SingleValueSeries.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.marker.MarkerSeries,
        anychart.plots.axesPlot.data.SingleValueSeries);
//------------------------------------------------------------------------------
//                         Properties
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerSeries.prototype.isClusterizableOnCategorizedAxis = function() {
    return false;
};
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerSeries.prototype.createPoint = function() {
    return new anychart.plots.axesPlot.series.marker.MarkerPoint();
};
anychart.plots.axesPlot.series.marker.MarkerSeries.prototype.deserializeMarkerElement_ = function(data, stylesList) {
    var des = anychart.utils.deserialization;
    if (des.hasProp(data, 'style')) {
        if (des.hasProp(data, 'marker'))
            des.setProp(data, 'marker', des.getProp(data, 'style'));
        else {
            var res = {};
            des.setProp(res, 'style', des.getProp(data, 'style'));
            des.setProp(res, '#name#', 'marker');
            des.setProp(data, 'marker', res);
        }
    }

    if (this.marker_.canDeserializeForSeries(des.getProp(data, 'marker'))) {
        this.marker_ = this.marker_.copy();
        this.marker_.deserializeSeriesSettings(des.getProp(data, 'marker'), stylesList);
        this.style_ = this.marker_.getStyle();
    }
    this.marker_.setEnabled(true);
};
//------------------------------------------------------------------------------
//
//                          MarkerGlobalSeriesSettings class
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings = function() {
    anychart.plots.axesPlot.data.AxesGlobalSeriesSettings.apply(this);
};
goog.inherits(anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings,
        anychart.plots.axesPlot.data.AxesGlobalSeriesSettings);
//------------------------------------------------------------------------------
//                         Deserialization
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.deserializeMarkerElement_ = function(data, stylesList) {
    if (!this.marker_)
        this.marker_ = this.createMarkerElement();
    if (data)
        this.marker_.deserializeGlobalSettings(data, stylesList);
    else
        this.marker_.setStyle(new anychart.styles.Style(this.marker_.getStyleStateClass()));

    this.marker_.setEnabled(true);
    this.style_ = this.marker_.getStyle();
};
//------------------------------------------------------------------------------
//                         Settings
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.createSeries = function(seriesType) {
    var series = new anychart.plots.axesPlot.series.marker.MarkerSeries();
    series.setType(anychart.series.SeriesType.MARKER);
    return series;
};

anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.getSettingsNodeName = function() {
    return 'marker_series';
};

anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.getStyleNodeName = function() {
    return null;
};

anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.getStyleStateClass = function() {
    return null;
};
anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings.prototype.createMarkerElement = function() {
    return new anychart.plots.axesPlot.series.marker.Marker();
};
//------------------------------------------------------------------------------
//                         Icon
//------------------------------------------------------------------------------
//todo:implement icon