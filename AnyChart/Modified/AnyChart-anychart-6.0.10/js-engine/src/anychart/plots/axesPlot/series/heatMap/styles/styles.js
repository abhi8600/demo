/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview
 * File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes}</li>
 *  <li>@class {anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyle}</li>
 * <ul>
 */
goog.provide('anychart.plots.axesPlot.series.heatMap.styles');
//------------------------------------------------------------------------------
//
//                          HeatMapFillShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.heatMap.styles.HeatMapFillShape = function() {
    anychart.styles.background.FillWithStrokeStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.styles.HeatMapFillShape,
        anychart.styles.background.FillWithStrokeStyleShape);
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.heatMap.styles.HeatMapFillShape.prototype.updatePath = function() {
    this.styleShapes_.updateElement(this.element_);
};
//------------------------------------------------------------------------------
//
//                          HeatMapHatchFillShape class.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 */
anychart.plots.axesPlot.series.heatMap.styles.HeatMapHatchFillShape = function() {
    anychart.styles.background.HatchFillStyleShape.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.styles.HeatMapHatchFillShape,
        anychart.styles.background.HatchFillStyleShape);
//------------------------------------------------------------------------------
//                              Drawing.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.heatMap.styles.HeatMapHatchFillShape.prototype.updatePath = function() {
    this.styleShapes_.updateElement(this.element_);
};
//------------------------------------------------------------------------------
//
//                          HeatMapStyleShapes.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyleShapes}
 */
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes = function() {
    anychart.styles.background.BackgroundStyleShapes.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes,
        anychart.styles.background.BackgroundStyleShapes);
//------------------------------------------------------------------------------
//                          Initialization.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes.prototype.createShapes = function() {
    var style = this.point_.getStyle();

    if (style.needCreateFillAndStrokeShape()) {
        this.fillShape_ = new anychart.plots.axesPlot.series.heatMap.styles.HeatMapFillShape();
        this.fillShape_.initialize(this.point_, this);
    }

    if (style.needCreateHatchFillShape()) {
        this.hatchFillShape_ = new anychart.plots.axesPlot.series.heatMap.styles.HeatMapHatchFillShape();
        this.hatchFillShape_.initialize(this.point_, this);
    }
};
//------------------------------------------------------------------------------
//                          Drawing
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes.prototype.update = function(opt_styleState, opt_updateData) {
    if(opt_updateData) this.point_.calculateBounds();
    goog.base(this, 'update', opt_styleState, opt_updateData);
};
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes.prototype.createElement = function() {
    return this.point_.getSVGManager().createPath();
};
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes.prototype.updateElement = function(element) {
    element.setAttribute('d', this.point_.getSVGManager().pRectByBounds(this.point_.getBounds()));
};
//------------------------------------------------------------------------------
//
//                          HeatMapStyle.
//
//------------------------------------------------------------------------------
/**
 * @constructor
 * @extends {anychart.styles.background.BackgroundStyle}
 */
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyle = function() {
    anychart.styles.background.BackgroundStyle.call(this);
};
goog.inherits(anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyle,
        anychart.styles.background.BackgroundStyle);
//------------------------------------------------------------------------------
//                              Shapes.
//------------------------------------------------------------------------------
anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyle.prototype.createStyleShapes = function(point) {
    return new anychart.plots.axesPlot.series.heatMap.styles.HeatMapStyleShapes();
};

