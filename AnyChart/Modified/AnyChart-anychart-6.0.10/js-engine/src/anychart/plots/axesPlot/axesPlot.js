/**
 * Copyright 2011 AnyChart. All rights reserved.
 *
 * @fileoverview File contains classes:
 * <ul>
 *  <li>@class {anychart.plots.axesPlot.AxesPlot}</li>
 * <ul>
 */

//namespace
goog.provide('anychart.plots.axesPlot');

//require
goog.require('anychart.series');
goog.require('anychart.utils');
goog.require('anychart.layout');
goog.require('anychart.plots.seriesPlot');
goog.require('anychart.plots.axesPlot.templates');
goog.require('anychart.plots.axesPlot.axes');
goog.require('anychart.plots.axesPlot.axes.categorization');
goog.require('anychart.plots.axesPlot.axes.categorization.axes');
goog.require('anychart.plots.axesPlot.axes.categorization.axes.labels');
goog.require('anychart.plots.axesPlot.axes.viewPorts');

//series
goog.require('anychart.plots.axesPlot.series.bar');
goog.require('anychart.plots.axesPlot.series.bubble');
goog.require('anychart.plots.axesPlot.series.line');
goog.require('anychart.plots.axesPlot.series.area');
goog.require('anychart.plots.axesPlot.series.stock');
goog.require('anychart.plots.axesPlot.series.marker');
goog.require('anychart.plots.axesPlot.series.heatMap');
goog.require('anychart.plots.axesPlot.series.spline');


//------------------------------------------------------------------------------
//
//                          AxesPlot class
//
//------------------------------------------------------------------------------
/**
 * @public
 * @constructor
 * @extends {anychart.plots.seriesPlot.SeriesPlot}
 */
anychart.plots.axesPlot.AxesPlot = function () {
    anychart.plots.seriesPlot.SeriesPlot.call(this);

    this.defaultSeriesType_ = anychart.series.SeriesType.BAR;
    this.axesList_ = [];
    this.argumentsAxesList_ = [];
    this.argumentAxesMap_ = {};
    this.valueAxesList_ = [];
    this.valueAxesMap_ = {};
    this.xCategoriesList_ = [];
    this.yCategoriesList_ = [];
    this.dataCategoriesList_ = [];
    this.dataCategoriesMap_ = {};
};
//inheritance
goog.inherits(anychart.plots.axesPlot.AxesPlot, anychart.plots.seriesPlot.SeriesPlot);
//------------------------------------------------------------------------------
//                          Common properties
//------------------------------------------------------------------------------
/**
 * Set plot type.
 * @public
 * @override
 * @param {string} value Plot type value.
 */
anychart.plots.axesPlot.AxesPlot.prototype.setPlotType = function (value) {
    goog.base(this, 'setPlotType', value);

    if (value == 'heatmap') this.defaultSeriesType_ = anychart.series.SeriesType.HEAT_MAP;
    this.isHorizontal_ = value.indexOf('horizontal') != -1;
    this.isScatter_ = value == 'scatter';
};

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.isScatter_ = null;

/**
 * Bounds without axes.
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.AxesPlot.prototype.backgroundBounds_ = null;

/**
 * Bounds without axes for outer.
 * @type {anychart.utils.geom.Rectangle}
 */
anychart.plots.axesPlot.AxesPlot.prototype.boundsForCalculatingChartTitle_ = null;

/**
 * Getter for boundsForCalculatingChartTitle_
 * @return {anychart.utils.geom.Rectangle} bounds for calculating.
 */
anychart.plots.axesPlot.AxesPlot.prototype.getBoundsForCalculatingChartTitle = function () {
    return this.boundsForCalculatingChartTitle_;
};

/**
 * @public
 * @return {Boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.isScatter = function () {
    return this.isScatter_;
};

/**
 * @private
 * @type {Boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.isHorizontal_ = null;

/**
 * @public
 * @return {Boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.isHorizontal = function () {
    return this.isHorizontal_;
};

//------------------------------------------------------------------------------
//                          Axes
//------------------------------------------------------------------------------

/**
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.hasLeftAxis_ = false;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.hasRightAxis_ = false;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.hasTopAxis_ = false;

/**
 * @private
 * @type {boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.hasBottomAxis_ = false;

/**
 * @private
 * @type {Array.<anychart.plots.axesPlot.axes.Axis>}
 */
anychart.plots.axesPlot.AxesPlot.prototype.axesList_ = null;

/**
 * @private
 * @type {Array.<anychart.plots.axesPlot.axes.Axis>}
 */
anychart.plots.axesPlot.AxesPlot.prototype.argumentsAxesList_ = null;

/**
 * @private
 * @type {Array.<anychart.plots.axesPlot.axes.Axis>}
 */
anychart.plots.axesPlot.AxesPlot.prototype.valueAxesList_ = null;

/**
 * @protected
 * @type {object.<string, anychart.plots.axesPlot.axes.Axis>}
 */
anychart.plots.axesPlot.AxesPlot.prototype.argumentAxesMap_ = null;

/**
 * @protected
 * @type {object.<string, anychart.plots.axesPlot.axes.Axis>}
 */
anychart.plots.axesPlot.AxesPlot.prototype.valueAxesMap_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.AxesPlot.prototype.outsideXAxesContainer_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.AxesPlot.prototype.outsideYAxesContainer_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.AxesPlot.prototype.axesFixedContainer_ = null;

/**
 * @private
 * @type {anychart.svg.SVGSprite}
 */
anychart.plots.axesPlot.AxesPlot.prototype.axesBottomContainer_ = null;
//------------------------------------------------------------------------------
//                          Locale.
//------------------------------------------------------------------------------
/**
 * @return {anychart.locale.DateTimeLocale}
 */
anychart.plots.axesPlot.AxesPlot.prototype.getDateTimeLocale = function () {
    return this.mainChartView_.getDateTimeLocale();
};
/**
 * @return {anychart.locale.NumberLocale}
 */
anychart.plots.axesPlot.AxesPlot.prototype.getNumberLocale = function () {
    return this.mainChartView_.getNumberLocale();
};
//------------------------------------------------------------------------------
//                          Categorization
//------------------------------------------------------------------------------

anychart.plots.axesPlot.AxesPlot.prototype.xCategoriesList_ = null;
anychart.plots.axesPlot.AxesPlot.prototype.getXCategoriesList = function () {
    return this.xCategoriesList_;
};
anychart.plots.axesPlot.AxesPlot.prototype.addXCategory = function (category) {
    this.xCategoriesList_.push(category);
};

anychart.plots.axesPlot.AxesPlot.prototype.yCategoriesList_ = null;
anychart.plots.axesPlot.AxesPlot.prototype.getYCategoriesList = function () {
    return this.yCategoriesList_;
};
anychart.plots.axesPlot.AxesPlot.prototype.addYCategory = function (category) {
    this.yCategoriesList_.push(category);
};

anychart.plots.axesPlot.AxesPlot.prototype.dataCategoriesList_ = null;
anychart.plots.axesPlot.AxesPlot.prototype.getDataCategoriesList = function () {
    return this.dataCategoriesList_;
};
anychart.plots.axesPlot.AxesPlot.prototype.addDataCategory = function (category) {
    this.dataCategoriesList_.push(category);
};

anychart.plots.axesPlot.AxesPlot.prototype.dataCategoriesMap_ = null;

anychart.plots.axesPlot.AxesPlot.prototype.getDataCategory = function (axis, categoryName) {
    if (this.dataCategoriesMap_[categoryName]) return this.dataCategoriesMap_[categoryName];
    var category = new anychart.plots.axesPlot.axes.categorization.CategoryInfo(this);
    category.setAxis(axis);
    category.setIndex(this.dataCategoriesList_.length);
    category.setName(categoryName);

    this.dataCategoriesList_.push(category);
    this.dataCategoriesMap_[categoryName] = category;

    return category;
};

//------------------------------------------------------------------------------
//                          Template
//------------------------------------------------------------------------------

anychart.plots.axesPlot.AxesPlot.prototype.getTemplatesMerger = function () {
    return new anychart.plots.axesPlot.templates.AxesPlotTemplatesMerger();
};
//------------------------------------------------------------------------------
//                          Factory
//------------------------------------------------------------------------------
anychart.plots.axesPlot.AxesPlot.prototype.getSettingsType = function (seriesType) {
    switch (seriesType) {
        case anychart.series.SeriesType.LINE:
        case anychart.series.SeriesType.STEP_LINE_BACKWARD:
        case anychart.series.SeriesType.STEP_LINE_FORWARD:
            return anychart.series.SeriesType.LINE;
        case anychart.series.SeriesType.AREA:
        case anychart.series.SeriesType.SPLINE_AREA:
        case anychart.series.SeriesType.STEP_AREA_BACKWARD:
        case anychart.series.SeriesType.STEP_AREA_FORWARD:
            return anychart.series.SeriesType.AREA;
        default:
            return seriesType;
    }
};

/**
 * Return global series settings by series type.
 * @protected
 * @override
 * @param {anychart.series.SeriesType} seriesType Series type.
 */
anychart.plots.axesPlot.AxesPlot.prototype.createGlobalSeriesSettings = function (seriesType) {
    switch (seriesType) {
        case anychart.series.SeriesType.BAR:
            return new anychart.plots.axesPlot.series.bar.BarGlobalSeriesSettings();
        case anychart.series.SeriesType.RANGE_BAR:
            return new anychart.plots.axesPlot.series.bar.RangeBarGlobalSeriesSettings();
        case anychart.series.SeriesType.BUBBLE:
            return new anychart.plots.axesPlot.series.bubble.BubbleGlobalSeriesSettings();
        case anychart.series.SeriesType.SPLINE:
            return new anychart.plots.axesPlot.series.spline.SplineGlobalSeriesSettings();
        case anychart.series.SeriesType.LINE:
        case anychart.series.SeriesType.STEP_LINE_BACKWARD:
        case anychart.series.SeriesType.STEP_LINE_FORWARD:
            return new anychart.plots.axesPlot.series.line.LineGlobalSeriesSettings();
        case anychart.series.SeriesType.AREA:
        case anychart.series.SeriesType.SPLINE_AREA:
        case anychart.series.SeriesType.STEP_AREA_BACKWARD:
        case anychart.series.SeriesType.STEP_AREA_FORWARD:
            return new anychart.plots.axesPlot.series.area.AreaGlobalSeriesSettings();
        case anychart.series.SeriesType.RANGE_AREA:
        case anychart.series.SeriesType.RANGE_SPLINE_AREA:
            return new anychart.plots.axesPlot.series.area.RangeAreaGlobalSeriesSettings();
        case anychart.series.SeriesType.OHLC:
            return new anychart.plots.axesPlot.series.stock.OHLCGlobalSeriesSettings();
        case anychart.series.SeriesType.CANDLESTICK:
            return new anychart.plots.axesPlot.series.stock.CandlestickGlobalSeriesSettings();
        case anychart.series.SeriesType.MARKER:
            return new anychart.plots.axesPlot.series.marker.MarkerGlobalSeriesSettings();
        case anychart.series.SeriesType.HEAT_MAP:
            return new anychart.plots.axesPlot.series.heatMap.HeatMapGlobalSeriesSetting();
    }
    return null;
};

/**
 * Return series type.
 * @protected
 * @override
 * @param {string} seriesType
 */
anychart.plots.axesPlot.AxesPlot.prototype.getSeriesType = function (seriesType) {
    if (this.plotType_ == 'heatmap') return anychart.series.SeriesType.HEAT_MAP;
    if (seriesType == null) return this.defaultSeriesType_;
    switch (seriesType) {
        case 'bubble':
            return anychart.series.SeriesType.BUBBLE;
        case 'bar':
            return anychart.series.SeriesType.BAR;
        case 'rangebar':
            return anychart.series.SeriesType.RANGE_BAR;
        case 'rangearea':
            return anychart.series.SeriesType.RANGE_AREA;
        case 'candlestick':
            return anychart.series.SeriesType.CANDLESTICK;
        case 'ohlc':
            return anychart.series.SeriesType.OHLC;
        case 'line':
            return anychart.series.SeriesType.LINE;
        case 'steplineforward':
            return anychart.series.SeriesType.STEP_LINE_FORWARD;
        case 'steplinebackward':
            return anychart.series.SeriesType.STEP_LINE_BACKWARD;
        case 'spline':
            return anychart.series.SeriesType.SPLINE;
        case 'area' :
            return anychart.series.SeriesType.AREA;
        case 'splinearea' :
            return anychart.series.SeriesType.SPLINE_AREA;
        case 'stepareaforward':
        case 'steplineforwardarea':
            return anychart.series.SeriesType.STEP_AREA_FORWARD;
        case 'stepareabackward':
        case 'steplinebackwardarea':
            return anychart.series.SeriesType.STEP_AREA_BACKWARD;
        case 'rangesplinearea':
            return anychart.series.SeriesType.RANGE_SPLINE_AREA;
        case 'marker':
            return anychart.series.SeriesType.MARKER;
        default :
            return this.defaultSeriesType_;
    }
};
//------------------------------------------------------------------------------
//                          Deserialization
//------------------------------------------------------------------------------
/**
 * @protected
 * @override
 * @param {object} config
 * @param {anychart.styles.StylesList} stylesList
 */
anychart.plots.axesPlot.AxesPlot.prototype.deserializeChartSettings_ = function (config, stylesList) {
    goog.base(this, 'deserializeChartSettings_', config, stylesList);
    this.hasLeftAxis_ = false;
    this.hasRightAxis_ = false;
    this.hasTopAxis_ = false;
    this.hasBottomAxis_ = false;

    var des = anychart.utils.deserialization;
    var axesNode = des.getProp(config, 'axes');

    //primary axes
    this.deserializeAxis_(des.getProp(axesNode, 'x_axis'), true, stylesList, 'x_axis');
    this.deserializeAxis_(des.getProp(axesNode, 'y_axis'), true, stylesList, 'y_axis');

    //extra axes
    if (des.hasProp(axesNode, 'extra')) {
        var extraAxisNode = des.getProp(axesNode, 'extra');

        var i;

        if (des.hasProp(extraAxisNode, 'x_axis')) {
            var extraXAxes = des.getPropArray(extraAxisNode, 'x_axis');
            for (i = 0; i < extraXAxes.length; i++)
                this.deserializeAxis_(extraXAxes[i], false, stylesList, 'x_axis');
        }

        if (des.hasProp(extraAxisNode, 'y_axis')) {
            var extraYAxes = des.getPropArray(extraAxisNode, 'y_axis');
            for (i = 0; i < extraYAxes.length; i++)
                this.deserializeAxis_(extraYAxes[i], false, stylesList, 'y_axis');
        }
    }
};

/**
 * @private
 * @param {object} axisNode
 * @param {boolean} isPrimary
 * @param {anychart.styles.StylesList} stylesList
 * @param {string} nodeName
 */
anychart.plots.axesPlot.AxesPlot.prototype.deserializeAxis_ = function (axisNode, isPrimary, stylesList, nodeName) {
    var des = anychart.utils.deserialization;

    //non primary without name - invalid
    if (!isPrimary && !des.hasProp(axisNode, 'name')) return;

    //check axis node name
    if (nodeName != "x_axis" && nodeName != "y_axis") return;

    var isValue = des.hasProp(axisNode, 'scale') && des.getLStringProp(des.getProp(axisNode, 'scale'), 'type') == 'linear';
    var isHeatMap = this.plotType_ == 'heatmap';
    var isCategorizedBySeries = this.plotType_ == 'categorizedbyserieshorizontal' || this.plotType_ == 'categorizedbyseriesvertical';
    var isCategorizedHorizontal = this.plotType_ == 'categorizedhorizontal' || this.plotType_ == 'categorizedbyserieshorizontal';
    var isArgument = nodeName == 'x_axis';

    var axis;

    if (!isHeatMap && (this.isScatter_ || !isArgument || isValue))
        axis = new anychart.plots.axesPlot.axes.ValueAxis();
    else if (!isHeatMap && isCategorizedBySeries)
        axis = new anychart.plots.axesPlot.axes.categorization.axes.CategorizedBySeriesAxis();
    else
        axis = new anychart.plots.axesPlot.axes.categorization.axes.CategorizedAxis();

    axis.setPlot(this);
    var position = null;
    if (des.hasProp(axisNode, 'position')) {
        position = des.getEnumProp(axisNode, 'position');
        if (position == 'bottom' || position == 'right') {
            des.setProp(axisNode, 'position', 'opposite');
            position = 'opposite';
        }
    }
    var isOpposite = (position == "opposite") || (position == null && !isPrimary);
    var isXAxis = (isArgument && (this.isScatter_ || !isCategorizedHorizontal)) ||
        (!isArgument && isCategorizedHorizontal);

    if (isCategorizedHorizontal && isXAxis) isOpposite = !isOpposite;

    axis.setIsArgument(isArgument);
    axis.deserializeScale(axisNode);

    var isCrossAxis = axis.getScale().isCrossingEnabled();
    axis.setIsFirstInList(isCrossAxis);

    if (!isXAxis) {
        if (isOpposite) {
            axis.setPosition(anychart.layout.Position.RIGHT);
            if (!isCrossAxis && !this.hasRightAxis_) {
                axis.setIsFirstInList(true);
                this.hasRightAxis_ = true;
            }
        } else {
            axis.setPosition(anychart.layout.Position.LEFT);
            if (!isCrossAxis && !this.hasLeftAxis_) {
                axis.setIsFirstInList(true);
                this.hasLeftAxis_ = true;
            }
        }
    } else {
        if (isOpposite) {
            axis.setPosition(anychart.layout.Position.TOP);
            if (!isCrossAxis && !this.hasTopAxis_) {
                axis.setIsFirstInList(true);
                this.hasTopAxis_ = true;
            }
        } else {
            axis.setPosition(anychart.layout.Position.BOTTOM);
            if (!isCrossAxis && !this.hasBottomAxis_) {
                axis.setIsFirstInList(true);
                this.hasBottomAxis_ = true;
            }
        }
    }

    var primaryArgumentAxis = this.argumentAxesMap_["primary"];
    var primaryValueAxis = this.valueAxesMap_["primary"];

    axis.setIsOpposite(!isPrimary &&
        (isArgument ? primaryArgumentAxis : primaryValueAxis).getPosition() != axis.getPosition());

    axis.setContainers(this.axesFixedContainer_,
        (isXAxis ? this.outsideXAxesContainer_ : this.outsideYAxesContainer_),
        this.axesBottomContainer_);

    axis.createViewPort(this.getViewPortFactoryClass());
    axis.deserialize(axisNode, stylesList);

    var axisKey = isPrimary ? "primary" : des.getLStringProp(axisNode, "name");
    axis.setName(axisKey);

    if (isCategorizedHorizontal && isArgument)
        axis.getScale().setInverted(!axis.getScale().isInverted());

    if (isCategorizedHorizontal && !isArgument && primaryArgumentAxis.getPosition() == anychart.layout.Position.RIGHT)
        axis.getScale().setInverted(!axis.getScale().isInverted());

    if (isArgument) {
        this.argumentAxesMap_[axisKey] = axis;
        this.argumentsAxesList_.push(axis);
    } else {
        this.valueAxesMap_[axisKey] = axis;
        this.valueAxesList_.push(axis);
    }

    this.axesList_.push(axis);
};

anychart.plots.axesPlot.AxesPlot.prototype.getArgumentAxis_ = function (opt_name) {
    return (opt_name != null && this.argumentAxesMap_[opt_name]) ? this.argumentAxesMap_[opt_name] : this.getArgumentAxis_('primary');
};

anychart.plots.axesPlot.AxesPlot.prototype.getValueAxis_ = function (opt_name) {
    return (opt_name != null && this.valueAxesMap_[opt_name]) ? this.valueAxesMap_[opt_name] : this.getValueAxis_('primary');
};

/**
 * @protected
 * @override
 * @param {anychart.plots.seriesPlot.data.BaseSeries} series
 * @param {Object} seriesNode
 */
anychart.plots.axesPlot.AxesPlot.prototype.deserializeSeriesArguments_ = function (series, seriesNode) {
    var des = anychart.utils.deserialization;

    var argumentAxisName = des.hasProp(seriesNode, 'x_axis') ? des.getLStringProp(seriesNode, 'x_axis') : null;
    var valueAxisName = des.hasProp(seriesNode, 'y_axis') ? des.getLStringProp(seriesNode, 'y_axis') : null;

    var argAxis = this.getArgumentAxis_(argumentAxisName);
    var valAxis = this.getValueAxis_(valueAxisName);

    series.setArgumentAxis(argAxis);
    series.setValueAxis(valAxis);

    argAxis.checkSeries(series, this.svgManager_);
    series.configureValueScale(valAxis.getScale());
};

/**@inheritDoc*/
anychart.plots.axesPlot.AxesPlot.prototype.onAfterDeserializeData = function (config) {
    for (var i = 0; i < this.axesList_.length; i++)
        this.axesList_[i].onAfterDeserializeData(this.drawingPoints_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.sortSeriesByOrder = function (seriesArray) {
    var des = anychart.utils.deserialization;
    var seriesToReplace = [];
    var i, count;

    for (i = 0, count = seriesArray.length; i < count; i++) {
        var series = seriesArray[i];
        if (this.needReplaceSeries(des.getLStringProp(series, 'type'))) {
            seriesToReplace.push(series);
            seriesArray.splice(i, 1);
        }
    }
    if (seriesToReplace.length > 0) {
        for (i = 0, count = seriesToReplace.length; i < count; i++) {
            seriesArray.push(seriesToReplace[i]);
        }
    }

};
/**
 * @protected
 * @param {String} seriesType
 * @return {Boolean}
 */
anychart.plots.axesPlot.AxesPlot.prototype.needReplaceSeries = function (seriesType) {
    return  seriesType &&
        (seriesType == 'line' ||
            seriesType == 'spline' ||
            seriesType == 'steplineforward' ||
            seriesType == 'steplinebackward');
};
/**
 * @protected
 * @return {Function}
 */
anychart.plots.axesPlot.AxesPlot.prototype.getViewPortFactoryClass = function () {
    return anychart.plots.axesPlot.axes.viewPorts.ViewPortFactory;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.getBackgroundBounds = function () {
    return this.backgroundBounds_;
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.getBoundsForCalculatingChartTitle = function () {
    return this.boundsForCalculatingChartTitle_;
};
anychart.plots.axesPlot.AxesPlot.prototype.getLabelsSprite = function (label) {
    if (label.getStyle() &&
        label.getStyle().getNormal() &&
        label.getStyle().getNormal().getAnchor() == anychart.layout.Anchor.X_AXIS)
        return this.isHorizontal() ?
            this.outsideYAxesContainer_ : this.outsideXAxesContainer_;

    else return goog.base(this, 'getLabelsSprite', label);
};
//------------------------------------------------------------------------------
//                          Initialization
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.initializeBottomPlotContainers_ = function () {
    this.sprite_.appendSprite(this.axesBottomContainer_);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.initializeSVG = function (svgManager) {
    goog.base(this, 'initializeSVG', svgManager);
    this.outsideXAxesContainer_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.outsideXAxesContainer_.setId('OutsideXAxisSprite');

    this.outsideYAxesContainer_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.outsideYAxesContainer_.setId('OutsideYAxisSprite');

    this.axesFixedContainer_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.axesFixedContainer_.setId('AxisFixedSprite');

    this.axesBottomContainer_ = new anychart.svg.SVGSprite(svgManager);
    if (IS_ANYCHART_DEBUG_MOD) this.axesBottomContainer_.setId('AxisBottomSprite');
};

anychart.plots.axesPlot.AxesPlot.prototype.initialize = function (bounds, tooltipContainer, opt_drawBackground) {
    this.baseBounds_ = bounds.clone();
    this.initializeAxis_();

    var axis;
    var axesCount = this.axesList_.length;
    var sprite = goog.base(this, 'initialize', bounds, tooltipContainer, opt_drawBackground);

    bounds = bounds.clone();
//    bounds.x = 0;
//    bounds.y = 0;

    sprite.appendSprite(this.outsideXAxesContainer_);
    sprite.appendSprite(this.outsideYAxesContainer_);
    sprite.appendSprite(this.axesFixedContainer_);

    for (var i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        axis.initMarkers();
        axis.applyPointElementsSpace();
    }

    /*this.bounds_ =*/
    this.setAxesSize_(bounds);

    this.cropContainersByPlotBounds_();

    for (i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        axis.initMarkers();
    }

    return sprite;
};
/**
 * Initialize axes
 * @private
 */
anychart.plots.axesPlot.AxesPlot.prototype.initializeAxis_ = function () {
    var i, axis;
    var axesCount = this.axesList_.length;

    for (i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        var scale = axis.getScale();
        if (scale.isCrossingEnabled()) {
            var crossAxis = axis.isArgument() ? this.getValueAxis_() : this.getArgumentAxis_();
            axis.setCrossAxis(crossAxis);
            scale.setCrossingValue(crossAxis.getScale().deserializeValue(scale.getStringCrossingValue()));
        }

        if (!axis.hasData() && !axis.isArgument() && axis.getName() != 'primary') {
            axis.copyScale(this.getValueAxis_());
        }
    }
};
/**
 * @private
 */
anychart.plots.axesPlot.AxesPlot.prototype.cropContainersByPlotBounds_ = function () {
    this.dataSprite_.updateClipPath(this.backgroundBounds_);
    this.markersSprite_.updateClipPath(this.backgroundBounds_);
    this.labelsSprite_.updateClipPath(this.backgroundBounds_);
};
//------------------------------------------------------------------------------
//                          Drawing.
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.draw = function () {
    this.drawAxes_();
    goog.base(this, 'draw');
};
/**
 * Draw plot axes
 * @private
 */
anychart.plots.axesPlot.AxesPlot.prototype.drawAxes_ = function () {
    var i, count;
    for (i = 0, count = this.valueAxesList_.length; i < count; i++)
        this.valueAxesList_[i].drawGridInterlaces();

    for (i = 0, count = this.argumentsAxesList_.length; i < count; i++)
        this.argumentsAxesList_[i].drawGridInterlaces();

    for (i = 0, count = this.valueAxesList_.length; i < count; i++)
        this.valueAxesList_[i].drawMinorGridLines();

    for (i = 0, count = this.argumentsAxesList_.length; i < count; i++)
        this.argumentsAxesList_[i].drawMinorGridLines();

    for (i = 0; i < this.valueAxesList_.length; i++)
        this.valueAxesList_[i].drawMajorGridLines();

    for (i = 0, count = this.argumentsAxesList_.length; i < count; i++)
        this.argumentsAxesList_[i].drawMajorGridLines();

    for (i = 0, count = this.valueAxesList_.length; i < count; i++) {
        this.valueAxesList_[i].drawTickmarks();
        this.valueAxesList_[i].drawLabels();
    }
    for (i = 0, count = this.argumentsAxesList_.length; i < count; i++) {
        this.argumentsAxesList_[i].drawTickmarks();
        this.argumentsAxesList_[i].drawLabels();
    }

    for (i = 0, count; i < this.valueAxesList_.length; i++) {
        this.valueAxesList_[i].drawAxisLine();
        this.valueAxesList_[i].drawZeroLine();
    }
    for (i = 0, count = this.argumentsAxesList_.length; i < count; i++) {
        this.argumentsAxesList_[i].drawAxisLine();
        this.argumentsAxesList_[i].drawZeroLine();
    }

    for (i = 0, count = this.axesList_.length; i < count; i++) {
        this.axesList_[i].drawTitle();
        this.axesList_[i].drawAxisMarkers(this.getSVGManager());
    }
};

anychart.plots.axesPlot.AxesPlot.prototype.setAxesSize_ = function (bounds) {
    var axis;

    var i = 0;
    var axesCount = this.axesList_.length;
    bounds = bounds.clone();
    var axesBounds = bounds.clone();
    axesBounds.x = 0;
    axesBounds.y = 0;

    axesBounds.width *= .75;
    axesBounds.height *= .75;

    for (i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        axis.calculateWidthDefaultSize(axesBounds);
        axis.initTitle();
    }

    axesBounds.width /= .75;
    axesBounds.height /= .75;

    var baseBounds = axesBounds.clone();

    //apply axes fixed offsets
    for (i = 0; i < axesCount; i++)
        this.axesList_[i].applyFixedOffset(axesBounds);

    var initBounds = axesBounds.clone();

    //apply dynamic offsets
    for (i = 0; i < axesCount; i++)
        this.axesList_[i].applyDynamicOffset(axesBounds);

    //recalculate axes
    for (i = 0; i < axesCount; i++)
        this.axesList_[i].recalculateWithDynamicSize(axesBounds);

    axesBounds = initBounds;
    for (i = 0; i < axesCount; i++)
        this.axesList_[i].applyDynamicOffset(axesBounds);

    //labels
    var leftSpace = axesBounds.x - baseBounds.x;
    var topSpace = axesBounds.y - baseBounds.y;
    var rightSpace = baseBounds.width - axesBounds.width - leftSpace;
    var bottomSpace = baseBounds.height - axesBounds.height - topSpace;

    for (i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        var tmpBounds = baseBounds.clone();
        axis.setLabelsSpace(tmpBounds, axesBounds);

        var newLeftSpace = tmpBounds.x - baseBounds.x;
        var newTopSpace = tmpBounds.y - baseBounds.y;
        var newRightSpace = baseBounds.width - tmpBounds.width - newLeftSpace;
        var newBottomSpace = baseBounds.height - tmpBounds.height - newTopSpace;

        if (newLeftSpace > leftSpace) leftSpace = newLeftSpace;
        if (newRightSpace > rightSpace) rightSpace = newRightSpace;
        if (newTopSpace > topSpace) topSpace = newTopSpace;
        if (newBottomSpace > bottomSpace) bottomSpace = newBottomSpace;

        tmpBounds = null;
    }

    axesBounds.x = baseBounds.x + leftSpace;
    axesBounds.y = baseBounds.y + topSpace;
    axesBounds.width = baseBounds.width - leftSpace - rightSpace;
    axesBounds.height = baseBounds.height - topSpace - bottomSpace;

    for (i = 0; i < axesCount; i++)
        this.axesList_[i].setBounds(axesBounds);

    var leftOffset = 0;
    var rightOffset = 0;
    var topOffset = 0;
    var bottomOffset = 0;

    for (i = 0; i < axesCount; i++) {
        axis = this.axesList_[i];
        var offset = 0;
        switch (axis.getPosition()) {
            case anychart.layout.Position.LEFT:
                offset = leftOffset;
                leftOffset += axis.getSpace();
                break;
            case anychart.layout.Position.RIGHT:
                offset = rightOffset;
                rightOffset += axis.getSpace();
                break;
            case anychart.layout.Position.TOP:
                offset = topOffset;
                topOffset += axis.getSpace();
                break;
            case anychart.layout.Position.BOTTOM:
                offset = bottomOffset;
                bottomOffset += axis.getSpace();
                break;
        }
        if (!axis.isFirstInList()) {
            offset += axis.getExtraAdditionalOffset();
        }
        axis.setOffset(offset);
        axis.setBounds(axesBounds);
    }

    this.backgroundBounds_ = axesBounds;

    bounds.x += axesBounds.x;
    bounds.y += axesBounds.y;
    bounds.width = axesBounds.width;
    bounds.height = axesBounds.height;
    this.boundsForCalculatingChartTitle_ = bounds;
    return bounds;
};
//------------------------------------------------------------------------------
//                          Resize.
//------------------------------------------------------------------------------
/**@inheritDoc*/
anychart.plots.axesPlot.AxesPlot.prototype.calculateResize = function (bounds) {
    this.baseBounds_ = bounds.clone();
    bounds = bounds.clone();
//    bounds.x = 0;
//    bounds.y = 0;
    /*bounds = */
    this.setAxesSize_(bounds);
    goog.base(this, 'calculateResize', bounds);
};
anychart.plots.axesPlot.AxesPlot.prototype.execResize = function () {
    this.axesFixedContainer_.clear();
    this.outsideXAxesContainer_.clear();
    this.outsideYAxesContainer_.clear();
    this.axesBottomContainer_.clear();

    goog.base(this, 'execResize');

    this.drawAxes_();
    this.cropContainersByPlotBounds_();
};
//------------------------------------------------------------------------------
//                          Formatting
//------------------------------------------------------------------------------
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.setDefaultTokenValues = function () {
    goog.base(this, 'setDefaultTokenValues');
    this.tokensHash_.add('%DataPlotXMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotXMin', Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotXSum', 0);
    this.tokensHash_.add('%DataPlotXAverage', 0);
    this.tokensHash_.add('%DataPlotYRangeMin', Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotYRangeMax', -Number.MAX_VALUE);
    this.tokensHash_.add('%DataPlotYRangeSum', 0);
};
/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.getTokenType = function (token) {
    switch (token) {
        case '%DataPlotXMax':
        case '%DataPlotXMin':
        case '%DataPlotXSum':
        case '%DataPlotXAverage':
        case '%DataPlotYAverage':
        case '%DataPlotYRangeMin':
        case '%DataPlotYRangeMax':
        case '%DataPlotYRangeSum':
            return anychart.formatting.TextFormatTokenType.NUMBER;
    }
    return goog.base(this, 'getTokenType', token);
};

/**
 * @inheritDoc
 */
anychart.plots.axesPlot.AxesPlot.prototype.getTokenValue = function (token) {
    if (!this.tokensHash_.has('%DataPlotXAverage'))
        this.tokensHash_.add(
            '%DataPlotXAverage',
            this.tokensHash_.get('%DataPlotXSum') /
                this.tokensHash_.get('%DataPlotPointCount'));

    if (!this.tokensHash_.has('%DataPlotYAverage'))
        this.tokensHash_.add(
            '%DataPlotYAverage',
            this.tokensHash_.get('%DataPlotYSum') /
                this.tokensHash_.get('%DataPlotPointCount'));

    return goog.base(this, 'getTokenValue', token);
};
//------------------------------------------------------------------------------
//                          Serialize
//------------------------------------------------------------------------------
anychart.plots.axesPlot.AxesPlot.prototype.serialize = function (opt_target) {
    opt_target = goog.base(this, 'serialize', opt_target);
    opt_target['YRangeMax'] = this.tokensHash_.get('%DataPlotRangeMax');
    opt_target['YRangeMin'] = this.tokensHash_.get('%DataPlotRangeMin');
    opt_target['YRangeSum'] = this.tokensHash_.get('%DataPlotRangeSum');
    opt_target['XSum'] = this.tokensHash_.get('%DataPlotXSum');
    opt_target['XMax'] = this.tokensHash_.get('%DataPlotXMax');
    opt_target['XMin'] = this.tokensHash_.get('%DataPlotXMin');
    opt_target['XAverage'] = this.tokensHash_.get('%DataPlotXSum') / this.tokensHash_.get('%DataPlotPointCount');

    opt_target['Categories'] = [];

    var i;
    var categoryCount = this.dataCategoriesList_.length;
    for (i = 0; i < categoryCount; i++) {
        var category = this.dataCategoriesList_[i];
        if (category) opt_target['Categories'].push(category.serialize());
    }

    opt_target['XAxes'] = {};
    opt_target['YAxes'] = {};
    var axis;
    var argumentAxisCount = this.argumentsAxesList_.length;
    var valueAxisCount = this.valueAxesList_.length;
    for (i = 0; i < argumentAxisCount; i++) {
        axis = this.argumentsAxesList_[i];
        opt_target['XAxes'][axis.getName()] = axis.serialize();
    }
    for (i = 0; i < valueAxisCount; i++) {
        axis = this.valueAxesList_[i];
        opt_target['YAxes'][axis.getName()] = axis.serialize();
    }
    return opt_target;
};
//------------------------------------------------------------------------------
//                          External series methods
//------------------------------------------------------------------------------
anychart.plots.axesPlot.AxesPlot.prototype.highlightCategory = function (categoryName, highlight) {
    var category;
    for (var i in this.categoriesMap_) {
        category = this.categoriesMap_[i];
        if (category.getName == categoryName) break;
    }

    if (category) {
        if (highlight) {
            category.setHover();
        } else {
            category.setNormal();
        }
    }

};
